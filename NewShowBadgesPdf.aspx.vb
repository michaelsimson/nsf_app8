Imports System.IO
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports NorthSouth.BAL
Imports System.Data.SqlClient
Imports System
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.UI

Partial Class NewShowBadgesPdf
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateBadges
    Protected lblHeader1 As Label
    Protected lblHeader2 As Label
    Protected lblName As Label
    Protected lblBadgeNumber As Label
    Protected lblTshirt_Food As Label
    Protected lblWeekDay As Label
    Protected lblChildLocation As Label
    Protected lblGrade As Label
    Protected lblParentChildID As Label
    Protected lblChapterCode As Label
    Protected lbltest As Label
    ' Protected imgPhotoBadtes As System.Web.UI.WebControls.Image
    Protected imgPhotoBadtes As System.Web.UI.HtmlControls.HtmlImage
    Protected imageBarCode As System.Web.UI.HtmlControls.HtmlImage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Session("SelChapterID").ToString() = "1" Then
                pnlDataNat.Visible = True
                pnlData.Visible = False
                LoadNationalBadges()
            Else
                pnlDataNat.Visible = False
                pnlData.Visible = True
                LoadBadges()
            End If
        End If
    End Sub

    Private Sub LoadBadges()
        Dim dsBadge As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        Dim j As Integer
        Dim iBadgeCount As Integer
        Dim iPerPageCount As Integer
        Dim strBandgeNumber As String
        generateBadge = CType(Context.Handler, GenerateBadges)
        dsBadge = badge.GetChildsHavingBadges(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates)
        If dsBadge.Tables(0).Rows.Count > 0 Then
            iPerPageCount = 10
            j = 1
            For i = 1 To dsBadge.Tables(0).Rows.Count
                iBadgeCount = 0
                strBandgeNumber = ""
                lblHeader1 = New Label
                lblHeader2 = New Label
                lblName = New Label
                lblChapterCode = New Label
                lbltest = New Label
                Dim dsBadgeNumber As New DataSet
                If i Mod 2 = 1 Then
                    BadgeHolder.Controls.Add(New LiteralControl("<tr style='height:3.0in;' >"))
                    BadgeHolder.Controls.Add(New LiteralControl("<td align='center' style='width:4.0in;padding:0in .75pt 0in .75pt;height:3.0in'>"))
                    lblHeader1.Text = "North South Foundation"
                    lblHeader1.Font.Bold = True
                    lblHeader1.ForeColor = Color.Navy
                    lblHeader1.Font.Name = "Verdana"
                    lblHeader1.Font.Size = 16
                    BadgeHolder.Controls.Add(lblHeader1)
                    lblHeader2.Text = "Regional Educational Contests"
                    lblHeader2.Font.Name = "Times New Roman"
                    lblHeader2.Font.Size = 11
                    lblHeader2.ForeColor = Color.Navy
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(lblHeader2)
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    lblName.Text = dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & " " & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()
                    lblName.Font.Name = "Arial"
                    lblName.Font.Bold = True
                    If Len(lblName.Text) > 22 Then
                        lblName.Font.Size = 12
                    Else
                        lblName.Font.Size = 15
                    End If
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(lblName)
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    dsBadgeNumber = badge.ShowBadge(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, Convert.ToDouble(dsBadge.Tables(0).Rows(i - 1)("ChildNumber")))
                    For iBadgeCount = 0 To dsBadgeNumber.Tables(0).Rows.Count - 1
                        lblBadgeNumber = New Label
                        If dsBadgeNumber.Tables(0).Rows.Count > 3 And dsBadgeNumber.Tables(0).Rows.Count <= 5 Then
                            lblBadgeNumber.Font.Size = 12
                            lblBadgeNumber.Font.Bold = True
                        ElseIf dsBadgeNumber.Tables(0).Rows.Count > 5 Then
                            lblBadgeNumber.Font.Size = 10
                            lblBadgeNumber.Font.Bold = True
                        Else
                            lblBadgeNumber.Font.Size = 15
                            lblBadgeNumber.Font.Bold = True
                        End If
                        lblBadgeNumber.Text = dsBadgeNumber.Tables(0).Rows(iBadgeCount)("BadgeNumber").ToString()
                        If dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "SB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "VB" Then
                            lblBadgeNumber.ForeColor = Color.Blue
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "MB" Then
                            lblBadgeNumber.ForeColor = Color.Red
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "EW" Then
                            lblBadgeNumber.ForeColor = Color.DeepPink
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "PS" Then
                            lblBadgeNumber.ForeColor = Color.Orange
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "BB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "GB" Then
                            lblBadgeNumber.ForeColor = Color.Black
                        End If
                        BadgeHolder.Controls.Add(lblBadgeNumber)
                        BadgeHolder.Controls.Add(New LiteralControl(" "))
                    Next
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    lblChapterCode.Text = dsBadge.Tables(0).Rows(i - 1)("ChapterCode").ToString()
                    lblChapterCode.ForeColor = Color.Black
                    lblChapterCode.Font.Size = 13
                    BadgeHolder.Controls.Add(lblChapterCode)
                    BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                Else
                    BadgeHolder.Controls.Add(New LiteralControl("<td align='center'  style='width:4.0in;padding:0in .75pt 0in .75pt;height:3.0in'> "))
                    lblHeader1.Text = "North South Foundation"
                    lblHeader1.Font.Bold = True
                    lblHeader1.Font.Name = "Verdana"
                    lblHeader1.ForeColor = Color.Navy
                    lblHeader1.Font.Size = 16
                    BadgeHolder.Controls.Add(lblHeader1)
                    lblHeader2.Text = "Regional Educational Contests"
                    lblHeader2.Font.Name = "Times New Roman"
                    lblHeader2.ForeColor = Color.Navy
                    lblHeader2.Font.Size = 11
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(lblHeader2)
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    lblName.Text = dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & " " & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()
                    lblName.Font.Name = "Arial"
                    lblName.Font.Bold = True
                    If Len(lblName.Text) > 22 Then
                        lblName.Font.Size = 12
                    Else
                        lblName.Font.Size = 15
                    End If
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(lblName)
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    dsBadgeNumber = badge.ShowBadge(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, Convert.ToDouble(dsBadge.Tables(0).Rows(i - 1)("ChildNumber")))

                    For iBadgeCount = 0 To dsBadgeNumber.Tables(0).Rows.Count - 1
                        lblBadgeNumber = New Label
                        If dsBadgeNumber.Tables(0).Rows.Count > 3 And dsBadgeNumber.Tables(0).Rows.Count <= 5 Then
                            lblBadgeNumber.Font.Size = 12
                            lblBadgeNumber.Font.Bold = True
                        ElseIf dsBadgeNumber.Tables(0).Rows.Count > 5 Then
                            lblBadgeNumber.Font.Size = 10
                            lblBadgeNumber.Font.Bold = True
                        Else
                            lblBadgeNumber.Font.Size = 15
                            lblBadgeNumber.Font.Bold = True
                        End If

                        lblBadgeNumber.Text = dsBadgeNumber.Tables(0).Rows(iBadgeCount)("BadgeNumber").ToString()
                        If dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "SB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "VB" Then
                            lblBadgeNumber.ForeColor = Color.Blue
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "MB" Then
                            lblBadgeNumber.ForeColor = Color.Red
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "EW" Then
                            lblBadgeNumber.ForeColor = Color.DeepPink
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "PS" Then
                            lblBadgeNumber.ForeColor = Color.Orange
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "BB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "GB" Then
                            lblBadgeNumber.ForeColor = Color.Black
                        End If
                        BadgeHolder.Controls.Add(lblBadgeNumber)
                        BadgeHolder.Controls.Add(New LiteralControl(" "))
                    Next
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    lblChapterCode.ForeColor = Color.Black
                    lblChapterCode.Font.Size = 13
                    lblChapterCode.Text = dsBadge.Tables(0).Rows(i - 1)("ChapterCode").ToString()
                    BadgeHolder.Controls.Add(lblChapterCode)
                    BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<br/>"))
                    BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                End If
                If iPerPageCount = j Then
                    j = 1
                    'BadgeHolder.Controls.Add(New LiteralControl("<tr><td colspan='2'>&nbsp;</td></tr>"))
                    'BadgeHolder.Controls.Add(New LiteralControl("<div style='page-break-after:always'>&nbsp;</div>"))
                    'BadgeHolder.Controls.Add(New LiteralControl("<P CLASS='breakhere'>"))
                End If
                j = j + 1
            Next
            pnlData.Visible = True
            pnlMessage.Visible = False
        Else
            pnlData.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Badge Details found."
        End If
        If pnlMessage.Visible = True Then
            Exit Sub
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/pdf"
            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            'Response.AddHeader("content-disposition", "attachment;filename=Badges_Participants_" & strChapterName & ".doc")
            'Response.Charset = ""
            'Response.ContentType = "application/vnd.word"
            'Dim stringWrite As New System.IO.StringWriter()
            'Dim htmlWrite As New HtmlTextWriter(stringWrite)
            'pnlData.RenderControl(htmlWrite)
            Response.AddHeader("content-disposition", "attachment;filename=Badges_Participants_" & strChapterName & ".pdf")
            Response.ContentEncoding = System.Text.Encoding.UTF8
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As StringWriter = New StringWriter
            Dim ht As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim Str1 As New Label
            pnlData.RenderControl(ht)
            Response.Write("<html>")
            Response.Write("<head>")
            Response.Write("<style>")
            Response.Write(" @page Section1 {size:8.5in 11.0in;margin:50.0pt .25in 0in .25in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:4;}")
            Response.Write("div.Section1 {page:Section1;}")
            Response.Write("</style>")
            Response.Write("</head>")
            Response.Write("<body>")
            Response.Write("</body>")
            Response.Write("</html>")
            Dim sr As New StringReader(sw.ToString())
            'Dim pdfDoc As New Document
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 5, 5, 100, 0)
            'Dim pgSize As New iTextSharp.text.PageSize(8.5, 11.0)
            'Dim pdfdoc As New iTextSharp.text.Document(pgSize, 5, 5, 55, 0)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.End()
        End If
    End Sub

    Private Sub LoadNationalBadges()
        Dim dsBadge As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        Dim j As Integer
        ' Dim iBadgeCount As Integer
        Dim iPerPageCount As Integer
        '  Dim strBandgeNumber As String
        '   Dim badge_number As Integer
        '   Dim week_day_number As Integer
        Dim TshirtName As String = ""
        generateBadge = CType(Context.Handler, GenerateBadges)

        Dim StrRange As String() = Session("PageRange").ToString.Split("_")
        Try
            dsBadge = badge.GetChildsHavingBadgesFinals(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, StrRange(0), StrRange(1))

            If dsBadge.Tables(0).Rows.Count > 0 Then
                iPerPageCount = 10
                j = 1
                dl.DataSource = dsBadge.Tables(0)
                dl.DataBind()
                pnlDataNat.Visible = True
                pnlMessage.Visible = False
            Else
                pnlDataNat.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No Badge Details found."
            End If
            If pnlMessage.Visible = True Then
                Exit Sub
            End If
            If generateBadge.Export = True Then
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/pdf"
                Dim strChapterName As String
                If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                    strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
                End If
                Response.AddHeader("content-disposition", "attachment;filename=Badges_Participants_" & strChapterName & ".pdf")
                Response.Cache.SetCacheability(HttpCacheability.NoCache)
                Dim sw As StringWriter = New StringWriter
                Dim ht As HtmlTextWriter = New HtmlTextWriter(sw)
                Dim Str1 As New Label
                pnlDataNat.RenderControl(ht)
                Response.Write("<html>")
                Response.Write("<head>")
                Response.Write("<style>")
                Response.Write(" @page Section1 {size:8.5in 11.0in;margin:1.00in .10in .80in .10in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:4;}")
                Response.Write(" @page t {border-color:red;}")
                Response.Write("</style>")
                Response.Write("</head>")
                Response.Write("<body>")
                Response.Write("</body>")
                Response.Write("</html>")

                Dim sr As New StringReader(sw.ToString())
                Dim pgSize As New iTextSharp.text.Rectangle(600, 600)
                Dim pdfdoc As New iTextSharp.text.Document(PageSize.LETTER, 18, 18, 72, 0)
                Dim htmlparser As New HTMLWorker(pdfdoc)
                PdfWriter.GetInstance(pdfdoc, Response.OutputStream)
                pdfdoc.Open()
                htmlparser.Parse(sr)
                pdfdoc.Close()
                Response.Write(pdfdoc)
                Response.End()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Function contestdate(ByVal str As String) As String
        Dim returnstr As String = ""
        Dim strarr() As String
        Dim count As Integer
        strarr = str.Split(",")
        For count = 0 To strarr.Length - 1
            If count = 0 Then
                returnstr = strarr(count)
            ElseIf Not returnstr.Contains(strarr(count)) Then
                If (strarr(count).Substring(0, 3) = strarr(count - 1).Substring(0, 3)) And strarr(count) <> strarr(count - 1) Then ' (strarr(count).Substring(3, (strarr(count).Length - 3)) <> strarr(count - 1).Substring(3, (strarr(count - 1).Length - 3)))
                    returnstr = returnstr & "-" & strarr(count).Substring(3, (strarr(count).Length - 3))
                ElseIf strarr(count) = strarr(count - 1) Then
                    returnstr = returnstr
                Else
                    returnstr = returnstr & "," & strarr(count)
                End If
            End If
        Next
        Return returnstr
    End Function

    Public Function CreateThumbnail(ByVal imageUrl As String) As String

        'thumbnail creation starts
        Try
            'Read in the image filename whose thumbnail has to be created
            ' Dim imageUrl As String = UploadedFileName

            'Read in the width and height
            Dim imageHeight As Integer = 70 ' = Convert.ToInt32(h.Text)
            Dim imageWidth As Integer = 70 '= Convert.ToInt32(w.Text)

            'You may even specify a standard thumbnail size
            'int imageWidth  = 70; 
            'int imageHeight = 70;

            'If imageUrl.IndexOf("/") >= 0 OrElse imageUrl.IndexOf("\") >= 0 Then
            'We found a / or \
            'Response.End()
            'End If

            'the uploaded image will be stored in the Pics folder.
            'to get resize the image, the original image has to be accessed from the
            'Pics folder
            'imageUrl = "pics/" + imageUrl

            Dim fullSizeImg As System.Drawing.Image = System.Drawing.Image.FromFile(Server.MapPath(imageUrl))

            Dim dummyCallBack As New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)

            Dim thumbNailImg As System.Drawing.Image = fullSizeImg.GetThumbnailImage(imageWidth, imageHeight, dummyCallBack, IntPtr.Zero)

            'We need to create a unique filename for each generated image
            Dim MyDate As DateTime = DateTime.Now

            Dim MyString As String = MyDate.ToString("ddMMyyhhmmss") + ".png"

            'Save the thumbnail in Png format. You may change it to a diff format with the ImageFormat property
            ' thumbNailImg.Save(Request.PhysicalApplicationPath + "photos_badge\" + MyString, System.Drawing.Imaging.ImageFormat.Png)
            thumbNailImg.Save(Server.MapPath(".") & MyString, System.Drawing.Imaging.ImageFormat.Png)
            thumbNailImg.Dispose()

            Return MyString
            'Display the original & the newly generated thumbnail
            ' Image1.AlternateText = "Original image"
            'Image1.ImageUrl = "pics\" + UploadedFileName

            'Image2.AlternateText = "Thumbnail"
            'Image2.ImageUrl = "pics\" + MyString
        Catch ex As Exception
            'Response.Write(Request.PhysicalApplicationPath + "photos_badge\")
            'Response.Write("An error occurred - " + ex.ToString())
            Response.Write(Server.MapPath(imageUrl))
            Response.Write(Server.MapPath("./photos_badge/bee.jpg"))
            Return Server.MapPath("./photos_badge/bee.jpg")
        End Try

    End Function

    'this function is reqd for thumbnail creation
    Public Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Public Function FileExists(ByVal FileFullPath As String) _
         As Boolean
        Try
            FileFullPath = Server.MapPath(FileFullPath)
        Catch ex As Exception

        End Try
        Dim f As New IO.FileInfo(FileFullPath)
        Return f.Exists

    End Function
    Dim week_day_number As Integer
    Dim badge_number As Integer
    Dim lblEmpty As Label
    Protected Sub dl_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dl.ItemDataBound
        Try

            lblTshirt_Food = New Label
            'Dim lbl As Label = e.Item.FindControl("lbl")
            'lbl.Text = Now.Year.ToString() & " " & lbl.Text
            'Dim lblCDates As Label = e.Item.FindControl("lblCDates")
            'lblCDates.Text = contestdate(lblCDates.Text) & "," & Now.Year.ToString()
           
            Dim lblWkDays As Label = e.Item.FindControl("lblWeekDays")
            Dim lblBadNum As Label = e.Item.FindControl("lblBadNum")
            Dim lblTShirt As Label = e.Item.FindControl("lblTShirt")
            Dim lblLunchType As Label = e.Item.FindControl("lblLunchType")
            Dim NationalBadgeHolder As PlaceHolder = e.Item.FindControl("NationalBadgeHolder")
            Dim img As ImageButton = e.Item.FindControl("Imgb")
            Dim lblChildNumber As Label = e.Item.FindControl("lblChildNumber")
            Dim lblPhotoname As Label = e.Item.FindControl("lblPhotoname")
            Dim lblPhoImgSrc As Label = e.Item.FindControl("lblPhoImgSrc")

            Dim img1 As New HtmlImage
            If FileExists(lblPhotoname.Text.ToString()) Then
                img1.Src = lblPhoImgSrc.Text.ToString()
            Else
                img1.Src = "http://www.northsouth.org/app8/photos_badge/bee.jpg"
            End If
            img1.Height = 90
            img1.Width = 90
            NationalBadgeHolder.Controls.Add(New LiteralControl("<tr bgcolor='#F0FFF0'><td align='center' colspan='3'><table border=0><tr><td></td><td colspan='10'>")) '<img src='" & img1.Src & "' style='left:50px;margin-left:5px;' width='50' height='50'/>
            NationalBadgeHolder.Controls.Add(img1)
            NationalBadgeHolder.Controls.Add(New LiteralControl("</td><td></td></tr></table>"))
            NationalBadgeHolder.Controls.Add(New LiteralControl("</td><td colspan='4' style='width:50%'><table border=0 width='100%'><tr><td colspan='7' align='center'>"))

            Dim Image1 As New HtmlImage '= e.Item.FindControl("Image1")
            'imageBarCode = New System.Web.UI.HtmlControls.HtmlImage
            Image1.Src = "http://www.tec-it.com/aspx/service/tbarcode/barcode.ashx?accesskey=NORTHSOUTH120802&code=QRCode&modulewidth=2&unit=px&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3D" & lblChildNumber.Text & "&dpi=96&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=Low"
            '  Image1.Width = 50
            ' Image1.Height = 50

            Dim lblFName As Label = CType(e.Item.FindControl("lblFName"), Label)
            Dim lblLName As Label = CType(e.Item.FindControl("lblLName"), Label)
            Dim lblName As New Label '= e.Item.FindControl("lblName")
            lblName.Text = lblFName.Text & " <br/>" & lblLName.Text
            lblName.Font.Size = 10
            lblName.Font.Name = "Georgia"
            lblName.Font.Bold = True
            'If Len(lblFName.Text.ToString() & lblLName.Text.ToString()) < 15 Then
            '    lblFName.Font.Size = 10
            'Else
            '    lblName.Font.Size = 10 '6
            'End If
            ' NationalBadgeHolder.Controls.Add(New LiteralControl("<font size=10>" & lblFName.Text & "<br/>" & lblLName.Text & "</font>"))
            NationalBadgeHolder.Controls.Add(lblName)
            NationalBadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))
            'NationalBadgeHolder.Controls.Add(New LiteralControl("<tr><td colspan='6'>"))
            'NationalBadgeHolder.Controls.Add(lblLName)
            'NationalBadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))
            NationalBadgeHolder.Controls.Add(New LiteralControl("<tr><td></td><td></td><td colspan='5'>"))
            NationalBadgeHolder.Controls.Add(Image1)
            NationalBadgeHolder.Controls.Add(New LiteralControl("</td></tr></table></td></tr>"))
            Dim week_days() As String
            week_days = lblWkDays.Text.ToString().Split(";")
            For week_day_number = 0 To week_days.GetUpperBound(0)

                lblWeekDay = New Label
                lblWeekDay.Font.Size = 10
                lblWeekDay.Font.Bold = True
                If week_day_number <> week_days.GetUpperBound(0) Then
                    lblWeekDay.Text = week_days(week_day_number).ToString() & ":"
                End If
                If lblWeekDay.Text.Trim.Length = 0 Then
                    Continue For
                End If
                NationalBadgeHolder.Controls.Add(New LiteralControl("<tr bgcolor='#F0FFF0'>"))
                NationalBadgeHolder.Controls.Add(New LiteralControl("<td align='center' colspan='3'>"))
                NationalBadgeHolder.Controls.Add(lblWeekDay)
                NationalBadgeHolder.Controls.Add(New LiteralControl("</td>"))

                NationalBadgeHolder.Controls.Add(New LiteralControl("<td colspan='4' align='center'>"))

                'Display Badgenumbers for particular day
                Dim a() As String
                Dim iCheckEmpty As Boolean = True
                a = lblBadNum.Text.ToString().Split(";")
                For badge_number = 0 To a.GetUpperBound(0)

                    If UCase(a(badge_number).Substring(0, 3)) = UCase(week_days(week_day_number).ToString()) Then
                        lblBadgeNumber = New Label
                        iCheckEmpty = False
                        If a.Length >= 3 And a.Length < 5 Then
                            lblBadgeNumber.Font.Size = 7
                            lblBadgeNumber.Font.Bold = True
                        ElseIf a.Length > 5 Then
                            lblBadgeNumber.Font.Size = 5
                            lblBadgeNumber.Font.Bold = True
                            'ElseIf a.Length = 3 Then
                            '    lblBadgeNumber.Font.Size = 7
                            '    lblBadgeNumber.Font.Bold = True
                        Else
                            lblBadgeNumber.Font.Size = 9
                            lblBadgeNumber.Font.Bold = True
                        End If
                        lblBadgeNumber.Text = Mid(a(badge_number).ToString(), 4)
                        NationalBadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))

                        If UCase(a(badge_number).Substring(4, 2)) = "SB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf UCase(a(badge_number).Substring(4, 2)) = "VB" Then
                            lblBadgeNumber.ForeColor = Color.Blue
                        ElseIf UCase(a(badge_number).Substring(3, 2)) = "MB" Then
                            lblBadgeNumber.ForeColor = Color.Red
                        ElseIf UCase(a(badge_number).Substring(3, 2)) = "EW" Then
                            lblBadgeNumber.ForeColor = Color.DeepPink
                        ElseIf UCase(a(badge_number).Substring(3, 2)) = "PS" Then
                            lblBadgeNumber.ForeColor = Color.Red
                        ElseIf UCase(a(badge_number).Substring(3, 2)) = "BB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf UCase(a(badge_number).Substring(3, 2)) = "GB" Then
                            lblBadgeNumber.ForeColor = Color.Black
                        End If
                        lblBadgeNumber.Text = lblBadgeNumber.Text & "  "
                        NationalBadgeHolder.Controls.Add(lblBadgeNumber)
                        ' NationalBadgeHolder.Controls.Add(New LiteralControl("&nbsp;&nbsp;&nbsp;"))
                    End If
                Next
                NationalBadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))
                NationalBadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))
            Next
            'To display TshirtSize and LunchType on Badges
            NationalBadgeHolder.Controls.Add(New LiteralControl("<tr><td colspan='3' bgcolor='#F0FFF0'>&nbsp;</td><td colspan='4'>"))
            Dim TshirtName As String = ""
            If lblTShirt.Text.ToString() = "YYL" Then
                TshirtName = "L"
            ElseIf lblTShirt.Text.ToString() = "YM" Then
                TshirtName = "M"
            ElseIf lblTShirt.Text.ToString() = "YL" Then
                TshirtName = "YL"
            Else
                TshirtName = ""
            End If
            ' lblTshirt_Food = New Label
            lblTshirt_Food.Font.Size = 10
            lblTshirt_Food.Font.Bold = True
            lblTshirt_Food.Text = "Food: " & IIf(lblLunchType.Text.ToString() = "Pizza", "P", "I") & "&nbsp;&nbsp;&nbsp;  TSize: " & TshirtName ' dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString()
            NationalBadgeHolder.Controls.Add(lblTshirt_Food)
            'end of display badge number
            NationalBadgeHolder.Controls.Add(New LiteralControl("</td>"))
            NationalBadgeHolder.Controls.Add(New LiteralControl("</tr>"))
        Catch ex As Exception
            '  Response.Write(ex.Message)
        End Try
    End Sub
End Class
