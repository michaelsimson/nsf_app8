﻿<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<%@ Import Namespace="System.Text" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ds As New DataSet
        If TextBox1.Text.ToLower.IndexOf("delete") > -1 Then
            Label1.Text = "You can't Delete"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("truncate") > -1 Then
            Label1.Text = "You can't Truncate"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("drop") > -1 Then
            Label1.Text = "You can't use Drop"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("alter") > -1 Then
            Label1.Text = "You can't use Alter"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("create") > -1 Then
            Label1.Text = "You can't use Create"
            Exit Sub
        End If
        Try
            ds = SqlHelper.ExecuteDataset("Data Source=sql.northsouth.org;uid=northsouth;pwd=uttaramdakshinam;Initial Catalog=northsouth_prd", CommandType.Text, TextBox1.Text)
            
            If ds.Tables(0).Rows.Count > 0 Then
                GridView1.DataSource = ds.Tables(0)
                GridView1.DataBind()
                Button3.Visible = True
            Else
                GridView1.DataSource = Nothing
                GridView1.DataBind()
                Button3.Visible = False
                Label1.Text = "No record to Display"
            End If
            
        Catch ex As Exception
            Label1.Text = ex.ToString()
        End Try
        
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If TextBox1.Text.ToLower.IndexOf("delete") > -1 Then
            Label1.Text = "You can't Delete"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("truncate") > -1 Then
            Label1.Text = "You can't Truncate"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("drop") > -1 Then
            Label1.Text = "You can't use Drop"
            Exit Sub
            'ElseIf TextBox1.Text.ToLower.IndexOf("alter") > -1 Then
            '    Label1.Text = "You can't use Alter"
            '    Exit Sub
            'ElseIf TextBox1.Text.ToLower.IndexOf("create") > -1 Then
            '    Label1.Text = "You can't use Create"
            '    Exit Sub
        End If
        SqlHelper.ExecuteNonQuery("Data Source=sql.northsouth.org;uid=northsouth;pwd=uttaramdakshinam;Initial Catalog=northsouth_prd", CommandType.Text, TextBox1.Text)
    End Sub
   
    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=SQLExport_" & Now.Year.ToString() & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GridView1.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Database</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:800px; text-align:center">
        <asp:TextBox ID="TextBox1" Text ="select * from SentEmailLog where SentEmailLogID >550" runat="server" TextMode="MultiLine" Width="500px" Height="100px"></asp:TextBox>
        <br />
        <asp:Label ForeColor="Red"  ID="Label1" runat="server" ></asp:Label>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Execute" 
            onclick="Button1_Click" />
            &nbsp;&nbsp;
              <asp:Button Visible="false" ID="Button2" runat="server" Text="Non Query" 
            onclick="Button2_Click" />
            &nbsp;&nbsp;
              <asp:Button Visible="false" ID="Button3" runat="server" Text="Export" 
            onclick="Button3_Click" />
        <br /><br />
        <asp:GridView ID="GridView1" AutoGenerateColumns="true" runat="server">
        </asp:GridView>
    </div>
    </form>
</body>
</html>
