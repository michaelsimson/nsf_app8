﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using VRegistration;


public partial class CalSignUpList : System.Web.UI.Page
{
    string StrTableName;
    string wherecntn;
    string sDatakey;
    bool trueVal = false;
    string sortby = " LastName, FirstName";
    protected void Semester(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {

            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            list.Add(new ListItem("One", "1"));
            list.Add(new ListItem("Two", "2"));
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
        }

    }
    protected void Accepted(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            list.Add(new ListItem("No", "2"));
            list.Add(new ListItem("Yes", "1"));
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
        }

    }
    protected void level(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {
            string st = lbProd.Text;
            string ddLevel;
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            if (lbProd.Text == "SAT")
            {

                list.Add(new ListItem("Junior", "Junior"));
                list.Add(new ListItem("Senior", "Senior"));
            }
            else if (lbProd.Text == "Universal Values")
            {
                list.Add(new ListItem("Junior", "Junior"));
                list.Add(new ListItem("Intermediate", "Intermediate"));
                list.Add(new ListItem("Senior", "Senior"));
            }
            else
            {
                list.Add(new ListItem("Beginner", "Beginner"));
                list.Add(new ListItem("Intermediate", "Intermediate"));
                list.Add(new ListItem("Advanced", "Advanced"));

            }


            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "Select");
        }
    }
    protected void Day(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {

            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            list.Add(new ListItem("Select", "0"));
            list.Add(new ListItem("Monday", "Monday"));
            list.Add(new ListItem("Tuesday", "Tuesday"));
            list.Add(new ListItem("Wednesday", "Wednesday"));
            list.Add(new ListItem("Thursday", "Thursday"));
            list.Add(new ListItem("Friday", "Friday"));
            list.Add(new ListItem("Saturday", "Saturday"));
            list.Add(new ListItem("Sunday", "Sunday"));
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
        }

    }

    protected void Time(object sender, System.EventArgs e)
    {
        DropDownList ddlTemp = null;
        ddlTemp = (DropDownList)sender;
        ddlTemp.Items.Clear();
        DataTable dt = new DataTable();
        if ((lblday.Text == "Saturday") || (lblday.Text == "Sunday"))
        {

            DataRow dr = dt.NewRow();
            DateTime StartTime = Convert.ToDateTime("8:00:00 AM");
            dt.Columns.Add("ddlText", Type.GetType("System.String"));
            dt.Columns.Add("ddlValue", Type.GetType("System.String"));
            while (StartTime <= Convert.ToDateTime("11:59:00 PM"))
            {
                dr = dt.NewRow();
                dr["ddlText"] = StartTime.ToString("h:mmtt");
                dr["ddlValue"] = StartTime.ToString("h:mmtt");
                dt.Rows.Add(dr);
                StartTime = StartTime.AddHours(1);
            }
        }
        else
        {

            DataRow dr = dt.NewRow();
            DateTime StartTime = Convert.ToDateTime("6:00 PM");
            dt.Columns.Add("ddlText", Type.GetType("System.String"));
            dt.Columns.Add("ddlValue", Type.GetType("System.String"));
            while (StartTime <= Convert.ToDateTime("11:59:00 PM"))
            {
                dr = dt.NewRow();
                dr["ddlText"] = StartTime.ToString("h:mmtt");
                dr["ddlValue"] = StartTime.ToString("h:mmtt");
                dt.Rows.Add(dr);
                StartTime = StartTime.AddHours(1);
            }
        }

        ddlTemp.DataSource = dt;
        ddlTemp.DataTextField = "ddlText";
        ddlTemp.DataValueField = "ddlValue";
        ddlTemp.DataBind();
        ddlTemp.Items.Add(new ListItem("12:00AM", "12:00AM"));
        ddlTemp.Items.Insert(0, "Select time");
    }
    protected void MaxCap(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            for (int i = 1; i <= 50; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, new ListItem("Select", "0"));
        }

    }
    protected void VRoom(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            for (int i = 1; i <= 30; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, new ListItem("Select", "0"));
        }

    }
    protected void Cycle(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();

            for (int i = 1; i <= 10; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, new ListItem("Select", "0"));
        }

    }
    protected void SessionNo(object sender, System.EventArgs e)
    {
        if (trueVal != true)
        {
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            ArrayList list = new ArrayList();
            for (int i = 1; i <= 5; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlTemp.DataSource = list;
            ddlTemp.DataTextField = "Text";
            ddlTemp.DataValueField = "Value";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, new ListItem("Select", "0"));
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["LoginID"] = "4240";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {
            int first_year = 2006;
            int year = Convert.ToInt32(DateTime.Now.Year) + 2;
            int count = year - first_year;
            int year_index;
            for (int i = 0; i < count; i++)
            {
                // lstYear.Items.Insert(i, new ListItem(Convert.ToString(year - (i + 1))));

                lstYear.Items.Add(new ListItem(Convert.ToString(year - (i + 2)) + "-" + Convert.ToString(year - (i + 1)).Substring(2, 2), Convert.ToString(year - (i + 2))));
            }

            if (DateTime.Now.Month <= 3)
            {
                year_index = DateTime.Now.Year - 1;
            }
            else
            {
                year_index = DateTime.Now.Year;
            }

            lstYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            //  lstYear.SelectedIndex = lstYear.Items.IndexOf(lstYear.Items.FindByText(year_index.ToString()));

            // lstYear.Items[1].Selected = true;
            loadPhase();

            if (((Convert.ToInt32(Session["RoleID"])) == 88) || (Convert.ToInt32(Session["RoleID"]) == 89))
            {
                if ((Convert.ToInt32(Session["RoleID"])) == 88)
                {
                    StrTableName = "CalSignUp";
                }
                else
                {
                    StrTableName = "Volunteer";
                }
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select count (*) from " + StrTableName + " where Memberid=" + Session["LoginID"] + "  and ProductId is not Null")) > 1) //" and RoleId=" + Session["RoleId"] +
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select Distinct ProductGroupID,ProductID from " + StrTableName + " where Memberid=" + Session["LoginID"] + " and ProductId is not Null "); //" and RoleId=" + Session["RoleId"] + 
                    int i;
                    String prd = "";//String.Empty;
                    String Prdgrp = ""; /////String.Empty
                    for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (prd.Length == 0)
                            prd = ds.Tables[0].Rows[i][1].ToString();
                        else
                            prd = prd + "," + ds.Tables[0].Rows[i][1].ToString();


                        if (Prdgrp.Length == 0)
                            Prdgrp = ds.Tables[0].Rows[i][0].ToString();
                        else
                            Prdgrp = Prdgrp + "," + ds.Tables[0].Rows[i][0].ToString();
                    }
                    lblPrd.Text = prd;
                    lblPrdGrp.Text = Prdgrp;
                    GetProductGroup(lstProductGroup);// LoadProductGroup();
                }
                else
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select Distinct ProductGroupID,ProductID from " + StrTableName + " where Memberid=" + Session["LoginID"] + " and ProductId is not Null "); //" and RoleId=" + Session["RoleId"] + 
                    String prd = ""; //String.Empty
                    String Prdgrp = "";//String.Empty
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        prd = ds.Tables[0].Rows[0][1].ToString();
                        Prdgrp = ds.Tables[0].Rows[0][0].ToString();
                        lblPrd.Text = prd;
                        lblPrdGrp.Text = Prdgrp;
                    }
                    GetProductGroup(lstProductGroup);//  LoadProductGroup();
                }
            }
            else
            {
                lblPrd.Text = "";
                lblPrdGrp.Text = "";
                ddlRegistype.Enabled = true;
                GetProductGroup(lstProductGroup); //LoadProductGroup();
            }
            LoadCoachList();
        }

    }

    private void GetProductGroup(DropDownList ddlObject)
    {
        String year = "(0";
        foreach (ListItem i in lstYear.Items)
        {
            if (i.Selected)
            {
                year = year + "," + i.Value;
            }
        }

        year = year + ") ";
        String StrPrdGrp = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN CalSignup EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where P.EventId=13 and EF.EventYear in " + year + " and EF.Semester='" + ddlPhase.SelectedValue + "'";

        if (lblPrdGrp.Text != "")
        {
            StrPrdGrp = StrPrdGrp + " and P.ProductGroupId in(" + lblPrdGrp.Text + ")";
        }
        StrPrdGrp = StrPrdGrp + " order by P.ProductGroupID";
        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrPrdGrp); //EF.EventYear>=YEAR(GETDATE()) AND
        ddlObject.DataSource = dsProductGroup;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "ProductGroupID";
        ddlObject.DataBind();
        if (ddlObject.Items.Count < 1)
        {
            lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
        }
        else if (ddlObject.Items.Count > 1)// && lblPrdGrp.Text == "")
        {
            ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
            GetProduct();
            lblerr.Text = "";
        }
        else if (ddlObject.Items.Count == 1)
        {
            ddlObject.Enabled = false;
            ddlObject.SelectedIndex = 0;
            GetProduct();
            lblerr.Text = "";
        }
    }
    private void GetProduct()
    {
        try
        {
            string prodGrp = "";
            String year = string.Empty;
            if ((lstProductGroup.SelectedIndex != 0) || (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text != "All"))
            {
                prodGrp = " and P.ProductGroupID in (0";
                foreach (ListItem i in lstProductGroup.Items)
                {
                    if (i.Selected)
                    {
                        prodGrp = prodGrp + "," + i.Value;
                    }
                }
                prodGrp = prodGrp + ") ";
            }
            else if (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text == "All")
            {
                prodGrp = " and P.ProductGroupID in (0";
                foreach (ListItem i in lstProductGroup.Items)
                {
                    prodGrp = prodGrp + "," + i.Value;
                }
                prodGrp = prodGrp + ") ";
            }

            year = "(0";
            foreach (ListItem i in lstYear.Items)
            {
                if (i.Selected)
                {
                    year = year + "," + i.Value;
                }
            }
            year = year + ") ";
            String StrPrd = "Select Distinct P.ProductID, CASE WHEN P.CoachName IS NULL THEN P.Name ELSE P.CoachName END as Name from Product P INNER JOIN CalSignup EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where  P.EventID=13 " + prodGrp + " and EF.EventYear in " + year + " and EF.Semester='" + ddlPhase.SelectedValue + "'";
            if (lblPrd.Text != "")
            {
                StrPrd = StrPrd + " and P.ProductID in(" + lblPrd.Text + ")";
            }
            StrPrd = StrPrd + " order by P.ProductID ";
            DataSet dsProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrPrd); //EF.EventYear>=YEAR(GETDATE()) AND
            lstProduct.DataSource = dsProduct;
            lstProduct.DataTextField = "Name";
            lstProduct.DataValueField = "ProductID";
            lstProduct.DataBind();
            if (lstProduct.Items.Count < 1)
            {
                lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
            }
            else if (lstProduct.Items.Count > 1)//&& lblPrd.Text == "")
            {
                lstProduct.Items.Insert(0, new ListItem("All", "0"));
                lstProduct.SelectedIndex = 0;
                if (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text == "All")
                    lstProduct.Enabled = true;
                else
                    lstProduct.Enabled = true;
                lblerr.Text = "";
            }
            else if (lstProduct.Items.Count == 1)
            {
                lstProduct.SelectedIndex = 0;
                lstProduct.Enabled = true;
                lblerr.Text = "";
            }

        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    protected void ddday_SelectedIndexChanged(object sender, EventArgs e)
    {
        //TxtRecType1.Text = "";
        trueVal = true;
        this.lblday.Text = ((DropDownList)sender).SelectedValue;

    }
    protected void lstYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(lstProductGroup);
        GetProduct();
        LoadCoachList();
    }
    protected void lstProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct();
        LoadCoachList();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string filename = lstYear.SelectedItem.Value + "_" + lstProductGroup.SelectedItem.Text + "_"
               + lstProduct.SelectedItem.Text + "_" + ddlPhase.SelectedItem.Text + "_"
               + ddlSessionNo.SelectedItem.Text + "_" + ddlRegistype.SelectedItem.Text + "_CoachList" + "_" + DateTime.Now.Date.ToShortDateString();
        if (lbprevious.Visible == true)
        {
            GeneralExport((DataTable)Session["CoachDetailsList"], filename + ".xls");
        }
        else
        {
            GeneralExport((DataTable)Session["CoachList"], filename + ".xls");
        }


    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        try
        {
            string attach = string.Empty;
            attach = "attachment;filename=" + fname;
            Response.ClearContent();
            Response.AddHeader("content-disposition", attach);
            Response.ContentType = "application/vnd.xls";
            if (dtdata != null)
            {
                foreach (DataColumn dc in dtdata.Columns)
                {
                    Response.Write(dc.ColumnName + "\t");
                }
                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtdata.Rows)
                {
                    for (int i = 0; i < dtdata.Columns.Count; i++)
                    {
                        Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                    }
                    Response.Write("\n");
                }
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void btnSendEMail_Click1(object sender, EventArgs e)
    {
        Response.Redirect("~/CalSignupList.aspx");
        // LoadCoachList();
    }
    public void LoadCoachList()
    {

        lbProd.Text = "";
        lbProd.Text = lstProductGroup.SelectedItem.Text;

        String year = "(0";
        foreach (ListItem i in lstYear.Items)
        {
            if (i.Selected)
            {
                year = year + "," + i.Value;
            }
        }

        year = year + ") ";

        string strCR1 = " (select isnull(count(*),0) from CoachReg CR1 where CR1.CMemberId=cs.Memberid and CR1.EventYear in " + year + " and CR1.Semester=C1.Semester and CR1.ProductId= C1.ProductId ";
        strCR1 = strCR1 + " and CR1.SessionNo=C1.SessionNo and C1.Level=CR1.Level and CR1.EventId=C1.EventId and CR1.Approved='Y' ) >0";

        string strCR = " and (select isnull(count(*),0) from CoachReg CR1 where CR1.CMemberId=cs.Memberid and CR1.EventYear in (c.EventYear) and CR1.Approved='Y' ) >0";
        //  Reset strcr as empty to avoid validation against student approval
        strCR = "";
        string strAward = "select case AwardsType when 'Distinguished Service Award' then 'Dist' when 'Founder’s Award'  then 'Founder' when 'Lifetime Achievement Award' then 'Lifetime' else AwardsType end from DonVolAwards where RecType='Volunteer' and MemberId=cs.MemberID";
        string strPrdGrp = " STUFF((SELECT ', ' + ProductGroupCode FROM (select distinct productgroupcode from dbo.CalSignup C WHERE C.[memberid] = cs.[memberid] and C.EventYear in " + year + strCR + " ) as t  FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '')";
        string strPrd = " STUFF((SELECT ', ' + ProductCode FROM (select distinct ProductCode from dbo.CalSignup C WHERE C.[memberid] = cs.[memberid] and C.EventYear in " + year + strCR + ") as t FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '')";

        string StrSQL = "select distinct IP.AutoMemberID as MemberID, IP.FirstName,IP.LastName,  IP.Email, 'Prod Group' = " + strPrdGrp + ", Product=" + strPrd + ", IP.HPhone, IP.CPhone,IP.City,IP.State,IP.Chapter,";
        StrSQL = StrSQL + "(select COUNT(distinct(EventYear)) from CalSignUp C where C.MemberID=CS.MemberID and C.Accepted='Y'  and C.EventYear in " + year + strCR + ") as Years, (select COUNT(MemberID) from CalSignUp C where C.MemberID=CS.MemberID and C.Accepted='Y' and C.EventYear in " + year + strCR + ") as Sessions, cs.Semester,(" + strAward + ") as Award from CalSignup CS inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear in " + year;
        //StrSQL = StrSQL + " and isnull(cs.accepted,'') = case when (select count(*) from CalSignUp where MemberID = cs.memberid and accepted='Y')>0 then 'y' else '' end ";
        StrSQL = StrSQL + " and cs.Accepted='Y'  and Cs.Semester='" + ddlPhase.SelectedValue + "' ";//and " + strCR;
        try
        {
            if (lstProductGroup.SelectedValue != "0")
            {
                StrSQL += " and CS.ProductGroupID=" + lstProductGroup.SelectedValue + "";
            }
            if (lstProduct.SelectedValue != "0")
            {
                StrSQL += " and CS.ProductID=" + lstProduct.SelectedValue + "";
            }

            if (ddlSessionNo.SelectedValue != "0")
            {
                StrSQL += " and CS.SessionNo=" + ddlSessionNo.SelectedValue + "";
            }
            if (ddlRegistype.SelectedValue == "2")
            {
                StrSQL += " and CS.Accepted='Y'";
            }
            //if (ddlRegistype.SelectedValue == "3")
            //{
            //    StrSQL += " and (CS.Accepted is null or cs.Accepted='N' and cs.Accepted<>'Y' ) ";
            //}

            StrSQL += " order by " + sortby;

            StringBuilder sbEmailList = new StringBuilder();
            DataSet dsCoach = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (dsCoach.Tables[0].Rows.Count > 0)
            {
                Button1.Enabled = true;
                DataTable dtcoach = dsCoach.Tables[0];
                //  GridCoachList.PageIndex = 0;
                pnlCoachList.Visible = true;
                GridCoachList.DataSource = dsCoach.Tables[0];
                GridCoachList.DataBind();

                Session["CoachTable"] = dsCoach.Tables[0];
                dtcoach.Columns.Add("Ser#").SetOrdinal(0);
                for (int i = 0; i < dtcoach.Rows.Count; i++)
                {
                    dtcoach.Rows[i]["Ser#"] = i + 1;
                }
                dtcoach.AcceptChanges();
                Session["CoachList"] = dtcoach;

            }
            else
            {
                lblerr.Text = "Sorry No Coach found";
                pnlCoachList.Visible = false;
            }

        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void wherCond()
    {
        try
        {
            wherecntn = "";
            lblerr.Text = "";
            if (lstProductGroup.Items.Count <= 0)
                lblerr.Text = " No Product Groups present";
            else if (lstProduct.Items.Count <= 0)
                lblerr.Text = " No Products present";
            if (lblerr.Text != "")
                return;
            if ((lstProductGroup.Items[0].Selected != true) || (lstProductGroup.SelectedIndex == 0 && lstProductGroup.SelectedItem.Text != "All"))
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0," + lstProductGroup.SelectedValue + ")";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0," + lstProductGroup.SelectedValue + ")";

                if (lstProduct.Items[0].Selected == true && lstProduct.SelectedItem.Text == "All")
                {
                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in lstProduct.Items)
                    {
                        wherecntn = wherecntn + "," + i.Value;
                    }
                    wherecntn = wherecntn + ") ";
                }
                else
                {
                    if (wherecntn == "")
                        wherecntn = " ProductID in (0";
                    else
                        wherecntn = wherecntn + " AND ProductID in (0";
                    foreach (ListItem i in lstProduct.Items)
                    {
                        if (i.Selected)
                        {
                            wherecntn = wherecntn + "," + i.Value;
                        }
                    }
                    wherecntn = wherecntn + ") ";
                }
            }
            else if (lstProductGroup.Items[0].Selected == true && lstProductGroup.SelectedItem.Text == "All")//&& (lblPrdGrp.Text != "")
            {
                if (wherecntn == "")
                    wherecntn = " ProductGroupID in (0";
                else
                    wherecntn = wherecntn + " AND ProductGroupID in (0";
                foreach (ListItem i in lstProductGroup.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";

                if (wherecntn == "" && lblPrd.Text != "")
                    wherecntn = " ProductID in (" + lblPrd.Text + ")";
                else if (lblPrd.Text != "")
                    wherecntn = wherecntn + " and ProductID in (" + lblPrd.Text + ")";
            }
            if (wherecntn == "")
                wherecntn = " EventYear in (0";
            else
                wherecntn = wherecntn + " AND EventYear in (0";
            foreach (ListItem i in lstYear.Items)
            {
                if (i.Selected)
                {
                    wherecntn = wherecntn + "," + i.Value;

                }
            }
            wherecntn = wherecntn + ") ";
            if (ddlRegistype.SelectedValue == "2")
                wherecntn = wherecntn + " And Accepted='Y' ";
            wherecntn = wherecntn + " And Semester='" + ddlPhase.SelectedValue + "'";
            if (ddlSessionNo.SelectedValue != "0")
                wherecntn = wherecntn + " And SessionNo=" + ddlSessionNo.SelectedValue;
        }
        catch (Exception err)
        {
        }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            btnSendEMail.Enabled = false;
            lbprevious.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField AutoMemberID = (HiddenField)GridCoachList.Rows[index].Cells[0].FindControl("HdmemberId");
            lblMemberID.Text = AutoMemberID.Value;
            coachDetails(AutoMemberID.Value);

        }
        catch (Exception err)
        {
        }
    }

    protected void coachDetails(string IDVal)
    {
        wherCond();
        string str = wherecntn;
        string StrSQL = " select C.SignupID,I.FirstName,I.LastName,PG.Name as ProductGroup, C.productcode, C.level, C.SessionNo, C.EventYear Year, C.Semester, C.day,C.time, " +
       "  C.Accepted, C.MaxCapacity, C.Cycle, C.VRoom, C.UserID, C.PWD , (select isnull(count(*),0) from CoachReg CR where CR.CMemberId=C.Memberid and CR.EventYear=C.EventYear and CR.Semester=C.Semester and CR.ProductId= C.ProductId " +
        " and CR.SessionNo=C.SessionNo and C.Level=CR.Level and CR.EventId=C.EventId and CR.Approved='Y' ) NoOfStudents from calsignup C  left join productgroup PG on  PG.productGroupID=C.productGroupID" +
       "  left join Indspouse I on I.AutoMemberID=C.MemberID where (select isnull(count(*),0) from CoachReg CR1 where CR1.CMemberId=C.Memberid and CR1.EventYear=C.EventYear and CR1.Semester=C.Semester and CR1.ProductId= C.ProductId " +
        " and CR1.SessionNo=C.SessionNo and C.Level=CR1.Level and CR1.EventId=C.EventId and CR1.Approved='Y' ) >0 and C.Accepted='Y' and C.MemberID=" + IDVal + " and C." + wherecntn + "  order by  C.EventYear desc, C.ProductGroupID, C.ProductID, C.Semester, C.SessionNo";
        DataSet dsCoachcalsignDetails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        if (dsCoachcalsignDetails.Tables[0].Rows.Count > 0)
        {
            Button1.Enabled = true;
            pnlCoachList.Visible = false;
            pnlCoachClassDetails.Visible = true;
            DataTable dtcoach = dsCoachcalsignDetails.Tables[0];

            Session["CoachDetailsList"] = dtcoach;
            GriDEdit.DataSource = dsCoachcalsignDetails.Tables[0];
            GriDEdit.DataBind();

        }


    }

    protected void GridCoachList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridCoachList.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["CoachTable"];

            GridCoachList.DataSource = (DataTable)Session["CoachTable"];
            GridCoachList.DataBind();
            GridCoachList.HeaderRow.Cells[1].Visible = false;
            for (int i = 0; i < GridCoachList.Rows.Count; i++)
            {
                GridCoachList.Rows[i].Cells[1].Visible = false;
            }
        }
        catch (Exception ex) { }
    }
    protected void lbprevious_Click(object sender, EventArgs e)
    {
        pnlCoachList.Visible = true;
        pnlCoachClassDetails.Visible = false;
        btnSendEMail.Enabled = true;
        lbprevious.Visible = false;
        btnSendEMail.Enabled = true;

    }
    protected void GriDEdit_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            btnSendEMail.Enabled = false;
            GriDEdit.EditIndex = e.NewEditIndex;
            coachDetails(lblMemberID.Text);
            lblday.Text = "";
            HiddenField kProductGroup = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("HdnProd");
            lbProd.Text = kProductGroup.Value;

            HiddenField klevel = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("Hdnlevel");
            DropDownList ddlevel = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddlevel");
            if (klevel.Value == "")
            {
            }
            else
            {
                ddlevel.SelectedValue = klevel.Value;
            }

            HiddenField kPhase = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("Hdnphase");
            DropDownList ddlphase = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("DPhase");
            if ((kPhase.Value == "") || (kPhase.Value == "0"))
            {

            }
            else
            {
                ddlphase.SelectedValue = kPhase.Value;
            }

            HiddenField kSession = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("HdnSession");
            DropDownList ddlsession = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddSession");
            if ((kSession.Value == "") || (kSession.Value == "0"))
            {

            }
            else
            {
                ddlsession.SelectedValue = kSession.Value;
            }

            HiddenField k = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("Hdden");
            DropDownList ddlMax = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddlMax");
            if ((k.Value == "") || (k.Value == "0"))
            {

            }
            else
            {
                ddlMax.SelectedValue = k.Value;
            }

            HiddenField kday = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("HdnDay");
            DropDownList ddlDay = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddday");
            if (kday.Value == "")
            {
            }
            else
            {
                ddlDay.SelectedValue = kday.Value;
            }
            lblday.Text = kday.Value;
            HiddenField kCycle = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("Hdncycle");
            DropDownList ddlCycle = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("DCycle");
            if ((kCycle.Value == "0") || (kCycle.Value == ""))
            {
            }
            else
            {
                ddlCycle.SelectedValue = kCycle.Value;
            }

            HiddenField kVroom = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("HdncVroom");
            DropDownList ddlVroom = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddVroom");
            if ((kVroom.Value == "") || (kVroom.Value == "0"))
            {
            }
            else
            {
                ddlVroom.SelectedValue = kVroom.Value;
            }

            HiddenField kAccept = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("HdnAccept");
            DropDownList ddlAccept = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddlAccept");
            if (kAccept.Value == "")
            {
                ddlAccept.SelectedValue = "2";
            }
            else
            {
                ddlAccept.SelectedValue = "1";
            }


            //HiddenField kTime = (HiddenField)GriDEdit.Rows[e.NewEditIndex].FindControl("HdnTime");
            //DropDownList ddlTime = (DropDownList)GriDEdit.Rows[e.NewEditIndex].FindControl("ddtime");
            //ddlTime.SelectedValue = kTime.Value;

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }


    protected void GriDEdit_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GriDEdit.EditIndex = -1;
        coachDetails(lblMemberID.Text);
    }
    protected void GriDEdit_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string level;
            string sessionno;
            string phase1;
            string day;
            string time;
            string Accepted;
            string MaxCap;
            string Cycle1;
            string Vroom;

            sDatakey = GriDEdit.DataKeys[e.RowIndex].Value.ToString();


            DropDownList ddPhase = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("DPhase");
            TextBox Txtphase = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("Txtphase");
            DropDownList ddlevel = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddlevel");
            TextBox Txtlevel = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("Txtlevel");

            DropDownList ddSessionNo = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddSession");
            TextBox TxtSessionNo = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxtSession");

            DropDownList ddday = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddday");
            TextBox TxtDay = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxtDay");


            TextBox Txttime = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("Txttime");
            TextBox TxtUserID = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxtUser");
            TextBox TxtPwd = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("Txtpwd");

            DropDownList DDAccept = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddlAccept");
            TextBox Txtaccept = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxAccept");

            DropDownList ddMax = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddlMax");
            TextBox TxtMax = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxtMax");

            DropDownList DCycle = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("DCycle");
            TextBox TxtCycle = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxtCycle");

            DropDownList ddVroom = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddVroom");
            TextBox TxtVroom = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxtVroom");


            DropDownList ddTime = (DropDownList)GriDEdit.Rows[e.RowIndex].FindControl("ddtime");
            TextBox TxtTime = (TextBox)GriDEdit.Rows[e.RowIndex].FindControl("TxTime");


            if (ddPhase.SelectedItem.Text != "Select")
            {
                phase1 = ddPhase.SelectedValue;
            }
            else
            {
                phase1 = Txtphase.Text;
            }
            if (ddlevel.SelectedItem.Text != "Select")
            {
                level = ddlevel.SelectedItem.Text;
            }
            else
            {
                level = Txtlevel.Text;
            }
            if (ddSessionNo.SelectedItem.Text != "Select")
            {
                sessionno = ddSessionNo.SelectedItem.Text;
            }
            else
            {
                sessionno = TxtSessionNo.Text;
            }

            if (ddday.SelectedItem.Text != "Select")
            {
                day = ddday.SelectedItem.Text;
            }
            else
            {
                day = TxtDay.Text;
            }
            if (DDAccept.SelectedItem.Text != "Select")
            {
                Accepted = DDAccept.SelectedItem.Text;
            }
            else
            {
                Accepted = Txtaccept.Text;
            }


            if (ddMax.SelectedItem.Text != "Select")
            {
                MaxCap = ddMax.SelectedItem.Text;
            }
            else
            {
                MaxCap = TxtMax.Text;
            }


            if (DCycle.SelectedItem.Text != "Select")
            {
                Cycle1 = DCycle.SelectedItem.Text;
            }
            else
            {
                Cycle1 = TxtCycle.Text;
            }

            if (ddVroom.SelectedItem.Text != "Select")
            {
                Vroom = ddVroom.SelectedItem.Text;
            }
            else
            {
                Vroom = TxtVroom.Text;
            }
            if (Accepted == "Yes")
            {
                Accepted = "Y";
            }
            else
            {
                Accepted = "";
            }
            if (ddTime.SelectedItem.Text != "Select time")
            {
                time = ddTime.SelectedItem.Text;
            }
            else
            {
                time = TxtTime.Text;
            }

            string qryUpdate = "update calsignup set Level= '" + level + "', SessionNo=" + sessionno + " ,Semester='" + phase1 + "',DAY='" + day + "',time='" + time + "',Accepted='"
                   + Accepted + "',MaxCapacity=" + MaxCap + ",Cycle=" + Cycle1 + ",UserID='" + TxtUserID.Text + "',PWD='" + TxtPwd.Text + "',VRoom =" + Vroom + ",modifieddate=Getdate(),modifiedby=" + Session["loginID"]
                   + " where SignupID=" + sDatakey + "";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);
            Response.Write("<script>alert('Updated successfully')</script>");
            GriDEdit.EditIndex = -1;
            coachDetails(lblMemberID.Text);
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void GriDEdit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Button edit = (Button)e.Row.Cells[0].FindControl("btnEdit");
        foreach (ListItem i in lstYear.Items)
        {
            if (i.Selected)
            {
                DataRowView drview = e.Row.DataItem as DataRowView;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int year = Convert.ToInt32(DateTime.Now.Year);
                    if (i.ToString() != year.ToString())
                    {
                        Button dpEmpdept = (Button)e.Row.FindControl("btnEdit");
                        dpEmpdept.Enabled = false;
                    }

                }
            }
        }

    }
    protected void btnSortBy_Click(object sender, EventArgs e)
    {
        sortby = "LastName, FirstName";
        btnSendEMail_Click1(sender, e);
    }
    protected void btnSortBy2_Click(object sender, EventArgs e)
    {
        sortby = "Years desc,  Sessions, LastName, FirstName";
        btnSendEMail_Click1(sender, e);
    }
    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlPhase.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlPhase.SelectedValue = objCommon.GetDefaultSemester(lstYear.SelectedValue);
    }
    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroup(lstProductGroup);
        GetProduct();
        LoadCoachList();
    }
    protected void lstProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCoachList();
    }
    protected void ddlSessionNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCoachList();
    }
    protected void ddlRegistype_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCoachList();
    }
}