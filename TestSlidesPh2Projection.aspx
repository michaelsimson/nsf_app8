﻿<%@ Page Language="C#" CodeFile="TestSlidesPh2Projection.aspx.cs" Inherits="TestSlidesPh2Projection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>NorthSouth Foundation</title>
        <script type="text/javascript">
        function answer(txt) {
            alert(txt)
        }
        var mwin = null;
        var awin = null;
        var pwin = null;

        function MeaningWindow(word, meaning) {
            if (mwin != null) mwin.close()
            mwin = window.open('#', 'newWin2', 'toolbar=yes,location=yes,scrollbars=yes,status=yes,width=700,height=500')
            mwin.document.write("<html><head><title>Meaning</title></head><body bgcolor='#FFFFFF' text='#000000'><div align='left'><h1><font FACE='Verdana' SIZE='6' color='#990000'>Meaning for - " + word + "<br><br>" + meaning + "</font></h1></div></body></html>")
            mwin.document.close
        }

        function AnswerWindow(word, answer) {
            if (awin != null) awin.close()
            awin = window.open('#', 'newWin3', 'toolbar=yes,location=yes,scrollbars=yes,status=yes,width=600,height=300')
            awin.document.write("<html><head><title>Answer</title></head><body bgcolor='#FFFFFF' text='#000000'><div align='left'><h1><font FACE='Verdana' SIZE='6' color='#FF0000'>Answer for - " + word + "<br><br>" + answer + "</font></h1></div></body></html>")
            awin.document.close
        }


</script>
    </head>
<body>
    <asp:Repeater ID="rSlideProjection" runat="server" OnItemDataBound="rSlideProjection_ItemBound" >
        <ItemTemplate>
            <asp:HiddenField ID="hdPhaseValue" runat="server" Value='<%# Bind("Phase") %>' Visible="false" />
            <a NAME="word<%# DataBinder.Eval(Container.DataItem, "SNo").GetHashCode()%>"></a>
                     <div id="divPrev" runat="server"><span style="float:left;" runat="server"><a style="color: #3B81DD;  font-size: 16px;  padding: 5px;  font-family: arial;  text-decoration: none;" href="#word<%# DataBinder.Eval(Container.DataItem, "SNo").GetHashCode()-1%>" ><< Prev Word</a></span></div>
                 <div id="divNext" runat="server"><span style="float:right;" runat="server"><a style="color: #3B81DD;    font-size: 16px;    padding: 5px;    font-family: arial;    text-decoration: none;" href="#word<%# DataBinder.Eval(Container.DataItem, "SNo").GetHashCode()+1%>">Next Word >></a></span></div>
   <P style='page-break-before: always'>
				<div align="left"><FONT FACE='Verdana' SIZE='9'>
                <asp:Label ID="lblProductTitle" runat="server" Text=""></asp:Label></FONT></div>
			 &nbsp;
			<table width=100% border=0 cellspacing=8 cellpadding=8 style="margin-top:10px;">
			<tr >
				<td width=20%> <FONT FACE="Verdana" SIZE="6"><b>Level: <%# DataBinder.Eval(Container.DataItem, "Level/Sublevel")%> </b> </td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b><asp:Label runat="server" ID="SNoStuPhase2" Text=""  ></asp:Label>.</b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "Word").ToString().ToUpper()%></b></td>
			</tr>
			<tr>
				<td width=20%></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>A. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "A")%> </b></td>
			</tr>
			<tr>
				<td width=20%></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>B. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "B")%> </b></td>
			</tr>
			<tr>
				<td width=20%></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>C. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "C")%> </b></td>
			</tr>
			<tr>
				<td width=20%> </td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>D. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "D")%> </b></td>
			</tr>
			
			<tr>
				<td width=20%></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b><asp:Label runat="server" ID="lblENo" Text="E. "  ></asp:Label> </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><asp:Label runat="server" ID="lblEValue" Text='<%# DataBinder.Eval(Container.DataItem, "E")%>'  ></asp:Label> </b></td>
			</tr>
			
			</table>
			
				
			 &nbsp; &nbsp;&nbsp; &nbsp;
			<a href='javascript:MeaningWindow("<%# DataBinder.Eval(Container.DataItem, "Word").ToString().ToUpper()%>","<%# DataBinder.Eval(Container.DataItem, "Meaning")%>")'
				onMouseOver="window.status='status bar text';return true" onFocus="window.status='status bar text';return true"
				onMouseOut="window.status='status bar text';return true">Meaning</a>
            &nbsp; &nbsp;&nbsp; &nbsp;
			<a href='javascript:AnswerWindow("<%# DataBinder.Eval(Container.DataItem, "Word").ToString().ToUpper()%>","<%# DataBinder.Eval(Container.DataItem, "Answer")%>")'
				onMouseOver="window.status='status bar text';return true" onFocus="window.status='status bar text';return true"
				onMouseOut="window.status='status bar text';return true">Answer</a>
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


       </ItemTemplate>

	</asp:Repeater>
    
  </body>
