<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContestSelectionSum.aspx.vb" Inherits="ContestSelectionSum" title="Summary of Your Contest Selections" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table width="100%">
				<tr>
					<td class="Heading"  align="center" colspan="2">Summary of Your Contest Selections
					</td>
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">&nbsp;
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
					<asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" SortExpression="FIRST_NAME">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CName") %>' CssClass="SmallFont">
										</asp:Label>
									
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Selected" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" SortExpression="ContestCode">
									<ItemTemplate>
									<asp:Label id="lblChildNumber" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildNumber") %>' CssClass="SmallFont"/>
									<asp:Label id="lblProductCode" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>' CssClass="SmallFont"/>
									<asp:Label id="lblChapterID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterID") %>' CssClass="SmallFont"/>

									<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true"  SortExpression="Fee" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee", "{0:c}") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Date" HeaderStyle-Font-Bold="true" SortExpression="contestdate" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										
										<asp:Label id="Labe2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDate") %>' CssClass="SmallFont">
										</asp:Label>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" SortExpression="Chapter" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										
										<asp:Label id="Labe3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>' CssClass="SmallFont">
										</asp:Label>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Venue" HeaderStyle-Font-Bold="true" SortExpression="Venue" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										
										<asp:Label id="Labe4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Venue") %>' CssClass="SmallFont">
										</asp:Label>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								
							</Columns>
						</asp:datagrid>
						<center>
                            <asp:Label ID="Lblsum" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label></center>
						</td>
				</tr>
				<tr><td class="SmallFont">2/3rd of the Registration Fee is tax-deductible.
					</td>
				</tr>
				<tr id="trMeal1" runat="server" >
					<td class="SmallFont" align="center">Meals Information 
					</td>
				</tr>
				<tr id="trmeal2" runat="server"><td class="SmallFont" align="center">
				<asp:datagrid id="dgMealList" runat="server" CssClass="GridStyle" DataKeyField="Name"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Visible="False" >
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name" SortExpression="Name">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
									
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Amount" SortExpression="Amount">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Date"  SortExpression="ContestDate">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDate") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="MealType"  SortExpression="ContestDate">
									<ItemTemplate>
										<asp:Label id="MealsType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MealType") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
									</Columns>
						</asp:datagrid>
				
                    <asp:Label ID="lblmealsum" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label><br />
                    <asp:Label ID="lblgrandsum" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label>
					</td>
				</tr>
								
				
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnRegister" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="MidnightBlue" Visible="False" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo1" runat="server" CssClass="SmallFont" ForeColor="MidnightBlue" Visible="False" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestDateInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>
			</table>
			<table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left">
						<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
						</td>
						<td class="ContentSubTitle" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
						<td class="ContentSubTitle" align="left">
						<asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="~/ContestantRegistration.aspx">Back to Contest Registration</asp:hyperlink>
						 </td>
				</tr>
			</table>
    <asp:Label ID="lbltest" runat="server"></asp:Label>
</asp:Content>



 
 
 