Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class MealCharge1
    Inherits System.Web.UI.Page
    Dim Date1, Date2 As String
    Dim Mealamt1, MealAmt2 As Decimal
    Dim Year As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Year = Now.Year
        '**Debug 
        'Session("CustIndID") = 15405
        'Session("LoginID") = 15405
        'Session("LoggedIn") = "True"
        'Session("EventID") = 1   

        If LCase(Session("LoggedIn")) <> "true" Or Session("CustIndID") = 0 Then
            Server.Transfer("maintest.aspx")
        End If
        If Not IsPostBack Then
            Session("MealChargeChildID") = ""
            Session("AdultFlag") = 0
            Session("No.of.Days") = 0
            DisplayContestant()
            showcontestantGrid()
            LoadGridFamily()
            LoadDGOthers()
            DeleteRemoveContestantMealCharge()
            getselection()
            disablepaid()
            If Session("No.of.Days") = 1 Then
                If Date1 = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Convert(Varchar(10),Min(cn.contestDate),101) As Day1 from Contest Cn where cn.Contest_Year=" & Year & " and cn.EventID=1") Then
                    dgRegisteredCnst.Columns(2).Visible = False
                Else
                    dgRegisteredCnst.Columns(1).HeaderText = "Day2"
                    dgRegisteredCnst.DataBind()
                    dgRegisteredCnst.Columns(2).Visible = False
                    dgContestants.Columns(5).HeaderText = "Day2"
                    dgContestants.DataBind()
                    dgFamily.Columns(6).HeaderText = "Day2"
                    dgFamily.DataBind()
                    dgOthers.Columns(5).HeaderText = "Day2"
                    dgOthers.DataBind()
                End If

            End If
        End If
        calculateMealCharge()
    End Sub
    Private Sub calculateMealCharge()
        Dim amt As Decimal
        Try
            amt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(amount) from MealCharge where Automemberid=" & Session("CustIndID") & " and ContestYear = " & Year & "  and PaymentReference is NULL")
            lblTotal.Text = "Total Meal Charge to be Paid : " & Format$(amt, "Currency")
        Catch ex As Exception
            lblTotal.Text = "Total Meal Charge to be Paid : $0.00"
            amt = 0.0
        End Try
        If amt > 0.0 And Session("AdultFlag") = 1 And checkselection() = True Then
            btnPayNow.Visible = True
        Else
            btnPayNow.Visible = False
        End If
    End Sub
    Private Sub RemoveunpaidmealCharge()
        Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, " select * from mealcharge where Contestdate not in ('" & lblDate1.Text & "') and mealtype='Lunch' and automemberid=" & Session("CustIndID") & " and ContestYear=" & Year & " and PaymentReference is Null")
        While Reader.Read()
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from MealCharge Where MealChargeID in (" & Reader("MealChargeID") & ") and mealtype='Lunch' and ContestYear=" & Year & " and PaymentReference is NULL")
        End While
        Reader.Close()
    End Sub
    Private Sub addmealCharge()
        Try
            Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select automemberid,name,contestyear from mealcharge where mealtype='Lunch' and contestyear=" & Year & " and automemberid=" & Session("CustIndID") & "  group by  automemberid,name,contestyear having count(name)=1")
            '" select * from mealcharge where Contestdate not in ('" & lblDate1.Text & "') and automemberid=" & Session("CustIndID") & " and ContestYear=" & Year & " and PaymentReference is Null")
            While Reader.Read()
                ' Response.Write("Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & lblDate1.Text & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, " & lblMealAmt1.Text & ", ContestYear,ModifiedUserID,GetDate(),MealType from MealCharge  WHERE mealtype='Lunch' and amount > 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate2.Text & "'")

                ' Response.Write("br" & Reader("Name"))
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE mealtype='Lunch' and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate1.Text & "'") < 1 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,MealMemberID) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & lblDate1.Text & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, " & Convert.ToDecimal(lblMealAmt1.Text) & ", ContestYear,ModifiedUserID,GetDate(),MealType,MealMemberID from MealCharge  WHERE mealtype='Lunch' and amount > 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate2.Text & "'")
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & lblDate1.Text & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,GetDate(),MealType from MealCharge  WHERE mealtype='Lunch' and amount = 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate2.Text & "'")
                End If
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE mealtype='Lunch' and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate2.Text & "'") < 1 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,MealMemberID) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & lblDate2.Text & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, " & Convert.ToDecimal(LblMealAmt2.Text) & ", ContestYear,ModifiedUserID,GetDate(),MealType,MealMemberID from MealCharge  WHERE mealtype='Lunch' and amount > 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate1.Text & "'")
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & lblDate2.Text & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,GetDate(),MealType from MealCharge  WHERE mealtype='Lunch' and amount = 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name Like '%" & Reader("Name") & "%' AND ContestDate = '" & lblDate1.Text & "'")
                End If
            End While
            Reader.Close()
        Catch ex As Exception
            lblContestantsErr.Text = ex.ToString()
        End Try
    End Sub
    Sub DisplayContestant()
        Try
            ' get records from the products table
            Date1 = ""
            Date2 = ""
            Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Convert(Varchar(10),Min(cn.contestDate),101) As Day1,Convert(Varchar(10),Max(cn.contestDate),101) As Day2 from Contestant C,Child Ch,Contest Cn where c.contestYear=" & Year & " and C.ChildNumber=Ch.ChildNumber and cn.ContestID=C.ContestID and ch.Memberid=" & Session("CustIndID") & " and c.EventID=1")
            'Response.Write("select Convert(Varchar(10),Min(cn.contestDate),101) As Day1,Convert(Varchar(10),Max(cn.contestDate),101) As Day2 from Contestant C,Child Ch,Contest Cn where c.contestYear=" & Year & " and C.ChildNumber=Ch.ChildNumber and cn.ContestID=C.ContestID and ch.Memberid=" & Session("CustIndID") & " and c.EventID=1")
            Dim prevDate As Date
            While readr.Read()
                If readr("Day1") = readr("Day2") Then
                    Date1 = readr("Day1")
                    ltlDay1.Text = readr("Day1")
                    lblDate1.Text = Date1
                    Session("No.of.Days") = 1
                    RemoveunpaidmealCharge()
                    prevDate = DateTime.Parse(Date1).AddDays(-1)
                    ltlDay0.Text = prevDate.ToString("d")
                    If getCharge(lblDate1.Text, "SatDay1") = False Then
                        getCharge(lblDate1.Text, "SunDay2")
                        trDay2.Visible = True
                        trDay1.Visible = True

                        trDay0.Visible = False
                        rbtnDay1By.Visible = False
                        rbtnDay1Bn.Visible = False
                        lblD1B.Visible = False

                        'rbtnDay1By.Enabled = False
                        rbtnDay1Bn.Checked = True
                        rbtnDay0Dn.Checked = True
                        ltlDay2.Text = ltlDay1.Text
                        ltlDay1.Text = ltlDay0.Text
                    Else
                        trDay1.Visible = True
                        trDay0.Visible = True
                        rbtnDay2Bn.Checked = True
                        rbtnDay2Dn.Checked = True
                        prevDate = DateTime.Parse(Date1).AddDays(1)
                        ltlDay2.Text = prevDate.ToString("d")
                    End If
                Else
                    Date1 = readr("Day1")
                    lblDate1.Text = Date1
                    ltlDay1.Text = readr("Day1")
                    Date2 = readr("Day2")
                    ltlDay2.Text = readr("Day2")
                    lblDate2.Text = Date2
                    Session("No.of.Days") = 2
                    getCharge(lblDate1.Text, "SatDay1")
                    addmealCharge()
                    prevDate = DateTime.Parse(Date1).AddDays(-1)
                    ltlDay0.Text = prevDate.ToString("d")
                    trDay0.Visible = True
                    trDay1.Visible = True
                    trDay2.Visible = True
                End If
            End While
            readr.Close()
            '***
            Dim commandString As String = "select Ch.First_Name + ' ' + Ch.Last_name as ChildName,ch.Gender,c.ParentID,C.ChildNumber,c.ContestID,Convert(Varchar(10),cn.contestDate,101) AS contestDate,Cn.productCode,IsNull(c.PaymentReference,'(Not Paid)') as Paid from Contestant C,Child Ch,Contest Cn where c.contestYear=" & Year & " and c.EventID=1 and C.ChildNumber=Ch.ChildNumber and cn.ContestID=C.ContestID and c.Parentid=" & Session("CustIndID") & " order by C.ChildNumber,Cn.ContestDate" 'Session("CustIndID")
            'Response.Write(commandString)
            Dim dt As DataTable
            dt = New DataTable()
            Dim dr As DataRow
            dt.Columns.Add("ChildName", Type.GetType("System.String"))
            dt.Columns.Add("Gender", Type.GetType("System.String"))
            dt.Columns.Add("ChildNumber", Type.GetType("System.Int32"))
            dt.Columns.Add("ProductCodes1", Type.GetType("System.String"))
            dt.Columns.Add("ProductCodes2", Type.GetType("System.String"))
            dr = dt.NewRow()
            dr("ChildName") = ""
            dr("ProductCodes1") = Date1
            dr("ProductCodes2") = Date2
            dt.Rows.Add(dr)
            Dim LastChildNumber As Integer = 0
            Dim ChildName As String = ""
            Dim ProductCode1 As String = ""
            Dim ProductCode2 As String = ""
            Dim Gender As String = ""
            Dim ParentID As Integer = 0
            Dim flag As Integer = 0
            Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, commandString)
            While Reader.Read()
                flag = 1
                If LastChildNumber <> Reader("ChildNumber") And LastChildNumber <> 0 Then
                    dr = dt.NewRow()
                    dr("ChildName") = ChildName
                    dr("ChildNumber") = LastChildNumber
                    dr("ProductCodes1") = ProductCode1
                    dr("ProductCodes2") = ProductCode2
                    dr("Gender") = Gender
                    dt.Rows.Add(dr)
                    ProductCode1 = ""
                    ProductCode2 = ""
                    If Date1 = Reader("contestDate") Then
                        ProductCode1 = Reader("productCode")
                    Else
                        ProductCode2 = Reader("productCode")
                    End If
                    ChildName = Reader("ChildName")
                    Gender = Reader("Gender")
                    LastChildNumber = Reader("ChildNumber")
                ElseIf LastChildNumber = Reader("ChildNumber") And ProductCode1 <> "" And Date1 = Reader("contestDate") Then
                    ProductCode1 = ProductCode1 & ", " & Reader("productCode")
                    ChildName = Reader("ChildName")
                ElseIf LastChildNumber = Reader("ChildNumber") And ProductCode2 <> "" And Date2 = Reader("contestDate") Then
                    ProductCode2 = ProductCode2 & ", " & Reader("productCode")
                    ChildName = Reader("ChildName")
                ElseIf LastChildNumber = Reader("ChildNumber") And ProductCode1 = "" And Date1 = Reader("contestDate") Then
                    ProductCode1 = Reader("productCode")
                    ChildName = Reader("ChildName")
                ElseIf LastChildNumber = Reader("ChildNumber") And ProductCode2 = "" And Date2 = Reader("contestDate") Then
                    ProductCode2 = Reader("productCode")
                    ChildName = Reader("ChildName")
                ElseIf LastChildNumber <> Reader("ChildNumber") And LastChildNumber = 0 Then
                    LastChildNumber = Reader("ChildNumber")
                    If Date1 = Reader("contestDate") Then
                        ProductCode1 = Reader("productCode")
                    Else
                        ProductCode2 = Reader("productCode")
                    End If
                    ChildName = Reader("ChildName")
                    Gender = Reader("Gender")
                Else
                    'MsgBox("Error")
                End If
            End While
            Reader.Close()
            If flag = 1 Then
                dr = dt.NewRow()
                dr("ChildName") = ChildName
                dr("ChildNumber") = LastChildNumber
                dr("ProductCodes1") = ProductCode1
                dr("ProductCodes2") = ProductCode2
                dr("Gender") = Gender
                ProductCode1 = ""
                ProductCode2 = ""
                dt.Rows.Add(dr)
                dgRegisteredCnst.DataSource = dt
                dgRegisteredCnst.DataBind()
                If Session("No.of.Days") = 2 Then
                    addContestant(dt)
                End If
            End If
            dgRegisteredCnst.Visible = True
        Catch ex As Exception
            lblMsg.Text = "<BR> May Be No Contestant To Display or There is some problem in populating Contestant <br>"
            lblMsg.Text = lblMsg.Text & ex.Message
        End Try
    End Sub
    Function getCharge(ByVal cnstdate As String, ByVal cnstday As String) As Boolean
        Dim readr As SqlDataReader
        Dim flag As Boolean = False
        readr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT   DinnerDay0, BreakfastDay1, MealDay1, DinnerDay1, BreakfastDay2, MealDay2, DinnerDay2 FROM WeekCalendar where " & cnstday & "='" & cnstdate & "'")
        While readr.Read()
            flag = True
            'If Session("No.of.Days") = 1 And cnstday = "SunDay2" Then
            '    Mealamt1 = readr("MealDay2")
            '    lblMealAmt1.Text = Mealamt1
            '    'MealAmt2 = readr("MealDay2")
            '    'LblMealAmt2.Text = MealAmt2
            '    lbllunch.Text = " Lunch is mandatory. Day 1 Lunch - " & Format$(Mealamt1, "Currency")
            '    'lblDay0Dinner.Text = readr("DinnerDay1")
            '    'lblD0B.Text = Format$(lblDay0Dinner.Text, "Currency")
            '    'trDay1.Visible = False
            '    'trDay2.Visible = True
            '    'lblDay2Breakfast.Text = readr("BreakfastDay2")
            '    'lblD2B.Text = Format$(lblDay2Breakfast.Text, "Currency")
            '    'lblDay2Dinner.Text = readr("DinnerDay2")
            '    'lblD2D.Text = Format$(lblDay2Dinner.Text, "Currency")
            'Else
            Mealamt1 = readr("MealDay1")
            lblMealAmt1.Text = Mealamt1
            MealAmt2 = readr("MealDay2")
            LblMealAmt2.Text = MealAmt2

            If Session("No.of.Days") = 2 Then
                lbllunch.Text = " Lunch is mandatory. Day 1 Lunch - " & Format$(Mealamt1, "Currency") & " & Day 2 Lunch - " & Format$(MealAmt2, "Currency")
            ElseIf Session("No.of.Days") = 1 And cnstday = "SunDay2" Then
                Mealamt1 = readr("MealDay2")
                lblMealAmt1.Text = Mealamt1
                lbllunch.Text = " Lunch is mandatory. Day 2 Lunch - " & Format$(Mealamt1, "Currency")
            Else
                lbllunch.Text = " Lunch is mandatory. Day 1 Lunch - " & Format$(Mealamt1, "Currency")
            End If
            lblDay0Dinner.Text = readr("DinnerDay0")
            If Convert.ToDecimal(lblDay0Dinner.Text) = 0.0 Then
                rbtnDay0Dn.Enabled = False
                rbtnDay0Dn.Checked = True
                rbtnDay0Dy.Enabled = False
            End If
            lblD0B.Text = Format$(lblDay0Dinner.Text, "Currency")

            lblDay1Breakfast.Text = readr("BreakfastDay1")
            If Convert.ToDecimal(lblDay1Breakfast.Text) = 0.0 Then
                rbtnDay1Bn.Enabled = False
                rbtnDay1Bn.Checked = True
                rbtnDay1By.Enabled = False
            End If
            lblD1B.Text = Format$(lblDay1Breakfast.Text, "Currency")

            lblDay1Dinner.Text = readr("DinnerDay1")
            If Convert.ToDecimal(lblDay1Dinner.Text) = 0.0 Then
                rbtnDay1Dn.Enabled = False
                rbtnDay1Dn.Checked = True
                rbtnDay1DY.Enabled = False
            End If
            lblD1D.Text = Format$(lblDay1Dinner.Text, "Currency")

            lblDay2Breakfast.Text = readr("BreakfastDay2")
            If Convert.ToDecimal(lblDay2Breakfast.Text) = 0.0 Then
                rbtnDay2Bn.Enabled = False
                rbtnDay2Bn.Checked = True
                rbtnDay2By.Enabled = False
            End If
            lblD2B.Text = Format$(lblDay2Breakfast.Text, "Currency")
            lblDay2Dinner.Text = readr("DinnerDay2")
            If Convert.ToDecimal(lblDay2Dinner.Text) = 0.0 Then
                rbtnDay2Dn.Enabled = False
                rbtnDay2Dn.Checked = True
                rbtnDay2Dy.Enabled = False
            End If
            lblD2D.Text = Format$(lblDay2Dinner.Text, "Currency")
            'End If
        End While
        readr.Close()
        Return flag
    End Function

    Private Sub addContestant(ByVal dt As DataTable)
        Dim dr As DataRow
        For Each dr In dt.Rows
            If dr("ProductCodes1") = "" Then
                Dim prmArrayInsert(12) As SqlParameter
                Try
                    prmArrayInsert(0) = New SqlParameter("@EventName", "Finals")
                    prmArrayInsert(1) = New SqlParameter("@ContestYear", Year)
                    prmArrayInsert(2) = New SqlParameter("@AutoMemberID", Session("CustIndID"))
                    prmArrayInsert(3) = New SqlParameter("@ChildNumber", dr("ChildNumber"))
                    prmArrayInsert(4) = New SqlParameter("@ContestDate", DateTime.Parse(Date1))
                    prmArrayInsert(5) = New SqlParameter("@Name", dr("ChildName"))
                    prmArrayInsert(6) = New SqlParameter("@ModifiedDate", Now)
                    prmArrayInsert(7) = New SqlParameter("@ModifiedUserID", Session("LoginID"))
                    prmArrayInsert(8) = New SqlParameter("@Gender", dr("Gender"))
                    prmArrayInsert(9) = New SqlParameter("@Amount", Mealamt1)
                    prmArrayInsert(10) = New SqlParameter("@MealType", "Lunch")
                    prmArrayInsert(11) = New SqlParameter("@RetValue", SqlDbType.Int)
                    prmArrayInsert(11).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_InsertContestantForMealCharge", prmArrayInsert)
                Catch ex As SqlException
                End Try
            ElseIf dr("ProductCodes2") = "" Then
                Dim prmArrayInsert(12) As SqlParameter
                Try
                    prmArrayInsert(0) = New SqlParameter("@EventName", "Finals")
                    prmArrayInsert(1) = New SqlParameter("@ContestYear", Year)
                    prmArrayInsert(2) = New SqlParameter("@AutoMemberID", Session("CustIndID"))
                    prmArrayInsert(3) = New SqlParameter("@ChildNumber", dr("ChildNumber"))
                    prmArrayInsert(4) = New SqlParameter("@ContestDate", DateTime.Parse(Date2))
                    prmArrayInsert(5) = New SqlParameter("@Name", dr("ChildName"))
                    prmArrayInsert(6) = New SqlParameter("@ModifiedDate", Now)
                    prmArrayInsert(7) = New SqlParameter("@ModifiedUserID", Session("LoginID"))
                    prmArrayInsert(8) = New SqlParameter("@Gender", dr("Gender"))
                    prmArrayInsert(9) = New SqlParameter("@Amount", MealAmt2)
                    prmArrayInsert(10) = New SqlParameter("@MealType", "Lunch")
                    prmArrayInsert(11) = New SqlParameter("@RetValue", SqlDbType.Int)
                    prmArrayInsert(11).Direction = ParameterDirection.Output
                    SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_InsertContestantForMealCharge", prmArrayInsert)
                Catch ex As SqlException
                End Try
            End If

        Next
    End Sub
    Private Sub DeleteRemoveContestantMealCharge()
        Dim param(2) As SqlParameter
        Try
            param(0) = New SqlParameter("@ParentID", Session("CustIndID"))
            param(1) = New SqlParameter("@ContestYear", Year)
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, "usp_DeleteRemovedContestantMealCharge", param)
        Catch ex As Exception
            lblMsg.Text = ex.Message
            lblMsg.Text = (lblMsg.Text + "<BR>There is some problem deleting redundant MealCharges. Please try again.")
            Return
        End Try
    End Sub
    Private Sub showcontestantGrid()
        Try
            ' get records from the products table
            'Dim SqlChild As String = "Select LunchType from MealCharge Where AutoMemberID=" & Session("CustIndID") & " And ChildNumber is not Null"
            'Dim commandString As String = "select Ch.First_Name + ' ' + Ch.Last_name as ChildName,C.ChildNumber, Ch.Grade,Ch.Gender,Case when meal1.LunchType is NULL Then '" & ddllunchType.SelectedItem.Text & "' else meal1.LunchType End as LunchType,Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End as Day1,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End as Day2 from Contestant  C,Contest Cn,(Child Ch Left Join (Select MealChargeID,automemberid,ChildNumber,ContestDate,Mealtype,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day1 From MealCharge) Meal1  ON Meal1.ChildNumber=ch.Childnumber and meal1.mealtype='Lunch' and meal1.ContestDate in('" & lblDate1.Text & "'))Left Join (Select MealChargeID,automemberid,childnumber,Mealtype,LunchType,ContestDate,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day2 From MealCharge) Meal2  ON Meal2.childnumber=ch.childnumber and meal2.mealtype='Lunch' and meal2.ContestDate in('" & lblDate2.Text & "') where c.contestYear=" & Year & " and c.EventId=1 and C.ChildNumber=Ch.ChildNumber and cn.ContestID=C.ContestID and c.Parentid=" & Session("CustIndID") & " Group by Ch.First_Name + ' ' + Ch.Last_name,C.ChildNumber, Ch.Grade,Ch.Gender,Case when meal1.LunchType is NULL Then '" & ddllunchType.SelectedItem.Text & "' else meal1.LunchType End , Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End"
            Dim commandString As String = "select Ch.First_Name + ' ' + Ch.Last_name as ChildName,C.ChildNumber, Ch.Grade,Ch.Gender,Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End as Day1,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End as Day2,Case when meal1.LunchType is NULL Then  Case when meal2.LunchType is NULL Then '' Else meal2.Day2 End ELSE meal1.LunchType END  as LunchType from Contestant  C,Contest Cn,(Child Ch Left Join (Select MealChargeID,automemberid,ChildNumber,ContestDate,Mealtype,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day1 From MealCharge) Meal1  ON Meal1.ChildNumber=ch.Childnumber and meal1.mealtype='Lunch' and meal1.ContestDate in('" & lblDate1.Text & "'))Left Join (Select MealChargeID,automemberid,childnumber,Mealtype,LunchType,ContestDate,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day2 From MealCharge) Meal2  ON Meal2.childnumber=ch.childnumber and meal2.mealtype='Lunch' and meal2.ContestDate in('" & lblDate2.Text & "') where c.contestYear=" & Year & " and c.EventId=1 and C.ChildNumber=Ch.ChildNumber and cn.ContestID=C.ContestID and c.Parentid=" & Session("CustIndID") & " Group by Ch.First_Name + ' ' + Ch.Last_name,C.ChildNumber, Ch.Grade,Ch.Gender,Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End,Case when meal1.LunchType is NULL Then Case when meal2.LunchType is NULL Then '' Else meal2.Day2 End ELSE meal1.LunchType END"
            'Response.Write(commandString)
            'LunchType
            'select Ch.First_Name + ' ' + Ch.Last_name as ChildName,C.ChildNumber, Convert(Varchar(10),cn.contestDate,101) AS contestDate, Ch.Grade,Ch.Gender,CASE  WHEN mc.PaymentReference is NULL THEN 'Not Paid' ELSE 'Paid' END  as Paid from Child Ch,Contest Cn,Contestant  C Left Outer Join mealcharge  mc ON mc.childnumber=c.childNumber where c.contestYear=" & Year & " and C.ChildNumber=Ch.ChildNumber and cn.ContestID=C.ContestID and c.Parentid=" & Session("CustIndID") & " and mc.Contestdate=cn.contestdate group by Ch.First_Name + ' ' + Ch.Last_name ,C.ChildNumber, Convert(Varchar(10),cn.contestDate,101) , Ch.Grade,Ch.Gender,CASE  WHEN mc.PaymentReference is NULL THEN 'Not Paid' ELSE 'Paid' END "
            Dim dt As DataTable
            dt = New DataTable()
            Dim dr As DataRow
            dt.Columns.Add("ChildNumber", Type.GetType("System.Int32"))
            dt.Columns.Add("ChildName", Type.GetType("System.String"))
            dt.Columns.Add("Gender", Type.GetType("System.String"))
            dt.Columns.Add("Grade", Type.GetType("System.Int32"))
            dt.Columns.Add("Day1", Type.GetType("System.String"))
            dt.Columns.Add("Day2", Type.GetType("System.String"))
            dt.Columns.Add("LunchType", Type.GetType("System.String"))
            ' dt.Columns.Add("LunchType", Type.GetType("System.String"))
            dr = dt.NewRow()
            'dr("ChildName") = ""
            'dr("ProductCodes1") = Date1
            'dr("ProductCodes2") = Date2
            'dt.Rows.Add(dr)
            Dim LastChildNumber As Integer = 0
            Dim ChildName As String = ""
            Dim Day1 As String = ""
            Dim Day2 As String = ""
            Dim Grade As Integer = 0
            Dim Gender As String = ""
            Dim LunchType As String = ""
            Dim flag As Integer = 0
            Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, commandString)
            While Reader.Read()
                flag = 1
                If LastChildNumber <> Reader("ChildNumber") And LastChildNumber <> 0 Then
                    dr = dt.NewRow()
                    dr("ChildNumber") = LastChildNumber
                    dr("ChildName") = ChildName
                    dr("Day1") = Day1
                    dr("Day2") = Day2
                    dr("Grade") = Grade
                    dr("Gender") = Gender
                    dr("LunchType") = LunchType
                    dt.Rows.Add(dr)
                    Grade = 0
                    Gender = ""
                    Day1 = ""
                    Day2 = ""
                    Day1 = lblDate1.Text
                    Day2 = lblDate1.Text
                    Grade = Reader("Grade")
                    Gender = Reader("Gender")
                    ChildName = Reader("ChildName")
                    LastChildNumber = Reader("ChildNumber")
                    LunchType = Reader("LunchType")
                    If Session("MealChargeChildID") = "" And Reader("ChildNumber") > 0 Then
                        Session("MealChargeChildID") = Reader("ChildNumber").ToString()
                    ElseIf Reader("ChildNumber") > 0 Then
                        Session("MealChargeChildID") = Session("MealChargeChildID") & "," & Reader("ChildNumber")
                    End If
                    Day1 = Reader("Day1")
                    Day2 = Reader("Day2")
                ElseIf LastChildNumber <> Reader("ChildNumber") And LastChildNumber = 0 Then
                    Day1 = Reader("Day1")
                    Day2 = Reader("Day2")
                    Grade = Reader("Grade")
                    Gender = Reader("Gender")
                    ChildName = Reader("ChildName")
                    LastChildNumber = Reader("ChildNumber")
                    LunchType = Reader("LunchType")
                    If Session("MealChargeChildID") = "" And Reader("ChildNumber") > 0 Then
                        Session("MealChargeChildID") = Reader("ChildNumber").ToString()
                    End If
                Else
                    'MsgBox("Error")
                End If
            End While
            Reader.Close()
            If flag = 1 Then
                dr = dt.NewRow()
                dr("ChildNumber") = LastChildNumber
                dr("ChildName") = ChildName
                dr("Day1") = Day1
                dr("Day2") = Day2
                dr("Grade") = Grade
                dr("Gender") = Gender
                dr("LunchType") = LunchType
                dt.Rows.Add(dr)
                dgContestants.DataSource = dt
                dgContestants.DataBind()
                If Session("No.of.Days") = 1 Then
                    dgContestants.Columns(6).Visible = False
                End If
                If Session("MealChargeChildID") = "" And LastChildNumber > 0 Then
                    Session("MealChargeChildID") = LastChildNumber
                ElseIf LastChildNumber > 0 Then
                    Session("MealChargeChildID") = Session("MealChargeChildID") & "," & LastChildNumber.ToString()
                End If
            End If
            lblMsg.Text = ""
        Catch ex As Exception
            lblMsg.Text = "<BR> There is some problem showing Contestants <br>"
            lblMsg.Text = lblMsg.Text & ex.Message
        End Try
    End Sub
    Private Sub LoadGridFamily()
        If Not Session("MealChargeChildID") Is Nothing Then
            If Session("MealChargeChildID") = "" Then
                lblTotal.Visible = False
                lblError.Text = "No contests were registered.  Please go back and select contests."
                lblFamilyErr.Text = "No Contestant details to Display"
            Else
                lblError.Text = ""
                lblTotal.Visible = True
                Dim dsFamily As New DataSet
                Dim tblFamily() As String = {"Family"}

                'Dim StrSQL As String = "(SELECT DISTINCT  i.Automemberid,  '0000' as ChildNumber, 'Adult' as UnderFive,  i.Email, i.HPhone, i.CPhone,i.Gender,  RelationToContestant = case lower(i.Gender) when 'male' then 'Father' else 'Mother' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') AS   Parents   FROM indspouse i  INNER JOIN contestant c on c.ParentID = i.automemberid  WHERE  i.automemberid  IN(SELECT DISTINCT memberid from child where  childnumber IN (" & Session("MealChargeChildID") & ") ) UNION "
                'StrSQL = StrSQL & "select  '0000' as Automemberid, ch.ChildNumber,    UnderFive = CASE  WHEN DateDiff(yy, date_of_Birth , getdate()) BETWEEN 0 AND 5 THEN 'Under 5' WHEN DateDiff(yy, date_of_Birth , getdate()) BETWEEN 5 AND 19 THEN 'Minor' ELSE 'Adult' END,   i.Email, i.HPhone, i.CPhone,i.Gender,  RelationToContestant = case lower(ch.Gender) when 'male' then 'Brother' else 'Sister' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(ch.FIRST_NAME + ' ' + COALESCE (ch.MIDDLE_INITIAL, '') + ' ' + ch.LAST_NAME, '  ', ' ') AS   Parents       	 from Child ch  INNER JOIN indspouse i  on ch.MemberID = i.automemberid  where ch.memberid in (select distinct parentid from contestant where childnumber in (" & Session("MealChargeChildID") & ")) and ch.childnumber NOT IN (" & Session("MealChargeChildID") & "))  ORDER BY Parents"
                Dim StrSQL As String = "(SELECT DISTINCT i.Automemberid,  '0000' as ChildNumber, 'Adult' as UnderFive,  i.Email, i.HPhone, i.CPhone,i.Gender,  RelationToContestant = case lower(i.Gender) when 'male' then 'Father' else 'Mother' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') AS   Parents,Case when meal1.LunchType is NULL Then '" + ddllunchType.SelectedItem.Text + "' Else meal1.LunchType End as LunchType,Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End as Day1,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End as Day2,Case When Meal2.MealChargeID is Null then Convert(Varchar,Meal1.MealChargeID) Else Convert(Varchar,Meal1.MealChargeID) + ','+ Convert(Varchar,Meal2.MealChargeID) End  AS MealChargeID FROM  contestant c, (indspouse i  Left Join (Select MealChargeID,automemberid,Name,ContestDate,MealType,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day1 From MealCharge) Meal1  ON Meal1.automemberid=i.automemberid and meal1.mealtype='Lunch' and meal1.Name = REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') and meal1.ContestDate in('" & lblDate1.Text & "')) Left Join (Select MealChargeID,automemberid,Name,MealType,LunchType,ContestDate,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day2 From MealCharge) Meal2  ON Meal2.automemberid=i.automemberid and meal2.mealtype='Lunch' and meal2.Name = REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') and meal2.ContestDate in('" & lblDate2.Text & "') WHERE  i.automemberid  IN(SELECT DISTINCT memberid from child where  childnumber IN(" & Session("MealChargeChildID") & ")) and c.ParentID = i.automemberid) UNION "
                StrSQL = StrSQL & "(SELECT DISTINCT  i.Automemberid, '0000' as ChildNumber, 'Adult' as UnderFive,  i.Email, i.HPhone, i.CPhone, i.Gender, RelationToContestant = case lower(i.Gender) when 'male' then 'Father' else 'Mother' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') AS   Parents,Case when meal1.LunchType is NULL Then '" + ddllunchType.SelectedItem.Text + "' Else meal1.LunchType End as LunchType,Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End as Day1,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End as Day2,Case When Meal2.MealChargeID is Null then Convert(Varchar,Meal1.MealChargeID) Else Convert(Varchar,Meal1.MealChargeID) + ','+ Convert(Varchar,Meal2.MealChargeID) End  AS MealChargeID  FROM  contestant c, (indspouse i  Left Join (Select MealChargeID,automemberid,Name,ContestDate,MealType,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day1 From MealCharge) Meal1  ON Meal1.automemberid=i.relationship and meal1.mealtype='Lunch' and meal1.Name = REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') and meal1.ContestDate in('" & lblDate1.Text & "')) Left Join (Select MealChargeID,automemberid,Name,ContestDate,MealType,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day2 From MealCharge) Meal2  ON Meal2.automemberid=i.relationship and meal2.mealtype='Lunch' and meal2.Name = REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') and meal2.ContestDate in('" & lblDate2.Text & "') WHERE  i.relationship  IN(SELECT DISTINCT memberid from child where  childnumber IN(" & Session("MealChargeChildID") & ")) and c.ParentID =  i.relationship) UNION "
                StrSQL = StrSQL & "(select  '0000' as Automemberid, ch.ChildNumber,    UnderFive = CASE  WHEN DateDiff(yy, date_of_Birth , getdate()) BETWEEN 0 AND 5 THEN 'Under 5' WHEN DateDiff(yy, date_of_Birth , getdate()) BETWEEN 5 AND 19 THEN 'Minor' ELSE 'Adult' END,   i.Email, i.HPhone, i.CPhone,ch.Gender,  RelationToContestant = case lower(ch.Gender) when 'male' then 'Brother' else 'Sister' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(ch.FIRST_NAME + ' ' + COALESCE (ch.MIDDLE_INITIAL, '') + ' ' + ch.LAST_NAME, '  ', ' ') AS   Parents,Case when meal1.LunchType is NULL Then '" + ddllunchType.SelectedItem.Text + "' Else meal1.LunchType End as LunchType,Case when meal1.Day1 is NULL Then 'Not Selected' Else meal1.Day1 End as Day1,Case when meal2.Day2 is NULL Then 'Not Selected' Else meal2.Day2 End as Day2, Case When Meal2.MealChargeID is Null then Convert(Varchar,Meal1.MealChargeID) Else Convert(Varchar,Meal1.MealChargeID) + ','+ Convert(Varchar,Meal2.MealChargeID) End  AS MealChargeID from indspouse i, (Child ch  Left Join (Select MealChargeID,childNumber,Name,ContestDate,MealType,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day1 From MealCharge) Meal1  ON Meal1.ChildNumber=Ch.ChildNumber and meal1.mealtype='Lunch' and meal1.Name = REPLACE(ch.FIRST_NAME + ' ' + COALESCE (ch.MIDDLE_INITIAL, '') + ' ' + ch.LAST_NAME, '  ', ' ') and meal1.ContestDate in('" & lblDate1.Text & "')) Left Join (Select MealChargeID,childNumber,Name,ContestDate,MealType,LunchType,Case when PaymentReference is NULL Then 'Not Paid' Else 'Paid' End as Day2 From MealCharge) Meal2  ON Meal2.ChildNumber=ch.childNumber and meal2.mealtype='Lunch' and meal2.Name = REPLACE(ch.FIRST_NAME + ' ' + COALESCE (ch.MIDDLE_INITIAL, '') + ' ' + ch.LAST_NAME, '  ', ' ') and meal2.ContestDate in('" & lblDate2.Text & "')   where ch.memberid in (select distinct parentid from contestant where childnumber in (" & Session("MealChargeChildID") & ")) and ch.childnumber NOT IN (" & Session("MealChargeChildID") & ") and ch.MemberID = i.automemberid)"
                StrSQL = StrSQL & "  UNION  SELECT '0000' as Automemberid,'0000' as ChildNumber, Meal1.Age as UnderFive,Meal1.EmailID as Email,Meal1.HomePhone as Hphone,Meal1.CellPHone as Cphone,Meal1.Gender as Gender,Meal1.RelationType as RelationToContestant, Meal1.Amount as Day1MealCharge ,  meal2.Amount as Day2MealCharge,Meal1.Name as Parents,Case when meal1.LunchType is NULL Then '" + ddllunchType.SelectedItem.Text + "' Else meal1.LunchType End as LunchType, Case when Meal1.PaymentReference is Null Then 'Not Paid' Else 'Paid' END as Day1,Case when Meal2.Day2 is Null Then 'Not Selected' Else Meal2.Day2 END as Day2, Case When Meal2.MealChargeID is Null then Convert(Varchar,Meal1.MealChargeID) Else Convert(Varchar,Meal1.MealChargeID) + ','+ Convert(Varchar,Meal2.MealChargeID) End  AS MealChargeID  FROM MealCharge Meal1 Left Join(SELECT Automemberid,Amount,Name, MealChargeID, Gender, Age,RelationType,ContestDate,MealType,LunchType,Case when PaymentReference is Null Then 'Not Paid' Else 'Paid' END as Day2  FROM MealCharge) Meal2 ON Meal2.Automemberid=Meal1.Automemberid  and meal2.mealtype='Lunch' and Meal2.Name=Meal1.Name and Meal1.Relationtype=Meal2.RelationType and Meal2.ContestDate in ('" & lblDate2.Text & "') where Meal1.RelationType not in('Other Guest') and Meal1.OtherGuest is not NUll  and Meal1.automemberid=" & Session("CustIndID") & " and meal1.mealtype='Lunch' and Meal1.ContestDate='" & lblDate1.Text & "' ORDER BY Parents"

                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsFamily, tblFamily)
                Dim mydv As New DataView

                mydv.Table = dsFamily.Tables(0)
                If mydv.Count > 0 Then
                    dgFamily.DataSource = mydv
                    dgFamily.DataBind()
                Else
                    lblFamilyErr.Text = "No Family Record to Display"
                End If
                If Session("No.of.Days") = 1 Then
                    dgFamily.Columns(7).Visible = False
                End If
                calculateMealCharge()
            End If
        End If
    End Sub
    Private Sub LoadDGOthers()
        Dim dsOthers As New DataSet
        Dim tblOthers() As String = {"Others"}
        'Dim StrSQL As String = "(SELECT DISTINCT  i.Automemberid,  '0000' as ChildNumber, 'Adult' as UnderFive,  i.Email, i.HPhone, i.CPhone,i.Gender,  RelationToContestant = case lower(i.Gender) when 'male' then 'Father' else 'Mother' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(i.FIRSTNAME + ' ' + COALESCE (i.MIDDLEINITIAL, '') + ' ' + i.LASTNAME, '  ', ' ') AS   Parents   FROM indspouse i  INNER JOIN contestant c on c.ParentID = i.automemberid  WHERE  i.automemberid  IN(SELECT DISTINCT memberid from child where  childnumber IN (" & Session("MealChargeChildID") & ") ) UNION "
        'StrSQL = StrSQL & "select  '0000' as Automemberid, ch.ChildNumber,    UnderFive = CASE  WHEN DateDiff(yy, date_of_Birth , getdate()) BETWEEN 0 AND 5 THEN 'Under 5' WHEN DateDiff(yy, date_of_Birth , getdate()) BETWEEN 5 AND 19 THEN 'Minor' ELSE 'Adult' END,   i.Email, i.HPhone, i.CPhone,i.Gender,  RelationToContestant = case lower(ch.Gender) when 'male' then 'Brother' else 'Sister' End,  Day1MealCharge = Convert(Decimal(8,2),10),   Day2MealCharge = Convert(Decimal(8,2),10),   REPLACE(ch.FIRST_NAME + ' ' + COALESCE (ch.MIDDLE_INITIAL, '') + ' ' + ch.LAST_NAME, '  ', ' ') AS   Parents       	 from Child ch  INNER JOIN indspouse i  on ch.MemberID = i.automemberid  where ch.memberid in (select distinct parentid from contestant where childnumber in (" & Session("MealChargeChildID") & ")) and ch.childnumber NOT IN (" & Session("MealChargeChildID") & "))  ORDER BY Parents"
        Dim StrSQL As String = "SELECT Meal1.Name,Meal1.RelationType,  Case When Meal2.MealChargeID is Null then Convert(Varchar,Meal1.MealChargeID) Else Convert(Varchar,Meal1.MealChargeID) + ','+ Convert(Varchar,Meal2.MealChargeID) End  AS MealChargeID, Meal1.Gender, Meal1.Age,Case when Meal1.PaymentReference is Null Then 'Not Paid' Else 'Paid' END as Day1,Case when Meal2.Day2 is Null Then 'Not Selected' Else Meal2.Day2 END as Day2,Case when Meal1.LunchType IS Null Then 'NULL' Else Meal1.LunchType END as LunchType  FROM MealCharge Meal1 Left Join(SELECT Automemberid,Name, MealChargeID, Gender, Age,RelationType,MealType,ContestDate,LunchType,Case when PaymentReference is Null Then 'Not Paid' Else 'Paid' END as Day2  FROM MealCharge) Meal2 ON Meal2.Automemberid=Meal1.Automemberid  and meal2.mealtype='Lunch' and Meal2.Name=Meal1.Name and Meal1.Relationtype=Meal2.RelationType and Meal2.ContestDate in ('" & lblDate2.Text & "') where  Meal1.ContestDate='" & lblDate1.Text & "' AND meal1.mealtype='Lunch' and Meal1.RelationType = 'Other Guest' and Meal1.automemberid=" & Session("CustIndID")
        'Response.Write(StrSQL)
        SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsOthers, tblOthers)
        Dim mydv As New DataView
        mydv.Table = dsOthers.Tables(0)

        dgOthers.DataSource = mydv
        dgOthers.DataBind()
        If mydv.Count = 0 Then
            lblOthersErr.Text = "No Guest Record to Display"
            dgOthers.Visible = False
        Else
            dgOthers.Visible = True
            lblOthersErr.Text = ""
        End If
        If Session("No.of.Days") = 1 Then
            dgOthers.Columns(6).Visible = False
        End If
        calculateMealCharge()
    End Sub

    Private Sub InsertFamilyMember()
        Dim intCtr As Integer, blnAddMembersForMeal As Boolean
        Dim myTableCell As TableCell, chkSelected As CheckBox, mylbl As Label, blnInclude1 As Boolean, blnInclude2 As Boolean
        If dgFamily.Items.Count <= 0 Then Exit Sub
        blnInclude1 = False
        myTableCell = dgFamily.Items.SyncRoot.Item(intCtr).Cells(5)
        mylbl = CType(myTableCell.FindControl("lblFamilyInclude1"), Label)
        blnInclude1 = True
        If Session("No.of.Days") = 2 Then
            blnInclude2 = True
        End If
        'MsgBox(Session("No.of.Days"))
        lblFamilyErr.Text = ""
        blnAddMembersForMeal = False
        For intCtr = 0 To dgFamily.Items.Count - 1
            myTableCell = dgFamily.Items.SyncRoot.Item(intCtr).Cells(1)
            chkSelected = myTableCell.Controls(1)
            If chkSelected.Checked = True Then
                If blnInclude1 = True Then
                    If InsertMembersForMeal(intCtr, DateTime.Parse(lblDate1.Text), lblMealAmt1.Text) = True Then
                        blnAddMembersForMeal = True
                    End If
                End If

                If blnInclude2 = True Then
                    If InsertMembersForMeal(intCtr, DateTime.Parse(lblDate2.Text), LblMealAmt2.Text) = True Then
                        blnAddMembersForMeal = True
                    End If
                End If
            End If
        Next
        If Trim(lblFamilyErr.Text) <> "" Then
            lblFamilyErr.Visible = True
        Else
            lblFamilyErr.Visible = False
        End If
        'DisplayAllForMeal()
        If blnAddMembersForMeal = True Then
            LoadGridFamily()
        End If
    End Sub

    Private Function InsertMembersForMeal(ByVal GridItem As Integer, ByVal dtContestDate As Date, ByVal Mealamt As String) As Boolean

        Dim EventName As String, AutoMemberID As Int32, ChildNumber As Int32
        Dim EmailID As String, HomePhone As String, CellPhone As String, ChaperoneFlag As String = ""
        Dim Name As String, RelationType As String, Gender As String, Age As String, LunchType As String, Amount As Decimal
        Dim SpouseID As Integer = 0
        Dim myTableCell As TableCell
        Dim checkSelected As CheckBox

        'EventName
        EventName = "Finals"

        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(1)
        checkSelected = myTableCell.Controls(1)

        'RelationType
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(2)
        RelationType = CType(myTableCell.FindControl("lblRelationship"), Label).Text

        'Gender
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(3)
        Gender = CType(myTableCell.FindControl("lblGender"), Label).Text

        'Age
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(4)
        Age = CType(myTableCell.FindControl("lblAge"), Label).Text

        'LunchType
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(14)
        LunchType = CType(myTableCell.FindControl("ddllunchTypeNC"), DropDownList).SelectedItem.Text '  ddllunchTypeNC.SelectedItem.Text

        If Age.Trim = "Under 5" Then
            Amount = 0.0
        Else
            Amount = Convert.ToDecimal(Mealamt)
        End If

        'AutoMemberID
        AutoMemberID = Session("CustIndID")

        'ChildNumber, ChaperoneFlag
        Select Case LCase(RelationType)
            Case "father", "mother"
                myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(0)
                ' If Not AutoMemberID = myTableCell.Text Then
                SpouseID = myTableCell.Text
                'End If
                ChildNumber = 0
                ChaperoneFlag = "Y"
            Case "brother", "sister"
                myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(11)
                ChildNumber = myTableCell.Text
                ChaperoneFlag = "N"
        End Select

        'Name
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(12)
        Name = myTableCell.Text

        'EmailID
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(8)
        EmailID = myTableCell.Text

        'HomePhone
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(9)
        HomePhone = myTableCell.Text

        'CellPhone
        myTableCell = dgFamily.Items.SyncRoot.Item(GridItem).Cells(10)
        CellPhone = myTableCell.Text
        Dim param(20) As SqlParameter
        Try
            param(0) = New SqlParameter("@EventName", EventName)
            param(1) = New SqlParameter("@AutoMemberID", AutoMemberID)
            param(2) = New SqlParameter("@ChildNumber", IIf(ChildNumber = 0, DBNull.Value, ChildNumber))
            param(3) = New SqlParameter("@ContestDate", dtContestDate)
            param(4) = New SqlParameter("@Name", Name)
            param(5) = New SqlParameter("@RelationType", RelationType)
            param(6) = New SqlParameter("@EmailID", EmailID)
            param(7) = New SqlParameter("@HomePhone", HomePhone)
            param(8) = New SqlParameter("@CellPhone", IIf(CellPhone.Length < 3, DBNull.Value, CellPhone))
            param(9) = New SqlParameter("@ModifiedDate", Now())
            param(10) = New SqlParameter("@ModifiedUserID", Session("LoginID"))
            param(11) = New SqlParameter("@ChaperoneFlag", ChaperoneFlag)
            param(12) = New SqlParameter("@ContestYear", Year)
            param(13) = New SqlParameter("@Gender", Gender)
            param(14) = New SqlParameter("@Age", Age)
            param(15) = New SqlParameter("@Amount", Amount)
            param(16) = New SqlParameter("@MealType", "Lunch")
            param(17) = New SqlParameter("@MealMemberID", IIf(SpouseID = 0, DBNull.Value, SpouseID))
            param(18) = New SqlParameter("@LunchType", LunchType)
            param(19) = New SqlParameter("@RetValue", SqlDbType.Int)
            param(19).Direction = ParameterDirection.Output

            SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_InsertMembersForMealCharge", param)
        Catch ex As SqlException
            lblFamilyErr.Text = lblFamilyErr.Text & " " & ex.Message
            Return False
        End Try
        Return True

    End Function

    Protected Sub dgFamily_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFamily.ItemCommand
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lblRemove"), LinkButton)
        ' Dim ddlLunchType As DropDownList = e.Item.FindControl("ddllunchtype") ', TextBox)
        If (Not (lbtn) Is Nothing) Then

            Dim Name As String = e.Item.Cells(15).Text
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from MealCharge Where Name like ('" & Name & "') and ContestYear=" & Year & " and PaymentReference is NULL and Automemberid=" & Session("CustIndID") & "")
                Session("AdultFlag") = 0
                LoadDGOthers()
                LoadGridFamily()

                'If e..Row.RowType = DataControlRowType.DataRow Then
                If Not DataBinder.Eval(e.Item.DataItem, "ddllunchtype") Is DBNull.Value Then
                    Dim lunchtype1 As String = DataBinder.Eval(e.Item.DataItem, "ddllunchtype")
                    e.Item.Cells(19).Text = lunchtype1 ' IIf(CheckPaymentNotes(ddllunchType), "True", "False")
                    ddllunchType.SelectedIndex = ddllunchType.Items.IndexOf(ddllunchType.Items.FindByValue(lunchtype1))
                End If
                ' End If

            Catch ex As Exception
                lblMsg.Text = ex.Message
                lblMsg.Text = (lblMsg.Text + "<BR>Remove MealCharge failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub dgFamily_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFamily.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim Chk1 As CheckBox = CType(e.Item.FindControl("chkFamilySelect"), CheckBox)
                Dim btnlink As LinkButton, lblStatus, lblLunch As Label, lblStatus1 As Label, lblage As Label
                Dim ddllunchTypeNC As DropDownList

                btnlink = CType(e.Item.FindControl("lblRemove"), LinkButton)
                lblStatus = CType(e.Item.FindControl("lblDay1"), Label)
                lblStatus1 = CType(e.Item.FindControl("lblDay2"), Label)
                lblLunch = CType(e.Item.FindControl("lblLunchType"), Label)
                ddllunchTypeNC = CType(e.Item.FindControl("ddllunchTypeNC"), DropDownList)
                lblage = CType(e.Item.FindControl("lblAge"), Label)

                If lblStatus1.Text = "Paid" Then
                    dgFamily.Columns(7).Visible = True
                End If
                If lblStatus.Text = "Paid" Or lblStatus1.Text = "Paid" Then
                    Chk1.Visible = False
                    btnlink.Visible = False
                ElseIf lblStatus.Text = "Not Selected" And lblStatus1.Text = "Not Selected" Then
                    btnlink.Visible = False
                Else
                    btnlink.Visible = True
                    Chk1.Visible = False
                End If
                If lblage.Text.Trim = "Under 5" Then
                    lblStatus.Text = "Free"
                    lblStatus1.Text = "Free"
                ElseIf lblage.Text.Trim = "Adult" And lblStatus.Text <> "Not Selected" Then
                    Session("AdultFlag") = 1
                End If

                If Chk1.Visible = True Then
                    ddllunchTypeNC.Visible = True
                    lblLunch.Visible = False
                Else
                    ddllunchTypeNC.Visible = False
                    lblLunch.Visible = True
                End If
        End Select
    End Sub

    Protected Sub dgOthers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOthers.ItemCommand
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lblRemove"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim Name As String = e.Item.Cells(0).Text
            Dim DeleteStatus As String = e.Item.Cells(1).Text
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from MealCharge Where Name like ('" & Name & "') and ContestYear=" & Year & " and PaymentReference is NULL and Automemberid=" & Session("CustIndID") & "")
                Session("AdultFlag") = 0
                LoadDGOthers()
                LoadGridFamily()
            Catch ex As Exception
                lblMsg.Text = ex.Message
                lblMsg.Text = (lblMsg.Text + "<BR>Remove MealCharge failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub dgOthers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOthers.ItemDataBound

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                'Dim Chk1 As CheckBox = CType(e.Item.FindControl("chkFamilySelect"), CheckBox)
                'lblDay1
                Dim btnlink As LinkButton, lblStatus, lblLunch As Label, lblStatus1 As Label, lblage As Label
                btnlink = CType(e.Item.FindControl("lblRemove"), LinkButton)
                lblStatus = CType(e.Item.FindControl("lblDay1"), Label)
                lblage = CType(e.Item.FindControl("lblAge"), Label)
                'lblAge
                'If lblStatus.Text = "Paid" Then
                'Chk1.Visible = False
                'End If
                lblStatus1 = CType(e.Item.FindControl("lblDay2"), Label)
                lbllunch = CType(e.Item.FindControl("lblLunchType"), Label)
                If lblStatus1.Text = "Paid" Then
                    dgFamily.Columns(6).Visible = True
                End If
                If lblStatus.Text = "Paid" Or lblStatus1.Text = "Paid" Then
                    btnlink.Visible = False
                ElseIf lblStatus.Text = "Not Selected" And lblStatus1.Text = "Not Selected" Then
                    btnlink.Visible = False
                Else
                    btnlink.Visible = True
                End If
                If lblage.Text.Trim = "Under 5" Then
                    lblStatus.Text = "Free"
                    lblStatus1.Text = "Free"
                ElseIf lblage.Text.Trim = "Adult" Then
                    Session("AdultFlag") = 1
                End If
        End Select
    End Sub

    Protected Sub BtnAddGuest_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddGuest.Visible = True
    End Sub

    Protected Sub btnOtherAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOtherAdd.Click
        lblOtherErr.Text = ""
        Dim RelationType As String = "Other Guest", ChaperoneFlag As String = ""
        Dim EventName As String, AutoMemberID As Int32, UnderFive As String, intAmount As Int32, Name As String
        Try
            If txtOtherFName.Text.Length < 3 Then
                lblOtherErr.Text = "Please Enter Name"
            ElseIf txtOtherEmail.Text.Length < 3 Then
                lblOtherErr.Text = "Please Enter Email"
            ElseIf txtOtherHphone.Text.Length < 3 Then
                lblOtherErr.Text = "Please Enter Home Phone Number"
            Else
                Name = txtOtherFName.Text & " " & txtOtherLName.Text
                'EventName
                EventName = "Finals"
                'AutoMemberID
                AutoMemberID = Session("CustIndID")

                'UnderFive
                Select Case ddlOtherAge.SelectedValue
                    Case "Under 5"
                        UnderFive = "Y"
                        intAmount = 0
                        ChaperoneFlag = "N"

                    Case Else
                        UnderFive = "N"
                        intAmount = 10
                        ChaperoneFlag = "Y"
                End Select
                If ddlOtherNSF.SelectedValue = "N" Then
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE ContestYear = " & Year & " AND AutoMemberID = " & AutoMemberID & " AND Name Like '%" & Name & "%' AND ContestDate = '" & lblDate1.Text & "'") < 1 Then
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID, OtherGuest, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,LunchType) values('" & EventName & "'," & AutoMemberID & ",'N','" & DDLOtherGender.SelectedValue & "','" & ddlOtherAge.SelectedValue & "','" & lblDate1.Text & "','" & Name & "','Other Guest','" & UnderFive & "','" & txtOtherEmail.Text & "','" & txtOtherHphone.Text & "','" & txtOtherCPhone.Text & "','" & ChaperoneFlag & "'," & IIf(intAmount = 0, 0, Convert.ToDecimal(lblMealAmt1.Text)) & "," & Year & "," & Session("LoginID") & ",GetDate(),'Lunch','" & ddllunchType.SelectedItem.Text & "')")
                        lblOtherErr.Text = "Inserted Successfully"
                    End If
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE ContestYear = " & Year & " AND AutoMemberID = " & AutoMemberID & " AND Name Like '%" & Name & "%' AND ContestDate = '" & lblDate2.Text & "'") < 1 And Session("No.of.Days") = 2 Then
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID, OtherGuest, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,LunchType) values('" & EventName & "'," & AutoMemberID & ",'N','" & DDLOtherGender.SelectedValue & "','" & ddlOtherAge.SelectedValue & "','" & lblDate2.Text & "','" & Name & "','Other Guest','" & UnderFive & "','" & txtOtherEmail.Text & "','" & txtOtherHphone.Text & "','" & txtOtherCPhone.Text & "','" & ChaperoneFlag & "'," & IIf(intAmount = 0, 0, Convert.ToDecimal(LblMealAmt2.Text)) & "," & Year & "," & Session("LoginID") & ",GetDate(),'Lunch','" & ddllunchType.SelectedItem.Text & "')")
                        lblOtherErr.Text = "Inserted Successfully"
                    End If
                    ClearOther()
                Else
                    Try
                        Dim OtherGuestID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Automemberid from Indspouse where email='" & txtOtherEmail.Text & "' and firstname like '%" & txtOtherFName.Text & "%' ")
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE ContestYear = " & Year & " AND AutoMemberID = " & AutoMemberID & " AND Name Like '%" & txtOtherFName.Text & "%' AND ContestDate = '" & lblDate1.Text & "'") < 1 Then
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID, OtherGuest, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,OtherGuestID,ModifiedUserID,Modifieddate,MealType,LunchType)  Select '" & EventName & "'," & AutoMemberID & ",'Y',Gender,'" & ddlOtherAge.SelectedValue & "','" & lblDate1.Text & "','" & Name & "','Other Guest','" & UnderFive & "',Email,Hphone,Cphone,'" & ChaperoneFlag & "'," & IIf(intAmount = 0, 0, Convert.ToDecimal(lblMealAmt1.Text)) & "," & Year & "," & OtherGuestID & "," & Session("LoginID") & ",GetDate(),'Lunch','" & ddllunchType.SelectedItem.Text & "' from Indspouse where automemberid=" & OtherGuestID & "")
                            lblOtherErr.Text = "Inserted Successfully"
                        End If
                        If Session("No.of.Days") = 2 Then
                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE ContestYear = " & Year & " AND AutoMemberID = " & AutoMemberID & " AND Name Like '%" & txtOtherFName.Text & "%' AND ContestDate = '" & lblDate2.Text & "'") < 1 Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID, OtherGuest, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,OtherGuestID,ModifiedUserID,Modifieddate,MealType) Select '" & EventName & "'," & AutoMemberID & ",'Y',Gender,'" & ddlOtherAge.SelectedValue & "','" & lblDate2.Text & "','" & Name & "','Other Guest','" & UnderFive & "',Email,Hphone,Cphone,'" & ChaperoneFlag & "'," & IIf(intAmount = 0, 0, Convert.ToDecimal(LblMealAmt2.Text)) & "," & Year & "," & OtherGuestID & "," & Session("LoginID") & ",GetDate(),'Lunch','" & ddllunchType.SelectedItem.Text & "' from Indspouse where automemberid=" & OtherGuestID & "")
                                lblOtherErr.Text = "Inserted Successfully"
                            End If
                        End If
                        ClearOther()
                    Catch ex As Exception
                        lblOtherErr.Text = "No match found for Email and Name" '& ex.ToString()
                    End Try
                End If
            End If
            LoadDGOthers()
            selections()
        Catch ex As SqlException
            lblOthersErr.Text = lblOtherErr.Text & " " & ex.Message
        End Try

    End Sub
    Private Sub ClearOther()
        txtOtherFName.Text = ""
        txtOtherLName.Text = ""
        txtOtherEmail.Text = ""
        txtOtherHphone.Text = ""
        txtOtherCPhone.Text = ""
    End Sub

    Protected Sub AddFamily_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddFamilyMembers.Visible = True
    End Sub

    Protected Sub btnOthersClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        AddGuest.Visible = False
    End Sub

    Protected Sub btnFamilyAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblAddFamilyErr.Text = ""
        If txtFname.Text.Length < 3 Then
            lblAddFamilyErr.Text = "Please Enter First Name"
        ElseIf txtLname.Text.Length < 3 Then
            lblAddFamilyErr.Text = "Please Enter Last Name"
        Else
            Dim EventName As String, AutoMemberID As Int32, ChildNumber As Int32
            Dim EmailID As String, HomePhone As String, CellPhone As String, ChaperoneFlag As String = "", UnderFive As String, intAmount As Int32
            Dim Name As String
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select * from Indspouse where Automemberid=" & Session("CustIndID") & "")
            While reader.Read()
                EmailID = reader("Email")
                HomePhone = reader("Hphone")
                CellPhone = reader("Cphone")
            End While
            Dim param(16) As SqlParameter
            'EventName
            EventName = "Finals"
            'AutoMemberID
            AutoMemberID = Session("CustIndID")
            'ChildNumber, ChaperoneFlag
            Select Case LCase(ddlRelationship.SelectedValue)
                Case "father", "mother"
                    ChildNumber = 0
                    ChaperoneFlag = "Y"
                    UnderFive = "N"

                Case "brother", "sister"
                    ChildNumber = 0
                    ChaperoneFlag = "N"
            End Select
            'UnderFive
            Select Case ddlAge.SelectedValue
                Case "Under 5"
                    UnderFive = "Y"
                    intAmount = 0
                    ChaperoneFlag = "N"
                Case Else
                    UnderFive = "N"
                    intAmount = 10
                    ChaperoneFlag = "Y"
            End Select
            'Name
            Name = txtFname.Text & " " & txtLname.Text
            Try
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE ContestYear = " & Year & " AND AutoMemberID = " & AutoMemberID & " AND Name Like '%" & Name & "%' AND ContestDate = '" & lblDate1.Text & "'") < 1 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID, OtherGuest, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,LunchType) values('" & EventName & "'," & AutoMemberID & ",'N','" & ddlGender.SelectedValue & "','" & ddlAge.SelectedValue & "','" & lblDate1.Text & "','" & Name & "','" & ddlRelationship.SelectedValue & "','" & UnderFive & "','" & EmailID & "','" & HomePhone & "','" & CellPhone & "','" & ChaperoneFlag & "'," & IIf(intAmount = 0, 0, Convert.ToDecimal(lblMealAmt1.Text)) & "," & Year & "," & Session("LoginID") & ",GetDate(),'Lunch','" & ddllunchType.SelectedItem.Text & "')")
                    lblAddFamilyErr.Text = "Inserted Successfully"
                End If
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*)  FROM MealCharge WHERE ContestYear = " & Year & " AND AutoMemberID = " & AutoMemberID & " AND Name Like '%" & Name & "%' AND ContestDate = '" & lblDate2.Text & "'") < 1 And Session("No.of.Days") = 2 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID, OtherGuest, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,LunchType) values('" & EventName & "'," & AutoMemberID & ",'N','" & ddlGender.SelectedValue & "','" & ddlAge.SelectedValue & "','" & lblDate2.Text & "','" & Name & "','" & ddlRelationship.SelectedValue & "','" & UnderFive & "','" & EmailID & "','" & HomePhone & "','" & CellPhone & "','" & ChaperoneFlag & "'," & IIf(intAmount = 0, 0, Convert.ToDecimal(LblMealAmt2.Text)) & "," & Year & "," & Session("LoginID") & ",GetDate(),'Lunch','" & ddllunchType.SelectedItem.Text & "')")
                    lblAddFamilyErr.Text = "Inserted Successfully"
                End If
                txtFname.Text = ""
                txtLname.Text = ""
                LoadGridFamily()
                selections()
            Catch ex As SqlException
                lblAddFamilyErr.Text = lblFamilyErr.Text & " " & ex.Message
            End Try
        End If
    End Sub

    Protected Sub btnFamily_Close(ByVal sender As Object, ByVal e As System.EventArgs)
        AddFamilyMembers.Visible = False
    End Sub
    Protected Sub Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If checkselection() = True Then
            lblContestantsErr.Text = ""
            UpdateChildMeal()
            InsertFamilyMember()
            selections()
            calculateMealCharge()
            Dim nRegFee As Integer = CType(Session("RegFee"), Integer)
            Dim mealsFee As Integer
            Try
                mealsFee = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Sum(amount) from MealCharge where Automemberid=" & Session("CustIndID") & " and ContestYear = " & Year & "  and PaymentReference is NULL")
            Catch ex As Exception
                mealsFee = 0.0
            End Try
            If Session("AdultFlag") = 1 Then
                If nRegFee > 0 Or mealsFee > 0 Then
                    btnPayNow.Visible = True
                Else
                    btnPayNow.Visible = False
                    lblContestantsErr.ForeColor = Drawing.Color.Red
                    lblContestantsErr.Text = "You cannot proceed to pay, since no selections were made."
                End If
            Else
                btnPayNow.Visible = False
                lblError.ForeColor = Drawing.Color.Red
                lblError.Text = "Contestant must be accompanied by an Adult (Age Above 18)"
            End If
        Else
            lblError.ForeColor = Drawing.Color.Red
            lblError.Text = "Please make your selection (Yes or No) in Optional Meals"
        End If
    End Sub
    Protected Sub btnPayNow_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        calculateMealCharge()
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN datediff(d,Getdate(),MIN(contestdate))>9 then 1 else 0 END as Status from contest where contest_year=YEAR(Getdate()) and eventid=1") = 0 Then
            lblError.Text = "It is too late to register for meals."
            Exit Sub
        End If
        If checkselection() = True Then
            If Session("AdultFlag") = 1 Then
                Session("MealCharges") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select case when Sum(amount) is null then 0.00 else Sum(amount) end from MealCharge where Automemberid=" & Session("CustIndID") & " and ContestYear = " & Year & "  and PaymentReference is NULL")
                'Session("contestSelSumNo") = 0
                'Session.Remove("No.of.Days")
                'Session.Remove("AdultFlag")
                Page.Response.Redirect("~/ContestSelectionSum.aspx")
                'Page.Response.Redirect("~/TermsAndConditions.aspx")            
            Else
                lblError.ForeColor = Drawing.Color.Red
                lblError.Text = "Contestant must be accompanied by an Adult (Age Above 18)"
            End If
        Else
            lblError.ForeColor = Drawing.Color.Red
            lblError.Text = "Please make your selection (Yes or No) in Optional Meals"
        End If
    End Sub
    Protected Sub selections()
        Dim Day0D, Day1B, Day1D, Day2B, Day2D As String
        If rbtnDay0Dy.Checked = True Then
            Day0D = "Y"
            InsertOptMeal(ltlDay0.Text, "Dinner", lblDay0Dinner.Text)
        ElseIf rbtnDay0Dn.Checked = True Then
            Day0D = "N"
            DeleteOptMeal(ltlDay0.Text, "Dinner")
        End If

        If rbtnDay1By.Checked = True Then
            Day1B = "Y"
            InsertOptMeal(ltlDay1.Text, "Breakfast", lblDay1Breakfast.Text)
        ElseIf rbtnDay1Bn.Checked = True Then
            Day1B = "N"
            DeleteOptMeal(ltlDay1.Text, "Breakfast")
        End If
        If rbtnDay1DY.Checked = True Then
            Day1D = "Y"
            InsertOptMeal(ltlDay1.Text, "Dinner", lblDay1Dinner.Text)
        ElseIf rbtnDay1Dn.Checked = True Then
            Day1D = "N"
            DeleteOptMeal(ltlDay1.Text, "Dinner")
        End If

        If rbtnDay2By.Checked = True Then
            Day2B = "Y"
            InsertOptMeal(ltlDay2.Text, "Breakfast", lblDay2Breakfast.Text)
        ElseIf rbtnDay2Bn.Checked = True Then
            Day2B = "N"
            DeleteOptMeal(ltlDay2.Text, "Breakfast")
        End If
        If rbtnDay2Dy.Checked = True Then
            Day2D = "Y"
            InsertOptMeal(ltlDay2.Text, "Dinner", lblDay2Dinner.Text)
        ElseIf rbtnDay2Dn.Checked = True Then
            Day2D = "N"
            DeleteOptMeal(ltlDay2.Text, "Dinner")
        End If
        Dim sql As String = ""
        Try
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(MealSelID)  FROM MealSelection WHERE MemberId = " & Session("CustIndID") & " AND YEAR(CreatedDate) = YEAR(GETDATE())") < 1 Then
                sql = "Insert into  MealSelection(MemberId, Day0Dinner, Day1Breakfast, Day1Dinner, Day2Breakfast, Day2Dinner, CreatedDate,Createdby) values (" & Session("CustIndID") & ",'" & Day0D.Trim & "','" & Day1B.Trim & "','" & Day1D.Trim & "','" & Day2B.Trim & "','" & Day2D.Trim & "',GetDate()," & Session("LoginID") & ")"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sql)
            Else
                sql = "Update MealSelection Set Day0Dinner='" & Day0D.Trim & "',Day1Breakfast='" & Day1B.Trim & "', Day1Dinner='" & Day1D.Trim & "', Day2Breakfast='" & Day2B.Trim & "', Day2Dinner='" & Day2D.Trim & "', Modifieddate=getdate(),Modifiedby=" & Session("LoginID") & " where MemberId = " & Session("CustIndID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sql)
            End If
        Catch ex As Exception
            lblContestantsErr.Text = sql.ToString() & "<br>"
            lblContestantsErr.Text = lblContestantsErr.Text & ex.ToString()
        End Try
    End Sub

    Protected Sub InsertOptMeal(ByVal contestdate As String, ByVal mealtype As String, ByVal mealamt As String)
        Try
            Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select automemberid,name,contestyear from mealcharge where mealtype='Lunch' and contestdate='" & lblDate1.Text & "' and automemberid=" & Session("CustIndID") & " and name not in (select name from mealcharge where mealtype='" & mealtype & "' and contestdate='" & contestdate & "' and automemberid=" & Session("CustIndID") & ")")
            '" select * from mealcharge where Contestdate not in ('" & lblDate1.Text & "') and automemberid=" & Session("CustIndID") & " and ContestYear=" & Year & " and PaymentReference is Null")
            While Reader.Read()
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,MealMemberID,LunchType) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & contestdate & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, " & Convert.ToDecimal(mealamt) & ", ContestYear,ModifiedUserID,GetDate(),'" & mealtype & "',MealMemberID,LunchType from MealCharge  WHERE mealtype='Lunch' and amount > 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name = '" & Reader("Name") & "' AND ContestDate = '" & lblDate1.Text & "'")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert Into MealCharge(EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, ContestDate, Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,Modifieddate,MealType,MealMemberID,LunchType) Select EventName, AutoMemberID,ChildNumber, OtherGuest,OtherGuestID, Gender, Age, '" & contestdate & "', Name, RelationType, ChildFlag, EmailID, HomePhone, CellPhone, ChaperoneFlag, Amount, ContestYear,ModifiedUserID,GetDate(),'" & mealtype & "',MealMemberID,LunchType from MealCharge  WHERE mealtype='Lunch' and amount = 0 and ContestYear = " & Year & " AND AutoMemberID = " & Session("CustIndID") & " AND Name = '" & Reader("Name") & "' AND ContestDate = '" & lblDate1.Text & "'")
            End While
        Catch ex As Exception
            lblContestantsErr.Text = lblContestantsErr.Text & ex.ToString()
        End Try

    End Sub
    Protected Sub DeleteOptMeal(ByVal contestdate As String, ByVal mealtype As String)
        Try
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from mealcharge where  mealtype='" & mealtype & "'and PaymentReference is NULL and contestdate='" & contestdate & "' and automemberid=" & Session("CustIndID") & "")
        Catch ex As Exception
            lblContestantsErr.Text = lblContestantsErr.Text & ex.ToString()
        End Try

    End Sub
    Protected Sub UpdateChildMeal()
        Dim intCtr, childnumber As Integer
        Dim myTableCell As TableCell
        Dim ddllunchType As DropDownList
        If dgContestants.Items.Count <= 0 Then
            Exit Sub
        Else
            For intCtr = 0 To dgContestants.Items.Count - 1
                myTableCell = dgContestants.Items.SyncRoot.Item(intCtr).Cells(0)
                childnumber = myTableCell.Text
                myTableCell = dgContestants.Items.SyncRoot.Item(intCtr).Cells(7)
                ddllunchType = CType(myTableCell.FindControl("ddlLunchType"), DropDownList)
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update MealCharge Set LunchType='" & ddllunchType.SelectedItem.Text & "' Where AutoMemberID=" & Session("CustIndID") & " And ChildNumber =" & childnumber & "") 'and ContestDate='" & lblDate1.Text & "'")
            Next
        End If
    End Sub
    Protected Sub dgContestants_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContestants.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim ddl As DropDownList = CType(e.Item.FindControl("ddlLunchType"), DropDownList)
                If Not DataBinder.Eval(e.Item.DataItem, "LunchType") = "" Then
                    ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByText(DataBinder.Eval(e.Item.DataItem, "LunchType").ToString()))
                End If
        End Select

    End Sub

    Protected Sub getselection()
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT *  FROM MealSelection WHERE MemberId = " & Session("CustIndID") & " AND YEAR(CreatedDate) = YEAR(GETDATE())")
        While reader.Read()
            If Trim(reader("Day0Dinner")) = "Y" Then
                rbtnDay0Dy.Checked = True
                trDay0.Visible = True
                rbtnDay0Dn.Checked = False
            Else
                rbtnDay0Dy.Checked = False
                rbtnDay0Dn.Checked = True
            End If

            If Trim(reader("Day1Breakfast")) = "Y" Then
                rbtnDay1By.Visible = True
                rbtnDay1Bn.Visible = True
                lblD1B.Visible = True

                rbtnDay1By.Checked = True
                rbtnDay1Bn.Checked = False
            Else
                rbtnDay1By.Checked = False
                rbtnDay1Bn.Checked = True
            End If
            If Trim(reader("Day1Dinner")) = "Y" Then
                rbtnDay1DY.Checked = True
                rbtnDay1Dn.Checked = False
            Else
                rbtnDay1DY.Checked = False
                rbtnDay1Dn.Checked = True
            End If
            If Trim(reader("Day2Breakfast")) = "Y" Then
                trDay2.Visible = True
                rbtnDay2By.Checked = True
                rbtnDay2Bn.Checked = False
            Else
                rbtnDay2By.Checked = False
                rbtnDay2Bn.Checked = True
            End If

            If Trim(reader("Day2Dinner")) = "Y" Then
                trDay2.Visible = True
                rbtnDay2Dy.Checked = True
                rbtnDay2Dn.Checked = False
            Else
                rbtnDay2Dy.Checked = False
                rbtnDay2Dn.Checked = True
            End If
        End While
        reader.Close()
    End Sub
    Private Sub disablepaid()
        If checkselection() = True Then
            Dim str As String = "Select Sum(t1.Day0) as Day0,Sum(t1.Day1B) as Day1B,Sum(t1.Day1D) as Day1D,Sum(t1.Day2B) as Day2B,Sum(t1.Day2D) as Day2D from ("
            str = str & "(select count(*) as Day0,0 as Day1B,0 as Day1D,0 as Day2B,0 as Day2D from MealCharge where MealType='Dinner'    and Contestdate='" & ltlDay0.Text & "' and Automemberid=" & Session("CustIndID") & " and PaymentReference is NOT NULL AND ContestYear = YEAR(GETDATE())) Union All"
            str = str & "(select 0 as Day0,count(*) as Day1B,0 as Day1D,0 as Day2B,0 as Day2D from MealCharge where MealType='Breakfast' and Contestdate='" & ltlDay1.Text & "' and Automemberid=" & Session("CustIndID") & " and PaymentReference is NOT NULL AND ContestYear = YEAR(GETDATE())) Union All"
            str = str & "(select 0 as Day0,0 as Day1B,count(*) as Day1D,0 as Day2B,0 as Day2D from MealCharge where MealType='Dinner'    and Contestdate='" & ltlDay1.Text & "' and Automemberid=" & Session("CustIndID") & " and PaymentReference is NOT NULL AND ContestYear = YEAR(GETDATE())) Union All"
            str = str & "(select 0 as Day0,0 as Day1B,0 as Day1D,count(*) as Day2B,0 as Day2D from MealCharge where MealType='Breakfast' and Contestdate='" & ltlDay2.Text & "' and Automemberid=" & Session("CustIndID") & " and PaymentReference is NOT NULL AND ContestYear = YEAR(GETDATE())) Union All"
            str = str & " select 0 as Day0,0 as Day1B,0 as Day1D,0 as Day2B,count(*) as Day2D from MealCharge where MealType='Dinner'    and Contestdate='" & ltlDay2.Text & "' and Automemberid=" & Session("CustIndID") & " and PaymentReference is NOT NULL AND ContestYear = YEAR(GETDATE())) t1"
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, str)
            While reader.Read()
                If reader("Day0") > 0 Then
                    rbtnDay0Dy.Enabled = False
                    rbtnDay0Dn.Enabled = False
                End If
                If reader("Day1B") > 0 Then
                    rbtnDay1By.Enabled = False
                    rbtnDay1Bn.Enabled = False
                End If
                If reader("Day1D") > 0 Then
                    rbtnDay1DY.Enabled = False
                    rbtnDay1Dn.Enabled = False
                End If
                If reader("Day2B") > 0 Then
                    rbtnDay2By.Enabled = False
                    rbtnDay2By.Checked = True
                    rbtnDay2Bn.Enabled = False
                    trDay2.Visible = True
                End If
                If reader("Day2D") > 0 Then
                    rbtnDay2Dy.Enabled = False
                    rbtnDay2Dy.Checked = True
                    rbtnDay2Dn.Enabled = False
                    trDay2.Visible = True
                End If
            End While
            reader.Close()
        End If
    End Sub
    Function checkselection() As Boolean
        If ((rbtnDay0Dy.Checked = True Or rbtnDay0Dn.Checked = True) And (rbtnDay1By.Checked = True Or rbtnDay1Bn.Checked = True) And (rbtnDay1DY.Checked = True Or rbtnDay1Dn.Checked = True) And (rbtnDay2By.Checked = True Or rbtnDay2Bn.Checked = True) And (rbtnDay2Dy.Checked = True Or rbtnDay2Dn.Checked = True)) Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
