Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class FundRaisingCal
    Inherits System.Web.UI.Page

    Dim cnTemp As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'lblError.Text = ""
        'lblGridErr.Text = ""
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not Page.IsPostBack Then
            drpYear.Items.Add(Year(DateTime.Now).ToString())
            drpYear.Items.Add(Integer.Parse(Year(DateTime.Now).ToString()) + 1)
            lblHeading2.Text = Session("selChapterName")
            LoadDropDowns()
            loadgrid()
        End If
    End Sub
    Private Sub loadgrid()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Convert(varchar,eventdate,101) as eventdate,FundRCalID from  FundRaisingCal where ChapterID=" & Session("selChapterID") & "")
        If ds.Tables(0).Rows.Count > 0 Then
            trEvent.Visible = True
            ddlEventDates.DataSource = ds.Tables(0)
            ddlEventDates.DataBind()
            ddlEventDates.Items.Insert(0, New ListItem("New", "0"))
            ddlEventDates.Items.Insert(0, New ListItem("Select one", "-1"))
        Else
            PnlMain.Visible = True
        End If
    End Sub
    Private Sub LoadDropDowns()
        ddlVenue.Items.Clear()
        Dim dsVenue As DataSet
        dsVenue = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_Venue_GetByChapterID", New SqlParameter("@ChapterID", Session("selChapterID")))
        If dsVenue.Tables(0).Rows.Count > 0 Then
            ddlVenue.DataSource = dsVenue
            ddlVenue.DataBind()
        Else
            ' ddlVenue.Items.Insert(0, "")
        End If
        LoadChapter(ddlChapter)
    End Sub
    Private Sub LoadChapter(ByVal ddlst As DropDownList)
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, "select chapterID,chaptercode from chapter where Status='A' order by State,Chaptercode")
        While drStates.Read()
            ddlst.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlst.SelectedIndex = ddlst.Items.IndexOf(ddlst.Items.FindByValue(Session("selChapterID")))
        ddlst.Enabled = False
    End Sub
    Private Sub loadSelctiongrid(ByVal FundRCalID As Integer)
        Dim strSQL As String
        If FundRCalID = 0 Then
            strSQL = "select ProductId, ProductCode,ProductGroupCode,ProductGroupId,Name,0.00 as taxamt from Product where eventid=9 and Status='O' order by ProductGroupID"
        Else
            loadSelectedgrid(FundRCalID)
            strSQL = "select ProductId, ProductCode,ProductGroupCode,ProductGroupId,Name,0.00 as taxamt from Product where eventid=9 and Status='O' and ProductID not IN (Select ProductID from  FundRFees where FundRCalID = " & HdnFundRCalID.Value & " ) order by ProductGroupID"
        End If
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Catch ex As Exception
            Response.Write(strSQL)
            Exit Sub
        End Try
        dgSelect.DataSource = ds
        dgSelect.DataBind()
        pnlSelect.Visible = True
    End Sub
    Private Sub loadSelectedgrid(ByVal FundRCalID As Integer)
        Dim strSQL As String
        strSQL = " SELECT     FundRFeesID,FundRCalID,  EventCode,ChapterCode,  ProductGroupCode,ProductCode, EventYear, Convert(varchar,eventdate,101) as EventDate, FeeFrom, FeeTo, PaymentMode, TaxDeduction, Exclusive, ByContest, Share,ProductGroupID,ProductID FROM  FundRFees where FundRCalID = " & HdnFundRCalID.Value & " order by ProductGroupID,FeeFrom Desc "
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Catch ex As Exception
            Response.Write(strSQL)
            Exit Sub
        End Try
        DGSelected.DataSource = ds
        DGSelected.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            pnlSelected.Visible = True
            pnlCopy.Visible = False
        Else
            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Convert(varchar,eventdate,101) as eventdate,FundRCalID from  FundRaisingCal where ChapterID=" & Session("selChapterID") & "")
            If ds1.Tables(0).Rows.Count > 0 Then
                ddlCopyEventDT.DataSource = ds1.Tables(0)
                ddlCopyEventDT.DataBind()
                pnlCopy.Visible = True
            End If
            pnlSelected.Visible = False
        End If
    End Sub
    Protected Sub DGSelected_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGSelected.CancelCommand
        DGSelected.EditItemIndex = -1
        loadSelectedgrid(HdnFundRCalID.Value)
    End Sub

    Protected Sub DGSelected_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGSelected.EditCommand
        DGSelected.EditItemIndex = CInt(e.Item.ItemIndex)
        HdnFundRFeesID.Value = CInt(e.Item.Cells(1).Text)
        loadSelectedgrid(HdnFundRCalID.Value)
        Session("PaymntMthd") = CType(e.Item.Cells(6).FindControl("lblPaymentMode"), Label).Text.Trim
        Session("Exclusive") = CType(e.Item.Cells(7).FindControl("lblExclusive"), Label).Text.Trim
        Session("ByContest") = CType(e.Item.Cells(8).FindControl("lblByContest"), Label).Text.Trim
        Session("Share") = CType(e.Item.Cells(6).FindControl("lblByShare"), Label).Text.Trim
    End Sub

    Public Sub SetDropDown_PaymntMthd(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("PaymntMthd")))
    End Sub

    Public Sub SetDropDown_Exclusive(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("Exclusive")))
    End Sub

    Public Sub SetDropDown_ByContest(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("ByContest")))
    End Sub

    Public Sub SetDropDown_Share(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("Share")))
    End Sub

    Protected Sub DGSelected_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGSelected.UpdateCommand
        lblError.Text = ""
        Dim row As Integer = CInt(e.Item.ItemIndex)
        Dim txtEditText As TextBox, ddlTemp As DropDownList
        '        Dim intSelStartTime As Integer, intSelEndTime As Integer, intSelCheckInTime As Integer

        Dim txtAmtFrom As TextBox, txtAmtTo As TextBox, ddlPaymntMthd As DropDownList, ddlExclusive As DropDownList, ddlByContest As DropDownList, ddlShare As DropDownList, txtTax As TextBox

        txtAmtFrom = e.Item.FindControl("txtAmtFrom")
        txtAmtTo = e.Item.FindControl("txtAmtTo")
        ddlPaymntMthd = e.Item.FindControl("ddlPaymntMthd")
        ddlExclusive = e.Item.FindControl("ddlExclusive")
        ddlByContest = e.Item.FindControl("ddlByContest")
        ddlShare = e.Item.FindControl("ddlShare")
        txtTax = e.Item.FindControl("txtTax")

        If IsNumeric(txtAmtFrom.Text) = False Then
            lblError.Text = "Enter Amount From "
            Exit Sub
        ElseIf Decimal.Parse(txtAmtFrom.Text) < 0 Then
            lblError.Text = "Amount From value should not be Negative"
            Exit Sub
        ElseIf IsNumeric(txtAmtTo.Text) = False And Not txtAmtTo.Text.Length = 0 Then
            lblError.Text = "Enter valid Amount to"
            Exit Sub
        ElseIf Not txtAmtTo.Text.Length = 0 And Decimal.Parse(IIf(txtAmtTo.Text.Length = 0, -1, txtAmtTo.Text)) < 0 Then
            lblError.Text = "Amount To value should not be Negative"
            Exit Sub
        ElseIf Decimal.Parse(txtAmtFrom.Text) > Decimal.Parse(IIf(txtAmtTo.Text.Length = 0, -1, txtAmtTo.Text)) And Not txtAmtTo.Text.Length = 0 Then
            lblError.Text = "Amount to value must be higher than Amount From value"
            Exit Sub
        ElseIf IsNumeric(txtTax.Text) = False Then
            lblError.Text = "Enter Tax Deduction Amount between 0 and 100"
            Exit Sub
        ElseIf Decimal.Parse(txtTax.Text) < 0 Or Decimal.Parse(txtTax.Text) > 100 Then
            lblError.Text = "Enter Tax Deduction Amount between 0 and 100"
            Exit Sub
        End If
        Try
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update FundRFees set FeeFrom =" & txtAmtFrom.Text & ",FeeTo=" & IIf(txtAmtTo.Text.Length = 0, "Null", txtAmtTo.Text) & " ,PaymentMode='" & ddlPaymntMthd.SelectedItem.Text & "'	,TaxDeduction=" & txtTax.Text & "	,Exclusive='" & ddlExclusive.SelectedItem.Text & "'	,ByContest='" & ddlByContest.SelectedItem.Text & "'	,Share='" & ddlShare.SelectedItem.Text & "',ModifyDate=GetDate(),ModifiedBy=" & Session("LoginID") & "	 where FundRFeesID=" & HdnFundRFeesID.Value & "")
            DGSelected.EditItemIndex = -1
            loadSelectedgrid(HdnFundRCalID.Value)
        Catch ex As Exception
            lblError.Text = "Data Error"
        End Try
    End Sub

    Protected Sub dgSelect_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim Chk1 As CheckBox = CType(e.Item.FindControl("chkSelect"), CheckBox)
                Dim txtAmtFrom As TextBox, txtAmtTo As TextBox, ddlPaymntMthd As DropDownList, ddlExclusive As DropDownList, ddlByContest As DropDownList, ddlShare As DropDownList, txtTax As TextBox
                txtAmtFrom = CType(e.Item.FindControl("txtAmtFrom"), TextBox)
                txtAmtTo = CType(e.Item.FindControl("txtAmtTo"), TextBox)
                ddlPaymntMthd = CType(e.Item.FindControl("ddlPaymntMthd"), DropDownList)
                ddlExclusive = CType(e.Item.FindControl("ddlExclusive"), DropDownList)
                ddlByContest = CType(e.Item.FindControl("ddlByContest"), DropDownList)
                ddlShare = CType(e.Item.FindControl("ddlShare"), DropDownList)
                txtTax = CType(e.Item.FindControl("txtTax"), TextBox)
                txtAmtFrom.Enabled = False
                txtAmtTo.Enabled = False
                ddlPaymntMthd.Enabled = False
                ddlExclusive.Enabled = False
                ddlByContest.Enabled = False
                ddlShare.Enabled = False
                txtTax.Enabled = False
        End Select
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim dgitem As DataGridItem = CType(chk.Parent.Parent, DataGridItem)
        Dim txtAmtFrom As TextBox, txtAmtTo As TextBox, ddlPaymntMthd As DropDownList, ddlExclusive As DropDownList, ddlByContest As DropDownList, ddlShare As DropDownList, txtTax As TextBox
        txtAmtFrom = CType(dgitem.FindControl("txtAmtFrom"), TextBox)
        txtAmtTo = CType(dgitem.FindControl("txtAmtTo"), TextBox)
        ddlPaymntMthd = CType(dgitem.FindControl("ddlPaymntMthd"), DropDownList)
        ddlExclusive = CType(dgitem.FindControl("ddlExclusive"), DropDownList)
        ddlByContest = CType(dgitem.FindControl("ddlByContest"), DropDownList)
        ddlShare = CType(dgitem.FindControl("ddlShare"), DropDownList)
        txtTax = CType(dgitem.FindControl("txtTax"), TextBox)

        If chk.Checked = True Then
            txtAmtFrom.Enabled = True
            txtAmtTo.Enabled = True
            ddlPaymntMthd.Enabled = True
            ddlExclusive.Enabled = True
            ddlByContest.Enabled = True
            ddlShare.Enabled = True
            txtTax.Enabled = True
        Else
            txtAmtFrom.Text = ""
            txtAmtTo.Text = ""
            ddlPaymntMthd.SelectedIndex = ddlPaymntMthd.Items.IndexOf(ddlPaymntMthd.Items.FindByText("CreditCard only"))
            ddlExclusive.SelectedIndex = ddlExclusive.Items.IndexOf(ddlExclusive.Items.FindByText("No"))
            ddlByContest.SelectedIndex = ddlByContest.Items.IndexOf(ddlByContest.Items.FindByText("No"))
            ddlShare.SelectedIndex = ddlShare.Items.IndexOf(ddlShare.Items.FindByText("No"))
            txtTax.Text = "0.00"
            txtAmtFrom.Enabled = False
            txtAmtTo.Enabled = False
            ddlPaymntMthd.Enabled = False
            ddlExclusive.Enabled = False
            ddlByContest.Enabled = False
            ddlShare.Enabled = False
            txtTax.Enabled = False
        End If
    End Sub

    Function checkquantity() As Boolean
        Dim flag As Boolean = False
        Dim item As DataGridItem
        Dim chk As CheckBox, txtAmtFrom As TextBox, txtAmtTo As TextBox, txtTax As TextBox
        For Each item In dgSelect.Items
            chk = CType(item.FindControl("chkSelect"), CheckBox)
            txtAmtFrom = CType(item.FindControl("txtAmtFrom"), TextBox)
            txtAmtTo = CType(item.FindControl("txtAmtTo"), TextBox)
            txtTax = CType(item.FindControl("txtTax"), TextBox)
            If chk.Checked = True Then
                If IsNumeric(txtAmtFrom.Text) = False Then
                    Return True
                    Exit Function
                ElseIf Decimal.Parse(txtAmtFrom.Text) < 0 Then
                    Return True
                    Exit Function
                ElseIf IsNumeric(txtAmtTo.Text) = False And Not txtAmtTo.Text.Length = 0 Then
                    Return True
                    Exit Function
                ElseIf Not txtAmtTo.Text.Length = 0 And Decimal.Parse(IIf(txtAmtTo.Text.Length = 0, -1, txtAmtTo.Text)) < 0 Then
                    Return True
                    Exit Function
                ElseIf Decimal.Parse(txtAmtFrom.Text) > Decimal.Parse(IIf(txtAmtTo.Text.Length = 0, -1, txtAmtTo.Text)) And Not txtAmtTo.Text.Length = 0 Then
                    Return True
                    Exit Function
                ElseIf IsNumeric(txtTax.Text) = False Then
                    Return True
                    Exit Function
                ElseIf Decimal.Parse(txtTax.Text) < 0 Or Decimal.Parse(txtTax.Text) > 100 Then
                    Return True
                    Exit Function
                End If
            End If
        Next
        Return flag
    End Function

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblGridErr.Text = ""
        Dim flag As Boolean = False
        Dim totamount As Decimal
        If checkquantity() Then
            lblGridErr.Text = "Amount cannot be negative.  Tax Deduction must be between 0 and 100."
            Exit Sub
        Else
            lblGridErr.Text = ""
            totamount = 0.0
            Dim item As DataGridItem

            Dim chk As CheckBox, StrSQL As String = ""
            Dim txtAmtFrom As TextBox, txtAmtTo As TextBox, ddlPaymntMthd As DropDownList, ddlExclusive As DropDownList, ddlByContest As DropDownList, ddlShare As DropDownList, txtTax As TextBox, lblprdcode As Label, lblPrdID As Label, lblPrdGrpCode As Label, lblPrdGrpID As Label

            For Each item In dgSelect.Items
                chk = CType(item.FindControl("chkSelect"), CheckBox)
                If chk.Checked = True Then
                    flag = True
                    txtAmtFrom = CType(item.FindControl("txtAmtFrom"), TextBox)
                    txtAmtTo = CType(item.FindControl("txtAmtTo"), TextBox)
                    ddlPaymntMthd = CType(item.FindControl("ddlPaymntMthd"), DropDownList)
                    ddlExclusive = CType(item.FindControl("ddlExclusive"), DropDownList)
                    ddlByContest = CType(item.FindControl("ddlByContest"), DropDownList)
                    ddlShare = CType(item.FindControl("ddlShare"), DropDownList)
                    txtTax = CType(item.FindControl("txtTax"), TextBox)
                    lblPrdGrpCode = CType(item.FindControl("lblProductGroupCode"), Label)
                    lblPrdGrpID = CType(item.FindControl("lblProductGroupID"), Label)
                    lblprdcode = CType(item.FindControl("lblProductCode"), Label)
                    lblPrdID = CType(item.FindControl("lblProductID"), Label)
                    StrSQL = " If Not Exists(Select * from FundRFees where FundRCalID = " & HdnFundRCalID.Value & " and ProductID=" & lblPrdID.Text & ") Begin"
                    StrSQL = StrSQL & "  Insert into  FundRFees(FundRCalID, EventId, EventCode, ChapterId, ChapterCode, ProductGroupId, ProductGroupCode, ProductId, ProductCode, EventYear, "
                    StrSQL = StrSQL & " EventDate, FeeFrom, FeeTo, PaymentMode, TaxDeduction, Exclusive, ByContest, Share, CreateDate, CreatedBy)"
                    StrSQL = StrSQL & " values (" & HdnFundRCalID.Value & ",9,'FundRD'," & ddlChapter.SelectedValue + ",'" & ddlChapter.SelectedItem.Text & "'," & lblPrdGrpID.Text & ",'" & lblPrdGrpCode.Text & "'," & lblPrdID.Text & ",'"
                    StrSQL = StrSQL & lblprdcode.Text & "'," & drpYear.SelectedValue & ",'" & txtEventDate.Text & "','" & txtAmtFrom.Text & "'," & IIf(txtAmtTo.Text.Length = 0, "Null", "'" & txtAmtTo.Text & "'") & ",'" & ddlPaymntMthd.SelectedItem.Text & "','"
                    StrSQL = StrSQL & txtTax.Text & "','" & ddlExclusive.SelectedValue & "','" & ddlByContest.SelectedValue & "','" & ddlShare.SelectedValue & "',GetDate()," & Session("LOginID") & ") END"
                    Try
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL)
                    Catch ex As Exception
                        Response.Write("<BR>" & StrSQL & "<br>" & e.ToString())
                        Exit Sub
                    End Try
                End If
            Next
        End If
        If flag = False Then
            lblGridErr.Text = "Sorry No item seleted"
        Else
            loadSelctiongrid(HdnFundRCalID.Value)
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim StrSQl As String
        If Not IsDate(txtStartDate.Text) Then
            lblError.Text = "Please Enter Start Date in MM/DD/YYYY format"
            Exit Sub
        ElseIf Not IsDate(txtEndDate.Text) Then
            lblError.Text = "Please Enter End Date in MM/DD/YYYY format"
            Exit Sub
        ElseIf Not IsDate(txtEventDate.Text) Then
            lblError.Text = "Please Enter Event Date in MM/DD/YYYY format"
            Exit Sub
        ElseIf ddlVenue.SelectedValue = "" Then
            lblError.Text = "Please Select venue"
            Exit Sub
        ElseIf Convert.ToDateTime(txtStartDate.Text) < Now.Date And btnSubmit.Text = "Submit" Then
            lblError.Text = " Start date must not be less than Current Date"
            Exit Sub
        ElseIf Convert.ToDateTime(txtStartDate.Text) > Convert.ToDateTime(txtEventDate.Text) Then
            lblError.Text = " Start date  must not be after Event Date"
            Exit Sub
        ElseIf Convert.ToDateTime(txtStartDate.Text) > Convert.ToDateTime(txtEndDate.Text) Then
            lblError.Text = " Start date  must not be after End Date"
            Exit Sub
        Else
            If btnSubmit.Text = "Submit" Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(FundRCalID)  From FundRaisingCal where DateDiff(d,CampEndDate,'" & txtStartDate.Text & "')<0 and ChapterID=" & ddlChapter.SelectedValue & "") > 0 Then
                    lblError.Text = "Event Already Exist"
                Else
                    StrSQl = " Insert into FundRaisingCal(ChapterID, ChapterCode, EventID, EventCode, EventYear, VenueID, EventDate, StartTime, EndTime, Building, CampStartDate, CampEndDate, EventDescription, CreateDate, CreatedBy) Values ("
                    StrSQl = StrSQl & ddlChapter.SelectedValue + ",'" & ddlChapter.SelectedItem.Text & "',9,'FundRD'," & drpYear.SelectedValue & "," & ddlVenue.SelectedValue & ",'" & txtEventDate.Text & "','" & ddlStartTime.SelectedValue & "','" & ddlEndTime.SelectedValue & "',Null,'"
                    StrSQl = StrSQl & txtStartDate.Text & "','" & txtEndDate.Text & "','" & txtDescription.Text & "',GetDate()," & Session("LoginID") & "); Select Scope_Identity()"
                    lblError.Text = "Inserted Successfully"
                    loadSelctiongrid(0)
                    pnlSelect.Visible = True
                End If
                Try
                    HdnFundRCalID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQl)
                    loadSelectedgrid(HdnFundRCalID.Value)
                Catch ex As Exception
                    Response.Write(StrSQl)
                End Try
            Else
                'for updation
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(FundRCalID)  From FundRaisingCal where DateDiff(d,CampEndDate,'" & txtStartDate.Text & "')<0 and ChapterID=" & ddlChapter.SelectedValue & " and FundRCalID not in ( " & HdnFundRCalID.Value & ")") > 0 Then
                    lblError.Text = "Event Already Starts before this event Ends"
                Else
                    StrSQl = " Update FundRaisingCal set  VenueID=" & ddlVenue.SelectedValue & ", EventDate='" & txtEventDate.Text & "', StartTime='" & ddlStartTime.SelectedValue & "', EndTime='" & ddlEndTime.SelectedValue & "', Building=Null, CampStartDate='" & txtStartDate.Text & "', CampEndDate='" & txtEndDate.Text & "', EventDescription='" & txtDescription.Text & "',  ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & " where  FundRCalID = " & HdnFundRCalID.Value
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
                    lblError.Text = "Updated Successfully"
                    loadSelctiongrid(HdnFundRCalID.Value)
                End If
            End If
        End If
    End Sub

    Protected Sub ddlEventDates_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlEventDates.SelectedValue = -1 Then
            If ddlEventDates.SelectedValue = 0 Then
                PnlMain.Visible = True
                pnlSelect.Visible = False
                pnlSelected.Visible = False
                btnSubmit.Text = "Submit"
                HdnFundRCalID.Value = String.Empty
                ddlEventDates.Enabled = False
            Else
                PnlMain.Visible = True
                HdnFundRCalID.Value = ddlEventDates.SelectedValue
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ChapterID, ChapterCode, EventID, EventCode, EventYear, VenueID,Convert(varchar,eventdate,101) as eventdate , StartTime, EndTime, Building, Convert(varchar, CampStartDate,101) as  CampStartDate, Convert(varchar,CampEndDate,101) as CampEndDate, EventDescription from  FundRaisingCal where FundRCalID=" & ddlEventDates.SelectedValue & "")
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlVenue.SelectedIndex = ddlVenue.Items.IndexOf(ddlVenue.Items.FindByValue(ds.Tables(0).Rows(0)("VenueID")))
                    ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime").ToString.Trim))
                    ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime").ToString.Trim))
                    txtDescription.Text = ds.Tables(0).Rows(0)("EventDescription")
                    txtEventDate.Text = ds.Tables(0).Rows(0)("EventDate")
                    txtStartDate.Text = ds.Tables(0).Rows(0)("CampStartDate")
                    txtEndDate.Text = ds.Tables(0).Rows(0)("CampEndDate")
                    btnSubmit.Text = "Update"
                    loadSelctiongrid(HdnFundRCalID.Value)
                End If
            End If
        Else
            PnlMain.Visible = False
            pnlSelect.Visible = False
            pnlSelected.Visible = False
        End If
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT (FundRFeesID) FROM FundRFees WHERE FundRCalID=" & ddlCopyEventDT.SelectedValue) < 1 Then
            lblError.Text = "No record to copy"
        Else
            Dim StrSQL As String = ""
            StrSQL = StrSQL & " Insert into  FundRFees(FundRCalID, EventId, EventCode, ChapterId, ChapterCode, ProductGroupId, ProductGroupCode, ProductId, ProductCode, EventYear, "
            StrSQL = StrSQL & " EventDate, FeeFrom, FeeTo, PaymentMode, TaxDeduction, Exclusive, ByContest, Share, CreateDate, CreatedBy)"
            StrSQL = StrSQL & " SELECT " & HdnFundRCalID.Value & ", EventId, EventCode, ChapterId, ChapterCode, ProductGroupId, ProductGroupCode, ProductId, ProductCode, " & drpYear.SelectedValue & ",'" & txtEventDate.Text & "', FeeFrom, FeeTo, PaymentMode, TaxDeduction, Exclusive, ByContest, Share, GetDate(), " & Session("LoginID") & " FROM FundRFees WHERE FundRCalID=" & ddlCopyEventDT.SelectedValue
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL)
                loadSelectedgrid(HdnFundRCalID.Value)
                loadSelctiongrid(HdnFundRCalID.Value)
            Catch ex As Exception
                Response.Write(ex.ToString())
            End Try
        End If
    End Sub
End Class