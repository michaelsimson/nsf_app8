<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="MealCharge.aspx.vb" Inherits="MealCharge1" title="Meal Charge" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server"><center>
<table border ="0" cellpadding ="0" cellspacing ="0" width="900px">
<tr><td align="center">
<asp:datagrid id="dgRegisteredCnst" runat="server" CssClass="GridStyle" CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Visible="true" >
		<FooterStyle CssClass="GridFooter"></FooterStyle>
		<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
		<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
		<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
		<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
        <Columns>
				<asp:TemplateColumn HeaderText="Registered Contestant" >
					<ItemTemplate>
						<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildName") %>' CssClass="SmallFont">
						</asp:Label>
					</ItemTemplate>
                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Day1" >
					<ItemTemplate>
						<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCodes1") %>' CssClass="SmallFont">
						</asp:Label>
					</ItemTemplate>
                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
				</asp:TemplateColumn>
				<asp:TemplateColumn HeaderText="Day2"  >
					<ItemTemplate>
						<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCodes2") %>' CssClass="SmallFont">
						</asp:Label>
					</ItemTemplate>
                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
				</asp:TemplateColumn>
		</Columns>
		</asp:datagrid>

</td></tr>
<tr>
<td align ="center" >
<div style="text-align:left; color :Red" class ="smallfont"> ** Amounts are per person</div>
<b>OPTIONAL MEALS</b>

<table border ="0" cellpadding ="3" cellspacing ="0" >
<tr><td>
<table border ="1" cellpadding ="3" cellspacing ="0" >
<tr><td align="center"></td><td colspan="2" align="center">Breakfast</td><td colspan="2" align="center">Dinner</td></tr>

<tr runat="server" id="trDay0" visible="false"><td align="center" class="Smallfont"><asp:Literal ID="ltlDay0" runat="server"></asp:Literal>
<br />For early bird registrants

</td>
    <td align="right" colspan="2"><table border ="0" cellpadding ="0" cellspacing ="0" ><tr></tr></table></td>
    <td align="right" colspan="2" style="vertical-align :middle"><table border ="0" cellpadding ="0" cellspacing ="0" ><tr><td><asp:Label ID="lblD0B" runat="server" Text=""></asp:Label></td><td align="center"><asp:RadioButton ID="rbtnDay0Dy" GroupName="Day0D" Text ="Yes" runat="server" /></td><td align="center"><asp:RadioButton ID="rbtnDay0Dn" GroupName="Day0D" Text ="No" runat="server" /></td></tr></table></td>
</tr><tr runat="server" id="trDay1" visible="false"><td align="center"><asp:Literal ID="ltlDay1" runat="server"></asp:Literal></td>
    <td align="right" colspan="2"><table border ="0" cellpadding ="0" cellspacing ="0" ><tr><td><asp:Label ID="lblD1B" runat="server" Text=""></asp:Label></td><td align="center"><asp:RadioButton ID="rbtnDay1By" GroupName="Day1B" Text ="Yes" runat="server" /></td><td align="center"><asp:RadioButton ID="rbtnDay1Bn" GroupName="Day1B" Text ="No" runat="server" /></td></tr></table></td>
    <td  colspan="2" align="right" ><table border ="0" cellpadding ="0" cellspacing ="0" ><tr><td><asp:Label ID="lblD1D" runat="server" Text=""></asp:Label></td><td align="center"><asp:RadioButton ID="rbtnDay1DY" GroupName="Day1D" Text ="Yes" runat="server" /></td><td align="center"><asp:RadioButton ID="rbtnDay1Dn" GroupName="Day1D" Text ="No" runat="server" /></td></tr></table></td>
</tr><tr runat="server" id="trDay2" visible="false"><td align="center"><asp:Literal ID="ltlDay2" runat="server"></asp:Literal></td>    
    <td align="right" colspan="2"><table border ="0" cellpadding ="0" cellspacing ="0" ><tr><td><asp:Label ID="lblD2B" runat="server" Text=""></asp:Label></td><td align="center"><asp:RadioButton ID="rbtnDay2By" GroupName="Day2B" Text ="Yes" runat="server" /></td><td align="center"><asp:RadioButton ID="rbtnDay2Bn" GroupName="Day2B" Text ="No" runat="server" /></td></tr></table></td>
    <td align="right" colspan="2"><table border ="0" cellpadding ="0" cellspacing ="0" ><tr><td><asp:Label ID="lblD2D" runat="server" Text=""></asp:Label></td><td align="center"><asp:RadioButton ID="rbtnDay2Dy" GroupName="Day2D" Text ="Yes" runat="server" /></td><td align="center"><asp:RadioButton ID="rbtnDay2Dn" GroupName="Day2D" Text ="No" runat="server" /></td></tr></table></td>
</tr> 
</table>
</td><td style="vertical-align:middle; text-align:center"><asp:HyperLink  Text="Details on Meals" runat="server" ID="hlnkView" NavigateUrl="javascript:var w=window.open('MealChargehelp.html','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=700,height=750;');"  ></asp:HyperLink>
</td></tr></table>
</td></tr>
<tr><td> </td></tr>
<tr><td align="center">

<table border ="0" cellpadding ="3" cellspacing ="0" width="800px" >
<tr><td colspan ="3"> </td></tr>
<tr><td class="smallfont" valign="bottom" align="left" > 
    <asp:Label ID="lbllunch" CssClass="Smallfont" runat="server"/>
</td><td> </td>
<td align="right" valign="bottom"  > <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label> <asp:Button id="btnPayNow" Visible="false" runat="server" text="Pay Now" Width="100px" Font-Bold="true" OnClick="btnPayNow_click" ForeColor="Red"  />
</td></tr>
<tr><td class="smallfont" valign="bottom" align="center"  colspan="3" > 
   <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red" 
        Font-Size="Medium"></asp:Label>
   </td></tr>
</tableREGISTRATION FOR 
</td></tr>
<tr><td align="center">
<asp:Panel ID="pnlContestants" GroupingText="Family Members" Font-Bold="true" runat="server" Width="100%" BorderStyle="Solid" >
  <asp:Label ID="lblContestantsErr" runat="server"></asp:Label>					  			   
     <ASP:DATAGRID id="dgContestants" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                          Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
                          BorderColor="#336666" ForeColor="White" Font-Bold="True">
                     <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                     <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
                     <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
		 <COLUMNS>
                <ASP:BOUNDCOLUMN DataField="ChildNumber" Visible="false"></ASP:BOUNDCOLUMN>
                  <ASP:TemplateColumn HeaderStyle-Width="5%" ItemStyle-Width="5%" >
                     <ItemTemplate>
                        <asp:CheckBox ID="chkContestantSelect" runat="server" Checked="True" Enabled="false" />
		             </ItemTemplate>
                  </ASP:TemplateColumn>
			    <ASP:TEMPLATECOLUMN HeaderText="Contestant" HeaderStyle-Width="35%" ItemStyle-Width="35%" >
				    <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
				    <ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ChildName")%>
				    </ITEMTEMPLATE>
			    </ASP:TEMPLATECOLUMN>
                <ASP:TemplateColumn HeaderText="Gender" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
                    <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                      <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Gender")%>
                      </ItemTemplate>
                 </ASP:TemplateColumn>
                <ASP:TemplateColumn HeaderText="Grade" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
                    <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                      <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Grade")%>
                      </ItemTemplate>
                 </ASP:TemplateColumn>
                <ASP:TemplateColumn HeaderText="Day1" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
                    <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                      <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "Day1")%>
                      </ItemTemplate>
                 </ASP:TemplateColumn>
                  <ASP:TemplateColumn HeaderText="Day2" HeaderStyle-Width="30%" ItemStyle-Width="30%" >
                    <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                      <ItemTemplate>
                            <%#DataBinder.Eval(Container.DataItem, "Day2")%>
                      </ItemTemplate>
                 </ASP:TemplateColumn>
                 <ASP:TemplateColumn HeaderText="Lunch Type" HeaderStyle-Width="30%" ItemStyle-Width="30%" >
                    <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                     <ItemTemplate>
                        <ASP:DropDownList ID="ddlLunchType" runat="server" Visible="true">
                            <ASP:ListItem Value="1" Text="Indian Food"></asp:ListItem> 
                            <ASP:ListItem Value="2" Text="Pizza"></asp:ListItem>
                        </ASP:DropDownList>
                    </ItemTemplate>
                 </ASP:TemplateColumn>
		 </COLUMNS>											
			<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
            <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>
</asp:Panel>
										
<asp:Panel ID="pnlFamily" GroupingText="Non Contestants" Font-Bold="true" 
        runat="server" Width="100%" BorderStyle="Solid" style="margin-top: 0px" >
  <asp:Label ID="lblFamilyErr" runat="server"></asp:Label>&nbsp;
	<ASP:DATAGRID id="dgFamily" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
		        	Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
		        <COLUMNS>
				        <ASP:BOUNDCOLUMN DataField="Automemberid" Visible="false"></ASP:BOUNDCOLUMN>
				        <ASP:TemplateColumn  HeaderStyle-Width="5%" ItemStyle-Width="5%">
				            <ItemTemplate>
				               <asp:CheckBox ID="chkFamilySelect" runat="server" Checked="false" />
                            </ItemTemplate>
				        </ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Name" HeaderStyle-Width="20%" ItemStyle-Width="20%">
					        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
					        <ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "Parents")%></ITEMTEMPLATE>
				        </ASP:TEMPLATECOLUMN>
                       <ASP:TemplateColumn HeaderText="Relationship"  HeaderStyle-Width="15%" ItemStyle-Width="15%">
					        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                          <asp:Label ID="lblRelationship" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "RelationToContestant")%>'></asp:Label>
                              </ItemTemplate>
                         </ASP:TemplateColumn>
				        <ASP:TemplateColumn HeaderText="Gender" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
					        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
				            <ItemTemplate>
				               <asp:Label ID="lblGender" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Gender")%>'></asp:Label>
				            </ItemTemplate>
				        </ASP:TemplateColumn>
				        <ASP:TemplateColumn HeaderText="Age" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
					        <HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
				            <ItemTemplate>
				               <asp:Label ID="lblAge" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "UnderFive")%>'></asp:Label>
				            </ItemTemplate>
				        </ASP:TemplateColumn>
				        <ASP:TemplateColumn HeaderText="Day1" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
					        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDay1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day1")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Day2" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
					        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDay2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day2")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                          </ASP:TemplateColumn>
                        <ASP:BOUNDCOLUMN DataField="Email" Visible="false"></ASP:BOUNDCOLUMN>
				        <ASP:BOUNDCOLUMN DataField="HPhone" Visible="false"></ASP:BOUNDCOLUMN>
				        <ASP:BOUNDCOLUMN DataField="CPhone" Visible="false"></ASP:BOUNDCOLUMN>
				        <ASP:BOUNDCOLUMN DataField="ChildNumber" Visible="false"></ASP:BOUNDCOLUMN>
				        <ASP:BOUNDCOLUMN DataField="Parents" Visible="false"></ASP:BOUNDCOLUMN>
				         <ASP:TemplateColumn HeaderText="Lunch Type" HeaderStyle-Width="20%" ItemStyle-Width="20%" >
					        <HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                            <ItemTemplate>
                                    <asp:Label ID="lblLunchType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "LunchType")%>'  Visible="True"></asp:Label>
                                    <asp:DropDownList ID="ddllunchTypeNC" runat="server" Visible="false">
                                             <asp:ListItem Text="Indian Food" Value="1"></asp:ListItem>
                                             <asp:ListItem Text="Pizza" Value="2"></asp:ListItem>                                                   
                                    </asp:DropDownList>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
				         <ASP:TemplateColumn><ItemTemplate>
				        <asp:LinkButton id="lblRemove" runat="server" CausesValidation="false" CommandName="Select" Text="Remove"></asp:LinkButton>
				        </ItemTemplate>
				        </ASP:TemplateColumn>
			            <ASP:BOUNDCOLUMN DataField="Parents" Visible="false"></ASP:BOUNDCOLUMN>
                 </COLUMNS>
                       <HeaderStyle HorizontalAlign="Left" />
                       <ItemStyle HorizontalAlign ="Left" />
		        <PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
		                 <AlternatingItemStyle BackColor="LightBlue" />
	 </ASP:DATAGRID>
	    
	<div style="text-align :Right"><asp:Button ID="AddFamily" runat="server" Text="Add Family Member" OnClick ="AddFamily_Click" />  </div> 
	<asp:Panel ID="AddFamilyMembers" runat="server" BorderStyle="Solid" Visible="false">
    <table cellpadding ="3" cellspacing ="0" border ="0">
    <tr><td align="left" >First Name</td><td align="left"><asp:TextBox ID="txtFname" runat="server"></asp:TextBox></td></tr>
    <tr><td align="left" >Last Name</td><td align="left"><asp:TextBox ID="txtLname" runat="server"></asp:TextBox></td></tr>
    <tr><td align="left">Relationship</td><td align ="left" >
            <asp:DropDownList ID="ddlRelationship" runat="server">
                    <asp:ListItem Text="Brother" Value="Brother"></asp:ListItem>
                    <asp:ListItem Text="Sister" Value="Sister"></asp:ListItem>
                    <asp:ListItem Text="GrandFather" Value="GrandFather"></asp:ListItem>
                    <asp:ListItem Text="GrandMother" Value="GrandMother"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
            </asp:DropDownList></td></tr>
    <tr><td align="left">Gender</td><td align="left"> 
            <asp:DropDownList ID="ddlGender" runat="server"> <asp:ListItem Text="Male" Value="Male"></asp:ListItem> <asp:ListItem Text="Female" Value="Female"></asp:ListItem> </asp:DropDownList></td></tr>
    <tr><td align="left">Age Group</td><td align="left">
            <asp:DropDownList ID="ddlAge" runat="server"><asp:ListItem Text="Adult (Above 18)" Value="Adult"></asp:ListItem>
                    <asp:ListItem Text="Minor (5 to 18)" Value="Minor"></asp:ListItem>
                    <asp:ListItem Text="Under 5" Value="Under 5"></asp:ListItem>                                                   
            </asp:DropDownList></td>
   <tr><td align="left">Lunch Type</td><td align="left">
            <asp:DropDownList ID="ddllunchType" runat="server">
                     <asp:ListItem Text="Indian Food" Value="1"></asp:ListItem>
                     <asp:ListItem Text="Pizza" Value="2"></asp:ListItem>                                                   
            </asp:DropDownList></td>
   </tr>
        
     <tr><td colspan ="2"> 
         <asp:Label ID="lblAddFamilyErr" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
    <tr><td colspan ="2" align="center"> 
        <asp:Button ID="btnFamilyAdd" runat="server" Text="Submit" OnClick="btnFamilyAdd_Click" /> 
        <asp:Button ID="btnFamily" runat="server" Text="Close Panel" OnClick="btnFamily_Close" /></td></tr>
    </table>
    </asp:Panel>
	<div style="text-align:left"><B>Other Guests</B></div>
	    <asp:Label ID="lblOthersErr" runat="server"></asp:Label>&nbsp;
		    <ASP:DATAGRID id="dgOthers" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
				<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="Name" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TEMPLATECOLUMN HeaderText="Name" HeaderStyle-Width="20%" ItemStyle-Width="20%">
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "Name")%></ITEMTEMPLATE>
						</ASP:TEMPLATECOLUMN>
                       <ASP:TemplateColumn HeaderText="Relationship"  HeaderStyle-Width="15%" ItemStyle-Width="15%">
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                          <asp:Label ID="lblRelationship" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "RelationType")%>'></asp:Label>
                              </ItemTemplate>
                         </ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Gender" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblGender" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Gender")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Age" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblAge" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Age")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="Day1" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                   <asp:Label ID="lblDay1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day1")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="Day2" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                   <asp:Label ID="lblDay2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day2")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="LunchType" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
                          <ItemTemplate>
                                <asp:Label ID="lblLunchType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "LunchType")%>'  Visible="True"></asp:Label>
                           </ItemTemplate>
                        </ASP:TemplateColumn>
                        
                        
                        
                       	<ASP:TemplateColumn><ItemTemplate>
						<asp:LinkButton id="lblRemove" runat="server" CausesValidation="false" CommandName="Select" Text="Remove"></asp:LinkButton>
						</ItemTemplate>
						</ASP:TemplateColumn>
				   </COLUMNS>
		<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>
	<div align="right" > <asp:Button ID="BtnAddGuest" runat="server" Text="Add Guest" OnClick ="BtnAddGuest_Click" />  </div>   
    <asp:Panel ID="AddGuest" runat="server" BorderStyle="Solid" Visible="false">
    <table cellpadding ="3" cellspacing ="0" border ="0">
    <tr><td align="left" >First Name</td><td align="left"><asp:TextBox ID="txtOtherFName" runat="server"></asp:TextBox></td></tr>
    <tr><td align="left" >Last Name</td><td align="left"><asp:TextBox ID="txtOtherLName" runat="server"></asp:TextBox></td></tr>
    <tr><td align="left" >Email</td><td align="left"><asp:TextBox ID="txtOtherEmail" runat="server"></asp:TextBox></td></tr>
    <tr><td align="left">Gender</td><td align="left"> <asp:DropDownList ID="DDLOtherGender" runat="server"> <asp:ListItem Text="Male" Value="Male"></asp:ListItem> <asp:ListItem Text="Female" Value="Female"></asp:ListItem> </asp:DropDownList></td></tr>
    <tr><td align="left">Age Group</td>
        <td align="left">
        <asp:DropDownList ID="ddlOtherAge" runat="server"><asp:ListItem Text="Adult (Above 18)" Value="Adult"></asp:ListItem>
            <asp:ListItem Text="Minor (5 to 18)" Value="Minor"></asp:ListItem>
            <asp:ListItem Text="Under 5" Value="Under 5"></asp:ListItem>                                                   
        </asp:DropDownList></td></tr>
    <tr><td align="left">Home Phone</td><td align="left"><asp:TextBox ID="txtOtherHphone" runat="server"></asp:TextBox></td></tr>
    <tr><td align="left">Cell Phone</td><td align="left"><asp:TextBox ID="txtOtherCPhone" runat="server"></asp:TextBox></td></tr>
    <tr><td align ="left" >NSF Parent</td><td align ="left" >
        <asp:DropDownList ID="ddlOtherNSF" runat="server">
              <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
              <asp:ListItem Text="No" Value="N" Selected="True">
        </asp:ListItem></asp:DropDownList></td></tr>
     <tr><td align="left"> 
         Lunch Type</td>
         <td align="left">
             <asp:DropDownList ID="DropDownList1" runat="server">
                 <asp:ListItem Text="Indian Food" Value="1"></asp:ListItem>
                 <asp:ListItem Text="Pizza" Value="2"></asp:ListItem>
             </asp:DropDownList>
         </td>
        </tr>
        <asp:Label ID="lblOtherErr" runat="server" Text=""></asp:Label>
        </td>
        </tr>
    <tr><td colspan ="2" align="center"> 
        <asp:Button ID="btnOtherAdd" runat="server" Text="Submit" OnClick="btnOtherAdd_Click" />&nbsp;<asp:Button
            ID="btnOthersClose" runat="server" Text="Close Panel" OnClick="btnOthersClose_Click" /></td></tr>
    </table>
    </asp:Panel>
</asp:Panel>
	<br />
    <asp:Label ID="lblDate1" runat="server"  Visible="false"></asp:Label>
    <asp:Label ID="lblDate2" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblMealAmt1" runat="server"  Visible="false"></asp:Label>
    <asp:Label ID="LblMealAmt2" runat="server" Visible="false"></asp:Label>
      <asp:Label ID="lblDay0Dinner" runat="server"  Visible="false"></asp:Label>
        <asp:Label ID="lblDay1Breakfast" runat="server"  Visible="false"></asp:Label>
         <asp:Label ID="lblDay1Dinner" runat="server"  Visible="false"></asp:Label>
         <asp:Label ID="lblDay2Breakfast" runat="server"  Visible="false"></asp:Label>
           <asp:Label ID="lblDay2Dinner" runat="server"  Visible="false"></asp:Label>
     
    <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" BackColor="DeepSkyBlue" />
</td></tr>
</table>
    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
    
    <table cellspacing="1" cellpadding="1" width="900px" border="1">
				<tbody>
					<tr>
						<td class="heading" style="FONT-SIZE: medium; COLOR: red; FONT-FAMILY: Verdana, Arial; BACKGROUND-COLOR: white; font-weight: bold;">INSTRUCTIONSTRUCTIONSTRUCTIONS</td>
					</tr>
					<tr>
						<td class="largewordingbold" align="left" style="COLOR: red;">
							<p>1. Charge for lunch is mandatory for all attendees.  Children under five years of age are not charged.  Lunch is being served as there are no
							 facilities nearby to purchase food.  Neither will you have time to go out for lunch and come back in time for the next event.  Breakfast and dinner are optional.
							   If you select breakfast or dinner, charges will be made to all attendees.

							</p>
							<p>2. Based on the contest(s) registered, the system has prepared 
									the grid above. If your children have contests on both days, everyone who is accompanying the contestant, will be charged for meals for both days.
							</p>
							<p>3. Meal charge cannot be refunded under any circumstances except in the case of a system error. If you register for meal,
							      but do not participate in the event, the entire meal charge becomes tax-deductible donation to NSF.
							</p>
							<p>4. At least one adult should accompany your children. If it is not possible for either parent to accompany, you have two options.
							</p>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									a. You can nominate another NSF parent who is going to be at the event and willing to take responsibility for your children. <%--Before 
									you can nominate, that person must have registered and paid for meals for the same days your children are participating.--%>
									&nbsp;&nbsp;&nbsp;&nbsp;</p>
                            <p>
                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;b. Alternatively, you can nominate a guardian to take responsibility for your children. The guardian will be charged for meals. 
                            </p>
							<p>5. NSF is run by volunteers. To make things simple, volunteers had to make certain choices in implementing the meals program. Currently, the system doesn't have the flexibility to opt out of lunch. </p>
							<p>6. If you want to change the contest selections for your children, <asp:hyperlink id="hlcontests" runat="server" Font-Size="X-Small" NavigateUrl="~/ContestantRegistration.aspx"
										Font-Bold="True">Click Here</asp:hyperlink>	to go back to Contests Page. 
							</p>
						</td>
					</tr>
                    </tbody></table>
                    </center>
</asp:Content>

