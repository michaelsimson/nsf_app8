﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="RE4.aspx.vb" Inherits="Donor_Donate" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table style="width: 100%">
    <tr>
        <td colspan="2" style="text-align: left">
            <asp:LinkButton ID="LinkButton1" runat="server" style="text-align: left">Back to Donor Functions</asp:LinkButton>
        </td>
        <td colspan="2" style="text-align: left">
            <span style="font-size: 10.0pt; line-height: 115%; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;; mso-fareast-font-family: &quot;Times New Roman&quot;; color: #222222; mso-ansi-language: EN-IN; mso-fareast-language: EN-IN; mso-bidi-language: AR-SA; text-align: left;">
            Global Gateway E4 -Refund Demo</span></td>
    </tr>
    <tr>
        <td colspan="4" style="text-align: left">
            <asp:Label ID="lblIP" runat="server" ForeColor="#33CC33"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left">
            &nbsp;</td>
        <td colspan="2">
            <asp:Label ID="Label1" runat="server" BackColor="#0066FF" Font-Bold="False" 
                Font-Size="Small" ForeColor="White" Text="Merchant Defined Values"></asp:Label>
                                                                                           </td>
    </tr>
    <tr>
        <td style="text-align: left; height: 16px; width: 126px">
            Card Holder Name:</td>
        <td style="height: 16px; width: 235px">
            <asp:TextBox ID="txtName" runat="server" AutoPostBack="True" style="margin-left: 0px" Width="213px">Test User</asp:TextBox>
        </td>
        <td style="height: 16px; width: 137px">
            &nbsp;</td>
        <td style="height: 16px; width: 368px;">
            <asp:TextBox ID="txtCR" runat="server" Width="161px" Visible="False">998855</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="text-align: left; width: 126px; height: 26px;">
            Card Number:</td>
        <td style="width: 235px; height: 26px;">
            <asp:TextBox ID="txtcNum" runat="server" Width="213px">4111111111111111</asp:TextBox>
        </td>
        <td style="width: 137px; height: 26px;">
            &nbsp;</td>
        <td style="height: 26px; width: 368px;">
            <asp:TextBox ID="txtRefN" runat="server" Width="162px" Visible="False">776655</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="text-align: left; height: 24px; width: 126px">
            Amount in $ (Refund)</td>
        <td style="height: 24px; width: 235px">
            <asp:TextBox ID="txtAmt" runat="server">100.00</asp:TextBox>
        </td>
        <td style="height: 24px; width: 137px">
            Client Email ID:</td>
        <td style="height: 24px; width: 368px;">
            <asp:TextBox ID="txtMail" runat="server" Enabled="False" Width="221px"></asp:TextBox>
                                                                                           </td>
    </tr>
    <tr>
        <td style="text-align: left; width: 126px; height: 33px;">
            Expiration</td>
        <td style="width: 235px; height: 33px;">
            (Month)&nbsp;
            <asp:DropDownList ID="ddlMonth" runat="server" Height="22px" Width="50px">
            </asp:DropDownList>
            &nbsp; (Year)&nbsp;
            <asp:DropDownList ID="ddlYear" runat="server" Height="23px" Width="70px">
            </asp:DropDownList>
                                                                                           </td>
        <td style="width: 137px; height: 33px;">
            Client IP :</td>
        <td style="height: 33px; width: 368px;">
            <asp:TextBox ID="txtIP" runat="server" Enabled="False" Width="219px"></asp:TextBox>
            </td>
    </tr>
    <tr>
        <td style="text-align: left; width: 126px; height: 33px;">
            CVV2</td>
        <td style="width: 235px; height: 33px;">
            <asp:TextBox ID="txtcvv2" runat="server">123</asp:TextBox>
        </td>
        <td style="width: 137px; height: 33px;">
            &nbsp;</td>
        <td style="height: 33px; width: 368px;">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: left; width: 126px; height: 30px;">
            </td>
        <td style="width: 235px; height: 30px;">
            <asp:Button ID="Button1" runat="server" Text="Process Data" Width="125px" />
        </td>
        <td style="width: 137px; height: 30px;">
            <asp:Button ID="Button2" runat="server" Text="Details" Width="103px" 
                Visible="False" />
            </td>
        <td style="height: 30px; width: 368px;">
            </td>
    </tr>
    <tr>
        <td style="text-align: left; " colspan="4">
            <asp:Label ID="lblStatus" runat="server" Width="300px"></asp:Label>
        </td>
    </tr>
    </table>
</asp:Content>

