Imports NorthSouth.BAL
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class ShowParticipantCertificate
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateParticipantCertificates

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadCertificates()
        End If
    End Sub
    Private Sub LoadCertificates()
        Dim dsCertificates As New DataSet
        Dim badge As New Badge()
        
        generateBadge = CType(Context.Handler, GenerateParticipantCertificates)
        Dim filenameRange As String = ""
        If Session("SelChapterID").ToString() = "1" Then
            Dim StrRange As String() = Session("PageRange").ToString.Split("_")
            filenameRange = Session("PageRange").ToString()
            dsCertificates = badge.GetParticipantsHavingCertificatesFinals(Application("ConnectionString"), Session("SelChapterID"), Session("Event_Year"), generateBadge.ContestDates, StrRange(0), StrRange(1))
        Else
            dsCertificates = badge.GetParticipantsHavingCertificates(Application("ConnectionString"), Session("SelChapterID"), Session("Event_Year"), generateBadge.ContestDates)
        End If
        If dsCertificates.Tables(0).Rows.Count > 0 Then
            rptCertificate.Visible = True
            pnlMessage.Visible = False
            Try
                rptCertificate.DataSource = dsCertificates
                rptCertificate.DataBind()
            Catch ex As Exception

            End Try
        Else
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found."
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            Response.ContentType = "application/vnd.word"
            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If

            Response.AddHeader("content-disposition", "attachment;filename=Certificates_Participants_" & strChapterName.ToString().Replace(" ", "").Replace(",", "_") & "_" & filenameRange & ".doc")
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            rptCertificate.RenderControl(htmlWrite)
            Response.Write("<html>")
            Response.Write("<head>")
            Response.Write("<style>")
            '612.0  792.0
            Response.Write("@page Section2 {size:792.0pt 612.0pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("@page Section1 {size:792.0pt 612.05pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

            'Response.Write("@page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            'Response.Write("@page Section1 {size:841.7pt 595.45pt;mso-page-orientation:landscape;margin:0.7in 0.7in 0.5in 0.7in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")

            'Response.Write("@page Section1 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("div.Section1 {page:Section1;}")
            'Response.Write("@page Section2 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("div.Section2 {page:Section2;}")
            Response.Write("</style>")
            Response.Write("</head>")
            Response.Write("<body>")
            Response.Write("<div class='Section2'>")
            Response.Write(stringWrite.ToString())
            Response.Write("</div>")
            Response.Write("</body>")
            Response.Write("</html>")
            Response.End()
        End If
    End Sub
    Protected Function GetProductName(ByVal ChildNumber As Double) As String
        Dim strProductName As String
        strProductName = ""
        Dim dsProductNames As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), generateBadge.ContestDates, ChildNumber)
        If dsProductNames.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsProductNames.Tables(0).Rows.Count - 1
                If Len(strProductName) > 0 Then
                    If i = dsProductNames.Tables(0).Rows.Count - 1 Then
                        strProductName = strProductName & " and " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    Else
                        strProductName = strProductName & ", " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    End If
                Else
                    strProductName = dsProductNames.Tables(0).Rows(i)(0).ToString()
                End If
            Next
        Else
            strProductName = ""
        End If
        Return strProductName
    End Function

    Protected Function GetContestDates(ByVal ChildNumber As Double, ByVal ContestDate As String, ByVal ProductCnt As Integer) As String
        Dim strcnstDate As String
        strcnstDate = ""
        If ProductCnt = 1 Then
            strcnstDate = " on " & ContestDate
        Else
            Dim dscnstDates As New DataSet
            Dim i As Integer
            dscnstDates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select convert(varchar(30),Cn.contestdate,107), DATENAME(MM, Cn.contestdate) AS CnstMonth from contestant C Inner Join Contest Cn ON C.ContestID=Cn.ContestID AND C.ChapterID = Cn.NSFChapterID and C.EventId = Cn.EventId   where C.ChildNumber=" & ChildNumber & " and Cn.ContestDate in (" & generateBadge.ContestDates & ") and C.ContestYear=" & Session("Event_Year") & " and C.ChapterID=" & Convert.ToInt32(Session("SelChapterID")) & " Group By  Cn.contestdate")  ' Convert.ToInt32(Session("SelChapterID")), , generateBadge.ContestDates, )
            If dscnstDates.Tables(0).Rows.Count > 1 Then
                For i = 0 To dscnstDates.Tables(0).Rows.Count - 1
                    If Len(strcnstDate) > 0 Then
                        If i = dscnstDates.Tables(0).Rows.Count - 1 Then
                            strcnstDate = strcnstDate & "-" & dscnstDates.Tables(0).Rows(i)(0).ToString().Substring(4, 8)
                        Else
                            strcnstDate = strcnstDate & "-" & dscnstDates.Tables(0).Rows(i)(0).ToString().Substring(4, 2)
                        End If
                    Else
                        strcnstDate = " during " & dscnstDates.Tables(0).Rows(i)(1).ToString() & " " & dscnstDates.Tables(0).Rows(i)(0).ToString().Substring(4, 2)
                    End If
                Next
            Else
                strcnstDate = " on " & ContestDate
            End If
        End If
        Return strcnstDate
    End Function



    Protected Function GetContestNo(ByVal ChildNumber As Double, ByVal DBLeft As String, ByVal SessionLeft As String) As String
        Try
            Dim ContestNo As Integer = 0
            Dim dsProductNames As New DataSet
            Dim badge As New Badge()
            dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), generateBadge.ContestDates, ChildNumber)
            If dsProductNames.Tables(0).Rows.Count > 0 Then
                If dsProductNames.Tables(0).Rows.Count = 1 Then
                    Return DBLeft
                Else
                    Return SessionLeft
                End If
            Else
                Return SessionLeft
            End If
        Catch ex As Exception
            Response.Write(ex.ToString() & "<br> test " & ChildNumber)
        End Try
        Return SessionLeft

    End Function

    Public Function ShowImage(ByVal imageURL As String, ByVal spacerURL As String, ByVal imagePath As String)
        Dim f As New IO.FileInfo(Server.MapPath(imagePath))
        'Dim path As String = "https://www.northsouth.org" & imagePath
        'Dim f As New IO.FileInfo(path)
        If f.Exists Then
            Return imageURL
        Else
            Return spacerURL
        End If

    End Function

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
    End Sub

    Public Function GetLeftShowImage(ByVal ChildNumber As Double, ByVal imageURL As String, ByVal spacerURL As String, ByVal imagePath As String, ByVal SessimageURL As String)
        Dim ContestNo As Integer = 0
        Dim dsProductNames As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), generateBadge.ContestDates, ChildNumber)
        If dsProductNames.Tables(0).Rows.Count > 0 Then
            If dsProductNames.Tables(0).Rows.Count = 1 Then
                Dim f As New IO.FileInfo(Server.MapPath(imagePath))
                'Dim path As String = "https://www.northsouth.org" & imagePath
                'Dim f As New IO.FileInfo(path)
                If f.Exists Then

                    Return imageURL
                Else
                    Return spacerURL
                End If
            Else
                Return SessimageURL
            End If
        Else
            Return SessimageURL
        End If
        Return SessimageURL
    End Function
End Class
