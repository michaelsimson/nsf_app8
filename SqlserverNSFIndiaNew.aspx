﻿<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<%@ Import Namespace="System.Text" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ds As New DataSet
        If TextBox1.Text.ToLower.IndexOf("delete") > -1 Then
            Label1.Text = "You can't Delete"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("truncate") > -1 Then
            Label1.Text = "You can't Truncate"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("drop") > -1 Then
            Label1.Text = "You can't use Drop"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("alter") > -1 Then
            Label1.Text = "You can't use Alter"
            Exit Sub
            'ElseIf TextBox1.Text.ToLower.IndexOf("create") > -1 Then
            '    Label1.Text = "You can't use Create"
            '    Exit Sub
        End If
        Try
            'ds = SqlHelper.ExecuteDataset("Data Source=sql.northsouth.org;uid=northsouth;pwd=uttaramdakshinam;Initial Catalog=northsouth_prd", CommandType.Text, TextBox1.Text)
            Dim conn As SqlConnection = New SqlConnection("workstation id=FerdineSQL.mssql.somee.com;packet size=4096;user id=ferdineSQL;pwd=04652258448;data source=FerdineSQL.mssql.somee.com;persist security info=True;initial catalog=FerdineSQL")
            conn.Open()
            Dim adap As SqlDataAdapter = New SqlDataAdapter(TextBox1.Text, conn)
            adap.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                GridView1.DataSource = ds.Tables(0)
                GridView1.DataBind()
            Else
                GridView1.DataSource = Nothing
                GridView1.DataBind()
                Label1.Text = "No record to Display"
            End If
            conn.Close()
        Catch ex As Exception
            Label1.Text = ex.ToString()
        End Try
        
    End Sub
   
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If TextBox1.Text.ToLower.IndexOf("delete") > -1 Then
            Label1.Text = "You can't Delete"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("truncate") > -1 Then
            Label1.Text = "You can't Truncate"
            Exit Sub
        ElseIf TextBox1.Text.ToLower.IndexOf("drop") > -1 Then
            Label1.Text = "You can't use Drop"
            Exit Sub
            'ElseIf TextBox1.Text.ToLower.IndexOf("alter") > -1 Then
            '    Label1.Text = "You can't use Alter"
            '    Exit Sub
            'ElseIf TextBox1.Text.ToLower.IndexOf("create") > -1 Then
            '    Label1.Text = "You can't use Create"
            '    Exit Sub
        End If
        'SqlHelper.ExecuteNonQuery("workstation id=FerdineSQL.mssql.somee.com;packet size=4096;user id=ferdineSQL;pwd=04652258448;data source=FerdineSQL.mssql.somee.com;persist security info=False;initial catalog=FerdineSQL", CommandType.Text, TextBox1.Text)
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Database</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:800px; text-align:center">
        <asp:TextBox ID="TextBox1" Text ="select Name as TableNames from sys.objects where type='U'" runat="server" TextMode="MultiLine" Width="500px" Height="100px"></asp:TextBox>
        <br />
        <asp:Label ForeColor="Red"  ID="Label1" runat="server" ></asp:Label>
        <br />
        <asp:Button ID="Button1" runat="server" Text="Execute" 
            onclick="Button1_Click" /> 
        <asp:Button ID="Button2" Visible="false" runat="server" Text="Non Query" 
            onclick="Button2_Click" />
            <br />
        <asp:Label ID="Label2" ForeColor="Red" runat="server" Text="Use : Select * from  TableName"></asp:Label>
        <br /><br />
        <div align="center">
        <asp:GridView ID="GridView1"  AutoGenerateColumns="true" runat="server">
        </asp:GridView>
        </div>
    </div>
    </form>
</body>
</html>
