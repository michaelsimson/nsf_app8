Imports System
Imports system.io
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Net
Imports System.Net.Mail
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class Reg_Game
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim strSql1 As String
    Dim strSql2 As String
    Dim strSql3 As String
    Dim strSql4 As String
    Dim memberID As Integer
    Dim ChildNumber As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Session("entryToken") = "Parent" Then
            lnkmain.Text = "Back to Parent Functions Page"
        ElseIf Session("entryToken") = "Donor" Then
            lnkmain.Text = "Back to Donor Functions Page"
        ElseIf Session("entryToken") = "Volunteer" Then
            lnkmain.Text = "Back to Volunteer Functions Page"
        End If
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "Show popup", "window.open (...);", True)
        If Not Page.IsPostBack Then
            If Session("entrytoken") = "Parent" Then
                LoadChild()
                LoadGame()
                btnemail.Enabled = False
                memberID = Session("CustIndID")
            ElseIf Session("entrytoken") = "Volunteer" Then
                lnkGS.Visible = True
                memberID = Session("MemberID")
                ChildNumber = Session("ChildNumber")
                If Not Session("ChildNumber") Is Nothing Then
                    LoadChild()
                    LoadGame()
                Else
                    LoadChild()
                    LoadGame()
                End If
            End If
        Else
            If Session("entrytoken") = "Parent" Then
                memberID = Session("CustIndID")
            ElseIf Session("entrytoken") = "Volunteer" Then
                memberID = Session("MemberID")
            End If
        End If
        lblerr.Text = ""
        lblerror.Text = ""
        lblerr3.Text = ""
        lblerr4.Text = ""
        lblerr5.Text = ""
    End Sub

    Private Sub LoadChild()  'Load Child Dropdown using below SQL statment
        Dim LoginId As String = ""
        If Session("entrytoken") = "Parent" Then
            LoginId = Session("CustIndID")
        ElseIf Session("entrytoken") = "Volunteer" Then
            LoginId = Session("MemberID")
        End If

        strSql = "Select First_Name +' '+ Middle_initial +' '+ Last_Name 'ChildName', ChildNumber from child where memberid = " + LoginId
        Dim drchild As SqlDataReader
        Dim rowcount As Integer = 0
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drchild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        While drchild.Read()
            drpChild.Items.Add(New ListItem(drchild(0).ToString(), drchild(1).ToString()))
            rowcount = rowcount + 1
        End While
        If rowcount = 1 Then
            drpChild.SelectedIndex = 0
            drpChild.Enabled = False
        Else
            drpChild.Items.Insert(0, New ListItem("Select a Child", ""))
        End If
        If Session("entrytoken") = "Volunteer" Then
            If Not Session("ChildNumber") Is Nothing Then
                drpChild.SelectedValue = ChildNumber
                drpChild.Enabled = False
            End If
        End If

    End Sub

    Private Sub LoadGame() 'Load Game dropdown using below SQL statment

        If Not Session("ProductID") = "" Then
            strSql = "Select  ProductID ,Name  from Product where eventid= " & Session("EventID") & " and status='O' and ProductID = " & Session("ProductID")
            Dim drgame As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drgame = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            While drgame.Read()
                drpgame.Items.Add(New ListItem(drgame(1).ToString, drgame(0).ToString()))
            End While
            drpgame.Enabled = False
        Else
            strSql = "Select  ProductID ,Name  from Product where eventid=4 and status='O'"
            Dim drgame As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drgame = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            While drgame.Read()
                drpgame.Items.Add(New ListItem(drgame(1).ToString, drgame(0).ToString()))
            End While
            drpgame.Items.Insert(0, New ListItem("Select Game", ""))
        End If
    End Sub

    Protected Sub bindPanel1()        'This method to Bind Panel1 i.e Panel 1: Game records for the selected Child

        grd1.DataSource = Nothing   'Refresh the gridview
        grd1.DataBind()

        ChildNumber = drpChild.SelectedValue
        Dim dsdas As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        strSql = "Select g.GameID, c.first_name + ' ' + c.last_name ChildName, p.name, g.CreateDate, g.ChildLoginID, g.ChildPWD, g.Approved, g.StartDate, "
        strSql = strSql + "g.EndDate, i.lastname + ' ' + i.firstname ParentName, CASE WHEN (g.EndDate >= GetDate() or g.Enddate IS NULL) THEN 1 Else 0 END 'Active', i.hphone, i.email,"
        strSql = strSql + " i.ChapterID, i.Chapter, i.City, i.State from Game g, Child c, Product p, IndSpouse i "
        strSql = strSql + "where g.childnumber = c.childnumber and g.eventid = p.eventid and g.productcode = p.productcode and "
        strSql = strSql + "g.memberid = i.automemberid and g.memberid =" & memberID & "and g.childnumber = " & ChildNumber & "order by c.last_name,"
        strSql = strSql + " c.first_name, g.productid, g.enddate DESC"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        lbl1.Visible = True
        lblAmountused.Visible = True
        lblAmountused1.Visible = True
        lblAmountunused.Visible = True
        txtamountunused1.Visible = True
        lblshortfall.Visible = True
        txtshortfall1.Visible = True
        lblnote.Visible = True

        If (dsdas.Tables(0).Rows.Count > 0) Then   'if record exist 
            grd1.DataSource = dsdas.Tables(0)
            grd1.DataBind()
            Dim count1 As Integer = dsdas.Tables(0).Rows.Count
            Dim i As Integer
            For i = 0 To count1 - 1
                Dim edate As String = dsdas.Tables(0).Rows(i)(8).ToString
                If edate = "" Then
                    DirectCast(grd1.Rows(i).Cells(0).FindControl("lnkgrd1"), LinkButton).Visible = True
                Else
                    Dim edate1 As DateTime = dsdas.Tables(0).Rows(i)(8)
                    If edate1 > DateTime.Now Then
                        DirectCast(grd1.Rows(i).Cells(0).FindControl("lnkgrd1"), LinkButton).Visible = True
                    End If
                End If
            Next i
        Else
            lblerr.Text = "No history exists."
        End If

    End Sub

    Protected Sub Bind()  'method to bind Panel2, Panel3, Panel4

        Dim dsdas As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        ChildNumber = drpChild.SelectedValue
        Dim Cumulative_DAS_Amount As Integer = 0
        Dim Cumulative_Donations As Integer = 0
        Dim Number_of_active_accesses1 As Integer = 0
        Dim Number_of_active_accesses2 As Integer = 0
        Dim Amountused1 As Integer = 0
        Dim Amountused2 As Integer = 0
        Dim das50 As Integer = 0
        Dim taxdeductible As Integer = 0
        Dim Total_amount As Integer = 0
        Dim Amount_Unused As Integer = 0
        Dim shortfall As Integer = 0

        'code for amountused, amountunuses,shortfall, das, taxdecucatble, total amount, Cumulative donations

        strSql4 = "Select Count(*)  from Game where memberid =" & memberID & " and childnumber = " & ChildNumber & " and EndDate >= GetDate()"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql4)
        If dsdas.Tables(0).Rows(0)(0) > 0 Then
            Number_of_active_accesses1 = dsdas.Tables(0).Rows(0)(0).ToString
            Amountused1 = Number_of_active_accesses1 * 50
            lblAmountused1.Text = Amountused1
        End If

        strSql4 = "Select Count(*)  from Game where memberid = " & memberID & " and childnumber <> " & ChildNumber & " and EndDate >= GetDate()"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql4)
        If dsdas.Tables(0).Rows(0)(0) > 0 Then
            Number_of_active_accesses2 = dsdas.Tables(0).Rows(0)(0)
            Amountused2 = Number_of_active_accesses2 * 50
            txtAU1.Text = Amountused2
        End If

        strSql4 = "Select  Sum(AmountRec) from DAS d, Child c, IndSpouse i where d.childnumber = c.childnumber and d.memberid = i.automemberid and d.memberid = " & memberID & " and d.DateReceived + 366 > GetDate()  "
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql4)
        If Not dsdas.Tables(0).Rows(0)(0) Is DBNull.Value Then
            Cumulative_DAS_Amount = dsdas.Tables(0).Rows(0)(0)
            txtDAS1.Text = Cumulative_DAS_Amount
            das50 = 0.5 * Cumulative_DAS_Amount
            txtDAS501.Text = das50
        End If
        strSql4 = "Select Sum(Amount), Sum(d.Amount * d.TaxDeduction/100) from DonationsInfo d, IndSpouse i where d.memberid = i.automemberid and d.donortype = 'IND' and  d.EventId not in (13) and d.memberid = " & memberID & " and d. DonationDate + 366 > GetDate() "
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql4)
        If Not dsdas.Tables(0).Rows(0)(0) Is DBNull.Value Then
            Cumulative_Donations = dsdas.Tables(0).Rows(0)(0)
            txtCdonations1.Text = Cumulative_Donations
            taxdeductible = dsdas.Tables(0).Rows(0)(1)
            txttaxdeductible.Text = taxdeductible

        End If
        Total_amount = das50 + taxdeductible
        Amount_Unused = Total_amount - Amountused1 - Amountused2
        txtamountunused1.Text = Amount_Unused

        'Use the below SQL statement for required donation to get access.

        strSql1 = "select RegFee as ReqDonation, ProductID, ProductCode from eventfees where productid = " & drpgame.SelectedValue ' 
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
        Dim ReqDonation As Integer = ds1.Tables(0).Rows(0)(0)
        If Amount_Unused >= ReqDonation Then
            shortfall = 0
            txtshortfall1.Text = shortfall
        Else
            shortfall = ReqDonation - Amount_Unused
            txtshortfall1.Text = shortfall
        End If

        'code to bind Panel2 i.e Panel 2: Game records for other children in the family 
        strSql1 = "Select g.GameID, c.first_name +' '+ c.last_name ChildName, p.name, g.CreateDate, g.ChildLoginID, g.ChildPWD, g.Approved, g.StartDate, g.EndDate,"
        strSql1 = strSql1 + "i.lastname + ' '+ i.firstname ParentName, CASE WHEN (g.EndDate >= GetDate() or g.Enddate IS NULL) THEN 1 Else 0 END 'Active', i.hphone, i.email, i.ChapterID,"
        strSql1 = strSql1 + " i.Chapter, i.City, i.State from Game g, Child c, Product p, IndSpouse i "
        strSql1 = strSql1 + " where g.childnumber = c.childnumber and g.eventid = p.eventid and g.productcode = p.productcode and "
        strSql1 = strSql1 + " g.memberid = i.automemberid and g.memberid = " & memberID & " and "
        strSql1 = strSql1 + " g.childnumber <> " & ChildNumber & " order by c.last_name, c.first_name, g.productid, g.StartDate DESC"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
        lblAU.Visible = True
        txtAU1.Visible = True
        lbl2.Visible = True
        If (dsdas.Tables(0).Rows.Count > 0) Then
            grd2.DataSource = dsdas.Tables(0)
            grd2.DataBind()
        Else
            lblerr3.Text = "No history exists."

        End If

        'code to Bind Panel3 i.e Panel 3: DAS Collections 
        strSql2 = "Select d.DASID, c.first_name + ' ' + c.last_name ChildName, d.CreateDate, d.AmountCol, d.DateMailed, d.AmountRec, d.DateReceived, i.lastname + ' ' + i.firstname PName"
        strSql2 = strSql2 + " from DAS d, Child c, IndSpouse i where d.childnumber = c.childnumber and d.memberid = i.automemberid and d.memberid =" & memberID
        strSql2 = strSql2 + "and d.DateReceived + 366 > GetDate()  order by c.last_name, c.first_name, d.datereceived DESC"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql2)
        lblDAS.Visible = True
        txtDAS1.Visible = True
        lblDAS50.Visible = True
        txtDAS501.Visible = True
        lbl3.Visible = True
        If (dsdas.Tables(0).Rows.Count > 0) Then
            grd3.DataSource = dsdas.Tables(0)
            grd3.DataBind()
        Else
            lblerr4.Text = "No history exists."
        End If

        'Code to bind Panel4 i.e Panel 4: Donations
        strSql3 = "Select d.DonationID, i.lastname + ' ' + i.firstname Parentname, d.Amount, d.TaxDeduction, d.DonationDate from DonationsInfo d, IndSpouse i where d.memberid = i.automemberid and d.donortype = 'IND' "
        strSql3 = strSql3 + "and d.memberid = " & memberID & " and  d.EventId not in (13) and d. DonationDate + 366 > GetDate() order by i.lastname, i.firstname, d.donationdate DESC"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql3)
        lblCdonations.Visible = True
        txtCdonations1.Visible = True
        lbldonation.Visible = True
        txttaxdeductible.Visible = True
        lbltaxdeductible.Visible = True
        If (dsdas.Tables(0).Rows.Count > 0) Then
            grd4.DataSource = dsdas.Tables(0)
            grd4.DataBind()
        Else
            lblerr5.Text = "No history exists."
        End If

    End Sub

    Protected Sub tablebind()

        'Code to bind Pop-up Panel i.e for New/Renew Access to game

        ChildNumber = drpChild.SelectedValue
        txtcname.Text = Nothing
        txtClogin.Text = Nothing
        txtCPwd.Text = Nothing
        txtSdate.Text = Nothing
        txtEdate.Text = Nothing
        txtchapter.Text = Nothing

        table154.Visible = True
        Dim ds As New DataSet
        Dim ds1 As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        '1. to find out Active record for selected child and selected game
        strSql = "Select * from Game where childnumber = " & ChildNumber & " and memberid =" & memberID & "and productid = " & drpgame.SelectedValue & " and (enddate >= '" & Now.Date() & "' or enddate is NULL)"
        ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        If ds1.Tables(0).Rows.Count > 0 Then 'if active record exist for selected child and selected game
            lblerror.Text = "An active record already exists for this game.  So you cannot create a new one.You can change Child Login ID and Password in Panel 1 by clicking on Edit hyperlink."
            table154.Visible = False
        Else

            If txtshortfall1.Text > 0 Then '2. if shortfall is greater then 0 (Only for Parents)
                If Session("entrytoken") = "Parent" Then
                    lblerror.Text = "To get access, donation is requested. Current shortfall is $" & txtshortfall1.Text & " in order to get access to the game. 1) To donate please <a href='UserFunctions.aspx'> Click Here</a> and then select Donate option on the Parent Functions Page.  2) Next, after donating, you need to request game access by coming back to Register for Game option."
                    'lblerror.Text = lblerror.Text.Replace("Click Here", "<a href='UserFunctions.aspx'>Click Here</a>")
                    table154.Visible = False
                End If
            End If
            'Else
            '3. below SQL statement is to search , if selected child and selected game data already exist in Database 
            strSql = "Select * from Game where childnumber = " & ChildNumber & " and memberid =" & memberID & "and productid = " & drpgame.SelectedValue & " and enddate <= '" & Now.Date() & "'"
            ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

            If ds1.Tables(0).Rows.Count <= 0 Then '4. if record does not exist, this will be New Access to game

                lblheader.Text = "New Game Record for Child"
                'strSql = "Select  c.first_name + ' ' + c.last_name ChildName, i.lastname + ' ' + i.firstname ParentName, i.hphone, i.email, "
                'strSql = strSql + " i.ChapterID, i.Chapter, i.City, i.State, CASE WHEN g.EndDate >= GetDate() THEN 1 Else 0 END 'Active' from game g, Child c, IndSpouse i "
                'strSql = strSql + " where g.childnumber = c.childnumber and g.memberid = i.automemberid and c.memberid = i.automemberid and c.memberid =" & memberID '& "and c.Childnumber = " & ChildNumber
                strSql = "Select  i.lastname + ' ' + i.firstname ParentName, i.hphone, i.email,  i.ChapterID, i.Chapter from indspouse i where i.automemberid = " & memberID
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    txtcname.Text = drpChild.SelectedItem.Text
                    txtchapter.Text = ds.Tables(0).Rows(0)(4)
                    txtDateRegistered.Text = Now.Date()
                    txtPName.Text = ds.Tables(0).Rows(0)(0)
                    txtPhone.Text = ds.Tables(0).Rows(0)(1)
                    txtemail.Text = ds.Tables(0).Rows(0)(2)
                    txtgname.Text = drpgame.SelectedItem.Text
                    ViewState("ChapterID") = ds.Tables(0).Rows(0)(3)
                    ViewState("Chaptercode") = ds.Tables(0).Rows(0)(4)
                End If
            Else '5. if record exist , this will be Renew Access to game
                lblheader.Text = "Renew Game Record for Child"
                strSql = "Select g.GameID, c.first_name + ' ' + c.last_name ChildName, p.name, g.CreateDate, g.ChildLoginID, g.ChildPWD, g.Approved,  "
                strSql = strSql + " i.lastname + ' ' + i.firstname ParentName, CASE WHEN g.EndDate >= GetDate() THEN 1 Else 0 END 'Active', i.hphone, i.email, p.productid, "
                strSql = strSql + " i.ChapterID, i.Chapter, i.City, i.State from Game g, Child c, Product p, IndSpouse i "
                strSql = strSql + "where g.childnumber = c.childnumber and g.eventid = p.eventid and g.productcode = p.productcode and "
                strSql = strSql + "g.memberid = i.automemberid and g.memberid =" & memberID & "and g.childnumber = " & ChildNumber & ""

                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim gameid1 As Integer = Nothing
                    gameid1 = ds.Tables(0).Rows(0)(0)
                    ViewState("gameid") = gameid1
                    txtcname.Text = ds.Tables(0).Rows(0)(1)
                    txtClogin.Text = ds.Tables(0).Rows(0)(4)
                    txtCPwd.Text = ds.Tables(0).Rows(0)(5)
                    'If Not ds.Tables(0).Rows(0)(6) Is DBNull.Value Then
                    '    drpapporved.SelectedValue = ds.Tables(0).Rows(0)(6)
                    'End If
                    txtDateRegistered.Text = ds.Tables(0).Rows(0)(3)
                    txtPName.Text = ds.Tables(0).Rows(0)(7)
                    txtchapter.Text = ds.Tables(0).Rows(0)(13)
                    txtPhone.Text = ds.Tables(0).Rows(0)(9)
                    txtemail.Text = ds.Tables(0).Rows(0)(10)
                    txtgname.Text = drpgame.SelectedItem.Text
                    ViewState("ChapterID") = ds.Tables(0).Rows(0)(12)
                    ViewState("Chaptercode") = ds.Tables(0).Rows(0)(13)
                End If
            End If
        End If

        If Session("entrytoken") = "Parent" Then ' if entrytoken is Parent approve dropdown and Startdate textbox will be disabled
            drpapporved.Enabled = False
            txtSdate.Enabled = False
        End If
    End Sub

    Protected Sub btncontinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncontinue.Click
        ' onclick will bind data for all 4 Panels and disable Continue button and enable the New/Renew access to Game button
        lblchildselection.Text = ""
        lblgameselection.Text = ""

        If drpChild.SelectedValue = "" And drpgame.SelectedValue = "" Then
            lblchildselection.Text = " Select Child"
            lblgameselection.Text = "Select Game"
        ElseIf drpChild.SelectedValue = "" Then
            lblchildselection.Text = " Select Child"
        ElseIf drpgame.SelectedValue = "" Then
            lblgameselection.Text = "Select Game"
        Else
            Btnnewrenew.Enabled = True
            btncontinue.Enabled = False
            drpChild.Enabled = False
            drpgame.Enabled = False

            'set all values to initial
            grd2.DataSource = Nothing
            grd2.DataBind()
            grd3.DataSource = Nothing
            grd3.DataBind()
            grd4.DataSource = Nothing
            grd4.DataBind()
            table154.Visible = False

            'bind all Panels
            bindPanel1()
            Bind()
        End If
    End Sub

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        If txtClogin.Text = "" Or txtCPwd.Text = "" Then
            lblvalidations.Text = "Error"
        Else
            Dim pattern As String = "^[\s\S]{4,20}$"
            Dim pattern1 As String = "^[a-zA-Z0-9][\w\.-@\-]*[a-zA-Z0-9]$"
            Dim Cloginmatch As Match = Regex.Match(txtClogin.Text, pattern)
            Dim Cpwdmatch As Match = Regex.Match(txtCPwd.Text, pattern)
            Dim Cloginmatch1 As Match = Regex.Match(txtClogin.Text, pattern1)
            Dim Cpwdmatch1 As Match = Regex.Match(txtCPwd.Text, pattern1)
            If Not Cloginmatch.Success Or Not Cpwdmatch.Success Or Not Cloginmatch1.Success Or Not Cpwdmatch1.Success Then
                lblvalidations.Text = "Error"
            Else

                'onclick Save button for New/Renew Access to Game
                Dim sb As StringBuilder = New StringBuilder
                Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))

                ChildNumber = drpChild.SelectedValue
                Dim cloginid As String = txtClogin.Text
                Dim dsdas As New DataSet

                'Validate child login id to be unique for each child
                strSql = "Select ChildNumber, ChildLoginID from Game where ChildLoginID ='" & cloginid & "' and childnumber <> " & ChildNumber
                dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

                If dsdas.Tables(0).Rows.Count > 0 Then
                    lblerr.Text = "Login ID is already taken.  Select another Login ID"
                Else

                    'Insert new Access when entrytoken is Parent
                    If Session("entrytoken") = "Parent" Then
                        InsertChild()

                        'Inset/update data , when entry token is Volunteer
                    ElseIf Session("entrytoken") = "Volunteer" Then

                        'Start date required field validation for Volunteer
                        If txtSdate.Text = "" Then
                            lbldateerr.Text = "Please enter Date"
                        ElseIf drpapporved.SelectedIndex = "0" Then
                            lbldateerr.Text = "Select approved"

                        Else
                            Dim enddate As Date = txtSdate.Text
                            enddate = enddate.AddYears(1)  'End date should 1year+ to Start date

                            Dim gameid1 As Integer
                            If ViewState("gameid") Is Nothing Then 'if game id doesnot exist and new record is added
                                InsertChild()

                            Else
                                gameid1 = ViewState("gameid") 'if game id exist

                                Dim saved As Boolean = True
                                Try
                                    strSql = "Update Game Set Productid= '" & drpgame.SelectedValue & "', ChildLoginID ='" & txtClogin.Text & "', ChildPWD = '" & txtCPwd.Text & "', StartDate='" & txtSdate.Text & "',EndDate='" & enddate & "',Approved ='" & drpapporved.SelectedItem.Text & "',ApprovedBy = '" & Session("Loginid") & "',ApprovedDate = '" & Now.Date() & "', ModifyDate ='" & Now.Date() & "',ModifiedBy ='" & Session("LoginID") & "' where ChildNumber =" & ChildNumber & " and Memberid = " & memberID & " and Gameid = " & gameid1
                                    dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                                    Dim dsProduct1 As New DataSet
                                    strSql = "Select ProductGroupID, ProductGroupCode, ProductCode, Productid from Product where eventid = 4 and Status = 'O' and Name='" & drpgame.SelectedItem.Text & "'"
                                    dsProduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                                    strSql = "Update game Set ProductGroupID='" & dsProduct1.Tables(0).Rows(0)(0) & "',ProductGroupCode ='" & dsProduct1.Tables(0).Rows(0)(1) & "', ProductCode='" & dsProduct1.Tables(0).Rows(0)(2) & "',Productid= '" & dsProduct1.Tables(0).Rows(0)(3) & "' where gameid = " & gameid1
                                    dsProduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                                    table154.Visible = False
                                    bindPanel1()
                                    Bind()

                                Catch se As SqlException
                                    lblerr.Text = "The following error occured while saving the payment information.Please contact for technical support" & _
                                    "."
                                    lblerr.Text = (lblerr.Text + se.Message)
                                    saved = False
                                End Try
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub InsertChild() 'This method will insert data , when new acces to game is added (For Parent and Volunteer)

        Dim sb As StringBuilder = New StringBuilder
        Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))

        Dim dsProduct As New DataSet
        strSql = "Select ProductGroupID, ProductGroupCode, ProductID, ProductCode from Product where eventId = 4 and Status='O' and Name = '" & drpgame.SelectedItem.Text & "'"
        dsProduct = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        sb.Append(" declare @GameID int ")
        sb.Append(" SELECT @GameID = MAX(GameID) ")
        sb.Append(" FROM GAME ")
        sb.Append(" ")
        sb.Append("  if @GameID is null ")
        sb.Append("	begin ")
        sb.Append(" set @GameID = 1 ")
        sb.Append("	end ")
        sb.Append(" else ")
        sb.Append(" begin ")
        sb.Append(" set @GameID = @GameID + 1  ")
        sb.Append("	end ")
        sb.Append("INSERT INTO GAME([GameID],[ChapterID],[MemberID],[ChildNumber],[ChildLoginID],[ChildPWD],[StartDate],[EndDate],")
        sb.Append("[Approved],[ApprovedBy],[ApprovedDate],[ChapterCode],[EventID],[EventCode],[EventYear],[ProductGroupID],[ProductGroupCode],")
        sb.Append("[ProductID],[ProductCode],[CreateDate],[CreatedBy],[ModifyDate],[ModifiedBy])")
        sb.Append(" VALUES(@GameID,'<CI>','<MI>','<CN>', '<CLI>', '<CPWD>', '<SD>','<ED>',")
        sb.Append("'<AP>','<AB>','<AD>', '<CC>', '<EI>', '<EC>', '<EY>', '<PGI>', '<PGC>',")
        sb.Append("'<PI>','<PC>','<CD>','<CB>','<MD>','<MB>')")
        sb.Replace("<CI>", ViewState("ChapterID"))
        sb.Replace("<MI>", memberID)
        sb.Replace("<CN>", drpChild.SelectedValue)
        sb.Replace("<CLI>", txtClogin.Text)
        sb.Replace("<CPWD>", txtCPwd.Text)
        sb.Replace("<SD>", "")
        sb.Replace("<ED>", "")
        sb.Replace("<AP>", drpapporved.SelectedItem.Text)
        sb.Replace("<AB>", "")
        sb.Replace("<AD>", "")
        sb.Replace("<CC>", ViewState("Chaptercode"))
        sb.Replace("<EI>", 4)
        sb.Replace("<EC>", "Game")
        sb.Replace("<EY>", Now.Year())
        sb.Replace("<PGI>", dsProduct.Tables(0).Rows(0)(0))
        sb.Replace("<PGC>", dsProduct.Tables(0).Rows(0)(1))
        sb.Replace("<PI>", dsProduct.Tables(0).Rows(0)(2))
        sb.Replace("<PC>", dsProduct.Tables(0).Rows(0)(3))
        sb.Replace("<CD>", Now.Date())
        sb.Replace("<CB>", Session("LoginID"))
        sb.Replace("<MD>", "")
        sb.Replace("<MB>", "")
        ViewState("Productcode") = dsProduct.Tables(0).Rows(0)(3)
        Dim saved As Boolean = True

        Try
            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sb.ToString)
            table154.Visible = False
            If Session("entrytoken") = "Parent" Then
                strSql = "Update Game set ModifyDate = NULL, ModifiedBY = NULL,StartDate = NULL,EndDate = NULL ,Approved = NULL, ApprovedBy = NULL,ApprovedDate = NULL where ModifiedBy = 0 and ChildNumber =" & ChildNumber & " and Memberid = " & memberID & " and Gameid = (Select max(gameid) from game)"
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strSql)
                bindPanel1()
                Bind()
                EmailBody()
            ElseIf Session("entrytoken") = "Volunteer" Then
                Dim enddate As Date = txtSdate.Text
                enddate = enddate.AddYears(1)
                strSql = "Update Game set ModifyDate ='" & Now.Date() & "', ModifiedBY = " & Session("LoginID") & ",StartDate = '" & txtSdate.Text & "',EndDate = '" & enddate & "', Approved = '" & drpapporved.SelectedItem.Text & "',ApprovedBy = " & Session("LoginID") & ",ApprovedDate = '" & Now.Date() & "' where ModifiedBy = 0 and ChildNumber =" & ChildNumber & " and Memberid = " & memberID & " and Gameid = (Select max(gameid) from game)"
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strSql)
                bindPanel1()
                Bind()
                lblerr.Text = "The game record was updated. Please verify."
            End If

        Catch se As SqlException
            Response.Write(sb.ToString)
            lblerr.Text = "The following error occured while saving the payment information.Please contact for technical support" & _
            "."
            lblerr.Text = (lblerr.Text + se.Message)
            saved = False
        End Try
    End Sub

    Protected Sub grd1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        ChildNumber = drpChild.SelectedValue
        grd1.EditIndex = e.NewEditIndex

        'Bind data for Panel1 after editing
        Dim dsdas As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        strSql = "Select g.GameID, c.first_name + ' ' + c.last_name ChildName, p.name, g.CreateDate, g.ChildLoginID, g.ChildPWD, g.Approved, g.StartDate, "
        strSql = strSql + "g.EndDate, i.lastname + ' ' + i.firstname ParentName, CASE WHEN (g.EndDate >= GetDate() or g.EndDate is null) THEN 1 Else 0 END 'Active', i.hphone, i.email,"
        strSql = strSql + " i.ChapterID, i.Chapter, i.City, i.State from Game g, Child c, Product p, IndSpouse i "
        strSql = strSql + "where g.childnumber = c.childnumber and g.eventid = p.eventid and g.productcode = p.productcode and "
        strSql = strSql + "g.memberid = i.automemberid and g.memberid =" & memberID & "and g.childnumber = " & ChildNumber & "order by c.last_name,"
        strSql = strSql + " c.first_name, g.productid, g.enddate DESC"
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        lbl1.Visible = True
        lblAmountused.Visible = True
        lblAmountused1.Visible = True
        lblAmountunused.Visible = True
        txtamountunused1.Visible = True
        lblshortfall.Visible = True
        txtshortfall1.Visible = True
        lblnote.Visible = True

        If (dsdas.Tables(0).Rows.Count > 0) Then
            grd1.DataSource = dsdas.Tables(0)
            grd1.DataBind()

            ' If (Session("entryToken") = "Volunteer") Then
            Dim drpappr As DropDownList = DirectCast(grd1.Rows(e.NewEditIndex).Cells(7).FindControl("drpapp"), DropDownList)
            Dim Count1 As Integer = 0
            Count1 = Convert.ToInt32(dsdas.Tables(0).Rows.Count.ToString())
            Dim i As Integer
            For i = 0 To Count1 - 1
                If Not dsdas.Tables(0).Rows(i)(6) Is DBNull.Value Then
                    drpappr.SelectedValue = dsdas.Tables(0).Rows(i)(6)
                End If
            Next i
            'End If

            If Session("Entrytoken") = "Parent" Then
                'Dim drpappr1 As DropDownList = DirectCast(grd1.Rows(e.NewEditIndex).Cells(7).FindControl("drpapp"), DropDownList)
                Dim txtstrat As TextBox = DirectCast(grd1.Rows(e.NewEditIndex).Cells(8).FindControl("txtSdate"), TextBox)
                drpappr.Enabled = False
                txtstrat.Enabled = False
            End If
        Else
            lblerr.Text = "No history exists."
        End If
    End Sub

    Protected Sub grd1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs)
        grd1.EditIndex = -1
        bindPanel1() 'Bind data for Panel1
        Bind()  'Bind Data for Panel2, Panel3, Panel4
    End Sub

    Protected Sub grd1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)

        'update data for edit gridview Panel1
        Dim dsdas As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        Dim row As GridViewRow = DirectCast(grd1.Rows(e.RowIndex), GridViewRow)
        Dim Sdate As TextBox = DirectCast(row.FindControl("txtSdate"), TextBox)
        Dim Edate As DateTime
        Dim drpapproved As DropDownList = DirectCast(row.FindControl("drpapp"), DropDownList)
        Dim Stdateerr As Label = DirectCast(row.FindControl("lblstdate"), Label)
        Dim lblapproved As Label = DirectCast(row.FindControl("lblapproved"), Label)
        Dim gameId As Label = DirectCast(row.FindControl("lblgmid"), Label)
        Dim Cloginid As TextBox = DirectCast(row.FindControl("txtcid"), TextBox)
        Dim loginpwd As TextBox = DirectCast(row.FindControl("txtcpwd1"), TextBox)
        Dim approve As DropDownList = DirectCast(row.FindControl("drpapp"), DropDownList)
        Dim LoginId As String = Session("LoginID")
        Dim str As String
        Dim dsproduct1 As New DataSet
        ChildNumber = drpChild.SelectedValue

        'Validation for Unique userid for each child

        'str = "Select ChildNumber, ChildLoginID from Game where ChildLoginID = '" & Cloginid.Text & "' and ChildNumber <> " & ChildNumber
        str = "Select ChildNumber, ChildLoginID from Game where ChildLoginID = '" & Cloginid.Text & "' and childnumber <> " & ChildNumber
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)

        If dsdas.Tables(0).Rows.Count > 0 Then
            lblerr.Text = "Login ID is already taken.  Select another Login ID"

        Else
            'cmd.Connection = conn
            'conn.Open()
            If Session("entrytoken") = "Volunteer" Then
                If Sdate.Text Is "" Or drpapproved.SelectedIndex = 0 Then
                    If Sdate.Text = "" Then
                        Stdateerr.Text = "Please enter Date"
                    End If
                    If drpapproved.SelectedIndex = 0 Then
                        lblapproved.Text = "Select Approved"
                    End If
                Else
                    Edate = Sdate.Text
                    Edate = Edate.AddYears(1)
                    str = "update Game Set ChildLoginID ='" & Cloginid.Text & "', ChildPWD='" & loginpwd.Text & "', StartDate = '" & Sdate.Text & "', Enddate = '" & Edate & "', ModifyDate='" & Now.Date() & "', ModifiedBy =" & LoginId & ", Approved = '" & approve.SelectedItem.Text & "', ApprovedBY = " & LoginId & ", ApprovedDate= '" & Now.Date() & "' where GameID =" & gameId.Text
                    dsproduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                    str = "Select ProductGroupID, ProductGroupCode, ProductCode,Productid from Product where eventid = 4 and Status = 'O' and Name= '" & drpgame.SelectedItem.Text & "'"
                    dsproduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                    str = "Update game Set ProductGroupID='" & dsproduct1.Tables(0).Rows(0)(0) & "',ProductGroupCode ='" & dsproduct1.Tables(0).Rows(0)(1) & "', ProductCode='" & dsproduct1.Tables(0).Rows(0)(2) & "',Productid= '" & dsproduct1.Tables(0).Rows(0)(3) & "' where gameid = " & gameId.Text & " and ChildLoginID ='" & Cloginid.Text & "'"
                    dsproduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                    lblerr.Text = "The game record was updated. Please verify."
                    grd1.EditIndex = -1
                    bindPanel1()
                    'Bind()
                End If
            ElseIf Session("entrytoken") = "Parent" Then
                str = "update Game Set ChildLoginID ='" & Cloginid.Text & "', ChildPWD='" & loginpwd.Text & "', ModifyDate='" & Now.Date() & "', ModifiedBy =" & LoginId & " where GameID =" & gameId.Text
                SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                str = "Select ProductGroupID, ProductGroupCode, ProductCode,Productid from Product where eventid = 4 and Status = 'O' and Name= '" & drpgame.SelectedItem.Text & "'"
                dsproduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                str = "Update game Set ProductGroupID='" & dsproduct1.Tables(0).Rows(0)(0) & "',ProductGroupCode ='" & dsproduct1.Tables(0).Rows(0)(1) & "', ProductCode='" & dsproduct1.Tables(0).Rows(0)(2) & "',Productid= '" & dsproduct1.Tables(0).Rows(0)(3) & "' where gameid = " & gameId.Text & " and ChildLoginID ='" & Cloginid.Text & "'"
                dsproduct1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, str)
                lblerr.Text = "The game record was updated."
                grd1.EditIndex = -1
                bindPanel1()
                'Bind()
            End If
        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean

        'send email to Volunteer, when parent save new Data
        Dim email As New MailMessage
        email.From = New MailAddress("nsfgame@gmail.com")
        email.To.Add(sMailTo)
        'If Not ViewState("emailto") = "" Then
        '    email.To.Add(CType(ViewState("emailto"), String))
        'End If
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server
        Dim client As New SmtpClient()
       ' Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host

        Dim ok As Boolean = True
        Try
            ' client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            ' client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis
            client.Timeout = 20000
            client.Send(email)

        Catch e As Exception
            lblerr.Text = e.Message.ToString
            ok = False
        End Try
        Return ok
    End Function

    Protected Sub EmailBody()

        'this is email body for Volunteer, to sent the Parent Information
        Dim re As StreamReader
        Dim emailBody As String = ""
        Dim subMail As String = "Game Table Access by Parent"
        re = File.OpenText(Server.MapPath("GameVolunteeremail.htm"))
        emailBody = re.ReadToEnd
        re.Close()

        emailBody = emailBody.Replace("[LoginEmail]", Session("LoginEmail"))
        emailBody = emailBody.Replace("[MemberID]", memberID)
        emailBody = emailBody.Replace("[ChildNumber]", drpChild.SelectedValue)
        emailBody = emailBody.Replace("[ProductCode]", ViewState("Productcode"))
        emailBody = emailBody.Replace("[CreateDate]", Now())
        emailBody = emailBody.Replace("[LoginIdofchild]", txtClogin.Text)
        emailBody = emailBody.Replace("[PwdofChild]", txtCPwd.Text)
        emailBody = emailBody.Replace("[ChildName]", drpChild.SelectedItem.Text)
        If drpapporved.SelectedIndex = 0 Then
            emailBody = emailBody.Replace("[ApprovedFlag]", "NULL")
        Else
            emailBody = emailBody.Replace("[ApprovedFlag]", drpapporved.SelectedItem.Text)
        End If
        Dim email As String = "nsfgame@gmail.com"
        If SendEmail(subMail, emailBody.ToString, CType(email, String)) Then
            lblerr.Text = "A new record was created for your child.  An email was sent to nsfgame@gmail.com for giving you access.  You should hear back in 48 hours.  Thank you."
        Else
            lblerr.Text = "There was an error sending email. Please print/save details of this page for your records."
        End If
    End Sub

    Protected Sub lnkmain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkmain.Click

        Session("ChildNumber") = ""
        Session("MemberID") = ""
        Session("EventID") = ""
        Session("ProductID") = ""

        'hyperlink to go back to main funcations page
        If Session("entryToken") = "Parent" Then
            Response.Redirect("UserFunctions.aspx")
        ElseIf Session("entryToken") = "Donor" Then
            Response.Redirect("DonorFunctions.aspx")
        ElseIf Session("entryToken") = "Volunteer" Then
            Response.Redirect("VolunteerFunctions.aspx")
        End If

    End Sub

    Protected Sub lnkGS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGS.Click
        Session("ChildNumber") = ""
        Session("MemberID") = ""
        Session("EventID") = ""
        Session("ProductID") = ""
        memberID = Nothing
        'hyperlink to go back to gamesearch page, this is enable only for volunteer
        Response.Redirect("GameSearch.aspx")
    End Sub

    Protected Sub Btnnewrenew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnnewrenew.Click
        'onclick new/Renew access to game button
        bindPanel1()
        Bind()
        tablebind()
    End Sub

    Protected Sub btnemail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnemail.Click

        'onclick Send email to parent button, wil send an email to selected parent
        Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
        Dim dsdas As New DataSet
        Dim re As StreamReader
        Dim emailBody As String = ""
        Dim subMail As String = "Access to Game"
        re = File.OpenText(Server.MapPath("Gameparentemail.htm"))
        emailBody = re.ReadToEnd
        re.Close()
        strSql = "Select email from indspouse where automemberid = " & memberID & " or relationship = " & memberID
        'strSql = "Select email from indSpouse where automemberid = " & memberID
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        Dim emailto As String = dsdas.Tables(0).Rows(0)(0)
        ViewState("emailto") = dsdas.Tables(0).Rows(1)(0)
        If SendEmail(subMail, emailBody.ToString, CType(emailto, String)) Then
            lblerror.Text = "E-mail has been sent to Parents."
        Else
            lblerror.Text = "There was an error sending email. Please print/save details of this page for your records."
        End If

    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        
        bindPanel1()
        Bind()
        table154.Visible = False
    End Sub

End Class

