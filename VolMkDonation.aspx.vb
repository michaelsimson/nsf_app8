﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class VolMkDonation
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Not IsPostBack Then
            If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                clearsession()
                pIndSearch.Visible = True
                Session("EventID") = 11
            Else
                Response.Redirect("maintest.aspx")
            End If
        End If
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        lblCustIndID.Text = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and Email like '%" + email + "%'")
            Else
                strSql.Append("  Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by lastname,firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No member match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        clearsession()
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Automemberid,Relationship,DonorType,Email,chapterID,FirstName+' '+LastName as Name from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & "")
        While reader.Read()
            If reader("DonorType").ToUpper() = "IND" Then
                lblCustIndID.Text = reader("Automemberid")
                Session("CustIndID") = reader("Automemberid")
                Session("CustIndChapterID") = reader("ChapterID")
                ltl1.Text = "Name : " & reader("Name")
                Response.Redirect("donor_donate.aspx")
            ElseIf reader("DonorType").ToUpper() = "SPOUSE" Then
                lblCustIndID.Text = reader("Relationship")
                Session("CustIndID") = reader("Relationship")
                Session("CustIndChapterID") = reader("ChapterID")
                ltl1.Text = "Name : " & reader("Name")
                Response.Redirect("donor_donate.aspx")
            End If
        End While
        reader.Close()
        If Not lblCustIndID.Text = String.Empty Then
            ' Panel4.Visible = False
            pIndSearch.Visible = False
            ' lblIndSearch.Visible = False
            btnSrchParent.Visible = True
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "No data found"
        End If
    End Sub

    Protected Sub btnSrchParent_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = True
        ltl1.Text = String.Empty
    End Sub
    Private Sub clearsession()
        Session.Remove("CustIndID")
        Session.Remove("Donation")
        Session.Remove("outXml")
        Session.Remove("PaymentReference")
        Session.Remove("R_Approved")
        Session.Remove("SaleAmt")
        Session.Remove("SaleItems")
        Session.Remove("SChapterID")
        Session.Remove("SEmail")
        Session.Remove("SEventID")
    End Sub
End Class
