Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Text
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Partial Class dbsearch
    Inherits System.Web.UI.Page

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

        
            Dim sSQL As String
            Dim sSQLORG As String
            'If LCase(Session("LoggedIn")) <> "true" Then
            '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            'End If
            'If Len(Trim("" & Session("LoginID"))) = 0 Then
            '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            'End If
            'If Len(Trim("" & Session("entryToken"))) = 0 Then
            '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            'End If
            sSQL = "Select MemberID,AutoMemberID,CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END as CustINDID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <> 'Yes'"
            sSQLORG = "Select * from OrganizationInfo WHERE DeletedFlag <>'Yes'"

            ' Dim MemberID As String = ""
            Dim blnCheck As Boolean = False
            Dim Email As String = tbEmail.Text
            Dim FirstName As String = tbFnameOrg.Text
            Dim LastName As String = tbLname.Text
            Dim Street As String = tbStreet.Text
            Dim City As String = tbCity.Text
            Dim state As String = ddlState.Text
            Dim ZipCode As String = tbZip.Text
            Dim HomePhone As String = tbPhone.Text
            Dim ReferredBy As String = tbRef.Text
            Dim LiaisonPerson As String = tbLiaison.Text
            Dim NSFChapter As String = ddlChapter.SelectedItem.Text
            'Dim otherState As String = tbOther.Text
            ' If (Len(Trim(MemberID)) > 0) Then
            'sSQL = sSQL & " and AutoMemberID like '%" & MemberID & "%'"
            'sSQLORG = sSQLORG & " and AutoMemberID like '%" & MemberID & "%'"
            'blnCheck = True
            'End If
            Dim ChapterID As Integer
            If Len(Session("LoginChapterID")) > 0 Then 'Session("LoginChapterID") <> "" Then
                ChapterID = Session("LoginChapterID")
            Else
                ChapterID = -1
            End If
            If (Len(Trim(FirstName)) > 0) Then
                sSQL = sSQL & " and FIRSTNAME like '%" & FirstName & "%'"
                sSQLORG = sSQLORG & " and ORGANIZATION_NAME like '%" & FirstName & "%'"
                blnCheck = True
            End If

            If (Len(Trim(LastName)) > 0) Then
                sSQL = sSQL & " and LASTNAME like '%" & LastName & "%'"
                sSQLORG = sSQLORG & " and ORGANIZATION_NAME like '%" & LastName & "%'"
                blnCheck = True
            End If

            If (Len(Trim(Street)) > 0) Then
                sSQL = sSQL & " and ADDRESS1 like '%" & Street & "%'"
                sSQLORG = sSQLORG & " and ADDRESS1 like '%" & Street & "%'"
                blnCheck = True
            End If

            If (Len(Trim(City)) > 0) Then
                sSQL = sSQL & " and CITY like '%" & City & "%'"
                sSQLORG = sSQLORG & " and CITY like '%" & City & "%'"
                blnCheck = True
            End If

            If (Len(Trim(state)) > 0 And Trim(state) <> "Other") Then
                sSQL = sSQL & " and STATE like '%" & state & "%'"
                sSQLORG = sSQLORG & " and STATE like '%" & state & "%'"
                blnCheck = True
            End If

            'If (Len(Trim(otherState)) > 0 And Trim(state) = "Other") Then
            'sSQL = sSQL & " and STATE like '%" & otherState & "%'"
            'sSQLORG = sSQLORG & " and STATE like '%" & otherState & "%'"
            'blnCheck = True
            'End If

            If (Len(Trim(ZipCode)) > 0) Then
                sSQL = sSQL & " and ZIP like '%" & ZipCode & "%'"
                sSQLORG = sSQLORG & " and ZIP like '%" & ZipCode & "%'"
                blnCheck = True
            End If

            If (Len(Trim(Email)) > 0) Then
                sSQL = sSQL & " and EMAIL like '%" & Email & "%'"
                sSQLORG = sSQLORG & " and EMAIL like '%" & Email & "%'"
                blnCheck = True
            End If

            If (Len(Trim(HomePhone)) > 0) Then
                sSQL = sSQL & " and HPHONE like '%" & HomePhone & "%'"
                sSQLORG = sSQLORG & " and PHONE like '%" & HomePhone & "%'"
                blnCheck = True
            End If

            If (Len(Trim(ReferredBy)) > 0) Then
                sSQL = sSQL & " and ReferredBy like '%" & ReferredBy & "%'"
                sSQLORG = sSQLORG & " and Referred_By like '%" & ReferredBy & "%'"
                blnCheck = True
            End If

            If (Len(Trim(LiaisonPerson)) > 0) Then
                sSQL = sSQL & " and LiasonPerson like '%" & LiaisonPerson & "%'"
                sSQLORG = sSQLORG & " and LiaisonPerson like '%" & LiaisonPerson & "%'"
                blnCheck = True
            End If
            
            If (Convert.ToInt32(Session("RoleID")) = 5 And ChapterID > 1) Or Convert.ToInt32(Session("RoleID")) = 4 Then
                Dim i As Integer
                NSFChapter = ddlChapter.SelectedItem.Text
                If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                    sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                    sSQLORG = sSQLORG & " and NSF_CHAPTER like '%" & NSFChapter & "%'"
                    blnCheck = True
                Else
                    For i = 1 To ddlChapter.Items.Count - 1
                        If Len(NSFChapter) > 0 And NSFChapter <> "Select Chapter" Then
                            NSFChapter = NSFChapter & ",'" & ddlChapter.Items(i).Text & "'"
                        Else
                            NSFChapter = "'" & ddlChapter.Items(i).Text & "'"
                        End If
                    Next
                    sSQL = sSQL & " and CHAPTER IN (" & NSFChapter & ")"
                    sSQLORG = sSQLORG & " and NSF_CHAPTER IN(" & NSFChapter & ") "
                    blnCheck = True
                End If
            Else
                If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") And ChapterID > 1 Then
                    sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                    sSQLORG = sSQLORG & " and NSF_CHAPTER like '%" & NSFChapter & "%'"
                    blnCheck = True
                End If
            End If
            If (blnCheck = False) Then
                If (Session("RoleID") = "5" And ChapterID > 1) Or Session("RoleID") = "4" Then
                    sSQL = "Select MemberID,AutoMemberID,CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END as CustINDID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,City,Zip,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <>'Yes' AND Chapter IN(" & NSFChapter & ") ORDER BY LastName,FirstName ASC"
                    sSQLORG = "Select * from OrganizationInfo WHERE DeletedFlag <>'Yes' AND NSF_Chapter in(" & NSFChapter & ")  ORDER BY ORGANIZATION_NAME"
                Else
                    sSQL = "Select MemberID,AutoMemberID,CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END as CustINDID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,City,Zip,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <>'Yes' ORDER BY LastName,FirstName ASC"
                    sSQLORG = "Select * from OrganizationInfo WHERE DeletedFlag <>'Yes' ORDER BY ORGANIZATION_NAME"
                End If
            Else
                sSQLORG = sSQLORG & " ORDER BY ORGANIZATION_NAME "
            End If
            Session("sSQL") = sSQL
            Session("sSQLORG") = sSQLORG
            Response.Redirect("dbsearchresults.aspx")
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Session("NavPath") = "Search"
            Page.MaintainScrollPositionOnPostBack = True
            '*** Populate State DropDown
            Dim dsStates As DataSet
            Dim strSqlQuery As String
            strSqlQuery = "SELECT Distinct StateCode,Name FROM StateMaster Order By Name"
            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSqlQuery)
            If dsStates.Tables.Count > 0 Then
                ddlState.DataSource = dsStates.Tables(0)
                ddlState.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlState.DataBind()
                ddlState.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If

            '*** Populate Chapter DropDown
            '*** Populate Chapter Names List
            Dim objChapters As New Chapter
            Dim dsChapters As New DataSet
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

            If dsChapters.Tables.Count > 0 Then
                ddlChapter.DataSource = dsChapters.Tables(0)
                ddlChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                ddlChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlChapter.DataBind()
                ddlChapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            End If
            If Len(Session("LoginChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("LoginChapterID")
                ddlChapter.Enabled = False
            End If
            If Session("RoleID") = "5" Or Session("RoleID") = "4" Then
                ddlChapter.Items.Clear()
                Dim strSql As String
                strSql = "Select chapterid, chaptercode, state from chapter "
                'strSql = strSql & " where clusterid in ( "
                If Session("RoleID") = "4" Then
                    strSql = strSql & " where clusterid in ( "
                    strSql = strSql & " Select clusterid from chapter where chapterid = " + Session("LoginChapterID") & ")"
                ElseIf Session("RoleID") = "5" Then
                    strSql = strSql & " where ChapterID = " + Session("LoginChapterID") & "" '  strSql = strSql & Session("selClusterID") & ")"
                End If

                'If Session("RoleID") = "5" And Session("SelchapterID") = 1 Then
                'End If
                strSql = strSql & " order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))
                Dim drChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drChapters.Read())
                    ddlChapter.Items.Add(New ListItem(drChapters(1).ToString(), drChapters(0).ToString()))
                End While
                If ddlChapter.Items.Count > 1 Then
                    ddlChapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                    ddlChapter.Enabled = True
                ElseIf ddlChapter.Items.Count = 1 Then
                    ddlChapter.Enabled = False
                End If
            End If
        End If
    End Sub
End Class
