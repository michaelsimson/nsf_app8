﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingRegistrationUpdate.aspx.vb" Inherits="CoachingRegistrationUpdate" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div>
        <link href="css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>
        <link href="css/jquery.toastmessage.css" rel="stylesheet" />
        <link href="css/Hover.css" rel="stylesheet" />
        <script type="text/javascript">
            $(function (e) {
                roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;
            });
        </script>

        <script type="text/javascript">

            $(function (e) {
                document.getElementById("<%=txtUserId.ClientID%>").value = parentEmail;

            });
        </script>
        <script src="js/ezmodal.js"></script>
        <script src="js/Hover.js"></script>
    </div>

    <div align="left">
        &nbsp;&nbsp;&nbsp;
      <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
    </div>
    <div style="text-align: center">
        <table border="0" cellpadding="3" cellspacing="0" style="width: 800px; text-align: center">
            <tr>
                <td align="center" style="color: #000000; font-family: Arial Rounded MT Bold; font-size: 14px;">Update Student Registration Data</td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr id="trvol" runat="server">
                            <td class="ItemLabel" valign="top" nowrap align="right">Parent Email ID</td>
                            <td>
                                <asp:TextBox ID="txtUserId" runat="server" CssClass="SmallFont" Width="300" MaxLength="50"></asp:TextBox>
                                <asp:Button ID="btnLoadChild" OnClick="btnLoadChild_Click" runat="server" CssClass="FormButtonCenter" Text="Load Child(ren)"></asp:Button>
                                <br>
                                &nbsp;
                 
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
                                    ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnContinue" runat="server" CausesValidation="false" Text="Load All" OnClick="btnContinue_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trEdit" visible="false" runat="server">
                <td align="center">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="left">Approved </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlApproved" runat="server">
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Product </td>
                            <td align="left">
                                <asp:DropDownList Width="150px" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" ID="ddlProduct" DataTextField="ProductName" DataValueField="ProductID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Level </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlLevel" Enabled="false" DataTextField="LevelCode" DataValueField="LevelCode" runat="server" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Coach </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCoachName" Enabled="false" DataTextField="CoachName" DataValueField="SignUpID" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnUpdate" runat="server" CausesValidation="false" Text="Update" OnClick="btnUpdate_Click" />
                                &nbsp;&nbsp;&nbsp;   
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblApproved" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr id="trExcCapacity" runat="server" visible="false">
                <td>
                    <table align="center">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblExcCap" ForeColor="Red" Font-Bold="true" runat="server" Text="Capacity will be exceeded, do you still want to approve this case?"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="BtnYes" runat="server" Text="Yes" Style="width: 37px" />
                                &nbsp;
                        <asp:Button ID="BtnNo" runat="server" Text="No" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgselected" runat="server" OnItemCommand="dgselected_ItemCommand" OnPageIndexChanged="dgselected_PageIndexChanged" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                        Height="14px" GridLines="Both" DataKeyField="CoachRegID" CellPadding="4" BackColor="#755200" BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666" PageSize="10" AllowPaging="true" ForeColor="White" Font-Bold="True">
                        <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                        <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SignUpID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductID" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" CommandName="Select" Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Student Name" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="12%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachName" CssClass="lnkCoachName" Style="cursor: pointer; color: blue;" attr-coachid='<%#DataBinder.Eval(Container.DataItem, "CMemberID")%>' runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>' attr-name='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>' attr-email='<%#DataBinder.Eval(Container.DataItem, "CoachEMail")%>' attr-Accepted="Y" attr-semester='<%#DataBinder.Eval(Container, "DataItem.Semester")%>' attr-ProductGroupID='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>' attr-productID='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>' attr-SessionNo='<%#DataBinder.Eval(Container, "DataItem.SessionNo")%>' attr-level='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                                    <asp:Label ID="lblEventyear" runat="server" Style="display: none;" Text='<%#DataBinder.Eval(Container.DataItem, "EventYear")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ApprovedCount" ItemStyle-HorizontalAlign="Center" HeaderText="Approved Count" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" Visible="true" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" Visible="true" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SessionNo" HeaderText="Session#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>

                            <asp:TemplateColumn HeaderText="Semester" HeaderStyle-Width="15%" Visible="true" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSemester" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Semester")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>


                            <asp:TemplateColumn HeaderText="Start Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="FatherName" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFatherName" runat="server" CssClass="lnkParentName" Style="color: blue; cursor: pointer;" attr-parentID='<%#DataBinder.Eval(Container.DataItem, "PMemberID")%>' Text='<%#DataBinder.Eval(Container.DataItem, "FatherName")%>' attr-name='<%#DataBinder.Eval(Container.DataItem, "FatherName")%>' atr-SessionKey='<%#DataBinder.Eval(Container, "DataItem.MeetingKey")%>' attr-email='<%#DataBinder.Eval(Container, "DataItem.Email")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <%--  <asp:BoundColumn DataField="FatherName" HeaderText="Father Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="EMail" HeaderText="Email" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="City" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="State" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Type" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRegType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Type")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        <PagerStyle ForeColor="white" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PagerStyle>
                        <AlternatingItemStyle BackColor="LightBlue" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblCoachRegID" runat="server" ForeColor="White"></asp:Label>
                    <asp:Label ID="lblSignUpID" runat="server" ForeColor="White"></asp:Label>
                    <asp:Label ID="lblChildNumber" runat="server" ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

    <button type="button" style="display: none;" class="btnPopUP" ezmodal-target="#demo">Open</button>

    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                <span class="spnMemberTitle">Parent Information</span>
            </div>

            <div class="ezmodal-content">

                <div class="dvSendEMailContent" style="display; none;">
                    <div align="center"><span class="spnErrMsg" style="color: red;"></span></div>
                    <div style="clear: both; margin-bottom: 5px;"></div>
                    <div>
                        <div class="from">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">From </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txFrom" value="coaching@northsouth.org" style="width: 400px;" />
                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="To">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">To </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txtTo" style="width: 400px;" />
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="CC">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">CC </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtCC" style="width: 600px; height: 18px;"></textarea>

                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="subjext">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Subject </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtSubject" style="width: 600px; height: 18px;"></textarea>
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="body">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Body </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <div>
                                <asp:TextBox ID="txtPP" runat="server" Style="display: none;"></asp:TextBox>
                                <textarea class="mailBody" style="width: 800px; height: 150px;"></textarea>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="dvgridContent" style="display: none;">
                    <div>
                        <table align="center" class="tblParentInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px;">
                        </table>
                    </div>

                    <div>
                        <table align="center" class="tblCoacnInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px;">
                        </table>
                    </div>
                </div>
            </div>



            <div class="ezmodal-footer">
                <button id="Button1" type="button" class="btnSenEmail">SenEmail</button>
                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>

    <button type="button" class="btnEmailPopUp" ezmodal-target="#dvEmailpopUp" style="display: none;">Open</button>

    <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>
    </div>
    <div id="overlay"></div>


    <input type="hidden" id="hdnRoleID" value="" runat="server" />
    <input type="hidden" id="hdnmemberID" value="" runat="server" />
    <input type="hidden" id="hdnLevel" value="" runat="server" />
    <input type="hidden" id="hdnEventyear" value="" runat="server" />
    <input type="hidden" id="hdnSemester" value="" runat="server" />
    <input type="hidden" id="hdnChildNumber" value="" runat="server" />
    <input type="hidden" id="hdnRegType" value="" runat="server" />
    <input type="hidden" id="hdnEventYears" value="" runat="server" />
    <input type="hidden" id="hdnSemesters" value="" runat="server" />

</asp:Content>

