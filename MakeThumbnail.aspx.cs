using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Creates a thumbnail image from a file spec in the calling URL.
/// </summary>
public partial class MakeThumbnail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // get the file name -- fall800.jpg
        string file = Request.QueryString["file"];

        // create an image object, using the filename we just retrieved
        System.Drawing.Image image = System.Drawing.Image.FromFile(Server.MapPath(file));

        // create the actual thumbnail image
        System.Drawing.Image thumbnailImage = image.GetThumbnailImage(64, 64, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

        // make a memory stream to work with the image bytes
        MemoryStream imageStream = new MemoryStream();

        // put the image into the memory stream
        thumbnailImage.Save(imageStream, System.Drawing.Imaging.ImageFormat.Jpeg);

        // make byte array the same size as the image
        byte[] imageContent = new Byte[imageStream.Length];

        // rewind the memory stream
        imageStream.Position = 0;

        // load the byte array with the image
        imageStream.Read(imageContent, 0, (int)imageStream.Length);

        // return byte array to caller with image type
        Response.ContentType = "image/jpeg";
        Response.BinaryWrite(imageContent);
    }

    ///  <summary>
    /// Required, but not used
    /// </summary>
    /// <returns>true</returns>
    public bool ThumbnailCallback()
    {
        return true;
    }
}
