﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using System.Drawing;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Web.Services;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Collections;
using VRegistration;
using System.Net;
using System.IO;

public partial class RecCoachesReport : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["LoginID"]="4240";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                PopulateYear(ddlYear);

            }
        }
        lblerr.Text = "";
        LblSuccess.Text = "";
    }

    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            ArrayList list = new ArrayList();
            for (int i = DateTime.Now.Year; i >= DateTime.Now.Year - 5; i--)
            {
                list.Add(new ListItem((i.ToString() + "-" + (i + 1).ToString().Substring(2, 2)), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            //ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
            ddlObject.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        }
        catch (Exception ex) { }
    }

    public void LoadCoachNotAssignedCaoch()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where not exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) order by B.LastName,B.FirstName ASC";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BtnExpo.Visible = true;
                    GrdNotAssignedCoach.DataSource = ds;
                    GrdNotAssignedCoach.DataBind();
                    spnNotAssignedCoah.Visible = false;
                    spnNotAssignedCoach.Visible = true;
                    spnNotAssignedCoach.InnerText = "Count = " + ds.Tables[0].Rows.Count + "";
                }
                else
                {
                    spnNotAssignedCoah.Visible = true;
                    GrdNotAssignedCoach.DataSource = ds;
                    GrdNotAssignedCoach.DataBind();
                    spnNotAssignedCoach.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }

    }

    public void LoadCoachAssignedCaoch()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID )and not exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by B.LastName,B.FirstName ASC";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BtnExpo.Visible = true;
                    GrdAssignedCoach.DataSource = ds;
                    GrdAssignedCoach.DataBind();
                    spnAssignedCoach.Visible = false;
                    spnAssignedCoachCount.Visible = true;
                    spnAssignedCoachCount.InnerText = "Count = " + ds.Tables[0].Rows.Count + "";
                }
                else
                {
                    spnAssignedCoach.Visible = true;
                    GrdAssignedCoach.DataSource = ds;
                    GrdAssignedCoach.DataBind();
                    spnAssignedCoachCount.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }

    }

    public void LoadCalendarSignedUpCoach()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by B.LastName,B.FirstName ASC;";

            cmdText += "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.MemberID in( select B.AutoMemberID from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID )  ) order by IP.LastName,IP.Firstname, MemberID";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BtnExpo.Visible = true;
                    int j = 0;
                    string PCode = string.Empty;
                    ds.Tables[0].Columns.Add("Products").SetOrdinal(10);
                    string MemberID = string.Empty;
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {



                        if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                        {
                            PCode += ", " + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                        }
                        else
                        {
                            PCode = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                        }


                        if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                        {
                            j--;

                        }




                        ds.Tables[0].Rows[j]["Products"] = PCode;
                        j++;
                        MemberID = ds.Tables[1].Rows[i]["MemberID"].ToString();

                    }

                    GrdSignedUpCalendar.DataSource = ds;
                    GrdSignedUpCalendar.DataBind();
                    spnSignedUpCal.Visible = false;

                    spnSignedupCalCount.Visible = true;
                    spnSignedupCalCount.InnerText = "Count = " + ds.Tables[0].Rows.Count + "";
                }
                else
                {
                    spnSignedUpCal.Visible = true;
                    GrdSignedUpCalendar.DataSource = ds;
                    GrdSignedUpCalendar.DataBind();
                    spnSignedupCalCount.Visible = false;

                }
            }
        }
        catch (Exception ex)
        {
        }

    }

    public void LoadReturningCalendarSignedUpCoach()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from CalSignUp where EventYear<" + ddlYear.SelectedValue + " and B.AutoMemberID=MemberID and accepted='Y') and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by LastName, FirstName; ";

            cmdText += "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.MemberID in(select B.AutoMemberID from IndSpouse B where exists (select * from CalSignUp where EventYear<" + ddlYear.SelectedValue + " and B.AutoMemberID=MemberID and accepted='Y') and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID )) order by IP.LastName,IP.Firstname ";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BtnExpo.Visible = true;

                    int j = 0;
                    string PCode = string.Empty;
                    ds.Tables[0].Columns.Add("Products").SetOrdinal(10);
                    string MemberID = string.Empty;
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {



                        if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                        {
                            PCode += ", " + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                        }
                        else
                        {
                            PCode = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                        }


                        if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                        {
                            j--;

                        }




                        ds.Tables[0].Rows[j]["Products"] = PCode;
                        j++;
                        MemberID = ds.Tables[1].Rows[i]["MemberID"].ToString();

                    }

                    GrdReturningSignedUpCal.DataSource = ds;
                    GrdReturningSignedUpCal.DataBind();
                    spnReturningSignedUpCal.Visible = false;
                    spnSignedUpCount.Visible = true;
                    spnSignedUpCount.InnerText = "Count of Calendar SignUps = " + ds.Tables[0].Rows.Count.ToString();
                }
                else
                {
                    spnReturningSignedUpCal.Visible = true;
                    GrdReturningSignedUpCal.DataSource = ds;
                    GrdReturningSignedUpCal.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
        }

    }

    public void LoadReturningNotCalendarSignedUpCoach()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from CalSignUp where EventYear<" + ddlYear.SelectedValue + " and B.AutoMemberID=MemberID and accepted='Y') and not exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by LastName, FirstName";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    BtnExpo.Visible = true;
                    GrdReturningNotSignedupCal.DataSource = ds;
                    GrdReturningNotSignedupCal.DataBind();
                    spnReturningNotSignedupCal.Visible = false;
                    spnNoCalSignup.Visible = true;
                    spnNoCalSignup.InnerText = "Count of those who did not sign up = " + ds.Tables[0].Rows.Count + "";
                }
                else
                {
                    spnReturningNotSignedupCal.Visible = true;
                    GrdReturningNotSignedupCal.DataSource = ds;
                    GrdReturningNotSignedupCal.DataBind();
                    spnNoCalSignup.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        btnColseTable2.Visible = false;
        BtnCloaseTable4.Visible = false;
        dvCalSignUp4AppThreeSess.Visible = false;
        try
        {
            if (validateCoaches() == "1")
            {
                GrdAssignedCoach.PageIndex = 0;
                GrdNotAssignedCoach.PageIndex = 0;
                GrdSignedUpCalendar.PageIndex = 0;
                GrdReturningNotSignedupCal.PageIndex = 0;
                GrdReturningSignedUpCal.PageIndex = 0;
                if (ddlCoachType.SelectedValue == "New Coaches")
                {
                    dvAssignedCoach.Visible = true;
                    dvCoachingNew.Visible = true;
                    dvSignedUpCalendar.Visible = true;

                    LoadCoachNotAssignedCaoch();
                    LoadCalendarSignedUpCoach();
                    LoadCoachAssignedCaoch();

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;

                    dvCoachNotAccepted.Visible = false;
                    dvMemberInfo.Visible = false;
                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvMultiplePrdsAndSessionsCoaches.Visible = false;
                }
                else if (ddlCoachType.SelectedValue == "Returning Coaches")
                {

                    DvReturningSignedUpCal.Visible = true;
                    dvReturningNotSignedupCal.Visible = true;

                    LoadReturningCalendarSignedUpCoach();
                    LoadReturningNotCalendarSignedUpCoach();

                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    dvCoachNotAccepted.Visible = false;
                    dvMemberInfo.Visible = false;
                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvMultiplePrdsAndSessionsCoaches.Visible = false;
                }
                else if (ddlCoachType.SelectedValue == "Approve Calendar Signups")
                {
                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;
                    dvMemberInfo.Visible = false;
                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvCoachNotAccepted.Visible = true;
                    dvMultiplePrdsAndSessionsCoaches.Visible = false;
                    LoadCoachNotAcceptedList();
                    LoadCoachAcceptedNotAcceptedList();
                }
                else if (ddlCoachType.SelectedValue == "Approved Multiple Sessions")
                {
                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;
                    dvMemberInfo.Visible = false;

                    dvCoachNotAccepted.Visible = false;

                    dvApprovedTwoSessions.Visible = true;
                    dvApprovedThreeSessions.Visible = true;

                    dvMultiplePrdsAndSessionsCoaches.Visible = false;


                    LoadApprovedTwoSessions();
                    LoadApprovedThreeMoreSessions();
                }
                else if (ddlCoachType.SelectedValue == "Multiple Sessions/Products")
                {
                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;
                    dvMemberInfo.Visible = false;

                    dvCoachNotAccepted.Visible = false;

                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvMultiplePrdsAndSessionsCoaches.Visible = true;
                    LoadMultiplePrdsAndSessionsCoach();
                }
            }
            else
            {
                validateCoaches();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ExportToExcelNewCoach()
    {
        try
        {
            string CmdText = string.Empty;
            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where not exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) order by B.LastName,B.FirstName ASC;";

            CmdText += "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID )and not exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by B.LastName,B.FirstName ASC;";

            CmdText += "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by B.LastName,B.FirstName ASC;";

            string cmdText = "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.MemberID in( select B.AutoMemberID from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year=" + ddlYear.SelectedValue + " and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID )  ) order by IP.LastName,IP.Firstname, MemberID";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            DataSet dsPrd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null)
                {
                    ds.Tables[0].TableName = "Was Not Assigned Coach";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[0].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[0].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ds.Tables[0].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[0].Rows[i]["EventID"] = 13;
                            ds.Tables[0].Rows[i]["Team"] = "Coaching";
                        }
                    }
                }
                if (ds.Tables[1] != null)
                {
                    ds.Tables[1].TableName = "Was Assigned Coach, but did not Signup Calendar";
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ds.Tables[1].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[1].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[1].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            ds.Tables[1].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[1].Rows[i]["EventID"] = 13;
                            ds.Tables[1].Rows[i]["Team"] = "Coaching";
                        }
                    }
                }
                if (ds.Tables[2] != null)
                {
                    ds.Tables[2].TableName = "Signed up Calendar";
                    if (ds.Tables[2].Rows.Count > 0)
                    {

                        ds.Tables[2].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[2].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[2].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                        {
                            ds.Tables[2].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[2].Rows[i]["EventID"] = 13;
                            ds.Tables[2].Rows[i]["Team"] = "Coaching";
                        }

                        int j = 0;
                        string PCode = string.Empty;
                        ds.Tables[2].Columns.Add("Products").SetOrdinal(13);
                        string MemberID = string.Empty;
                        for (int k = 0; k < dsPrd.Tables[0].Rows.Count; k++)
                        {



                            if (MemberID == dsPrd.Tables[0].Rows[k]["MemberID"].ToString())
                            {
                                PCode += ", " + dsPrd.Tables[0].Rows[k]["ProductCode"].ToString();
                            }
                            else
                            {
                                PCode = dsPrd.Tables[0].Rows[k]["ProductCode"].ToString();
                            }


                            if (MemberID == dsPrd.Tables[0].Rows[k]["MemberID"].ToString())
                            {
                                j--;

                            }




                            ds.Tables[2].Rows[j]["Products"] = PCode;
                            j++;
                            MemberID = dsPrd.Tables[0].Rows[k]["MemberID"].ToString();

                        }
                    }
                }
                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "NewCoaches_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ExportExcelReturningCoach()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();

            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from CalSignUp where EventYear<" + ddlYear.SelectedValue + " and B.AutoMemberID=MemberID and accepted='Y') and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by LastName, FirstName;";

            CmdText += "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from CalSignUp where EventYear<" + ddlYear.SelectedValue + " and B.AutoMemberID=MemberID and accepted='Y') and not exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID ) order by LastName, FirstName;";

            string cmdText = "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.MemberID in(select B.AutoMemberID from IndSpouse B where exists (select * from CalSignUp where EventYear<" + ddlYear.SelectedValue + " and B.AutoMemberID=MemberID and accepted='Y') and exists (select * from CalSignUp where EventYear=" + ddlYear.SelectedValue + " and b.AutoMemberID=MemberID )) order by IP.LastName,IP.Firstname ";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            DataSet dsPrd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null)
                {
                    ds.Tables[0].TableName = "Signed up Calendar";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[0].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[0].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ds.Tables[0].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[0].Rows[i]["EventID"] = 13;
                            ds.Tables[0].Rows[i]["Team"] = "Coaching";
                        }

                        int j = 0;
                        string PCode = string.Empty;
                        ds.Tables[0].Columns.Add("Products").SetOrdinal(13);
                        string MemberID = string.Empty;
                        for (int i = 0; i < dsPrd.Tables[0].Rows.Count; i++)
                        {



                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                PCode += ", " + dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                            }
                            else
                            {
                                PCode = dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                            }


                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                j--;

                            }




                            ds.Tables[0].Rows[j]["Products"] = PCode;
                            j++;
                            MemberID = dsPrd.Tables[0].Rows[i]["MemberID"].ToString();

                        }
                    }
                }
                if (ds.Tables[1] != null)
                {
                    ds.Tables[1].TableName = "Did not sign up Calendar";
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ds.Tables[1].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[1].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[1].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            ds.Tables[1].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[1].Rows[i]["EventID"] = 13;
                            ds.Tables[1].Rows[i]["Team"] = "Coaching";
                        }
                    }
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ReturningCoaches_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void BtnExpo_Click(object sender, EventArgs e)
    {

        if (ddlCoachType.SelectedValue == "New Coaches")
        {
            ExportToExcelNewCoach();
        }
        else if (ddlCoachType.SelectedValue == "Returning Coaches")
        {
            ExportExcelReturningCoach();

        }
        else if (ddlCoachType.SelectedValue == "Approve Calendar Signups")
        {
            ExportExcelApproveCalendarSignup();
        }
        else if (ddlCoachType.SelectedValue == "Approved Multiple Sessions")
        {
            ExportToExcelApproveMultipleSessions();
        }
        else if (ddlCoachType.SelectedValue == "Multiple Sessions/Products")
        {
            ExportToExcelMultipleProductsSessionsCoach();
        }
    }

    public string validateCoaches()
    {
        string RetVal = "1";
        if (ddlYear.SelectedValue == "0")
        {
            RetVal = "-1";
            lblerr.Text = "Please select Year";
        }
        else if (ddlCoachType.SelectedValue == "0")
        {
            RetVal = "-1";
            lblerr.Text = "Please select Type Of Coaches";
        }
        return RetVal;
    }

    protected void GrdNotAssignedCoach_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdNotAssignedCoach.PageIndex = e.NewPageIndex;
            LoadCoachNotAssignedCaoch();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    protected void GrdAssignedCoach_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdAssignedCoach.PageIndex = e.NewPageIndex;
            LoadCoachAssignedCaoch();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }

    protected void GrdSignedUpCalendar_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdSignedUpCalendar.PageIndex = e.NewPageIndex;
            LoadCalendarSignedUpCoach();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    protected void GrdReturningSignedUpCal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdReturningSignedUpCal.PageIndex = e.NewPageIndex;
            LoadReturningCalendarSignedUpCoach();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    protected void GrdReturningNotSignedupCal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GrdReturningNotSignedupCal.PageIndex = e.NewPageIndex;
            LoadReturningNotCalendarSignedUpCoach();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }

    protected void gvNothingAccepted_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvNothingAccepted.PageIndex = e.NewPageIndex;
            LoadCoachNotAcceptedList();
        }
        catch (Exception ex) { }
    }

    protected void GrdCoachListNotYetAccepted_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GrdCoachListNotYetAccepted.PageIndex = e.NewPageIndex;
            LoadCoachAcceptedNotAcceptedList();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    protected void grdMultiplePrdsAndSessionsCoaches_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {

            grdMultiplePrdsAndSessionsCoaches.PageIndex = e.NewPageIndex;
            LoadMultiplePrdsAndSessionsCoach();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    protected void GrdApprovedTwoSessions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdApprovedTwoSessions.PageIndex = e.NewPageIndex;
            LoadApprovedTwoSessions();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    protected void GrdApprovedThreeSessions_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdApprovedThreeSessions.PageIndex = e.NewPageIndex;
            LoadApprovedThreeMoreSessions();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }
    public void LoadCoachNotAcceptedList()
    {
        string CmdText = string.Empty;
        string Year = ddlYear.SelectedValue;

        CmdText = " select distinct CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter, ";
        CmdText += " Products=STUFF((SELECT distinct ', ' + productcode FROM calsignup WHERE  memberid=cs.memberid and eventyear=" + Year + "  FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') ";
        CmdText += " from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + " and 0=(Select count(*) from CalSignUp where EventYear=" + Year + " and Accepted='Y' and MemberId=cs.MemberID) group by CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter  order by IP.LastName, IP.FirstName ASC;";

        //CmdText += " select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where  CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where  Eventyear=" + Year + ") and CS.MemberID not in (select MemberID from CalSignUp where  Eventyear=" + Year + " and Accepted='Y' and ProductGroupID=CS.ProductGroupID and ProductID=CS.ProductID and Level=CS.Level and SessionNo=CS.SessionNo)  order by IP.LastName, IP.FirstName ASC;";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

        if (ds.Tables[0].Rows.Count > 0)
        {
            Span2.Visible = false;
            gvNothingAccepted.Visible = true;
            gvNothingAccepted.DataSource = ds;
            gvNothingAccepted.DataBind();
        }
        else
        {
            Span2.Visible = true;
            gvNothingAccepted.Visible = false;
        }
    }


    public void LoadCoachAcceptedNotAcceptedList()
    {
        string CmdText = string.Empty;
        string Year = ddlYear.SelectedValue;

        CmdText = " select distinct CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + " and CS.MemberID not in (Select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' and ProductGroupID=CS.ProductGroupID and ProductID=CS.ProductID and Level=CS.Level and SessionNo=CS.SessionNo) group by CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter  order by IP.LastName, IP.FirstName ASC;";

        CmdText += " select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where  CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where  Eventyear=" + Year + ") and CS.MemberID not in (select MemberID from CalSignUp where  Eventyear=" + Year + " and Accepted='Y' and ProductGroupID=CS.ProductGroupID and ProductID=CS.ProductID and Level=CS.Level and SessionNo=CS.SessionNo)  order by IP.LastName, IP.FirstName ASC;";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                BtnExpo.Visible = true;
                int j = 0;
                string PCode = string.Empty;
                ds.Tables[0].Columns.Add("Products").SetOrdinal(10);
                string MemberID = string.Empty;
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {



                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        PCode += ", " + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                    }
                    else
                    {
                        PCode = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                    }


                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        j--;

                    }




                    ds.Tables[0].Rows[j]["Products"] = PCode;
                    j++;
                    MemberID = ds.Tables[1].Rows[i]["MemberID"].ToString();

                }

                Span1.Visible = false;
                GrdCoachListNotYetAccepted.DataSource = ds;
                GrdCoachListNotYetAccepted.DataBind();
            }
            else
            {
                Span1.Visible = true;
                GrdCoachListNotYetAccepted.DataSource = ds;
                GrdCoachListNotYetAccepted.DataBind();
            }
        }
    }

    public void ExportExcelApproveCalendarSignup()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            string Year = ddlYear.SelectedValue;
            CmdText = "select distinct CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + " and CS.MemberID not in (Select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' and ProductGroupID=CS.ProductGroupID and ProductID=CS.ProductID and Level=CS.Level and SessionNo=CS.SessionNo) group by CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter  order by IP.LastName, IP.FirstName ASC; ";

            if (dvMemberInfo.Visible == true)
            {

                CmdText += "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Phase as PhaseEx,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " AND C.EventID=13  and C.MemberID=" + hdnMemberID.Value + "  group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD";
            }

            string cmdText = " select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where  CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where  Eventyear=" + Year + ") and CS.MemberID not in (select MemberID from CalSignUp where  Eventyear=" + Year + " and Accepted='Y' and ProductGroupID=CS.ProductGroupID and ProductID=CS.ProductID and Level=CS.Level and SessionNo=CS.SessionNo)  order by IP.LastName, IP.FirstName ASC;";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            DataSet dsPrd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null)
                {
                    ds.Tables[0].TableName = "Coach List - Not Yet Accepted";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[0].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[0].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ds.Tables[0].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[0].Rows[i]["EventID"] = 13;
                            ds.Tables[0].Rows[i]["Team"] = "Coaching";
                        }

                        int j = 0;
                        string PCode = string.Empty;
                        ds.Tables[0].Columns.Add("Products").SetOrdinal(13);
                        string MemberID = string.Empty;
                        for (int i = 0; i < dsPrd.Tables[0].Rows.Count; i++)
                        {



                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                PCode += ", " + dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                            }
                            else
                            {
                                PCode = dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                            }


                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                j--;

                            }




                            ds.Tables[0].Rows[j]["Products"] = PCode;
                            j++;
                            MemberID = dsPrd.Tables[0].Rows[i]["MemberID"].ToString();

                        }
                    }
                }
                if (dvMemberInfo.Visible == true)
                {
                    if (ds.Tables[1] != null)
                    {
                        ds.Tables[1].TableName = "Calendar Signups for Acceptance";
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            ds.Tables[1].Columns.Add("Ser#").SetOrdinal(0);

                            ds.Tables[1].Columns.Add("Team").SetOrdinal(5);
                            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                            {
                                ds.Tables[1].Rows[i]["Ser#"] = i + 1;

                                ds.Tables[1].Rows[i]["Team"] = "Coaching";
                            }
                        }
                    }
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ApproveCoaches_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void gvNothingAccepted_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                hdnFromVal.Value = "ApproveCalendarSignups";

                GridViewRow row = null;
                gvNothingAccepted.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                gvNothingAccepted.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string MemberID = string.Empty;
                MemberID = ((Label)gvNothingAccepted.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnMemberID.Value = MemberID;
                GrdCoachSignUp.EditIndex = -1;
                LoadCoachCalendarSignup(MemberID);
                spnMemberTitle.InnerText = "Table 2: Calendar Signups for Acceptance";
                btnColseTable2.Text = "Close Table 2";
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void GrdCoachListNotYetAccepted_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                hdnFromVal.Value = "ApproveCalendarSignups";

                GridViewRow row = null;
                GrdCoachListNotYetAccepted.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdCoachListNotYetAccepted.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string MemberID = string.Empty;
                MemberID = ((Label)GrdCoachListNotYetAccepted.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnMemberID.Value = MemberID;
                GrdCoachSignUp.EditIndex = -1;
                LoadCoachCalendarSignup(MemberID);
                spnMemberTitle.InnerText = "Table 2: Calendar Signups for Acceptance";
                btnColseTable2.Text = "Close Table 2";
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void grdMultiplePrdsAndSessionsCoaches_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                hdnFromVal.Value = "MultipleProducts";

                GridViewRow row = null;
                grdMultiplePrdsAndSessionsCoaches.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                grdMultiplePrdsAndSessionsCoaches.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string MemberID = string.Empty;
                MemberID = ((Label)grdMultiplePrdsAndSessionsCoaches.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnMemberID.Value = MemberID;
                GrdCoachSignUp.EditIndex = -1;
                LoadCoachCalendarSignup(MemberID);
                spnMemberTitle.InnerText = "Table 2: Calendar Signups for Acceptance";
                btnColseTable2.Text = "Close Table 2";
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void LoadCoachCalendarSignup(string MemberID)
    {
        try
        {
            int CurrentYear = DateTime.Now.Year;
            string Year = ddlYear.SelectedValue;
            string cmdText = string.Empty;
            DataSet ds = new DataSet();
            string FromVal = string.Empty;
            FromVal = hdnFromVal.Value;

            if (FromVal == "ApproveCalendarSignups" || FromVal == "MultipleProducts")
            {
                cmdText = "SELECT C.SignUpID,vl.hostid,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester as PhaseEx, C.Semester as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, case when C.[Level]='Beginner' then '1.Beginner' when C.[Level]='Intermediate' then '2.Intermediate' when C.[Level]='Advanced' then '3.Advanced' when C.[Level]='Junior' then '1.Junior' when C.[Level]='Senior' then '2.Senior' when C.[Level]='One Level' then '1.One Level' else C.[Level] end as [Level], C.[Level] as SLevel,C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day, C.MeetingKey , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID inner join virtualRoomLookup VL on (VL.Vroom=c.Vroom) where C.EventYear=" + Year + " AND C.EventID=13  and C.MemberID=" + MemberID + "  group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD, C.MeetingKey, vl.HostId order by C.EventYear, C.MemberID, C.Semester, C.ProductGroupID, C.ProductID, [Level], C.SessionNo, C.MeetingKey,vl.HostId";

            }
            else if (FromVal == "ApproveTwoSessions")
            {
                cmdText = "SELECT C.SignUpID,vl.hostid,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Phase as PhaseEx, C.Semester  as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, case when C.[Level]='Beginner' then '1.Beginner' when C.[Level]='Intermediate' then '2.Intermediate' when C.[Level]='Advanced' then '3.Advanced' when C.[Level]='Junior' then '1.Junior' when C.[Level]='Senior' then '2.Senior' when C.[Level]='One Level' then '1.One Level' else C.[Level] end as [Level], C.[Level] as SLevel,C.SessionNo,C.MeetingKey, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID inner join virtualRoomLookup VL on (VL.Vroom=c.Vroom) where C.EventYear=" + Year + " AND C.EventID=13  and C.MemberID=" + MemberID + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD, C.MeetingKey, vl.HostId order by C.EventYear, C.MemberID, C.Semester, C.ProductGroupID, C.ProductID, [Level], C.SessionNo, C.MeetingKey, vl.HostId";
            }
            else if (FromVal == "ApproveThreeSessions")
            {
                cmdText = "SELECT C.SignUpID,c.MeetingKey,vl.hostid,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester as PhaseEx, C.Semester as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, case when C.[Level]='Beginner' then '1.Beginner' when C.[Level]='Intermediate' then '2.Intermediate' when C.[Level]='Advanced' then '3.Advanced' when C.[Level]='Junior' then '1.Junior' when C.[Level]='Senior' then '2.Senior' when C.[Level]='One Level' then '1.One Level' else C.[Level] end as [Level], C.[Level] as SLevel,C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom,C.MeetingKey, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID inner join virtualRoomLookup VL on (VL.Vroom=c.Vroom) where C.EventYear=" + Year + " AND C.EventID=13  and C.MemberID=" + MemberID + " and C.Accepted='Y'  group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD, C.MeetingKey, vl.HostId order by C.EventYear,C.MeetingKey, C.MemberID, C.Semester, C.ProductGroupID, C.ProductID, [Level], C.SessionNo, vl.HostId";
            }

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    btnColseTable2.Visible = true;
                    dvCalSignUp4AppThreeSess.Visible = false;
                    BtnCloaseTable4.Visible = false;
                    GrdCoachSignUp.DataSource = ds;
                    GrdCoachSignUp.DataBind();
                    dvMemberInfo.Visible = true;

                    if (FromVal == "MultipleProducts")
                    {
                        int rowCount = GrdCoachSignUp.Rows.Count;
                        string accepted = "";
                        for (int i = 0; i < rowCount; i++)
                        {
                            accepted = ((Label)GrdCoachSignUp.Rows[i].FindControl("lblHdnAccepted") as Label).Text;
                            if (accepted == "Y")
                            {
                                GrdCoachSignUp.Rows[i].Style.Add("background-color", "#58d68d");
                            }

                        }

                        dvColorStatus.Visible = true;
                    }
                    else
                    {
                        dvColorStatus.Visible = false;
                    }

                }
                else
                {
                    GrdCoachSignUp.DataSource = ds;
                    GrdCoachSignUp.DataBind();
                    dvCalSignUp4AppThreeSess.Visible = false;
                    BtnCloaseTable4.Visible = false;

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdCoachSignUp_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GrdCoachSignUp.EditIndex = e.NewEditIndex;
        int rowIndex = e.NewEditIndex;
        LoadCoachCalendarSignup(hdnMemberID.Value);
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = true;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = true;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = false;

    }
    protected void GrdCoachSignUp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdCoachSignUp.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        GrdCoachSignUp.EditIndex = -1;
        LoadCoachCalendarSignup(hdnMemberID.Value);
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = false;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = false;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = true;

    }


    protected void GrdCoachSignUp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int rowIndex = e.RowIndex;
            string ProductGroup = string.Empty;
            string Day = string.Empty;
            string Year = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlEventYear") as DropDownList).SelectedValue;
            Day = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlDay") as DropDownList).SelectedValue;

            ProductGroup = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblPgCode") as Label).Text;
            string ProductGroupID = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblProductGroupID") as Label).Text;
            string EventID = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEventID") as Label).Text;
            string MemberID = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlVolunteerList") as DropDownList).SelectedValue;
            string ProductID = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlProduct") as DropDownList).SelectedValue;
            string ProductCode = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlProduct") as DropDownList).SelectedItem.Text;
            string Phase = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblHdnPhase") as Label).Text;
            string Level = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlLevel") as DropDownList).SelectedValue;
            string SessionNo = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlSessionNo") as DropDownList).SelectedValue;
            string Time = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlTime") as DropDownList).SelectedValue;
            string Accepted = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlAccepted") as DropDownList).SelectedValue;
            string Preference = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlPreference") as DropDownList).SelectedValue;
            string MaxCap = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlMaxCap") as DropDownList).SelectedValue;
            string VRoom = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlVRoom") as DropDownList).SelectedValue;
            string UserID = ((TextBox)GrdCoachSignUp.Rows[rowIndex].FindControl("txtUserID") as TextBox).Text;
            string PWD = ((TextBox)GrdCoachSignUp.Rows[rowIndex].FindControl("txtPWD") as TextBox).Text;
            string SignUpID = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblSignUpID") as Label).Text;
            string MeetingKey = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblmeetingKey") as Label).Text;
            string HostId = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblhostId") as Label).Text;
            string HostName = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblCoachName") as Label).Text;
            string sLevel = string.Empty;
            sLevel = (Level == "" ? "is null" : "'" + Level + "'");



            string cmdText = string.Empty;
            cmdText = "select count(*) as CountSet from CalSignUp where Memberid=" + MemberID + " and Eventyear=" + Year + " and EventID=" + EventID + " and Day='" + Day + "' and Time='" + Time + "' and  SignUpID not in (" + SignUpID + ")";
            int count = 0;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            int countset = 0;
            cmdText = "select count(*) as CountSet from CalSignUp where Memberid=" + MemberID + " and eventyear=" + Year + " and Semester='" + Phase + "' and ProductID=" + ProductID + " and Level =" + sLevel + " AND SessionNo=" + SessionNo + " and Accepted='Y' and  SignUpID not in (" + SignUpID + ")";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    countset = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            if (count > 0)
            {
                lblerr.Text = "Coach is already scheduled for this Period.Please modify the Day or Time.";
            }
            else if (countset < 1)
            {
                Level = (Level == "" ? "is null" : "'" + Level + "'");
                string sSessionNo = (SessionNo == "" ? "null" : "'" + SessionNo + "'");
                string sAccepted = (Accepted == "" ? "null" : "'" + Accepted + "'");
                string sUserID = (UserID == "" ? "null" : "'" + UserID + "'");
                string sPWD = (PWD == "" ? "null" : "'" + PWD + "'");
                string sVRoom = (VRoom == "0" ? "null" : "'" + VRoom + "'");
                cmdText = "UPDATE CalSignUp SET MemberID = " + MemberID + ",EventYear = " + Year + ",ProductID = " + ProductID + ",ProductCode = '" + ProductCode + "',[Level] =" + Level + ",SessionNo=" + sSessionNo + ",Day='" + Day + "',Time = '" + Time + "',Accepted=" + sAccepted + ",Preference=" + Preference + ",MaxCapacity =" + MaxCap + ",UserID=" + sUserID + ",PWD=" + sPWD + ",Vroom=" + VRoom + ",ModifiedDate = Getdate(),ModifiedBy = " + Session["loginID"].ToString() + " WHERE SignUpID=" + SignUpID + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                LoadCoachCalendarSignup(hdnMemberID.Value);
                GrdCoachSignUp.EditIndex = -1;
                LoadCoachCalendarSignup(hdnMemberID.Value);
                ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = false;
                ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = false;
                ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = true;


                lblerr.Text = "Updated Successfully";

                deleteMeeting(HostId, MeetingKey);
                hdnSignupId.Value = SignUpID;
                createZoomMeeting(HostId, HostName + "-" + ProductCode + "-" + Level + "-" + SessionNo);

            }
            else
            {
                lblerr.Text = "Coach with same product, Phase, Level and Session are already found.";


            }
        }
        catch (Exception ex)
        {
        }


    }
    protected void GrdCoachSignUp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Edit")
        //{
        //    retFlag = "Edit";
        //}
        //else if (e.CommandName == "Update")
        //{
        //    retFlag = "Update";
        //}
        //else
        //{
        //    retFlag = "Cancel";
        //}
    }

    protected void GrdCoachSignUp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && GrdCoachSignUp.EditIndex == e.Row.RowIndex)
            {
                DropDownList ddlLevel = ((DropDownList)e.Row.FindControl("ddlLevel") as DropDownList);
                string ProductGroup = string.Empty;
                string Day = string.Empty;
                string Year = ((Label)e.Row.FindControl("lblHdnEventYear") as Label).Text;
                Day = ((Label)e.Row.FindControl("lblHdnDay") as Label).Text;
                ProductGroup = ((Label)e.Row.FindControl("lblPgCode") as Label).Text;
                string ProductGroupID = ((Label)e.Row.FindControl("lblProductGroupID") as Label).Text;
                string EventID = ((Label)e.Row.FindControl("lblEventID") as Label).Text;
                string MemberID = ((Label)e.Row.FindControl("lblhdnAutoMemberID") as Label).Text;
                string ProductID = ((Label)e.Row.FindControl("lblProductID") as Label).Text;
                string Phase = ((Label)e.Row.FindControl("lblHdnPhase") as Label).Text;
                string Level = ((Label)e.Row.FindControl("lblHdnLevel") as Label).Text;
                string SessionNo = ((Label)e.Row.FindControl("lblHdnSession") as Label).Text;
                string Time = ((Label)e.Row.FindControl("lblHdnTime") as Label).Text;
                string Accepted = ((Label)e.Row.FindControl("lblHdnAccepted") as Label).Text;
                string Preference = ((Label)e.Row.FindControl("lblHdnPreference") as Label).Text;
                string MaxCap = ((Label)e.Row.FindControl("lblHdnMaxCap") as Label).Text;
                string VRoom = ((Label)e.Row.FindControl("lblHdnVRoom") as Label).Text;
                string UserID = ((Label)e.Row.FindControl("lblHdnUserID") as Label).Text;
                string PWD = ((Label)e.Row.FindControl("lblHdnPWD") as Label).Text;

                DropDownList ddlAccepted = (DropDownList)e.Row.FindControl("ddlAccepted");
                DropDownList ddlDay = (DropDownList)e.Row.FindControl("ddlDay");

                TextBox txtUserID = (TextBox)e.Row.FindControl("txtUserID");
                TextBox txtPWD = (TextBox)e.Row.FindControl("txtPWD");
                if (UserID != "")
                {
                    txtUserID.Text = UserID;
                }
                if (PWD != "")
                {
                    txtPWD.Text = PWD;
                }

                if (Accepted != "")
                {
                    ddlAccepted.SelectedValue = Accepted;
                }
                if (Day != "")
                {
                    ddlDay.SelectedValue = Day;
                }

                LoadLevel(ddlLevel, ProductGroupID, Year, EventID, ProductID);
                if (Level != "")
                {
                    ddlLevel.SelectedValue = Level;
                }

                DropDownList ddlMaxCap = (DropDownList)e.Row.FindControl("ddlMaxCap");
                LoadCapacity(ddlMaxCap);
                if (MaxCap != "")
                {
                    ddlMaxCap.SelectedValue = MaxCap;
                }

                DropDownList ddlVRoom = (DropDownList)e.Row.FindControl("ddlVRoom");
                LoadVRoom(ddlVRoom, Time, Day, Year, 120);
                if (VRoom != "")
                {
                    ddlVRoom.SelectedValue = VRoom;
                }

                DropDownList ddlYear = (DropDownList)e.Row.FindControl("ddlEventYear");
                loadYear(ddlYear);
                if (Year != "")
                {
                    ddlYear.SelectedValue = Year;
                }

                DropDownList ddlTime = (DropDownList)e.Row.FindControl("ddlTime");
                if (Day == "Saturday" || Day == "Sunday")
                {
                    LoadDisplayTime(ddlTime);
                }
                else
                {
                    LoadWeekDisplayTime(ddlTime);
                }
                if (Time != "")
                {
                    ddlTime.SelectedValue = Time;
                }

                DropDownList ddlVolunteer = (DropDownList)e.Row.FindControl("ddlVolunteerList");
                loadVolunteer(ddlVolunteer);
                if (MemberID != "")
                {
                    ddlVolunteer.SelectedValue = MemberID;
                }

                DropDownList ddlProduct = (DropDownList)e.Row.FindControl("ddlProduct");
                LoadProduct(ddlProduct, Year, EventID, ProductGroupID);
                if (ProductID != "")
                {
                    ddlProduct.SelectedValue = ProductID;
                }

            }
        }
        catch (Exception ex)
        {

        }
    }

    private void LoadLevel(DropDownList ddlObject, string ProductGroup, string eventYear, string eventID, string ProductID)
    {
        try
        {

            ddlObject.Items.Clear();
            ddlObject.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" + eventYear + " and ProductGroupID=" + ProductGroup + " and ProductID=" + ProductID + " and EventID=" + eventID + "";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlObject.DataValueField = "LevelCode";
            ddlObject.DataTextField = "LevelCode";

            ddlObject.DataSource = ds;
            ddlObject.DataBind();
        }
        catch
        {
        }

    }

    private void LoadCapacity(DropDownList ddltemp)
    {
        ddltemp.Items.Clear();
        ListItem li = default(ListItem);
        int i = 0;
        //10 To 100 Step 5
        for (i = 0; i <= 30; i += 1)
        {
            li = new ListItem(i.ToString());
            ddltemp.Items.Add(li);
        }

    }
    public void LoadVRoom(DropDownList ddltemp, string Time, string Day, string Year, int Duration)
    {
        ddltemp.Items.Clear();
        //ListItem li = default(ListItem);
        //int i = 0;
        //for (i = 1; i <= 100; i++)
        //{
        //    li = new ListItem(i.ToString());
        //    ddltemp.Items.Add(li);
        //}


        string cmdtext = string.Empty;
        DataSet ds = new DataSet();
        string hostID = string.Empty;

        string strStartTime = Time;
        string strStartDay = Day;
        Duration = Duration + 30;
        string strEndTime = Convert.ToDateTime(strStartTime).ToString();
        strStartTime = Convert.ToDateTime(strStartTime).ToString();

        DateTime dtStarttime = new DateTime();
        DateTime dtEndTime = new DateTime();
        DateTime dtPrStartTime = Convert.ToDateTime(strStartTime).AddMinutes(-30);
        DateTime dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);


        dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);


        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd, CS.[Begin], CS.[End], CS.Day, cs.Productgroupcode, cs.Time from CalSignup CS  inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=" + Year + " and cs.Accepted='Y' order by Time";

        Double iDurationBasedOnPG = 0;


        try
        {
            string vRooms = string.Empty;


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        try
                        {
                            dtStarttime = Convert.ToDateTime(dr["Begin"].ToString());
                            if (dr["ProductGroupCode"].ToString() == "COMP")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "GB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "LSP")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "MB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SAT")
                            {
                                iDurationBasedOnPG = 1.5;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SC")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "UV")
                            {
                                iDurationBasedOnPG = 1;
                            }

                            dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);

                            string Meetingday = dr["Day"].ToString();
                            string meetVroom = dr["Vroom"].ToString();




                            if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                            {


                                vRooms += dr["VRoom"].ToString() + ",";
                            }
                        }
                        catch
                        {
                        }


                    }
                }
            }



            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Day,VC.BeginTime,VC.EndTime, VC.StartDate, VC.ProductGroupCode from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=" + Year + " and (SessionType='Practice' or SessionType='Scheduled Meeting' or SessionType='Extra') and StartDate>getDate() order by VC.BeginTime";
            try
            {
                DataSet dsSingle = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
                if (dsSingle.Tables.Count > 0)
                {
                    if (dsSingle.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSingle.Tables[0].Rows)
                        {
                            try
                            {
                                dtStarttime = Convert.ToDateTime(dr["BeginTime"].ToString());
                                if (dr["ProductGroupCode"].ToString() == "COMP")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "GB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "LSP")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "MB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SAT")
                                {
                                    iDurationBasedOnPG = 1.5;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SC")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "UV")
                                {
                                    iDurationBasedOnPG = 1;
                                }

                                dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);

                                string Meetingday = dr["Day"].ToString();
                                string meetVroom = dr["Vroom"].ToString();

                                if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                                {

                                    vRooms += dr["VRoom"].ToString() + ",";
                                }


                            }
                            catch
                            {
                            }


                        }
                    }
                }
            }
            catch
            {
            }


            if (!string.IsNullOrEmpty(vRooms))
            {
                vRooms = vRooms.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(vRooms))
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp where Vroom not in (" + vRooms + ") order by vroom desc";
            }
            else
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp order by vroom desc";
            }

            DataSet dsVroom = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (dsVroom.Tables.Count > 0)
            {
                ddltemp.DataValueField = "vroom";
                ddltemp.DataTextField = "vroom";
                ddltemp.DataSource = dsVroom;
                ddltemp.DataBind();
                ddltemp.Items.Insert(0, new ListItem("Select", "0"));
            }

            // hostID = "";
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }

    }

    public void loadYear(DropDownList dropDown)
    {
        dropDown.Items.Clear();
        dropDown.Items.Insert(0, Convert.ToString(DateTime.Now.Year + 1));
        dropDown.Items.Insert(1, Convert.ToString(DateTime.Now.Year));
        dropDown.Items.Insert(2, Convert.ToString(DateTime.Now.Year - 1));

        dropDown.SelectedValue = Convert.ToString(DateTime.Now.Year);
    }

    public void LoadDisplayTime(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Insert(0, "Select Time");
        ddlObject.Items.Insert(1, "8:00AM");
        ddlObject.Items.Insert(2, "9:00AM");
        ddlObject.Items.Insert(3, "10:00AM");
        ddlObject.Items.Insert(4, "11:00AM");
        ddlObject.Items.Insert(5, "12:00PM");
        ddlObject.Items.Insert(6, "1:00PM");
        ddlObject.Items.Insert(7, "2:00PM");
        ddlObject.Items.Insert(8, "3:00PM");
        ddlObject.Items.Insert(9, "4:00PM");
        ddlObject.Items.Insert(10, "5:00PM");
        ddlObject.Items.Insert(11, "6:00PM");
        ddlObject.Items.Insert(12, "7:00PM");
        ddlObject.Items.Insert(13, "8:00PM");
        ddlObject.Items.Insert(14, "9:00PM");
        ddlObject.Items.Insert(15, "10:00PM");
        ddlObject.Items.Insert(16, "11:00PM");
        ddlObject.Items.Insert(17, "12:00AM");
    }

    public void LoadWeekDisplayTime(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Insert(0, "Select Time");
        ddlObject.Items.Insert(1, "6:00PM");
        ddlObject.Items.Insert(2, "7:00PM");
        ddlObject.Items.Insert(3, "8:00PM");
        ddlObject.Items.Insert(4, "9:00PM");
        ddlObject.Items.Insert(5, "10:00PM");
        ddlObject.Items.Insert(6, "11:00PM");
        ddlObject.Items.Insert(7, "12:00AM");
    }

    public void loadVolunteer(DropDownList ddlObject)
    {
        string CmdText = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlObject.DataValueField = "MemberID";
                ddlObject.DataTextField = "Name";
                ddlObject.DataSource = ds;
                ddlObject.DataBind();

                ddlObject.Items.Insert(0, "Select");
            }
            else
            {
                ddlObject.DataValueField = "MemberID";
                ddlObject.DataTextField = "Name";
                ddlObject.DataSource = ds;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, "Select");

            }
        }
    }

    public void LoadProduct(DropDownList dropdown, string Year, string EventID, string ProductGroupID)
    {
        string CmdText = "Select P.ProductID, P.Name,P.ProductCode from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" + Year + " AND P.EventID=" + EventID + " and P.ProductGroupID =" + ProductGroupID + "  order by P.ProductID ";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                dropdown.DataValueField = "ProductID";
                dropdown.DataTextField = "ProductCode";
                dropdown.DataSource = ds;
                dropdown.DataBind();

                dropdown.Items.Insert(0, "Select");
            }
            else
            {
                dropdown.DataValueField = "ProductID";
                dropdown.DataTextField = "ProductCode";
                dropdown.DataSource = ds;
                dropdown.DataBind();
                dropdown.Items.Insert(0, "Select");

            }
        }
    }
    protected void btnColseTable2_Click(object sender, EventArgs e)
    {
        dvMemberInfo.Visible = false;
        btnColseTable2.Visible = false;
    }
    protected void btnColseTable4_Click(object sender, EventArgs e)
    {

        dvCalSignUp4AppThreeSess.Visible = false;
        BtnCloaseTable4.Visible = false;
    }

    public void LoadApprovedTwoSessions()
    {
        string CmdText = string.Empty;
        string Year = ddlYear.SelectedValue;

        CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)=2) order by B.LastName, B.FirstName ASC;";

        CmdText += "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.Accepted='Y' and CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)=2) order by IP.LastName, IP.FirstName ASC";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                BtnExpo.Visible = true;
                int j = 0;
                string PCode = string.Empty;
                ds.Tables[0].Columns.Add("Products").SetOrdinal(10);
                string MemberID = string.Empty;
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {



                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        PCode += ", " + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                    }
                    else
                    {
                        PCode = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                    }


                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        j--;

                    }




                    ds.Tables[0].Rows[j]["Products"] = PCode;
                    j++;
                    MemberID = ds.Tables[1].Rows[i]["MemberID"].ToString();

                }


                spnTableAppTwo.Visible = false;
                GrdApprovedTwoSessions.DataSource = ds;
                GrdApprovedTwoSessions.DataBind();
            }
            else
            {
                spnTableAppTwo.Visible = true;
                GrdApprovedTwoSessions.DataSource = ds;
                GrdApprovedTwoSessions.DataBind();
            }
        }
    }

    public void LoadApprovedThreeMoreSessions()
    {
        string CmdText = string.Empty;
        string Year = ddlYear.SelectedValue;

        CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)>2) order by B.LastName, B.FirstName ASC;";

        CmdText += "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.Accepted='Y' and CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)>2) order by IP.LastName, IP.FirstName ASC";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                BtnExpo.Visible = true;

                int j = 0;
                string PCode = string.Empty;
                ds.Tables[0].Columns.Add("Products").SetOrdinal(10);
                string MemberID = string.Empty;
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {



                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        PCode += ", " + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                    }
                    else
                    {
                        PCode = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                    }


                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        j--;

                    }




                    ds.Tables[0].Rows[j]["Products"] = PCode;
                    j++;
                    MemberID = ds.Tables[1].Rows[i]["MemberID"].ToString();

                }
                spnTblAppThreeSess.Visible = false;
                GrdApprovedThreeSessions.DataSource = ds;
                GrdApprovedThreeSessions.DataBind();
            }
            else
            {
                spnTblAppThreeSess.Visible = true;
                GrdApprovedThreeSessions.DataSource = ds;
                GrdApprovedThreeSessions.DataBind();
            }
        }
    }


    protected void GrdApprovedTwoSessions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                hdnFromVal.Value = "ApproveTwoSessions";
                GridViewRow row = null;
                GrdApprovedTwoSessions.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdApprovedTwoSessions.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string MemberID = string.Empty;
                MemberID = ((Label)GrdApprovedTwoSessions.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnMemberID.Value = MemberID;
                GrdCoachSignUp.EditIndex = -1;
                LoadCoachCalendarSignup(MemberID);
                spnMemberTitle.InnerText = "Table 3:  Calendar Signups for Approved Two Sessions";
                btnColseTable2.Text = "Close Table 3";
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdApprovedThreeSessions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                hdnFromVal.Value = "ApproveThreeSessions";
                GridViewRow row = null;
                GrdApprovedThreeSessions.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdApprovedThreeSessions.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string MemberID = string.Empty;
                MemberID = ((Label)GrdApprovedThreeSessions.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnMemberID.Value = MemberID;
                GrdCoachSignUp.EditIndex = -1;

                LoadCalSignup4AppThreeSess(MemberID);
                spnMemberTitle.InnerText = "Table 4:  Approved Three or Sessions";
                btnColseTable2.Text = "Close Table 4";
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void CalSignUpRowUpdating(GridView GrdDynamic, int rowIndex)
    {
        string ProductGroup = string.Empty;
        string Day = string.Empty;
        string Year = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlEventYear") as DropDownList).SelectedValue;
        Day = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlDay") as DropDownList).SelectedValue;
        ProductGroup = ((Label)GrdDynamic.Rows[rowIndex].FindControl("lblPgCode") as Label).Text;
        string ProductGroupID = ((Label)GrdDynamic.Rows[rowIndex].FindControl("lblProductGroupID") as Label).Text;
        string EventID = ((Label)GrdDynamic.Rows[rowIndex].FindControl("lblEventID") as Label).Text;
        string MemberID = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlVolunteerList") as DropDownList).SelectedValue;
        string ProductID = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlProduct") as DropDownList).SelectedValue;
        string ProductCode = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlProduct") as DropDownList).SelectedItem.Text;
        string Phase = ((Label)GrdDynamic.Rows[rowIndex].FindControl("lblHdnPhase") as Label).Text;
        string Level = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlLevel") as DropDownList).SelectedValue;
        string SessionNo = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlSessionNo") as DropDownList).SelectedValue;
        string Time = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlTime") as DropDownList).SelectedValue;
        string Accepted = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlAccepted") as DropDownList).SelectedValue;
        string Preference = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlPreference") as DropDownList).SelectedValue;
        string MaxCap = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlMaxCap") as DropDownList).SelectedValue;
        string VRoom = ((DropDownList)GrdDynamic.Rows[rowIndex].FindControl("ddlVRoom") as DropDownList).SelectedValue;
        string UserID = ((TextBox)GrdDynamic.Rows[rowIndex].FindControl("txtUserID") as TextBox).Text;
        string PWD = ((TextBox)GrdDynamic.Rows[rowIndex].FindControl("txtPWD") as TextBox).Text;
        string SignUpID = ((Label)GrdDynamic.Rows[rowIndex].FindControl("lblSignUpID") as Label).Text;
        string sLevel = string.Empty;
        sLevel = (Level == "" ? "is null" : "'" + Level + "'");



        string cmdText = string.Empty;
        cmdText = "select count(*) as CountSet from CalSignUp where Memberid=" + MemberID + " and Eventyear=" + Year + " and EventID=" + EventID + " and Day='" + Day + "' and Time='" + Time + "' and  SignUpID not in (" + SignUpID + ")";
        int count = 0;
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
            }
        }
        int countset = 0;
        cmdText = "select count(*) as CountSet from CalSignUp where Memberid=" + MemberID + " and eventyear=" + Year + " and Phase=" + Phase + " and ProductID=" + ProductID + " and Level =" + sLevel + " AND SessionNo=" + SessionNo + " and  SignUpID not in (" + SignUpID + ")";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                countset = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
            }
        }
        if (count > 0)
        {
            LblSuccess.Text = "Coach is already scheduled for this Period.Please modify the Day or Time.";
        }
        else if (countset < 1)
        {
            Level = (Level == "" ? "is null" : "'" + Level + "'");
            string sSessionNo = (SessionNo == "" ? "null" : "'" + SessionNo + "'");
            string sAccepted = (Accepted == "" ? "null" : "'" + Accepted + "'");
            string sUserID = (UserID == "" ? "null" : "'" + UserID + "'");
            string sPWD = (PWD == "" ? "null" : "'" + PWD + "'");
            string sVRoom = (VRoom == "0" ? "null" : "'" + VRoom + "'");
            cmdText = "UPDATE CalSignUp SET MemberID = " + MemberID + ",EventYear = " + Year + ",ProductID = " + ProductID + ",ProductCode = '" + ProductCode + "',[Level] =" + Level + ",SessionNo=" + sSessionNo + ",Day='" + Day + "',Time = '" + Time + "',Accepted=" + sAccepted + ",Preference=" + Preference + ",MaxCapacity =" + MaxCap + ",UserID=" + sUserID + ",PWD=" + sPWD + ",Vroom=" + VRoom + ",ModifiedDate = Getdate(),ModifiedBy = " + Session["loginID"].ToString() + " WHERE SignUpID=" + SignUpID + "";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


            LoadCalSignup4AppThreeSess(hdnMemberID.Value);
            GrdDynamic.EditIndex = -1;
            LoadCalSignup4AppThreeSess(hdnMemberID.Value);
            ((LinkButton)GrdDynamic.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = false;
            ((LinkButton)GrdDynamic.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = false;
            ((LinkButton)GrdDynamic.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = true;


            LblSuccess.Text = "Updated Successfully";

        }
        else
        {
            LblSuccess.Text = "Coach with same product, Phase, Level and Session are already found.";


        }
    }

    public void CalSignUpRowDataBound(GridViewRowEventArgs e, GridView GrdDynamic)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && GrdDynamic.EditIndex == e.Row.RowIndex)
        {
            DropDownList ddlLevel = ((DropDownList)e.Row.FindControl("ddlLevel") as DropDownList);
            string ProductGroup = string.Empty;
            string Day = string.Empty;
            string Year = ((Label)e.Row.FindControl("lblHdnEventYear") as Label).Text;
            Day = ((Label)e.Row.FindControl("lblHdnDay") as Label).Text;
            ProductGroup = ((Label)e.Row.FindControl("lblPgCode") as Label).Text;
            string ProductGroupID = ((Label)e.Row.FindControl("lblProductGroupID") as Label).Text;
            string EventID = ((Label)e.Row.FindControl("lblEventID") as Label).Text;
            string MemberID = ((Label)e.Row.FindControl("lblhdnAutoMemberID") as Label).Text;
            string ProductID = ((Label)e.Row.FindControl("lblProductID") as Label).Text;
            string Phase = ((Label)e.Row.FindControl("lblHdnPhase") as Label).Text;
            string Level = ((Label)e.Row.FindControl("lblHdnLevel") as Label).Text;
            string SessionNo = ((Label)e.Row.FindControl("lblHdnSession") as Label).Text;
            string Time = ((Label)e.Row.FindControl("lblHdnTime") as Label).Text;
            string Accepted = ((Label)e.Row.FindControl("lblHdnAccepted") as Label).Text;
            string Preference = ((Label)e.Row.FindControl("lblHdnPreference") as Label).Text;
            string MaxCap = ((Label)e.Row.FindControl("lblHdnMaxCap") as Label).Text;
            string VRoom = ((Label)e.Row.FindControl("lblHdnVRoom") as Label).Text;
            string UserID = ((Label)e.Row.FindControl("lblHdnUserID") as Label).Text;
            string PWD = ((Label)e.Row.FindControl("lblHdnPWD") as Label).Text;

            DropDownList ddlAccepted = (DropDownList)e.Row.FindControl("ddlAccepted");
            DropDownList ddlDay = (DropDownList)e.Row.FindControl("ddlDay");
            TextBox txtUserID = (TextBox)e.Row.FindControl("txtUserID");
            TextBox txtPWD = (TextBox)e.Row.FindControl("txtPWD");
            if (UserID != "")
            {
                txtUserID.Text = UserID;
            }
            if (PWD != "")
            {
                txtPWD.Text = PWD;
            }

            if (Accepted != "")
            {
                ddlAccepted.SelectedValue = Accepted;
            }
            if (Day != "")
            {
                ddlDay.SelectedValue = Day;
            }

            LoadLevel(ddlLevel, ProductGroupID, Year, EventID, ProductID);
            if (Level != "")
            {
                ddlLevel.SelectedValue = Level;
            }

            DropDownList ddlMaxCap = (DropDownList)e.Row.FindControl("ddlMaxCap");
            LoadCapacity(ddlMaxCap);
            if (MaxCap != "")
            {
                ddlMaxCap.SelectedValue = MaxCap;
            }

            DropDownList ddlVRoom = (DropDownList)e.Row.FindControl("ddlVRoom");
            LoadVRoom(ddlVRoom, Time, Day, Year, 120);
            if (VRoom != "")
            {
                ddlVRoom.SelectedValue = VRoom;
            }

            DropDownList ddlYear = (DropDownList)e.Row.FindControl("ddlEventYear");
            loadYear(ddlYear);
            if (Year != "")
            {
                ddlYear.SelectedValue = Year;
            }

            DropDownList ddlTime = (DropDownList)e.Row.FindControl("ddlTime");
            if (Day == "Saturday" || Day == "Sunday")
            {
                LoadDisplayTime(ddlTime);
            }
            else
            {
                LoadWeekDisplayTime(ddlTime);
            }
            if (Time != "")
            {
                ddlTime.SelectedValue = Time;
            }

            DropDownList ddlVolunteer = (DropDownList)e.Row.FindControl("ddlVolunteerList");
            loadVolunteer(ddlVolunteer);
            if (MemberID != "")
            {
                ddlVolunteer.SelectedValue = MemberID;
            }

            DropDownList ddlProduct = (DropDownList)e.Row.FindControl("ddlProduct");
            LoadProduct(ddlProduct, Year, EventID, ProductGroupID);
            if (ProductID != "")
            {
                ddlProduct.SelectedValue = ProductID;
            }

        }
    }


    protected void GrdCalSignUp4AppThreeSess_RowEditing(object sender, GridViewEditEventArgs e)
    {


        GrdCalSignUp4AppThreeSess.EditIndex = e.NewEditIndex;
        int rowIndex = e.NewEditIndex;
        LoadCalSignup4AppThreeSess(hdnMemberID.Value);
        ((LinkButton)GrdCalSignUp4AppThreeSess.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = true;
        ((LinkButton)GrdCalSignUp4AppThreeSess.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = true;
        ((LinkButton)GrdCalSignUp4AppThreeSess.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = false;

    }
    protected void GrdCalSignUp4AppThreeSess_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdCalSignUp4AppThreeSess.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        GrdCalSignUp4AppThreeSess.EditIndex = -1;
        LoadCalSignup4AppThreeSess(hdnMemberID.Value);
        ((LinkButton)GrdCalSignUp4AppThreeSess.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = false;
        ((LinkButton)GrdCalSignUp4AppThreeSess.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = false;
        ((LinkButton)GrdCalSignUp4AppThreeSess.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = true;

    }
    protected void GrdCalSignUp4AppThreeSess_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int rowIndex = e.RowIndex;
            CalSignUpRowUpdating(GrdCalSignUp4AppThreeSess, rowIndex);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void GrdCalSignUp4AppThreeSess_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            CalSignUpRowDataBound(e, GrdCalSignUp4AppThreeSess);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void LoadCalSignup4AppThreeSess(string MemberID)
    {
        try
        {
            int CurrentYear = DateTime.Now.Year;
            string Year = ddlYear.SelectedValue;
            string cmdText = string.Empty;
            DataSet ds = new DataSet();
            string FromVal = string.Empty;
            FromVal = hdnFromVal.Value;

            if (FromVal == "ApproveThreeSessions")
            {
                cmdText = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Phase as PhaseEx,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " AND C.EventID=13  and C.MemberID=" + MemberID + "  group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD";
            }

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dvCalSignUp4AppThreeSess.Visible = true;
                    GrdCalSignUp4AppThreeSess.DataSource = ds;
                    GrdCalSignUp4AppThreeSess.DataBind();
                    BtnCloaseTable4.Visible = true;
                    dvMemberInfo.Visible = false;
                    btnColseTable2.Visible = false;
                }
                else
                {
                    dvCalSignUp4AppThreeSess.Visible = true;
                    GrdCalSignUp4AppThreeSess.DataSource = ds;
                    GrdCalSignUp4AppThreeSess.DataBind();
                    BtnCloaseTable4.Visible = true;
                    dvMemberInfo.Visible = false;
                    btnColseTable2.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }


    public void ExportToExcelApproveMultipleSessions()
    {
        try
        {
            string Year = ddlYear.SelectedValue;
            string CmdText = string.Empty;
            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)=2) order by B.LastName, B.FirstName ASC;";

            CmdText += "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)>2) order by B.LastName, B.FirstName ASC;";

            string cmdPrdText = string.Empty;

            cmdPrdText += "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.Accepted='Y' and CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)=2) order by IP.LastName, IP.FirstName ASC; ";

            cmdPrdText += "select Distinct(ProductCode), CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where CS.Accepted='Y' and CS.EventYear=" + Year + " and CS.MemberID in (select MemberID from CalSignUp where EventYear=" + Year + " and Accepted='Y' group by MemberID HAVING Count(*)>2) order by IP.LastName, IP.FirstName ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            DataSet dsPrd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdPrdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null)
                {
                    ds.Tables[0].TableName = "Approved Two Sessions";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[0].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[0].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ds.Tables[0].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[0].Rows[i]["EventID"] = 13;
                            ds.Tables[0].Rows[i]["Team"] = "Coaching";
                        }

                        int j = 0;
                        string PCode = string.Empty;
                        ds.Tables[0].Columns.Add("Products").SetOrdinal(13);
                        string MemberID = string.Empty;
                        for (int i = 0; i < dsPrd.Tables[0].Rows.Count; i++)
                        {



                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                PCode += ", " + dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                            }
                            else
                            {
                                PCode = dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                            }


                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                j--;

                            }




                            ds.Tables[0].Rows[j]["Products"] = PCode;
                            j++;
                            MemberID = dsPrd.Tables[0].Rows[i]["MemberID"].ToString();

                        }

                    }
                }
                if (ds.Tables[1] != null)
                {
                    ds.Tables[1].TableName = "Approved Three or More Sessions";
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ds.Tables[1].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[1].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[1].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            ds.Tables[1].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[1].Rows[i]["EventID"] = 13;
                            ds.Tables[1].Rows[i]["Team"] = "Coaching";
                        }

                        int j = 0;
                        string PCode = string.Empty;
                        ds.Tables[1].Columns.Add("Products").SetOrdinal(13);
                        string MemberID = string.Empty;
                        for (int i = 0; i < dsPrd.Tables[1].Rows.Count; i++)
                        {



                            if (MemberID == dsPrd.Tables[1].Rows[i]["MemberID"].ToString())
                            {
                                PCode += ", " + dsPrd.Tables[1].Rows[i]["ProductCode"].ToString();
                            }
                            else
                            {
                                PCode = dsPrd.Tables[1].Rows[i]["ProductCode"].ToString();
                            }


                            if (MemberID == dsPrd.Tables[1].Rows[i]["MemberID"].ToString())
                            {
                                j--;

                            }




                            ds.Tables[1].Rows[j]["Products"] = PCode;
                            j++;
                            MemberID = dsPrd.Tables[1].Rows[i]["MemberID"].ToString();

                        }
                    }
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "ApprovedMultipleSessions_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
        catch (Exception ex)
        {
        }
    }


    public void LoadMultiplePrdsAndSessionsCoach()
    {
        string CmdText = string.Empty;
        string Year = ddlYear.SelectedValue;

        CmdText = " select distinct CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + " group by CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter having (COUNT(distinct(CS.ProductID))>1 or (COUNT(distinct(CS.SessionNo))>1)) order by IP.LastName,IP.FirstName ASC;";

        CmdText += "select Distinct(ProductCode),count(distinct(CS.SessionNo)) as sessionCount,CS.MemberID,IP.FirstName,IP.LastName, Accepted from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where  CS.EventYear=" + Year + " and CS.MemberID in (select distinct CS.MemberID from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + "  group by CS.MemberID having (COUNT(distinct(CS.ProductID))>1 or (COUNT(distinct(CS.SessionNo))>1))) Group by CS.ProductCode,CS.MemberID,IP.FirstName,IP.LastName, Accepted  order by IP.LastName, IP.FirstName ASC";

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                BtnExpo.Visible = true;
                dvMultipleSessionColorCode.Visible = true;
                int j = 0;
                string PCode = string.Empty;
                ds.Tables[0].Columns.Add("Products").SetOrdinal(10);

                grdMultiplePrdsAndSessionsCoaches.DataSource = ds;
                grdMultiplePrdsAndSessionsCoaches.DataBind();

                string MemberID = string.Empty;
                string sessionNo = string.Empty;
                string prdID = string.Empty;
                int sessionCount = 1;
                string productCodes = string.Empty;
                string isGreen = "0";
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {



                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        int sessCount = Convert.ToInt32(ds.Tables[1].Rows[i]["sessionCount"].ToString());
                        if (sessCount > 1)
                        {
                            if (ds.Tables[1].Rows[i]["Accepted"].ToString() == "Y")
                            {
                                PCode += ", <span style='color:#04B431; font-weight:bold;'>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount + "</span>";
                                productCodes += "," + ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount;
                            }
                            else
                            {
                                isGreen = "-1";
                                PCode += ", <span>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount + "</span>";
                                productCodes += "," + ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount;
                            }
                        }
                        else
                        {
                            if (ds.Tables[1].Rows[i]["Accepted"].ToString() == "Y")
                            {
                                PCode += ", <span style='color:#04B431; font-weight:bold;'>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + "</span>";
                                productCodes += "," + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                            }
                            else
                            {
                                isGreen = "-1";
                                PCode += ", <span>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + "</span>";
                                productCodes += "," + ds.Tables[1].Rows[i]["ProductCode"].ToString();
                            }
                        }

                    }
                    else
                    {
                        int sessCount = Convert.ToInt32(ds.Tables[1].Rows[i]["sessionCount"].ToString());
                        if (sessCount > 1)
                        {
                            if (ds.Tables[1].Rows[i]["Accepted"].ToString() == "Y")
                            {
                                PCode = "<span style='color:#04B431; font-weight:bold;'>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount + "</span>";
                                productCodes = ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount;
                            }
                            else
                            {
                                isGreen = "-1";
                                PCode = "<span>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount + "</span>";
                                productCodes = ds.Tables[1].Rows[i]["ProductCode"].ToString() + " - " + sessCount;
                            }
                        }
                        else
                        {
                            if (ds.Tables[1].Rows[i]["Accepted"].ToString() == "Y")
                            {
                                PCode = "<span style='color:#04B431; font-weight:bold;'>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + "</span>";
                                productCodes = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                            }
                            else
                            {
                                isGreen = "-1";
                                PCode = "<span>" + ds.Tables[1].Rows[i]["ProductCode"].ToString() + "</span>";
                                productCodes = ds.Tables[1].Rows[i]["ProductCode"].ToString();
                            }
                        }
                    }


                    if (MemberID == ds.Tables[1].Rows[i]["MemberID"].ToString())
                    {
                        j--;

                    }




                    //  ds.Tables[0].Rows[j]["Products"] = PCode;

                    for (int k = 0; k < ds.Tables[1].Rows.Count; k++)
                    {
                        if (ds.Tables[1].Rows[k]["MemberID"].ToString() == ds.Tables[1].Rows[i]["MemberID"].ToString())
                        {
                            if (ds.Tables[1].Rows[k]["Accepted"].ToString() == "Y")
                            {

                            }
                            else
                            {
                                isGreen = "-1";
                            }
                        }
                    }
                    if (isGreen == "0")
                    {
                        grdMultiplePrdsAndSessionsCoaches.Rows[j].Cells[12].ForeColor = Color.White;
                        grdMultiplePrdsAndSessionsCoaches.Rows[j].Cells[12].Font.Bold = true;
                        grdMultiplePrdsAndSessionsCoaches.Rows[j].Cells[12].BackColor = ColorTranslator.FromHtml("#58d68d");
                        grdMultiplePrdsAndSessionsCoaches.Rows[j].Cells[12].Text = productCodes;
                    }
                    else if (isGreen == "-1")
                    {
                        grdMultiplePrdsAndSessionsCoaches.Rows[j].Cells[12].Text = PCode;
                    }

                    j++;
                    isGreen = "0";
                    MemberID = ds.Tables[1].Rows[i]["MemberID"].ToString();
                    //sessionNo = ds.Tables[1].Rows[i]["SessionNo"].ToString();
                    prdID = ds.Tables[1].Rows[i]["ProductCode"].ToString();

                }

                spnMultipleCoachSession.Visible = false;
                //grdMultiplePrdsAndSessionsCoaches.DataSource = ds;
                //grdMultiplePrdsAndSessionsCoaches.DataBind();

                //grdMultiplePrdsAndSessionsCoaches.Rows[1].Cells[9].Text = "<span style='color:Blue;'>Text</span>";
            }
            else
            {
                dvMultipleSessionColorCode.Visible = false;
                spnMultipleCoachSession.Visible = true;
                grdMultiplePrdsAndSessionsCoaches.DataSource = ds;
                grdMultiplePrdsAndSessionsCoaches.DataBind();
            }
        }
    }


    public void ExportToExcelMultipleProductsSessionsCoach()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            string Year = ddlYear.SelectedValue;
            CmdText = " select distinct CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + " group by CS.MemberID,IP.AutoMemberID, IP.FirstName, IP.LastName, IP.Gender, IP.Email, IP.HPhone, IP.CPhone, IP.City, IP.State, IP.Chapter having (COUNT(distinct(CS.ProductID))>1 or (COUNT(distinct(CS.SessionNo))>1)) order by IP.LastName,IP.FirstName ASC;";

            if (dvMemberInfo.Visible == true)
            {

                CmdText += "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Phase as PhaseEx,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " AND C.EventID=13  and C.MemberID=" + hdnMemberID.Value + "  group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD";
            }

            string cmdText = "select Distinct(ProductCode),count(distinct(CS.SessionNo)) as sessionCount,CS.MemberID,IP.FirstName,IP.LastName from CalSignUp CS inner join IndSpouse IP on (IP.AutoMemberID=CS.MemberID) where  CS.EventYear=" + Year + " and CS.MemberID in (select distinct CS.MemberID from CalSignup CS inner join Indspouse IP on (CS.MemberID=IP.AutoMemberID) where CS.EventYear=" + Year + "  group by CS.MemberID having (COUNT(distinct(CS.ProductID))>1 or (COUNT(distinct(CS.SessionNo))>1))) Group by CS.ProductCode,CS.MemberID,IP.FirstName,IP.LastName  order by IP.LastName, IP.FirstName ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            DataSet dsPrd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0] != null)
                {
                    ds.Tables[0].TableName = "Multiple Products or Sessions";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                        ds.Tables[0].Columns.Add("EventID").SetOrdinal(4);
                        ds.Tables[0].Columns.Add("Team").SetOrdinal(5);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            ds.Tables[0].Rows[i]["Ser#"] = i + 1;
                            ds.Tables[0].Rows[i]["EventID"] = 13;
                            ds.Tables[0].Rows[i]["Team"] = "Coaching";
                        }

                        int j = 0;
                        string PCode = string.Empty;
                        ds.Tables[0].Columns.Add("Products").SetOrdinal(14);
                        string MemberID = string.Empty;
                        for (int i = 0; i < dsPrd.Tables[0].Rows.Count; i++)
                        {



                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                int sessCount = Convert.ToInt32(dsPrd.Tables[0].Rows[i]["sessionCount"].ToString());
                                if (sessCount > 1)
                                {
                                    PCode += ", " + dsPrd.Tables[0].Rows[i]["ProductCode"].ToString() + " - " + sessCount;
                                }
                                else
                                {
                                    PCode += ", " + dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                                }
                            }
                            else
                            {
                                int sessCount = Convert.ToInt32(dsPrd.Tables[0].Rows[i]["sessionCount"].ToString());
                                if (sessCount > 1)
                                {
                                    PCode = dsPrd.Tables[0].Rows[i]["ProductCode"].ToString() + " - " + sessCount;
                                }
                                else
                                {
                                    PCode = dsPrd.Tables[0].Rows[i]["ProductCode"].ToString();
                                }
                            }


                            if (MemberID == dsPrd.Tables[0].Rows[i]["MemberID"].ToString())
                            {
                                j--;

                            }




                            ds.Tables[0].Rows[j]["Products"] = PCode;
                            j++;
                            MemberID = dsPrd.Tables[0].Rows[i]["MemberID"].ToString();

                        }
                    }
                }
                if (dvMemberInfo.Visible == true)
                {
                    if (ds.Tables[1] != null)
                    {
                        ds.Tables[1].TableName = "Calendar Signups for Acceptance";
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            ds.Tables[1].Columns.Add("Ser#").SetOrdinal(0);

                            ds.Tables[1].Columns.Add("Team").SetOrdinal(5);
                            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                            {
                                ds.Tables[1].Rows[i]["Ser#"] = i + 1;

                                ds.Tables[1].Rows[i]["Team"] = "Coaching";
                            }
                        }
                    }
                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "CoachesWithMultipleSessionsProducts_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
        catch (Exception ex)
        {

        }

    }
    protected void ddlCoachType_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnColseTable2.Visible = false;
        BtnCloaseTable4.Visible = false;
        dvCalSignUp4AppThreeSess.Visible = false;
        try
        {
            if (validateCoaches() == "1")
            {
                GrdAssignedCoach.PageIndex = 0;
                GrdNotAssignedCoach.PageIndex = 0;
                GrdSignedUpCalendar.PageIndex = 0;
                GrdReturningNotSignedupCal.PageIndex = 0;
                GrdReturningSignedUpCal.PageIndex = 0;
                if (ddlCoachType.SelectedValue == "New Coaches")
                {
                    dvAssignedCoach.Visible = true;
                    dvCoachingNew.Visible = true;
                    dvSignedUpCalendar.Visible = true;

                    LoadCoachNotAssignedCaoch();
                    LoadCalendarSignedUpCoach();
                    LoadCoachAssignedCaoch();

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;

                    dvCoachNotAccepted.Visible = false;
                    dvMemberInfo.Visible = false;
                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvMultiplePrdsAndSessionsCoaches.Visible = false;
                }
                else if (ddlCoachType.SelectedValue == "Returning Coaches")
                {

                    DvReturningSignedUpCal.Visible = true;
                    dvReturningNotSignedupCal.Visible = true;

                    LoadReturningCalendarSignedUpCoach();
                    LoadReturningNotCalendarSignedUpCoach();

                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    dvCoachNotAccepted.Visible = false;
                    dvMemberInfo.Visible = false;
                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvMultiplePrdsAndSessionsCoaches.Visible = false;
                }
                else if (ddlCoachType.SelectedValue == "Approve Calendar Signups")
                {
                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;
                    dvMemberInfo.Visible = false;
                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvCoachNotAccepted.Visible = true;
                    dvMultiplePrdsAndSessionsCoaches.Visible = false;
                    LoadCoachNotAcceptedList();
                    LoadCoachAcceptedNotAcceptedList();
                }
                else if (ddlCoachType.SelectedValue == "Approved Multiple Sessions")
                {
                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;
                    dvMemberInfo.Visible = false;

                    dvCoachNotAccepted.Visible = false;

                    dvApprovedTwoSessions.Visible = true;
                    dvApprovedThreeSessions.Visible = true;

                    dvMultiplePrdsAndSessionsCoaches.Visible = false;


                    LoadApprovedTwoSessions();
                    LoadApprovedThreeMoreSessions();
                }
                else if (ddlCoachType.SelectedValue == "Multiple Sessions/Products")
                {
                    dvAssignedCoach.Visible = false;
                    dvCoachingNew.Visible = false;
                    dvSignedUpCalendar.Visible = false;

                    DvReturningSignedUpCal.Visible = false;
                    dvReturningNotSignedupCal.Visible = false;
                    dvMemberInfo.Visible = false;

                    dvCoachNotAccepted.Visible = false;

                    dvApprovedTwoSessions.Visible = false;
                    dvApprovedThreeSessions.Visible = false;

                    dvMultiplePrdsAndSessionsCoaches.Visible = true;
                    LoadMultiplePrdsAndSessionsCoach();
                }
            }
            else
            {
                validateCoaches();
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void deleteMeeting(string HostId, string MeetingKey)
    {
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + HostId + "";
            urlParameter += "&id=" + MeetingKey + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnHostURL.Value = json["start_url"].ToString();
                hdnJoinMeetingUrl.Value = json["join_url"].ToString();

                String strQuery = "update CalSignup set MeetingKey = " + json["id"].ToString() + ", HostjoinURL=" + json["start_url"].ToString() + "  where SignupId=" + hdnSignupId.Value + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strQuery);

            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }

    public void createZoomMeeting(string hostID, string Subject)
    {
        try
        {

            string URL = string.Empty;
            string durationHrs = "0";
            string service = "1";
            URL = "https://api.zoom.us/v1/meeting/create";
            string urlParameter = string.Empty;


            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //  urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";

            urlParameter += "&host_id=" + hostID + "";

            urlParameter += "&topic=" + Subject + "";

            urlParameter += "&type=3";


            urlParameter += "&option_jbh=true";
            urlParameter += "&option_host_video=true";
            urlParameter += "&option_audio=both";


            makeZoomAPICall(urlParameter, URL, service);


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

}