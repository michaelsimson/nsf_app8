<%@ Register TagPrefix="uc1" TagName="footer" Src="include/footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.Welcome" CodeFile="Welcome.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Welcome</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
	</HEAD>
	<body bgColor="#f2cfa9">
		<form id="Form1" method="post" runat="server">
			<table width="75%">
				<tr>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<H1 align="center"><FONT face="Arial" color="#0000ff" size="3"><B>Welcome to North South 
									Foundation Website</B></FONT>&nbsp;<BR>
						</H1>
					</td>
				</tr>
				<tr>
					<td>
						<p><font color="red" size="3"> </font>&nbsp;</p>
						<P>&nbsp;</P>
						<P><FONT face="Arial" color="#0000ff" size="3">We are attempting to improve the NSF 
								web-interface.&nbsp;&nbsp; Our goal is to make it easy for you to interact with 
								the NSF online applications.&nbsp; Your feedback is important to make the 
								interface more user-friendly.&nbsp; Send all your comments to </FONT><A href="mailto:nsfcontests@gmail.com" target="_blank">
								<FONT face="Arial" size="3"><U>nsfcontests@gmail.com</U></FONT></A><FONT face="Arial" color="#0000ff" size="3">&nbsp; 
								Please be as specific as possible to make our mutual communication more 
								effective.</FONT>&nbsp;<BR>
						</P>
						<P><FONT face="Arial" color="#0000ff" size="3">The new system is menu driven.&nbsp; You 
								will be able to update your personal records, including your children, at 
								anytime.&nbsp; You will be able to register for the NSF educational contests 
								when they are available.&nbsp; Similarly you will be able to view scores once 
								they are loaded into the NSF database.</FONT>&nbsp;<BR>
						</P>
						<P><FONT face="Arial" color="#0000ff" size="3">You will always need a login id (which 
								is the email address you use daily) and password to access the system.&nbsp; 
								Please keep both in a safe place.&nbsp; If you forget your password, you can 
								get it by providing the login ID to where the password will be emailed.&nbsp; 
								If you changed your email address, please let us know by sending an email to </FONT>
							<A href="mailto:nsfcontests@gmail.com" target="_blank"><FONT face="Arial" size="3"><U>nsfcontests@gmail.com</U></FONT></A>&nbsp;<BR>
							&nbsp;<BR>
						</P>
						<P></FONT></P>
						<P></P>
						<P>
						<P></P>
						<P></P>
					</td>
				</tr>
				<tr>
					<td align="center">
						<h6><FONT face="Arial Unicode MS" size="3"><FONT face="Arial" color="#0000ff" size="3"><B>If 
										you are new to North South Foundation (NSF), please click here:</B></FONT>
							</FONT>
							<asp:hyperlink id="hlinkNewUser" runat="server" NavigateUrl="CreateUser.aspx">New To NSF</asp:hyperlink>
						</h6>
					</td>
				</tr>
				<tr>
					<td align="left">
						<h6><FONT face="Arial Unicode MS" size="3"><FONT face="Arial" color="#0000ff" size="3"><B>If 
										you have previously registered with NSF, please click here</B></FONT><FONT face="Times New Roman" size="3"><B>:</B></FONT>&nbsp;</FONT>
							<asp:hyperlink id="hlinkLogin" runat="server" NavigateUrl="Login.aspx?Action=2006Reg">Part of NSF Family</asp:hyperlink></h6>
					</td>
				</tr>
				<!--				<tr>
					<td align="left">
						<h6><FONT face="Arial Unicode MS" size="3">Chapter Coordinators: Please click for 
								Coordinator Functions&nbsp;</FONT> &nbsp;
							<asp:hyperlink id="hlinkCoordinator" runat="server" NavigateUrl="Login.aspx">Chapter Coordinator Functions</asp:hyperlink></h6>
					</td>
				</tr> -->
				<tr>
					<td>
						<uc1:footer id="Footer1" runat="server"></uc1:footer>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

 

 
 
 