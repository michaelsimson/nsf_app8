'Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Partial Class GameRegistration
    Inherits System.Web.UI.Page
    Public nRegFee As Decimal = 0
    Dim conn As SqlConnection
    Dim StatusIndiCator As Integer = 0
    Dim updateRecord As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Debug 
        'Session("CustIndID") = 9210  '4
        'Session("LoggedIn") = "True"
        'Session("CustSpouseID") = 9210
        'Session("EventID") = 3
        'Session("LoginChapterID") = 48
        Session("LATEFEE") = 0   'this is a variable that is going to be used in the future
        nRegFee = 0
        conn = New SqlConnection(Application("ConnectionString"))
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=p")
        End If
        If Not Page.IsPostBack Then
            LoadChildren()
            LoadPaidList()
            LoadAvailableEventsList()
        Else
            'LoadPaidList()
            'LoadAvailableEventsList()
        End If
        tdPrepclub.InnerHtml = "Game selection for " & " <font style='color:Maroon'>" & ddlChildren.SelectedItem.Text & "</font>"
        lblWarning.Visible = False
    End Sub
    Private Sub LoadAvailableEventsList()
        conn = New SqlConnection()
        conn.ConnectionString = Application("ConnectionString")
        Dim cmd As New SqlCommand
        Dim dsPaidList As New DataSet
        Dim da As New SqlDataAdapter
        Dim SqlStr As String
        ''SqlStr = "IF EXISTS (SELECT GAMEID FROM GAME WHERE ChildNumber =" & ddlChildren.SelectedValue & " AND GetDate() <= DATEADD(day, 1,Coalesce(EndDate, '')))"
        ''SqlStr = SqlStr & " SELECT DISTINCT I.Chapter,I.ChapterID, CH.ChildNumber,P.EventID,P.EventCode,P.ProductCode,P.Name as ProductName,P.ProductGroupId ,P.ProductGroupCode ,P.ProductId, EF.RegFee,CASE WHEN GA.PaymentReference IS NULL or GA.PaymentReference ='' THEN 'Select' ELSE GA.PaymentReference END AS Status FROM Product P "
        ''SqlStr = SqlStr & " INNER JOIN EventFees EF ON EF.ProductID = P.ProductId AND P.EventId =EF.EventID "
        ''SqlStr = SqlStr & " INNER JOIN GAME GA ON Coalesce(GA.PaymentReference,'') <>'Paid' and EF.ProductID =GA.ProductID "
        ''SqlStr = SqlStr & " INNER JOIN Child CH ON CH.ChildNumber = " & ddlChildren.SelectedValue
        ''SqlStr = SqlStr & " INNER JOIN IndSpouse I ON I.AutoMemberID = CH.MEMBERID  "
        ''SqlStr = SqlStr & " WHERE P.EventId = 4 AND P.Status='O' and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo AND GA.ChildNumber = " & ddlChildren.SelectedValue & " AND GetDate() <= DATEADD(day, 1,Coalesce(EndDate, '')) ORDER BY P.ProductCode ELSE "
        ''SqlStr = SqlStr & " SELECT DISTINCT I.Chapter,I.ChapterID, CH.ChildNumber,P.EventID,P.EventCode,P.ProductCode,P.Name as ProductName,P.ProductGroupId ,P.ProductGroupCode ,P.ProductId, EF.RegFee,CASE WHEN GA.PaymentReference IS NULL or GA.PaymentReference ='' THEN 'Select' ELSE GA.PaymentReference END AS Status FROM Product P "
        ''SqlStr = SqlStr & " INNER JOIN EventFees EF ON EF.ProductID = P.ProductId AND P.EventId =EF.EventID "
        ''SqlStr = SqlStr & " INNER JOIN GAME GA ON Coalesce(GA.PaymentReference,'') <>'Paid' and Coalesce(GA.PaymentReference,'') <>'Pending' and EF.ProductID = GA.ProductID "
        ''SqlStr = SqlStr & " INNER JOIN Child CH ON CH.ChildNumber =" & ddlChildren.SelectedValue & " INNER JOIN IndSpouse I ON I.AutoMemberID = CH.MEMBERID "
        ''SqlStr = SqlStr & " WHERE P.EventId = 4 AND P.Status='O' and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo  ORDER BY P.ProductCode"
        SqlStr = " Select DISTINCT I.Chapter,I.ChapterID,CH.ChildNumber,P.EventID,P.EventCode,P.ProductCode,P.Name as ProductName,P.ProductGroupId,P.ProductGroupCode,P.ProductId ,EF.RegFee, "
        SqlStr = SqlStr & " CASE WHEN exists (select gameid from GAME where ChildNumber =" & ddlChildren.SelectedValue & " AND ((Approved ='') or (Approved IS NULL) or (Approved ='No')) and ProductID =P.ProductId and GetDate() <= DATEADD(day, 1,Coalesce(EndDate, '')) )  then 'Pending' else 'Select'    END "
        SqlStr = SqlStr & " AS Status FROM Product P  INNER JOIN EventFees EF ON EF.ProductID = P.ProductId AND P.EventId = EF.EventID INNER JOIN Child CH ON CH.ChildNumber = " & ddlChildren.SelectedValue & " INNER JOIN IndSpouse I ON I.AutoMemberID = CH.MEMBERID   "
        SqlStr = SqlStr & " WHERE P.EventId =" & Session("EventID") & " AND P.Status='O' and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo AND CH.ChildNumber =" & ddlChildren.SelectedValue
        SqlStr = SqlStr & " AND NOT exists (select * from GAME where ChildNumber =" & ddlChildren.SelectedValue & " AND Approved IS NOT NULL and Approved !='' and Approved !='No' AND ProductId = P.ProductID and GetDate() <= DATEADD(day, 1,Coalesce(EndDate, '')) ) ORDER BY P.ProductCode"
        cmd.Connection = conn
        conn.Open()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = SqlStr
        da.SelectCommand = cmd
        da.Fill(dsPaidList)
        If dsPaidList.Tables(0).Rows.Count > 0 Then
            'InsertGameRecords(dsPaidList, ddlChildren.SelectedValue, Session("CustIndID").ToString, False)
            dgWkSelectionList.Visible = True
            lblWkSelectionList.Visible = False
            'If updateRecord = True Then
            '    dsPaidList.Clear()
            '    cmd.CommandType = CommandType.Text
            '    cmd.CommandText = SqlStr
            '    da.SelectCommand = cmd
            '    da.Fill(dsPaidList)
            dgWkSelectionList.DataSource = dsPaidList.Tables(0)
            dgWkSelectionList.DataBind()
            'Else
            '    dgWkSelectionList.DataSource = dsPaidList.Tables(0)
            '    dgWkSelectionList.DataBind()
            'End If
            '        Else
            ''InsertGameRecords(dsPaidList, ddlChildren.SelectedValue, Session("CustIndID").ToString, True)
            'If updateRecord = True Then
            '    dsPaidList.Clear()
            '    cmd.CommandType = CommandType.Text
            '    cmd.CommandText = SqlStr
            '    da.SelectCommand = cmd
            '    da.Fill(dsPaidList)
            '    dgWkSelectionList.DataSource = dsPaidList.Tables(0)
            '    dgWkSelectionList.DataBind()
        Else
            dgWkSelectionList.Visible = False
            lblWkSelectionList.Text = "No selections are available for this child."
            lblWkSelectionList.Visible = True
        End If

        enableDisableCheckBox()
        DisableGroups()
        If conn.State <> ConnectionState.Closed Then conn.Close()
    End Sub
    Private Sub enableDisableCheckBox()
        Dim dgItem As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim Lbl3 As Label
        Dim totalAmt As Double
        totalAmt = 0.0
        btnPayNow.Enabled = False
        tdSubmit.Style.Add("display", "none")
        tdWant.Style.Add("display", "none")
        Session("ContestsSelected") = Nothing
        For Each dgItem In dgWkSelectionList.Items
            selectRadioButton = dgItem.FindControl("chkEvent")
            Lbl1 = dgItem.FindControl("lblStatus")
            If Lbl1.Text.Equals("Pending") Then
                Lbl2 = dgItem.FindControl("lblFee")
                totalAmt = totalAmt + CInt(Lbl2.Text)
                Lbl3 = dgItem.FindControl("lblProductCode")
                Session("ContestsSelected") = Session("ContestsSelected") & Lbl3.Text & "(" & ddlChildren.SelectedValue & ")(" & Session("CustIndChapterID") & ")(" & Lbl2.Text & ")"
                Lbl1.Style.Add("color", "Red")
                selectRadioButton.Text = "Remove"
                selectRadioButton.Attributes.Add("style", "color:Red")
                btnPayNow.Enabled = True
                tdSubmit.Style.Add("display", "block")
                tdWant.Style.Add("display", "block")
            End If
            lblTotalFee.Text = totalAmt.ToString()
        Next
        '*********************** Function added to enable PayNow Button if there is pending status for any of the children other then the selected one**********'
        CheckPendingStatus()
    End Sub
    Private Sub CheckPendingStatus()
        If btnPayNow.Enabled = False Then
            Dim SqlStr As String = "select COUNT(*) from GAME where ChildNumber = " & ddlChildren.SelectedValue & " and MemberID =" & Session("CustINDID") & " and ((Approved ='') or (Approved IS NULL)) and GetDate() <= DATEADD(day, 1,EndDate)"
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SqlStr) > 0 Then
                btnPayNow.Enabled = True
            End If
        End If
    End Sub
    Private Sub LoadPaidList()
        conn = New SqlConnection()
        conn.ConnectionString = Application("ConnectionString")
        Dim cmd As New SqlCommand
        Dim dsPaidList As New DataSet
        Dim da As New SqlDataAdapter
        Dim SqlStr As String = "with VTBL AS (SELECT DISTINCT EF.RegFee as Fee,GA.EndDate as RegDeadline,I.Chapter,P.ProductCode,P.Name as ProductName,CASE WHEN GetDate() >= DATEADD(D,1,GA.EndDate) THEN 'Expired' ELSE 'Paid' END AS Status FROM Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductId AND P.EventId =EF.EventID INNER JOIN GAME GA ON GA.ProductID =EF.ProductID AND Approved IS NOT NULL and Approved !='No' INNER JOIN Child CH ON CH.ChildNumber = GA.ChildNumber INNER JOIN IndSpouse I ON I.AutoMemberID = CH.MEMBERID WHERE GA.ChildNumber =" & ddlChildren.SelectedValue & " AND  P.EventId = 4 AND P.Status='O' and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo and GetDate() <= DATEADD(year,2,GA.EndDate)) select MAX(VTBL.RegDeadline) as RegDeadline ,count(Fee) as Rcount,VTBL.Fee ,VTBL.ProductCode ,VTBL.ProductName ,VTBL.Chapter ,VTBL.Status from VTBL group by VTBL.Status ,VTBL.ProductCode ,VTBL.ProductName ,VTBL.Chapter ,VTBL.Fee "
        cmd.Connection = conn
        conn.Open()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = SqlStr
        'cmd.Parameters.Add(New SqlParameter("@ChildNumber", ddlChildren.SelectedValue))
        ''Following ChapterId 48 has to be brought
        'cmd.Parameters.Add(New SqlParameter("@ChapterID", Session("CustIndChapterID")))
        'cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
        'cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
        da.SelectCommand = cmd
        da.Fill(dsPaidList)
        If dsPaidList.Tables(0).Rows.Count > 0 Then
            dgPaidList.Visible = True
            lblContestInfo.Visible = False
            dgPaidList.DataSource = dsPaidList.Tables(0)
            dgPaidList.DataBind()
        Else
            dgPaidList.DataSource = Nothing
            dgPaidList.Visible = False
            lblContestInfo.Text = "No Transactions to List"
            lblContestInfo.Visible = True
        End If
        If conn.State <> ConnectionState.Closed Then conn.Close()
    End Sub
    Private Sub LoadChildren()
        Dim objChild As New Child
        Dim dsChild As New DataSet
        Dim strWhere As String
        conn = New SqlConnection()
        conn.ConnectionString = Application("ConnectionString")
        strWhere = "MemberId='" & Session("CustIndID") & "' or SpouseID='" & Session("CustIndID") & "'"
        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, strWhere)
        If dsChild.Tables.Count > 0 Then
            If dsChild.Tables(0).Rows.Count > 0 Then
                Dim dvChildren As New DataView
                dvChildren = dsChild.Tables(0).DefaultView
                Dim dtMdate As Date = FormatDateTime(("10/1/" & (Application("ContestYear") - 1)), DateFormat.ShortDate)
                Dim strFilter As String = "((ModifyDate >= '" & dtMdate & "') or (CreateDate >= '" & dtMdate & "' and  ModifyDate is Null ))"
                dvChildren.RowFilter = strFilter
                ddlChildren.DataSource = dvChildren
                ddlChildren.DataBind()
                Session("ChildCount") = dsChild.Tables(0).Rows.Count
                ddlChildren.SelectedIndex = 0
                Session("SelectedChildID") = ddlChildren.SelectedValue
            End If
        End If
        If conn.State <> ConnectionState.Closed Then conn.Close()
        'lblDebug.Text = strWhere
        'lblDebug.Visible = True
    End Sub
    Protected Sub chkEvent_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objCheckBox As CheckBox
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lblFee As Label
        Dim lbl3 As Label
        Dim strSelected As String
        Dim totalFee As Double
        totalFee = 0.0

        objCheckBox = sender
        strSelected = ""
        If objCheckBox.Checked = True Then
            For Each dgItem In dgWkSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked Then
                    lblFee = dgItem.FindControl("lblFee")
                    If lblFee.Text.Trim().Length > 0 Then
                        totalFee = totalFee + Double.Parse(lblFee.Text)
                    End If

                    Lbl1 = dgItem.FindControl("lblProductCode")
                    For Each dgItem1 In dgWkSelectionList.Items
                        lbl3 = dgItem.FindControl("lblStatus")
                        If lbl3.Text <> "Pending" Then
                            If dgWkSelectionList.DataKeys(dgItem.ItemIndex) <> dgWkSelectionList.DataKeys(dgItem1.ItemIndex) Then
                                Lbl2 = dgItem1.FindControl("lblProductCode")
                                If Lbl2.Text.Equals(Lbl1.Text) Then
                                    selectRadioButton1 = dgItem1.FindControl("chkEvent")
                                    selectRadioButton1.Checked = False
                                    selectRadioButton1.Enabled = False
                                End If
                            End If
                        End If


                    Next
                End If
            Next
        ElseIf objCheckBox.Checked = False Then
            strSelected = ""
            For Each dgItem In dgWkSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked = True Then
                    lblFee = dgItem.FindControl("lblFee")
                    If lblFee.Text.Trim().Length > 0 Then
                        totalFee = totalFee + Double.Parse(lblFee.Text)
                    End If
                    Lbl1 = dgItem.FindControl("lblProductCode")
                    strSelected = strSelected + "," + Lbl1.Text + "-" + dgWkSelectionList.DataKeys(dgItem.ItemIndex).ToString()
                End If
                '    Lbl1 = dgItem.FindControl("ItemType")
                '    For Each dgItem1 In DataGrid1.Items
                '        If DataGrid1.DataKeys(dgItem.ItemIndex) <> DataGrid1.DataKeys(dgItem1.ItemIndex) Then
                '            Lbl2 = dgItem1.FindControl("ItemType")
                '            If Lbl2.Text.Equals(Lbl1.Text) Then
                '                selectRadioButton1 = dgItem1.FindControl("SelectCheck")
                '                selectRadioButton1.Enabled = True
                '            End If
                '        End If
                '    Next
                'End If
            Next
            Dim strAr() As String
            Dim str1() As String
            Dim i As Integer
            Dim itmType As String
            Dim itmIndex As String
            strAr = strSelected.Split(",")
            If strAr.Length > 1 Then
                For i = 0 To strAr.Length - 1
                    str1 = strAr(i).Split("-")
                    If str1.Length = 2 Then
                        itmType = str1(0)
                        itmIndex = str1(1)
                        For Each dgItem In dgWkSelectionList.Items
                            selectRadioButton = dgItem.FindControl("chkEvent")
                            Lbl1 = dgItem.FindControl("lblProductCode")
                            lbl3 = dgItem.FindControl("lblStatus")
                            If lbl3.Text <> "Pending" Then
                                If Lbl1.Text.Equals(itmType) And dgWkSelectionList.DataKeys(dgItem.ItemIndex) <> itmIndex Then
                                    selectRadioButton.Enabled = False
                                Else
                                    selectRadioButton.Enabled = True
                                End If
                            End If
                        Next
                    End If
                Next
            Else
                For Each dgItem In dgWkSelectionList.Items

                    Lbl2 = dgItem.FindControl("lblStatus")
                    If Lbl2.Text <> "Pending" Then
                        selectRadioButton = dgItem.FindControl("chkEvent")
                        selectRadioButton.Enabled = True
                    End If
                Next
            End If
        End If
        totalFee = 0.0
        For Each dgItem In dgWkSelectionList.Items
            Lbl2 = dgItem.FindControl("lblStatus")
            Lbl1 = dgItem.FindControl("lblFee")
            selectRadioButton = dgItem.FindControl("chkEvent")
            If selectRadioButton.Checked = True And Lbl2.Text.Equals("Select") Then
                totalFee = totalFee + Double.Parse(Lbl1.Text)
            ElseIf selectRadioButton.Checked = False And Lbl2.Text.Equals("Pending") Then
                totalFee = totalFee + Double.Parse(Lbl1.Text)
            End If
        Next
        lblTotalFee.Text = totalFee.ToString()
    End Sub
    Public Sub DisableGroups()
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lblIt1 As Label
        Dim lblStatus As Label

        Dim strSelected As String

        strSelected = ""

        For Each dgItem In dgWkSelectionList.Items
            selectRadioButton = dgItem.FindControl("chkEvent")
            Lbl1 = dgItem.FindControl("lblStatus")
            If Lbl1.Text.Equals("Pending") Then
                Lbl2 = dgItem.FindControl("lblProductCode")
                For Each dgItem1 In dgWkSelectionList.Items
                    lblIt1 = dgItem1.FindControl("lblProductCode")
                    lblStatus = dgItem1.FindControl("lblStatus")
                    If lblStatus.Text <> "Pending" And lblIt1.Text.Equals(Lbl2.Text) Then
                        selectRadioButton1 = dgItem1.FindControl("chkEvent")
                        selectRadioButton1.Enabled = False
                    End If
                Next
            End If
        Next
    End Sub
    Protected Sub ddlChildren_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChildren.SelectedIndexChanged
        LoadPaidList()
        LoadAvailableEventsList()
    End Sub
    Protected Sub btnSubmitSelections_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitSelections.Click
        Dim bFlagEnabled As Boolean = False
        btnSubmitSelections.Enabled = False
        If btnPayNow.Enabled = True Then
            bFlagEnabled = True
            btnPayNow.Enabled = False
        End If

        'insert into Registration table
        Dim dgItem As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim intKeyValue As Integer
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lbl3 As Label
        Dim bolSelected As Boolean
        Dim totalFee As Double
        Dim sqlUpdate = String.Empty
        totalFee = 0.0
        lblWkSelectionList.Text = ""
        Try
            bolSelected = False
            For Each dgItem In dgWkSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked = True Then
                    bolSelected = True
                End If
            Next
            If bolSelected = False Then

                'Display Warning message.
                lblWarning.Text = "No Event selected, please select at least one event."
                lblWarning.Visible = True
                GoTo lblEnd
            End If
            conn = New SqlConnection()
            conn.ConnectionString = Application("ConnectionString")
            conn.Open()
            For Each dgItem In dgWkSelectionList.Items
                totalFee = 0.0
                selectRadioButton = dgItem.FindControl("chkEvent")
                Lbl1 = dgItem.FindControl("lblStatus")
                If selectRadioButton.Checked = True And Lbl1.Text <> "Pending" Then
                    lbl3 = dgItem.FindControl("lblProductCode")
                    ' Insert Record Into Game
                    intKeyValue = dgWkSelectionList.DataKeys(dgItem.ItemIndex)
                    Dim cmd As New SqlCommand
                    Dim dsEventCaldata As New DataSet
                    Dim da As New SqlDataAdapter
                    cmd.Connection = conn
                    cmd.CommandType = CommandType.Text
                    'SET IDENTITY_INSERT GAME ON; commented by DINO

                    sqlUpdate = " Insert Into GAME (ChapterID,ChapterCode,EventId,EventCode,ProductGroupID,ProductGroupCode,ProductID,ProductCode,EventYear,GameID,StartDate,EndDate,ChildNumber,MemberID,CreateDate,CreatedBy,Amount) "
                    sqlUpdate = sqlUpdate & " SELECT I.ChapterID,I.Chapter,P.EventID,P.EventCode,P.ProductGroupID,P.ProductGroupCode,P.ProductID,P.ProductCode,(select YEAR(getdate())),(select MAX(gameID)+1 from Game),GETDATE (),dateadd(year,+1,getdate()),"
                    sqlUpdate = sqlUpdate & intKeyValue & "," & Session("CustIndID").ToString & ",GETDATE()," & Session("CustIndID").ToString & ",E.RegFee FROM IndSpouse I, Product P,EventFees E WHERE I.AutoMemberID =" & Session("CustIndID").ToString & " and P.ProductCode ='" & lbl3.Text & "' and P.EventId=" & Session("EventID") & " and P.Status='O'"
                    sqlUpdate = sqlUpdate & " and E.EventID=P.EventID and E.ProductGroupID =P.ProductGroupId and E.ProductID=P.ProductId and E.EventYear =YEAR(GETDATE()); "
                    'SET IDENTITY_INSERT GAME OFF;   commented by DINO

                    cmd.CommandText = sqlUpdate
                    'cmd.CommandText = "UPDATE GAME Set PaymentReference ='Pending' where ChildNumber = " & intKeyValue & " and ProductCode ='" & lbl3.Text & "' and  GetDate() <= DATEADD(day, 1, EndDate )"
                    cmd.ExecuteNonQuery()
                ElseIf selectRadioButton.Checked = True And Lbl1.Text.Equals("Pending") Then
                    intKeyValue = dgWkSelectionList.DataKeys(dgItem.ItemIndex)
                    lbl3 = dgItem.FindControl("lblProductCode")
                    Dim cmd As New SqlCommand
                    Dim dsEventCaldata As New DataSet
                    Dim da As New SqlDataAdapter
                    cmd.Connection = conn
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = "Delete From Game where ChildNumber = " & intKeyValue & " and ProductCode ='" & lbl3.Text & "' and  GetDate() <= DATEADD(day, 1, EndDate )"
                    cmd.ExecuteNonQuery()
                    'SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_DeletePending_Registration_PrepClub", New SqlParameter("@RegId", Integer.Parse(Lbl2.Text)))
                End If
            Next
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            LoadAvailableEventsList()
            ' Server.Transfer("WkShopREgistration.aspx")
            lblWarning.Text = " Submitted Successfully"
            lblWarning.Visible = True
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            '  Response.Write(ex.ToString())
            'lblDataErr.Text = ex.Message
            'lblDataErr.Visible = True
            'Return False
        End Try
lblEnd:
        If bFlagEnabled = True Then
            btnPayNow.Enabled = True
        End If
        btnSubmitSelections.Enabled = True
    End Sub
    Protected Sub btnPayNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayNow.Click
        'Session("ContestsSelected") = Session("ContestsSelected")  & "RegFee:" & lblTotalFee.Text
        Session("RegFee") = lblTotalFee.Text
        Session("Mealsamount") = 0
        Session("LateFee") = 0
        Page.Response.Redirect("SummarySelections_Game.aspx")
    End Sub
    Protected Sub dgWkSelectionList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dgWkSelectionList.SelectedIndexChanged
    End Sub
    Protected Sub InsertGR(ByRef dset As DataSet, ByVal child As String, ByRef memberID As String, ByVal PrCode As String)
        Dim sqlstr As String = String.Empty
        Dim sqlPrd As String = "Select ProductId,ProductCode,ProductGroupId,ProductGroupCode   from Product where EventId = 4 and Status='O' and ProductCode='" & PrCode & "'"
        Dim dsprid As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString, CommandType.Text, sqlPrd)
        For Each dr As DataRow In dset.Tables(0).Rows
            For Each dr1 As DataRow In dsprid.Tables(0).Rows
                If dr.Item("ProductCode") = PrCode Then
                    sqlstr = "if not exists (select GameID   from GAME where ChildNumber = " & dr.Item("ChildNumber").ToString & "  and ProductID = " & dr1.Item("ProductId").ToString & " and  GetDate() <= DATEADD(day, 1, EndDate ))select 0 else Select 1"
                    If CInt(SqlHelper.ExecuteScalar(Application("ConnectionString").ToString, CommandType.Text, sqlstr)) = 0 Then
                        Dim insertSqlstring As String = "insert into GAME (GameID,StartDate,EndDate,ChildNumber ,MemberID ,ChapterID,ChapterCode,EventID ,EventCode,EventYear,ProductGroupID ,ProductGroupCode,ProductID,ProductCode,CreateDate,CreatedBy )values((select MAX(gameID)+1 from GAME),"
                        insertSqlstring = insertSqlstring & "getdate() , dateadd(year,+1,getdate())," & child & "," & memberID & "," & dr.Item("ChapterID").ToString & ",'" & dr.Item("Chapter").ToString & "'," & dr.Item("EventID").ToString & ",'" & dr.Item("EventCode").ToString & "', (select YEAR(getdate()))," & dr1.Item("ProductGroupID").ToString & ",'" & dr1.Item("ProductGroupCode").ToString & "'," & dr1.Item("ProductID").ToString & ",'" & dr1.Item("ProductCode").ToString & "',GETDATE ()," & memberID & ")"
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString, CommandType.Text, insertSqlstring)
                        updateRecord = True
                    End If
                End If
            Next
        Next
    End Sub
    Protected Sub InsertGameRecords(ByRef dset As DataSet, ByVal child As String, ByRef memberID As String, ByRef FCR As Boolean)
        Dim mID As String = memberID
        Dim cID As String = child
        Dim R_exists As Integer = 10
        Dim sqlstr As String = String.Empty
        Dim sqlPrd As String = "Select ProductId,ProductCode,ProductGroupId,ProductGroupCode   from Product where EventId = 4 and Status='O'"
        Dim dsprid As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString, CommandType.Text, sqlPrd)
        If FCR = True Then
            Dim sqlNstr As String = "SELECT DISTINCT I.Chapter,I.ChapterID, CH.ChildNumber,P.EventID,P.EventCode,P.ProductCode,P.Name as ProductName,P.ProductGroupId ,P.ProductGroupCode ,P.ProductId, EF.RegFee,CASE WHEN GA.PaymentReference IS NULL or GA.PaymentReference ='' THEN 'Select' ELSE GA.PaymentReference END AS Status FROM Product P  INNER JOIN EventFees EF ON EF.ProductID = P.ProductId AND P.EventId =EF.EventID  INNER JOIN GAME GA ON Coalesce(GA.PaymentReference,'') <>'Paid' and Coalesce(GA.PaymentReference,'') <>'Pending' and EF.ProductID = GA.ProductID  INNER JOIN Child CH ON CH.ChildNumber = " & child & " INNER JOIN IndSpouse I ON I.AutoMemberID = CH.MEMBERID  WHERE P.EventId = 4 AND P.Status='O' and Ch.GRADE Between EF.GradeFrom and Ef.GradeTo  ORDER BY P.ProductCode"
            dset.Clear()
            dset = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString, CommandType.Text, sqlNstr)
            For Each dr As DataRow In dset.Tables(0).Rows
                For Each dr1 As DataRow In dsprid.Tables(0).Rows
                    sqlstr = "if not exists (select GameID   from GAME where ChildNumber = " & dr.Item("ChildNumber").ToString & "  and ProductID = " & dr1.Item("ProductId").ToString & " and  GetDate() <= DATEADD(day, 1, EndDate ))select 0 else Select 1"
                    If CInt(SqlHelper.ExecuteScalar(Application("ConnectionString").ToString, CommandType.Text, sqlstr)) = 0 Then
                        Dim insertSqlstring As String = "insert into GAME (GameID,StartDate,EndDate,ChildNumber ,MemberID ,ChapterID,ChapterCode,EventID ,EventCode,EventYear,ProductGroupID ,ProductGroupCode,ProductID,ProductCode,CreateDate,CreatedBy )values((select MAX(gameID)+1 from GAME),"
                        insertSqlstring = insertSqlstring & "getdate() , dateadd(year,+1,getdate())," & child & "," & memberID & "," & dr.Item("ChapterID").ToString & ",'" & dr.Item("Chapter").ToString & "'," & dr.Item("EventID").ToString & ",'" & dr.Item("EventCode").ToString & "', (select YEAR(getdate()))," & dr1.Item("ProductGroupID").ToString & ",'" & dr1.Item("ProductGroupCode").ToString & "'," & dr1.Item("ProductID").ToString & ",'" & dr1.Item("ProductCode").ToString & "',GETDATE ()," & memberID & ")"
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString, CommandType.Text, insertSqlstring)
                        updateRecord = True
                    End If
                Next
            Next
            ' LoadAvailableEventsList()
        Else
            For Each dr As DataRow In dset.Tables(0).Rows
                For Each dr1 As DataRow In dsprid.Tables(0).Rows
                    sqlstr = "if not exists (select GameID   from GAME where ChildNumber = " & dr.Item("ChildNumber").ToString & "  and ProductID = " & dr1.Item("ProductId").ToString & " and  GetDate() <= DATEADD(day, 1, EndDate ))select 0 else Select 1"
                    If CInt(SqlHelper.ExecuteScalar(Application("ConnectionString").ToString, CommandType.Text, sqlstr)) = 0 Then
                        Dim insertSqlstring As String = "insert into GAME (GameID,StartDate,EndDate,ChildNumber ,MemberID ,ChapterID,ChapterCode,EventID ,EventCode,EventYear,ProductGroupID ,ProductGroupCode,ProductID,ProductCode,CreateDate,CreatedBy )values((select MAX(gameID)+1 from GAME),"
                        insertSqlstring = insertSqlstring & "getdate() , dateadd(year,+1,getdate())," & child & "," & memberID & "," & dr.Item("ChapterID").ToString & ",'" & dr.Item("Chapter").ToString & "'," & dr.Item("EventID").ToString & ",'" & dr.Item("EventCode").ToString & "', (select YEAR(getdate()))," & dr1.Item("ProductGroupID").ToString & ",'" & dr1.Item("ProductGroupCode").ToString & "'," & dr1.Item("ProductID").ToString & ",'" & dr1.Item("ProductCode").ToString & "',GETDATE ()," & memberID & ")"
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString, CommandType.Text, insertSqlstring)
                        updateRecord = True
                    End If
                Next
            Next
        End If
    End Sub
End Class
