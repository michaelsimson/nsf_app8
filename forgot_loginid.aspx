<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="~/forgot_loginid.aspx.vb"  Inherits="NEW_forgot_loginid" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
     <div>
          <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
            <tr bgcolor="#FFFFFF" >
                <td class="Heading" colspan="4" align="center" >Forgot Login ID</td>
            </tr>
              <tr bgcolor="#ffffff">
                  <td align="center" colspan="4">
                  </td>
              </tr>
            
            <tr bgcolor="#FFFFFF" >
               <td  colspan="4" align="center"><asp:Label ID="Label5" runat="server" Text="Please provide the following so that we can find your Login ID." ></asp:Label></td>
            </tr>
              <tr bgcolor="#ffffff">
                  <td align="center" colspan="4">
                  </td>
              </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    First Name:</td>
                <td >
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="inputBox"> </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                    ErrorMessage="Enter First Name" Width="1px" Font-Bold="True">*</asp:RequiredFieldValidator>
                    </td>
                <td >
                    City:</td>
                <td >
                    <asp:TextBox ID="txtCity" runat="server" CssClass="inputBox"></asp:TextBox>
                    </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Last Name:</td>
                <td >
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="inputBox"></asp:TextBox>
                    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLastName"
                                    ErrorMessage="Enter Last Name" Width="1px" Font-Bold="True">*</asp:RequiredFieldValidator>
                    </td>
                <td >
                    State:</td>
                <td >
                      <asp:DropDownList ID="ddlState" runat="server" CssClass="inputBox">
                    </asp:DropDownList>
                </td> 
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Home Phone:</td>
                <td >
                    <asp:TextBox ID="txtHomePhone" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td >
                    Zip:</td>
                <td >
                    <asp:TextBox ID="txtZip" runat="server" CssClass="inputBox"></asp:TextBox></td>
                 
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Spouse First Name:</td>
                <td >
                    <asp:TextBox ID="txtSpouseFirstName" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td >
                    Email To Contact:</td>
                <td >
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="inputBox"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="Enter Valid Email" Width="1px" Font-Bold="True">*</asp:RequiredFieldValidator>

                    </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Spouse Last Name:</td>
                <td >
                    <asp:TextBox ID="txtSpouseLastName" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td  colspan="2">
                    &nbsp;
                </td>                
            </tr>            
            <tr bgcolor="#FFFFFF" >
                <td class="ItemCenter" align="center" colspan="4"> 
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="FormButton" />
                </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td colspan="4" align="center" class="Heading">
                    <asp:Label runat="server" id="lblMessage" CssClass="Heading"></asp:Label>
                </td>
            </tr>
        </table>
 </asp:Content>



 
 
 