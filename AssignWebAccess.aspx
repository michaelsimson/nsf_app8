﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AssignWebAccess.aspx.vb" Inherits="AssignWebAccess" title="Assign Roles Web Folder Access" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div style="text-align:left">
   
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
    <asp:LinkButton ID="LinkButton1" PostBackUrl="WebPageMgmtMain.aspx" runat="server">Back</asp:LinkButton>
</div>
<div style ="width:800px; text-align : center ">
<table border="0px" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align="center" width="800px">
<div align="center">
<table border="0" cellpadding ="3" cellspacing ="0"> 
 <tr><td align="center" colspan="3"><h5>Assign Roles for Web Folder Access </h5></td></tr>
<tr><td align="left">Add/Update Volunteer : </td><td align="left">
        <asp:TextBox  Width="145px" ID="txtParent" runat="server" Enabled="false"></asp:TextBox></td>
        <td align="left">
            <asp:Button ID="Search" runat="server" Text="Search" /></td></tr>
           <tr><td align="center" colspan="3">
            <asp:Button ID="btnassigncoach" OnClick ="btnassigncoach_Click" runat="server" Text="Web Folder Access" />&nbsp; &nbsp; <asp:Button ID="btnassignwebadmin" Visible = "false" OnClick ="btnassignwebadmin_Click" runat="server" Text="Web Folder Admin" />
       </td></tr> <tr><td align="center" colspan ="3">
                <asp:Label ID="lblerr" ForeColor = "Red" runat="server" ></asp:Label></td></tr></table>

<asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<b> Search NSF member</b>   
<div align = "center">       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
    	<tr>
    	    <td align="center" colspan="2">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	   		&nbsp;&nbsp;&nbsp;		
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
</div> 
<br />
<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                               
         </Columns> </asp:GridView>    
 </asp:Panel>
 </asp:Panel>
 </div>
</td></tr></table>
 <asp:Label ID="HlblMemberID" runat="server" Visible="false"></asp:Label>
  <br />
 <br />
 <div align="center">
 <asp:DataGrid ID="DGVolunteer" runat="server" DataKeyField="volunteerID" 
        AutoGenerateColumns="False"  OnItemCommand="DGVolunteer_ItemCommand" 
        CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>
           <ASP:TemplateColumn>
           <ItemTemplate>
						<asp:LinkButton id="lbtnRemove" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"></asp:LinkButton>
			</ItemTemplate>
						</ASP:TemplateColumn>
           <asp:Boundcolumn DataField="volunteerID"  HeaderText="Volunteer ID" Visible="false" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Vname"  HeaderText="Volunteer Name" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="EMail" HeaderText="EMail"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="City" HeaderText="City"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="State" HeaderText="State"/>
             <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="RoleCode" HeaderText="Role"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="HPhone" HeaderText="Home Phone"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="CPhone" HeaderText="Cell Phone"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ChapterCode" HeaderText="Chapter"/>
                  </COLUMNS>         
               <HeaderStyle BackColor="White" />
    </asp:DataGrid><br />
     <asp:Label ID="HlblParentID" runat="server" Visible="false"></asp:Label>
 </div>
</div>
</asp:Content>

