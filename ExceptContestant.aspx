﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ExceptContestant.aspx.vb" Inherits="ExceptContestant" title="Exceptional  Contestants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <script type="text/javascript"  language="jscript">
        function ConfirmToSave(s) {
            if (confirm(s)) {
                document.getElementById('<%=btnSave.ClientID%>').click();
            }
        }
        function ConfirmToUpdate(s) {
            if (confirm(s)) {
                document.getElementById('<%=btnUpdate.ClientID%>').click();
            }
        }
        </script>
    
     <div style="text-align:left">
        <asp:Button ID="btnSave" runat="server" Text="Save" style="display:none" />
        <asp:Button ID="btnUpdate" runat="server" Text="Update" style="display:none" />
    <br />
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl = "VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp;<br /></div>

<table><tr><td align="center">
<table border = "0" cellpadding="3" cellspacing="0">
<tr><td colspan="8" align="center"><b>Exception List for Contest Registration</b></td></tr>
<tr><td align="left">Chapter</td><td align="left">
    <asp:DropDownList Width="150px" ID="ddlChapter" runat="server"  DataTextField="Chaptercode" DataValueField="ChapterID">
    </asp:DropDownList></td><td align="left"></td>
    <td align="left">Contest Year</td><td align="left">
        <asp:DropDownList ID="ddlEventYear" runat="server">
        </asp:DropDownList>
    </td><td align="left"></td>
    <td align="left">Event</td><td align="left">
        <asp:DropDownList  ID="ddlEvent"  Width="150px" DataTextField="Name" DataValueField="EventID" runat="server"></asp:DropDownList>
    </td></tr>
    <tr><td align="left">Parent</td><td align="left">
        <asp:TextBox  Width="145px" ID="txtParent" runat="server" Enabled="false"></asp:TextBox></td>
        <td align="left">
            <asp:Button ID="Search" runat="server" Text="Search" /></td>
    <td align="left">Child </td><td align="left"><asp:DropDownList Enabled="false" ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField ="ChildNumber" Width="150px" runat="server"></asp:DropDownList></td><td align="left"></td><td align="left"></td><td align="left"></td></tr>
    <tr><td align="left">Product </td><td align="left"><asp:DropDownList  Width="150px" Enabled="false" ID="ddlProduct" DataTextField="ProductCode" DataValueField="ProductID" runat="server"></asp:DropDownList></td><td align="left"></td>
    <td align="left"></td><td align="left"></td><td align="left"></td><td align="left"></td><td align="left"></td></tr>
    <tr><td align="left">New Deadline</td><td align="left">
        <asp:TextBox ID="txtDeadline" Width="145" runat="server"></asp:TextBox></td><td align="left" class="SmallFont">MM/DD/YYYY</td><td align="left"></td><td align="left">
        <asp:Label ID="HlblParentID" runat="server" Visible="false"></asp:Label>
        </td><td align="left"></td><td align="left"></td><td align="left">
        <asp:Label ID="hlblExContestantID" runat="server" Visible="false"></asp:Label>
        </td></tr>
        <tr><td colspan="6" align="center">
            <asp:Button ID="btnSubmit" runat="server" Text="Add" OnClick="btnSubmit_Click" />   &nbsp;&nbsp; <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" /></td><td align="left"></td><td align="left"></td></tr>
        <tr><td colspan="6" align="center">
            <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
            </td><td align="left">&nbsp;</td><td align="left">&nbsp;</td></tr>
</table>
</td></tr><tr><td align="center">
<asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<b> Search NSF member</b>   
<div align = "center">       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
    	<tr>
    	    <td align="right">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	    </td>
    	    <td  align="left">					
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
</div> 
<br />
<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="Left" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                                <asp:BoundField DataField="DonorType" headerText="DonorType" ></asp:BoundField>
                               
         </Columns> </asp:GridView>    
 </asp:Panel>
 </asp:Panel>
 </td></tr><tr><td align="center">
 <asp:Panel runat="server" ID="panel3"   Visible="False">

    <center> <asp:Label ID="Pnl3Msg"  ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>    </center>
     <br />
     <br />
 <asp:DataGrid ID="GridView1" runat="server" DataKeyField="ExContestID" AutoGenerateColumns="False"  OnRowCommand="GridView1_RowCommand" >
        <Columns>
            <asp:Buttoncolumn ButtonType="PushButton" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:buttoncolumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" HeaderText="Delete" />
            <asp:Boundcolumn DataField="ExContestID"  HeaderText="ExContestID" Visible="false" />
            <asp:Boundcolumn DataField="ChapterID"  HeaderText="ChapterID" />
            <asp:Boundcolumn DataField="ContestYear" HeaderText="ContestYear"/>
            <asp:Boundcolumn DataField="EventID" HeaderText="EventID"  ItemStyle-HorizontalAlign="Left" />
            <asp:Boundcolumn DataField="MemberID" HeaderText="MemberID" Visible="false"/>
             <asp:Boundcolumn DataField="ParentName" HeaderText="Parent Name"/>
             <asp:Boundcolumn DataField="ChildNumber" HeaderText="ChildNumber" Visible ="false"/>
              <asp:Boundcolumn DataField="ChildName" HeaderText="Child Name"/>
              <asp:Boundcolumn DataField="email" HeaderText="Email"/>            
            <asp:Boundcolumn DataField="Paid" HeaderText="Paid" />
            <asp:Boundcolumn DataField="ProductID" HeaderText="ProductID" />
            <asp:Boundcolumn DataField="ProductCode" HeaderText="ProductCode" />
            <asp:Boundcolumn DataField="NewDeadline" HeaderText="NewDeadline" DataFormatString="{0:d}"/>
             <asp:Boundcolumn DataField="ProductGroupID" HeaderText="ProductGroupID"/>            
            <asp:Boundcolumn DataField="ProductGroupCode" HeaderText="ProductGroupCode" />
            <asp:Boundcolumn DataField="CreateDate" HeaderText="Date" DataFormatString="{0:d}"/>
            <asp:Boundcolumn DataField="CreatedBy" HeaderText="Created By"/>
            <asp:Boundcolumn DataField="ModifyDate" HeaderText="Modified Date" DataFormatString="{0:d}"/>
            <asp:Boundcolumn DataField="ModifiedBy" HeaderText="Modified By"/>           
        </Columns>
    
    </asp:DataGrid>
    </asp:Panel>
  </td></tr>
  </table>


</asp:Content>

