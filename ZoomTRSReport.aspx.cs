﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NativeExcel;
using System.Collections;
using VRegistration;

public partial class WebExTRSReport : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            lblErrMsg.Text = "";
            if (!IsPostBack)
            {
                loadyear();
                loadPhase();
                fillCoach();

                // TestCreateTrainingSession();
            }
        }
    }

    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    public void ZoomMeetingHistoryReport(string hostID, string startDate, string endDate, string serviceType)
    {
        try
        {
            string URL = string.Empty;
            string service = serviceType;
            URL = "https://api.zoom.us/v1/report/getuserreport";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&user_id=" + hostID + "";
            urlParameter += "&from=" + startDate + "";
            urlParameter += "&to=" + endDate + "";
            urlParameter += "&page_size=300";
            hdnHostID.Value = hostID;
            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }

    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();

                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                string strHtml = string.Empty;
                if (serviceType == "1")
                {

                    strHtml += "<div align='center' style='font-weight:bold;'>Table 1: Zoom Usage Report</div>";
                    strHtml += "<div style='clear:both;'></div>";
                    strHtml += "<div align='left' style='font-weight:bold;'><input type='button' id='btnGenerateReportUsage' value='Export To Excel' /></div>";
                    strHtml += "<div style='clear:both;'></div>";
                    strHtml += "<table style='width:100%; border:1px solid Black; border-collapse:collapse;'>";
                    strHtml += "<tr style='background-color:#FFFFCC; font-weight:bold; font-family:Calibri;'>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:50px; font-family:Calibri;'>Ser#";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:250px; font-family:Calibri;'>Topic";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:120px; font-family:Calibri;'>Meeting Number";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:120px; font-family:Calibri;'>Metting Type";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:120px; font-family:Calibri;'>Date";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:120px; font-family:Calibri;'>Start Time";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:120px; font-family:Calibri;'>End Time";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; width:120px; font-family:Calibri;'>Duration";
                    strHtml += "</td>";
                    //strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Invited";
                    //strHtml += "</td>";
                    //strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Registered";
                    //strHtml += "</td>";
                    //strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Attended";
                    //strHtml += "</td>";
                    strHtml += "</tr>";

                    int serNo = 0;
                    try
                    {

                        foreach (var item in json["meetings"])
                        {

                            if (item["id"].ToString() == hdnSessionKey.Value)
                            {
                                serNo++;
                                strHtml += "<tr >";
                                strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + serNo + "";
                                strHtml += "</td>";
                                strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'><a id='ancConfName' attr-sessionkey=" + item["id"].ToString() + " attr-date= " + item["start_time"].ToString().Substring(0, 10) + " attr-hostID=" + hdnHostID.Value + " attr-duration=" + item["duration"].ToString() + " style='cursor:pointer; color:blue; text-decoration:underline;'>" + item["topic"].ToString() + "</a>";
                                strHtml += "</td>";
                                strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + item["id"].ToString() + "";
                                strHtml += "</td>";
                                strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>Recurring";
                                strHtml += "</td>";

                                strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Convert.ToDateTime(item["start_time"].ToString()).ToString("MM/dd/yyyy") + "";
                                strHtml += "</td>";
                                try
                                {
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Convert.ToDateTime(item["start_time"].ToString()).ToString("HH:mm:ss") + "";
                                    strHtml += "</td>";
                                }
                                catch
                                {
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + item["start_time"].ToString().Substring(9, 10) + "";
                                    strHtml += "</td>";
                                }
                                try
                                {
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Convert.ToDateTime(item["end_time"]).ToString("HH:mm:ss") + "";
                                    strHtml += "</td>";
                                }
                                catch
                                {
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + item["end_time"].ToString().Substring(9, 10) + "";
                                    strHtml += "</td>";
                                }


                                strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + item["duration"].ToString() + "";
                                strHtml += "</td>";

                                strHtml += "</tr>";
                            }
                        }

                        if (serNo <= 0)
                        {
                            strHtml += "<tr><td colspan='8'><center><span style='color:red;'>No record exists.</span></center></td></tr>";
                        }
                    }
                    catch
                    {
                    }
                    strHtml += "</table>";

                    LtrUsageReport.Text = strHtml;

                    if (hdnIsExcel.Value == "1")
                    {
                        ExportToExcelAll(json);
                    }
                }
                else
                {

                    strHtml += "<div align='center' style='font-weight:bold;'>Table 2: Zoom Meeting Attendee Report</div>";
                    strHtml += "<div style='clear:both;'></div>";
                    strHtml += "<div align='left' style='font-weight:bold;'><input type='button' id='btnGenerateReportAttendee' value='Export To Excel' /></div>";
                    strHtml += "<div style='clear:both;'></div>";
                    strHtml += "<table style='width:100%; border:1px solid Black; border-collapse:collapse;'>";
                    strHtml += "<tr style='background-color:#FFFFCC; font-weight:bold; font-family:Calibri;'>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Ser#";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Topic";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Metting Number";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Attendee Name";
                    strHtml += "</td>";

                    //strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>AttendeeID";
                    // strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Date";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Start Time";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>End Time";
                    strHtml += "</td>";
                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse; color:Green; font-family:Calibri;'>Duration";
                    strHtml += "</td>";


                    strHtml += "</tr>";

                    string confName = string.Empty;
                    string SessionNo = string.Empty;
                    string AttendeeEmail = string.Empty;
                    string AttendeeName = string.Empty;
                    string Date = string.Empty;
                    string stTime = string.Empty;
                    string stEndTime = string.Empty;
                    string Duration = string.Empty;
                    object jobject = new object();
                    JArray jParticipant = new JArray();
                    string participant = string.Empty;
                    int ptSerNo = 0;

                    foreach (var item in json["meetings"])
                    {

                        //Response.Write("hai");
                        if (item["id"].ToString() == hdnSessionKey.Value)
                        {
                            confName = item["topic"].ToString();
                            SessionNo = item["id"].ToString();
                            Date = Convert.ToDateTime(item["start_time"].ToString()).ToString("MM/dd/yyyy");
                            stEndTime = item["end_time"].ToString();
                            Duration = item["duration"].ToString();
                            //lblErrMsg.Text = Date.Substring(0, 10) + "" + hdnDate.Value;
                            foreach (var it in item["participants"])
                            {

                                if (Date.Substring(0, 10).Trim() == hdnDate.Value && Duration == hdnDuration.Value)
                                {
                                    ptSerNo++;
                                    string name = it["name"].ToString();
                                    strHtml += "<tr>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + ptSerNo + "";
                                    strHtml += "</td>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + confName + "";
                                    strHtml += "</td>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + SessionNo + "";
                                    strHtml += "</td>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + name + "";
                                    strHtml += "</td>";


                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Date + "";
                                    strHtml += "</td>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Convert.ToDateTime(it["join_time"]).ToString("HH:mm:ss") + "";
                                    strHtml += "</td>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Convert.ToDateTime(it["leave_time"]).ToString("HH:mm:ss") + "";
                                    strHtml += "</td>";
                                    strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Duration + "";
                                    strHtml += "</td>";

                                    //strHtml += "<td style='border:1px solid Black; border-collapse:collapse;'>" + Attended + "";
                                    //strHtml += "</td>";
                                    strHtml += "</tr>";
                                }
                            }
                        }
                    }


                    strHtml += "</table>";

                    if (hdnIsAttendeeExcel.Value == "1")
                    {
                        ExportToExcelAttendees(json);
                    }
                    LtrAttendeeReport.Text = strHtml;


                }

            }


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }


    public void fillCoach()
    {
        string cmdText = "";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.Semester='" + DDLSemester.SelectedValue + "' order by ID.FirstName ASC";
        }
        else
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Semester='" + DDLSemester.SelectedValue + "' and V.Accepted='Y' order by ID.FirstName ASC";
        }
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                DDlCoach.DataSource = ds;
                DDlCoach.DataTextField = "Name";
                DDlCoach.DataValueField = "AutoMemberID";
                DDlCoach.DataBind();
                DDlCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    //DDlCoach.Items.Insert(1, new ListItem("All", "All"));
                }
                else if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlCoach.SelectedValue = Session["LoginId"].ToString();
                }

            }
        }
        catch
        {
        }

    }

    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where  EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + DDlCoach.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and Accepted='Y' and Semester='" + DDLSemester.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where  ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Semester='" + DDLSemester.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                DDlProduct.Enabled = true;
                DDlProduct.DataSource = ds;
                DDlProduct.DataTextField = "ProductCode";
                DDlProduct.DataValueField = "ProductID";
                DDlProduct.DataBind();
                // DDlProduct.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    DDlProduct.SelectedIndex = 0;
                    DDlProduct.Enabled = false;

                }
                else
                {
                    DDlProduct.SelectedIndex = 0;
                    DDlProduct.Enabled = true;
                }
                LoadSessionNo();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void DDlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
    }



    public void GenerateWebExReport()
    {
        dvAttendeeReport.Visible = false;
        string WebExID = string.Empty;
        string Pwd = string.Empty;
        string hostID = string.Empty;
        string cmdText = string.Empty;
        cmdText = "select vl.UserID, vl.Pwd, Vl.HostID,cs.Meetingkey from CalSignup cs inner join VirtualRoomLookup VL on (cs.Vroom=Vl.Vroom) where MemberID=" + DDlCoach.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and Accepted='Y' and ProductId=" + DDlProduct.SelectedValue + " and vl.userID is not null and SessionNo=" + DDlSessionNo.SelectedValue + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                WebExID = ds.Tables[0].Rows[0]["UserID"].ToString();
                Pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();
                hostID = ds.Tables[0].Rows[0]["Hostid"].ToString();
                hdnSessionKey.Value = ds.Tables[0].Rows[0]["Meetingkey"].ToString();
            }
        }

        ZoomMeetingHistoryReport(hostID, TxtStartDate.Text, TxtEndDate.Text, "1");
        //  ListWebExSessionHistoryReport(WebExID, Pwd);


    }


    protected void BtnGenerateReport_Click(object sender, EventArgs e)
    {
        if (validateReport() == "1")
        {
            GenerateWebExReport();
        }
    }

    public void UsageReportHtml()
    {

    }


    public string validateReport()
    {
        string Retval = "1";
        if (DDlCoach.SelectedValue == "0")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please select Coach";
        }
        else if (DDlProduct.SelectedValue == "0")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please select Product";
        }
        else if (TxtStartDate.Text == "")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please fill From Date";
        }
        else if (TxtEndDate.Text == "")
        {
            Retval = "-1";
            lblErrMsg.Text = "Please fill To Date";
        }

        if (TxtStartDate.Text != "" && TxtEndDate.Text != "")
        {
            string startDate = Convert.ToDateTime(TxtStartDate.Text).ToShortDateString();
            string endDate = Convert.ToDateTime(TxtEndDate.Text).ToShortDateString();
            DateTime dtStartDate = new DateTime();
            dtStartDate = Convert.ToDateTime(startDate);

            DateTime dtEndDate = new DateTime();
            dtEndDate = Convert.ToDateTime(endDate);

            TimeSpan t = dtEndDate - dtStartDate;
            double noOfDays = t.TotalDays;
            if (noOfDays > 30)
            {
                Retval = "-1";
                lblErrMsg.Text = "Please select date period maximum of one month";
            }
            if (dtStartDate > dtEndDate)
            {
                Retval = "-1";
                lblErrMsg.Text = "End date should be greater than or equal to start date.";
            }
        }
        return Retval;
    }



    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        try
        {
            dvAttendeeReport.Visible = true;
            string date = Convert.ToDateTime(hdnDate.Value).ToString("MM/dd/yyyy");
            DateTime dtDate = Convert.ToDateTime(date);
            string day = string.Empty;
            string month = string.Empty;
            string year = string.Empty;

            day = dtDate.Day.ToString();
            month = dtDate.Month.ToString();
            year = dtDate.Year.ToString();



            day = day.Length == 1 ? "0" + day : day;
            month = month.Length == 1 ? "0" + month : month;
            year = year.Length == 1 ? "0" + year : year;

            //day = date.Substring(0, 2);
            //month = date.Substring(3, 2);
            //year = date.Substring(6, 4);
            string strDate = year + "-" + month + "-" + day;
            string strServDAte = month + "/" + day + "/" + year;
            hdnDate.Value = strServDAte;
            ZoomMeetingHistoryReport(hdnHostID.Value, strDate, strDate, "2");
        }
        catch
        {
        }
    }
    protected void BtnExportExcelUsage_onClick(object sender, EventArgs e)
    {
        hdnIsExcel.Value = "1";
        hdnIsAttendeeExcel.Value = "0";
        GenerateWebExReport();
    }
    protected void BtnExportAttendee_onClick(object sender, EventArgs e)
    {
        hdnIsAttendeeExcel.Value = "1";
        hdnIsExcel.Value = "0";

        string date = hdnDate.Value;
        string day = string.Empty;
        string month = string.Empty;
        string year = string.Empty;
        day = date.Substring(0, 2);
        month = date.Substring(3, 2);
        year = date.Substring(6, 4);
        string strDate = year + "-" + day + "-" + month;
        string strServDAte = day + "/" + month + "/" + year;
        hdnDate.Value = strServDAte;
        ZoomMeetingHistoryReport(hdnHostID.Value, strDate, strDate, "2");
    }

    public void ExportToExcel(string strHtml)
    {
        hdnIsExcel.Value = "0";
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;


        string filename = "WebExUsageReport_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        Response.ContentType = "application/vnd.ms-excel";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.Write(strHtml.ToString());

        Response.End();

    }

    public void ExportToExcelAttendee(string strHtml)
    {
        hdnIsExcel.Value = "0";
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;


        string filename = "WebExAttendeeReport_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        Response.ContentType = "application/vnd.ms-excel";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.Write(strHtml.ToString());

        Response.End();

    }
    protected void BtnCloseTable2_Click(object sender, EventArgs e)
    {
        dvAttendeeReport.Visible = false;
        BtnCloseTable2.Visible = false;
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + DDlCoach.SelectedValue + "' and V.ProductID=" + DDlProduct.SelectedValue + "";


        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.ProductID=" + DDlProduct.SelectedValue + "";

        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {


            DDlSessionNo.DataTextField = "SessionNo";
            DDlSessionNo.DataValueField = "SessionNo";
            DDlSessionNo.DataSource = ds;
            DDlSessionNo.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                DDlSessionNo.Enabled = true;
            }
            else
            {
                DDlSessionNo.Enabled = false;
            }
        }

    }
    protected void DDlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSessionNo();
    }

    public void ExportToExcelAll(JObject json)
    {
        hdnIsExcel.Value = "0";
        IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
        IWorksheet oSheet = default(IWorksheet);

        oSheet = oWorkbooks.Worksheets.Add();

        oSheet.Name = "Zoom Usage Report";
        oSheet.Range["A1:Q1"].MergeCells = true;
        oSheet.Range["A1"].Value = "Zoom Usage Report";
        oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
        oSheet.Range["A1"].Font.Bold = true;
        oSheet.Range["A1"].Font.Color = Color.Black;


        oSheet.Range["A3"].Value = "Ser#";
        oSheet.Range["A3"].Font.Bold = true;

        oSheet.Range["B3"].Value = "Topic";
        oSheet.Range["B3"].Font.Bold = true;

        oSheet.Range["C3"].Value = "Meeting Number";
        oSheet.Range["C3"].Font.Bold = true;

        oSheet.Range["D3"].Value = "Meeting Type";
        oSheet.Range["D3"].Font.Bold = true;

        oSheet.Range["E3"].Value = "Date";
        oSheet.Range["E3"].Font.Bold = true;

        oSheet.Range["F3"].Value = "Start Time";
        oSheet.Range["F3"].Font.Bold = true;

        oSheet.Range["G3"].Value = "EndTime";
        oSheet.Range["G3"].Font.Bold = true;

        oSheet.Range["H3"].Value = "Duration";
        oSheet.Range["H3"].Font.Bold = true;

        int iRowIndex = 4;
        IRange CRange = default(IRange);
        int i = 0;
        foreach (var item in json["meetings"])
        {
            if (item["id"].ToString() == hdnSessionKey.Value)
            {
                CRange = oSheet.Range["A" + iRowIndex.ToString()];
                CRange.Value = i + 1;

                CRange = oSheet.Range["B" + iRowIndex.ToString()];
                CRange.Value = item["topic"].ToString();

                CRange = oSheet.Range["C" + iRowIndex.ToString()];
                CRange.Value = item["id"].ToString().Replace("&nbsp;", " ");

                CRange = oSheet.Range["D" + iRowIndex.ToString()];
                CRange.Value = "Recurring Meeting";

                CRange = oSheet.Range["E" + iRowIndex.ToString()];
                CRange.Value = item["start_time"].ToString().Substring(0, 10);

                CRange = oSheet.Range["F" + iRowIndex.ToString()];
                CRange.Value = item["start_time"].ToString().Substring(10, 9);

                CRange = oSheet.Range["G" + iRowIndex.ToString()];
                CRange.Value = item["end_time"].ToString().Substring(10, 9);

                CRange = oSheet.Range["H" + iRowIndex.ToString()];
                CRange.Value = item["duration"].ToString();


                iRowIndex = iRowIndex + 1;
                i = i + 1;
            }
        }

        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;

        string filename = "ZoomUsageReport" + "_" + monthDay + "_" + year + ".xls";

        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel");
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
        oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

        Response.End();

    }
    public void ExportToExcelAttendees(JObject json)
    {
        hdnIsAttendeeExcel.Value = "0";
        IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
        IWorksheet oSheet = default(IWorksheet);

        oSheet = oWorkbooks.Worksheets.Add();

        oSheet.Name = "Zoom Attendee";
        oSheet.Range["A1:Q1"].MergeCells = true;
        oSheet.Range["A1"].Value = "Zoom Attendee Usage Report";
        oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
        oSheet.Range["A1"].Font.Bold = true;
        oSheet.Range["A1"].Font.Color = Color.Black;


        oSheet.Range["A3"].Value = "Ser#";
        oSheet.Range["A3"].Font.Bold = true;

        oSheet.Range["B3"].Value = "Topic";
        oSheet.Range["B3"].Font.Bold = true;

        oSheet.Range["C3"].Value = "Meeting Number";
        oSheet.Range["C3"].Font.Bold = true;

        oSheet.Range["D3"].Value = "Attendee Name";
        oSheet.Range["D3"].Font.Bold = true;

        oSheet.Range["E3"].Value = "Date";
        oSheet.Range["E3"].Font.Bold = true;

        oSheet.Range["F3"].Value = "Start Time";
        oSheet.Range["F3"].Font.Bold = true;

        oSheet.Range["G3"].Value = "EndTime";
        oSheet.Range["G3"].Font.Bold = true;

        oSheet.Range["H3"].Value = "Duration";
        oSheet.Range["H3"].Font.Bold = true;

        int iRowIndex = 4;
        IRange CRange = default(IRange);
        int i = 0;
        string confName = string.Empty;
        string SessionNo = string.Empty;
        string AttendeeEmail = string.Empty;
        string AttendeeName = string.Empty;
        string Date = string.Empty;
        string stTime = string.Empty;
        string stEndTime = string.Empty;
        string Duration = string.Empty;
        object jobject = new object();
        JArray jParticipant = new JArray();
        string participant = string.Empty;
        int ptSerNo = 0;
        foreach (var item in json["meetings"])
        {
            participant = item["participants"].ToString();

            if (item["id"].ToString() == hdnSessionKey.Value)
            {
                confName = item["topic"].ToString();
                SessionNo = item["id"].ToString();
                Date = item["start_time"].ToString();
                stEndTime = item["end_time"].ToString();
                Duration = item["duration"].ToString();
                foreach (var it in item["participants"])
                {
                    if (Date.Substring(0, 10) == hdnDate.Value && Duration == hdnDuration.Value)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = confName;

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = SessionNo;

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = it["name"].ToString();

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = Date.Substring(0, 10);

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = it["join_time"].ToString().Substring(10, 9);

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = it["leave_time"].ToString().Substring(10, 9);

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = Duration;


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                }
            }
        }

        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;

        string filename = "ZoomAttendeeUsageReport" + "_" + monthDay + "_" + year + ".xls";

        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel");
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
        oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

        Response.End();

    }
    protected void DDLSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }
    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            DDLSemester.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        DDLSemester.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }
}