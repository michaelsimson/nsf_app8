'**********************************************
'* WebLogger v.1.0
'* Copyright 2003 by Marco Bellinaso
'* Web: http://www.vb2themax.com
'* E-mail: mbellinaso@vb2themax.com
'**********************************************


Partial Class CalSignHelp
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ID = "DatePick"

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        If Request.QueryString("ID") = "Phase" Then
            TrPhase.Visible = True
            TrSession.Visible = False
            TrPref.Visible = False

        ElseIf Request.QueryString("ID") = "Session" Then
            TrPhase.Visible = False
            TrSession.Visible = True
            TrPref.Visible = False

        ElseIf Request.QueryString("ID") = "Pref" Then
            TrPhase.Visible = False
            TrSession.Visible = False
            TrPref.Visible = True

        End If
    End Sub
End Class


