<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Don_athon_schedule.aspx.vb" Inherits="Don_athon_schedule" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Literal ID="ltlTitle" runat="server"></asp:Literal></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">
      function PopupPicker(ctl, w, h) {
          var PopupWindow = null;
          settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
          PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
          PopupWindow.focus();
      }
		</script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	text-align:center;
	width:100%;
	background-color:#FFF;
}
.style2 {
	font-size: 12px;
	font-family: Calibri;
	color: #FFFFFF;
}
-->
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center" style="width :100%" >
    <table  border="1"  cellpadding = "0" cellspacing = "0" bordercolor="#189A2C" style="width:1000px; text-align:center">
    <tr>
    <td>
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:1000px; text-align:center">
    <tr><td> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="right"  >
                      <img src="images_new/img_01.jpg" alt="" width="100%" height="144" border="0" usemap="#Map" />
                        <map name="Map" id="Map">
                          <area shape="circle" coords="105,81,61" href="http://www.northsouth.org/" />
                        </map></td>
                      <td width="76%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="51%"  background="images_new/topbg_1.jpg"><img src="images_new/img_02.jpg" alt="" width="399" height="109" /></td>
                              <td width="49%" valign="top" background="images_new/img_03.jpg">
                              <table width="89%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="30%">&nbsp;</td>         
                                  <td width="70%" valign="top" >
                                  <table width="95%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td valign="middle" style="padding-left:10px; padding-top:5px">
                                        <asp:Menu ID="NavLinks" CssClass="Nav_menu" runat="server" Orientation="Horizontal">
                                        <Items>                    
                                       
                                        </Items>     
                                        </asp:Menu>
                                        </td>
                                      </tr>
                                  </table></td>
                                </tr>
                                <tr><td colspan ="2" align="center" ><br />
                                    <asp:Label Font-Names="Arial Rounded MT Bold" Font-Size="25pt" ForeColor="White" ID="lblHeading" runat="server" ></asp:Label>
                                    </td></tr>
                              </table>
                              </td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="35" valign="middle" background="images_new/menubg.jpg">
<table border = "0" cellpadding = "3" cellspacing = "0" width="100%">
    <tr><td style="text-align:center; width :10px" >  </td><td style="vertical-align:middle; text-align:center;">
        <asp:Label ID="lblPageCaption" Font-Names="Arial Rounded MT Bold" Font-Size="14pt" ForeColor="White" runat="server" ></asp:Label>
        <asp:Literal ID="ltlEvents" runat="server"></asp:Literal> </td></tr> 
    </table>
</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
    </td>
    </tr>
  
      
        <tr><td align="left" style="height :1px">
            &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink></td></tr>
        <tr><td align="center">        
           
    <table cellpadding = "3" border="0" cellspacing = "0">
    <tr><td align="left" > Event</td><td align="left" >  : <asp:DropDownList ID="ddlEvent"  runat="server">  </asp:DropDownList></td></tr>
     <tr><td align="left" > Event Year</td><td align="left" >  : <asp:DropDownList ID="ddlEventYear"  runat="server">  </asp:DropDownList></td></tr>
    <tr><td align="left" >Venue And Time</td><td align="left" >  : 
        <asp:TextBox ID="txtVenue" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RFVtxtVenue" runat="server" 
            ErrorMessage="*" ControlToValidate ="txtVenue"></asp:RequiredFieldValidator>
        </td></tr>
    <tr><td align="left" >Description</td><td align="left" >  : 
        <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RFVtxtDes" runat="server" 
            ErrorMessage="*" ControlToValidate ="txtDescription"></asp:RequiredFieldValidator>
        </td></tr>
    <tr><td align="left" >StartDate</td><td align="left" >  : 
        <asp:TextBox ID="txtStrtDate" runat="server"></asp:TextBox>
         <asp:RequiredFieldValidator ID="RFVtxtStartdate" runat="server" 
            ErrorMessage="*" ControlToValidate ="txtStrtDate"></asp:RequiredFieldValidator>
         &nbsp; <a href="javascript:PopupPicker('txtStrtDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td></tr>
    <tr><td align="left" >EventDate</td><td align="left" >  : 
        <asp:TextBox ID="txtEventDate" runat="server"></asp:TextBox>
        
         <asp:RequiredFieldValidator ID="RFVtxtEventDate" runat="server" 
            ErrorMessage="*" ControlToValidate ="txtEventDate"></asp:RequiredFieldValidator>&nbsp; <a href="javascript:PopupPicker('txtEventDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td></tr>
     <tr><td align="left" >EndDate</td><td align="left" >  : 
         <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
          <asp:RequiredFieldValidator ID="RFVEndDate" runat="server" 
            ErrorMessage="*" ControlToValidate ="txtEndDate"></asp:RequiredFieldValidator>
         &nbsp; <a href="javascript:PopupPicker('txtEndDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td></tr>
    <tr><td align="left" >Cluster Event</td><td align="left" >  : <asp:DropDownList ID="ddlCluster"  runat="server">  </asp:DropDownList></td></tr>
       
    </table>
    
        </td> </tr>
          <tr runat="server" id="trExcelethon" visible="true" >
          <td align="center" style="padding-left:5px; padding-right:5px">
           

              <asp:Literal ID="Literal1" runat="server"></asp:Literal>
           

</td> </tr>
         
                    <tr><td height="5px" align="center"></td> </tr> 
                     <tr><td  align="center">
                         <asp:Button ID="BtnSubmit" OnClick ="BtnSubmit_Click" runat="server" Text="Submit" /><br>
                         
                         <asp:Label ID="lblErr" runat="server" ForeColor = "Red"></asp:Label>
                     </td> </tr> 
                     
                     <tr><td align="center">
                     <asp:DataGrid ID="DGAthon" Width="800px" runat="server" DataKeyField="DonateAThonID" 
        AutoGenerateColumns="False" 
        CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>
          <%-- <ASP:TemplateColumn>
           <ItemTemplate>
						<asp:LinkButton id="lbtnRemove" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"  Visible  = '<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:LinkButton>
			</ItemTemplate>
						</ASP:TemplateColumn>--%>
           <asp:Boundcolumn DataField="DonateAThonID"  HeaderText="DonateAThonID" Visible="false" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="EventYear"  HeaderText="EventYear" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ClusterCode" HeaderText="ClusterCode"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="VenueAndTime" HeaderText="VenueAndTime"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Description" HeaderText="Description"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="StartDate" HeaderText="StartDate" DataFormatString="{0:d}"/>
               <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EndDate" HeaderText="EndDate" DataFormatString="{0:d}"/>
                <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EventDate" HeaderText="EventDate" DataFormatString="{0:d}"/>
             <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Name" HeaderText="Event_Name"/>          
            
        </Columns>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>
                     </td></tr>
            <tr><td align="right"  bgcolor="#99CC33" style="height:25px; vertical-align:middle ;" class="style2">
                � North South Foundation. All worldwide rights reserved.
<a class="btn_01" href="/public/main/privacy.aspx">Copyright</a>&nbsp;&nbsp;&nbsp;
</td> 
            </tr>
    </table>
   
        </div>
    </form>
</body>
</html>
