﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="Game_Reg.aspx.cs" Inherits="Game_Reg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<asp:LinkButton ID="lbtnVolunteerFunctions" runat="server" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions</asp:LinkButton>
<div align="center" style="font-size: large" ><strong>Game Registrations</strong></div>
<br />
<div align="center" >
    <asp:DropDownList ID="ddlChoice" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlChoice_SelectedIndexChanged">
        <asp:ListItem Value="-1">[Select Status]</asp:ListItem>
        <asp:ListItem Value="1">Approved</asp:ListItem>
        <asp:ListItem Value="2">Not Approved</asp:ListItem>
    </asp:DropDownList>
    </div>
<br />
 <div align="center" >
     <asp:Panel ID="PnlMain" runat="server" Height="97px" Visible="False">
     <table style="width: 25%; height: 82px;">
         <tr>
             <td align="left" style="width: 82px; " nowrap="nowrap">LoginID</td>
             <td align="left" style="width: 129px">
                 <asp:TextBox ID="txtLoginId" runat="server" Width="125px"></asp:TextBox>
             </td>
             <td align="left" style="width: 26px;" nowrap="nowrap"></td>
             <td align="left" nowrap="nowrap">Start Date</td>
             <td align="left" style="width: 128px;" nowrap="nowrap">
                <span style="font-size: xx-small"><strong>
                 <asp:TextBox ID="txtStartDate" runat="server" Width="125px"></asp:TextBox>
                 (MM/DD/YYYY)<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtStartDate" Display="Dynamic" ErrorMessage="Invalid Date" SetFocusOnError="True" ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$" style="font-size: x-small"></asp:RegularExpressionValidator>
                 </strong></span>
             </td>
         </tr>
         <tr>
             <td align="left" style="width: 82px; height: 12px;" nowrap="nowrap">Password</td>
             <td align="left" style="width: 129px; height: 12px;" nowrap="nowrap">
                 <asp:TextBox ID="txtPassword" runat="server" Width="125px"></asp:TextBox>
             </td>
             <td align="left" nowrap="nowrap" style="height: 12px; width: 26px"></td>
             <td align="left" nowrap="nowrap" style="height: 12px">End Date</td>
             <td align="left" nowrap="nowrap" style="width: 244px; height: 12px;">
                 <asp:TextBox ID="txtEndDate" runat="server" Width="125px"></asp:TextBox>
                <span style="font-size: xx-small"><strong>(MM/DD/YYYY)</strong></span><asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtEndDate" Display="Dynamic" ErrorMessage="Invalid Date" SetFocusOnError="True" ValidationExpression="^([1-9]|0[1-9]|1[012])[- /.]([1-9]|0[1-9]|[12][0-9]|3[01])[- /.][0-9]{4}$" style="font-size: x-small; font-weight: 700"></asp:RegularExpressionValidator>
             </td>
         </tr>
         <tr>
             <td align="left" style="width: 82px">&nbsp;</td>
             <td align="left" style="width: 129px" nowrap="nowrap">
                 <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="60px" OnClick="Button1_Click" />
                 <asp:Button ID="Button2" runat="server" Text="Cancel" OnClick="Button2_Click" />
             </td>
             <td align="left" nowrap="nowrap" style="width: 26px">&nbsp;</td>
             <td align="left" nowrap="nowrap">Approved</td>
             <td align="left" nowrap="nowrap" style="width: 128px">
                 <asp:DropDownList ID="ddlApproved" runat="server">
                     <asp:ListItem Value="-1" Selected="True">[Status]</asp:ListItem>
                     <asp:ListItem>Yes</asp:ListItem>
                     <asp:ListItem>No</asp:ListItem>
                     <asp:ListItem>NULL</asp:ListItem>
                 </asp:DropDownList>
             </td>
         </tr>
     </table>

     </asp:Panel>
    </div>
<br />
 <div align="left"  >

     <asp:Label ID="lblStatus" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>

 </div>
    <div align="center" >
        <asp:GridView ID="gvGame" runat="server" AllowPaging="True" EnableModelValidation="True" OnPageIndexChanging="gvGame_PageIndexChanging" OnRowCommand="gvGame_RowCommand" OnRowDataBound="gvGame_RowDataBound" PageSize="25" OnSelectedIndexChanged="gvGame_SelectedIndexChanged">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Modify" />
            </Columns>
            <SelectedRowStyle BackColor="#66FFFF" />
        </asp:GridView>

    </div>
</asp:Content>

