<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="emaillist_E.aspx.vb" ValidateRequest="false" Inherits="emaillist_E" title="Untitled Page" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<script type="text/javascript" language="javascript">

function hide_fromdropdown(var1)
{
document.getElementById(var1).style.display = "none";;
}
function hide_fromtext(var1)
{
document.getElementById(var1).style.display = "none";
}

</script>

<table style="width: 680px">
<tr><td style="width: 300px; height: 18px;">
<asp:HyperLink ID="hybbackvol" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="254px" ></asp:HyperLink>
</td><td><asp:HyperLink ID="Hypbackemail" runat="server" Text="Back to Email" Width="284px" ></asp:HyperLink>
</td></tr></table>
<table id="tableemail" cellspacing="1" cellpadding="3" width="700" style="margin-left:10px" border="0" runat="server">
<tr><td><asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label> </td></tr>
      <tr><td><asp:Label ID="lblfrom" runat="server" Text="From:" Width="50px" ></asp:Label> 
      <asp:TextBox ID="txtfrom" Text="nsfindiascholarships@gmail.com" runat="server" BackColor="#FFE0C0" ></asp:TextBox>
      <asp:DropDownList Visible="false" ID="drpfrom" runat="server">
      <asp:ListItem Text="Select From Email" Value="0" Selected="True"></asp:ListItem>
      <asp:ListItem Text="nsfcontests@northsouth.org" Value="1"></asp:ListItem>
      <asp:ListItem Text="nsfmathcounts@northsouth.org" Value="2"></asp:ListItem>
      <asp:ListItem Text="nsfcontests@gmail.com" Value="1"></asp:ListItem>
      <asp:ListItem Text="nsfmathcounts@gmail.com" Value="2"></asp:ListItem>
      <asp:ListItem Text="nsfscience@gmail.com" Value="3"></asp:ListItem>
      <asp:ListItem Text="nsfvolunteer@gmail.com" Value="4"></asp:ListItem>
      <asp:ListItem Text="nsfpremathcounts@gmail.com" Value="5"></asp:ListItem>
      <asp:ListItem Text="nsfsatact@gmail.com" Value="6"></asp:ListItem>
           </asp:DropDownList> 
      <asp:Label ID="lblfromemailerror" runat="server" ForeColor="red" ></asp:Label>
      <%--<asp:RequiredFieldValidator id="reqFrommEmaiID" Text="Select From EmailID" InitialValue="0" ControlToValidate="drpfrom" Runat="server" />--%>
        <%--<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ControlToValidate="txtfrom" 
      ErrorMessage="You must enter an Correct email address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
</asp:RegularExpressionValidator>--%>
</td></tr>
      <tr>
    <td><asp:Label ID="lblemailsub" runat="server" Text="Please Type the Subject of the Email Below" ></asp:Label> 
    </td></tr><tr><td style="height: 14px">
    <asp:TextBox id="txtEmailSubject" Text="India Scholarships news flash" runat="server" Width="800px" MaxLength="1000" BackColor="#FFE0C0"></asp:TextBox>&nbsp;
    </td></tr>
     <tr style="height: 25px;">
    <td><asp:Label ID="lblattfile" Text="File to be Attached:" runat="server"></asp:Label> <asp:FileUpload ID="AttachmentFile" runat="server"/></td>
    
    </tr>
    <tr style="height: 25px;">
    <td><asp:Label ID="Label1" Text="File to be Attached:" runat="server"></asp:Label> <asp:FileUpload ID="AttachmentFile1" runat="server"/></td>
    
    </tr>
    <tr><td>
		<asp:Label ID="lblemailbody" Text="Please Type the Body of the Email Below" runat="server" ></asp:Label> 
        <FTB:FreeTextBox id="txtEmailBody"  runat="Server" Width="99%" Focus="false" SupportFolder="aspnet_client/FreeTextBox/" />
    </td></tr>
    <tr><td>
				<asp:Button id="btnsendemail" runat="server" OnClick ="btnsendemail_Click" Text="Send Email" CssClass="FormButton"></asp:Button>
			</td></tr><tr><td>
					<asp:label id="lblEMailError" runat="server"></asp:label></td></tr>
		</table>
                    <table>
                    <tr ><td><asp:Label ID="lblsentemailnote" runat="server" Text="**NOTE: When giving your own list make sure than they should not be any gap/space after comma (,) or  between emails eg: xxx@xx.com,aaa@aa.com " Visible="false"></asp:Label></td></tr>
					<tr><td style="height:5px"><asp:TextBox id="txtSendList" runat="server"  Enabled="false" Width="800px" TextMode="MultiLine" BorderColor="Black">
					</asp:TextBox></td></tr>
    </table>
</asp:Content>

