Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Net.Mail
Imports System.IO
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data

Namespace VRegistration
    Partial Class ChangeEmail
        Inherits System.Web.UI.Page
        Private Sub Page_Load(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Dim SecureUrl As String
            If LCase(Session("LoggedIn")) <> "true" Then
                txtPassword.Enabled = True
                ''Server.Transfer("maintest.aspx") Commented on 13/09/2013
            Else
                If Session("entryToken").ToString() = "Parent" And Not Request.QueryString("ID") = 1 Then
                    hlinkParentRegistration.Visible = True
                ElseIf Session("entryToken").ToString() = "Volunteer" Then
                    hlinkVolunteerRegistration.Visible = True
                ElseIf Session("entryToken").ToString() = "Donor" Then
                    hlinkDonorFunctions.Visible = True
                End If
                If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost" And Not (Request.Url.Host = "ferdine")) Then
                    SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                    Response.Redirect(SecureUrl, True)
                End If
                If Request.QueryString("CustServ") = 1 Then 'Session("RoleID") = 29 Then
                    txtPassword.Enabled = False
                    rfvPassword.Enabled = False
                    lblPwdError.Text = "Not Needed"
                Else
                    txtPassword.Enabled = True
                    rfvPassword.Enabled = True
                End If
            End If
        End Sub
        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            lblSuccess.Text = ""
            If txtNewEmail.Text.Length < 3 Or txtOldEmail.Text.Length < 3 Or (txtPassword.Text.Length < 1 And Request.QueryString("CustServ") <> 1) Or txtConfirm.Text.Length < 3 Then
                txtErrMsg.Visible = True
                txtErrMsg.Text = "Please Enter all Datas Correctly"
            Else
                If txtNewEmail.Text = txtConfirm.Text Then
                    '1. make sure the user account is valid 
                    Dim conn, conn1 As New SqlConnection(Application("ConnectionString"))
                    Dim StrSql, StrSql1 As String
                    Dim loginID As String = ""  'Login_master
                    Dim isDuplicate As Boolean = True
                    Dim entryToken As String = Session("entryToken")
                    txtErrMsg.Visible = False
                    'Checks for more email being in login_master table more than once
                    Dim C_ELogincnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) from child where Email='" & txtNewEmail.Text & "'")
                    If C_ELogincnt > 0 Then
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "The current email you entered already exists in Child record."
                        Exit Sub
                    End If
                    Dim O_ELogincnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) from login_master where user_email='" & txtOldEmail.Text & "'")
                    If O_ELogincnt = 0 Then
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "The current email you entered doesn�t have a password associated with it to log in.  Please provide the information."
                        '"The current email you entered does not appear in your profile.Please create a record and update."
                        '/********* Added on 28-01-2014 to create records if present in IndSpouse and not in login_master **********************************************/
                        btnUpdate.Enabled = False
                        txtNewUserEmail.Text = txtNewEmail.Text
                        txtNewUserEmail.Enabled = False
                        tblLoginmaster.Visible = True

                    ElseIf O_ELogincnt < 2 Then
                        'authenticate
                        loginID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text)))
                        If (loginID <> "") Or (loginID = "" And Request.QueryString("CustServ") = 1) Then
                            '2. if yes,make sure the new email is unique in Login_master
                            isDuplicate = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_IsDuplicateEmail", New SqlParameter("@NewEmail", Server.HtmlEncode(txtNewEmail.Text)))
                            If (isDuplicate = "False") Then
                                '3. if not duplicate, 
                                'a. check if the original email is present only once on indspouse,
                                'if yes, update it and redirect to registration.aspx.  
                                'if no, add change request and show confirmation
                                Dim O_EIndCount As Integer = 0
                                Dim N_EIndCount As Integer = 0
                                Try
                                    'Checking OldEmail Count in Indspouse
                                    O_EIndCount = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail.Text)))
                                    N_EIndCount = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtNewEmail.Text)))
                                Catch se As SqlException
                                    '?
                                End Try

                                If O_EIndCount = 0 And N_EIndCount = 0 Then
                                    'MsgBox("1")
                                    'No IndSpouse record exists with old email & new email
                                    StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','No IndSpouse record exists with old email or new email',GetDate())"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 0 And N_EIndCount > 0 Then
                                    'MsgBox("2")
                                    'New Email existing in Indspouse
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','If allowed, the user will get access to an existing record, which may not be him',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 1 And N_EIndCount = 0 Then
                                    'MsgBox("3")
                                    'One IndSpouse record exists with old email and No new email Exist
                                    Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                                    StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 1 And N_EIndCount = 1 Then
                                    ' MsgBox("4")
                                    Dim OldMemberID, OldRelationship, NewMemberID, NewRelationship, volcount As Integer
                                    Dim OldDonorType, NewDonorType As String
                                    'New Email & Old Email Exist Once in Indspouse
                                    Dim readr1 As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "Select automemberid as OldMemberID, Relationship as OldRelationship, DonorType as OldDonorType from IndSpouse where email = '" & txtOldEmail.Text & "'")
                                    While readr1.Read()
                                        OldMemberID = readr1("OldMemberID")
                                        OldRelationship = readr1("OldRelationship")
                                        OldDonorType = readr1("OldDonorType")
                                    End While
                                    readr1.Close()
                                    Dim readr2 As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "Select automemberid as NewMemberID, Relationship as NewRelationship, DonorType as NewDonorType from IndSpouse where email ='" & txtNewEmail.Text & "'")
                                    While (readr2.Read())
                                        NewMemberID = readr2("NewMemberID")
                                        NewRelationship = readr2("NewRelationship")
                                        NewDonorType = readr2("NewDonorType")
                                    End While
                                    readr2.Close()
                                    volcount = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) as VolCount from volunteer where memberid in (" & OldMemberID & "," & NewMemberID & ")")
                                    'to check whether two people are spouses  or not 
                                    If (OldRelationship = 0 And NewRelationship = 0) Or (OldRelationship > 0 And NewRelationship > 0) Or (OldRelationship > 0 And (OldRelationship <> NewMemberID Or OldDonorType <> "SPOUSE" Or NewDonorType <> "IND")) Or (NewRelationship > 0 And (NewRelationship <> OldMemberID Or NewDonorType <> "SPOUSE" Or OldDonorType <> "IND")) Then
                                        'Not Spouses
                                        'MsgBox((NewRelationship > 0 And (NewRelationship <> OldMemberID Or NewDonorType <> "SPOUSE" Or OldDonorType <> "IND")))
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID, OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','NewEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & NewMemberID & "," & OldMemberID & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplayFailMsg.Visible = True
                                        tblEmail.Visible = False
                                    Else
                                        If volcount <> 0 Or Session("entryToken") = "Volunteer" Then
                                            'One IndSpouse record exists with old email and One new email Exist
                                            txtErrMsg.Visible = True
                                            txtErrMsg.Text = "Email already exists.  Must be unique in a volunteer family."
                                        Else
                                            StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                            StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & OldMemberID & " where email = '" & txtOldEmail.Text & "'"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                            StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, NewMemberID, OldMemberID) values('"
                                            StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & NewMemberID & "," & OldMemberID & ")"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                            DisplaySuccessMsg.Visible = True
                                            tblEmail.Visible = False
                                        End If
                                    End If
                                ElseIf O_EIndCount = 1 And N_EIndCount > 1 Then
                                    'New Email existing in Indspouse
                                    'MsgBox("7. New Email existing in Indspouse")
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','NewEmail exists in multiple records; at least one elsewhere outside',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg5.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 2 And N_EIndCount = 0 Then
                                    'Old Email exist two times to Check whether they are Spouses
                                    Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn1, CommandType.Text, "SELECT a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & txtOldEmail.Text & "' AND b.Email = '" & txtOldEmail.Text & "' AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
                                    If readr.Read() Then
                                        'One IndSpouse record exists with old email 
                                        'MsgBox("8. One IndSpouse record exists with old email")
                                        Dim OldMemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                                        StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                        StrSql = "update indspouse set ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & OldMemberID & " where email = '" & txtOldEmail.Text & "'"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & OldMemberID & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplaySuccessMsg.Visible = True
                                        tblEmail.Visible = False
                                    Else
                                        'MsgBox("8. Not Spouses One IndSpouse record exists with old email")
                                        StrSql1 = "Insert Into ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','OldEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplayFailMsg6.Visible = True
                                        tblEmail.Visible = False
                                    End If
                                    readr.Close()
                                ElseIf O_EIndCount = 2 And N_EIndCount > 0 Then
                                    'MsgBox("9. New Email existing more than zero OLD Email two times in Indspouse")
                                    'New Email existing more than zero  & OLD Email two times in Indspouse 
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','OldEmail has duplicates and NewEmail already exists',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail.Text))) & "," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg2.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount > 2 Then
                                    'MsgBox("10 OLD Email more than two times in Indspouse ")
                                    'OLD Email more than two times in Indspouse 
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','OldEmail exists in more than two places',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg3.Visible = True
                                    tblEmail.Visible = False
                                End If
                            Else
                                'duplicate email , error out
                                Dim dupemail_loginmaster As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) as IndCount from Indspouse where Email='" & txtNewEmail.Text & "'")
                                If dupemail_loginmaster > 0 Then
                                    txtErrMsg.Visible = True
                                    txtErrMsg.Text = "The new email address is already present, please correct if there is an error."
                                Else
                                    tblEmail.Visible = False
                                    trEmailChangeNw.Visible = True
                                    If Request.QueryString("CustServ") = 1 Then
                                        txtNewPassword.Enabled = False
                                        lblEmailChangeNew.Text = "The new email address already exists as LoginID along with a password."
                                        lblNewPwdError.Text = "NotNeeded"
                                    Else
                                        lblEmailChangeNew.Text = "The new email address already exists as LoginID along with a password.  Please provide the password."
                                    End If
                                End If
                            End If

                        Else
                            '2. if no, error out
                            'Invalid email and  or password
                            txtErrMsg.Visible = True
                            txtErrMsg.Text = "The pwd you entered is not correct."
                        End If
                    Else
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "We have discovered a problem with your email not being unique. Please send an email to nsfcontests@northsouth.org"  'nsfcontests@gmail.com"
                    End If
                Else
                    txtErrMsg.Visible = True
                    txtErrMsg.Text = "The 'New Email Address' does not match with the 'Confirm New Email Address'."
                End If
            End If
            'If Request.QueryString("ID") = 1 And (Session("EventID") = "1" Or Session("EventID") = "2") Then
            '    Session("LoginEmail") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSPouse where AutoMemberID=" & Session("LoginID"))
            '    'hlinkBack.PostBackUrl = "ContestSelectionSum.aspx"
            '    Response.Redirect("ContestSelectionSum.aspx")
            'ElseIf Request.QueryString("ID") = 2 And ("EventID") = "13" Then
            '    Response.Redirect("CoachingSelectionSum.aspx")
            'Else
            '    Response.Redirect("Http://northsouth.org")
            'End If

        End Sub
        Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            Dim sFrom As String = "nsfcontests@northsouth.org" '  "nsfcontests@gmail.com"
            Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            mail.IsBodyHtml = True

            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Try
                client.Send(mail)
            Catch e As Exception
                ok = False
            End Try
        End Sub

        Protected Sub btnUpdateNw_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim loginid As String = ""
            Dim StrSql As String
            loginid = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtNewPassword.Text)))
            If (loginid <> "" Or (loginid = "" And Request.QueryString("CustServ") = 1)) Then
                Dim O_EIndCount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail.Text)))
                If O_EIndCount = 1 Then
                    Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                    StrSql = "Delete From Login_Master where user_email='" & txtOldEmail.Text & "'"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                    StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "'"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                    StrSql = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                    StrSql = StrSql & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "',0,'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                    trEmailChangeNw.Visible = False
                    DisplaySuccessMsg.Visible = True
                    tblEmail.Visible = False
                ElseIf O_EIndCount = 2 Then
                    'Now two indspouse record exist so check whether they are Spouse
                    Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT (I.AutoMemberID) from IndSpouse I inner Join IndSpouse I1 ON I.Relationship = I1.AutoMemberID Where I.Email = I1.Email and I.Email ='" & txtOldEmail.Text & "'")
                    If count = 1 Then
                        Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                        StrSql = "Delete From Login_Master where user_email='" & txtOldEmail.Text & "'"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "'"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        StrSql = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                        StrSql = StrSql & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "',0,'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        trEmailChangeNw.Visible = False
                        DisplaySuccessMsg.Visible = True
                        tblEmail.Visible = False
                    Else
                        'MsgBox("8. Not Spouses One IndSpouse record exists with old email")
                        StrSql = "Insert Into ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                        StrSql = StrSql & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "',0,'Pending','OldEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        trEmailChangeNw.Visible = False
                        DisplayFailMsg6.Visible = True
                        tblEmail.Visible = False
                    End If
                End If
            Else
                txtErrMsg.Visible = True
                txtErrMsg.Text = "Invalid New  Password. Please correct and Redo if there is an error."
                trEmailChangeNw.Visible = False
                tblEmail.Visible = True
            End If
            'If Request.QueryString("ID") = 1 And (Session("EventID") = "1" Or Session("EventID") = "2") Then
            '    Session("LoginEmail") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSPouse where AutoMemberID=" & Session("LoginID"))
            '    'hlinkBack.PostBackUrl = "ContestSelectionSum.aspx"
            '    Response.Redirect("ContestSelectionSum.aspx")
            'ElseIf Request.QueryString("ID") = 2 And ("EventID") = "13" Then
            '    Response.Redirect("CoachingSelectionSum.aspx")
            'Else
            '    Response.Redirect("Http://northsouth.org")
            'End If
        End Sub

        Protected Sub hlinkBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkBack.Click
            If Request.QueryString("ID") = 1 And (Session("EventID") = "1" Or Session("EventID") = "2") Then
                Session("LoginEmail") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSPouse where AutoMemberID=" & Session("LoginID"))
                Response.Redirect("ContestSelectionSum.aspx")
            ElseIf Request.QueryString("ID") = 2 And ("EventID") = "13" Then
                Response.Redirect("CoachingSelectionSum.aspx")
            Else
                Response.Redirect("Http://northsouth.org")
            End If
        End Sub

        Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
            '/********* Added on 28-01-2014 to create records if present in IndSpouse and not in login_master **********************************************/
            Dim StrSql, StrSql1, StrSql2 As String
            Dim O_EIndCount, N_EIndCount As Integer
            Try
                Dim OldMemberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewUserEmail.Text)))
                'Dim StrSql As String
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ChapterID,Chapter from IndSpouse where email='" & txtOldEmail.Text & "'")
                O_EIndCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail.Text)))
                N_EIndCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtNewEmail.Text)))
                Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))

                If ds.Tables(0).Rows.Count > 0 Then
                    'Add Record to login_master
                    StrSql = "Insert into Login_Master (user_email,user_pwd,date_added,last_login,user_role, user_name, chapter, ChapterID, active, CreateDate, CreatedBy) Values ('" & txtNewUserEmail.Text & "','" & txtNewUserPwd.Text & "',GETDATE(),GETDATE() ,'" & Session("entryToken").ToString() & "','" & txtNewUserName.Text & "','" & ds.Tables(0).Rows(0)("Chapter") & "'," & ds.Tables(0).Rows(0)("ChapterID") & ",'Yes',GetDate(),'" & Session("LoginID") & "')"
                    'Update email to IndSpouse table
                    StrSql1 = "update indspouse set ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "'"
                    'Add Detail to ChangeEmail table
                    StrSql2 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('" & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist in login_master',GetDate()," & AutomemberID & ")"

                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Select Count(*) From login_master where user_email='" & txtNewUserEmail.Text & "'") > 0 Then
                        lblSuccess.Text = "Email already exists."
                        tblLoginmaster.Visible = False
                        btnUpdate.Enabled = True
                    Else
                        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql) > 0 Then  ''Add Record to login_master
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql1)  'Update email to IndSpouse table
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSql2)  'Add Detail to ChangeEmail table

                            txtErrMsg.Text = ""
                            tblLoginmaster.Visible = False
                            DisplaySuccessMsg.Visible = True
                            tblEmail.Visible = False
                        End If

                    End If
                    btnUpdate.Enabled = True
                Else
                    txtErrMsg.Text = "Current Email already exists.Please verify the data."
                    tblLoginmaster.Visible = False
                    btnUpdate.Enabled = True
                End If

            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End Sub

        Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
            tblLoginmaster.Visible = False
            btnUpdate.Enabled = True
        End Sub
    End Class
End Namespace

