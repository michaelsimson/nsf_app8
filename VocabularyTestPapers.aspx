﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="VocabularyTestPapers.aspx.cs" Inherits="VocabularyTestPapers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     


    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
         <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>
        </tr>
        <tr>

            <td>
                   <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong> Vocabulary Test Papers</strong> </div>
            </td>
        </tr>
       
      <tr>

            <td align="right">
              
            </td>
        </tr>
       <tr>
           <td colspan="2" align="center">
               

                            <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
 <table style="margin-left: auto; margin-right: auto; font-weight: bold" runat="server" id="tblCoach" visible="true" >
                     
                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" AutoPostBack="True" runat="server" >
                            </asp:DropDownList>
                        </td>
                        
                        <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" AutoPostBack="True" runat="server"  Width="110px" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" >
                                  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Finals</asp:ListItem>
                          <asp:ListItem Value="2">Chapter</asp:ListItem>
                        
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList style="width: 168px;" ID="ddlProduct" runat="server" AutoPostBack="True"  Width="110px"  >
                                <asp:ListItem Value="0">Select one</asp:ListItem>
                          <asp:ListItem Value="JVB">Junior Vocabulary</asp:ListItem>
                          <asp:ListItem Value="IVB">Intermediate Vocabulary</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>Set</td>
                        <td>
                             <asp:DropDownList ID="ddlSet" runat="server" AutoPostBack="True"   Width="90px" >
                                  <asp:ListItem Value="0">Select</asp:ListItem>
                          <asp:ListItem Value="1">1</asp:ListItem>
                          <asp:ListItem Value="2">2</asp:ListItem>
                          <asp:ListItem Value="3">3</asp:ListItem>
                          <asp:ListItem Value="4">4</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>Phase</td>
                        <td>
                             <asp:DropDownList ID="ddlPhase" runat="server" AutoPostBack="True"   Width="110px" >
                                  <%--<asp:ListItem Value="0">All</asp:ListItem>--%>
                          <asp:ListItem Value="1">Phase I</asp:ListItem>
                          <asp:ListItem Value="2">Phase II</asp:ListItem>
                          <asp:ListItem Value="3">Phase III</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                       <td>Action</td>
                        <td>
                             <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True"   Width="165px" >
                                  <asp:ListItem Value="0">Select One</asp:ListItem>
                          <asp:ListItem Value="1">Generate Words</asp:ListItem>
                          <asp:ListItem Value="2">Generate PDF Reports</asp:ListItem>
                          <asp:ListItem Value="3">Display Words</asp:ListItem>
                          <asp:ListItem Value="4">Modify Words</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                    </tr>
     <tr>
         <td colspan="10" align="center"><asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"  />
                        <br />
                        <asp:Label ID="lblErr" runat="server" Font-Bold="true"></asp:Label>
                     </td>
         

     </tr>
     <tr align="center">

         <td colspan="12"><div style="width:1000px;height:400px; overflow:auto;">
             
             <asp:GridView ID="gvVocabwords_new" runat="server" EnableModelValidation="True" AutoGenerateColumns="False" OnRowEditing="gvVocabwords_new_RowEditing" OnRowUpdating="gvVocabwords_new_RowUpdating">
                 <Columns>
                     <asp:TemplateField HeaderText="Action">
                         <ItemTemplate>
                             <asp:Button ID="Button1" runat="server" Text="Modify" CommandName="Edit" />
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="VocabTPID">
                         <ItemTemplate>
                             <asp:Label ID="Label1" runat="server" Text='<%#Eval("VocabTPID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Word">
                         <ItemTemplate>
                             <asp:Label ID="Label2" runat="server" Text='<%#Eval("Word") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                      <asp:TemplateField HeaderText="Unpub">
                         <ItemTemplate>
                             <asp:Label ID="Label245" runat="server" Text='<%#Eval("Unpub") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Level">
                         <ItemTemplate>
                             <asp:Label ID="Label3" runat="server" Text='<%#Eval("Level") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="SubLevel">
                         <ItemTemplate>
                             <asp:Label ID="Label4" runat="server" Text='<%#Eval("SubLevel") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Year">
                         <ItemTemplate>
                             <asp:Label ID="Label5" runat="server" Text='<%#Eval("Year") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                       <asp:TemplateField HeaderText="EventID">
                         <ItemTemplate>
                             <asp:Label ID="Label62" runat="server" Text='<%#Eval("EventID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Event">
                         <ItemTemplate>
                             <asp:Label ID="Label6" runat="server" Text='<%#Eval("Event") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ProductGroupID">
                         <ItemTemplate>
                             <asp:Label ID="Label7" runat="server" Text='<%#Eval("ProductGroupID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ProductGroupCode">
                         <ItemTemplate>
                             <asp:Label ID="Label8" runat="server" Text='<%#Eval("ProductGroupCode") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ProductID">
                         <ItemTemplate>
                             <asp:Label ID="Label9" runat="server" Text='<%#Eval("ProductID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ProductCode">
                         <ItemTemplate>
                             <asp:Label ID="Label10" runat="server" Text='<%#Eval("ProductCode") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="SetNo">
                         <ItemTemplate>
                             <asp:Label ID="Label11" runat="server" Text='<%#Eval("SetNo") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Phase">
                         <ItemTemplate>
                             <asp:Label ID="Label12" runat="server" Text='<%#Eval("Phase") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="CreatedBy">
                         <ItemTemplate>
                             <asp:Label ID="Label13" runat="server" Text='<%#Eval("CreatedBy") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="CreateDate">
                         <ItemTemplate>
                             <asp:Label ID="Label14" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ModifiedBy">
                         <ItemTemplate>
                             <asp:Label ID="Label15" runat="server" Text='<%#Eval("ModifiedBy") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="ModifyDate">
                         <ItemTemplate>
                             <asp:Label ID="Label16" runat="server" Text='<%#Eval("ModifyDate") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
             
             </div></td>

     </tr>


                  
                  
                </table>
               </td>

       </tr>

  
        
                </table>
       </asp:Content>
    