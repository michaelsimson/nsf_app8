Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class GameSearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' hlnkback.NavigateUrl = "VolunteerFunctions.aspx"
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        lblnodata.Text = ""

        If Not Page.IsPostBack Then
            If Session("entryToken") = "Parent" Then
                lnkmain.Text = "Back to Parent Functions Page"
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblVolselection.ClientID & "').style.display = 'none';</script>")
            ElseIf Session("entryToken") = "Donor" Then
                lnkmain.Text = "Back to Donor Functions Page"
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblVolselection.ClientID & "').style.display = 'none';</script>")
            ElseIf Session("entryToken") = "Volunteer" Then
                lnkmain.Text = "Back to Volunteer Functions Page"
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblsearch.ClientID & "').style.display = 'none';</script>")
            End If
        Else
            If Session("entryToken") = "Parent" Then
                lnkmain.Text = "Back to Parent Functions Page"
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblVolselection.ClientID & "').style.display = 'none';</script>")
            ElseIf Session("entryToken") = "Donor" Then
                lnkmain.Text = "Back to Donor Functions Page"
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblVolselection.ClientID & "').style.display = 'none';</script>")
            ElseIf Session("entryToken") = "Volunteer" Then
                lnkmain.Text = "Back to Volunteer Functions Page"
                If drpvolselection.SelectedIndex = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblsearch.ClientID & "').style.display = 'none';</script>")
                ElseIf drpvolselection.SelectedValue = 1 Then
                    btnsubmit.Enabled = False
                ElseIf drpvolselection.SelectedValue = 2 Then
                    ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblsearch.ClientID & "').style.display = 'none';</script>")
                End If
            End If

        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If (txtpname.Text = "" And txtemail.Text = "" And txtphone.Text = "" And txtname.Text = "" And txtsname.Text = "") Then
            lblerror.Text = "Please Enter atleast one search box"
        Else
            lblsearchR.Visible = True
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim prmArray(5) As SqlParameter

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ChildName"
            prmArray(1).Value = txtname.Text
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter
            prmArray(2).ParameterName = "@ParentName"
            prmArray(2).Value = txtpname.Text
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter
            prmArray(3).ParameterName = "@Email"
            prmArray(3).Value = txtemail.Text
            prmArray(3).Direction = ParameterDirection.Input

            prmArray(4) = New SqlParameter
            prmArray(4).ParameterName = "@Phone"
            prmArray(4).Value = txtphone.Text
            prmArray(4).Direction = ParameterDirection.Input

            prmArray(5) = New SqlParameter
            prmArray(5).ParameterName = "@SpouseName"
            prmArray(5).Value = txtsname.Text
            prmArray(5).Direction = ParameterDirection.Input


            Dim ds As DataSet = Nothing

            Try

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_Game_Registration_Search", prmArray)
            Catch se As SqlException
                'lblmsg.Text = se.Message
                Return
            End Try

            If (ds.Tables(0).Rows.Count > 0) Then

                grd1.DataSource = ds.Tables(0)
                grd1.DataBind()
            Else
                lblnodata.Text = "No data exist"
            End If
        End If
    End Sub

    Protected Sub lnkmain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkmain.Click
        Session("ChildNumber") = ""
        Session("MemberID") = ""
        Session("EventID") = ""
        Session("ProductID") = ""
        If Session("entryToken") = "Parent" Then
            Response.Redirect("UserFunctions.aspx")
        ElseIf Session("entryToken") = "Donor" Then
            Response.Redirect("DonorFunctions.aspx")
        ElseIf Session("entryToken") = "Volunteer" Then
            Response.Redirect("VolunteerFunctions.aspx")
        End If

    End Sub

    Protected Sub grd1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grd1.RowCommand
        Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
        Dim DASId As LinkButton = DirectCast(row.FindControl("lnk1"), LinkButton)
        Dim childnumber As Label = DirectCast(row.FindControl("lblChildnumber"), Label)
        Dim automemberid As Label = DirectCast(row.FindControl("lblAMI"), Label)

        Session("ChildNumber") = childnumber.Text
        Session("MemberID") = automemberid.Text
        Response.Redirect("Reg_Game.aspx")

    End Sub

    Protected Sub Bindnotapproved()
        Dim ds As DataSet = Nothing
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim Strgamerequest As String = " Select c.ChildNumber, i.automemberid, g.eventid, g.productid, g.GameID, c.first_name + ' ' + c.last_name ChildName, c.ChildNumber , p.name, g.CreateDate, g.ChildLoginID, g.ChildPWD, g.Approved, "
            Strgamerequest = Strgamerequest + " g.StartDate, g.EndDate, i.lastname + ' ' + i.firstname ParentName, CASE WHEN (g.EndDate >= GetDate() or g.Enddate IS NULL) "
            Strgamerequest = Strgamerequest + " THEN 1 Else 0 END 'Active', i.hphone, i.email, i.ChapterID, i.Chapter, i.City, i.State from Game g, Child c, Product p, IndSpouse i "
            Strgamerequest = Strgamerequest + " where g.memberid = i.automemberid and g.childnumber=c.childnumber and g.eventid = p.eventid and g.productcode = p.productcode "
            Strgamerequest = Strgamerequest + " and g.approved is NULL order by g.createdate desc"
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Strgamerequest)
        Catch se As SqlException
            'lblmsg.Text = se.Message
            Return
        End Try
        If (ds.Tables(0).Rows.Count > 0) Then
            Grd2.DataSource = ds.Tables(0)
            Grd2.DataBind()
        Else
            lblnodata.Text = "No Data Exist"
        End If

    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        lblvolselerror.Text = ""
        If drpvolselection.SelectedIndex = 0 Then
            lblvolselerror.Text = "Make a Selection"
            ' ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblsearch.ClientID & "').style.display = 'none';</script>")
        ElseIf drpvolselection.SelectedValue = 1 Then
            drpvolselection.Enabled = False
            btnsubmit.Enabled = False
            '  ClientScript.RegisterStartupScript(Me.GetType(), "show_searchtable", "<script language=""javascript"">document.getElementById('" & tblsearch.ClientID & "').style.display = 'block';</script>")
        ElseIf drpvolselection.SelectedValue = 2 Then
            drpvolselection.Enabled = False
            btnsubmit.Enabled = False
            ' ClientScript.RegisterStartupScript(Me.GetType(), "hide_table", "<script language=""javascript"">document.getElementById('" & tblsearch.ClientID & "').style.display = 'none';</script>")
            Bindnotapproved()
        End If
    End Sub

    Protected Sub Grd2_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grd2.PageIndexChanging
        Grd2.PageIndex = e.NewPageIndex
        Bindnotapproved()
    End Sub

    Protected Sub Grd2_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grd2.RowCommand
        If e.CommandName = "Select" Then
            Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
            'Dim DASId As LinkButton = DirectCast(row.FindControl("lnk1"), LinkButton)
            Dim childnumber As Label = DirectCast(row.FindControl("lblCnumber"), Label)
            Dim automemberid As Label = DirectCast(row.FindControl("lblAMemberID"), Label)
            Dim eventid As Label = DirectCast(row.FindControl("lblEventID"), Label)
            Dim productid As Label = DirectCast(row.FindControl("lblproductID"), Label)
            Session("ChildNumber") = childnumber.Text
            Session("MemberID") = automemberid.Text
            Session("EventID") = eventid.Text
            Session("ProductID") = productid.Text
            Response.Redirect("Reg_Game.aspx")
        End If
    End Sub

End Class
