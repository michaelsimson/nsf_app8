Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Net.Mail
Imports System.IO
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data


Namespace VRegistration
    Partial Class MakeEmailIdsUnique
        Inherits System.Web.UI.Page
        Private Sub Page_Load(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim SecureUrl As String
            If Session("entryToken").ToString() = "Parent" Then
                hlinkParentRegistration.Visible = True
            ElseIf Session("entryToken").ToString() = "Volunteer" Then
                hlinkVolunteerRegistration.Visible = True
            ElseIf Session("entryToken").ToString() = "Donor" Then
                hlinkDonorFunctions.Visible = True
            End If

            Response.Write(Session("SpouseID") & Session("userid"))
            txtOldEmail.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Distinct Email From IndSpouse Where AutoMemberId=" & Session("LoginID"))
            txtOldEmail1.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSpouse Where Relationship=" & Session("LoginID"))

            ValidateEmails()
            If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost" And Not (Request.Url.Host = "ferdine")) Then
                SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                Response.Redirect(SecureUrl, True)
            End If
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            If txtNewEmail.Text.Length < 3 Or txtOldEmail.Text.Length < 3 Or txtPassword.Text.Length < 1 Or txtConfirm.Text.Length < 3 Then
                txtErrMsg.Visible = True
                txtErrMsg.Text = "Please Enter all Datas Correctly"
            Else
                If txtNewEmail.Text = txtConfirm.Text Then
                    '1. make sure the user account is valid 
                    Dim conn, conn1 As New SqlConnection(Application("ConnectionString"))
                    Dim StrSql, StrSql1 As String
                    Dim loginID As String = ""  'Login_master
                    Dim isDuplicate As Boolean = True
                    Dim entryToken As String = Session("entryToken")
                    txtErrMsg.Visible = False
                    'Checks for more email being in login_master table more than once
                    Dim O_ELogincnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) from login_master where user_email='" & txtOldEmail.Text & "'")
                    If O_ELogincnt = 0 Then
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "The current email you entered does not appear in your profile."
                    ElseIf O_ELogincnt < 2 Then
                        'authenticate
                        loginID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text)))
                        If (loginID <> "") Then
                            '2. if yes,make sure the new email is unique in Login_master
                            isDuplicate = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_IsDuplicateEmail", New SqlParameter("@NewEmail", Server.HtmlEncode(txtNewEmail.Text)))
                            Response.Write(isDuplicate)
                            'Exit Sub
                            If (isDuplicate = "False") Then
                                '3. if not duplicate, 
                                'a. check if the original email is present only once on indspouse,
                                'if yes, update it and redirect to registration.aspx.  
                                'if no, add change request and show confirmation
                                Dim O_EIndCount As Integer = 0
                                Dim N_EIndCount As Integer = 0
                                Try
                                    'Checking OldEmail Count in Indspouse
                                    O_EIndCount = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail.Text)))
                                    N_EIndCount = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtNewEmail.Text)))
                                Catch se As SqlException
                                    '?
                                End Try

                                If O_EIndCount = 0 And N_EIndCount = 0 Then
                                    MsgBox("1")
                                    'No IndSpouse record exists with old email & new email
                                    StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','No IndSpouse record exists with old email or new email',GetDate())"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    ' tblEmail.Visible = False
                                ElseIf O_EIndCount = 0 And N_EIndCount > 0 Then
                                    MsgBox("2")
                                    'New Email existing in Indspouse
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','If allowed, the user will get access to an existing record, which may not be him',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 1 And N_EIndCount = 0 Then
                                    MsgBox("3")
                                    'One IndSpouse record exists with old email and No new email Exist
                                    Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                                    StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "' and AutoMemberID=" & Session("LoginID")
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    'tblEmail.Visible = False
                                ElseIf O_EIndCount = 1 And N_EIndCount = 1 Then
                                    MsgBox("4")
                                    Dim OldMemberID, OldRelationship, NewMemberID, NewRelationship, volcount As Integer
                                    Dim OldDonorType, NewDonorType As String
                                    'New Email & Old Email Exist Once in Indspouse
                                    Dim readr1 As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "Select automemberid as OldMemberID, Relationship as OldRelationship, DonorType as OldDonorType from IndSpouse where email = '" & txtOldEmail.Text & "'")
                                    While readr1.Read()
                                        OldMemberID = readr1("OldMemberID")
                                        OldRelationship = readr1("OldRelationship")
                                        OldDonorType = readr1("OldDonorType")
                                    End While
                                    readr1.Close()
                                    Dim readr2 As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "Select automemberid as NewMemberID, Relationship as NewRelationship, DonorType as NewDonorType from IndSpouse where email ='" & txtNewEmail.Text & "'")
                                    While (readr2.Read())
                                        NewMemberID = readr2("NewMemberID")
                                        NewRelationship = readr2("NewRelationship")
                                        NewDonorType = readr2("NewDonorType")
                                    End While
                                    readr2.Close()
                                    volcount = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) as VolCount from volunteer where memberid in (" & OldMemberID & "," & NewMemberID & ")")
                                    'to check whether two people are spouses  or not 
                                    If (OldRelationship = 0 And NewRelationship = 0) Or (OldRelationship > 0 And NewRelationship > 0) Or (OldRelationship > 0 And (OldRelationship <> NewMemberID Or OldDonorType <> "SPOUSE" Or NewDonorType <> "IND")) Or (NewRelationship > 0 And (NewRelationship <> OldMemberID Or NewDonorType <> "SPOUSE" Or OldDonorType <> "IND")) Then
                                        'Not Spouses
                                        MsgBox("5") '(NewRelationship > 0 And (NewRelationship <> OldMemberID Or NewDonorType <> "SPOUSE" Or OldDonorType <> "IND")))
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID, OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','NewEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & NewMemberID & "," & OldMemberID & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplayFailMsg.Visible = True
                                        tblEmail.Visible = False
                                    Else
                                        If volcount <> 0 Or Session("entryToken") = "Volunteer" Then
                                            'One IndSpouse record exists with old email and One new email Exist
                                            txtErrMsg.Visible = True
                                            txtErrMsg.Text = "Email already exists.  Must be unique in a volunteer family."
                                        Else
                                            StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                            StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & OldMemberID & " where email = '" & txtOldEmail.Text & "' and AutoMemberID=" & Session("LoginID")
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                            StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, NewMemberID, OldMemberID) values('"
                                            StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & NewMemberID & "," & OldMemberID & ")"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                            DisplaySuccessMsg.Visible = True
                                            'tblEmail.Visible = False
                                        End If
                                    End If
                                ElseIf O_EIndCount = 1 And N_EIndCount > 1 Then
                                    'New Email existing in Indspouse
                                    MsgBox("7. New Email existing in Indspouse")
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','NewEmail exists in multiple records; at least one elsewhere outside',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg5.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 2 And N_EIndCount = 0 Then
                                    'Old Email exist two times to Check whether they are Spouses
                                    Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn1, CommandType.Text, "SELECT a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & txtOldEmail.Text & "' AND b.Email = '" & txtOldEmail.Text & "' AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
                                    If readr.Read() Then
                                        'One IndSpouse record exists with old email 
                                        'MsgBox("8. One IndSpouse record exists with old email")
                                        Dim OldMemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                                        StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                        StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & OldMemberID & " where email = '" & txtOldEmail.Text & "' and AutoMemberID=" & Session("LoginID")
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & OldMemberID & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplaySuccessMsg.Visible = True
                                        ' tblEmail.Visible = False
                                    Else
                                        'MsgBox("8. Not Spouses One IndSpouse record exists with old email")
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','OldEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplayFailMsg6.Visible = True
                                        tblEmail.Visible = False
                                    End If
                                    readr.Close()
                                ElseIf O_EIndCount = 2 And N_EIndCount > 0 Then
                                    'MsgBox("9. New Email existing more than zero OLD Email two times in Indspouse")
                                    'New Email existing more than zero  & OLD Email two times in Indspouse 
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','OldEmail has duplicates and NewEmail already exists',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail.Text))) & "," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg2.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount > 2 Then
                                    'MsgBox("10 OLD Email more than two times in Indspouse ")
                                    'OLD Email more than two times in Indspouse 
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "'," & N_EIndCount & ",'Pending','OldEmail exists in more than two places',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg3.Visible = True
                                    tblEmail.Visible = False
                                End If
                            Else
                                'duplicate email , error out
                                Dim O_EIndCount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail.Text)))
                                Dim N_EIndCount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtNewEmail.Text)))

                                Dim dupemail_loginmaster As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) as IndCount from Indspouse where Email='" & txtNewEmail.Text & "'")  ' and AutoMemberID<>" & Session("LoginID") & " And Relationship <>" & Session("LoginID"))
                                If dupemail_loginmaster > 0 Then
                                    txtErrMsg.Visible = True
                                    txtErrMsg.Text = "Your proposed new email exists elsewhere in the NSF database.  Please contact <a href='mailto:nsfcontests@gmail.com'> nsfcontests@gmail.com </a>this error message."
                                    DisableOptions()
                                    'txtErrMsg.Text = "The new email address is already present, please correct if there is an error."
                                Else
                                    tblEmail.Visible = False
                                    trEmailChangeNw.Visible = True
                                    'StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                    'SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql = "Update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & Session("LoginID") & " where email = '" & txtOldEmail.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True

                                    'tblEmail.Visible = False
                                    'trEmailChangeNw.Visible = True
                                End If

                            End If

                        Else
                            '2. if no, error out
                            'Invalid email and  or password
                            txtErrMsg.Visible = True
                            txtErrMsg.Text = "The pwd you entered is not correct."
                        End If
                    Else
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "We have discovered a problem with your email not being unique. Please send an email to nsfcontests@gmail.com"
                    End If
                Else
                    txtErrMsg.Visible = True
                    txtErrMsg.Text = "The 'New Email Address' does not match with the 'Confirm New Email Address'."
                End If
            End If

        End Sub
        Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            Dim sFrom As String = "nsfcontests@gmail.com"
            Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            mail.IsBodyHtml = True

            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Try
                client.Send(mail)
            Catch e As Exception
                ok = False
            End Try
        End Sub

        Protected Sub btnUpdateNw_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Response.Write("**###" & txtPassword.Text)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim loginid As String = ""
            Dim StrSql As String
            loginid = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtNewPassword.Text)))
            If (loginid <> "") Then
                Dim O_EIndCount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail.Text)))
                If O_EIndCount = 1 Then
                    Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                    StrSql = "Delete From Login_Master where user_email='" & txtOldEmail.Text & "'"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                    StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "'"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                    StrSql = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                    StrSql = StrSql & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "',0,'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                    trEmailChangeNw.Visible = False
                    DisplaySuccessMsg.Visible = True
                    tblEmail.Visible = False
                ElseIf O_EIndCount = 2 Then
                    'Now two indspouse record exist so check whether they are Spouse
                    Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT (I.AutoMemberID) from IndSpouse I inner Join IndSpouse I1 ON I.Relationship = I1.AutoMemberID Where I.Email = I1.Email and I.Email ='" & txtOldEmail.Text & "'")
                    If count = 1 Then
                        Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text)))
                        StrSql = "Delete From Login_Master where user_email='" & txtOldEmail.Text & "'"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail.Text & "'"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        StrSql = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                        StrSql = StrSql & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "',0,'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        trEmailChangeNw.Visible = False
                        DisplaySuccessMsg.Visible = True
                        tblEmail.Visible = False
                    Else
                        'MsgBox("8. Not Spouses One IndSpouse record exists with old email")
                        StrSql = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                        StrSql = StrSql & txtOldEmail.Text & "'," & O_EIndCount & ",'" & txtNewEmail.Text & "',0,'Pending','OldEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail.Text))) & ")"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                        trEmailChangeNw.Visible = False
                        DisplayFailMsg6.Visible = True
                        tblEmail.Visible = False
                    End If
                End If
            Else
                txtErrMsg.Visible = True
                txtErrMsg.Text = "Invalid New  Password. Please correct and Redo if there is an error."
                trEmailChangeNw.Visible = False
                tblEmail.Visible = True
            End If
        End Sub

      
        Private Sub btnUpdate1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate1.Click
            If txtNewEmail1.Text.Length < 3 Or txtOldEmail1.Text.Length < 3 Or txtPassword1.Text.Length < 1 Or txtConfirm1.Text.Length < 3 Then
                txtErrMsg.Visible = True
                txtErrMsg.Text = "Please Enter all Datas Correctly"
            Else
                If txtNewEmail1.Text = txtConfirm1.Text Then
                    '1. make sure the user account is valid 
                    Dim conn, conn1 As New SqlConnection(Application("ConnectionString"))
                    Dim StrSql, StrSql1 As String
                    Dim loginID As String = ""  'Login_master
                    Dim isDuplicate As Boolean = True
                    Dim entryToken As String = Session("entryToken")
                    txtErrMsg.Visible = False
                    'Checks for more email being in login_master table more than once
                    Dim O_ELogincnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) from login_master where user_email='" & txtOldEmail1.Text & "'")
                    If O_ELogincnt = 0 Then
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "The current email you entered does not appear in your profile."
                    ElseIf O_ELogincnt < 2 Then
                        'authenticate
                        loginID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword1.Text)))
                        If (loginID <> "") Then
                            '2. if yes,make sure the new email is unique in Login_master
                            isDuplicate = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_IsDuplicateEmail", New SqlParameter("@NewEmail", Server.HtmlEncode(txtNewEmail1.Text)))
                            If (isDuplicate = "False") Then
                                '3. if not duplicate, 
                                'a. check if the original email is present only once on indspouse,
                                'if yes, update it and redirect to registration.aspx.  
                                'if no, add change request and show confirmation
                                Dim O_EIndCount As Integer = 0
                                Dim N_EIndCount As Integer = 0
                                Try
                                    'Checking OldEmail Count in Indspouse
                                    O_EIndCount = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail1.Text)))
                                    N_EIndCount = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtNewEmail1.Text)))
                                Catch se As SqlException
                                    '?
                                End Try

                                If O_EIndCount = 0 And N_EIndCount = 0 Then
                                    MsgBox("1")
                                    'No IndSpouse record exists with old email & new email
                                    StrSql = "Update Login_Master set user_email='" & txtNewEmail1.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail1.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'AutoApproval','No IndSpouse record exists with old email or new email',GetDate())"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    ' tblEmail.Visible = False
                                ElseIf O_EIndCount = 0 And N_EIndCount > 0 Then
                                    MsgBox("2")
                                    'New Email existing in Indspouse
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'Pending','If allowed, the user will get access to an existing record, which may not be him',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail1.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg.Visible = True
                                    'tblEmail.Visible = False
                                ElseIf O_EIndCount = 1 And N_EIndCount = 0 Then
                                    MsgBox("3")
                                    'One IndSpouse record exists with old email and No new email Exist
                                    Dim AutomemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text)))
                                    StrSql = "Update Login_Master set user_email='" & txtNewEmail1.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail1.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail1.Text & "', Modifydate=GetDate(),  ModifiedBy=" & AutomemberID & " where email = '" & txtOldEmail1.Text & "' and Relationship=" & Session("LoginID")
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'AutoApproval','OldEmail is unique in IndSpouse table and NewEmail doesnt exist there',GetDate()," & AutomemberID & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    'tblEmail.Visible = False
                                ElseIf O_EIndCount = 1 And N_EIndCount = 1 Then
                                    MsgBox("4")
                                    Dim OldMemberID, OldRelationship, NewMemberID, NewRelationship, volcount As Integer
                                    Dim OldDonorType, NewDonorType As String
                                    'New Email & Old Email Exist Once in Indspouse
                                    Dim readr1 As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "Select automemberid as OldMemberID, Relationship as OldRelationship, DonorType as OldDonorType from IndSpouse where email = '" & txtOldEmail1.Text & "'")
                                    While readr1.Read()
                                        OldMemberID = readr1("OldMemberID")
                                        OldRelationship = readr1("OldRelationship")
                                        OldDonorType = readr1("OldDonorType")
                                    End While
                                    readr1.Close()
                                    Dim readr2 As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "Select automemberid as NewMemberID, Relationship as NewRelationship, DonorType as NewDonorType from IndSpouse where email ='" & txtNewEmail1.Text & "'")
                                    While (readr2.Read())
                                        NewMemberID = readr2("NewMemberID")
                                        NewRelationship = readr2("NewRelationship")
                                        NewDonorType = readr2("NewDonorType")
                                    End While
                                    readr2.Close()
                                    volcount = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) as VolCount from volunteer where memberid in (" & OldMemberID & "," & NewMemberID & ")")
                                    'to check whether two people are spouses  or not 
                                    If (OldRelationship = 0 And NewRelationship = 0) Or (OldRelationship > 0 And NewRelationship > 0) Or (OldRelationship > 0 And (OldRelationship <> NewMemberID Or OldDonorType <> "SPOUSE" Or NewDonorType <> "IND")) Or (NewRelationship > 0 And (NewRelationship <> OldMemberID Or NewDonorType <> "SPOUSE" Or OldDonorType <> "IND")) Then
                                        'Not Spouses
                                        MsgBox("5") '(NewRelationship > 0 And (NewRelationship <> OldMemberID Or NewDonorType <> "SPOUSE" Or OldDonorType <> "IND")))
                                        StrSql1 = "Insert Into ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID, OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'Pending','NewEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & NewMemberID & "," & OldMemberID & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplayFailMsg.Visible = True
                                        tblEmail.Visible = False
                                    Else
                                        If volcount <> 0 Or Session("entryToken") = "Volunteer" Then
                                            'One IndSpouse record exists with old email and One new email Exist
                                            txtErrMsg.Visible = True
                                            txtErrMsg.Text = "Email already exists.  Must be unique in a volunteer family."
                                        Else
                                            StrSql = "Update Login_Master set user_email='" & txtNewEmail1.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail1.Text & "'"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                            StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail1.Text & "', Modifydate=GetDate(),  ModifiedBy=" & OldMemberID & " where email = '" & txtOldEmail1.Text & "' and Relationship=" & Session("LoginID")
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                            StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, NewMemberID, OldMemberID) values('"
                                            StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & NewMemberID & "," & OldMemberID & ")"
                                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                            DisplaySuccessMsg.Visible = True
                                            'tblEmail.Visible = False
                                        End If
                                    End If
                                ElseIf O_EIndCount = 1 And N_EIndCount > 1 Then
                                    'New Email existing in Indspouse
                                    MsgBox("7. New Email existing in Indspouse")
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'Pending','NewEmail exists in multiple records; at least one elsewhere outside',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg5.Visible = True
                                    tblEmail.Visible = False
                                ElseIf O_EIndCount = 2 And N_EIndCount = 0 Then
                                    'Old Email exist two times to Check whether they are Spouses
                                    Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn1, CommandType.Text, "SELECT a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & txtOldEmail1.Text & "' AND b.Email = '" & txtOldEmail1.Text & "' AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
                                    If readr.Read() Then
                                        'One IndSpouse record exists with old email 
                                        MsgBox("8. One IndSpouse record exists with old email")
                                        Dim OldMemberID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text)))
                                        StrSql = "Update Login_Master set user_email='" & txtNewEmail.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail.Text & "'"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                        StrSql = "update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail1.Text & "', Modifydate=GetDate(),  ModifiedBy=" & OldMemberID & " where email = '" & txtOldEmail1.Text & "' and Relationship=" & Session("LoginID")
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & OldMemberID & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplaySuccessMsg.Visible = True
                                        ' tblEmail.Visible = False
                                    Else
                                        MsgBox("8. Not Spouses One IndSpouse record exists with old email")
                                        StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                        StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'Pending','OldEmail exists elsewhere, not in the same IND/Spouse pair',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text))) & ")"
                                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                        DisplayFailMsg6.Visible = True
                                        tblEmail.Visible = False
                                    End If
                                    readr.Close()
                                ElseIf O_EIndCount = 2 And N_EIndCount > 0 Then
                                    'MsgBox("9. New Email existing more than zero OLD Email two times in Indspouse")
                                    'New Email existing more than zero  & OLD Email two times in Indspouse 
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,NewMemberID,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'Pending','OldEmail has duplicates and NewEmail already exists',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtNewEmail1.Text))) & "," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg2.Visible = True
                                    tblEmail1.Visible = False
                                ElseIf O_EIndCount > 2 Then
                                    'MsgBox("10 OLD Email more than two times in Indspouse ")
                                    'OLD Email more than two times in Indspouse 
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate,OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'Pending','OldEmail exists in more than two places',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplayFailMsg3.Visible = True
                                    tblEmail1.Visible = False
                                End If
                            Else
                                'duplicate email , error out
                                Response.Write("new id exists")
                                Dim O_EIndCount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtOldEmail1.Text)))
                                Dim N_EIndCount As Integer = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEmailCount", New SqlParameter("@oldEmail", Server.HtmlEncode(txtNewEmail1.Text)))

                                Dim dupemail_loginmaster As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) as IndCount from Indspouse where Email='" & txtNewEmail1.Text & "'")  '  and AutoMemberID<>" & Session("LoginID") & " And Relationship <>" & Session("LoginID"))
                                If dupemail_loginmaster > 0 Then
                                    txtErrMsg.Visible = True
                                    txtErrMsg.Text = "Your proposed new email exists elsewhere in the NSF database.  Please contact <a href='mailto:nsfcontests@gmail.com'> nsfcontests@gmail.com </a> with this error message."
                                    DisableOptions()
                                    'txtErrMsg.Text = "The new email address is already present, please correct if there is an error."
                                Else
                                    tblEmail1.Visible = False
                                    trEmailChangeNw.Visible = True
                                    StrSql = "Insert into Login_Master set user_email='" & txtNewEmail1.Text & ",user_Pwd='" & txtPassword1.Text & "'date_added=GETDATE(),CreatedBy=" & Session("LoginID") & ",CreatedDate=GETDATE(),"

                                    '' StrSql = "Update Login_Master set user_email='" & txtNewEmail1.Text & "', ModifyDate=GetDate(),Modifiedby ='LoginProcess' where user_email='" & txtOldEmail1.Text & "'"
                                    ''SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql = "Update indspouse set  ValidEmailFlag = Null, email = '" & txtNewEmail1.Text & "', Modifydate=GetDate(),  ModifiedBy=" & Session("LoginID") & " where email = '" & txtOldEmail1.Text & "'"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql)
                                    StrSql1 = "Insert Into  ChangeEmail(OldEmail, OldE_IndCount, NewEmail, NewE_IndCount, Status, Remarks,CreatedDate, OldMemberID) values('"
                                    StrSql1 = StrSql1 & txtOldEmail1.Text & "'," & O_EIndCount & ",'" & txtNewEmail1.Text & "'," & N_EIndCount & ",'AutoApproval','Both Spouses are sharing the same email',GetDate()," & SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtOldEmail1.Text))) & ")"
                                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql1)
                                    DisplaySuccessMsg.Visible = True
                                    '' tblEmail.Visible = False
                                End If

                            End If

                        Else
                            '2. if no, error out
                            'Invalid email and  or password
                            txtErrMsg.Visible = True
                            txtErrMsg.Text = "The pwd you entered is not correct."
                        End If
                    Else
                        txtErrMsg.Visible = True
                        txtErrMsg.Text = "We have discovered a problem with your email not being unique. Please send an email to nsfcontests@gmail.com"
                    End If
                Else
                    txtErrMsg.Visible = True
                    txtErrMsg.Text = "The 'New Email Address' does not match with the 'Confirm New Email Address'."
                End If
            End If

        End Sub

        Protected Sub ddlchange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlchange.SelectedIndexChanged
            lblError.Text = ""
            If ddlchange.SelectedValue = "1" And tblEmail1.Visible = "True" Then
                If ddlChange1.SelectedValue = "1" Then
                    lblError.Text = "Change can only  be requested on one side, but not both sides at the same time."
                    ddlchange.SelectedValue = "0"
                    Exit Sub
                Else
                    txtPassword.Enabled = True
                    txtNewEmail.Enabled = True
                    txtConfirm.Enabled = True
                    btnUpdate.Enabled = True
                End If
            ElseIf tblEmail1.Visible = "False" Then
                txtPassword.Enabled = True
                txtNewEmail.Enabled = True
                txtConfirm.Enabled = True
                btnUpdate.Enabled = True
            Else
                txtPassword.Enabled = False
                txtNewEmail.Enabled = False
                txtConfirm.Enabled = False
                btnUpdate.Enabled = False
            End If
        End Sub

        Protected Sub ddlChange1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChange1.SelectedIndexChanged
            lblError.Text = ""
            If ddlChange1.SelectedValue = "1" And tblEmail.Visible = "True" Then
                If ddlchange.SelectedValue = "1" Then
                    lblError.Text = "Change can only  be requested on one side, but not both sides at the same time."
                    ddlChange1.SelectedValue = "0"
                    Exit Sub
                Else
                    txtPassword1.Enabled = True
                    txtNewEmail1.Enabled = True
                    txtConfirm1.Enabled = True
                    btnUpdate1.Enabled = True
                End If
            ElseIf tblEmail.Visible = False Then
                txtPassword1.Enabled = True
                txtNewEmail1.Enabled = True
                txtConfirm1.Enabled = True
                btnUpdate1.Enabled = True
            Else
                txtPassword1.Enabled = False
                txtNewEmail1.Enabled = False
                txtConfirm1.Enabled = False
                btnUpdate1.Enabled = False
            End If
        End Sub
        Private Sub DisableOptions()
            ddlchange.SelectedValue = 0
            ddlChange1.SelectedValue = 0

            'Clear fields
            txtPassword.Text = ""
            txtNewEmail.Text = ""
            txtConfirm.Text = ""
            txtPassword1.Text = ""
            txtNewEmail.Text = ""
            txtConfirm1.Text = ""
            
            'DisableFields
            txtPassword.Enabled = False
            txtNewEmail.Enabled = False
            txtConfirm.Enabled = False
            btnUpdate.Enabled = False
            txtPassword1.Enabled = False
            txtNewEmail1.Enabled = False
            txtConfirm1.Enabled = False
            btnUpdate1.Enabled = False
        End Sub
        Private Sub ValidateEmails()
            ddlchange.Enabled = True
            ddlChange1.Enabled = True
            If txtOldEmail.Text <> txtOldEmail1.Text Then
                lblError.Text = "The emails are already unique or one of them does not exist. So you cannot use this application. "
                ddlchange.Enabled = False
                ddlChange1.Enabled = False
                txtPassword.Enabled = False
                txtNewEmail.Enabled = False
                txtConfirm.Enabled = False
                btnUpdate.Enabled = False
                txtPassword1.Enabled = False
                txtNewEmail1.Enabled = False
                txtConfirm1.Enabled = False
                btnUpdate1.Enabled = False
                Exit Sub
            ElseIf txtOldEmail.Text = txtNewEmail.Text Then
                lblError.Text = "Both Current Email and New Email are the same.Please validate the Email."
                Exit Sub
            ElseIf txtOldEmail1.Text = txtNewEmail1.Text Then
                lblError.Text = "Both Current Email and New Email are the same.Please validate Email."
                Exit Sub
            End If
        End Sub
    End Class
End Namespace

