<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<script language="vb" runat="server">
Sub Page_Load(Sender As Object, E As EventArgs)
        Dim fileID As Integer = Convert.ToInt32(Request.QueryString("file")) '-- if something was passed to the file querystring
        If fileID > 0 Then 'get absolute path of the file
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select FileName, dbo.ufn_getFolderURL(folderListid) as folderurl from filelist where FileListID=" & fileID & "")
            If ds.Tables(0).Rows.Count > 0 Then
                Dim path As String = "d:\inetpub\wwwroot\northsouth\" & ds.Tables(0).Rows(0)("folderurl")
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(path & ds.Tables(0).Rows(0)("FileName")) '-- if the file exists on the server
                If file.Exists Then 'set appropriate headers
                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                    Response.AddHeader("Content-Length", file.Length.ToString())
                    Response.ContentType = "application/octet-stream"
                    Response.WriteFile(file.FullName)
                    Response.End() 'if file does not exist
                Else
                    Response.Write("This file does not exist.")
                End If 'nothing in the URL as HTTP GET
            Else
                Response.Write("Please provide a file to download.")
            End If
        End If
    End Sub
</script>