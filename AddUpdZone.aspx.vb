Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Partial Class AddUpdZone
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            GetRecords()
        End If
    End Sub
    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Zone")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        BtnAdd.Text = "Update"
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim TransID As Integer
        TransID = Val(GridView1.DataKeys(index).Value)
        If (TransID) And TransID > 0 Then
            Session("EventID") = TransID
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from Zone where ZoneId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtZoneCode.Text = dsRecords.Tables(0).Rows(0)("ZoneCode").ToString()
        txtZoneName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        txtDescription.Text = dsRecords.Tables(0).Rows(0)("Description").ToString()
        txtZoneCode.Enabled = False
        Dim i As Integer
        ddlStatus.ClearSelection()
        For i = 0 To ddlStatus.Items.Count - 1
            If (ddlStatus.Items(i).Value = dsRecords.Tables(0).Rows(0)("Status").ToString()) Then
                ddlStatus.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        If BtnAdd.Text = "Add" Then
            If txtZoneCode.Text.Length > 0 Then
                If txtZoneName.Text.Length > 0 Then
                    If txtDescription.Text.Length > 0 Then
                        If Not ddlStatus.Text = "Select Status" Then
                            Dim cnt As Integer = 0
                            Strsql = "Select count(ZoneCode) from Zone where ZoneCode='" & txtZoneCode.Text & "'"
                            cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            If cnt = 0 Then
                                Strsql = "Insert Into Zone(ZoneCode, Name, Description, Status, CreateDate, CreatedBy) Values ('"
                                Strsql = Strsql & txtZoneCode.Text & "','" & txtZoneName.Text & "','" & txtDescription.Text & "','"
                                Strsql = Strsql & ddlStatus.Text & "','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "','" & Session("LoginID") & "')"
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                                GetRecords()
                                clear()
                                Pnl3Msg.Text = "Record Added Successfully"
                            Else
                                Pnl3Msg.Text = "The Zone Code already Exist"
                            End If
                        Else
                            Pnl3Msg.Text = "Please Select Valid Status"
                        End If
                        Else
                            Pnl3Msg.Text = "Please Enter Description"
                        End If
                Else
                    Pnl3Msg.Text = "Please Enter Zone Name"
                End If

                Else
                    Pnl3Msg.Text = "Please Enter Zone Code"
                End If
        ElseIf BtnAdd.Text = "Update" Then
            If txtZoneCode.Text.Length > 0 Then
                If txtZoneName.Text.Length > 0 Then
                    If Not ddlStatus.Text = "Select Status" Then
                        Strsql = "Update Zone Set Description='" & txtDescription.Text & "', Status='" & ddlStatus.SelectedItem.Value & "', ModifyDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', ModifiedBy='" & Session("LoginID") & "', ZoneCode='" & txtZoneCode.Text & "',Name = '" & txtZoneName.Text & "'  Where ZoneID=" & Session("EventID")
                        'MsgBox(Strsql)
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetRecords()
                        clear()
                        Pnl3Msg.Text = "Record Updated Successfully"
                        BtnAdd.Text = "Add"
                    Else
                        Pnl3Msg.Text = "Please Select Valid Status"
                    End If
                Else
                    Pnl3Msg.Text = "Please Enter Zone Name"
                End If

            Else
                Pnl3Msg.Text = "Please Enter Zone Code"
            End If
        End If
    End Sub
    Private Sub clear()
        txtZoneCode.Text = ""
        txtZoneName.Text = ""
        txtDescription.Text = ""
        ddlStatus.ClearSelection()
        ddlStatus.Items.FindByText("Select Status").Selected = True
        BtnAdd.Text = "Add"
        txtZoneCode.Enabled = True
    End Sub
   

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
    End Sub
End Class
