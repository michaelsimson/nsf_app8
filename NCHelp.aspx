﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NCHelp.aspx.cs" Inherits="NCHelp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        .style1
        {
            color: #FF0000;
        }
        .style2
        {
            font-family: "Times New Roman", Times, serif;
            font-weight: bold;
            font-style: italic;
            color: #3399FF;
        }
        .style3
        {
            font-family: Arial, Helvetica, sans-serif;
            color: #33CC33;
        }
        .style4
        {
            font-family: "Times New Roman", Times, serif;
        }
        .style5
        {
            font-family: "Times New Roman", Times, serif;
            font-size: medium;
            font-style: italic;
        }
        .style6
        {
            font-family: "Times New Roman", Times, serif;
            font-style: italic;
        }
    </style>
</head>
<body alink="#ccffff">
<center >
    <p class="style3" style="font-weight: 700">
        File Name Convention Format</p>
    <form id="form1" runat="server">
    </center>
    <div>
    
        <p class="MsoNormal">
            <span class="style2">Default File Format:</span><p class="MsoNormal">
            2013_Coaching_HW_MB_MB2_INT_Set2_Wk2_Sec1_Q.zip<br />
            <ul>
                <li class="style6">&nbsp;Year (2013)</li>
           <li class="style6"> Event (Coaching)</li>
            <li class="style6">Paper Type (HW)</li>
            <li class="style6">Product Group (MB)</li>
            <li class="style6">Product Code (MB2)</li>
            <li style="height: 20px"><span class="style6">Level (INT) - </span>
                <span class="style5">If level column is disabled, then the naming would be &quot;NA&quot;</span><i><br 
                    class="style4" /></i>
            <li class="style6">Set Number (Set2)</li>
            <li class="style6">Week Number (Wk2)</li>
            <li class="style6">Section Number (Sec1)</li>
            <li class="style6"> &nbsp;Document Type (Q)</li>
            </ul>
            <b><i>
            <br />
            If Level Colum contains no data or disabled, then the uploading file name would be the 
            following</i></b><br />
            <br />
            2013_Coaching_HW_MB_MB2_<span class="style1">NA</span>_Set2_Wk2_Sec1_Q.zip<o:p></o:p></p>
    
    </div>
    </form>
</body>
</html>
