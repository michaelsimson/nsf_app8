<%@ Register TagPrefix="uc1" TagName="footer" Src="include/footer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.ChapterMain" CodeFile="ChapterMain.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ChapterMain</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
	</HEAD>
	<body bgColor="#f2cfa9">
		<form id="frmChapterMain" method="post" runat="server">
			<table width="51%">
				<tr>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center" width="25%" colSpan="2">
						<h1 align="center">Chapter Functions</h1>
					</td>
				</tr>
			</table>
			<table border="3" bgcolor="#cccccc" bordercolor="#ff9900" width="51%">
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap>
						Chapter Name:</td>
					<td>
						<asp:Label id="lblChapterName" runat="server" CssClass="SmallFont"></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap>
						Chapter City:</td>
					<td>
						<asp:Label id="lblChapterCity" runat="server" CssClass="SmallFont"></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap>
						Coordinator Name:</td>
					<td>
						<asp:Label id="lblCoordinatorName" runat="server" CssClass="SmallFont"></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap>
						Contact Phone:</td>
					<td>
						<asp:Label id="lblContactPhone" runat="server" CssClass="SmallFont"></asp:Label></td>
				</tr>
			</table>
			<br>
			<table>
				<tr>
					<td align="center" colSpan="2">
						<h6>Please click here to Add New Contests
							<asp:hyperlink id="hlinkAddEvents" runat="server" NavigateUrl="AddEvent.aspx">Add New Contest(s) </asp:hyperlink></h6>
					</td>
				</tr>
				<tr>
					<td align="left" colSpan="2">
						<h6>Please Click here to Edit/Modify Contest Info
							<asp:hyperlink id="hlinkForgotPassword" runat="server" NavigateUrl="EventsList.aspx">Edit/Modify Contest</asp:hyperlink></h6>
					</td>
				</tr>
				<tr>
					<td align="left" colSpan="2">
						<h6>Please Click here to View the Registeration Details
							<asp:hyperlink id="hlinkViewRegistrations" runat="server" NavigateUrl="RegistrationList.aspx">Registered Persons</asp:hyperlink></h6>
					</td>
				</tr>
				<tr>
					<td align="left" colSpan="2">
						<h6>Please Click here to View the Aspiring Volunteer List in your Chapter
							<asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="AspiringVolunteers.aspx">Aspiring Volunteers</asp:hyperlink></h6>
					</td>
				</tr>
				<tr>
					<td align="left" colSpan="2"><asp:hyperlink id="hlinkHomePage" runat="server" NavigateUrl="http://www.northsouth.org">Back to Homepage</asp:hyperlink></td>
				</tr>
			</table>
			<uc1:footer id="Footer1" runat="server"></uc1:footer>
		</form>
	</body>
</HTML>

 

 
 
 