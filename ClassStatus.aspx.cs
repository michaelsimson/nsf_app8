﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using NativeExcel;
using System.Collections;
using VRegistration;

public partial class ClassStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            lblCoachClassStatus.Text = "";

            if (!IsPostBack)
            {
                loadyear();
                loadPhase();

                fillClassStatus();
                fillCoacheswhodidnotCCC();
                fillCancelledClassStatus();
                fillCoachListForNext7Days();
                fillCoachListForNotNext7Days();
                fillPracticeAndGuestSessions();
                LoadEvent(ddEvent);

            }
        }
    }

    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlSemester.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }

    public void fillProductGroup()
    {
        try
        {
            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "'  and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "'";
                if (ddlCoach.SelectedValue != "0")
                {
                    Cmdtext += " and MemberID=" + ddlCoach.SelectedValue + "";
                }
                Cmdtext += ")";
                //Cmdtext+=" and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
            }
            else
            {
                Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
            }

            //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductGroup.DataSource = ds;
                ddlProductGroup.DataTextField = "ProductGroupCode";
                ddlProductGroup.DataValueField = "ProductGroupId";
                ddlProductGroup.DataBind();
                ddlProductGroup.Items.Insert(0, new ListItem("All", "0"));
                //  ddlProduct.Items.Insert(0, new ListItem("All", "0"));
                // ddlLevel.Items.Insert(0, new ListItem("All", "0"));
                //  ddlSession.Items.Insert(0, new ListItem("All", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProductGroup.SelectedIndex = 1;
                    ddlProductGroup.Enabled = false;
                    fillProduct();
                }
                else
                {

                    ddlProductGroup.Enabled = true;
                }
            }
        }
        catch
        {
        }
    }
    public void fillProduct()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                // Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y'  and Semester='" + ddlSemester.SelectedValue + "')";

                Cmdtext = "select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where  EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and Accepted='Y'  and Semester='" + ddlSemester.SelectedValue + "'";
                if (ddlCoach.SelectedValue != "0")
                {
                    Cmdtext += " and MemberId=" + ddlCoach.SelectedValue + "";
                }
                if (ddlProductGroup.SelectedValue != "0")
                {
                    Cmdtext += " and ProductGroupId=" + ddlProductGroup.SelectedValue + "";
                }
                Cmdtext += ")";
            }
            else
            {
                Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Semester='" + ddlSemester.SelectedValue + "')";
            }


            // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                ddlProduct.Enabled = true;
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "ProductCode";
                ddlProduct.DataValueField = "ProductID";

                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("All", "0"));
                //ddlLevel.Items.Insert(0, new ListItem("All", "0"));
                //  ddlSession.Items.Insert(0, new ListItem("All", "0"));
                //ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProduct.SelectedIndex = 1;
                    ddlProduct.Enabled = false;
                    loadLevel();

                    if (Session["RoleID"].ToString() == "88")
                    {
                    }

                }
                else
                {

                    ddlProduct.Enabled = true;
                }

            }
        }
        catch
        {
        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlProduct.Items.Clear();
        if (ddlProductGroup.SelectedValue != "0")
        {

            fillProduct();
            fillCoach();
            loadLevel();
            LoadSessionNo();
            LoadWeekNo();
        }
        else
        {
            ddlProduct.Items.Insert(0, new ListItem("All", "0"));
        }

        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();

    }


    public void fillCoach()
    {
        try
        {
            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Semester='" + ddlSemester.SelectedValue + "' and V.Accepted='Y'";
                
                if (ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "0")
                {
                    cmdText += "  and V.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
                }
                if (ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "0")
                {
                    cmdText += " and V.ProductId=" + ddlProduct.SelectedValue + "";
                }
                if (ddlLevel.SelectedValue != "" && ddlLevel.SelectedValue != "0")
                {
                    cmdText += " and V.Level='" + ddlLevel.SelectedValue + "'";
                }
                if (ddlSession.SelectedValue != "" && ddlSession.SelectedValue != "0")
                {
                    cmdText += " and V.SessionNo=" + ddlSession.SelectedValue + "";
                }
              

                cmdText += "  order by ID.FirstName ASC";


            }
            else
            {
                cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlCoach.DataSource = ds;
                ddlCoach.DataTextField = "Name";
                ddlCoach.DataValueField = "AutoMemberID";
                ddlCoach.DataBind();
                ddlCoach.Items.Insert(0, new ListItem("All", "0"));
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    ddlCoach.Enabled = true;
                }
                else
                {
                    ddlCoach.SelectedValue = Session["LoginID"].ToString();
                    ddlCoach.Enabled = false;
                }

            }
        }
        catch
        {
        }
    }

    public void LoadEvent(DropDownList ddlObject)
    {
        try
        {
            string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
            "here EF.EventYear ="
                        + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.DataSource = ds;
                ddlObject.DataTextField = "EventCode";
                ddlObject.DataValueField = "EventId";
                ddlObject.DataBind();
                if ((ds.Tables[0].Rows.Count > 1))
                {
                    ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                    ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                    ddlObject.Enabled = false;


                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                    {
                        fillCoach();
                        fillProductGroup();
                        fillProduct();
                        loadLevel();
                        LoadSessionNo();
                        LoadWeekNo();
                    }
                    else
                    {
                        fillCoach();
                        fillProductGroup();
                    }
                }
                else
                {
                    ddlObject.Enabled = false;
                    fillProductGroup();
                }

            }
            else
            {

                // lblerr.Text = "No Events present for the selected year";
            }
        }
        catch
        {
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);
        fillCoach();
    }

    public void loadLevel()
    {

        try
        {
            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y'";

                //  cmdText +=" and V.MemberID='" + ddlCoach.SelectedValue + "'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
                if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
                {
                    cmdText += " and V.MemberId=" + ddlCoach.SelectedValue + "";
                }
            }
            else
            {
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                ddlLevel.DataTextField = "Level";
                ddlLevel.DataValueField = "Level";
                ddlLevel.DataSource = ds;
                ddlLevel.DataBind();
                ddlLevel.Items.Insert(0, new ListItem("All", "0"));

                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlLevel.Enabled = true;
                }
                else
                {
                    ddlLevel.Enabled = false;
                    ddlLevel.SelectedIndex = 1;
                    LoadSessionNo();
                }
            }
        }
        catch
        {
        }
    }

    public void LoadSessionNo()
    {
        try
        {
            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                //  cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }
                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
                if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
                {
                    cmdText += " and V.MemberId=" + ddlCoach.SelectedValue + "";
                }
                if (ddlLevel.SelectedValue != "0")
                {
                    cmdText += " and V.Level='" + ddlLevel.SelectedValue + "'";
                }
            }
            else
            {
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {

                ddlSession.DataTextField = "SessionNo";
                ddlSession.DataValueField = "SessionNo";
                ddlSession.DataSource = ds;
                ddlSession.DataBind();
                ddlSession.Items.Insert(0, new ListItem("All", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlSession.Enabled = true;
                }
                else
                {
                    ddlSession.Enabled = false;
                    ddlSession.SelectedIndex = 1;
                    LoadWeekNo();
                }

                populateRecClassDate();
            }
        }
        catch
        {
        }

    }

    public void LoadWeekNo()
    {
        try
        {
            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                //  cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                cmdText = "select distinct V.WeekNo from CoachClassCal V where V.EventYear=" + ddYear.SelectedValue + "  and V.Semester='" + ddlSemester.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }
                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
                if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
                {
                    cmdText += " and V.MemberId=" + ddlCoach.SelectedValue + "";
                }
                if (ddlLevel.SelectedValue != "0")
                {
                    cmdText += " and V.Level='" + ddlLevel.SelectedValue + "'";

                }
                cmdText += " order by WeekNo";
            }
            else
            {
                cmdText = "select distinct V.WeekNo from CoachClassCal V where V.EventYear=" + ddYear.SelectedValue + " and V.MemberID='" + Session["LoginID"].ToString() + "'  and V.Semester='" + ddlSemester.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
                cmdText += " order by WeekNo";
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {

                ddlWeekNo.DataTextField = "WeekNo";
                ddlWeekNo.DataValueField = "WeekNo";
                ddlWeekNo.DataSource = ds;
                ddlWeekNo.DataBind();
                ddlWeekNo.Items.Insert(0, new ListItem("All", "0"));
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlWeekNo.Enabled = true;
                }
                else
                {
                    ddlWeekNo.Enabled = false;
                    ddlWeekNo.SelectedIndex = 1;
                }

                populateRecClassDate();
            }
        }
        catch
        {
        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }

    protected void btnExort_Click(object sender, EventArgs e)
    {
        exportToExcel();
    }

    protected void btnExportCancelledClass_Click(object sender, EventArgs e)
    {
        exportExcelCancelledclass();
    }


    protected void btnExportCoachList_Click(object sender, EventArgs e)
    {
        exportToExcelCoachList();
    }
    protected void btnExportNext7DaysReport_Click(object sender, EventArgs e)
    {
        exportToExcelNext7Days();
    }

    protected void btnExportNotNext7DaysReport_Click(object sender, EventArgs e)
    {
        exportToExcelNot7Days();
    }

    protected void btnPractiseAndGuest_Click(object sender, EventArgs e)
    {
        exportToExcelPractiseAndGuest();
    }




    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCoach.SelectedValue != "0")
        {
            fillProductGroup();


        }
        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlLevel.Items.Clear();
        if (ddlProduct.SelectedValue != "0")
        {
            fillCoach();
            loadLevel();
            LoadSessionNo();
            LoadWeekNo();
        }
        else
        {
            ddlLevel.Items.Insert(0, new ListItem("All", "0"));
        }
        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }

    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSession.Items.Clear();
        if (ddlLevel.SelectedValue != "0")
        {
            LoadSessionNo();
        }
        else
        {

            ddlSession.Items.Insert(0, new ListItem("All", "0"));
        }

        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }

    public void fillClassStatus()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone,CL.Date,CL.Status,CL.WeekNo, CL.ProductGroup, CL.Product, CL.Level,CL.SessionNo, CL.Semester, CL.ClassType from CoachClassCal CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and CL.Status='On' and CL.Semester='" + ddlSemester.SelectedValue + "'";
            if (ddlCoach.SelectedValue != "0")
            {
                cmdText += " and CL.MemberID=" + ddlCoach.SelectedValue + "";
            }
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {

                cmdText += " and CL.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "0")
            {
                cmdText += " and CL.ProductID=" + ddlProduct.SelectedValue + "";
            }
            if (ddlLevel.SelectedValue != "" && ddlLevel.SelectedValue != "0")
            {
                cmdText += " and CL.Level='" + ddlLevel.SelectedValue + "'";
            }
            if (ddlSession.SelectedValue != "" && ddlSession.SelectedValue != "0")
            {
                cmdText += " and CL.SessionNO=" + ddlSession.SelectedValue + "";
            }
            if (ddlWeekNo.SelectedValue != "0" && ddlWeekNo.SelectedValue != "")
            {
                cmdText += " and CL.WeekNo=" + ddlWeekNo.SelectedValue + "";
            }
            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo, WeekNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {

                grdCoachClassStatus.DataSource = ds;
                grdCoachClassStatus.DataBind();
                if (ds.Tables[0].Rows.Count <= 0)
                {
                    lblCoachClassStatus.Text = "No records exists";
                    lblCoachClassStatus.Visible = true;
                    btnExort.Visible = false;
                }
                else
                {
                    lblCoachClassStatus.Visible = false;
                    btnExort.Visible = true;
                }
            }
        }
        catch
        {
        }
    }
    public void fillCoacheswhodidnotCCC()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, CL.ProductGroupCode, CL.ProductCode, CL.Level,CL.SessionNo, CL.Semester from CalSignup CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and cl.MemberID not in (select MemberID from CoachClassCal where EventYear=CL.EventYear) and CL.Accepted='Y' and CL.Semester='" + ddlSemester.SelectedValue + "'";

            //if (ddlCoach.SelectedValue != "0")
            //{
            //    cmdText += " and CL.MemberID=" + ddlCoach.SelectedValue + "";
            //}
            //if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            //{

            //    cmdText += " and CL.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            //}
            //if (ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "0")
            //{
            //    cmdText += " and CL.ProductID=" + ddlProduct.SelectedValue + "";
            //}
            //if (ddlLevel.SelectedValue != "" && ddlLevel.SelectedValue != "0")
            //{
            //    cmdText += " and CL.Level='" + ddlLevel.SelectedValue + "'";
            //}
            //if (ddlSession.SelectedValue != "" && ddlSession.SelectedValue != "0")
            //{
            //    cmdText += " and CL.SessionNO=" + ddlSession.SelectedValue + "";
            //}
            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                grdCoachList.DataSource = ds;
                grdCoachList.DataBind();

                if (ds.Tables[0].Rows.Count <= 0)
                {

                    btnExportCoachList.Visible = false;
                    lblTable2Status.Visible = true;
                    lblTable2Status.Text = "Mo records exists";
                }
                else
                {

                    btnExportCoachList.Visible = true;
                    lblTable2Status.Visible = false;
                }
            }
        }
        catch
        {
        }
    }

    public void fillCancelledClassStatus()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone,CL.Date,CL.Status,CL.WeekNo, CL.ProductGroup, CL.Product, CL.Level,CL.SessionNo, CL.Semester, Cl.ClassType from CoachClassCal CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and CL.Status not in('On') and CL.Semester='" + ddlSemester.SelectedValue + "'";
            if (ddlCoach.SelectedValue != "0")
            {
                cmdText += " and CL.MemberID=" + ddlCoach.SelectedValue + "";
            }
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {

                cmdText += " and CL.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "0")
            {
                cmdText += " and CL.ProductID=" + ddlProduct.SelectedValue + "";
            }
            if (ddlLevel.SelectedValue != "" && ddlLevel.SelectedValue != "0")
            {
                cmdText += " and CL.Level='" + ddlLevel.SelectedValue + "'";
            }
            if (ddlSession.SelectedValue != "" && ddlSession.SelectedValue != "0")
            {
                cmdText += " and CL.SessionNO=" + ddlSession.SelectedValue + "";
            }
            if (ddlWeekNo.SelectedValue != "" && ddlWeekNo.SelectedValue != "0")
            {
                cmdText += " and CL.WeekNo=" + ddlWeekNo.SelectedValue + "";
            }
            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo, WeekNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                grdCancelledClass.DataSource = ds;
                grdCancelledClass.DataBind();

                if (ds.Tables[0].Rows.Count <= 0)
                {

                    btnExportCancelledClass.Visible = false;
                    lblTable3Status.Visible = true;
                    lblTable3Status.Text = "No records exists";
                }
                else
                {

                    btnExportCancelledClass.Visible = true;
                    lblTable3Status.Visible = false;
                }
            }
        }
        catch
        {
        }
    }

    public void fillCoachListForNext7Days()
    {
        try
        {
            DateTime dtTdayDAte = DateTime.Now;
            string strTodayDAte = dtTdayDAte.ToShortDateString();
            DateTime dtNextD7DAy = DateTime.Now.AddDays(6);
            string strNext7Day = dtNextD7DAy.ToShortDateString();

            //strTodayDAte = "11/03/2016";
            //strNext7Day = "11/09/2016";

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone,CL.Date,CL.Status,CL.WeekNo, CL.ProductGroup, CL.Product, CL.Level,CL.SessionNo, CL.Semester, CL.ClassType from CoachClassCal CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and  CL.status is not null and CL.Semester='" + ddlSemester.SelectedValue + "'  and CL.Date between '" + strTodayDAte + "' and '" + strNext7Day + "' ";

            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo, WeekNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                grdCoachNext7DaysReport.DataSource = ds;
                grdCoachNext7DaysReport.DataBind();

                if (ds.Tables[0].Rows.Count <= 0)
                {

                    btnExportNext7DaysReport.Visible = false;
                    lbltable4Status.Visible = true;
                    lbltable4Status.Text = "No records exists";
                }
                else
                {

                    btnExportNext7DaysReport.Visible = true;
                    lbltable4Status.Visible = false;
                }
            }
        }
        catch
        {
        }
    }

    public void fillCoachListForNotNext7Days()
    {
        try
        {
            DateTime dtTdayDAte = DateTime.Now;
            string strTodayDAte = dtTdayDAte.ToShortDateString();
            DateTime dtNextD7DAy = DateTime.Now.AddDays(6);
            string strNext7Day = dtNextD7DAy.ToShortDateString();

            //strTodayDAte = "11/07/2016";
            //strNext7Day = "11/13/2016";

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, CL.ProductGroupCode, CL.ProductCode, CL.Level,CL.SessionNo, CL.Semester, (select max(Date) from CoachClassCal where EventYear=CL.EventYear and productGroupID=CL.ProductGroupID and ProductId=CL.ProductID and Level=CL.Level and SessionNo=CL.SessionNo and MemberID=CL.MemberID ) as LastDAteOn from CalSignup CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and CL.Accepted='Y' and CL.Semester='" + ddlSemester.SelectedValue + "' and CL.MemberID not in(select MemberID from CoachClassCAl where status is not null  and Date between '" + strTodayDAte + "' and '" + strNext7Day + "')  order by LastDAteOn DESC";



            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                grdCoachListNotNext7Days.DataSource = ds;
                grdCoachListNotNext7Days.DataBind();

                if (ds.Tables[0].Rows.Count <= 0)
                {

                    btnExportNotNext7DaysReport.Visible = false;
                    lbltable5Status.Visible = true;
                    lbltable5Status.Text = "No records exists";
                }
                else
                {

                    btnExportNotNext7DaysReport.Visible = true;
                    lbltable5Status.Visible = false;
                }
            }
        }
        catch
        {
        }
    }

    public void fillPracticeAndGuestSessions()
    {
        try
        {
            DateTime dtTdayDAte = DateTime.Now;
            string strTodayDAte = dtTdayDAte.ToShortDateString();

            string cmdText = string.Empty;
            cmdText = "select WC.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, WC.ProductGroupCode, WC.ProductCode, WC.Level,WC.Session, 'Practice' as SessionType, WC.Semester from WebConfLog WC inner join Indspouse IP on (IP.AutoMemberID=WC.MemberID) where CAST(CreatedDate as date)>=Cast(Getdate() as date) and WC.SessionType='Practise' and WC.Semester='" + ddlSemester.SelectedValue + "'";

            cmdText += " Union All";

            cmdText += " select GA.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, CS.ProductGroupCode, CS.ProductCode, CS.Level,CS.SessionNo as Session, 'Guest' as SessionType, CS.Semester from GuestAttendance GA inner join Indspouse IP on (IP.AutoMemberID=GA.MemberID) inner join CalSignUp CS on (CS.Meetingkey=GA.SessionKey) where CAST(GA.CreatedDate as date)>=Cast(Getdate() as date) and CS.Semester='" + ddlSemester.SelectedValue + "'";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                grdPractiseAndGuest.DataSource = ds;
                grdPractiseAndGuest.DataBind();


                if (ds.Tables[0].Rows.Count <= 0)
                {

                    btnPractiseAndGuest.Visible = false;
                    lbltable6Status.Visible = true;
                    lbltable6Status.Text = "No records exists";
                }
                else
                {

                    btnPractiseAndGuest.Visible = true;
                    lbltable6Status.Visible = false;
                }
            }
            else
            {

            }
        }
        catch
        {
        }
    }



    protected void grdCoachClassStatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdCoachClassStatus.PageIndex = e.NewPageIndex;
        fillClassStatus();

    }
    protected void grdCoachList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdCoachList.PageIndex = e.NewPageIndex;
        fillCoacheswhodidnotCCC();
    }
    protected void grdCoachNext7DaysReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdCoachNext7DaysReport.PageIndex = e.NewPageIndex;
        fillCoachListForNext7Days();
    }

    protected void grdCoachListNotNext7Days_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdCoachListNotNext7Days.PageIndex = e.NewPageIndex;
        fillCoachListForNotNext7Days();
    }

    protected void grdPractiseAndGuest_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdPractiseAndGuest.PageIndex = e.NewPageIndex;
        fillCoachListForNotNext7Days();
    }

    protected void grdCancelledClass_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdCancelledClass.PageIndex = e.NewPageIndex;
        fillCancelledClassStatus();
    }



    public void exportToExcel()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone,CL.Date,CL.Status,CL.WeekNo, CL.ProductGroup, CL.Product, CL.Level,CL.SessionNo, CL.Semester, Cl.ClassType from CoachClassCal CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and CL.Status='On' and CL.Semester='" + ddlSemester.SelectedValue + "' ";

            if (ddlCoach.SelectedValue != "0")
            {
                cmdText += " and CL.MemberID=" + ddlCoach.SelectedValue + "";
            }
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {

                cmdText += " and CL.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "0")
            {
                cmdText += " and CL.ProductID=" + ddlProduct.SelectedValue + "";
            }
            if (ddlLevel.SelectedValue != "" && ddlLevel.SelectedValue != "0")
            {
                cmdText += " and CL.Level='" + ddlLevel.SelectedValue + "'";
            }
            if (ddlSession.SelectedValue != "" && ddlSession.SelectedValue != "0")
            {
                cmdText += " and CL.SessionNO=" + ddlSession.SelectedValue + "";
            }
            if (ddlWeekNo.SelectedValue != "0")
            {
                cmdText += " and CL.WeekNo=" + ddlWeekNo.SelectedValue + "";
            }
            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo, WeekNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Class Status";
                    oSheet.Range["A1:M1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Class Status";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "MemberId";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "CPhone";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "ProductGroup";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Product";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Level";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "SessionNo";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Date";
                    oSheet.Range["K3"].Font.Bold = true;

                    oSheet.Range["L3"].Value = "Class Type";
                    oSheet.Range["L3"].Font.Bold = true;


                    oSheet.Range["M3"].Value = "Status";
                    oSheet.Range["M3"].Font.Bold = true;

                    oSheet.Range["N3"].Value = "WeekNo";
                    oSheet.Range["N3"].Font.Bold = true;

                    oSheet.Range["O3"].Value = "Semester";
                    oSheet.Range["O3"].Font.Bold = true;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["WPhone"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroup"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Product"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionNo"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Date"].ToString();

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["ClassType"].ToString();

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Status"].ToString();

                        CRange = oSheet.Range["N" + iRowIndex.ToString()];
                        CRange.Value = dr["WeekNo"].ToString();

                        CRange = oSheet.Range["O" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "ClassStatus_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }

    public void exportToExcelCoachList()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, CL.ProductGroupCode, CL.ProductCode, CL.Level,CL.SessionNo, CL.Semester from CalSignup CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and cl.MemberID not in (select MemberID from CoachClassCal where EventYear=CL.EventYear) and CL.Accepted='Y' and CL.Semester='" + ddlSemester.SelectedValue + "'";


            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Coach List";
                    oSheet.Range["A1:M1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Coach List";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "MemberId";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "CPhone";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "ProductGroup";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Product";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Level";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "SessionNo";
                    oSheet.Range["J3"].Font.Bold = true;


                    oSheet.Range["K3"].Value = "Semester";
                    oSheet.Range["K3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["WPhone"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductCode"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionNo"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "CoachesWhoDidnotUseCCC_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }


    public void exportToExcelNot7Days()
    {
        try
        {
            DateTime dtTdayDAte = DateTime.Now;
            string strTodayDAte = dtTdayDAte.ToShortDateString();
            DateTime dtNextD7DAy = DateTime.Now.AddDays(6);
            string strNext7Day = dtNextD7DAy.ToShortDateString();

            //strTodayDAte = "11/03/2016";
            //strNext7Day = "11/09/2016";

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, CL.ProductGroupCode, CL.ProductCode, CL.Level,CL.SessionNo, CL.Semester, (select max(Date) from CoachClassCal where EventYear=CL.EventYear and productGroupID=CL.ProductGroupID and ProductId=CL.ProductID and Level=CL.Level and SessionNo=CL.SessionNo and MemberID=CL.MemberID ) as LastDAteOn from CalSignup CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and CL.Accepted='Y' and CL.Semester='" + ddlSemester.SelectedValue + "' and CL.MemberID not in(select MemberID from CoachClassCAl where status is not null  and Date between '" + strTodayDAte + "' and '" + strNext7Day + "')  order by LastDAteOn DESC";



            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Coach List";
                    oSheet.Range["A1:M1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Coaches who have not set up the calendar for the next 7 days";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "MemberId";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "CPhone";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "ProductGroup";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Product";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Level";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "SessionNo";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Semester";
                    oSheet.Range["K3"].Font.Bold = true;



                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["WPhone"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductCode"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionNo"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();

                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "CoachesWhoDidNotSetClassForNext7Days_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }

    public void exportToExcelPractiseAndGuest()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select WC.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, WC.ProductGroupCode, WC.ProductCode, WC.Level,WC.Session, 'Practice' as SessionType, WC.Semester from WebConfLog WC inner join Indspouse IP on (IP.AutoMemberID=WC.MemberID) where CAST(CreatedDate as date)>=Cast(Getdate() as date) and WC.SessionType='Practise' and WC.Semester='" + ddlSemester.SelectedValue + "'";

            cmdText += " Union All";

            cmdText += " select GA.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone, CS.ProductGroupCode, CS.ProductCode, CS.Level,CS.SessionNo as Session, 'Guest' as SessionType, CS.Semester from GuestAttendance GA inner join Indspouse IP on (IP.AutoMemberID=GA.MemberID) inner join CalSignUp CS on (CS.Meetingkey=GA.SessionKey) where CAST(GA.CreatedDate as date)>=Cast(Getdate() as date) and CS.Semester='" + ddlSemester.SelectedValue + "'";


            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Coach List";
                    oSheet.Range["A1:M1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Practice And Guest Sessions";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "MemberId";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "CPhone";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "ProductGroup";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Product";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Level";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "SessionNo";
                    oSheet.Range["J3"].Font.Bold = true;


                    oSheet.Range["K3"].Value = "SessionType";
                    oSheet.Range["K3"].Font.Bold = true;

                    oSheet.Range["L3"].Value = "Semester";
                    oSheet.Range["L3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["WPhone"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductCode"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["Session"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionType"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "PracticeAndGuestSessions_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }


    public void exportExcelCancelledclass()
    {

        try
        {
            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone,CL.Date,CL.Status,CL.WeekNo, CL.ProductGroup, CL.Product, CL.Level,CL.SessionNo, CL.Semester, Cl.ClassType from CoachClassCal CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and CL.Semester='" + ddlSemester.SelectedValue + "'  and CL.Status not in('On')";

            if (ddlCoach.SelectedValue != "0")
            {
                cmdText += " and CL.MemberID=" + ddlCoach.SelectedValue + "";
            }
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {

                cmdText += " and CL.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            if (ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "0")
            {
                cmdText += " and CL.ProductID=" + ddlProduct.SelectedValue + "";
            }
            if (ddlLevel.SelectedValue != "" && ddlLevel.SelectedValue != "0")
            {
                cmdText += " and CL.Level='" + ddlLevel.SelectedValue + "'";
            }
            if (ddlSession.SelectedValue != "" && ddlSession.SelectedValue != "0")
            {
                cmdText += " and CL.SessionNO=" + ddlSession.SelectedValue + "";
            }
            if (ddlWeekNo.SelectedValue != "" && ddlWeekNo.SelectedValue != "0")
            {
                cmdText += " and CL.WeekNo=" + ddlWeekNo.SelectedValue + "";
            }
            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo, WeekNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Class Status";
                    oSheet.Range["A1:M1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Class Status";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "MemberId";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "CPhone";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "ProductGroup";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Product";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Level";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "SessionNo";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Date";
                    oSheet.Range["K3"].Font.Bold = true;

                    oSheet.Range["L3"].Value = "Class Rype";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Status";
                    oSheet.Range["M3"].Font.Bold = true;

                    oSheet.Range["N3"].Value = "WeekNo";
                    oSheet.Range["N3"].Font.Bold = true;

                    oSheet.Range["O3"].Value = "Semester";
                    oSheet.Range["O3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["WPhone"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroup"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Product"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionNo"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Date"].ToString();

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["ClassType"].ToString();

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Status"].ToString();

                        CRange = oSheet.Range["N" + iRowIndex.ToString()];
                        CRange.Value = dr["WeekNo"].ToString();

                        CRange = oSheet.Range["O" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "ClassStatusMakeup_Substitute_Cancelled_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }

    public void exportToExcelNext7Days()
    {
        try
        {

            DateTime dtTdayDAte = DateTime.Now;
            string strTodayDAte = dtTdayDAte.ToShortDateString();
            DateTime dtNextD7DAy = DateTime.Now.AddDays(6);
            string strNext7Day = dtNextD7DAy.ToShortDateString();

            //strTodayDAte = "11/03/2016";
            //strNext7Day = "11/09/2016";

            string cmdText = string.Empty;
            cmdText = "select CL.MemberId,IP.FirstName+' '+IP.LastName as Name,IP.Email,IP.CPhone,IP.WPhone,CL.Date,CL.Status,CL.WeekNo, CL.ProductGroup, CL.Product, CL.Level,CL.SessionNo, CL.Semester, CL.ClassType from CoachClassCal CL inner join IndSpouse IP on (CL.MemberId=IP.AutoMemberID) where CL.EventYear=" + ddYear.SelectedValue + " and  CL.status is not null  and CL.Semester='" + ddlSemester.SelectedValue + "' and CL.Date between '" + strTodayDAte + "' and '" + strNext7Day + "' ";

            cmdText += " order by MemberID,ProductGroupID, ProductID, Level, SessionNo, WeekNo ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Class Status";
                    oSheet.Range["A1:M1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Class Status";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "MemberId";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "CPhone";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "WPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "ProductGroup";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Product";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Level";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "SessionNo";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Date";
                    oSheet.Range["K3"].Font.Bold = true;

                    oSheet.Range["L3"].Value = "Class Type";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Status";
                    oSheet.Range["M3"].Font.Bold = true;

                    oSheet.Range["N3"].Value = "WeekNo";
                    oSheet.Range["N3"].Font.Bold = true;

                    oSheet.Range["O3"].Value = "Semester";
                    oSheet.Range["O3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["WPhone"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroup"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Product"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionNo"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Date"].ToString();

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["ClassType"].ToString();

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Status"].ToString();

                        CRange = oSheet.Range["N" + iRowIndex.ToString()];
                        CRange.Value = dr["WeekNo"].ToString();

                        CRange = oSheet.Range["O" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();


                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "ClassStatusOnForNext7Days_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();
                }
            }
        }
        catch
        {
        }
    }


    public void populateRecClassDate()
    {

        string cmdText = string.Empty;
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term') where  CS.MemberID=" + ddlCoach.SelectedValue + " and CS.EventYear=" + ddYear.SelectedValue + " and CS.Accepted='Y' ";

        if (ddlProductGroup.SelectedValue != "0")
        {
            cmdText += " and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " ";
        }
        if (ddlProduct.SelectedValue != "0")
        {
            cmdText += " and CS.ProductID=" + ddlProduct.SelectedValue + " ";
        }
        if (ddlLevel.SelectedValue != "0")
        {
            cmdText += " and CS.Level='" + ddlLevel.SelectedValue + "' ";
        }
        if (ddlSession.SelectedValue != "0")
        {
            cmdText += " and CS.SessionNo=" + ddlSession.SelectedValue + "";
        }
        //    IFieldControl(DD

        //and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.EventYear=" + ddYear.SelectedValue + " and CS.Accepted='Y' and CS.Level='" + ddlLevel.SelectedValue + "' and CS.SessionNo=" + ddlSession.SelectedValue + "";

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime dtStartDate = new DateTime();
                    DateTime dtEndDate = new DateTime();
                    DateTime dtTodayDate = DateTime.Now;
                    DateTime dtAdvanceDate = new DateTime();
                    DateTime dtPrevDate = new DateTime();


                    dtStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString());
                    string strStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("MM/dd/yyyy");
                    string EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("MM/dd/yyyy");
                    dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString());
                    string day = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("dddd");
                    string classDay = ds.Tables[0].Rows[0]["day"].ToString();
                    DateTime dtClassDate = Convert.ToDateTime(dtStartDate);
                    ddlWeekNo.Items.Clear();
                    int x = 0;
                    int y = 0;
                    if (day == classDay)
                    {
                        int j = 0;
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");
                            j++;
                            //  string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");

                            dtTodayDate = dtTodayDate.AddDays(7);
                            dtAdvanceDate = dtTodayDate.AddDays(7);
                            string strAdvanceDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");
                            dtTodayDate = dtTodayDate.AddDays(-7);
                            dtPrevDate = dtTodayDate.AddDays(-7);
                            string strPrevDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");

                            //if (dtPrevDate >= dtClassDate && dtClassDate <= dtAdvanceDate)
                            //{
                            //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                            //}
                            //if (dtClassDate >= dtAdvanceDate && dtPrevDate <= dtClassDate)
                            //{
                            //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                            //}

                            ddlWeekNo.Items.Insert(j - 1, new ListItem(j.ToString(), j.ToString()));

                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= 7; i++)
                        {
                            dtClassDate = dtClassDate.AddDays(1);
                            string strDay = Convert.ToDateTime(dtClassDate).ToString("dddd");
                            if (strDay == classDay)
                            {
                                break;
                            }
                        }
                        int j = 0;
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");
                            j++;
                            // string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");

                            // dtTodayDate = dtTodayDate.AddDays(7);
                            dtAdvanceDate = dtTodayDate.AddDays(7);
                            string strAdvanceDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");
                            //  dtTodayDate = dtTodayDate.AddDays(-7);
                            dtPrevDate = dtTodayDate.AddDays(-7);
                            string strPrevDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");

                            //if (dtPrevDate >= dtClassDate && dtClassDate <= dtAdvanceDate)
                            //{
                            ddlWeekNo.Items.Insert(j - 1, new ListItem(j.ToString(), j.ToString()));
                            // }
                            //if (dtClassDate >= dtAdvanceDate && dtPrevDate <= dtClassDate)
                            //{
                            //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                            //}
                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    ddlWeekNo.Items.Insert(0, new ListItem("Select", "0"));
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();

        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/ClassStatus.aspx");
    }
    protected void ddlWeekNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();

        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }
    protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();

        fillClassStatus();
        fillCoacheswhodidnotCCC();
        fillCancelledClassStatus();
        fillCoachListForNext7Days();
        fillCoachListForNotNext7Days();
        fillPracticeAndGuestSessions();
    }
}