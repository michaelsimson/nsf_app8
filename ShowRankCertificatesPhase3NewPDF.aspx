﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ShowRankCertificatesPhase3NewPDF.aspx.vb" Inherits="ShowRankCertificatesPhase3NewPDF" title="Untitled Page" %>
<%@ Reference Page="~/GenerateParticipantCertificates.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
  <asp:HyperLink runat="server" Text="Back to ScoreSheet" ID="hlnScoreSheet" NavigateUrl="~/ManageScoresheet.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<body>
<div class="Section1">
            <asp:Repeater runat="server" ID="rptCertificate"  OnItemDataBound="rptCertificate_OnItemDataBound">
                <ItemTemplate>
             <div class="Section1" style="page-break-before:always">     
               <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0" >                
                       <tr>
                  <td colspan="8">
                  <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0">
                  <tr>
                  <td rowspan="3" align="left" width="20%">
                  
                  <asp:Image runat="server" ID="imgThinkingMan" ImageUrl="http://www.northsouth.org/app8/Images/nsf.jpg"/>                            
                  </td>
                  
                  <td  rowspan="5" align="left" width="80%">
                  <asp:Image runat="server" ID="imgHeader"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg1A.jpg"/><br />
                     <% If Session("SelChapterID") = 1 Then%>
                            <asp:Image runat="server" ID="imgTitleNational"  ImageUrl="http://www.northsouth.org/app8/Images/image007_National.gif"/>
                        <%else %>
                            <asp:Image runat="server" ID="imgTitle"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg2A.jpg"/>
                            <%end if %>
                  </td>
                  </tr>

               
                  </table>
                  </td>
                  </tr>
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                             <br />                      
                        </td>
                    </tr>          
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle1" ForeColor="#0033cc" Text="Certificate of Excellence" Font-Bold="true" ></asp:Label>
                        </td>
                    </tr>  
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                                  <br />                         
                        </td>
                    </tr>                      
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle2" ForeColor="#0033cc" Text="awarded to" Font-Bold="true" ></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                                   <br />                         
                        </td>
                    </tr>
       
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top">
                            <asp:Label runat="server" ID="Label2" Font-Names="Arial" ForeColor="Brown" Font-Bold="true"  Font-Italic="true" Font-Size="22" Text='<%# DataBinder.Eval(Container,"DataItem.Participant_Name") %>'></asp:Label>
                        </td>
                    </tr>
               <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                             <br />                      
                        </td>
                    </tr>

   <tr>
                        <td class="ItemCenter" colspan="8" align="left" valign="top" >
                            <asp:Label runat="server" ID="lblcomm"   Font-Bold="true" >for achieving
                           <asp:Label runat="server" ID="Label11"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Rank_Alpha") %>'></asp:Label>
                            rank in 
                           <%-- <asp:Label runat="server" ID="lblContestYear"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.contest_year") %>'></asp:Label> --%>
                           
                             <% If Session("SelChapterID") = 1 Then%>
                            <%--National--%> the <asp:Label ID="lblYearNat" runat="server" Text ='<%# Now.Year() %>'></asp:Label> <asp:Label runat="server" ID="Label6"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.name") %>'></asp:Label> held on  <asp:Label runat="server" ID="Label7"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ContestDate") %>'></asp:Label> at the 
                            <%Else%>
                            
                            the <asp:Label ID="lblYearReg" runat="server" Text ='<%# Now.Year() %>' ></asp:Label> Regional <asp:Label runat="server" ID="Label8" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.name") %>'/> held at the
                            <% end if %>
                            
                            <% If Session("SelChapterID") = 1 Then%>
                               <asp:Label runat="server" ID="Label9" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label>.
                                 <%Else%>
                                 <asp:Label runat="server" ID="Label10" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label> Chapter.
                                <% end if %>  
                               
                            
                              </asp:Label>
                    
                        </td>
                    </tr>
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                             <br />                      
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                            
                            <asp:Label runat="server" ID="lblNSF"  Font-Italic="true">North South Foundation (NSF) is a non-profit organization involved in implementing educational programs for children in North America and India. The Foundation believes that this world can be a better place to live if the children of today are better prepared to be good citizens of tomorrow. Toward this end, the Foundation encourages children to endeavor to become the best they can be, by giving their best. Further, while it is self-evident that all humans are created equal, it is education that is paramount to actually realizing the rights of equality including life, liberty and the pursuit of happiness as the Founding Fathers of this Nation envisaged more than two hundred years ago.
                            </asp:Label>
                        </td>
                    </tr>
                   
                    
                     <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                              <br />                                     
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" >
                             <table cellspacing="2" cellpadding="2" width="98%" border="0" >                
                                <tr >
                                <td ></td>
                                
                                    <td rowspan="4" width="20%" align="center">
                        <br />
                                <asp:Image runat="server" ID="Image1" ImageUrl="http://www.northsouth.org/app8/Images/CertImg3A.jpg" />
                   

                                    </td> 
                                
                                     <td ></td>
                                </tr>
                                     <tr >
                                    <td align="left" >
                                     <% If Session("SelChapterID") = 1 Then%>
                                         <img name="leftsign1"  runat="server" id="leftsign1" src='<%# ShowImage(DataBinder.Eval(Container,"DataItem.LeftSignatureImage"),DataBinder.Eval(Container,"DataItem.SpacerURL"),DataBinder.Eval(Container,"DataItem.LeftSignatureImagePath")) %>' alt="LeftSign" width="150" height="60" />
                                     <%Else%>
                                     <img name="leftsign2" id="leftsign2" runat="server" src='<%# GetLeftSignature(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%end if%><br />
                                     <asp:Image runat="server" ID="Image2"  ImageUrl="http://www.northsouth.org/app8/Images/Signline.jpg" Width="90%" />
                                    </td> 
                                
                             
                                
                                    <td align="left" >
                                           <% If Session("SelChapterID") = 1 Then%>
                                      <img name="rightsign" id="rightsing1" runat="server" src='<%# ShowImage(DataBinder.Eval(Container,"DataItem.RightSignatureImage"),DataBinder.Eval(Container,"DataItem.SpacerURL"),DataBinder.Eval(Container,"DataItem.RightSignatureImagePath")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%Else%>
                                     <img name="rightsing2" id="rightsing2" runat="server" src='<%# GetRightSignature(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' alt="RightSign" width="150" height="60"/>
                                     <%end if%><br />
                                      <asp:Image runat="server" ID="Image3"  ImageUrl="http://www.northsouth.org/app8/Images/Signline.jpg" Width="90%" />
                                    </td> 
                                </tr>
                     
                          <%-- <tr>
                                    <td ><br /></td>
                                    <td ><br /></td>
                                </tr>--%>
                                <tr>
                                    <td align="left"  valign="top"> 
                                        <asp:Label runat="server" ID="lblLeftTitle" Font-Bold="true"   Text='<%# GetLeftSignatureName(DataBinder.Eval(Container,"DataItem.ProductCode")) %>'></asp:Label>
                                   
       
                                    </td>
                          
                                    <td align="left" valign="top" >
                                        <asp:Label runat="server" ID="lblRightTitle" Font-Size="14"  Font-Bold="true" Text='<%# GetRightSignatureName(DataBinder.Eval(Container,"DataItem.ProductCode"))%>'></asp:Label>
                             
                                        </td>
                               
                                </tr> 
                                <tr>
                                    <td align="left"  valign="top" >
                                    <asp:Label runat="server" Font-Size="14" ID="lblSigTitle"  Text='<%# GetLeftSignatureTitle (DataBinder.Eval(Container,"DataItem.ProductCode"))  %>' ></asp:Label>

                                    </td>
                                    <td align="left" valign="top" >
                                   <asp:Label  runat="server" ID="lblRightSigTitle"  Text='<%# GetRightSignatureTitle(DataBinder.Eval(Container,"DataItem.ProductCode")) %>' ></asp:Label>

                                    </td>
                                </tr>
                                
                            </table>
                        </td> 
                    </tr>                   
		       </table> 
		       </div> 
		       </ItemTemplate>	   
		       </asp:Repeater>
		
		<asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" class="tblMain" cellpadding="0" width="100%"  align="left" border="0" >
                <tr >
                    <td class="Heading">
                        <asp:Label runat="server" ID="lblMessage" ></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
    </div>
        <asp:HyperLink runat="server" Text="Back to Main Menu" ID="HyperLink1" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />
 </body>
</html>


 
 
 
</asp:Content>

