Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data
Imports NativeExcel
' Created by :Shalini 

Partial Class Email_CC
    Inherits System.Web.UI.Page
    Dim attach As Attachment
    Dim ServerPath As String
    Dim fileExt As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If

        lblmessage.Text = ""
        lblselected.Text = ""

        Dim roleid As Integer = Session("RoleID")
        'User code to initialize the page here
        'only Roleid = 1, 2, 3, 4, 5 Can access this page
        If Not Page.IsPostBack Then
            Session("EmailSource") = "Email_CC"
            Session("Productid") = ""
            If (roleid = 1) Or (roleid = 2) Or (roleid = 3) Or (roleid = 4) Or (roleid = 5) Or (roleid = 11) Or (roleid = 94) Then

                ' Load Year into ListBox
                Dim year As Integer = 0
                'year = Convert.ToInt32(DateTime.Now.Year)
                Dim first_year As Integer = 2006

                year = Convert.ToInt32(DateTime.Now.Year) + 2
                Dim count As Integer = year - first_year

                For i As Integer = 0 To count - 1 'first_year To Convert.ToInt32(DateTime.Now.Year)
                    lstyear.Items.Insert(i, Convert.ToString(year - (i + 1)))
                    'year = year - 1
                Next
                'lstyear.Items.Insert(0, Convert.ToString(year + 1))
                'lstyear.Items.Insert(1, DateTime.Now.Year.ToString())
                'lstyear.Items.Insert(2, Convert.ToString(year - 1))
                'lstyear.Items.Insert(3, Convert.ToString(year - 2))
                'lstyear.Items.Insert(4, Convert.ToString(year - 3))
                'lstyear.Items.Insert(5, Convert.ToString(year - 4))


                lstyear.Items(1).Selected = True
                Session("Year") = lstyear.Items(1).Text
                'loadRegionalCategory()
                'LoadProductGroup()   ' method to  Load Product group ListBox
                'LoadProductID()      ' method to  Load Product ID ListBox
                'LoadRoleforassignedVol() ' method to  Load Assigned Volunteer ListBox
                'LoadRoleforUnassignedVol() ' method to  Load UnAssigned Volunteer ListBox

                Dim conn As New SqlConnection(Application("ConnectionString"))

                ' If Not Session("LoginChapterID") = "" Then
                If Session("Panel") = "ChapterPanel" Then
                    If roleid = 3 Then
                        tabletarget.Visible = False
                        Dim strSql As String = "Select chapterID, ChapterCode from Chapter a where zoneid in (Select ZoneID from Volunteer where RoleID = 3 and memberid = " & Session("LoginID") & ") order by state, chaptercode"
                        Dim chapters As SqlDataReader
                        chapters = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                        lstchapter.DataSource = chapters
                        lstchapter.DataBind()
                        lstchapter.Items.Insert(0, New ListItem("Select Chapter(s)", 0))
                        lstchapter.Items(0).Selected = True
                    ElseIf roleid = 4 Then
                        tabletarget.Visible = False
                        Dim strSql As String = "Select chapterID, ChapterCode from Chapter a where clusterid in (Select ClusterID from Volunteer where RoleID = 4 and memberid = " & Session("LoginID") & ") order by state, chaptercode"
                        Dim chapters As SqlDataReader
                        chapters = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                        lstchapter.DataSource = chapters
                        lstchapter.DataBind()
                        lstchapter.Items.Insert(0, New ListItem("Select Chapter(s)", 0))
                        lstchapter.Items(0).Selected = True
                    ElseIf (roleid = 1) Or (roleid = 2) Then
                        tabletarget.Visible = False
                        Dim strSql As String = "Select chapterID, ChapterCode from Chapter order by state, chaptercode"
                        Dim chapters As SqlDataReader
                        chapters = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                        lstchapter.DataSource = chapters
                        lstchapter.DataBind()
                        lstchapter.Items.Insert(0, New ListItem("Select Chapter(s)", 0))
                        lstchapter.Items(0).Selected = True
                    ElseIf (roleid = 5) Then
                        drpChapter.Items.Insert(0, Session("LoginChapterName"))
                        drpChapter.Enabled = False
                    ElseIf (roleid = 11) Then
                        drpChapter.Items.Insert(0, Session("LoginChapterName"))
                        drpChapter.Enabled = False
                        drpevent.SelectedValue = 3
                        drpevent.Enabled = False
                    ElseIf (roleid = 94) Then
                        drpChapter.Items.Insert(0, Session("LoginChapterName"))
                        drpChapter.Enabled = False
                        drpevent.SelectedValue = 19
                        drpevent.Enabled = False
                    End If
                    'ElseIf Not Session("LoginClusterID") = 0 Then
                ElseIf Session("Panel") = "ClusterPanel" Then
                    drpChapter.Items.Insert(0, Session("LoginClusterName"))
                    drpChapter.Enabled = False
                    ' ElseIf Not Session("LoginZoneID") = 0 Then
                ElseIf Session("Panel") = "ZonalPanel" Then
                    drpChapter.Items.Insert(0, Session("LoginZoneName"))
                    drpChapter.Enabled = False
                End If
                loadRegionalCategory()
                LoadProductGroup()   ' method to  Load Product group ListBox
                LoadProductID()      ' method to  Load Product ID ListBox
                LoadRoleforassignedVol() ' method to  Load Assigned Volunteer ListBox
                LoadRoleforUnassignedVol() ' method to  Load UnAssigned Volunteer ListBox

            Else
                lblerr.Visible = True
                tabletarget.Visible = False
            End If
        End If

        'If Not Session("LoginChapterID") = "" Then
        If Session("Panel") = "ChapterPanel" Then
            If roleid = 3 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & drpChapter.ClientID & "').style.display = 'none';</script>")
            ElseIf roleid = 4 Then
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & drpChapter.ClientID & "').style.display = 'none';</script>")
            ElseIf (roleid = 1) Or (roleid = 2) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & drpChapter.ClientID & "').style.display = 'none';</script>")
            ElseIf (roleid = 5) Then
                lblselected.Text = "Selected Chapter: "
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & lstchapter.ClientID & "').style.display = 'none';</script>")
                txtselectedchapter.Visible = False
                btnsubmit.Visible = False
            ElseIf (roleid = 11) Or (roleid = 94) Then
                lblselected.Text = "Selected Chapter: "
                ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & lstchapter.ClientID & "').style.display = 'none';</script>")
                txtselectedchapter.Visible = False
                btnsubmit.Visible = False
                rbtassigndvolunteer.Visible = False
                rbtunassigndvolunteer.Visible = False
                rbtownlist.Visible = False
                lblassignrole.Visible = False
                lstAssignRole.Visible = False
                lblunassignrole.Visible = False
                lstUnassignRole.Visible = False
            End If
            '  ElseIf Not Session("LoginClusterID") = 0 Then
        ElseIf Session("Panel") = "ClusterPanel" Then
            lblselected.Text = "Selected Cluster: "
            ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & lstchapter.ClientID & "').style.display = 'none';</script>")
            txtselectedchapter.Visible = False
            btnsubmit.Visible = False
            ' ElseIf Not Session("LoginZoneID") = 0 Then
        ElseIf Session("Panel") = "ZonalPanel" Then
            lblselected.Text = "Selected Zone: "
            ClientScript.RegisterStartupScript(Me.GetType(), "hide_chapter", "<script language=""javascript"">document.getElementById('" & lstchapter.ClientID & "').style.display = 'none';</script>")
            txtselectedchapter.Visible = False
            btnsubmit.Visible = False
        End If
        Session("emaillist") = ""
    End Sub

    Private Sub LoadProductGroup()
        Dim eventid As String = drpevent.SelectedValue
        Dim strSql As String '= "Select ProductGroupID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
        If eventid = "1" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
        ElseIf eventid = "2" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
        ElseIf eventid = "3" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P  where P.EventId in (3)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
        ElseIf eventid = "19" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P  where P.EventId in (19)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
        End If
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductGroup.DataSource = drproductgroup
        lstProductGroup.DataBind()
        If lstProductGroup.Items.Count > 0 Then
            lstProductGroup.Items.Insert(0, "All")
            lstProductGroup.Items(0).Selected = True
        End If
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim eventid As String = drpevent.SelectedValue
        Dim productgroup As String = ""
        If lstProductGroup.Items.Count > 0 Then
            If lstProductGroup.Items(0).Selected = True Then
                ' if selected item in Productgroup is ALL
                lstProductid.Enabled = False
                Dim sbvalues As New StringBuilder
                Dim strSql1 As String '= "Select ProductGroupID as ID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
                ' strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
                If eventid = "1" Then
                    strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
                ElseIf eventid = "2" Then
                    strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
                ElseIf eventid = "3" Then
                    strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P  where P.EventId in (3)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
                ElseIf eventid = "19" Then
                    strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P  where P.EventId in (19)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
                End If
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("ProductGroupId") = sbvalues.ToString
                End If
            End If
        End If

        Dim strSql As String '= "Select ProductID, Name from Product where EventID in (" & eventid & ") and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        Dim drproductid As SqlDataReader

        If Session("ProductGroupId") <> "" Then
            strSql = "Select ProductCode, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            lstProductid.DataSource = drproductid
            lstProductid.DataBind()
            lstProductid.Items.Insert(0, "All")
            lstProductid.Items(0).Selected = True
        End If

    End Sub

    Private Sub LoadRoleforassignedVol()
        Dim strSql As String = "Select RoleID, Name from Role where Chapter = 'Y' order by RoleID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstAssignRole.DataSource = drrole
        lstAssignRole.DataBind()
        lstAssignRole.Items.Insert(0, "All")
        lstAssignRole.Items(0).Selected = True
    End Sub

    Private Sub LoadRoleforUnassignedVol()
        Dim strSql As String = "Select  VolunteerTaskID, TaskDescription from VolunteerTasks where LevelCode = 3 order by VolunteerTaskID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstUnassignRole.DataSource = drrole
        lstUnassignRole.DataBind()
        lstUnassignRole.Items.Insert(0, "All")
        lstUnassignRole.Items(0).Selected = True
    End Sub

    Protected Sub btnselectmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnselectmail.Click
        ' Continue to Email button
        ' will select emails depending on selections made
        ' tableemail.Visible = True
       
        Dim dsEmails As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = ""
        Dim eventid As String = drpevent.SelectedValue
        Dim chapterid As String = ""
        Session("mailEventID") = drpevent.SelectedItem.Value
        ' If Not Session("LoginChapterID") = "" Then
        If Session("Panel") = "ChapterPanel" Then
            chapterid = Session("selectedChapter")
            If chapterid = "" Then
                chapterid = Session("LoginChapterID")
            End If

            ' ElseIf Not Session("LoginZoneID") = 0 Then   ' Set chapter id depending on selected Coordinator in Volunteer functions page
        ElseIf Session("Panel") = "ZonalPanel" Then
            strSql = "Select Chapterid from Chapter where ZoneID = " & Session("LoginZoneID")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            Dim sbzonechapter As New StringBuilder
            If ds.Tables(0).Rows.Count > 0 Then
                Dim zonechaptercount As Integer
                For zonechaptercount = 0 To ds.Tables(0).Rows.Count - 1
                    sbzonechapter.Append(ds.Tables(0).Rows(zonechaptercount)("chapterid").ToString)
                    If zonechaptercount < ds.Tables(0).Rows.Count - 1 Then
                        sbzonechapter.Append(",")
                    End If
                Next
                chapterid = sbzonechapter.ToString
                'chapterid = Session("chapterid")
            End If
            ' chapterid = Session("LoginChapterID")
            ' ElseIf Not Session("LoginClusterID") = 0 Then
        ElseIf Session("Panel") = "ClusterPanel" Then
            strSql = "Select Chapterid from Chapter where ClusterID = " & Session("LoginClusterID")


            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            Dim sbclusterchapter As New StringBuilder
            If ds.Tables(0).Rows.Count > 0 Then
                Dim clusterchaptercount As Integer
                For clusterchaptercount = 0 To ds.Tables(0).Rows.Count - 1
                    sbclusterchapter.Append(ds.Tables(0).Rows(clusterchaptercount)("chapterid").ToString)
                    If clusterchaptercount < ds.Tables(0).Rows.Count - 1 Then
                        sbclusterchapter.Append(",")
                    End If
                Next
                chapterid = sbclusterchapter.ToString
                'chapterid = Session("chapterid")
            End If
        End If


        If rbtall.Checked = True Then   ' If selected value is ALL from Target
            ' ***************************************** commented code is IF we want to use Stored procedure *************************
            ' Dim strAll As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ChapterID in (" & chapterid & ")"
            Dim strAll As String = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and ChapterID in (" & chapterid & ")" & IIf(chkPaid.Checked = True, " and not exists(select * from Contestant where contestyear=" & Now.Year() & " and PaymentReference is not null and ChapterID in (" & chapterid & ") and (AutoMemberID=ParentID or Relationship=ParentID))", "")
            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, strAll)
        ElseIf btnregisteredparetns.Checked = True Then 'If selected value is Registered parents from Target
            If lstProductGroup.Items.Count > 0 Then
                If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
                    Dim sbvalues As New StringBuilder
                    ' strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
                    If eventid = "1" Then
                        strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
                    ElseIf eventid = "2" Then
                        strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
                    ElseIf eventid = "3" Then
                        strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P  where P.EventId in (3)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
                    ElseIf eventid = "19" Then
                        strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P  where P.EventId in (19)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"

                    End If
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                    If ds.Tables.Count > 0 Then
                        Dim i As Integer
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                            If i < ds.Tables(0).Rows.Count - 1 Then
                                sbvalues.Append(",")
                            End If
                        Next
                        Session("ProductGroupId") = sbvalues.ToString
                    End If
                End If
            End If
            If lstProductid.Items.Count > 0 Then
                If lstProductid.Items(0).Selected = True And Session("Productid") = "" Then  ' if selected item in ProductId is ALL
                    Dim sbvalues As New StringBuilder
                    If Session("ProductGroupId") <> "" Then
                        strSql = "Select ProductCode as ID, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
                    End If
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                    If ds.Tables.Count > 0 Then
                        Dim i As Integer
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            sbvalues.Append("'")
                            sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                            sbvalues.Append("'")
                            If i < ds.Tables(0).Rows.Count - 1 Then
                                sbvalues.Append(",")
                            End If
                        Next
                        Session("Productid") = sbvalues.ToString
                    End If
                End If
            End If
            Dim dsPaid As DataSet = New DataSet
            If Session("Productid") <> "" Then
                dsPaid = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct a.ParentId From Contestant a Where a.ContestYear=" & Now.Year() & " and a.Productcode in (" & Session("Productid") & ") and ChapterID in (" & chapterid & ") and eventid =" & eventid & " and a.PaymentReference is not null")
            End If
            Dim PaidParents As String = ""
            If dsEmails.Tables.Count > 0 Then
                If dsPaid.Tables(0).Rows.Count > 0 Then
                    For i As Integer = 0 To dsPaid.Tables(0).Rows.Count - 1
                        PaidParents = PaidParents + dsPaid.Tables(0).Rows(i)("ParentId").ToString() + ","
                    Next
                    If chkPaid.Checked = True Then
                        PaidParents = " and not exists(select * from Contestant where contestyear=" & Now.Year() & " and PaymentReference is not null and ChapterID in (" & chapterid & ") and a.Productcode in (" & Session("Productid") & ") and eventid =" & eventid & " and (AutoMemberID=ParentID or Relationship=ParentID))"
                        '  and a.ParentID not in (" & PaidParents.Trim(",") & ")" 'and (a.PaymentReference is null and a.ContestYear=" & Now.Year() & ")" '"
                    Else
                        PaidParents = ""
                    End If
                End If
            End If
            Dim strChpCondn As String = " and ChapterID in (" & chapterid & ")"
            If Session("ProductId") <> String.Empty Then
                strChpCondn = strChpCondn & " and Productcode in (" & Session("Productid") & ") "
            End If
            If eventid = 1 Then
                'If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                '    ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                '    Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                '    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                'ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                '    ' Dim StrPendingregistration As String = " SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                '    Dim StrPendingregistration As String = " SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                '    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                'ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                '    '  Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & ")))) "
                '    Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & ")))) "
                '    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                'End If
                Dim StrSQL1 As String
                If drpregistrationtype.SelectedItem.Value = 1 Then   'All invitees
                    StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and a.nationalinvitee=1 and invite_decline_comments is null and ChapterID in (" & chapterid & ") and a.EventID in (2)" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ")  and a.nationalinvitee=1 and invite_decline_comments is null and ChapterID in (" & chapterid & ") and a.EventID in (2) " & PaidParents & "))) group by Email"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'Paid Parent's Emails
                    'StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND' and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is not null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is not null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & ")))) group by Email"
                    StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and ChapterID in (" & chapterid & ") and a.ParentID in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productid") & ") and eventid=1 and paymentreference is not null " & PaidParents & ");SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'Pending Parent's Emails
                    'StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND' and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & ")))) group by Email"
                    StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and ChapterID in (" & chapterid & ") and a.ParentID in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productid") & ") and eventid=1 and paymentreference is null" & PaidParents & ");SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) AND ValidEmailFlag is Null   and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 4 Then  'invitees less Paid
                    StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and a.nationalinvitee=1 and invite_decline_comments is null and ChapterID in (" & chapterid & ") and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productid") & ") and eventid=1 and paymentreference is not null);SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 5 Then  'invitees less Paid less Pending
                    StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and a.nationalinvitee=1 and invite_decline_comments is null and ChapterID in (" & chapterid & ") and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productid") & ") and eventid=1" & PaidParents & ");SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE'  AND (newsletter not in ('2','3') OR (Newsletter is null)) and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                End If
                '**
                'lblerr.Text = StrSQL1
                'lblerr.Visible = True
                'Exit Sub
            ElseIf eventid = 2 Then
                If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                    ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and Productcode in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and Productcode in (" & Session("Productid") & "))))"
                    Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ")" & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                    ' Dim StrPendingregistration As String = " SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and Productcode in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                    Dim StrPendingregistration As String = " SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ")" & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                    '  Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & ")))) "
                    Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") " & strChpCondn & PaidParents & "))) "
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                End If
            ElseIf eventid = 3 Then
                If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                    ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                    Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                    ' Dim StrPendingregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and Productcode in (" & Session("Productid") & "))))"
                    Dim StrPendingregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                    ' Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                    Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND'   and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                End If
            ElseIf eventid = 19 Then
                Try
                    If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                        ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                       
                        Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration_Prepclub a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration_Prepclub a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"

                        dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                    ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                        ' Dim StrPendingregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and Productcode in (" & Session("Productid") & "))))"
                        Dim StrPendingregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration_Prepclub a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration_Prepclub a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                        dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                    ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                        ' Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and EventID in (" & eventid & ") and ChapterID in (" & chapterid & ") and ProductID in (" & Session("Productid") & "))))"
                        Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and ((donortype = 'IND'   and exists (select memberid from Registration_Prepclub a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration_Prepclub a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and EventID in (" & eventid & ") " & strChpCondn & PaidParents & ")))"
                        dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                    End If
                Catch ex As Exception
                    '   Response.Write("Err" & ex.ToString)
                End Try
            End If
        ElseIf rbtassigndvolunteer.Checked = True Then  'If selected value is Assigned Volunteer from Target
            If lstAssignRole.Items(0).Selected = True Then   'If selected value is ALL from Assigned Volunteer
                Dim sbvalues As New StringBuilder
                Dim strSql1 As String = "Select RoleID as ID, Name from Role where Chapter = 'Y' order by RoleID"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Assignvolunteer") = sbvalues.ToString
                End If
            End If
            ' Dim StrAssign As String = "SELECT  AutoMemberID, Email as EmailID  From IndSpouse b where email<>'' and exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and ChapterID in ( " & chapterid & " ) and   RoleID in ( " & Session("AssignVolunteer") & " ))"
            Dim StrAssign As String = "SELECT  distinct(Email) as EmailID  From IndSpouse b where email<>'' and email is not null AND ValidEmailFlag is Null and exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and ChapterID in ( " & chapterid & " ) and   RoleID in ( " & Session("AssignVolunteer") & " ))"
            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrAssign)

        ElseIf rbtunassigndvolunteer.Checked = True Then       'If selected value is UnAssigned Volunteer from Target
            If lstUnassignRole.Items(0).Selected = True Then   'If selected value is ALL from UnAssigned VOlunteer
                Dim sbvalues As New StringBuilder
                Dim strSql1 As String = "Select  VolunteerTaskID as ID, TaskDescription from VolunteerTasks where LevelCode = 3 order by VolunteerTaskID"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("UnAssignvolunteer") = sbvalues.ToString
                End If
            End If
        '  Dim StrUnAssign As String = " SELECT  AutoMemberID, Email as EmailID From IndSpouse b where Email <> '' and (VolunteerRole1 in ( " & Session("UnAssignvolunteer") & ") OR VolunteerRole2 in (" & Session("UnAssignvolunteer") & ") OR VolunteerRole3 in (" & Session("UnAssignvolunteer") & ")) and ChapterID in (" & chapterid & ") and not exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and ChapterID in (" & chapterid & "))"
            Dim StrUnAssign As String = " SELECT  distinct(Email)  as EmailID From IndSpouse b where Email <> '' AND ValidEmailFlag is Null and (VolunteerRole1 in ( " & Session("UnAssignvolunteer") & ") OR VolunteerRole2 in (" & Session("UnAssignvolunteer") & ") OR VolunteerRole3 in (" & Session("UnAssignvolunteer") & ")) and ChapterID in (" & chapterid & ") and not exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and ChapterID in (" & chapterid & "))"
            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrUnAssign)
        ElseIf rbtownlist.Checked = True Then ' If Own List radio button is selected
            Session("sentemaillistenable") = "Yes"
            If (AttachmentFile.HasFile) Then
                Dim validFileTypes As String() = {"xlsx", "xls"}
                Dim ext As String = System.IO.Path.GetExtension(AttachmentFile.PostedFile.FileName)
                Dim isValidFile As Boolean = False
                For i As Integer = 0 To validFileTypes.Length - 1
                    If ext = "." & validFileTypes(i) Then
                        isValidFile = True
                        Exit For
                    End If
                Next
                If Not isValidFile Then
                    Session("ExcelFileUploadedorNot") = "No"
                    lblerr.ForeColor = System.Drawing.Color.Red
                    lblerr.Text = "Invalid File. Please upload a File with extension " & String.Join(",", validFileTypes)
                    Exit Sub
                Else
                    Session("ExcelFileUploadedorNot") = "Yes"
                    uploadExcelSheet()
                End If
                Session("sentemaillistenable") = "No"
            End If
            'Response.Redirect("emaillist.aspx")
        End If

        ' depending on selections  emails list will be selected

        Dim tblEmails() As String = {"EmailContacts"}
        Dim sbEmailList As New StringBuilder
        If dsEmails.Tables.Count > 0 Then

            If dsEmails.Tables(0).Rows.Count > 0 Then
                Dim ctr As Integer
                For ctr = 0 To dsEmails.Tables(0).Rows.Count - 1
                    sbEmailList.Append(dsEmails.Tables(0).Rows(ctr).Item("EmailID").ToString)
                    If ctr <= dsEmails.Tables(0).Rows.Count - 2 Then
                        sbEmailList.Append(",")
                    End If
                Next
                Session("emaillist") = sbEmailList.ToString
            End If
        End If

        '** ferdine silvaa
        ''lblerr.Text = Session("emaillist")
        ''lblerr.Visible = True
        Session("sentemaillistenable") = "No"
        Response.Redirect("emaillist.aspx")
    End Sub

    Protected Sub rbtall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in All from Target
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = False
        divchooseexcelownlist.Visible = False
    End Sub

    Protected Sub btnregisteredparetns_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in registered parents  from Target
        drpregistrationtype.Enabled = True
        If Session("RoleID") <> 11 And Session("RoleID") <> 94 Then
            drpevent.Enabled = True
        End If

        lstProductGroup.Enabled = True
        lstProductid.Enabled = False
        lstyear.Enabled = True
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = False
        divchooseexcelownlist.Visible = False
    End Sub

    Protected Sub rbtassigndvolunteer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in AssignedVolunteer  from Target
        LoadRoleforassignedVol()
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssignRole.Enabled = True
        lstUnassignRole.Enabled = False
        divchooseexcelownlist.Visible = False
    End Sub

    Protected Sub rbtunassigndvolunteer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Unassigned Volunteer  from Target
        LoadRoleforassignedVol()
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = True
        divchooseexcelownlist.Visible = False
    End Sub

    Protected Sub drpevent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Event from Registered Parents
        If drpevent.SelectedValue = 1 Then
            loadFinalCategory()
        Else
            loadRegionalCategory()
        End If
        LoadProductGroup()
    End Sub
    Protected Sub loadRegionalCategory()
        drpregistrationtype.Items.Clear()
        drpregistrationtype.Items.Add(New ListItem("Paid Registrations", "1"))
        drpregistrationtype.Items.Add(New ListItem("Pending Registrations", "2"))
        drpregistrationtype.Items.Add(New ListItem("Both", "3"))
    End Sub

    Protected Sub loadFinalCategory()
        drpregistrationtype.Items.Clear()
        drpregistrationtype.Items.Add(New ListItem("All Invites", "1"))
        drpregistrationtype.Items.Add(New ListItem("Paid Registrants", "2"))
        drpregistrationtype.Items.Add(New ListItem("Pending", "3"))
        drpregistrationtype.Items.Add(New ListItem("Invites less Paid", "4"))
        drpregistrationtype.Items.Add(New ListItem("Invites less Paid less Pending", "5"))
    End Sub

    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If
        lstProductid.Enabled = True
        Session("Productid") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 1 To lstProductid.Items.Count - 1
            If lstProductid.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductid.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Productid") = sbvalues.ToString
        End If
        ' End If
    End Sub

    Protected Sub lstyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in year from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstyear.Items.Count - 1
            If lstyear.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstyear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Year") = sbvalues.ToString
        End If
    End Sub

    Protected Sub lstAssignRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Role froom Assigned Volunteer 
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstAssignRole.Items.Count - 1
            If lstAssignRole.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstAssignRole.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Assignvolunteer") = sbvalues.ToString
        End If
    End Sub

    Protected Sub lstUnassignRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Role from Unassigned Volunteer
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstUnassignRole.Items.Count - 1
            If lstUnassignRole.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstUnassignRole.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("UnAssignvolunteer") = sbvalues.ToString
        End If
    End Sub

    Protected Sub rbtownlist_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtownlist.CheckedChanged
        'if own list radio button is checked from Target
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = False
        divchooseexcelownlist.Visible = True

    End Sub

    Protected Sub lstchapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstchapter.SelectedIndexChanged
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstchapter.Items.Count - 1
            If lstchapter.Items(i).Selected And Not lstchapter.Items(i).Value.Trim = "0" Then
                dr = dt.NewRow()
                dr("Values1") = lstchapter.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("selectedChapter") = sbvalues.ToString

        End If


        Dim i1 As Integer
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable("selected")
        Dim dc1 As DataColumn = New DataColumn("Values2")
        dc1.DataType = System.Type.GetType("System.String")
        dt1.Columns.Add(dc1)
        Dim dr1 As DataRow
        For i1 = 0 To lstchapter.Items.Count - 1
            If lstchapter.Items(i1).Selected And Not lstchapter.Items(i1).Value.Trim = "0" Then
                dr1 = dt1.NewRow()
                dr1("Values2") = lstchapter.Items(i1).Text
                dt1.Rows.Add(dr1)
            End If
        Next
        ds1.Tables.Add(dt1)
        ds1.AcceptChanges()

        If ds1.Tables.Count > 0 Then
            Dim sbtext As New StringBuilder
            Dim a1 As Integer
            For a1 = 0 To ds1.Tables(0).Rows.Count - 1
                sbtext.Append(ds1.Tables(0).Rows(a1).Item("Values2").ToString)
                If a1 < ds1.Tables(0).Rows.Count - 1 Then
                    sbtext.Append(";")
                End If
            Next
            txtselectedchapter.Text = sbtext.ToString
        End If

    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Dim li As ListItem
        Dim i As Integer = 0
        For Each li In lstchapter.Items
            If li.Selected = True Then
                i = i + 1
            End If
        Next
        If lstchapter.SelectedIndex = 0 And i = 1 Then
            lblmessage.Text = "Select Chapter(s)"
        Else
            lstchapter.Enabled = False
            tabletarget.Visible = True
            btnsubmit.Visible = False
        End If
    End Sub
    Protected Sub uploadExcelSheet()
        ServerPath = Server.MapPath("~/UploadFiles/")

        Dim FileName As [String] = AttachmentFile.FileName()
        'txtEmailBody.Text = "<b>Hai</b>";
        fileExt = System.IO.Path.GetExtension(AttachmentFile.FileName)
        AttachmentFile.PostedFile.SaveAs(ServerPath + AttachmentFile.FileName)
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.OpenWorkbook(ServerPath + AttachmentFile.FileName)
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets(1)
        Dim cells As IRange
        cells = oSheet.UsedRange
        'cells = oSheet.Cells("A1:A7")


        Dim stremaillist As String
        stremaillist = ""
        Dim i As Integer
        Dim j As Integer
        For i = 2 To cells.Rows.Count
            For j = 1 To cells.Columns.Count
                If Not cells(i, j).Value Is Nothing Then
                    stremaillist = stremaillist & cells(i, j).Value.ToString().Trim() & ","
                End If


            Next
        Next
        stremaillist = stremaillist.Substring(0, stremaillist.Length - 1)
        Session("emaillist") = stremaillist.ToString()

    End Sub
End Class
