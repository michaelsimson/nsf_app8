'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/AddEvent.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page allows the User to add new events at both Chapter 
'           level and at National level,
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetChapters
'           2) NSF\usp_InsertNewEvent
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data

Imports NorthSouth.BAL


Namespace VRegistration


Partial Class AddEvent
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtContestYear As System.Web.UI.WebControls.TextBox
    Protected WithEvents cvContestDate As System.Web.UI.WebControls.CompareValidator


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            'commented by silva
            ''If Session("LoggedIn") <> "" Then
            ''    Server.Transfer("login.aspx")
            ''End If
            Dim strScript As String = "if (this.submitted) return false; this.submitted = true; return true;"
            'ClientScript.RegisterOnSubmitStatement("submit", "Form1", strScript)

        If Page.IsPostBack = False Then
            ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
            rblContestType.Items.FindByValue(Application("ContestType")).Selected = True
            rblContestType.Enabled = False
            GetContestForYear(Application("ContestYear"))
                'MsgBox(Session("ChapterID"))
            'If Session("ChapterID") <> Nothing Then
            '    ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
            '    rblContestType.Items.FindByValue(2).Selected = True
            '    rblContestType.Enabled = False
            '    GetContestForYear(Application("ContestYear"))
            'Else
            '    ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
            '    rblContestType.Items.FindByValue(0).Selected = True
            '    rblContestType.Enabled = False
            '    GetContestForYear(Application("ContestYear"))
            'End If

        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Session("RoleGeneral") = True
            Dim strContests As New StringBuilder
        Dim i As Integer
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand

        For i = 0 To chkSpelling.Items.Count - 1
            If chkSpelling.Items(i).Selected = True Then
                strContests.Append(chkSpelling.Items(i).Value & "~")
            End If
        Next
        For i = 0 To chkVocab.Items.Count - 1
            If chkVocab.Items(i).Selected = True Then
                strContests.Append(chkVocab.Items(i).Value & "~")
            End If
        Next

        For i = 0 To chkMath.Items.Count - 1
            If chkMath.Items(i).Selected = True Then
                strContests.Append(chkMath.Items(i).Value & "~")
            End If
        Next
        For i = 0 To chkGeography.Items.Count - 1
            If chkGeography.Items(i).Selected = True Then
                strContests.Append(chkGeography.Items(i).Value & "~")
            End If
        Next
        For i = 0 To chkEssay.Items.Count - 1
            If chkEssay.Items(i).Selected = True Then
                strContests.Append(chkEssay.Items(i).Value & "~")
            End If
        Next
        For i = 0 To chkSpeaking.Items.Count - 1
            If chkSpeaking.Items(i).Selected = True Then
                strContests.Append(chkSpeaking.Items(i).Value & "~")
            End If
        Next
        For i = 0 To chkBrain.Items.Count - 1
            If chkBrain.Items(i).Selected = True Then
                strContests.Append(chkBrain.Items(i).Value & "~")
            End If
        Next


        Try
            Dim prmArray(5) As SqlParameter

            cmd.Connection = conn
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = "usp_InsertNewEvent"
            conn.Open()

                cmd.Parameters.AddWithValue("@Contest_Type", DbType.Int32).Value = Server.HtmlEncode(rblContestType.SelectedValue)
                cmd.Parameters.AddWithValue("@Contest_Year", DbType.String).Value = (Server.HtmlEncode(ddlEventYear.SelectedValue))
                If Not Server.HtmlEncode(txtContestDate.Text) = "" Then
                    cmd.Parameters.AddWithValue("@Contest_Date", DbType.DateTime).Value = Convert.ToDateTime(Server.HtmlEncode(txtContestDate.Text))
                Else
                    cmd.Parameters.AddWithValue("@Contest_Date", DbType.DateTime).Value = DBNull.Value
                End If
                cmd.Parameters.AddWithValue("@ChapterID", DbType.String).Value = Session("ChapterID")
                cmd.Parameters.AddWithValue("@Contests", DbType.String).Value = Server.HtmlEncode(strContests.ToString)

            cmd.ExecuteScalar()

            conn.Close()
        Catch ex As SqlException
                MsgBox(ex.ToString())
        Finally

            cmd = Nothing
            conn = Nothing

        End Try

            Select Case Application("ContestType").ToString
                Case "1"
                    hlinkChapterFunctions.Text = "Back to Volunteer Functions"
                    Response.Redirect("UserFunctions.aspx")
                Case "2"
                    Response.Redirect("EventsList.aspx")
            End Select

    End Sub

    Private Sub GetContestForYear(ByVal Year As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim dsChapters As New DataSet
        Dim tblChapters() As String = {"Chapters"}
        Dim dsContestCategory As New DataSet
        Dim tblContestCategory() As String = {"Spelling", "Vocab", "Math", "Geography", "Essay", "Speaking", "Brain"}
        Dim prmArray(2) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ContestYear"
        prmArray(0).Value = ddlEventYear.SelectedValue
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestType"
        prmArray(1).Value = Convert.ToInt16(Application("ContestType"))
        prmArray(1).Direction = ParameterDirection.Input

        '*** Populate Event Name List
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_SelectContestCategoryByYear", dsContestCategory, tblContestCategory, prmArray)
        If dsContestCategory.Tables.Count = 7 Then
            chkSpelling.DataSource = dsContestCategory.Tables(0).DefaultView()
            chkSpelling.DataValueField = dsContestCategory.Tables(0).Columns.Item("ContestCategoryID").ToString
            chkSpelling.DataTextField = dsContestCategory.Tables(0).Columns.Item("ContestDesc").ToString
            chkSpelling.DataBind()

            chkVocab.DataSource = dsContestCategory.Tables(1).DefaultView()
            chkVocab.DataValueField = dsContestCategory.Tables(1).Columns.Item("ContestCategoryID").ToString
            chkVocab.DataTextField = dsContestCategory.Tables(1).Columns.Item("ContestDesc").ToString
            chkVocab.DataBind()

            chkMath.DataSource = dsContestCategory.Tables(2).DefaultView()
            chkMath.DataValueField = dsContestCategory.Tables(2).Columns.Item("ContestCategoryID").ToString
            chkMath.DataTextField = dsContestCategory.Tables(2).Columns.Item("ContestDesc").ToString
            chkMath.DataBind()

            chkGeography.DataSource = dsContestCategory.Tables(3).DefaultView()
            chkGeography.DataValueField = dsContestCategory.Tables(3).Columns.Item("ContestCategoryID").ToString
            chkGeography.DataTextField = dsContestCategory.Tables(3).Columns.Item("ContestDesc").ToString
            chkGeography.DataBind()

            chkEssay.DataSource = dsContestCategory.Tables(4).DefaultView()
            chkEssay.DataValueField = dsContestCategory.Tables(4).Columns.Item("ContestCategoryID").ToString
            chkEssay.DataTextField = dsContestCategory.Tables(4).Columns.Item("ContestDesc").ToString
            chkEssay.DataBind()

            chkSpeaking.DataSource = dsContestCategory.Tables(5).DefaultView()
            chkSpeaking.DataValueField = dsContestCategory.Tables(5).Columns.Item("ContestCategoryID").ToString
            chkSpeaking.DataTextField = dsContestCategory.Tables(5).Columns.Item("ContestDesc").ToString
            chkSpeaking.DataBind()

            chkBrain.DataSource = dsContestCategory.Tables(6).DefaultView()
            chkBrain.DataValueField = dsContestCategory.Tables(6).Columns.Item("ContestCategoryID").ToString
            chkBrain.DataTextField = dsContestCategory.Tables(6).Columns.Item("ContestDesc").ToString
            chkBrain.DataBind()
        End If

        conn = Nothing
        dsChapters = Nothing
        dsContestCategory = Nothing
        tblChapters = Nothing
        tblContestCategory = Nothing


    End Sub
    Private Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        GetContestForYear(ddlEventYear.SelectedValue)
    End Sub
End Class

End Namespace

