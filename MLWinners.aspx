﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" Debug="true" CodeFile="MLWinners.aspx.cs" Inherits="MLWinners" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        NSF Children in Major Leagues
             <br />
        <br />
    </div>


    <table style="width: 25%; margin-left: 0px;">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <asp:Button ID="btnExport1" runat="server" Text="Export to Excel"
                    OnClick="btnExport1_Click" /></td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Child Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td align="left" nowrap="nowrap" style="width: 237px">
                <asp:TextBox ID="txtChildName" runat="server" Width="147px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsearchChild" runat="server" Text="Search"
                    OnClick="btnsearchChild_Click" />
                <asp:HiddenField ID="hdnChildID" runat="server" />
                <asp:HiddenField ID="hdnMemberid" runat="server" />
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">Contest Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddlContestName" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td></td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Contest Year</td>
            <td align="left" style="width: 237px;">
                <asp:DropDownList ID="ddlContestYear" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap" style="width: 142px;" width="140px"></td>
            <td align="left" nowrap="nowrap" style="width: 103px; height: 3px;">Place&nbsp;</td>
            <td align="left" style="height: 3px; width: 141px;">
                <asp:DropDownList ID="ddlPlace" runat="server" Width="150px">
                </asp:DropDownList>
            </td>

        </tr>
        <tr>
            <td align="left" nowrap="nowrap" style="height: 3px">Grade</td>
            <td align="left" style="height: 3px; width: 237px">
                <asp:DropDownList ID="ddlGrade" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap" style="width: 142px; height: 3px;"></td>
            <td align="left" nowrap="nowrap" style="width: 103px">Amount</td>
            <td align="left" style="width: 141px">
                <asp:TextBox ID="txtAmount" runat="server" Width="147px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAmount" Display="Dynamic" ErrorMessage="Invalid Amount" ValidationExpression="^(?:\d+(?:,\d{3})*(?:\.\d{2})?|\d+(?:\.\d{3})*(?:,\d{2})?)$"></asp:RegularExpressionValidator>
            </td>

        </tr>
        <tr>
            <td align="left" nowrap="nowrap">Chapter</td>
            <td align="left" style="width: 237px">
                <asp:DropDownList ID="ddlChapter" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap" style="width: 142px"></td>
            <td style="width: 141px" align="left">City&nbsp;</td>
            <td style="width: 141px" align="left">
                <asp:TextBox ID="TxtCity" runat="server" Width="147px"></asp:TextBox></td>
            <td align="left" style="width: 141px">&nbsp;</td>
        </tr>

        <tr>
            <td align="left" nowrap="nowrap">&nbsp;</td>
            <td align="left" style="width: 237px">
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="75px"
                    OnClick="btnAdd_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="75px"
                    OnClick="btnCancel_Click" />
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;</td>
            <td align="left" style="width: 141px">State&nbsp;</td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddlState" runat="server" Width="150px">
                </asp:DropDownList></td>
            <td align="left" style="width: 141px"></td>
        </tr>
        <tr>
            <td align="left" nowrap="nowrap">&nbsp;</td>
            <td align="left" style="width: 237px">
                <asp:Label ID="lblA" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
            <td nowrap="nowrap" style="width: 142px">&nbsp;</td>
            <td align="left" nowrap="nowrap" style="width: 103px">&nbsp;</td>
            <td align="left" style="width: 141px"></td>
            <td align="left" style="width: 141px">&nbsp;</td>
        </tr>
    </table>

    <div align="center" id="divChildSearch" runat="server" title="Search Child" visible="false">
        <div align="left" style="font-size: medium">
            <asp:Label ID="lblSearch" runat="server" CssClass="btn_02"></asp:Label>
            <strong style="font-size: small" class="btn_02"></strong>
        </div>

        <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%" visible="true" bgcolor="silver" __designer:mapid="1ca">
            <tr __designer:mapid="1d1">
                <td class="ItemLabel" valign="top" nowrap align="right" __designer:mapid="1d2">&nbsp;Name:</td>
                <td align="left" __designer:mapid="1d3">
                    <asp:TextBox ID="txtChName" runat="server"></asp:TextBox></td>
            </tr>
            <tr __designer:mapid="1d5">
                <td class="ItemLabel" valign="top" nowrap align="right" __designer:mapid="1d6">&nbsp;Principal Parent Name:</td>
                <td align="left" __designer:mapid="1d7">
                    <asp:TextBox ID="txtParentName" runat="server"></asp:TextBox></td>
            </tr>

            <tr __designer:mapid="1dd">
                <td class="ItemLabel" valign="top" nowrap align="right" __designer:mapid="1de">&nbsp;E-Mail:</td>
                <td align="left" __designer:mapid="1df">
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
            </tr>
            <tr __designer:mapid="1e1">
                <td class="ItemLabel" valign="top" nowrap align="right" __designer:mapid="1e2">&nbsp;Phone Number Contains:</td>
                <td align="left" __designer:mapid="1e3">
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></td>
            </tr>

            <tr __designer:mapid="1e5">
                <td align="right" __designer:mapid="1e6">
                    <asp:Button ID="btnSearch" runat="server" Text="Find" CausesValidation="False"
                        OnClick="btnSearch_Click" />
                    <asp:Button ID="btnclose" runat="server" Text="Close"
                        OnClick="btnclose_Click" />
                </td>
                <%--  <td  align="left">					
		                <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		            </td>--%>
            </tr>
        </table>

        <br />
        <asp:Label ID="lblchError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        <br />
        <asp:GridView ID="gvChSearch" runat="server" EnableModelValidation="True"
            OnRowCommand="gvChSearch_RowCommand" OnRowDataBound="gvChSearch_RowDataBound">
            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select" />
            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>
        <br />
    </div>
    <br />
    <div runat="server" id="fetchgrid" align="center">
        <asp:GridView ID="GVRecords" runat="server" EnableModelValidation="True"
            OnRowCommand="GVRecords_RowCommand" AutoGenerateColumns="false" HeaderStyle-BackColor="#ffffcc" PageSize="50" AllowPaging="true" OnPageIndexChanging="GVRecords_PageIndexChanging">

            <Columns>
                <asp:ButtonField ButtonType="Button" CommandName="Select" HeaderText="Modify" Text="Modify" />


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MJLeagueID" HeaderText="MJLeagueID"></asp:BoundField>
                <asp:BoundField DataField="Contestyear" HeaderText="Contestyear"></asp:BoundField>
                <asp:BoundField DataField="ChildName" HeaderText="ChildName"></asp:BoundField>
                <asp:BoundField DataField="chapter" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                <asp:BoundField DataField="Place" HeaderText="Place"></asp:BoundField>
                <asp:BoundField DataField="Contest" HeaderText="Contest Name"></asp:BoundField>
                <asp:BoundField DataField="Amount" HeaderText="Amount"></asp:BoundField>
                <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>


            </Columns>
            <RowStyle HorizontalAlign="Left" />
        </asp:GridView>
    </div>
</asp:Content>

