Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data

Partial Class Email_TC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'User code to initialize the page here
        'only Roleid = 1 or 2, Can access this page
        If Not Page.IsPostBack Then

            Session("EmailSource") = "Email_TC"
            Session("Productid") = ""
            Dim roleid As Integer = Session("RoleID")
            LoadYear()
            LoadEvent()
            LoadRoleforassignedVol()

            'If (roleid = 1) Or (roleid = 2) Or (roleid = 5 And Session("SelChapterID") = 1) Then
            'ElseIf (roleid = 3) Or (roleid = 4) Or (roleid = 5) Then
            '    lblerr.Visible = True
            '    tabletarget.Visible = False
            'End If

        End If
        Session("emaillist") = ""
    End Sub
    Private Sub LoadYear()

        Dim first_year As Integer = 2007
        Dim year As Integer

        Year = Convert.ToInt32(DateTime.Now.Year)
        Dim count As Integer = year - first_year

        For i As Integer = 0 To count - 1 'first_year To Convert.ToInt32(DateTime.Now.Year)
            lstyear.Items.Insert(i, New ListItem(year - i, year - i))
        Next
      
        'lstyear.Items(1).Selected = True
        ' Session("Year") = lstyear.SelectedItem.Value '.Items(0).Value
        Dim sbvalues As New StringBuilder
        If lstyear.Items.Count > 0 Then
            lstyear.Items.Insert(0, New ListItem("All", 0))
            lstyear.Items(0).Selected = True

            Dim i As Integer
            For i = 1 To lstyear.Items.Count - 1
                sbvalues.Append(lstyear.Items(i).ToString)
                If i < lstyear.Items.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Session("Year") = sbvalues.ToString
        End If
    End Sub
    Private Sub LoadEvent()

        Dim strSql As String = ""
        '"SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ")  and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
        strSql = "SELECT  Distinct EventId,Name from Event where EventID in (1,2)"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            drpevent.DataSource = ds
            drpevent.DataBind()
            drpevent.Items.Insert(0, New ListItem("All", 0))
            drpevent.Items(0).Selected = True

            LoadChapterList()
        End If
    End Sub

    Private Sub LoadRoleforassignedVol()
        Dim strSql As String = "Select RoleID, Name from Role where Chapter = 'Y'  and RoleID in (5,9,15,16,17,18,19,20,21) order by RoleID" ',63
        Dim drrole As SqlDataReader
        lstAssignRole.Items.Clear()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstAssignRole.DataSource = drrole
        lstAssignRole.DataBind()
        lstAssignRole.Items.Insert(0, "All")
        lstAssignRole.Items(0).Selected = True
        lstAssignRole.Items.Insert(lstAssignRole.Items.Count, New ListItem("Signature", 94))
        lstAssignRole.Enabled = True

    End Sub

    Protected Sub drpevent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        txtselectedchapter.Text = ""
        LoadChapterList()
        LoadYear()
        'LoadProductGroup()
        'LoadProductID()
        'LoadRoleforassignedVol()
        'LoadWeekList()
    End Sub
    Protected Sub LoadChapterList()
        lstchapter.Items.Clear()
        If drpevent.SelectedValue = 0 Then
            Dim strSql As String = "Select chapterID, ChapterCode from Chapter Order by State, ChapterCode"
            Dim chapters As SqlDataReader
            chapters = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            lstchapter.DataSource = chapters
            lstchapter.DataBind()
            'Session("selectedChapter") = 1
        ElseIf drpevent.SelectedValue = 1 Then
            Dim strSql As String = "Select chapterID, ChapterCode from Chapter a where ChapterID = 1  Order by State, ChapterCode"
            Dim chapters As SqlDataReader
            chapters = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            lstchapter.DataSource = chapters
            lstchapter.DataBind()
            lstchapter.Items(0).Selected = True
            Session("selectedChapter") = 1
            'LoadWeekList()
        Else
            If Session("RoleId") = 1 Or Session("RoleId") = 2 Then
                Dim strSql As String = "Select chapterID, ChapterCode from Chapter a where ChapterID > 1  Order by State, ChapterCode"
                Dim chapters As SqlDataReader
                chapters = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                lstchapter.DataSource = chapters
                lstchapter.DataBind()
            ElseIf Session("RoleId") = 3 Or Session("RoleId") = 4 Or Session("RoleId") = 5 Then
                Dim strSql As String = "Select chapterID, ChapterCode from Chapter a where ChapterID in (Select ChapterID from Volunteer where RoleID in (" & Session("RoleId") & " )and memberid = " & Session("LoginID") & ") order by state, chaptercode"
                Dim chapters As SqlDataReader
                chapters = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                lstchapter.DataSource = chapters
                lstchapter.DataBind()
            End If
        End If
        If lstchapter.Items.Count > 0 Then
            If lstchapter.Items.Count > 1 Then
                lstchapter.Items.Insert(0, New ListItem("All", 0))


                Dim sbvalues As New StringBuilder
                'Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                   Dim i As Integer
                For i = 0 To lstchapter.Items.Count - 1
                    sbvalues.Append(lstchapter.Items(i).Value.ToString)
                    If i < lstchapter.Items.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("selectedChapter") = sbvalues.ToString
            End If
            lstchapter.Items(0).Selected = True
            LoadWeekList()
        End If
        'tabletarget.Visible = False
    End Sub
    Protected Sub LoadWeekList()
        Try
            lblPageErr.Text = ""
            lstWeekOf.Items.Clear()
            Dim strSql As String = ""
            strSql = "Select Distinct (Convert(varchar(14),C.ContestDate,106) +  ' - ' + Convert(varchar(14),C1.ContestDate,106)) as Week, Year(C.ContestDate) as Year,C.ContestDate,C1.ContestDate from "
            strSql = strSql & " Contest C join Contest C1 On C.Contest_Year = C1.Contest_Year and C.EventID =C1.EventID and  C.ContestID <> C1.ContestID and DATEDIFF(dd,C.ContestDate,C1.ContestDate)=1"
            strSql = strSql & " Where (C.ContestDate is not null and c1.ContestDate is not null) " & IIf(drpevent.SelectedValue > 0, " and C.EventID =" & drpevent.SelectedValue, " and C.EventID in (1,2) ") & " and  C.Contest_Year in (" & Session("Year") & ")" & IIf(lstchapter.SelectedIndex > 0, " and (C.NSFChapterID in (" & Session("selectedChapter") & ") Or C1.NsfChapterID in (" & Session("selectedChapter") & "))", "")
            strSql = strSql & " Group by C.Eventid, C.NSFChapterID, C.Contest_Year, C.ContestDate,C1.ContestDate order by Year(C.ContestDate) desc,C.ContestDate Asc "
            'Response.Write(strSql)

            Dim WeekList As SqlDataReader
            WeekList = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            lstWeekOf.DataSource = WeekList
            lstWeekOf.DataBind()
            If lstWeekOf.Items.Count > 0 Then

                lstWeekOf.Items.Insert(0, New ListItem("All", 0))
                lstWeekOf.Enabled = True
                lstWeekOf.Items(0).Selected = True

                Dim sbvalues As New StringBuilder
                Dim i As Integer
                For i = 1 To lstWeekOf.Items.Count - 1
                    sbvalues.Append("'")
                    sbvalues.Append(lstWeekOf.Items(i).ToString).Replace("-", "','")
                    sbvalues.Append("'")
                    If i < lstWeekOf.Items.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Dim st As String = sbvalues.ToString
                Session("Week") = sbvalues.ToString
                LoadProductGroup()
            Else
                lstProductGroup.Items.Clear()
                lstProductid.Items.Clear()
                lblPageErr.Text = "No ContestDates present for the Selections made. "
                Exit Sub
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Private Sub LoadProductGroup()
        Try
            lblPageErr.Text = ""
            Dim eventid As String = drpevent.SelectedValue
            Dim strSql As String = ""
            lstProductGroup.Items.Clear()
            '"SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ")  and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            If eventid = "1" Then
                strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") and P.EventId in (" & eventid & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            ElseIf eventid = "2" Then
                strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") and P.EventId in (" & eventid & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            Else
                'strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") " & IIf(lstchapter.SelectedIndex > 0 And lstchapter.SelectedValue.Contains("1"), " and C.NSFChapterID in (" & Session("selectedChapter") & ") and P.EventId in (1)", " and P.EventId in (2)") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
                'strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID  and C.Contest_Year in (" & Session("Year") & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and P.EventID in (Case when (Select COUNT(Distinct ProductCode) from Contest where EventId = 1 and Contest_Year in (" & Session("Year") & ")) >( Select COUNT(Distinct ProductCode) from Contest where EventId = 2 and Contest_Year in (" & Session("Year") & ")) then 1 Else 2 end ) and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
                strSql = "SELECT  Distinct P.ProductGroupCode, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID  and C.Contest_Year in (" & Session("Year") & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and P.EventID in(1,2)  and C.ContestDate in (" & Session("Week") & ")" ' order by P.ProductGroupID"

            End If

            'Response.Write(strSql)
            'Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ' drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

            lstProductGroup.DataSource = ds
            lstProductGroup.DataBind()
            If lstProductGroup.Items.Count > 0 Then

                lstProductGroup.Items.Insert(0, "All")
                lstProductGroup.Items(0).Selected = True

                Dim sbvalues As New StringBuilder
                'Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append("'")
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ProductGroupCode").ToString)
                        sbvalues.Append("'")
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("ProductGroupId") = sbvalues.ToString
                End If
                LoadProductID()
            Else
                lstProductid.Items.Clear()
                lblPageErr.Text = "No Products present for the Selected chapter(s). "
                Exit Sub
            End If
           
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim eventid As String = drpevent.SelectedValue
            Dim productgroup As String = ""
            'If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            '    lstProductid.Enabled = False
            '    Dim sbvalues As New StringBuilder
            '    Dim strSql1 As String '= "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ")  and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            '    If eventid = "1" Then
            '        strSql1 = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode as ID, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") and P.EventId in (" & eventid & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            '    ElseIf eventid = "2" Then
            '        strSql1 = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode as ID, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") and P.EventId in (" & eventid & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            '    Else
            '        strSql1 = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode as ID, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID  and C.Contest_Year in (" & Session("Year") & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and P.EventID in (Case when (Select COUNT(*) from Contest where EventId = 1 and Contest_Year in (" & Session("Year") & ") )>0 then 1 Else 2 end ) and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            '    End If

            '    Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            '    If ds.Tables.Count > 0 Then
            '        Dim i As Integer
            '        For i = 0 To ds.Tables(0).Rows.Count - 1
            '            sbvalues.Append("'")
            '            sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
            '            sbvalues.Append("'")
            '            If i < ds.Tables(0).Rows.Count - 1 Then
            '                sbvalues.Append(",")
            '            End If
            '        Next
            '        Session("ProductGroupId") = sbvalues.ToString
            '    End If
            'End If
            lstProductid.Items.Clear()
            ' Dim strSql As String = "Select Distinct ProductID,ProductCode, Name from Product where EventID in (Case when (Select COUNT(Distinct ProductCode) from Contest where EventId = 1 and Contest_Year in (" & Session("Year") & ") )> (Select COUNT(Distinct ProductCode) from Contest where EventId = 2 and Contest_Year in (" & Session("Year") & ")) then 1 Else 2 end ) and  Status='O' and ProductGroupCode In (" & Session("ProductGroupId") & ") order by ProductID"
            Dim strSql As String = ""
            If eventid > 0 Then
                strSql = "Select Distinct ProductID,ProductCode, Name from Product where EventID in (" & eventid & ") and  Status='O' and ProductGroupCode In (" & Session("ProductGroupId") & ") order by ProductID"
            Else
                strSql = "Select Distinct ProductCode, Name from Product where EventID in (1,2) and  Status='O' and ProductGroupCode In (" & Session("ProductGroupId") & ")" ' order by ProductID"
            End If
            'Response.Write(strSql)
            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            lstProductid.DataSource = drproductid
            lstProductid.DataBind()
            lstProductid.Items.Insert(0, "All")
            lstProductid.Items(0).Selected = True
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub lstchapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstchapter.SelectedIndexChanged
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstchapter.Items.Count - 1
            If lstchapter.Items(i).Selected And Not lstchapter.Items(i).Value.Trim = "0" Then
                dr = dt.NewRow()
                dr("Values1") = lstchapter.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("selectedChapter") = sbvalues.ToString
        End If

        Dim i1 As Integer
        Dim ds1 As New DataSet
        Dim dt1 As New DataTable("selected")
        Dim dc1 As DataColumn = New DataColumn("Values2")
        dc1.DataType = System.Type.GetType("System.String")
        dt1.Columns.Add(dc1)
        Dim dr1 As DataRow
        For i1 = 0 To lstchapter.Items.Count - 1
            If lstchapter.Items(i1).Selected And Not lstchapter.Items(i1).Value.Trim = "0" Then
                dr1 = dt1.NewRow()
                dr1("Values2") = lstchapter.Items(i1).Text
                dt1.Rows.Add(dr1)
            End If
        Next
        ds1.Tables.Add(dt1)
        ds1.AcceptChanges()

        If ds1.Tables.Count > 0 Then
            Dim sbtext As New StringBuilder
            Dim a1 As Integer
            For a1 = 0 To ds1.Tables(0).Rows.Count - 1
                sbtext.Append(ds1.Tables(0).Rows(a1).Item("Values2").ToString)
                If a1 < ds1.Tables(0).Rows.Count - 1 Then
                    sbtext.Append(";")
                End If
            Next
            txtselectedchapter.Text = sbtext.ToString
        End If
        LoadWeekList()

    End Sub

    Protected Sub lstWeekOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstWeekOf.SelectedIndexChanged
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstWeekOf.Items.Count - 1
            If lstWeekOf.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstWeekOf.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString).Replace("-", "','")
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Week") = sbvalues.ToString
        End If

        LoadProductGroup()
    End Sub
    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If

        lstProductid.Enabled = True
        Session("Productid") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductid.Items.Count - 1
            If lstProductid.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductid.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Productid") = sbvalues.ToString
        End If
        ' End If
    End Sub

    Protected Sub lstyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in year from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow

        For i = 0 To lstyear.Items.Count - 1
            If lstyear.Items(i).Selected Or lstyear.SelectedIndex = 0 Then
                dr = dt.NewRow()
                dr("Values1") = lstyear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Year") = sbvalues.ToString
        End If
        'LoadEvent()
        LoadWeekList()
    End Sub

    Protected Sub lstAssignRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstAssignRole.SelectedIndexChanged
        'on selection change in Role froom Assigned Volunteer 
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstAssignRole.Items.Count - 1
            If lstAssignRole.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstAssignRole.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Assignvolunteer") = sbvalues.ToString
        End If
    End Sub
    Protected Sub btnselectmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnselectmail.Click
        ' Continue to Email button
        ' will select emails depending on selections made
        ' tableemail.Visible = True
        Try
            Dim dsEmails, dsIgnEmails As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim strSql As String = ""
            Dim eventid As String = drpevent.SelectedValue
            ' Dim chapterid As String = ""

            Session("mailEventID") = drpevent.SelectedItem.Value.ToString()

            If lstchapter.Items(0).Selected = True Then  ' if selected item in Chapter is ALL
                Dim sbvalues As New StringBuilder
                If lstchapter.Items.Count > 0 Then
                    Dim i As Integer
                    For i = 1 To lstchapter.Items.Count - 1
                        sbvalues.Append(lstchapter.Items(i).Value)
                        If i < lstchapter.Items.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("selectedChapter") = sbvalues.ToString
                End If
            End If

            If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Chapter is ALL
                Dim sbvalues As New StringBuilder
                If lstProductGroup.Items.Count > 0 Then
                    Dim i As Integer
                    For i = 1 To lstProductGroup.Items.Count - 1
                        sbvalues.Append("'")
                        sbvalues.Append(lstProductGroup.Items(i).Value)
                        sbvalues.Append("'")
                        If i < lstProductGroup.Items.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("ProductGroupId") = sbvalues.ToString
                End If
            End If

            If lstProductid.Items(0).Selected = True Then  ' if selected item in Chapter is ALL
                Dim sbvalues As New StringBuilder
                If lstProductid.Items.Count > 0 Then
                    Dim i As Integer
                    For i = 1 To lstProductid.Items.Count - 1
                        sbvalues.Append("'")
                        sbvalues.Append(lstProductid.Items(i).Value)
                        sbvalues.Append("'")
                        If i < lstProductid.Items.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Productid") = sbvalues.ToString
                End If
            End If


            'If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            '    Dim sbvalues As New StringBuilder
            '    'strSql = "Select ProductGroupID as ID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
            '    'strSql = "Select Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            '    If eventid = "1" Then
            '        strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode as ID , P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") and P.EventId in (" & eventid & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            '    ElseIf eventid = "2" Then
            '        strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode as ID , P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID and P.EventID = C.EventID and C.Contest_Year in (" & Session("Year") & ") and P.EventId in (" & eventid & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            '    Else
            '        strSql = "SELECT  Distinct P.ProductGroupID,P.ProductGroupCode as ID, P.Name from ProductGroup P, Contest C  where P.ProductGroupCode = C.ProductGroupCode and P.ProductGroupID = C.ProductGroupID  and C.Contest_Year in (" & Session("Year") & ") " & IIf(lstchapter.SelectedIndex > 0, " and C.NSFChapterID in (" & Session("selectedChapter") & ")", "") & " and P.EventID in (Case when (Select COUNT(*) from Contest where EventId = 1 and Contest_Year in (" & Session("Year") & ") )>0 then 1 Else 2 end ) and C.ContestDate in (" & Session("Week") & ") order by P.ProductGroupID"
            '    End If
            '    'Response.Write(strSql & "<br />")
            '    Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            '    If ds.Tables.Count > 0 Then
            '        Dim i As Integer
            '        For i = 0 To ds.Tables(0).Rows.Count - 1
            '            sbvalues.Append("'")
            '            sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
            '            sbvalues.Append("'")
            '            If i < ds.Tables(0).Rows.Count - 1 Then
            '                sbvalues.Append(",")
            '            End If
            '        Next
            '        Session("ProductGroupId") = sbvalues.ToString

            '    End If
            'End If

            'If lstProductid.Items(0).Selected = True Then 'And Session("Productid") = "" Then  ' if selected item in ProductId is ALL
            '    Dim sbvalues As New StringBuilder
            '    'strSql = "Select ProductID as ID, Name from Product where EventID in (" & eventid & ") and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
            '    'strSql = "Select ProductCode as ID, Name from Product where " & IIf(eventid > 0, " EventID in (" & eventid & ") and", " EventID in(1,2) and ") & " Status='O' and ProductGroupCode In (" & Session("ProductGroupId") & ") " 'order by ProductID"
            '    strSql = "Select Distinct ProductID,ProductCode as ID, Name from Product where EventID in (Case when (Select COUNT(*) from Contest where EventId = 1 and Contest_Year in (" & Session("Year") & ") )>0 then 1 Else 2 end ) and  Status='O' and ProductGroupCode In (" & Session("ProductGroupId") & ") order by ProductID"
            '    Response.Write(strSql & "<br />")
            '    Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            '    If ds.Tables.Count > 0 Then
            '        Dim i As Integer
            '        For i = 0 To ds.Tables(0).Rows.Count - 1
            '            sbvalues.Append("'")
            '            sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
            '            sbvalues.Append("'")
            '            If i < ds.Tables(0).Rows.Count - 1 Then
            '                sbvalues.Append(",")
            '            End If
            '        Next
            '        Session("Productid") = sbvalues.ToString
            '    End If
            'End If

            If lstWeekOf.Items(0).Selected = True And lstWeekOf.Items.Count > 1 Then  ' if selected item in Productgroup is ALL
                Dim sbvalues As New StringBuilder
                If lstWeekOf.Items.Count > 0 Then
                    Dim i As Integer
                    For i = 1 To lstWeekOf.Items.Count - 1
                        sbvalues.Append("'")
                        sbvalues.Append(lstWeekOf.Items(i).Text.ToString).Replace("-", "','")
                        sbvalues.Append("'")
                        If i < lstWeekOf.Items.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Week") = sbvalues.ToString
                End If
            End If

            If lstAssignRole.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
                Dim sbvalues As New StringBuilder
                If lstAssignRole.Items.Count > 0 Then
                    Dim i As Integer
                    For i = 1 To lstAssignRole.Items.Count - 1
                        sbvalues.Append(lstAssignRole.Items(i).Value)
                        If i < lstAssignRole.Items.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Assignvolunteer") = sbvalues.ToString
                End If
            End If

            Dim StrEmailSel As String = ""
            Dim StrEmailVal As String = ""
            Dim Str_WhereCndn As String = ""
            Dim Str_Where_Contest As String = ""
            Dim Str_IgnoredEmails As String = ""


            If lstAssignRole.Items.Count > 0 Then
                Dim i As Integer
                Str_WhereCndn = " Where " & IIf(lstyear.Items(0).Selected = True, "", " ContestYear in (" & Session("Year") & ") and ") & IIf(drpevent.SelectedValue > 0, " EventID =" & drpevent.SelectedValue & " and ", "") & IIf(lstchapter.SelectedIndex > 0, " ChapterID in (" & Session("selectedChapter") & ") and ", "") & " ProductGroupCode in (" & Session("ProductGroupID") & ") and ProductCode in (" & Session("ProductID") & ") "
                Str_Where_Contest = " Where " & IIf(lstyear.Items(0).Selected = True, "", " Contest_Year in (" & Session("Year") & ") and ") & IIf(drpevent.SelectedValue > 0, " EventID =" & drpevent.SelectedValue & " and ", "") & IIf(lstchapter.SelectedIndex > 0, " NsfChapterID in (" & Session("selectedChapter") & ") and ", "") & " ProductGroupCode in (" & Session("ProductGroupID") & ") and ProductCode in (" & Session("ProductID") & ") and ContestDate in (" & Session("Week") & ")"
                For i = 0 To lstAssignRole.Items.Count - 1

                    If lstAssignRole.Items(i).Selected = True Or lstAssignRole.Items(0).Selected = True Then '' Selected Row or All

                        If lstAssignRole.Items(i).Value = "5" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct MemberID from Volunteer Where RoleID = 5 " & IIf(lstyear.Items(0).Selected = True, "", " and EventYear in (" & Session("Year") & ") ") & IIf(drpevent.SelectedValue > 0, " and EventID =" & drpevent.SelectedValue, "") & IIf(lstchapter.SelectedIndex > 0, " and ChapterID in (" & Session("selectedChapter") & ")", "")
                        ElseIf lstAssignRole.Items(i).Value = "9" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct ExamRecID as MemberID From Contest " & Str_Where_Contest
                        ElseIf lstAssignRole.Items(i).Value = "15" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct PronMemberID  as MemberID From ContestTeamSchedule " & Str_WhereCndn

                        ElseIf lstAssignRole.Items(i).Value = "16" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct CJMemberID   as MemberID From ContestTeamSchedule " & Str_WhereCndn


                        ElseIf lstAssignRole.Items(i).Value = "17" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct AJMemberID   as MemberID From ContestTeamSchedule " & Str_WhereCndn


                        ElseIf lstAssignRole.Items(i).Value = "18" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct LTJudMemberID   as MemberID From ContestTeamSchedule " & Str_WhereCndn


                        ElseIf lstAssignRole.Items(i).Value = "19" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct DictHMemberID   as MemberID From ContestTeamSchedule " & Str_WhereCndn


                        ElseIf lstAssignRole.Items(i).Value = "20" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct ProcMemberID   as MemberID From ContestTeamSchedule " & Str_WhereCndn


                        ElseIf lstAssignRole.Items(i).Value = "21" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct GradMemberID  as MemberID From ContestTeamSchedule " & Str_WhereCndn


                        ElseIf lstAssignRole.Items(i).Value = "94" Then
                            If StrEmailSel <> "" Then
                                StrEmailSel = StrEmailSel & " UNION ALL "
                            End If
                            StrEmailSel = StrEmailSel & " Select Distinct LeftSignID as MemberID From Contest " & Str_Where_Contest
                            StrEmailSel = StrEmailSel & " UNION ALL "
                            StrEmailSel = StrEmailSel & " Select Distinct RightSignID as MemberID From Contest " & Str_Where_Contest
                        End If
                    End If

                    'If i < lstAssignRole.Items.Count - 1 Then
                    '    sbvalues.Append(",")
                    'End If
                Next
                'Response.Write(StrEmailSel)
            End If

            If StrEmailSel <> "" Then
                'StrEmailVal = "Select distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and AutoMemberID in (" & StrEmailSel & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in  (" & StrEmailSel & ")))"

                ''Added on 23-04-2014 to Update Newsletter to 9 (Willing Volunteer) if Newsletter is in 2 or 3 ******************************'
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from IndSpouse where ValidEmailFlag is Null and newsletter in (2,3) and AutoMemberID in (" & StrEmailSel & ") ") > 0 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update IndsPouse set Newsletter=9 where ValidEmailFlag is Null and newsletter in (2,3) and AutoMemberID in (" & StrEmailSel & ")")
                End If

                StrEmailVal = "Select distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Null and (newsletter not in ('2','3') OR Newsletter is null) and AutoMemberID in (" & StrEmailSel & ")"
                dsEmails = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrEmailVal)
                'Response.Write(StrEmailVal)
                Dim tblEmails() As String = {"EmailContacts"}
                Dim sbEmailList As New StringBuilder
                If dsEmails.Tables(0).Rows.Count > 0 Then
                    Dim ctr As Integer
                    For ctr = 0 To dsEmails.Tables(0).Rows.Count - 1
                        sbEmailList.Append(dsEmails.Tables(0).Rows(ctr).Item("EmailID").ToString)
                        If ctr <= dsEmails.Tables(0).Rows.Count - 2 Then
                            sbEmailList.Append(",")
                        End If
                    Next
                    Session("emaillist") = sbEmailList.ToString
                End If
                'Session("sentemaillistenable") = "No" 

                Str_IgnoredEmails = "Select distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null AND ValidEmailFlag is Not Null and AutoMemberID in (" & StrEmailSel & ")"
                dsIgnEmails = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Str_IgnoredEmails)

                Dim sbIgnEmailList As New StringBuilder
                If dsIgnEmails.Tables(0).Rows.Count > 0 Then
                    Dim ctr As Integer
                    For ctr = 0 To dsIgnEmails.Tables(0).Rows.Count - 1
                        sbIgnEmailList.Append(dsIgnEmails.Tables(0).Rows(ctr).Item("EmailID").ToString)
                        If ctr <= dsIgnEmails.Tables(0).Rows.Count - 2 Then
                            sbIgnEmailList.Append(",")
                        End If
                    Next
                    Session("IgnoredEmailList") = sbIgnEmailList.ToString
                End If

                Session("sentemaillistenable") = "No"
                '*****************Move to emaillist.aspx to send Emails*************************************************'
                Response.Redirect("emaillist.aspx")

                'Response.Write(" <br /> " & " <br /> " & Session("emaillist") & " <br /> " & Session("IgnoredEmailList"))
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
End Class
