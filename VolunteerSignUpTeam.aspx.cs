﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;

public partial class VolunteerSignUpTeam : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        EventYear();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void EventYear()
    {


        try
        {
            ddlEventYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = MaxYear-1; i <= DateTime.Now.Year+1; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlEventYear.DataSource = list;
            ddlEventYear.DataTextField = "Text";
            ddlEventYear.DataValueField = "Value";
            ddlEventYear.DataBind();
            ddlEventYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //SessionExp();
        }
       
         
    }
}