﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;

public partial class Game_Reg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["LoggedIn"] == null || Session["LoggedIn"].ToString().ToLower() != "true")
        //{
        //    Server.Transfer("login.aspx?entry=v");
        //}
        //if (!IsPostBack)
        //{
        //    if (Session["LoggedIn"] == null || Session["LoggedIn"].ToString().ToLower() != "true")
        //    {
        //        Server.Transfer("login.aspx?entry=" + Session["entryToken"]);
        //    }
        //    if (Session["RoleId"] != null && (Session["RoleId"].ToString() == "1" | Session["RoleId"].ToString() == "2"))
        //    {

        //    }
        //    else
        //    {

        //    }
        //}
       // FetchRecord("Default");
     
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Clear();
        PnlMain.Visible = false;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Text = "";
            bool datavalid = dataCheck();
            if (datavalid == false) { return; }
            if (!this.IsValid) { return; }          
            DateTime t1 = DateTime.Parse(txtStartDate.Text);
            DateTime t2 = DateTime.Parse(txtEndDate.Text);
            if (t2 <= t1) { lblStatus.Text = "End Date must be greater than Start Date."; return; } else { lblStatus.Text = ""; }
            string sqlUpdate=string.Empty;
            if (ddlApproved.SelectedItem.Text == "NULL")
            {
                sqlUpdate = " UPDATE GAME SET ChildLoginID ='" + txtLoginId.Text + "',ChildPWD='" + txtPassword.Text + "',StartDate ='" + txtStartDate.Text + "',EndDate='" + txtEndDate.Text + "',Approved=NULL,Modifydate=getdate(),Modifiedby=" + Session["LoginID"].ToString() + " Where GameID=" + Session["GameID"].ToString();
            }
            else
            {
                sqlUpdate = " UPDATE GAME SET ChildLoginID ='" + txtLoginId.Text + "',ChildPWD='" + txtPassword.Text + "',StartDate ='" + txtStartDate.Text + "',EndDate='" + txtEndDate.Text + "',Approved='" + ddlApproved.SelectedItem.Text +"',Modifydate=getdate(),Modifiedby=" + Session["LoginID"].ToString() + " Where GameID=" + Session["GameID"].ToString();
            }
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlUpdate);            
            Clear();
            PnlMain.Visible = false;
            lblStatus.Text = "Record updated succesfully.";
            FetchRecord(ddlChoice.SelectedItem.Value); 

        }
        catch (Exception ex)
        {
            lblStatus.Text = ex.Message.ToString();
        }
    }
    protected void ddlChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    FetchRecord(ddlChoice.SelectedItem.Value);
      
    }
    protected void FetchRecord(string ipValue)
    {
        string whCont=string.Empty;
        lblStatus.Text ="";
        if (ipValue == "1") { whCont = " and g.Approved ='YES' "; }
        else if (ipValue == "2") { whCont = " and (g.Approved is null or g.Approved ='NO')"; }
        else if (ipValue == "Default") { whCont = string.Empty; }
        string sqlStr = " Select g.GameID, c.first_name + ' ' + c.last_name ChildName, p.name as GameName, convert (varchar(10),g.CreateDate,101) as CreateDate, g.ChildLoginID, g.ChildPWD, g.Approved, convert (varchar(10),g.StartDate,101) as StartDate, ";
        sqlStr += " convert (varchar(10),g.EndDate,101) as EndDate, i.lastname + ' ' + i.firstname ParentName, i.hphone, i.Email,convert (varchar(10),g.PaymentDate,101) as PaymentDate,g.PaymentReference, g.PaymentNotes,i.ChapterID, i.Chapter, i.City, i.State from Game g, Child c, Product p, IndSpouse i ";
        sqlStr +=" where g.EndDate >= GetDate() and g.childnumber = c.childnumber and g.eventid = p.eventid and g.productcode = p.productcode and  ";
        sqlStr += " g.memberid = i.automemberid " + whCont + " order by g.gameid DESC";
        try
        {
         DataSet dsMain=SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString (),CommandType.Text,sqlStr );
        if(dsMain.Tables[0].Rows.Count>0)
        {
         DataTable dt=dsMain.Tables[0];
         gvGame.DataSource=dt;
         gvGame.DataBind();
         Session["GameData"]=dt;
         lblStatus.Text ="";
        }
        else
        {
            lblStatus.Text ="No Records Found.";
        }
        }
        catch (Exception ex){}
    }
    protected void gvGame_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvGame.PageIndex = e.NewPageIndex;
            gvGame.DataSource = (DataTable)Session["GameData"];
            gvGame.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void gvGame_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int index = int.Parse(e.CommandArgument.ToString());
            int TransID;
            TransID = Int32.Parse(gvGame.Rows[index].Cells[1].Text);
            Session["GameID"] = TransID;
            if (e.CommandName == "Select")
            {
                txtLoginId.Text = gvGame.Rows[index].Cells[5].Text;
                txtPassword.Text = gvGame.Rows[index].Cells[6].Text;
                txtStartDate.Text = gvGame.Rows[index].Cells[8].Text;
                txtEndDate.Text = gvGame.Rows[index].Cells[9].Text;
                string txtValue = string.Empty;
                if (gvGame.Rows[index].Cells[7].Text == string.Empty)
                {
                    txtValue = "NULL";
                }
                else
                {
                    txtValue = gvGame.Rows[index].Cells[7].Text;
                }
                ddlApproved.SelectedIndex = ddlApproved.Items.IndexOf(ddlApproved.Items.FindByText(txtValue));
                PnlMain.Visible = true;
            }
        }
        catch (Exception ex) { }
    }
    protected bool dataCheck()
    {          
        bool state = true;
        if (txtLoginId.Text.Trim().Length < 1)
        {
            lblStatus.Text = "Please type a LoginID.";
            state = false;
            return state;
        }
        if (txtPassword.Text.Trim().Length < 1)
        {
            lblStatus.Text = "Please type a Password.";
            state = false;
            return state;
        }
        if (txtStartDate.Text.Trim().Length < 1)
        {
            lblStatus.Text = "Please type Start Date.";
            state = false;
            return state;
        }
        if (txtEndDate.Text.Trim().Length < 1)
        {
            lblStatus.Text = "Please type End Date.";
            state = false;
            return state;
        }
        if (ddlApproved.SelectedItem.Value == "-1")
        {
            lblStatus.Text = "Please select Approved Status.";
            state = false;
            return state;
        }
        return state;
    }
    protected void Clear()
    {
        try
        {
            lblStatus.Text = "";
            txtLoginId.Text = "";
            txtPassword.Text = "";
            txtStartDate.Text = "";
            txtEndDate.Text = "";
            ddlApproved.SelectedIndex = 0;
            gvGame.SelectedIndex = -1;
        }
        catch (Exception ex) { }
            
    }
    protected void gvGame_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
               // e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
                if (e.Row.Cells[i].Text == "&nbsp;") { e.Row.Cells[i].Text = String.Empty; }
            }
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    e.Row.Cells[6].Text = Convert.ToDecimal(e.Row.Cells[6].Text).ToString("n2");
            //}
        }
        catch (Exception ex)
        {

        }
    }
    protected void gvGame_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}