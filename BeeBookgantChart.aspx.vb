﻿Imports System.Collections.Generic
Imports Microsoft.ApplicationBlocks.Data

Partial Class BeeBook_BeeBookgantChart
    Inherits System.Web.UI.Page
    Dim xmlData As String
    Dim cmdText As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        'If Session("LoginID") Is Nothing Then
        '    Response.Redirect("~/maintest.aspx")
        'End If
        If IsPostBack = False Then
            CreateChart()
            'xmlData = "<NewDataSet>"
            'xmlData += "    <group>"
            'xmlData += "      <name>Recruiting</name>"
            'xmlData += "      <blockcolor>#ff0000</blockcolor>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=7</href>"
            'xmlData += "        <StartDate>2014-11-01</StartDate>"
            'xmlData += "        <EndDate>2014-11-01</EndDate>"
            'xmlData += "        <name>Recruit Team Lead</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2014-11-01</StartDate>"
            'xmlData += "        <EndDate>2014-11-15</EndDate>"
            'xmlData += "        <name> Recruit Editors </name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2014-11-15</StartDate>"
            'xmlData += "        <EndDate>2014-11-29</EndDate>"
            'xmlData += "        <name>Secure Software</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2014-11-29</StartDate>"
            'xmlData += "        <EndDate>2015-12-27</EndDate>"
            'xmlData += "        <name>Learning Software</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-12-27</StartDate>"
            'xmlData += "        <EndDate>2015-01-10</EndDate>"
            'xmlData += "        <name>Load Previous Bee Book</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-01-10</StartDate>"
            'xmlData += "        <EndDate>2015-01-24</EndDate>"
            'xmlData += "        <name>Prepare Schedules</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-01-24</StartDate>"
            'xmlData += "        <EndDate>2015-02-21</EndDate>"
            'xmlData += "        <name>Front Cover</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-02-21</StartDate>"
            'xmlData += "        <EndDate>2014-03-21</EndDate>"
            'xmlData += "        <name>Back Cover</name>"
            'xmlData += "      </block>"
            'xmlData += "    </group>"
            'xmlData += "    <group>"
            'xmlData += "      <name>Article 1</name>"
            'xmlData += "      <blockcolor>#99ff66</blockcolor>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=14</href>"
            'xmlData += "        <StartDate>2015-06-07</StartDate>"
            'xmlData += "        <EndDate>2015-06-07</EndDate>"
            'xmlData += "        <name>Invite articles</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=15</href>"
            'xmlData += "        <StartDate>2015-06-07</StartDate>"
            'xmlData += "        <EndDate>2015-07-05</EndDate>"
            'xmlData += "        <name>Deadline For Articles</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=15</href>"
            'xmlData += "        <StartDate>2015-07-05</StartDate>"
            'xmlData += "        <EndDate>2015-07-19</EndDate>"
            'xmlData += "        <name>Review Articles</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=15</href>"
            'xmlData += "        <StartDate>2015-07-19</StartDate>"
            'xmlData += "        <EndDate>2015-07-26</EndDate>"
            'xmlData += "        <name>Edit And placements</name>"
            'xmlData += "      </block>"
            'xmlData += "    </group>"


            'xmlData += "</NewDataSet>"
            'EventCalendarControl1.XMLData = xmlData
            'EventCalendarControl1.BlankGifPath = "trans.gif"
            'EventCalendarControl1.Year = Now.Year
            'EventCalendarControl1.BeginDate = "01/11/2014"
            'EventCalendarControl1.EndDate = "01/09/2015"
            'EventCalendarControl1.Quarter = 4
            'EventCalendarControl1.BlockColor = "blue"
            'EventCalendarControl1.ToggleColor = "#dcdcdc"
            'EventCalendarControl1.CellHeight = 15
            'EventCalendarControl1.CellWidth = 15
            'EventCalendarControl1.EnableViewState = True

            ' BindbeeBookSchedule()
        End If
    End Sub
    Public m_Rnd As New Random
    Public Function RandomRGBColor() As Color

        Return Color.FromArgb(255, _
            m_Rnd.Next(0, 255), _
            m_Rnd.Next(0, 255), _
            m_Rnd.Next(0, 255))
    End Function
    Private Sub CreateChart()

        Try

            Dim inputs() As String = {"#0000FF", "#FFA500", "#808000", "#FF00FF", "#800080", "#848482", "#348017", "#E2A76F", "#E56717", "#8E35EF", "#95F035", "#088FF0", "#FFA500", "#8CC403", "#FF3C08", "#24E4C2", "#F518CB"}
            Dim lstOfColors As New ArrayList
            lstOfColors.Add("#E94C6F")
            lstOfColors.Add("#A23BEC")
            lstOfColors.Add("#7F38EC")
            lstOfColors.Add("#DB3340")
            lstOfColors.Add("#E8B71A")
            lstOfColors.Add("#FA6900")
            lstOfColors.Add("#6AA121")
            lstOfColors.Add("#982395")
            lstOfColors.Add("#0020C2")
            lstOfColors.Add("#461B7E")
            lstOfColors.Add("#83BF17")
            lstOfColors.Add("#1352A2")

            lstOfColors.Add("#333300")
            lstOfColors.Add("#CC0063")
            lstOfColors.Add("#3F0082")
            lstOfColors.Add("#FF8C00")
            lstOfColors.Add("#ff8000")
            lstOfColors.Add("#8CC403")
            lstOfColors.Add("#FF3C08")
            Dim lstOfProdGrp As New ArrayList
            Dim dStartDate As DateTime, dEndDate As DateTime
            dStartDate = Nothing
            dEndDate = Nothing


            Dim dsDetails As DataSet
            If ddYear.SelectedValue = -1 Then
                cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList"
            Else
                cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList"
                'cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList where Year=" & ddYear.SelectedValue & " order by ComponentID DESC"
            End If


            dsDetails = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If ddYear.SelectedValue = -1 Then
                cmdText = "select distinct Components,ComponentID from BeeBookSchedule"
            Else
                cmdText = "select distinct Components,ComponentID from BeeBookSchedule"
            End If

            Dim dsPGroup As DataSet
            dsPGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            'div1.Style.Add("visibihttp://localhost:49160/App8Full/bin/Interop.LPICOM_6_0Lib.dlllity", "hidden")
            Response.Write(dsDetails.Tables(0).Rows.Count)
            If dsDetails.Tables(0).Rows.Count = 0 Then
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "No Record Exists!"

            Else
                div1.Style.Add("visibility", "visible")
                lblMessage.Text = ""
                ' EventCalendarControl1.Style.Add("Display", "None")
            End If
            xmlData = Nothing
            Dim startDate As String
            startDate = Nothing
            'For Each drGrp As DataRow In dsPGroup.Tables(0).Rows
            '    If lstOfProdGrp.Contains(drGrp("ComponentID")) = False Then
            '        lstOfProdGrp.Add(drGrp("ComponentID"))
            '    End If
            '    xmlData += "    <group>"
            '    xmlData += "      <name>" & drGrp("Components") & "</name>"
            '    xmlData += "      <blockcolor>" & lstOfColors.Item(lstOfProdGrp.IndexOf(drGrp("ComponentID"))) & "</blockcolor>"
            '    For Each dr As DataRow In dsDetails.Tables(0).Select("ComponentID =" & drGrp("ComponentID"))
            '        If startDate = Nothing Then
            '            startDate = dr("EndDate")
            '        End If

            '        xmlData += "      <block>"
            '        xmlData += "        <StartDate>" & dr("EndDate") & "</StartDate>"
            '        xmlData += "        <EndDate>" & dr("EndDate") & "</EndDate>"
            '        xmlData += "        <name>" & dr("SubComponents") & "</name>"
            '        xmlData += "      </block>"

            '    Next
            '    xmlData += "    </group>"
            'Next
            Dim i As Integer
            i = 0

            Dim duration As Integer
            For Each drGrp As DataRow In dsPGroup.Tables(0).Rows
                If lstOfProdGrp.Contains(drGrp("ComponentID")) = False Then
                    lstOfProdGrp.Add(drGrp("ComponentID"))
                End If
                xmlData += "    <group>"
                xmlData += "      <name>" & drGrp("Components") & "</name>"
                xmlData += "      <rowspan>1</rowspan>"
                xmlData += "      <blockcolor>" & lstOfColors.Item(lstOfProdGrp.IndexOf(drGrp("ComponentID"))) & "</blockcolor>"
                For Each dr As DataRow In dsDetails.Tables(0).Select("ComponentID =" & drGrp("ComponentID"))
                    If startDate = Nothing Then
                        startDate = dr("EndDate")
                    End If


                    Dim answer = dr("EndDate").AddDays(-(dr("Duration")))
                    If i >= 12 Then
                        i = 0
                    End If
                    duration = dr("Duration")
                    If duration = 0 Then
                        duration = 7
                    ElseIf duration < 6 Then
                        duration = 8
                    ElseIf duration < 10 Then
                        duration = 9
                    End If
                    duration = duration * 8
                    If duration > 200 Then
                        duration = 175


                    End If

                    xmlData += "      <block>"
                    xmlData += "        <href>activity.aspx?ActID=7</href>"
                    xmlData += "        <StartDate>" & answer & "</StartDate>"
                    xmlData += "        <EndDate>" & dr("EndDate") & "</EndDate>"
                    xmlData += "        <name>" & dr("SubComponents") & "</name>"
                    xmlData += "        <color>" & lstOfColors(i) & "</color>"
                    xmlData += "        <colspan>" & dr("Duration") & "</colspan>"
                    xmlData += "        <Duration>" & duration & "</Duration>"
                    xmlData += "      </block>"
                    startDate = dr("EndDate")
                    i = i + 1
                Next
                xmlData += "    </group>"
            Next

            xmlData = "<NewDataSet>" + xmlData + "</NewDataSet>"

            If dStartDate = Nothing And dEndDate = Nothing Then
                Dim dtMin As Date
                cmdText = " select MIN(EndDate) from BeeBookScheduleList "
                dStartDate = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdText)
                Dim dtMax As Date
                cmdText = " select MAX(EndDate) from BeeBookScheduleList"
                dEndDate = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdText)
            End If
            'xmlData = "<NewDataSet>"
            'xmlData += "    <group>"
            'xmlData += "      <name>Recruiting</name>"
            'xmlData += "      <blockcolor>#ff0000</blockcolor>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=7</href>"
            'xmlData += "        <StartDate>2014-11-01</StartDate>"
            'xmlData += "        <EndDate>2014-11-01</EndDate>"
            'xmlData += "        <name>Recruit Team Lead</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2014-11-01</StartDate>"
            'xmlData += "        <EndDate>2014-11-15</EndDate>"
            'xmlData += "        <name> Recruit Editors </name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2014-11-15</StartDate>"
            'xmlData += "        <EndDate>2014-11-29</EndDate>"
            'xmlData += "        <name>Secure Software</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2014-11-29</StartDate>"
            'xmlData += "        <EndDate>2015-12-27</EndDate>"
            'xmlData += "        <name>Learning Software</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-12-27</StartDate>"
            'xmlData += "        <EndDate>2015-01-10</EndDate>"
            'xmlData += "        <name>Load Previous Bee Book</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-01-10</StartDate>"
            'xmlData += "        <EndDate>2015-01-24</EndDate>"
            'xmlData += "        <name>Prepare Schedules</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-01-24</StartDate>"
            'xmlData += "        <EndDate>2015-02-21</EndDate>"
            'xmlData += "        <name>Front Cover</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=12</href>"
            'xmlData += "        <StartDate>2015-02-21</StartDate>"
            'xmlData += "        <EndDate>2014-03-21</EndDate>"
            'xmlData += "        <name>Back Cover</name>"
            'xmlData += "      </block>"
            'xmlData += "    </group>"
            'xmlData += "    <group>"
            'xmlData += "      <name>Article 1</name>"
            'xmlData += "      <blockcolor>#99ff66</blockcolor>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=14</href>"
            'xmlData += "        <StartDate>2015-06-07</StartDate>"
            'xmlData += "        <EndDate>2015-06-07</EndDate>"
            'xmlData += "        <name>Invite articles</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=15</href>"
            'xmlData += "        <StartDate>2015-06-07</StartDate>"
            'xmlData += "        <EndDate>2015-07-05</EndDate>"
            'xmlData += "        <name>Deadline For Articles</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=15</href>"
            'xmlData += "        <StartDate>2015-07-05</StartDate>"
            'xmlData += "        <EndDate>2015-07-19</EndDate>"
            'xmlData += "        <name>Review Articles</name>"
            'xmlData += "      </block>"
            'xmlData += "      <block>"
            'xmlData += "        <href>activity.aspx?ActID=15</href>"
            'xmlData += "        <StartDate>2015-07-19</StartDate>"
            'xmlData += "        <EndDate>2015-07-26</EndDate>"
            'xmlData += "        <name>Edit And placements</name>"
            'xmlData += "      </block>"
            'xmlData += "    </group>"



            EventCalendarControl1.XMLData = xmlData
            EventCalendarControl1.BlankGifPath = "trans.gif"
            EventCalendarControl1.Year = Now.Year
            'EventCalendarControl1.BeginDate = "01/11/2014"
            'EventCalendarControl1.EndDate = "01/12/2015"
            EventCalendarControl1.BeginDate = "11/01/2014"
            EventCalendarControl1.EndDate = "12/01/2015"
            EventCalendarControl1.Quarter = 4
            EventCalendarControl1.BlockColor = "blue"
            EventCalendarControl1.ToggleColor = "#dcdcdc"
            EventCalendarControl1.CellHeight = 60
            EventCalendarControl1.CellWidth = 20
            EventCalendarControl1.EnableViewState = True
        Catch ex As Exception
            Response.Write("Error :" & ex.ToString)
        End Try
    End Sub


    'Private Sub CreateRecruitingChart()

    '    Try

    '        Dim inputs() As String = {"#0000FF", "#FFA500", "#808000", "#FF00FF", "#800080", "#848482", "#348017", "#E2A76F", "#E56717", "#8E35EF", "#95F035", "#088FF0", "#FFA500", "#8CC403", "#FF3C08", "#24E4C2", "#F518CB"}
    '        Dim lstOfColors As New ArrayList
    '        lstOfColors.Add("#0000FF")
    '        lstOfColors.Add("#FFA500")
    '        lstOfColors.Add("#808000")
    '        lstOfColors.Add("#FF00FF")
    '        lstOfColors.Add("#800080")
    '        lstOfColors.Add("#848482")
    '        lstOfColors.Add("#348017")
    '        lstOfColors.Add("#E2A76F")
    '        lstOfColors.Add("#E56717")
    '        lstOfColors.Add("#8E35EF")
    '        lstOfColors.Add("#95F035")

    '        lstOfColors.Add("#ff0000")
    '        lstOfColors.Add("#99ff66")
    '        lstOfColors.Add("#cc66cc")
    '        lstOfColors.Add("#088FF0")
    '        lstOfColors.Add("#FFA500")
    '        lstOfColors.Add("#8CC403")
    '        lstOfColors.Add("#FF3C08")
    '        Dim lstOfProdGrp As New ArrayList
    '        Dim dStartDate As DateTime, dEndDate As DateTime
    '        dStartDate = Nothing
    '        dEndDate = Nothing


    '        Dim dsDetails As DataSet

    '        cmdText = "select SubComponents,Duration,EndDate,ComponentID from BeeBookScheduleList where ComponentID=1"



    '        dsDetails = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
    '        If ddYear.SelectedValue = -1 Then
    '            cmdText = "select distinct Components,ComponentID from BeeBookSchedule where ComponentID=1"
    '        Else
    '            cmdText = "select distinct Components,ComponentID from BeeBookSchedule where ComponentID=1"
    '        End If

    '        Dim dsPGroup As DataSet
    '        dsPGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
    '        'div1.Style.Add("visibihttp://localhost:49160/App8Full/bin/Interop.LPICOM_6_0Lib.dlllity", "hidden")
    '        Response.Write(dsDetails.Tables(0).Rows.Count)
    '        If dsDetails.Tables(0).Rows.Count = 0 Then
    '            lblMessage.ForeColor = Color.Red
    '            lblMessage.Text = "No Record Exists!"

    '        Else
    '            div1.Style.Add("visibility", "visible")
    '            lblMessage.Text = ""
    '            ' EventCalendarControl1.Style.Add("Display", "None")
    '        End If
    '        xmlData = Nothing
    '        Dim startDate As String
    '        startDate = Nothing
    '        'For Each drGrp As DataRow In dsPGroup.Tables(0).Rows
    '        '    If lstOfProdGrp.Contains(drGrp("ComponentID")) = False Then
    '        '        lstOfProdGrp.Add(drGrp("ComponentID"))
    '        '    End If
    '        '    xmlData += "    <group>"
    '        '    xmlData += "      <name>" & drGrp("Components") & "</name>"
    '        '    xmlData += "      <blockcolor>" & lstOfColors.Item(lstOfProdGrp.IndexOf(drGrp("ComponentID"))) & "</blockcolor>"
    '        '    For Each dr As DataRow In dsDetails.Tables(0).Select("ComponentID =" & drGrp("ComponentID"))
    '        '        If startDate = Nothing Then
    '        '            startDate = dr("EndDate")
    '        '        End If

    '        '        xmlData += "      <block>"
    '        '        xmlData += "        <StartDate>" & dr("EndDate") & "</StartDate>"
    '        '        xmlData += "        <EndDate>" & dr("EndDate") & "</EndDate>"
    '        '        xmlData += "        <name>" & dr("SubComponents") & "</name>"
    '        '        xmlData += "      </block>"

    '        '    Next
    '        '    xmlData += "    </group>"
    '        'Next
    '        Dim i As Integer
    '        i = 0
    '        For Each drGrp As DataRow In dsPGroup.Tables(0).Rows
    '            If lstOfProdGrp.Contains(drGrp("ComponentID")) = False Then
    '                lstOfProdGrp.Add(drGrp("ComponentID"))
    '            End If
    '            xmlData += "    <group>"
    '            xmlData += "      <name>" & drGrp("Components") & "</name>"
    '            xmlData += "      <blockcolor>" & lstOfColors.Item(lstOfProdGrp.IndexOf(drGrp("ComponentID"))) & "</blockcolor>"
    '            For Each dr As DataRow In dsDetails.Tables(0).Select("ComponentID =" & drGrp("ComponentID"))
    '                If startDate = Nothing Then
    '                    startDate = dr("EndDate")
    '                End If


    '                Dim answer = dr("EndDate").AddDays(-(dr("Duration")))


    '                xmlData += "      <block>"
    '                xmlData += "        <href>activity.aspx?ActID=7</href>"
    '                xmlData += "        <StartDate>" & answer & "</StartDate>"
    '                xmlData += "        <EndDate>" & dr("EndDate") & "</EndDate>"
    '                xmlData += "        <name>" & dr("SubComponents") & "</name>"
    '                xmlData += "      </block>"
    '                startDate = dr("EndDate")

    '            Next
    '            xmlData += "    </group>"
    '        Next

    '        xmlData = "<NewDataSet>" + xmlData + "</NewDataSet>"

    '        If dStartDate = Nothing And dEndDate = Nothing Then
    '            Dim dtMin As Date
    '            cmdText = " select MIN(EndDate) from BeeBookScheduleList "
    '            dStartDate = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdText)
    '            Dim dtMax As Date
    '            cmdText = " select MAX(EndDate) from BeeBookScheduleList"
    '            dEndDate = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdText)
    '        End If
    '        'xmlData = "<NewDataSet>"
    '        'xmlData += "    <group>"
    '        'xmlData += "      <name>Recruiting</name>"
    '        'xmlData += "      <blockcolor>#ff0000</blockcolor>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=7</href>"
    '        'xmlData += "        <StartDate>2014-11-01</StartDate>"
    '        'xmlData += "        <EndDate>2014-11-01</EndDate>"
    '        'xmlData += "        <name>Recruit Team Lead</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2014-11-01</StartDate>"
    '        'xmlData += "        <EndDate>2014-11-15</EndDate>"
    '        'xmlData += "        <name> Recruit Editors </name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2014-11-15</StartDate>"
    '        'xmlData += "        <EndDate>2014-11-29</EndDate>"
    '        'xmlData += "        <name>Secure Software</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2014-11-29</StartDate>"
    '        'xmlData += "        <EndDate>2015-12-27</EndDate>"
    '        'xmlData += "        <name>Learning Software</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2015-12-27</StartDate>"
    '        'xmlData += "        <EndDate>2015-01-10</EndDate>"
    '        'xmlData += "        <name>Load Previous Bee Book</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2015-01-10</StartDate>"
    '        'xmlData += "        <EndDate>2015-01-24</EndDate>"
    '        'xmlData += "        <name>Prepare Schedules</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2015-01-24</StartDate>"
    '        'xmlData += "        <EndDate>2015-02-21</EndDate>"
    '        'xmlData += "        <name>Front Cover</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=12</href>"
    '        'xmlData += "        <StartDate>2015-02-21</StartDate>"
    '        'xmlData += "        <EndDate>2014-03-21</EndDate>"
    '        'xmlData += "        <name>Back Cover</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "    </group>"
    '        'xmlData += "    <group>"
    '        'xmlData += "      <name>Article 1</name>"
    '        'xmlData += "      <blockcolor>#99ff66</blockcolor>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=14</href>"
    '        'xmlData += "        <StartDate>2015-06-07</StartDate>"
    '        'xmlData += "        <EndDate>2015-06-07</EndDate>"
    '        'xmlData += "        <name>Invite articles</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=15</href>"
    '        'xmlData += "        <StartDate>2015-06-07</StartDate>"
    '        'xmlData += "        <EndDate>2015-07-05</EndDate>"
    '        'xmlData += "        <name>Deadline For Articles</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=15</href>"
    '        'xmlData += "        <StartDate>2015-07-05</StartDate>"
    '        'xmlData += "        <EndDate>2015-07-19</EndDate>"
    '        'xmlData += "        <name>Review Articles</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "      <block>"
    '        'xmlData += "        <href>activity.aspx?ActID=15</href>"
    '        'xmlData += "        <StartDate>2015-07-19</StartDate>"
    '        'xmlData += "        <EndDate>2015-07-26</EndDate>"
    '        'xmlData += "        <name>Edit And placements</name>"
    '        'xmlData += "      </block>"
    '        'xmlData += "    </group>"



    '        EventCalendarControlRecruiting.XMLData = xmlData
    '        EventCalendarControlRecruiting.BlankGifPath = "trans.gif"
    '        EventCalendarControlRecruiting.Year = Now.Year
    '        EventCalendarControlRecruiting.Quarter = 4
    '        EventCalendarControlRecruiting.BlockColor = "blue"
    '        EventCalendarControlRecruiting.ToggleColor = "#dcdcdc"
    '        EventCalendarControlRecruiting.CellHeight = 15
    '        EventCalendarControlRecruiting.CellWidth = 20
    '        EventCalendarControlRecruiting.EnableViewState = True
    '    Catch ex As Exception
    '        Response.Write("Error :" & ex.ToString)
    '    End Try
    'End Sub
    Private Sub Team()
        Dim StrQryCheck As String
        If ddEvent.SelectedItem.Text = "All" Then
            StrQryCheck = "select TeamID,TeamName from  VolTeamMatrix "
        Else
            StrQryCheck = "select TeamID,TeamName from  VolTeamMatrix where " + ddEvent.SelectedItem.Text + "='Y'"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrQryCheck)
        ddTeams.DataSource = ds.Tables(0)
        ddTeams.DataBind()
        ddTeams.Items.Insert(0, "Select Team")
        ddTeams.Items(0).Selected = True


    End Sub
    Private Sub Chapter()
        div1.Visible = False
        Dim StrQryCheck As String
        If ddEvent.SelectedItem.Text = "All" Then
            StrQryCheck = "select ChapterID,ChapterCode from  Chapter "
        Else
            StrQryCheck = "select ChapterID,ChapterCode from  Chapter "
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrQryCheck)
        ddchapter.DataSource = ds.Tables(0)
        ddchapter.DataBind()
        ddchapter.Items.Insert(0, "Select Chapter")
        ddchapter.Items(0).Selected = True


    End Sub
    Protected Sub ddEvent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddEvent.SelectedIndexChanged
        div1.Visible = False
        Team()
        Chapter()
    End Sub
    Private Sub BindbeeBookSchedule()
        div1.Visible = False
        Dim strCond As String = ""
        If ddTeams.SelectedIndex = 5 Then
            If ddYear.SelectedValue = -1 Then
                'cmdText = "select Event,Chapter,Components,ComponentID from BeeBookSchedule where ChapterID= " & ddchapter.SelectedValue & " And EventID= " & ddEvent.SelectedValue & " "
                cmdText = "select Event,Chapter,Components,ComponentID from BeeBookSchedule"
            Else
                'cmdText = "select Event,Chapter,Components,ComponentID from BeeBookSchedule where ChapterID= " & ddchapter.SelectedValue & " And EventID= " & ddEvent.SelectedValue & " "
                cmdText = "select Event,Chapter,Components,ComponentID from BeeBookSchedule"
            End If

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            cmdText = "select BSL.Year,BS.Event,BS.Chapter,BS.Components,BSL.SubComponents,BSL.Duration,BSL.EndDate from BeeBookSchedule BS join BeeBookScheduleList BSL on BS.ComponentID=BSL.ComponentID"
            Dim dsSub As DataSet
            dsSub = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            grdBeeBookSchedule.DataSource = ds.Tables(0)
            grdBeeBookSchedule.DataBind()

        Else
            cmdText = "select Event,Chapter,Components,ComponentID from BeeBookSchedule"
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            cmdText = "select BSL.Year,BS.Event,BS.Chapter,BS.Components,BSL.SubComponents,BSL.Duration,BSL.EndDate from BeeBookSchedule BS join BeeBookScheduleList BSL on BS.ComponentID=BSL.ComponentID"
            Dim dsSub As DataSet
            dsSub = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            grdBeeBookSchedule.DataSource = ds.Tables(0)
            grdBeeBookSchedule.DataBind()
        End If
    End Sub
    Protected Sub grdBeeBookSchedule_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdBeeBookSchedule.RowCommand
        If e.CommandName = "Modify" Then
            Dim r As Integer
            For r = 0 To grdBeeBookSchedule.Rows.Count - 1
                grdBeeBookSchedule.Rows(r).BackColor = Nothing
            Next
            lblMsg.Text = ""

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
            Dim RowIndex As Integer = gvRow.RowIndex
            Dim componentID As Integer = DirectCast(gvRow.FindControl("lblComponentID"), Label).Text
        End If
        If e.CommandName = "Select" Then
            Dim r As Integer
            For r = 0 To grdBeeBookSchedule.Rows.Count - 1
                grdBeeBookSchedule.Rows(r).BackColor = Nothing
            Next
            lblMsg.Text = ""

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
            Dim RowIndex As Integer = gvRow.RowIndex
            Dim componentID As Integer = DirectCast(gvRow.FindControl("lblComponentID"), Label).Text

        End If
    End Sub
    Private Sub BindbeeBookScheduleList()
        div1.Visible = False
        Dim strCond As String = ""

        Dim ds As DataSet
        'ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        cmdText = "select BSL.Year,BS.Event,BS.Chapter,BS.Components,BSL.SubComponents,BSL.Duration,BSL.EndDate from BeeBookSchedule BS join BeeBookScheduleList BSL on BS.ComponentID=BSL.ComponentID"
        Dim dsSub As DataSet
        dsSub = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        grdBeeBookSchedule.DataSource = dsSub.Tables(0)
        grdBeeBookSchedule.DataBind()

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        div1.Visible = False
        Div5.Visible = True
        dvAddComponents.Visible = True
        BindbeeBookSchedule()
    End Sub
    Protected Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        div1.Visible = True
        If ddTeams.SelectedIndex = 5 Then
            CreateChart()
        End If
    End Sub
    Protected Sub btnAddSubComponents_Click(sender As Object, e As EventArgs) Handles btnAddSubComponents.Click
        tblAddUpdateSubComponents.Visible = True
    End Sub
    Protected Sub btnAddComponents_Click(sender As Object, e As EventArgs) Handles btnAddComponents.Click
        tblAddUpdateComponents.Visible = True
    End Sub
End Class
