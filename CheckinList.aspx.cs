﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.IO;

public partial class CheckinList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LblErrMsg.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {

            fillCheckinList();
        }
    }
    public void fillCheckinList()
    {
        string ChapterID=string.Empty;
        if (Request.QueryString["Chap"] != null)
        {
            ChapterID = Request.QueryString["Chap"].ToString();
        }
        string CmdText = string.Empty;
        CmdText = "select EventDescription,FundRCalID from FundRaisingCal where EventID=" + DDLEvent.SelectedValue + " and EventYear=" + DDlYear.SelectedValue + " and ChapterID=" + ChapterID + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            DDlCheckInList.DataValueField = "FundRCalID";
            DDlCheckInList.DataTextField = "EventDescription";
            DDlCheckInList.DataSource = ds;

            DDlCheckInList.DataBind();
            DDlCheckInList.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count > 1)
            {
                DDlCheckInList.Enabled = true;
            }
            else
            {
                DDlCheckInList.Enabled = false;
                DDlCheckInList.SelectedIndex = 1;
            }
        }
    }

    public void LoadCheckInList()
    {
        try
        {
            string tblHtml = string.Empty;
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            string EventYear = DDlYear.SelectedValue;
            string Event = DDLEvent.SelectedValue;
            string FundRCallID = DDlCheckInList.SelectedValue;
            string ListType = DDLListType.SelectedValue;
            string PaymentRef = string.Empty;
            string PaymentCRef = string.Empty;
            if (ListType == "Pending")
            {
                PaymentRef = " FR.PaymentReference is null and FR.MemberId not in (select MemberID from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and PaymentReference is not null)";
                PaymentCRef = " FCR.PaymentReference is null and FCR.MemberId not in (select MemberID from FundRReg where EventYear=FCR.EventYear and MemberID= FCR.MemberId and PaymentReference is not null)";
            }
            else if (ListType == "Paid")
            {
                PaymentRef = " FR.PaymentReference is not null ";
                PaymentCRef = " FCR.PaymentReference is not null ";
            }

            CmdText = "select (select Quantity from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and ProductCode='Adult' and FundRCalID=FR.FundRCalID) as CountAdult, (select Quantity from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and ProductCode='Child' and FundRCalID=FR.FundRCalID) as CountChild, (select Quantity from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and ProductCode='Dinner2' and FundRCalID=FR.FundRCalID) as CountExtra, FR.MemberID, IP.FirstName, IP.LastName,FR.ProductID, FR.ProductCode, FR.ProductGroupID,FR.Quantity,IP.Hphone,IP.CPhone,IP.Email,IP.City,IP.State,IP.Address1,FR.PaymentReference,FR.PaymentDate,IP.Zip from FundRReg FR inner join IndSpouse IP on (FR.MemberID=IP.AutoMemberID) where FR.EventYear=" + EventYear + " and FR.EventID=" + Event + " and FR.FundRCalID=" + FundRCallID + " and " + PaymentRef + " and FR.ChapterID=" + Request.QueryString["Chap"].ToString() + " order by IP.LastName, IP.FirstName ASC; ";

            CmdText += "select FCR.MemberID, FCR.ProductID, FCR.ProductCode,IP.Hphone,IP.CPhone,FCR.Approved,FCR.PaymentReference,C.First_Name,C.Last_Name,C.ChildNumber from FundRContestReg FCR inner join IndSpouse IP on (FCR.MemberID=IP.AutoMemberID) inner join Child C on (FCR.ChildNumber=C.ChildNumber) where FCR.EventYear=" + EventYear + " and FCR.EventID=" + Event + " and FCR.FundRCalID=" + FundRCallID + " and " + PaymentCRef + " and FCR.ChapterID=" + Request.QueryString["Chap"].ToString() + "  order by IP.LastName, IP.FirstName ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dvExcel.Visible = true;

                    string MemberID = string.Empty;
                    int count = 0;
                    tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse; width:1300px; border:1px solid black; border-collapse:collapse; font-family:verdana,arial;'>";

                    tblHtml += "<tr style='color:Green;background-color:#FFFFCC;font-weight:bold; font-family:verdana,arial;'>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ser#</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>MemberID</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>FirstName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>LastName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Status</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Amount</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Adults</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Children</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Extra</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Total</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Donation</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Fees</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Child1</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Contests</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Child2</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Contests</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Email</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>HPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>CPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Address</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>City</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>ST</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Zip</td>";

                    tblHtml += "</tr>";

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string PMemberID = string.Empty;
                        string Amount = string.Empty;
                        int AdultsCount = 0;
                        int ChildCount = 0;
                        int ExtraCount = 0;
                        int TotalCount = 0;
                        int ProductCount = 0;
                        string Status = string.Empty;
                        string Fees = string.Empty;
                        string Donatation = string.Empty;
                        if (MemberID != dr["MemberID"].ToString())
                        {
                            count++;
                            tblHtml += "<tr>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + count + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["MemberID"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["FirstName"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["LastName"].ToString() + "</td>";
                            if (dr["PaymentDate"].ToString() != "" && dr["PaymentReference"].ToString() != "")
                            {
                                Status = "Paid";
                            }
                            else
                            {
                                Status = "Pending";
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Status + "</td>";
                            PMemberID = dr["MemberID"].ToString();

                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                if (PMemberID == dr1["MemberID"].ToString())
                                {
                                    if (dr1["ProductID"].ToString() == "73")
                                    {
                                        Amount = dr1["ProductCode"].ToString();
                                        ProductCount = Convert.ToInt32(dr1["Quantity"].ToString());
                                    }
                                    if (dr1["CountAdult"].ToString() != "")
                                    {
                                        AdultsCount = Convert.ToInt32(dr1["CountAdult"].ToString());
                                    }
                                    if (dr1["CountChild"].ToString() != "")
                                    {
                                        ChildCount = Convert.ToInt32(dr1["CountChild"].ToString());
                                    }
                                    if (dr1["CountExtra"].ToString() != "")
                                    {
                                        ExtraCount = Convert.ToInt32(dr1["CountExtra"].ToString());
                                    }
                                    if (dr1["ProductID"].ToString() == "105")
                                    {
                                        Fees = dr1["ProductCode"].ToString();
                                    }

                                    if (dr1["ProductGroupID"].ToString() == "35")
                                    {
                                        Donatation += dr1["ProductCode"].ToString() + ", ";
                                    }
                                }
                            }
                            TotalCount = AdultsCount + ChildCount;
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Amount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + AdultsCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ChildCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ExtraCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + TotalCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Donatation + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Fees + "</td>";

                            // Child Section

                            string Child1Number = string.Empty;
                            string PChildNumebr = string.Empty;
                            string Child1Name = string.Empty;
                            string Child2Name = string.Empty;
                            string Contests1 = string.Empty;
                            string Contests2 = string.Empty;
                            int Childrencount = 0;
                            foreach (DataRow dr2 in ds.Tables[1].Rows)
                            {

                                if (PMemberID == dr2["MemberID"].ToString())
                                {

                                    if (Childrencount == 0)
                                    {
                                        Child1Number = dr2["ChildNumber"].ToString();
                                        Child1Name = dr2["First_Name"].ToString();
                                        Contests1 += dr2["ProductCode"].ToString() + ", ";
                                    }
                                    else
                                    {
                                        if (Child1Number == dr2["ChildNumber"].ToString())
                                        {
                                            Contests1 += dr2["ProductCode"].ToString() + ", ";
                                        }
                                        else
                                        {
                                            Child2Name = dr2["First_Name"].ToString();
                                            Contests2 += dr2["ProductCode"].ToString() + ", ";
                                        }
                                    }
                                    Childrencount++;

                                }
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Child1Name + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Contests1 + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Child2Name + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Contests2 + "</td>";

                            // Member Section
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Email"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["HPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["CPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Address1"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["City"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["State"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Zip"].ToString() + "</td>";


                            tblHtml += "</tr>";
                        }
                        MemberID = dr["MemberID"].ToString();
                    }


                    LtrCheckinList.Text = tblHtml;
                }
                else
                {
                    LblErrMsg.Text = "No recolrd exists";
                }
            }


        }
        catch (Exception ex)
        {

        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (validateCheckinList() == "1")
        {
            LoadCheckInList();
        }
    }


    public void ExportToExcel()
    {
        try
        {
            string tblHtml = string.Empty;
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            string EventYear = DDlYear.SelectedValue;
            string Event = DDLEvent.SelectedValue;
            string FundRCallID = DDlCheckInList.SelectedValue;
            string ListType = DDLListType.SelectedValue;
            string PaymentRef = string.Empty;
            string PaymentCRef = string.Empty;
            if (ListType == "Pending")
            {
                PaymentRef = " FR.PaymentReference is null and FR.MemberId not in (select MemberID from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and PaymentReference is not null)";
                PaymentCRef = " FCR.PaymentReference is null and FCR.MemberId not in (select MemberID from FundRReg where EventYear=FCR.EventYear and MemberID= FCR.MemberId and PaymentReference is not null)";
            }
            else if (ListType == "Paid")
            {
                PaymentRef = " FR.PaymentReference is not null ";
                PaymentCRef = " FCR.PaymentReference is not null ";
            }

            CmdText = "select (select Quantity from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and ProductCode='Adult' and FundRCalID=FR.FundRCalID) as CountAdult, (select Quantity from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and ProductCode='Child' and FundRCalID=FR.FundRCalID) as CountChild, (select Quantity from FundRReg where EventYear=FR.EventYear and MemberID= FR.MemberId and ProductCode='Dinner2' and FundRCalID=FR.FundRCalID) as CountExtra, FR.MemberID, IP.FirstName, IP.LastName,FR.ProductID, FR.ProductCode, FR.ProductGroupID,FR.Quantity,IP.Hphone,IP.CPhone,IP.Email,IP.City,IP.State,IP.Address1,FR.PaymentReference,FR.PaymentDate,IP.Zip from FundRReg FR inner join IndSpouse IP on (FR.MemberID=IP.AutoMemberID) where FR.EventYear=" + EventYear + " and FR.EventID=" + Event + " and FR.FundRCalID=" + FundRCallID + " and " + PaymentRef + " and FR.ChapterID=" + Request.QueryString["Chap"].ToString() + " order by IP.LastName, IP.FirstName ASC; ";

            CmdText += "select FCR.MemberID, FCR.ProductID, FCR.ProductCode,IP.Hphone,IP.CPhone,FCR.Approved,FCR.PaymentReference,C.First_Name,C.Last_Name,C.ChildNumber from FundRContestReg FCR inner join IndSpouse IP on (FCR.MemberID=IP.AutoMemberID) inner join Child C on (FCR.ChildNumber=C.ChildNumber) where FCR.EventYear=" + EventYear + " and FCR.EventID=" + Event + " and FCR.FundRCalID=" + FundRCallID + " and " + PaymentCRef + " and FCR.ChapterID=" + Request.QueryString["Chap"].ToString() + "  order by IP.LastName, IP.FirstName";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {


                    string MemberID = string.Empty;
                    int count = 0;
                    tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse; width:1300px; border:1px solid black; border-collapse:collapse; font-family:verdana,arial;'>";

                    tblHtml += "<tr style='color:Green;background-color:#FFFFCC;font-weight:bold; font-family:verdana,arial;'>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ser#</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>MemberID</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>FirstName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>LastName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Status</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Amount</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Adults</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Children</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Extra</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Total</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Donation</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Fees</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Child1</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Contests</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Child2</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Contests</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Email</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>HPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>CPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Address</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>City</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>ST</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Zip</td>";

                    tblHtml += "</tr>";

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string PMemberID = string.Empty;
                        string Amount = string.Empty;
                        int AdultsCount = 0;
                        int ChildCount = 0;
                        int ExtraCount = 0;
                        int TotalCount = 0;
                        int ProductCount = 0;
                        string Status = string.Empty;
                        string Fees = string.Empty;
                        string Donatation = string.Empty;
                        if (MemberID != dr["MemberID"].ToString())
                        {
                            count++;
                            tblHtml += "<tr>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + count + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["MemberID"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["FirstName"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["LastName"].ToString() + "</td>";
                            if (dr["PaymentDate"].ToString() != "" && dr["PaymentReference"].ToString() != "")
                            {
                                Status = "Paid";
                            }
                            else
                            {
                                Status = "Pending";
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Status + "</td>";
                            PMemberID = dr["MemberID"].ToString();

                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                if (PMemberID == dr1["MemberID"].ToString())
                                {
                                    if (dr1["ProductID"].ToString() == "73")
                                    {
                                        Amount = dr1["ProductCode"].ToString();
                                        ProductCount = Convert.ToInt32(dr1["Quantity"].ToString());
                                    }
                                    if (dr1["CountAdult"].ToString() != "")
                                    {
                                        AdultsCount = Convert.ToInt32(dr1["CountAdult"].ToString());
                                    }
                                    if (dr1["CountChild"].ToString() != "")
                                    {
                                        ChildCount = Convert.ToInt32(dr1["CountChild"].ToString());
                                    }
                                    if (dr1["CountExtra"].ToString() != "")
                                    {
                                        ExtraCount = Convert.ToInt32(dr1["CountExtra"].ToString());
                                    }
                                    if (dr1["ProductID"].ToString() == "105")
                                    {
                                        Fees = dr1["ProductCode"].ToString();
                                    }

                                    if (dr1["ProductGroupID"].ToString() == "35")
                                    {
                                        Donatation += dr1["ProductCode"].ToString() + ", ";
                                    }
                                }
                            }
                            TotalCount = AdultsCount + ChildCount;
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Amount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + AdultsCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ChildCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ExtraCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + TotalCount + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Donatation + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Fees + "</td>";

                            // Child Section

                            string Child1Number = string.Empty;
                            string PChildNumebr = string.Empty;
                            string Child1Name = string.Empty;
                            string Child2Name = string.Empty;
                            string Contests1 = string.Empty;
                            string Contests2 = string.Empty;
                            int Childrencount = 0;
                            foreach (DataRow dr2 in ds.Tables[1].Rows)
                            {

                                if (PMemberID == dr2["MemberID"].ToString())
                                {

                                    if (Childrencount == 0)
                                    {
                                        Child1Number = dr2["ChildNumber"].ToString();
                                        Child1Name = dr2["First_Name"].ToString();
                                        Contests1 += dr2["ProductCode"].ToString() + ", ";
                                    }
                                    else
                                    {
                                        if (Child1Number == dr2["ChildNumber"].ToString())
                                        {
                                            Contests1 += dr2["ProductCode"].ToString() + ", ";
                                        }
                                        else
                                        {
                                            Child2Name = dr2["First_Name"].ToString();
                                            Contests2 += dr2["ProductCode"].ToString() + ", ";
                                        }
                                    }
                                    Childrencount++;

                                }
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Child1Name + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Contests1 + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Child2Name + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Contests2 + "</td>";

                            // Member Section
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Email"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["HPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["CPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Address1"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["City"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["State"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Zip"].ToString() + "</td>";


                            tblHtml += "</tr>";
                        }
                        MemberID = dr["MemberID"].ToString();
                    }



                }
            }
            DateTime dt = DateTime.Now;
            string month = dt.ToString("MMM");
            string day = dt.ToString("dd");
            string year = dt.ToString("yyyy");
            string monthDay = month + "" + day;
            string filename = "Check-inList" + monthDay + "_" + year + ".xls";
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
            Response.Charset = "";

            Response.ContentType = "application/vnd.xls";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            Response.Write("<br>Check-in List<br>");
            Response.Write(tblHtml.ToString());
            Response.End();

        }
        catch (Exception ex)
        {

        }
    }
    protected void BtnExcel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }

    public string validateCheckinList()
    {
        string RetVal = "1";
        if (DDlYear.SelectedValue == "0")
        {
            RetVal = "-1";
            LblErrMsg.Text = "Please select Year";
        }
        else if (DDLListType.SelectedValue == "0")
        {
            RetVal = "-1";
            LblErrMsg.Text = "Please select List Type";
        }
        else if (DDlCheckInList.SelectedValue == "0")
        {
            RetVal = "-1";
            LblErrMsg.Text = "Please select Check-in List";
        }
        return RetVal;
    }
}