<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="SuppressionList.aspx.cs" Inherits="SuppressionList" Title="SuppressionList" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="Content_Head" Runat="Server">

</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
			<script>

			    function ConfirmToUpdate(sender) {

			        var hf =document.getElementById('<%=hfSelection.ClientID%>').value;
			        if (hf.localeCompare("Show") == 0) {
			            if (confirm("Did Socket Labs remove the Abuse flag?")) {

			                return true;
			            }
			            else {

			                return false;

			            }
			        }
	      
			    }
    </script>
					    <asp:HyperLink runat="server" ID="HyperLink1" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>&nbsp;&nbsp;&nbsp;					    
					
<center>
<table>
    
    <tr>
        <td style="width: 208px">
            <asp:DropDownList ID="ddlSuppressionOptions" runat="server">
                <asp:ListItem Value="0">Select one</asp:ListItem>
                          <asp:ListItem Value="1">Suppression Removal List</asp:ListItem>
                          <asp:ListItem Value="2">Removal Request List</asp:ListItem>
                          <asp:ListItem Value="3">Update Removal Flag</asp:ListItem>
                          <asp:ListItem Value="4">Update Suppression Flag</asp:ListItem>
                     
            </asp:DropDownList></td>
       <%-- <td style="width: 207px"><asp:RadioButton ID="rbSupRemovalList" Text="Suppression Removal List" GroupName="suppression" runat="server" /></td>
        <td style="width: 187px"><asp:RadioButton ID="rbUpdateSupFlag" Text="Update Suppression Flag" GroupName="suppression" runat="server" /></td>--%>
        <td>
            <asp:Button ID="bSubmit" runat="server" Text="Submit" OnClick="bSubmit_Click" /></td>
    </tr>
  </table>
  
<asp:Label ID="lblAlert" runat="server" Text="" Visible="false"></asp:Label>


            <div id="divGridView" align="left" runat="server" visible="false"><asp:Button ID="excel" runat="server"  Text="Export To Excel" 
                onclick="excel_Click" enabled="true" />

                    <asp:HiddenField  runat="server" ID="hfSelection"/>      
    <asp:GridView ID="gvSupRemovalList" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" OnRowUpdating="gvSupRemovalList_RowUpdating" AllowPaging="True" OnPageIndexChanging="gvSupRemovalList_PageIndexChanging" OnRowDataBound="gvSupRemovalList_RowDataBound" PageSize="50">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                       <%--<asp:HiddenField ID="SignupID" Value='<%# Bind("SignupID")%>' runat="server" />--%>
                    <asp:Button ID="upButton" runat="server" Enabled="False" Text="Update" OnClientClick="return ConfirmToUpdate(this)" CommandName="Update" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FailCompID">
                <ItemTemplate>
                    <asp:Label ID="FailCompID" runat="server" Text='<%#Eval("FailCompID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email">
                <ItemTemplate>
                    <asp:Label ID="Email" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FirstName">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="LastName">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("LastName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="HPhone">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("HPhone") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CPhone">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("CPhone") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="State">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="City">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Chapter">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%#Eval("Chapter") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date">
                <ItemTemplate>
                    <asp:Label ID="Label23" runat="server" Text='<%#Eval("Date") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ComplaintType">
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%#Eval("ComplaintType") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="NewsLetter">
                <ItemTemplate>
                    <asp:Label ID="NewsLetter" runat="server" Text='<%#Eval("NewsLetter") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AutoMemberID">
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%#Eval("AutoMemberID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="ReqRem">
                <ItemTemplate>
                    <asp:Label ID="lblReqRem" runat="server" Text='<%#Eval("ReqRem") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField HeaderText="ReqRemDate">
                <ItemTemplate>
                    <asp:Label ID="lblReqRemDate" runat="server" Text='<%#Eval("ReqRemDate") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>

                 </div>     
    <br />


                        </center>
</asp:Content>

