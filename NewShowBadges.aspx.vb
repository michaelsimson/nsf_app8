Imports NorthSouth.BAL
Imports System.Data.SqlClient
Imports System
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections
Imports System.Web.UI

Partial Class NewShowBadges
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateBadges
    Protected lblHeader1 As Label
    Protected lblHeader2 As Label
    Protected lblName As Label
    Protected lblBadgeNumber As Label
    Protected lblTshirt_Food As Label
    Protected lblWeekDay As Label
    Protected lblChildLocation As Label
    Protected lblGrade As Label
    Protected lblParentChildID As Label
    Protected lblChapterCode As Label
    Protected lbltest As Label
    ' Protected imgPhotoBadtes As System.Web.UI.WebControls.Image
    Protected imgPhotoBadtes As System.Web.UI.HtmlControls.HtmlImage
    Protected imageBarCode As System.Web.UI.HtmlControls.HtmlImage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Session("SelChapterID").ToString() = "1" Then
                LoadNationalBadges()
            Else
                LoadBadges()
            End If
        End If
    End Sub
    Private Sub LoadBadges()
        Dim dsBadge As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        Dim j As Integer
        Dim iBadgeCount As Integer
        Dim iPerPageCount As Integer
        Dim strBandgeNumber As String
        generateBadge = CType(Context.Handler, GenerateBadges)
        dsBadge = badge.GetChildsHavingBadges(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates)
        If dsBadge.Tables(0).Rows.Count > 0 Then
            iPerPageCount = 10
            j = 1
            For i = 1 To dsBadge.Tables(0).Rows.Count
                iBadgeCount = 0
                strBandgeNumber = ""
                lblHeader1 = New Label
                lblHeader2 = New Label
                lblName = New Label
                lblChapterCode = New Label
                lbltest = New Label

                Dim dsBadgeNumber As New DataSet
                If i Mod 2 = 1 Then
                    BadgeHolder.Controls.Add(New LiteralControl("<tr style='height:3.0in;' >"))
                    BadgeHolder.Controls.Add(New LiteralControl("<td align='center' style='width:4.0in;padding:0in .75pt 0in .75pt;height:3.0in'>"))
                    lblHeader1.Text = "North South Foundation"
                    lblHeader1.Font.Bold = True
                    lblHeader1.ForeColor = Color.Navy
                    lblHeader1.Font.Name = "Verdana"
                    lblHeader1.Font.Size = 14
                    BadgeHolder.Controls.Add(lblHeader1)
                    lblHeader2.Text = "Regional Educational Contests"
                    lblHeader2.Font.Name = "Times New Roman"
                    lblHeader2.Font.Size = 12
                    lblHeader2.ForeColor = Color.Navy
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblHeader2)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblName.Text = dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & " " & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()
                    lblName.Font.Name = "Arial"
                    lblName.Font.Bold = True
                    If Len(lblName.Text) > 22 Then
                        lblName.Font.Size = 12
                    Else
                        lblName.Font.Size = 15
                    End If
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblName)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    dsBadgeNumber = badge.ShowBadge(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, Convert.ToDouble(dsBadge.Tables(0).Rows(i - 1)("ChildNumber")))
                    For iBadgeCount = 0 To dsBadgeNumber.Tables(0).Rows.Count - 1
                        lblBadgeNumber = New Label
                        If dsBadgeNumber.Tables(0).Rows.Count > 3 And dsBadgeNumber.Tables(0).Rows.Count <= 5 Then
                            lblBadgeNumber.Font.Size = 12
                            lblBadgeNumber.Font.Bold = True
                        ElseIf dsBadgeNumber.Tables(0).Rows.Count > 5 Then
                            lblBadgeNumber.Font.Size = 10
                            lblBadgeNumber.Font.Bold = True
                        Else
                            lblBadgeNumber.Font.Size = 15
                            lblBadgeNumber.Font.Bold = True
                        End If
                        lblBadgeNumber.Text = dsBadgeNumber.Tables(0).Rows(iBadgeCount)("BadgeNumber").ToString()
                        If dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "SB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "VB" Then
                            lblBadgeNumber.ForeColor = Color.Blue
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "MB" Then
                            lblBadgeNumber.ForeColor = Color.Red
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "EW" Then
                            lblBadgeNumber.ForeColor = Color.DeepPink
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "PS" Then
                            lblBadgeNumber.ForeColor = Color.Orange
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "BB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "GB" Then
                            lblBadgeNumber.ForeColor = Color.Black
                        End If
                        BadgeHolder.Controls.Add(lblBadgeNumber)
                        BadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))
                    Next
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblChapterCode.Text = dsBadge.Tables(0).Rows(i - 1)("ChapterCode").ToString()
                    lblChapterCode.ForeColor = Color.Black
                    lblChapterCode.Font.Size = 13
                    BadgeHolder.Controls.Add(lblChapterCode)
                    BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                Else
                    BadgeHolder.Controls.Add(New LiteralControl("<td align='center'  style='width:4.0in;padding:0in .75pt 0in .75pt;height:3.0in'> "))
                    lblHeader1.Text = "North South Foundation"
                    lblHeader1.Font.Bold = True
                    lblHeader1.Font.Name = "Verdana"
                    lblHeader1.ForeColor = Color.Navy
                    lblHeader1.Font.Size = 14
                    BadgeHolder.Controls.Add(lblHeader1)
                    lblHeader2.Text = "Regional Educational Contests"
                    lblHeader2.Font.Name = "Times New Roman"
                    lblHeader2.ForeColor = Color.Navy
                    lblHeader2.Font.Size = 12
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblHeader2)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblName.Text = dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & " " & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()
                    lblName.Font.Name = "Arial"
                    lblName.Font.Bold = True
                    If Len(lblName.Text) > 22 Then
                        lblName.Font.Size = 12
                    Else
                        lblName.Font.Size = 15
                    End If
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblName)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    dsBadgeNumber = badge.ShowBadge(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, Convert.ToDouble(dsBadge.Tables(0).Rows(i - 1)("ChildNumber")))

                    For iBadgeCount = 0 To dsBadgeNumber.Tables(0).Rows.Count - 1
                        lblBadgeNumber = New Label
                        If dsBadgeNumber.Tables(0).Rows.Count > 3 And dsBadgeNumber.Tables(0).Rows.Count <= 5 Then
                            lblBadgeNumber.Font.Size = 12
                            lblBadgeNumber.Font.Bold = True
                        ElseIf dsBadgeNumber.Tables(0).Rows.Count > 5 Then
                            lblBadgeNumber.Font.Size = 10
                            lblBadgeNumber.Font.Bold = True
                        Else
                            lblBadgeNumber.Font.Size = 15
                            lblBadgeNumber.Font.Bold = True
                        End If

                        lblBadgeNumber.Text = dsBadgeNumber.Tables(0).Rows(iBadgeCount)("BadgeNumber").ToString()
                        If dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "SB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "VB" Then
                            lblBadgeNumber.ForeColor = Color.Blue
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "MB" Then
                            lblBadgeNumber.ForeColor = Color.Red
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "EW" Then
                            lblBadgeNumber.ForeColor = Color.DeepPink
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "PS" Then
                            lblBadgeNumber.ForeColor = Color.Orange
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "BB" Then
                            lblBadgeNumber.ForeColor = Color.Green
                        ElseIf dsBadgeNumber.Tables(0).Rows(iBadgeCount)("ProductGroupCode").ToString() = "GB" Then
                            lblBadgeNumber.ForeColor = Color.Black
                        End If
                        BadgeHolder.Controls.Add(lblBadgeNumber)
                        BadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))
                    Next
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblChapterCode.ForeColor = Color.Black
                    lblChapterCode.Font.Size = 13
                    lblChapterCode.Text = dsBadge.Tables(0).Rows(i - 1)("ChapterCode").ToString()
                    BadgeHolder.Controls.Add(lblChapterCode)
                    BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                    BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                End If
                If iPerPageCount = j Then
                    j = 1
                    'BadgeHolder.Controls.Add(New LiteralControl("<tr><td colspan='2'>&nbsp;</td></tr>"))
                    'BadgeHolder.Controls.Add(New LiteralControl("<div style='page-break-after:always'>&nbsp;</div>"))
                    'BadgeHolder.Controls.Add(New LiteralControl("<P CLASS='breakhere'>"))
                End If
                j = j + 1
            Next
            pnlData.Visible = True
            pnlMessage.Visible = False
        Else
            pnlData.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Badge Details found."
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Badges_Participants_" & strChapterName & ".doc")
            Response.Charset = ""
            Response.ContentType = "application/vnd.word"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            pnlData.RenderControl(htmlWrite)
            Response.Write("<html>")
            Response.Write("<head>")
            Response.Write("<style>")
            Response.Write(" @page Section1 {size:8.5in 11.0in;margin:50.0pt .25in 0in .25in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:4;}")
            Response.Write("div.Section1 {page:Section1;}")
            Response.Write("</style>")
            Response.Write("</head>")
            Response.Write("<body>")
            Response.Write(stringWrite.ToString())
            Response.Write("</body>")
            Response.Write("</html>")
            Response.End()
        End If
    End Sub

    Private Sub LoadNationalBadges()
        Dim dsBadge As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        Dim j As Integer
        Dim iBadgeCount As Integer
        Dim iPerPageCount As Integer
        Dim strBandgeNumber As String
        Dim badge_number As Integer
        Dim week_day_number As Integer
        Dim TshirtName As String = ""
        generateBadge = CType(Context.Handler, GenerateBadges)

        Dim StrRange As String() = Session("PageRange").ToString.Split("_")
        Try
            dsBadge = badge.GetChildsHavingBadgesFinals(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, StrRange(0), StrRange(1))
            If dsBadge.Tables(0).Rows.Count > 0 Then
                iPerPageCount = 10
                j = 1
                For i = 1 To dsBadge.Tables(0).Rows.Count
                    iBadgeCount = 0
                    strBandgeNumber = ""
                    lblHeader1 = New Label
                    lblHeader2 = New Label
                    lblName = New Label
                    lblChapterCode = New Label
                    lbltest = New Label
                    lblTshirt_Food = New Label
                    Dim dsBadgeNumber As New DataSet
                    If i Mod 2 = 1 Then
                        BadgeHolder.Controls.Add(New LiteralControl("<tr>"))
                        'BadgeHolder.Controls.Add(New LiteralControl("<td align='center' style='width:4.0in;padding:0in .25pt 0in .0625pt;height:4.0in'>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td align='center' width='350px'>"))
                        'start of table 2
                        BadgeHolder.Controls.Add(New LiteralControl("<table border=5 height='250px' width='350px'  BORDERCOLOR='#0000FF'  BORDERCOLORLIGHT='#33CCFF' BORDERCOLORDARK='#0000CC'><tr><td bgcolor='lightyellow' align='center'>"))

                        ' lblHeader1.Text = "NSF National Championship Finals - " & dsBadge.Tables(0).Rows(i - 1)("Contest_Dates").ToString()
                        lblHeader1.Text = Now.Year.ToString() & " NSF National Championship Finals"
                        lblHeader1.Font.Bold = True
                        lblHeader1.ForeColor = Color.Navy
                        lblHeader1.Font.Name = "Times New Roman"
                        lblHeader1.Font.Size = 12
                        BadgeHolder.Controls.Add(lblHeader1)
                        lblHeader2.Text = "<font name='times new roman' size='3'><b> " & dsBadge.Tables(0).Rows(i - 1)("Venue").ToString() & "</b></font> &nbsp; <font name='arial' size=3 color='red'><b>(" & contestdate(dsBadge.Tables(0).Rows(i - 1)("Contest_Dates").ToString()) & ", " & Now.Year.ToString() & ")</b></font>" ' .Replace("Sep", "")
                        ' lblHeader2.Font.Name = "Times New Roman"
                        ' lblHeader2.Font.Size = 12
                        ' lblHeader2.Font.Italic = True
                        lblHeader2.ForeColor = Color.DeepPink
                        BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                        BadgeHolder.Controls.Add(lblHeader2)
                        BadgeHolder.Controls.Add(New LiteralControl("</td></tr><tr><td bgcolor='#F3F5DC' valign='top'>"))
                        'strat of table 2-1
                        BadgeHolder.Controls.Add(New LiteralControl("<table border=1 style='width:100%;border-collapse:collapse;border-color=linen;' >"))
                        BadgeHolder.Controls.Add(New LiteralControl("<tr bgcolor='#F0FFF0'>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td height=128 align='center'>"))

                        imgPhotoBadtes = New System.Web.UI.HtmlControls.HtmlImage
                        If FileExists(Server.MapPath(dsBadge.Tables(0).Rows(i - 1)("Photo_Name").ToString())) Then
                            imgPhotoBadtes.Src = dsBadge.Tables(0).Rows(i - 1)("Photo_Image").ToString()
                        Else
                            imgPhotoBadtes.Src = "http://www.northsouth.org/app8/photos_badge/bee.jpg"
                        End If
                        'imgPhotoBadtes.ImageUrl = CreateThumbnail("/app8/photos_badge/" & dsBadge.Tables(0).Rows(i - 1)("Photo_Image").ToString()) '"ShowImage.aspx?img=" & dsBadge.Tables(0).Rows(i - 1)("Photo_Image").ToString()
                        imgPhotoBadtes.Height = 120
                        imgPhotoBadtes.Width = 120
                        BadgeHolder.Controls.Add(imgPhotoBadtes)
                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td align='center' colspan=2>"))
                        lblName.Text = dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & "<br> " & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()
                        lblName.Font.Name = "Georgia"
                        lblName.Font.Bold = True
                        If Len(dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()) < 15 Then
                            lblName.Font.Size = 18
                        Else
                            lblName.Font.Size = 15
                        End If

                        BadgeHolder.Controls.Add(lblName)

                        BadgeHolder.Controls.Add(New LiteralControl("<br>"))
                        imageBarCode = New System.Web.UI.HtmlControls.HtmlImage

                        imageBarCode.Src = "http://www.tec-it.com/aspx/service/tbarcode/barcode.ashx?accesskey=NORTHSOUTH120802&code=QRCode&modulewidth=2&unit=px&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3DUnknown&dpi=96&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=Low"

                        'imageBarCode.Src = "http://www.tec-it.com/aspx/service/tbarcode/barcode.ashx?accesskey=NORTHSOUTH120802&code=QRCode&modulewidth=20.417&unit=mils&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3D" & dsBadge.Tables(0).Rows(i - 1)("ChildNumber") & "&dpi=72&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=/""Barcode generated by TEC-IT/"""
                        'imageBarCode.Src = "http://barcode.tec-it.com/barcode.ashx?code=QRCode&modulewidth=20.417&unit=mils&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3D" & dsBadge.Tables(0).Rows(i - 1)("ChildNumber") & "&dpi=72&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=/""Barcode generated by TEC-IT/"""
                        BadgeHolder.Controls.Add(imageBarCode)

                        BadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))

                        Dim week_days() As String
                        week_days = dsBadge.Tables(0).Rows(i - 1)("Week_Days").ToString().Split(";")
                        For week_day_number = 0 To week_days.GetUpperBound(0)
                            BadgeHolder.Controls.Add(New LiteralControl("<tr bgcolor='#F0FFF0'>"))
                            BadgeHolder.Controls.Add(New LiteralControl("<td align='center'>"))

                            lblWeekDay = New Label
                            lblWeekDay.Font.Size = 12
                            lblWeekDay.Font.Bold = True
                            If week_day_number <> week_days.GetUpperBound(0) Then
                                lblWeekDay.Text = week_days(week_day_number).ToString() & ":"
                            End If
                            BadgeHolder.Controls.Add(lblWeekDay)
                            BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                            BadgeHolder.Controls.Add(New LiteralControl("<td colspan='2' align='center'>"))

                            'Display Badgenumbers for particular day
                            Dim a() As String

                            a = dsBadge.Tables(0).Rows(i - 1)("Badge_Numbers").ToString().Split(";")
                            For badge_number = 0 To a.GetUpperBound(0)

                                If UCase(a(badge_number).Substring(0, 3)) = UCase(week_days(week_day_number).ToString()) Then
                                    lblBadgeNumber = New Label

                                    If a.Length > 3 And a.Length < 5 Then
                                        lblBadgeNumber.Font.Size = 12
                                        lblBadgeNumber.Font.Bold = True
                                    ElseIf a.Length > 5 Then
                                        lblBadgeNumber.Font.Size = 10
                                        lblBadgeNumber.Font.Bold = True
                                    ElseIf a.Length = 3 Then
                                        lblBadgeNumber.Font.Size = 12
                                        lblBadgeNumber.Font.Bold = True
                                    Else
                                        lblBadgeNumber.Font.Size = 15
                                        lblBadgeNumber.Font.Bold = True
                                    End If

                                    lblBadgeNumber.Text = Mid(a(badge_number).ToString(), 4)
                                    BadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))

                                    'Response.Write(UCase(a(badge_number)))
                                    'Response.Write("<br>4,2" & UCase(a(badge_number).Substring(4, 2)))
                                    'Response.Write("<br>3,2" & UCase(a(badge_number).Substring(3, 2)))
                                    'Response.Write("<br>")

                                    If UCase(a(badge_number).Substring(4, 2)) = "SB" Then
                                        lblBadgeNumber.ForeColor = Color.Green
                                    ElseIf UCase(a(badge_number).Substring(4, 2)) = "VB" Then
                                        lblBadgeNumber.ForeColor = Color.Blue
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "MB" Then
                                        lblBadgeNumber.ForeColor = Color.Red
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "EW" Then
                                        lblBadgeNumber.ForeColor = Color.DeepPink
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "PS" Then
                                        lblBadgeNumber.ForeColor = Color.Red
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "BB" Then
                                        lblBadgeNumber.ForeColor = Color.Green
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "GB" Then
                                        lblBadgeNumber.ForeColor = Color.Black
                                    End If
                                    BadgeHolder.Controls.Add(lblBadgeNumber)
                                End If
                                ' BadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))

                            Next
                            'To display TshirtSize and LunchType on Badges


                            If dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString() = "YYL" Then
                                TshirtName = "L"
                            ElseIf dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString() = "YM" Then
                                TshirtName = "M"
                            ElseIf dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString() = "YL" Then
                                TshirtName = "YL"
                            Else
                                TshirtName = ""
                            End If

                            lblTshirt_Food.Font.Size = 12
                            lblTshirt_Food.Font.Bold = True
                            lblTshirt_Food.Text = "Food: " & IIf(dsBadge.Tables(0).Rows(i - 1)("LunchType").ToString() = "Pizza", "P", "I") & "&nbsp;&nbsp;&nbsp;  TSize: " & TshirtName ' dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString()
                            BadgeHolder.Controls.Add(lblTshirt_Food)

                            'end of display badge number
                            BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                            BadgeHolder.Controls.Add(New LiteralControl("</tr>"))

                        Next

                        BadgeHolder.Controls.Add(New LiteralControl("<tr>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td bgcolor='#F0E68C' align='center'>"))
                        lblParentChildID = New Label
                        lblParentChildID.ForeColor = Color.Red
                        lblParentChildID.Font.Name = "Verdana"
                        lblParentChildID.Font.Size = 12
                        lblParentChildID.Font.Bold = True
                        lblParentChildID.Text = dsBadge.Tables(0).Rows(i - 1)("parent_id").ToString() & "-" & dsBadge.Tables(0).Rows(i - 1)("child_number").ToString()
                        BadgeHolder.Controls.Add(lblParentChildID)
                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                        BadgeHolder.Controls.Add(New LiteralControl("<td bgcolor='#F0E68C' align='center'>"))

                        lblChildLocation = New Label
                        lblChildLocation.ForeColor = Color.Red
                        lblParentChildID.Font.Name = "Verdana"
                        lblChildLocation.Font.Size = 12
                        lblChildLocation.Font.Bold = True
                        lblChildLocation.Text = dsBadge.Tables(0).Rows(i - 1)("city").ToString() & ", " & dsBadge.Tables(0).Rows(i - 1)("state").ToString()
                        BadgeHolder.Controls.Add(lblChildLocation)


                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                        BadgeHolder.Controls.Add(New LiteralControl("<td bgcolor='#F0E68C' align='center'>"))
                        lblGrade = New Label
                        lblGrade.ForeColor = Color.Red
                        lblGrade.Font.Name = "Verdana"
                        lblGrade.Font.Size = 12
                        lblGrade.Font.Bold = True
                        lblGrade.Text = "Gr:&nbsp;<font color=blue>" & dsBadge.Tables(0).Rows(i - 1)("grade").ToString() & "</font>"

                        BadgeHolder.Controls.Add(lblGrade)

                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                        BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                        BadgeHolder.Controls.Add(New LiteralControl("</table>"))
                        'end of table 2-1
                        BadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))
                        'end of table 2
                        BadgeHolder.Controls.Add(New LiteralControl("</table>"))

                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                    Else
                        ' BadgeHolder.Controls.Add(New LiteralControl("<td align='center'  style='width:4.0in;padding:0in .25pt 0in .625pt;height:3.0in'> "))
                        ' BadgeHolder.Controls.Add(New LiteralControl("<td align='center' width='24px'> </td> "))
                        BadgeHolder.Controls.Add(New LiteralControl("<td align='center' width='350px'> "))
                        'start of table 2
                        BadgeHolder.Controls.Add(New LiteralControl("<table border=5 width='350px'  height='250px' BORDERCOLOR='#0000FF'  BORDERCOLORLIGHT='#33CCFF' BORDERCOLORDARK='#0000CC'><tr><td bgcolor='lightyellow' align='center'>"))

                        'lblHeader1.Text = "NSF National Championship Finals - " & dsBadge.Tables(0).Rows(i - 1)("Contest_Dates").ToString()
                        lblHeader1.Text = Now.Year.ToString() & " NSF National Championship Finals"
                        lblHeader1.Font.Bold = True
                        lblHeader1.ForeColor = Color.Navy
                        lblHeader1.Font.Name = "Times New Roman"
                        lblHeader1.Font.Size = 12
                        BadgeHolder.Controls.Add(lblHeader1)

                        lblHeader2.Text = "<font name='times new roman' size='3'><b> " & dsBadge.Tables(0).Rows(i - 1)("Venue").ToString() & "</b></font> &nbsp; <font name='arial' size=3 color='red'><b>(" & contestdate(dsBadge.Tables(0).Rows(i - 1)("Contest_Dates").ToString()) & ", " & Now.Year.ToString() & ")</b></font>" '.Replace("Sep", "")
                        ' lblHeader2.Font.Name = "Times New Roman"
                        ' lblHeader2.Font.Size = 12
                        ' lblHeader2.Font.Italic = True
                        lblHeader2.ForeColor = Color.DeepPink
                        BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                        BadgeHolder.Controls.Add(lblHeader2)
                        BadgeHolder.Controls.Add(New LiteralControl("</td></tr><tr><td bgcolor='#F3F5DC' valign='top'>"))
                        'strat of table 2-1
                        BadgeHolder.Controls.Add(New LiteralControl("<table  border=1 style='width:100%;border-collapse:collapse;border-color=linen;'>"))

                        BadgeHolder.Controls.Add(New LiteralControl("<tr bgcolor='#F0FFF0'>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td height=128 align='center'>"))

                        imgPhotoBadtes = New System.Web.UI.HtmlControls.HtmlImage
                        If FileExists(Server.MapPath(dsBadge.Tables(0).Rows(i - 1)("Photo_Name").ToString())) Then
                            imgPhotoBadtes.Src = dsBadge.Tables(0).Rows(i - 1)("Photo_Image").ToString()
                        Else
                            imgPhotoBadtes.Src = "http://www.northsouth.org/app8/photos_badge/bee.jpg"
                        End If
                        'imgPhotoBadtes.ImageUrl = CreateThumbnail("/app8/photos_badge/" & dsBadge.Tables(0).Rows(i - 1)("Photo_Image").ToString()) '"ShowImage.aspx?img=" & dsBadge.Tables(0).Rows(i - 1)("Photo_Image").ToString()
                        imgPhotoBadtes.Height = 120
                        imgPhotoBadtes.Width = 120
                        BadgeHolder.Controls.Add(imgPhotoBadtes)
                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td align='center' colspan=2 >"))
                        lblName.Text = dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & "<br> " & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()
                        lblName.Font.Name = "Georgia"
                        lblName.Font.Bold = True
                        If Len(dsBadge.Tables(0).Rows(i - 1)("First_Name").ToString() & dsBadge.Tables(0).Rows(i - 1)("Last_Name").ToString()) < 15 Then
                            lblName.Font.Size = 18
                        Else
                            lblName.Font.Size = 15
                        End If

                        BadgeHolder.Controls.Add(lblName)
                        BadgeHolder.Controls.Add(New LiteralControl("<br>"))
                        imageBarCode = New System.Web.UI.HtmlControls.HtmlImage
                        imageBarCode.Src = "http://www.tec-it.com/aspx/service/tbarcode/barcode.ashx?accesskey=NORTHSOUTH120802&code=QRCode&modulewidth=2&unit=px&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3DUnknown&dpi=96&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=Low"

                        'imageBarCode.Src = "http://www.tec-it.com/aspx/service/tbarcode/barcode.ashx?accesskey=NORTHSOUTH120802&code=QRCode&modulewidth=20.417&unit=mils&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3D" & dsBadge.Tables(0).Rows(i - 1)("ChildNumber") & "&dpi=72&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=/""Barcode generated by TEC-IT/"""
                        'imageBarCode.Src = "http://barcode.tec-it.com/barcode.ashx?code=QRCode&modulewidth=20.417&unit=mils&data=http%3A%2F%2Fnorthsouth.org%2F" & Request.ApplicationPath.ToString().Replace("/", "") & "%2FQRAttendance.aspx%3FChildNumber%3D" & dsBadge.Tables(0).Rows(i - 1)("ChildNumber") & "&dpi=72&imagetype=gif&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mils&eclevel=/""Barcode generated by TEC-IT/"""
                        BadgeHolder.Controls.Add(imageBarCode)

                        BadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))

                        Dim week_days() As String
                        week_days = dsBadge.Tables(0).Rows(i - 1)("Week_Days").ToString().Split(";")
                        For week_day_number = 0 To week_days.GetUpperBound(0)
                            BadgeHolder.Controls.Add(New LiteralControl("<tr bgcolor='#F0FFF0'>"))
                            BadgeHolder.Controls.Add(New LiteralControl("<td align='center'>"))

                            lblWeekDay = New Label
                            lblWeekDay.Font.Size = 12
                            lblWeekDay.Font.Bold = True

                            If week_day_number <> week_days.GetUpperBound(0) Then
                                lblWeekDay.Text = week_days(week_day_number).ToString() & ":"
                            End If
                            'lblWeekDay.Text = week_days(week_day_number).ToString() & ":"

                            BadgeHolder.Controls.Add(lblWeekDay)
                            BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                            BadgeHolder.Controls.Add(New LiteralControl("<td colspan='2' align='center'>"))

                            'Display Badgenumbers for particular day
                            Dim a() As String

                            a = dsBadge.Tables(0).Rows(i - 1)("Badge_Numbers").ToString().Split(";")
                            For badge_number = 0 To a.GetUpperBound(0)

                                If UCase(a(badge_number).Substring(0, 3)) = UCase(week_days(week_day_number).ToString()) Then
                                    lblBadgeNumber = New Label

                                    'If a.Length > 3 And a.Length < 5 Then
                                    '    lblBadgeNumber.Font.Size = 12
                                    '    lblBadgeNumber.Font.Bold = True
                                    'ElseIf a.Length > 5 Then
                                    '    lblBadgeNumber.Font.Size = 10
                                    '    lblBadgeNumber.Font.Bold = True
                                    'ElseIf a.Length = 3 Then
                                    lblBadgeNumber.Font.Size = 12
                                    lblBadgeNumber.Font.Bold = True
                                    'Else
                                    '    lblBadgeNumber.Font.Size = 15
                                    '    lblBadgeNumber.Font.Bold = True
                                    'End If

                                    lblBadgeNumber.Text = Mid(a(badge_number).ToString(), 4)
                                    BadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))

                                    If UCase(a(badge_number).Substring(4, 2)) = "SB" Then
                                        lblBadgeNumber.ForeColor = Color.Green
                                    ElseIf UCase(a(badge_number).Substring(4, 2)) = "VB" Then
                                        lblBadgeNumber.ForeColor = Color.Blue
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "MB" Then
                                        lblBadgeNumber.ForeColor = Color.Red
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "EW" Then
                                        lblBadgeNumber.ForeColor = Color.DeepPink
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "PS" Then
                                        lblBadgeNumber.ForeColor = Color.Red
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "BB" Then
                                        lblBadgeNumber.ForeColor = Color.Green
                                    ElseIf UCase(a(badge_number).Substring(3, 2)) = "GB" Then
                                        lblBadgeNumber.ForeColor = Color.Black
                                    End If
                                    BadgeHolder.Controls.Add(lblBadgeNumber)
                                End If
                                ' BadgeHolder.Controls.Add(New LiteralControl("&nbsp;"))

                            Next
                            'To display TshirtSize and LunchType on Badges
                            If dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString() = "YYL" Then
                                TshirtName = "L"
                            ElseIf dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString() = "YM" Then
                                TshirtName = "M"
                            ElseIf dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString() = "YL" Then
                                TshirtName = "YL"
                            Else
                                TshirtName = ""
                            End If

                            lblTshirt_Food.Font.Size = 12
                            lblTshirt_Food.Font.Bold = True
                            lblTshirt_Food.Text = "Food: " & IIf(dsBadge.Tables(0).Rows(i - 1)("LunchType").ToString() = "Pizza", "P", "I") & "&nbsp;&nbsp;&nbsp;  TSize: " & TshirtName ' dsBadge.Tables(0).Rows(i - 1)("TshirtSize").ToString()
                            BadgeHolder.Controls.Add(lblTshirt_Food)

                            'end of display badge number
                            BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                            BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                            'end of display badge numbe
                            BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                            BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                        Next

                        BadgeHolder.Controls.Add(New LiteralControl("<tr>"))
                        BadgeHolder.Controls.Add(New LiteralControl("<td bgcolor='#F0E68C' align='center'>"))
                        lblParentChildID = New Label
                        lblParentChildID.ForeColor = Color.Red
                        lblParentChildID.Font.Name = "Verdana"
                        lblParentChildID.Font.Size = 12
                        lblParentChildID.Font.Bold = True
                        lblParentChildID.Text = dsBadge.Tables(0).Rows(i - 1)("parent_id").ToString() & "-" & dsBadge.Tables(0).Rows(i - 1)("child_number").ToString()
                        BadgeHolder.Controls.Add(lblParentChildID)
                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                        BadgeHolder.Controls.Add(New LiteralControl("<td bgcolor='#F0E68C' align='center'>"))

                        lblChildLocation = New Label
                        lblChildLocation.ForeColor = Color.Red
                        lblParentChildID.Font.Name = "Verdana"
                        lblChildLocation.Font.Size = 12
                        lblChildLocation.Font.Bold = True
                        lblChildLocation.Text = dsBadge.Tables(0).Rows(i - 1)("city").ToString() & ", " & dsBadge.Tables(0).Rows(i - 1)("state").ToString()
                        BadgeHolder.Controls.Add(lblChildLocation)


                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                        BadgeHolder.Controls.Add(New LiteralControl("<td bgcolor='#F0E68C' align='center'>"))
                        lblGrade = New Label
                        lblGrade.ForeColor = Color.Red
                        lblGrade.Font.Name = "Verdana"
                        lblGrade.Font.Size = 12
                        lblGrade.Font.Bold = True
                        lblGrade.Text = "Gr:&nbsp;<font color=blue>" & dsBadge.Tables(0).Rows(i - 1)("grade").ToString() & "</font>"

                        BadgeHolder.Controls.Add(lblGrade)

                        BadgeHolder.Controls.Add(New LiteralControl("</td>"))

                        BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                        BadgeHolder.Controls.Add(New LiteralControl("</table>"))
                        'end of table 2-1
                        BadgeHolder.Controls.Add(New LiteralControl("</td></tr>"))
                        'end of table 2
                        BadgeHolder.Controls.Add(New LiteralControl("</table>"))

                        BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                    End If
                    If iPerPageCount = j Then
                        j = 1
                        'BadgeHolder.Controls.Add(New LiteralControl("<tr><td colspan='2'>&nbsp;</td></tr>"))
                        'BadgeHolder.Controls.Add(New LiteralControl("<div style='page-break-after:always'>&nbsp;</div>"))
                        'BadgeHolder.Controls.Add(New LiteralControl("<P CLASS='breakhere'>"))
                    End If
                    j = j + 1
                Next
                pnlData.Visible = True
                pnlMessage.Visible = False
            Else
                pnlData.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No Badge Details found."
            End If
            If generateBadge.Export = True Then
                Response.Clear()
                Response.Buffer = True
                Dim strChapterName As String
                If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                    strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
                End If
                Response.AddHeader("content-disposition", "attachment;filename=Badges_Participants_" & strChapterName & ".doc")
                Response.Charset = ""
                Response.ContentType = "application/vnd.word"
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As New HtmlTextWriter(stringWrite)
                pnlData.RenderControl(htmlWrite)
                Response.Write("<html>")
                Response.Write("<head>")
                Response.Write("<style>")
                'Response.Write(" @page Section1 {size:8.5in 11.0in;margin:60.0pt .25in 0in .06in;mso-header-margin:.25in;mso-footer-margin:.06in;mso-paper-source:4;}")
                'Response.Write(" @page Section1 {size:8.5in 11.0in;margin:1.00in .50in .70in .50in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:4;}")
                Response.Write(" @page Section1 {size:8.5in 11.0in;margin:1.00in .10in .80in .10in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:4;}")
                'Response.Write("div.Section1 {page:Section1;}")
                Response.Write("</style>")
                Response.Write("</head>")
                Response.Write("<body>")
                Response.Write(stringWrite.ToString())
                Response.Write("</body>")
                Response.Write("</html>")
                Response.End()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Function contestdate(ByVal str As String) As String
        Dim returnstr As String = ""
        Dim strarr() As String
        Dim count As Integer
        strarr = str.Split(",")
        For count = 0 To strarr.Length - 1
            If count = 0 Then
                returnstr = strarr(count)
            ElseIf Not returnstr.Contains(strarr(count)) Then
                If (strarr(count).Substring(0, 3) = strarr(count - 1).Substring(0, 3)) And strarr(count) <> strarr(count - 1) Then ' (strarr(count).Substring(3, (strarr(count).Length - 3)) <> strarr(count - 1).Substring(3, (strarr(count - 1).Length - 3)))
                    returnstr = returnstr & "-" & strarr(count).Substring(3, (strarr(count).Length - 3))
                ElseIf strarr(count) = strarr(count - 1) Then
                    returnstr = returnstr
                Else
                    returnstr = returnstr & "," & strarr(count)
                End If
            End If
        Next
        Return returnstr
    End Function

    Public Function CreateThumbnail(ByVal imageUrl As String) As String

        'thumbnail creation starts
        Try
            'Read in the image filename whose thumbnail has to be created
            ' Dim imageUrl As String = UploadedFileName

            'Read in the width and height
            Dim imageHeight As Integer = 70 ' = Convert.ToInt32(h.Text)
            Dim imageWidth As Integer = 70 '= Convert.ToInt32(w.Text)

            'You may even specify a standard thumbnail size
            'int imageWidth  = 70; 
            'int imageHeight = 70;

            'If imageUrl.IndexOf("/") >= 0 OrElse imageUrl.IndexOf("\") >= 0 Then
            'We found a / or \
            'Response.End()
            'End If

            'the uploaded image will be stored in the Pics folder.
            'to get resize the image, the original image has to be accessed from the
            'Pics folder
            'imageUrl = "pics/" + imageUrl

            Dim fullSizeImg As System.Drawing.Image = System.Drawing.Image.FromFile(Server.MapPath(imageUrl))

            Dim dummyCallBack As New System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)

            Dim thumbNailImg As System.Drawing.Image = fullSizeImg.GetThumbnailImage(imageWidth, imageHeight, dummyCallBack, IntPtr.Zero)

            'We need to create a unique filename for each generated image
            Dim MyDate As DateTime = DateTime.Now

            Dim MyString As String = MyDate.ToString("ddMMyyhhmmss") + ".png"

            'Save the thumbnail in Png format. You may change it to a diff format with the ImageFormat property
            ' thumbNailImg.Save(Request.PhysicalApplicationPath + "photos_badge\" + MyString, System.Drawing.Imaging.ImageFormat.Png)
            thumbNailImg.Save(Server.MapPath(".") & MyString, System.Drawing.Imaging.ImageFormat.Png)
            thumbNailImg.Dispose()

            Return MyString
            'Display the original & the newly generated thumbnail
            ' Image1.AlternateText = "Original image"
            'Image1.ImageUrl = "pics\" + UploadedFileName

            'Image2.AlternateText = "Thumbnail"
            'Image2.ImageUrl = "pics\" + MyString
        Catch ex As Exception
            'Response.Write(Request.PhysicalApplicationPath + "photos_badge\")
            'Response.Write("An error occurred - " + ex.ToString())
            Response.Write(Server.MapPath(imageUrl))
            Response.Write(Server.MapPath("./photos_badge/bee.jpg"))
            Return Server.MapPath("./photos_badge/bee.jpg")
        End Try

    End Function

    'this function is reqd for thumbnail creation
    Public Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Public Function FileExists(ByVal FileFullPath As String) _
         As Boolean

        Dim f As New IO.FileInfo(FileFullPath)
        Return f.Exists

    End Function

 

End Class
