﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingRegistration.aspx.vb" Inherits="CoachingRegistration"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <script type = "text/javascript">
     function CheckBoxCheck(rb) {
         alert("****************" + "<%=dgCoachSelection.ClientID%>" );
         var gv = document.getElementById("<%=dgCoachSelection.ClientID%>");
         var row = rb.parentNode.parentNode;
         var rbs = row.getElementsByTagName("input");
         for (var i = 0; i < rbs.length; i++) {
             if (rbs[i].type == "checkbox") {
                 if (rbs[i].checked && rbs[i] != rb) {
                     rbs[i].checked = false;
                     break;
                 }
             }
         }
     }   
</script>
<table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left" colspan="2">
						<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
                        
                    </td>
				</tr>			</table>
			<table  border="0" cellpadding="3" cellspacing="0">
			<tr><td align="left" width="200px">&nbsp;</td><td align="center" colspan="2"><H5>Registration for Coaching</H5></td><td align="left" width="100px">
                &nbsp;</td><td align="left"><asp:Label ID="PayError" runat="server" Text="" ForeColor="Red"  ></asp:Label></td></tr>
			<tr><td align="left">&nbsp;</td><td align="left">Child</td><td align="left"><asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField ="ChildNumber" Width="150px" runat="server"></asp:DropDownList></td><td align="left"></td><td align="left"> 
                                    <asp:Button ID="btnDonate" runat="server" Text="Pay Now" Visible="false" 
                                        Font-Bold="True" ForeColor="Red" /></td></tr>
            <tr><td align="left">&nbsp;</td><td align="left">Grade</td><td align="left"><asp:DropDownList ID="ddlGrade" AutoPostBack="false" DataTextField="Grade" DataValueField ="Grade" Width="150px" runat="server"></asp:DropDownList></td><td align="left"></td><td align="left"> 
                                    </td></tr>                                       
			<tr><td align="left">&nbsp;</td><td align="left">Product</td><td align="left"><asp:DropDownList  Width="150px" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" ID="ddlProduct" DataTextField="ProductName" DataValueField="ProductID" runat="server"></asp:DropDownList></td><td align="left"></td><td align="left">
                  Your registration helps a poor child go to college in India.</td></tr>
                 <tr ID ="TrDeadlineDate" runat="server" visible="False"><td></td><td></td><td></td><td></td>
                <td>
                <table border ="0" cellpadding ="2" cellspacing ="0">
                <tr><td>Registration Deadline</td><td>: <asp:Label ID="lblDLDate" ForeColor="Red" runat="server"></asp:Label></td></tr>
                <tr><td>Late Registration Deadline</td><td>: <asp:Label ID="lblLateDLDate" ForeColor="Red"  runat="server" Text=""/></td></tr>
                 </table>
                 </td>
                </tr>
                
      		<tr><td align="left">&nbsp;</td><td align="left" colspan="4">
      		        <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label> <br />
                    <asp:Label ID="lblNotReg" runat="server" ></asp:Label><br />
                    <asp:Label ID="lblRegErr" ForeColor="Red"  runat="server" ></asp:Label><br />
                    <asp:Label ID="lblGridError" runat="Server" Text="" ForeColor="Green" Font-Bold="true"></asp:Label><br />
                </td></tr>
          
			<tr><td align="left" colspan="5"><font color="red"> &nbsp; &nbsp; &nbsp; &nbsp; * If minimum capacity is not reached, child may be asked to change to another coach.</font></td></tr>
            <tr><td align="left" colspan="5"><font color="red"><asp:label ID="lblNote" Visible="true" runat="server" Font-Bold="true" Text="" ForeColor="Red" Font-Size="Small"></asp:label> </font></td></tr>
            	</table>
			 
			 <br /><br />
			<asp:Panel ID="pnlCoachSelection" GroupingText="Select Coach" Font-Bold="true" runat="server" Width="100%" BorderStyle="Solid" Visible="false" >
					   
					    <asp:Label ID="lblCoachSelection" runat="server"></asp:Label>&nbsp;
					   	<ASP:DATAGRID id="dgCoachSelection" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="SignUpID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn  HeaderStyle-Width="5%" ItemStyle-Width="5%">
						    <ItemTemplate>
						   <%-- <input type="checkbox" ID="chkCoachSelect" runat="server" Checked="false" onclick="CheckBoxCheck(this);" />
						   --%>    <asp:CheckBox ID="chkCoachSelect" runat="server" Checked="false" OnCheckedChanged="chkEvent_OnCheckedChanged"
                                        CssClass="SmallFont" AutoPostBack="true"/>
						        <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                            </ItemTemplate>
						</ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Product" HeaderStyle-Width="8%" ItemStyle-Width="8%">
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ITEMTEMPLATE>
						</ASP:TEMPLATECOLUMN>
                       <ASP:TemplateColumn HeaderText="Coach Name"  HeaderStyle-Width="15%" ItemStyle-Width="15%">
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                          <asp:Label ID="lblCoachName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                              </ItemTemplate>
                         </ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblLevel" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>						
						<ASP:TemplateColumn HeaderText="Capacity" ItemStyle-HorizontalAlign="Center" >							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblCapacity" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:BOUNDCOLUMN ItemStyle-HorizontalAlign = "Center" HeaderText="ApprovedCount" HeaderStyle-ForeColor = "White" HeaderStyle-Font-Bold = "true" DataField="ApprovedCount" Visible="true"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn> <%-- --%>
						<ASP:TemplateColumn HeaderText="Class Day" HeaderStyle-Width="25%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="25%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                       
                         <ASP:TemplateColumn HeaderText="End Date" Visible="False" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="LateReg.Deadline Date" Visible="False" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblRegDLDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "LateRegDLDate","{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Fee" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDonation" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Regfee","{0:c}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="Late Fee" HeaderStyle-Width="15%" ItemStyle-Width="15%" visible="true">
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblLateFee" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "latefee","{0:c}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="City" HeaderStyle-Width="15%" ItemStyle-Width="15%" visible="false">
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="State" HeaderStyle-Width="15%" ItemStyle-Width="15%" visible="false">
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
						<ASP:TemplateColumn><ItemTemplate>
<%--						<asp:LinkButton id="lblRemove" runat="server" CausesValidation="false" CommandName="Select" Text="Remove"></asp:LinkButton>
--%>						</ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:BOUNDCOLUMN DataField="Email" Visible="False" HeaderText="CoachEmail" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
					   <ASP:BOUNDCOLUMN DataField="ProductLevelPricing" Visible="true" HeaderText="ProductLevelPricing" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                      </COLUMNS>
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>	
             <div align="center">   <asp:Button ID="btnSubmit" runat="server" Visible="false" Text="Submit Selection" /></div>
    </asp:Panel>
    
    <asp:Panel ID="pnlSeleted" GroupingText="Selected Coaching Classes" Font-Bold="true" runat="server" Width="100%" BorderStyle="Solid" Visible="false" >
					    <asp:Label ID="lblSelected" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					    <asp:label ID="lblDiscount" runat="server" Visible="true"></asp:label>
						<br /><br />
						<ASP:DATAGRID id="dgselected" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="CoachRegID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Status" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="childnumber" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="SignUpID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="LateRegDLDate" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Email" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn><ItemTemplate>
						<asp:LinkButton id="lbtnRemove" runat="server" CausesValidation="false" CommandName="Select" Text="Remove"></asp:LinkButton>
						 <asp:Label ID="lblStatus1" runat="server" ></asp:Label>
						</ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						      <asp:Label ID="lblChildName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                            </ItemTemplate>
						</ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Product" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "Name")%></ITEMTEMPLATE>
						</ASP:TEMPLATECOLUMN>
                       <ASP:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate>
                                          <asp:Label ID="lblCoachName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                              </ItemTemplate>
                         </ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblLevel" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>										
						<ASP:TemplateColumn HeaderText="Capacity" ItemStyle-HorizontalAlign="Center"  >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						       <asp:Label ID="lblCapacity" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
						    </ItemTemplate>
						</ASP:TemplateColumn>
						<ASP:BOUNDCOLUMN ItemStyle-HorizontalAlign = "Center" HeaderText="ApprovedCount" HeaderStyle-ForeColor = "White" HeaderStyle-Font-Bold = "true" DataField="ApprovedCount" Visible="true"></ASP:BOUNDCOLUMN>
						 <ASP:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn><%----%>
						<ASP:TemplateColumn HeaderText="Class Day" HeaderStyle-Width="25%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="25%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                         </ASP:TemplateColumn>
                         <ASP:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        
                         <ASP:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:BOUNDCOLUMN DataField="LateRegDLDate" Visible="false" DataFormatString="{0:d}" HeaderText="LateReg.DeadLineDate" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                        <%--<ASP:TemplateColumn HeaderText="LateReg.Deadline Date" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblRegDLDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "LateRegDLDate","{0:d}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>--%>
                          <ASP:TemplateColumn HeaderText="Fee" HeaderStyle-Width="15%" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblDonation" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Regfee","{0:c}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="Late Fee" HeaderStyle-Width="15%" ItemStyle-Width="15%" visible="true">
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblLateFee" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "latefee","{0:c}")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="City" HeaderStyle-Width="15%" ItemStyle-Width="15%" visible="false">
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:TemplateColumn HeaderText="State" HeaderStyle-Width="15%" ItemStyle-Width="15%" visible="false">
							<HEADERSTYLE Font-Bold="True" ForeColor="White" ></HEADERSTYLE>
                              <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>'  Visible="True"></asp:Label>
                             </ItemTemplate>
                        </ASP:TemplateColumn>
                        <ASP:BOUNDCOLUMN DataField="Email" Visible="False" HeaderText="CoachEmail"></ASP:BOUNDCOLUMN>
                        <ASP:BOUNDCOLUMN DataField="ProductLevelPricing" Visible="true" HeaderText="ProductLevel Pricing" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                         </COLUMNS>
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>	
             <asp:Label ID="lblPendingErr" runat="server" Text="" ForeColor="Red"></asp:Label>
    </asp:Panel>
    <asp:Label ID="HlblBtnDonate" runat="server" Visible="false"  Text="N"></asp:Label>
</asp:Content>

