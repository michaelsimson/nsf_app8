	#region IndSpouse
	//***********************************************************************************************
	//                                    R I V I S I O N   H I S T O R Y                            
	//                                    -------------------------------                            
	// Class: dbo.IndSpouse.cs
	// Author: NSF
	// Organization: NorthSouth.org
	// Date Created: Wednesday, November 16, 2005
	// Purpose: IndSpouse table
	//***********************************************************************************************
	using System;
	using System.Data;
	using System.Data.SqlClient;
	using NorthSouth.DAL;

	/// <summary>
	/// This object represents the properties and methods of a IndSpouse.
	/// </summary>
	namespace NorthSouth.BAL
	{
	    public class IndSpouse10
	{
		protected int _id;
		protected int _relationship;
		protected string _deleteReason = String.Empty;
		protected string _deletedFlag = String.Empty;
		protected DateTime _createDate;
		protected DateTime _memberSince;
		protected DateTime _modifyDate;
		protected string _mailingLabel = String.Empty;
		protected string _sendReceipt = String.Empty;
		protected string _liasonPerson = String.Empty;
		protected string _referredBy = String.Empty;
		protected string _chapter = String.Empty;
		protected string _newsLetter = String.Empty;
		protected string _group2 = String.Empty;
		protected string _group3 = String.Empty;
		protected string _potentialDonor = String.Empty;
		protected string _youthVol = String.Empty;
		protected int _volunteerRole1;
		protected string _group1 = String.Empty;
		protected string _career = String.Empty;
		protected string _employer = String.Empty;
		protected string _countryOfOrigin = String.Empty;
		protected string _email = String.Empty;
		protected string _secondaryEmail = String.Empty;
		protected string _education = String.Empty;
		protected string _fax = String.Empty;
		protected string _wPhone = String.Empty;
		protected string _wFax = String.Empty;
		protected string _gender = String.Empty;
		protected string _hPhone = String.Empty;
		protected string _cPhone = String.Empty;
		protected string _state = String.Empty;
		protected string _zip = String.Empty;
		protected string _country = String.Empty;
		protected string _address1 = String.Empty;
		protected string _address2 = String.Empty;
		protected string _city = String.Empty;
		protected string _firstName = String.Empty;
		protected string _middleInitial = String.Empty;
		protected string _lastName = String.Empty;
		protected string _memberID = String.Empty;
		protected string _donorType = String.Empty;
		protected string _title = String.Empty;
		protected string _stateOfOrigin = String.Empty;
		protected string _primaryEmail_Old = String.Empty;
		protected string _homePhone_Old = String.Empty;
		protected string _cellPhone_Old = String.Empty;
		protected string _address1_Old = String.Empty;
		protected string _address2_Old = String.Empty;
		protected string _city_old = String.Empty;
		protected string _state_Old = String.Empty;
		protected string _zipCode_Old = String.Empty;
		protected int _primaryIndSpouseID;
		protected string _primaryContact = String.Empty;
		protected int _volunteerRole2;
		protected int _volunteerRole3;
        protected int _chapterID;
        protected string _maritalStatus = String.Empty;
        protected string _sponsor = String.Empty;
        protected string _liaison = String.Empty;
        protected int _createdBy;
        protected int _modifiedBy;


		public IndSpouse10()
		{
		}
		/// <summary>
		/// Retrieves Message Details for a given PK_IndSpouse
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="MessageID"></param>
		/// <returns></returns>
		public IndSpouse10 GetIndSpouseByID(string connectionString,int AutoMemberID)
		{
		
			IndSpouseDAL objDAL = new IndSpouseDAL(connectionString);
			SqlDataReader reader = objDAL.GetIndSpouseByID(AutoMemberID);			
			try
			{
				while (reader.Read())
				{
				_id = reader.GetInt32(0);
					if (reader["Relationship"] != DBNull.Value)
					{
						this.Relationship =(int) reader["Relationship"];
					}
					if (reader["DeleteReason"] != DBNull.Value)
					{
						this.DeleteReason =(string) reader["DeleteReason"];
					}
					if (reader["DeletedFlag"] != DBNull.Value)
					{
						this.DeletedFlag =(string) reader["DeletedFlag"];
					}
					if (reader["CreateDate"] != DBNull.Value)
					{
						this.CreateDate =(DateTime) reader["CreateDate"];
					}
					if (reader["MemberSince"] != DBNull.Value)
					{
						this.MemberSince =(DateTime) reader["MemberSince"];
					}
					if (reader["ModifyDate"] != DBNull.Value)
					{
						this.ModifyDate =(DateTime) reader["ModifyDate"];
					}
					if (reader["MailingLabel"] != DBNull.Value)
					{
						this.MailingLabel =(string) reader["MailingLabel"];
					}
					
					if (reader["SendReceipt"] != DBNull.Value)
					{
						this.SendReceipt =(string) reader["SendReceipt"];
					}
					if (reader["LiasonPerson"] != DBNull.Value)
					{
						this.LiasonPerson =(string) reader["LiasonPerson"];
					}
					if (reader["ReferredBy"] != DBNull.Value)
					{
						this.ReferredBy =(string) reader["ReferredBy"];
					}
					if (reader["Chapter"] != DBNull.Value)
					{
						this.Chapter =(string) reader["Chapter"];
					}
					if (reader["NewsLetter"] != DBNull.Value)
					{
						this.NewsLetter =(string) reader["NewsLetter"];
					}
					if (reader["Group2"] != DBNull.Value)
					{
						this.Group2 =(string) reader["Group2"];
					}
					if (reader["Group3"] != DBNull.Value)
					{
						this.Group3 =(string) reader["Group3"];
					}
					if (reader["PotentialDonor"] != DBNull.Value)
					{
						this.PotentialDonor =(string) reader["PotentialDonor"];
					}
					if (reader["YouthVol"] != DBNull.Value)
					{
                        this.YouthVol = (string)reader["YouthVol"];
					}
					if (reader["VolunteerRole1"] != DBNull.Value)
					{
						this.VolunteerRole1 =(int) reader["VolunteerRole1"];
					}
					if (reader["Group1"] != DBNull.Value)
					{
						this.Group1 =(string) reader["Group1"];
					}
					if (reader["Career"] != DBNull.Value)
					{
						this.Career =(string) reader["Career"];
					}

					if (reader["Employer"] != DBNull.Value)
					{
						this.Employer =(string) reader["Employer"];
					}

					if (reader["CountryOfOrigin"] != DBNull.Value)
					{
						this.CountryOfOrigin =(string) reader["CountryOfOrigin"];
					}
					if (reader["Email"] != DBNull.Value)
					{
						this.Email =(string) reader["Email"];
					}
					if (reader["SecondaryEmail"] != DBNull.Value)
					{
						this.SecondaryEmail =(string) reader["SecondaryEmail"];
					}
					if (reader["Education"] != DBNull.Value)
					{
						this.Education =(string) reader["Education"];
					}
					if (reader["Fax"] != DBNull.Value)
					{
						this.Fax =(string) reader["Fax"];
					}
					if (reader["WPhone"] != DBNull.Value)
					{
						this.WPhone =(string) reader["WPhone"];
					}
					if (reader["WFax"] != DBNull.Value)
					{
						this.WFax =(string) reader["WFax"];
					}
					if (reader["Gender"] != DBNull.Value)
					{
						this.Gender =(string) reader["Gender"];
					}
					if (reader["HPhone"] != DBNull.Value)
					{
						this.HPhone =(string) reader["HPhone"];
					}
					if (reader["CPhone"] != DBNull.Value)
					{
						this.CPhone =(string) reader["CPhone"];
					}
					if (reader["State"] != DBNull.Value)
					{
						this.State =(string) reader["State"];
					}
					if (reader["Zip"] != DBNull.Value)
					{
						this.Zip =(string) reader["Zip"];
					}
					if (reader["Country"] != DBNull.Value)
					{
						this.Country =(string) reader["Country"];
					}
					if (reader["Address1"] != DBNull.Value)
					{
						this.Address1 =(string) reader["Address1"];
					}
					if (reader["Address2"] != DBNull.Value)
					{
						this.Address2 =(string) reader["Address2"];
					}
					if (reader["City"] != DBNull.Value)
					{
						this.City =(string) reader["City"];
					}
					if (reader["City"] != DBNull.Value)
					{
						this.City =(string) reader["City"];
					}
					if (reader["FirstName"] != DBNull.Value)
					{
						this.FirstName =(string) reader["FirstName"];
					}
					if (reader["MiddleInitial"] != DBNull.Value)
					{
						this.MiddleInitial =(string) reader["MiddleInitial"];
					}
					if (reader["LastName"] != DBNull.Value)
					{
						this.LastName =(string) reader["LastName"];
					}
					if (reader["MemberID"] != DBNull.Value)
					{
						this.MemberID =(string) reader["MemberID"];
					}
					if (reader["DonorType"] != DBNull.Value)
					{
						this.DonorType =(string) reader["DonorType"];
					}
					if (reader["Title"] != DBNull.Value)
					{
						this.Title =(string) reader["Title"];
					}
					if (reader["StateOfOrigin"] != DBNull.Value)
					{
						this.StateOfOrigin =(string) reader["StateOfOrigin"];
					}
					if (reader["PrimaryEmail_Old"] != DBNull.Value)
					{
						this.PrimaryEmail_Old =(string) reader["PrimaryEmail_Old"];
					}
					if (reader["HomePhone_Old"] != DBNull.Value)
					{
						this.HomePhone_Old =(string) reader["HomePhone_Old"];
					}
					if (reader["CellPhone_Old"] != DBNull.Value)
					{
						this.CellPhone_Old =(string) reader["CellPhone_Old"];
					}
					if (reader["Address1_Old"] != DBNull.Value)
					{
						this.Address1_Old =(string) reader["Address1_Old"];
					}
					if (reader["Address2_Old"] != DBNull.Value)
					{
						this.Address2_Old =(string) reader["Address2_Old"];
					}
					if (reader["City_old"] != DBNull.Value)
					{
						this.City_old =(string) reader["City_old"];
					}
					if (reader["State_Old"] != DBNull.Value)
					{
						this.State_Old =(string) reader["State_Old"];
					}
					if (reader["ZipCode_Old"] != DBNull.Value)
					{
						this.ZipCode_Old =(string) reader["ZipCode_Old"];
					}
					if (reader["PrimaryIndSpouseID"] != DBNull.Value)
					{
						this.PrimaryIndSpouseID =(int) reader["PrimaryIndSpouseID"];
					}
					if (reader["PrimaryContact"] != DBNull.Value)
					{
						this.PrimaryContact =(string) reader["PrimaryContact"];
					}
					if (reader["VolunteerRole2"] != DBNull.Value)
					{
						this.VolunteerRole2 =(int) reader["VolunteerRole2"];
					}
					if (reader["VolunteerRole3"] != DBNull.Value)
					{
						this.VolunteerRole3 =(int) reader["VolunteerRole3"];
					}
                    if (reader["ChapterID"] != DBNull.Value)
                    {
                        this.ChapterID = (int)reader["ChapterID"];
                    }
                    if (reader["MaritalStatus"] != DBNull.Value)
                    {
                        this.MaritalStatus = (string)reader["MaritalStatus"];
                    }
                    if (reader["Sponsor"] != DBNull.Value)
                    {
                        this.Sponsor = (string)reader["Sponsor"];
                    }
                    if (reader["Liaison"] != DBNull.Value)
                    {
                        this.Liaison = (string)reader["Liaison"];
                    }
                    if (reader["CreatedBy"] != DBNull.Value)
                    {
                        this.CreatedBy = (int)reader["CreatedBy"];
                    }
                    if (reader["ModifiedBy"] != DBNull.Value)
                    {
                        this.ModifiedBy = (int)reader["ModifiedBy"];
                    }



                
                
                
                }
			}
					finally
			{
				reader.Close();		
			}
			return this;
		}
		
		/// <summary>
		/// Deletes a IndSpouse for a given id
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="MessageID"></param>
		public void DeleteIndSpouse(string connectionString)
		{
			IndSpouseDAL objDAL = new IndSpouseDAL(connectionString);
			objDAL.DeleteIndSpouse(this._id);			
		}
		/// <summary>
		/// Adds New IndSpouse Entry to IndSpouse
		/// </summary>
		/// <param name="connectionString"></param>
		/// <returns></returns>
		public int AddIndSpouse(string connectionString)
		{
			IndSpouseDAL objDAL = new IndSpouseDAL(connectionString);
            return objDAL.AddIndSpouse(this.Relationship, this.DeleteReason, this.DeletedFlag, this.CreateDate, this.MemberSince, this.ModifyDate, this.MailingLabel, this.SendReceipt, this.LiasonPerson, this.ReferredBy, this.Chapter, this.NewsLetter, this.Group2, this.Group3, this.PotentialDonor, this.YouthVol, this.VolunteerRole1, this.Group1, this.Career, this.Employer, this.CountryOfOrigin, this.Email, this.SecondaryEmail, this.Education, this.Fax, this.WPhone, this.WFax, this.Gender, this.HPhone, this.CPhone, this.State, this.Zip, this.Country, this.Address1, this.Address2, this.City, this.FirstName, this.MiddleInitial, this.LastName, this.MemberID, this.DonorType, this.Title, this.StateOfOrigin, this.PrimaryEmail_Old, this.HomePhone_Old, this.CellPhone_Old, this.Address1_Old, this.Address2_Old, this.City_old, this.State_Old, this.ZipCode_Old, this.PrimaryIndSpouseID, this.PrimaryContact, this.VolunteerRole2, this.VolunteerRole3, this.ChapterID, this.MaritalStatus, this.Sponsor, this.Liaison, this.CreatedBy, this.ModifiedBy);
		}
		/// <summary>
		/// Updates given IndSpouse Entry in IndSpouse Table
		/// </summary>
		/// <param name="connectionString"></param>
		public int UpdateIndSpouse(string connectionString)  
		{
			IndSpouseDAL objDAL = new IndSpouseDAL(connectionString);
            return objDAL.UpdateIndSpouse(this._id, this.Relationship, this.DeleteReason, this.DeletedFlag, this.CreateDate, this.MemberSince, this.ModifyDate, this.MailingLabel, this.SendReceipt, this.LiasonPerson, this.ReferredBy, this.Chapter, this.NewsLetter, this.Group2, this.Group3, this.PotentialDonor, this.YouthVol, this.VolunteerRole1, this.Group1, this.Career, this.Employer, this.CountryOfOrigin, this.Email, this.SecondaryEmail, this.Education, this.Fax, this.WPhone, this.WFax, this.Gender, this.HPhone, this.CPhone, this.State, this.Zip, this.Country, this.Address1, this.Address2, this.City, this.FirstName, this.MiddleInitial, this.LastName, this.MemberID, this.DonorType, this.Title, this.StateOfOrigin, this.PrimaryEmail_Old, this.HomePhone_Old, this.CellPhone_Old, this.Address1_Old, this.Address2_Old, this.City_old, this.State_Old, this.ZipCode_Old, this.PrimaryIndSpouseID, this.PrimaryContact, this.VolunteerRole2, this.VolunteerRole3, this.ChapterID, this.MaritalStatus, this.Sponsor, this.Liaison, this.CreatedBy, this.ModifiedBy);
		}

		/// <summary>
		/// Retrieves All IndSpouses in IndSpouse Table
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="dsBlogMessages"></param>
		/// <returns></returns>
		public DataSet GetAllIndSpouses(string connectionString, DataSet ds)
		{

			IndSpouseDAL objDAL = new IndSpouseDAL(connectionString);
			ds = objDAL.GetIndSpouseAll(ds);
			return ds;
		}

		/// <summary>
		/// Retrieves Rows from IndSpouses in IndSpouse Table Where Condition
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="dsBlogMessages"></param>
		/// <returns></returns>
		public DataSet SearchIndSpouseWhere(string connectionString, DataSet ds, string WhereCondition)
		{

			IndSpouseDAL objDAL = new IndSpouseDAL(connectionString);
			ds = objDAL.SearchIndSpouseWhere(ds,WhereCondition);
			return ds;
		}

		
		#region Public Properties
		public int Id
		{
			get {return _id;}
		}
		
		public int Relationship
		{
			get {return _relationship;}
			set {_relationship = value;}
		}

		public string DeleteReason
		{
			get {return _deleteReason;}
			set {_deleteReason = value;}
		}

		public string DeletedFlag
		{
			get {return _deletedFlag;}
			set {_deletedFlag = value;}
		}

		public DateTime CreateDate
		{
			get {return _createDate;}
			set {_createDate = value;}
		}

		public DateTime MemberSince
		{
			get {return _memberSince;}
			set {_memberSince = value;}
		}

		public DateTime ModifyDate
		{
			get {return _modifyDate;}
			set {_modifyDate = value;}
		}

		public string MailingLabel
		{
			get {return _mailingLabel;}
			set {_mailingLabel = value;}
		}

		public string SendReceipt
		{
			get {return _sendReceipt;}
			set {_sendReceipt = value;}
		}

		public string LiasonPerson
		{
			get {return _liasonPerson;}
			set {_liasonPerson = value;}
		}

		public string ReferredBy
		{
			get {return _referredBy;}
			set {_referredBy = value;}
		}

		public string Chapter
		{
			get {return _chapter;}
			set {_chapter = value;}
		}

		public string NewsLetter
		{
			get {return _newsLetter;}
			set {_newsLetter = value;}
		}

		public string Group2
		{
			get {return _group2;}
			set {_group2 = value;}
		}

		public string Group3
		{
			get {return _group3;}
			set {_group3 = value;}
		}

		public string PotentialDonor
		{
			get {return _potentialDonor;}
			set {_potentialDonor = value;}
		}

        public string YouthVol
		{
			get {return _youthVol;}
			set {_youthVol = value;}
		}

		public int VolunteerRole1
		{
			get {return _volunteerRole1;}
			set {_volunteerRole1 = value;}
		}

		public string Group1
		{
			get {return _group1;}
			set {_group1 = value;}
		}

		public string Career
		{
			get {return _career;}
			set {_career = value;}
		}

		public string Employer
		{
			get {return _employer;}
			set {_employer = value;}
		}

		public string CountryOfOrigin
		{
			get {return _countryOfOrigin;}
			set {_countryOfOrigin = value;}
		}

		public string Email
		{
			get {return _email;}
			set {_email = value;}
		}

		public string SecondaryEmail
		{
			get {return _secondaryEmail;}
			set {_secondaryEmail = value;}
		}

		public string Education
		{
			get {return _education;}
			set {_education = value;}
		}

		public string Fax
		{
			get {return _fax;}
			set {_fax = value;}
		}

		public string WPhone
		{
			get {return _wPhone;}
			set {_wPhone = value;}
		}

		public string WFax
		{
			get {return _wFax;}
			set {_wFax = value;}
		}

		public string Gender
		{
			get {return _gender;}
			set {_gender = value;}
		}

		public string HPhone
		{
			get {return _hPhone;}
			set {_hPhone = value;}
		}

		public string CPhone
		{
			get {return _cPhone;}
			set {_cPhone = value;}
		}

		public string State
		{
			get {return _state;}
			set {_state = value;}
		}

		public string Zip
		{
			get {return _zip;}
			set {_zip = value;}
		}

		public string Country
		{
			get {return _country;}
			set {_country = value;}
		}

		public string Address1
		{
			get {return _address1;}
			set {_address1 = value;}
		}

		public string Address2
		{
			get {return _address2;}
			set {_address2 = value;}
		}

		public string City
		{
			get {return _city;}
			set {_city = value;}
		}

		public string FirstName
		{
			get {return _firstName;}
			set {_firstName = value;}
		}

		public string MiddleInitial
		{
			get {return _middleInitial;}
			set {_middleInitial = value;}
		}

		public string LastName
		{
			get {return _lastName;}
			set {_lastName = value;}
		}

		public string MemberID
		{
			get {return _memberID;}
			set {_memberID = value;}
		}

		public string DonorType
		{
			get {return _donorType;}
			set {_donorType = value;}
		}

		public string Title
		{
			get {return _title;}
			set {_title = value;}
		}

		public string StateOfOrigin
		{
			get {return _stateOfOrigin;}
			set {_stateOfOrigin = value;}
		}

		public string PrimaryEmail_Old
		{
			get {return _primaryEmail_Old;}
			set {_primaryEmail_Old = value;}
		}

		public string HomePhone_Old
		{
			get {return _homePhone_Old;}
			set {_homePhone_Old = value;}
		}

		public string CellPhone_Old
		{
			get {return _cellPhone_Old;}
			set {_cellPhone_Old = value;}
		}

		public string Address1_Old
		{
			get {return _address1_Old;}
			set {_address1_Old = value;}
		}

		public string Address2_Old
		{
			get {return _address2_Old;}
			set {_address2_Old = value;}
		}

		public string City_old
		{
			get {return _city_old;}
			set {_city_old = value;}
		}

		public string State_Old
		{
			get {return _state_Old;}
			set {_state_Old = value;}
		}

		public string ZipCode_Old
		{
			get {return _zipCode_Old;}
			set {_zipCode_Old = value;}
		}

		public int PrimaryIndSpouseID
		{
			get {return _primaryIndSpouseID;}
			set {_primaryIndSpouseID = value;}
		}

		public string PrimaryContact
		{
			get {return _primaryContact;}
			set {_primaryContact = value;}
		}

		public int VolunteerRole2
		{
			get {return _volunteerRole2;}
			set {_volunteerRole2 = value;}
		}

		public int VolunteerRole3
		{
			get {return _volunteerRole3;}
			set {_volunteerRole3 = value;}
		}
        public int ChapterID
        {
            get { return _chapterID; }
            set { _chapterID = value; }
        }
        public string MaritalStatus
        {
            get { return _maritalStatus; }
            set { _maritalStatus = value; }
        }
        public string Sponsor
        {
            get { return _sponsor; }
            set { _sponsor = value; }
        }
        public string Liaison
        {
            get { return _liaison; }
            set { _liaison = value; }
        }
        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }
        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }


#endregion
		}
	}
	#endregion
