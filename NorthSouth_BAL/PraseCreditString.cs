using System;
using System.Text;

namespace NorthSouth.BAL
{
	/// <summary>
	/// Summary description for PraseCreditString.
	/// </summary>
	public class   PraseCreditString
	{
		// response holders
		public string R_Time;
		public string R_Ref;
		public string R_Approved;
		public string R_Code;
		public string R_Authresr;
		public string R_Error;
		public string R_OrderNum;
		public string R_Message;
		public string R_Score;
		public string R_TDate;
		public string R_AVS;
		public string R_FraudCode;
		public string R_ESD;
		public string R_Tax;
		public string R_Shipping;


		public void ParseResponse( string rsp )
		{

			R_Time=ParseTag("r_time",rsp);
			R_Ref=ParseTag("r_ref",rsp);
			R_Approved=ParseTag("r_approved",rsp);
			R_Code=ParseTag("r_code",rsp);
			R_Authresr=ParseTag("r_authresronse",rsp);
			R_Error=ParseTag("r_error",rsp);
			R_OrderNum=ParseTag("r_ordernum",rsp);
			R_Message=ParseTag("r_message",rsp);
			R_Score=ParseTag("r_score",rsp);
			R_TDate=ParseTag("r_tdate",rsp);
			R_AVS=ParseTag("r_avs",rsp);
			R_Tax=ParseTag("r_tax",rsp);
			R_Shipping=ParseTag("r_shipping",rsp);
			R_FraudCode=ParseTag("r_fraudCode",rsp);
			R_ESD=ParseTag("esd",rsp);		
		}

		protected string ParseTag(string tag, string rsp )
		{
			StringBuilder sb = new StringBuilder(256);
			sb.AppendFormat("<{0}>",tag);
			int len = sb.Length; 
			int idxSt=-1,idxEnd=-1; 
			if( -1 == (idxSt = rsp.IndexOf(sb.ToString())))
			{ return ""; }
			idxSt += len;
			sb.Remove(0,len);
			sb.AppendFormat("</{0}>",tag);
			if( -1 == (idxEnd = rsp.IndexOf(sb.ToString(),idxSt)))
			{ return ""; }
			return rsp.Substring(idxSt,idxEnd-idxSt);
		}
	}
}
