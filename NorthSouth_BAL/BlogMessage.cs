//***********************************************************************************************
//                                    R I V I S I O N   H I S T O R Y                            
//                                    -------------------------------                            
// Class:
// Author:
// Organization:
// Date Created:
// Purpose:
//***********************************************************************************************
using System;
using System.Data;
using System.Data.SqlClient;
using NorthSouth.DAL;

namespace NorthSouth.BAL
{
		/// <summary>
		/// Summary description for BlogMessage.
		/// </summary>
		public class BlogMessage
		{
			/// <summary>
			/// <Blog_Messages MessageID="1" Title="ASPNET" Message="Some Test" AddedDate="2005-01-01T00:00:00"/>
			/// </summary>
		
			private int _messageID;
			private string _title;
			private string _message;
			private DateTime _addedDate;
		
			public int MessageID
			{
				get {return _messageID;}
				set {_messageID = value;}
			}
			public string Title
			{
				get {return _title;}
				set {_title = value;}
			}
			public string Message
			{
				get {return _message;}
				set {_message = value;}
			}
			public DateTime AddedDate
			{
				get {return _addedDate;}
				set {_addedDate = value;}
			}

		
			public BlogMessage()
			{

			}
			/// <summary>
			/// Retrieves Message Details for a given MessageID
			/// </summary>
			/// <param name="connectionString"></param>
			/// <param name="MessageID"></param>
			/// <returns></returns>
			public BlogMessage GetBlogMessageByID(string connectionString,int MessageID)
			{
			
				NorthSouth.DAL.BlogMessageDAL blogMessage = new NorthSouth.DAL.BlogMessageDAL(connectionString);
				SqlDataReader drBlogMessage = blogMessage.GetBlogMessageByID(MessageID);			
				if (drBlogMessage != null)
				{
					while (drBlogMessage.Read())
					{
						// Set the properties for the Business object
						this.MessageID = (int)(drBlogMessage["MessageID"]);
						this.Title = (string)(drBlogMessage["Title"]);
						this.Message = (string) (drBlogMessage["Message"]);
						this.AddedDate = (DateTime) (drBlogMessage["AddedDate"]);

					}
					return this;
				}
				else
					return null;
			}
			/// <summary>
			/// Deletes a Message for a given MessageID
			/// </summary>
			/// <param name="connectionString"></param>
			/// <param name="MessageID"></param>
			public void DeleteMessage(string connectionString)
			{
				NorthSouth.DAL.BlogMessageDAL blogMessage = new NorthSouth.DAL.BlogMessageDAL(connectionString);
				blogMessage.DeleteBlogMessage(this.MessageID);			
			}
			/// <summary>
			/// 
			/// </summary>
			/// <param name="connectionString"></param>
			/// <returns></returns>
			public int AddMessage(string connectionString)
			{
				NorthSouth.DAL.BlogMessageDAL blogMessage = new NorthSouth.DAL.BlogMessageDAL(connectionString);
				return blogMessage.AddBlogMessage(this.Title, this.Message, this.AddedDate);
			}
			/// <summary>
			/// 
			/// </summary>
			/// <param name="connectionString"></param>
			public void UpdateMessage(string connectionString)
			{
				NorthSouth.DAL.BlogMessageDAL blogMessage = new NorthSouth.DAL.BlogMessageDAL(connectionString);
				blogMessage.UpdateBlogMessage(this.MessageID,this.Title, this.Message, this.AddedDate);
			}
			/// <summary>
			/// 
			/// </summary>
			/// <param name="connectionString"></param>
			/// <param name="dsBlogMessages"></param>
			/// <returns></returns>
			public DataSet GetAllMessages(string connectionString, DataSet dsBlogMessages)
			{

				NorthSouth.DAL.BlogMessageDAL blogMessage = new NorthSouth.DAL.BlogMessageDAL(connectionString);
				dsBlogMessages = blogMessage.GetAllBlogMessages(dsBlogMessages);
				return dsBlogMessages;
			}

		}
	}

