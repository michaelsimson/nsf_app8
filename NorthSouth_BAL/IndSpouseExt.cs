using System;
using System.Data;
using System.Data.SqlClient;
using NorthSouth.DAL;

namespace NorthSouth.BAL
{
	/// <summary>
	/// Summary description for IndSpouseExt.
	/// </summary>
	public class IndSpouseExt : IndSpouse10
	{
		public IndSpouseExt()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public IndSpouse10 GetIndSpouseByEmail(string connectionString,string email)
		{
		
			IndSpouseExtDAL objDAL = new IndSpouseExtDAL(connectionString);
			SqlDataReader reader = objDAL.GetIndSpouseByEmail(email);			
			try
			{
				while (reader.Read())
				{
					_id = reader.GetInt32(0);
					if (reader["Relationship"] != DBNull.Value)
					{
						this.Relationship =(int) reader["Relationship"];
					}
					if (reader["DeleteReason"] != DBNull.Value)
					{
						this.DeleteReason =(string) reader["DeleteReason"];
					}
					if (reader["DeletedFlag"] != DBNull.Value)
					{
						this.DeletedFlag =(string) reader["DeletedFlag"];
					}
					if (reader["CreateDate"] != DBNull.Value)
					{
						this.CreateDate =(DateTime) reader["CreateDate"];
					}
					if (reader["MemberSince"] != DBNull.Value)
					{
						this.MemberSince =(DateTime) reader["MemberSince"];
					}
					if (reader["ModifyDate"] != DBNull.Value)
					{
						this.ModifyDate =(DateTime) reader["ModifyDate"];
					}
					if (reader["MailingLabel"] != DBNull.Value)
					{
						this.MailingLabel =(string) reader["MailingLabel"];
					}
					
					if (reader["SendReceipt"] != DBNull.Value)
					{
						this.SendReceipt =(string) reader["SendReceipt"];
					}
					if (reader["LiasonPerson"] != DBNull.Value)
					{
						this.LiasonPerson =(string) reader["LiasonPerson"];
					}
					if (reader["ReferredBy"] != DBNull.Value)
					{
						this.ReferredBy =(string) reader["ReferredBy"];
					}
					if (reader["Chapter"] != DBNull.Value)
					{
						this.Chapter =(string) reader["Chapter"];
					}
					if (reader["NewsLetter"] != DBNull.Value)
					{
						this.NewsLetter =(string) reader["NewsLetter"];
					}
					if (reader["Group2"] != DBNull.Value)
					{
						this.Group2 =(string) reader["Group2"];
					}
					if (reader["Group3"] != DBNull.Value)
					{
						this.Group3 =(string) reader["Group3"];
					}
					if (reader["PotentialDonor"] != DBNull.Value)
					{
						this.PotentialDonor =(string) reader["PotentialDonor"];
					}
                    if (reader["YouthVol"] != DBNull.Value)
					{
                        this.YouthVol = (string)reader["YouthVol"];
					}
					if (reader["VolunteerRole1"] != DBNull.Value)
					{
						this.VolunteerRole1 =(int) reader["VolunteerRole1"];
					}
					if (reader["Group1"] != DBNull.Value)
					{
						this.Group1 =(string) reader["Group1"];
					}
					if (reader["Career"] != DBNull.Value)
					{
						this.Career =(string) reader["Career"];
					}

					if (reader["Employer"] != DBNull.Value)
					{
						this.Employer =(string) reader["Employer"];
					}

					if (reader["CountryOfOrigin"] != DBNull.Value)
					{
						this.CountryOfOrigin =(string) reader["CountryOfOrigin"];
					}
					if (reader["Email"] != DBNull.Value)
					{
						this.Email =(string) reader["Email"];
					}
					if (reader["SecondaryEmail"] != DBNull.Value)
					{
						this.SecondaryEmail =(string) reader["SecondaryEmail"];
					}
					if (reader["Education"] != DBNull.Value)
					{
						this.Education =(string) reader["Education"];
					}
					if (reader["Fax"] != DBNull.Value)
					{
						this.Fax =(string) reader["Fax"];
					}
					if (reader["WPhone"] != DBNull.Value)
					{
						this.WPhone =(string) reader["WPhone"];
					}
					if (reader["WFax"] != DBNull.Value)
					{
						this.WFax =(string) reader["WFax"];
					}
					if (reader["Gender"] != DBNull.Value)
					{
						this.Gender =(string) reader["Gender"];
					}
					if (reader["HPhone"] != DBNull.Value)
					{
						this.HPhone =(string) reader["HPhone"];
					}
					if (reader["CPhone"] != DBNull.Value)
					{
						this.CPhone =(string) reader["CPhone"];
					}
					if (reader["State"] != DBNull.Value)
					{
						this.State =(string) reader["State"];
					}
					if (reader["Zip"] != DBNull.Value)
					{
						this.Zip =(string) reader["Zip"];
					}
					if (reader["Country"] != DBNull.Value)
					{
						this.Country =(string) reader["Country"];
					}
					if (reader["Address1"] != DBNull.Value)
					{
						this.Address1 =(string) reader["Address1"];
					}
					if (reader["Address2"] != DBNull.Value)
					{
						this.Address2 =(string) reader["Address2"];
					}
					if (reader["City"] != DBNull.Value)
					{
						this.City =(string) reader["City"];
					}
					if (reader["City"] != DBNull.Value)
					{
						this.City =(string) reader["City"];
					}
					if (reader["FirstName"] != DBNull.Value)
					{
						this.FirstName =(string) reader["FirstName"];
					}
					if (reader["MiddleInitial"] != DBNull.Value)
					{
						this.MiddleInitial =(string) reader["MiddleInitial"];
					}
					if (reader["LastName"] != DBNull.Value)
					{
						this.LastName =(string) reader["LastName"];
					}
					if (reader["MemberID"] != DBNull.Value)
					{
						this.MemberID =(string) reader["MemberID"];
					}
					if (reader["DonorType"] != DBNull.Value)
					{
						this.DonorType =(string) reader["DonorType"];
					}
					if (reader["Title"] != DBNull.Value)
					{
						this.Title =(string) reader["Title"];
					}
					if (reader["StateOfOrigin"] != DBNull.Value)
					{
						this.StateOfOrigin =(string) reader["StateOfOrigin"];
					}
					if (reader["PrimaryEmail_Old"] != DBNull.Value)
					{
						this.PrimaryEmail_Old =(string) reader["PrimaryEmail_Old"];
					}
					if (reader["HomePhone_Old"] != DBNull.Value)
					{
						this.HomePhone_Old =(string) reader["HomePhone_Old"];
					}
					if (reader["CellPhone_Old"] != DBNull.Value)
					{
						this.CellPhone_Old =(string) reader["CellPhone_Old"];
					}
					if (reader["Address1_Old"] != DBNull.Value)
					{
						this.Address1_Old =(string) reader["Address1_Old"];
					}
					if (reader["Address2_Old"] != DBNull.Value)
					{
						this.Address2_Old =(string) reader["Address2_Old"];
					}
					if (reader["City_old"] != DBNull.Value)
					{
						this.City_old =(string) reader["City_old"];
					}
					if (reader["State_Old"] != DBNull.Value)
					{
						this.State_Old =(string) reader["State_Old"];
					}
					if (reader["ZipCode_Old"] != DBNull.Value)
					{
						this.ZipCode_Old =(string) reader["ZipCode_Old"];
					}
					if (reader["PrimaryIndSpouseID"] != DBNull.Value)
					{
						this.PrimaryIndSpouseID =(int) reader["PrimaryIndSpouseID"];
					}
					if (reader["PrimaryContact"] != DBNull.Value)
					{
						this.PrimaryContact =(string) reader["PrimaryContact"];
					}
					if (reader["VolunteerRole2"] != DBNull.Value)
					{
						this.VolunteerRole2 =(int) reader["VolunteerRole2"];
					}
					if (reader["VolunteerRole3"] != DBNull.Value)
					{
						this.VolunteerRole3 =(int) reader["VolunteerRole3"];
					}

				}
			}
			finally
			{
				reader.Close();		
			}
			return this;
		}

		public int CheckIndSpouseDuplicateEmail(string connectionString,string email, string lastName, string address1, string phone)
		{
		
			int id=0;
			IndSpouseExtDAL objDAL = new IndSpouseExtDAL(connectionString);
			SqlDataReader reader = objDAL.CheckIndSpouseDuplicateEmail(email,lastName, address1, phone);			
			try
			{
				if (reader.Read())
				{
					id = reader.GetInt32(0);
				}
			}
			finally
			{
				reader.Close();		
			}
			return id;
		}
	}
}
