#region Contest
	//***********************************************************************************************
	//                                    R I V I S I O N   H I S T O R Y                            
	//                                    -------------------------------                            
	// Class: dbo.Contest.cs
	// Author: Venkat Ambati
	// Organization: NorthSouth.org
	// Date Created: Saturday, December 17, 2005
	// Purpose: Maintaines Contest Information 
	//***********************************************************************************************
using System;
using System.Data;
using System.Data.SqlClient;
using NorthSouth.DAL;

	/// <summary>
	/// This object represents the properties and methods of a Contest.
	/// </summary>
namespace NorthSouth.BAL
{
	public class Contest
	{
		protected int _id;
		protected int _contestTypeID;
		protected int _nSFChapterID;
		protected int _contestCategoryID;
		protected int _phase;
		protected int _groupNo;
		protected int _badgeNoBeg;
		protected int _badgeNoEnd;
		protected DateTime _contestDate;
		protected string _startTime = String.Empty;
		protected string _endTime = String.Empty;
		protected string _building = String.Empty;
		protected string _contest_Year = String.Empty;
		protected string _room = String.Empty;
		protected DateTime _registrationDeadline;
		protected string _checkinTime = String.Empty;
		
		public Contest()
		{
		}
		/// <summary>
		/// Retrieves Message Details for a given PK_Contest
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="MessageID"></param>
		/// <returns></returns>
		public Contest GetContestByID(string connectionString,int ContestID)
		{
		
			ContestDAL objDAL = new ContestDAL(connectionString);
			SqlDataReader reader = objDAL.GetContestByID(ContestID);			
			try
			{
				while (reader.Read())
				{
					_id = reader.GetInt32(0);
					if (reader["ContestTypeID"] != DBNull.Value)
					{
						this.ContestTypeID =(int) reader["ContestTypeID"];
					}
					if (reader["NSFChapterID"] != DBNull.Value)
					{
						this.NSFChapterID =(int) reader["NSFChapterID"];
					}
					if (reader["ContestCategoryID"] != DBNull.Value)
					{
						this.ContestCategoryID =(int) reader["ContestCategoryID"];
					}
					if (reader["Phase"] != DBNull.Value)
					{
						this.Phase =(int) reader["Phase"];
					}
					if (reader["GroupNo"] != DBNull.Value)
					{
						this.GroupNo =(int) reader["GroupNo"];
					}
					if (reader["BadgeNoBeg"] != DBNull.Value)
					{
						this.BadgeNoBeg =(int) reader["BadgeNoBeg"];
					}
					if (reader["BadgeNoEnd"] != DBNull.Value)
					{
						this.BadgeNoEnd =(int) reader["BadgeNoEnd"];
					}
					if (reader["ContestDate"] !=  DBNull.Value)
					{
						this.ContestDate =(DateTime) reader["ContestDate"];
					}
					if (reader["StartTime"] != DBNull.Value)
					{
						this.StartTime =(string) reader["StartTime"];
					}
					if (reader["EndTime"] != DBNull.Value)
					{
						this.EndTime =(string) reader["EndTime"];
					}
					if (reader["Building"] != DBNull.Value)
					{
						this.Building =(string) reader["Building"];
					}
					if (reader["Contest_Year"] != DBNull.Value)
					{
						this.Contest_Year =(string) reader["Contest_Year"];
					}
					if (reader["Room"] != DBNull.Value)
					{
						this.Room =(string) reader["Room"];
					}
					if (reader["RegistrationDeadline"] != DBNull.Value)
					{
						this.RegistrationDeadline =(DateTime) reader["RegistrationDeadline"];
					}
					if (reader["CheckinTime"] != DBNull.Value)
					{
						this.CheckinTime =(string) reader["CheckinTime"];
					}
				}
			}
			finally
			{
				reader.Close();		
			}
			return this;
		}
		
		/// <summary>
		/// Deletes a Contest for a given id
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="MessageID"></param>
		public void DeleteContest(string connectionString)
		{
			ContestDAL objDAL = new ContestDAL(connectionString);
			objDAL.DeleteContest(this._id);			
		}
		/// <summary>
		/// Adds New Contest Entry to Contest
		/// </summary>
		/// <param name="connectionString"></param>
		/// <returns></returns>
		public int AddContest(string connectionString)
		{
			ContestDAL objDAL = new ContestDAL(connectionString);
			return objDAL.AddContest(this.ContestTypeID, this.NSFChapterID, this.ContestCategoryID, this.Phase, this.GroupNo, this.BadgeNoBeg, this.BadgeNoEnd, this.ContestDate, this.StartTime, this.EndTime, this.Building, this.Contest_Year, this.Room, this.RegistrationDeadline, this.CheckinTime );
		}
		/// <summary>
		/// Updates given Contest Entry in Contest Table
		/// </summary>
		/// <param name="connectionString"></param>
		public void UpdateContest(string connectionString)
		{
			ContestDAL objDAL = new ContestDAL(connectionString);
			objDAL.UpdateContest(this._id, this.ContestTypeID, this.NSFChapterID, this.ContestCategoryID, this.Phase, this.GroupNo, this.BadgeNoBeg, this.BadgeNoEnd, this.ContestDate, this.StartTime, this.EndTime, this.Building, this.Contest_Year, this.Room, this.RegistrationDeadline, this.CheckinTime );
		}

		/// <summary>
		/// Retrieves All Contests in Contest Table
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="dsBlogMessages"></param>
		/// <returns></returns>
		public DataSet GetAllContests(string connectionString, DataSet ds)
		{

			ContestDAL objDAL = new ContestDAL(connectionString);
			ds = objDAL.GetAllContests(ds);
			return ds;
		}

		/// <summary>
		/// Retrieves Rows from Contests in Contest Table Where Condition
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="dsBlogMessages"></param>
		/// <returns></returns>
		public DataSet SearchContestWhere(string connectionString, DataSet ds, string WhereCondition)
		{

			ContestDAL objDAL = new ContestDAL(connectionString);
			ds = objDAL.SearchContestWhere(ds,WhereCondition);
			return ds;
		}

		
		#region Public Properties
		public int Id
		{
			get {return _id;}
			set {_id = value;}
		}
		
		public int ContestTypeID
		{
			get {return _contestTypeID;}
			set {_contestTypeID = value;}
		}

		public int NSFChapterID
		{
			get {return _nSFChapterID;}
			set {_nSFChapterID = value;}
		}

		public int ContestCategoryID
		{
			get {return _contestCategoryID;}
			set {_contestCategoryID = value;}
		}

		public int Phase
		{
			get {return _phase;}
			set {_phase = value;}
		}

		public int GroupNo
		{
			get {return _groupNo;}
			set {_groupNo = value;}
		}

		public int BadgeNoBeg
		{
			get {return _badgeNoBeg;}
			set {_badgeNoBeg = value;}
		}

		public int BadgeNoEnd
		{
			get {return _badgeNoEnd;}
			set {_badgeNoEnd = value;}
		}

		public DateTime ContestDate
		{
			get {return _contestDate;}
			set {_contestDate = value;}
		}

		public string StartTime
		{
			get {return _startTime;}
			set {_startTime = value;}
		}

		public string EndTime
		{
			get {return _endTime;}
			set {_endTime = value;}
		}

		public string Building
		{
			get {return _building;}
			set {_building = value;}
		}

		public string Contest_Year
		{
			get {return _contest_Year;}
			set {_contest_Year = value;}
		}

		public string Room
		{
			get {return _room;}
			set {_room = value;}
		}

		public DateTime RegistrationDeadline
		{
			get {return _registrationDeadline;}
			set {_registrationDeadline = value;}
		}

		public string CheckinTime
		{
			get {return _checkinTime;}
			set {_checkinTime = value;}
		}
		#endregion
	}
}
#endregion
