#region Chapter
//***********************************************************************************************
//                                    R I V I S I O N   H I S T O R Y                            
//                                    -------------------------------                            
// Class: dbo.Chapter.cs
// Author: nsf
// Organization: NorthSouth.org
// Date Created: Monday, December 25, 2006
// Purpose: chapter
//***********************************************************************************************
using System;
using System.Data;
using System.Data.SqlClient;
using NorthSouth.DAL;

/// <summary>
/// This object represents the properties and methods of a Chapter.
/// </summary>
namespace NorthSouth.BAL
{
    public class Chapter
    {
        protected int _id;
        protected string _chapterCode = String.Empty;
        protected string _name = String.Empty;
        protected string _state = String.Empty;
        protected string _city = String.Empty;
        protected string _status = String.Empty;
        protected int _zoneId;
        protected string _zoneCode = String.Empty;
        protected int _clusterId;
        protected string _clusterCode = String.Empty;
        protected DateTime _createDate;
        protected int _createdBy;
        protected DateTime _modifyDate;
        protected int _modifiedBy;

        public Chapter()
        {
        }
        /// <summary>
        /// Retrieves Message Details for a given PK_Chapter
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="MessageID"></param>
        /// <returns></returns>
        public Chapter GetChapterByID(string connectionString, int ChapterID)
        {

            ChapterDAL objDAL = new ChapterDAL(connectionString);
            SqlDataReader reader = objDAL.GetChapterByID(ChapterID);
            try
            {
                while (reader.Read())
                {
                    _id = reader.GetInt32(0);
                    this.ChapterCode = (string)reader["ChapterCode"];
                    this.Name = (string)reader["Name"];
                    this.State = (string)reader["State"];
                    this.City = (string)reader["City"];
                    this.Status = (string)reader["Status"];
                    this.ZoneId = (int)reader["ZoneId"];
                    this.ZoneCode = (string)reader["ZoneCode"];
                    this.ClusterId = (int)reader["ClusterId"];
                    this.ClusterCode = (string)reader["ClusterCode"];
                    this.CreateDate = (DateTime)reader["CreateDate"];
                    this.CreatedBy = (int)reader["CreatedBy"];
                    this.ModifyDate = (DateTime)reader["ModifyDate"];
                    this.ModifiedBy = (int)reader["ModifiedBy"];
                }
            }
            finally
            {
                reader.Close();
            }
            return this;
        }

        /// <summary>
        /// Deletes a Chapter for a given id
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="MessageID"></param>
        public void DeleteChapter(string connectionString)
        {
            ChapterDAL objDAL = new ChapterDAL(connectionString);
            objDAL.DeleteChapter(this._id);
        }
        /// <summary>
        /// Adds New Chapter Entry to Chapter
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public int AddChapter(string connectionString)
        {
            ChapterDAL objDAL = new ChapterDAL(connectionString);
            return objDAL.AddChapter(this.ChapterCode, this.Name, this.State, this.City, this.Status, this.ZoneId, this.ZoneCode, this.ClusterId, this.ClusterCode, this.CreateDate, this.CreatedBy, this.ModifyDate, this.ModifiedBy);
        }
        /// <summary>
        /// Updates given Chapter Entry in Chapter Table
        /// </summary>
        /// <param name="connectionString"></param>
        public void UpdateMessage(string connectionString)
        {
            ChapterDAL objDAL = new ChapterDAL(connectionString);
            objDAL.UpdateChapter(this._id, this.ChapterCode, this.Name, this.State, this.City, this.Status, this.ZoneId, this.ZoneCode, this.ClusterId, this.ClusterCode, this.CreateDate, this.CreatedBy, this.ModifyDate, this.ModifiedBy);
        }

        /// <summary>
        /// Retrieves All Chapters in Chapter Table
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="dsBlogMessages"></param>
        /// <returns></returns>
        public DataSet GetAllChapters(string connectionString, DataSet ds)
        {

            ChapterDAL objDAL = new ChapterDAL(connectionString);
            ds = objDAL.GetAllChapters(ds);
            return ds;
        }

        /// <summary>
        /// Retrieves Rows from Chapters in Chapter Table Where Condition
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="dsBlogMessages"></param>
        /// <returns></returns>
        public DataSet SearchChapterWhere(string connectionString, DataSet ds, string WhereCondition)
        {

            ChapterDAL objDAL = new ChapterDAL(connectionString);
            ds = objDAL.SearchChapterWhere(ds, WhereCondition);
            return ds;
        }


        #region Public Properties
        public int Id
        {
            get { return _id; }
        }

        public string ChapterCode
        {
            get { return _chapterCode; }
            set { _chapterCode = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string City
        {
            get { return _city; }
            set { _city = value; }
        }

        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int ZoneId
        {
            get { return _zoneId; }
            set { _zoneId = value; }
        }

        public string ZoneCode
        {
            get { return _zoneCode; }
            set { _zoneCode = value; }
        }

        public int ClusterId
        {
            get { return _clusterId; }
            set { _clusterId = value; }
        }

        public string ClusterCode
        {
            get { return _clusterCode; }
            set { _clusterCode = value; }
        }

        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        public int CreatedBy
        {
            get { return _createdBy; }
            set { _createdBy = value; }
        }

        public DateTime ModifyDate
        {
            get { return _modifyDate; }
            set { _modifyDate = value; }
        }

        public int ModifiedBy
        {
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        #endregion
    }
}
#endregion
