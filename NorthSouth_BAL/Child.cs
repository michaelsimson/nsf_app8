#region Child
	//***********************************************************************************************
	//                                    R I V I S I O N   H I S T O R Y                            
	//                                    -------------------------------                            
	// Class: dbo.Child.cs
	// Author: Anuradha karimikonda
	// Organization: NorthSouth.org
	// Date Created: Friday, December 09, 2005
	// Purpose: Maintains Child table
	//***********************************************************************************************
using System;
using System.Data;
using System.Data.SqlClient;
using NorthSouth.DAL;

	/// <summary>
	/// This object represents the properties and methods of a Child.
	/// </summary>
namespace NorthSouth.BAL
{
	public class Child
	{
		protected int _id;
		protected int _mEMBERID;
		protected int _sPOUSEID;
		protected string _gENDER = String.Empty;
		protected string _fIRST_NAME = String.Empty;
		protected string _mIDDLE_INITIAL = String.Empty;
		protected string _lAST_NAME = String.Empty;
		protected DateTime _dATE_OF_BIRTH;
		protected int _gRADE;
		protected string _schoolName = String.Empty;
		protected string _countryOfBirth = String.Empty;
		protected string _acheivements = String.Empty;
		protected string _hobbies = String.Empty;
		protected DateTime _createDate;
		protected DateTime _modifyDate;
		protected string _deleteReason = String.Empty;
		protected string _deletedFlag = String.Empty;
		
		public Child()
		{
		}
		/// <summary>
		/// Retrieves Message Details for a given PK_Child
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="MessageID"></param>
		/// <returns></returns>
		public Child GetChildByID(string connectionString,int ChildNumber, int ParentID)
		{
		
			ChildDAL objDAL = new ChildDAL(connectionString);
			SqlDataReader reader = objDAL.GetChildByID(ChildNumber, ParentID);			
			try
			{
				while (reader.Read())
				{
					_id = reader.GetInt32(0);
					if (reader["MEMBERID"] != DBNull.Value)
					{
						this.MEMBERID = (int) reader["MEMBERID"];
					}
					if (reader["SPOUSEID"] != DBNull.Value)
					{
						this.SPOUSEID = (int) reader["SPOUSEID"];
					}
					if (reader["GENDER"] != DBNull.Value)
					{
						this.GENDER = (string) reader["GENDER"];
					}
					if (reader["FIRST_NAME"] != DBNull.Value)
					{
						this.FIRST_NAME = (string) reader["FIRST_NAME"];
					}
					if (reader["MIDDLE_INITIAL"] != DBNull.Value)
					{
						this.MIDDLE_INITIAL = (string) reader["MIDDLE_INITIAL"];
					}
					if (reader["LAST_NAME"] != DBNull.Value)
					{
						this.LAST_NAME = (string) reader["LAST_NAME"];
					}
					if (reader["DATE_OF_BIRTH"] != DBNull.Value)
					{
						this.DATE_OF_BIRTH = (DateTime) reader["DATE_OF_BIRTH"];
					}
					if (reader["GRADE"] != DBNull.Value)
					{
						this.GRADE = (int) reader["GRADE"];
					}
					if (reader["SchoolName"] != DBNull.Value)
					{
						this.SchoolName = (string) reader["SchoolName"];
					}
					if (reader["CountryOfBirth"] != DBNull.Value)
					{
						this.CountryOfBirth = (string) reader["CountryOfBirth"];
					}
					if (reader["Acheivements"] != DBNull.Value)
					{
						this.Acheivements = (string) reader["Acheivements"];
					}
					if (reader["Hobbies"] != DBNull.Value)
					{
						this.Hobbies = (string) reader["Hobbies"];
					}
					if (reader["CreateDate"] != DBNull.Value)
					{
						this.CreateDate = (DateTime) reader["CreateDate"];
					}
					if (reader["ModifyDate"] != DBNull.Value)
					{
						this.ModifyDate = (DateTime) reader["ModifyDate"];
					}
					if (reader["DeleteReason"] != DBNull.Value)
					{
						this.DeleteReason = (string) reader["DeleteReason"];
					}
					if (reader["DeleteReason"] != DBNull.Value)
					{
						this.DeletedFlag = (string) reader["DeletedFlag"];
					}
				}
			}
			finally
			{
				reader.Close();		
			}
			return this;
		}
		
		/// <summary>
		/// Deletes a Child for a given id
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="MessageID"></param>
		public void DeleteChild(string connectionString)
		{
			ChildDAL objDAL = new ChildDAL(connectionString);
			objDAL.DeleteChild(this._id, this.MEMBERID);			
		}
		/// <summary>
		/// Adds New Child Entry to Child
		/// </summary>
		/// <param name="connectionString"></param>
		/// <returns></returns>
		public int AddChild(string connectionString)
		{
			ChildDAL objDAL = new ChildDAL(connectionString);
			return objDAL.AddChild(this.MEMBERID, this.SPOUSEID, this.GENDER, this.FIRST_NAME, this.MIDDLE_INITIAL, this.LAST_NAME, this.DATE_OF_BIRTH, this.GRADE, this.SchoolName, this.CountryOfBirth, this.Acheivements, this.Hobbies, this.CreateDate, this.ModifyDate, this.DeleteReason, this.DeletedFlag );
		}
		/// <summary>
		/// Updates given Child Entry in Child Table
		/// </summary>
		/// <param name="connectionString"></param>
		public void UpdateChild(string connectionString)
		{
			ChildDAL objDAL = new ChildDAL(connectionString);
			objDAL.UpdateChild(this._id, this.MEMBERID, this.SPOUSEID, this.GENDER, this.FIRST_NAME, this.MIDDLE_INITIAL, this.LAST_NAME, this.DATE_OF_BIRTH, this.GRADE, this.SchoolName, this.CountryOfBirth, this.Acheivements, this.Hobbies, this.CreateDate, this.ModifyDate, this.DeleteReason, this.DeletedFlag );
		}

		/// <summary>
		/// Retrieves All Childs in Child Table
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="dsBlogMessages"></param>
		/// <returns></returns>
		public DataSet GetAllChilds(string connectionString, DataSet ds)
		{

			ChildDAL objDAL = new ChildDAL(connectionString);
			ds = objDAL.GetChildAll(ds);
			return ds;
		}

		/// <summary>
		/// Retrieves Rows from Childs in Child Table Where Condition
		/// </summary>
		/// <param name="connectionString"></param>
		/// <param name="dsBlogMessages"></param>
		/// <returns></returns>
		public DataSet SearchChildWhere(string connectionString, DataSet ds, string WhereCondition)
		{

			ChildDAL objDAL = new ChildDAL(connectionString);
			ds = objDAL.SearchChildWhere(ds,WhereCondition);
			return ds;
		}

		
		#region Public Properties
		public int Id
		{
			get {return _id;}
		}
		
		public int MEMBERID
		{
			get {return _mEMBERID;}
			set {_mEMBERID = value;}
		}

		public int SPOUSEID
		{
			get {return _sPOUSEID;}
			set {_sPOUSEID = value;}
		}

		public string GENDER
		{
			get {return _gENDER;}
			set {_gENDER = value;}
		}

		public string FIRST_NAME
		{
			get {return _fIRST_NAME;}
			set {_fIRST_NAME = value;}
		}

		public string MIDDLE_INITIAL
		{
			get {return _mIDDLE_INITIAL;}
			set {_mIDDLE_INITIAL = value;}
		}

		public string LAST_NAME
		{
			get {return _lAST_NAME;}
			set {_lAST_NAME = value;}
		}

		public DateTime DATE_OF_BIRTH
		{
			get {return _dATE_OF_BIRTH;}
			set {_dATE_OF_BIRTH = value;}
		}

		public int GRADE
		{
			get {return _gRADE;}
			set {_gRADE = value;}
		}

		public string SchoolName
		{
			get {return _schoolName;}
			set {_schoolName = value;}
		}

		public string CountryOfBirth
		{
			get {return _countryOfBirth;}
			set {_countryOfBirth = value;}
		}

		public string Acheivements
		{
			get {return _acheivements;}
			set {_acheivements = value;}
		}

		public string Hobbies
		{
			get {return _hobbies;}
			set {_hobbies = value;}
		}

		public DateTime CreateDate
		{
			get {return _createDate;}
			set {_createDate = value;}
		}

		public DateTime ModifyDate
		{
			get {return _modifyDate;}
			set {_modifyDate = value;}
		}

		public string DeleteReason
		{
			get {return _deleteReason;}
			set {_deleteReason = value;}
		}

		public string DeletedFlag
		{
			get {return _deletedFlag;}
			set {_deletedFlag = value;}
		}
		#endregion
	}
}
#endregion
