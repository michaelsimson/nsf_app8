<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" CodeFile="ScheduleTechCoordinator.aspx.vb" Inherits="ScheduleTechCoordinator" Title="NSF - Schedule Technical Coordinator" %>
<asp:Content ID="cntScheduleTechCoordinators" ContentPlaceHolderID="Content_main" Runat="Server">
     <div align="left" >
<asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
<table border=0  width=100%>
    <tr>
        <td align="center">
               <div class="Heading"  >
                    <h2>Schedule Technical Coordinators - <asp:Label ID="lblChapter" runat="server" Text="" /></h2>
                </div>
             <asp:Label ID="lblReportError" runat="server" ForeColor="red" ></asp:Label><br />
                 
        </td>
    </tr>
    <tr>
        <td align="center">
               
             <asp:Label ID="Label2" runat="server" Text="Select Year"></asp:Label>: 
            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
            </asp:DropDownList>
                 
        </td>
    </tr>
    <tr>
        <td>
               <asp:DataGrid ID="grdContests" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="contestid"
			    Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
			    BorderColor="#336666" AllowPaging=true PageSize=10  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
			    <AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
			    <ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>  
			    <HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
			    <FooterStyle BackColor="Gainsboro" ></FooterStyle>
			    <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                        <Columns>
                            <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                            <asp:BoundColumn DataField="ContestDate" HeaderText="ContestDate" DataFormatString="{0:MM/dd/yyyy}" readonly=true  Visible="true" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="ContestName" headerText="ContestName" ReadOnly=true Visible="true" ></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Ph1 Rooms" ItemStyle-Width="10%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblRooms1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Ph1Rooms")%>'></asp:Label>
                                    <asp:HiddenField ID="hfRooms1" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.Ph1Rooms")%>' />
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlRooms1" runat="server" onprerender="ddlRooms_PreRender1" >
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                    </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                            <asp:TemplateColumn HeaderText="Ph2 Rooms" ItemStyle-Width="10%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblRooms" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Ph2Rooms")%>'></asp:Label>
                                    <asp:HiddenField ID="hfRooms" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.Ph2Rooms")%>' />
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlRooms" runat="server" onprerender="ddlRooms_PreRender">
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                    </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                            <asp:TemplateColumn HeaderText="Tech Coordinator" ItemStyle-Width="17%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblExamReceiver" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ExamReceiver")%>'></asp:Label>
                                    <asp:HiddenField ID="hfExamRecID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ExamRecID")%>' />
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlExamReceiver" runat="server" DataTextField="FullName" DataValueField="memberid" onprerender="ExamReceiver_PreRender">
                                    </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                            <asp:TemplateColumn HeaderText="Left Signature" ItemStyle-Width="15%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblLeftSignature" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LeftSignature")%>'></asp:Label>
                                    <asp:HiddenField ID="hfLeftSignID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.LeftSignID")%>' />
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlLeftSignature" runat="server" DataTextField="FullName" DataValueField="memberid" onprerender="LeftSign_PreRender">
                                    </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                            <asp:TemplateColumn HeaderText="Left Title" ItemStyle-Width="15%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblLeftTitle" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LeftTitle")%>'></asp:Label>
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlLeftTitle" OnPreRender="ddlLeftTitle_PreRender"  runat="server">
                                        <asp:ListItem Value="" Text="Please Select"></asp:ListItem> 
                                          <asp:ListItem Value="Chapter Coordinator" Text="Chapter Coordinator"></asp:ListItem>  
                                            <asp:ListItem Value="Chief Judge" Text="Chief Judge"></asp:ListItem>
  			<asp:ListItem Value="Technical Coordinator" Text="Technical Coordinator"></asp:ListItem>
  			<asp:ListItem Value="Tech Coordinator, Overall" Text="Tech Coordinator, Overall"></asp:ListItem>
                                            <asp:ListItem Value="National Coordinator" Text="National Coordinator"></asp:ListItem>  
                                            <asp:ListItem Value="Spelling Coordinator" Text="Spelling Coordinator"></asp:ListItem>
                                            <asp:ListItem Value="Vocabulary Coordinator" Text="Vocabulary Coordinator"></asp:ListItem>
			<asp:ListItem Value="Math Coordinator" Text="Math Coordinator"></asp:ListItem> 
 			<asp:ListItem Value="Geography Coordinator" Text="Geography Coordinator"></asp:ListItem> 
			<asp:ListItem Value="Essay Writing Coordinator" Text="Essay Writing Coordinator"></asp:ListItem> 
			<asp:ListItem Value="Public Speaking Coordinator" Text="Public Speaking Coordinator"></asp:ListItem>
			<asp:ListItem Value="Brain Bee Coordinator" Text="Brain Bee Coordinator"></asp:ListItem>
            <asp:ListItem Value="Science Coordinator" Text="Science Coordinator"></asp:ListItem>

                                        </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                            <asp:TemplateColumn HeaderText="Right Signature" ItemStyle-Width="15%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblRightSignature" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RightSignature")%>'></asp:Label>
                                    <asp:HiddenField ID="hfRightSignID" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.RightSignID")%>' />
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlRightSignature" runat="server" DataTextField="FullName" DataValueField="memberid" onprerender="RightSign_PreRender">
                                    </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                            <asp:TemplateColumn HeaderText="Right Title" ItemStyle-Width="15%">
                                  <ItemTemplate >
                                    <asp:Label ID="lblRightTitle" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RightTitle")%>'></asp:Label>
                                  </ItemTemplate>  
                                   <EditItemTemplate>
                                    <asp:DropDownList id="ddlRightTitle" runat="server"  OnPreRender="ddlRightTitle_PreRender" >
                                           <asp:ListItem Value="" Text="Please Select"></asp:ListItem> 
                                           <asp:ListItem Value="Chapter Coordinator" Text="Chapter Coordinator"></asp:ListItem>  
                                        <asp:ListItem Value="Chief Judge" Text="Chief Judge"></asp:ListItem>
                                        <asp:ListItem Value="National Convener" Text="National Convener"></asp:ListItem>
                                        <asp:ListItem Value="National Coordinator" Text="National Coordinator"></asp:ListItem>
                                        <asp:ListItem Value="Technical Coordinator" Text="Technical Coordinator"></asp:ListItem>
                                        <asp:ListItem Value="Tech Coordinator, Overall" Text="Tech Coordinator, Overall"></asp:ListItem>
                                           </asp:DropDownList> 
                                  </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                     </Columns>
		    </asp:DataGrid>
        </td>
     </tr>
</table>
</asp:Content>

 
 
 