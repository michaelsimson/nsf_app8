<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.MainChild" CodeFile="MainChild.aspx.vb" MasterPageFile="~/NSFMasterPage.master"  %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
            <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
				<tr bgcolor="#FFFFFF" >
					<td class="title02" colspan="2">Child Information</td>
				</tr>
				
				<tr bgcolor="#FFFFFF" >
					<td align="right" colspan="2"><asp:hyperlink  id="lnkAdd"  CssClass="btn_02"  runat="server" Font-Bold="True" NavigateUrl="~/AddChild.aspx" Text="Add Child"></asp:hyperlink >&nbsp;&nbsp;&nbsp;
					<asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/donorfunctions.aspx" >Back to Main Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;
					<asp:hyperlink id="hlnkSearch" runat="server"  CssClass="btn_02" NavigateUrl="~/dbsearchresults.aspx" >Back to Search Results</asp:hyperlink>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF" >
					<td class="forms-total" valign="top" colspan="2"><b>We must have latest information on 
							your child(ren) including date of birth, place of birth, gender, grade, school, hobbies and achievements. So please click 
							on&nbsp;Update&nbsp;link to verify and update information. Otherwise you may not be able 
							to select Contests. Further, your child may get disqualified if the information is not accurate. The grade should be as of January 1st of the contest year.</b></td>
				</tr>
				<tr bgcolor="#FFFFFF" >
					<td colspan="2"><asp:datagrid id="dgChildList" runat="server" Width="100%" CssClass="GridStyle" AllowSorting="false"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="ChildNumber">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" SortExpression="FIRST_NAME">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FIRST_NAME") %>' CssClass="SmallFont">
										</asp:Label>&nbsp;
										<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MIDDLE_INITIAL") %>' CssClass="SmallFont"></asp:Label>
										&nbsp;
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LAST_NAME") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="School Name" SortExpression="SchoolName">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SchoolName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Grade" SortExpression="GRADE">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GRADE") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Date Of Birth" SortExpression="DOB">
									<ItemTemplate>
										<asp:Label id="lblDOB" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Date_of_Birth").ToShortDateString() %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Update">
									<ItemTemplate>
										<asp:HyperLink id="hlUpdateChild" CssClass="btn_02" runat="server" Text='Update'></asp:HyperLink>&nbsp;
										<asp:Label id="alertUpdate" runat="server" Text='(Needs Update)' CssClass="SmallFont" Font-Bold="True"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
						</asp:datagrid>						
					</td>
				</tr>
				
				<tr bgcolor="#FFFFFF" >
					<td class="ItemCenter" align="center" colspan="2"><asp:label id="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red" Font-Size="Medium"></asp:label><br>
					<asp:Button runat="server" Text="Choose Contest Details" ID="btnYes" CssClass="FormButton" Visible =false  />
					</td>
				</tr>
				<tr bgcolor="#FFFFFF" >
					<td class="SmallFont" align="justify" colspan="2">
					Please update the Child information for all the children, even if you are not planning to send all of them to NSF regional contests.
Only when you update the grade, school and other data for all the children, then only the �Choose Contest Details� button will be active.
If you update one child and not the other, the �Choose Contest Details� button will NOT be active.
					</td>
				</tr>
				
 				<tr bgcolor="#FFFFFF" >
					<td >
					    <asp:Label ID="lblDebug" runat="server" Visible="false"></asp:Label></td>
				</tr> 
	            </table>
</asp:content>

 

 
 
 