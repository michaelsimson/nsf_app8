﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class volAssessmentForm
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If Session("RoleID") = 88 Or Session("RoleID") = 89 Then
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                    'more than one 
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                    Dim i As Integer
                    Dim prd As String = String.Empty
                    Dim Prdgrp As String = String.Empty
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If prd.Length = 0 Then
                            prd = ds.Tables(0).Rows(i)(1).ToString()
                        Else
                            prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                        End If

                        If Prdgrp.Length = 0 Then
                            Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                        Else
                            Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                        End If
                    Next
                    lblPrd.Text = prd
                    lblPrdGrp.Text = Prdgrp
                    LoadProductGroup()
                    LoadProductID()
                Else
                    'only one
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                    Dim prd As String = String.Empty
                    Dim Prdgrp As String = String.Empty
                    If ds.Tables(0).Rows.Count > 0 Then
                        prd = ds.Tables(0).Rows(0)(1).ToString()
                        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                    End If
                    LoadProductGroup()
                    LoadProductID()
                End If
            Else
                LoadProductGroup()
                LoadProductID()
            End If
            '   ShowExisting()
            'pnl.visible = false
        End If
    End Sub

    Private Sub LoadProductGroup()
        Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup where EventId=13" & IIf(lblPrdGrp.Text.Length > 0, " AND ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & " order by ProductGroupID"
        Dim dsproductgroup As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsproductgroup = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        If dsproductgroup.Tables(0).Rows.Count > 0 Then
            ddlProductGroup.DataSource = dsproductgroup
            ddlProductGroup.DataBind()
        End If
        If dsproductgroup.Tables(0).Rows.Count > 1 Then
            ddlProductGroup.Items.Insert(0, "Select Product Group")
            ddlProductGroup.Items(0).Selected = True
            ddlProductGroup.Enabled = True
        ElseIf dsproductgroup.Tables(0).Rows.Count = 1 Then
            ddlProductGroup.Enabled = False
            LoadProductID()
        End If

    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))

        Dim strSql As String
        Try
            strSql = "Select ProductID, Name as ProductName from Product where EventID in (13) " & IIf(lblPrd.Text.Length > 0, " AND ProductID IN (" & lblPrd.Text & ")", "") & " "

            If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                strSql = strSql & " and ProductGroupID =" & ddlProductGroup.SelectedValue & ""
            End If
            strSql = strSql & "  order by ProductID"
            Dim dsproductid As DataSet
            dsproductid = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            If dsproductid.Tables(0).Rows.Count > 0 Then
                ddlProduct.DataSource = dsproductid
                ddlProduct.DataBind()
            End If
            If dsproductid.Tables(0).Rows.Count > 1 Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
                ddlProduct.Enabled = True
            ElseIf dsproductid.Tables(0).Rows.Count = 1 Then
                ddlProduct.Enabled = False
            End If

        Catch ex As Exception
            lblerr.Text = strSql
            lblerr.Text = lblerr.Text & ex.ToString()
        End Try

    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadProductID()
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlProduct.Items.Count < 1 Then
            lblerr.Text = "Please select Product Group and Product"
            Exit Sub
        ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
            lblerr.Text = "Please select Product"
            Exit Sub
        End If
        btnExport.Visible = False
        setgradefrom(0)
        setgradeto(0)
        lblProductID.Text = ddlProduct.SelectedValue
        ShowPrIDExisting()
        CheckData()
        ddlProduct.Enabled = False
        ddlProductGroup.Enabled = False
        btnContinue.Enabled = False
        divResult.Visible = False
    End Sub

    Private Sub ShowExisting()
        lblEditResult.Text = "E"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT AE.AssessExamID, AE.StartDate, AE.EndDate, AE.GradeFrom, AE.GradeTo, P.Name,0 as Total  From AssessExam AE INNER JOIN Product P ON AE.ProductID = P.ProductId WHERE YEAR(AE.EndDate) >= YEAR(GETDATE())" & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", ""))
        GdAssessExam.DataSource = ds
        GdAssessExam.DataBind()
        If GdAssessExam.Rows.Count > 0 Then
            GdAssessExam.Visible = True
            GdAssessExam.Columns(6).Visible = False
            lblHeader.Text = "Please select AssessExamID to Edit"
        Else
            GdAssessExam.Visible = False
            lblHeader.Text = ""
            CheckData()
        End If
    End Sub

    Private Sub ShowPrIDExisting()
        lblEditResult.Text = "E"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT AE.AssessExamID, AE.StartDate, AE.EndDate, AE.GradeFrom, AE.GradeTo, P.Name,0 as Total  From AssessExam AE INNER JOIN Product P ON AE.ProductID = P.ProductId WHERE AE.ProductID=" & ddlProduct.SelectedValue & " AND YEAR(AE.EndDate) >= YEAR(GETDATE())")
        GdAssessExam.DataSource = ds
        GdAssessExam.DataBind()
        If GdAssessExam.Rows.Count > 0 Then
            GdAssessExam.Columns(6).Visible = False
            lblHeader.Text = "Please select AssessExamID to Edit"
            GdAssessExam.Visible = True
        Else
            GdAssessExam.Visible = False
            lblHeader.Text = ""
        End If
    End Sub

    Protected Sub GdAssessExam_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim Readr As SqlDataReader
        If lblEditResult.Text = "E" Then
            Readr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from AssessExam where AssessExamID = " & GdAssessExam.DataKeys(index).Value & "")
            While Readr.Read()
                txtStartDate.Text = Date.Parse(Readr("StartDate").ToString())
                txtEndDate.Text = Date.Parse(Readr("EndDate").ToString())
                If Date.Parse(Readr("StartDate").ToString()) < Now.Date Then
                    setgradefrom(Readr("Gradefrom"))
                    setgradeto(Readr("Gradeto"))
                Else
                    setgradefrom(0)
                    setgradeto(0)
                    ddlGradeFrom.SelectedIndex = ddlGradeFrom.Items.IndexOf(ddlGradeFrom.Items.FindByText(Readr("Gradefrom")))
                    ddlGradeTo.SelectedIndex = ddlGradeTo.Items.IndexOf(ddlGradeTo.Items.FindByText(Readr("Gradeto")))
                End If
                txtQ1.Text = Readr("Q1")
                txtQ2.Text = Readr("Q2")
                txtQ3.Text = Readr("Q3")
                txtQ4.Text = Readr("Q4")
                txtQ5.Text = Readr("Q5")
                txtQ6.Text = Readr("Q6")
                txtQ7.Text = Readr("Q7")
                txtQ8.Text = Readr("Q8")
                txtQ9.Text = Readr("Q9")
                txtQ10.Text = Readr("Q10")
                lblAssessExamID.Text = Readr("AssessExamID")
                lblProductID.Text = Readr("ProductID")
                btnSubmit.Text = "Modifiy"
            End While
            DivID2.Visible = True
        Else
            lblerr.Text = ""
            Dim ds As DataSet
            Dim reader As SqlDataReader
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select CAST(AR.Grade as Varchar) as Grade,CASE WHEN CHARINDEX(AE.Q1+' ',AR.Q1+' ')>0 THEN 1 ELSE 0 END AS Q1,CASE WHEN CHARINDEX(AE.Q2+' ',AR.Q2+' ')>0 THEN 1 ELSE 0 END AS Q2,CASE WHEN CHARINDEX(AE.Q3+' ',AR.Q3+' ')>0 THEN 1 ELSE 0 END AS Q3,CASE WHEN CHARINDEX(AE.Q4+' ',AR.Q4+' ')>0 THEN 1 ELSE 0 END AS Q4,CASE WHEN CHARINDEX(AE.Q5+' ',AR.Q5+' ')>0 THEN 1 ELSE 0 END AS Q5,CASE WHEN CHARINDEX(AE.Q6+' ',AR.Q6+' ')>0 THEN 1 ELSE 0 END AS Q6,CASE WHEN CHARINDEX(AE.Q7+' ',AR.Q7+' ')>0 THEN 1 ELSE 0 END AS Q7,CASE WHEN CHARINDEX(AE.Q8+' ',AR.Q8+' ')>0 THEN 1 ELSE 0 END AS Q8,CASE WHEN CHARINDEX(AE.Q9+' ',AR.Q9+' ')>0 THEN 1 ELSE 0 END AS Q9,CASE WHEN CHARINDEX(AE.Q10+' ',AR.Q10+' ')>0 THEN 1 ELSE 0 END AS Q10 INTO #tmpAssessResult FROM AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID AND AE.AssessExamID=" & GdAssessExam.DataKeys(index).Value & ";select Grade,SUM(Q1+Q2+Q3+Q4+Q5+Q6+Q7+Q8+Q9+Q10+0.00)/COUNT(Grade) as Total,COUNT(Grade) as Participants from #tmpAssessResult GROUP BY Grade;Drop TABLE #tmpAssessResult")
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select AR.ProductID,CASE WHEN CHARINDEX(AE.Q1+' ',AR.Q1+' ')>0 THEN 1 ELSE 0 END AS Q1,CASE WHEN CHARINDEX(AE.Q2+' ',AR.Q2+' ')>0 THEN 1 ELSE 0 END AS Q2,CASE WHEN CHARINDEX(AE.Q3+' ',AR.Q3+' ')>0 THEN 1 ELSE 0 END AS Q3,CASE WHEN CHARINDEX(AE.Q4+' ',AR.Q4+' ')>0 THEN 1 ELSE 0 END AS Q4,CASE WHEN CHARINDEX(AE.Q5+' ',AR.Q5+' ')>0 THEN 1 ELSE 0 END AS Q5,CASE WHEN CHARINDEX(AE.Q6+' ',AR.Q6+' ')>0 THEN 1 ELSE 0 END AS Q6,CASE WHEN CHARINDEX(AE.Q7+' ',AR.Q7+' ')>0 THEN 1 ELSE 0 END AS Q7,CASE WHEN CHARINDEX(AE.Q8+' ',AR.Q8+' ')>0 THEN 1 ELSE 0 END AS Q8,CASE WHEN CHARINDEX(AE.Q9+' ',AR.Q9+' ')>0 THEN 1 ELSE 0 END AS Q9,CASE WHEN CHARINDEX(AE.Q10+' ',AR.Q10+' ')>0 THEN 1 ELSE 0 END AS Q10 INTO #tmpAssessResult FROM AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID AND AE.AssessExamID=" & GdAssessExam.DataKeys(index).Value & ";select 0 as Grade,SUM(Q1+Q2+Q3+Q4+Q5+Q6+Q7+Q8+Q9+Q10+0.00)/COUNT(ProductID) as Total,COUNT(ProductID) as Participants from #tmpAssessResult GROUP BY ProductID;Drop TABLE #tmpAssessResult")
            While reader.Read()
                Dim dr As DataRow = ds.Tables(0).NewRow
                dr(0) = "All" 'DBNull.Value 'reader("grade")
                dr(1) = reader("total")
                dr(2) = reader("Participants")
                ds.Tables(0).Rows.Add(dr)
            End While
            reader.Close()
            GridOverAll.DataSource = ds
            GridOverAll.DataBind()
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select CAST(AR.Grade as Varchar) as Grade,CASE WHEN CHARINDEX(AE.Q1+' ',AR.Q1+' ')>0 THEN 1 ELSE 0 END AS Q1,CASE WHEN CHARINDEX(AE.Q2+' ',AR.Q2+' ')>0 THEN 1 ELSE 0 END AS Q2,CASE WHEN CHARINDEX(AE.Q3+' ',AR.Q3+' ')>0 THEN 1 ELSE 0 END AS Q3,CASE WHEN CHARINDEX(AE.Q4+' ',AR.Q4+' ')>0 THEN 1 ELSE 0 END AS Q4,CASE WHEN CHARINDEX(AE.Q5+' ',AR.Q5+' ')>0 THEN 1 ELSE 0 END AS Q5,CASE WHEN CHARINDEX(AE.Q6+' ',AR.Q6+' ')>0 THEN 1 ELSE 0 END AS Q6,CASE WHEN CHARINDEX(AE.Q7+' ',AR.Q7+' ')>0 THEN 1 ELSE 0 END AS Q7,CASE WHEN CHARINDEX(AE.Q8+' ',AR.Q8+' ')>0 THEN 1 ELSE 0 END AS Q8,CASE WHEN CHARINDEX(AE.Q9+' ',AR.Q9+' ')>0 THEN 1 ELSE 0 END AS Q9,CASE WHEN CHARINDEX(AE.Q10+' ',AR.Q10+' ')>0 THEN 1 ELSE 0 END AS Q10 INTO #tmpAssessResult FROM AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID AND AE.AssessExamID=" & GdAssessExam.DataKeys(index).Value & ";select Grade,(SUM(Q1+0.0)/COUNT(Grade))*100 AS Q1,(SUM(Q2+0.0)/COUNT(Grade))*100 AS Q2,(SUM(Q3+0.0)/COUNT(Grade))*100 AS Q3,(SUM(Q4+0.0)/COUNT(Grade))*100 AS Q4,(SUM(Q5+0.0)/COUNT(Grade))*100 AS Q5,(SUM(Q6+0.0)/COUNT(Grade))*100 AS Q6,(SUM(Q7+0.0)/COUNT(Grade))*100 AS Q7,(SUM(Q8+0.0)/COUNT(Grade))*100 AS Q8,(SUM(Q9+0.0)/COUNT(Grade))*100 AS Q9,(SUM(Q10+0.0)/COUNT(Grade))*100 AS Q10,COUNT(Grade) as Participants from #tmpAssessResult GROUP BY Grade;Drop TABLE #tmpAssessResult")
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select AR.ProductID,CASE WHEN CHARINDEX(AE.Q1+' ',AR.Q1+' ')>0 THEN 1 ELSE 0 END AS Q1,CASE WHEN CHARINDEX(AE.Q2+' ',AR.Q2+' ')>0 THEN 1 ELSE 0 END AS Q2,CASE WHEN CHARINDEX(AE.Q3+' ',AR.Q3+' ')>0 THEN 1 ELSE 0 END AS Q3,CASE WHEN CHARINDEX(AE.Q4+' ',AR.Q4+' ')>0 THEN 1 ELSE 0 END AS Q4,CASE WHEN CHARINDEX(AE.Q5+' ',AR.Q5+' ')>0 THEN 1 ELSE 0 END AS Q5,CASE WHEN CHARINDEX(AE.Q6+' ',AR.Q6+' ')>0 THEN 1 ELSE 0 END AS Q6,CASE WHEN CHARINDEX(AE.Q7+' ',AR.Q7+' ')>0 THEN 1 ELSE 0 END AS Q7,CASE WHEN CHARINDEX(AE.Q8+' ',AR.Q8+' ')>0 THEN 1 ELSE 0 END AS Q8,CASE WHEN CHARINDEX(AE.Q9+' ',AR.Q9+' ')>0 THEN 1 ELSE 0 END AS Q9,CASE WHEN CHARINDEX(AE.Q10+' ',AR.Q10+' ')>0 THEN 1 ELSE 0 END AS Q10 INTO #tmpAssessResult FROM AssessExamResp AR INNER JOIN AssessExam AE ON AR.AssessExamID = AE.AssessExamID AND AE.AssessExamID=" & GdAssessExam.DataKeys(index).Value & ";select 0 as Grade,(SUM(Q1+0.0)/COUNT(ProductID))*100 AS Q1,(SUM(Q2+0.0)/COUNT(ProductID))*100 AS Q2,(SUM(Q3+0.0)/COUNT(ProductID))*100 AS Q3,(SUM(Q4+0.0)/COUNT(ProductID))*100 AS Q4,(SUM(Q5+0.0)/COUNT(ProductID))*100 AS Q5,(SUM(Q6+0.0)/COUNT(ProductID))*100 AS Q6,(SUM(Q7+0.0)/COUNT(ProductID))*100 AS Q7,(SUM(Q8+0.0)/COUNT(ProductID))*100 AS Q8,(SUM(Q9+0.0)/COUNT(ProductID))*100 AS Q9,(SUM(Q10+0.0)/COUNT(ProductID))*100 AS Q10,COUNT(ProductID) as Participants from #tmpAssessResult GROUP BY ProductID;Drop TABLE #tmpAssessResult")
            While reader.Read()
                Dim dr As DataRow = ds.Tables(0).NewRow
                dr(0) = "All" 'DBNull.Value 'reader("grade")
                dr(1) = reader("Q1")
                dr(2) = reader("Q2")
                dr(3) = reader("Q3")
                dr(4) = reader("Q4")
                dr(5) = reader("Q5")
                dr(6) = reader("Q6")
                dr(7) = reader("Q7")
                dr(8) = reader("Q8")
                dr(9) = reader("Q9")
                dr(10) = reader("Q10")
                dr(11) = reader("Participants")
                ds.Tables(0).Rows.Add(dr)
            End While
            reader.Close()
            GridByQ.DataSource = ds
            GridByQ.DataBind()
            btnExport.Visible = True
            divResult.Visible = True
        End If

    End Sub

    Private Sub setgradefrom(ByVal grade As Integer)
        ddlGradeFrom.Items.Clear()
        grade = IIf(grade = 0, 12, grade)
        Dim li As ListItem
        Dim i As Integer
        For i = 1 To grade
            li = New ListItem(i)
            ddlGradeFrom.Items.Add(li)
        Next
        If Not grade = 12 Then
            ddlGradeFrom.SelectedIndex = ddlGradeFrom.Items.IndexOf(ddlGradeFrom.Items.FindByText(grade.ToString()))
        End If
    End Sub

    Private Sub setgradeto(ByVal grade As Integer)
        ddlGradeTo.Items.Clear()
        grade = IIf(grade = 0, 1, grade)
        Dim li As ListItem
        Dim i As Integer
        For i = grade To 12
            li = New ListItem(i)
            ddlGradeTo.Items.Add(li)
        Next
        ddlGradeTo.SelectedIndex = ddlGradeTo.Items.IndexOf(ddlGradeTo.Items.FindByText(grade.ToString()))
    End Sub

    Function CheckData() As Boolean
        cleardiv2()
        Dim flag As Boolean = True
        If ddlProduct.Items.Count = 0 Then
            Return flag
            Exit Function
        End If
        Try
            lblCount.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(AssessExamID) from  AssessExam where  ProductID = " & ddlProduct.SelectedValue & " AND DATEDIFF(d,StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),EndDate)>=0 ")
            If CInt(lblCount.Text) > 0 Then
                'Dim Readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from AssessExam where ProductID = " & ddlProduct.SelectedValue & " AND DATEDIFF(d,StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),EndDate)>=0")
                'While Readr.Read()
                '    txtStartDate.Text = Date.Parse(Readr("StartDate").ToString())
                '    txtEndDate.Text = Date.Parse(Readr("EndDate").ToString())
                '    setgradefrom(Readr("Gradefrom"))
                '    setgradeto(Readr("Gradeto"))
                '    txtQ1.Text = Readr("Q1")
                '    txtQ2.Text = Readr("Q2")
                '    txtQ3.Text = Readr("Q3")
                '    txtQ4.Text = Readr("Q4")
                '    txtQ5.Text = Readr("Q5")
                '    txtQ6.Text = Readr("Q6")
                '    txtQ7.Text = Readr("Q7")
                '    txtQ8.Text = Readr("Q8")
                '    txtQ9.Text = Readr("Q9")
                '    txtQ10.Text = Readr("Q10")
                '    lblAssessExamID.Text = Readr("AssessExamID")
                '    btnSubmit.Text = "Modifiy"
                '    flag = True
                'End While
                lblerr.Text = "Exam is open for same Product Already. So Start date must be less than Previous Enddate."
            Else
                flag = True
            End If
        Catch ex As Exception
            lblerr.Text = ex.ToString()
        End Try
        DivID2.Visible = True
        Return flag
    End Function

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim enddate As String = ""
        If lblCount.Text = "" Then
            lblCount.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(AssessExamID) from  AssessExam where  ProductID = " & lblProductID.Text & " AND DATEDIFF(d,StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),EndDate)>=0 ")
        End If
        If btnSubmit.Text = "Save" And CInt(lblCount.Text) > 0 Then
            enddate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select EndDate from  AssessExam where  ProductID = " & ddlProduct.SelectedValue & " AND DATEDIFF(d,StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),EndDate)>=0 ")
        ElseIf CInt(lblCount.Text) > 1 Then
            enddate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select EndDate from  AssessExam where  ProductID = " & lblProductID.Text & " AND DATEDIFF(d,StartDate,GetDate())>= 0 AND  DATEDIFF(d,GetDate(),EndDate)>=0 and AssessExamID not in (" & lblAssessExamID.Text & ")")
        Else
            enddate = "1/1/" & Now.Year - 5
        End If
        If ddlGradeTo.SelectedValue = 1 Then
            lblerr.Text = "Grade To must be Greater than 1"
            Exit Sub
        ElseIf Val(ddlGradeTo.SelectedValue) < Val(ddlGradeFrom.SelectedValue) Then
            lblerr.Text = "Grade To is less than Grade From"
            Exit Sub
        ElseIf txtStartDate.Text = "" Then
            lblerr.Text = "Please Enter StartDate"
            Exit Sub
        ElseIf IsDate(txtStartDate.Text) = False Then
            lblerr.Text = "Please Enter StartDate in mm/dd/yyyy format"
            Exit Sub
        ElseIf txtEndDate.Text = "" Then
            lblerr.Text = "Please Enter EndDate"
            Exit Sub
        ElseIf IsDate(txtEndDate.Text) = False Then
            lblerr.Text = "Please Enter EndDate in mm/dd/yyyy format"
            Exit Sub
        ElseIf Date.Parse(txtEndDate.Text) < Date.Parse(txtStartDate.Text) Then
            lblerr.Text = "End Date is less than StartDate"
            Exit Sub
        ElseIf Date.Parse(txtStartDate.Text) <= Date.Parse(enddate) Then
            lblerr.Text = "Caution: The End Date of Currently Opened Exam have not yet over. Please Change Start Date."
            Exit Sub
        ElseIf txtQ1.Text = "" Then
            lblerr.Text = "Please Enter Q1"
            Exit Sub
        ElseIf txtQ2.Text = "" Then
            lblerr.Text = "Please Enter Q2"
            Exit Sub
        ElseIf txtQ3.Text = "" Then
            lblerr.Text = "Please Enter Q3"
            Exit Sub
        ElseIf txtQ4.Text = "" Then
            lblerr.Text = "Please Enter Q4"
            Exit Sub
        ElseIf txtQ5.Text = "" Then
            lblerr.Text = "Please Enter Q5"
            Exit Sub
        ElseIf txtQ6.Text = "" Then
            lblerr.Text = "Please Enter Q6"
            Exit Sub
        ElseIf txtQ7.Text = "" Then
            lblerr.Text = "Please Enter Q7"
            Exit Sub
        ElseIf txtQ8.Text = "" Then
            lblerr.Text = "Please Enter Q8"
            Exit Sub
        ElseIf txtQ9.Text = "" Then
            lblerr.Text = "Please Enter Q9"
            Exit Sub
        ElseIf txtQ10.Text = "" Then
            lblerr.Text = "Please Enter Q10"
            Exit Sub
        End If
        Dim StrSQl As String
        Try

            If btnSubmit.Text = "Save" Then
                StrSQl = "INSERT INTO AssessExam( StartDate, EndDate, GradeFrom, GradeTo, ProductID, ProductCode, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, CreateDate, CreatedBy) VALUES ('"
                StrSQl = StrSQl & txtStartDate.Text & "','" & txtEndDate.Text & "'," & ddlGradeFrom.SelectedValue & "," & ddlGradeTo.SelectedValue & "," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "','" & txtQ1.Text & "','" & txtQ2.Text & "','" & txtQ3.Text & "','" & txtQ4.Text & "','" & txtQ5.Text & "','" & txtQ6.Text & "','" & txtQ7.Text & "','" & txtQ8.Text & "','" & txtQ9.Text & "','" & txtQ10.Text & "', Getdate()," & Session("LoginID") & ")"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
                cleardiv2()
                ShowPrIDExisting()
                lblerr.Text = "Inserted Successfully"
            Else
                StrSQl = "UPDATE AssessExam SET StartDate='" & txtStartDate.Text & "', EndDate='" & txtEndDate.Text & "', GradeFrom=" & ddlGradeFrom.SelectedValue & ", GradeTo=" & ddlGradeTo.SelectedValue & " ,Q1='" & txtQ1.Text & "', Q2='" & txtQ2.Text & "', Q3='" & txtQ3.Text & "', Q4='" & txtQ4.Text & "', Q5='" & txtQ5.Text & "', Q6='" & txtQ6.Text & "', Q7='" & txtQ7.Text & "', Q8='" & txtQ8.Text & "', Q9='" & txtQ9.Text & "', Q10='" & txtQ10.Text & "',ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID") & " WHERE AssessExamID=" & lblAssessExamID.Text
                StrSQl = StrSQl & ""
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
                If ddlProduct.SelectedValue = "" Then
                    ShowExisting()
                Else
                    ShowPrIDExisting()
                End If
                cleardiv2()
                lblerr.Text = "Updated Successfully"
            End If
        Catch ex As Exception
            lblerr.Text = ex.ToString()
        End Try
        btnContinue.Enabled = True
        DivID2.Visible = False
    End Sub

    Private Sub cleardiv2()
        txtQ1.Text = String.Empty
        txtQ2.Text = String.Empty
        txtQ3.Text = String.Empty
        txtQ4.Text = String.Empty
        txtQ5.Text = String.Empty
        txtQ6.Text = String.Empty
        txtQ7.Text = String.Empty
        txtQ8.Text = String.Empty
        txtQ9.Text = String.Empty
        txtQ10.Text = String.Empty
        ddlProduct.Enabled = True
        ddlProductGroup.Enabled = True
        lblAssessExamID.Text = ""
        lblProductID.Text = ""
        lblerr.Text = ""
        btnSubmit.Text = "Save"
        txtEndDate.Text = String.Empty
        txtStartDate.Text = String.Empty
        setgradefrom(0)
        setgradeto(0)
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cleardiv2()
        btnContinue.Enabled = True
        DivID2.Visible = False
        ShowExisting()
    End Sub
    Private Sub disable()
        btnContinue.Enabled = False
        ddlProductGroup.Enabled = False
        ddlProduct.Enabled = False
        ddlGradeFrom.Enabled = False
        ddlGradeTo.Enabled = False
        txtStartDate.Enabled = False
    End Sub
    Private Sub enable()
        ddlProductGroup.Enabled = True
        ddlProduct.Enabled = True
        ddlGradeFrom.Enabled = True
        ddlGradeTo.Enabled = True
        txtStartDate.Enabled = True
    End Sub

    Protected Sub btnResult_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        cleardiv2()
        btnContinue.Enabled = True
        DivID2.Visible = False
        If ddlProduct.Items.Count < 1 Then
            lblerr.Text = "Please select Product Group and Product"
            Exit Sub
        ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
            lblerr.Text = "Please select Product"
            Exit Sub
        ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " SELECT COUNT(AssessExRespID) FROM AssessExamResp WHERE ProductID=" & ddlProduct.SelectedValue & "") > 0 Then
            ShowResult()
        Else
            lblerr.Text = "No Answer available for selected Product"
            GdAssessExam.Visible = False
            lblHeader.Text = ""
            Exit Sub
        End If
    End Sub

    Private Sub ShowResult()
        lblEditResult.Text = "R"
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select AE.AssessExamID,AE.StartDate,AE.EndDate,AE.GradeFrom,AE.GradeTo,P.Name,COUNT(AR.AssessExRespID) AS Total  from AssessExam AE INNER JOIN AssessExamResp AR ON AE.AssessExamID = AR.AssessExamID and AE.ProductID =" & ddlProduct.SelectedValue & " INNER JOIN Product P ON P.ProductId = AE.ProductID Group BY AE.AssessExamID,AE.StartDate,AE.EndDate,AE.GradeFrom,AE.GradeTo,P.Name")
        GdAssessExam.Columns(6).Visible = True
        GdAssessExam.DataSource = ds
        GdAssessExam.DataBind()
        If GdAssessExam.Rows.Count > 0 Then
            GdAssessExam.Visible = True
            lblHeader.Text = "Please select AssessExamID to view Result"
        Else
            GdAssessExam.Visible = False
            lblHeader.Text = ""
        End If
    End Sub

    Protected Sub Btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        divResult.Visible = False
        ShowExisting()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Assessment_Result.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Response.Write("<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Assessment Results</b><br>")
        Response.Write("<br>By Grade :<br>")
        GridOverAll.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.Write("<br>By Question :<br>")
        Dim stringWrite1 As New System.IO.StringWriter()
        Dim htmlWrite1 As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite1)
        GridByQ.RenderControl(htmlWrite1)
        Response.Write(stringWrite1.ToString())
        Response.[End]()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim strSql As String = "select productgroupid from product where productid=" & ddlProduct.SelectedValue & ""
        Dim PgId As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, strSql))
        ddlProductGroup.SelectedValue = PgId
    End Sub
End Class


