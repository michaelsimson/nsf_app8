﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="RoomSchedule.aspx.vb" Inherits="RoomSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <asp:HyperLink ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx" CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
    </div>
    <table align="right">
        <tr>
            <td align="right">
                <asp:Button ID="btnExportReport" runat="server" Text="Generate Report" /></td>
        </tr>
    </table>
    <table id="MainTable" runat="server" align="center" border="0">
        <tr>
            <td align="center">Room Schedule </td>
        </tr>
        <tr>
            <td>
                <table border="1" runat="server" align="left">
                    <tr>
                        <td align="left" width="75px">ContestYear </td>
                        <td>
                            <asp:DropDownList ID="ddlYear" DataTextField="Year" DataValueField="Year" AutoPostBack="true" runat="server" Width="125px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="75px">Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" DataTextField="Name" DataValueField="EventId" AutoPostBack="true" runat="server" Width="125px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="75px">Chapter</td>
                        <td>
                            <asp:DropDownList ID="ddlChapter" DataTextField="Chapter" DataValueField="ChapterId" AutoPostBack="true" runat="server" Width="125px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="75px">Purpose</td>
                        <td>
                            <asp:DropDownList ID="ddlPurpose" DataTextField="Purpose" DataValueField="Purpose" AutoPostBack="true" runat="server" Width="125px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="100px">Date </td>
                        <td>
                            <asp:DropDownList ID="ddlDate" AutoPostBack="true" runat="server" Width="100px" Visible="true"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" runat="server">
                    <tr>
                        <td>
                            <table border="1" runat="server">
                                <tr id="TrProductGroup" runat="server">
                                    <td align="left" width="100px">ProductGroup </td>
                                    <td>
                                        <asp:DropDownList ID="ddlProductGroup" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" AutoPostBack="true" runat="server" Width="100px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrProduct" runat="server">
                                    <td width="100px">Product</td>
                                    <td>
                                        <asp:DropDownList ID="ddlProduct" DataTextField="Product" DataValueField="ProductId" AutoPostBack="true" runat="server" Width="100px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrPhase" runat="server">
                                    <td width="100px">Phase</td>
                                    <td>
                                        <asp:DropDownList ID="ddlPhase" AutoPostBack="true" runat="server" Width="100px">
                                            <asp:ListItem Text="0" Value="0">Select Phase</asp:ListItem>
                                            <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                                            <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                                            <asp:ListItem Text="3" Value="3">3</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <%--<tr id="TrStarttime" runat="server"> 
                                        <td width="100px">StartTime</td><td> 
                                        <asp:DropDownList ID="ddlStartTime" runat="server" width="100px"></asp:DropDownList> </td> 
                                </tr><tr id="TrEndtime" runat="server"> 
                                        <td width="100px"> EndTime </td><td> 
                                        <asp:DropDownList ID="ddlEndTime" runat="server" width="100px"></asp:DropDownList> </td> 
                                </tr>--%><%--<tr id="TrCapacity" runat="server">
                                        <td width="100px"> Capacity </td><td> 
                                        <asp:TextBox ID="txtCapacity" runat="server" width="125px"></asp:TextBox></td>
                                </tr> --%>
                            </table>
                        </td>
                        <td></td>
                        <td>
                            <table border="1" runat="server">
                                <tr>
                                    <td width="75px">BuildingID</td>
                                    <td>
                                        <asp:DropDownList ID="ddlBldgID" AutoPostBack="true" runat="server" Width="100px"></asp:DropDownList></td>
                                    <td width="75px">SequenceNo</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSeqNo" AutoPostBack="true" runat="server" Width="100px">
                                            <asp:ListItem Text="0" Value="0">Select Seq. No</asp:ListItem>
                                            <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                                            <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                                            <asp:ListItem Text="3" Value="3">3</asp:ListItem>
                                            <asp:ListItem Text="4" Value="4">4</asp:ListItem>
                                            <asp:ListItem Text="5" Value="5">5</asp:ListItem>
                                            <asp:ListItem Text="6" Value="6">6</asp:ListItem>
                                            <asp:ListItem Text="7" Value="7">7</asp:ListItem>
                                            <asp:ListItem Text="8" Value="8">8</asp:ListItem>
                                            <asp:ListItem Text="9" Value="9">9</asp:ListItem>
                                            <asp:ListItem Text="10" Value="10">10</asp:ListItem>
                                            <asp:ListItem Text="11" Value="11">11</asp:ListItem>
                                            <asp:ListItem Text="12" Value="12">12</asp:ListItem>
                                            <asp:ListItem Text="13" Value="13">13</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td width="75px">RoomNumber</td>
                                    <td>
                                        <asp:DropDownList ID="ddlRoomNo" AutoPostBack="true" runat="server" Width="100px" Enabled="True"></asp:DropDownList></td>
                                    <td width="75px">StartBadge</td>
                                    <td>
                                        <asp:TextBox ID="txtStartBadge" runat="server" Width="100px"></asp:TextBox></td>
                                    <td width="75px">EndBadge</td>
                                    <td>
                                        <asp:TextBox ID="txtEndBadge" runat="server" Width="100px"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="TrCopy" runat="server" align="center" visible="false">
            <td>
                <table>
                    <tr>
                        <%--<td width="75px">From </td>--%>

                        <td width="120px" align="right">To(ProductGroup)</td>
                        <td>
                            <asp:DropDownList ID="ddlToPG" AutoPostBack="true" runat="server" DataTextField="ProductGroupCode" DataValueField="ProductGroupID" Width="125px" Enabled="True"></asp:DropDownList>
                            <asp:DropDownList ID="ddlToProduct" AutoPostBack="true" runat="server" DataTextField="Product" DataValueField="ProductID" Width="125px" Enabled="True"></asp:DropDownList>
                            &nbsp;&nbsp;
                        </td>
                        <%--<td>Phase</td><td><asp:DropDownList ID="ddlPhase1" AutoPostBack="true" runat="server" width="125px" Enabled="True">
                        <asp:ListItem Text="0" Value="0">Select Phase</asp:ListItem>
                        <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                        <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                        <asp:ListItem Text="3" Value="3">3</asp:ListItem>
                </asp:DropDownList> &nbsp;&nbsp;
                
           </td>--%>
                        <%--<td>
                StartTime</td><td><asp:DropDownList ID="ddlStartCpTime" AutoPostBack="true" runat="server" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
               </td><td> EndTime</td><td><asp:DropDownList ID="ddlEndCpTime" AutoPostBack="true" runat="server" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
           </td>--%>

                        <td>
                            <asp:Button ID="BtnCopySchedule" runat="server" Text="CopySchedule"></asp:Button></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr id="TrAddUpdate" runat="server" align="center">
            <td align="center">
                <asp:Button ID="btnCopy" runat="server" Text="Copy"></asp:Button>
                <asp:Button ID="btnAddUpdate" runat="server" Text="Add"></asp:Button>
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" /></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblAddUPdate" runat="server" Text="" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblErr" runat="server" Text="" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr id="TrSeqUpdate" runat="server" visible="false" align="center">
            <td>
                <asp:Button ID="BtnYes" runat="server" Text="Yes" Style="width: 37px"></asp:Button>
                <asp:Button ID="BtnNo" runat="server" Text="No" /></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblRoomSchID" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
        </tr>
    </table>

    <table align="center">
        <tr>
            <td>
                <asp:DataGrid ID="DGRoomSchedule" runat="server" DataKeyField="RoomSchID" AutoGenerateColumns="False" OnItemCommand="DGRoomSchedule_ItemCommand" CellPadding="4"
                    BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black">
                    <FooterStyle BackColor="#CCCCCC" />
                    <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                    <ItemStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="RoomSchID" HeaderText="RoomSchID" Visible="true" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Chapter" HeaderText="Chapter" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Event" HeaderText="Event" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="BldgID" HeaderText="BldgID" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SeqNo" HeaderText="SequenceNo" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="RoomNumber" HeaderText="RoomNumber" />
                        <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblCapacity" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Capacity") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity"/>--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Date" DataFormatString="{0:d}" HeaderText="Date" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Phase" HeaderText="Phase" />
                        <%-- <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="StartTime" HeaderText="StartTime"/>
             <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EndTime" HeaderText="EndTime"/>--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="StartBadgeNo" HeaderText="StartBadgeNo" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EndBadgeNo" HeaderText="EndBadgeNo" />

                    </Columns>
                    <HeaderStyle BackColor="White" />
                </asp:DataGrid>
            </td>
        </tr>
    </table>




</asp:Content>

