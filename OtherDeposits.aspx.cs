﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using NativeExcel;

public partial class OtherDeposits : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        lblErr.Text = ""; 
        if (!IsPostBack)
        {
            GetSponsor(ddlSponsorshipType);
            GetSalesType(ddlSalesType);
            GetEvents(ddlEvent);
            GetBank(ddlBank);
            GetChapters(DdlChapter);
            GetEventYear(DdlYear);
            GetStates();
            Getdata();
        }
    }


    private void GetEventYear(DropDownList ddlObject)
    {
        int[] year = new int[4];
                year[0] = DateTime.Now.Year;
        year[1] = DateTime.Now.AddYears(-1).Year;
        year[2] = DateTime.Now.AddYears(-2).Year;
        year[3] = DateTime.Now.AddYears(-3).Year;      
        ddlObject.DataSource = year;
        ddlObject.DataBind();
        ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByValue(DateTime.Now.Year.ToString()));

    }
    private void GetSponsor(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT SponsorDesc, SponsorId FROM SponsorCat");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "SponsorDesc";
        ddlObject.DataValueField = "SponsorId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetSalesType(DropDownList ddlObject)
    {

        DataSet dsChoice = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT SalesCatDesc, SalesCatID FROM SalesCat");
        ddlObject.DataSource = dsChoice;
        ddlObject.DataTextField = "SalesCatDesc";
        ddlObject.DataValueField = "SalesCatID";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetEvents(DropDownList ddlObject)
    {
        DataSet dsEvent = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Name,EventId  from Event Group by Name,EventId");
        ddlObject.DataSource = dsEvent;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "EventId";
        ddlObject.DataBind();
        if (dsEvent.Tables[0].Rows.Count > 1)
        {
            ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
        }
        else
            ddlObject.Enabled = false;

    }
    private void GetBank(DropDownList ddlobject)
    {
        DataSet dsBanks = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT BankID, BankCode  from Bank");
        ddlobject.DataSource = dsBanks.Tables[0];
        ddlobject.DataTextField = "BankCode";
        ddlobject.DataValueField = "BankID";
        ddlobject.DataBind();
        ddlobject.SelectedIndex = ddlobject.Items.IndexOf(ddlobject.Items.FindByValue("2"));

    }
    private void GetChapters(DropDownList ddlObject)
    {
        DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetChapterAll");
        ddlObject.DataSource = ds_Chapter;
        ddlObject.DataTextField = "ChapterCode";
        ddlObject.DataValueField = "ChapterId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
   
    protected void DdlRevType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Fees
//Small Donations

        if ((DdlRevType.SelectedValue == "Small Donations") || (DdlRevType.SelectedValue == "Fees"))
        {
            BtnincueredExpFind.Enabled = false;
            ddlSalesType.Enabled = false;
            txtName.Text = string.Empty; 
            ddlSponsorshipType.SelectedIndex = ddlSponsorshipType.Items.IndexOf(ddlSponsorshipType.Items.FindByValue("-1"));
            ddlSalesType.SelectedIndex = ddlSalesType.Items.IndexOf(ddlSalesType.Items.FindByValue("-1"));
            ddlSponsorshipType.Enabled = false;
        }
        else if (DdlRevType.SelectedValue == "Sales")
        {
            ddlSalesType.Enabled = true;
            //ddlSponsorshipType
            BtnincueredExpFind.Enabled = true;
            ddlSponsorshipType.SelectedIndex = ddlSponsorshipType.Items.IndexOf(ddlSponsorshipType.Items.FindByValue("-1"));
            ddlSponsorshipType.Enabled = false; 
        }
        else
        {
            ddlSalesType.Enabled = false;
            BtnincueredExpFind.Enabled = true;
            ddlSalesType.SelectedIndex = ddlSalesType.Items.IndexOf(ddlSalesType.Items.FindByValue("-1"));
            ddlSponsorshipType.Enabled = true ;
        }
    }

    protected void Find_Click(object sender, EventArgs e)
    {
            ddlDonorType.SelectedIndex = ddlDonorType.Items.IndexOf(ddlDonorType.Items.FindByValue("INDSPOUSE"));
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
            ddlDonorType.Enabled = true;
            pIndSearch.Visible = true;          
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            ddlState.SelectedIndex = 0;   
      }

    protected void GridMemberDt_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int index = int.Parse(e.CommandArgument.ToString());
        GridViewRow row = GridMemberDt.Rows[index];
        // IncurredExpence name Search

            txtName.Text = row.Cells[4].Text.Trim() == "OWN" ? row.Cells[1].Text : row.Cells[1].Text.Trim() + " " + row.Cells[2].Text.Trim();
            HdnMemberID.Value   = GridMemberDt.DataKeys[index].Value.ToString ();
            HdnDonorType.Value   = row.Cells[4].Text;
            pIndSearch.Visible = false;
            Panel4.Visible = false;
    }
   protected void btnSearch_onClick(object sender, EventArgs e)
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty;
        string email = string.Empty;
        string orgname = string.Empty;
        StringBuilder strSql = new StringBuilder();
        StringBuilder strSqlOrg = new StringBuilder();
        firstName = txtFirstName.Text;
        lastName = txtLastName.Text;
        state = ddlState.SelectedItem.Value.ToString();
        email = txtEmail.Text;
        orgname = txtOrgName.Text;
        //if (state.Length < 1)
        //{
        //    lblIndSearch.Text = "Please Select State";
        //    lblIndSearch.Visible = true;
        //    return;
        //}
        //else
        //{
        //    lblIndSearch.Text = "";
        //    lblIndSearch.Visible = false;
        //}

        if (orgname.Length > 0)
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'");
        }

        if (firstName.Length > 0)
        {
            strSql.Append("  I.firstName like '%" + firstName + "%'");
        }

        if (lastName.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.lastName like '%" + lastName + "%'");
                //strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
            else
            {
                strSql.Append("  I.lastName like '%" + lastName + "%'");
                // strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
        }

        if (state.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.State like '%" + state + "%'");
            }
            else
            {
                strSql.Append("  I.State like '%" + state + "%'");
            }
        }

        if (email.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
                strSql.Append(" and I.Email like '%" + email + "%'");
            else
                strSql.Append("  I.Email like '%" + email + "%'");
        }

        if (state.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            else
                strSqlOrg.Append(" O.State like '%" + state + "%'");
        }

        if (email.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");

        }



        if (firstName.Length > 0 || orgname.Length > 0 || lastName.Length > 0 || email.Length > 0 || state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorType.SelectedValue == "INDSPOUSE")
                SearchMembers(strSql.ToString());
            else
                SearchOrganization(strSqlOrg.ToString());
        }
        else
        {
            lblIndSearch.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";
            lblIndSearch.Visible = true;
            return;
        }
    }

    private void SearchOrganization(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }
    protected void ddlDonorType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDonorType.SelectedValue == "Organization")
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtOrgName.Text = String.Empty;
            txtLastName.Enabled = false;
            txtFirstName.Enabled = false;
            txtOrgName.Enabled = true;
        }
        else
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
        }
    }

    protected void btnIndClose_onclick(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;    
      
    }
    private void GetStates()
    {
        DataSet dsStates;
        dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
        ddlState.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("Select State", String.Empty));
        ddlState.SelectedIndex = 0;
    }
    private void SearchMembers(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }

    protected void Save_Click(object sender, EventArgs e)
    {
        //SELECT    OtherDepositID, BankID, EventID, ChapterID, EventYear, DepositSlipNo, DepositDate, Amount, RevenueType, SponsorId, SalesId, MemberID, DonorType, CreatedBy, CreatedDate, ModifiedBy, ModifyDate FROM  OtherDeposits
        lblErr.Text = string.Empty;
        String DonorType = "Null";
        String MemberID = "Null"; 
        String SponsorId  = "Null";
        String SalesId = "Null"; 
        if (ddlEvent.SelectedValue == "-1")
        {
            lblErr.Text = "Please select Event";
            return;
        }
        else if (DdlChapter.SelectedValue == "-1")
        {
            lblErr.Text = "Please select Chapter";
            return;
        }
        else if (txtDepositDate.Text.Length < 1)
        {
            lblErr.Text = "Please enter Deposit Date";
            return;
        }
        else if (txtDepositSlipNo.Text.Length < 1)
        {
            lblErr.Text = "Please enter Deposit Slip#";
            return;
        }
        else if (txtAmount.Text.Length < 1)
        {
            lblErr.Text = "Please enter Amount";
            return;
        }
        else if ((DdlRevType.SelectedValue == "Sales") && ( ddlSalesType.SelectedValue == "-1" ))
        {
           lblErr.Text = "Please select Sales Type";
            return;
        }
        else if ((DdlRevType.SelectedValue == "Sponsorship") && (ddlSponsorshipType.SelectedValue == "-1" ))
        {
           lblErr.Text = "Please select Sponsorship Type";
            return;
        }
       


        if (ddlSalesType.SelectedValue != "-1")
          SalesId = ddlSalesType.SelectedValue;

        if (ddlSponsorshipType.SelectedValue != "-1")
            SponsorId = ddlSponsorshipType.SelectedValue;
       
        if (txtName.Text.Length > 0)
        {
             DonorType = "'" +HdnDonorType.Value + "'";
             MemberID = HdnMemberID.Value ;
        }
        String strsql;
        if (Save.Text == "Update")
        {
            strsql = "Update OtherDeposits set BankID=" + ddlBank.SelectedValue + ", EventID=" + ddlEvent.SelectedValue + ", ChapterID=" + DdlChapter.SelectedValue + ", EventYear=" + DdlYear.SelectedValue + ", DepositSlipNo=" + txtDepositSlipNo.Text + ", DepositDate='" + txtDepositDate.Text + "', Amount=" + txtAmount.Text + ", RevenueType='" + DdlRevType.SelectedValue + "', SponsorId=" + SponsorId + ", SalesId=" + SalesId + ", MemberID=" + MemberID + ", DonorType=" + DonorType + ",  ModifiedBy=" + Session["LoginID"].ToString() + ", ModifyDate=GetDate() WHERE OtherDepositID=" + HdnOtherDepositID.Value ;
        }
        else
        {
            strsql = " Insert Into  OtherDeposits (BankID, EventID, ChapterID, EventYear, DepositSlipNo, DepositDate, Amount, RevenueType, SponsorId, SalesId, MemberID, DonorType, CreatedBy, CreatedDate,RestrictionType) values (";
            strsql = strsql + ddlBank.SelectedValue + "," + ddlEvent.SelectedValue + "," + DdlChapter.SelectedValue + "," + DdlYear.SelectedValue + "," + txtDepositSlipNo.Text + ",'" + txtDepositDate.Text + "'," + txtAmount.Text + ",'" + DdlRevType.SelectedValue + "'," + SponsorId + "," + SalesId + "," + MemberID + "," + DonorType + "," + Session["LoginID"].ToString() + ",GetDate(),'Unrestricted')";
        }
        try
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strsql);
            lblErr.Text = "Updated Successfully";
            if (Save.Text == "Update")
                ClearData();
            else
                lblErr.Text = "Inserted Successfully";
            txtAmount.Text = string.Empty;
            txtDepositDate.Text = string.Empty;
            txtDepositSlipNo.Text = string.Empty;
            Getdata();
        }
        catch (Exception ex)
        {
            lblErr.Text = ex.ToString();
        }
       
       
    }

    private void Getdata()
    {
        DataSet ds;
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT  O.OtherDepositID, B.BankCode , E.Name as EventName, C.ChapterCode, O.EventYear, O.DepositSlipNo, O.DepositDate, O.Amount, O.RevenueType,S.SponsorDesc, SC.SalesCatDesc, CASE WHEN O.DonorType is not null then CASE WHEN O.DonorType = 'OWN' then  Org.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END ELSE ''  END as Name , O.DonorType, O.CreatedBy,  O.CreatedDate FROM     OtherDeposits O Inner Join Bank B ON B.BankID = O.BankID  Inner Join Chapter C on C.ChapterID = O.ChapterID Inner Join Event E On  E.EventId = O.EventID   Left Join SponsorCat S ON S.SponsorID=O.SponsorId Left Join SalesCat SC ON SC.SalesCatID=O.SalesId Left JOIN INDSPOUSE I ON I.AutoMemberID=O.MemberID Left Join OrganizationInfo Org ON O.DonorType = 'OWN' and Org.AutoMemberid =O.Memberid ORDER BY O.DepositDate DESC");
        GVOtherDeposits.DataSource = ds.Tables[0];
        GVOtherDeposits.DataBind();
       
    }
    protected void GVOtherDeposits_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = int.Parse(e.CommandArgument.ToString());
        //GridViewRow row = GVOtherDeposits.Rows[index];
        HdnOtherDepositID.Value = GVOtherDeposits.DataKeys[index].Value.ToString();
        GetSeletedData();
    }
    private void GetSeletedData()
    {
        DataSet ds;
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT O.OtherDepositID, O.BankID, O.EventID, O.ChapterID, O.EventYear, O.DepositSlipNo, O.DepositDate, O.Amount, O.RevenueType, O.SponsorId, O.SalesId, O.MemberID, O.DonorType, O.CreatedBy , CASE WHEN O.DonorType is not null then CASE WHEN O.DonorType = 'OWN' then  Org.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END ELSE ''  END as Name FROM OtherDeposits  O Left JOIN INDSPOUSE I ON I.AutoMemberID=O.MemberID Left Join OrganizationInfo Org ON O.DonorType = 'OWN' and Org.AutoMemberid =O.Memberid where O.OtherDepositID = " + HdnOtherDepositID.Value);
        //Response.Write ("SELECT O.OtherDepositID, O.BankID, O.EventID, O.ChapterID, O.EventYear, O.DepositSlipNo, O.DepositDate, O.Amount, O.RevenueType, O.SponsorId, O.SalesId, O.MemberID, O.DonorType, O.CreatedBy , CASE WHEN O.DonorType is not null then CASE WHEN O.DonorType = 'OWN' then  Org.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END ELSE ''  END as Name FROM OtherDeposits  O Left JOIN INDSPOUSE I ON I.AutoMemberID=O.MemberID Left Join OrganizationInfo Org ON O.DonorType = 'OWN' and Org.AutoMemberid =O.Memberid where O.OtherDepositID = " + HdnOtherDepositID.Value);
        ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(ds.Tables[0].Rows[0]["EventID"].ToString()));
        DdlYear.SelectedIndex = DdlYear.Items.IndexOf(DdlYear.Items.FindByValue(ds.Tables[0].Rows[0]["EventYear"].ToString()));
        DdlChapter.SelectedIndex = DdlChapter.Items.IndexOf(DdlChapter.Items.FindByValue(ds.Tables[0].Rows[0]["ChapterID"].ToString()));
        ddlBank.SelectedIndex = ddlBank.Items.IndexOf(ddlBank.Items.FindByValue(ds.Tables[0].Rows[0]["BankID"].ToString()));
        DdlRevType.SelectedIndex = DdlRevType.Items.IndexOf(DdlRevType.Items.FindByValue(ds.Tables[0].Rows[0]["RevenueType"].ToString()));
        if (ds.Tables[0].Rows[0]["SalesId"] != DBNull.Value)
            ddlSalesType.SelectedIndex = ddlSalesType.Items.IndexOf(ddlSalesType.Items.FindByValue(ds.Tables[0].Rows[0]["SalesId"].ToString()));
        else
            ddlSalesType.SelectedIndex = ddlSalesType.Items.IndexOf(ddlSalesType.Items.FindByValue("-1"));

        if (ds.Tables[0].Rows[0]["SponsorId"] != DBNull.Value)
            ddlSponsorshipType.SelectedIndex = ddlSponsorshipType.Items.IndexOf(ddlSponsorshipType.Items.FindByValue(ds.Tables[0].Rows[0]["SponsorId"].ToString()));
        else
            ddlSponsorshipType.SelectedIndex = ddlSponsorshipType.Items.IndexOf(ddlSponsorshipType.Items.FindByValue("-1"));

        txtDepositDate.Text = Convert.ToDateTime (ds.Tables[0].Rows[0]["DepositDate"].ToString ()).ToString("d");
        txtDepositSlipNo.Text = ds.Tables[0].Rows[0]["DepositSlipNo"].ToString().Trim();
        txtAmount.Text = Math.Round (Convert.ToDecimal (ds.Tables[0].Rows[0]["Amount"].ToString()),2).ToString();
        txtName.Text = ds.Tables[0].Rows[0]["Name"].ToString();
        if (txtName.Text.Length > 0)
        {
            HdnDonorType.Value = ds.Tables[0].Rows[0]["MemberID"].ToString();
            HdnMemberID.Value = ds.Tables[0].Rows[0]["DonorType"].ToString();
        }
        else
        {
            HdnDonorType.Value = String.Empty;
            HdnMemberID.Value = String.Empty;
        }
        Save.Text = "Update";
    }
    private void ClearData()
    {
        ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue("-1"));
        DdlChapter.SelectedIndex = DdlChapter.Items.IndexOf(DdlChapter.Items.FindByValue("-1"));
        ddlBank.SelectedIndex = ddlBank.Items.IndexOf(ddlBank.Items.FindByValue("2"));
        DdlRevType.SelectedIndex = DdlRevType.Items.IndexOf(DdlRevType.Items.FindByValue("Fees"));
        ddlSalesType.SelectedIndex = ddlSalesType.Items.IndexOf(ddlSalesType.Items.FindByValue("-1"));
        DdlYear.SelectedIndex = DdlYear.Items.IndexOf(DdlYear.Items.FindByValue(DateTime.Now.Year.ToString()));
        ddlSalesType.Enabled = false;
        ddlSponsorshipType.SelectedIndex = ddlSponsorshipType.Items.IndexOf(ddlSponsorshipType.Items.FindByValue("-1"));
        ddlSponsorshipType.Enabled = false;
        txtDepositDate.Text = string.Empty;
        txtDepositSlipNo.Text = string.Empty;
        txtAmount.Text = string.Empty;
        txtName.Text = string.Empty;
        HdnDonorType.Value = String.Empty;
        HdnMemberID.Value = String.Empty;
        HdnOtherDepositID.Value = String.Empty;
        Save.Text = "Submit";
       
    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        ClearData();
    }
}
