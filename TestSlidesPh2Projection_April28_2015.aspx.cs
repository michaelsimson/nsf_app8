﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TestSlidesPh2Projection : System.Web.UI.Page
{
    int icTemp;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }
            icTemp = 26;
            ViewState["increValue"] = icTemp;
            bindData();
        }
        
       
    }


    protected void bindData()
    {

       
        Session["ddlYear"] = "2015";
        Session["ddlEvent"] = "2";
        Session["ddlProductGroup"] = "VB";
        Session["ddlProduct"] = "Junior Vocabulary";
        Session["ddlSet"] = "1";
        string connectionString = Application["ConnectionString"].ToString();
        SqlConnection connection = new SqlConnection(connectionString);
        DataSet ds = new DataSet();
        string sql = "(select distinct top 35 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= case '" + Session["ddlProduct"] + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  )";
        sql = sql + "union(select distinct top 35 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= case '" + Session["ddlProduct"] + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  ) Order by sp.Phase,sp.[Sub-level]";
       
        int val = Convert.ToInt16(txtHidden.Value);
        if (val <= 0)
            val = 0;
        connection.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
        adapter.Fill(ds, val, 1, "SpVocabTPWords");
        connection.Close();
        rSlideProjection.DataSource = ds;
        rSlideProjection.DataBind();

        if (val <= 0)
        {
            lnkBtnPrev.Visible = false;
            lnkBtnNext.Visible = true;
        }

        if (val >= 1)
        {
            lnkBtnPrev.Visible = true;
            lnkBtnNext.Visible = true;
        }

        if ((val + 1) >= 70)
        {
            lnkBtnNext.Visible = false;
        }
    }
    //protected void bindData()
    //{
    //    icTemp = 25;
    //    Session["ddlYear"] = "2015";
    //    Session["ddlEvent"] = "2";
    //    Session["ddlProductGroup"] = "VB";
    //    Session["ddlProduct"] = "Junior Vocabulary";
    //    Session["ddlSet"] = "1";


    //    string strqryStuPh2 = "";
    //    // Judge Copy Phase1 & Phase2
    //    strqryStuPh2 = "(select distinct top 35 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= case '" + Session["ddlProduct"] + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  )";
    //    strqryStuPh2 = strqryStuPh2 + "union(select distinct top 35 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= case '" + Session["ddlProduct"] + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  ) Order by sp.Phase,sp.[Sub-level]";
    //    DataSet dsJVBStuPh2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqryStuPh2);
    //    //adding serial number to dataTable
    //    DataColumn column1 = dsJVBStuPh2.Tables[0].Columns.Add("SNo", typeof(Int32));
    //    column1.SetOrdinal(0);// to put the column in position 0;
    //    int k1 = 26;
    //    foreach (DataRow dr in dsJVBStuPh2.Tables[0].Rows)
    //    {
    //        dr["SNo"] = k1;
    //        k1++;

    //    }

    //    rSlideProjection.DataSource = dsJVBStuPh2;
    //    rSlideProjection.DataBind();
    //}


    protected void rSlideProjection_ItemBound(object sender, RepeaterItemEventArgs e)
    {


        icTemp = Convert.ToInt32(ViewState["increValue"].ToString());
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("SNoStuPhase2");
        lblSNo.Text = Convert.ToString(icTemp);

        ViewState["increValue"] = icTemp;

    }

    protected void lnkBtnNext_Click(object sender, EventArgs e)
    {
        ViewState["increValue"] = Convert.ToInt32(ViewState["increValue"].ToString()) +1;
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) + 1);
        bindData();
    }
    protected void lnkBtnPrev_Click(object sender, EventArgs e)
    {
        ViewState["increValue"] = Convert.ToInt32(ViewState["increValue"].ToString()) - 1;
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) - 1);
        bindData();
    }
    
}