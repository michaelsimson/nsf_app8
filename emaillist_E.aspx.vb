Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data
' Created by :Shalini


Partial Class emaillist_E
    Inherits System.Web.UI.Page
    Dim attach As Attachment
    Dim attach1 As Attachment

    ' Dim conn As New SqlConnection("Data Source=sql.northsouth.org; Initial Catalog=northsouth_prd; User ID='northsouth'; Password='Nhemi$SHemi';")
    Dim conn As New SqlConnection("Data Source=sql.northsouth.org; Initial Catalog=northsouth_prd; User ID='northsouth'; Password='Nhemi$SHemi';")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblerr.Text = ""
        lblfromemailerror.Text = ""
        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then  ' for attachment
            '  Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            attach = New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName)
        End If

        If String.IsNullOrEmpty(AttachmentFile1.FileName) OrElse AttachmentFile1.PostedFile Is Nothing Then  ' for attachment
            '  Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            attach1 = New Attachment(AttachmentFile1.PostedFile.InputStream, AttachmentFile1.FileName)
        End If

    End Sub

    Protected Sub btnsendemail_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer = 1
        Dim j As Integer = 1
        Dim StrSQL As String
        StrSQL = "select Top 5001 Email,ID from NewsletterEmails where flag='N'"
        Dim dsEmails As New DataSet
        Dim tblEmails() As String = {"Emails"}
        SqlHelper.FillDataset(conn, CommandType.Text, StrSQL, dsEmails, tblEmails)
        Dim mm As MailMessage
        'lblemailstatus.Text = "Email Send To :"
        If dsEmails.Tables("Emails").Rows.Count > 0 Then
            Dim n As Integer
            Dim SuccessmailIDs As String = "0"
            Dim failuremailIDs As String = "0"
            Dim count As Integer = 0
            Dim emailadd As String
            For n = 0 To dsEmails.Tables("Emails").Rows.Count - 1
                count = count + 1
                emailadd = Trim(dsEmails.Tables(0).Rows(n)("Email"))
                If EmailAddressCheck(emailadd) Then
                    mm = GetMailMessage(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''"), emailadd, txtfrom.Text)
                    If SendEmail(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''"), emailadd, txtfrom.Text, mm) = True Then ''** 
                        SuccessmailIDs = SuccessmailIDs & "," & dsEmails.Tables(0).Rows(n)("ID")
                        i = i + 1
                    Else
                        failuremailIDs = failuremailIDs & "," & dsEmails.Tables(0).Rows(n)("ID")
                        j = j + 1
                    End If
                Else
                    failuremailIDs = failuremailIDs & "," & dsEmails.Tables(0).Rows(n)("ID")
                    j = j + 1
                End If
                If (count = 500) Then  ' for every 500 emails sent
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='Y',ModifiedDate=Getdate() where ID in (" & SuccessmailIDs & ")")
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='F',ModifiedDate=Getdate() where ID in (" & failuremailIDs & ")")
                    SuccessmailIDs = "0"
                    failuremailIDs = "0"
                    count = 0
                ElseIf n = dsEmails.Tables("Emails").Rows.Count - 1 Then
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='Y',ModifiedDate=Getdate() where ID in (" & SuccessmailIDs & ")")
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "update NewsletterEmails set flag='F',ModifiedDate=Getdate() where ID in (" & failuremailIDs & ")")
                    'SendEmail("Newsletter Sent all Receipients", "Mail sent to all : " & n.ToString(), "ferdin448@gmail.com")
                    'Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert(' " & i & "message sent ');"), True)
                End If

                
                If n Mod 2500 = 0 Then
                    System.Threading.Thread.Sleep(10000) ' will pause for 10 seconds
                End If
                
            Next n
            lblEMailError.Text = "Newsletter Successfully Send to " & i & " Emails"
            If j < 1 Then
                lblEMailError.Text = ""
            End If
        Else
            lblEMailError.ForeColor = Color.Red
            lblEMailError.Text = "Please load Emails"
        End If
       
        Dim email As New MailMessage
        email = New MailMessage(txtfrom.Text, "fsilva@redegginfoexpert.com")
        If i > 100 Then
            SendEmail("NSF - Sent Email Status", "Dear Sir,<br>" & i & " emails were successfully sent.<br> NSF admin", Session("LoginEmail"), txtfrom.Text, email)

        End If
        If lblEMailError.Text = "" Then
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert('All Emails Sent');"), True)
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert('Failed Sending to below Emails');"), True)
        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal smailfrom As String, ByVal email As MailMessage) As Boolean ''**

        'Build Email Message
        'Dim email As New MailMessage
        email.From = New MailAddress(smailfrom)
        'email.To.Add(sMailTo)
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server


        Dim client As New SmtpClient()
        'SMTP client info is stored in web.config file

        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host

        Dim ok As Boolean = True
        Try
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            client.Timeout = 20000
            client.Send(email)
        Catch e As Exception
            lblerr.Text = lblerr.Text & " <br> " & sMailTo & " " & e.Message.ToString
            ok = False
            'Return False
        End Try
        Return ok

    End Function

    Private Function GetMailMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sMailFrom As String) As MailMessage
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim mm As New MailMessage

        '(1) Create the MailMessage instance
        Try
            mm = New MailMessage(sMailFrom, sMailTo)
        Catch
            Response.Write("Email Address Having Problem is " & sMailTo & " From Address:" & sMailFrom)
        End Try

        '(2) Assign the MailMessage's properties
        mm.Subject = sSubject
        mm.Body = sBody
        mm.IsBodyHtml = True

        strEmailAddressList = Split(sMailTo, ",")
        Dim EmailAdd As String
        For Each item In strEmailAddressList
            'check if email address is valid
            EmailAdd = Trim(item.ToString.Replace(vbCrLf, ""))
            If EmailAddressCheck(EmailAdd) Then
                'mm.Bcc.Add(EmailAdd)
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            End If
        Next
        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then
            ' Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            ' mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName))
            mm.Attachments.Add(attach)
        End If

        If String.IsNullOrEmpty(AttachmentFile1.FileName) OrElse AttachmentFile1.PostedFile Is Nothing Then
            ' Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            ' mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName))
            mm.Attachments.Add(attach1)
        End If

        Return mm

    End Function

    Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        'Validate email address
        Try
            Dim pattern As String = "^[a-zA-Z0-9][\w\.-\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            '"^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
            If emailAddressMatch.Success Then
                EmailAddressCheck = True
            Else
                EmailAddressCheck = False
            End If
        Catch ex As Exception
            EmailAddressCheck = False
        End Try
    End Function

End Class
