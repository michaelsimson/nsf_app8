﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net.Mail
Partial Class SignatureUpload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        lblErr.Text = String.Empty
        If Not IsPostBack() Then
            If Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "2" Or Session("RoleId").ToString() = "3" Or Session("RoleId").ToString() = "4" Or Session("RoleId").ToString() = "5" Then
                lblChapter.Text = Session("selChapterName")
                LoadVolunteer()
            Else
                Server.Transfer("VolunteerFunctions.aspx")
            End If
        End If
    End Sub

    Private Sub showgrid()
        'for local use 'Signs/' for App8 or App9 '/Signs/'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.FirstName +' '+ I.LastName as VName, I.AutoMemberID as MemberID, '/Signs/' + CSign.SignFileName as SignFileName  from contest C Inner Join IndSpouse I ON I.AutoMemberID = C.LeftSignID or I.AutoMemberID = C.RightSignID  Inner Join CertificateSign CSign ON CSign.MemberID = I.AutoMemberID where C.NSFchapterID=" & Session("selChapterID") & " and C.Contest_Year = YEAR(GETDATE())")
        If ds.Tables(0).Rows.Count > 0 Then
            GridMemberSign.DataSource = ds
            GridMemberSign.DataBind()
            GridMemberSign.Visible = True
        Else
            GridMemberSign.Visible = False
        End If
    End Sub

    Public Sub LoadVolunteer()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.FirstName +' '+ I.LastName as VName, I.AutoMemberID as MemberID from contest C Inner Join IndSpouse I ON I.AutoMemberID = C.LeftSignID or I.AutoMemberID = C.RightSignID  where C.NSFchapterID=" & Session("selChapterID") & " and C.Contest_Year = YEAR(GETDATE())")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlSign.DataSource = ds
            ddlSign.DataBind()
            ddlSign.Items.Add(New ListItem("Select Volunteer", "0"))
            ddlSign.SelectedIndex = ddlSign.Items.IndexOf(ddlSign.Items.FindByValue("0"))
            ddlSign.Enabled = True
            showgrid()
        Else
            GridMemberSign.Visible = False
            lblErr.Text = "No volunteers were assigned to sign the certificates"
            ddlSign.DataSource = Nothing
            ddlSign.DataBind()
            ddlSign.Enabled = False
        End If
    End Sub

    Protected Sub ddlSign_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlSign.SelectedItem.Text = "Select Volunteer" Then
            lblFilname.Text = " File name must be : " & ddlSign.SelectedItem.Text.Replace(" ", "_") & "_" & ddlSign.SelectedValue & ".jpg"
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from CertificateSign where MemberID = " & ddlSign.SelectedValue & "") > 0 Then
                btnUpload.Attributes.Add("onclick", "return confirm('Signature of Selected Volunteer Already Exist, Do you want to Replace it?');")
            Else
                btnUpload.Attributes.Clear()
            End If
            trupload.Visible = True
        Else
            lblFilname.Text = ""
            trupload.Visible = False
        End If
    End Sub

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim sFrom As String = "nsfcontests@gmail.com"
        'Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        Dim ok As Boolean = True
        Try
            client.Send(mail)
        Catch e As Exception
            ok = False
        End Try
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If FileUpload1.HasFile Then
            Dim fileExt As String = System.IO.Path.GetExtension(FileUpload1.FileName)
            Dim checkfile As String() = FileUpload1.FileName.ToString.Split(".")
            Dim filename As String = ddlSign.SelectedItem.Text.Replace(" ", "_") & "_" & ddlSign.SelectedValue
            If checkfile(0).ToLower <> filename.ToString().ToLower() Then
                lblErr.Text = "Filename mismatch"
                Exit Sub
            End If
            If fileExt.ToLower = ".jpg" Then
                Try
                    FileUpload1.PostedFile.SaveAs(Server.MapPath("/Signs/") & FileUpload1.FileName.Replace(" ", ""))
                    'FileUpload1.PostedFile.SaveAs(Server.MapPath("ScoreSheets/") & FileUpload1.FileName.Replace(" ", ""))
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from  CertificateSign where Memberid = " & ddlSign.SelectedValue & "") < 1 Then
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into CertificateSign(MemberID, SignFileName, CreateDate, CreatedBy) Values(" & ddlSign.SelectedValue & ",'" & FileUpload1.FileName.Replace(" ", "") & "',Getdate()," & Session("LoginID") & ")")
                    Else
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update CertificateSign set ModifyDate= Getdate(), ModifiedBy=" & Session("LoginID") & ",SignFileName='" & FileUpload1.FileName.Replace(" ", "") & "'  WHERE Memberid = " & ddlSign.SelectedValue & "")
                    End If
                    SendEmail("Signature Uploaded by CC of " & lblChapter.Text, "Dear Sir,<br> Chapter Co-ordinator of " & lblChapter.Text & " have uploaded Mr." & ddlSign.SelectedItem.Text & "'s Signature. Please do the needful.", "fsilva@redegginfoexpert.com")
                    lblErr.Text = "File Uploaded Successfully"
                    showgrid()
                Catch ex As Exception
                    lblErr.Text = ex.ToString()
                End Try
            Else
                lblErr.Text = "Only .jpg format Accepted"
            End If
        Else
            lblErr.Text = "No uploaded file"
        End If
    End Sub
End Class
