<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="VolUniqueEmail.aspx.vb" Inherits="VRegistration.VolUniqueEmail"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <table border="0" cellpadding="0" style="width:100%">
        <tr>
            <td align="left">
<table width="1000" cellpadding="0" cellspacing ="0" border="0">
				<tr>
					<td class="Heading" align="center" colspan="2">
                        Establish Unique Login (Email Address) for Volunteers</td>
				</tr>
				<tr>
					<td  align="center" colspan="2"><br /> <br /><asp:Label ID="lblMessage" runat="server" Width="500px"></asp:Label><br /></td></tr>
				<tr><td colspan = "2" align="center">
                <asp:Panel ID="PnlNew" runat="server"  Visible = "false">
                <table cellpadding = "4" cellspacing = "0" border="0" style ="width:720px">
                <tr><td  align="center" colspan = "3"><br /><br />Note: A volunteer should have a unique email address, separate from the spouse.<br /><br /></td></tr>		
				<tr><td align="left" style ="width:120px;font-family: Georgia,serif;font-size: 16px; font-weight:bold;color:#009900">Current</td>
				<td align="left" style ="width:300px;font-family: Georgia,serif;font-size: 16px; font-weight:bold;color:#009900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Spouse 1</td><td align="left" style ="width:300px;font-family: Georgia,serif;font-size: 16px; font-weight:bold; color:#009900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Spouse 2</td></tr>						
				<tr><td align="left">Name</td><td align="left"><asp:Label ID="lblIndName" runat="server" ></asp:Label></td><td  align="left"><asp:Label ID="lblIndSpName" runat="server" ></asp:Label> </td></tr>	
				<tr><td align="left">Email </td><td align="left"><asp:Label ID="lblIndEmail" runat="server" ></asp:Label> </td><td  align="left"><asp:Label ID="LblIndSpEmail" runat="server" ></asp:Label> </td></tr>	
				<tr><td align="left">Password </td><td align="left"><asp:Label ID="lblIndPwd" runat="server" ></asp:Label> </td><td  align="left"><asp:Label ID="lblIndSpPwd" runat="server"></asp:Label> </td></tr>	
				<tr><th align="left" colspan ="3" style ="font-family: Georgia,serif;font-size: 16px; font-weight:bold;color:#009900"> To make it unique, replace with:</th></tr>
				<tr><td align="left">Email </td><td align="left"> 
                    <asp:TextBox ID="txtIndEmail" runat="server"></asp:TextBox>
                   
                    <br />
                    <asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
                        ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtIndEmail"
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td><td  align="left"><asp:TextBox ID="txtIndSpEmail" runat="server"></asp:TextBox> 
                    
                    
                     <br />
                     <asp:RegularExpressionValidator
                        ID="RegularExpressionValidator2" runat="server" CssClass="smFont" Display="Dynamic"
                        ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtIndSpEmail"
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td></tr>	
				<tr><td align="left">Password </td><td align="left"><asp:TextBox ID="txtIndPwd" runat="server" TextMode="Password"></asp:TextBox> 
                </td><td style="width: 162px" align="left"><asp:TextBox ID="txtIndSpPwd" runat="server" TextMode="Password"></asp:TextBox> 
                </td></tr>	
				<tr><td align="left">Retype Password</td><td align="left"><asp:TextBox ID="txtIndCPwd" runat="server" TextMode="Password"></asp:TextBox> 
                    <br /><asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="smFont" Display="Dynamic" ControlToCompare="txtIndPwd"
                        ControlToValidate="txtIndCPwd" ErrorMessage="ReType Password Incorrect"></asp:CompareValidator></td><td style="width: 162px" align="left"><asp:TextBox ID="txtIndSpCPwd" runat="server" TextMode="Password"></asp:TextBox> 
                    <br /><asp:CompareValidator ID="CompareValidator2" runat="server" CssClass="smFont" Display="Dynamic" ControlToCompare="txtIndSpPwd"
                        ControlToValidate="txtIndSpCPwd" ErrorMessage="ReType Password Incorrect"></asp:CompareValidator>
                </td></tr>	
				<tr><td align="center" colspan="3"><asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td>
				</tr><tr><td align="center" colspan="3" style="height: 32px"><asp:Label ID="lblError" runat="server"></asp:Label><br />
                    <asp:Label ID="Label1" ForeColor="Red"  runat="server" Text="Please Enter the Email and Password in either Spouse 1 or Spouse 2"></asp:Label>
				</td></tr>
                </table>
                </asp:Panel>
                </td></tr>
                <tr><td colspan="2" align="center">
                    <asp:Panel ID="PnlNo" runat="server" Width="100%" Visible="false">
                    <table border="0" cellpadding="4" cellspacing = "2" align="center" style="width:500px">
                    <tr><td colspan ="2" align="left"><br /> <br /><p align="justify" style="font-size:13px; font-family:Georgia, Times, serif; ">Did you (or spouse) ever enter a profile with your name, address and so on ? Or did you (or spouse) ever donate to North South Foundation ? </p> </td></tr>
                    <tr><td align="left" colspan ="2" style="font-size:13px; font-family:Georgia, Times, serif; ">Please select</td></tr>
                    <tr><td style="width :25%">&nbsp;</td><td align="left" >
                        <asp:RadioButton ID="RBtnYes" runat="server" GroupName="RBTN" AutoPostBack="true" Text="Yes" /></td></tr>
                         <tr><td ></td><td align="left" >
                        <asp:RadioButton ID="RBtnNo" runat="server" GroupName="RBTN" AutoPostBack="true" Text="No" /></td></tr>
                    </table>
                    </asp:Panel>
                </td></tr>
                <tr><td colspan = "2" align="center">
               <table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr runat="Server" id="trContinue"  visible="false">
    <td align="left"><br /><br /> <p align="justify"  style="font-size:13px; font-family:Georgia, Times, serif; ">The email address you gave already exists in multiple places.  A message is being sent to nsfcontests@gmail.com for investigation.  You may also contact your contact at NSF on this matter.  Press to Continue</p>
           </td>
  </tr>
  <tr runat="Server" id="trMaill"  visible="false">
    <td align="left" ><br />
      <p align="justify"  style="font-size:13px; font-family:Georgia, Times, serif; ">We have your profile in our  system.&nbsp; Please use the email address  used in that profile to access your record or send an email to  nsfcontests@gmail.com explaining your situation or contact your contact at NSF.  Press to Continue</p>
      <p align="justify">&nbsp;</p>
      <br /></td>
  </tr>
  <tr runat="Server" >
    <td >  <center>       <asp:Button ID="btncontinue" runat="server" Text="Continue" Visible ="false" /></center></td>
  </tr>
</table>

               </td></tr>
			</table>
            </td></tr></table>
			
	</asp:Content>
 

 
 
 
