<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false"
    CodeFile="Wrkshop_Success.aspx.vb" Inherits="Wrkshop_Success" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table width="100%" id='tabId0'>
        <tr>
            <td class="Heading" align="center" colspan="2">
                2 Marissa Ct, Burr Ridge, IL60527<br />
                Tax ID:363659998<br />
                Receipt<br />
            </td>
        </tr>
        <tr>
            <td class="SmallFont" style="height: 17px">
                Parent Detailed Information
            </td>
        </tr>
        <tr>
            <td style="width: 819px">
                <table style="width: 671px" border="0">
                    <tr>
                        <td>
                            <asp:Label ID="lblParentName" runat="server" CssClass="SmallFont"></asp:Label></td>
                            <td align="right">
                            <asp:Label ID="lblPaymentDateMsg" Text="Payment Date :" runat="server" CssClass="SmallFont"></asp:Label></td>
                        <td align="left">
                            <asp:Label ID="lblPaymentDate" runat="server" CssClass="SmallFont" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAddress1" runat="server" CssClass="SmallFont"></asp:Label></td>
                            <td align="right">
                            <asp:Label ID="Label1" Text="Payment Reference :" runat="server" CssClass="SmallFont"></asp:Label></td>
                        <td align="left">
                            <asp:Label ID="lblReference" runat="server" CssClass="SmallFont" />
                        </td>
                    </tr>
                    <tr id="trAddress2" runat ="server">
                        <td>
                            <asp:Label ID="lblAddress2" runat="server" CssClass="SmallFont"></asp:Label></td>
                            <td colspan="2">&nbsp;</td>
                        
                    </tr>
                   <%-- <tr>
                        <td>
                            <asp:Label ID="lblCity" runat="server" CssClass="SmallFont"></asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td>
                            <asp:Label ID="lblStateZip" runat="server" CssClass="SmallFont"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblHomePhone" runat="server" CssClass="SmallFont"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr id="trChild1" runat="server">
            <td class="SmallFont">
                Child/Children Detailed Information
            </td>
        </tr>
        <tr>
            <td style="width: 819px">
                <asp:DataGrid ID="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="ECalendarID"
                    CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                    Width="100%">
                    <FooterStyle CssClass="GridFooter"></FooterStyle>
                    <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                    <AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
                    <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                    <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'
                                    CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="WorkShops Selected" HeaderStyle-Font-Bold="true"
                            HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblWorkShop" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>'
                                    CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                            <ItemTemplate>
                                <asp:Label ID="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:C2}") %>'
                                    CssClass="SmallFont">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Workshop Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                            HeaderStyle-ForeColor="#990000">
                            <ItemTemplate>
                                <asp:Label ID="lblEventDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate","{0:d}") %>'
                                    CssClass="SmallFont"></asp:Label>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                            HeaderStyle-ForeColor="#990000">
                            <ItemTemplate>
                                <asp:Label ID="lblChapter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>'
                                    CssClass="SmallFont"></asp:Label>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Location" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                            HeaderStyle-ForeColor="#990000">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Venue") %>'
                                    CssClass="SmallFont"></asp:Label>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                </td>
        </tr>
        <%--	<tr><td class="SmallFont">
					</td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>--%>
        <%--<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestDateInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>--%>
        <%--	<tr>
					<td class="ItemCenter" align="center" colspan="2">
					<asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="true"  Text="Please verify your selections for each child along with the event date, chapter and location. <br> The date and location are subject to change.  Please visit www.northsouth.org for the latest information.  Once paid, your payment is non-refundable"></asp:Label></td>
				</tr>--%>
    </table>
    <br />
    <table align="center" cellspacing="1" cellpadding="1" border="0" style="border-color: Gray;
        border-width: 1px; border-style: solid; width: 500px">
        <tr>
            <td colspan="3" align="right" class="GridHeader" style="color:Maroon">
                Tax-deductible
            </td>
        </tr>
        <tr>
        <td style="border-bottom-color:Black;border-style:solid;border-width:1px;"></td>
        </tr>
        <tr>
            <td style="width: 260px">
                Registration Fee
            </td>
            <td style="width: 150px" align ="Right">
                <asp:Label ID="lblRegFee" runat="server" />
            </td>
            <td style="width: 80px" align ="right">
                <asp:Label ID="lblRegFeeTax" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                Donation
            </td>
            <td align ="right">
                <asp:Label ID="lblDonation" runat="server" />
            </td>
            <td align ="right">
                <asp:Label ID="lblDonationTax" runat="server" />
            </td>
        </tr>
        <%--	<tr>
			<td>
			Meals
			</td>
			<td>
			<asp:Label ID="lblMeals" runat="server" />
			</td>
			<td>
			<asp:Label ID="lblMealsTax" runat="server" />
			</td>

			</tr>--%>
        <tr>
            <td>
                Total Amount Paid
            </td>
            <td align ="Right">
                <asp:Label ID="lblTotAmt" runat="server" />
            </td>
            <td align ="right">
                <asp:Label ID="lblTotAmtTax" runat="server" />
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="lblNoteMsg" style="color:blue" runat="server" Visible="false" />
            </td>
        </tr>
        <TR>
					<TD class="largewordingbold"><asp:label id="lblEmailStatus" runat="server"></asp:label></TD>
				</TR>
    </table>
    
    <table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left" colspan="2">
						<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
				</tr>
			</table>
</asp:Content>
