﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class ViewSATTestScores
    Inherits System.Web.UI.Page
    Dim User As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not IsPostBack() Then
            Try
                Dim year As Integer = 0
                year = Now.Year
                'If Now.Month <= 5 Then
                '    year = Now.Year - 1
                'Else
                '    year = Now.Year
                'End If
                ' year = Convert.ToInt32(DateTime.Now.Year)


                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year + 1) + "-" + Convert.ToString(year + 2)), Convert.ToString(year + 1)))
                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1)), Convert.ToString(year)))
                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 1) + "-" + Convert.ToString(year)), Convert.ToString(year - 1)))
                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 2) + "-" + Convert.ToString(year - 1)), Convert.ToString(year - 2)))

                'ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
                'ddlEventYear.Items.Insert(1, Convert.ToString(year))
                'ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
                'ddlEventYear.Items.Insert(3, Convert.ToString(year - 2))

                'ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))

                If Now.Month >= 3 Then
                    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
                    Dim iMaxYear As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "if not exists(select 1 from coachreg where approved='y' and eventyear=year(GetDate())) begin select  max(eventyear) from coachreg where  approved='y'  End Else select 0")
                    If iMaxYear > 0 Then
                        ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(iMaxYear)))
                    End If
                Else
                    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year - 1)))
                End If

                ''Options for Volunteer Name Selection
                If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    TrInsVol.Visible = False
                    TrInsParStd.Visible = True
                    lnkVolFunction.Visible = False
                    Session("User") = True
                    TrStudReport.Visible = False
                    lblNote.Visible = False

                    TrParent.Visible = True
                    TrCoach.Visible = False

                    'btnGradeCard_Parent.Visible = True
                    If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
                        lnkParentPage.Visible = True
                        lnkstudentPage.Visible = False
                        Loadchild(Convert.ToInt32(Session("CustIndID")), 0)
                    ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                        lnkParentPage.Visible = False
                        lnkstudentPage.Visible = True
                        Loadchild(Convert.ToInt32(Session("CustIndID")), Convert.ToInt32(Session("StudentId")))

                    End If
                ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                    TrInsVol.Visible = True
                    TrInsParStd.Visible = False
                    lnkParentPage.Visible = False
                    lnkVolFunction.Visible = True
                    lnkstudentPage.Visible = False
                    Session("User") = False
                    TrStudReport.Visible = True
                    lblNote.Visible = True
                    'TrParent.Visible = False
                    TrCoach.Visible = True

                    'btnGradeCard_Parent.Visible = False

                    TdChild.Visible = False
                    Td_ddlChild.Visible = False
                    Dim TeamLead As String = ""
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=" & Session("RoleID") & " and V.TeamLead='Y' and V.MemberID=" & Session("LoginID")) > 0 Then
                        TeamLead = "Y"
                    Else
                        TeamLead = "N"
                    End If

                    If Session("RoleID") = 88 And TeamLead = "N" Then
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I inner join CoachReg CR On CR.CMemberID = I.AutoMemberID where CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " and CR.CMemberID=" & Session("LoginID") & "  order by I.FirstName,I.LastName")

                        If ds.Tables(0).Rows.Count > 0 Then
                            ddlVolName.DataSource = ds
                            ddlVolName.DataBind()
                        Else
                            lblScoreErr.Text = "Volunteer not present for Calendar Sign up for the selected year"
                        End If
                    ElseIf (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 89 Or (Session("RoleID") = 88 And TeamLead = "Y") Or Session("RoleID") = 96) Then 'Coach Admin can schedule for other Coaches. 

                        ddlVolName.Visible = True
                        '********* To display all Coaches ****************'
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CoachReg CR On CR.CMemberID = I.AutoMemberID Where  CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " order by I.FirstName,I.LastName") ' V.RoleId in (88) order by I.LastName,I.FirstName")

                        If ds.Tables(0).Rows.Count > 0 Then
                            ddlVolName.DataSource = ds
                            ddlVolName.DataBind()
                            If ds.Tables(0).Rows.Count > 0 Then
                                ddlVolName.Items.Insert(0, New ListItem("Select Coach", -1))
                                ddlVolName.SelectedIndex = 0
                            End If
                        Else
                            lblScoreErr.Text = "No Volunteers present for Calendar Sign up"
                        End If

                    Else
                        ddlVolName.Visible = False
                    End If

                    If ddlVolName.Items.Count = 1 Then
                        LoadCoaches()
                        LoadProductGroup(False)
                        BindddlStudentID()
                    End If
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End If

    End Sub
    Private Sub LoadCoaches()
        '**********To get Products assigned for Volunteers RoleIs =1,2,88,89****************'
        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then 'All Products
            LoadProductGroup(False)
        ElseIf Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then 'Team Lead='Y' gets Products From Volunteer Table
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & ddlVolName.SelectedValue & " and RoleId in(88,89) and TeamLead='Y' and [National]='Y' and ProductId is not Null") > 1 Then
                'more than one 
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & ddlVolName.SelectedValue & " and RoleId in(88,89)" & " and ProductId is not Null ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                LoadProductGroup(False)
            Else 'TeamLead<>'Y' gets Products from CoachReg table
                'only one
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " Select ProductGroupID,ProductID from CoachReg where CMemberid=" & ddlVolName.SelectedValue & " and EventYear =" & ddlEventYear.SelectedValue & " and ProductId is not Null ") 'and RoleId=" & Session("RoleId") & "
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                'If ds.Tables(0).Rows.Count > 0 Then
                '    prd = ds.Tables(0).Rows(0)(1).ToString()
                '    Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                '    lblPrd.Text = prd
                '    lblPrdGrp.Text = Prdgrp
                'End If
                'LoadProductGroup(False)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                LoadProductGroup(False)
            End If
        End If
    End Sub
    Private Sub LoadProductGroup(ByVal User As Boolean)
        Try
            Dim strSql As String
            If Session("User") = False Then
                strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN CoachReg CR ON CR.ProductGroupID = P.ProductGroupID AND CR.EventId = P.EventId where CR.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "")
                If ddlVolName.SelectedValue <> "-1" Then
                    strSql = strSql & " and CR.CMemberID=" & ddlVolName.SelectedValue & ""
                End If
                strSql = strSql & "  order by P.ProductGroupID"
            ElseIf Session("User") = True Then
                strSql = "SELECT Distinct P.[Name], P.ProductGroupID from dbo.[ProductGroup] P INNER JOIN dbo.[CoachReg] CR  ON P.ProductGroupid =CR.ProductGroupid "
                strSql = strSql & " Where CR.PMemberID =" & Session("CustIndID") & " and CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " and CR.ChildNumber=" & ddlChild_Par.SelectedValue & " and P.EventId =" & ddlEvent.SelectedValue
            End If

            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblScoreErr.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, New ListItem("Select Product Group", -1))
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
                LoadProductID()
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
                LoadLevel(Session("User"))
                LoadSession(Session("User"))
            End If
        Catch ex As Exception
            lblScoreErr.Text = ex.ToString()
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ddlProduct.SelectedIndex = 0
                ddlProduct.Enabled = False
            Else
                Dim strSql As String
                'Volunteer
                If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" Then
                    strSql = "Select Distinct P.ProductID, P.Name from Product P INNER JOIN CoachReg CR ON CR.ProductID = P.ProductID AND CR.EventId = P.EventId  where CR.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " AND P.ProductGroupID =" & ddlProductGroup.SelectedValue  'and P.Status='O'
                    If ddlVolName.SelectedValue <> "-1" Then
                        strSql = strSql & " AND CR.CMemberID=" & ddlVolName.SelectedValue & ""
                    End If
                    strSql = strSql & " ORDER BY P.ProductID "
                ElseIf Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT" Then
                    strSql = "SELECT distinct P.[Name], P.ProductID from dbo.[Product] P INNER JOIN dbo.[CoachReg] CR ON P.ProductGroupid = CR.ProductGroupid and P.ProductID = CR.ProductID"
                    strSql = strSql & " Where CR.PMemberID =" & Session("CustIndID") & " and CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " and CR.ChildNumber=" & ddlChild_Par.SelectedValue & " and P.EventId =" & ddlEvent.SelectedValue
                    strSql = strSql & " and P.ProductGroupId=" & ddlProductGroup.SelectedValue
                End If

                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, New ListItem("Select Product", -1))
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    LoadLevel(Session("User"))
                    'LoadSession(User)
                End If
            End If
        Catch ex As Exception
            lblScoreErr.Text = lblScoreErr.Text & "<br>" & ex.ToString
        End Try
    End Sub
    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            Loadchild(Convert.ToInt32(Session("CustIndID")), Convert.ToInt32(Session("StudentId")))
        ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False

            ClearScoreGrids()
            ClearReportGrids()
            If ddlVolName.SelectedValue <> "-1" Then
                LoadCoaches()
                LoadProductGroup(False)
                BindddlStudentID()
            End If


            'If ddlVolName.Items.Count = 1 Then
            '    LoadCoaches()
            '    LoadProductGroup(False)
            '    BindddlStudentID()
            'End If
        End If



    End Sub

    Private Sub LoadLevel(ByVal User As Boolean) ', ByVal ProductGroupId As Integer)
        Try
            Dim StrSQL As String
            ddlLevel.Items.Clear()
            ddlLevel.Enabled = True
            If (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                StrSQL = "SELECT DISTINCT Level FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and PMemberID = " & Session("CustIndId") & " AND ChildNumber = " & ddlChild_Par.SelectedValue & " and  Approved ='Y' " 'and ProductGroupID=" & ddlProductGroup.SelectedValue
            Else
                If ddlVolName.SelectedItem.Text.Contains("Select") Then
                    lblScoreErr.Text = " Please Select Coach"
                    Exit Sub
                End If
                StrSQL = "SELECT DISTINCT Level FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and CMemberID = " & ddlVolName.SelectedValue & " and  Approved ='Y' " ' AND ChildNumber = " & ddlChild.SelectedValue & "  and ProductGroupID=" & ddlProductGroup.SelectedValue
            End If

            If Not ddlProductGroup.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductGroupID = " & ddlProductGroup.SelectedValue
            End If

            If Not ddlProduct.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductID = " & ddlProduct.SelectedValue
            End If

            'Response.Write(StrSQL)
            Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            If dsGrade.Tables(0).Rows.Count > 0 Then
                ddlLevel.DataSource = dsGrade
                ddlLevel.DataBind()
                ddlLevel.Enabled = True 'False
            End If
            If ddlLevel.Items.Count = 0 Then
                ddlLevel.SelectedValue = "-1"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadSession(ByVal User As Boolean)
        Try
            Dim StrSQL As String
            ddlSession.Items.Clear()
            ddlSession.Enabled = True

            If (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                StrSQL = "SELECT DISTINCT SessionNo FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and PMemberID = " & Session("CustIndId") & " AND ChildNumber = " & ddlChild_Par.SelectedValue & " and  Approved ='Y' " 'and ProductGroupID=" & ddlProductGroup.SelectedValue
            Else
                If ddlVolName.SelectedItem.Text.Contains("Select") Then
                    lblScoreErr.Text = " Please Select Coach"
                    Exit Sub
                Else
                    lblScoreErr.Text = ""
                    StrSQL = "SELECT DISTINCT SessionNo FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and CMemberID = " & ddlVolName.SelectedValue & " and  Approved ='Y' " ' " AND ChildNumber = " & ddlChild.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue
                End If
            End If
            If Not ddlProductGroup.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductGroupID = " & ddlProductGroup.SelectedValue
            End If

            If Not ddlProduct.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductID = " & ddlProduct.SelectedValue
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlSession.DataSource = ds
                ddlSession.DataBind()
                ddlSession.Enabled = False
            End If
            If ddlSession.Items.Count = 0 Then
                ddlSession.SelectedValue = "-1"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub Loadchild(ByVal ParentID As Integer, ByVal ChildNumber As Integer)
        Try
            lblScoreErr.Text = ""
            ''Dim strSql As String = "select Distinct C.First_name+' '+C.Last_name as ChildName, C.ChildNumber,C.Grade From Child C Inner Join ChildTestSummary CR on CR.MemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber where C.memberid=" ParentID & " and CR.EventYear=" & ddlEventYear.SelectedValue
            Dim strSql As String = "select Distinct C.First_name + ' ' + C.Last_name as ChildName, C.ChildNumber,C.Grade,C.First_name, C.Last_name From Child C Inner Join CoachReg CR on CR.PMemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber where C.memberid=" & ParentID & " and CR.EventYear=" & ddlEventYear.SelectedValue
            If ChildNumber > 0 Then
                strSql = strSql & " and C.ChildNumber=" & ChildNumber
            End If
            'strSql = strSql & " and CR.Completed='Y' and CR.EventYear =Year(GetDATE())"
            strSql = strSql & " and CR.Approved='Y' and CR.EventYear =" & ddlEventYear.SelectedValue & " order by C.First_name, C.Last_name" 'Year(GetDATE())"

            Dim dsChild As DataSet
            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strSql)
            TdChild.Visible = True
            Td_ddlChild.Visible = True

            ddlChild_Par.DataSource = dsChild
            ddlChild_Par.DataBind()
            ddlChild_Par.Enabled = True


            If (ddlChild_Par.Items.Count < 1) Then
                ddlChild_Par.SelectedValue = "-1"
                lblScoreErr.Text = "Child(ren) has/have not yet taken the test."
                Btn_Submit.Enabled = False
                DGCoachPapers.DataSource = Nothing
                DGCoachPapers.DataBind()
            ElseIf (ddlChild_Par.Items.Count > 1) Then
                Btn_Submit.Enabled = True
                ddlChild_Par.Items.Insert(0, New ListItem("Select", "0"))
                ddlChild_Par.Items(0).Selected = True
                ddlChild_Par.Enabled = True
            Else
                Btn_Submit.Enabled = True
                ddlChild_Par.Enabled = False
                LoadProductGroup(True)
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Protected Sub BindddlStudentID()
        Try
            Dim ds As DataSet = New DataSet()
            Dim StrSQL As String = "SELECT Distinct C.ChildNumber,C.FIRST_NAME + '  ' + C.LAST_NAME as ChildName,C.FIRST_NAME , C.LAST_NAME  FROM Child C Inner Join CoachReg CR on CR.PMemberID = C.MEMBERID and CR.ChildNumber =C.ChildNumber WHERE C.Grade>=4 and CR.Approved='Y' and CR.EventYear =" & ddlEventYear.SelectedValue & " and CR.CMemberID=" & ddlVolName.SelectedValue

            If ddlProductGroup.SelectedValue > 0 Then
                StrSQL = StrSQL & " and CR.ProductGroupID =" & ddlProductGroup.SelectedValue
            End If
            If ddlProduct.SelectedValue > 0 Then
                StrSQL = StrSQL & " and CR.ProductID =" & ddlProduct.SelectedValue
            End If
            If ddlLevel.SelectedItem.Text <> "-1" Then
                StrSQL = StrSQL & " and CR.Level ='" & ddlLevel.SelectedItem.Text & "'"
            End If
            If ddlSession.SelectedValue > 0 Then
                StrSQL = StrSQL & " and CR.SessionNo =" & ddlSession.SelectedValue
            End If

            StrSQL = StrSQL & " order by C.FIRST_NAME , C.LAST_NAME"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlChild.DataSource = ds
                ddlChild.DataBind()
            End If
            If ds.Tables(0).Rows.Count > 1 Then
                ddlChild.Items.Insert(0, New ListItem("Select Child", "-1"))
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ViewGrid()
        Try
            ClearScoreGrids()
            If lblScoreErr.Text <> "" Then
                DisableGrids()
                Exit Sub
            Else
                LoadCoachPapers()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub DGCoachPapers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

        Dim CoachPaperID As Integer = Convert.ToInt32(e.Item.Cells(2).Text)
        Session("CoachPaperID") = CoachPaperID
        For i As Integer = 0 To DGCoachPapers.Items.Count - 1
            DGCoachPapers.Items(i).BackColor = Color.White
        Next
        If e.CommandName = "Select" And (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            e.Item.BackColor = Color.Gainsboro
            GetScores(CoachPaperID, True)
        Else
            e.Item.BackColor = Color.Gainsboro
            ClearReportGrids()
            ClearScoreGrids()
            BtnMissedAnswers.Enabled = True
            BtnHWSubmission.Enabled = True
            BtnViewSCores.Enabled = True
            BtnGradeCard.Enabled = True
            BtnCalcualteScores.Enabled = True
        End If
    End Sub
    Protected Sub DGCoachPapers_ItemDataBound(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)


    End Sub
    Private Sub LoadCoachPapers()
        Try
            Dim sqlCommand As String = "usp_GetChildAnswerSheet"
            Dim param(8) As SqlParameter
            param(0) = New SqlParameter("@PaperType", ddlPaperType.SelectedValue)
            param(1) = New SqlParameter("@ProductGroupID", ddlProductGroup.SelectedValue)
            param(2) = New SqlParameter("@ProductID", ddlProduct.SelectedValue)
            param(3) = New SqlParameter("@PMemberId", IIf(Convert.ToInt32(Session("CustIndID")) > 0, Convert.ToInt32(Session("CustIndID")), DBNull.Value))
            param(4) = New SqlParameter("@Level", ddlLevel.SelectedItem.Text)
            param(5) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
            param(6) = New SqlParameter("@ChildId", IIf(Td_ddlChild.Visible = True, IIf(ddlChild_Par.SelectedValue > 0, ddlChild_Par.SelectedValue, DBNull.Value), IIf(ddlChild.SelectedValue > 0, ddlChild.SelectedValue, DBNull.Value)))
            param(7) = New SqlParameter("@MemberID", ddlVolName.SelectedValue)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)

            If ds.Tables(0).Rows.Count > 0 Then
                lblCoachPapers.Visible = True
                DGCoachPapers.DataSource = ds
                DGCoachPapers.DataBind()
            Else
                lblCoachPapers.Visible = False
                DGCoachPapers.DataSource = Nothing
                DGCoachPapers.DataBind()
                lblScoreErr.Text = "Test Papers not yet released"
                Exit Sub
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub DisableGrids()
        GVAnswers.Visible = False
        GVTotalScore.Visible = False
        GVScaledScore.Visible = False
    End Sub
    Private Sub ClearScoreGrids()
        TrAnsExp.Visible = False
        lblScoreErr.Text = ""
        lblScores.Visible = False
        GVTotalScore.Visible = False
        GVTotalScore.DataSource = Nothing
        GVTotalScore.DataBind()

        GVAnswers.Visible = False
        GVAnswers.DataSource = Nothing
        GVAnswers.DataBind()
        btnGVAnsUpdate.Visible = False
        lblCScoreErr.Text = ""

        GVScaledScore.Visible = False
        GVScaledScore.DataSource = Nothing
        GVScaledScore.DataBind()
    End Sub
    Private Sub ClearReportGrids()
        lblMissedAns.Visible = False
        TrSort.Visible = False
        DGMissedAns.DataSource = Nothing
        DGMissedAns.DataBind()

        'Dg_HwSubmit.Visible = False
        lblHW_Sub.Visible = False
        Dg_HwSubmit.DataSource = Nothing
        Dg_HwSubmit.DataBind()

    End Sub
    Private Sub GetScores(ByVal CoachPaperID As Integer, ByVal ParentFlag As Boolean)

        Try
            Dim flag As Boolean = False
            Dim dsScale As DataSet
            lblScoreErr.Text = ""

            '******Child Test Answers for each Question******************************************************'
            Dim StrSQL As String
            StrSQL = " Select Distinct Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,Ch.Answer,SA.CorrectAnswer,Ch.Score,Ch.CScore as CscorePar,Ch.CScore,IsNull(SA.Manual,'') as Manual From"
            StrSQL = StrSQL + " ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID and SA.SectionNumber =Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
            StrSQL = StrSQL + " Inner join ChildTestSummary CS on CS.CoachPaperId= Ch.CoachPaperID and CS.ChildNumber =Ch.ChildNumber  and CS.MemberID =Ch.MemberID"
            StrSQL = StrSQL + " WHERE Ch.ChildNumber=" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & " and Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperID & " and CS.Completed='Y'" 'and Ch.Score is not null"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL) '
            '"Select Distinct CT.CoachPaperId,CT.SectionNumber,CT.QuestionNumber,CT.Answer,CT.Score from ChildTestAnswers CT  WHERE CT.ChildNumber=" & ddlChild.SelectedValue & " and CT.EventYear=" & Now.Year & " and CT.CoachPaperID=" & CoachPaperID & " and CT.Score is not null")


            If ds.Tables(0).Rows.Count > 0 Then
                'BtnExport.Visible = True
                lblScores.Visible = True
                GVAnswers.Visible = True
                GVAnswers.DataSource = ds
                GVAnswers.DataBind()
                If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then '"PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    btnGVAnsUpdate.Visible = True
                    GVAnswers.Columns(6).Visible = False
                Else
                    btnGVAnsUpdate.Visible = False
                    GVAnswers.Columns(7).Visible = False
                    GVAnswers.Columns(8).Visible = False
                End If
            Else
                lblScores.Visible = False
                GVAnswers.Visible = False
                GVAnswers.DataSource = Nothing
                GVAnswers.DataBind()
                lblScoreErr.Text = " Child has not yet completed the test."
            End If

            '****************************Raw Scores from Child Test Summary************************************************************'
            Dim StrTot As String = "Select Distinct CT.CoachPaperID,CT.S1Score,CT.S2Score,CT.S3Score,CT.S4Score,CT.S5Score,CT.WSRawScore,CT.CRRawScore,CT.MathRawScore,CT.TotalRawScore from ChildTestSummary CT  WHERE CT.ChildNumber=" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & " and CT.EventYear=" & ddlEventYear.SelectedValue & " and CT.CoachPaperID=" & CoachPaperID 'Now.Year
            Dim dsTotScore As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrTot)
            If dsTotScore.Tables(0).Rows.Count > 0 Then
                'BtnExport.Visible = True
                GVTotalScore.Visible = True
                GVTotalScore.DataSource = dsTotScore
                GVTotalScore.DataBind()
            Else
                GVTotalScore.Visible = False
                GVTotalScore.DataSource = Nothing
                GVTotalScore.DataBind()

                GVAnswers.Visible = False
                GVAnswers.DataSource = Nothing
                GVAnswers.DataBind()
                lblScoreErr.Text = " Child has not yet completed the test."
                'lblscoreerr.Text = "No Child Scores present to show"
            End If

            If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                If lblScoreErr.Text <> "" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CoachRelDates where CoachPaperID=" & CoachPaperID & " and GETDATE()> QDeadlineDate and AReleaseDate < GETDATE() and MemberID =" & ddlVolName.SelectedValue) > 0 Then
                    Session("CoachPaperID") = CoachPaperID
                    'lblScoreErr.Text = ""
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from EnterAnsException where CoachPaperID =" & Session("CoachPaperID") & " and MemberID=" & ddlVolName.SelectedValue & " and Childnumber=" & ddlChild.SelectedValue) > 0 Then
                        lblScoreErr.Text = "Child has not submitted the answers."
                        TrAnsExp.Visible = False
                    Else
                        lblScoreErr.Text = ""
                        TrAnsExp.Visible = True
                    End If
                Else
                    TrAnsExp.Visible = False
                End If
            End If

            '*****************Scaled Scores From Child Test Summary*****************************'
            dsScale = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT CoachPaperID,WSScaledScore,CRScaledScore,MathScaledScore FROM ChildTestSummary WHERE ChildNumber =" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & "  AND EventYear=" & ddlEventYear.SelectedValue & " and TotalScaledScore is not null and CoachPaperID=" & CoachPaperID)
            If dsScale.Tables(0).Rows.Count > 0 Then
                GVScaledScore.Visible = True
                GVScaledScore.DataSource = dsScale
                GVScaledScore.DataBind()
            Else
                GVScaledScore.Visible = False
                GVScaledScore.DataSource = Nothing
                GVScaledScore.DataBind()
            End If

            'BtnExport.Visible = True
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblScoreErr.Text = ""
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False

            ClearScoreGrids()
            ClearReportGrids()

            If ddlProductGroup.SelectedIndex <> 0 Then
                LoadProductID()
                LoadLevel(Session("User"))
                LoadSession(Session("User"))
                'LoadGrid()
            End If

            If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" Then
                BindddlStudentID()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            lblScoreErr.Text = ""
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False

            ClearScoreGrids()
            ClearReportGrids()

            If ddlProduct.SelectedValue = "Select Product" Then
                lblScoreErr.Text = "Please select Valid Product"
            Else
                LoadLevel(Session("User"))
                'LoadSession(User)
                'LoadGrid()
            End If

            If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" Then
                BindddlStudentID()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Clear()
        ClearScoreGrids()
        ClearReportGrids()

        If Session("entryToken").ToString().ToUpper() = "PARENT" Then
            LoadProductGroup(Session("User"))
        End If
        'Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Grade From child where childNumber=" & ddlChild.SelectedValue)
        'If dsGrade.Tables(0).Rows.Count > 0 Then

        '    'hdnChGrade.Value = dsGrade.Tables(0).Rows(0)("Grade").ToString()
        'End If
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        DGCoachPapers.DataSource = Nothing
        DGCoachPapers.DataBind()
        lblCoachPapers.Visible = False

        ClearScoreGrids()
        ClearReportGrids()
        If ddlVolName.SelectedValue <> "-1" Then
            LoadCoaches()
            LoadProductGroup(False)
            BindddlStudentID()
        End If
    End Sub

    Protected Sub Btn_Submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Btn_Submit.Click
        ClearScoreGrids()
        ClearReportGrids()
        Try
            If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" And ddlVolName.SelectedValue = "-1" Then 'And Session("User") = False Then
                lblScoreErr.Text = "Please Select Coach"
                Exit Sub
            ElseIf Session("entryToken").ToString().ToUpper() = "PARENT" And ddlChild_Par.SelectedItem.Value = "-1" Then '.Text.Contains("Select") Then
                lblScoreErr.Text = "Please select Child"
                Exit Sub
            End If

            If ddlPaperType.SelectedValue = "-1" Then '.SelectedItem.Text.Contains("Select") Then
                lblScoreErr.Text = "Please select PaperType"
            ElseIf ddlProductGroup.SelectedValue <= 0 Then
                lblScoreErr.Text = "Please select Product Group"
            ElseIf ddlProduct.SelectedValue <= 0 Then '.SelectedItem.Text.Contains("Select") Then
                lblScoreErr.Text = "Please select Product"
            Else
                lblScoreErr.Text = ""
            End If
            If lblScoreErr.Text <> "" Then
                Exit Sub
            Else
                btnGradeCard_Parent.Enabled = True
                ViewGrid()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Protected Sub BtnMissedAnswers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnMissedAnswers.Click
        Try
            ddlChild.Enabled = False
            ClearScoreGrids()
            ClearReportGrids()

            GetCoachPaperID()
            Dim CoachPaperId As Integer
            CoachPaperId = Session("CoachPaperID")
            'If hdnCoachPaperID.Value <> "" Then
            '    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            'End If

            If CoachPaperId > 0 Then

                'Check if ChildTestAnswers has data--If child has completed the test
                If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'")) = 0 Then
                    lblScoreErr.Text = "No child has taken the test."
                    Exit Sub
                End If


                Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,"
                StrSQL = StrSQL & "(Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber and C.Score=1 and CR.CMemberID = " & ddlVolName.SelectedValue & ") as CorrectCount, "
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score!=1) as IncorrectCount, "
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score is not null) as TotalCount"
                StrSQL = StrSQL & " From ChildTestAnswers Ch Inner Join CoachReg CR on CR.Childnumber = Ch.Childnumber and CR.PMemberID =Ch.MemberID INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
                StrSQL = StrSQL & " WHERE Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperId & " and CR.CMemberID =" & ddlVolName.SelectedValue & " and  Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc "

                'Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber," 'Ch.Answer,SA.CorrectAnswer,Ch.Score,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score=1) as CorrectCount,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score!=1) as IncorrectCount,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score is not null) as TotalCount"
                'StrSQL = StrSQL & " From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                'StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & ""
                'StrSQL = StrSQL & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc"
                'Response.Write(StrSQL)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

                If ds.Tables(0).Rows.Count > 0 Then
                    DGMissedAns.DataSource = ds
                    DGMissedAns.DataBind()
                    lblMissedAns.Visible = True
                    TrSort.Visible = True
                Else
                    lblScoreErr.Text = "No child has taken the test."
                    TrSort.Visible = False
                End If
            Else
                lblScoreErr.Text = "No Coach Papers present."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnHWSubmission_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHWSubmission.Click
        Try
            ddlChild.Enabled = False
            ClearScoreGrids()
            ClearReportGrids()

            GetCoachPaperID()
            Dim CoachPaperId As Integer
            CoachPaperId = Session("CoachPaperID")
            'If hdnCoachPaperID.Value <> "" Then
            '    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            'End If

            If CoachPaperId > 0 Then
                Dim StrSQL As String = ""
                '"Select Distinct CT.EventYear ,CT.CoachPaperID ,C.ChildNumber,C.FIRST_NAME + '' + C.LAST_NAME as StudentName,CT.Completed from Child C Inner join ChildTestSummary CT on C.ChildNumber = Ct.ChildNumber and C.MEMBERID =CT.MemberID "
                'StrSQL = StrSQL & " where CT.EventYear =" & ddlEventYear.SelectedValue & " And CT.CoachPaperID = " & CoachPaperId
                Dim Level As String
                If ddlProductGroup.SelectedItem.Text = "SAT" Or ddlProductGroup.SelectedItem.Text = "Math" Then
                    Level = ddlLevel.SelectedItem.Text
                Else
                    Level = ""
                End If

                If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'")) = 0 Then
                    lblScoreErr.Text = "No child has taken the test."
                    Exit Sub
                End If

                StrSQL = StrSQL & " Select Distinct CR.EventYear," & CoachPaperId & " as CoachPaperID ,C.ChildNumber,C.FIRST_NAME as FirstName, C.LAST_NAME as LastName,C.FIRST_NAME + ' ' + C.LAST_NAME as StudentName,CR.SessionNo,(Select Distinct Completed from ChildTestSummary where CoachPaperID =" & CoachPaperId & " and MemberID =CR.PMemberID and ChildNumber=CR.ChildNumber and EventYear =" & ddlEventYear.SelectedValue & ") as Completed,CR.CMemberID "
                StrSQL = StrSQL & " from  CoachReg CR Inner join Child C on CR.ChildNumber = C.ChildNumber and CR.PMemberID =C.MEMBERID "
                ' StrSQL = StrSQL & " Left join ChildTestSummary CT on CR.ChildNumber = CT.ChildNumber and CR.PMEMBERID =CT.MemberID and CR.EventYear =CT.EventYear"
                StrSQL = StrSQL & " where CR.Approved='Y' and  CR.EventYear =" & ddlEventYear.SelectedValue & " and CR.CMemberID = " & ddlVolName.SelectedValue
                StrSQL = StrSQL & " and CR.ProductID=" & ddlProduct.SelectedValue & " and CR.Level='" & ddlLevel.SelectedItem.Text & "' and CR.SessionNo=" & ddlSession.SelectedValue & " Order By Completed Desc,FirstName,LastName Asc "

                'Response.Write(StrSQL & "<br />")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dg_HwSubmit.DataSource = ds
                    Dg_HwSubmit.DataBind()
                    lblHW_Sub.Visible = True
                Else
                    lblScoreErr.Text = "No Data Present for the Coach"
                End If
            Else
                lblScoreErr.Text = "No Coach Papers present."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnViewSCores_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewSCores.Click
        ClearScoreGrids()
        ClearReportGrids()

        ddlChild.Enabled = True
        If ddlChild.SelectedValue = -1 Then
            lblScoreErr.Text = "Please Select Child Name"
            Exit Sub
        Else
            lblScoreErr.Text = ""
            GetScores(Session("CoachPaperID"), False)
        End If
    End Sub

    Protected Sub BtnGradeCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGradeCard.Click
        ClearScoreGrids()
        ClearReportGrids()
        If ddlChild.SelectedIndex = 0 Then
            lblScoreErr.Text = "Please Select Child Name."
        Else
            Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" & ddlChild.SelectedValue & "&EventYear=" & ddlEventYear.SelectedValue & "&ProductGroup=" & ddlProductGroup.SelectedValue & "&Product=" & ddlProduct.SelectedValue & "&Level=" & ddlLevel.SelectedItem.Text & "','_blank','fullscreen=yes,left=150,top=0,toolbar=0,location=0,scrollbars=1');</script> ")
        End If
    End Sub
    Private Sub GetCoachPaperID()
        Dim StrSQL As String = " SELECT DISTINCT C.[CoachPaperId] FROM  CoachRelDates C INNER JOIN CoachReg CR ON C.MemberID = CR.CMemberID and C.EventYear=CR.EventYear and C.ProductGroupId =C.ProductgroupId and C.ProductID =CR.ProductID and C.Level=CR.Level AND C.[Session]= CR.SessionNo "
        StrSQL = StrSQL + " WHERE C.ProductGroupID = ISNULL(" & ddlProductGroup.SelectedValue & ", C.ProductGroupCode) AND C.ProductID = ISNULL (" & ddlProduct.SelectedValue & ",CR.ProductID) AND C.[Level] = ISNULL ('" & ddlLevel.SelectedItem.Text & "',CR.Level)"
        StrSQL = StrSQL + " And C.Session=" & ddlSession.SelectedValue & " AND CR.CMemberID = " & ddlVolName.SelectedValue & "  AND C.EventYear =" & ddlEventYear.SelectedValue & " "

        hdnCoachPaperID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL)
    End Sub

    Protected Sub BtnCalcualteScores_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCalcualteScores.Click
        lblScoreErr.Text = ""
        ' GetCoachPaperID()
        Dim sqlCommand As String = "usp_CalcSATStudentScores_Overall"
        Dim sqlCommand1 As String = "usp_CalcSATScaledTestScores_Overall"

        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
        param(1) = New SqlParameter("@CoachPaperID", Session("CoachPaperID"))

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)
        '******************Calcualtion Of Scores for Each Question******************************************'
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)
        '********************Score Summary to ChildTestSummary *********************************************'
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand1, param)

        lblScoreErr.Text = "Scores Calculated Successfully."


    End Sub

    Protected Sub btnSort_Ques_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort_Ques.Click
        Try
            ddlChild.Enabled = False
            ClearScoreGrids()
            ClearReportGrids()

            GetCoachPaperID()
            Dim CoachPaperId As Integer
            CoachPaperId = Session("CoachPaperID")
            'If hdnCoachPaperID.Value <> "" Then
            '    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            'End If

            If CoachPaperId > 0 Then

                'Check if ChildTestAnswers has data--If child has completed the test
                If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'")) = 0 Then
                    lblScoreErr.Text = "No child has taken the test."
                    Exit Sub
                End If


                Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,"
                StrSQL = StrSQL & "(Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber and C.Score=1 and CR.CMemberID = " & ddlVolName.SelectedValue & ") as CorrectCount, "
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score!=1) as IncorrectCount, "
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score is not null) as TotalCount"
                StrSQL = StrSQL & " From ChildTestAnswers Ch Inner Join CoachReg CR on CR.Childnumber = Ch.Childnumber and CR.PMemberID =Ch.MemberID INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
                StrSQL = StrSQL & " WHERE Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperId & " and CR.CMemberID =" & ddlVolName.SelectedValue & " and  Ch.Score is not null Order by Ch.SectionNumber,Ch.QuestionNumber asc "

                'Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber," 'Ch.Answer,SA.CorrectAnswer,Ch.Score,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score=1) as CorrectCount,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score!=1) as IncorrectCount,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score is not null) as TotalCount"
                'StrSQL = StrSQL & " From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                'StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & ""
                'StrSQL = StrSQL & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc"
                'Response.Write(StrSQL)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

                If ds.Tables(0).Rows.Count > 0 Then
                    DGMissedAns.DataSource = ds
                    DGMissedAns.DataBind()
                    lblMissedAns.Visible = True
                    TrSort.Visible = True
                Else
                    lblScoreErr.Text = "No child has taken the test."
                    TrSort.Visible = False
                End If
            Else
                lblScoreErr.Text = "No Coach Papers present."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try


    End Sub

    Protected Sub btnSort_Missed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort_Missed.Click
        Try
            ddlChild.Enabled = False
            ClearScoreGrids()
            ClearReportGrids()

            GetCoachPaperID()
            Dim CoachPaperId As Integer
            CoachPaperId = Session("CoachPaperID")
            'If hdnCoachPaperID.Value <> "" Then
            '    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            'End If

            If CoachPaperId > 0 Then

                'Check if ChildTestAnswers has data--If child has completed the test
                If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'")) = 0 Then
                    lblScoreErr.Text = "No child has taken the test."
                    Exit Sub
                End If


                Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,"
                StrSQL = StrSQL & "(Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber and C.Score=1 and CR.CMemberID = " & ddlVolName.SelectedValue & ") as CorrectCount, "
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score!=1) as IncorrectCount, "
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
                StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
                StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score is not null) as TotalCount"
                StrSQL = StrSQL & " From ChildTestAnswers Ch Inner Join CoachReg CR on CR.Childnumber = Ch.Childnumber and CR.PMemberID =Ch.MemberID INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
                StrSQL = StrSQL & " WHERE Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperId & " and CR.CMemberID =" & ddlVolName.SelectedValue & " and  Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc "

                'Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber," 'Ch.Answer,SA.CorrectAnswer,Ch.Score,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score=1) as CorrectCount,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score!=1) as IncorrectCount,"
                'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score is not null) as TotalCount"
                'StrSQL = StrSQL & " From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                'StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & ""
                'StrSQL = StrSQL & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc"
                'Response.Write(StrSQL)
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

                If ds.Tables(0).Rows.Count > 0 Then
                    DGMissedAns.DataSource = ds
                    DGMissedAns.DataBind()
                    lblMissedAns.Visible = True
                    TrSort.Visible = True
                Else
                    lblScoreErr.Text = "No child has taken the test."
                    TrSort.Visible = False

                End If
            Else
                lblScoreErr.Text = "No Coach Papers present."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub GVAnswers_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Try
            If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If e.Row.Cells(7).Text = "Y" Then
                        e.Row.Cells(8).Enabled = True
                        e.Row.Cells(8).BackColor = Color.LemonChiffon
                    Else
                        e.Row.Cells(8).Enabled = False
                    End If
                    'e.Row.Cells(7).Visible = False
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub GVAnswersUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim StrAnswers_Update As String = ""
            Dim txtManual As TextBox
            Dim ErrFlag As Boolean = False
            Dim CoachPaperID As Integer

            For Each row As GridViewRow In GVAnswers.Rows
                txtManual = row.Cells(8).FindControl("txtManual")
                If row.Cells(7).Text = "Y" Then
                    If txtManual.Text = "" Then
                        StrAnswers_Update = StrAnswers_Update & "Update ChildTestAnswers set CScore=NULL "
                    Else
                        If Convert.ToDouble(txtManual.Text) >= -1 And Convert.ToDouble(txtManual.Text) <= 1 Then
                            StrAnswers_Update = StrAnswers_Update & "Update ChildTestAnswers set CScore=" & txtManual.Text
                        Else
                            ErrFlag = True
                        End If
                    End If
                    StrAnswers_Update = StrAnswers_Update & " where EventYear =" & ddlEventYear.SelectedValue & " and ChildNumber = " & ddlChild.SelectedValue & " and CoachPaperID =" & row.Cells(0).Text & " and SectionNumber=" & row.Cells(1).Text & " and QuestionNumber =" & row.Cells(2).Text & ""
                    CoachPaperID = row.Cells(0).Text
                    Session("CoachPaperID") = row.Cells(0).Text
                End If
            Next
            If ErrFlag = True Then
                lblCScoreErr.Text = "Score value must be between -1 and 1."
                Exit Sub
            Else
                If StrAnswers_Update <> "" Then
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrAnswers_Update) > 0 Then
                        lblCScoreErr.Text = "Scores updated successfully."
                        Dim sqlCommand As String = "usp_CalcSATScaledTestScores"

                        Dim param(3) As SqlParameter
                        param(0) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
                        param(1) = New SqlParameter("@ChildNumber", ddlChild.SelectedValue)
                        param(2) = New SqlParameter("@CoachPaperID", Session("CoachPaperID"))

                        '******************Calculation Of CScores for selected Child******************************************'
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)
                        GetScores(CoachPaperID, False)

                    End If
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Protected Sub ddlChild_Par_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Clear()
        ClearScoreGrids()
        ClearReportGrids()

        If Session("entryToken").ToString().ToUpper() = "PARENT" Then
            LoadProductGroup(Session("User"))
        End If
        'Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Grade From child where childNumber=" & ddlChild.SelectedValue)
        'If dsGrade.Tables(0).Rows.Count > 0 Then
        '    'hdnChGrade.Value = dsGrade.Tables(0).Rows(0)("Grade").ToString()
        'End If
    End Sub

    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        Try
            Dim StrInsertExp As String = ""
            StrInsertExp = StrInsertExp & " Insert into EnterAnsException (EventYear, MemberID, ChildNumber, CoachPaperID, ProductGroupID, ProductGroupCode, Phase, ProductID, ProductCode, Level, WeekID, NewDeadLine, CreateDate, CreatedBy)"
            StrInsertExp = StrInsertExp & " Select EventYear, " & ddlVolName.SelectedValue & "," & ddlChild.SelectedValue & ", CoachPaperID, ProductGroupID, ProductGroupCode, (Select Distinct Phase from CoachRelDates Where CoachPaperID =" & Session("CoachPaperID") & " and MemberID =" & ddlVolName.SelectedValue & "), ProductID, ProductCode, Level, WeekID, GETDATE()+7, GETDATE(), " & Session("LoginID") & " from CoachPapers where CoachPaperID=" & Session("CoachPaperID")

            'If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from EnterAnsException where CoachPaperID =" & Session("CoachPaperID") & " and MemberID=" & ddlVolName.SelectedValue & "") > 0 Then
            '    lblScoreErr.Text = "Child has not submitted the answers."
            '    TrAnsExp.Visible = False
            'Else
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrInsertExp) > 0 Then
                lblScoreErr.Text = "Exception Added for child " & ddlChild.SelectedItem.Text
                TrAnsExp.Visible = False
            End If
            'End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        TrAnsExp.Visible = False
        lblScoreErr.Text = "Child has not submitted the answers before the deadline."
    End Sub

    Protected Sub btnGradeCard_Parent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGradeCard_Parent.Click

        If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            If ddlChild_Par.Items.Count <= 0 Then
                lblScoreErr.Text = "Child(ren) has/have not yet taken the test."
                Exit Sub
            ElseIf ddlChild_Par.SelectedValue = -1 Then
                lblScoreErr.Text = "Please Select Child Name"
                Exit Sub
            Else
                lblScoreErr.Text = ""
                Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" & ddlChild_Par.SelectedValue & "&EventYear=" & ddlEventYear.SelectedValue & "&ProductGroup=" & ddlProductGroup.SelectedValue & "&Product=" & ddlProduct.SelectedValue & "&Level=" & ddlLevel.SelectedItem.Text & "','_blank','fullscreen=yes,left=150,top=0,toolbar=0,location=0,scrollbars=1');</script> ")
            End If
        ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
            If ddlChild.Items.Count <= 0 Then
                lblScoreErr.Text = "Child(ren) has/have not yet taken the test."
                Exit Sub
            ElseIf ddlChild.SelectedValue = -1 Then
                lblScoreErr.Text = "Please Select Child Name"
                Exit Sub
            Else
                lblScoreErr.Text = ""
                Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" & ddlChild.SelectedValue & "&EventYear=" & ddlEventYear.SelectedValue & "&ProductGroup=" & ddlProductGroup.SelectedValue & "&Product=" & ddlProduct.SelectedValue & "&Level=" & ddlLevel.SelectedItem.Text & "','_blank','fullscreen=yes,left=150,top=0,toolbar=0,location=0,scrollbars=1');</script> ")
            End If

        End If

    End Sub
End Class

