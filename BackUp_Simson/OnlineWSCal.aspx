

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="OnlineWSCal.aspx.vb"  EnableSessionState="True" Inherits="OnlineWSCal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div style="text-align:left">
      &nbsp;
     <table width="100%"><tr><td><asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton></td>
     <td align="right"> <a href="javascript:PopupPickerHelp();">Help</a></td>
     </tr></table> 
      &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
      <%--    <script language="javascript" type="text/javascript">
         function PopupPicker(ctl, w, h) {
             var PopupWindow = null;
             settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
             PopupWindow = window.open('DatePicker.aspx?Ctl=_ctl0_Content_main_' + ctl, 'DatePicker', settings);
             PopupWindow.focus();
         }
         </script> --%> 
      <script language="javascript" type="text/javascript">
          function PopupPicker(ctl, w, h) {
              var PopupWindow = null;
              settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
              PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
              PopupWindow.focus();
          }
          function PopupPickerHelp() {
              var PopupWindow = null;
              settings = 'width=550,height=375,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
              PopupWindow = window.open('OnlineWSCalHelp.aspx', 'Online Workshop Calendar Help', settings);
              PopupWindow.focus();
          }
      </script>
   </div>
   <table id="Table1" width="100%" runat="server" >
      <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
         <td class="Heading" align="center" style="width: 991px" >
            <strong>
               <asp:Label ID="lblHeading1" runat="server" Text="Online Workshop Calendar"></asp:Label>
            </strong>
            <br />
         </td>
      </tr>
   </table>
   <table>
      <tr>
         <td align="left" style="width: 103px"><b>Event Year</b></td>
         <td align="left" style="width: 1828px">
            <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="120px"></asp:DropDownList>
         </td>
         <td align="left" style="width: 116px"><b>Product Group</b></td>
         <td align="left" style="width: 864px">
            <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID"  OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList>
         </td>
         <td align="left" style="width: 163px"><b>Product</b></td>
         <td align="left" style="width: 247px">
            <asp:DropDownList ID="ddlProduct" DataTextField="Name" 
               DataValueField="ProductID" AutoPostBack="true" 
               OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false" 
               runat="server" Height="20px" Width="152px"></asp:DropDownList>
         </td>
         <%-- <td style="width: 117px"></td>
            <td style="width: 191px"></td>--%>
      </tr>
      <tr id="Tr4" runat="server"  >
         <td style="width: 103px; height: 17px;" align="left"><b> Date</b></td>
         <td id="Td6" style="width: 1828px; height: 17px;"  runat="server"  >
            <asp:textbox id="txtStartDate" runat="server" AutoPostBack ="true" 
               CssClass="SmallFont" Height="16px" Width="98px"></asp:textbox>
            <a href="javascript:PopupPicker('<%=txtStartDate.ClientID%>', 200, 200);"><img src="Images/Calendar.gif" border="0"></a>
            <asp:Label ID="lbldate" runat="server" Text='mm/dd/yy|TBD' ForeColor ="Red" Font-Bold="false"></asp:Label>
         </td>
         <td align="left" style="width: 116px; height: 17px;"><b> Time</b></td>
         <td align="left" style="width: 864px; height: 17px;">
            <asp:DropDownList ID="ddlDisplayTime" runat="server" Height="20px" Width="120px"></asp:DropDownList>
         </td>
         <td align="left" style="height: 17px; width: 163px"><b>Duration</b></td>
         <td style="width: 247px; height: 17px;">
            <asp:DropDownList ID="ddlDuration" runat="server" Height="20px" Width="125px">
               <asp:ListItem Value="0">TBD</asp:ListItem>
               <asp:ListItem Value="1">1.0</asp:ListItem>
               <asp:ListItem Value="1.5">1.5</asp:ListItem>
               <asp:ListItem Value="2">2.0</asp:ListItem>
               <asp:ListItem Value="2.5">2.5</asp:ListItem>
               <asp:ListItem Value="3">3.0</asp:ListItem>
               <asp:ListItem Value="3.5">3.5</asp:ListItem>
               <asp:ListItem Value="4">4.0</asp:ListItem>
               <asp:ListItem Value="4.5">4.5</asp:ListItem>
               <asp:ListItem Value="5">5</asp:ListItem>
               <asp:ListItem Value="5.5">5.5</asp:ListItem>
               <asp:ListItem Value="6">6.0</asp:ListItem>
            </asp:DropDownList>
         </td>
         <%--       <td style="width: 117px; height: 17px"></td>
            <td style="width: 191px; height: 17px"></td>
            --%>     
      </tr>
      <tr>
         <td style="width: 103px" align="left"><b>Reg.Deadline</b> </td>
         <td style="width: 1828px">
            <asp:textbox id="txtRegDeadline" runat="server" CssClass="SmallFont" 
               Height="17px" Width="100px"></asp:textbox>
            <a href="javascript:PopupPicker('<%=txtRegDeadline.ClientID%>', 200, 200);"><img src="Images/Calendar.gif" border="0"> </a>
            <asp:Label ID="Label11" runat="server" Text='mm/dd/yy|TBD' ForeColor ="Red" Font-Bold="false"></asp:Label>
         </td>
         <td style="width: 116px" align="left"><b>LateReg.Deadline</b> </td>
         <td style="width: 864px">
            <asp:TextBox ID="txtLateRegDeadline" runat="server"  CssClass="SmallFont" 
               Height="16px" Width="83px"></asp:TextBox>
            <a href="javascript:PopupPicker('<%=txtLateRegDeadline.ClientID%>', 200, 200);"><img src="Images/Calendar.gif" border="0"> </a>
            <asp:Label ID="Label12" runat="server" Text='mm/dd/yy|TBD' ForeColor ="Red" Font-Bold="false"></asp:Label>
         </td>
         <td style="width: 163px" align="left"><b>Teacher</b> </td>
         <td style="width: 247px">
            <asp:DropDownList ID="ddlTeacher" runat="server" Height="20px" Width="145px"></asp:DropDownList>
            <%-- <asp:TextBox ID="txtName" runat="server" 
               style="margin-left: 0px" Enabled="false" ></asp:TextBox>--%>
         </td>
         <%--<td align="center" style="width: 117px">
            <asp:Button Id="btnSearch" runat="server" Text="Search" Height="25px" Width="89px" Visible="true" style="margin-left: 0px"/>
            &nbsp;
            </td>
            <td style="width: 191px">
            <asp:Button ID="btnClear" runat="server" Text="Clear" Height="25px" Width="50px"/>
            
            </td>--%>
      </tr>
      <tr>
            <td style="width: 103px" align="left"><b>Max.Capacity</b> </td>
            <td>  
                <asp:TextBox ID="txtMaxCap" runat="server"  CssClass="SmallFont" 
                    Height="21px" Width="109px"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" Operator="DataTypeCheck" Type="Integer" 
 ControlToValidate="txtMaxCap" ErrorMessage="MaxCap must be Integer." /></td>
                         <td>
                   <b>FileName</b></td>
                <td colspan="3">
                    <asp:FileUpload ID="FileUpLoad1" runat="server" Height="20px" />
                    <asp:Label ID="LabelExampleFormat" runat="server"  Font-Size=Small Text="Ex: 2016_SB_OWorkShopBook_02212016.zip"></asp:Label>
                    
                </td>
            


          </tr>
      <tr>
         <td align="center" colspan="7">
            <asp:Button ID="btnAddUpdate" runat="server" Text="Add/Update" />
            &nbsp;
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
         </td>
      </tr>
      <tr>
         <td align="center" colspan="6" >
            <asp:Label ID="lblerr" runat="server" Text="" ForeColor="Red" ></asp:Label><asp:Label ID="lblFileErr" runat="server" Text="" ForeColor="Red" ></asp:Label>
            <asp:HiddenField Id="hdnEventFeesID" runat="server"  Visible="false" />
             
         </td>
      </tr>
   </table>
   <%--<asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
      <b> Search NSF member</b>   
      <div align = "center">
         <table border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >
            <tr>
               <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
               <td align="left" >
                  <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
               </td>
            </tr>
            <tr>
               <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
               <td  align ="left" >
                  <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
               </td>
            </tr>
            <tr>
               <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
               <td align="left">
                  <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
               </td>
            </tr>
            <tr>
               <td align="right">
                  <asp:Button ID="Button1" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
               </td>
               <td align="left">
                  <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>
               </td>
            </tr>
         </table>
         <asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
      </div>
      <br />
      <asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
         <b> Search Result</b>
         <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
            <Columns>
               <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
               <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
               <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
               <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
               <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
               <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
               <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
               <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
               <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
               <asp:BoundField DataField="chapter" headerText="Chapter" ></asp:BoundField>
            </Columns>
         </asp:GridView>
      </asp:Panel>
      </asp:Panel>--%>
   <asp:Panel ID ="PnlAddOWkshops" runat="server" Enabled ="true ">
      <asp:HiddenField ID ="CalRow" runat="server"   />
      <table ID="tblAddOWkshops" runat="server"  visible ="true" width="100%">
         <tr>
            <td>
               <asp:Label ID="lblGridError" runat="server" width="100%" ForeColor ="Red"></asp:Label>
            </td>
         </tr>
         <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
            <td style="width:991px" align="center" class="ContentSubTitle">
               <asp:DataGrid ID="grdTarget" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="OnlineWSCalID"
                  Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Inset"
                  BorderColor="#336666" Visible="False">
                  <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C" ></SelectedItemStyle>
                  <ItemStyle ForeColor="Black" BackColor="#EEEEEE" ></ItemStyle>
                  <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false"></HeaderStyle>
                  <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
                  <%--	<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
                     <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="Black" BackColor="Gainsboro" ></HeaderStyle>
                     --%> 	
                  <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                  <Columns>
                     <asp:EditCommandColumn ItemStyle-ForeColor="Blue"  ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Modify" >
                        <ItemStyle ForeColor="Blue"></ItemStyle>
                     </asp:EditCommandColumn>
                     <asp:BoundColumn  DataField="OnlineWSCalID" Visible="True" readonly="true" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="EventFeesID" Visible="False" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="EventYear" HeaderText="EventYear" Readonly="true" HeaderStyle-ForeColor="White" Visible="True" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="EventID" HeaderText="Event"  Readonly="true" HeaderStyle-ForeColor="White" Visible="True" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="ProductGroupID" HeaderText="ProductGroupID" HeaderStyle-ForeColor="White" Visible="False" ></asp:BoundColumn>
                     <asp:BoundColumn DataField="ProductID" HeaderText="ProductGroupID" HeaderStyle-ForeColor="White" Visible="False" ></asp:BoundColumn>
                     <asp:TemplateColumn HeaderText="ProductGroup">
                        <ItemTemplate>
                           <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDGProductGroup_SelectedIndexChanged" OnPreRender="SetDropDown_ProductGroup"></asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                        <ItemStyle Width="10%" />
                     </asp:TemplateColumn >
                     <asp:TemplateColumn HeaderText="ProductCode">
                        <ItemTemplate >
                           <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <asp:DropDownList ID="ddlProduct"  DataTextField="Name" DataValueField="ProductID" runat="server" OnPreRender="SetDropDown_Product"  OnSelectedIndexChanged="ddlDGProduct_SelectedIndexChanged" ></asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                        <ItemStyle Width="10%" />
                     </asp:TemplateColumn >
                     <asp:TemplateColumn HeaderText="Event Date">
                        <ItemTemplate>
                           <asp:Label ID="lblEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Date", "{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <asp:TextBox ID="txtEditEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Date", "{0:d}")%>'  OnPreRender="SetEventDate_PreRender"></asp:TextBox>
                           <asp:Button ID="Button1" runat="server"  CommandName="ShowHideCalendar1" OnClick="Button1_Click" Text="Calendar" />
                           <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged" Visible="False"></asp:Calendar>
                           <br />
                           <asp:Label ID="lblEvDt" runat="server" Text='mm/dd/yy | TBD' ForeColor ="Red" Font-Bold="false"></asp:Label>
                           <br />
                           <asp:Label ID="lblEt" runat="server" Text='Note**:Type Date and Press Enter' ForeColor ="Green" Font-Bold="false"></asp:Label>
                        </EditItemTemplate>
                        <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                        <ItemStyle Width="10%" />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn  HeaderText="Time">
                        <ItemTemplate>
                           <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>' ></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <asp:DropDownList ID="ddlTime" runat="server" OnPreRender="SetDropDown_Time" OnSelectedIndexChanged="ddlDGTime_SelectedIndexChanged"></asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" Wrap="True" />
                        <ItemStyle Width="5%" />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn  HeaderText="Duration">
                        <ItemTemplate>
                           <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Duration")%>' ></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <asp:DropDownList ID="ddlDuration" runat="server" OnPreRender="SetDropDown_Duration" OnSelectedIndexChanged="ddlDGDuration_SelectedIndexChanged" >
                              <asp:ListItem Value="0">TBD</asp:ListItem>
                              <asp:ListItem Value="1">1.0</asp:ListItem>
                              <asp:ListItem Value="1.5">1.5</asp:ListItem>
                              <asp:ListItem Value="2">2.0</asp:ListItem>
                              <asp:ListItem Value="2.5">2.5</asp:ListItem>
                              <asp:ListItem Value="3">3.0</asp:ListItem>
                              <asp:ListItem Value="3.5">3.5</asp:ListItem>
                              <asp:ListItem Value="4">4.0</asp:ListItem>
                              <asp:ListItem Value="4.5">4.5</asp:ListItem>
                              <asp:ListItem Value="5">5.0</asp:ListItem>
                              <asp:ListItem Value="5.5">5.5</asp:ListItem>
                              <asp:ListItem Value="6">6.0</asp:ListItem>
                           </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" Wrap="True" />
                        <ItemStyle Width="5%" />
                     </asp:TemplateColumn>
                     <asp:BoundColumn DataField="TeacherID" HeaderText="TeacherID" HeaderStyle-ForeColor="White" Visible="False" ></asp:BoundColumn>
                     <asp:TemplateColumn  HeaderText="Teacher">
                        <ItemTemplate>
                           <asp:Label ID="lblTeacher" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Teacher")%>' ></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <asp:DropDownList ID="ddlTeacher" runat="server" OnPreRender="SetDropDown_Teacher" OnSelectedIndexChanged="ddlDGTeacher_SelectedIndexChanged" >
                           </asp:DropDownList>
                        </EditItemTemplate>
                        <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="True" Wrap="True" />
                        <ItemStyle Width="5%" />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Registration Deadline">
                        <ItemTemplate>
                           <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <%--  <asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label> 
                              --%>    
                           <asp:TextBox ID="txtEditRegDeadLine" runat="server"   OnPreRender="SetRegDeadline_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:TextBox>
                           <asp:Button ID="Button2" runat="server"  CommandName="ShowHideCalendar2" OnClick="Button2_Click" Text="Calendar" />
                           <asp:Calendar ID="Calendar2" runat="server" OnSelectionChanged="Calendar2_SelectionChanged" Visible="False"></asp:Calendar>
                           <br />
                           <asp:Label ID="lblRegDt" runat="server" Text='mm/dd/yy | TBD' ForeColor ="Red" Font-Bold="false"></asp:Label>
                        </EditItemTemplate>
                        <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                        <ItemStyle Width="10%" />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="LateFee Deadline">
                        <ItemTemplate>
                           <asp:Label ID="lblLateFeeDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                           <%--<asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>--%>
                           <asp:TextBox ID="txtEditLateFeeDeadLine" runat="server" OnPreRender="SetLateRegDLDate_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:TextBox>
                           <asp:Button ID="Button3" runat="server"  CommandName="ShowHideCalendar3" OnClick="Button3_Click" Text="Calendar" />
                           <asp:Calendar ID="Calendar3" runat="server" OnSelectionChanged="Calendar3_SelectionChanged" Visible="False"></asp:Calendar>
                           <br />
                           <asp:Label ID="lbllateRegDt" runat="server" Text='mm/dd/yy | TBD'  ForeColor ="Red" Font-Bold="false"></asp:Label>
                           <%--   --%>      
                        </EditItemTemplate>
                        <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                        <ItemStyle Width="10%" />
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="MaxCapacity">
                            <ItemTemplate>
                               <asp:Label ID="lblMaxCap" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCap")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                      <asp:TextBox ID="txtEditMaxCap" runat="server" Width ="30px" OnPreRender="SetMaxCap_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCap")%>'></asp:TextBox><br />
                            </EditItemTemplate>
                            <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                            <ItemStyle Width="10%" />
                     </asp:TemplateColumn>
                       <asp:TemplateColumn HeaderText="FileName">
                            <ItemTemplate>
                               <asp:Label ID="lblFileName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FileName")%>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                   <asp:FileUpload ID="fupload" runat="server" />
                                 <asp:Label ID="lblFileName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FileName")%>'></asp:Label>
                            </EditItemTemplate>
                            <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="True" />
                            <ItemStyle Width="10%" />
                     </asp:TemplateColumn>

                     <%--<asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn>
                        <asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>--%>
                  </Columns>
               </asp:DataGrid>
               &nbsp;
            </td>
         </tr>
      </table>
   </asp:Panel>
   <%--   <div>
      <asp:HyperLink runat="server" NavigateUrl="~/VolunteerFunctions.aspx" ID="hlinkChapterFunctions">Go back to Menu</asp:HyperLink>
      </div>--%>
</asp:Content>

