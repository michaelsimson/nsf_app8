﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="SearchParentandChild.aspx.cs" Inherits="SearchParentandChild" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Search Records of Parents and Children

             <br />
        <br />
    </div>
    <br />
    <table>
        <tr runat="server" visible="false">
            <td width="90px"></td>
            <td>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="txtChildName" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsearchChild" runat="server" Text="Search"
                    OnClick="btnsearchChild_Click" />
            </td>
            <td width="90px"></td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Child Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button1" runat="server" Text="Search"
                    OnClick="Button1_Click" /></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="height: 172px"></td>
            <td style="height: 172px">
                <asp:Panel ID="pIndSearch" runat="server" Visible="true" Style="margin-left: 100px;">
                    <span style="position: relative; left: 50px;"><b>Search Parent Name</b></span>
                    <table border="1" runat="server" id="Table1" style="text-align: center" width="30%"
                        visible="true" bgcolor="silver">
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                            <td align="left">
                                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                            <td align="left">
                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlState" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="Button2" runat="server" Text="Find"
                                    CausesValidation="False" OnClick="Button2_Click" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnIndClose" runat="server" Text="Close"
                            CausesValidation="False" OnClick="btnIndClose_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lbsearchresults" runat="server" ForeColor="Red"></asp:Label>
                </asp:Panel>
            </td>
            <td style="height: 172px"></td>

            <td style="height: 172px;">
                <asp:Panel ID="Pnlchild" runat="server" Visible="true" Style="margin-left: 100px;">
                    <span style="position: relative; left: 50px;"><b>Search Child Name</b></span><table
                        border="1" runat="server" id="tblIndSearch" style="text-align: center"
                        width="30%" visible="true" bgcolor="silver">
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Name:</td>
                            <td align="left">
                                <asp:TextBox ID="txtChName" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Principal Parent Name:</td>
                            <td align="left">
                                <asp:TextBox ID="txtParentName" runat="server"></asp:TextBox></td>
                        </tr>

                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                            <td align="left">
                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Phone Number Contains:</td>
                            <td align="left">
                                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></td>
                        </tr>

                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSearch" runat="server" Text="Find" CausesValidation="False" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnclose" runat="server" Text="Close" OnClick="btnclose_Click" />
                            </td>
                            <%--  <td  align="left">					
		                <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		            </td>--%>
                        </tr>

                    </table>
                    <asp:Label ID="lblchError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table>
        <tr>
            <td width="50%">
                <div>


                    <asp:Panel ID="Panel4" runat="server" HorizontalAlign="Center" Visible="false">
                        <b>Login(Table 1)</b>
                        <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GVLogin"
                            AutoGenerateColumns="false" runat="server"
                            RowStyle-CssClass="SmallFont">
                            <Columns>

                                <asp:BoundField DataField="email" HeaderText="Email Address"></asp:BoundField>
                                <asp:BoundField DataField="Exists" HeaderText="Exists"></asp:BoundField>

                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </td>
            <td>
                <asp:Panel ID="Panel3" runat="server" HorizontalAlign="Center" Visible="false">
                    <b>Profile (Table 2)</b>
                    <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GVprofile"
                        AutoGenerateColumns="false" runat="server"
                        RowStyle-CssClass="SmallFont">
                        <Columns>

                            <asp:BoundField DataField="automemberid" HeaderText="MemberID"></asp:BoundField>
                            <asp:BoundField DataField="firstname" HeaderText="Fist Name"></asp:BoundField>
                            <asp:BoundField DataField="lastname" HeaderText="Last Name"></asp:BoundField>
                            <asp:BoundField DataField="email" HeaderText="Email"></asp:BoundField>
                            <asp:BoundField DataField="Exists" HeaderText="Login"></asp:BoundField>
                            <asp:BoundField DataField="DonorType" HeaderText="Donor Type"></asp:BoundField>
                            <asp:BoundField DataField="hphone" HeaderText="Hphone"></asp:BoundField>

                            <asp:BoundField DataField="Cphone" HeaderText="Cphone"></asp:BoundField>
                            <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                            <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                            <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                            <asp:BoundField DataField="chapter" HeaderText="Chapter"></asp:BoundField>

                        </Columns>
                    </asp:GridView>
                </asp:Panel>
        </tr>


        <tr>
            <td></td>
            <td>
                <div>


                    <asp:Panel ID="Panel5" runat="server" HorizontalAlign="Center" Visible="false">
                        <b>Child (Table 3)</b>
                        <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GVchild"
                            AutoGenerateColumns="false" runat="server"
                            RowStyle-CssClass="SmallFont">
                            <Columns>

                                <asp:BoundField DataField="ChildNumber" HeaderText="Child Number"></asp:BoundField>
                                <asp:BoundField DataField="FIRST_NAME" HeaderText="Fist Name"></asp:BoundField>
                                <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name"></asp:BoundField>
                                <asp:BoundField DataField="DATE_OF_BIRTH" HeaderText="DOB" DataFormatString="{0:d}"></asp:BoundField>
                                <asp:BoundField DataField="GRADE" HeaderText="Grade"></asp:BoundField>
                                <asp:BoundField DataField="SchoolName" HeaderText="School"></asp:BoundField>

                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </td>


        </tr>

    </table>

</asp:Content>

