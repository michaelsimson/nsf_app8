﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MakeupSessions.aspx.cs" Inherits="MakeupSessions" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">


        <style type="text/css">
            .ui-datepicker-trigger {
                padding-left: 5px;
                position: relative;
                top: 3px;
            }

            .ui-datepicker {
                font-size: 8pt !important;
            }

            .style8 {
                width: 99px;
            }

            .style10 {
                width: 147px;
            }

            .style12 {
                width: 84px;
                text-align: left;
            }

            .style16 {
                width: 126px;
            }

            .style17 {
                width: 88px;
            }

            .style20 {
                width: 113px;
                text-align: left;
            }

            .style47 {
                width: 100%;
            }

            .style53 {
                width: 179px;
            }

            .style60 {
                width: 68px;
            }

            .style67 {
                width: 15px;
                font-weight: bold;
            }

            .style70 {
                width: 286px;
            }

            .style87 {
            }

            .style90 {
                width: 184px;
            }

            .style95 {
                width: 93px;
            }

            .style97 {
                width: 117px;
            }

            .style98 {
                width: 37px;
            }

            .style99 {
                width: 120px;
                text-align: left;
            }

            .style101 {
                width: 116px;
            }

            .style102 {
                width: 10px;
            }

            .style118 {
                width: 286px;
                text-align: left;
            }

            .style120 {
                width: 15px;
            }

            .style121 {
                width: 63px;
            }

            .style129 {
                width: 133px;
            }

            .style1 {
                width: 100%;
                height: 92px;
            }

            .style158 {
                width: 95px;
            }

            .style160 {
                width: 271px;
                text-align: left;
            }

            .style162 {
                width: 121px;
            }

            .style163 {
                width: 4px;
            }

            .style165 {
            }

            .style167 {
                text-align: left;
            }

            .style176 {
                width: 199px;
                text-align: left;
            }

            .style177 {
                width: 101px;
                text-align: left;
            }

            .style178 {
                width: 95px;
                text-align: left;
            }

            .style185 {
                width: 152px;
            }

            .style186 {
                width: 101px;
            }

            .style187 {
                width: 181px;
                text-align: left;
            }

            .style191 {
                width: 158px;
            }

            .style195 {
                font-weight: bold;
                width: 158px;
            }

            .style197 {
                width: 54px;
                font-weight: bold;
            }

            .style198 {
                width: 54px;
            }

            .style199 {
                text-align: left;
                width: 182px;
            }

            .style200 {
                width: 120px;
            }

            .style201 {
                width: 179px;
                height: 29px;
            }

            .style202 {
                width: 68px;
                height: 29px;
            }

            .style203 {
                width: 15px;
                height: 29px;
            }

            .style204 {
                width: 10px;
                height: 29px;
            }

            .style205 {
                width: 117px;
                height: 29px;
            }

            .style206 {
                height: 29px;
            }

            .style207 {
                width: 286px;
                height: 29px;
            }
        </style>


        <script src="Scripts/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>
        <script language="javascript" type="text/javascript">
            $(function (e) {
                var pickerOpts = {
                    dateFormat: $.datepicker.regional['fr']
                };
                $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });
                $("#<%=txtDate.ClientID %>").datepicker({ dateFormat: 'yy-mm-dd' });

            });

            function ConfirmmeetingCancel() {
                if (confirm("Are you sure you want to cancel this meeting?")) {
                    document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                }
            }


            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }
            function showAlert() {
                alert("Meeting attendees can only join their class up to 30 minutes before class time");
            }

            function StartMeeting() {

                JoinMeeting();

            }

            function showAlertmsgClassStatus() {

                alert("Your previous class status was not yet updated. Please set the status of the class appropriately");

            }

            function startChildMeeting() {
                var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
                $("#ancClick").target = "_blank";
                $("#ancClick").attr("href", url);
                $("#ancClick").attr("target", "_blank");
                document.getElementById("ancClick").click();
                //var win = window.open(url, '_blank');

                //if (win) {
                //    //Browser has allowed it to be opened
                //    win.focus();
                //} else {
                //    //Broswer has blocked it
                //    alert('Please allow popups for this site');
                //}

            }
            function joinChildMeeting() {
                startChildMeeting();
            }

            function showAlert(prdCode, coachName) {
                var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
                var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
                var dueMins = parseInt(startMins);
                var msg = "";
                if (dueMins > 0) {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                else if (dueMins >= -15) {
                    msg = "The coach has not started the class.";
                } else if (dueMins < -15) {
                    msg = "The coach has either not started or may have cancelled the class.";
                } else {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
                alert(msg);
            }

            function deleteMeeting() {

                if (confirm("Are you sure want to delete meeting?")) {

                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }
            }
            function JoinMeeting() {

                var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

                window.open(url, '_blank');

            }

            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }
            function showAlert() {
                alert("Meeting attendees can only join their class up to 30 minutes before class time");
            }
        </script>
    </div>
    <a id="ancClick" target="_blank" href=""></a>
    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <asp:Button ID="btnDeleteMeeting" runat="server" Text="Delete Meeting" Style="display: none;" OnClick="btnDeleteMeeting_Click" />
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Makeup Sessions
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; background-color: #ffffcc;">
        <tr class="ContentSubTitle" align="center">
            <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:DropDownList ID="ddYear" runat="server" Width="85px"
                    AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                </asp:DropDownList>

            </td>
            <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Event </td>
            <td style="width: 439px" align="left">
                <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                    <%--<asp:ListItem Value="13">Coaching</asp:ListItem>--%>
                </asp:DropDownList>
            </td>
            <td style="width: 100px" align="left" nowrap="nowrap" id="Td1" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chapter </td>
            <td style="width: 100px" runat="server" align="left" id="Td2">
                <asp:DropDownList ID="ddchapter" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td id="tdCoachTitle" runat="server" style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coach
            </td>
            <td id="tdCoach" runat="server" style="width: 125px;" align="left">
                <asp:DropDownList ID="ddlCoach" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" Style="height: 22px">
                    <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>

            </td>
            <td style="text-align: right; font-weight: bold;">Product Group</td>
            <td style="text-align: left;">
                <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="text-align: right; font-weight: bold;">Product</td>
            <td style="text-align: left">
                <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </td>




        </tr>
        <tr>
            <td align="center" colspan="12">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />
                <asp:Button ID="BtnCreateSession" runat="server" Visible="false" Text="Create Meeting" OnClick="btnAddNewMeeting_Click" />

            </td>
        </tr>



    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <table id="tblAddNewMeeting" runat="server" visible="true" align="center" style="margin-left: auto; margin-right: auto; width: 100%; background-color: #ffffcc;">

        <tr class="ContentSubTitle">


            <td align="left">Phase
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlPhase" runat="server" Width="75px" AutoPostBack="True">

                    <asp:ListItem Value="1">One</asp:ListItem>
                    <asp:ListItem Value="2">Two</asp:ListItem>
                    <asp:ListItem Value="3">Three</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left">Level
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlLevel" runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left" runat="server" visible="true">Session#
            </td>
            <td align="left" runat="server" visible="true">
                <asp:DropDownList ID="ddlSession" runat="server" Width="75px" AutoPostBack="True">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                </asp:DropDownList>
            </td>

            <td align="left" id="Td5" runat="server" runat="server" visible="false">Meeting Password </td>
            <td style="width: 100px" runat="server" align="left" id="Td6" runat="server" visible="false">
                <asp:TextBox ID="txtMeetingPwd" TextMode="Password" runat="server" Width="100" Wrap="False"></asp:TextBox>
                <br />
                <br />
            </td>
            <td align="left" nowrap="nowrap" id="Td3" runat="server" visible="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Zone </td>
            <td style="width: 100px" runat="server" align="left" id="Td4" visible="true">
                <asp:DropDownList ID="ddlTimeZone" runat="server" Width="110px"
                    AutoPostBack="True">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="11" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                    <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                    <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                    <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                </asp:DropDownList>
                <div style="clear: both; margin-bottom: 10px;"></div>
            </td>
        </tr>


        <tr class="ContentSubTitle">

            <td align="left">
                <asp:Label ID="lblSubRecurDate" runat="server" Font-Bold="true">Rec Class Date</asp:Label>
            </td>
            <td align="left">
                <asp:DropDownList ID="DDlSubDate" runat="server">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                </asp:DropDownList>
            </td>

            <td align="left">Makeup Date</td>

            <td align="left">
                <asp:TextBox ID="txtDate" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                YYYY - MM - DD</td>

            <td align="left">Begin Time </td>
            <td align="left">
                <%--  <asp:TextBox ID="txtTime" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                HH : MM : SS--%>

                <asp:DropDownList ID="ddlMakeupTime" runat="server">
                    <asp:ListItem Value="">Select</asp:ListItem>
                    <asp:ListItem Value="">Select</asp:ListItem>
                    <asp:ListItem Value="08:00:00">08:00AM</asp:ListItem>
                    <asp:ListItem Value="09:00:00">09:00AM</asp:ListItem>
                    <asp:ListItem Value="10:00:00">10:00AM</asp:ListItem>
                    <asp:ListItem Value="11:00:00">11:00AM</asp:ListItem>
                    <asp:ListItem Value="12:00:00">12:00PM</asp:ListItem>
                    <asp:ListItem Value="13:00:00">01:00PM</asp:ListItem>
                    <asp:ListItem Value="14:00:00">02:00PM</asp:ListItem>
                    <asp:ListItem Value="15:00:00">03:00PM</asp:ListItem>
                    <asp:ListItem Value="16:00:00">04:00PM</asp:ListItem>
                    <asp:ListItem Value="17:00:00">05:00PM</asp:ListItem>
                    <asp:ListItem Value="18:00:00">06:00PM</asp:ListItem>
                    <asp:ListItem Value="19:00:00">07:00PM</asp:ListItem>
                    <asp:ListItem Value="20:00:00">08:00PM</asp:ListItem>
                    <asp:ListItem Value="21:00:00">09:00PM</asp:ListItem>
                    <asp:ListItem Value="22:00:00">10:00PM</asp:ListItem>
                    <asp:ListItem Value="23:00:00">11:00PM</asp:ListItem>
                    <asp:ListItem Value="24:00:00">12:00AM</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left">Reason
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="Illness">Illness</asp:ListItem>
                    <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                    <asp:ListItem Value="Business Trip">Business Trip</asp:ListItem>
                    <asp:ListItem Value="Work Related">Work Related</asp:ListItem>
                    <asp:ListItem Value="Holiday">Holiday</asp:ListItem>
                    <asp:ListItem Value="Other">Other</asp:ListItem>
                </asp:DropDownList>

            </td>
            <td align="left" nowrap="nowrap" id="lbchapter" runat="server" visible="false">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duration (Mins)</td>
            <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                <asp:TextBox ID="txtDuration" runat="server" Width="93px" Visible="false"></asp:TextBox>
            </td>


        </tr>

        <tr class="ContentSubTitle" runat="server" visible="false">
            <td align="left">End Date</td>

            <td align="center">
                <asp:TextBox ID="txtEndDate" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                MM/DD/YYYY</td>

            <td align="left">End Time </td>
            <td align="center">
                <asp:TextBox ID="txtEndTime" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                HH : MM : SS
            </td>
            <td align="left" id="Td7" runat="server">Day </td>
            <td runat="server" align="center" id="Td8">
                <asp:DropDownList ID="ddlDay" runat="server" Width="110px"
                    AutoPostBack="True">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="MONDAY">Monday</asp:ListItem>
                    <asp:ListItem Value="TUESDAY">Tuesday</asp:ListItem>
                    <asp:ListItem Value="WEDNESDAY">Wednesday</asp:ListItem>
                    <asp:ListItem Value="THURSDAY">Thursday</asp:ListItem>
                    <asp:ListItem Value="FRIDAY">Friday</asp:ListItem>
                    <asp:ListItem Value="SATURDAY">Saturday</asp:ListItem>
                    <asp:ListItem Value="SUNDAY">Sunday</asp:ListItem>
                </asp:DropDownList>
            </td>

            <td align="left" id="TdUserIDTitle" runat="server">WebEx UserID</td>
            <td align="center" id="TdUserID" runat="server">
                <asp:TextBox ID="txtUserID" runat="server" Style="width: 95px;"></asp:TextBox>
            </td>
            <td align="left" id="TdPWDTitle" runat="server">PWD</td>
            <td align="center" id="TdPWD" runat="server">
                <asp:TextBox ID="txtPWD" TextMode="Password" runat="server" Style="width: 95px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="12">



                <table align="center">
                    <tr>
                        <td>
                            <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Makeup Session" OnClick="btnCreateMeeting_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" OnClick="btnCancelMeeting_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblSuccess" runat="server" Style="color: blue;"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" style="font-weight: bold; color: #64A81C;">
        <span id="spnTable1Title" runat="server" visible="false">Table 1 : Coach Meeting List</span>


    </div>
    <div style="clear: both;"></div>
    <asp:Button ID="btnAddNewMeeting" runat="server" Visible="false" Text="New Meeting" OnClick="btnAddNewMeeting_Click" Style="float: right;" />
    <div style="clear: both;"></div>
    <div>
        <asp:Button ID="btnDowloadFIle" runat="server" Text="Downoad Exception Log" Visible="false" OnClick="btnDowloadFIle_Click" />
    </div>
    <div style="clear: both;"></div>
    <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand" OnRowCancelingEdit="GrdMeeting_RowCancelingEdit" EnableModelValidation="True" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
        <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                <ItemTemplate>
                    <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                <ItemTemplate>
                    <div style="display: none;">
                        <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                        <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                        <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                        <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                        <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                        <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                        <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                        <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                        <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                        <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StartDate","{0:yyyy-MM-dd}") %>'>'></asp:Label>
                        <asp:Label ID="lblSignupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SignupID") %>'>'></asp:Label>
                        <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'>'></asp:Label>
                        <asp:Label ID="lblHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'>'></asp:Label>
                    </div>
                    <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="Modify" />
                    <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="DeleteMeeting" />
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Ser#
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSRNO" runat="server"
                        Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
            <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
            <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
            <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
            <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
            <asp:TemplateField HeaderText="Coach">

                <ItemTemplate>

                    <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
            <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
            <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


            <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
            <asp:BoundField DataField="StartDate" HeaderText="Makeup Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
            <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
            <asp:TemplateField HeaderText="Begin Time">

                <ItemTemplate>
                    <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Time" Visible="false">

                <ItemTemplate>
                    <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
            <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
            <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
            <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


            <asp:TemplateField HeaderText="meeting URL">

                <ItemTemplate>

                    <div style="display: none;">
                        <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString()+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>

                        <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString() %></asp:Label>

                        <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                        <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                        <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                    </div>
                    <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />

                    <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Vroom" HeaderText="Vroom"></asp:BoundField>
            <asp:BoundField DataField="Pwd" HeaderText="Pwd" Visible="false"></asp:BoundField>
        </Columns>

        <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

        <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
    </asp:GridView>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <table id="trChildList" runat="server" visible="false">
            <tr>
                <td align="center">
                    <div style="font-weight: bold; color: #64A81C;">Table 2 : Child List</div>
                    <div style="clear: both;">
                    </div>

                    <div style="clear: both;"></div>
                    <asp:Label ID="lblChildMsg" runat="server" Style="color: red;"></asp:Label>
                    <div style="clear: both;"></div>
                    <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdStudentsList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 900px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdStudentsList_RowCommand">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                                <ItemTemplate>
                                    <div style="display: none;">
                                        <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                        <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                        <asp:Label ID="lblPMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>'>'></asp:Label>
                                        <asp:Label ID="LblChildURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeJoinURL") %>'>'></asp:Label>
                                        <asp:Label ID="lblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>


                                        <asp:Button ID="btnSelect" runat="server" Text="Register" CommandName="Register" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Ser#
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSRNO" runat="server"
                                        Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Child"></asp:BoundField>
                            <asp:BoundField DataField="ParentName" HeaderText="Parent"></asp:BoundField>
                            <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                            <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                            <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                            <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                            <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                            <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                            <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                            <asp:BoundField DataField="AttendeeID" HeaderText="Attendee ID" Visible="false"></asp:BoundField>
                            <asp:BoundField DataField="RegisteredID" HeaderText="Registered ID" Visible="false"></asp:BoundField>
                            <asp:TemplateField HeaderText="meeting URL">

                                <ItemTemplate>
                                    <div style="display: none;">
                                        <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>
                                        <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'><%# Eval("Time").ToString().Substring(0,Math.Min(5,Eval("Time").ToString().Length)) %></asp:Label>

                                        <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>

                                    </div>
                                    <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                    <div style="clear: both;"></div>
                    <asp:Button ID="btnClosetable2" Style="margin-bottom: 10px;" runat="server" Text="Close Table 2" OnClick="btnClosetable2_Click" />
                </td>
            </tr>
        </table>
    </div>

    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey1" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingPwd" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingUrl" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnWebExPwd" value="0" runat="server" />
    <input type="hidden" id="hdnCoachID" value="0" runat="server" />
    <input type="hidden" id="hdnChildID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeURL" value="0" runat="server" />
    <input type="hidden" id="hdnException" value="Error Occured....." runat="server" />
    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />

    <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="hdnCoachname" value="0" runat="server" />
    <input type="hidden" id="HdnLevel" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
    <input type="hidden" id="hdnSignupID" value="0" runat="server" />
    <input type="hidden" id="hdnWebConfLogID" value="0" runat="server" />
    <input type="hidden" id="hdnZoomURL" value="0" runat="server" />
    <input type="hidden" id="hdnDuration" value="0" runat="server" />
    <input type="hidden" id="hdnSessionNo" value="0" runat="server" />
    <input type="hidden" id="hdnStartDate" value="0" runat="server" />
    <input type="hidden" id="hdnStartTime" value="" runat="server" />
    <input type="hidden" id="hdnDay" value="" runat="server" />

    <input type="hidden" id="hdnZoomUserID" value="0" runat="server" />
    <input type="hidden" id="hdnZoomPwd" value="0" runat="server" />
    <input type="hidden" id="hdnHostID" value="0" runat="server" />

</asp:Content>
