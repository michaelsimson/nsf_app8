﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;

public partial class SearchParentandChild : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    string strSql;
    bool Ischild=false;
    string StrQrySearch;
    string WhCont = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }


         if (!IsPostBack)
         {
             States();
         }

    }
    protected void States()
    {
        try
        {
            string DDLstate = "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
            ddlState.DataSource = dsstate;
            ddlState.DataTextField = "Name";
            ddlState.DataValueField = "StateCode";
            ddlState.DataBind();
            ddlState.Items.Insert(0, "Select State");
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lblchError.Text = "";
        bool executeQuery = false;

        string sqlString = "SELECT distinct  CH.ChildNumber,CH.FIRST_NAME ,CH.LAST_NAME ,CH.GRADE,CH.SchoolName,CH.DATE_OF_BIRTH, ";
        sqlString += " I.FirstName +' ' + I.LastName as ParentName,CH.MEMBERID,I.Chapter as Center,I.City,I.State,";
        sqlString += " I.Email as ParentsEmail,I.HPhone as ParentsPhone ";
        sqlString += "  FROM   CHILD CH Inner JOIN IndSpouse I ON CH.Memberid=I.AutoMemberID  ";
        sqlString += "  left join StateMaster St on st.StateCode=I.State ";
        if (txtChName.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where CH.first_name + ' ' + CH.last_name  like '%" + txtChName.Text + "%' ";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and CH.first_name + ' ' + CH.last_name  like '%" + txtChName.Text + "%' ";
            }
        }
        if (txtParentName.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where I.firstname + ' ' + I.lastname like '%" + txtParentName.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and I.firstname + ' ' + I.lastname like '%" + txtParentName.Text + "%'";
                executeQuery = true;
            }
        }

        if (txtEmail.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where (I.Email like '%" + txtEmail.Text + "%')";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and (I.Email like '%" + txtEmail.Text + "%')";
                executeQuery = true;
            }
        }
        if (txtPhone.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where I.Hphone like '%" + txtPhone.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and I.Hphone like '%" + txtPhone.Text + "%'";
                executeQuery = true;
            }
        }
        try
        {
            DataSet ds = null;
            Ischild = true;
            SearchMembers(WhCont);
            if (executeQuery == true)
            {
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlString + WhCont);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                Panel5.Visible = true;
                GVchild.Visible = true;
                GVchild.DataSource = ds.Tables[0];
                GVchild.DataBind();
                lblchError.Text = "";
            }
            else
            {
                lblchError.Text = "No Record found";
            }
        }
        catch (Exception ex) { }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
          
   
            string firstName = string.Empty;
            string lastName = string.Empty;
            string email = string.Empty;
            string state = string.Empty;
            firstName = txtFirstName.Text;
            lastName = txtLastName.Text;
            email = TextBox2.Text;
            int length = 0;
            if (firstName.Length > 0)
            {
                strSql += ("I.firstName like '%" + firstName + "%'");
            }
            if (lastName.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += (" and I.lastName like '%" + lastName + "%'");
                }
                else
                {
                    strSql += ("  I.lastName like '%" + lastName + "%'");
                }
            }

            if (email.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += ("and I.Email like '%" + email + "%'");
                }
                else
                {
                    strSql += ("  I.Email like '%" + email + "%'");
                }
            }
            if (ddlState.SelectedItem.Text != "Select State")
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                state = ddlState.SelectedValue;
                if (length > 0)
                {
                    strSql += ("  and I.state like '%" + state + "%'");
                }
                else
                {
                    strSql += ("   I.state like '%" + state + "%'");
                }
            }
            if ((firstName.Length > 0) || (lastName.Length > 0) || (email.Length > 0) || (state.Length > 0))
            {
                strSql += ("  order by I.lastname,I.firstname");
                SearchMembers(strSql);
            }
            else
            {
                lbsearchresults.Text = "Please Enter Email (or) First Name (or) Last Name (or) state";
                lbsearchresults.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    private void SearchMembers(string StrSearchcond)
    {
        try
        {
            if (Ischild == true)
            {
                StrQrySearch = "select distinct I.automemberid,I.Employer,I.firstname,I.lastname,I.donortype,I.email,case when I.Email<>'' then 'Yes' else 'NO' end as [Exists],I.hphone,I.chapter,I.Cphone,"
        + " I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID left join child ch on  ch.Memberid=I.automemberID   " + StrSearchcond + "";
           
            }
            else
            {
                StrQrySearch = "select distinct I.automemberid,I.Employer,I.firstname,I.lastname,I.donortype,I.email,case when I.Email<>'' then 'Yes' else 'NO' end as [Exists],I.hphone,I.chapter,I.Cphone,"
        + " I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID where  " + StrSearchcond + "";
           
            }
         
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
            {
                lbsearchresults.Visible = true;
                lbsearchresults.Text = "No Search found ";
            }
            else
            {
                lbsearchresults.Visible = false;
            }

            Panel4.Visible = true;
            Panel3.Visible = true;
            GVLogin.Visible = true;
            GVprofile.Visible = true;
            GVLogin.DataSource = ds;
           
            GVLogin.DataBind();
            GVprofile.DataSource = ds;
            GVprofile.DataBind();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        pIndSearch.Visible = true;
        Pnlchild.Visible = false;
        visiblePanel();
        txtLastName.Text = string.Empty;
        txtFirstName.Text = string.Empty;
        TextBox2.Text = string.Empty;
        ddlState.SelectedIndex = 0;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Pnlchild.Visible = true;
        Ischild = true;
        pIndSearch.Visible = false;
        visiblePanel();
        txtChName.Text = string.Empty;
        txtParentName.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtPhone.Text = string.Empty;

    }
    protected void btnIndClose_Click(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;
        Panel4.Visible = false;
        Panel3.Visible = false;
        visiblePanel();

    }
    protected void btnclose_Click(object sender, EventArgs e)
    {
        Pnlchild.Visible = false;
        GVchild.Visible = false;
        Panel5.Visible = false;
        visiblePanel();
    }

    protected void visiblePanel()
    {
        GVchild.Visible = false;
        GVLogin.Visible = false;
        GVprofile.Visible = false;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
    }
}