﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="SurveyList.aspx.cs" Inherits="SurveyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div style="text-align: left">
        <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">

        <%--  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
        --%>
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <%--<script src="js/jquery.table2excel.js"></script>--%>
        <%--  <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>--%>
        <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

        <link href="css/Confirm/jquery.confirm.css" rel="stylesheet" />
        <script src="css/Confirm/jquery.confirm.js"></script>
        <script src="Scripts/jquery.toastmessage.js"></script>
        <script src="js/jquery.table2excel.js"></script>
        <link href="css/jquery.toastmessage.css" rel="stylesheet" />
        <style type="text/css">
            #fountainTextG {
                width: 234px;
                margin: auto;
            }

            .fountainTextG {
                color: rgb(0,0,0);
                font-family: Arial;
                font-size: 24px;
                text-decoration: none;
                font-weight: normal;
                font-style: normal;
                float: left;
                animation-name: bounce_fountainTextG;
                -o-animation-name: bounce_fountainTextG;
                -ms-animation-name: bounce_fountainTextG;
                -webkit-animation-name: bounce_fountainTextG;
                -moz-animation-name: bounce_fountainTextG;
                animation-duration: 2.09s;
                -o-animation-duration: 2.09s;
                -ms-animation-duration: 2.09s;
                -webkit-animation-duration: 2.09s;
                -moz-animation-duration: 2.09s;
                animation-iteration-count: infinite;
                -o-animation-iteration-count: infinite;
                -ms-animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
                -moz-animation-iteration-count: infinite;
                animation-direction: normal;
                -o-animation-direction: normal;
                -ms-animation-direction: normal;
                -webkit-animation-direction: normal;
                -moz-animation-direction: normal;
                transform: scale(.5);
                -o-transform: scale(.5);
                -ms-transform: scale(.5);
                -webkit-transform: scale(.5);
                -moz-transform: scale(.5);
            }

            #fountainTextG_1 {
                animation-delay: 0.75s;
                -o-animation-delay: 0.75s;
                -ms-animation-delay: 0.75s;
                -webkit-animation-delay: 0.75s;
                -moz-animation-delay: 0.75s;
            }

            #fountainTextG_2 {
                animation-delay: 0.9s;
                -o-animation-delay: 0.9s;
                -ms-animation-delay: 0.9s;
                -webkit-animation-delay: 0.9s;
                -moz-animation-delay: 0.9s;
            }

            #fountainTextG_3 {
                animation-delay: 1.05s;
                -o-animation-delay: 1.05s;
                -ms-animation-delay: 1.05s;
                -webkit-animation-delay: 1.05s;
                -moz-animation-delay: 1.05s;
            }

            #fountainTextG_4 {
                animation-delay: 1.2s;
                -o-animation-delay: 1.2s;
                -ms-animation-delay: 1.2s;
                -webkit-animation-delay: 1.2s;
                -moz-animation-delay: 1.2s;
            }

            #fountainTextG_5 {
                animation-delay: 1.35s;
                -o-animation-delay: 1.35s;
                -ms-animation-delay: 1.35s;
                -webkit-animation-delay: 1.35s;
                -moz-animation-delay: 1.35s;
            }

            #fountainTextG_6 {
                animation-delay: 1.5s;
                -o-animation-delay: 1.5s;
                -ms-animation-delay: 1.5s;
                -webkit-animation-delay: 1.5s;
                -moz-animation-delay: 1.5s;
            }

            #fountainTextG_7 {
                animation-delay: 1.64s;
                -o-animation-delay: 1.64s;
                -ms-animation-delay: 1.64s;
                -webkit-animation-delay: 1.64s;
                -moz-animation-delay: 1.64s;
            }




            @keyframes bounce_fountainTextG {
                0% {
                    transform: scale(1);
                    color: rgb(0,0,0);
                }

                100% {
                    transform: scale(.5);
                    color: rgb(255,255,255);
                }
            }

            @-o-keyframes bounce_fountainTextG {
                0%;

            {
                -o-transform: scale(1);
                color: rgb(0,0,0);
            }

            100% {
                -o-transform: scale(.5);
                color: rgb(255,255,255);
            }

            }

            @-ms-keyframes bounce_fountainTextG {
                0% {
                    -ms-transform: scale(1);
                    color: rgb(0,0,0);
                }

                100% {
                    -ms-transform: scale(.5);
                    color: rgb(255,255,255);
                }
            }

            @-webkit-keyframes bounce_fountainTextG {
                0% {
                    -webkit-transform: scale(1);
                    color: rgb(0,0,0);
                }

                100% {
                    -webkit-transform: scale(.5);
                    color: rgb(255,255,255);
                }
            }

            @-moz-keyframes bounce_fountainTextG {
                0% {
                    -moz-transform: scale(1);
                    color: rgb(0,0,0);
                }

                100% {
                    -moz-transform: scale(.5);
                    color: rgb(255,255,255);
                }
            }

            #overlay {
                z-index: 999;
                position: fixed;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                display: none;
            }

            .sk-fading-circle {
                margin: 100px auto;
                width: 40px;
                height: 40px;
                position: relative;
            }

                .sk-fading-circle .sk-circle {
                    width: 100%;
                    height: 100%;
                    position: absolute;
                    left: 0;
                    top: 0;
                }

                    .sk-fading-circle .sk-circle:before {
                        content: '';
                        display: block;
                        margin: 0 auto;
                        width: 15%;
                        height: 15%;
                        background-color: #ff6b33;
                        border-radius: 100%;
                        -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
                        animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
                    }

                .sk-fading-circle .sk-circle2 {
                    -webkit-transform: rotate(30deg);
                    -ms-transform: rotate(30deg);
                    transform: rotate(30deg);
                }

                .sk-fading-circle .sk-circle3 {
                    -webkit-transform: rotate(60deg);
                    -ms-transform: rotate(60deg);
                    transform: rotate(60deg);
                }

                .sk-fading-circle .sk-circle4 {
                    -webkit-transform: rotate(90deg);
                    -ms-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

                .sk-fading-circle .sk-circle5 {
                    -webkit-transform: rotate(120deg);
                    -ms-transform: rotate(120deg);
                    transform: rotate(120deg);
                }

                .sk-fading-circle .sk-circle6 {
                    -webkit-transform: rotate(150deg);
                    -ms-transform: rotate(150deg);
                    transform: rotate(150deg);
                }

                .sk-fading-circle .sk-circle7 {
                    -webkit-transform: rotate(180deg);
                    -ms-transform: rotate(180deg);
                    transform: rotate(180deg);
                }

                .sk-fading-circle .sk-circle8 {
                    -webkit-transform: rotate(210deg);
                    -ms-transform: rotate(210deg);
                    transform: rotate(210deg);
                }

                .sk-fading-circle .sk-circle9 {
                    -webkit-transform: rotate(240deg);
                    -ms-transform: rotate(240deg);
                    transform: rotate(240deg);
                }

                .sk-fading-circle .sk-circle10 {
                    -webkit-transform: rotate(270deg);
                    -ms-transform: rotate(270deg);
                    transform: rotate(270deg);
                }

                .sk-fading-circle .sk-circle11 {
                    -webkit-transform: rotate(300deg);
                    -ms-transform: rotate(300deg);
                    transform: rotate(300deg);
                }

                .sk-fading-circle .sk-circle12 {
                    -webkit-transform: rotate(330deg);
                    -ms-transform: rotate(330deg);
                    transform: rotate(330deg);
                }

                .sk-fading-circle .sk-circle2:before {
                    -webkit-animation-delay: -1.1s;
                    animation-delay: -1.1s;
                }

                .sk-fading-circle .sk-circle3:before {
                    -webkit-animation-delay: -1s;
                    animation-delay: -1s;
                }

                .sk-fading-circle .sk-circle4:before {
                    -webkit-animation-delay: -0.9s;
                    animation-delay: -0.9s;
                }

                .sk-fading-circle .sk-circle5:before {
                    -webkit-animation-delay: -0.8s;
                    animation-delay: -0.8s;
                }

                .sk-fading-circle .sk-circle6:before {
                    -webkit-animation-delay: -0.7s;
                    animation-delay: -0.7s;
                }

                .sk-fading-circle .sk-circle7:before {
                    -webkit-animation-delay: -0.6s;
                    animation-delay: -0.6s;
                }

                .sk-fading-circle .sk-circle8:before {
                    -webkit-animation-delay: -0.5s;
                    animation-delay: -0.5s;
                }

                .sk-fading-circle .sk-circle9:before {
                    -webkit-animation-delay: -0.4s;
                    animation-delay: -0.4s;
                }

                .sk-fading-circle .sk-circle10:before {
                    -webkit-animation-delay: -0.3s;
                    animation-delay: -0.3s;
                }

                .sk-fading-circle .sk-circle11:before {
                    -webkit-animation-delay: -0.2s;
                    animation-delay: -0.2s;
                }

                .sk-fading-circle .sk-circle12:before {
                    -webkit-animation-delay: -0.1s;
                    animation-delay: -0.1s;
                }

            @-webkit-keyframes sk-circleFadeDelay {
                0%, 39%, 100% {
                    opacity: 0;
                }

                40% {
                    opacity: 1;
                }
            }

            @keyframes sk-circleFadeDelay {
                0%, 39%, 100% {
                    opacity: 0;
                }

                40% {
                    opacity: 1;
                }
            }
        </style>
        <script language="javascript" type="text/javascript">
            function LoadSurveysAPI() {

                var tblHtml = "";
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/testSurvey",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        var jsonData = data.d;
                        var asc = false;
                        var prop = "DateCreated";
                        jsonData = jsonData.sort(function (a, b) {
                            if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                            else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                        });



                        tblHtml += " <thead>";
                        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Ser#</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Action</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Title</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>SurveyID</td>";
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Created</td>"
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Modified</td>"
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Responses</td>";
                        tblHtml += "</tr>";
                        tblHtml += " </thead>";

                        if (JSON.stringify(data.d).length > 0) {
                            $.each(data.d, function (index, value) {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'><input type='button' class='btnApprove' value='Select' attr-SurID=" + value.Id + " attr-title=" + value.Title + " />"
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.Title + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.Id + "";
                                tblHtml += "</td>";


                                var modifiedDate = convertDate(value.DateModified);
                                var createdDate = convertDate(value.DateCreated);


                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + createdDate + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + modifiedDate + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.ResponseCount + "";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";
                            });
                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='6' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";
                        }
                        $("#tblSurveyList").html(tblHtml);
                        if (JSON.stringify(data.d).length > 0) {
                            $("#tblSurveyList").dataTable({
                                "bPaginate": true,
                                "bInfo": true,
                                "bFilter": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "bDestroy": true,
                                //"bSort": false,
                                "aoColumnDefs": [
              { 'bSortable': false, 'aTargets': [0, 1, 2, 5] }
                                ],
                                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    //debugger;
                                    var index = iDisplayIndexFull + 1;
                                    $("td:first", nRow).html(index);
                                    return nRow;
                                }

                            });
                        }
                        $("#spnSMTitle").text("Table 1: Survey List from Survey Monkey");
                        var RoleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;
                        // var RoleID = $("#hdnRoleID").val();
                        //if (RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") {
                        //    // $(".btnSelect").css("display", "none");
                        //    $(".btnResponse").css("display", "block");

                        //} else {
                        //    //  $(".btnSelect").css("display", "block");
                        //    $(".btnResponse").css("display", "none");
                        //}

                        if (arrSurvey.length > 0) {
                            if (RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") {

                                $(".btnSelect").css("display", "none");
                                //$(".btnResponse").css("display", "none");
                                for (var i = 0; i < arrSurvey.length; i++) {

                                    //$("#btnResponse" + arrSurvey[i] + "").css("display", "block");
                                }


                            } else {
                                $(".btnSelect").css("display", "none");
                                for (var i = 0; i < arrSurvey.length; i++) {

                                    $("#btnSelect" + arrSurvey[i] + "").css("display", "block");
                                }
                                //$(".btnResponse").css("display", "none");
                                $(".btnApprove").css("display", "none");
                            }

                        } else {
                            $(".btnSelect").css("display", "none");
                            //$(".btnResponse").css("display", "none");
                            $(".btnApprove").css("display", "block");
                        }
                    },
                    failure: function (response) {
                        //alert(response.d);
                    }
                });

            }


            function GetCollectorList(surveyID) {
                var tblHtml = "";

                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/GetCollectorList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ SurveyID: surveyID }),
                    async: true,
                    cache: false,
                    success: function (data) {
                        var responseURL;


                        if (data.d.length <= 1) {
                            CreateCollector(surveyID);
                            //createCollectorJSON(surveyID);
                        } else {
                            var i = 0;
                            $.each(data.d, function (index, value) {
                                i++;
                                if (i == 1) {
                                    responseURL = value.Url;
                                }
                            });
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                            window.open(responseURL, '_blank');
                        }



                    },
                    failure: function (response) {

                    }
                });
            }



            function GetRespondantsList() {
                var tblHtml = "";
                var surveyID = getParameterByName("SurveyID");
                var jsonData = { SurveyID: surveyID };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/GetRespondantList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {
                        var surveyID = 0;
                        var userID = 0;
                        var respondentID = 0;
                        var i = 0;
                        $.each(data.d, function (index, value) {

                            if (value.Id != "") {
                                if (i < 1) {
                                    surveyID = getParameterByName("SurveyID");
                                    userID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                                    respondentID = value.Id;
                                    i++;
                                }
                            }
                        });

                        updateUserResponse(userID, surveyID, respondentID);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            function updateUserResponse(userID, surveyID, respondentID) {

                var jsonData = { SurveyID: surveyID, MemberID: userID, RespondentID: respondentID };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/updateSurveyResponse",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {
                        location.href = "SurveyList.aspx?source=FS";
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function convertDate(date) {

                var MyDate_String_Value = date;
                var dateVal = new Date
                            (
                                 parseInt(MyDate_String_Value.replace(/(^.*\()|([+-].*$)/g, ''))
                            );
                var dat = dateVal.getMonth() +
                                         1 +
                                       "/" +
                           dateVal.getDate() +
                                       "/" +
                       dateVal.getFullYear();

                return dat;
            }
            $(function (e) {


                var surveyID = getParameterByName("SurveyID");
                if (surveyID != "") {
                    GetRespondantsList();
                } else {
                    var isSession = document.getElementById('<%=hdnIsSessionOut.ClientID%>').value;
                    if (isSession == "false") {
                        location.href = "Maintest.aspx";
                    }
                }

                var RoleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;
                var source = getParameterByName("source");
                if ((RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") && source == "CS") {
                    //loadAllSurvey();

                    LoadSurveysAPI();
                    lisAllSurvey();

                } else {
                    lisAllSurvey();
                }



            });
            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
            function CreateCollector(surveyID) {
                var tblHtml = "";
                var url = "";
                var pageURL = window.location.href;
                pageURL = pageURL.replace("#", "");

                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var url = "https://www.northsouth.org/App9/SurveyList.aspx?MemberID=" + memberID + "&SurveyID=" + surveyID + "";
                url = pageURL + "&MemberID=" + memberID + "&SurveyID=" + surveyID + "";


                var jsonData = { SurveyId: surveyID, URl: url };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/CreateCollector",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {

                        //alert(JSON.stringify(data.d));
                        GetCollectorList(surveyID);


                        // $("#tblSurveyCollectorList").html(tblHtml);
                    },
                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }


            $(document).on("click", ".btnSelect", function (e) {
                var surveyID = $(this).attr("attr-surID");
                GetCollectorList();
                //CreateCollector();
            });
            var arrSurvey = new Array();
            function loadAllSurvey() {
                var entryToken = document.getElementById('<%=hdnEntryToken.ClientID%>').value;
                var roleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;
                var jsonData = { EntryToken: entryToken, RoleID: roleID };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/GetAllSurvey",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {


                        $.each(data.d, function (index, value) {
                            arrSurvey.push(value.SurveyID);
                        });
                    }
                })
            }

            $(document).on("click", ".btnApprove", function (e) {
                $("#tblSurveyList tbody tr").css("background-color", "#fff");
                //$(this).parent().parent().prev().css("background-color", "#fff");
                $(this).parent().parent().css("background-color", "#E5F4F3");

                var surveyTitle = $(this).parent().next()[0].innerHTML;

                $("#hdnSurveyTite").val(surveyTitle);


                $("#dvSNewSurvey").css("display", "block");
                // var surveyTitle = $(this).attr("attr-title");
                var surveyID = $(this).attr("attr-surID");
                $("#hdnSurveyTite").val(surveyTitle);
                $("#hdnSurID").val(surveyID);
                $("#btnSaveSurvey").val("Save");
            });

            function postSurvey(surveyText) {
                var year = document.getElementById('<%=ddlYear.ClientID%>').value;
                var eventID = document.getElementById('<%=ddlEvent.ClientID%>').value;
                var chapterID = document.getElementById('<%=ddlChapter.ClientID%>').value;
                var progGroupID = document.getElementById('<%=ddlPgGroup.ClientID%>').value;
                var respondentType = document.getElementById('<%=ddlRespondantType.ClientID%>').value;

                var prodGrpText = document.getElementById('<%=ddlPgGroup.ClientID%>');

                if (prodGrpText.value != "") {
                    prodGrpText = prodGrpText.options[prodGrpText.selectedIndex].text;
                } else {
                    prodGrpText = "";
                }

                var eventCode = document.getElementById('<%=ddlEvent.ClientID%>');
                eventCode = eventCode.options[eventCode.selectedIndex].text;

                var chapterCode = document.getElementById('<%=ddlChapter.ClientID%>');
                chapterCode = chapterCode.options[chapterCode.selectedIndex].text;
                var surveyTitle = $("#hdnSurveyTite").val();


                // alert(prodGrpText);
                // alert(chapterCode);
                if (progGroupID == "0") {
                    progGroupID = null;
                }
                var approved = $("#selApprove").val();
                var begDate = $("#txtBeginDate").val();
                var endDate = $("#txtEndDate").val();
                var userID = document.getElementById('<%=hdnUserID.ClientID%>').value;

                var mode = 1;
                if ($("#btnSaveSurvey").val() == "Save") {
                    mode = 1;
                } else {
                    mode = 2;
                }
                var jsonData = JSON.stringify({ objSurvey: { SurveyID: $("#hdnSurID").val(), SurveyTitle: surveyTitle, Year: year, EventID: eventID, EventCode: eventCode, ChapterID: chapterID, ChapterCode: chapterCode, ProductGroupID: progGroupID, ProductGroupCode: prodGrpText, RespondentType: respondentType, Open: approved, BeginDate: begDate, EndDate: endDate, CreatedBy: userID, Mode: mode } });
                if (validateNewSurvey() == "1") {
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/PostNewSurvey",
                        data: jsonData,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //  alert(JSON.stringify(data.d));
                            if (JSON.stringify(data.d) == 1) {

                                $().toastmessage('showToast', {
                                    text: 'Survey approved successfully into the NSF database.',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });

                                //$("#spnErrMsg").text("Survey approved successfully into the NSF database.");
                                lisAllSurvey();
                                // location.reload();
                            } else if (JSON.stringify(data.d) == -2) {
                                $().toastmessage('showToast', {
                                    text: 'Duplicate exists',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'error',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            } else if (JSON.stringify(data.d) == -3) {
                                $().toastmessage('showToast', {
                                    text: 'End date should be greater than today date',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'error',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            }
                            else {
                                $().toastmessage('showToast', {
                                    text: 'Operation failed',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'error',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            }
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                } else {
                    validateNewSurvey();
                }
            }
            $(document).on("click", "#btnSaveSurvey", function (e) {

                postSurvey($("#hdnSurveyTite").val());

            });

            function validateNewSurvey() {
                var retVal = "1";
                var year = document.getElementById('<%=ddlYear.ClientID%>').value;
                var eventID = document.getElementById('<%=ddlEvent.ClientID%>').value;
                var chapterID = document.getElementById('<%=ddlChapter.ClientID%>').value;
                var progGroupID = document.getElementById('<%=ddlPgGroup.ClientID%>').value;
                var respondentType = document.getElementById('<%=ddlRespondantType.ClientID%>').value;
                var approved = $("#selApprove").val();
                var begDate = $("#txtBeginDate").val();
                var endDate = $("#txtEndDate").val();

                if (year == "0" || year == "1") {
                    $("#spnErrMsg").text("Please select Year");
                    retVal = "-1";
                }

                else if (respondentType == 0) {
                    $("#spnErrMsg").text("Please select Respondent Type");
                    retVal = "-1";
                }

                else if (begDate == "") {
                    $("#spnErrMsg").text("Please enter Begin Date");
                    retVal = "-1";
                }
                else if (endDate == "") {
                    $("#spnErrMsg").text("Please enter End Date");
                    retVal = "-1";
                }

                return retVal;
            }

            function lisAllSurvey() {

                var source = getParameterByName("source");

                var tblHtml = "";
                var entryToken = document.getElementById('<%=hdnEntryToken.ClientID%>').value;
                var roleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;
                var year = document.getElementById('<%=ddlYear.ClientID%>').value;
                var eventID = document.getElementById('<%=ddlEvents.ClientID%>').value;
                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var source = getParameterByName("source");
                var jsonData = { EntryToken: entryToken, RoleID: roleID, EventID: eventID, Year: year, MemberID: memberID, Source: source };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/GetAllSurvey",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {
                        tblHtml += " <thead>";
                        tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Ser#</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Action</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Event</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Product Group Code</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Open</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>SurveyTitle</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>SurveyID</td>";
                        if (getParameterByName("source") == "FS") {

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Completed</td>";
                        }
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Begin Date</td>"
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>End Date</td>"

                        tblHtml += "</tr>";
                        tblHtml += " </thead>";
                        if (JSON.stringify(data.d.length) > 0) {
                            $.each(data.d, function (index, value) {

                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>";

                                if (getParameterByName("source") == "FS") {

                                    tblHtml += " <input type='button' class='btnRespondSurvey' id='btnSelect" + value.SurveyID + "' attr-ResType='" + value.RespondentType + "'  value='Fill out a Survey' attr-SurID=" + value.SurveyID + " style='display:block; float:left;' />";
                                } else if (getParameterByName("source") == "CS") {

                                    tblHtml += "<input type='button' class='btnExportSurvey' id='btnExport" + value.SurveyID + "' attr-ResType='" + value.RespondentType + "'  value='Export Responses' attr-SurID=" + value.SurveyID + " style=' float:left;' />";
                                }

                                tblHtml += " <input type='button' class='btnResponse' style='float:left;' id='btnResponse" + value.SurveyID + "' value='View Response' attr-SurID=" + value.SurveyID + " /> <input type='button' class='btnModify' style='float:left;' id='btnModify" + value.SurveyID + "'  value='Modify' attr-SurID=" + value.SurveyID + "  /> <input type='button' class='DeleteSurvey' id='btnDelete" + value.SurveyID + "' style='float:left;'  value='Delete' attr-SurID=" + value.SurveyID + "  />";



                                tblHtml += "</td>";



                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.EventCode + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.ProductGroupCode + "";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.Open + "";
                                tblHtml += "</td>";


                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.SurveyTitle + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.SurveyID + "";
                                tblHtml += "</td>";
                                var completed = value.Completed;
                                if (value.Completed == "-1") {
                                    completed = "";
                                }
                                if (getParameterByName("source") == "FS") {
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + completed + "";
                                    tblHtml += "</td>";
                                }

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.BeginDate + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.EndDate + "";
                                tblHtml += "</td>";

                                if (value.Completed == "Y") {

                                    $("#btnSelect" + value.SurveyID + "").attr("disabled", "disabled");
                                }

                                tblHtml += "</tr>";
                            });
                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='9' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";
                        }
                        $("#tblSurveyListFromNSF").html(tblHtml);

                        if (JSON.stringify(data.d.length) > 0) {

                            $("#tblSurveyListFromNSF").dataTable({
                                "bPaginate": true,
                                "bInfo": true,
                                "bFilter": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "bAutoWidth": false,
                                "bDestroy": true,
                                //"bSort": false,
                                "aoColumnDefs": [
              { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 4, 5] }
                                ],
                                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    //debugger;
                                    var index = iDisplayIndexFull + 1;
                                    $("td:first", nRow).html(index);
                                    return nRow;
                                }

                            });
                        }
                        $("#spnNSFTitle").text("Table 2: Survey List from NSF Website");

                        var RoleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;
                        // var RoleID = $("#hdnRoleID").val();
                        //if (RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") {
                        //    // $(".btnSelect").css("display", "none");
                        //    $(".btnResponse").css("display", "block");

                        //} else {
                        //    //  $(".btnSelect").css("display", "block");
                        //    $(".btnResponse").css("display", "none");
                        //}

                        var source = getParameterByName("source");
                        if ((RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") & source == "CS") {

                            $(".btnRespondSurvey").css("display", "block");
                            $(".btnResponse").css("display", "block");
                            $(".btnModify").css("display", "block");
                            $(".DeleteSurvey").css("display", "block");

                            $("#spnNSFTitle").text("Table 2: Survey List from NSF Website");
                        } else {
                            $(".btnRespondSurvey").css("display", "block");
                            $(".btnModify").css("display", "none");
                            $(".btnResponse").css("display", "none");
                            $(".DeleteSurvey").css("display", "none");

                            $("#spnNSFTitle").text("Table 1: Survey List from NSF Website");
                        }


                    }
                })
                $(document).on("click", ".btnModify", function (e) {
                    var surveyID = $(this).attr("attr-SurID");
                    $("#hdnSurID").val(surveyID);
                    getSurveyDetails(surveyID);
                    $("#dvSNewSurvey").css("display", "block");
                    $("#btnSaveSurvey").val("Update");

                });
                $(document).on("click", ".DeleteSurvey", function (e) {

                    var surveyID = $(this).attr("attr-SurID");
                    checkResponse(surveyID);

                });

                function checkResponse(surveyID) {

                    var jsonData = ({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/checkResponse",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            var count = parseInt(data.d);

                            if (parseInt(count) > 0) {
                                $().toastmessage('showToast', {
                                    text: 'You cannot delete this survey, since there are already some responses saved',
                                    sticky: true,
                                    position: 'middle-center',
                                    type: 'error',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                                //alert("You cannot delete this survey, since there are already some responses saved");
                            } else if (parseInt(count) <= 0) {

                                if (confirm("Are you sure you want to delete this survey?")) {

                                    DeleteSurvey(surveyID);
                                }
                            }

                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }

                function DeleteSurvey(surveyID) {
                    var jsonData = ({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/DeleteSurvey",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {

                            if (parseInt(data.d) > 0) {
                                $().toastmessage('showToast', {
                                    text: 'Survey deleted successfully',
                                    sticky: true,
                                    position: 'middle-center',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                                // alert("Survey deleted successfully");
                                lisAllSurvey();

                            }
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });

                }
                $(document).on("click", "#btnCanel", function (e) {
                    $("#dvSNewSurvey").css("display", "none");
                });

                $(document).on("click", "#btnExport", function (e) {
                    //window.open('data:application/vnd.ms-excel,' + $('#dvData').html());

                    $("#surveyResponse").table2excel({
                        // exclude CSS class
                        exclude: ".noExl",
                        name: "Survey Repsponse"
                    });

                    e.preventDefault();
                });

                //$("#btnExport").click(function (e) {
                //    window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
                //    e.preventDefault();
                //});

            }

            function createCollectorJSON(surveyID) {
                var pageURL = window.location.href;
                pageURL = pageURL.replace("#", "");

                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var url = "https://www.northsouth.org/App9/SurveyList.aspx?MemberID=" + memberID + "&SurveyID=" + surveyID + "";
                url = pageURL + "&MemberID=" + memberID + "&SurveyID=" + surveyID + "";

                // url = "http://localhost:52713/App8Full/SurveyList.aspx?MemberID=" + memberID + "&SurveyID=" + surveyID + "";
                $.ajax({
                    url: 'https://api.surveymonkey.net/v2/collectors/create_collector?api_key=cqrfdehmyse424xpqy4njg3e',
                    type: 'post',
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify({ "survey_id": surveyID, "collector": { "type": "weblink", "name": "My Collector", "redirect_url": url } }),
                    headers: {
                        Authorization: 'bearer paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=',
                        'Content-Type': 'application/json',
                    },
                    complete: function (jqXHR, textStatus) {

                        console.log(jqXHR.responseJSON);

                        GetCollectorList(surveyID);

                    }
                });
            }



            function saveResponses(surveyID, memberID, respondentType, completed) {
                var year = document.getElementById('<%=ddlYear.ClientID%>').value;
                var eventId = document.getElementById('<%=ddlEvents.ClientID%>').value;
                var jsonData = ({ objSurvey: { SurveyID: surveyID, MemberID: memberID, RespondantType: respondentType, Completed: completed, CreatedBy: memberID, EventID: eventId, Year: year } });
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/SaveSurveyResponse",
                    data: JSON.stringify(jsonData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d) == 1) {
                            GetCollectorList(surveyID);
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            $(document).on("click", ".btnResponse", function (e) {
                //$("#tblSurveyCollectorList").dataTable().fnDestroy();
                // $("#tblSurveyResponseListNSF").dataTable().fnDestroy();
                var surveyID = $(this).attr("attr-surID");
                document.getElementById('<%=hdnSurveyID.ClientID%>').value = surveyID;
                // LoadSurveysDetails(surveyID);
                GetSurveyTitle(surveyID);
                GetRespondantList(surveyID);
                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var RoleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;
                var source = getParameterByName("source");

                if ((RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") && source == "CS") {
                    memberID = 0;
                } else {
                    memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                }

                loadSurveyResponseFromNSF(surveyID, memberID, "0");
            });

            function loadSurveyResponseFromNSF(surveyID, memberID, respondentID) {

                var jsonData = ({ SurveyID: surveyID, MemberID: memberID, RespondentID: respondentID });
                var surveyCompletedCount = 0;
                var surveyNotCompletedCount = 0;
                var year = document.getElementById('<%=ddlYear.ClientID%>').value;
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/listSurveyResponseFromNSF",
                    data: JSON.stringify(jsonData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var tblHtml = "";
                        tblHtml += " <thead>";
                        tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>SurveyID</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Survey Title</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Event</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:93px;'>Respondent ID</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>MemberID</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Product Group Code</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Name</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Email</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"

                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Completed</td>"
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Accepted</td>"

                        tblHtml += "</tr>";
                        tblHtml += " </thead>";
                        var responseCount = 0;
                        var acceptedCount = 0;
                        var declinedCount = 0;
                        var i = 0;
                        var surveyTitle = "";
                        if (JSON.stringify(data.d.length) > 0) {
                            $("#btnSendEmailToN").show();
                            $("#btnExportResponseToExcel").show();
                            responseCount = JSON.stringify(data.d.length);
                            $.each(data.d, function (index, value) {
                                if (value.Completed == "Y") {
                                    surveyCompletedCount++;
                                } else if (value.Completed == "N") {
                                    surveyNotCompletedCount++;
                                }


                                acceptedCount = value.AcceptedCount;
                                declinedCount = value.DeclinedCount;

                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyID + "";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyTitle + "";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.EventCode + "";
                                tblHtml += "</td>";
                                surveyTitle = value.SurveyTitle.replace("&nbsp;", " ");

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.RespondentID + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.MemberID + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ProductGroupCode + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Name + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Email + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Completed + "";
                                tblHtml += "</td>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Accepted + "";
                                tblHtml += "</td>";



                                tblHtml += "</tr>";
                            });
                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='11' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";

                            $("#btnSendEmailToN").hide();
                            $("#btnExportResponseToExcel").hide();
                        }
                        $("#tblSurveyResponseListNSF").html(tblHtml);
                        $("#dvSurveyResponseListNSF").show();
                        $("#spnNSFSurveyTitle").text(surveyTitle);
                        $("#spnNSFResondentCount").text(responseCount);
                        $("#spnSurveyCompleted").text(surveyCompletedCount);
                        $("#spnSurveyNotComleted").text(surveyNotCompletedCount);

                        $("#spnAccepted").text(acceptedCount);
                        $("#spnDeclined").text(declinedCount)



                        if (JSON.stringify(data.d.length) > 0) {

                            $("#tblSurveyResponseListNSF").dataTable({
                                "bPaginate": true,
                                "bInfo": true,
                                "bFilter": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "bAutoWidth": false,
                                "bDestroy": true,
                                //"bSort": false,
                                "aoColumnDefs": [
              { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 5, 6, 8, 9, 10] }
                                ],
                                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    //debugger;
                                    var index = iDisplayIndexFull + 1;
                                    $("td:first", nRow).html(index);
                                    return nRow;
                                }

                            });
                        }

                        loadVolNoRoles(surveyID, year);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            $(document).on("click", ".btnRespondSurvey", function (e) {
                var surveyID = $(this).attr("attr-surID");
                $("#hdnSurID").val(surveyID);
                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var respondentType = $(this).attr("attr-restype");
                $("#hdnResType").val(respondentType);
                var completed = "N";
                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");
                // getResponseCount(surveyID, memberID, respondentType, completed);

                // saveResponses(surveyID, memberID, respondentType, completed)
                var userName = document.getElementById('<%=hdnUserName.ClientID%>').value;

                $("#confirmOverlay").css("display", "block");
                $("#spnConfirmation").text("Is your name " + userName + " ?");
                //validateSurveyCountUserBased(surveyID, memberID, respondentType, completed);


            });

            $(document).on("click", "#btnYes", function (e) {
                $("#confirmOverlay").css("display", "none");
                var surveyID = $("#hdnSurID").val();
                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var respondentType = $("#hdnResType").val();
                var completed = "N";
                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");

                var userName = document.getElementById('<%=hdnUserName.ClientID%>').value;

                validateSurveyCountUserBased(surveyID, memberID, respondentType, completed);
            });

            $(document).on("click", "#btnNo", function (e) {
                $("#confirmOverlay").css("display", "none");
                $("#fountainTextG").css("display", "none");
                $("#overlay").css("display", "none");
                $().toastmessage('showToast', {
                    text: 'Please login with the email ID in your profile to fill in this survey',
                    sticky: true,
                    position: 'middle-center',
                    type: 'notice',
                    close: function () { console.log("toast is closed ..."); }
                });

                // alert("Please login with the email ID in your profile to fill in this survey");

            });

            function validateSurveyCountUserBased(surveyID, memberID, respondentType, completed) {

                var jsonData = { MemberID: memberID, SurveyID: surveyID };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/validateSurveyCountUserBased",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {
                        var completed = data.d;
                        var compVal = "N";
                        if (completed == "Y") {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");

                            $().toastmessage('showToast', {
                                text: 'You already responded this survey. Please choose another survey',
                                sticky: true,
                                position: 'middle-center',
                                type: 'notice',
                                close: function () { console.log("toast is closed ..."); }
                            });
                            //alert("You already responded this survey. Please choose another survey");
                        } else if (completed == "N") {

                            saveResponses(surveyID, memberID, respondentType, compVal);
                        }
                        else {

                            saveResponses(surveyID, memberID, respondentType, compVal);
                        }
                    }
                })
            }


            function getResponseCount(surveyID, memberID, respondentType, completed) {

                var jsonData = { MemberID: memberID, SurveyID: surveyID };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/ResponseCount",
                    data: JSON.stringify(jsonData),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d) > 0) {
                            GetCollectorList(surveyID);
                        } else {
                            saveResponses(surveyID, memberID, respondentType, completed);
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function getSurveyDetails(surveyID) {

                var jsonData = { SurveyID: surveyID };
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/GetSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {


                        $.each(data.d, function (index, value) {
                            document.getElementById('<%=ddlYear.ClientID%>').value = value.Year;
                            document.getElementById('<%=ddlEvent.ClientID%>').value = value.EventID;
                            document.getElementById('<%=ddlChapter.ClientID%>').value = value.ChapterID;
                            document.getElementById('<%=ddlPgGroup.ClientID%>').value = value.ProductGroupID;
                            document.getElementById('<%=ddlRespondantType.ClientID%>').value = value.RespondentType;

                            document.getElementById("selApprove").value = value.Open;

                            document.getElementById("txtBeginDate").value = value.BeginDate;
                            document.getElementById("txtEndDate").value = value.EndDate;



                        });
                    }
                })
            }

            $(document).on("click", "#btnSearch", function (e) {

                try {
                    $("#tblSurveyList").dataTable().fnDestroy();
                } catch (err) {
                }
                var RoleID = document.getElementById('<%=hdnRoleID.ClientID%>').value;

                var source = getParameterByName("source");
                if ((RoleID == "1" || RoleID == "2" || RoleID == "96" || RoleID == "89") && source == "CS") {
                    //loadAllSurvey();

                    LoadSurveysAPI();
                    lisAllSurvey();

                } else {
                    lisAllSurvey();
                }

            });

            function GetRespondantList(surveyID) {


                var tblHtml = "";
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/GetRespondantList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        tblHtml += " <thead>";
                        tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";
                        //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Action</td>";
                        //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Srvey Title</td>";
                        //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>#Of Responses</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Ser#</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; width:250px;'>Collector</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; width:100px;'>RespondentID</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; width:100px;'>Started</td>";
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; width:100px;'>Last Modified</td>"

                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Result URL</td>"

                        tblHtml += "</tr>";
                        tblHtml += " </thead>";

                        if (JSON.stringify(data.d.length) > 0) {

                            var ansJson = data.d;
                            var asc = true;
                            var prop = "Id";
                            ansJson = ansJson.sort(function (a, b) {
                                if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                                else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                            });

                            var count = $("#hdnResponseCount").val();
                            $.each(data.d, function (index, value) {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'><input type='button' class='btnRespondentSelect' value='Select' attr-ResID=" + value.RespondentId + " attr-SurID=" + surveyID + " />"
                                //tblHtml += "</td>";
                                //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>2015 - Need Coaches for Online Coaching";
                                //tblHtml += "</td>";
                                //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + $("#hdnResponseCount").val() + "";
                                //tblHtml += "</td>";
                                //count =
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>Web Link 1 (Web Link)";
                                tblHtml += "</td>";



                                alert(value.DateModified);
                                var modifiedDate = convertDate(value.DateModified);
                                var createdDate = convertDate(value.DateCreated);

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.Id + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + createdDate + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + modifiedDate + "";
                                tblHtml += "</td>";

                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'><a href='" + value.AnalyzeUrl + "' target='blank'>" + (value.AnalyzeUrl).substr(0, 60) + "...</a>";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";

                            });
                            //$("#spnRespondentCount").text($("#hdnResponseCount").val());


                            $("#btnExportToExcel").css("display", "none");
                            $("#dvSurveyResponseListSM").show();
                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold; text-align:center;' colspan='6'>No record exists"
                            tblHtml += "</td>";
                            tblHtml += "</tr>";

                            $("#btnExportToExcel").css("display", "none");
                        }
                        $("#dvSurveyResponseListSM").show();
                        $("#tblSurveyCollectorList").html(tblHtml);
                        $("#spnSurveyTitle").text($("#hdnSurveyTitle").val());

                        if (JSON.stringify(data.d.length) > 0) {
                            $("#tblSurveyCollectorList").dataTable({
                                "bPaginate": true,
                                "bInfo": true,
                                "bFilter": false,
                                "bLengthChange": false,
                                "sPaginationType": "full_numbers",
                                "bAutoWidth": false,
                                "bDestroy": true,
                                //"bSort": false,
                                "aoColumnDefs": [
              { 'bSortable': false, 'aTargets': [0, 1, 5] }
                                ],
                                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                    //debugger;
                                    var index = iDisplayIndexFull + 1;
                                    $("td:first", nRow).html(index);
                                    return nRow;
                                }

                            });
                        }
                    },
                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            $(document).on("click", ".btnRespondentSelect", function (e) {
                var memberID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var surveyID = $(this).attr("attr-surID");
                var respondentID = $(this).attr("attr-ResID");
                $("#tblSurveyResponseListNSF").dataTable().fnDestroy();
                loadSurveyResponseFromNSF(surveyID, memberID, respondentID);
            });
            function searchRespondentDetails() {

            }
            function LoadSurveysDetails(surveyID) {


                var jsonData = JSON.stringify({ SurveyID: surveyID });

                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        $.each(data.d, function (index, value) {

                            $("#hdnResponseCount").val(value.NumResponses);
                            $("#hdnSurveyTitle").val(value.Nickname);

                        });

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            }

            function GetSurveyTitle(surveyID) {
                var dvHtml = "";
                //  var surveyID = getParameterByName("SurveyID");
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {
                        //alert(JSON.stringify(data));
                        $.each(data.d, function (index, value) {
                            $("#hdnSurveyTitle").val(value.Title);
                            $("#spnSurveyTitle").text(value.Title);
                            $("#spnRespondentCount").text(value.ResponseCount);
                        });
                    }
                });
            }


            $(document).ready(function () {

                $('.item .delete').click(function () {

                    var elem = $(this).closest('.item');

                    $.confirm({
                        'title': 'Delete Confirmation',
                        'message': 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
                        'buttons': {
                            'Yes': {
                                'class': 'blue',
                                'action': function () {
                                    elem.slideUp();
                                }
                            },
                            'No': {
                                'class': 'gray',
                                'action': function () { }	// Nothing to do in this case. You can as well omit the action property.
                            }
                        }
                    });

                });

            });


            function getResponsesDetails(surveyID) {

                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                var arrQuestionID = new Array();
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {
                        var tblHtml = "";
                        var answerJosn = data.d;
                        tblHtml += "<table>";
                        // tblHtml += "<thead>";
                        tblHtml += "<tr>";
                        tblHtml += "<td style='font-weight:bold;'>SurveyID";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>RespondentID";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>CollectorID";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Start Date";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>End Date";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>IP Address";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Email Address";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>First Name";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Last Name";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Custom Data";
                        tblHtml += "</td>";
                        var colspan = 0;

                        var i = 0;
                        $.each(data.d, function (index, value) {

                            $.each(value.Questions, function (index1, value1) {
                                arrQuestionID.push(value1.QuestionId);
                                if (value1.Answers.length > 0) {
                                    $.each(value1.Answers, function (index5, value5) {

                                        if (value5.Type == "1") {
                                            i++;
                                        }
                                    });
                                    colspan = i;
                                } else {
                                    colspan = 0;
                                }
                                tblHtml += "<td style='font-weight:bold;' colspan='" + colspan + "'>" + value1.Heading + "</td>";
                                i = 0;
                            });
                            tblHtml += "</tr>";
                            tblHtml += "<tr>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";

                            $.each(value.Pages, function (index2, value2) {

                                $.each(value2.Questions, function (index3, value3) {
                                    if (value3.Answers.length > 0) {
                                        var answersData = value3.Answers;
                                        var asc = true;
                                        var prop = "Position";
                                        //answersData = answersData.sort(function (a, b) {
                                        //    if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                                        //    else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                                        //});
                                        // alert(JSON.stringify(answersData));
                                        $.each(answersData, function (index4, value4) {
                                            if (value4.Type == "1") {
                                                tblHtml += "<td style='font-weight:bold;'>" + value4.Text + "</td>";
                                            }
                                        });
                                    } else {

                                        tblHtml += "<td style='font-weight:bold;'>&nbsp;</td>";
                                    }
                                });

                            });
                            tblHtml += "</tr>";

                        });
                        // $("#tblSurveyResponseExcel").html(tblHtml);

                        GetRespondantListExcel(surveyID, tblHtml, answerJosn, arrQuestionID);
                        //tblHtml += "</thead>";

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function GetRespondantListExcel(surveyID, tblHtml, answerJosn, arrQuestionID) {


                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetRespondantList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        var respondentID = "";
                        var json = data;
                        var arrRespondentID = new Array();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                respondentID += value.Id + ",";
                                arrRespondentID.push(value.Id);
                            });

                            var respondantID = respondentID.slice(0, -1);

                            GetResponseExcel(surveyID, arrRespondentID, json, tblHtml, answerJosn, arrQuestionID);
                        }
                        else {
                            var d = new Date();
                            var currentDate = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();

                            $("#tblSurveyResponseExcel").html(tblHtml);
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                            $("#tblSurveyResponseExcel").table2excel({
                                // exclude CSS class
                                exclude: ".noExl",
                                name: "Survey Repsponse",
                                filename: $("#hdnSurveyTite").val() + "_Responses_" + currentDate + ""
                            });
                        }

                    },

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            function GetResponseExcel(surveyID, respondentID, json, tblHtml, answerJosn, arrQuestionID) {
                var arrResQuesID = new Array();
                var jsonData = JSON.stringify({ SurveyID: surveyID, RespondantID: respondentID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetResponseExcel",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {
                        var i = 0;
                        var ansJson = json.d;
                        var asc = true;
                        var prop = "RespondentId";
                        ansJson = ansJson.sort(function (a, b) {
                            if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                            else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                        });

                        $.each(ansJson, function (index, value) {
                            tblHtml += "<tr>";
                            arrResQuesID = [];
                            $.each(data.d, function (index1, value1) {

                                if (value.RespondentId == value1.RespondentId) {
                                    tblHtml += "<td>" + surveyID + "</td>";
                                    tblHtml += "<td>" + value.RespondentId + "</td>";
                                    tblHtml += "<td>" + value.CollectorId + "</td>";
                                    tblHtml += "<td>" + convertJsonDate(value.DateStart) + "</td>";
                                    tblHtml += "<td>" + convertJsonDate(value.DateModified) + "</td>";
                                    tblHtml += "<td>" + value.IpAddress + "</td>";
                                    tblHtml += "<td>" + value.Email + "</td>";
                                    tblHtml += "<td>" + value.FirstName + "</td>";
                                    tblHtml += "<td>" + value.LastName + "</td>";
                                    tblHtml += "<td>&nbsp;</td>";

                                    $.each(value1.Questions, function (index9, value9) {
                                        if (value.RespondentId == value1.RespondentId) {
                                            arrResQuesID.push(value9.QuestionId);
                                        }
                                    });

                                    $.each(answerJosn, function (index2, value2) {
                                        //alert(JSON.stringify(value2.Questions));

                                        $.each(value2.Questions, function (index3, value3) {

                                            $.each(value1.Questions, function (index4, value4) {
                                                //alert(JSON.stringify(value4.Answers));
                                                // 

                                                if (value3.QuestionId == value4.QuestionId) {
                                                    if ($.inArray(arrQuestionID[i], arrResQuesID) > -1) {
                                                    } else {
                                                        tblHtml += "<td>&nbsp;</td>";
                                                    }
                                                    if (value3.Answers.length > 0) {
                                                        $.each(value3.Answers, function (index5, value5) {
                                                            if (value4.Answers.length > 0) {
                                                                $.each(value4.Answers, function (index6, value6) {


                                                                    // if (value6.Row != "0") {
                                                                    // alert(value6.Text);
                                                                    if (value5.AnswerId == value6.Row) {

                                                                        if (value6.Col == "0") {
                                                                            tblHtml += "<td>" + value6.Text + "</td>";
                                                                        } else {
                                                                            $.each(value3.Answers, function (index7, value7) {
                                                                                if (value6.Col == value7.AnswerId) {
                                                                                    tblHtml += "<td>" + value7.Text + "</td>";
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    //} else {
                                                                    //    tblHtml += "<td>" + value6.Text + "</td>";
                                                                    //}
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        //alert(value3.QuestionId);
                                                        if (value3.QuestionId == value4.QuestionId) {

                                                            $.each(value4.Answers, function (index6, value6) {
                                                                if (value6.Row == "0") {
                                                                    tblHtml += "<td>" + value6.Text + "</td>";
                                                                }
                                                            });

                                                        }
                                                    }
                                                }
                                                //} else {
                                                //    tblHtml += "<td>&nbsp;</td>";
                                                //}
                                                i++;
                                            });
                                            i = 0;
                                        });

                                    });


                                    i = 0;
                                }
                            });
                            tblHtml += "</tr>";
                        });
                        var d = new Date();
                        var currentDate = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();
                        tblHtml += "</table>";

                        $("#fountainTextG").css("display", "none");
                        $("#overlay").css("display", "none");
                        $("#tblSurveyResponseExcel").html(tblHtml);
                        var tableHtml = $("#tblSurveyResponseExcel").html();
                        var fileName = $("#hdnSurveyTite").val() + "_Responses_" + currentDate + ""
                        var jsonData = JSON.stringify({ ResponseHTML: tblHtml, FileName: fileName });
                        $.ajax({
                            type: "POST",
                            url: "SurveyList.aspx/GetResponseHTML",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: jsonData,
                            async: true,
                            cache: false,
                            success: function (data) {

                                document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                            }
                        });

                        //var ua = window.navigator.userAgent;
                        //var msie = ua.indexOf("MSIE ");
                        //if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                        //{
                        //    var fileName = $("#hdnSurveyTite").val() + "_Responses_" + currentDate + "";
                        //    exportToExcelIE(tblHtml, fileName);
                        //} else {
                        //    $("#tblSurveyResponseExcel").table2excel({
                        //        // exclude CSS class
                        //        exclude: ".noExl",
                        //        name: "Survey Repsponse",
                        //        filename: $("#hdnSurveyTite").val() + "_Responses_" + currentDate + ""
                        //    });
                        //}
                    },

                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


                }

                $(document).on("click", ".btnExportSurvey", function (e) {
                    var responseText = $(this).parent().next("td").next("td").next("td").next("td").text();
                    var surveyID = $(this).attr("attr-surid");
                    $("#hdnSurveyTite").val(responseText)
                    getResponsesDetails(surveyID);
                });
                //$(function (e) {

                //});

                //$(document).on("click", "#btnExport", function (e) {
                //    //window.open('data:application/vnd.ms-excel,' + $('#dvData').html());

                //    $("#tblSurveyResponseExcel").table2excel({
                //        // exclude CSS class
                //        exclude: ".noExl",
                //        name: "Survey Repsponse",
                //        filename: "Survey Repsponse"
                //    });

                //    e.preventDefault();
                //});





                function convertJsonDate(jsonDate) {
                    var dateString = jsonDate.substr(6);
                    var currentTime = new Date(parseInt(dateString));
                    var month = currentTime.getMonth() + 1;
                    var day = currentTime.getDate();
                    var year = currentTime.getFullYear();
                    var date = month + "/" + day + "/" + year;
                    return date;
                }

                function exportToExcelIE(tblHtml, fileName) {
                    var tableHtml = "";

                    var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
                    var textRange; var j = 0;
                    tab = document.getElementById('tblSurveyResponseExcel'); // id of table



                    tab_text = tblHtml + "</table>";
                    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                    var ua = window.navigator.userAgent;
                    var msie = ua.indexOf("MSIE ");

                    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                        txtArea1.document.open("txt/html", "replace");
                        txtArea1.document.write(tab_text);
                        txtArea1.document.close();
                        txtArea1.focus();
                        sa = txtArea1.document.execCommand("SaveAs", true, fileName + ".xls");
                    }
                }

                //Send Emails to Volunteer
                function senEmailToVolunteer() {
                    $("#fountainTextG").css("display", "block");
                    $("#overlay").css("display", "block");
                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/SendEmailsToVolunteers",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                $("#fountainTextG").css("display", "none");
                                $("#overlay").css("display", "none");
                                $().toastmessage('showToast', {
                                    text: 'Emails sent successfully',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });
                }
                $(document).on("click", "#btnSendEmailToN", function (e) {

                    senEmailToVolunteer();
                });

                function exportNSFResponseToExcel() {
                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/ExportSurveyResponse",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                document.getElementById('<%= btnExportToExcel.ClientID%>').click();
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });
                }
                $(document).on("click", "#btnExportResponseToExcel", function (e) {

                    exportNSFResponseToExcel();
                });


                function bulkVolAssignment() {
                    var eventID = document.getElementById('<%=ddlEvents.ClientID%>').value;
                    var year = document.getElementById('<%=ddlYear.ClientID%>').value;
                    var userID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                    var jsonData = JSON.stringify({ EventID: eventID, MemberID: userID, Year: year });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/BulkVolAssignment",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                $("#fountainTextG").css("display", "none");
                                $("#overlay").css("display", "none");
                                $().toastmessage('showToast', {
                                    text: 'Saved successfully',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            } else {
                                $("#fountainTextG").css("display", "none");
                                $("#overlay").css("display", "none");
                                $().toastmessage('showToast', {
                                    text: 'Problem occured',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'error',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });
                }

                $(document).on("click", "#btnBulkAssignment", function (e) {

                    if (confirm("Are you sure want to do Bulk Assignment?")) {
                        bulkVolAssignment();
                    }
                });


                function loadVolNoRoles(surveyID, year) {

                    var jsonData = ({ SurveyID: surveyID, Year: year });

                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/listNoRoles",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var tblHtml = "";
                            tblHtml += " <thead>";
                            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>SurveyID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Survey Title</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Event</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:93px;'>Respondent ID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>MemberID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Product Group Code</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Name</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Email</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"

                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Completed</td>"
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Accepted</td>"

                            tblHtml += "</tr>";
                            tblHtml += " </thead>";
                            var responseCount = 0;
                            var acceptedCount = 0;
                            var i = 0;
                            var surveyTitle = "";
                            if (JSON.stringify(data.d.length) > 0) {

                                $.each(data.d, function (index, value) {


                                    tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyID + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyTitle + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.EventCode + "";
                                    tblHtml += "</td>";
                                    surveyTitle = value.SurveyTitle.replace("&nbsp;", " ");

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.RespondentID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.MemberID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ProductGroupCode + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Name + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Email + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Completed + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Accepted + "";
                                    tblHtml += "</td>";



                                    tblHtml += "</tr>";

                                });
                                document.getElementById("btnExportNoRoles").style.display = 'block'
                            } else {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";

                                document.getElementById("btnExportNoRoles").style.display = 'none';
                            }
                            $("#tblNoRoles").html(tblHtml);
                            $("#dvNoRoles").show();

                            if (JSON.stringify(data.d.length) > 0) {

                                $("#tblNoRoles").dataTable({
                                    "bPaginate": true,
                                    "bInfo": true,
                                    "bFilter": false,
                                    "bLengthChange": false,
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false,
                                    "bDestroy": true,
                                    //"bSort": false,
                                    "aoColumnDefs": [
                  { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 5, 6, 8, 9, 10] }
                                    ],
                                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                        //debugger;
                                        var index = iDisplayIndexFull + 1;
                                        $("td:first", nRow).html(index);
                                        return nRow;
                                    }

                                });
                            }
                            loadVolNoCalSignup(surveyID, year);
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }

                function loadVolNoCalSignup(surveyID, year) {

                    var jsonData = ({ SurveyID: surveyID, Year: year });

                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/listNoCalSignup",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var tblHtml = "";
                            tblHtml += " <thead>";
                            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>SurveyID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Survey Title</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Event</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:93px;'>Respondent ID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>MemberID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Product Group Code</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Name</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Email</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"

                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Completed</td>"
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Accepted</td>"

                            tblHtml += "</tr>";
                            tblHtml += " </thead>";
                            var responseCount = 0;
                            var acceptedCount = 0;
                            var i = 0;
                            var surveyTitle = "";
                            if (JSON.stringify(data.d.length) > 0) {

                                $.each(data.d, function (index, value) {


                                    tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyID + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyTitle + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.EventCode + "";
                                    tblHtml += "</td>";
                                    surveyTitle = value.SurveyTitle.replace("&nbsp;", " ");

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.RespondentID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.MemberID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ProductGroupCode + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Name + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Email + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Completed + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Accepted + "";
                                    tblHtml += "</td>";



                                    tblHtml += "</tr>";

                                });
                                document.getElementById("btnExportNoCalSignup").style.display = 'block';
                            } else {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";
                                document.getElementById("btnExportNoCalSignup").style.display = 'none';

                            }
                            $("#tblNoCalSignup").html(tblHtml);
                            $("#dvNoCalSignup").show();

                            if (JSON.stringify(data.d.length) > 0) {

                                $("#tblNoCalSignup").dataTable({
                                    "bPaginate": true,
                                    "bInfo": true,
                                    "bFilter": false,
                                    "bLengthChange": false,
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false,
                                    "bDestroy": true,
                                    //"bSort": false,
                                    "aoColumnDefs": [
                  { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 5, 6, 8, 9, 10] }
                                    ],
                                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                        //debugger;
                                        var index = iDisplayIndexFull + 1;
                                        $("td:first", nRow).html(index);
                                        return nRow;
                                    }

                                });
                            }
                            loadVolRolesAndCalSignup(surveyID, year);
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }

                function loadVolRolesAndCalSignup(surveyID, year) {

                    var jsonData = ({ SurveyID: surveyID, Year: year });

                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/listAssignedRolesAndCalSignup",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var tblHtml = "";
                            tblHtml += " <thead>";
                            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";



                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";



                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>SurveyID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Survey Title</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Event</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:93px;'>Respondent ID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>MemberID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Product Group Code</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Name</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Email</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"

                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Completed</td>"
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Accepted</td>"

                            tblHtml += "</tr>";
                            tblHtml += " </thead>";
                            var responseCount = 0;
                            var acceptedCount = 0;
                            var i = 0;
                            var surveyTitle = "";
                            if (JSON.stringify(data.d.length) > 0) {

                                $.each(data.d, function (index, value) {


                                    tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'></td>";


                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyID + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyTitle + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.EventCode + "";
                                    tblHtml += "</td>";
                                    surveyTitle = value.SurveyTitle.replace("&nbsp;", " ");

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.RespondentID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.MemberID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ProductGroupCode + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Name + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Email + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Completed + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Accepted + "";
                                    tblHtml += "</td>";



                                    tblHtml += "</tr>";


                                });

                                document.getElementById("btnExportVolRolesAndCalSignup").style.display = 'block';
                            } else {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";

                                document.getElementById("btnExportVolRolesAndCalSignup").style.display = 'none';

                            }
                            $("#tblRoleAssignedAndCalSignup").html(tblHtml);
                            $("#dvRoleAssignedAndCalSignup").show();

                            if (JSON.stringify(data.d.length) > 0) {

                                $("#tblRoleAssignedAndCalSignup").dataTable({
                                    "bPaginate": true,
                                    "bInfo": true,
                                    "bFilter": false,
                                    "bLengthChange": false,
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false,
                                    "bDestroy": true,
                                    //"bSort": false,
                                    "aoColumnDefs": [
                  { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 5, 6, 8, 9, 10] }
                                    ],
                                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                        //debugger;
                                        var index = iDisplayIndexFull + 1;
                                        $("td:first", nRow).html(index);
                                        return nRow;
                                    }

                                });
                            }
                            loadUnAcceptedCoaches(surveyID, year);
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }

                function exportNoRoles() {

                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/ExportSurveyResponse",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                document.getElementById('<%= btnExportToExcelNoRoles.ClientID%>').click();
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });


                }
                function exportNoCalSignup() {

                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/ExportSurveyResponse",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                document.getElementById('<%= btnExportToExcelNoCalSignup.ClientID%>').click();
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });


                }
                function exportRolesAndCalSignup() {

                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/ExportSurveyResponse",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                document.getElementById('<%= btnExportToExcelCalSignupRoles.ClientID%>').click();
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });

                }

                function exportunAcceptedCoaches() {

                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/ExportSurveyResponse",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                document.getElementById('<%= btnExportUnAccepCoaches.ClientID%>').click();
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });

                }

                function exportDeclinedCoaches() {

                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyID: surveyID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/ExportSurveyResponse",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                document.getElementById('<%= btnExportDeclinedCoaches.ClientID%>').click();
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });

                }

                $(document).on("click", "#btnExportVolRolesAndCalSignup", function (e) {


                    exportRolesAndCalSignup();


                });

                $(document).on("click", "#btnExportNoCalSignup", function (e) {
                    exportNoCalSignup();


                });

                $(document).on("click", "#btnExportNoRoles", function (e) {
                    exportNoRoles();


                });

                $(document).on("click", "#btnExportUnAcceptedList", function (e) {


                    exportunAcceptedCoaches();


                });

                $(document).on("click", "#btnExportDeclined", function (e) {


                    exportDeclinedCoaches();


                });




                function loadUnAcceptedCoaches(surveyID, year) {

                    var jsonData = ({ SurveyID: surveyID, Year: year });
                    $("#hdnClSurID").val(surveyID);
                    $("#hdnYear").val(year);
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/listunAcceptedCoaches",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var tblHtml = "";
                            tblHtml += " <thead>";
                            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Action</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>SurveyID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Survey Title</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Event</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:93px;'>Respondent ID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>MemberID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Product Group Code</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Name</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Email</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"

                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Completed</td>"
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Accepted</td>"

                            tblHtml += "</tr>";
                            tblHtml += " </thead>";
                            var responseCount = 0;
                            var acceptedCount = 0;
                            var i = 0;
                            var surveyTitle = "";
                            if (JSON.stringify(data.d.length) > 0) {

                                $.each(data.d, function (index, value) {


                                    tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'></td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'><input type='button' class='btnDecide' attr-SurID=" + value.SurveyID + " attr-surRespID=" + value.SurveyResponseID + " value='Decide' /></td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyID + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyTitle + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.EventCode + "";
                                    tblHtml += "</td>";
                                    surveyTitle = value.SurveyTitle.replace("&nbsp;", " ");

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.RespondentID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.MemberID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ProductGroupCode + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Name + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Email + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Completed + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Accepted + "";
                                    tblHtml += "</td>";



                                    tblHtml += "</tr>";


                                });

                                document.getElementById("btnExportUnAcceptedList").style.display = 'block';
                            } else {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";

                                document.getElementById("btnExportUnAcceptedList").style.display = 'none';

                            }
                            $("#tblUnAcceptedSurveyResp").html(tblHtml);
                            $("#dvUnAcceptedCoaches").show();

                            if (JSON.stringify(data.d.length) > 0) {

                                $("#tblUnAcceptedSurveyResp").dataTable({
                                    "bPaginate": true,
                                    "bInfo": true,
                                    "bFilter": false,
                                    "bLengthChange": false,
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false,
                                    "bDestroy": true,
                                    //"bSort": false,
                                    "aoColumnDefs": [
                  { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 5, 6, 8, 9, 10] }
                                    ],
                                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                        //debugger;
                                        var index = iDisplayIndexFull + 1;
                                        $("td:first", nRow).html(index);
                                        return nRow;
                                    }

                                });
                            }
                            loadDeclinedCoaches(surveyID, year);
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }

                $(document).on("click", ".btnDecide", function (e) {
                    var surveyRsponseID = $(this).attr("attr-surrespid");
                    $("#hdnSurveyResponseID").val(surveyRsponseID);
                    $("#dvDecide").show();
                    $("#dvDeclineDecide").hide();
                });

                $(document).on("click", ".btnDeclineDecide", function (e) {
                    var surveyRsponseID = $(this).attr("attr-surrespid");
                    $("#hdnSurveyResponseID").val(surveyRsponseID);
                    $("#dvDeclineDecide").show();
                    $("#dvDecide").hide();
                });

                function updateDecission() {

                    var surveyResponseID = $("#hdnSurveyResponseID").val();

                    var decide = $("#selDecide").val();
                    var userID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                    var jsonData = JSON.stringify({ SurveyResponseID: surveyResponseID, Decide: decide, UserID: userID });
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/UpdateDecission",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {

                                $().toastmessage('showToast', {
                                    text: 'Updated successfully',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                                var surveyID = $("#hdnClSurID").val();
                                var year = $("#hdnYear").val();
                                loadUnAcceptedCoaches(surveyID, year);
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });

                }
                $(document).on("click", "#btnSaveDecide", function (e) {

                    updateDecission();
                });

                $(document).on("click", "#btnCancel", function (e) {
                    $("#dvDecide").hide();
                });

                $(document).on("click", "#btnDeclineDecideSave", function (e) {

                    var decide = $("#selDecide").val();
                    if (decide == "Y") {
                        if (confirm("Are you sure want to Accept?")) {
                            updateDecission();
                        }
                    }
                });

                $(document).on("click", "#btnDeclineDecideCancel", function (e) {
                    $("#dvDeclineDecide").hide();
                });




                function loadDeclinedCoaches(surveyID, year) {

                    var jsonData = ({ SurveyID: surveyID, Year: year });
                    $("#hdnClSurID").val(surveyID);
                    $("#hdnYear").val(year);
                    $.ajax({
                        type: "POST",
                        url: "SurveyList.aspx/listDeclinedCoaches",
                        data: JSON.stringify(jsonData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var tblHtml = "";
                            tblHtml += " <thead>";
                            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Action</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>SurveyID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Survey Title</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Event</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:93px;'>Respondent ID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>MemberID</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Product Group Code</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Name</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Email</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"

                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Completed</td>"
                            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:75px;'>Accepted</td>"

                            tblHtml += "</tr>";
                            tblHtml += " </thead>";
                            var responseCount = 0;
                            var acceptedCount = 0;
                            var i = 0;
                            var surveyTitle = "";
                            if (JSON.stringify(data.d.length) > 0) {

                                $.each(data.d, function (index, value) {


                                    tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'></td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'><input type='button' class='btnDeclineDecide' attr-SurID=" + value.SurveyID + " attr-surRespID=" + value.SurveyResponseID + " value='Decide' /></td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyID + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SurveyTitle + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.EventCode + "";
                                    tblHtml += "</td>";
                                    surveyTitle = value.SurveyTitle.replace("&nbsp;", " ");

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.RespondentID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.MemberID + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ProductGroupCode + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Name + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Email + "";
                                    tblHtml += "</td>";

                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Completed + "";
                                    tblHtml += "</td>";
                                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.Accepted + "";
                                    tblHtml += "</td>";



                                    tblHtml += "</tr>";


                                });

                                document.getElementById("btnExportDeclined").style.display = 'block';
                            } else {
                                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                                tblHtml += "</td>";
                                tblHtml += "</tr>";

                                document.getElementById("btnExportDeclined").style.display = 'none';

                            }
                            $("#tblDeclinedCoaches").html(tblHtml);
                            $("#dvDeclinedCoaches").show();

                            if (JSON.stringify(data.d.length) > 0) {

                                $("#tblDeclinedCoaches").dataTable({
                                    "bPaginate": true,
                                    "bInfo": true,
                                    "bFilter": false,
                                    "bLengthChange": false,
                                    "sPaginationType": "full_numbers",
                                    "bAutoWidth": false,
                                    "bDestroy": true,
                                    //"bSort": false,
                                    "aoColumnDefs": [
                  { 'bSortable': false, 'aTargets': [0, 1, 2, 3, 5, 6, 8, 9, 10] }
                                    ],
                                    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                                        //debugger;
                                        var index = iDisplayIndexFull + 1;
                                        $("td:first", nRow).html(index);
                                        return nRow;
                                    }

                                });
                            }
                        },
                        failure: function (response) {
                            alert(response.d);
                        }
                    });
                }


        </script>
    </div>
    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
    <asp:Button ID="btnExportToExcel" Style="display: none;" runat="server" OnClick="btnExportToExcel_onClick" />

    <asp:Button ID="btnExportToExcelNoRoles" Style="display: none;" runat="server" OnClick="btnExportToExcelNoRoles_onClick" />
    <asp:Button ID="btnExportToExcelNoCalSignup" Style="display: none;" runat="server" OnClick="btnExportToExcelNoCalSignup_onClick" />
    <asp:Button ID="btnExportToExcelCalSignupRoles" Style="display: none;" runat="server" OnClick="btnExportToExcelCalSignupRoles_onClick" />
    <asp:Button ID="btnExportUnAccepCoaches" Style="display: none;" runat="server" OnClick="btnExportUnAccepCoaches_onClick" />
    <asp:Button ID="btnExportDeclinedCoaches" Style="display: none;" runat="server" OnClick="btnExportDeclinedCoaches_onClick" />

    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Survey List
             <br />
        <br />
    </div>
    <br />
    <%--<input type="button" id="btnExport" value=" Export Table data into Excel " />--%>
    <div id="dvData">
        <table id="surveyResponse">
            <%--   <tr class="noExl">
                <th>Column One</th>
                <th>Column Two</th>
                <th>Column Three</th>
            </tr>
            <tr>
                <td>row1 Col1</td>
                <td>row1 Col2</td>
                <td>row1 Col3</td>
            </tr>
            <tr>
                <td>row2 Col1</td>
                <td>row2 Col2</td>
                <td>row2 Col3</td>
            </tr>
            <tr>
                <td>row3 Col1</td>
                <td>row3 Col2</td>
                <td><a href="http://www.jquery2dotnet.com/">http://www.jquery2dotnet.com/</a>
                </td>
            </tr>--%>
        </table>
    </div>

    <table align="center" style="width: 700px;" id="tblEntrySection" runat="server">
        <tr>

            <td align="center" nowrap="nowrap" style="font-weight: bold;">Name</td>
            <td align="left">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>

            <td align="center" nowrap="nowrap" style="font-weight: bold;">Year</td>
            <td align="left">
                <asp:DropDownList ID="ddlYear" runat="server" Width="100px">
                </asp:DropDownList>
            </td>
            <td align="center" nowrap="nowrap" style="font-weight: bold;">Event </td>
            <td align="left">
                <asp:DropDownList ID="ddlEvents" runat="server" Width="100px">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">All</asp:ListItem>
                    <asp:ListItem Value="2016" Selected="True">2016</asp:ListItem>
                    <asp:ListItem Value="2015">2015</asp:ListItem>

                </asp:DropDownList>
            </td>
            <%-- <td align="left" nowrap="nowrap" style="font-weight: bold;">Event&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left"></td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Chapter&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left"></td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Product Group&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left"></td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Respondant Type&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left"></td>--%>
            <td>
                <%-- <asp:Button ID="btnsubmit" runat="server"
                    Text="Submit" OnClick="btnsubmit_Click" Style="display: none;" />--%>
                <input type="button" id="btnSearch" value="Submit" />
            </td>

            <%--    <td align="center">
                <asp:Button ID="BtnExpo"
                    runat="server" Visible="false"
                    Text="Export to Excel" /></td>--%>
        </tr>

    </table>
    <div style="clear: both; margin-bottom: 30px;">
    </div>
    <div id="dvSNewSurvey" style="margin-left: auto; margin-right: auto; width: 900px; display: none;">
        <div style="float: left; width: 300px; margin-left: 200px;">

            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">Event</span></div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlEvent" runat="server" Width="100px" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div style="clear: both; margin-bottom: 10px;">
            </div>
            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">Chapter</span></div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlChapter" runat="server" Width="100px">
                </asp:DropDownList>
            </div>
            <div style="clear: both; margin-bottom: 10px;">
            </div>

            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">Respondent Type</span></div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlRespondantType" runat="server" Width="160px">
                    <asp:ListItem Value="0">Select</asp:ListItem>

                    <asp:ListItem Value="Parents for Feedback">Parents for Feedback</asp:ListItem>
                    <asp:ListItem Value="Coaches for Feedback" Selected="True">Coaches for Feedback</asp:ListItem>
                    <asp:ListItem Value="Prospects for Admin">Prospects for Admin</asp:ListItem>
                    <asp:ListItem Value="Prospects for Coaching">Prospects for Coaching</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div style="clear: both; margin-bottom: 10px;">
            </div>


            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">Product Group</span></div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlPgGroup" runat="server" Width="100px">
                </asp:DropDownList>
            </div>

        </div>
        <div style="float: left; width: 250px;">
            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">Approved</span></div>
            <div style="float: left;">
                <select id="selApprove" style="width: 80px;">
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </div>
            <div style="clear: both; margin-bottom: 10px;">
            </div>
            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">Begin Date</span></div>
            <div style="float: left;">
                <input type="text" id="txtBeginDate" />
            </div>
            <div style="clear: both; margin-bottom: 10px;">
            </div>
            <div style="float: left; width: 100px;"><span style="font-weight: bold; text-align: left;">End Date</span></div>
            <div style="float: left;">
                <input type="text" id="txtEndDate" />
            </div>
            <div style="clear: both; margin-bottom: 10px;">
            </div>



        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="margin-left: auto; margin-right: auto; width: 130px;">
            <div style="float: left;">
                <input type="button" id="btnSaveSurvey" value="Save" />
            </div>
            <div style="float: left;">
                <input type="button" id="btnCanel" value="Cancel" />
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 5px;">
    </div>
    <div align="center">
        <span id="spnErrMsg" style="color: red;"></span>
    </div>

    <div runat="server" align="center" id="dvCreateNewSurvey" visible="false">
        <table align="center" style="width: 300px; background-color: #FFFFCC;">
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Survey Title</td>
                <td align="left" style="width: 140px;">
                    <asp:TextBox ID="txtSurveyTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>

                <td colspan="2" align="center">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" OnClick="BtnSave_Click" />
                </td>

            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div align="center">
        <asp:Label ID="lblMsg" runat="server" ForeColor="red"> </asp:Label>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div align="center" style="font-weight: bold;"><span id="spnSMTitle"></span></div>
    <div style="clear: both;"></div>
    <table align="center" id="tblSurveyList" class="display" cellspacing="0" style="border: 1px solid black; width: 1100px; border-collapse: collapse;">
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" style="font-weight: bold;"><span id="spnNSFTitle"></span></div>
    <table align="center" id="tblSurveyListFromNSF" class="display" style="border: 1px solid black; width: 1110px; border-collapse: collapse;">
    </table>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <%-- <div align="center" style="font-weight: bold;">Table 2: Survey Response List</div>
    <div style="clear: both;"></div>
    <table align="center" id="tblSurveyCollectorList" style="border: 1px solid black; border-collapse: collapse;">
    </table>--%>


    <div style="clear: both;"></div>
    <div id="dvSurveyResponseListSM" style="display: none;">
        <div align="center" style="font-weight: bold;">Table 3: Survey Response List from Survey Monkey</div>
        <div style="clear: both;"></div>
        <div align="center" style="margin-left: auto; margin-right: auto; width: 600px;">
            <div style="float: left;">
                <b>Survey Title:</b> <span id="spnSurveyTitle">N/A</span>

            </div>
            <div style="float: left; margin-left: 20px;"><b>Respondent Count:</b> <span id="spnRespondentCount">N/A</span></div>
        </div>
        <div style="clear: both;"></div>
        <table align="center" id="tblSurveyCollectorList" style="border: 1px solid black; border-collapse: collapse; width: 1100px;">
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div id="dvSurveyResponseListNSF" style="display: none;">
        <div align="center" style="font-weight: bold;">Table 4: Survey Response List from NSF Website</div>
        <div style="clear: both;"></div>
        <div align="center" style="margin-left: auto; margin-right: auto; width: 1000px;">
            <div style="float: left;">
                <b>Survey Title:</b> <span id="spnNSFSurveyTitle">N/A</span>

            </div>
            <div style="float: left; margin-left: 20px;"><b>Respondent Count:</b> <span id="spnNSFResondentCount">N/A</span></div>
            <div style="float: left; margin-left: 20px;"><b>Completed:</b> <span id="spnSurveyCompleted">N/A</span></div>
            <div style="float: left; margin-left: 20px;"><b>Not Completed:</b> <span id="spnSurveyNotComleted">N/A</span></div>
            <div style="float: left; margin-left: 20px;"><b>Accepted:</b> <span id="spnAccepted">N/A</span></div>
            <div style="float: left; margin-left: 20px;"><b>Declined:</b> <span id="spnDeclined">N/A</span></div>
        </div>
        <div style="clear: both;"></div>
        <div style="float: left;">

            <input type="button" id="btnSendEmailToN" value="Send Email to 'N'" />
            <input type="button" id="btnBulkAssignment" value="Bulk Assignment of Coach Role" />
            <%--    <asp:Button ID="btnSendEMail" runat="server" Text="Send EMail to 'N'" OnClick="btnSendEMail_Click" />--%>
        </div>
        <div style="float: right;">
            <input type="button" id="btnExportResponseToExcel" value="Export To Excel" />
            <%-- <asp:Button ID="btnExportNSFResponse" runat="server" Text="Export To Excel" />--%>
        </div>
        <div style="clear: both;"></div>
        <table align="center" class="display" id="tblSurveyResponseListNSF" style="border: 1px solid black; border-collapse: collapse; width: 1110px;">
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div id="dvNoRoles" style="display: none;">
        <div align="center" style="font-weight: bold;">Table 5: Volunteer accepted, but coach role assignment was not made</div>
        <div style="clear: both;"></div>

        <div style="float: right;">
            <input type="button" id="btnExportNoRoles" value="Export To Excel" />

        </div>
        <div style="clear: both;"></div>
        <table align="center" id="tblNoRoles" style="border: 1px solid black; border-collapse: collapse; width: 1100px;">
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div id="dvNoCalSignup" style="display: none;">
        <div align="center" style="font-weight: bold;">
            Table 6: Accepted, coach role was assigned, but calendar signup is missing
        </div>
        <div style="clear: both;"></div>
        <div style="float: right;">
            <input type="button" id="btnExportNoCalSignup" value="Export To Excel" />

        </div>
        <div style="clear: both;"></div>
        <table align="center" id="tblNoCalSignup" style="border: 1px solid black; border-collapse: collapse; width: 1100px;">
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div id="dvRoleAssignedAndCalSignup" style="display: none;">
        <div align="center" style="font-weight: bold;">
            Table 7: Accepted, coach role was assigned, and calendar signup was made

        </div>
        <div style="clear: both;"></div>
        <div style="float: right;">
            <input type="button" id="btnExportVolRolesAndCalSignup" value="Export To Excel" />

        </div>
        <div style="clear: both;"></div>
        <table align="center" id="tblRoleAssignedAndCalSignup" style="border: 1px solid black; border-collapse: collapse; width: 1100px;">
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div id="dvUnAcceptedCoaches" style="display: none;">
        <div align="center" style="font-weight: bold;">
            Table 8: Completed Survey, not yet accepted, not declined

        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="display: none;" id="dvDecide">
            <div align="center">
                <span id="spnDecide" style="font-weight: bold;">Decide</span>

                <select id="selDecide">
                    <option value="Y">Accept</option>
                    <option value="D">Decline</option>
                    <option value="C">Cancel</option>
                    <option value="N">Not Ready</option>

                </select>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center">
                    <input type="button" id="btnSaveDecide" value="Save" />
                    <input type="button" id="btnCancel" value="Cancel" />
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div style="float: right;">
            <input type="button" id="btnExportUnAcceptedList" value="Export To Excel" />

        </div>
        <div style="clear: both;"></div>
        <table align="center" id="tblUnAcceptedSurveyResp" style="border: 1px solid black; border-collapse: collapse; width: 1100px;">
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div id="dvDeclinedCoaches" style="display: none;">
        <div align="center" style="font-weight: bold;">
            Table 9: Completed Survey, Declined

        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="display: none;" id="dvDeclineDecide">
            <div align="center">
                <span id="spnDeclineDecide" style="font-weight: bold;">Decide</span>

                <select id="selDeclineDecide">
                    <option value="Y">Accept</option>
                    <option value="D">Decline</option>
                    <option value="C">Cancel</option>
                    <option value="N">Not Ready</option>

                </select>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center">
                    <input type="button" id="btnDeclineDecideSave" value="Save" />
                    <input type="button" id="btnDeclineDecideCancel" value="Cancel" />
                </div>
            </div>
        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: right;">
            <input type="button" id="btnExportDeclined" value="Export To Excel" />

        </div>
        <div style="clear: both;"></div>
        <table align="center" id="tblDeclinedCoaches" style="border: 1px solid black; border-collapse: collapse; width: 1100px;">
        </table>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>

    <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>
    </div>
    <div id="overlay"></div>

    <div id="confirmOverlay" style="display: none;">
        <div id="confirmBox">

            <center>
                <h1 id="spnConfirmation">Title of the confirm dialog</h1>
            </center>

            <div id="confirmButtons">
                <a class="button blue" id="btnYes" style="background-color: blue" href="#">Yes<span></span></a>
                <a class="button gray" id="btnNo" href="#" style="background-color: red;">No<span></span></a>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <table id="tblSurveyResponseExcel" style="display: none;">
        <tr class="noExl">
            <th>Column One</th>
            <th>Column Two</th>
            <th>Column Three</th>
        </tr>
    </table>
    <input type="hidden" id="hdnSurveyID" runat="server" value="0" />
    <input type="hidden" id="hdnRoleID" value="0" runat="server" />
    <input type="hidden" id="hdnUserID" runat="server" value="0" />
    <input type="hidden" id="hdnSurveyTite" value="0" />
    <input type="hidden" id="hdnSurID" value="0" />
    <input type="hidden" id="hdnResType" value="0" />
    <input type="hidden" id="hdnEntryToken" value="" runat="server" />
    <input type="hidden" id="hdnResponseCount" value="0" />
    <input type="hidden" id="hdnIsSessionOut" runat="server" value="0" />
    <input type="hidden" id="hdnUserName" runat="server" value="0" />
    <input type="hidden" id="hdnButtonDelID" value="0" />
    <input type="hidden" id="hdnSurveyResponseID" value="0" />
    <input type="hidden" id="hdnYear" value="0" />
    <input type="hidden" id="hdnClSurID" value="0" />

</asp:Content>

