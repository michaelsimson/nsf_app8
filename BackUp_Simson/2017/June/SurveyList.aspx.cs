﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using SurveyMonkey.Containers;
using SurveyMonkey.RequestSettings;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;

public partial class SurveyList : System.Web.UI.Page
{
    public static string responseHtml = string.Empty;
    public static string reponseExcelFileName = string.Empty;
    public static string Token = "M4zxG9yQPT6T8y55LmGKr1HTzYtmVf34IDPapvSI2ITpc0YftiSGuehbOl.P8msRkk.tHsdBGPm0FJk-ghM8e6xmlgyRtE85BoJBNke.Asko68FpQloAG7.rKtFUW7gO";
    //  public static string apiKey = "cqrfdehmyse424xpqy4njg3e";
    // public static string apiKey = "_dJ2rzVZQrSYVi5VipVljQ";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Session["LoginID"] = Request.QueryString["MemberID"].ToString();
        try
        {
            if (Request.QueryString["SurveyID"] != null)
            {

                var surveyID = Request.QueryString["SurveyID"];
                HttpCookie aCookie = Request.Cookies["loginCookie1"];
                string userId = Server.HtmlEncode(aCookie.Value);
                hdnUserID.Value = userId;
                hdnSurveyID.Value = surveyID;

                //Response.Redirect("SurveryList.aspx?MemberID=" + hdnUserID.Value + "&SurveyID=" + surveyID + "");
                //updateResponse();

                //string CmdText = string.Empty;


                //CmdText = "Update SurveyResponse set Completed='Y', ModifiedBy=" + userId + ", ModifiedDate=GetDate() where SurveyID=" + surveyID + " and MemberID=" + userId + "";

                //try
                //{
                //    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                //    Response.Redirect("SurveyList.aspx");
                //    ListAllSurvey();
                //}
                //catch (Exception ex)
                //{

                //}
            }
        }
        catch
        {
        }
        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}
        if (!IsPostBack)
        {
            try
            {
                hdnEntryToken.Value = Session["EntryToken"].ToString();
                if (Session["RoleID"] != null)
                {
                    hdnRoleID.Value = Session["RoleID"].ToString();
                }

                Response.Cookies.Clear();
                hdnUserID.Value = Session["LoginID"].ToString();
                HttpCookie loginCookie1 = new HttpCookie("loginCookie");
                Response.Cookies["loginCookie1"].Value = Session["LoginID"].ToString();
                Response.Cookies.Add(loginCookie1);

                //if (hdnRoleID.Value == "88")
                //{
                // tblEntrySection.Visible = false;
                // }
                loadyear();
                fillEvent();
                fillChapter();
                fillProductGroup();
                // ListAllSurvey();

                if (Session["EntryToken"].ToString() == "Parent")
                {
                    lbtnVolunteerFunctions.Text = "Back to Parent Functions";
                    lbtnVolunteerFunctions.PostBackUrl = "~/UserFunctions.aspx";
                }
                else
                {
                    lbtnVolunteerFunctions.Text = "Back to Volunteer Functions";
                    lbtnVolunteerFunctions.PostBackUrl = "~/VolunteerFunctions.aspx";
                }
                getLoginName();
            }
            catch
            {
                hdnIsSessionOut.Value = "false";
            }


        }

    }

    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddlYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddlYear.SelectedValue = DateTime.Now.Year.ToString();
        //ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    public void fillEvent()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        cmdText = "select EventId, Name from Event where Name <>'Employer Volunteer Program' and Name <>'Transfer' and Name<>'General and Admin' and Name <>'Grant'";
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlEvent.DataSource = ds;
            ddlEvent.DataTextField = "Name";
            ddlEvent.DataValueField = "EventId";
            ddlEvent.DataBind();

            ddlEvents.DataSource = ds;
            ddlEvents.DataTextField = "Name";
            ddlEvents.DataValueField = "EventId";
            ddlEvents.DataBind();

            ddlEvent.Items.Insert(0, new ListItem("Select", "0"));
            ddlEvent.SelectedValue = "13";
            ddlEvents.SelectedValue = "13";
        }
        catch (Exception ex)
        {
        }
    }

    public void fillChapter()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        cmdText = "select ChapterID, ChapterCode from Chapter order by state, ChapterCode Asc";
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlChapter.DataSource = ds;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();
            ddlChapter.Items.Insert(0, new ListItem("Select", "0"));
            ddlChapter.SelectedValue = "112";
        }
        catch (Exception ex)
        {

        }
    }

    public void fillProductGroup()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            cmdText = "select ProductGroupId,Name,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + "";

            //cmdText = "select ProductGroupID,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlPgGroup.DataSource = ds;
            ddlPgGroup.DataTextField = "ProductGroupCode";
            ddlPgGroup.DataValueField = "ProductGroupId";
            ddlPgGroup.DataBind();
            //ddlPgGroup.Items.Insert(0, new ListItem("Select", "0"));
            ddlPgGroup.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        //dvSurveyList.Visible = true;
        //ListAllSurvey();
    }
    protected void BtnCreateNewSurvey_Click(object sender, EventArgs e)
    {
        //dvCreateNewSurvey.Visible = true;
    }

    //public void ListAllSurvey()
    //{
    //    string CmdText = string.Empty;
    //    DataSet ds = new DataSet();
    //    CmdText = "select SL.SurveyID,SL.Year,SL.EventID,SL.ChapterID,SL.ProductGroupID,SL.RespondentType,SL.Title,ResponseCount,E.Name as EventName,C.ChapterCode,P.Name from SurveyList SL inner join Event E on(SL.EventID=E.EventID) inner join ProductGroup P on(SL.ProductGroupID=P.ProductGroupID)inner join Chapter C on (SL.ChapterID=C.ChapterID)";
    //    try
    //    {
    //        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
    //        if (null != ds && ds.Tables.Count > 0)
    //        {
    //            if (ds.Tables[0].Rows.Count > 0)
    //            {
    //                GrdSurveyList.DataSource = ds;
    //                GrdSurveyList.DataBind();
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    public void InsertNewSurvey()
    {
        string CmdText = string.Empty;
        if (ValidateSurvey() == "1")
        {
            if (BtnSave.Text == "Save")
            {
                CmdText = "Insert into SurveyList (Year, EventID, ChapterID, ProductGroupID, RespondentType,ResponseCount,Title,CreatedBY,CreatedDate) values(" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + "," + ddlChapter.SelectedValue + "," + ddlPgGroup.SelectedValue + ",'" + ddlRespondantType.SelectedValue + "',0,'" + txtSurveyTitle.Text + "',4240,GetDate())";
            }
            else
            {
                CmdText = "Update SurveyList set Year=" + ddlYear.SelectedValue + ", EventID=" + ddlEvent.SelectedValue + ",ChapterID=" + ddlChapter.SelectedValue + ",ProductGroupID=" + ddlPgGroup.SelectedValue + ",RespondentType='" + ddlRespondantType.SelectedValue + "',Title='" + txtSurveyTitle.Text + "',ModifiedBy=4240, ModifiedDate=GetDate() where SurveyID=" + hdnSurveyID.Value + "";
            }
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                if (BtnSave.Text == "Save")
                {
                    lblMsg.Text = "Survey created successfully";
                }
                else
                {
                    lblMsg.Text = "Survey updated successfully";

                }

                // ListAllSurvey();
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            ValidateSurvey();
        }

    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        InsertNewSurvey();
    }

    //protected void GrdSurveyList_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    try
    //    {
    //        if (e.CommandName == "Select")
    //        {

    //            GridViewRow row = null;

    //            row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
    //            int selIndex = row.RowIndex;



    //            string Year = string.Empty;
    //            string EventID = string.Empty;
    //            string ChapterID = string.Empty;
    //            string ProductGroupID = string.Empty;
    //            string RespondType = string.Empty;
    //            string Title = string.Empty;
    //            Year = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblYear") as Label).Text;
    //            EventID = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblEventID") as Label).Text;
    //            ChapterID = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
    //            ProductGroupID = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
    //            RespondType = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblRespondantType") as Label).Text;
    //            Title = GrdSurveyList.Rows[selIndex].Cells[6].Text;
    //            hdnSurveyID.Value = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblSurveyID") as Label).Text;

    //            ddlEvent.SelectedValue = EventID;
    //            fillProductGroup();
    //            ddlYear.SelectedValue = Year;
    //            ddlChapter.SelectedValue = ChapterID;
    //            ddlPgGroup.SelectedValue = ProductGroupID;
    //            ddlRespondantType.SelectedValue = RespondType;
    //            txtSurveyTitle.Text = Title;
    //           // dvSurveyList.Visible = true;
    //            // dvCreateNewSurvey.Visible = true;
    //            BtnSave.Text = "Update";
    //        }
    //        else if (e.CommandName == "Design Survey")
    //        {
    //            GridViewRow row = null;

    //            row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
    //            int selIndex = row.RowIndex;
    //            hdnSurveyID.Value = ((Label)GrdSurveyList.Rows[selIndex].FindControl("lblSurveyID") as Label).Text;
    //            Response.Redirect("SurveyTemplate.aspx?SurveyID=" + hdnSurveyID.Value + "");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}

    public string ValidateSurvey()
    {
        string Retval = "1";
        if (ddlYear.SelectedValue == "0")
        {
            lblMsg.Text = "Please select year";
            Retval = "-1";
        }
        else if (ddlEvent.SelectedValue == "0")
        {
            lblMsg.Text = "Please select Event";
            Retval = "-1";
        }
        else if (ddlChapter.SelectedValue == "0")
        {
            lblMsg.Text = "Please select Chapter";
            Retval = "-1";
        }
        else if (ddlPgGroup.SelectedValue == "0")
        {
            lblMsg.Text = "Please select Product Group";
            Retval = "-1";
        }
        else if (ddlRespondantType.SelectedValue == "0")
        {
            lblMsg.Text = "Please select Respondent Type";
            Retval = "-1";
        }
        else if (txtSurveyTitle.Text == "")
        {
            lblMsg.Text = "Please fill Survey Title";
            Retval = "-1";
        }
        return Retval;
    }

    [WebMethod]
    public static List<Survey> testSurvey()
    {


        List<Survey> surveys = new List<Survey>();
        using (var api = new SurveyMonkeyApi(Token))
        {
            surveys = api.GetSurveyList();
        }

        return surveys;

    }

    [WebMethod]
    public static List<Survey> ListSurveyDetails(long SurveyID)
    {


        List<Survey> surveys = new List<Survey>();
        Survey sur = new Survey();
        using (var api = new SurveyMonkeyApi(Token))
        {
            sur = api.GetSurveyDetails(SurveyID);
            surveys.Add(sur);
        }


        return surveys;

    }


    [WebMethod]
    public static List<Collector> GetCollectorList(long SurveyID)
    {


        List<Collector> Collector = new List<Collector>();
        using (var api = new SurveyMonkeyApi(Token))
        {
            Collector = api.GetCollectorList(SurveyID);
        }

        return Collector;

    }

    [WebMethod]
    public static List<Response> GetRespondantList(int SurveyID)
    {
        List<Response> ResponseList = new List<Response>();
        using (var api = new SurveyMonkeyApi(Token))
        {
            ResponseList = api.GetSurveyResponseDetailsList(SurveyID);
            // ResponseList = api.GetSurveyResponseOverviewList(SurveyID);
        }
        return ResponseList;
    }

    //[WebMethod]
    //public static List<Respondent> GetRespondantList(int SurveyDID)
    //{

    //    string apiKey = "cqrfdehmyse424xpqy4njg3e";
    //    string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
    //    var sm = new SurveyMonkeyApi(apiKey, token);
    //    var settings = new GetSurveyListSettings
    //    {
    //        //Title = "2015 NSF Finals - Parent Survey",
    //        StartDate = Convert.ToDateTime("01/01/2015")
    //    };


    //    List<Respondent> Respondant = new List<Respondent>();
    //    Respondant = sm.GetRespondentList(SurveyDID);

    //    return Respondant;

    //}


    public class SurveyQuestions
    {
        public string SurveyID { get; set; }
        public string MemberID { get; set; }
        public string SurveyTitle { get; set; }
        public string Year { get; set; }
        public string EventID { get; set; }
        public string EventCode { get; set; }
        public string ChapterID { get; set; }
        public string ChapterCode { get; set; }
        public string ProductGroupID { get; set; }
        public string ProductGroupCode { get; set; }
        public string RespondentType { get; set; }
        public string Open { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public int Mode { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string Completed { get; set; }
        public string Accepted { get; set; }
        public string AcceptedCount { get; set; }
        public string RespondentID { get; set; }
        public string SurveyResponseID { get; set; }
        public string DeclinedCount { get; set; }
    }

    public class SurveyResponse
    {
        public string SurveyID { get; set; }
        public string MemberID { get; set; }
        public string RespondantType { get; set; }
        public string RespDate { get; set; }
        public string Completed { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string EventID { get; set; }
        public string Year { get; set; }

    }

    [WebMethod]
    public static List<SurveyQuestions> GetAllSurvey(string EntryToken, int RoleID, string EventID, string Year, int MemberID, string Source)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select * from SurveyList where [Open]='Yes' and GetDate() between BeginDate and EndDate";
            if (Source == "CS")
            {
                cmdText = "select * from SurveyList SL where [Open]='Yes'";
            }
            else
            {
                cmdText = "select SL.Event,SL.ProductGroupCode,SL.[Open],SL.Title,SL.SurveyID,SR.Completed,SL.BeginDate,SL.EndDate,SL.RespondentType from SurveyList SL left join SurveyResponse SR on(SL.SurveyID=SR.SurveyID and SR.MemberID=" + MemberID + ") where [Open]='Yes' and GetDate() between BeginDate and EndDate";
            }
            if (Year != "0" && Year != "" && Year != "1")
            {
                cmdText += "   and SL.[Year]=" + Year + "";
            }
            if (EventID != "0" && EventID != "")
            {
                cmdText += "   and SL.EventID=" + EventID + "";
            }

            if (EntryToken == "Parent")
            {
                // cmdText = "select * from SurveyList where (RespondentType='Parents for Feedback' or RespondentType='Prospects for Coaching') and [Open]='Yes' and GetDate() between BeginDate and EndDate";

                cmdText = "select SL.Event,SL.ProductGroupCode,SL.[Open],SL.Title,SL.SurveyID,SR.Completed,SL.BeginDate,SL.EndDate,SL.RespondentType from SurveyList SL left join SurveyResponse SR on(SL.SurveyID=SR.SurveyID and SR.MemberID=" + MemberID + ") where (RespondentType='Parents for Feedback' or RespondentType='Prospects for Coaching') and [Open]='Yes' and GetDate() between BeginDate and EndDate";
                if (Year != "0" && Year != "" && Year != "1")
                {
                    cmdText += "   and SL.[Year]=" + Year + "";
                }
                if (EventID != "0" && EventID != "")
                {
                    cmdText += "   and SL.EventID=" + EventID + "";
                }

            }
            else if (EntryToken == "Volunteer" && (RoleID != 1 && RoleID != 2 && RoleID != 96 && RoleID != 89))
            {
                cmdText = "select SL.Event,SL.ProductGroupCode,SL.[Open],SL.Title,SL.SurveyID,SR.Completed,SL.BeginDate,SL.EndDate,SL.RespondentType from SurveyList SL left join SurveyResponse SR on(SL.SurveyID=SR.SurveyID and SR.MemberID=" + MemberID + ") where (RespondentType='Coaches for Feedback' or RespondentType='Prospects for Coaching') and [Open]='Yes' and GetDate() between BeginDate and EndDate";
                if (Year != "0" && Year != "" && Year != "1")
                {
                    cmdText += "   and SL.[Year]=" + Year + "";
                }
                if (EventID != "0" && EventID != "")
                {
                    cmdText += "   and SL.EventID=" + EventID + "";
                }
            }
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["Event"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.BeginDate = Convert.ToDateTime(dr["BeginDate"].ToString()).ToString("MM/dd/yyyy");
                        sur.EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        sur.Open = dr["Open"].ToString();
                        sur.RespondentType = dr["RespondentType"].ToString();
                        try
                        {
                            sur.Completed = dr["Completed"].ToString();
                        }
                        catch
                        {
                        }
                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }
    [WebMethod]
    public static int PostNewSurvey(SurveyQuestions objSurvey)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string cmdText = string.Empty;
            string prodGroupID = objSurvey.ProductGroupID;

            DateTime dtCurrent = DateTime.Now;
            string strCurrentDate = DateTime.Now.ToString("MM/dd/yyyy");

            // string strCurrentDate = DateTime.Now.ToString("dd/MM/yyyy");

            dtCurrent = Convert.ToDateTime(strCurrentDate);
            string startDate = objSurvey.EndDate;
            DateTime dtStartDate = Convert.ToDateTime(startDate.ToString());



            if (dtStartDate <= dtCurrent)
            {

                retVal = -3;
            }
            else
            {
                retVal = 1;
            }
            if (retVal > 0)
            {
                if (objSurvey.Mode == 1)
                {
                    int surveyCount = 0;
                    cmdText = "select count(*) as CountSet from SurveyList where Year=" + objSurvey.Year + " and SurveyID=" + objSurvey.SurveyID + "";
                    SqlCommand cmd = new SqlCommand(cmdText, cn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (null != ds)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            surveyCount = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                        }
                    }
                    if (surveyCount == 0)
                    {
                        if (prodGroupID == "" || prodGroupID == null)
                        {
                            cmdText = "insert into SurveyList (SurveyID,Year, EventID,Event, ChapterID,ChapterCode, ProductGroupID,ProductGroupCode,RespondentType, Title, [Open], BeginDate, EndDate,CreatedBy,CreatedDate) values(" + objSurvey.SurveyID + "," + objSurvey.Year + "," + objSurvey.EventID + ",'" + objSurvey.EventCode + "'," + objSurvey.ChapterID + ",'" + objSurvey.ChapterCode + "',null,null,'" + objSurvey.RespondentType + "','" + objSurvey.SurveyTitle + "','" + objSurvey.Open + "','" + objSurvey.BeginDate + "','" + objSurvey.EndDate + "'," + objSurvey.CreatedBy + ",GetDate())";
                        }
                        else
                        {
                            cmdText = "insert into SurveyList (SurveyID,Year, EventID,Event, ChapterID,ChapterCode, ProductGroupID,ProductGroupCode,RespondentType, Title, [Open], BeginDate, EndDate,CreatedBy,CreatedDate) values(" + objSurvey.SurveyID + "," + objSurvey.Year + "," + objSurvey.EventID + ",'" + objSurvey.EventCode + "'," + objSurvey.ChapterID + ",'" + objSurvey.ChapterCode + "'," + objSurvey.ProductGroupID + ",'" + objSurvey.ProductGroupCode + "','" + objSurvey.RespondentType + "','" + objSurvey.SurveyTitle + "','" + objSurvey.Open + "','" + objSurvey.BeginDate + "','" + objSurvey.EndDate + "'," + objSurvey.CreatedBy + ",GetDate())";
                        }
                        cmd = new SqlCommand(cmdText, cn);
                        cmd.ExecuteNonQuery();
                        retVal = 1;
                    }
                    else
                    {
                        retVal = -2;
                    }

                }
                else if (objSurvey.Mode == 2)
                {
                    if (prodGroupID == "")
                    {
                        cmdText = "Update SurveyList set Year=" + objSurvey.Year + ", EventID=" + objSurvey.EventID + ",Event='" + objSurvey.EventCode + "', ChapterID=" + objSurvey.ChapterID + ",ChapterCode='" + objSurvey.ChapterCode + "',RespondentType='" + objSurvey.RespondentType + "',[Open]='" + objSurvey.Open + "', BeginDate='" + objSurvey.BeginDate + "', EndDate='" + objSurvey.EndDate + "',ModifiedBy=" + objSurvey.CreatedBy + ",ModifiedDate=GetDate() where SurveyID=" + objSurvey.SurveyID + "";
                    }
                    else
                    {
                        cmdText = "Update SurveyList set Year=" + objSurvey.Year + ", EventID=" + objSurvey.EventID + ",Event='" + objSurvey.EventCode + "', ChapterID=" + objSurvey.ChapterID + ",ChapterCode='" + objSurvey.ChapterCode + "',RespondentType='" + objSurvey.RespondentType + "',[Open]='" + objSurvey.Open + "',ProductGroupID=" + objSurvey.ProductGroupID + ", ProductGroupCode='" + objSurvey.ProductGroupCode + "' BeginDate='" + objSurvey.BeginDate + "', EndDate='" + objSurvey.EndDate + "',ModifiedBy=" + objSurvey.CreatedBy + ",ModifiedDate=GetDate() where SurveyID=" + objSurvey.SurveyID + "";
                    }
                    SqlCommand cmd = new SqlCommand(cmdText, cn);
                    cmd.ExecuteNonQuery();
                    retVal = 1;

                }
            }




        }
        catch (Exception ex)
        {

        }
        return retVal;

    }




    [WebMethod]
    public static List<Collector> CreateCollector(long SurveyId, string URl)
    {


        List<Collector> Collector = new List<Collector>();

        var settings = new CreateCollectorSettings
        {
            RedirectUrl = URl

        };
        Collector ObjCol = new Collector();

        using (var api = new SurveyMonkeyApi(Token))
        {
            ObjCol = api.CreateCollector(SurveyId, settings);
            Collector.Add(ObjCol);
        }


        //var settings = new CreateCollectorListSettings
        //{

        //    SurveyID = 73629800,
        //    Type = "weblink",
        //    RedirectURL = "www.northsouth.org/app9/maintest.aspx"

        //};
        //List<Collector> objColl = new List<Collector>();
        //objColl = sm.CreateCollector(settings);

        return Collector;

    }

    [WebMethod]
    public static int SaveSurveyResponse(SurveyResponse objSurvey)
    {
        string CmdText = string.Empty;
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();

        int retval = -1;

        int count = 0;
        try
        {
            CmdText = "select count(*) as CountSet from SurveyResponse where SurveyID=" + objSurvey.SurveyID + " and MemberID=" + objSurvey.MemberID + "";

            SqlCommand cmd = new SqlCommand(CmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            
            CmdText = "Insert into SurveyResponse (SurveyID, MemberID, RespondantType, RespDate, Completed,CreatedDate,CreatedBy,EventID,EventYear) values(" + objSurvey.SurveyID + "," + objSurvey.MemberID + ",'" + objSurvey.RespondantType + "',GetDate(),'" + objSurvey.Completed + "',GetDate(),'" + objSurvey.CreatedBy + "'," + objSurvey.EventID + "," + objSurvey.Year + ")";




            if (count <= 0)
            {
                cmd = new SqlCommand(CmdText, cn);
                cmd.ExecuteNonQuery();
                retval = 1;
            }
            else
            {
                retval = 1;
            }
        }
        catch (Exception ex)
        {

        }
        return retval;

    }

    [WebMethod]
    public static int ResponseCount(int MemberID, int SurveyID)
    {


        int count = 0;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select count(*) as CountSet from SurveyResponse where MemberID=" + MemberID + " and SurveyID=" + SurveyID + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
        }
        return count;

    }

    public void updateResponse()
    {
        string CmdText = string.Empty;


        CmdText = "Update SurveyResponse set Completed='Y', ModifiedBy=" + hdnUserID.Value + ", ModifiedDate=GetDate() where SurveyID=" + hdnSurveyID.Value + " and MemberID=" + hdnUserID.Value + "";

        try
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            Response.Redirect("SurveyList.aspx");
            // ListAllSurvey();
        }
        catch (Exception ex)
        {

        }

    }

    [WebMethod]
    public static List<SurveyQuestions> GetSurveyDetails(int SurveyID)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select Year,EventID,ChapterID,ProductGroupID,RespondentType,BeginDate,EndDate,[Open] from SurveyList where SurveyID=" + SurveyID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.Year = dr["Year"].ToString();
                        sur.ChapterID = dr["ChapterID"].ToString();
                        sur.EventID = dr["EventID"].ToString();
                        sur.ProductGroupID = dr["ProductGroupID"].ToString();
                        sur.BeginDate = Convert.ToDateTime(dr["BeginDate"].ToString()).ToString("MM/dd/yyyy");
                        sur.EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        sur.Open = dr["Open"].ToString();
                        sur.RespondentType = dr["RespondentType"].ToString();
                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static string validateSurveyCountUserBased(int MemberID, int SurveyID)
    {

        string retVal = "-1";

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select Completed from SurveyResponse where MemberID=" + MemberID + " and SurveyID=" + SurveyID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = ds.Tables[0].Rows[0]["Completed"].ToString();
                }
            }


        }
        catch (Exception ex)
        {
        }
        return retVal;

    }



    [WebMethod]
    public static List<SurveyQuestions> listSurveyResponseFromNSF(int SurveyID, int MemberID, string RespondentID)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select distinct SL.SurveyID,E.EventCode, SL.Title,SL.Year,IP.EMail, IP.FirstName+' '+IP.LastName as Name,PG.ProductGroupCode,IP.HPhone, IP.CPhone,SR.Completed,SR.RespondentID,SR.MemberID,SR.Accepted,(select count(distinct(MemberID)) from SurveyResponse where Accepted='Y' and SurveyID=" + SurveyID + ") as AcceptCount, (select count(distinct(MemberID)) from SurveyResponse where Accepted='D' and SurveyID=" + SurveyID + ") as DeclinedCount from SurveyList SL inner join SurveyResponse SR on(SL.SurveyID=SR.SurveyID) inner join Event E on (E.EventID=SL.EventID) inner join IndSpouse IP on(IP.AutoMemberID=SR.MemberID) left join ProductGroup PG on (PG.ProductGroupID=SL.ProductGroupID) where SR.SurveyID=" + SurveyID + "";

            //if (MemberID > 0)
            //{
            //    cmdText += " and SR.MemberID=" + MemberID + "";
            //}
            if (RespondentID != "0")
            {
                cmdText += " and SR.RespondentID=" + RespondentID + "";
            }
            cmdText += " order by SR.RespondentID ASC";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["EventCode"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.Name = dr["Name"].ToString();
                        sur.Email = dr["Email"].ToString();
                        sur.CPhone = dr["CPhone"].ToString();
                        sur.HPhone = dr["HPhone"].ToString();
                        sur.Completed = dr["Completed"].ToString();
                        sur.RespondentID = dr["RespondentID"].ToString();
                        sur.MemberID = dr["MemberID"].ToString();
                        sur.Accepted = dr["Accepted"].ToString().Trim();
                        sur.AcceptedCount = dr["AcceptCount"].ToString().Trim();
                        sur.DeclinedCount = dr["DeclinedCount"].ToString().Trim();
                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }
    [WebMethod]
    public static int updateSurveyResponse(string SurveyID, string MemberID, string RespondentID)
    {
        int retVal = -1;
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            string cmdText = string.Empty;
            cmdText = " Update SurveyResponse set Completed='Y',RespondentID='" + RespondentID + "', ModifiedBy=" + MemberID + ", ModifiedDate=GetDate() where SurveyID=" + SurveyID + " and MemberID=" + MemberID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;

        }
        catch
        {

        }
        return retVal = 1;
    }

    public void getLoginName()
    {
        string cmdText = string.Empty;
        string UserName = string.Empty;
        DataSet ds = new DataSet();
        cmdText = "select FirstName+' '+LastName as Name from Indspouse where AutoMemberID=" + hdnUserID.Value + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                UserName = ds.Tables[0].Rows[0]["Name"].ToString();
                txtName.Text = UserName;
                txtName.Enabled = false;
                hdnUserName.Value = UserName;
            }
        }
    }


    [WebMethod]
    public static int checkResponse(int SurveyID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select count(*) as CountSet from SurveyResponse where SurveyID=" + SurveyID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
        }
        return retVal;

    }
    [WebMethod]
    public static int DeleteSurvey(int SurveyID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = "Delete from SurveyList where SurveyID='" + SurveyID + "'; Delete from SurveyResponse where SurveyID=" + SurveyID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();

            retVal = 1;
        }
        catch (Exception ex)
        {
            retVal = -1;
        }


        return retVal;

    }

    [WebMethod]
    public static void GetResponseHTML(string ResponseHTML, string FileName)
    {
        responseHtml = "";
        reponseExcelFileName = "";
        responseHtml = ResponseHTML;
        reponseExcelFileName = FileName;
    }

    public void ExportToExcel()
    {
        string reposneHTml = responseHtml;
        string fileName = reponseExcelFileName;
        fileName = fileName.Replace(" ", "");

        Response.ContentType = "application/force-download";
        Response.AddHeader("content-disposition", "attachment; filename=" + fileName + ".xls");
        Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        Response.Write("<head>");
        Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-     8\">");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name>Response Data</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        StringWriter tw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);

        Response.Write(reposneHTml.ToString());

        Response.Write("</head>");
        Response.End();
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        ExportToExcel();
    }



    [WebMethod]
    public static int SendEmailsToVolunteers(int SurveyID)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select IP.AutoMemberID,IP.Email,IP.FirstName,IP.LastName, SL.EventID, SL.Title from SurveyResponse SR inner join Indspouse IP on (SR.MemberID=IP.AutoMemberID) inner join SurveyList SL  on (SR.SurveyID=SL.SurveyID) where SR.Completed='N' and SR.SurveyID=" + SurveyID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string volunteerEmail = string.Empty;
            string eventID = string.Empty;
            string toEmail = string.Empty;
            string subject = string.Empty;
            string fromEMail = string.Empty;
            string surveyTitle = string.Empty;
            string tblHtml = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        volunteerEmail += dr["Email"].ToString() + ";";
                        eventID = dr["EventID"].ToString();
                        subject = dr["Title"].ToString();
                        subject = subject.Replace("&nbsp;", " ");
                    }
                    if (eventID == "13")
                    {
                        toEmail = "nsfprogramleads@gmail.com";
                        fromEMail = "nsfprogramleads@gmail.com";
                    }
                    else
                    {
                        toEmail = "nsfcontests@gmail.com";
                        fromEMail = "nsfcontests@gmail.com";
                    }

                    tblHtml += "<table>";
                    tblHtml += "<tr>";
                    tblHtml += "<td>";
                    tblHtml += "<span>Dear Volunteer, </span> </br>";
                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<td>";

                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<td>";
                    tblHtml += "<span>You did not complete the survey.  It was not saved by Survey Monkey.  Please complete the survey again.</span> </br>";
                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<td>";

                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<td>";
                    tblHtml += "<span>If you have any questions, please send an email to <b>" + toEmail + "</b></span> </br>";
                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<tr>";
                    tblHtml += "<td>";

                    tblHtml += "</td>";
                    tblHtml += "</tr>";

                    tblHtml += "</table>";
                    //  volunteerEmail = "michael.simson@capestart.com;arul.raj@capestart.com,emim.lumina@capestart.com;";
                    volunteerEmail = volunteerEmail.TrimEnd(';');
                    try
                    {
                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress(fromEMail);
                        mail.Body = tblHtml;
                        mail.Subject = subject;

                        mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

                        String[] strEmailAddressList = null;
                        String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
                        Match EmailAddressMatch = default(Match);
                        strEmailAddressList = volunteerEmail.Replace(',', ';').Split(';');
                        foreach (object item in strEmailAddressList)
                        {
                            EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);

                            if (EmailAddressMatch.Success)
                            {

                                mail.Bcc.Add(new MailAddress(item.ToString().Trim()));

                            }
                        }

                        SmtpClient client = new SmtpClient();
                        // client.Host = host;
                        mail.IsBodyHtml = true;
                        try
                        {
                            client.Send(mail);
                            retVal = 1;
                            //lblError.Text = ex.ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    catch
                    {
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return retVal;

    }


    [WebMethod]
    public static int ExportSurveyResponse(int SurveyID)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {


            retVal = 1;



        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return 1;

    }


    public void ExportToExcelNSfResponse()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select distinct SL.SurveyID,E.EventCode, SL.Title,SL.Year,IP.EMail, IP.FirstName+' '+IP.LastName as Name,PG.ProductGroupCode,IP.HPhone, IP.CPhone,SR.Completed,SR.Accepted,SR.RespondentID,SR.MemberID from SurveyList SL inner join SurveyResponse SR on(SL.SurveyID=SR.SurveyID) inner join Event E on (E.EventID=SL.EventID) inner join IndSpouse IP on(IP.AutoMemberID=SR.MemberID) left join ProductGroup PG on (PG.ProductGroupID=SL.ProductGroupID) where SR.SurveyID=" + hdnSurveyID.Value + "";


            cmdText += " order by SR.RespondentID ASC";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Survey Repsonse";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Survey Response List from NSF Website";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "SurveyID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "SurveyTitle";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Event";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "RespondentID";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "MemberID";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Product Group Code";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "CPhone";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "HPhone";
                    oSheet.Range["K3"].Font.Bold = true;


                    oSheet.Range["L3"].Value = "Completed";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Accepted";
                    oSheet.Range["M3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["SurveyID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Title"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["RespondentID"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Completed"];

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Accepted"];




                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "SurveyRespList_" + surveyTitle + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }

    protected void btnExportToExcel_onClick(object sender, EventArgs e)
    {
        ExportToExcelNSfResponse();
    }



    [WebMethod]
    public static int BulkVolAssignment(int EventID, int MemberID, string Year)
    {

        int retVal = -1;

        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string cmdText = string.Empty;

            if (EventID == 13)
            {
                cmdText = "insert into Volunteer (MemberID, ROleId, RoleCode,TeamID, EventYear,EventID,EventCode,[National],CreateDate, CreatedBy) select MemberID, 88,'Coach',7," + Year + ",13,'Coaching','Y', GETDATE()," + MemberID + " from SurveyResponse where Accepted='Y' and MemberID not in(Select MemberID from Volunteer where RoleId=88)";



                SqlCommand cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();

                retVal = 1;
            }
        }
        catch (Exception ex)
        {
            retVal = -1;
        }


        return retVal;

    }



    [WebMethod]
    public static List<SurveyQuestions> listNoRoles(int SurveyID, int Year)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted='Y' and SR.EventYear=" + Year + " and SR.SurveyID=" + SurveyID + " and SR.memberID not in (select MemberID from Volunteer where RoleID=88)  group by SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event  order by IP.FirstName, IP.LastName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["EventCode"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.Name = dr["Name"].ToString();
                        sur.Email = dr["Email"].ToString();
                        sur.CPhone = dr["CPhone"].ToString();
                        sur.HPhone = dr["HPhone"].ToString();
                        sur.Completed = dr["Completed"].ToString();
                        sur.RespondentID = dr["RespondentID"].ToString();
                        sur.MemberID = dr["MemberID"].ToString();
                        sur.Accepted = dr["Accepted"].ToString().Trim();

                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }


    [WebMethod]
    public static List<SurveyQuestions> listNoCalSignup(int SurveyID, int Year)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR inner join Volunteer V on(SR.MemberID=V.MemberID and SR.Accepted='Y' and V.RoleID=88) inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted='Y' and V.RoleID=88 and SR.EventYear=" + Year + " and SR.SurveyID=" + SurveyID + "  and SR.MemberID not in(Select MemberID from CalSignUp where Eventyear=2016)  group by SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["EventCode"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.Name = dr["Name"].ToString();
                        sur.Email = dr["Email"].ToString();
                        sur.CPhone = dr["CPhone"].ToString();
                        sur.HPhone = dr["HPhone"].ToString();
                        sur.Completed = dr["Completed"].ToString();
                        sur.RespondentID = dr["RespondentID"].ToString();
                        sur.MemberID = dr["MemberID"].ToString();
                        sur.Accepted = dr["Accepted"].ToString().Trim();

                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static List<SurveyQuestions> listAssignedRolesAndCalSignup(int SurveyID, int Year)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR inner join Volunteer V on(SR.MemberID=V.MemberID and SR.Accepted='Y' and V.RoleID=88) inner join CalSignup CS on(CS.memberID=SR.MemberID and cs.EventYear=2016) inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted='Y' and V.RoleID=88 and SR.EventYear=" + Year + " and SR.SurveyID=" + SurveyID + "  group by SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC ";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["EventCode"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.Name = dr["Name"].ToString();
                        sur.Email = dr["Email"].ToString();
                        sur.CPhone = dr["CPhone"].ToString();
                        sur.HPhone = dr["HPhone"].ToString();
                        sur.Completed = dr["Completed"].ToString();
                        sur.RespondentID = dr["RespondentID"].ToString();
                        sur.MemberID = dr["MemberID"].ToString();
                        sur.Accepted = dr["Accepted"].ToString().Trim();

                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }


    public void ExportToExcelNoRoles()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR inner join Volunteer V on(SR.MemberID=V.MemberID and SR.Accepted='Y' and V.RoleID=88) inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted='Y' and V.RoleID=88 and SR.EventYear=" + ddlYear.SelectedValue + " and SR.SurveyID=" + hdnSurveyID.Value + "  group by SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event having count(V.MemberID)=0 order by IP.FirstName, IP.LastName ASC";




            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Volunteer Roles";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Volunteer accepted, but coach role assignment was not made";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "SurveyID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "SurveyTitle";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Event";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "RespondentID";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "MemberID";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Product Group Code";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "CPhone";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "HPhone";
                    oSheet.Range["K3"].Font.Bold = true;


                    oSheet.Range["L3"].Value = "Completed";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Accepted";
                    oSheet.Range["M3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["SurveyID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Title"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["RespondentID"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Completed"];

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Accepted"];




                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "VolunteerRoleMissing_" + surveyTitle + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }

    public void ExportToExcelNoCalSignup()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR inner join Volunteer V on(SR.MemberID=V.MemberID and SR.Accepted='Y' and V.RoleID=88) inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted='Y' and V.RoleID=88 and SR.EventYear=" + ddlYear.SelectedValue + " and SR.SurveyID=" + hdnSurveyID.Value + "  and SR.MemberID not in(Select MemberID from CalSignUp where Eventyear=2016)  group by SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Volunteer Calendar Signup";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Accepted, coach role was assigned, but calendar signup is missing";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "SurveyID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "SurveyTitle";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Event";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "RespondentID";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "MemberID";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Product Group Code";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "CPhone";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "HPhone";
                    oSheet.Range["K3"].Font.Bold = true;


                    oSheet.Range["L3"].Value = "Completed";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Accepted";
                    oSheet.Range["M3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["SurveyID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Title"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["RespondentID"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Completed"];

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Accepted"];




                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "CalendarSignupMissing_" + surveyTitle + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }

    public void ExportToExcelVolRolesAndCalSignup()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR inner join Volunteer V on(SR.MemberID=V.MemberID and SR.Accepted='Y' and V.RoleID=88) inner join CalSignup CS on(CS.memberID=SR.MemberID and cs.EventYear=2016) inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted='Y' and V.RoleID=88 and SR.EventYear=" + ddlYear.SelectedValue + " and SR.SurveyID=" + hdnSurveyID.Value + "  group by SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";




            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Volunteers";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Accepted, coach role was assigned, and calendar signup was made";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "SurveyID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "SurveyTitle";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Event";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "RespondentID";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "MemberID";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Product Group Code";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "CPhone";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "HPhone";
                    oSheet.Range["K3"].Font.Bold = true;


                    oSheet.Range["L3"].Value = "Completed";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Accepted";
                    oSheet.Range["M3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["SurveyID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Title"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["RespondentID"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Completed"];

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Accepted"];




                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "VolunteerRoleAndCalSignup" + surveyTitle + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }


    public void ExportToExcelUnAcceptedCoaches()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.SurveyReponseID, SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR  inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted is null and SR.Completed='Y' and SR.EventYear=" + ddlYear.SelectedValue + " and SR.SurveyID=" + hdnSurveyID.Value + "  group by SR.SurveyReponseID,SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";




            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Volunteers";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Completed Survey, not yet accepted, not declined";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "SurveyID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "SurveyTitle";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Event";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "RespondentID";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "MemberID";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Product Group Code";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "CPhone";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "HPhone";
                    oSheet.Range["K3"].Font.Bold = true;


                    oSheet.Range["L3"].Value = "Completed";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Accepted";
                    oSheet.Range["M3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["SurveyID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Title"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["RespondentID"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Completed"];

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Accepted"];




                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "VolunteerNotAcceptedNotDeclined" + surveyTitle + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }

    public void ExportToExcelDeclinedCoaches()
    {
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.SurveyReponseID, SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR  inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted ='D' and SR.Completed='Y' and SR.EventYear=" + ddlYear.SelectedValue + " and SR.SurveyID=" + hdnSurveyID.Value + "  group by SR.SurveyReponseID,SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";




            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string surveyTitle = string.Empty;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Volunteers";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Completed Survey, Declined";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "SurveyID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "SurveyTitle";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Event";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "RespondentID";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "MemberID";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Product Group Code";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "CPhone";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "HPhone";
                    oSheet.Range["K3"].Font.Bold = true;


                    oSheet.Range["L3"].Value = "Completed";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Accepted";
                    oSheet.Range["M3"].Font.Bold = true;


                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        surveyTitle = dr["Title"].ToString();
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["SurveyID"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Title"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["RespondentID"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MemberID"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Completed"];

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Accepted"];




                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }
                    surveyTitle = surveyTitle.Replace("&nbsp;", " ");
                    surveyTitle = Regex.Replace(surveyTitle, @"\s", "");

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "VolunteerDeclined" + surveyTitle + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                    Response.End();



                }
            }


        }
        catch (Exception ex)
        {

        }

    }


    protected void btnExportToExcelNoRoles_onClick(object sender, EventArgs e)
    {
        ExportToExcelNoRoles();
    }

    protected void btnExportToExcelNoCalSignup_onClick(object sender, EventArgs e)
    {
        ExportToExcelNoCalSignup();
    }

    protected void btnExportToExcelCalSignupRoles_onClick(object sender, EventArgs e)
    {
        ExportToExcelVolRolesAndCalSignup();
    }
    protected void btnExportUnAccepCoaches_onClick(object sender, EventArgs e)
    {
        ExportToExcelUnAcceptedCoaches();
    }
    protected void btnExportDeclinedCoaches_onClick(object sender, EventArgs e)
    {
        ExportToExcelDeclinedCoaches();
    }

    [WebMethod]
    public static List<SurveyQuestions> listunAcceptedCoaches(int SurveyID, int Year)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.SurveyReponseID, SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR  inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted is null and SR.Completed='Y' and SR.EventYear=" + Year + " and SR.SurveyID=" + SurveyID + "  group by SR.SurveyReponseID,SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["EventCode"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.Name = dr["Name"].ToString();
                        sur.Email = dr["Email"].ToString();
                        sur.CPhone = dr["CPhone"].ToString();
                        sur.HPhone = dr["HPhone"].ToString();
                        sur.Completed = dr["Completed"].ToString();
                        sur.RespondentID = dr["RespondentID"].ToString();
                        sur.MemberID = dr["MemberID"].ToString();
                        sur.Accepted = dr["Accepted"].ToString().Trim();
                        sur.SurveyResponseID = dr["SurveyReponseID"].ToString().Trim();

                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }




    [WebMethod]
    public static int UpdateDecission(int SurveyResponseID, string Decide, int UserID)
    {

        int retVal = -1;

        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string cmdText = string.Empty;

            if (Decide != "N" || Decide != "C")
            {
                cmdText = "Update SurveyResponse set Accepted='" + Decide + "', ModifiedDate=GetDate(), ModifiedBy=" + UserID + " where SurveyReponseID=" + SurveyResponseID + "";



                SqlCommand cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();

                retVal = 1;
            }
        }
        catch (Exception ex)
        {
            retVal = -1;
        }


        return retVal;

    }


    [WebMethod]
    public static List<SurveyQuestions> listDeclinedCoaches(int SurveyID, int Year)
    {

        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select SR.SurveyReponseID, SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName+' '+IP.LastName as Name,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event as EventCode from SurveyResponse SR  inner join Indspouse IP on (Ip.AutoMemberID=SR.memberID) inner join SurveyList SL on (SL.SurveyID=SR.SurveyID) left join ProductGroup P on (P.ProductGroupID=SL.ProductGroupID) where SR.Accepted ='D' and SR.Completed='Y' and SR.EventYear=" + Year + " and SR.SurveyID=" + SurveyID + "  group by SR.SurveyReponseID,SR.MemberID,SR.SurveyID,SL.ProductGroupID,SL.ProductGroupCode,SL.Title,SL.EventID, SL.Year,SR.RespondentID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone,SR.Completed,SR.Accepted,SL.Event order by IP.FirstName, IP.LastName ASC";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.SurveyID = dr["SurveyID"].ToString();
                        sur.SurveyTitle = dr["Title"].ToString();
                        sur.EventCode = dr["EventCode"].ToString();
                        sur.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        sur.Name = dr["Name"].ToString();
                        sur.Email = dr["Email"].ToString();
                        sur.CPhone = dr["CPhone"].ToString();
                        sur.HPhone = dr["HPhone"].ToString();
                        sur.Completed = dr["Completed"].ToString();
                        sur.RespondentID = dr["RespondentID"].ToString();
                        sur.MemberID = dr["MemberID"].ToString();
                        sur.Accepted = dr["Accepted"].ToString().Trim();
                        sur.SurveyResponseID = dr["SurveyReponseID"].ToString().Trim();

                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    //[WebMethod]
    //public static List<Survey> ListSurveyDetails(long SurveyID)
    //{
    //    List<Survey> surveys = new List<Survey>();
    //    Survey sur = new Survey();
    //    using (var api = new SurveyMonkeyApi(Token))
    //    {
    //        sur = api.GetSurveyDetails(SurveyID);
    //        surveys.Add(sur);
    //    }


    //    return surveys;

    //}

    [WebMethod]
    public static List<Response> GetResponseFromSM(long SurveyID, long RespondantID)
    {

        List<Response> ResponseList = new List<Response>();
        using (var api = new SurveyMonkeyApi(Token))
        {
            ResponseList = api.GetSurveyResponseDetailsList(SurveyID);
            // ResponseList = api.GetSurveyResponseOverviewList(SurveyID);
        }
        return ResponseList;
        //  return Respondanse;

    }

}