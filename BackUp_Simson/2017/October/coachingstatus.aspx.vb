﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports RKLib.ExportData
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections

Imports System.Net

Imports System.Xml
Partial Class coachingstatus
    Inherits System.Web.UI.Page
    Dim Year As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("entryToken") = "Parent"
        'Session("LoggedIn") = "true"
        'Session("CustIndID") = 67479

        '**Debug 
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If Now.Month <= 5 Then
                Session("Year") = Now.Year - 1
            Else
                Session("Year") = Now.Year
            End If
            If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
                hlinkParentRegistration.Visible = True
                hlinkStudent.Visible = False
            ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                hlinkParentRegistration.Visible = False
                hlinkStudent.Visible = True

            End If
            Try

                LoadCochingStatus()

                'LoadSelectedCoaching()
                'LoadMakeUpSessions()
                'LoadSubstituteSessions()
                'If (hdMemberId.Value = "51336") Then
                lblCoachName.Visible = True
                Span2.Visible = True

                'Else
                ' Span2.Visible = False
                'lblCoachName.Visible = False
                'End If

            Catch ex As Exception
                CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Coaching Status")
            End Try

        End If
    End Sub

    Private Sub LoadCochingStatus()
        Dim SQLStr As String = String.Empty
        Dim year As Integer
        Dim cmdYrText As String = String.Empty

        cmdYrText = "select max(EventYear) from EventFees where EventID=13"

        'If (Session("StudentId") > 0) Then
        '    cmdYrText &= " and ChildNumber=" & Session("StudentId") & ""
        'Else
        '    cmdYrText &= " and PMemberid=" & Session("CustIndID") & ""
        'End If


        year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdYrText)

        ' Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' Else Case when C.ProductGroupCode in ('SC') then '2' Else Case when C.ProductGroupCode in ('GB') then '2' End End END End End
        SQLStr = "select CDC.StartDate,CDC.EndDate,C.Time,C.Day, CR.CoachRegID,CR.RegisteredID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+' EST' as Time,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, C.UserID, C.Pwd as WebExPwd, Ch.OnlineClassEmail "
        SQLStr = SQLStr & ",convert(varchar,isnull(EF.Duration,''))	+ ' hr(s)' as Duration, P.ProductGroupID,P.ProductID,CR.Semester,CR.SessionNo,CMemberID, C.MeetingKey, C.MeetingPwd,CR.RegisteredID, CR.Semester "
        SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join EventFees EF on EF.EventId=13 and EF.EventYear=CR.EventYear and EF.ProductId=CR.ProductId Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Semester = C.Semester and CR.SessionNo = C.SessionNo left join CoachingDateCal CDC on(CDC.EventYear=CR.EventYear and CDC.ProductGroupId=CR.ProductGroupID and CDC.ProductId=CR.ProductID and CDC.EventId=CR.EventID and CDC.ScheduleType='term' and CDC.Semester=CR.Semester) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & year & " and C.Accepted='Y'"
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))

            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            dgCoachingSTatus.DataSource = drCoaching
            dgCoachingSTatus.DataBind()
            If dgCoachingSTatus.Items.Count > 0 Then
                dgCoachingSTatus.Visible = True
                For i As Integer = 0 To dgCoachingSTatus.Items.Count - 1
                    Dim Status As String = String.Empty
                    Status = CType(dgCoachingSTatus.Items(i).FindControl("lblStatus"), Label).Text
                    If (Status = "Pending") Then
                        CType(dgCoachingSTatus.Items(i).FindControl("btnSelect"), Button).Enabled = False
                    Else
                        CType(dgCoachingSTatus.Items(i).FindControl("btnSelect"), Button).Enabled = True
                    End If
                Next

                'Dim startDate As String = String.Empty
                'Dim strDay As String = String.Empty
                'Dim strToday As String = DateTime.Now.ToShortDateString()
                'Dim dtTodayDate As DateTime = Convert.ToDateTime(strToday.ToString())
                'For i As Integer = 0 To dgCoachingSTatus.Items.Count - 1
                '    startDate = CType(dgCoachingSTatus.Items(i).FindControl("lblstartDate"), Label).Text
                '    strDay = CType(dgCoachingSTatus.Items(i).FindControl("lblCoachDay"), Label).Text
                '    Dim dtDateVal As DateTime = New DateTime()
                '    dtDateVal = Convert.ToDateTime(startDate.ToString())
                '    If dtTodayDate.ToShortDateString() >= dtDateVal Then
                '        Dim strMeetDAy As String = String.Empty
                '        strMeetDAy = dtDateVal.DayOfWeek().ToString()
                '        If strMeetDAy = strDay Then
                '            ' CType(dgCoachingSTatus.Items(i).FindControl("lblstartDate"), Label).Text = dtTodayDate.ToShortDateString()
                '        Else
                '            For j As Integer = 0 To 7
                '                dtTodayDate = DateTime.Now
                '                If dtTodayDate.AddDays(j).DayOfWeek().ToString() = strDay Then

                '                    '  CType(dgCoachingSTatus.Items(i).FindControl("lblstartDate"), Label).Text = dtTodayDate.AddDays(j).ToShortDateString()
                '                End If
                '            Next
                '        End If

                '    Else
                '        Dim dtMeetDate As String = dtDateVal.ToString()
                '        Dim dtCurrentDate As DateTime = dtTodayDate.ToShortDateString()
                '        Dim k As Integer = 1
                '        While dtDateVal > dtCurrentDate.ToShortDateString()
                '            dtCurrentDate = dtTodayDate.AddDays(k).ToShortDateString()
                '            k = k + 1
                '        End While
                '        If dtDateVal >= dtCurrentDate.ToShortDateString() Then
                '            Dim strMeetDAy As String = String.Empty
                '            strMeetDAy = dtDateVal.DayOfWeek().ToString()
                '            If strMeetDAy = strDay Then
                '                '  CType(dgCoachingSTatus.Items(i).FindControl("lblstartDate"), Label).Text = dtCurrentDate.ToShortDateString()
                '            Else
                '                For j As Integer = 0 To 7
                '                    dtTodayDate = DateTime.Now
                '                    If dtCurrentDate.AddDays(j).DayOfWeek().ToString() = strDay Then

                '                        '   CType(dgCoachingSTatus.Items(i).FindControl("lblstartDate"), Label).Text = dtCurrentDate.AddDays(j).ToShortDateString()
                '                    End If
                '                Next
                '            End If
                '        End If

                '    End If
                'Next

            Else
                lblErr.Text = "No coaching Registration"
                dgCoachingSTatus.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            'lblErr.Text = SQLStr + "" + ex.Message
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Private Sub LoadSelectedCoaching()
        Dim SQLStr As String = String.Empty
        Dim cmdYrText As String = String.Empty

        cmdYrText = "select max(EventYear) from EventFees where EventID=13"

        'If (Session("StudentId") > 0) Then
        '    cmdYrText &= " and ChildNumber=" & Session("StudentId") & ""
        'Else
        '    cmdYrText &= " and PMemberid=" & Session("CustIndID") & ""
        'End If


        Year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdYrText)

        SQLStr = "select CDC.StartDate,CDC.EndDate,C.Time,C.Day, CR.CoachRegID,CR.RegisteredID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+' EST' as Time,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, C.UserID, C.Pwd as WebExPwd, Ch.OnlineClassEmail "
        SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else case when c.ProductGroupCode in ('COMP') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1'  Else Case when C.ProductGroupCode in ('SC') then '1.5' Else Case when C.ProductGroupCode in ('GB') then '1.5' End End End END End End	+ ' hr(s)' as Duration, P.ProductGroupID,P.ProductID,CR.Semester,CR.SessionNo,CMemberID, C.MeetingKey, C.MeetingPwd,CR.RegisteredID, CR.Semester "
        SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "

        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Semester = C.Semester and CR.SessionNo = C.SessionNo left join CoachingDateCal CDC on(CDC.EventYear=CR.EventYear and CDC.ProductGroupId=CR.ProductGroupID and CDC.ProductId=CR.ProductID and CDC.EventId=CR.EventID and CDC.ScheduleType='term' and CDC.Semester=CR.Semester) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & Year & " and C.Accepted='Y' and CR.Approved='Y' and CR.CoachRegID=" & hdnCoachRegID.Value & ""

        Try

            Dim conn As New SqlConnection(Application("ConnectionString"))

            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            dgselected.DataSource = drCoaching
            dgselected.DataBind()
            If dgselected.Items.Count > 0 Then
                dgselected.Visible = True
            Else
                lblErr.Text = "No coaching Registration"
                dgselected.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
            If ds.Tables(0).Rows.Count > 0 Then




                lblCoachName.Text = "Coach Name: " & ds.Tables(0).Rows(0)("CoachName").ToString()
                lblChildName.Text = "Child Name: " & ds.Tables(0).Rows(0)("ChildName").ToString()
                lblPrdName.Text = "Product: " & ds.Tables(0).Rows(0)("ProductName").ToString()
                lblLevelName.Text = "Level: " & ds.Tables(0).Rows(0)("Level").ToString()

                hdnProductGroupID.Value = ds.Tables(0).Rows(0)("ProductGroupID").ToString()
                hdnProductID.Value = ds.Tables(0).Rows(0)("ProductID").ToString()
                hdnSemester.Value = ds.Tables(0).Rows(0)("Semester").ToString()
                hdnSessionNo.Value = ds.Tables(0).Rows(0)("SessionNo").ToString()
                hdMemberId.Value = ds.Tables(0).Rows(0)("CMemberID").ToString()
                hdnLevel.Value = ds.Tables(0).Rows(0)("Level").ToString()
                hdTable1Year.Value = Year

                populateRecClassDate(hdnLevel.Value, hdnSessionNo.Value, hdMemberId.Value, hdnProductGroupID.Value, hdnProductID.Value, hdTable1Year.Value)
                BindCoachClassDetailsInGrid(rpt_grdCoachClassCal, 0)

            Else
                lblCoachName.Text = ""
                lblChildName.Text = ""
                lblPrdName.Text = ""
                lblLevelName.Text = ""
                lblClassCalendar.Text = "No record Exists"
            End If
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            ' lblErr.Text = SQLStr + "" + ex.Message
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub
    Dim hasNotApprFlag As Boolean

    Protected Sub dgselected_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgselected.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ' Retrieve the text of the Approved status from the DataGridItem
            Dim ApprovedStatus As String = CType(e.Item.FindControl("lblApproved"), Label).Text
            If ApprovedStatus.ToLower.Trim <> "y" Then
                hasNotApprFlag = True
            End If
        End If

    End Sub


    Private Sub LoadMakeUpSessions()
        Try
            Dim year As Integer
            Dim cmdYrText As String = String.Empty

            cmdYrText = "select max(EventYear) from EventFees where EventID=13"

            'If (Session("StudentId") > 0) Then
            '    cmdYrText &= " and ChildNumber=" & Session("StudentId") & ""
            'Else
            '    cmdYrText &= " and PMemberid=" & Session("CustIndID") & ""
            'End If


            year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdYrText)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            SQLStr = "select WC.BeginTime, WC.Day, CR.CoachRegID,CR.MakeupRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,WC.Day, CONVERT(varchar(15),CAST(WC.BeginTIME AS TIME),100)+' EST' as Time,WC.StartDate,WC.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, WC.UserID, WC.Pwd as WebExPwd, Ch.OnlineClassEmail, C.MakeUpmeetKey, CR.Semester "
            SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	+ ' hr(s)' as Duration, WC.SessionKey, WC.MeetingPwd, CR.MakeUpRegID "
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Semester = C.Semester  inner join WebConfLog WC on (C.MemberID=WC.MemberID and C.MakeupURL=wc.MeetingURL) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & year & " and C.Accepted='Y' and CR.CoachRegID=" & hdnCoachRegID.Value & ""
            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            DgMakeupSession.DataSource = drCoaching
            DgMakeupSession.DataBind()
            If DgMakeupSession.Items.Count > 0 Then
                DgMakeupSession.Visible = True
                SpnMakeupTitle.Visible = False
                Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                Dim dateVal As String = String.Empty
                Dim i As Integer = 0

                For i = 0 To DgMakeupSession.Items.Count - 1
                    dateVal = CType(DgMakeupSession.Items(i).FindControl("lblstartDate"), Label).Text
                    Dim dtDateVal As DateTime = New DateTime()
                    dtDateVal = Convert.ToDateTime(dateVal.ToString())
                    If (dtDateVal < dtTodayDate) Then
                        CType(DgMakeupSession.Items(i).FindControl("btnMakeupJoinMeeting"), Button).Enabled = False
                    End If
                Next
            Else
                SpnMakeupTitle.Visible = True
                DgMakeupSession.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Private Sub LoadSubstituteSessions()
        Try
            Dim cmdYrText As String = String.Empty

            cmdYrText = "select max(EventYear) from EventFees where EventID=13"

            'If (Session("StudentId") > 0) Then
            '    cmdYrText &= " and ChildNumber=" & Session("StudentId") & ""
            'Else
            '    cmdYrText &= " and PMemberid=" & Session("CustIndID") & ""
            'End If


            Year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdYrText)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            SQLStr = "select C.SubstituteDate, C.Time,C.Day, CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,IP.FirstName + ' ' + IP.LastName as SubCoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,WC.Day, CONVERT(varchar(15),CAST(WC.BeginTIME AS TIME),100)+' EST' as Time,WC.StartDate,WC.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, C.UserID, C.Pwd as WebExPwd, Ch.OnlineClassEmail, CR.Semester "
            SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	+ ' hr(s)' as Duration, CR.SubstituteURL,C.SubstituteMeetKey"
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Semester = C.Semester  inner join WebConfLog WC on (C.SubstituteCoachID=WC.SubstituteCoachID and C.MemberID=WC.MemberID) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID Inner Join IndSpouse IP ON C.SubstituteCoachID=IP.AutoMemberID  where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & Year & " and C.Accepted='Y' and CR.CoachRegID=" & hdnCoachRegID.Value & " and C.SubstituteDate is not null and WC.SessionType='Recurring Meeting'"
            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            DataGrdSubstituteSessions.DataSource = drCoaching
            DataGrdSubstituteSessions.DataBind()
            If DataGrdSubstituteSessions.Items.Count > 0 Then
                DataGrdSubstituteSessions.Visible = True
                SpnSubstituteTitle.Visible = False
                Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                Dim dateVal As String = String.Empty
                Dim i As Integer = 0

                For i = 0 To DataGrdSubstituteSessions.Items.Count - 1
                    dateVal = CType(DataGrdSubstituteSessions.Items(i).FindControl("lblstartDate"), Label).Text
                    Dim dtDateVal As DateTime = New DateTime()
                    dtDateVal = Convert.ToDateTime(dateVal.ToString())
                    If (dtDateVal < dtTodayDate) Then
                        CType(DataGrdSubstituteSessions.Items(i).FindControl("btnMakeupJoinMeeting"), Button).Enabled = False
                    End If
                Next
            Else
                SpnSubstituteTitle.Visible = True
                DataGrdSubstituteSessions.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgselected.ItemCommand
        Dim cmdName As String = e.CommandName
        Try


            If e.CommandName = "SelectMeetingURL" Then
                hdnIsCheck.Value = "Yes"
                Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
                hdnChildMeetingURL.Value = MeetingURL
                Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

                Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
                Dim ChildName As String = CType(e.Item.FindControl("lblChildName"), Label).Text
                HdnChildName.Value = ChildName
                Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
                Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
                Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
                Dim Coachname As String = CType(e.Item.FindControl("lblCoach"), Label).Text

                Dim RegID As String = String.Empty
                RegID = CType(e.Item.FindControl("lblRegisteredID"), Label).Text
                Dim ds As DataSet = New DataSet()
                Dim CmdText As String = String.Empty

                HdnWebExID.Value = WebExID
                HdnWebExPwd.Value = WebExPwd
                hdnSessionKey.Value = SessionKey
                hdnOnlineClassEmail.Value = OnlineClassEmail
                hdnAttendeeRegisteredID.Value = RegID

                CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
                '  ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
                'If ds.Tables.Count > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                'If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then

                Dim meetingLink As String = CType(e.Item.FindControl("HlAttendeeMeetURL"), LinkButton).Text
                hdnZoomURL.Value = meetingLink
                Dim beginTime As String = CType(e.Item.FindControl("lblBegTime"), Label).Text
                Dim day As String = CType(e.Item.FindControl("lblMeetDay"), Label).Text
                hdnStartTime.Value = beginTime

                hdnDay.Value = day

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()

                'Hidden for temporary testing.. should be enabled for production

                'updated by bindhu on Oct05_2016 to allow parents to join meeting before 30 minutes of meeting
                If mins <= 30 AndAlso day = today Then
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Success", "", "Coaching Status")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                Else
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Failure", "", "Coaching Status")
                    ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)

                End If
                ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)


                ' Else
                ' lblErr.Text = "Only authorized child can join into the training session."
                ' End If

                'End If
                ' End If

            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName:" + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub



    Public Function GetTrainingSessions(WebExID As String, Pwd As String, SessionKey As String) As String
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.training.GetTrainingSession"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & SessionKey & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = ProcessTrainingStatusTestResponse(xmlReply)
        'lblMsg3.Text = result;
        Return result

    End Function
    Private Function ProcessTrainingStatusTestResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Dim MeetingStatus As String = String.Empty
        Dim startTime As String = String.Empty
        Dim Day As String = String.Empty
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)

            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession")
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText
            Dim ExpirationDate As String = String.Empty
            If status = "SUCCESS" Then
                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText
                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText
                Try
                    Day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText
                    Dim Attendees As String = String.Empty
                    Attendees = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:attendees/sess:participants/sess:participant/sess:person/com:name", manager).InnerText
                Catch ex As Exception
train:              ExpirationDate = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:expirationDate", manager).InnerText
                    Dim dtExpDate As DateTime = Convert.ToDateTime(ExpirationDate).ToString("MM/dd/yyyy")
                    Day = dtExpDate.ToString("dddd")
                End Try

                startTime = startTime.Substring(10, 9)

                hdnSessionStartTime.Value = startTime
                Dim Mins As Double = 0.0
                Dim dFrom As DateTime = DateTime.Now
                Dim dTo As DateTime = DateTime.Now
                Dim sDateFrom As String = startTime

                Dim today As String = DateTime.Today.DayOfWeek.ToString()
                If today.ToLower() = Day.ToLower() Then
                    Dim sDateTo As String = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                    'TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    'DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    Dim easternTime As DateTime = DateTime.Now
                    Dim strEasternTime As String = easternTime.ToShortDateString()
                    strEasternTime = Convert.ToString(strEasternTime & Convert.ToString(" ")) & sDateFrom
                    Dim dtEasternTime As DateTime = Convert.ToDateTime(strEasternTime)
                    Dim easternTimeNow As DateTime = DateTime.Now.AddHours(1)

                    Dim TS As TimeSpan = dtEasternTime - easternTimeNow


                    Mins = TS.TotalMinutes
                Else
                    For i As Integer = 1 To 7
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString()
                        If today.ToLower() = Day.ToLower() Then
                            dTo = DateTime.UtcNow.AddDays(i)
                            'TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            'DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            Dim easternTime As DateTime = DateTime.Now.AddDays(i)
                            Dim sDateTo As String = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                            Dim targetDateTime As String = Convert.ToString(easternTime.ToShortDateString() + " ") & sDateFrom
                            'DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            Dim easternTimeNow As DateTime = DateTime.Now.AddHours(1)

                            Dim dtTargetTime As DateTime = Convert.ToDateTime(targetDateTime)
                            Dim TS As TimeSpan = dtTargetTime - easternTimeNow


                            Mins = TS.TotalMinutes
                        End If
                    Next
                End If
                Dim DueMins As String = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."))
                hdnSessionStartTime.Value = startTime
                hdnStartMins.Value = Convert.ToString(DueMins)
            End If
            If MeetingStatus = "INPROGRESS" Then


                Dim Attendees As String = String.Empty
                Dim isExist As Integer = 0
                Try
                    For i As Integer = 1 To 100

                        Attendees = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:attendees/sess:participants/sess:participant[" & i & "]/sess:person/com:name", manager).InnerText
                        If Attendees = HdnChildName.Value Then
                            isExist = 1
                        Else

                        End If

                    Next
                Catch ex As Exception

                End Try
                If hdnIsCheck.Value = "Yes" Then


                    If isExist <> 1 Then
                        RegisterMeetingAttendee(hdnSessionKey.Value, HdnWebExID.Value, HdnWebExPwd.Value, "", HdnChildName.Value, "", hdnOnlineClassEmail.Value, "US")

                    Else

                    End If
                End If
            End If
        Catch e As Exception
            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", e.Message, "Coaching Status")
        End Try

        Return MeetingStatus
    End Function

    Protected Sub DgMakeupSession_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        Try

            If e.CommandName = "SelectMeetingURL" Then
                hdnIsCheck.Value = "No"
                Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
                hdnChildMeetingURL.Value = MeetingURL
                Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

                Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
                Dim ChildName As String = CType(e.Item.FindControl("lblChildName"), Label).Text
                HdnChildName.Value = ChildName
                Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
                Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
                Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
                Dim Coachname As String = CType(e.Item.FindControl("lblCoach"), Label).Text
                Dim RegID As String = String.Empty
                RegID = CType(e.Item.FindControl("lblRegisteredID"), Label).Text
                Dim ds As DataSet = New DataSet()
                Dim CmdText As String = String.Empty

                HdnWebExID.Value = WebExID
                HdnWebExPwd.Value = WebExPwd
                hdnSessionKey.Value = SessionKey
                hdnOnlineClassEmail.Value = OnlineClassEmail
                hdnAttendeeRegisteredID.Value = RegID


                CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
                ' ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
                'If ds.Tables.Count > 0 Then
                '    If ds.Tables(0).Rows.Count > 0 Then
                '        If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then
                Dim meetingLink As String = CType(e.Item.FindControl("HlAttendeeMeetURL"), LinkButton).Text
                hdnZoomURL.Value = meetingLink
                Dim beginTime As String = CType(e.Item.FindControl("lblBegTime"), Label).Text
                Dim day As String = CType(e.Item.FindControl("lblMeetDay"), Label).Text
                hdnStartTime.Value = beginTime

                hdnDay.Value = day

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Success", "", "Coaching Status")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                Else
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Failure", "", "Coaching Status")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If

                'Else
                '    lblErr.Text = "Only authorized child can join ito the training session."
                'End If

                '        End If
                '    End If

            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub
    Protected Sub DataGrdSubstituteSessions_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        Try


            If e.CommandName = "SelectMeetingURL" Then
                hdnIsCheck.Value = "No"
                Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
                hdnChildMeetingURL.Value = MeetingURL
                Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

                Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
                Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
                Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
                hdnSessionKey.Value = SessionKey
                Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
                Dim Coachname As String = CType(e.Item.FindControl("lblExistCoachName"), Label).Text
                Dim ds As DataSet = New DataSet()
                Dim CmdText As String = String.Empty
                CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
                ' ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
                'If ds.Tables.Count > 0 Then
                '    If ds.Tables(0).Rows.Count > 0 Then
                '        If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then
                Dim meetingLink As String = CType(e.Item.FindControl("HlAttendeeMeetURL"), LinkButton).Text
                hdnZoomURL.Value = meetingLink
                Dim beginTime As String = CType(e.Item.FindControl("lblBegTime"), Label).Text
                Dim day As String = CType(e.Item.FindControl("lblMeetDay"), Label).Text
                hdnStartTime.Value = beginTime

                hdnDay.Value = day

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Success", "", "Coaching Status")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                Else
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Failure", "", "Coaching Status")

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If

                'Else
                '    lblErr.Text = "Only authorized child can join ito the training session."
                'End If

                '        End If
                '    End If

            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Public Sub RegisterMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, City As String, Email As String, Country As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.RegisterMeetingAttendee"">" & vbCr & vbLf

        strXML += "<attendees>" & vbCr & vbLf
        strXML += "<person>" & vbCr & vbLf
        strXML += "<name>" & Name & "</name>" & vbCr & vbLf
        strXML += "<title>title</title>" & vbCr & vbLf
        strXML += "<company>microsoft</company>" & vbCr & vbLf
        strXML += "<address>" & vbCr & vbLf
        strXML += "<addressType>PERSONAL</addressType>" & vbCr & vbLf
        'strXML += "<city>" & City & "</city>" & vbCr & vbLf
        strXML += "<country>US</country>" & vbCr & vbLf
        strXML += "</address>" & vbCr & vbLf

        strXML += "<email>" & Email & "</email>" & vbCr & vbLf
        strXML += "<notes>notes</notes>" & vbCr & vbLf
        strXML += "<url>https://</url>" & vbCr & vbLf
        strXML += "<type>VISITOR</type>" & vbCr & vbLf
        strXML += "</person>" & vbCr & vbLf
        strXML += "<joinStatus>ACCEPT</joinStatus>" & vbCr & vbLf
        strXML += "<role>ATTENDEE</role>" & vbCr & vbLf

        strXML += "<sessionKey>" & sessionKey & "</sessionKey>" & vbCr & vbLf
        strXML += "</attendees>" & vbCr & vbLf


        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = RegisterMeetingAttendeeResponse(xmlReply)
    End Sub
    Private Function RegisterMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim RegID As String
                RegID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText
                Dim attendeeID As String

                attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText

                hdnMeetingAttendeeID.Value = attendeeID
                hdnAttendeeRegisteredID.Value = RegID
                GetJoinMeetingURL(hdnSessionKey.Value, HdnWebExID.Value, HdnWebExPwd.Value, "", HdnChildName.Value, hdnOnlineClassEmail.Value, "training")
            ElseIf status = "FAILURE" Then


            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
    Public Sub GetJoinMeetingURL(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, Email As String, MeetingPassword As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf

        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID.Trim() & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd.Trim() & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GetjoinurlMeeting"">" & vbCr & vbLf

        strXML += "<sessionKey>" + sessionKey.Trim() + "</sessionKey>" & vbCr & vbLf
        strXML += "<attendeeName>" + Name.Trim() + "</attendeeName>" & vbCr & vbLf
        strXML += "<attendeeEmail>" + Email.Trim() + "</attendeeEmail>" & vbCr & vbLf
        strXML += "<meetingPW>" + MeetingPassword.Trim() + "</meetingPW>" & vbCr & vbLf
        strXML += "<RegID>" + hdnAttendeeRegisteredID.Value.Trim() + "</RegID>" & vbCr & vbLf

        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingAttendeeURLResponse(xmlReply)
    End Sub
    Private Function MeetingAttendeeURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnChildMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
    Protected Sub rpt_grdCoachClassCal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lbl As Label = CType(e.Row.FindControl("lblSerial"), Label)
                lbl.Text = Convert.ToInt16(e.Row.RowIndex) + 1
                Dim lblWeek As Label = CType(e.Row.FindControl("lblWeekNo"), Label)
                Dim lblStartDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblStartDate"), Label)

                If lblWeek.Text = "0" Then
                    lblWeek.Text = ""
                End If

                Dim lblSemester As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblSemester"), Label)
                Dim lblSessionNo As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblSessionNo"), Label)
                Dim rep_hdWeekCnt As HiddenField = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_hdWeekCnt"), HiddenField)

                Dim lblDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblDate"), Label)
                Dim lblCoachClassCalID As Label = DirectCast(e.Row.Cells(1).FindControl("lblCoachClassCalID"), Label)

                Dim lblHWRelDate As Label = DirectCast(e.Row.Cells(10).FindControl("lblHWRelDate"), Label)
                Dim lblHWDueDate As Label = DirectCast(e.Row.Cells(11).FindControl("lblHWDueDate"), Label)
                Dim lblARelDate As Label = DirectCast(e.Row.Cells(12).FindControl("lblARelDate"), Label)
                Dim lblSRelDate As Label = DirectCast(e.Row.Cells(13).FindControl("lblSRelDate"), Label)
                Dim lblDay As Label = DirectCast(e.Row.Cells(2).FindControl("lblDay"), Label)
                Dim lblStatus As Label = DirectCast(e.Row.Cells(7).FindControl("lblStatus"), Label)
                Dim lblProdGroupID As Label = DirectCast(e.Row.Cells(7).FindControl("lblProductGroupID"), Label)
                Dim lblProdID As Label = DirectCast(e.Row.Cells(7).FindControl("lblProductID"), Label)
                Dim lblScheduleType As Label = DirectCast(e.Row.Cells(10).FindControl("lblScheduleType"), Label)


                Dim lblRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblRelDt"), Label)
                If lblRelDT.Text.Trim = "" Then
                    If lblStatus.Text <> "Cancelled" Then

                        If lblStatus.Text = "On" And lblRelDT.Text = "" Then
                            e.Row.Cells(10).Attributes("style") = "border-right-style:hidden;"
                            e.Row.Cells(11).Attributes("style") = "border-right-style:hidden;"
                            e.Row.Cells(12).Attributes("style") = "border-right-style:hidden;"

                            lblARelDate.Text = "Release dates were not yet set"
                        Else



                            lblHWRelDate.Text = lblDate.Text
                            lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                            lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                            If lblProdGroupID.Text = "31" Then
                                lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(1)).Date.ToString("MM/dd/yyyy")
                            ElseIf lblProdGroupID.Text = "33" Then
                                lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(1)).Date.ToString("MM/dd/yyyy")
                            ElseIf lblProdGroupID.Text = "41" Then
                                lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(1)).Date.ToString("MM/dd/yyyy")
                            Else
                                lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(1)).Date.ToString("MM/dd/yyyy")
                            End If
                        End If
                    End If
                Else

                    Dim lblDueDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblDueDt"), Label)
                    Dim lblARelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblARelDt"), Label)
                    Dim lblSRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblSRelDt"), Label)
                    lblHWRelDate.Text = lblRelDT.Text
                    lblHWDueDate.Text = DateTime.ParseExact(lblDueDT.Text, "MM/dd/yyyy", Nothing)
                    lblARelDate.Text = DateTime.ParseExact(lblARelDT.Text, "MM/dd/yyyy", Nothing)
                    lblSRelDate.Text = DateTime.ParseExact(lblSRelDT.Text, "MM/dd/yyyy", Nothing)

                End If

                If lblStatus.Text = "Cancelled" Then

                    Dim ArrSerNo As ArrayList = New ArrayList()
                    Dim i As Integer = 0
                    Dim strSerial As String = String.Empty
                    Dim finalSerial As String = String.Empty
                    Dim strSerNo As StringBuilder = New StringBuilder()
                    For i = 1 To Convert.ToInt32(lbl.Text) - 1
                        ArrSerNo.Add(i)
                        strSerNo.Append(i)
                        strSerial += i & ","
                    Next
                    finalSerial = strSerial.TrimEnd(",")
                    Dim weekSeqText As String = String.Empty


                    Dim lblWeekSeq As Label

                    Try


                        lblWeekSeq = CType(rpt_grdCoachClassCal.Rows(Convert.ToInt16(e.Row.RowIndex) - 1).FindControl("lblWeek"), Label)
                        weekSeqText = lblWeekSeq.Text
                    Catch ex As Exception
                        weekSeqText = "0"
                        finalSerial = "0"
                    End Try

                    Dim cmdText As String = String.Empty
                    cmdText = "select count(*) as CountSet from CoachClassCal where  EventYear=" & hdTable1Year.Value & " and MemberID=" & hdMemberId.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and SessionNo=" & hdnSessionNo.Value & " and SerNo in(" & finalSerial & ") and (Status='On' or Status='Makeup' or Status='Substitute')"


                    Dim ds As DataSet = New DataSet()
                    ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If ds.Tables(0).Rows.Count > 0 Then

                        If weekSeqText <> "" And weekSeqText <> "0" Then
                            lblWeek.Text = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString()) + 1
                        Else
                            lblWeek.Text = ""
                        End If


                    End If
                End If

            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Private Sub BindCoachClassDetailsInGrid(grCtrl As GridView, iShowAll As Integer)
        Try


            Dim params() As SqlParameter = {New SqlParameter("@MemberId", SqlDbType.Int), New SqlParameter("@EventYear", SqlDbType.Int), New SqlParameter("@ProductGroupId", SqlDbType.Int), New SqlParameter("@ProductId", SqlDbType.Int), New SqlParameter("@Semester", SqlDbType.Int), New SqlParameter("@Level", SqlDbType.VarChar, 50), New SqlParameter("@SessionNo", SqlDbType.Int), New SqlParameter("@Iterator", SqlDbType.Int), New SqlParameter("@ShowAll", SqlDbType.Int)}

            Dim totalClassValue As Integer = 0
            If hdnTotalClass.Value = "" Then
                totalClassValue = 0

            Else
                totalClassValue = Convert.ToInt32(hdnTotalClass.Value)

            End If

            params(0).Value = hdMemberId.Value
            params(1).Value = hdTable1Year.Value
            params(2).Value = hdnProductGroupID.Value
            params(3).Value = hdnProductID.Value
            params(4).Value = hdnSemester.Value
            params(5).Value = hdnLevel.Value
            params(6).Value = hdnSessionNo.Value
            params(7).Value = totalClassValue
            params(8).Value = 0
            'productgroupID, productID, Semester, level, week#, DocType=Q, 
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.StoredProcedure, "usp_GetCoachDetailsByWeek", params)
            If ds.Tables(0).Rows.Count = 0 Then
                'rpItem.Visible = False
                dvColorStatus.Visible = False
            Else
                grCtrl.DataSource = ds.Tables(0)
                grCtrl.DataBind()
                changeGridColor(grCtrl)
                dvColorStatus.Visible = True
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub


    Public Sub populateRecClassDate(ByVal level As String, ByVal sessionNo As String, ByVal coach As String, ByVal pgGroupID As String, ByVal pgID As String, ByVal year As String)

        Dim cmdText As String = String.Empty
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS left join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term' and CS.Semester=CD.Semester) where  CS.MemberID=" + coach + " and CS.ProductGroupID=" + pgGroupID + " and CS.ProductID=" + pgID + " and CS.EventYear=" + year + " and CS.Accepted='Y' and CS.Level='" + level + "' and CS.SessionNo=" + sessionNo + ""

        Dim ds As New DataSet()
        Try
            Dim x As Integer = 0
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dtStartDate As New DateTime()
                    Dim dtEndDate As New DateTime()
                    Dim dtTodayDate As DateTime = DateTime.Now


                    dtStartDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString())
                    Dim strStartDate As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToString("MM/dd/yyyy")
                    Dim EndDate As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("EndDate").ToString()).ToString("MM/dd/yyyy")
                    dtEndDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("EndDate").ToString())
                    Dim day As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToString("dddd")
                    Dim classDay As String = ds.Tables(0).Rows(0)("day").ToString()
                    Dim dtClassDate As DateTime = Convert.ToDateTime(dtStartDate)


                    Dim y As Integer = 0
                    If day = classDay Then

                        While dtClassDate <= dtEndDate
                            Dim [date] As String = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy")

                            x = x + 1

                            dtClassDate = dtClassDate.AddDays(7)
                        End While
                    Else
                        For i As Integer = 1 To 7
                            dtClassDate = dtClassDate.AddDays(1)
                            Dim strDay As String = Convert.ToDateTime(dtClassDate).ToString("dddd")
                            If strDay = classDay Then
                                Exit For
                            End If
                        Next
                        While dtClassDate <= dtEndDate

                            x = x + 1
                            'DDlSubDate.Items.Insert(0, new ListItem(date, date));

                            dtClassDate = dtClassDate.AddDays(7)
                        End While
                    End If

                End If
            End If
            hdnTotalClass.Value = x
        Catch ex As Exception

            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")

        End Try


    End Sub

    Protected Sub changeGridColor(ByVal grdCoachClass As GridView)
        Try


            Dim dateVal As String = String.Empty
            Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
            Dim status As String = String.Empty
            Dim prevStatus As String = String.Empty
            Dim prevDate As String = String.Empty
            Dim isInc As Integer = 0
            For i As Integer = 0 To grdCoachClass.Rows.Count - 1
                dateVal = DirectCast(grdCoachClass.Rows(i).FindControl("lblDate"), Label).Text
                status = DirectCast(grdCoachClass.Rows(i).FindControl("lblStatus"), Label).Text
                If i <> 0 Then
                    prevStatus = DirectCast(grdCoachClass.Rows(i - 1).FindControl("lblStatus"), Label).Text

                Else
                    prevStatus = DirectCast(grdCoachClass.Rows(i).FindControl("lblStatus"), Label).Text
                End If

                ' dateVal = Convert.ToDateTime(dateVal.ToString()).ToString("MM/dd/yyyy")
                Dim dtDateVal As DateTime = New DateTime()
                dtDateVal = Convert.ToDateTime(dateVal.ToString())
                If dtTodayDate > dtDateVal And (status = "On" Or status = "Makeup" Or status = "Substitute") Then
                    grdCoachClass.Rows(i).Style.Add("background-color", "#58d68d")

                ElseIf dtDateVal = dtTodayDate And (status = "On" Or status = "Makeup" Or status = "Substitute" Or status = "" Or status = "Not set") Then
                    grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")

                    'ElseIf dtDateVal <> dtTodayDate And dtTodayDate.AddDays(7) > dtDateVal And status <> "Cancelled" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled" And prevStatus <> "" Then
                    '    grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")
                    isInc = isInc + 1
                ElseIf dtDateVal <> dtTodayDate And dtDateVal > dtTodayDate And status <> "Cancelled" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled" And prevStatus <> "" Then
                    If isInc < 1 Then

                        grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")
                    End If
                    isInc = isInc + 1
                ElseIf dtDateVal <> dtTodayDate And dtDateVal > dtTodayDate And status <> "Notset" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled" And (prevStatus = "" Or status = "") Then
                    If isInc < 1 Then

                        grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")
                    End If
                    isInc = isInc + 1

                ElseIf status = "Cancelled" Or status = "Makeup Cancelled" Or status = "Substitute Cancelled" Then
                    grdCoachClass.Rows(i).Style.Add("background-color", "#FF0000")

                ElseIf status = "Not set" Or status = "" Then
                    grdCoachClass.Rows(i).Style.Add("background-color", "")
                End If
                If status = "Cancelled" Or status = "Makeup Cancelled" Or status = "Substitute Cancelled" Then
                    grdCoachClass.Rows(i).Style.Add("background-color", "#FF0000")

                End If


            Next
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Protected Sub dgCoachingSTatus_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCoachingSTatus.ItemCommand
        Dim cmdName As String = e.CommandName
        If e.CommandName = "SelectChild" Then

            Dim coachRegID As String = CType(e.Item.FindControl("lblCoachRegID"), Label).Text

            hdnCoachRegID.Value = coachRegID
            LoadSelectedCoaching()
            LoadMakeUpSessions()
            LoadSubstituteSessions()
            LoadExtraSessions()
            dvShowClassSchedule.Visible = True
            dvReCurringSessions.Visible = True
            dvMakeupSessions.Visible = True
            dvSubstituteSessions.Visible = True
            dvExtraSessions.Visible = True
        End If
    End Sub

    Protected Sub btnShowClassSchedule_Click(sender As Object, e As EventArgs)
        If dvCoachDateCalendar.Visible = False Then
            dvCoachDateCalendar.Visible = True
            btnShowClassSchedule.Text = "Hide class calendar"
            ' LoadSelectedCoaching()
        Else
            dvCoachDateCalendar.Visible = False
            btnShowClassSchedule.Text = "Show class calendar"
        End If
    End Sub
    Private Sub LoadExtraSessions()
        Try
            Dim year As Integer
            Dim cmdYrText As String = String.Empty

            cmdYrText = "select max(EventYear) from CoachReg where EventID=13 and Approved='Y'"

            If (Session("StudentId") > 0) Then
                cmdYrText &= " and ChildNumber=" & Session("StudentId") & ""
            Else
                cmdYrText &= " and PMemberid=" & Session("CustIndID") & ""
            End If


            year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdYrText)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            SQLStr = "select WC.BeginTime, WC.Day, CR.CoachRegID,CR.MakeupRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.ExtraSessionURL,WC.Day, CONVERT(varchar(15),CAST(WC.BeginTIME AS TIME),100)+' EST' as Time,WC.StartDate,WC.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.ExtraSessionKey, WC.UserID, WC.Pwd as WebExPwd, Ch.OnlineClassEmail, CR.Semester "
            SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	+ ' hr(s)' as Duration, WC.SessionKey, WC.MeetingPwd, CR.MakeUpRegID "
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Semester = C.Semester  inner join WebConfLog WC on (C.MemberID=WC.MemberID and C.ExtraSessionKey=wc.SessionKey) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & year & " and C.Accepted='Y' and CR.CoachRegID=" & hdnCoachRegID.Value & ""
            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False

            DGExtraSession.DataSource = drCoaching
            DGExtraSession.DataBind()
            If DGExtraSession.Items.Count > 0 Then
                DGExtraSession.Visible = True
                lblExtraSession.Text = ""
                spnExtraSession.Visible = True
                Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                Dim dateVal As String = String.Empty
                Dim i As Integer = 0

                For i = 0 To DGExtraSession.Items.Count - 1
                    dateVal = CType(DGExtraSession.Items(i).FindControl("lblstartDate"), Label).Text
                    Dim dtDateVal As DateTime = New DateTime()
                    dtDateVal = Convert.ToDateTime(dateVal.ToString())
                    If (dtDateVal < dtTodayDate) Then
                        CType(DGExtraSession.Items(i).FindControl("btnMakeupJoinMeeting"), Button).Enabled = False
                    End If
                Next
            Else
                spnExtraSession.Visible = True
                lblExtraSession.Text = "No record exists"
                DGExtraSession.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub

    Protected Sub DGExtraSession_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGExtraSession.ItemCommand
        Dim cmdName As String = e.CommandName
        Try


            If e.CommandName = "SelectMeetingURL" Then
                hdnIsCheck.Value = "Yes"
                Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
                hdnChildMeetingURL.Value = MeetingURL
                Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

                Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
                Dim ChildName As String = CType(e.Item.FindControl("lblChildName"), Label).Text
                HdnChildName.Value = ChildName
                Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
                Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
                Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
                Dim Coachname As String = CType(e.Item.FindControl("lblCoach"), Label).Text


                Dim ds As DataSet = New DataSet()
                Dim CmdText As String = String.Empty

                HdnWebExID.Value = WebExID
                HdnWebExPwd.Value = WebExPwd
                hdnSessionKey.Value = SessionKey
                hdnOnlineClassEmail.Value = OnlineClassEmail

                CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
                '  ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
                'If ds.Tables.Count > 0 Then
                'If ds.Tables(0).Rows.Count > 0 Then
                'If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then

                Dim meetingLink As String = CType(e.Item.FindControl("HlAttendeeMeetURL"), LinkButton).Text
                hdnZoomURL.Value = meetingLink
                Dim beginTime As String = CType(e.Item.FindControl("lblBegTime"), Label).Text
                Dim day As String = CType(e.Item.FindControl("lblMeetDay"), Label).Text
                hdnStartTime.Value = beginTime

                hdnDay.Value = day

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()

                'Hidden for temporary testing.. should be enabled for production

                'updated by bindhu on Oct05_2016 to allow parents to join meeting before 30 minutes of meeting
                If mins <= 30 AndAlso day = today Then
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Success", "", "Coaching Status")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                Else
                    CoachingExceptionLog.createExceptionLog("ChildName: " + HdnChildName.Value + "", "Failure", "", "Coaching Status")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)

                End If
                ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)


                ' Else
                ' lblErr.Text = "Only authorized child can join into the training session."
                ' End If

                'End If
                ' End If

            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("ChildName:" + HdnChildName.Value + "", "Exception", ex.Message, "Coaching Status")
        End Try
    End Sub
End Class

