﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="coachingstatus.aspx.vb" Inherits="coachingstatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>
        <script language="javascript" type="text/javascript">


            function JoinMeeting() {

                var sessionkey = document.getElementById("<%=hdnSessionKey.ClientID%>").value;
                var url = "https://northsouth.zoom.us/j/" + sessionkey
                //var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

                window.open(url, '_blank');

            }

            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }
            function showAlert() {
                alert("Meeting attendees can only join their class up to 30 minutes before class time");
            }

            function startChildMeeting() {
                var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
                $("#ancClick").target = "_blank";
                $("#ancClick").attr("href", url);
                $("#ancClick").attr("target", "_blank");
                document.getElementById("ancClick").click();

            }
            function joinChildMeeting() {
                startChildMeeting();
            }
            function showAlertTest(prdCode, coachName) {
                var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
                var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
                var dueMins = parseInt(startMins);
                var msg = "";
                if (dueMins > 0) {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                else if (dueMins >= -15) {
                    msg = "The coach has not started the class.";
                } else if (dueMins < -15) {
                    msg = "The coach has either not started or may have cancelled the class.";
                } else {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
                alert(msg);
            }

            function PopupPicker(ctl) {
                var PopupWindow = null;
                settings = 'width=600,height=300,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=yes,resizable=no,dependent=no';
                PopupWindow = window.open('CoachingStatusHelp.html', 'Coaching Help', settings);
                PopupWindow.focus();
            }

        </script>
    </div>
    <a id="ancClick" target="_blank" style="display: none;" href=""></a>
    <br />

    <div align="left">
        <asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink>
    </div>
    <div align="left">
        <asp:HyperLink ID="hlinkStudent" runat="server" NavigateUrl="~/StudentFunctions.aspx">Back to Student Functions Page</asp:HyperLink>

        <br />
        <div align="center">
            <h2>Coaching Status</h2>
        </div>
        <div style="margin-bottom: 10px;"></div>
        <div id="dvCoachingRegStatus" runat="server" visible="true">
            <div align="center">
                <span id="spnCoachingStatus" runat="server" style="font-weight: bold; color: #64a81c;">Table 1: Coaching Registration Status</span>
            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <asp:DataGrid ID="dgCoachingSTatus" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
                BorderColor="#336666" ForeColor="White" Font-Bold="True">
                <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                    <asp:TemplateColumn HeaderText="Action" Visible="true" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>


                            <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="SelectChild" />
                            <div style="display: none;">
                                <asp:Label ID="lblCoachRegID" runat="server" Text='<%# Eval("CoachRegID").ToString()%>'></asp:Label>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                    <%--<ASP:BOUNDCOLUMN DataField="Status" Visible="false"></ASP:BOUNDCOLUMN>--%>
                    <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Semester" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Semester")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coach Email" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachEmail")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%----%>
                    <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Session#" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionNo")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Session Key" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingKey")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="RegisteredID" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RegisteredID")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Session Pwd" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingPwd")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                <AlternatingItemStyle BackColor="LightBlue" />
            </asp:DataGrid>
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>

        <div id="dvReCurringSessions" runat="server" visible="false">
            <div style="clear: both; margin-bottom: 5px;"></div>

            <div style="float: left;">
                <div style="color: red;"><b>Note: If you are facing difficulty to join meeting then please click on the help link</b></div>
            </div>

            <div style="clear: both; margin-bottom: 5px;"></div>
            <div align="center" style="font-weight: bold; color: #64A81C;">
                <span id="Span3" runat="server" visible="true">Table 2 : Recurring Session</span>


            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <asp:DataGrid ID="dgselected" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
                BorderColor="#336666" ForeColor="White" Font-Bold="True">
                <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                    <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                    <%--<ASP:BOUNDCOLUMN DataField="Status" Visible="false"></ASP:BOUNDCOLUMN>--%>
                    <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Semester" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Semester")%></ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coach Email" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachEmail")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%" Visible="false">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%----%>
                    <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Session#" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionNo")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Meeting URL" Visible="true" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Visible="false" Text='<%# Eval("AttendeeJoinURL").ToString() + ""%>' CommandName="SelectMeetingURL1" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>

                            <asp:Button ID="btnJoin" runat="server" Text="Join Meeting" CommandName="SelectMeetingURL" />
                            <div style="float: right;">
                                <a href="javascript:PopupPicker();">Help</a>
                            </div>
                            <div style="display: none;">
                                <asp:Label ID="LblMeetingURL" runat="server" Text='<%# Eval("AttendeeJoinURL").ToString() %>'></asp:Label>
                                <asp:Label ID="LblOnlineClassEmail" runat="server" Text='<%# Eval("OnlineClassEmail").ToString() %>'></asp:Label>
                                <asp:Label ID="lblWebExID" runat="server" Text='<%# Eval("UserID").ToString() %>'></asp:Label>
                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%# Eval("WebExPwd").ToString() %>'></asp:Label>
                                <asp:Label ID="lblSessionKey" runat="server" Text='<%# Eval("MeetingKey").ToString() %>'></asp:Label>
                                <asp:Label ID="lblPrdCode" runat="server" Text='<%# Eval("ProductCode").ToString() %>'></asp:Label>
                                <asp:Label ID="lblCoach" runat="server" Text='<%# Eval("CoachName").ToString() %>'></asp:Label>
                                <asp:Label ID="lblRegisteredID" runat="server" Text='<%# Eval("RegisteredID").ToString() %>'></asp:Label>
                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'><%# Eval("Time").ToString().Substring(0,Math.Min(5,Eval("Time").ToString().Length)) %></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Session Key" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingKey")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="RegisteredID" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RegisteredID")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Session Pwd" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingPwd")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                <AlternatingItemStyle BackColor="LightBlue" />
            </asp:DataGrid>
            <asp:Label runat="server" ForeColor="Green" Text="Note**: Unless paid, seat is not guaranteed." Font-Size="Medium" Font-Bold="true" ID="lblNote"></asp:Label>
            <center>
                <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
            </center>
        </div>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvMakeupSessions" runat="server" visible="false">
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="spnTable1Title" runat="server" visible="true">Table 3 : Makeup Sessions</span>


        </div>
        <div align="center" style="color: #64A81C;">
            <span id="SpnMakeupTitle" runat="server" visible="false" style="color: red;">No record exists</span>


        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <asp:DataGrid ID="DgMakeupSession" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
                BorderColor="#336666" ForeColor="White" Font-Bold="True" OnItemCommand="DgMakeupSession_ItemCommand">
                <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                    <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                    <%--<ASP:BOUNDCOLUMN DataField="Status" Visible="false"></ASP:BOUNDCOLUMN>--%>
                    <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coach Email" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachEmail")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Makeup Date" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%----%>
                    <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "BeginTime")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Session#" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionNo")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Meeting URL" Visible="true" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" Visible="false" ID="HlAttendeeMeetURL" Text='<%# Eval("MakeUpURL").ToString() + ""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MakeUpURL").ToString()%>'></asp:LinkButton>

                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                            <asp:Button ID="btnMakeupJoinMeeting" runat="server" Text="Join Meeting" CommandName="SelectMeetingURL" />
                            <div style="float: right;">
                                <a href="javascript:PopupPicker();">Help</a>
                            </div>
                            <div style="display: none;">
                                <asp:Label ID="LblMeetingURL" runat="server" Text='<%# Eval("MakeUpURL").ToString() %>'></asp:Label>
                                <asp:Label ID="LblOnlineClassEmail" runat="server" Text='<%# Eval("OnlineClassEmail").ToString() %>'></asp:Label>
                                <asp:Label ID="lblWebExID" runat="server" Text='<%# Eval("UserID").ToString() %>'></asp:Label>
                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%# Eval("WebExPwd").ToString() %>'></asp:Label>
                                <asp:Label ID="lblSessionKey" runat="server" Text='<%# Eval("MakeUpMeetKey").ToString() %>'></asp:Label>
                                <asp:Label ID="lblPrdCode" runat="server" Text='<%# Eval("ProductCode").ToString() %>'></asp:Label>
                                <asp:Label ID="lblCoach" runat="server" Text='<%# Eval("CoachName").ToString() %>'></asp:Label>
                                <asp:Label ID="lblRegisteredID" runat="server" Text='<%# Eval("MakeupRegID").ToString() %>'></asp:Label>
                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "BeginTime")%>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Session Key" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionKey")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Registered ID" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMakeupRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MakeUpRegID")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Session Pwd" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblMeetingPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingPwd")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                <AlternatingItemStyle BackColor="LightBlue" />
            </asp:DataGrid>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvSubstituteSessions" runat="server" visible="false">
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="Span1" runat="server" visible="true">Table 4 : Substitute Sessions</span>


        </div>
        <div align="center" style="color: #64A81C;">
            <span id="SpnSubstituteTitle" runat="server" visible="false" style="color: red;">No record exists</span>


        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <asp:DataGrid ID="DataGrdSubstituteSessions" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
                BorderColor="#336666" ForeColor="White" Font-Bold="True" OnItemCommand="DataGrdSubstituteSessions_ItemCommand">
                <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                    <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                    <%--<ASP:BOUNDCOLUMN DataField="Status" Visible="false"></ASP:BOUNDCOLUMN>--%>
                    <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblExistCoachName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Substitute Coach Name" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblSubCoachName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SubCoachName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="Coach Email" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachEmail")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Substitute Date" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SubstituteDate", "{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <%----%>
                    <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Session#" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionNo")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>


                    <asp:TemplateColumn HeaderText="Meeting URL" Visible="true" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" Visible="false" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>

                            <asp:Button ID="btnMakeupJoinMeeting" runat="server" Text="Join Meeting" CommandName="SelectMeetingURL" />
                            <div style="float: right;">
                                <a href="javascript:PopupPicker();">Help</a>
                            </div>
                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                            <div style="display: none;">
                                <asp:Label ID="LblMeetingURL" runat="server" Text='<%# Eval("AttendeeJoinURL").ToString() %>'></asp:Label>
                                <asp:Label ID="LblOnlineClassEmail" runat="server" Text='<%# Eval("OnlineClassEmail").ToString() %>'></asp:Label>
                                <asp:Label ID="lblWebExID" runat="server" Text='<%# Eval("UserID").ToString() %>'></asp:Label>
                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%# Eval("WebExPwd").ToString() %>'></asp:Label>
                                <asp:Label ID="lblSessionKey" runat="server" Text='<%# Eval("SubstituteMeetKey").ToString() %>'></asp:Label>
                                <asp:Label ID="lblPrdCode" runat="server" Text='<%# Eval("ProductCode").ToString() %>'></asp:Label>
                                <asp:Label ID="lblCoachname" runat="server" Text='<%# Eval("CoachName").ToString() %>'></asp:Label>

                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'><%# Eval("Time").ToString().Substring(0,Math.Min(5,Eval("Time").ToString().Length)) %></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>

                            </div>
                        </ItemTemplate>
                    </asp:TemplateColumn>



                </Columns>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                <AlternatingItemStyle BackColor="LightBlue" />
            </asp:DataGrid>
        </div>
    </div>

      <div id="dvExtraSessions" runat="server" visible="false">
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="spnExtraSession" runat="server" visible="true">Table 5 : Extra Sessions</span>


        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <asp:DataGrid ID="DGExtraSession" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
            Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
            BorderColor="#336666" ForeColor="White" Font-Bold="True">
            <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
            <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
            <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
            <Columns>
                <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                <%--<ASP:BOUNDCOLUMN DataField="Status" Visible="false"></ASP:BOUNDCOLUMN>--%>
                <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Semester" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "Semester")%></ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblCoachName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Coach Email" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblCoachEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachEmail")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Status" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%" Visible="false">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <%----%>
                <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Session#" ItemStyle-HorizontalAlign="Center">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionNo")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Meeting URL" Visible="true" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Visible="false" Text='<%# Eval("ExtraSessionURL").ToString() + ""%>' CommandName="SelectMeetingURL1" ToolTip='<%# Eval("ExtraSessionURL").ToString()%>'></asp:LinkButton>

                        <asp:Button ID="btnJoin" runat="server" Text="Join Meeting" CommandName="SelectMeetingURL" />
                        <div style="float: right;">
                            <a href="javascript:PopupPicker();">Help</a>
                        </div>
                        <div style="display: none;">
                            <asp:Label ID="LblMeetingURL" runat="server" Text='<%# Eval("ExtraSessionURL").ToString()%>'></asp:Label>
                            <asp:Label ID="LblOnlineClassEmail" runat="server" Text='<%# Eval("OnlineClassEmail").ToString() %>'></asp:Label>
                            <asp:Label ID="lblWebExID" runat="server" Text='<%# Eval("UserID").ToString() %>'></asp:Label>
                            <asp:Label ID="lblWebExPwd" runat="server" Text='<%# Eval("WebExPwd").ToString() %>'></asp:Label>
                            <asp:Label ID="lblSessionKey" runat="server" Text='<%# Eval("SessionKey").ToString() %>'></asp:Label>
                            <asp:Label ID="lblPrdCode" runat="server" Text='<%# Eval("ProductCode").ToString() %>'></asp:Label>
                            <asp:Label ID="lblCoach" runat="server" Text='<%# Eval("CoachName").ToString() %>'></asp:Label>
                          
                            <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'><%# Eval("Time").ToString().Substring(0,Math.Min(5,Eval("Time").ToString().Length)) %></asp:Label>

                            <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:TemplateColumn HeaderText="Session Key" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblMeetingkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionKey")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>

               

                <asp:TemplateColumn HeaderText="Session Pwd" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblMeetingPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingPwd")%>' Visible="True"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>

            </Columns>
            <HeaderStyle HorizontalAlign="Left" />
            <ItemStyle HorizontalAlign="Left" />
            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
            <AlternatingItemStyle BackColor="LightBlue" />
        </asp:DataGrid>

        <center>
            <asp:Label ID="lblExtraSession" runat="server" ForeColor="Red"></asp:Label>
        </center>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div runat="server" id="dvShowClassSchedule" visible="false">
        <asp:Button ID="btnShowClassSchedule" Text="Show Class Calendar" runat="server" OnClick="btnShowClassSchedule_Click" />
    </div>

  

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvCoachDateCalendar" runat="server" visible="false">
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="Span2" runat="server" visible="true">Table 5: Class Calendar</span>


        </div>

        <div style="clear: both; margin-bottom: 5px;"></div>
        <center>
            <asp:Label ID="lblClassCalendar" runat="server" ForeColor="Red"></asp:Label>
        </center>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center">

            <div style="float: left;">
                <asp:Label ID="lblCoachName" runat="server" Font-Bold="true" Text=''></asp:Label>
            </div>
            <div style="float: left; margin-left: 20px;">
                <asp:Label ID="lblChildName" runat="server" Font-Bold="true" Text=''></asp:Label>
            </div>
            <div style="float: left; margin-left: 20px;">
                <asp:Label ID="lblPrdName" runat="server" Font-Bold="true" Text=''></asp:Label>
            </div>
            <div style="float: left; margin-left: 20px;">
                <asp:Label ID="lblLevelName" runat="server" Font-Bold="true" Text=''></asp:Label>
            </div>
            <asp:GridView ID="rpt_grdCoachClassCal" Width="100%" runat="server" DataKeyNames="SignUpID" AutoGenerateColumns="False" EnableViewState="true" HeaderStyle-BackColor="#000080"
                HeaderStyle-Height="25px" AlternatingRowStyle-BackColor="#F6F6F6" OnRowDataBound="rpt_grdCoachClassCal_RowDataBound">
                <Columns>

                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CurDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                            <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblCoachClassCalID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachClassCalID")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblSignUpId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SignUpID")%>' Visible="false"></asp:Label>

                            <asp:Label ID="lblRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qreleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblDueDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qdeadlinedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblARelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "areleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblSRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sreleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Day">
                        <ItemTemplate>
                            <asp:Label ID="lblDaySub" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day").ToString().Substring(0, 3)%>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time">
                        <ItemTemplate>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' DataFormatString="{0:hh:mm}"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Duration">
                        <ItemTemplate>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ser#">
                        <ItemTemplate>
                            <asp:Label ID="lblSerial" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SerNo")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Week#">
                        <ItemTemplate>
                            <asp:Label ID="lblWeekNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "WeekNo")%>'></asp:Label>
                            <%--    <asp:Label ID="lblWeek" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "WeekNo")%>'></asp:Label>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:Label>
                            <asp:Label ID="lblProductGroupID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem, "ProductGroupID")%>'></asp:Label>
                            <asp:Label ID="lblProductID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem, "ProductID")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Substitute">
                        <ItemTemplate>
                            <asp:Label ID="lblSubstitute" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SubstituteName")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reason">
                        <ItemTemplate>
                            <asp:Label ID="lblReason" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Reason")%>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Schedule Type">
                        <ItemTemplate>
                            <asp:Label ID="lblScheduleType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ScheduleType")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Homework">
                        <ItemTemplate>
                            <asp:Label ID="lblHWRelDate" runat="server" EnableViewState="true" Text="" dataformatstring="{0:MM/dd/yyyy}"></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="HW Due Date">
                        <ItemTemplate>
                            <asp:Label ID="lblHWDueDate" runat="server" EnableViewState="true" Text=""></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Answer">
                        <ItemTemplate>
                            <asp:Label ID="lblARelDate" runat="server" EnableViewState="true" Text=""></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SRelease">
                        <ItemTemplate>
                            <asp:Label ID="lblSRelDate" runat="server" EnableViewState="true" Text=""></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>


                    <%-- <asp:TemplateField HeaderText="UserID" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "UserID")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pwd" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PWD")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>
            </asp:GridView>
        </div>


        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvColorStatus" runat="server" visible="false">
            <div id="dvGreen" style="float: left;">
                <div style="width: 30px; height: 20px; background-color: #58d68d; float: left;"></div>
                <div style="float: left; font-weight: bold; margin-left: 10px;">Past</div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="dvRed" style="float: left;">
                <div style="width: 30px; height: 20px; background-color: #FF0000; float: left;"></div>
                <div style="float: left; font-weight: bold; margin-left: 10px;">Regular Cancelled/ Makeup Cancelled/ Substitute Cancelled</div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <%-- <div id="dvCurrent" style="float: left;">
                        <div style="width: 30px; height: 20px; background-color: #f4d03f; float: left;"></div>
                        <div style="float: left; font-weight: bold; margin-left: 10px;">Current Class with status On/Makeup/Substitute</div>
                    </div>--%>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="dvFuture" style="float: left;">
                <div style="width: 30px; height: 20px; background-color: #05a9e7; float: left;"></div>
                <div style="float: left; font-weight: bold; margin-left: 10px;">Upcoming</div>
            </div>
        </div>
    </div>

    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
    <input type="hidden" id="HdnChildName" value="0" runat="server" />
    <input type="hidden" id="hdnAttendeeRegisteredID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
    <input type="hidden" id="HdnWebExID" value="0" runat="server" />
    <input type="hidden" id="HdnWebExPwd" value="0" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdnOnlineClassEmail" value="0" runat="server" />
    <input type="hidden" id="hdnIsCheck" value="No" runat="server" />

    <asp:HiddenField ID="hdMemberId" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdSelectedRowInx" runat="server" />
    <asp:HiddenField ID="hdTable1Year" runat="server"></asp:HiddenField>

    <asp:HiddenField ID="hdnProductGroupID" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnProductID" runat="server" />
    <asp:HiddenField ID="hdnLevel" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnSemester" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnSessionNo" runat="server"></asp:HiddenField>

    <input type="hidden" id="hdnZoomURL" value="0" runat="server" />

    <input type="hidden" id="hdnStartDate" value="0" runat="server" />
    <input type="hidden" id="hdnStartTime" value="" runat="server" />
    <input type="hidden" id="hdnDay" value="" runat="server" />
    <input type="hidden" id="hdnTotalClass" value="" runat="server" />
    <input type="hidden" id="hdnCoachRegID" value="" runat="server" />
    <input type="hidden" id="hdnParentName" value="" runat="server" />

</asp:Content>

