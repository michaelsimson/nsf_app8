﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Collections;
using VRegistration;

public partial class ChildTestAnswers : System.Web.UI.Page
{
    protected const string C_Ascending = " ASC";
    protected const string C_Descending = " DESC";
    protected const string VS_SortDirection = "sortDirection";
    protected const string VS_SortExpression = "sortExpression";
    protected const string VS_FilterExpression = "filterExpression";

    #region Events

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = "63362";
        //Session["entryToken"] = "parent";
        //Session["StudentId"] = 54103;
        //Session["CustIndID"] = 63362;
        //Session["LoggedIn"]="true";

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            LoadYear();
            loadPhase(ddlSemester);

            if (Session["entryToken"].ToString().ToUpper() == "PARENT")
            {
                trVol.Visible = false;
                trStudent.Visible = false;
                trParent.Visible = true;

                Loadchild(0);
            }
            else if (Session["entryToken"].ToString().ToUpper() == "STUDENT")
            {
                trVol.Visible = false;
                trStudent.Visible = true;
                trParent.Visible = false;

                Loadchild(Convert.ToInt32(Session["StudentId"]));
            }
            else if (Session["EntryToken"].ToString().ToUpper() != "VOLUNTEER")
            {
                Response.Redirect("../login.aspx?entry=v");
            }
            else if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "30") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "89"))
            {

                trVol.Visible = true;
                trParent.Visible = false;
                trStudent.Visible = false;
                Loadchild(0);
                //BindddlStudentID();

                //trChild.Visible = false;
                if (Session["RoleId"].ToString() == "88" || Session["RoleId"].ToString() == "89")
                {
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count (*) from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + "  and ProductId is not Null")) > 1)
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionString"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                        int i;
                        String prd = String.Empty;
                        String Prdgrp = String.Empty;
                        for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (prd.Length == 0)
                                prd = ds.Tables[0].Rows[i][1].ToString();
                            else
                                prd = prd + "," + ds.Tables[0].Rows[i][1].ToString();


                            if (Prdgrp.Length == 0)
                                Prdgrp = ds.Tables[0].Rows[i][0].ToString();
                            else
                                Prdgrp = Prdgrp + "," + ds.Tables[0].Rows[i][0].ToString();

                        }
                        lblPrd.Text = prd;
                        lblPrdGrp.Text = Prdgrp;
                        // LoadProductGroup(false);
                    }
                    else
                    {
                        //only one
                        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                        String prd = String.Empty;
                        String Prdgrp = String.Empty;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            prd = ds.Tables[0].Rows[0][1].ToString();
                            Prdgrp = ds.Tables[0].Rows[0][0].ToString();
                            lblPrd.Text = prd;
                            lblPrdGrp.Text = Prdgrp;
                        }
                        // LoadProductGroup(false);
                    }
                }
                else
                {
                    // LoadProductGroup(false);
                }
            }
            else
            {
                Response.Redirect("../maintest.aspx");
            }

        }
    }
    private void LoadYear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlEventYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 8; i++)
        {

            ddlEventYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));

            // ddlEventYear.Items.Insert(i + 1, Convert.ToString(year - (i)));
        }

        ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        //if (DateTime.Now.Month <= 5) //If todate is less than May 31st, then default year = current year - 1 else default year = current year
        //{
        //    ddlEventYear.Items[2].Selected = true;
        //}
        //else
        //{
        //    ddlEventYear.Items[1].Selected = true;
        //}


        //BindTestNumbers();
    }
    private void Loadchild(int ChildNumber)
    {
        lblError.Text = "";
        int ParentID = Convert.ToInt32(Session["CustIndID"]);


        //  String strSql = "select Distinct C.First_name+' '+C.Last_name as ChildName, C.ChildNumber,C.Grade From Child C Inner Join CoachReg CR on CR.PMemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber where C.memberid=" + ParentID;

        String strSql = "select Distinct C.First_name+' '+C.Last_name as ChildName, C.ChildNumber,C.Grade From Child C inner join CoachReg CR on (C.childNumber=CR.ChildNumber and CR.EventYear=" + ddlEventYear.SelectedValue + " and CR.Semester='" + ddlSemester.SelectedValue + "' and CR.Approved='Y')";
        if (Session["entryToken"].ToString().ToUpper() == "PARENT")
        {
            strSql = strSql + " where C.memberid=" + ParentID + "";
        }

        if (ChildNumber > 0)
        {
            strSql = strSql + " and C.ChildNumber=" + ChildNumber;
        }
        // strSql = strSql + " and CR.Approved='Y' and CR.EventYear =" + ddlEventYear.SelectedValue;//Year(GetDATE())";
        DataSet dsChild;
        dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);
        ddlChild.DataSource = dsChild;
        ddlChild.DataBind();
        ddlChild.Enabled = true;

        if (ddlChild.Items.Count < 1)
        {
            ddlChild.SelectedValue = "-1";
            lblError.Text = "No Child Registration Found";
            Btn_Submit.Enabled = false;
        }
        else if (ddlChild.Items.Count > 1)
        {
            ddlChild.Items.Insert(0, new ListItem("Select", "0"));
            ddlChild.Items[0].Selected = true;
            ddlChild.Enabled = true;
        }
        else
        {
            ddlChild.Enabled = false;
            ddlChild.Items[0].Selected = true;
            LoadCoachDetails();
        }
    }


    private void LoadProductGroup(Boolean User)
    {
        // ddlProduct.Items.Clear();
        try
        {
            String strSql = "";

            //Volunteer
            if (User == false)
            {
                //Hide by Michael

                //strSql = "SELECT  Distinct ProductGroupID, Name from ProductGroup WHERE EventId=" + ddlEvent.SelectedValue;
                //if (lblPrdGrp.Text.Length > 0)
                //    strSql = strSql + " AND ProductGroupID IN (" + lblPrdGrp.Text + ")";
                //strSql = strSql + " order by ProductGroupID";

                strSql = "SELECT distinct P.[Name], P.ProductGroupID from dbo.[ProductGroup] P INNER JOIN dbo.[CoachReg] CR  ON P.ProductGroupid =CR.ProductGroupid ";
                strSql = strSql + " Where  CR.Approved='Y' and CR.EventYear=" + ddlEventYear.SelectedValue + " and CR.ChildNumber=" + ddlChild.SelectedValue + " and P.EventId =" + ddlEvent.SelectedValue + " and CR.Semester='" + ddlSemester.SelectedItem.Value + "'";

                if (Session["entryToken"].ToString().ToUpper() == "PARENT")
                {
                    strSql = strSql + " and CR.PMemberID =" + Session["CustIndID"] + "";
                }
            }
            //Parent 
            else if (User == true)
            {

                strSql = "SELECT distinct P.[Name], P.ProductGroupID from dbo.[ProductGroup] P INNER JOIN dbo.[CoachReg] CR  ON P.ProductGroupid =CR.ProductGroupid ";
                strSql = strSql + " Where  CR.Approved='Y' and CR.EventYear=" + ddlEventYear.SelectedValue + " and CR.ChildNumber=" + ddlChild.SelectedValue + " and P.EventId =" + ddlEvent.SelectedValue + " and CR.Semester='" + ddlSemester.SelectedItem.Value + "'";
                if (Session["entryToken"].ToString().ToUpper() == "PARENT")
                {
                    strSql = strSql + " and CR.PMemberID =" + Session["CustIndID"] + "";
                }
            }

            DataSet dsproductgroup;
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            dsproductgroup = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql);
            ddlProductGroup.DataSource = dsproductgroup;
            ddlProductGroup.DataBind();
            if (ddlProductGroup.Items.Count < 1)
            {
                lblError.Text = "No Product is opened.";
                ddlProduct.Items.Clear();
                ddlProductGroup.SelectedValue = "-1";
                ddlProduct.SelectedValue = "-1";
                //Btn_Submit.Enabled = false;
            }
            else if (ddlProductGroup.Items.Count > 1)
            {
                //Btn_Submit.Enabled = true;
                ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
                ddlProductGroup.Items[0].Selected = true;
                ddlProductGroup.Enabled = true;
            }
            else
            {
                // Btn_Submit.Enabled = true;
                ddlProductGroup.Enabled = false;
                LoadProductID();
                // LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text);
            }


        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    private void LoadProductID()
    {
        // will load depending on Selected item in Productgroup
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        if (ddlProductGroup.Items[0].Selected == true && ddlProductGroup.SelectedItem.Text == "Select Product Group")
            ddlProduct.Enabled = false;
        else
        {
            String strSql = "";
            try
            {
                //Volunteer
                if (Session["entryToken"].ToString().ToUpper() == "VOLUNTEER")
                {
                    //strSql = "Select Distinct ProductID, Name from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupID =" + ddlProductGroup.SelectedValue;
                    //if (lblPrd.Text.Length > 0)
                    //    strSql = strSql + " AND ProductID IN (" + lblPrd.Text + ")";
                    //strSql = strSql + " and ProductGroupID =" + ddlProductGroup.SelectedValue + " order by ProductID";

                    // Hide by Simson

                    strSql = "SELECT distinct P.[Name], P.ProductID from dbo.[Product] P INNER JOIN dbo.[CoachReg] CR ON P.ProductGroupid = CR.ProductGroupid and P.ProductID = CR.ProductID";
                    strSql = strSql + " Where CR.Approved='Y' and CR.EventYear=" + ddlEventYear.SelectedValue + " and CR.ChildNumber=" + ddlChild.SelectedValue + " and CR.Semester='" + ddlSemester.SelectedItem.Value + "' and P.EventId =" + ddlEvent.SelectedValue;
                    strSql = strSql + " and P.ProductGroupId=" + ddlProductGroup.SelectedValue;
                }
                else if (Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT")
                {
                    strSql = "SELECT distinct P.[Name], P.ProductID from dbo.[Product] P INNER JOIN dbo.[CoachReg] CR ON P.ProductGroupid = CR.ProductGroupid and P.ProductID = CR.ProductID";
                    strSql = strSql + " Where CR.PMemberID =" + Session["CustIndID"] + " and CR.Approved='Y' and CR.EventYear=" + ddlEventYear.SelectedValue + " and CR.ChildNumber=" + ddlChild.SelectedValue + " and CR.Semester='" + ddlSemester.SelectedItem.Value + "' and P.EventId =" + ddlEvent.SelectedValue;
                    strSql = strSql + " and P.ProductGroupId=" + ddlProductGroup.SelectedValue;
                }

                SqlDataReader drproductid;
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
                ddlProduct.DataSource = drproductid;
                ddlProduct.DataBind();
                if (ddlProduct.Items.Count > 1)
                {
                    ddlProduct.Items.Insert(0, new ListItem("Select Product"));
                    ddlProduct.Items[0].Selected = true;
                    ddlProduct.Enabled = true;
                    // Btn_Submit.Enabled = true;
                }
                else if (ddlProduct.Items.Count < 1)
                {
                    //Btn_Submit.Enabled = false;
                    ddlProduct.Enabled = false;
                    ddlProduct.SelectedValue = "-1";
                }
                else
                    ddlProduct.Enabled = false;
                //Btn_Submit.Enabled = true;
                LoadLevel(ddlLevel);
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
            }
        }
    }

    private void LoadLevel(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;

        String StrSQL = "SELECT DISTINCT Level FROM [CoachReg] WHERE EventYear = " + ddlEventYear.SelectedValue + " AND ChildNumber = " + ddlChild.SelectedValue + " and  Approved ='Y' and Semester='" + ddlSemester.SelectedValue + "'";

        if (!ddlProductGroup.SelectedItem.Text.Contains("Select"))
        {
            StrSQL = StrSQL + " AND ProductGroupID = " + ddlProductGroup.SelectedValue;
        }
        if (ddlProduct.Items.Count > 0)
        {
            if (!ddlProduct.SelectedItem.Text.Contains("Select"))
            {
                StrSQL = StrSQL + " AND ProductID = " + ddlProduct.SelectedValue;
            }
        }

        if (Session["EntryToken"].ToString() == "parent")
        {
            StrSQL = StrSQL + "and PMemberID = " + Session["CustIndId"].ToString() + "";
        }
        DataSet dsGrade = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);

        if (dsGrade.Tables[0].Rows.Count > 0)
        {
            ddlObject.DataSource = dsGrade;
            ddlObject.DataBind();
            ddlObject.Enabled = false;
        }
        if (ddlObject.Items.Count == 0)
            ddlObject.SelectedValue = "-1";

    }

    private void LoadCoachDetails()
    {
        DataSet dsChild = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, string.Format("SELECT Distinct C.ChildNumber,C.FIRST_NAME + ' ' + C.LAST_NAME as ChildName, c.grade  FROM Child C Inner Join CoachReg CR on CR.PMemberID = C.MEMBERID and CR.ChildNumber =C.ChildNumber WHERE C.Grade>=6 AND CR.Approved='Y' and CR.EventYear =" + ddlEventYear.SelectedValue + " and CR.Semester='" + ddlSemester.SelectedValue + "' and cr.childnumber =" + ddlChild.SelectedValue + " AND C.MemberId=" + Convert.ToInt32(Session["CustIndID"])));//and CR.EventYear =" + ddlEventYear.SelectedValue + "

        //if (dsChild.Tables.Count == 0)
        //{
        //    lblError.Text = "No Child Registration Found";

        //}
        //else 
        if (dsChild.Tables.Count > 0)
        {
            if (dsChild.Tables[0].Rows.Count > 0)
                hdnChGrade.Value = dsChild.Tables[0].Rows[0]["Grade"].ToString();
        }

        if (ddlChild.Items.Count > 0 && ddlChild.SelectedItem.Text == "Select")
        {
            lblError.Text = "Select Child name";
        }
        else if (ddlChild.Items.Count > 0 && ddlChild.SelectedItem.Text != "Select")
        {
            Clear();

            LoadProductGroup(true);
        }


    }

    protected void ddlChild_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        Clear();
        LoadCoachDetails();






        //DataSet dsGrade = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select Distinct Grade From child where childNumber=" + ddlChild.SelectedValue);
        //if (dsGrade.Tables[0].Rows.Count > 0)
        //{
        //    hdnChGrade.Value = dsGrade.Tables[0].Rows[0]["Grade"].ToString();
        //}


    }
    private void Clear()
    {
        lblError.Text = "";
        lblSuccess.Text = "";
    }
    protected void ddlProductGroup_SelectedIndexChanged(Object sender, System.EventArgs e)
    {
        Clear();
        ddlProduct.Items.Clear();
        LoadProductID();
        //LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text);
    }

    protected void SetSectionTimers()
    {
        try
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,
            string.Format("SELECT Distinct SectionNumber ,SectionTimeLimit AS SectionTimeLimit FROM [TestSetupSections] Where CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " Group By SectionNumber,SectionTimeLimit Order By SectionNumber ASC"));//  ddlEventYear.SelectedValue));//  ddlTestNumber.SelectedValue

            if (ds.Tables.Count > 0)
            {
                TrTimers.Visible = true;
                int Count = ds.Tables[0].Rows.Count;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        dictionary.Add(Int32.Parse(row["SectionNumber"].ToString()), row["SectionTimeLimit"].ToString());
                    }
                    Timer1.Text = dictionary[1];
                    if (Count > 1) Timer2.Text = dictionary[2];
                    if (Count > 2) Timer3.Text = dictionary[3];
                    if (Count > 3) Timer4.Text = dictionary[4];
                    if (Count > 4) Timer5.Text = dictionary[5];
                }
                else
                {
                    Timer1.Text = Timer2.Text = Timer3.Text = Timer4.Text = Timer5.Text = string.Empty;
                }
            }

        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }

    }
    /// <summary>
    /// Handles the Sorting event of the gvLevel1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (GridViewSortExpression == e.SortExpression && GridViewSortDirection == C_Ascending)
            GridViewSortDirection = C_Descending;
        else if (GridViewSortExpression == e.SortExpression && GridViewSortDirection == C_Descending)
            GridViewSortDirection = C_Ascending;
        else
            GridViewSortDirection = C_Ascending;

        GridViewSortExpression = e.SortExpression;

        // BindGridData();
    }

    /// <summary>
    /// Handles the RowDataBound event of the gvLevel1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = GridView1.DataKeys[e.Row.RowIndex];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        string lquestiontype = dataKey.Values["QuestionType"].ToString();
                        if (lquestiontype.Equals("RadioButton"))
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = false;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = true;
                            // var lradiobuttonlist = new RadioButtonList
                            //                            {
                            //                                ID =
                            //                                    string.Format("rbl_{0}_{1}_{2}", e.Row.Cells[0].Text,
                            //                                                  e.Row.Cells[2].Text,
                            //                                                  e.Row.Cells[3].Text),
                            //                                RepeatDirection = RepeatDirection.Horizontal,
                            //                                RepeatLayout = RepeatLayout.Table
                            //                            };
                            // lradiobuttonlist.Items.Add(new ListItem("A", "A"));
                            // lradiobuttonlist.Items.Add(new ListItem("B", "B"));
                            // lradiobuttonlist.Items.Add(new ListItem("C", "C"));
                            // lradiobuttonlist.Items.Add(new ListItem("D", "D"));
                            // lradiobuttonlist.Items.Add(new ListItem("E", "E"));
                            // e.Row.Cells[4].Controls.Clear();                                 
                            //.Add(lradiobuttonlist);
                        }
                        else
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = true;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = false;
                        }
                        //e.Row.Cells[0].Text = (count1).ToString()  ;


                    }
                    //DataRow row;

                }
            }
            int count1 = 0;
            foreach (GridViewRow row in GridView1.Rows)
            {
                row.Cells[0].Text = (++count1).ToString();
            }

        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString()); //throw;
        }
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = GridView2.DataKeys[e.Row.RowIndex];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        string lquestiontype = dataKey.Values["QuestionType"].ToString();
                        if (lquestiontype.Equals("RadioButton"))
                        {
                            e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = false;
                            e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = true;
                        }
                        else
                        {
                            e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = true;
                            e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = false;
                        }
                    }
                }
            }
            int count2 = 0;
            foreach (GridViewRow row in GridView2.Rows)
            {
                row.Cells[0].Text = (++count2).ToString();
            }
        }
        catch (Exception)
        {
            //throw;
        }
    }
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = GridView3.DataKeys[e.Row.RowIndex];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        string lquestiontype = dataKey.Values["QuestionType"].ToString();
                        if (lquestiontype.Equals("RadioButton"))
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = false;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = true;
                        }
                        else
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = true;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = false;
                        }
                    }
                }
            }
            int count3 = 0;
            foreach (GridViewRow row in GridView3.Rows)
            {
                row.Cells[0].Text = (++count3).ToString();
            }
        }
        catch (Exception)
        {
            //throw;
        }
    }

    protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = GridView4.DataKeys[e.Row.RowIndex];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        string lquestiontype = dataKey.Values["QuestionType"].ToString();
                        if (lquestiontype.Equals("RadioButton"))
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = false;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = true;
                        }
                        else
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = true;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = false;
                        }
                    }
                }
            }
            int count4 = 0;
            foreach (GridViewRow row in GridView4.Rows)
            {
                row.Cells[0].Text = (++count4).ToString();
            }
        }
        catch (Exception)
        {
            //throw;
        }
    }
    protected void GridView5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataKey dataKey = GridView5.DataKeys[e.Row.RowIndex];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        string lquestiontype = dataKey.Values["QuestionType"].ToString();
                        if (lquestiontype.Equals("RadioButton"))
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = false;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = true;
                        }
                        else
                        {
                            e.Row.Cells[1].FindControl("gvtxtCorrectAnswer").Visible = true;
                            e.Row.Cells[1].FindControl("rblCorrectAnswer").Visible = false;
                        }
                    }
                }
            }
            int count5 = 0;
            foreach (GridViewRow row in GridView5.Rows)
            {
                row.Cells[0].Text = (++count5).ToString();
            }
        }
        catch (Exception)
        {
            //throw;
        }
    }


    /// <summary>
    /// Handles the PageIndexChanging event of the gvLevel1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    //GridView1.PageIndex = e.NewPageIndex;
    //    //BindGridData();
    //}


    #endregion

    #region Private Methods

    /// <summary>
    /// Binds the level1.
    /// </summary>
    private void BindGridData()
    {
        ClearGridData();

        //if (Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT")
        //{
        //    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, " Select Count(*) From ChildTestSummary where ChildNumber=" + ddlChild.SelectedValue + " and EventYear=" + ddlEventYear.SelectedValue + " and CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " and Completed='Y'")) > 0)
        //    {
        //        btnstart.Enabled = false;
        //        btnSubmit.Enabled = false;
        //        Btn_Save.Enabled = false;
        //        //Btn_Load.Enabled = false;
        //        lblSuccess.Text = "Child has already taken the test.";
        //        return;
        //    }
        //}

        if (!ddlEventYear.SelectedValue.Equals(string.Empty))
        {

            //if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from CoachReldates where CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + "  and  EventYear=" + ddlEventYear.SelectedValue + " and  QReleaseDate < GETDATE() and  GETDATE()<= QDeadlineDate")) <= 0)
            //{
            //    lblSuccess.Text = " Answers can only be entered after the Release Date.";
            //    return;
            //}
            //else 
            //{ 
            //    lblSuccess.Text = " ";
            //}

            DataSet ds = new DataSet();
            StringBuilder sb = new StringBuilder("create TABLE #series(id int) ; WITH cte AS ( SELECT 1 AS n UNION ALL SELECT n + 1 FROM cte WHERE n < 100)");
            sb.Append("insert into #series select * from cte c ; Select A.TestSetUpSectionsID,A.CoachPaperID ,A.QuestionType,A.SectionNumber AS TestSectionNumber,");
            sb.Append(" (A.QuestionNumberTo -A.QuestionNumberFrom + 1) as NumberOfQuestions,ISNULL(A.NumberOfChoices,0) as NoOfChoices from #series AS S Cross Join TestSetupSections A");
            sb.Append(" where S.id  < = (A.QuestionNumberTo -A.QuestionNumberFrom + 1) and A.eventYear =" + ddlEventYear.SelectedValue + " AND A.CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " order by A.SectionNumber,A.QuestionNumberFrom; drop table #series ;"); // A.NumberOfQuestions //ddlTestNumber.SelectedValue 
            string query = string.Format(sb.ToString(), ddlEventYear.SelectedValue);

            //query = " Select Distinct A.TestSetUpSectionsID,A.CoachPaperID ,A.QuestionType,A.SectionNumber AS TestSectionNumber,";
            //query = query + "  (A.QuestionNumberTo -A.QuestionNumberFrom+1 )as NumberofQuestions from TestSetupSections A";
            //query = query + " where CoachPaperID=1 order by TestSectionNumber";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, query);

            hdnSections.Value = (SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count( Distinct SectionNumber) from TestSetUpSections where CoachPaperID =" + Convert.ToInt32(Session["CoachPaperID"]))).ToString();//  ddlTestNumber.SelectedValue);//ds.Tables[0].Rows.Count.ToString();

            DataView dv = ds.Tables[0].DefaultView;
            if (ds.Tables[0].Rows.Count > 0)
            {
                btnstart.Enabled = true;
                btnSubmit.Enabled = true;
                Btn_Save.Enabled = true;
                //Btn_Load.Enabled = true;
            }
            else
            {
                btnstart.Enabled = false;
                btnSubmit.Enabled = false;
                Btn_Save.Enabled = false;
                // Btn_Load.Enabled = false;
                TrTimers.Visible = false;
                lblSuccess.Text = "Test Section(s) / Answer Key(s) are not yet available.";// " No Data Present";
            }

            if (GridViewSortExpression.Equals(string.Empty))
            {
                GridViewSortExpression = "TestSectionNumber";
                GridViewSortDirection = C_Ascending;
            }

            dv.Sort = String.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);

            dv.RowFilter = "TestSectionNumber = 1";
            DataTable dt1 = ds.Tables[0];// dv.Table;
            dt1.Columns.Add("QuestionNumber", typeof(int));

            GridView1.DataSource = dt1;
            GridView1.DataBind();
            if (dt1.Rows.Count > 0)
            {
                int NoOfChoices1 = Convert.ToInt32(dt1.Rows[0]["NoOfChoices"]);
                //********************* To show the Options based on Number of Choices********************************/
                RadioButtonList rblChoice;
                foreach (GridViewRow row in GridView1.Rows)
                {
                    rblChoice = (RadioButtonList)(row.FindControl("rblCorrectAnswer"));
                    for (int i = rblChoice.Items.Count - 1; i >= (rblChoice.Items.Count - NoOfChoices1); i--)
                    {
                        if (rblChoice.Items.Count > NoOfChoices1)
                        {
                            rblChoice.Items.RemoveAt(i);
                        }
                    }
                }
            }

            dv.RowFilter = "TestSectionNumber = 2";
            DataTable dt2 = ds.Tables[0];// dv.Table;
            DataRow[] drs1 = dt2.Select("TestSectionNumber = 2");

            GridView2.DataSource = dv;
            GridView2.DataBind();

            if (dt2.Rows.Count > 0)
            {
                int NoOfChoices2 = Convert.ToInt32(dt2.Rows[0]["NoOfChoices"]);
                //********************* To show the Options based on Number of Choices********************************/
                RadioButtonList rblChoice;
                foreach (GridViewRow row in GridView2.Rows)
                {
                    rblChoice = (RadioButtonList)(row.FindControl("rblCorrectAnswer"));
                    for (int i = rblChoice.Items.Count - 1; i >= (rblChoice.Items.Count - NoOfChoices2); i--)
                    {
                        if (rblChoice.Items.Count > NoOfChoices2)
                        {
                            rblChoice.Items.RemoveAt(i);
                        }
                    }
                }
            }
            dv.RowFilter = "TestSectionNumber = 3";
            DataTable dt3 = dv.Table;

            GridView3.DataSource = dv;
            GridView3.DataBind();

            if (dt3.Rows.Count > 0)
            {
                int NoOfChoices3 = Convert.ToInt32(dt3.Rows[0]["NoOfChoices"]);
                //********************* To show the Options based on Number of Choices********************************/
                RadioButtonList rblChoice;
                foreach (GridViewRow row in GridView3.Rows)
                {
                    rblChoice = (RadioButtonList)(row.FindControl("rblCorrectAnswer"));
                    for (int i = rblChoice.Items.Count - 1; i >= (rblChoice.Items.Count - NoOfChoices3); i--)
                    {
                        if (rblChoice.Items.Count > NoOfChoices3)
                        {
                            rblChoice.Items.RemoveAt(i);
                        }
                    }
                }
            }

            dv.RowFilter = "TestSectionNumber = 4";
            DataTable dt4 = dv.Table;

            GridView4.DataSource = dv;
            GridView4.DataBind();

            if (dt4.Rows.Count > 0)
            {
                int NoOfChoices4 = Convert.ToInt32(dt4.Rows[0]["NoOfChoices"]);
                //********************* To show the Options based on Number of Choices********************************/
                RadioButtonList rblChoice;
                foreach (GridViewRow row in GridView4.Rows)
                {
                    rblChoice = (RadioButtonList)(row.FindControl("rblCorrectAnswer"));
                    for (int i = rblChoice.Items.Count - 1; i >= (rblChoice.Items.Count - NoOfChoices4); i--)
                    {
                        if (rblChoice.Items.Count > NoOfChoices4)
                        {
                            rblChoice.Items.RemoveAt(i);
                        }
                    }
                }
            }
            dv.RowFilter = "TestSectionNumber = '5'";
            DataTable dt5 = dv.Table;

            GridView5.DataSource = dv;
            GridView5.DataBind();
            if (dt5.Rows.Count > 0)
            {
                int NoOfChoices5 = Convert.ToInt32(dt5.Rows[0]["NoOfChoices"]);
                //********************* To show the Options based on Number of Choices********************************/
                RadioButtonList rblChoice;
                foreach (GridViewRow row in GridView5.Rows)
                {
                    rblChoice = (RadioButtonList)(row.FindControl("rblCorrectAnswer"));
                    for (int i = rblChoice.Items.Count - 1; i >= (rblChoice.Items.Count - NoOfChoices5); i--)
                    {
                        if (rblChoice.Items.Count > NoOfChoices5)
                        {
                            rblChoice.Items.RemoveAt(i);
                        }
                    }
                }
            }
        }
    }


    #endregion

    protected void ddlEventYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadCoachDetails();

        // Session.Timeout = 1;
        //BindddlStudentID();
        // BindTestNumbers();
        //BindGridData();
        //SetSectionTimers();
    }

    private void BindTestNumbers()
    {

        try
        {

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            string sqlCommand = "usp_GetChildAnswerSheet";
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@PaperType", ddlPaperType.SelectedValue);
            param[1] = new SqlParameter("@ProductGroupID", ddlProductGroup.SelectedValue);
            //param[2] = new SqlParameter("@ProductGroupCode", ddlProductGroup.SelectedItem.Text);
            param[2] = new SqlParameter("@ProductID", ddlProduct.SelectedValue);
            param[3] = new SqlParameter("@PMemberId", Convert.ToInt32(Session["CustIndID"]));
            param[4] = new SqlParameter("@Level", ddlLevel.SelectedItem.Text);
            param[5] = new SqlParameter("@EventYear", ddlEventYear.SelectedValue);
            param[6] = new SqlParameter("@ChildId", ddlChild.SelectedValue);
            param[7] = new SqlParameter("@MemberId", Convert.ToInt32(Session["CustIndID"]));
            param[8] = new SqlParameter("@semester", ddlSemester.SelectedValue);
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, sqlCommand, param);

            //DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL + StrWhereCndn);

            if (ds.Tables[0].Rows.Count > 0)
            {
                DGCoachPapers.DataSource = ds;
                DGCoachPapers.DataBind();
                //ddlTestNumber.Enabled = true;
                //ddlTestNumber.DataSource = ds;
                //ddlTestNumber.DataBind();
                SetSectionTimers();
            }
            else
            {
                DGCoachPapers.DataSource = null;
                DGCoachPapers.DataBind();
                ClearGridData();
                lblError.Text = "Test Papers not yet released";
                return;
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    private void ClearGridData()
    {
        Btn_Save.Enabled = false;
        //Btn_Load.Enabled = false;
        btnSubmit.Enabled = false;
        btnstart.Enabled = false;
        DisableSections();

        ClearGrid(GridView1);
        ClearGrid(GridView2);
        ClearGrid(GridView3);
        ClearGrid(GridView4);
        ClearGrid(GridView5);
        DisableGrids();
    }
    private void DisableSections()
    {
        btnsection1.Enabled = false;
        btnsection2.Enabled = false;
        btnsection3.Enabled = false;
        btnsection4.Enabled = false;
        btnsection5.Enabled = false;

    }
    private void DisableGrids()
    {
        GridView1.Enabled = false;
        GridView2.Enabled = false;
        GridView3.Enabled = false;
        GridView4.Enabled = false;
        GridView5.Enabled = false;

    }
    private void ClearGrid(GridView Grid_View)
    {
        Grid_View.DataSource = null;
        Grid_View.DataBind();
    }

    protected void DGCoachPapers_ItemCommand(Object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        int CoachPaperID = Convert.ToInt32(e.Item.Cells[3].Text);
        int sections = Convert.ToInt32(e.Item.Cells[9].Text);
        String ProductCode = (e.Item.Cells[3].Text).ToString();
        String ProductGroupCode = (e.Item.Cells[4].Text).ToString();
        DateTime QDeadlineDate, Today;
        Button BtnReGrade;
        Session["CoachPaperID"] = CoachPaperID.ToString();
        QDeadlineDate = Convert.ToDateTime(e.Item.Cells[14].Text);//, "QDeadlineDate"));
        Today = DateTime.Now.Date;
        //Response.Write(CoachPaperID);
        if (e.CommandName == "Select")
        {
            // BindGridData();
            LoadGridData();

            BtnReGrade = (Button)(e.Item.Cells[1].FindControl("btnReGrade"));

            //if (Today <= QDeadlineDate)
            //{
            //    BtnReGrade.Enabled = false;
            //}
            //else
            //{
            //e.Item.Cells[1].Enabled = true;
            //if (Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT")
            //{

            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, " Select Count(*) From ChildTestSummary where ChildNumber=" + ddlChild.SelectedValue + " and EventYear=" + ddlEventYear.SelectedValue + " and CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " and Completed='Y'")) > 0)
            {
                btnstart.Enabled = false;
                btnSubmit.Enabled = false;
                Btn_Save.Enabled = false;
                //Btn_Load.Enabled = false;
                lblSuccess.Text = "Child has already taken the test.<br />Please Regrade the scores if needed";

                BtnReGrade.Enabled = true;
            }
            else if (QDeadlineDate < Today)
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, " Select Count(*) From EnterAnsException where ChildNumber=" + ddlChild.SelectedValue + " and EventYear=" + ddlEventYear.SelectedValue + " and CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " and GETDATE() < NewDeadLine")) > 0)
                {
                    btnstart.Enabled = true;
                    btnSubmit.Enabled = true;
                    Btn_Save.Enabled = true;
                }
                else
                {
                    btnstart.Enabled = false;
                    btnSubmit.Enabled = false;
                    Btn_Save.Enabled = false;

                    BtnReGrade.Enabled = false;
                    lblSuccess.Text = "Deadline Date Crossed";
                }
            }
            //}
            //}


        }
        else if (e.CommandName == "Regrade")
        {
            lblSuccess.Text = "";
            GradeScores();
        }
    }
    protected void DGCoachPapers_ItemDataBound(Object source, System.Web.UI.WebControls.DataGridItemEventArgs e)
    {


        //     QDeadlineDate = Convert.ToDateTime(DGCoachPapers.Items[i].Cells[15].Text);//, "QDeadlineDate"));
        //     Today = DateTime.Now.Date;
        //     if (Today <= QDeadlineDate)
        //     { 
        //         DGCoachPapers.Items[i].Cells[1].Enabled =false;
        //     }
        //     else
        //     {
        //         DGCoachPapers.Items[i].Cells[1].Enabled = true;
        //     }

        //}

    }

    private void EnableSections()
    {
        btnsection1.Enabled = true;
        btnsection2.Enabled = true;
        btnsection3.Enabled = true;
        btnsection4.Enabled = true;

        int Sections = Convert.ToInt32(hdnSections.Value);
        if (Sections < 5)
            btnsection5.Enabled = false;
        else if (Sections == 5)
            btnsection5.Enabled = true;

        if (Sections < 4)
            btnsection4.Enabled = false;
        else if (Sections == 4)
            btnsection4.Enabled = true;

        if (Sections < 3)
            btnsection3.Enabled = false;
        else if (Sections == 3)
            btnsection3.Enabled = true;

        if (Sections < 2)
            btnsection2.Enabled = false;
        else if (Sections == 2)
            btnsection2.Enabled = true;

        if (Sections < 1)
            btnsection1.Enabled = false;
        else if (Sections == 1)
            btnsection1.Enabled = true;

    }
    protected void BindddlStudentID()
    {
        List<string> list = new List<string>();
        DataSet ds = new DataSet();

        //  ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, string.Format("SELECT Distinct C.ChildNumber,C.FIRST_NAME + ' ' + C.LAST_NAME as ChildName  FROM Child C Inner Join CoachReg CR on CR.PMemberID = C.MEMBERID and CR.ChildNumber =C.ChildNumber WHERE C.Grade>=6 AND CR.Approved='Y'  AND C.MemberId=" + Convert.ToInt32(Session["CustIndID"])));//and CR.EventYear =" + ddlEventYear.SelectedValue + "
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, string.Format("SELECT Distinct C.ChildNumber,C.FIRST_NAME + ' ' + C.LAST_NAME as ChildName  FROM Child C WHERE C.Grade>=6 AND C.MemberId=" + Convert.ToInt32(Session["CustIndID"])));//and CR.EventYear =" + ddlEventYear.SelectedValue + "

        if (ds.Tables.Count > 0)
        {

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlChild.DataSource = ds;
                    ddlChild.DataBind();
                }
            }
        }
    }

    protected void btnConfirmSubmit_Click(object sender, EventArgs e)
    {

        //Btn_Submit.Enabled = false;
        //btnConfirmSubmit.Enabled = false;
        Btn_Save.Enabled = false;
        //Btn_Load.Enabled = false;

        /*******Enters Answers to ChildTestAnswers Table for each section**********/
        SubmitResults();
        lblSuccess.Text = "Child Test completed successfully.";

        GradeScores();


        //Response.Redirect("~/Results.aspx");
    }
    private void GradeScores()
    {

        /**********Calculate Child Scores on submit************/
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@EventYear", ddlEventYear.SelectedValue);
        param[1] = new SqlParameter("@ChildNumber", ddlChild.SelectedValue);
        param[2] = new SqlParameter("@CoachPaperID", Convert.ToInt32(Session["CoachPaperID"]));

        /*********************Calculation of Student Scores in ChildTestAnswes table ******************************/
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_CalcSATStudentScores", param);

        /*****************Calculaion of Scaled scores in ChildTestSummary table**********************************/
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_CalcSATScaledTestScores", param);

        /**********Set Completed flag when the child has completed the test***************/
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, " Update ChildTestSummary set Completed='Y' Where EventYear =" + ddlEventYear.SelectedValue + " and ChildNumber=" + ddlChild.SelectedValue + " and CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]));

        lblSuccess.Text = lblSuccess.Text + "<br /> Student Test Scores Calulations Done.";

    }
    private void SubmitResults()
    {
        theTime.InnerHtml = "";
        btnSubmit.Enabled = false;
        btnstart.Enabled = false;

        int lChildNumber = 0;
        //if (Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT")
        //{
        lChildNumber = Convert.ToInt32(ddlChild.SelectedValue);
        //  SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, " Update ChildTestSummary set Completed='Y' Where Year =" + ddlEventYear.SelectedValue + " and ChildNumber=" + ddlChild.SelectedValue);
        //}
        //else
        //    lChildNumber = Convert.ToInt32(ddlChild.SelectedValue);

        int lCoachPaperID = Convert.ToInt32(Session["CoachPaperID"]);//ddlTestNumber.SelectedValue);// Convert.ToInt32(ddlEventYear.SelectedValue);
        SubmitSectionResults(GridView1, lChildNumber, lCoachPaperID);
        SubmitSectionResults(GridView2, lChildNumber, lCoachPaperID);
        SubmitSectionResults(GridView3, lChildNumber, lCoachPaperID);
        SubmitSectionResults(GridView4, lChildNumber, lCoachPaperID);
        SubmitSectionResults(GridView5, lChildNumber, lCoachPaperID);
        pnlsubmit.Visible = false;
    }

    protected void SubmitSectionResults(GridView grid, int lChildNumber, int lCoachPaperID)
    {
        /*********Entering Data to ChildTEstAnswers for each section *************/
        try
        {

            string correctanswer = string.Empty;
            int lSectionNumber = 0;
            if (grid.Rows.Count > 0)
            {
                lSectionNumber = Convert.ToInt32(grid.DataKeys[0]["TestSectionNumber"]);
            }
            int lQuestionNumber = 1;
            foreach (GridViewRow gvr in grid.Rows)
            {
                if (gvr.RowType == DataControlRowType.DataRow)
                {
                    if (gvr.Cells[1].HasControls())
                    {
                        int count = gvr.Cells[1].Controls.Count;
                        foreach (Control control in gvr.Cells[1].Controls)
                        {
                            if (control is RadioButtonList)
                            {
                                if (control.Visible == true)
                                {
                                    RadioButtonList list = (RadioButtonList)control;
                                    list.Enabled = true;
                                    correctanswer = list.SelectedValue.ToString();
                                }
                            }
                            else if (control is TextBox)
                            {
                                if (control.Visible == true)
                                {
                                    TextBox tb = (TextBox)control;
                                    tb.Enabled = true;
                                    correctanswer = tb.Text.ToString();
                                }
                            }
                        }
                    }
                }
                string lAnswer = correctanswer;
                int createdBy = Convert.ToInt32(Session["LoginID"]);// CurrentUser;
                string createdDate = CurrentTime;
                int modifiedBy = Convert.ToInt32(Session["LoginID"]); // CurrentUser;
                string modifiedDate = CurrentTime;
                SqlParameter[] param = new SqlParameter[11];

                param[0] = new SqlParameter("@EventYear", ddlEventYear.SelectedValue);
                param[1] = new SqlParameter("@MemberID", Session["CustIndID"]);
                param[2] = new SqlParameter("@ChildNumber", lChildNumber);
                param[3] = new SqlParameter("@CoachPaperID", lCoachPaperID);
                param[4] = new SqlParameter("@SectionNumber", lSectionNumber);
                param[5] = new SqlParameter("@QuestionNumber", lQuestionNumber);
                param[6] = new SqlParameter("@Answer", lAnswer);
                param[7] = new SqlParameter("@CreatedBy", createdBy);
                param[8] = new SqlParameter("@CreatedDate", createdDate);
                param[9] = new SqlParameter("@ModifiedBy", modifiedBy);
                param[10] = new SqlParameter("@ModifiedDate", modifiedDate);
                //param[0].Direction = ParameterDirection.Input;
                // param[0].Value = UserId;

                SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "[dbo].[usp_ChildTestAnswersInsertUpdate]", param);

                //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), "[dbo].[usp_ChildTestAnswersInsertUpdate]",ddlEventYear.SelectedValue, lChildNumber,
                //                                    lCoachPaperID,
                //                                 lSectionNumber, lQuestionNumber, lAnswer, createdBy, createdDate, modifiedBy,
                //                             modifiedDate);
                lQuestionNumber += 1;

            }
        }
        catch
        {
        }
    }
    private Boolean VerifyChild()
    {
        if ((Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT") && ddlChild.SelectedItem.Text.Contains("Select"))
        {
            lblSuccess.Text = " Please Select Child Name.";
            return false;
        }
        else if (Session["entryToken"].ToString().ToUpper() == "VOLUNTEER" && ddlChild.SelectedItem.Text.Contains("Select"))
        {
            lblSuccess.Text = " Please Select Child Name.";
            return false;
        }
        else
            return true;

    }
    private void LoadGridView(GridView grid)
    {
        try
        {
            string correctanswer = string.Empty;
            int lSectionNumber = 0;

            int lChildNumber = 0;
            if (Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT")
                lChildNumber = Convert.ToInt32(ddlChild.SelectedValue);
            else
                lChildNumber = Convert.ToInt32(ddlChild.SelectedValue);

            if (grid.Rows.Count > 0)
            {
                lSectionNumber = Convert.ToInt32(grid.DataKeys[0]["TestSectionNumber"]);
            }
            int lQuestionNumber = 1;
            String Strsql = "";
            Strsql = "Select CoachPaperId,SectionNumber,QuestionNumber,Answer From ChildTestAnswers Where EventYear =" + ddlEventYear.SelectedValue + " and childnumber =" + lChildNumber + " and CoachPaperID = " + Convert.ToInt32(Session["CoachPaperID"]) + " and SectionNumber = " + lSectionNumber; // ddlTestNumber.SelectedValue
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (GridViewRow gvr in grid.Rows)
                {

                    if (gvr.RowType == DataControlRowType.DataRow)
                    {
                        if (gvr.Cells[1].HasControls())
                        {
                            int count = gvr.Cells[1].Controls.Count;
                            foreach (Control control in gvr.Cells[1].Controls)
                            {
                                if (control is RadioButtonList)
                                {
                                    if (control.Visible == true)
                                    {
                                        RadioButtonList list = (RadioButtonList)control;
                                        list.Enabled = true;
                                        list.SelectedValue = ds.Tables[0].Rows[lQuestionNumber - 1]["Answer"].ToString();
                                        // correctanswer = list.SelectedValue.ToString();

                                    }
                                }
                                else if (control is TextBox)
                                {
                                    if (control.Visible == true)
                                    {
                                        TextBox tb = (TextBox)control;
                                        tb.Enabled = true;
                                        tb.Text = ds.Tables[0].Rows[lQuestionNumber - 1]["Answer"].ToString();
                                        //correctanswer = tb.Text.ToString();

                                    }
                                }
                            }
                        }
                    }
                    //string lAnswer = correctanswer;
                    lQuestionNumber += 1;

                }
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }
    protected string GridFilterExpression
    {
        get
        {
            if (ViewState[VS_FilterExpression] == null)
                ViewState[VS_FilterExpression] = string.Empty;

            return ViewState[VS_FilterExpression].ToString();
        }
        set { ViewState[VS_FilterExpression] = value; }
    }

    /// <summary>
    /// Property to store Sort Expression of the grid
    /// </summary>
    /// <value>The grid view sort expression.</value>
    protected string GridViewSortExpression
    {
        get
        {
            if (ViewState[VS_SortExpression] == null)
                ViewState[VS_SortExpression] = string.Empty;

            return ViewState[VS_SortExpression].ToString();
        }
        set { ViewState[VS_SortExpression] = value; }
    }

    /// <summary>
    /// Property to store Sort Direction of the grid
    /// </summary>
    /// <value>The grid view sort direction.</value>
    protected string GridViewSortDirection
    {
        get
        {
            if (ViewState[VS_SortDirection] == null)
                ViewState[VS_SortDirection] = C_Ascending;

            return ViewState[VS_SortDirection].ToString();
        }
        set { ViewState[VS_SortDirection] = value; }
    }
    protected string CurrentUser
    {
        get { return HttpContext.Current.User.Identity.Name; }
    }

    protected string CurrentTime
    {
        get { return DateTime.Now.ToString(); }
    }
    //protected void ddlmemberID_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Clear();
    //   // btnstart.Enabled = true;
    //   // btnSubmit.Enabled = true;

    //}

    protected void btnSubmit_Click1(object sender, EventArgs e)
    {
        btnSubmit.Enabled = false;
        lblWrngmsg.Visible = false;
        pnlsubmit.Visible = true;
        int lChildNumber = 0;
        lChildNumber = Convert.ToInt32(ddlChild.SelectedValue);

        if (pnlsubmit.Visible == true)
        {
            Session["AnsCount"] = 0;
            Session["QuesCount"] = 0;
            int lCoachPaperID = Convert.ToInt32(Session["CoachPaperID"]);//ddlTestNumber.SelectedValue);// Convert.ToInt32(ddlEventYear.SelectedValue);
            CalcSectionResults(GridView1, lChildNumber, lCoachPaperID);
            CalcSectionResults(GridView2, lChildNumber, lCoachPaperID);
            CalcSectionResults(GridView3, lChildNumber, lCoachPaperID);
            CalcSectionResults(GridView4, lChildNumber, lCoachPaperID);
            CalcSectionResults(GridView5, lChildNumber, lCoachPaperID);

        }
        if (Convert.ToInt32(Session["QuesCount"]) - Convert.ToInt32(Session["AnsCount"]) > 0)
        {
            lblTotalCount.Text = Convert.ToInt32(Session["QuesCount"]) - Convert.ToInt32(Session["AnsCount"]) + " out of " + Convert.ToInt32(Session["QuesCount"]) + " were not Answered.";
        }

        btnstart.Enabled = false;
        Btn_Save.Enabled = false;
        DisableSections();
        DisableGrids();
    }
    protected void CalcSectionResults(GridView grid, int lChildNumber, int lCoachPaperID)
    {
        /*********Calculation for Total Number of Answers given by the child for all the sections.*************/
        string correctanswer = string.Empty;
        int lSectionNumber = 0;
        if (grid.Rows.Count > 0)
        {
            lSectionNumber = Convert.ToInt32(grid.DataKeys[0]["TestSectionNumber"]);
        }
        int lQuestionNumber = 0;
        int AnsCount = 0;
        foreach (GridViewRow gvr in grid.Rows)
        {
            if (gvr.RowType == DataControlRowType.DataRow)
            {
                if (gvr.Cells[1].HasControls())
                {
                    int count = gvr.Cells[1].Controls.Count;
                    foreach (Control control in gvr.Cells[1].Controls)
                    {
                        if (control is RadioButtonList)
                        {
                            if (control.Visible == true)
                            {
                                RadioButtonList list = (RadioButtonList)control;
                                list.Enabled = true;
                                correctanswer = list.SelectedValue.ToString();

                                if (list.SelectedValue.ToString() != "")
                                {
                                    AnsCount = AnsCount + 1;
                                }

                            }
                        }
                        else if (control is TextBox)
                        {
                            if (control.Visible == true)
                            {
                                TextBox tb = (TextBox)control;
                                tb.Enabled = true;
                                correctanswer = tb.Text.ToString();

                                if (correctanswer != "")
                                {
                                    AnsCount = AnsCount + 1;
                                }
                            }
                        }
                    }
                }
            }
            string lAnswer = correctanswer;
            lQuestionNumber += 1;

        }
        Session["AnsCount"] = Convert.ToInt32(Session["AnsCount"]) + AnsCount;
        Session["QuesCount"] = Convert.ToInt32(Session["QuesCount"]) + lQuestionNumber;

    }

    protected void Btn_Submit_Click(object sender, EventArgs e)
    {
        Clear();
        ClearGridData();
        if (Session["entryToken"].ToString().ToUpper() == "PARENT" || Session["entryToken"].ToString().ToUpper() == "STUDENT")
        {

            if (ddlChild.SelectedItem.Text == "Select")
                lblError.Text = "Please Select Child";
            else if (ddlPaperType.SelectedItem.Text == "Select")
                lblError.Text = "Please Select Paper Type";
            else if (ddlProductGroup.Items.Count == 0 || ddlProduct.Items.Count == 0)
                lblError.Text = "No Product is opened";
            else if (ddlProductGroup.SelectedItem.Text == "Select")
                lblError.Text = "Please Select Product Group";
            else if (ddlProduct.SelectedItem.Text.Contains("Select"))
                lblError.Text = "Please Select Product";
            // else if (ddlLevel.SelectedItem.Text == "Select")
            //  lblError.Text = "Please Select Child";
            else
                BindTestNumbers();
        }
        else if (Session["EntryToken"].ToString().ToUpper() == "VOLUNTEER")
        {
            if (ddlChild.SelectedItem.Text == "Select")
                lblError.Text = "Please Select Child";
            else if (ddlPaperType.SelectedItem.Text == "Select")
                lblError.Text = "Please Select Paper Type";
            else if (ddlProductGroup.Items.Count == 0 || ddlProduct.Items.Count == 0)
                lblError.Text = "No Product is opened";
            else if (ddlProductGroup.SelectedItem.Text == "Select")
                lblError.Text = "Please Select Product Group";
            else if (ddlProduct.SelectedItem.Text.Contains("Select"))
                lblError.Text = "Please Select Product";
            // else if (ddlLevel.SelectedItem.Text == "Select")
            //  lblError.Text = "Please Select Child";
            else
                BindTestNumbers();

            // BindTestNumbers();
        }
    }
    protected void Btn_Save_Click(object sender, EventArgs e)
    {
        Clear();
        if (VerifyChild() == false)
            return;
        SubmitResults();
        lblSuccess.Text = "Child Test Answers saved successfully.";
        try
        {
            if (Session["CoachPaperID"] != null)
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, " Select Count(*) From ChildTestSummary where ChildNumber=" + ddlChild.SelectedValue + " and EventYear=" + ddlEventYear.SelectedValue + " and CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " and Completed='Y'")) > 0)
                {
                    btnstart.Enabled = false;

                    //Modified BtnSubmit as enabled as per Praveen's comment.
                    // btnSubmit.Enabled = false;
                    btnSubmit.Enabled = true;
                    Btn_Save.Enabled = false;
                }
                else
                {
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, " Select Count(*) From EnterAnsException where ChildNumber=" + ddlChild.SelectedValue + " and EventYear=" + ddlEventYear.SelectedValue + " and CoachPaperID=" + Convert.ToInt32(Session["CoachPaperID"]) + " and GETDATE() < NewDeadLine")) > 0)
                    {
                        btnstart.Enabled = true;
                        btnSubmit.Enabled = true;
                        Btn_Save.Enabled = true;
                    }
                    else
                    {
                        btnstart.Enabled = false;
                        //Modified BtnSubmit as enabled as per Praveen's comment.
                        //btnSubmit.Enabled = false;
                        btnSubmit.Enabled = true;
                        Btn_Save.Enabled = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }

    }
    //protected void Btn_Load_Click(object sender, EventArgs e)
    //{
    //    //LoadGridData();

    //}
    protected void LoadGridData()
    {
        Clear();

        if (VerifyChild() == false)
            return;

        BindGridData();
        LoadGridView(GridView1);
        LoadGridView(GridView2);
        LoadGridView(GridView3);
        LoadGridView(GridView4);
        LoadGridView(GridView5);

    }
    protected void btnsection1_click(object sender, EventArgs e)
    {
        btnsection1.Enabled = false;

        GridView1.Enabled = true;
        GridView2.Enabled = false;
        GridView3.Enabled = false;
        GridView4.Enabled = false;
        GridView5.Enabled = false;
    }
    protected void btnsection2_click(object sender, EventArgs e)
    {
        btnsection2.Enabled = false;

        GridView1.Enabled = false;
        GridView2.Enabled = true;
        GridView3.Enabled = false;
        GridView4.Enabled = false;
        GridView5.Enabled = false;
    }
    protected void btnsection3_click(object sender, EventArgs e)
    {
        btnsection3.Enabled = false;

        GridView1.Enabled = false;
        GridView2.Enabled = false;
        GridView3.Enabled = true;
        GridView4.Enabled = false;
        GridView5.Enabled = false;
    }
    protected void btnsection4_click(object sender, EventArgs e)
    {
        btnsection4.Enabled = false;

        GridView1.Enabled = false;
        GridView2.Enabled = false;
        GridView3.Enabled = false;
        GridView4.Enabled = true;
        GridView5.Enabled = false;
    }
    protected void btnsection5_click(object sender, EventArgs e)
    {
        btnsection5.Enabled = false;

        GridView1.Enabled = false;
        GridView2.Enabled = false;
        GridView3.Enabled = false;
        GridView4.Enabled = false;
        GridView5.Enabled = true;
    }

    protected void btnstart_Click(object sender, EventArgs e)
    {
        Clear();
        lblWrngmsg.Visible = true;
        if (VerifyChild() == false)
            return;

        btnstart.Enabled = false;
        EnableSections();


    }
    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        pnlsubmit.Visible = false;
        btnSubmit.Enabled = true;
        Btn_Save.Enabled = true;
        EnableSections();
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadLevel(ddlLevel);
    }
    protected void BtnGradeCard_Click(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToInt32(ddlChild.SelectedValue) <= 0)
            {
                lblError.Text = "Please Select Child Name.";
            }
            else if (ddlProductGroup.SelectedItem.Text == "Select")
            {
                lblError.Text = "Please Select Product Group";
            }
            else if (ddlProduct.Items.Count == 0 || ddlProduct.SelectedItem.Text == "Select")
            {
                lblError.Text = "Please Select Product";
            }
            else if (ddlLevel.SelectedValue == null)
            {
                lblError.Text = "Please Select Level";
            }
            else
            {
                Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" + ddlChild.SelectedValue + "&EventYear=" + ddlEventYear.SelectedValue + "&ProductGroup=" + ddlProductGroup.SelectedValue + "&Product=" + ddlProduct.SelectedValue + "&Level=" + ddlLevel.SelectedItem.Text + "','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');</script> ");
                //Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" + ddlChild.SelectedValue + "&EventYear=" + ddlEventYear.SelectedValue + "','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');</script> ");
            }
        }
        catch (Exception ex)
        {
            // lblError.Text = ex.ToString();
        }
    }

    protected void lnkParent_Click(object sender, EventArgs e)
    {
        if (Btn_Save.Enabled == true || btnSubmit.Enabled == true)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "warningMessage()", true);
        }
        else
        {
            Response.Redirect("~/UserFunctions.aspx");
        }
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["entryToken"].ToString().ToUpper() == "STUDENT")
        {
            Loadchild(Convert.ToInt32(Session["StudentId"]));
        }
        else
        {
            Loadchild(0);
        }
    }
    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue);
    }
}


