﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections;
using VRegistration;


public partial class MakeupSessions : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblerr.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {
                loadyear();
                loadPhase(ddlPhase);
                if (Session["RoleID"] != null)
                {
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96")
                    {
                        btnDowloadFIle.Visible = true;
                    }
                }

                LoadEvent(ddEvent);

                fillMeetingGrid();
                // TestCreateTrainingSession();
            }
        }
    }

    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }
    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }
    public void fillProductGroup()
    {

        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            // ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 0;
                ddlProduct.Enabled = false;
                loadLevel();
                LoadSessionNo();
                populateRecClassDate();
                if (Session["RoleID"].ToString() == "88")
                {
                }

            }
            else
            {
                loadLevel();
                LoadSessionNo();
                populateRecClassDate();
                ddlProduct.Enabled = true;
            }

        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();
        //fillCoach();
    }

    public void fillCoach()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedItem.Value + "' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
            }

        }

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        populateRecClassDate();
        loadLevel();
    }

    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
            }
        }

    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;

                ddchapter.SelectedValue = "112";
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {

                    fillCoach();
                }
                else
                {
                    fillCoach();
                    fillProductGroup();
                }
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }

        }
        else
        {

            lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {



        if (validatemeeting() == "1")
        {
            if (btnCreateMeeting.Text == "Create Makeup Session")
            {
                timeOverLapValdiation();
                //ValidatMakeUpSession();

            }
            else
            {
                deleteMeeting();
                timeOverLapValdiation();
                // updateZoomSession();
            }
        }
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid();
        btnAddNewMeeting.Visible = false;
        tblAddNewMeeting.Visible = true;
    }
    protected void btnAddNewMeeting_Click(object sender, EventArgs e)
    {
        tblAddNewMeeting.Visible = true;
    }

    public void ValidatMakeUpSession()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;
            string SessionCreationDate = string.Empty;
            string CoachChange = string.Empty;

            CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.Level='" + ddlLevel.SelectedValue + "' and CS.Semester='" + ddlPhase.SelectedValue + "'";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        count++;

                        int IsSameDay = 0;
                        int IsSameTime = 0;
                        string WebExID = dr["UserID"].ToString();
                        string Pwd = dr["PWD"].ToString();
                        int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                        string ScheduleType = dr["ScheduleType"].ToString();

                        string year = dr["EventYear"].ToString();
                        string eventID = dr["EventID"].ToString();
                        string chapterId = ddchapter.SelectedValue;
                        string ProductGroupID = dr["ProductGroupID"].ToString();
                        string ProductGroupCode = dr["ProductGroupCode"].ToString();
                        string ProductID = dr["ProductID"].ToString();
                        string ProductCode = dr["ProductCode"].ToString();
                        string Semester = dr["Semester"].ToString();
                        string Level = dr["Level"].ToString();
                        string Sessionno = dr["SessionNo"].ToString();
                        string CoachID = dr["MemberID"].ToString();
                        string CoachName = dr["CoachName"].ToString();

                        string MeetingPwd = "training";

                        string Date = txtDate.Text;
                        string Time = dr["Time"].ToString();
                        string Day = dr["Day"].ToString();
                        string BeginTime = dr["Begin"].ToString();
                        string EndTime = dr["End"].ToString();
                        string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        if (BeginTime == "")
                        {
                            BeginTime = "20:00:00";
                        }
                        if (EndTime == "")
                        {
                            EndTime = "21:00:00";
                        }


                        int Duration = 60;

                        string timeZoneID = string.Empty;

                        timeZoneID = ddlTimeZone.SelectedValue;

                        string TimeZone = ddlTimeZone.SelectedItem.Text;
                        string SignUpId = dr["SignupID"].ToString();
                        double Mins = 0.0;
                        DateTime dFrom;
                        DateTime dTo;


                        string sDateFrom = BeginTime;
                        string sDateTo = EndTime;
                        if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                        {
                            TimeSpan TS = dTo - dFrom;
                            Mins = TS.TotalMinutes;

                        }
                        string durationHrs = Mins.ToString("0");
                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }

                        int duration = Convert.ToInt32(durationHrs);
                        hdnDuration.Value = duration.ToString();
                        if (timeZoneID == "4")
                        {
                            TimeZone = "EST/EDT – Eastern";
                        }
                        else if (timeZoneID == "7")
                        {
                            TimeZone = "CST/CDT - Central";
                        }
                        else if (timeZoneID == "6")
                        {
                            TimeZone = "MST/MDT - Mountain";
                        }
                        string userID = Session["LoginID"].ToString();


                        if (dr["MakeupMeetKey"].ToString() == "" && dr["MeetingKey"].ToString() != "")
                        {
                            DateTime dStart;
                            DateTime dEnd;

                            string DateFrom = ddlMakeupTime.SelectedValue;
                            string DateTo = ddlMakeupTime.SelectedValue;

                            CmdText = "select * from CalSignup where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y'";
                            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                            foreach (DataRow dr1 in ds.Tables[0].Rows)
                            {
                                DateTime dtFromS = new DateTime();
                                DateTime dtEnds = new DateTime();
                                sDateFrom = dr1["Begin"].ToString();
                                sDateTo = dr1["End"].ToString();
                                if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                                {
                                    TimeSpan TS = dTo - dFrom;
                                    Mins = TS.TotalMinutes;
                                    dtFromS = dFrom;
                                    dtEnds = dTo;

                                }

                                if (DateTime.TryParse(DateFrom, out dStart) && DateTime.TryParse(DateTo, out dEnd))
                                {
                                    DateTime dtStart = dStart;
                                    DateTime dtEnd = dEnd;

                                    if (dtStart > dtFromS && dtStart > dtEnds)
                                    {

                                    }
                                    else if (dtStart < dtFromS && dtStart < dtEnds && dtEnd < dtEnds && dtEnd < dtFromS)
                                    {

                                    }
                                    else
                                    {
                                        IsSameTime = 1;
                                    }
                                }

                                if (dr1["Day"].ToString() == Convert.ToDateTime(txtDate.Text).ToString("dddd"))
                                {
                                    IsSameDay = 1;
                                }

                            }

                            if (IsSameDay == 1 && IsSameTime == 1)
                            {
                                string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");

                                lblerr.Text = "Time is not available...!";
                            }
                            else
                            {
                                IsSameTime = 0;
                                IsSameDay = 0;
                                string WebExUserID = string.Empty;
                                string WebExUserPwd = string.Empty;
                                string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                CmdText = "select * from CalSignup where EventYear=" + ddYear.SelectedValue + " and Accepted='Y' and UserID='" + WebExID + "' and Pwd='" + Pwd + "' and EventYear='" + year + "'";
                                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                                foreach (DataRow dr1 in ds.Tables[0].Rows)
                                {
                                    DateTime dtFromS = new DateTime();
                                    DateTime dtEnds = new DateTime();
                                    sDateFrom = dr1["Begin"].ToString();
                                    sDateTo = dr1["End"].ToString();
                                    if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                                    {
                                        TimeSpan TS = dTo - dFrom;
                                        Mins = TS.TotalMinutes;
                                        dtFromS = dFrom;
                                        dtEnds = dTo;

                                    }

                                    if (DateTime.TryParse(DateFrom, out dStart) && DateTime.TryParse(DateTo, out dEnd))
                                    {
                                        DateTime dtStart = dStart;
                                        DateTime dtEnd = dEnd;

                                        if (dtStart > dtFromS && dtStart > dtEnds)
                                        {
                                            WebExUserID = dr1["UserID"].ToString();
                                            WebExUserPwd = dr1["Pwd"].ToString();
                                        }
                                        else if (dtStart < dtFromS && dtStart < dtEnds && dtEnd < dtEnds && dtEnd < dtFromS)
                                        {
                                            WebExUserID = dr1["UserID"].ToString();
                                            WebExUserPwd = dr1["Pwd"].ToString();
                                        }
                                        else
                                        {
                                            IsSameTime = 1;
                                        }
                                    }

                                    if (dr1["Day"].ToString() == Convert.ToDateTime(txtDate.Text).ToString("dddd"))
                                    {
                                        IsSameDay = 1;
                                    }

                                }

                                //if (IsSameTime != 1 && IsSameDay != 1)
                                //{
                                //TimeSpan EndSessTime = GetTimeFromString(ddlMakeupTime.SelectedValue, Convert.ToInt32(txtDuration.Text));
                                //string strEndTime = EndSessTime.ToString();
                                //string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                //CreatMakeUpSessions(WebExUserID, WebExUserPwd, "Term", 200, txtDate.Text, ddlMakeupTime.SelectedValue, SignUpId, CoachID, strStartDay, strEndTime);
                                // }
                                //  else
                                // {
                                string strDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                int sessDuration = Convert.ToInt32(hdnDuration.Value) + 30;
                                TimeSpan EndSessTime = GetTimeFromString(ddlMakeupTime.SelectedValue, sessDuration);
                                string strEndTime = EndSessTime.ToString();
                                string strEndSessTime = EndSessTime.ToString();
                                TimeSpan TsStartSessTime = GetTimeFromStringSubtract(ddlMakeupTime.SelectedValue, -30);
                                string strStartTime = TsStartSessTime.ToString();

                                CmdText = "select distinct UserID,Pwd,VRoom from CalSignUp where EventYear=2015 and Accepted='Y' and [Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and [End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and UserID not in (select distinct UserID from CalSignUp where EventYear=2015 and Accepted='Y' and ([Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' or [End] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "') and UserID is not null and day='" + Day + "') order by Vroom ASC";

                                // CmdText = "select * from CalSignup CS left join WebConfLog WC on (CS.MemberID=WC.MemberID)  where  CS.[Begin] not between '" + ddlMakeupTime.SelectedValue + "' and '" + EndSessTime + "' and CS.day='" + day + "' and WC.BeginTime not between '" + ddlMakeupTime.SelectedValue + "' and '" + EndSessTime + "' and CS.eventyear = '" + ddYear.SelectedValue + "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" + ddlMakeupTime.SelectedValue + "' and '" + EndSessTime + "' and CS.day='" + strDay + "' and  eventyear = '" + ddYear.SelectedValue + "' and Accepted='Y') and CS.UserID<>'" + userID + "' and CS.PWD<>'" + Pwd + "'";

                                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                                DataSet ds1 = new DataSet();
                                CmdText = "select WC.UserID,WC.PWD from WebConfLog WC where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' or wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and  WC.eventyear = '" + year + "' and (SessionType='Substitute' or SessionType='Makeup') and WC.MemberID in(select CMemberID from CoachReg where EventYear='" + year + "' and Approved='Y') order by PWD ASC";

                                ds1 = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                                IsSameTime = 0;
                                IsSameDay = 0;
                                string Uid = string.Empty;
                                foreach (DataRow dr1 in ds.Tables[0].Rows)
                                {
                                    if (ds1.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow dr2 in ds1.Tables[0].Rows)
                                        {
                                            if (dr1["UserID"].ToString() != dr2["UserID"].ToString())
                                            {
                                                WebExUserID = dr1["UserID"].ToString();
                                                WebExUserPwd = dr1["Pwd"].ToString();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        WebExUserID = dr1["UserID"].ToString();
                                        WebExUserPwd = dr1["Pwd"].ToString();
                                    }
                                }
                                if (WebExUserID != "" && WebExUserPwd != "")
                                {
                                    string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                    // CreatMakeUpSessions(WebExUserID, WebExUserPwd, "Term", 200, txtDate.Text, ddlMakeupTime.SelectedValue, SignUpId, CoachID, strStartDay, strEndTime);
                                    makeZoomSession(txtDate.Text, ddlMakeupTime.SelectedValue, SignUpId, CoachID, strStartDay, strEndTime, duration);
                                }
                                else
                                {
                                    lblerr.Text = "Time is not available...!";
                                }

                                // }
                            }

                        }
                        else
                        {
                            if (dr["MeetingKey"].ToString() == "")
                            {
                                lblerr.Text = "Training sessions not created yet for the selected coach.!";
                            }
                            else if (dr["MakeupMeetKey"].ToString() != "")
                            {
                                lblerr.Text = "Please cancel the old makeup session before creating new makeup session.";
                            }
                        }
                    }
                }
                else
                {
                    lblerr.Text = "No child is assigned for the selected Coach..";
                }
            }


        }
        catch (Exception ex)
        {
            //throw new Exception(ex.Message);
        }

    }



    public void fillMeetingGrid()
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;
        if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,vl.vroom,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SignupID,vl.hostid from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID) inner join virtualroomlookup vl on (vl.userid=vc.userid) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.MemberID=" + ddlCoach.SelectedValue + " ";
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdtext += " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdtext += " and VC.ProductID=" + ddlProduct.SelectedValue + "";
            }
            else if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
            {
                cmdtext += " and VC.MemberID=" + ddlCoach.SelectedValue + "";
            }


            cmdtext += "order by VC.ProductGroupCode";


        }
        else
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SignupID,vl.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup cs on (cs.MakeupMeetkey=vc.Sessionkey) inner join virtualroomlookup Vl on (Vl.UserID=VC.userID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " ";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdtext += " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdtext += " and VC.ProductID=" + ddlProduct.SelectedValue + "";
            }



            cmdtext += " order by VC.ProductGroupCode";
        }

        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }

                        // enable/disable join meeting button

                        string startDate = GrdMeeting.Rows[i].Cells[15].Text;
                        DateTime dtStartDate = Convert.ToDateTime(startDate);
                        string currentDate = DateTime.Now.ToShortDateString();
                        DateTime dtCurrentDate = Convert.ToDateTime(currentDate);
                        if (dtStartDate >= dtCurrentDate)
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = true;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = false;
                        }


                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }


    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }


        else if (ddlPhase.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "")
        {
            lblerr.Text = "Please select Level";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (DDlSubDate.SelectedValue == "0")
        {
            lblerr.Text = "Please select Rec Class Date";
            retVal = "-1";
        }
        else if (txtDate.Text == "")
        {
            lblerr.Text = "Please fill Date";
            retVal = "-1";
        }

        else if (ddlMakeupTime.SelectedValue == "")
        {
            lblerr.Text = "Please fill Time";
            retVal = "-1";
        }

        if (ddlReason.SelectedValue == "0")
        {
            lblerr.Text = "Please select Reason";
            retVal = "-1";
        }
        if (validateMakeupDate() == "1")
        {
            if (validateMakeupDateAndTime() == "1")
            {
                if (valdiatePriorClassStatus() == "1")
                {

                }
                else
                {
                    retVal = "-1";
                }
            }
            else
            {
                retVal = "-1";
            }
        }
        else
        {
            retVal = "-1";
        }




        return retVal;
    }





    public string validateMakeupDate()
    {
        string retVal = "1";
        DateTime dtCurrent = DateTime.Now;
        string strCurrentDate = DateTime.Now.ToString("MM/dd/yyyy");

        //string strCurrentDate = DateTime.Now.ToString("dd/MM/yyyy");

        dtCurrent = Convert.ToDateTime(strCurrentDate);
        if (DDlSubDate.SelectedValue != "0" && txtDate.Text != "")
        {
            string startDate = txtDate.Text;
            DateTime dtStartDate = Convert.ToDateTime(startDate.ToString());

            DateTime dtNextRecDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(7);

            DateTime dtClassDate = Convert.ToDateTime(DDlSubDate.SelectedValue);

            if (dtStartDate < dtCurrent && dtClassDate < dtCurrent)
            {
                lblerr.Text = "Makeup Date should be greater than Rec Class Date";
                retVal = "-1";
            }
            else if (dtStartDate > dtNextRecDate)
            {
                lblerr.Text = "Makeup Date should be less than or equal to  next Rec Class Date";
                retVal = "-1";

            }
            else
            {
                retVal = "1";
            }
        }
        return retVal;

    }
    public string validateMakeupDateAndTime()
    {
        try
        {
            string cmdtext = "select BeginTime, EndTime, Duration from WebConfLog where Eventyear=" + ddYear.SelectedItem.Value + " and memberID=" + ddlCoach.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and Session=" + ddlSession.SelectedValue + " and SessionType='Recurring Meeting' and Semester='" + ddlPhase.SelectedValue + "' ";
            int duration = 0;
            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["Duration"] != null)
                    {
                        duration = Convert.ToInt32(ds.Tables[0].Rows[0]["Duration"].ToString());
                    }
                    else
                    {
                        duration = 90;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        string retVal = "1";
        DateTime dtCurrent = DateTime.Now;
        DateTime dtCurrentTime = DateTime.Now;
        string strCurrentDate = DateTime.Now.ToString("MM/dd/yyyy");
        //string strCurrentDate = DateTime.Now.ToString("dd/MM/yyyy");
        dtCurrent = Convert.ToDateTime(strCurrentDate);
        if (DDlSubDate.SelectedValue != "0" && txtDate.Text != "")
        {
            string startDate = txtDate.Text;
            string startTime = ddlMakeupTime.SelectedValue;
            string startDateTime = startDate + " " + startTime;
            DateTime dtStartDateTime = Convert.ToDateTime(startDateTime.ToString());

            DateTime dtStartDate = Convert.ToDateTime(startDate.ToString());
            DateTime dtClassDate = Convert.ToDateTime(DDlSubDate.SelectedValue);

            string nakeupTime = Convert.ToString(dtStartDateTime.AddMinutes(90).ToString("MM/dd/yyyy HH:mm:ss"));

            string strmakeupTime = nakeupTime.Substring(nakeupTime.IndexOf(' ')).Trim();
            nakeupTime = strmakeupTime.Substring(0, 8).Trim();
            int overlapCount = 0;
            DateTime dtNextRecDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(7);

            if (dtNextRecDate.ToShortDateString() == dtStartDateTime.ToShortDateString())
            {
                string cmdQuery = "select * from webconflog where Eventyear=" + ddYear.SelectedItem.Value + " and memberID=" + ddlCoach.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and Session=" + ddlSession.SelectedValue + " and (BeginTime between ('" + ddlMakeupTime.SelectedValue + "') and ('" + nakeupTime + "') or EndTime between('" + ddlMakeupTime.SelectedValue + "') and ('" + nakeupTime + "') ) and  SessionType='Recurring Meeting' and Semester='" + ddlPhase.SelectedValue + "'";


                try
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdQuery);
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            overlapCount = ds.Tables[0].Rows.Count;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                if (overlapCount > 0)
                {
                    lblerr.Text = "Makeup Time should not between rec class time.";
                    retVal = "-1";
                }
                else
                {
                    retVal = "1";
                }
            }


            else if (dtStartDate < dtCurrent && dtClassDate < dtCurrent)
            {
                lblerr.Text = "Makeup Date should be greater than today date and Rec Class Date";
                retVal = "-1";
            }
            else if (dtStartDateTime <= dtCurrentTime)
            {
                lblerr.Text = "Begin Time should be greater than current time";
                retVal = "-1";
            }
            else
            {
                retVal = "1";
            }
        }

        return retVal;

    }
    public string valdiatePriorClassStatus()
    {
        string retVal = "1";
        if (DDlSubDate.SelectedValue != "0" && txtDate.Text != "")
        {
            int selIndex = DDlSubDate.SelectedIndex;
            string recClassPrevDate = DDlSubDate.SelectedValue;
            string cmdText = string.Empty;
            int count = 0;


            if (selIndex > 1)
            {
                recClassPrevDate = DDlSubDate.Items[selIndex - 1].Value;
                cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + recClassPrevDate + "' and Semester='" + ddlPhase.SelectedValue + "'";
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }

                int holidayCount = 0;
                cmdText = "select count(*) as CountSet from CoachingDateCal where EventYear=" + ddYear.SelectedValue + " and EventID=13 and (ScheduleType='Christmas' or ScheduleType='Thanksgiving'  or ScheduleType='Diwali' or ScheduleType='Halloween') and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Semester='" + ddlPhase.SelectedValue + "' and  ('" + recClassPrevDate + "' between StartDate and EndDate) or (StartDate = '" + recClassPrevDate + "') or (EndDate='" + recClassPrevDate + "')";

                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        holidayCount = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }

                if (count == 0 && holidayCount == 0)
                {

                    retVal = "-1";
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlertmsgClassStatus();", true);
                }
                else
                {
                    if (holidayCount == 0)
                    {
                        count = 1;
                        try
                        {

                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        if (selIndex > 2)
                        {
                            recClassPrevDate = DDlSubDate.Items[selIndex - 1].Value;
                        }
                        else
                        {
                            recClassPrevDate = Convert.ToDateTime(DDlSubDate.Items[selIndex - 1].Value).AddDays(-7).ToShortDateString();
                        }

                        cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + recClassPrevDate + "' and Semester='" + ddlPhase.SelectedValue + "'";

                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        if (null != ds && ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                            }
                        }
                    }

                    if (holidayCount == 0 && count == 0)
                    {
                    }
                    else if (holidayCount > 0 && count > 0)
                    {
                        try
                        {
                            int cntWeek = 0;
                            cntWeek = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select max(weekno) from coachclasscal where eventyear=" + ddYear.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and memberID=" + ddlCoach.SelectedValue + " and level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Semester='" + ddlPhase.SelectedValue + "'"));
                            cntWeek = cntWeek + 1;

                            string coachClassText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Reason, CreateDate, CreatedBy)";
                            coachClassText += "select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo,'" + DDlSubDate.Items[selIndex - 1].Value + "',Day,Time,Duration," + cntWeek + "," + cntWeek + ",'Cancelled','" + ddlReason.SelectedValue + "',GetDate()," + Session["LoginID"].ToString() + " from calsignup where Eventyear=" + ddYear.SelectedValue + " and MemberId=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "'";

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);
                        }
                        catch
                        {
                        }
                    }

                    if (count > 0)
                    {
                        cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date between '" + DDlSubDate.SelectedItem.Text + "' and  '" + Convert.ToDateTime(recClassPrevDate).AddDays(7).ToShortDateString() + "' and Status='Makeup' and Semester='" + ddlPhase.SelectedValue + "'";
                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        if (null != ds && ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                            }
                        }

                        if (count == 0)
                        {
                        }
                        else
                        {
                            lblerr.Text = "Duplicate exists.";
                            retVal = "-1";
                        }
                    }
                    else
                    {
                        retVal = "-1";
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlertmsgClassStatus();", true);
                    }
                }

            }
        }
        return retVal;

    }

    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                if (hdnMeetingStatus.Value == "SUCCESS")
                {



                    string CmdText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "', MakeUpRegID=" + hdsnRegistrationKey.Value + ", makeUpAttendeeID=" + hdnMeetingAttendeeID.Value + " where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y' and Level='" + Level + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblSuccess.Text = "Selected child registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                    fillStudentList(MemberID, ddlProductGroup.SelectedItem.Text, ddlProduct.SelectedItem.Text);
                }

            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string PMemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblPMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;

                string LoginSessionID = string.Empty;
                string LoginSessionChildID = string.Empty;
                hdnChildMeetingURL.Value = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblChildURL") as Label).Text;
                DataSet ds = new DataSet();
                string email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                int countEmail = 0;
                string ProductCode = string.Empty;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + email.Trim() + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
                //if (Session["LoginID"] != null)
                //{
                //    LoginSessionID = Session["LoginID"].ToString();
                //}
                //if (Session["StudentID"] != null)
                //{
                //    LoginSessionChildID = Session["StudentID"].ToString();
                //}
                //if (LoginSessionID == PMemberID)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                //else if (LoginSessionChildID == ChildNumber)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                if (countEmail > 0)
                {

                }
                else
                {
                    lblerr.Text = "Only authorized child can join ito the training session.";
                }
            }
            else if (e.CommandName == "Join")
            {

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string meetingLink = ((LinkButton)GrdStudentsList.Rows[selIndex].FindControl("HlAttendeeMeetURL") as LinkButton).Text;
                hdnZoomURL.Value = meetingLink;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                hdnStartTime.Value = beginTime;

                string coachname = ((Label)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as Label).Text;

                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                hdnDay.Value = day;


                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {

                    //   logerrors(coachname, "Success", "");
                    CoachingExceptionLog.createExceptionLog(coachname, "Success", "", "Makeup Sessions");
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                }
                else
                {
                    //   logerrors(coachname, "Failure", "");
                    CoachingExceptionLog.createExceptionLog(coachname, "Success", "", "Makeup Sessions");
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert()", true);
                }


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string signupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSignupID") as Label).Text;
                hdnSignupID.Value = signupID;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;


                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnSessionKey.Value = sessionKey;
                hdnHostID.Value = hostID;

                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                ddlCoach.Enabled = false;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();

                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;


                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                loadLevel();
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                LoadSessionNo();
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;

                txtDate.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStartDate") as Label).Text;


                hdnDuration.Value = GrdMeeting.Rows[selIndex].Cells[19].Text;

                ddlMakeupTime.SelectedValue = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;

                sessionKey = GrdMeeting.Rows[selIndex].Cells[11].Text;
                hdnSessionKey.Value = sessionKey;
                tblAddNewMeeting.Visible = true;

                btnCreateMeeting.Text = "Update Makeup Session";
                hdnSessionKey.Value = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;




                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnSessionNo.Value = GrdMeeting.Rows[selIndex].Cells[10].Text;

                fillStudentList(MemberID, ProductGroupCode, ProductCode);
            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                //hdnWebExMeetURL.Value = MeetingURl;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;

                if (hdnMeetingStatus.Value == "SUCCESS")
                {
                    string MeetingURl = hdnHostURL.Value;
                    string URL = MeetingURl.Replace("&amp;", "&");
                    hdnWebExMeetURL.Value = URL;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
                }
                else
                {
                    //lblerr.Text = hdnMeetingStatus.Value;
                }
            }
            else if (e.CommandName == "DeleteMeeting")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;

                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnSessionNo.Value = GrdMeeting.Rows[selIndex].Cells[10].Text;

                string sessionStartDate = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStartDate") as Label).Text;
                hdnStartDate.Value = sessionStartDate;

                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnHostID.Value = hostID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteMeeting();", true);
            }

            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;
                string coachName = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStHostID") as Label).Text;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now.AddMinutes(60);
                double mins = 40.0;
                string duration = "-" + ((Label)GrdMeeting.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                int iduration = 0;
                try
                {
                    iduration = Convert.ToInt32(duration);
                }
                catch
                {
                    iduration = 120;
                }
                if (duration == "-")
                {
                    iduration = 120;
                }
                else
                {
                    iduration = Convert.ToInt32(duration);
                }

                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {
                    if (mins < iduration)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                    }
                    else
                    {
                        hdnHostID.Value = hostID;
                        try
                        {
                            listLiveMeetings();
                        }
                        catch
                        {
                        }
                        getmeetingIfo(sessionKey, hostID);
                        string cmdText = "";
                        cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        string meetingLink = hdnHostURL.Value;

                        try
                        {
                            // logerrors(coachName, "Success", "");
                            CoachingExceptionLog.createExceptionLog(coachName, "Success", "", "Makeup Sessions");
                        }
                        catch
                        {
                        }

                        hdnZoomURL.Value = meetingLink;
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                    }

                }
                else
                {
                    try
                    {
                        CoachingExceptionLog.createExceptionLog(coachName, "Failed", "", "Makeup Sessions");
                        // logerrors(coachName, "Failed", "");
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);
                    }
                    catch
                    {
                    }
                   
                }
            }
        }
        catch (Exception ex)
        {
            //  logerrors("", "Exception", ex.Message);
            CoachingExceptionLog.createExceptionLog("", "Exception", "", "Makeup Sessions");
        }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid();
    }

    protected void GrdMeeting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdMeeting.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        GrdMeeting.EditIndex = -1;
        string sessionKey = string.Empty;
        sessionKey = GrdMeeting.Rows[rowIndex].Cells[12].Text;
        string WebExID = string.Empty;
        string WebExPwd = string.Empty;
        WebExID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExID") as Label).Text;
        WebExPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExPwd") as Label).Text;
        hdnSessionKey.Value = sessionKey;
        hdnWebExID.Value = WebExID;
        hdnWebExPwd.Value = WebExPwd;
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);


    }


    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode)
    {

        trChildList.Visible = true;
        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select C1.ChildNumber,CS.Time,CS.Day,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber) inner join CalSignup CS on (CS.MemberID=CR.CmemberID and CS.ProductGroupID=CR.productGroupID and CS.ProductID=CR.ProductID and CS.Level=CR.Level and CS.SessionNo=CR.SessionNo and CS.EventYear=CR.EventYear and CS.Semester=CR.Semester) where CR.Level='" + HdnLevel.Value + "' and CR.Semester='" + ddlPhase.SelectedValue + "' and CR.EventYear='" + ddYear.SelectedValue + "' and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and  CR.EventID=" + ddEvent.SelectedValue + " and CR.SessionNo='" + ddlSession.SelectedValue + "' and Approved='Y' order by C1.Last_Name, C1.First_Name Asc";




        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdStudentsList.DataSource = ds;
                GrdStudentsList.DataBind();
                lblChildMsg.Text = "";
                btnClosetable2.Visible = true;

                for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                {
                    if (((LinkButton)GrdStudentsList.Rows[i].FindControl("HlAttendeeMeetURL") as LinkButton).Text != "")
                    {
                        ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                    }
                    else
                    {
                        ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                    }
                }
            }
            else
            {
                GrdStudentsList.DataSource = ds;
                GrdStudentsList.DataBind();
                lblChildMsg.Text = "No record found";
                btnClosetable2.Visible = false;

            }
        }
    }





    protected void btnClosetable2_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
    }

    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {

    }

    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }

    public void createZoomMeeting(string mode)
    {
        try
        {


            string URL = string.Empty;

            string service = "1";
            if (mode == "1")
            {
                URL = "https://api.zoom.us/v1/meeting/create";
            }
            else if (mode == "2")
            {
                URL = "https://api.zoom.us/v1/meeting/update";
            }
            string urlParameter = string.Empty;

            string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

            string date = txtDate.Text;
            string strMonth = date.Substring(0, date.IndexOf('-'));
            string strDay = date.Substring(date.IndexOf('-') + 1, 2);
            string year = date.Substring(date.LastIndexOf('-') + 1, 4);
            date = year + "-" + strMonth + "-" + strDay;

            string time = ddlMakeupTime.SelectedValue;
            string dateTime = date + "T" + time + "Z";
            string topic = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text + "-" + ddlSession.SelectedValue;

            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            DateTime dtTo = new DateTime();
            if (DateTime.TryParse(time, out dtFromS))
            {
                TimeSpan TS = dtFromS - dtEnds;
                mins = TS.TotalMinutes;
                dtTo = dtFromS.AddHours(4);
            }
            string timeFormat = dtTo.Hour + ":" + dtTo.Minute + ":" + dtTo.Second;

            dateTime = date + "T" + timeFormat + "Z";
            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            if (mode == "1")
            {
                urlParameter += "&topic=" + topic + "";
                urlParameter += "&password=training";
                urlParameter += "&type=2";
                urlParameter += "&start_time=" + dateTime + "";
                urlParameter += "&duration=" + hdnDuration.Value + "";
                urlParameter += "&timezone=GMT-5:00";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
            }
            else if (mode == "2")
            {
                service = "2";
                urlParameter += "&id=" + hdnSessionKey.Value + "";
                urlParameter += "&start_time=" + dateTime + "";
                urlParameter += "&duration=" + hdnDuration.Value + "";
                urlParameter += "&timezone=GMT-5:00";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
            }


            makeZoomAPICall(urlParameter, URL, service);


        }
        catch
        {
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnHostURL.Value = json["start_url"].ToString();

                hdnMeetingUrl.Value = json["join_url"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            else if (serviceType == "2")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            else if (serviceType == "3")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnSessionKey.Value + "");

                string cmdText = string.Empty;

                string strQuery;
                SqlCommand cmd;
                strQuery = "update CalSignUp set status=null, MakeUpURL=null,MakeupMeetKey=null,MakeUpPwd=null,makeUpDate=null,MakeUpTime=null, MakeUpDuration=null where MakeupMeetKey=@MeetingKey";
                cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@MeetingKey", hdnSessionKey.Value);

                InsertUpdateData(cmd);

                //cmdText = "update CalSignUp set status=null, MakeUpURL=null,MakeupMeetKey=null,MakeUpPwd=null,makeUpDate=null,MakeUpTime=null, MakeUpDuration=null where MakeupMeetKey='" + hdnSessionKey.Value + "'";


                //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);



                cmdText = "update CoachReg set status=null, MakeUpURL=null,MakeUpAttendeeID=null,MakeUpRegID=null,MakeUpDate=null,MakeUpTime=null,MakeUpDuration=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y' and SessionNo=" + hdnSessionNo.Value + "";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                string cmdCoachRegText = string.Empty;

                DateTime dtClassDate = Convert.ToDateTime(hdnStartDate.Value);
                DateTime dtToday = DateTime.Now;
                if (dtToday < dtClassDate)
                {
                    cmdCoachRegText = "Update CoachClassCal set Status='Makeup Cancelled' where MemberID=" + hdnCoachID.Value + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and SessionNo=" + hdnSessionNo.Value + " and [Date]='" + hdnStartDate.Value + "'";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdCoachRegText);
                }



                fillMeetingGrid();
                lblSuccess.Text = "Makeup Session deleted successfully";
            }

            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

            }

            if (serviceType == "6")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                JToken token = JToken.Parse(postResponse);
                JArray men = (JArray)token.SelectToken("meetings");
                string SessionKey = string.Empty;
                foreach (JToken m in men)
                {
                    if (m["host_id"].ToString() == hdnHostID.Value)
                    {
                        termianteMeeting(m["id"].ToString(), hdnHostID.Value);
                    }

                }

            }
            if (serviceType == "7")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);

                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            }

            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);


        }
        catch
        {
            hdnMeetingStatus.Value = "Failure";
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }

    public void makeZoomSession(string StartDate, string Time, string SignupID, string CoachID, string Day, string EndTime, int duration)
    {
        hdnDuration.Value = duration.ToString();
        createZoomMeeting("1");
        hdnMeetingStatus.Value = "SUCCESS";
        if (hdnMeetingStatus.Value == "SUCCESS")
        {
            string cmdText = "";

            string meetingURL = hdnHostURL.Value;
            string cancelledDate = DDlSubDate.SelectedValue;

            string makeUpDate = txtDate.Text;

            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SessionType)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + CoachID + ",'" + hdnSessionKey.Value + "','" + makeUpDate + "','" + makeUpDate + "','" + ddlMakeupTime.SelectedValue + "','" + EndTime + "','" + ddlPhase.SelectedValue + "','" + ddlLevel.SelectedValue + "',1,'training'," + duration + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + hdnZoomUserID.Value + "','" + hdnZoomPwd.Value + "','Scheduled Meeting')";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                cmdText = "Update CalSignup set MakeUpURL='" + meetingURL + "', MakeupMeetKey='" + hdnSessionKey.Value + "', MakeUpPwd='training',MakeUpDate='" + makeUpDate + "',MakeUpTime='" + ddlMakeupTime.SelectedValue + "',MakeUpDuration='" + hdnDuration.Value + "' where SignupID=" + SignupID + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                string ChidName = string.Empty;
                string Email = string.Empty;
                string City = string.Empty;
                string Country = string.Empty;
                string ChildNumber = string.Empty;
                string CoachRegID = string.Empty;

                DataSet dsChild = new DataSet();

                string ChildText = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber) where CR.Level='" + ddlLevel.SelectedValue + "' and CR.Semester='" + ddlPhase.SelectedValue + "' and CR.ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and CR.ProductCode='" + ddlProduct.SelectedItem.Text + "' and CR.CMemberID=" + ddlCoach.SelectedValue + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + " and CR.approved='Y' and CR.SessionNo=" + ddlSession.SelectedValue + "";



                dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                if (null != dsChild && dsChild.Tables.Count > 0)
                {
                    if (dsChild.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow drChild in dsChild.Tables[0].Rows)
                        {
                            ChidName = drChild["Name"].ToString();
                            Email = drChild["WebExEmail"].ToString();
                            if (Email.IndexOf(';') > 0)
                            {
                                Email = Email.Substring(0, Email.IndexOf(';'));
                            }
                            City = drChild["City"].ToString();
                            Country = drChild["Country"].ToString();
                            ChildNumber = drChild["ChildNumber"].ToString();
                            CoachRegID = drChild["CoachRegID"].ToString();
                            //registerMeetingsAttendees(ChidName, Email, City, Country);
                            // if (hdnMeetingStatus.Value == "SUCCESS")
                            //{

                            string CmdChildUpdateText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "',ModifyDate=GetDate(), ModifiedBy='" + Session["LoginID"].ToString() + "',MakeUpDate='" + makeUpDate + "',MakeUpTime='" + ddlMakeupTime.SelectedValue + "',MakeUpDuration='" + duration + "' where CoachRegID=" + CoachRegID + "";

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);
                            // }
                        }

                        string coachClassText = string.Empty;
                        int selIndex = DDlSubDate.SelectedIndex;
                        int recClassCount = DDlSubDate.Items.Count;
                        int serNo = recClassCount - selIndex;

                        string weekNoText = "select max(WeekNo) as WeekNo from CoachClassCal where MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and (Status='On' or Status='Substitute' or Status='Makeup')";
                        int weekNo = 0;
                        DataSet dsWeek = new DataSet();
                        dsWeek = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, weekNoText);
                        if (null != dsWeek && dsWeek.Tables.Count > 0)
                        {
                            if (dsWeek.Tables[0].Rows.Count > 0)
                            {
                                if (dsWeek.Tables[0].Rows[0]["WeekNo"].ToString() != "")
                                {
                                    weekNo = Convert.ToInt32(dsWeek.Tables[0].Rows[0]["WeekNo"].ToString());
                                }
                                else
                                {
                                    weekNo = 0;
                                }
                            }
                        }

                        if (btnCreateMeeting.Text == "Update Makeup Session")
                        {

                        }
                        else
                        {
                            weekNo = weekNo + 1;
                        }

                        //weekNo = (weekNo == 0 ? 1 : weekNo);
                        if (btnCreateMeeting.Text != "Update Makeup Session")
                        {
                            string cmdDuplicate = string.Empty;
                            cmdDuplicate = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + cancelledDate + "'";
                            int cntWeek = 0;
                            try
                            {
                                cntWeek = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdDuplicate));
                            }
                            catch
                            {
                            }
                            if (cntWeek <= 0)
                            {
                                coachClassText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Reason, CreateDate, CreatedBy)";
                                coachClassText += "select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo,'" + cancelledDate + "',Day,Time,Duration," + serNo + "," + weekNo + ",'Cancelled','" + ddlReason.SelectedValue + "',GetDate()," + Session["LoginID"].ToString() + " from calsignup where signupid=" + SignupID + "";
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);
                            }
                            else
                            {
                                coachClassText = "Update CoachClassCal set Status='Cancelled' where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + cancelledDate + "'";
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);
                            }

                            weekNoText = "select max(WeekNo) as WeekNo from CoachClassCal where MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and (Status='On' or Status='Substitute' or Status='Makeup')";
                            weekNo = 0;
                            dsWeek = new DataSet();
                            dsWeek = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, weekNoText);
                            if (null != dsWeek && dsWeek.Tables.Count > 0)
                            {
                                if (dsWeek.Tables[0].Rows.Count > 0)
                                {
                                    if (dsWeek.Tables[0].Rows[0]["WeekNo"].ToString() != "")
                                    {
                                        weekNo = Convert.ToInt32(dsWeek.Tables[0].Rows[0]["WeekNo"].ToString());
                                    }
                                    else
                                    {
                                        weekNo = 0;
                                    }
                                }
                            }

                            if (btnCreateMeeting.Text == "Update Makeup Session")
                            {

                            }
                            else
                            {
                                weekNo = weekNo + 1;
                            }

                            if (cntWeek > 0)
                            {

                                weekNo = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select distinct weekNo from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + cancelledDate + "'"));

                            }

                            coachClassText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status,Reason, CreateDate, CreatedBy)";
                            coachClassText += "select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo,'" + makeUpDate + "','" + Day + "','" + ddlMakeupTime.SelectedValue + "',Duration," + serNo + "," + weekNo + ",'Makeup','" + ddlReason.SelectedValue + "',GetDate()," + Session["LoginID"].ToString() + " from calsignup where signupid=" + SignupID + "";
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);

                        }
                        else
                        {
                            coachClassText = "update CoachClassCal set Date='" + makeUpDate + "', Day='" + Day + "', Time='" + ddlMakeupTime.SelectedValue + "' where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and EventID=13 and Status='Makeup' and WeekNo=" + weekNo + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + "";
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);
                        }

                        if (ddlProductGroup.SelectedValue != "42")
                        {

                            cmdText = "select CoachPaperId from CoachPapers where EventYear=" + ddYear.SelectedValue + " and EventId=13 and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId= " + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and WeekId=" + weekNo + " and DocType='Q'";
                            int iCoachPaperId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText));
                            if (iCoachPaperId > 0)
                            {

                                cmdText = "select coachRelID from coachreldates where EventYear=" + ddYear.SelectedValue + "  and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId= " + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and Session=" + ddlSession.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and CoachPaperID=" + iCoachPaperId + "";
                                int coachRelID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText));
                                if (coachRelID > 0)
                                {
                                    DateTime QRDate = Convert.ToDateTime(txtDate.Text);
                                    DateTime QDDate = Convert.ToDateTime(txtDate.Text).AddDays(5);

                                    DateTime ARDate = Convert.ToDateTime(txtDate.Text).AddDays(7);
                                    DateTime SRDate = Convert.ToDateTime(txtDate.Text).AddDays(-1);

                                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                                    string sqlCommand = "usp_Update_CoachRelDate";
                                    SqlParameter[] param = new SqlParameter[8];
                                    param[0] = new SqlParameter("@CoachRelID", iCoachPaperId);
                                    param[1] = new SqlParameter("@QReleaseDate", QRDate);
                                    param[2] = new SqlParameter("@QDeadlineDate", QDDate);
                                    param[3] = new SqlParameter("@AReleaseDate", ARDate);
                                    param[4] = new SqlParameter("@SReleaseDate", SRDate);
                                    param[5] = new SqlParameter("@SessionNO", ddlSession.SelectedValue);
                                    param[6] = new SqlParameter("@ModifiedBy", Int32.Parse(Session["LoginID"].ToString()));
                                    param[7] = new SqlParameter("@ModifyDate", System.DateTime.Now);
                                    SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                                }
                                else
                                {
                                    DateTime QRDate = Convert.ToDateTime(txtDate.Text);
                                    DateTime QDDate = Convert.ToDateTime(txtDate.Text).AddDays(5);

                                    DateTime ARDate = Convert.ToDateTime(txtDate.Text).AddDays(7);
                                    DateTime SRDate = Convert.ToDateTime(txtDate.Text).AddDays(-1);
                                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

                                    string sqlCommand = "usp_Insert_CoachRelDate";
                                    SqlParameter[] param = new SqlParameter[10];
                                    param[0] = new SqlParameter("@CoachPaperId", iCoachPaperId);
                                    param[1] = new SqlParameter("@Semester", "1");
                                    param[2] = new SqlParameter("@SessionNo", ddlSession.SelectedValue);
                                    param[3] = new SqlParameter("@QReleaseDate", QRDate);
                                    param[4] = new SqlParameter("@QDeadlineDate", QDDate);
                                    param[5] = new SqlParameter("@AReleaseDate", ARDate);
                                    param[6] = new SqlParameter("@SReleaseDate", SRDate);
                                    param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
                                    param[8] = new SqlParameter("@CreatedBy", Int32.Parse(Session["LoginID"].ToString()));
                                    param[9] = new SqlParameter("@MemberId", Int32.Parse(ddlCoach.SelectedItem.Value));

                                    SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                                }
                            }
                            else
                            {
                            }
                        }

                        if (btnCreateMeeting.Text == "Update Makeup Session")
                        {
                            lblSuccess.Text = "Makeup session updated successfully";
                            clear();
                        }
                        else
                        {
                            lblSuccess.Text = "Makeup session Created successfully";
                            clear();
                        }
                    }
                }

                fillMeetingGrid();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }

    public void updateZoomSession()
    {
        createZoomMeeting("2");
        if (hdnMeetingStatus.Value == "SUCCESS")
        {
            string cmdText = "";
            string meetingURL = hdnHostURL.Value;
            cmdText = "Update WebConfLog set StartDate='" + txtDate.Text + "', EndDate='" + txtDate.Text + "',BeginTime='" + ddlMakeupTime.SelectedValue + "', Duration=" + hdnDuration.Value + " where SessionKey=" + hdnSessionKey.Value + "";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                cmdText = "Update CalSignup set MakeUpDate='" + txtDate.Text + "',MakeUpTime='" + ddlMakeupTime.SelectedValue + "',MakeUpDuration='" + hdnDuration.Value + "' where SignupID=" + hdnSignupID.Value + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


                fillMeetingGrid();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            lblSuccess.Text = "Makeup session updated successfully";
        }
    }
    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        deleteMeeting();
    }
    public void deleteMeeting()
    {
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            urlParameter += "&id=" + hdnSessionKey.Value + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void populateRecClassDate()
    {

        string cmdText = string.Empty;
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term' and CS.Semester=CD.Semester) where  CS.MemberID=" + ddlCoach.SelectedValue + " and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.EventYear=" + ddYear.SelectedValue + " and CS.Accepted='Y' and CS.Level='" + ddlLevel.SelectedValue + "' and CS.SessionNo=" + ddlSession.SelectedValue + " and CD.Semester='" + ddlPhase.SelectedValue + "'";

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime dtStartDate = new DateTime();
                    DateTime dtEndDate = new DateTime();
                    DateTime dtTodayDate = DateTime.Now;
                    DateTime dtAdvanceDate = new DateTime();
                    DateTime dtPrevDate = new DateTime();


                    dtStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString());
                    string strStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("MM/dd/yyyy");
                    string EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("MM/dd/yyyy");
                    dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString());
                    string day = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("dddd");
                    string classDay = ds.Tables[0].Rows[0]["day"].ToString();
                    DateTime dtClassDate = Convert.ToDateTime(dtStartDate);
                    DDlSubDate.Items.Clear();
                    int x = 0;
                    int y = 0;
                    if (day == classDay)
                    {
                        int k = 0;
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");

                            //  string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");
                            if (dtClassDate > dtTodayDate.AddDays(-7))
                            {

                                k++;
                                dtTodayDate = dtTodayDate.AddDays(7);
                                dtAdvanceDate = dtTodayDate.AddDays(7);
                                string strAdvanceDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");
                                dtTodayDate = dtTodayDate.AddDays(-7);
                                dtPrevDate = dtTodayDate.AddDays(-7);
                                string strPrevDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");

                                //if (dtPrevDate >= dtClassDate && dtClassDate <= dtAdvanceDate)
                                //{
                                //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                                //}
                                //if (dtClassDate >= dtAdvanceDate && dtPrevDate <= dtClassDate)
                                //{
                                //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                                //}

                                DDlSubDate.Items.Insert(k - 1, new ListItem(date, date));
                            }
                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= 7; i++)
                        {
                            dtClassDate = dtClassDate.AddDays(1);
                            string strDay = Convert.ToDateTime(dtClassDate).ToString("dddd");
                            if (strDay == classDay)
                            {
                                break;
                            }
                        }
                        int j = 0;
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");
                            if (dtClassDate > dtTodayDate.AddDays(-7))
                            {

                                j++;
                                // string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");

                                // dtTodayDate = dtTodayDate.AddDays(7);
                                dtAdvanceDate = dtTodayDate.AddDays(7);
                                string strAdvanceDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");
                                //  dtTodayDate = dtTodayDate.AddDays(-7);
                                dtPrevDate = dtTodayDate.AddDays(-7);
                                string strPrevDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");

                                //if (dtPrevDate >= dtClassDate && dtClassDate <= dtAdvanceDate)
                                //{
                                DDlSubDate.Items.Insert(j - 1, new ListItem(date, date));
                                // }
                                //if (dtClassDate >= dtAdvanceDate && dtPrevDate <= dtClassDate)
                                //{
                                //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                                //}

                            }
                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    DDlSubDate.Items.Insert(0, new ListItem("Select", "0"));
                }
            }
        }
        catch (Exception ex)
        {
        }
    }




    public void timeOverLapValdiation()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey,VC.Duration from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join WebConfLog VC on(VC.MemberID=CS.MemberID and VC.ProductGroupID=CS.ProductGroupID and VC.ProductID=CS.ProductID and VC.Session=CS.SessionNo and VC.Level=CS.Level and VC.SessionKey=CS.MeetingKey) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.Level='" + ddlLevel.SelectedValue + "' and CS.Semester='" + ddlPhase.SelectedValue + "'";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            //string cmdDuration = "";
            //int iYr = SqlHelper.ExecuteScalar(Application["ConnectionString"], CommandType.Text, cmdText);
            string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");
            DataSet dsTime = new DataSet();
            string strStartTime = string.Empty;
            string strEndTime = string.Empty;
            int duration = 0;
            bool isScheduled = false;

            string vcUserID = string.Empty;
            string vcPwd = string.Empty;
            string userID = string.Empty;
            string pwd = string.Empty;
            string signupId = string.Empty;
            string coachId = string.Empty;


            if (null != ds && ds.Tables != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    userID = ds.Tables[0].Rows[0]["UserID"].ToString();
                    pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();
                    duration = Convert.ToInt32(ds.Tables[0].Rows[0]["Duration"].ToString());
                    signupId = ds.Tables[0].Rows[0]["SignupID"].ToString();
                    coachId = ds.Tables[0].Rows[0]["MemberID"].ToString();
                    strStartTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
                    DateTime dtStartTime = Convert.ToDateTime(strStartTime);
                    dtStartTime = dtStartTime.AddMinutes(-30);
                    strStartTime = dtStartTime.ToShortTimeString() + ":" + "00";
                    strEndTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
                    DateTime dtEndTime = Convert.ToDateTime(strEndTime);
                    dtEndTime = dtEndTime.AddMinutes(duration);
                    dtEndTime = dtEndTime.AddMinutes(30);
                    strEndTime = dtEndTime.ToShortTimeString();

                    string hostID = string.Empty;

                    hostID = checkAvailableVrooms(duration);
                    if (hostID != "")
                    {
                        string zoomUserID = hdnZoomUserID.Value;
                        string zoomPwd = hdnZoomPwd.Value;

                        hdnHostID.Value = hostID;

                        makeZoomSession(txtDate.Text, ddlMakeupTime.SelectedValue, signupId, coachId, day, strEndTime, duration);
                    }
                    else
                    {
                        lblerr.Text = "No Vroom is available at this time. Please choose different time.";
                    }
                }
                else
                {
                    lblerr.Text = "Training sessions not created yet for the selected coach.";
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    public string checkAvailableVrooms(int Duration)
    {

        string cmdtext = string.Empty;
        DataSet ds = new DataSet();
        string hostID = string.Empty;

        string strStartTime = ddlMakeupTime.SelectedValue;
        string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
        Duration = Duration + 30;
        string strEndTime = GetTimeFromString(ddlMakeupTime.SelectedValue, Duration).ToString();
        strStartTime = GetTimeFromStringSubtract(ddlMakeupTime.SelectedValue, -30).ToString();

        string substrEnd = strEndTime.Substring(0, 2);
        if (substrEnd == "00")
        {
            strEndTime = "23:59:00";
        }
        else
        {

        }
        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd from CalSignup CS inner join WebConfLog VC on (CS.Meetingkey=VC.SessionKey or CS.makeupMeetKey=VC.Sessionkey) inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=2016 and ((VC.BeginTime between '" + strStartTime + "' and '" + strEndTime + "') and VC.Day='" + strStartDay + "') or ((VC.EndTime between '" + strStartTime + "' and '" + strEndTime + "') and VC.Day='" + strStartDay + "') and VC.Day ='" + strStartDay + "'";

        try
        {
            string vRooms = string.Empty;


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        vRooms += dr["VRoom"].ToString() + ",";
                    }
                }
            }

            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=2016 and ((VC.BeginTime between '" + strStartTime + "' and '" + strEndTime + "') and VC.Day='" + strStartDay + "' and SessionType='Practise' and StartDate>= cast(GetDate() as date)) or ((VC.EndTime between '" + strStartTime + "' and '" + strEndTime + "') and VC.Day='" + strStartDay + "' and SessionType='Practise' and StartDate>= cast(GetDate() as date)) and (VC.Day ='" + strStartDay + "' and StartDate >= cast(GetDate() as date)) and SessionType='Practise'";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        vRooms += dr["VRoom"].ToString() + ",";
                    }
                }
            }


            if (!string.IsNullOrEmpty(vRooms))
            {
                vRooms = vRooms.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(vRooms))
            {
                cmdtext = "select HostID, USerID, PWD from VirtualRoomLookUp where Vroom not in (" + vRooms + ")";
            }
            else
            {
                cmdtext = "select HostID, USerID, PWD from VirtualRoomLookUp";
            }

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    hostID = ds.Tables[0].Rows[0]["HostID"].ToString();
                    hdnZoomUserID.Value = ds.Tables[0].Rows[0]["UserID"].ToString();
                    hdnZoomPwd.Value = ds.Tables[0].Rows[0]["PWD"].ToString();

                    hostID = ds.Tables[0].Rows[1]["HostID"].ToString();
                    hdnZoomUserID.Value = ds.Tables[0].Rows[1]["UserID"].ToString();
                    hdnZoomPwd.Value = ds.Tables[0].Rows[1]["PWD"].ToString();
                }
                else
                {
                    hostID = "";
                }
            }


        }
        catch (Exception ex)
        {
        }
        return hostID;
    }

    public void logerrors(string coachName, string status, string exception)
    {
        string filepath = string.Empty;
        try
        {

            // string[] fileArray = Directory.GetFiles(Server.MapPath("~/CoachingLog"));

            string pageName = Path.GetFileName(Request.Path);
            string filename = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt";

            string folderPath = Server.MapPath("~/CoachingLog");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            filepath = Server.MapPath("~/CoachingLog/" + filename);

            if (File.Exists(filepath))
            {
                using (StreamWriter stwriter = new StreamWriter(filepath, true))
                {
                    string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                    message += Environment.NewLine;
                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;
                    message += string.Format("Date & Time: {0}", DateTime.Now);
                    message += Environment.NewLine;
                    message += string.Format("Coach Name: {0}", coachName);
                    message += Environment.NewLine;
                    message += string.Format("Validation Status: {0}", status);
                    message += Environment.NewLine;

                    message += string.Format("Source: {0}", "Makeup Sessions");
                    message += Environment.NewLine;

                    message += Environment.NewLine;
                    message += string.Format("Exception: {0}", exception);
                    message += Environment.NewLine;

                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;

                    stwriter.WriteLine(message);
                }
            }
            else
            {
                StreamWriter stwriter = File.CreateText(filepath);
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Date & Time: {0}", DateTime.Now);
                message += Environment.NewLine;
                message += string.Format("Coach Name: {0}", coachName);
                message += Environment.NewLine;
                message += string.Format("Validation Status: {0}", status);
                message += Environment.NewLine;

                message += string.Format("Source: {0}", "Makeup Sessions");
                message += Environment.NewLine;

                message += Environment.NewLine;
                message += string.Format("Exception: {0}", exception);
                message += Environment.NewLine;

                message += "-----------------------------------------------------------";
                message += Environment.NewLine;

                stwriter.WriteLine(message);
                stwriter.Close();
            }
        }
        catch (Exception ex)
        {
            // lblerr.Text = ex.Message;
        }
    }

    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }

    }

    public void clear()
    {
        btnCreateMeeting.Text = "Create Makeup Session";
        txtDate.Text = "";
        ddlMakeupTime.SelectedValue = "";
        ddlReason.SelectedValue = "0";
        DDlSubDate.SelectedValue = "0";
    }

    public void listLiveMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "6";
            URL = "https://api.zoom.us/v1/meeting/live";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            //urlParameter += "&page_size=30";
            //urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void termianteMeeting(string sessionkey, string hostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "7";
            URL = "https://api.zoom.us/v1/meeting/end";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hostID + "";
            urlParameter += "&id=" + sessionkey + "";


            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSessionNo();
    }

    public void downloadTextFile()
    {
        string pageName = Path.GetFileName(Request.Path);
        string filename = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt";
        string filepath = Server.MapPath("~/CoachingLog/" + filename);
        FileInfo file = new FileInfo(filepath);

        // Checking if file exists
        if (file.Exists)
        {
            // Clear the content of the response
            Response.ClearContent();

            // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);

            // Add the file size into the response header
            Response.AddHeader("Content-Length", file.Length.ToString());

            // Set the ContentType
            Response.ContentType = ReturnExtension(file.Extension.ToLower());

            // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
            Response.TransmitFile(file.FullName);

            // End the response
            Response.End();
        }
    }
    private string ReturnExtension(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    protected void btnDowloadFIle_Click(object sender, EventArgs e)
    {
        downloadTextFile();
    }

    private Boolean InsertUpdateData(SqlCommand cmd)
    {
        String strConnString = System.Configuration.ConfigurationManager.AppSettings["DBConnection"];

        SqlConnection con = new SqlConnection(strConnString);
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            return true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            return false;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }
    }
    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }
}