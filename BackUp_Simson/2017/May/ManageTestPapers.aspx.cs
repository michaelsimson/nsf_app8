using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
//using System.Web.UI.Page;


#region " Class ManageTestPapers "
public partial class ManageTestPapers : System.Web.UI.Page
{
    #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion
    // **************** Roles Allowed to Access in this Page
    //  Role 1,  Role 9 National,  Role 31 and 32 have full access.
    //  Role 9 chapter and  Role 33 can only download test papers for the current year.  
    
    #region " Event Handlers "
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = 4240;
        //Session["RoleID"] = 2;
        //Session["LoggedIn"] = true;   
     
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
            Response.Redirect("login.aspx?entry=v");

        if (!IsPostBack)
        {
            gvSortDirection = "ASC";
            gvSortExpression = "ProductCode";
           
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "9") || (Session["RoleID"].ToString()== "93")))
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from Volunteer where RoleId=" + Session["RoleId"]+ " and [National]='Y' and MemberID = " + Session["LoginID"])) > 0)
                    hdnTechNational.Value = "Y";
                else
                    hdnTechNational.Value = "N";
            }

            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || ((Session["RoleId"].ToString() == "9") && (hdnTechNational.Value == "Y")) || (Session["RoleId"].ToString() == "32") || (Session["RoleId"].ToString() == "93") || (Session["RoleId"].ToString()=="30")))    //|| (Session["RoleId"].ToString() == "31") || (Session["RoleId"].ToString() == "32") 
            {
                GetDropDownChoice(dllfileChoice, true);
                dllfileChoice.Visible = true;
                if (Session["RoleId"].ToString() == "93" || Session["RoleId"].ToString() == "9")
                {
                    dllfileChoice.Items.RemoveAt(1);
                   
                    GetTestPapers_NatTechT();
                }
                else if (Session["RoleId"].ToString() == "30")
                {
                    dllfileChoice.Items.RemoveAt(1);
                     if (Session["RoleId"].ToString() == "30")
                    {
                          dllfileChoice.SelectedIndex = 1;
                          dllfileChoice.Enabled = false;
                         dllfileChoice_SelectedIndexChanged(dllfileChoice,new EventArgs());
                    }
                }

            }
            else if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "9") && (hdnTechNational.Value == "N")))//||(Session["RoleId"].ToString() == "93") //|| (Session["RoleId"].ToString() == "33")
            {
                if (Request.QueryString["id"] != null)
                {
                    SqlDataReader Reader = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString (), CommandType.Text, "select V.ChapterID, Ch.ChapterCode from Volunteer V inner Join Chapter Ch ON V.ChapterID= Ch.ChapterID  where  V. MemberID = " + Session["LoginID"] + " AND V.VolunteerId =" +Request.QueryString["id"].ToString() + "");
                    while(Reader.Read())
                    {
                        lblChapter.Text = Reader["ChapterCode"].ToString ();
                        lblChapter.Visible = true;
                        hdnChapterID.Value = Reader["ChapterID"].ToString ();
                    } 
                }

                if (hdnChapterID.Value.Length > 0)
                {
                    gvTestPapers.Columns[10].Visible = true;
                    int weekid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
                    int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);
                    
                    if (Count == 0)
                    {
                        LblexamRecErr.Text = "No records to Display.";// You can download only 10 days before contest.";
                    }
                    else
                    {
                        LblexamRecErr.Text = "";
                    }
                    gvTestPapers.Columns[10].Visible = false;
                    Panel3.Visible = true;  
                }
                else
                    // LoadSearchAndDownloadPanels(false); Commented and updated on 08-12-2014 to Load product groups and download options based on Event [Finals or Chapter] Select 
                LoadEventFlr(false);
            }
            else
            {
                Panel2.Visible = false;
                Panel3.Visible = false;
                lblNoPermission.Visible = true;
                lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
            }
        }
    }
    private void LoadReplYear()
    {
        String SQLStr = "SELECT MAX(ContestYear) + 1 as ContestYear  FROM TestPapers WHERE EventID in (" + ddlEvent.SelectedValue + ") and Doctype='Instr' ";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlReplYear.DataSource = ds;
            ddlReplYear.DataTextField = "ContestYear";
            ddlReplYear.DataValueField = "ContestYear";
            ddlReplYear.DataBind();
        }

        if (Convert.ToInt32(ddlReplYear.SelectedValue) > DateTime.Now.Year)
        {
            //divRepl.Visible = false;
            btnReplicate.Enabled = false;
        }
        else if (Convert.ToInt32(ddlReplYear.SelectedValue) == DateTime.Now.Year)
        {
            divRepl.Visible = true;
            btnReplicate.Enabled = true;
        }
    
    }
    private void LoadEvent()
    {
        Panel1.Visible = false;// true;
        //divRepl.Visible = false;
        btnReplicate.Enabled = false;
        Panel2.Visible = false;
        Panel3.Visible = false;// true;

        ddlEvent.Visible = true;
        ddlFlrEvent.Visible = false;

        String SQLStr = "SELECT Distinct EventId,Name FROM Event WHERE EventID in (1,2)";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlEvent.DataSource = ds;
            ddlEvent.DataTextField = "Name";
            ddlEvent.DataValueField = "EventId";
            ddlEvent.DataBind();
        }
        ddlEvent.Items.Insert(0, "Select Event");
        ddlEvent.SelectedIndex = 0;
    }
    private void LoadEventFlr(bool Flag)
    {
        Panel1.Visible = false;
        //divRepl.Visible = false;
        btnReplicate.Enabled = false;
        Panel2.Visible = false;// Flag;// Convert.ToBoolean(Session["EventFlag"]);
        Panel3.Visible = false; // true;

        ddlFlrEvent.Visible = true;
        ddlEvent.Visible = false;

        Session["EventFlag"] = Flag;
        String SQLStr = "SELECT Distinct EventId,Name FROM Event WHERE EventID in (1,2)";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlFlrEvent.DataSource = ds;
            ddlFlrEvent.DataTextField = "Name";
            ddlFlrEvent.DataValueField = "EventId";
            ddlFlrEvent.DataBind();
        }
        ddlFlrEvent.Items.Insert(0, "Select Event");
        ddlFlrEvent.SelectedIndex = 0;
   }

    private void GetTestPapers_NatTechT()
    {
            LblexamRecErr.Text = "";
            String StrSQL = " SELECT DISTINCT TP.TestPaperId, TP.ContestYear, TP.ProductId, TP.ProductCode, TP.ProductGroupId, TP.ProductGroupCode, TP.EventID, TP.WeekId, TP.SetNum, TP.NoOfContestants," ;
            StrSQL = StrSQL  + " TP.TestFileName,TP.DocType, TP.Description, TP.Password, TP.CreateDate, TP.CreatedBy, TP.ModifyDate, TP.ModifiedBy FROM ";
            StrSQL = StrSQL + " TestPapers TP INNER JOIn NatTechTeam N on N.ProductGroupCode = TP.ProductGroupCode Where TP.ContestYear = Year(GETDATE())  and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]);
            StrSQL = StrSQL + " order by TP.WeekId,TP.ProductID, TP.DocType, TP.NoOfContestants ";

            //Response.Write(StrSQL);
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Panel3.Visible = true;
                gvTestPapers.DataSource = ds;
                gvTestPapers.DataBind();
            }
            else 
            {
                LblexamRecErr.Text = "No records to Display.";// You can download only 10 days before contest.";
            }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
    }
    protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
    }
    protected void ddlNoOfContestants_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMessage.Text = "";
    }
    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblReplErr.Text = "";
        lblSuccessMsg.Text = "";
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {
           
            //LoadSearchAndDownloadPanels(true);//Commented and updated on 08-12-2014 to Load product groups and download options based on Event [Finals or Chapter] Select 
                LoadEventFlr(true);
            //Loads the Grid.....
             //GetTestPapers(m_objETP);
            if (Convert.ToInt32(Session["RoleID"]) == 9 && hdnTechNational.Value == "N") // || (Convert.ToInt32(Session["RoleID"]) == 93))
            {
                int weekid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
                int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);
               
                 if (Count == 0)
                 {
                         LblexamRecErr.Text = "No records to Display.";// You can download only 10 days before contest.";
                 }
                 else
                 {
                         LblexamRecErr.Text = "";
                         gvTestPapers.Columns[10].Visible = false;
                         Panel3.Visible = true;
                 }
            }
            else if (Convert.ToInt32(Session["RoleID"]) == 93)
            {
                GetTestPapers_NatTechT();
            }
            else
            {
                GetTestPapers(m_objETP,false);
            }

        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
            // LoadUploadPanel(); Commented and updated on 08-12-2014 to Load product groups and download options based on Event [Finals or Chapter] Select 
                 LoadEvent();
          

            //GetTestPapers(m_objETP);
            if (Convert.ToInt32(Session["RoleID"]) == 9 && hdnTechNational.Value == "N")// || (Convert.ToInt32(Session["RoleID"]) == 93))
            {
                int weekid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
                int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);

                if (Count == 0)
                {
                    LblexamRecErr.Text = "No records to Display.";// You can download only 10 days before contest.";
                }
                else
                {
                    LblexamRecErr.Text = "";
                    gvTestPapers.Columns[10].Visible = false;
                    Panel3.Visible = true;
                }
            }
            else if (Convert.ToInt32(Session["RoleID"]) == 93)
            { 
                    GetTestPapers_NatTechT();
            }
            else
            {
                GetTestPapers(m_objETP,false);
            }
            
        }
    }

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        int value = 0;
        lblMessage.Text = "";
        if (ddlContestYear.Items.Count == 0)
        {
            lblMessage.Text = "Contest Year should not be blank";
            return;
        }

        if (ddlDocType.SelectedItem.Text == "[Select Doc Type]")
        {
            lblMessage.Text = "Please select document type";
            return;
        }

        if (ddlProductGroup.Items.Count>0)
        {
            if (ddlProductGroup.SelectedItem.Text == "[Select Product Group]")
            {
                lblMessage.Text = "Please select Product Group";
                return;
            }
        }
        else if (ddlProductGroup.Items.Count == 0)
        {
            lblMessage.Text = "Product Group should not be blank";
            return;
        }

        if (ddlNoOfContestants.Items.Count > 0)
        {
            if (ddlNoOfContestants.SelectedItem.Text == "[Select #Of Children]")
            {
                lblMessage.Text = "Please select #Of Contestant";
                return;
            }            
        }
        else if (ddlNoOfContestants.Items.Count == 0)
        {
            lblMessage.Text = "#Of Contestant should not be blank";
            return;
        }

      if (ddlProduct.Items.Count > 0)
            {
                if (ddlProduct.SelectedItem.Text == "[Select Product]")
                {
                    lblMessage.Text = "Please select product";
                    return;
                }            
            }
      else if (ddlProduct.Items.Count == 0)
      {
          lblMessage.Text = "Product should not be blank";
          return;
      }

      if (ddlWeek.Items.Count > 0)
      {
          if (ddlWeek.SelectedItem.Text == "[Select Week]")
          {
              lblMessage.Text = "Please select week";
              return;
          }
      }
      else if (ddlWeek.Items.Count==0)
      {
          lblMessage.Text = "Week should not be blank";
          return;
      }

        if (FileUpLoad1.HasFile)
        {
            
            EntityTestPaper objETP = new EntityTestPaper();

            objETP.ProductId = int.Parse(ddlProduct.SelectedValue);
            objETP.ProductCode = ddlProduct.SelectedItem.Text;
            objETP.ProductGroupId = int.Parse(ddlProductGroup.SelectedValue);
            objETP.ProductGroupCode = GetProductGroupCode(ddlProductGroup.SelectedItem.Text);
            objETP.EventCode = GetEventCode(ddlProductGroup.SelectedItem.Text);
            objETP.WeekId = int.Parse(ddlWeek.SelectedItem.Value);
            objETP.SetNum = int.Parse(ddlSet.SelectedValue);
            objETP.NoOfContestants = int.Parse(ddlNoOfContestants.SelectedValue);
            objETP.ContestYear = ddlContestYear.SelectedItem.Text;
            objETP.DocType = ddlDocType.SelectedItem.Value;
            objETP.TestFileName = FileUpLoad1.FileName;
            objETP.Description = txtDescription.Text;
            objETP.Password = TxtPassword.Text;
            objETP.CreateDate = System.DateTime.Now;
            objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
            

            if (ValidateFileName(objETP))
            {
                try
                {
                   value =  TestPapers_Insert(objETP);
                    //string ftpPath = String.Format("{0}/{1}",
                    //    ConfigurationSettings.AppSettings["FTPTestPapersPath"],
                    //    FileUpLoad1.FileName);

                    //uploadFileUsingFTP(ftpPath, FileUpLoad1.FileContent);
                   if (value != 0)
                   {
                       SaveFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], FileUpLoad1);

                       lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;
                       GetTestPapers(m_objETP,true);
                   }
                   else
                   {
                       //lblMessage.Text = "You are inserting duplicate record";
                       FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString ()),"Temp_"+ FileUpLoad1.FileName.ToString ()));
                       hdnTempFileName.Value = "Temp_" + FileUpLoad1.FileName.ToString();
                       Panel1.Visible = false;
                       //divRepl.Visible = false;
                       btnReplicate.Enabled = false;
                       Panel5.Visible = true; 
                       dllfileChoice.Enabled = false ;
                   }
                }
                catch(Exception err)
                {
                    lblMessage.Text = err.Message;
                }
            }
        }
        else
        {
            lblMessage.Text = "No File Uploaded.";
        }
    }

     protected void btnYes_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        if (hdnTempFileName.Value.Length > 0)
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update testpapers SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate()  where  testFileName='" + hdnTempFileName.Value.Replace("Temp_", "").ToString() + "'");
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value), Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), true);
            Panel5.Visible = false;
            Panel1.Visible = true;
            divRepl.Visible = true;
            btnReplicate.Enabled = true;
            lblMessage.Text = "File Replaced : " + hdnTempFileName.Value.Replace("Temp_", "").ToString();
        }
        else
        {
            Panel5.Visible = false;
            Panel1.Visible = true;
            divRepl.Visible = true;
 btnReplicate.Enabled = true;
        }
    }

     protected void btnNo_Click(object sender, EventArgs e)
     {
         dllfileChoice.Enabled = true;
         if (hdnTempFileName.Value.Length > 0)
                 File.Delete (string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString()), hdnTempFileName.Value));
         Panel5.Visible = false;
         Panel1.Visible = true;
         divRepl.Visible = true;
         btnReplicate.Enabled = true;
     }
    //public void gvTestPapers_RowDataBound(object sender, GridViewRowEventArgs e) 
    //{
 
    //}
  
    protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        LblexamRecErr.Text = String.Empty;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                EntityTestPaper objETP = new EntityTestPaper();
                index = int.Parse((string)e.CommandArgument);
                objETP.TestPaperId = int.Parse(gvTestPapers.Rows[index].Cells[0].Text);
                objETP.TestFileName = gvTestPapers.Rows[index].Cells[6].Text;
                objETP.DocType = gvTestPapers.Rows[index].Cells[10].Text;
                if (e.CommandName == "DeleteTestPaper")
                {
                    DeleteTestPaper(objETP);
                    //lblMessage.Text = DeleteTestPaperFromFTPSite(objETP);
                    DeleteFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                    GetTestPapers(m_objETP,false);
                }
                else if (e.CommandName == "Download")
                {
                     //DownloadFileFromFTP(objETP);
                    //lblMessage.Text = string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"]), objETP.TestFileName);

                    if ((Session["RoleId"] != null) && (((Session["RoleId"].ToString() == "9") && (hdnTechNational.Value == "N")) || (Session["RoleId"].ToString() == "33")))
                    {
                        try
                        {
                           if ((objETP.DocType != "Instr"))
                            {
                                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(c.contestID) from contest C Inner Join TestPapers T ON C.ProductId = T.ProductId AND C.Contest_Year = T.ContestYear where C.InstrDownFlag is Not Null and  C.ExamRecID =  " + int.Parse(Session["LoginID"].ToString()) + " and C.NSFChapterID = " + hdnChapterID.Value + " and T.TestPaperId = " + objETP.TestPaperId + "")) > 0)
                                {
                                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                                }
                                else
                                    LblexamRecErr.Text = "Please download Instruction file before downloading test Papers";
                            }
                            else
                            {
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "update C set C.InstrDownFlag = 1  from contest C Inner Join TestPapers T ON C.ProductGroupId = T.ProductGroupId AND C.Contest_Year = T.ContestYear where C.InstrDownFlag is  Null and C.ExamRecID =  " + int.Parse(Session["LoginID"].ToString()) + " and C.NSFChapterID = " + hdnChapterID.Value + " and T.TestPaperId = " + objETP.TestPaperId + "");
                                DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                            }
                        }
                        catch (Exception ex)
                        {
                             //Response.Write(ex.ToString());
                        }

                    }
                        
                    else
                        DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"], objETP);
                        
                    }
                
            }
        }
    }

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        if (e.SortExpression == "CreateDate")
        {
            if (this.gvSortDirection == "ASC")
            {
                e.SortExpression = "YEAR DESC, MONTH DESC, DAY DESC";
                this.gvSortDirection = "DESC";
            }
            else
            {
                e.SortExpression = "YEAR ASC, MONTH ASC, DAY ASC";
                this.gvSortDirection = "ASC";
            }
        }

        else if (e.SortExpression == "ProductId")
        {
            if(this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {

                this.gvSortDirection = "ASC";
            }
            
            this.gvSortExpression = "ProductId";

        }
        else
        {
            if (this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {
                this.gvSortDirection = "ASC";
            }
        }
        if ((Session["RoleId"] != null) && ((((Session["RoleId"].ToString() == "9"))&& (hdnTechNational.Value == "N")) ))  //|| (Session["RoleId"].ToString() == "33")
        {
            LoadRecordsForExamRcvr();
        }
        else if(Session["RoleId"].ToString() == "93")
        {
            GetTestPapers_NatTechT();
        }
        else
        {
            GetTestPapers(m_objETP,false);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
       
        LblexamRecErr.Text = "";
        m_objETP.ProductId = int.Parse(ddlFlrProduct.SelectedValue);
        m_objETP.ProductGroupId = int.Parse(ddlFlrProductGroup.SelectedValue);
        m_objETP.WeekId = int.Parse(ddlFlrWeek.SelectedItem.Value);
        m_objETP.SetNum = int.Parse(ddlFlrSet.SelectedValue);
        m_objETP.NoOfContestants = int.Parse(ddlFlrNoOfContestants.SelectedValue);
        m_objETP.Description = tbxFlrDescription.Text;
        m_objETP.TestFileName = tbxFlrTestFileName.Text;
        m_objETP.EventId = int.Parse(ddlFlrEvent.SelectedValue);
        GetTestPapers(m_objETP,true);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        lblPrdError.Text = "";
        LblexamRecErr.Text = "";
        ddlFlrProduct.SelectedIndex = 0;
        ddlFlrProductGroup.SelectedIndex = 0;
        ddlFlrWeek.SelectedIndex = 0;
        ddlFlrSet.SelectedIndex = 0;
        ddlFlrNoOfContestants.SelectedIndex = 0;
        tbxFlrDescription.Text = String.Empty;
        tbxFlrTestFileName.Text = string.Empty;
       // GetTestPapers(m_objETP);

        if (Convert.ToInt32(Session["RoleID"]) == 9 && hdnTechNational.Value == "N") //|| (Convert.ToInt32(Session["RoleID"]) == 93)
        {
            int weekid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select top 1 WeekId From WeekCalendar where (GETDATE() <= Satday1 Or GETDATE() <= Sunday2) order by Satday1"));
            int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), weekid);

            if (Count == 0)
            {
                LblexamRecErr.Text = "No records to Display.";// You can download only 10 days before contest.";
            }
            else
            {
                LblexamRecErr.Text = "";
                gvTestPapers.Columns[10].Visible = false;
                Panel3.Visible = true;
            }
        }
        else if (Convert.ToInt32(Session["RoleID"]) == 93)
        {
           GetTestPapers_NatTechT();
        }
        else
        {
            GetTestPapers(m_objETP,false);
        }
    }

    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
    }

    #endregion

    #region " Private Methods - Data Access Layer "
    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Insert";
        SqlParameter[] param = new SqlParameter[17];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        param[4] = new SqlParameter("@EventCode", objETP.EventCode);
        param[5] = new SqlParameter("@WeekId", objETP.WeekId);
        param[6] = new SqlParameter("@SetNum", objETP.SetNum);
        param[7] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants == -1 ? 0 : objETP.NoOfContestants);
        param[8] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[9] = new SqlParameter("@Description", objETP.Description);
        param[10] = new SqlParameter("@Password", objETP.Password);
        param[11] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[12] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[13] = new SqlParameter("@ModifyDate", objETP.ModifyDate);
        param[14] = new SqlParameter("@ModifiedBy", objETP.ModifiedBy);
        param[15] = new SqlParameter("@DocType", objETP.DocType);
        param[16] = new SqlParameter("@ContestYear", objETP.ContestYear);
       
        value = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param);
        if(value == null)
        {
            return -1;
           // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem,int EventID)
    {
        try
        {
            DataSet dsproductgroup ;
            String  EventID_str ;
            if (EventID > 0) EventID_str = EventID.ToString(); else EventID_str = "1,2";

            if (Convert.ToInt32(Session["RoleId"]) == 93)
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(" + EventID_str + ") and A.ProductGroupCode in (Select distinct C.ProductGroupCode From ContestCategory C Left Join NatTechTeam N ON N.ProductGroupCode = C.ProductGroupCode where (C.RegionalStatus = 'Active' OR C.NationalFinalsStatus = 'Active') AND C.ContestYear = " + System.DateTime.Now.Year.ToString() + " and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]) + ") Order by A.EventCode");
            }
            else if (Convert.ToInt32(Session["RoleId"]) == 30)
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(" + EventID_str + ") and A.ProductGroupCode in (Select distinct ProductGroupCode From Volunteer where  MemberId ="+ Session["LoginId"] +" and RoleId=30) Order by A.EventCode");
            }
            else
            {
                dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(" + EventID_str + ") and A.ProductGroupCode in (Select distinct ProductGroupCode From ContestCategory where (RegionalStatus = 'Active' OR NationalFinalsStatus = 'Active') AND ContestYear = " + System.DateTime.Now.Year.ToString() + ") Order by A.EventCode");
            }

            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "EventCodeAndProductGroupCode";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
            if (Convert.ToInt32(Session["RoleId"]) == 30)
            {
                ddlObject.Enabled = true;
                if (ddlObject.Items.Count == 2)
                {
                    ddlObject.SelectedIndex = 1;
                    ddlFlrProductGroup_SelectedIndexChanged(ddlObject, new EventArgs());
                    ddlObject.Enabled = false;
                }
            }

        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetFlrContextYear()
    {
        ddlFlrYear.Items.Clear();
        if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "32"))
        {
            int Minyear;
            try
            {
                Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from Testpapers"));
                int year = DateTime.Now.Year;
                int j = 0;
                for (int i = Minyear; i <= year; i++)
                {
                    ddlFlrYear.Items.Insert(j, new ListItem(i.ToString()));
                    j = j + 1;
                }
                ddlFlrYear.SelectedIndex = ddlFlrYear.Items.IndexOf(ddlFlrYear.Items.FindByText(year.ToString()));
            }
            catch (SqlException se)
            {
                lblMessage.Text = se.Message;
                return;
            }

        }
        else if (Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "30")
        {
            int year = DateTime.Now.Year;
            ddlFlrYear.Items.Insert(0, new ListItem((year - 1).ToString()));
            ddlFlrYear.Items.Insert(1, new ListItem(year.ToString()));
            ddlFlrYear.SelectedIndex = 1;
        }
        else
        {
            ddlFlrYear.Items.Insert(0, new ListItem(DateTime.Now.Year.ToString()));
            ddlFlrYear.Enabled = false;
        }
    }
    private void GetContextYear(DropDownList ddlContestYear, bool blnCreateEmptyItem)
    {
        object year;
        try
        {
            ddlContestYear.Items.Clear();
            year = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select  max (EventYear) EventYear from Event");
         if (blnCreateEmptyItem)
            {
       
             ddlContestYear.Items.Insert(0, new ListItem(year.ToString()));
           
            }
         }

        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetWeek(DropDownList ddlWeek, bool blnCreateEmptyItem,int EventID,int Year)
    {
        try
        {
            DataSet dsWeek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,  "Select WeekID, (Convert(varchar(14),SatDay1,106) +  ' - ' + Convert(varchar(14),sunDay2,106)) as Week  from weekCalendar Where (Year(SatDay1)=Year(GETDATE()) Or Year(sunDay2)= YEAR(GETDATE())) and EventID= " + EventID);
             
            ddlWeek.DataSource = dsWeek;
            ddlWeek.DataTextField = "Week";
            ddlWeek.DataValueField = "WeekID";
            ddlWeek.DataBind();
            if (blnCreateEmptyItem) //(ddlWeek.Items.Count > 1)
            {
                ddlWeek.Items.Insert(0, new ListItem("[Select Week]", "-1"));
                ddlWeek.Enabled = true;

            }
            if( Year < DateTime.Now.Year)
            {
                ddlWeek.SelectedIndex = 0;
                ddlWeek.Enabled = false;
            }
            else
            {
                if (ddlWeek.Items.Count == 2)
                {
                    ddlWeek.SelectedIndex = 1;
                    ddlWeek.Enabled = false;
                }
                else if(ddlWeek.Items.Count >2)
                {
                    ddlWeek.SelectedIndex = 0;
                    ddlWeek.Enabled = true;
                }
           }
        }
        catch(SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
          
            if (ProductGroupId != -1)
            {
                ddlObject.Items.Clear();
                DataSet dsProduct ;
                if (Session["RoleId"].ToString() == "30")
                {
                    dsProduct= SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct ProductId, ProductCode from Volunteer Where  MemberId=" + Session["LoginId"] + " and RoleId=30 and ProductId is not null ");
                    if (dsProduct.Tables[0].Rows.Count == 0)
                    {
                        dsProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct ProductId, ProductCode FROM [Product] A Where A.EventId ="+ ddlFlrEvent.SelectedValue  +" and A.ProductGroupCode in (Select distinct ProductGroupCode From Volunteer where  MemberId =" + Session["LoginId"] + " and RoleId=30 and productgroupcode is not null) and Status='O' Order by A.ProductCode");
                    }                     
                }
                else
                {
                    SqlParameter[] param = new SqlParameter[1];
                    param[0] = new SqlParameter("@ProductGroupId", ProductGroupId);
                    dsProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "Product_GetByProductGroupId", param);                   
                }
                ddlObject.DataSource = dsProduct;
                ddlObject.DataTextField = "ProductCode";
                ddlObject.DataValueField = "ProductId";
                ddlObject.DataBind();               
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            ddlObject.SelectedIndex = 0;

            if (Session["RoleId"].ToString() == "30")
            {
                ddlObject.Enabled = true;
                if (ddlObject.Items.Count == 2)
                {
                    ddlObject.SelectedIndex = 1;
                    ddlObject.Enabled = false;
                
                }
            }
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void PopulateContestants(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 5, 10, 15, 20, 25, 30, 35 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select #Of Children]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem,int EventID,int Year)
    {
        //int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        //ddlObject.DataSource = Nos;
        //ddlObject.DataBind();
        //if (blnCreateEmptyItem)
        //{
        //    ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
        //}
        // ddlObject.SelectedIndex = 0;

        /******Commented and updated on 08-14-2014 to displayed SetNo from Week calendar table [Note: Set# and WeekId are the same after 2010]*************************************************************************************************************/
        try
        {
            String StrSQL = "";

            if (Year == 0)
            { 
                 StrSQL ="Select WeekID as SetNum from weekCalendar Where (Year(SatDay1)=Year(GETDATE()) Or Year(sunDay2)= YEAR(GETDATE())) and EventID= " + EventID ;
            }
            else if (Year>0 || Year  < DateTime.Now.Year )
            {
                 StrSQL ="select distinct(SetNum) from TestPapers where ContestYear=" + Year + " and eventID= " + EventID ;
            }
            DataSet dsWeek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
         
            ddlObject.DataSource = dsWeek;
            ddlObject.DataTextField = "SetNum";
            ddlObject.DataValueField = "SetNum";
            ddlObject.DataBind();


            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
            if (ddlObject.Items.Count == 2)
            {
                ddlObject.SelectedIndex = 1;
                ddlObject.Enabled = false;
            }
            else if (ddlObject.Items.Count > 2)
            {
                ddlObject.SelectedIndex = 0;
                ddlObject.Enabled = true;
            }
            //else
            //{ ddlObject.SelectedIndex = 0; }
           
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }

    }


    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        string[] Choice = { "UploadScreen", "DownloadScreen" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
        }
        ddlObject.SelectedIndex = 0;

    }
    private void GetTestPapers(EntityTestPaper objETP,Boolean TestFlag)
    {
        lblSearchErr.Text = "";
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[13];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        if (ddlFlrYear.SelectedValue == DateTime.Now.Year.ToString ())
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        else
            param[4] = new SqlParameter("@WeekId", -1);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[11] = new SqlParameter("@ContestYear", ddlFlrYear.SelectedValue);
        param[12] = new SqlParameter("@EventID", objETP.EventId);

        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0 && TestFlag == true)
            lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
        else
            lblSearchErr.Text = "";// string.Empty;
        DataView dv = new DataView(dt);
        //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

        gvTestPapers.DataSource = dv;
        gvTestPapers.DataBind();

        //if (Convert.ToInt32(Session["RoleID"]) == 93) //Added to Show only the eancbled productgroups for NatTechT(93)
        //{
        //    DataSet dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] FROM dbo.[Product] A Where A.EventID in(1,2) and A.ProductGroupCode in (Select distinct C.ProductGroupCode From ContestCategory C Left Join NatTechTeam N ON N.ProductGroupCode = C.ProductGroupCode where (C.RegionalStatus = 'Active' OR C.NationalFinalsStatus = 'Active') AND C.ContestYear = " + System.DateTime.Now.Year.ToString() + " and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]) + ") ");
        //    String ProductGroups = "";
        //    for (int j = 0; j < dsproductgroup.Tables[0].Rows.Count; j++)
        //    {
        //        ProductGroups = ProductGroups + dsproductgroup.Tables[0].Rows[j]["ProductGroupCode"] + ",";
        //    }
        //    ProductGroups = ProductGroups.TrimEnd(',');
        //    if (gvTestPapers.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < gvTestPapers.Rows.Count; i++)
        //        {
        //            //for (int j = 0; j < dsproductgroup.Tables[0].Rows.Count;j++ )
        //            if (!ProductGroups.Contains(gvTestPapers.Rows[i].Cells[2].Text))
        //            {
        //                gvTestPapers.Rows[i].Visible = false;
        //            }
        //        }
        //    }
        //}
               
    }
    private int GetTestPapers(int memberid, int roleid, string contestYear, int weekID)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_GetTestPapersforDownload";
        string SQLContest ="";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@memberid", memberid);
        param[1] = new SqlParameter("@contestyear", contestYear);
        param[2] = new SqlParameter("@WeekId", weekID);
        param[3] = new SqlParameter("@NoOfDaysBeforeContestDate", 12);
        param[4] = new SqlParameter("@Chapter", hdnChapterID.Value);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            DataTable dtNew = dt.Clone();
            String CurrTestFileName;
            String CurrTestFilePrefix;
            String PrevTestFilePrefix = "";
            try
            {
                //Following loop is to filter out the Test Papers, which belong to the same product code and same set but with 
                //different number of children. For example: If there are Set2_2008_SB_SSB_TestP_15.zip, Set2_2008_SB_SSB_TestP_20.zip, Set2_2008_SB_SSB_TestP_25.zip records exist then
                //this loop filters keeps only Set2_2008_SB_SSB_TestP_15.zip and filters out the other two.
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CurrTestFileName = dt.Rows[i].ItemArray[10].ToString();
                    CurrTestFilePrefix = CurrTestFileName.Substring(0, CurrTestFileName.Length - 6);
                    if (!(PrevTestFilePrefix.ToLower().Equals(CurrTestFilePrefix.ToLower())))
                    {
                        PrevTestFilePrefix = CurrTestFilePrefix;
                        DataRow dr = dt.Rows[i];
                        dtNew.ImportRow(dr);
                    }
                }

                DataRow[] drExistPrd = dtNew.Select("DocType='TestP'");
                 string productCode = "";
                 if (drExistPrd.Length > 0)
                 {
                     for (int icnt = 0; icnt < drExistPrd.Length; icnt++)
                     {
                         productCode = productCode + "'" + drExistPrd[icnt]["ProductCode"].ToString() + "',";
                     }
                     productCode = productCode.Substring(0, productCode.LastIndexOf(","));
                 }
                 else
                     productCode = "''";
                SQLContest = " Select COUNT(*) from TestPapers where ProductCode in (Select Distinct c.ProductCode From Contestant c Inner Join Contest cn on cn.Contest_year=c.ContestYear and Cn.ContestId=c.Contestcode ";
                SQLContest = SQLContest + " where c.parentid=" + memberid + " and c.ContestYear= " + contestYear + " and cn.contestdate - 12 <= Convert(datetime, Convert(int, GetDate())) and ExamRecID=" + memberid + ") and ContestYear=" + contestYear;
                if (weekID > 0)
                {
                    SQLContest = SQLContest + " and WeekId=" + weekID;
                }
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(),CommandType.Text,SQLContest))>0)
                {
                    lblPrdError.Text = "Your children are in the same contest(s).  So you cannot receive the other test papers.";
                }
                // Validate contest date and contestant number 
                string strCmd = "SELECT DISTINCT TP.TestPaperId, C.ContestDate ContestDate,(select Ph2Rooms from contest where ProductId=TP.ProductId and NsfChapterID = " + hdnChapterID.Value + " and ExamRecID = " + memberid + " and Contest_Year = " + contestYear + " ) ph2rooms, TP.ContestYear,TP.DocType,TP.NoOfContestants TPContestant,(select count(childnumber) from contestant ctx where ctx.contestyear=" + contestYear + " and ";
                strCmd = strCmd + "  ctx.chapterid=" + hdnChapterID.Value + " and ctx.ProductId=TP.ProductId and ctx.paymentreference !='') TotalContestant,  TP.ProductId, TP.ProductCode, TP.ProductGroupId, TP.ProductGroupCode, TP.EventID, TP.WeekId, TP.SetNum, ";
                strCmd = strCmd + " TP.TestFileName,TP.DocType, TP.Description, TP.Password, TP.CreateDate, TP.CreatedBy, TP.ModifyDate, TP.ModifiedBy FROM TestPapers AS TP INNER JOIN Contest AS C ON TP.ContestYear = C.Contest_Year and c.NSFChapterID=" + hdnChapterID.Value + " and c.productcode=tp.productcode ";// INNER JOIN WeekCalendar AS W ON C.ContestDate <= W.SunDay2 AND C.ContestDate > W.SatDay1 - 2 ";
                strCmd = strCmd + " WHERE TP.ContestYear = " + contestYear + " and C.NsfChapterID = ISNULL(" + hdnChapterID.Value + ", C.NsfChapterID) and((TP.DocType = 'TestP' ";// AND TP.WeekId = W.WeekId ";
                strCmd = strCmd + " and TP.ProductID in (select ProductID from contest where NsfChapterID = c.NsfChapterID  and ExamRecID = " + memberid + " and Contest_Year = " + contestYear + ")) and c.NsfChapterID in (select NsfChapterID from contest where ExamRecID = " + memberid + " and Contest_Year = " + contestYear + " ) ";
                 strCmd = strCmd + " and TP.WeekId = ISNULL(" + weekID + ", TP.WeekId)  and TP.ProductCode not IN (" + productCode + ") ) order by TP.ProductID,TP.NoOfContestants desc ";
                string prodcode;
                Hashtable TP = new Hashtable();
                int TotalContestant, ph2Room, TPContestant;
                DateTime ContestDate;
                DateTime dtCurrent = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                string strTP = " The test paper cannot be given due to high volume.  Need to split into more rooms. Inform your CC to increase #of rooms at the link Schedule TCs.";
                DataSet dsMissingTP = SqlHelper.ExecuteDataset(conn, CommandType.Text, strCmd);
                ArrayList arCheckedProduct=new ArrayList() ;
                if (dsMissingTP.Tables[0].Rows.Count > 0)
                {
                    DataTable dtTP = dsMissingTP.Tables[0];
                    for (int i = 0; i < dtTP.Rows.Count; i++)
                    {
                        DataRow[] drExist = ds.Tables[0].Select("TestPaperId = " + dtTP.Rows[i]["TestPaperId"].ToString());
                        if (drExist.Length == 0)
                        {
                            
                            prodcode = "<B>" + dtTP.Rows[i]["ProductCode"].ToString() + "</B>" ;
                            if (arCheckedProduct.Contains(prodcode))
                            {
                                continue;
                            }
                            arCheckedProduct.Add(prodcode);
                            TotalContestant = Convert.ToInt32(dtTP.Rows[i]["TotalContestant"].ToString());
                            ph2Room = Convert.ToInt32(dtTP.Rows[i]["ph2rooms"].ToString());
                            TPContestant = Convert.ToInt32(dtTP.Rows[i]["TPContestant"].ToString());
                            ContestDate = Convert.ToDateTime(dtTP.Rows[i]["ContestDate"].ToString());
                            if (dtCurrent > ContestDate)
                            {
                                TP.Add(prodcode,  " Contest Date has already passed.  You cannot download. ");                         
                                continue;
                            }
                            //Today < Contest_Date - #of Days then give a message, "Test Paper is normally available for downloaded starting mm/dd/yyyy" where the date = Contest_Date - #of Days
                            if (dtCurrent < Convert.ToDateTime(ContestDate.Date.AddDays(-12)))
                            {
                                TP.Add(prodcode,  " Test Paper is normally available for downloaded starting " + ContestDate.Date.AddDays(-12).ToShortDateString());
                                continue;
                            }
                            // The test paper cannot be given due to high volume.  Need to split into more rooms. Inform your CC to increase #of rooms at the link Schedule TCs. “  
                            if (TPContestant < (TotalContestant / ph2Room))
                            {
                                strTP = " The test paper cannot be given due to high volume(#of Contestant = " + TotalContestant + "). Need to split into more rooms. Inform your CC to increase #of rooms at the link Schedule TCs.";
                                TP.Add(prodcode,  strTP);
                                continue;
                            }
                        }
                    }
                }
                ltrMissingTP.Text = "";
                foreach (DictionaryEntry item in TP)
                {
                    ltrMissingTP.Text = ltrMissingTP.Text + item.Key + "  -  " + item.Value + " <br>";
                }
            }

            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            DataView dv = new DataView(dtNew);
            //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();

            return dtNew.Rows.Count;
        }
        else
        {
            return 0;
        }
    }
    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TestPaperId", objDelETP.TestPaperId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }
    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion
    
    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.Text = "No uploaded file";
        }
    }

    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            lblPrdError.Text = "";
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
                Server.Transfer("downloader.aspx");
            }
            else
            {
                lblPrdError.Text = "File doesn't exist";
            }
            //Session["DOWNLOAD_FILE_PATH"] = filePath;
            //Response.Redirect("~/downloader.aspx");
            

            //The following code calls Response.End which results in bad thread errors
            //so, trying to avoid it with above code
            /*
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                FileInfo finfo = new FileInfo(filePath);
                long FileInBytes = finfo.Length;
                Response.AddHeader("Content-Length", FileInBytes.ToString());
            }
            Response.Clear();
            //Response.Write("File Path: " + filePath);
            Response.WriteFile(@filePath);
            Response.Flush();
            Response.End();
            */
        }
        catch (Exception ex)
        {
           lblMessage.Text = ex.ToString();
        }
    }

    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }
    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {
       
        string ProductGroupCode = EventCodeAndProductGroupCode.Substring(EventCodeAndProductGroupCode.Length - 2);
        return ProductGroupCode;
    }

    private string GetEventCode(string EventCodeAndProductGroupCode)
    {
        string EventCode = EventCodeAndProductGroupCode.Substring(0, EventCodeAndProductGroupCode.Length - 5);
        return EventCode;
    }


    private void LoadSearchAndDownloadPanels(bool searchPanel,int EventID)
    {
        try
        {
       
        //Panel1.Visible = false;
        //Panel2.Visible = searchPanel;
        //Panel3.Visible = true;

        if (searchPanel == false)
        {
            Panel4.Visible = true;
            GetWeek(ddlFlrWeekForExamReceiver, true, EventID,int.Parse(ddlFlrYear.SelectedValue) );
        }
        GetProductGroupCodes(ddlFlrProductGroup, true,EventID);// ddlFlrEvent.SelectedIndex > 0 ? int.Parse(eventID) : 0);
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
        GetFlrContextYear();

        GetWeek(ddlFlrWeek, true, EventID,  int.Parse(ddlFlrYear.SelectedValue));

        PopulateContestants(ddlFlrNoOfContestants, true);
        PopulateSets(ddlFlrSet, true, EventID,int.Parse(ddlFlrYear.SelectedValue));
        //GetTestPapers(int.Parse(Session["RoleId"].ToString()));

            }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }

    }

    private void LoadUploadPanel(int EventID)
    {
        //Panel1.Visible = true;
        //Panel2.Visible = false;
        //Panel3.Visible = true;
        GetProductGroupCodes(ddlProductGroup, true, EventID);//ddlEvent.SelectedIndex > 0 ? int.Parse(ddlEvent.SelectedValue) : 0);
        GetContextYear(ddlContestYear, true);
        //GetWeek(ddlWeek, true,EventID,0 );
        GetWeek(ddlWeek, true, EventID, Convert.ToInt16(ddlContestYear.SelectedItem.Value));
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
        // GetWeeks(ddlWeek, true);
        PopulateContestants(ddlNoOfContestants, true);
        PopulateSets(ddlSet, true, EventID,0);
        //ddlDocType.Items.Insert(0, new ListItem("[Select Doc Type]", "-1"));  - 08/13/2014
        //GetTestPapers(m_objETP);
    }
    #endregion

    #region " Private Methods - Validations "
    private bool ValidateFileName(EntityTestPaper objETP)
    {
        StringBuilder TFileName = new StringBuilder();
        string ProductGroupCode;
        ProductGroupCode = objETP.ProductGroupCode.Substring(objETP.ProductGroupCode.Length - 2);
        string NoOfContestants = objETP.NoOfContestants.ToString();
        bool IsValidFileName = false;
        // Correct number of contestants if the selected value is "5"
        if (NoOfContestants == "5")
        {
            NoOfContestants = "05";
        }

        // Any product code other than MB, EW, and PS should have number of contestants
        if (ProductGroupCode == "MB" || ProductGroupCode == "EW" || ProductGroupCode == "PS")
        { 
            // No need to do anything here
        }
        else if (objETP.NoOfContestants == -1)
        {
            lblMessage.Text = "No. of Contestants is a required field.";
            return false;         
        }

        // Get the initial part of the file name
        if (objETP.NoOfContestants != -1)
        {
            TFileName.AppendFormat("Set{0}_{1}_{2}_{3}_{4}_{5}", objETP.SetNum, System.DateTime.Now.Year, objETP.ProductGroupCode, objETP.ProductCode, objETP.DocType, NoOfContestants); //Set1_SB_05_JSB_2007.pdf or .doc
        }
        else
        {
            TFileName.AppendFormat("Set{0}_{1}_{2}_{3}", objETP.SetNum,System.DateTime.Now.Year,objETP.ProductGroupCode,objETP.ProductCode,NoOfContestants); //Set1_SB_JSB_2007.pdf or .doc
        }

        // For MB the file name can have Q or A as suffix
        if (ProductGroupCode == "MB")
        {
            if (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip"))
                //||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_q", ".doc")) ||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_a", ".pdf")) ||
                //(objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), "_a", ".doc")))
            {
                IsValidFileName = true;
            }
        }
        else if ((objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip")) || (objETP.TestFileName.ToLower() == string.Concat(TFileName.ToString().ToLower(), ".zip")))
        {
            IsValidFileName = true;
        }

        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention. Required file name format is : '" + string.Concat(TFileName.ToString().ToLower() + ".zip'") ;
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion


    protected void ddlFlrWeekForExamReceiver_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ButtonForExamReceiver_Click(object sender, EventArgs e)
    {
        LoadRecordsForExamRcvr();
    }
    protected void LoadRecordsForExamRcvr()
    {
        int selectedWeekId = int.Parse(ddlFlrWeekForExamReceiver.SelectedValue);
        lblNoPermission.Visible = false;
        int Count = GetTestPapers(int.Parse(Session["LoginID"].ToString()), int.Parse(Session["RoleId"].ToString()), DateTime.Now.Year.ToString(), selectedWeekId);
        if (Count == 0)
        {
            lblNoPermission.Visible = true;
            lblNoPermission.Text = "There are no records";
        }
    }


    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblReplErr.Text = "";
        lblSuccessMsg.Text = "";
        
       if (ddlEvent.SelectedIndex > 0)
        {
            Panel1.Visible = true;
            divRepl.Visible = true;
            btnReplicate.Enabled = true;
            Panel2.Visible = false;
            Panel3.Visible = true;

            LoadUploadPanel(int.Parse(ddlEvent.SelectedValue));//
            LoadReplYear();
        }
            
    }
    protected void ddlFlrEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblReplErr.Text = "";
        lblSuccessMsg.Text = "";
        if (ddlFlrEvent.SelectedIndex > 0)
        {
            Panel1.Visible = false;
           // divRepl.Visible = false;
            btnReplicate.Enabled = false;

            Panel2.Visible =  Convert.ToBoolean(Session["EventFlag"]);//Flag;
            Panel3.Visible = true;


            LoadSearchAndDownloadPanels(Convert.ToBoolean(Session["EventFlag"]),int.Parse(ddlFlrEvent.SelectedValue));
        }
    }
    protected void ddlSet_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSet.SelectedIndex > 0)
        {
            ddlWeek.SelectedIndex = ddlSet.SelectedIndex;
            ddlWeek.Enabled = false;
        }
        else
        {
            ddlWeek.SelectedIndex = 0;
            ddlWeek.Enabled = true;
        }
    }
    protected void ddlWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSet.SelectedIndex = ddlWeek.SelectedIndex;
    }
    protected void ddlFlrSet_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Year = DateTime.Now.Year;
        if (int.Parse(ddlFlrYear.SelectedValue) ==  Year  && ddlFlrSet.SelectedIndex > 0)
        { 
                ddlFlrWeek.SelectedIndex = ddlFlrSet.SelectedIndex;
        }
        else if (int.Parse(ddlFlrYear.SelectedValue) < Year)
        {
            ddlFlrWeek.SelectedIndex = 0;
            ddlFlrWeek.Enabled = false;
        }
        else 
        {
            ddlFlrWeek.SelectedIndex = 0;
            ddlFlrWeek.Enabled = true;
        }
    }
    protected void ddlFlrWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFlrSet.Items.Count > 1)
        {        
            ddlFlrSet.SelectedIndex = ddlFlrWeek.SelectedIndex;
        }
    }
    protected void ddlFlrYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (int.Parse(ddlFlrYear.SelectedValue) < DateTime.Now.Year)
        //{
        //    ddlFlrWeek.SelectedIndex = 0;
        //    ddlFlrWeek.Enabled = false;
        //}
        //else
        //    ddlFlrWeek.Enabled = true;
        GetWeek(ddlFlrWeek, true, int.Parse(ddlFlrEvent.SelectedValue) , int.Parse(ddlFlrYear.SelectedValue));

        PopulateSets(ddlFlrSet, true, int.Parse(ddlFlrEvent.SelectedValue) , int.Parse(ddlFlrYear.SelectedValue));
    }
    protected void btnReplicate_Click(object sender, EventArgs e)
    {
        try
          {
              lblReplErr.Text = "";
              lblSuccessMsg.Text = "";
              string setNum = "4";
              if (ddlEvent.SelectedValue.ToString () == "2")
              {
                  setNum = "1";
              }
                int year = Convert.ToInt32(ddlReplYear.SelectedValue) - 1; // 'Now.Year
                int NewYear = Convert.ToInt32(ddlReplYear.SelectedValue); // Now.Year + 1
                int InsCount = 0;
                if (Convert.ToInt32(ddlReplYear.SelectedValue) < Convert.ToInt32(DateTime.Now.Year.ToString()))
                {
                    lblReplErr.Text = "Replication not Allowed";
                }

                InsCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from TestPapers Where EventID in (" + ddlEvent.SelectedValue + ") and Doctype='Instr' and ContestYear =" + year));

                String homePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString());
                String[] files = Directory.GetFiles(homePath, "*.zip"); //Directory.GetFiles(homePath, "*.xls");
                Boolean bFileExist = false;
                foreach (String f in files)
                {
                    String fileName = Path.GetFileName(f);
                    if (fileName.Contains(NewYear.ToString()) && (fileName.Contains("instr") || fileName.Contains("Instr")) && (fileName.Contains("Set" + setNum) || fileName.Contains("set" + setNum)))
                    {
                        lblReplErr.Text = "Test Paper - Instruction Files '" + fileName + "' already exists for the proposed year.";
                        return;
                    }
                }
                homePath = Server.MapPath("../TestPapers" + year.ToString() + "/");
                String[] Oldfiles = Directory.GetFiles(homePath, "*.zip"); //Directory.GetFiles(homePath, "*.xls");

                foreach (String f in Oldfiles)
                {
                    String fileName = Path.GetFileName(f);
                     if (fileName.Contains(year.ToString()) && (fileName.Contains("instr") || fileName.Contains("Instr")) && (fileName.Contains("Set" + setNum) || fileName.Contains("set" + setNum)))
                    {
                        if (InsCount > 0)
                        {
                            bFileExist = true;
                            copyfile(fileName, fileName.Replace(year.ToString(), NewYear.ToString()),year);
                        }
                    }
                }
            /*
             * Changed the code on July26_2016 By Bindhu 
             * to replicate instruction file from TestPapers2015 into TestPapers folder
             * 
                String  homePath = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString());
                String[] files  = Directory.GetFiles(homePath, "*.zip"); //Directory.GetFiles(homePath, "*.xls");
                Boolean bFileExist = false;
                foreach (String f in files)
                {
                    String fileName    = Path.GetFileName(f);
                    if (fileName.Contains(NewYear.ToString()) && ( fileName.Contains("instr") ||  fileName.Contains("Instr"))  && (fileName.Contains("Set" + setNum ) || fileName.Contains("set" +setNum )))
                    { 
                        lblReplErr.Text = "Test Paper - Instruction Files '"+ fileName +"' already exists for the proposed year.";
                        return; 
                    }
                    else if (fileName.Contains(year.ToString()) && (fileName.Contains("instr") || fileName.Contains("Instr")) && (fileName.Contains("Set" + setNum) || fileName.Contains("set" + setNum))) 
                    {
                        if (InsCount > 0)
                        {
                            bFileExist = true;
                            copyfile(fileName, fileName.Replace(year.ToString(), NewYear.ToString()));                                                    
                        }
                    }                   
                }
            */
                if (bFileExist == false)
                {
                    lblReplErr.Text = "Replication Failure : Test Paper-Instruction Files for the year " + year + " is not in the directory";
                    return;
                }
                else
                {
                    lblSuccessMsg.Text = "Test Paper - Instruction Files copied to year " + NewYear + " successfully.";
                }
                String StrInsTestPapers = "Insert into TestPapers(ProductId,ProductCode,ProductGroupId,ProductGroupCode,EventId,EventCode,WeekId,SetNum,NoOfContestants,TestFileName,Description,Password,CreateDate,CreatedBy,ContestYear,DocType)";
                StrInsTestPapers = StrInsTestPapers + " Select ProductId,ProductCode,ProductGroupId,ProductGroupCode,EventId,EventCode,WeekId,SetNum,NoOfContestants,Replace(TestFileName,'" + year + "','" + NewYear + "'),Description,Password,GETDATE()," + Session["LoginID"] + "," + NewYear + ",DocType";
                StrInsTestPapers = StrInsTestPapers + " from Testpapers WHERE EventID in (" + ddlEvent.SelectedValue + ") and Doctype='Instr' and ContestYear =" + year;

                InsCount = Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, StrInsTestPapers));
                LoadReplYear();
          }
        catch(Exception ex)
          {
            lblReplErr.Text = ex.ToString();
            //Response.Write(ex.ToString());
            return;

        }
     }  
    private void copyfile(String OldFileName ,String NewFileName,int oldYear  )
    {       
        try
        {
            if (File.Exists(Server.MapPath("../TestPapers"+ oldYear.ToString() +"/" + OldFileName.ToString().Replace("/", "\n"))))
                File.Copy(Server.MapPath("../TestPapers" + oldYear.ToString() + "/" + OldFileName.ToString().Replace("/", "\n")), (Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString() + NewFileName.ToString().Replace("/", "\n"))), true);
            else
                lblReplErr.Text = lblReplErr.Text + "<BR>" + OldFileName + " - Does not exist.";

            /*Changed the code on July26_2016 By Bindhu
             * While replicate files, copy files from TestPapers2015 into TestPapers folder
            if (File.Exists(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString() + OldFileName.ToString().Replace("/", "\n"))) )
                File.Copy(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString() + OldFileName.ToString().Replace("/", "\n")), (Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersPath"].ToString() + NewFileName.ToString().Replace("/", "\n"))), true);
            else
                lblReplErr.Text = lblReplErr.Text + "<BR>" + OldFileName + " - Does not exist.";
             */
        }
        catch(Exception ex)
        {
            lblReplErr.Text = ex.ToString();
        }
         
    }






   
}
#endregion

#region "Enums" 
public enum ScreenChoice
{
    DownloadScreen,
    UploadScreen

}

#endregion

#region " Class EntityTestPaper (ETP) "
public class EntityTestPaper
{
    public Int32 TestPaperId = -1;
    public Int32 ProductId = -1;
    public string ProductCode = string.Empty;
    public Int32 ProductGroupId = -1;
    public string ProductGroupCode = string.Empty;
    public string EventCode = string.Empty;
    public Int32 WeekId = -1;
    public Int32 SetNum = -1;
    public Int32 NoOfContestants = -1;
    public string TestFileName = string.Empty;
    public string Description = string.Empty;
    public string Password = string.Empty;
    public DateTime CreateDate = new System.DateTime(1900, 1, 1);
    public Int32 CreatedBy = -1;
    public DateTime? ModifyDate = null;
    public Int32? ModifiedBy = null;
    public string ContestYear = string.Empty;
     public string DocType = string.Empty;
    public string WeekOf = string.Empty;
    public string ReceivedBy = string.Empty;
    public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
    public Int32 EventId = -1;
  



    public string getCompleteFTPFilePath(String TestFileName)
    {
        return String.Format("{0}/{1}",
                  System.Configuration.ConfigurationManager.AppSettings["FTPTestPapersPath"], TestFileName);
    }
    public EntityTestPaper()
    {
    }

}
#endregion

 