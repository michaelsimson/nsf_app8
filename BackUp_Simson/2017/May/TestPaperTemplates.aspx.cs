﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;


#region " Class ManageTestPapers "

public partial class TestPaperTemplates : System.Web.UI.Page
{
    string Qurywhere;
     #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion
    // **************** Roles Allowed to Access in this Page
    //  Role 1,  Role 9 National,  Role 31 and 32 have full access.
    //  Role 9 chapter and  Role 33 can only download test papers for the current year.  
    
    #region " Event Handlers "
    protected void Page_Load(object sender, EventArgs e)
    {
        lblPGc.Visible = false;
        lbleventval.Visible = false;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
            Response.Redirect("login.aspx?entry=v");


        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "93") || (Session["RoleID"].ToString() == "30")))
        {
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from Volunteer where RoleId=" + Session["RoleId"] + " and [National]='Y' and TeamLead='Y' and MemberID = " + Session["LoginID"])) > 0)
            {
                hdnTechNational.Value = "Y";
                hdnteamlead.Value = "Y";
            }
            else
            {
                hdnTechNational.Value = "Y";
                hdnteamlead.Value = "N";
            }
        }
        if (!IsPostBack)
        {
            gvSortDirection = "ASC";
            gvSortExpression = "ProductCode";

            //if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || ((Session["RoleId"].ToString() == "9") && (hdnTechNational.Value == "Y")) || ((Session["RoleId"].ToString() == "93"))))    //|| (Session["RoleId"].ToString() == "31") || (Session["RoleId"].ToString() == "32") 
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "30") || ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y")))|| ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")))   //|| (Session["RoleId"].ToString() == "31") || (Session["RoleId"].ToString() == "32") 
            {
                    GetDropDownChoice(dllfileChoice, true);
                    dllfileChoice.Visible = true;
            }
            else
            {
                    Panel2.Visible = false;
                    Panel3.Visible = false;
                    lblNoPermission.Visible = true;
                    lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
            }
         }
    }
    private void GetTestPapers_NatTechT()
    {
            LblexamRecErr.Text = "";
            String StrSQL = "  SELECT DISTINCT TP.TestPaperTempId, TP.ContestYear, TP.ProductId, TP.ProductCode, TP.ProductGroupId, TP.ProductGroupCode, TP.EventID,";
            StrSQL = StrSQL + " TP.TestFileName,TP.DocType, TP.Description, TP.Password, TP.CreateDate, TP.CreatedBy, TP.ModifyDate, TP.ModifiedBy FROM ";
            StrSQL = StrSQL + " TestPaperTemplates TP INNER JOIn NatTechTeam N on N.ProductGroupCode = TP.ProductGroupCode Where TP.ContestYear = Year(GETDATE()) and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]);
            StrSQL = StrSQL + " order by TP.ProductID, TP.DocType";  

            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Panel3.Visible = true;
                gvTestPapers.DataSource = ds;
                gvTestPapers.DataBind();
            }
            else 
            {
                LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
            }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddevent.SelectedIndex = 0;

        DDeventD.SelectedIndex = 0;
       
        txtDescription.Text = "";
        TxtPassword.Text = "";
        gvTestPapers.Visible = false;
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {
           
            LoadSearchAndDownloadPanels(true);
           

        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
            LoadUploadPanel();
           
           
            
        }
    }

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        if (ddevent.SelectedItem.Text == "[Select Event]")
        {
            lbleventval.Visible = true;
        }

        //Response.Write(ddevent.SelectedItem.Value);
        int value = 0;
        
        if (FileUpLoad1.HasFile)
        {
            
            EntityTestPaper objETP = new EntityTestPaper();

            objETP.ProductId = int.Parse(ddlProduct.SelectedValue);
            objETP.ProductCode = ddlProduct.SelectedItem.Text;
            objETP.ProductGroupId = int.Parse(ddlProductGroup.SelectedValue);
            objETP.ProductGroupCode = GetProductGroupCode(ddlProductGroup.SelectedItem.Text);
            objETP.EventCode = GetEventCode(ddlProductGroup.SelectedItem.Text);
         
            objETP.ContestYear = ddlContestYear.SelectedItem.Text;
          
            objETP.TestFileName = FileUpLoad1.FileName;
            objETP.Description = txtDescription.Text;
            objETP.Password = TxtPassword.Text;
            objETP.CreateDate = System.DateTime.Now;
            objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());
           

            if (ValidateFileName(objETP))
            {
                try
                {
                   value =  TestPapers_Insert(objETP);
                  
                   if (value != 0)
                   {
                       SaveFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"], FileUpLoad1);

                       lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;
                       //GetTestPapers(m_objETP);
                   }
                   else
                   {
                       //lblMessage.Text = "You are inserting duplicate record";
                       FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString ()),"Temp_"+ FileUpLoad1.FileName.ToString ()));
                       hdnTempFileName.Value = "Temp_" + FileUpLoad1.FileName.ToString();
                       Panel1.Visible = false;
                       Panel5.Visible = true;
                       //dllfileChoice.Enabled = false;
                   }
                }
                catch(Exception err)
                {
                    lblMessage.Text = err.Message;
                }
            }
        }
        else
        {
            lblMessage.Text = "No File Uploaded.";
        }
    }

     protected void btnYes_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        if (hdnTempFileName.Value.Length > 0)
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update TestPaperTemplates SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate()  where  testFileName='" + hdnTempFileName.Value.Replace("Temp_", "").ToString() + "'");
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value), Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), true);
            Panel5.Visible = false;
            Panel1.Visible = true;
            lblMessage.Text = "File Replaced : " + hdnTempFileName.Value.Replace("Temp_", "").ToString();
        }
        else
        {
            Panel5.Visible = false;
            Panel1.Visible = true;
        }

    }

     protected void btnNo_Click(object sender, EventArgs e)
     {
         dllfileChoice.Enabled = true;
         if (hdnTempFileName.Value.Length > 0)
                 File.Delete (string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value));
         Panel5.Visible = false;
         Panel1.Visible = true;
     }
  

     protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
     {
         int index = -1;
         LblexamRecErr.Text = String.Empty;
         if (e.CommandArgument != null)
         {
             if (int.TryParse(e.CommandArgument.ToString(), out index))
             {
                 EntityTestPaper objETP = new EntityTestPaper();
                 index = int.Parse((string)e.CommandArgument);
                 objETP.TestPaperTempId = int.Parse(gvTestPapers.Rows[index].Cells[0].Text);
                 objETP.TestFileName = gvTestPapers.Rows[index].Cells[5].Text;
                 //objETP.DocType = gvTestPapers.Rows[index].Cells[10].Text;
                 if (e.CommandName == "DeleteTestPaper")
                 {
                     DeleteTestPaper(objETP);
                     //lblMessage.Text = DeleteTestPaperFromFTPSite(objETP);
                     DeleteFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"], objETP);
                     //GetTestPapers(m_objETP);
                 }
                 else if (e.CommandName == "Download")
                 {
                   
                     DownloadFile(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"], objETP);

                     // }

                 }
             }
         }
     }
            
    

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        if (e.SortExpression == "CreateDate")
        {
            if (this.gvSortDirection == "ASC")
            {
                e.SortExpression = "YEAR DESC, MONTH DESC, DAY DESC";
                this.gvSortDirection = "DESC";
            }
            else
            {
                e.SortExpression = "YEAR ASC, MONTH ASC, DAY ASC";
                this.gvSortDirection = "ASC";
            }
        }

        else if (e.SortExpression == "ProductId")
        {
            if(this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {

                this.gvSortDirection = "ASC";
            }
            
            this.gvSortExpression = "ProductId";

        }
        else
        {
            if (this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {
                this.gvSortDirection = "ASC";
            }
        }
       
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LblexamRecErr.Text = "";
        m_objETP.ProductId = int.Parse(ddlFlrProduct.SelectedValue);
        m_objETP.ProductGroupId = int.Parse(ddlFlrProductGroup.SelectedValue);
      
        gvTestPapers.Visible = true;
        GetTestPaperssearch(m_objETP);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        LblexamRecErr.Text = "";
        ddlFlrProduct.SelectedIndex = 0;
        ddlFlrProductGroup.SelectedIndex = 0;
        DDeventD.SelectedIndex = 0;
        gvTestPapers.Visible = false;
        lblnorecord.Visible = false;
        ddlFlrYear.SelectedIndex = 0;
      
       
    }

    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
    }

    #endregion

    #region " Private Methods - Data Access Layer "

    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;


        string sqlstring = "if Not Exists(Select * from TestPaperTemplates where TestFileName ='" + objETP.TestFileName + "')Begin INSERT INTO .[dbo].[TestPaperTemplates] ([ProductId],[ProductCode] ,[ProductGroupId] ,[ProductGroupCode] ,[EventCode],[TestFileName] ,[Description] ,[Password] ,[CreateDate] ,[CreatedBy] ,[ContestYear],[DocType],[EventId]) VALUES ('" + objETP.ProductId + "','" + objETP.ProductCode + "', '" + objETP.ProductGroupId + "','" + objETP.ProductGroupCode + "', '" + objETP.EventCode + "', '" + objETP.TestFileName + "', '" + objETP.Description + "','" + objETP.Password + "','" + objETP.CreateDate + "', '" + objETP.CreatedBy + "','" + objETP.ContestYear + "','" + objETP.DocType + "','"+ddevent.SelectedItem.Value+"')select @@rowcount End  else select 0 ";
        value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sqlstring);
       
        if (value == null)
        {
            return -1;
            // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }
    private void getProductCode(DropDownList ddlObjectload)
    {
       
        if (((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")) || ((Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")) || ((Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y")))
        {
            
            //dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct [ProductGroupId], [ProductGroupCode] AS EventCodeAndProductGroupCode from volunteer where [ProductGroupCode] is not null and EventID='" + EventId + "' and  MemberID = " + Session["LoginID"] + "");
            DataSet dsproductgroupload = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct V.ProductGroupId, V.MemberID,V.ProductGroupCode as EventCodeAndProductGroupCode  FROM Volunteer V inner join product  p on  V.ProductGroupCode=p.ProductGroupCode inner join Event E on E.EventId=P.eventId where E.EventId in (1,2) and  V.memberId= " + Session["LoginID"] + " and  V.ProductGroupCode is not null");
            DataTable dt = dsproductgroupload.Tables[0];

             if (dt.Rows.Count == 1)
             {
                 ddlObjectload.DataSource = dsproductgroupload;

                 ddlObjectload.DataTextField = "EventCodeAndProductGroupCode";
                 ddlObjectload.DataValueField = "ProductGroupId";
                 ddlObjectload.DataBind();
                 // ddlObjectload.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
                 GetProductCodes(int.Parse(ddlObjectload.SelectedValue), ddlProduct, true);
                 GetProductCodes(int.Parse(ddlObjectload.SelectedValue), ddlFlrProduct, true);
                ddlObjectload.Enabled = false;
             }
             else
             {
                 ddlObjectload.DataSource = dsproductgroupload;

                 ddlObjectload.DataTextField = "EventCodeAndProductGroupCode";
                 ddlObjectload.DataValueField = "ProductGroupId";
                 ddlObjectload.DataBind();
                  ddlObjectload.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
                  GetProductCodes(0, ddlFlrProduct, true);
                  //GetProductCodes(int.Parse(ddlObjectload.SelectedValue), ddlFlrProduct, true);
                 GetProductCodes(0, ddlProduct, true);
            }
        }
        // GetProductCodes(0, ddlFlrProduct, true);
    }
    private void GetProductGroupCodes(int EventId, DropDownList ddlObject,DropDownList ddyear, bool blnCreateEmptyItem)
    {
        try
        {
            DataTable dt;
            if (ddlFlrProductGroup.Enabled != false)
            {
                if (EventId != 0)
                {
                    DataSet dsproductgroup;
                    if (((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")) || ((Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")) || ((Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y")))
                    {
                        if (ddevent.SelectedItem.Text != "[Select Event]")
                        {
                            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct V.ProductGroupId, V.MemberID,V.ProductGroupCode as EventCodeAndProductGroupCode    FROM Volunteer V inner join product  p on  V.ProductGroupCode=p.ProductGroupCode inner join Event E on E.EventId=P.eventId where  E.EventId=" + EventId + " and V.memberId= " + Session["LoginID"] + " and  V.ProductGroupCode is not null");
                        }
                        else
                        {
                            //dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct [ProductGroupId], [ProductGroupCode] AS EventCodeAndProductGroupCode from volunteer where [ProductGroupCode] is not null and EventID='" + EventId + "' and  MemberID = " + Session["LoginID"] + "");
                            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct V.ProductGroupId, V.MemberID,V.ProductGroupCode as EventCodeAndProductGroupCode    FROM Volunteer V inner join product  p on  V.ProductGroupCode=p.ProductGroupCode inner join Event E on E.EventId=P.eventId where  E.EventId in(1,2) and V.memberId= " + Session["LoginID"] + " and  V.ProductGroupCode is not null");
                        }
                    }
                    else
                    {
                        string str = "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where  A.EventID='" + EventId + "'  and A.ProductGroupCode in (Select distinct ProductGroupCode From ContestCategory where (RegionalStatus = 'Active' OR NationalFinalsStatus = 'Active')) Order by A.EventCode";
                        dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, str);
                    }
                    dt = dsproductgroup.Tables[0];

                    if (dt.Rows.Count == 1)
                    {

                        ddlObject.DataSource = dsproductgroup;
                        ddlObject.DataTextField = "EventCodeAndProductGroupCode";
                        ddlObject.DataValueField = "ProductGroupId";
                        GetProductCodes(int.Parse(ddlObject.SelectedValue), ddlProduct, true);
                        GetProductCodes(int.Parse(ddlObject.SelectedValue), ddlFlrProduct, true);
                        ddlObject.DataBind();
                        ddlObject.Enabled = false;

                    }
                    else
                    {

                        ddlObject.DataSource = dsproductgroup;
                        ddlObject.DataTextField = "EventCodeAndProductGroupCode";
                        ddlObject.DataValueField = "ProductGroupId";
                        ddlObject.DataBind();
                        //if (blnCreateEmptyItem)
                        //{
                        //    ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
                        //}
                    }
                }
                if (blnCreateEmptyItem)
                {
                    ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
                }
                else
                {
                    ddlObject.Items.Clear();
                }
                // ddlObject.SelectedIndex = 0;
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }

    private void GetFlrContextYear()
    {
        //object year = DateTime.Now.Year;
        int year = DateTime.Now.Year + 1;
        ddlFlrYear.Items.Clear();
        for (int i = 0; i < 5;i++ )
        {
            ddlFlrYear.Items.Insert(i, new ListItem((year-i).ToString()));
        }

        ddlFlrYear.SelectedIndex = 1;
    }
    private void GetContextYear(DropDownList ddlContestYear, bool blnCreateEmptyItem)
    {
        object year;
        try
        {
            //year =  SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct EventYear from Event");
            year = DateTime.Now.Year;
            ddlContestYear.Items.Clear();
            if (blnCreateEmptyItem)
            {
                // ddlContestYear.Items.Insert(0, new ListItem(year.ToString()));
                 ddlContestYear.Items.Insert(0, new ListItem(year.ToString()));
                 ddlContestYear.Items.Insert(1, new ListItem((Convert.ToInt32(year) + 1).ToString()));
            }
         }
         catch (SqlException se)
         {
            lblMessage.Text = se.Message;
            return;
         }
    }

    private void GetWeek(DropDownList ddlWeek, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsWeek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select WeekID, (Convert(varchar(14),SatDay1,106) +  ' - ' + Convert(varchar(14),sunDay2,106)) as Week  from weekCalendar Where Year(SatDay1)=Year(GETDATE()) Or Year(sunDay2)= YEAR(GETDATE())");
            ddlWeek.DataSource = dsWeek;
            ddlWeek.DataTextField = "Week";
            ddlWeek.DataValueField = "WeekID";
            ddlWeek.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlWeek.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlWeek.SelectedIndex = 0;
        }
        catch(SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            if (ProductGroupId != -1)
            {
               
                DataSet dsproduct;
                //if ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N"))
                //{
                //    dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct ProductCode,ProductID  from  Product where eventid in (1,2) and  Status='o' and  ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "'");
                //}
                //else
                //{
                //    //dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where  A.ProductGroupCode in (Select distinct ProductGroupCode From ContestCategory where (RegionalStatus = 'Active' OR NationalFinalsStatus = 'Active') AND ContestYear = " + System.DateTime.Now.Year.ToString() + ") Order by A.EventCode");
                //}
                 if (Convert.ToInt32(Session["RoleID"]) == 30 && hdnteamlead.Value  == "N")
                {
                    dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct ProductCode,ProductId from  Volunteer where MemberID=" + Convert.ToInt32(Session["LoginID"]) + "  and  ProductGroupId=" + ProductGroupId.ToString()  + " and ProductID is not null");
                }
                else
                {
                    dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct ProductCode,ProductId from Product where   ProductGroupId=" + ProductGroupId.ToString());
                }
                if (dsproduct.Tables[0].Rows.Count > 0)
                {
                    ddlObject.DataSource = dsproduct;
                    ddlObject.DataTextField = "ProductCode";
                    ddlObject.DataValueField = "ProductId";
                    ddlObject.DataBind();
                    lblSearchErr.Text = "";
                    btnSearch.Enabled = true;
                    btnReset.Enabled = true;
                }
                else 
                {
                    ddlObject.DataSource = null ;
                    ddlObject.DataBind();
                    if (Convert.ToInt32(Session["RoleID"]) == 30 && hdnteamlead.Value == "N")
                    {
                        lblSearchErr.Text = "No Products assigned for the Volunteer.";
                    }
                    lblSearchErr.Visible = true;
                    btnSearch.Enabled = false;
                    btnReset.Enabled = false;
                    return;
                }
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
    private void PopulateContestants(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 5, 10, 15, 20, 25, 30, 35 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select #Of Children]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }
    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }


    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            if (Session["RoleId"].ToString() == "30")
            {
                string[] Choice = { "DownloadScreen" };
                ddlObject.DataSource = Choice;
                ddlObject.DataBind();

            }
            else
            {
                string[] Choice = { "UploadScreen", "DownloadScreen" };
                ddlObject.DataSource = Choice;
                ddlObject.DataBind();
            }

            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
            }
            ddlObject.SelectedIndex = 0;

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }


    private void GetTestPaperssearch(EntityTestPaper objETP)
    {
        //string valueEvent = DDeventD.SelectedItem.Text;
        try
        {
            string searchqry;

            if ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N") || (Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y") || (Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N"))
            {
                if (ddlFlrProductGroup.SelectedItem.Text != "[Select Product Group]")
                {
                    searchqry = "select distinct TestPaperTempId,ContestYear,EventCode,ProductCode,ProductGroupCode,TestFileName,Description,Password from TestPaperTemplates where ContestYear='" + ddlFlrYear.SelectedItem.Text + "'";
                    Qurywhere = searchqry + genWhereConditons();
                  
                    DataSet dsQuery = new DataSet();
                    dsQuery = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qurywhere);

                    DataTable dt = dsQuery.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        gvTestPapers.DataSource = dsQuery;
                        gvTestPapers.DataBind();
                        lblnorecord.Visible = false;
                    }
                    else
                    {
                        lblnorecord.Visible = true;
                        gvTestPapers.Visible = false;
                    }

                }
                else
                {
                    lblPGc.Visible = true;
                }


            }
            else
            {
                searchqry = "select distinct TestPaperTempId,ContestYear,EventCode,ProductCode,ProductGroupCode,TestFileName,Description,Password from TestPaperTemplates where ContestYear='" + ddlFlrYear.SelectedItem.Text+ "'";
                Qurywhere = searchqry + genWhereConditons();
                DataSet dsQuery = new DataSet();
                dsQuery = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qurywhere);
                DataTable dt = dsQuery.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    gvTestPapers.DataSource = dsQuery;
                    gvTestPapers.DataBind();
                    lblnorecord.Visible = false;

                }
                else
                {
                    lblnorecord.Visible = true;
                    gvTestPapers.Visible = false;
                }



            }



        }

        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }

    protected string genWhereConditons()
    {
            string iCondtions = string.Empty;
            if (DDeventD.SelectedItem.Text != "[Select Event]")
            {
                iCondtions += " and EventId=" + DDeventD.SelectedItem.Value.ToString();
            }
            if (ddlFlrProductGroup.SelectedItem.Text != "[Select Product Group]")
            {
                iCondtions += " and ProductGroupCode='" + ddlFlrProductGroup.SelectedItem.Text + "'";
            }
            if (ddlFlrProduct.SelectedItem.Text != "[Select Product]")
            {
                iCondtions += " and ProductCode='" + ddlFlrProduct.SelectedItem.Text + "'";
            }

            return iCondtions;
        }
    
    private void GetTestPapers(EntityTestPaper objETP)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct TestPaperTempId,ProductCode,ProductGroupCode,TestFileName,Description,Password from TestPaperTemplates");

        gvTestPapers.DataSource = ds;
        gvTestPapers.DataBind();


    }
    private int GetTestPapers(int memberid, int roleid)
    {
        DataSet ds = new DataSet();
        ds= SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,"select distinct TestPaperTempId,ProductCode,ProductGroupCode,TestFileName,Description,Password from TestPaperTemplates");
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            DataTable dtNew = dt.Clone();
            String CurrTestFileName;
            String CurrTestFilePrefix;
            String PrevTestFilePrefix = "";
            try
            {
                //Following loop is to filter out the Test Paprers, which belong to the same product code and same set but with 
                //different number of children. For example: If there are Set2_2008_SB_SSB_TestP_15.zip, Set2_2008_SB_SSB_TestP_20.zip, Set2_2008_SB_SSB_TestP_25.zip records exist then
                //this loop filters keeps only Set2_2008_SB_SSB_TestP_15.zip and filters out the other two.
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CurrTestFileName = dt.Rows[i].ItemArray[10].ToString();
                    CurrTestFilePrefix = CurrTestFileName.Substring(0, CurrTestFileName.Length - 6);
                    if (!(PrevTestFilePrefix.ToLower().Equals(CurrTestFilePrefix.ToLower())))
                    {
                        PrevTestFilePrefix = CurrTestFilePrefix;
                        DataRow dr = dt.Rows[i];
                        dtNew.ImportRow(dr);
                    }
                }

            
            }

            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            DataView dv = new DataView(dtNew);
            //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();

            return dtNew.Rows.Count;
        }
        else
        {
            return 0;
        }
    }
    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TestPaperTempId", objDelETP.TestPaperTempId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }
    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion
    
    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.Text = "No uploaded file";
        }
    }

    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            //Response.Write(objETP.TestFileName);
            
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
            Server.Transfer("downloader.aspx");
          
        }
        catch (Exception ex)
        {
           lblMessage.Text = ex.ToString();
        }
    }

    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }
    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }

    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {
       
        string ProductGroupCode = EventCodeAndProductGroupCode;
        return ProductGroupCode;
    }

    private string GetEventCode(string EventCodeAndProductGroupCode)
    {
        string EventCode = ddevent.SelectedItem.Text;
        return EventCode;
    }


    private void LoadSearchAndDownloadPanels(bool searchPanel)
    {

        try
        {
            getProductCode(ddlFlrProductGroup);
            // GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlProduct, true);
            Panel1.Visible = false;
            Panel2.Visible = searchPanel;
            Panel3.Visible = true;
            if (searchPanel == false)
            {
                Panel4.Visible = true;
                GetWeek(ddlFlrWeekForExamReceiver, true);
            }
            //GetProductGroupCodes(0, ddlProductGroup, true);
            // GetProductGroupCodes(0,ddlFlrProductGroup, true);
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y"))))
            {
                GetProductGroupCodes(0, ddlFlrProductGroup,ddlFlrYear, true);
                GetProductCodes(0, ddlFlrProduct, true);

            }
            // GetProductCodes(0, ddlFlrProduct, true);

            // GetProductCodes(int.Parse(ddlProductGroup.SelectedValue), ddlFlrProduct, true);
            // GetWeek(ddlFlrWeek, true);
            GetFlrContextYear();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }

       
    }

    private void LoadUploadPanel()
    {
        try
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = true;

            getProductCode(ddlProductGroup);
            //ddlProduct.Items.Insert(0, "[Select Product Group]");
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2")) || ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y")))
            {
                GetProductGroupCodes(0, ddlProductGroup,ddlContestYear, true);
                GetProductCodes(0, ddlProduct, true);
            }
            //GetProductGroupCodes(ddlProductGroup, true);
            GetContextYear(ddlContestYear, true);
            // GetWeek(ddlWeek, true);
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
        
      
    }
    #endregion

    #region " Private Methods - Validations "
    private bool ValidateFileName(EntityTestPaper objETP)
    {
        string TFileName = string.Empty;
        string ProductGroupCode;
        ProductGroupCode = objETP.ProductGroupCode;
       // string NoOfContestants = objETP.NoOfContestants.ToString();
        bool IsValidFileName = false;
        string []exArr=new string[]{"doc","docx","zip"};
        // Correct number of contestants if the selected value is "5"
       

        // Any product code other than MB, EW, and PS should have number of contestants
        

        // Get the initial part of the file name
        //string phase = "";
        //if (ddlPhase.SelectedIndex == 0)
        //    phase = "";
        //else
        //    phase = ddlPhase.SelectedItem.Text + "_";
        //phase+ 

        TFileName=string.Format("{0}_{1}_{2}_{3}_{4}", ddlContestYear.SelectedValue  , ddevent.SelectedItem.Text, objETP.ProductGroupCode, objETP.ProductCode, "TPTemplate"); //Set1_SB_05_JSB_2007.pdf or .doc  //System.DateTime.Now.Year

            if (Array.IndexOf(exArr, objETP.TestFileName.Substring(objETP.TestFileName.LastIndexOf('.') + 1)) >= 0)
            {
                if (string.Compare(objETP.TestFileName.Substring(0, objETP.TestFileName.LastIndexOf('.')), TFileName,true) == 0)
                {
                    IsValidFileName = true;
                }
                else
                {
                    //filename
                }
            }
            else
            {
                //ext
            }
        
        // For MB the file name can have Q or A as suffix
       
            

        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention. Required file name format is : '" + string.Concat(TFileName.ToString().ToLower() + "( Allow: .doc, .docx. .zip)");
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion


    protected void ddlFlrWeekForExamReceiver_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {
            LoadSearchAndDownloadPanels(true);
        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
            LoadUploadPanel();
        }
    }
    protected void ButtonForExamReceiver_Click(object sender, EventArgs e)
    {
        LoadRecordsForExamRcvr();
    }

    protected void LoadRecordsForExamRcvr()
    {
        int selectedWeekId = int.Parse(ddlFlrWeekForExamReceiver.SelectedValue);
        lblNoPermission.Visible = false;
      
    }


    //protected void ddevent_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //}
    protected void ddevent_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y"))))
            {
                if (ddevent.SelectedItem.Text != "[Select Event]")
                {
                    GetProductGroupCodes(int.Parse(ddevent.SelectedValue), ddlProductGroup,ddlContestYear, true);
                }
                else
                {
                    GetProductGroupCodes(0, ddlProductGroup,ddlContestYear, true);
                }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
        //GetProductGroupCodes(ddlFlrProductGroup, true);
    }
    protected void DDeventD_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || ((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y"))))
            {
                if (DDeventD.SelectedItem.Text != "[Select Event]")
                {
                    GetProductGroupCodes(int.Parse(DDeventD.SelectedValue), ddlFlrProductGroup,ddlFlrYear, true);
                }
            }
            if (DDeventD.SelectedItem.Text != "[Select Event]")
            {
                GetProductGroupCodes(int.Parse(DDeventD.SelectedValue), ddlFlrProductGroup,ddlFlrYear, true);
            }
        }
        catch
        {
        }
    }
}
#endregion

#region "Enums" 
public enum ScreenChoice
{
    DownloadScreen,
    UploadScreen

}

#endregion

#region " Class EntityTestPaper (ETP) "
public class EntityTestPaper
{
    public Int32 TestPaperTempId = -1;
    public Int32 ProductId = -1;
    public string ProductCode = string.Empty;
    public Int32 ProductGroupId = -1;
    public string ProductGroupCode = string.Empty;
    public string EventCode = string.Empty;
    //public Int32 WeekId = -1;
    //public Int32 SetNum = -1;
   // public Int32 NoOfContestants = -1;
    public string TestFileName = string.Empty;
    public string Description = string.Empty;
    public string Password = string.Empty;
    public DateTime CreateDate = new System.DateTime(1900, 1, 1);
    public Int32 CreatedBy = -1;
    public DateTime? ModifyDate = null;
    public Int32? ModifiedBy = null;
    public string ContestYear = string.Empty;
     public string DocType = string.Empty;
    public string WeekOf = string.Empty;
    public string ReceivedBy = string.Empty;
    public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
   
   

    public string getCompleteFTPFilePath(String TestFileName)
    {
        return String.Format("{0}/{1}",
                  System.Configuration.ConfigurationManager.AppSettings["FTPTestPapersPath"], TestFileName);
    }
    public EntityTestPaper()
    {
    }

}
#endregion

 