﻿Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Partial Class AddUpdateRoles
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            GetRecords()
        End If
    End Sub
    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Role")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        ExpExcel1.Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            lblError.Text = "No Records to Display"
            ExpExcel1.Visible = False
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        BtnAdd.Text = "Update"
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim RoleID As Integer
        RoleID = Val(GridView1.DataKeys(index).Value)
        If (RoleID) And RoleID > 0 Then
            Session("EditRoleID") = RoleID
            GetSelectedRecord(RoleID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from Role where RoleId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtRoleCode.Text = dsRecords.Tables(0).Rows(0)("RoleCode").ToString()
        txtName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        txtSelection.Text = dsRecords.Tables(0).Rows(0)("Selection").ToString()
        txtDescription.Text = dsRecords.Tables(0).Rows(0)("Description").ToString()
        txtRestrictions.Text = dsRecords.Tables(0).Rows(0)("Restrictions").ToString()
        txtResponsibilities.Text = dsRecords.Tables(0).Rows(0)("Responsibilities").ToString()

        ddlNational.SelectedIndex = ddlNational.Items.IndexOf(ddlNational.Items.FindByValue(dsRecords.Tables(0).Rows(0)("National").ToString()))
        ddlZonal.SelectedIndex = ddlZonal.Items.IndexOf(ddlZonal.Items.FindByValue(dsRecords.Tables(0).Rows(0)("Zonal").ToString()))
        ddlFinals.SelectedIndex = ddlFinals.Items.IndexOf(ddlFinals.Items.FindByValue(dsRecords.Tables(0).Rows(0)("Finals").ToString()))
        ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue(dsRecords.Tables(0).Rows(0)("Cluster").ToString()))
        ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(dsRecords.Tables(0).Rows(0)("Chapter").ToString()))
        ddlIndiaChapter.SelectedIndex = ddlIndiaChapter.Items.IndexOf(ddlIndiaChapter.Items.FindByValue(dsRecords.Tables(0).Rows(0)("IndiaChapter").ToString()))

    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        Try

            If BtnAdd.Text = "Add" Then
                If txtRoleCode.Text.Length < 1 Then
                    lblError.Text = "Please Enter Role Code"
                ElseIf txtName.Text.Length < 1 Then
                    lblError.Text = "Please Enter Role Name"
                ElseIf txtSelection.Text.Length < 1 Then
                    lblError.Text = "Please Select Role Selection"
                Else
                    Dim cnt As Integer = 0
                    Strsql = "Select count(RoleCode) from Role where RoleCode='" & txtRoleCode.Text & "'"      ' AND RoleId=" & ddlRoleID.SelectedItem.Value
                    cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                    If cnt = 0 Then
                        Strsql = "Insert Into Role(RoleCode,[National],Zonal,Chapter,Finals,Cluster,IndiaChapter,Name,Selection,Description,Restrictions,Responsibilities, CreateDate, CreatedBy) Values ('"
                        Strsql = Strsql & txtRoleCode.Text & "','" & ddlNational.SelectedValue & "','" & ddlZonal.SelectedValue & "','" & ddlChapter.SelectedValue & "','" & ddlFinals.SelectedValue & "','" & ddlCluster.SelectedValue & "','" & ddlIndiaChapter.SelectedValue & "','"
                        Strsql = Strsql & txtName.Text & "','" & txtSelection.Text & "','" & txtDescription.Text & "','" & txtRestrictions.Text & "','" & txtResponsibilities.Text & "',GETDATE()," & Session("LoginID") & ")"
                        If SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql) > 0 Then
                            lblError.Text = "Record Added Successfully"
                        End If
                        GetRecords()
                        clear()
                    Else
                        lblError.Text = "The Code with same RoleCode already Exist"
                    End If
                End If
            ElseIf BtnAdd.Text = "Update" Then
                If txtRoleCode.Text.Length < 1 Then
                    lblError.Text = "Please Enter Role Code"
                ElseIf txtName.Text.Length < 1 Then
                    lblError.Text = "Please Enter Role Name"
                ElseIf txtSelection.Text.Length < 1 Then
                    lblError.Text = "Please Select Role Selection"
                Else
                    Strsql = "Update Role Set RoleCode='" & txtRoleCode.Text & "',[National]='" & ddlNational.SelectedValue & "',Zonal='" & ddlZonal.SelectedValue & "',Chapter='" & ddlChapter.SelectedValue & "',Finals='" & ddlFinals.SelectedValue & "',Cluster='" & ddlCluster.SelectedValue & "',IndiaChapter='" & ddlIndiaChapter.SelectedValue & "',Name='" & txtName.Text & "',Selection='" & txtSelection.Text & "',Description='" & txtDescription.Text & "',Restrictions='" & txtRestrictions.Text & "',Responsibilities='" & txtResponsibilities.Text & "', ModifyDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', ModifiedBy='" & Session("LoginID") & "' Where RoleID=" & Session("EditRoleID")

                    'MsgBox(Strsql)
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql) > 0 Then
                        lblError.Text = "Record Updated Successfully"
                    End If
                    GetRecords()
                    clear()
                    BtnAdd.Text = "Add"
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub clear()
        txtRoleCode.Text = ""
        txtName.Text = ""
        txtDescription.Text = ""
        txtSelection.Text = ""
        txtRestrictions.Text = ""
        txtResponsibilities.Text = ""

        BtnAdd.Text = "Add"
        ddlNational.ClearSelection()
        ddlNational.Items.FindByText("Select").Selected = True
        ddlZonal.ClearSelection()
        ddlZonal.Items.FindByText("Select").Selected = True
        ddlCluster.ClearSelection()
        ddlCluster.Items.FindByText("Select").Selected = True
        ddlChapter.ClearSelection()
        ddlFinals.ClearSelection()
        ddlFinals.Items.FindByText("Select").Selected = True
        ddlChapter.Items.FindByText("Select").Selected = True
        ddlIndiaChapter.ClearSelection()
        ddlIndiaChapter.Items.FindByText("Select").Selected = True
    End Sub
    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
        lblError.Text = ""
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvCreditCardchapter As Control)
    End Sub
    Protected Sub ExpExcel1_Click(sender As Object, e As EventArgs) Handles ExpExcel1.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Add/Update Roles.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        GridView1.RenderControl(htmlWrite)

        'GridDonations.RenderControl(htmlWrite)
        Response.Write("<table><tr><td align='center' colspan='4'><font face='Arial' size='4'>North South Foundation<br />Organization Remittances</font></td></tr><tr><td align='right' colspan='4'>")
        Response.Write(stringWrite.ToString())
        Response.Write("</td></tr>") '<tr><td align='right' colspan='4'>Total : " & lblTotal.Text & "</td></tr>")
        Response.Write("</table>")
        Response.End()
    End Sub
End Class
