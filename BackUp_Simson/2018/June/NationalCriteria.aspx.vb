Imports System
Imports System.Data

Imports System.Diagnostics
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Imports System.Configuration
Imports System.Text
Imports NativeExcel

Partial Class Reports_NationalCriteria
    Inherits System.Web.UI.Page
    Dim Year As Integer
    Public dt As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Year = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetOpenEventYear", New SqlParameter("@EventID", 2))
        Year = Now.Year
        lblYear.Text = Year
        If Not IsPostBack Then
            'lblMessage.Text = "Please Update the scores and click Save button."
            Dim result As Boolean = ReadDataBase()
            ' result = False
            If result = False Then
                ReadContestCategories()
            End If
            ddlExport.Items(0).Enabled = True
            Response.Write("<script language='javascript'>window.open('NatCriteriaStatus.aspx','_blank','left=150,top=0,width=620,height=450,toolbar=0,location=0,scrollbars=1');</script> ")

            CheckInviteeFlags()
            GVtieBreaker.Visible = False
            GVTieBreakerRank.Visible = False
            lblTieBreaker.Text = String.Empty
            lblTieBreakerRank.Text = String.Empty

        End If
    End Sub

    Private Function ReadDataBase() As Boolean
        ' connect to the peoducts database
        Dim connectionString As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("NSFConnectionString").ToString()
        ' create and open the connection object
        Dim connection As SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
        connection.Open()
        Dim commandString As String = "SELECT N.ProductCode, N.Grade, N.MinScore,N.Rank1Score, N.NewChScore, N.MaxScore from NationalCriteria N Left Outer Join contestcategory C ON N.ProductCode = C.contestcode and N.ContestYear=C.ContestYear where c.contestyear = " & Year & " and (c.NationalSelectionCriteria is null or c.NationalSelectionCriteria ='I') and c.NationalFinalsStatus = 'Active' and N.List='Total' order by C.ContestCategoryID"
        'Dim commandString As String = "SELECT N.Contest, N.Grade, N.MinScore,N.Rank1Score, N.NewChScore, N.MaxScore from NationalCriteria N Left Outer Join Product P ON N.Contest = P.Productcode  where N.ContestYear = " & Year & " AND Eventid=2  order by P.ProductID"
        Dim daContests As SqlDataAdapter = New SqlDataAdapter(commandString, connection)
        Dim dsContests As DataSet = New DataSet()
        Dim retCount As Integer = daContests.Fill(dsContests, "Contests")
        If (retCount > 0) Then
            gvCriteria.DataSource = dsContests.Tables(0)
            gvCriteria.DataBind()
            dt = dsContests.Tables(0)
            ' Return True
        End If

        Dim commandString_1 As String = "SELECT N.ProductCode, N.Grade, N.MinScore,N.Rank1Score, N.NewChScore, N.MaxScore from NationalCriteria N Left Outer Join contestcategory C ON N.ProductCode = C.contestcode and N.ContestYear=C.ContestYear where c.contestyear = " & Year & " and (c.NationalSelectionCriteria is null or c.NationalSelectionCriteria ='I') and c.NationalFinalsStatus = 'Active' and N.List='Priority' order by C.ContestCategoryID"
        Dim daContests_1 As SqlDataAdapter = New SqlDataAdapter(commandString_1, connection)
        Dim dsContests_1 As DataSet = New DataSet()
        Dim retCount_1 As Integer = daContests_1.Fill(dsContests_1, "Contests")
        If (retCount_1 > 0) Then
            GVPriority.DataSource = dsContests_1.Tables(0)
            GVPriority.DataBind()
        Else
            Dim dt3 As DataTable = New DataTable()
            Dim dr1 As DataRow
            dt3.Columns.Add("ProductCode", Type.GetType("System.String"))
            dt3.Columns.Add("Grade", Type.GetType("System.Int32"))
            dt3.Columns.Add("MinScore", Type.GetType("System.Int32"))
            dt3.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
            dt3.Columns.Add("NewChScore", Type.GetType("System.Int32"))
            dt3.Columns.Add("MaxScore", Type.GetType("System.Int32"))
            Try

                For i As Integer = 0 To gvCriteria.Rows.Count - 1
                    dr1 = dt3.NewRow()
                    dr1("ProductCode") = CType(gvCriteria.Rows(i).Cells(0).FindControl("lblContest"), Label).Text
                    dr1("Grade") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
                    'CType(e.Item.FindControl("txtTransType"), TextBox).Text
                    dr1("Rank1Score") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(3).FindControl("lblRank1Score"), Label).Text) + 2
                    dr1("MinScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text) + 2
                    dr1("NewChScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(4).FindControl("txtNewChScore"), TextBox).Text) + 2
                    dr1("MaxScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(5).FindControl("txtMaxScore"), TextBox).Text)
                    dt3.Rows.Add(dr1)
                Next
                GVPriority.DataSource = dt3
                GVPriority.DataBind()
                'Return True

            Catch ex As Exception
                ' Response.Write(ex.ToString)
            End Try
        End If
        If gvCriteria.Rows.Count > 0 Then
            Return True
        End If
        Return False
    End Function

    Sub LoadCutOffScores()
        Try


            Dim cmdText As String = " select cs.productCode, cs.Grade, cs.PriorityCS, cs.TotalCs, (select min(prioritycs)-1 from CutOffScores where productcode=cs.productcode and contestyear=cs.contestyear) as NewChapterPriorityScore, (select min(TotalCs)-1 from CutOffScores where productcode=cs.productcode and contestyear=cs.contestyear) as NewChapterTotalScore,  (Select MaxScores from MaxScoresByPhase where EventID=1 and Year = 2018 and ProductGroupCode=cs.ProductGroupcode) as MaxScore  from CutOffScores CS inner join ContestCategory CC on (cs.ContestYear=CC.ContestYear and cs.productcode=cc.contestcode)  where  cs.contestyear=" & Year & " and (cc.NationalSelectionCriteria is null or cc.NationalSelectionCriteria ='I') and cc.NationalFinalsStatus = 'Active' order by ContestCategoryId"
            Dim connectionString As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("NSFConnectionString").ToString()
            Dim connection As SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
            connection.Open()
            Dim daContests As SqlDataAdapter = New SqlDataAdapter(cmdText, connection)
            Dim dsContests As DataSet = New DataSet()
            daContests.Fill(dsContests, "ContestCategory")
            Dim prevGrade As Integer = -1
            Dim row As Integer = 0
            For i As Integer = 0 To gvCriteria.Rows.Count - 1
                Dim grade As Integer = 0
                grade = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
                Dim dbGrade As Integer = 0



                If (prevGrade = 0) Then

                    ''Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text = dsContests.Tables(0).Rows()
                Else
                    If (i = 0) Then
                        row = 0
                    Else
                        row = row + 1
                    End If

                End If
                CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text = dsContests.Tables(0).Rows(row)("TotalCs").ToString()
                CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text = dsContests.Tables(0).Rows(row)("TotalCs").ToString()
                CType(gvCriteria.Rows(i).Cells(4).FindControl("txtNewChScore"), TextBox).Text = dsContests.Tables(0).Rows(row)("NewChapterTotalScore").ToString()

                CType(gvCriteria.Rows(i).Cells(5).FindControl("txtMaxScore"), TextBox).Text = dsContests.Tables(0).Rows(row)("MaxScore").ToString()

                CType(GVPriority.Rows(i).Cells(2).FindControl("TextBox2_pr"), TextBox).Text = dsContests.Tables(0).Rows(row)("PriorityCS").ToString()
                CType(GVPriority.Rows(i).Cells(4).FindControl("txtNewChScore_pr"), TextBox).Text = dsContests.Tables(0).Rows(row)("NewChapterPriorityScore").ToString()
                CType(GVPriority.Rows(i).Cells(5).FindControl("txtMaxScore_pr"), TextBox).Text = dsContests.Tables(0).Rows(row)("MaxScore").ToString()

                prevGrade = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Sub ReadContestCategories()
        'connect to the peoducts database
        Dim connectionString As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("NSFConnectionString").ToString()
        ' create and open the connection object
        Dim connection As SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
        connection.Open()
        ' get records from the products table
        Dim commandString As String = "SELECT ContestCode, GradeFrom, GradeTo From ContestCategory WHERE contestyear = " & Year & " and (NationalSelectionCriteria is null or NationalSelectionCriteria ='I') and NationalFinalsStatus = 'Active' order by ContestCategoryID"
        Dim daContests As SqlDataAdapter = New SqlDataAdapter(commandString, connection)
        Dim dsContests As DataSet = New DataSet()
        daContests.Fill(dsContests, "ContestCategory")
        dt = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("ProductCode", Type.GetType("System.String"))
        dt.Columns.Add("Grade", Type.GetType("System.Int32"))
        dt.Columns.Add("MinScore", Type.GetType("System.Int32"))
        dt.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
        dt.Columns.Add("NewChScore", Type.GetType("System.Int32"))
        dt.Columns.Add("MaxScore", Type.GetType("System.Int32"))
        Dim contest As String
        Dim gradeFrom, gradeTo, i As Integer
        For i = 0 To (dsContests.Tables("ContestCategory").Rows.Count - 1)
            contest = dsContests.Tables("ContestCategory").Rows(i).ItemArray(0).ToString()
            gradeFrom = Convert.ToInt32(dsContests.Tables("ContestCategory").Rows(i).ItemArray(1).ToString())
            gradeTo = Convert.ToInt32(dsContests.Tables("ContestCategory").Rows(i).ItemArray(2).ToString())
            While (gradeFrom <= gradeTo)
                dr = dt.NewRow()
                dr("ProductCode") = contest
                dr("Grade") = gradeFrom
                Dim n As Integer = 0
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT   isnull(MinScore,0) as MinScore,isnull(Rank1Score,0) as Rank1Score,isnull(NewChScore,0) as NewChScore,isnull(MaxScore,0) as MaxScore  FROM NationalCriteria WHERE ProductCode = '" & contest & "' AND  ContestYear = " & (Year - 1) & " AND Grade =" & gradeFrom & " and List='Total'")
                While readr.Read()
                    n = 1
                    dr("MinScore") = readr("MinScore")
                    dr("Rank1Score") = readr("Rank1Score")
                    dr("NewChScore") = readr("NewChScore")
                    dr("MaxScore") = readr("MaxScore")
                End While
                If n = 0 Then
                    dr("MinScore") = 0
                    dr("Rank1Score") = 0
                    dr("NewChScore") = 0
                    dr("MaxScore") = 0
                End If
                gradeFrom = gradeFrom + 1
                dt.Rows.Add(dr)
            End While
        Next

        gvCriteria.DataSource = dt
        gvCriteria.DataBind()

        Dim dt3 As DataTable = New DataTable()
        Dim dr1 As DataRow
        dt3.Columns.Add("ProductCode", Type.GetType("System.String"))
        dt3.Columns.Add("Grade", Type.GetType("System.Int32"))
        dt3.Columns.Add("MinScore", Type.GetType("System.Int32"))
        dt3.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
        dt3.Columns.Add("NewChScore", Type.GetType("System.Int32"))
        dt3.Columns.Add("MaxScore", Type.GetType("System.Int32"))
        'Dim i As Integer
        'MsgBox(gvCriteria.Rows.Count)
        Try

            For i = 0 To gvCriteria.Rows.Count - 1
                dr1 = dt3.NewRow()
                dr1("ProductCode") = CType(gvCriteria.Rows(i).Cells(0).FindControl("lblContest"), Label).Text
                dr1("Grade") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
                'CType(e.Item.FindControl("txtTransType"), TextBox).Text
                dr1("Rank1Score") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(3).FindControl("lblRank1Score"), Label).Text) + 2
                dr1("MinScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text) + 2
                dr1("NewChScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(4).FindControl("txtNewChScore"), TextBox).Text) + 2
                dr1("MaxScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(5).FindControl("txtMaxScore"), TextBox).Text)
                dt3.Rows.Add(dr1)
            Next
            GVPriority.DataSource = dt3
            GVPriority.DataBind()

        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        BtnLoadCutOffScores.Visible = False
        btnSave.Enabled = False
        Dim dt2 As DataTable = New DataTable()
        Dim dr As DataRow
        dt2.Columns.Add("ProductCode", Type.GetType("System.String"))
        dt2.Columns.Add("Grade", Type.GetType("System.Int32"))
        dt2.Columns.Add("MinScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
        dt2.Columns.Add("NewChScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("MaxScore", Type.GetType("System.Int32"))
        Dim i As Integer

        Try
            For i = 0 To gvCriteria.Rows.Count - 1
                dr = dt2.NewRow()
                dr("ProductCode") = CType(gvCriteria.Rows(i).Cells(0).FindControl("lblContest"), Label).Text
                dr("Grade") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
                'CType(e.Item.FindControl("txtTransType"), TextBox).Text
                dr("Rank1Score") = CType(gvCriteria.Rows(i).Cells(3).FindControl("lblRank1Score"), Label).Text
                dr("MinScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text)
                dr("NewChScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(4).FindControl("txtNewChScore"), TextBox).Text)
                dr("MaxScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(5).FindControl("txtMaxScore"), TextBox).Text)
                dt2.Rows.Add(dr)
            Next
            SaveToDB(dt2, "Total")

            Dim dt1 As DataTable = New DataTable()
            Dim dr1 As DataRow
            dt1.Columns.Add("ProductCode", Type.GetType("System.String"))
            dt1.Columns.Add("Grade", Type.GetType("System.Int32"))
            dt1.Columns.Add("MinScore", Type.GetType("System.Int32"))
            dt1.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
            dt1.Columns.Add("NewChScore", Type.GetType("System.Int32"))
            dt1.Columns.Add("MaxScore", Type.GetType("System.Int32"))
            Dim j As Integer
            For j = 0 To GVPriority.Rows.Count - 1
                dr1 = dt1.NewRow()
                dr1("ProductCode") = CType(GVPriority.Rows(j).Cells(0).FindControl("lblContest_pr"), Label).Text
                dr1("Grade") = Convert.ToInt32(CType(GVPriority.Rows(j).Cells(1).FindControl("lblGrade_pr"), Label).Text)
                dr1("Rank1Score") = CType(GVPriority.Rows(j).Cells(3).FindControl("lblRank1Score_pr"), Label).Text
                dr1("MinScore") = Convert.ToInt32(CType(GVPriority.Rows(j).Cells(2).FindControl("TextBox2_pr"), TextBox).Text)
                dr1("NewChScore") = Convert.ToInt32(CType(GVPriority.Rows(j).Cells(4).FindControl("txtNewChScore_pr"), TextBox).Text)
                dr1("MaxScore") = Convert.ToInt32(CType(GVPriority.Rows(j).Cells(5).FindControl("txtMaxScore_pr"), TextBox).Text)
                dt1.Rows.Add(dr1)
            Next
            SaveToDB(dt1, "Priority")

            Dim result As Boolean = ReadDataBase()
            btnCalCulate.Visible = True
            'BtnExport.Visible = True
            ddlExport.Items(0).Enabled = True
            lblMessage.Text = "Values saved successfully. Next click View/Calc Invitee Counts button."
            lblMessage.ForeColor = Color.Red

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Sub SaveToDB(ByVal dt As DataTable, ByVal List As String)
        ' Update rows
        Try
            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from NationalCriteria")
            'Find the data already exist for contest year update the old values else add new values
            'get records from the products table
            Dim dr As DataRow
            Dim i As Integer
            For Each dr In dt.Rows
                i = 0
                Dim commandString1 As String = "Update NationalCriteria SET ModifiedDate=GetDate(),MinScore=" & dr("MinScore") & ",NewChScore=" & dr("NewChScore") & ",MaxScore=" & dr("MaxScore") & " WHERE ProductCode='" & dr("ProductCode") & "' AND  Grade=" & dr("Grade") & " AND contestyear=" & Year & " and List='" & List & "'"
                i = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, commandString1)
                If i = 0 Then
                    Dim commandString As String = "INSERT INTO NationalCriteria (ProductCode, Grade, MinScore,Rank1Score,NewChScore,MaxScore,List,CreatedDate,contestyear) Values ("
                    commandString = commandString & "'" & dr("ProductCode") & "'," & dr("Grade") & "," & dr("MinScore") & "," & dr("Rank1Score") & "," & dr("NewChScore") & "," & dr("MaxScore") & ",'" & List & "',GetDate()," & Year & ")"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, commandString)
                End If
            Next

            '' This piece of code to set the Rank1Score and MaxScore
            Dim reader As SqlDataReader
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT C.ContestCode,Min(N.MinScore)As  MinScore,Max(NewChScore) as NewChScore,Max(MaxScore) as MaxScore From ContestCategory C, NationalCriteria N WHERE C.contestyear = " & Year & "  and (c.NationalSelectionCriteria is null or c.NationalSelectionCriteria ='I') and C.NationalFinalsStatus = 'Active' and C.ContestCode=N.ProductCode and C.ContestYear=N.ContestYear and N.List='" & List & "' Group by C.ContestCode")
            While reader.Read()
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update NationalCriteria Set Rank1Score=" & reader("MinScore") & ",MaxScore=" & reader("MaxScore") & " where ProductCode='" & reader("ContestCode") & "' and ContestYear=" & Year & " and List='" & List & "'")
                'Response.Write("<BR>" & "Update NationalCriteria Set Rank1Score=" & reader("MinScore") & ",NewChScore=" & reader("NewChScore") & ",MaxScore=" & reader("MaxScore") & " where contest='" & reader("ContestCode") & "' and ContestYear=" & Year & "")
            End While
            reader.Close()
        Catch ex As Exception
            Response.Write("update failed") ' & ex.ToString())
        End Try
    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            BtnLoadCutOffScores.Visible = False
            lblContestant.Text = "0"
            Dim CCount As New Hashtable()
            Dim CRank1 As New Hashtable()
            Dim reader1 As SqlDataReader
            Dim Rank1Tie(500, 3) As String
            Dim TieIndex As Integer
            TieIndex = 0
            Dim SQLTieQuery As String = " (EventId=1) "
            Dim overalltotal As Integer = 0
            Dim Rank1alltotal As Integer = 0
            Dim NewChapterCount As Integer = 0
            reader1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT ContestCode From ContestCategory WHERE contestyear = " & Year & "  and NationalSelectionCriteria ='I' and NationalFinalsStatus = 'Active' Group by ContestCode")
            While reader1.Read()
                CCount.Add(reader1("ContestCode"), 0)
                CRank1.Add(reader1("ContestCode"), 0)
            End While
            reader1.Close()
            Dim dt2 As DataTable = New DataTable()
            Dim dr As DataRow
            dt2.Columns.Add("ProductCode", Type.GetType("System.String"))
            dt2.Columns.Add("Grade", Type.GetType("System.Int32"))
            dt2.Columns.Add("MinScore", Type.GetType("System.Int32"))
            dt2.Columns.Add("NewChScore", Type.GetType("System.Int32"))
            dt2.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
            dt2.Columns.Add("MaxScore", Type.GetType("System.Int32"))
            dt2.Columns.Add("Count", Type.GetType("System.Int32"))
            dt2.Columns.Add("CumCount", Type.GetType("System.Int32"))
            dt2.Columns.Add("Contestants", Type.GetType("System.Int32"))
            dt2.Columns.Add("Invited", Type.GetType("System.String"))
            dt2.Columns.Add("AddRank1", Type.GetType("System.Int32"))
            dt2.Columns.Add("nwchcount", Type.GetType("System.Int32"))
            dt2.Columns.Add("CumulWrank1", Type.GetType("System.Int32"))
            dt2.Columns.Add("AvgScore", Type.GetType("System.Decimal"))
            Dim i, invitecount, numCntestants, cntRank1, nwchcount, rank1Tiecount As Integer
            Dim AvgScore As Decimal = 0.0
            Dim LastContestName As String = ""

            Dim NwChapIDs As String = "0,"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT ChapterID FROM Chapter WHERE Newchapterflag='Y'")
            For i = 0 To ds.Tables(0).Rows.Count - 1
                NwChapIDs = NwChapIDs & ds.Tables(0).Rows(i)(0).ToString() & ","
            Next
            For i = 0 To gvCriteria.Rows.Count - 1
                dr = dt2.NewRow()
                invitecount = 0
                numCntestants = 0
                cntRank1 = 0
                nwchcount = 0
                rank1Tiecount = 0
                dr("ProductCode") = CType(gvCriteria.Rows(i).Cells(0).FindControl("lblContest"), Label).Text
                dr("Grade") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
                dr("MinScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text)
                dr("Rank1Score") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(3).FindControl("lblRank1Score"), Label).Text)
                dr("NewChScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(4).FindControl("txtNewChScore"), TextBox).Text)
                dr("MaxScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(5).FindControl("txtMaxScore"), TextBox).Text)
                If LastContestName <> dr("ProductCode") Then
                    LastContestName = dr("ProductCode")
                    AvgScore = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Case COUNT(score1)  when  0 then 0 else Round(AVG(score1+ISNULL(score2,0)+ISNULL(score3,0)),2) end from contestant where contestyear=" & Year & " and score1+ISNULL(score2,0)+ISNULL(score3,0)>0 and productcode = '" & dr("ProductCode") & "' AND EventId=2")
                End If
                dr("AvgScore") = AvgScore
                Dim aScore As Integer = 0
                Dim gCount As Integer = 0
                Dim CumCount As Integer = 0
                Dim CumulWrank1 As Integer = 0

                If dr("MinScore") > 0 Then
                    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select ChildNumber, ContestCode, contestant_id, isnull(Score1,0) AS Score1, isnull( Score2,0) AS Score2,isnull(Score3,0) AS Score3, Rank,  BadgeNumber, ContestCategoryID, GRADE, ChapterID  from Contestant   Where ContestYear=" & Year & "  AND (Score1+ISNULL(score2,0)+ISNULL(score3,0))>0 AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND GRADE= " & dr("Grade") & "  ORDER BY ChapterID,Rank")
                    While reader.Read()
                        aScore = reader("Score1") + reader("Score2") + reader("Score3")
                        If aScore >= dr("MinScore") Then
                            invitecount = invitecount + 1
                            lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                        ElseIf reader("Rank") = 1 Then '** aScore >= dr("MinScore") And 
                            'Rank 1 score consideration where total contestant is >= 5
                            If aScore >= dr("Rank1Score") And dr("Rank1Score") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(ChildNumber)  from Contestant  Where (Score1+ISNULL(score2,0)+ISNULL(score3,0)) >0  AND ContestYear=" & Year & "   AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND ChapterID =" & reader("chapterid") & "") >= 5 Then
                                cntRank1 = cntRank1 + 1
                                TieIndex = TieIndex + 1
                                lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                                SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                            ElseIf aScore >= dr("NewChScore") And dr("NewChScore") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(C.ChildNumber)  from Contestant C,Chapter Ch  Where (C.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))>0  AND C.ContestYear=" & Year & "  AND C.ProductCode = '" & dr("ProductCode") & "' AND C.EventId=2 and C.ChapterID = Ch.ChapterID and Ch.NewChapterFlag='Y' AND C.chapterid=" & reader("chapterid") & "") >= 5 Then
                                'New Chapter Contestant where Rank 1 and total contestant is >= 5
                                nwchcount = nwchcount + 1
                                lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                                SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                                TieIndex = TieIndex + 1
                            End If
                        ElseIf NwChapIDs.Contains("," + reader("chapterid").ToString() & ",") Then
                            aScore = aScore + 1
                            If aScore >= dr("MinScore") Then
                                'New Chapter Contestant by Adding 1 
                                nwchcount = nwchcount + 1
                                lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                            End If
                        End If
                        numCntestants = numCntestants + 1
                    End While
                    reader.Close()
                End If
                NewChapterCount = NewChapterCount + nwchcount
                ' if we want new chapter count seperatly we can remove next line and relay on nwchcount and NewChapterCount
                'invitecount = invitecount + nwchcount
                dr("count") = invitecount
                CumCount = Convert.ToInt32(CCount.Item(dr("ProductCode"))) + invitecount
                CCount.Remove(dr("ProductCode"))
                CCount.Add(dr("ProductCode"), CumCount)
                CumulWrank1 = Convert.ToInt32(CRank1.Item(dr("ProductCode"))) + cntRank1 + invitecount + nwchcount
                CRank1.Remove(dr("ProductCode"))
                CRank1.Add(dr("ProductCode"), CumulWrank1)
                dr("CumCount") = CumCount
                dr("Contestants") = numCntestants
                'dr("Invited") = 0 '%
                If (numCntestants > 0) Then
                    dr("Invited") = String.Format("{0:N2}", (invitecount / numCntestants) * 100)
                Else
                    dr("Invited") = "0.00"
                End If
                dr("AddRank1") = cntRank1
                dr("nwchcount") = nwchcount
                dr("CumulWrank1") = CumulWrank1
                Rank1alltotal = Rank1alltotal + cntRank1
                overalltotal = overalltotal + invitecount
                dt2.Rows.Add(dr)
            Next
            If TieIndex > 0 Then
                'Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ChildNumber, contestant_id, (isnull(Score1,0) + isnull( Score2,0) + isnull(Score3,0)) AS TotalScore, Rank,  BadgeNumber, GRADE, ChapterID,ProductID,ProductCode  from Contestant   Where ContestYear=" & Year & "  AND (" & SQLTieQuery & ")  AND EventId=2 AND contestant_id not in (" & lblContestant.Text & ")  ORDER BY ProductID,ChapterID,Rank")
                Dim strText As String = "Select ChildNumber, contestant_id, (isnull(Score1,0) + isnull( Score2,0) + isnull(Score3,0)) AS TotalScore, Rank,  BadgeNumber, GRADE, ChapterID,ProductID,ProductCode  from Contestant ct  Where ContestYear=" & Year & "  AND (" & SQLTieQuery & ")  AND EventId=2 "
                strText = strText & " and not exists ( select contestant_id from contestant ct1 where ct1.contestant_id=ct.contestant_id and ct1.contestant_id in (" & lblContestant.Text & "))  ORDER BY ProductID,ChapterID,Rank"
                Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strText)
                Dim jk As Integer
                For jk = 0 To ds1.Tables(0).Rows.Count - 1
                    rank1Tiecount = rank1Tiecount + 1
                    'Response.Write(Rank1Tie(ii, 0) & " " & Rank1Tie(ii, 1) & " ch :" & Rank1Tie(ii, 2))
                    lblContestant.Text = lblContestant.Text & "," & ds1.Tables(0).Rows(jk)("contestant_id")
                    GVtieBreaker.Visible = True
                Next
                'lblTieBreaker.Text = "Rank1 tie contestants(Total List)"
                GVtieBreaker.DataSource = ds1
                GVtieBreaker.DataBind()
                Session("GVtieBreaker") = ds1
            End If

            Dim totCount, TotContestant, totRank1 As Integer ',totwRank1
            totCount = 0
            TotContestant = 0
            totRank1 = 0
            'totwRank1 = 0
            dr = dt2.NewRow()
            dr("ProductCode") = "Total"
            dr("Grade") = 0
            dr("MinScore") = 0
            dr("Rank1Score") = 0
            dr("NewChScore") = 0
            dr("MaxScore") = 0
            dr("AvgScore") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ISNULL(Round(AVG(ISNULL(score1,0)+ISNULL(score2,0)+ISNULL(score3,0)),2), 0) from contestant where contestyear=" & Year & " and score1+ISNULL(score2,0)+ISNULL(score3,0)>0 ")
            For i = 0 To dt2.Rows.Count - 1
                totCount = totCount + dt2.Rows(i).Item("count")
                TotContestant = TotContestant + dt2.Rows(i).Item("Contestants")
                totRank1 = totRank1 + dt2.Rows(i).Item("AddRank1")
            Next
            dr("count") = totCount
            dr("CumCount") = totCount
            dr("Contestants") = TotContestant
            dr("Invited") = String.Format("{0:N2}", (totCount / TotContestant) * 100)
            dr("AddRank1") = totRank1
            dr("nwchcount") = NewChapterCount
            dr("CumulWrank1") = totRank1 + totCount + NewChapterCount
            dt2.Rows.Add(dr)
            ViewState("gvOut") = dt2
            gvOut.DataSource = dt2
            gvOut.DataBind()
            gvOut.Visible = True
            CalculatePriorityList()
            CalculateRatio()
            btnSave.Visible = False
            btnNewChapter.Visible = False
            btnCalCulate.Visible = False
            gvCriteria.Visible = False
            GVPriority.Visible = False
            btnUpdateCnst.Visible = True
            ddlInvite.Visible = True
            ddlNatInvite.Visible = True
            btnNatInvite.Visible = True
            BtnExport.Visible = True
            ddlExport.Items(0).Enabled = False
            ddlExport.Items(1).Enabled = True
            ddlExport.Items(2).Enabled = True
            ddlExport.Items(3).Enabled = True
            ddlExport.Items(4).Enabled = False
            btnSaveInviteeCount.Visible = True
            lblMessage.Text = "<b>Total List -" & Year & "</b> <br />Grand Total: " & overalltotal & " with addl rank1 holders total is: " & (overalltotal + Rank1alltotal + NewChapterCount + rank1Tiecount) & "<br> Total New Chapter contestant count : " & NewChapterCount & "<br> Total Rank1 tie contestant count :" & rank1Tiecount
            lblMessage.ForeColor = Color.Black

        Catch ex As Exception
            lblMessage.Text = "<br />Error in calculation:" & ex.ToString

        End Try
    End Sub
    Private Sub CalculatePriorityList()
        lblContestants_Priority.Text = "0"
        Dim CCount As New Hashtable()
        Dim CRank1 As New Hashtable()
        Dim reader1 As SqlDataReader
        Dim Rank1Tie(500, 3) As String
        Dim TieIndex As Integer
        TieIndex = 0
        Dim SQLTieQuery As String = " (EventId=1) "
        Dim overalltotal As Integer = 0
        Dim Rank1alltotal As Integer = 0
        Dim NewChapterCount As Integer = 0
        reader1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT ContestCode From ContestCategory WHERE contestyear = " & Year & "  and NationalSelectionCriteria ='I' and NationalFinalsStatus = 'Active' Group by ContestCode")
        While reader1.Read()
            CCount.Add(reader1("ContestCode"), 0)
            CRank1.Add(reader1("ContestCode"), 0)
        End While
        reader1.Close()
        Dim dt2 As DataTable = New DataTable()
        Dim dr As DataRow
        dt2.Columns.Add("ProductCode", Type.GetType("System.String"))
        dt2.Columns.Add("Grade", Type.GetType("System.Int32"))
        dt2.Columns.Add("MinScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("NewChScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
        dt2.Columns.Add("MaxScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Count", Type.GetType("System.Int32"))
        dt2.Columns.Add("CumCount", Type.GetType("System.Int32"))
        dt2.Columns.Add("Contestants", Type.GetType("System.Int32"))
        dt2.Columns.Add("Invited", Type.GetType("System.String"))
        dt2.Columns.Add("AddRank1", Type.GetType("System.Int32"))
        dt2.Columns.Add("nwchcount", Type.GetType("System.Int32"))
        dt2.Columns.Add("CumulWrank1", Type.GetType("System.Int32"))
        dt2.Columns.Add("AvgScore", Type.GetType("System.Decimal"))
        Dim i, invitecount, numCntestants, cntRank1, nwchcount, rank1Tiecount As Integer
        Dim AvgScore As Decimal = 0.0
        Dim LastContestName As String = ""

        Dim NwChapIDs As String = "0,"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT ChapterID FROM Chapter WHERE Newchapterflag='Y'")
        For i = 0 To ds.Tables(0).Rows.Count - 1
            NwChapIDs = NwChapIDs & ds.Tables(0).Rows(i)(0).ToString() & ","
        Next
        For i = 0 To GVPriority.Rows.Count - 1
            dr = dt2.NewRow()
            invitecount = 0
            numCntestants = 0
            cntRank1 = 0
            nwchcount = 0
            rank1Tiecount = 0
            dr("ProductCode") = CType(GVPriority.Rows(i).Cells(0).FindControl("lblContest_pr"), Label).Text
            dr("Grade") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(1).FindControl("lblGrade_pr"), Label).Text)
            'CType(e.Item.FindControl("txtTransType"), TextBox).Text
            dr("MinScore") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(2).FindControl("TextBox2_pr"), TextBox).Text)
            dr("Rank1Score") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(3).FindControl("lblRank1Score_pr"), Label).Text)
            dr("NewChScore") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(4).FindControl("txtNewChScore_pr"), TextBox).Text)
            dr("MaxScore") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(5).FindControl("txtMaxScore_pr"), TextBox).Text)
            If LastContestName <> dr("ProductCode") Then
                LastContestName = dr("ProductCode")
                AvgScore = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Case COUNT(score1)  when  0 then 0 else Round(AVG(score1+ISNULL(score2,0)+ISNULL(score3,0)),2) end from contestant where contestyear=" & Year & " and score1+ISNULL(score2,0)+ISNULL(score3,0)>0 and productcode = '" & dr("ProductCode") & "' AND EventId=2")
            End If
            dr("AvgScore") = AvgScore
            Dim aScore As Integer = 0
            Dim gCount As Integer = 0
            Dim CumCount As Integer = 0
            Dim CumulWrank1 As Integer = 0

            If dr("MinScore") > 0 Then
                Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select ChildNumber, ContestCode, contestant_id, isnull(Score1,0) AS Score1, isnull( Score2,0) AS Score2,isnull(Score3,0) AS Score3, Rank,  BadgeNumber, ContestCategoryID, GRADE, ChapterID  from Contestant   Where ContestYear=" & Year & "  AND (Score1+ISNULL(score2,0)+ISNULL(score3,0))>0 AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND GRADE= " & dr("Grade") & "  ORDER BY ChapterID,Rank")
                While reader.Read()
                    aScore = reader("Score1") + reader("Score2") + reader("Score3")
                    If aScore >= dr("MinScore") Then
                        invitecount = invitecount + 1
                        lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                    ElseIf reader("Rank") = 1 Then '** aScore >= dr("MinScore") And 
                        'Rank 1 score consideration where total contestant is >= 5
                        If aScore >= dr("Rank1Score") And dr("Rank1Score") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(ChildNumber)  from Contestant  Where (Score1+ISNULL(score2,0)+ISNULL(score3,0)) >0  AND ContestYear=" & Year & "   AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND ChapterID =" & reader("chapterid") & "") >= 5 Then
                            cntRank1 = cntRank1 + 1
                            TieIndex = TieIndex + 1
                            lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                            SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                        ElseIf aScore >= dr("NewChScore") And dr("NewChScore") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(C.ChildNumber)  from Contestant C,Chapter Ch  Where (C.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))>0  AND C.ContestYear=" & Year & "  AND C.ProductCode = '" & dr("ProductCode") & "' AND C.EventId=2 and C.ChapterID = Ch.ChapterID and Ch.NewChapterFlag='Y' AND C.chapterid=" & reader("chapterid") & "") >= 5 Then
                            'New Chapter Contestant where Rank 1 and total contestant is >= 5
                            nwchcount = nwchcount + 1
                            lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                            SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                            TieIndex = TieIndex + 1
                        End If
                    ElseIf NwChapIDs.Contains("," + reader("chapterid").ToString() & ",") Then
                        aScore = aScore + 1
                        If aScore >= dr("MinScore") Then
                            'New Chapter Contestant by Adding 1 
                            nwchcount = nwchcount + 1
                            lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                        End If
                    End If
                    numCntestants = numCntestants + 1
                End While
                reader.Close()
            End If
            NewChapterCount = NewChapterCount + nwchcount
            ' if we want new chapter count seperatly we can remove next line and relay on nwchcount and NewChapterCount
            'invitecount = invitecount + nwchcount
            dr("count") = invitecount
            CumCount = Convert.ToInt32(CCount.Item(dr("ProductCode"))) + invitecount
            CCount.Remove(dr("ProductCode"))
            CCount.Add(dr("ProductCode"), CumCount)
            CumulWrank1 = Convert.ToInt32(CRank1.Item(dr("ProductCode"))) + cntRank1 + invitecount + nwchcount
            CRank1.Remove(dr("ProductCode"))
            CRank1.Add(dr("ProductCode"), CumulWrank1)
            dr("CumCount") = CumCount
            dr("Contestants") = numCntestants
            'dr("Invited") = 0 '%
            If (numCntestants > 0) Then
                dr("Invited") = String.Format("{0:N2}", (invitecount / numCntestants) * 100)
            Else
                dr("Invited") = "0.00"
            End If
            dr("AddRank1") = cntRank1
            'dr("TotalWrank1") = cntRank1 + invitecount
            dr("nwchcount") = nwchcount
            dr("CumulWrank1") = CumulWrank1
            Rank1alltotal = Rank1alltotal + cntRank1
            overalltotal = overalltotal + invitecount
            dt2.Rows.Add(dr)
        Next

        If TieIndex > 0 Then

            Dim strTextTieBreaker As String = "Select ChildNumber, contestant_id, (isnull(Score1,0) + isnull( Score2,0) + isnull(Score3,0)) AS TotalScore, Rank,  BadgeNumber, GRADE, ChapterID,ProductID,ProductCode  from Contestant ct   Where ContestYear=" & Year & "  AND (" & SQLTieQuery & ")  AND EventId=2 "
            strTextTieBreaker = strTextTieBreaker & " AND not exists ( select contestant_id from contestant ct1 where ct1.contestant_id=ct.contestant_id and ct1.contestant_id in (" & lblContestants_Priority.Text & "))  ORDER BY ProductID,ChapterID,Rank"
            ' strTextTieBreaker = strTextTieBreaker & " AND contestant_id not in (" & lblContestants_Priority.Text & ")  ORDER BY ProductID,ChapterID,Rank"
            '  Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ChildNumber, contestant_id, (isnull(Score1,0) + isnull( Score2,0) + isnull(Score3,0)) AS TotalScore, Rank,  BadgeNumber, GRADE, ChapterID,ProductID,ProductCode  from Contestant   Where ContestYear=" & Year & "  AND (" & SQLTieQuery & ")  AND EventId=2 AND contestant_id not in (" & lblContestants_Priority.Text & ")  ORDER BY ProductID,ChapterID,Rank")
            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strTextTieBreaker)
            Dim jk As Integer
            For jk = 0 To ds1.Tables(0).Rows.Count - 1
                rank1Tiecount = rank1Tiecount + 1
                lblContestants_Priority.Text = lblContestants_Priority.Text & "," & ds1.Tables(0).Rows(jk)("contestant_id")
                GVTieBreakerRank.Visible = True
            Next
            'lblTieBreakerRank.Text = "Rank1 tie contestants(Priority List)"
            GVTieBreakerRank.DataSource = ds1
            GVTieBreakerRank.DataBind()
            Session("GVtieBreakerRank") = ds1
        End If

        Dim totCount, TotContestant, totRank1 As Integer ',totwRank1
        totCount = 0
        TotContestant = 0
        totRank1 = 0
        'totwRank1 = 0
        dr = dt2.NewRow()
        dr("ProductCode") = "Total"
        dr("Grade") = 0
        dr("MinScore") = 0
        dr("Rank1Score") = 0
        dr("NewChScore") = 0
        dr("MaxScore") = 0
        dr("AvgScore") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ISNULL(Round(AVG(ISNULL(score1,0)+ISNULL(score2,0)+ISNULL(score3,0)),2), 0) from contestant where contestyear=" & Year & " and score1+ISNULL(score2,0)+ISNULL(score3,0)>0 ")
        For i = 0 To dt2.Rows.Count - 1
            totCount = totCount + dt2.Rows(i).Item("count")
            TotContestant = TotContestant + dt2.Rows(i).Item("Contestants")
            totRank1 = totRank1 + dt2.Rows(i).Item("AddRank1")
            ' totwRank1 = totwRank1 + dt2.Rows(i).Item("TotalWrank1")
        Next
        dr("count") = totCount
        dr("CumCount") = totCount
        dr("Contestants") = TotContestant
        dr("Invited") = String.Format("{0:N2}", (totCount / TotContestant) * 100)
        dr("AddRank1") = totRank1
        'dr("TotalWrank1") = totRank1 + totCount
        dr("nwchcount") = NewChapterCount
        dr("CumulWrank1") = totRank1 + totCount + NewChapterCount
        dt2.Rows.Add(dr)
        lblPriority.Text = "<b>Priority List - " & Year & "</b><br/> Grand Total: " & overalltotal & " with addl rank1 holders total is: " & (overalltotal + Rank1alltotal + NewChapterCount + rank1Tiecount) & "<br> Total New Chapter contestant count : " & NewChapterCount & "<br> Total Rank1 tie contestant count :" & rank1Tiecount
        ViewState("gvOutPriority") = dt2
        gvOutPriority.DataSource = dt2
        gvOutPriority.DataBind()
        gvOutPriority.Visible = True
    End Sub
    Protected Sub btnYear2Year_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMessage.ForeColor = Color.Red
        Try
            Dim SQL As String = "SELECT N.ProductCode, N.Grade, N.MinScore,IC.MinScore ,IC.count as Invited,ISNUll(LIC.MinScore,0) as LMinScore,ISNUll(LIC.count,0) as LInvited,ISNUll(Reg.RegisCnt,0) AS RegisCount,ISNUll(Shows.RegisCnt,0) AS ShowsCount, CASE WHEN LIC.count> 0 AND Reg.RegisCnt IS NOT NULL then ((Reg.RegisCnt+0.0)/(LIC.count+0.0))*100 ELSE 0.0 END as PersRegis, CASE WHEN Reg.RegisCnt is NOT NUll AND Shows.RegisCnt is NOT NULL THEN ((Shows.RegisCnt+0.0)/(Reg.RegisCnt+0.0))*100 ELSE 0.00 END as PersShows "
            SQL = SQL & " from NationalCriteria N Inner Join contestcategory C ON N.ProductCode = C.contestcode and N.ContestYear=C.ContestYear "
            SQL = SQL & " Inner Join InviteeCount IC ON IC.ContestYear = N.ContestYear AND IC.ProductCode = N.ProductCode and IC.Grade = N.Grade and IC.List=N.List"
            SQL = SQL & " Left Join InviteeCount LIC ON LIC.ContestYear = (N.ContestYear - 1) AND LIC.ProductCode = N.ProductCode and LIC.Grade = N.Grade and LIC.List=N.List"
            SQL = SQL & " Left Join (Select COUNT(contestant_ID) as RegisCnt,Grade,productCode,Contestyear from Contestant where ContestYear = " & Val(Year) - 1 & " AND PaymentReference is not null and EventId= 1 Group by Grade,productCode,Contestyear) Reg On Reg.ContestYear = LIC.ContestYear AND Reg.ProductCode = Lic.ProductCode AND Reg.Grade = Lic.Grade"
            SQL = SQL & " Left Join (Select COUNT(contestant_ID) as RegisCnt,Grade,productCode,Contestyear from Contestant where ContestYear = " & Val(Year) - 1 & " AND PaymentReference is not null and (Score1+ISNULL(score2,0)+ISNULL(score3,0))>0 and EventId= 1 Group by Grade,productCode,Contestyear) Shows On Shows.ContestYear = LIC.ContestYear AND Shows.ProductCode = Lic.ProductCode AND Shows.Grade = Lic.Grade"
            SQL = SQL & " where c.contestyear = " & Year & " and (c.NationalSelectionCriteria is null or c.NationalSelectionCriteria ='I') and c.NationalFinalsStatus = 'Active' and N.list='Total' order by C.ContestCategoryID"
            Dim dsY2Y As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQL)
            Dim dt2 As DataTable = New DataTable()
            Dim dr As DataRow
            dt2.Columns.Add("ProductCode", Type.GetType("System.String"))
            dt2.Columns.Add("Grade", Type.GetType("System.Int32"))
            dt2.Columns.Add("MinScore", Type.GetType("System.Int32"))
            dt2.Columns.Add("Invitees", Type.GetType("System.Int32"))
            dt2.Columns.Add("ExRegis", Type.GetType("System.Int32"))
            dt2.Columns.Add("ExpShows", Type.GetType("System.Int32"))
            dt2.Columns.Add("lastMinScore", Type.GetType("System.Int32"))
            dt2.Columns.Add("LasInvitees", Type.GetType("System.Int32"))
            dt2.Columns.Add("LastRegis", Type.GetType("System.Int32"))
            dt2.Columns.Add("LastShows", Type.GetType("System.String"))
            dt2.Columns.Add("RegisPers", Type.GetType("System.Decimal"))
            dt2.Columns.Add("ShowsPers", Type.GetType("System.Decimal"))
            Dim i As Integer
            For i = 0 To dsY2Y.Tables(0).Rows.Count - 1
                dr = dt2.NewRow()
                dr("ProductCode") = dsY2Y.Tables(0).Rows(i)("ProductCode")
                dr("Grade") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("Grade"))
                dr("MinScore") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("MinScore"))
                dr("Invitees") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("Invited"))
                dr("lastMinScore") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("LMinScore"))
                dr("LasInvitees") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("LInvited"))
                dr("LastRegis") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("RegisCount"))
                dr("LastShows") = Convert.ToInt32(dsY2Y.Tables(0).Rows(i)("ShowsCount"))
                dr("RegisPers") = String.Format("{0:N2}", dsY2Y.Tables(0).Rows(i)("PersRegis"))
                dr("ShowsPers") = String.Format("{0:N2}", dsY2Y.Tables(0).Rows(i)("PersShows"))
                Try
                    dr("ExRegis") = dr("Invitees") * ((dr("LastRegis") + 0.0) / (dr("LasInvitees") + 0.0))
                Catch ex As Exception
                    dr("ExRegis") = 0
                End Try
                Try
                    dr("ExpShows") = dr("ExRegis") * ((dr("LastShows") + 0.0) / (dr("LastRegis") + 0.0))
                Catch ex As Exception
                    dr("ExpShows") = 0
                End Try

                dt2.Rows.Add(dr)
            Next

            Dim Invitees, LasInvitees, LastRegis, LastShows, ExRegis, ExpShows As Integer
            Invitees = 0
            LasInvitees = 0
            LastRegis = 0
            LastShows = 0
            ExRegis = 0
            ExpShows = 0
            For i = 0 To dt2.Rows.Count - 1
                Invitees = Invitees + dt2.Rows(i).Item("Invitees")
                LasInvitees = LasInvitees + dt2.Rows(i).Item("LasInvitees")
                LastRegis = LastRegis + dt2.Rows(i).Item("LastRegis")
                LastShows = LastShows + dt2.Rows(i).Item("LastShows")
                ExRegis = ExRegis + dt2.Rows(i).Item("ExRegis")
                ExpShows = ExpShows + dt2.Rows(i).Item("ExpShows")
            Next
            dr = dt2.NewRow()
            dr("ProductCode") = "Total"
            dr("Grade") = DBNull.Value
            dr("MinScore") = DBNull.Value
            dr("Invitees") = Invitees
            dr("lastMinScore") = DBNull.Value
            dr("LasInvitees") = LasInvitees
            dr("LastRegis") = LastRegis
            dr("LastShows") = LastShows
            Try
                dr("RegisPers") = (LastRegis + 0.0) / (LasInvitees + 0.0) * 100
            Catch ex As Exception
                dr("RegisPers") = 0
            End Try
            Try
                dr("ShowsPers") = (LastShows + 0.0) / (LastRegis + 0.0) * 100
            Catch ex As Exception
                dr("ShowsPers") = 0
            End Try
            dr("ExRegis") = ExRegis
            dr("ExpShows") = ExpShows
            dt2.Rows.Add(dr)

            ViewState("gvY2Y") = dt2
            gvY2Y.DataSource = dt2
            gvY2Y.DataBind()
            If dt2.Rows.Count > 0 Then
                gvY2Y.Visible = True
                btnY2YHelp.Visible = True
                gvOut.Visible = False
                gvOutPriority.Visible = False
                gvRatio.Visible = False
                'btnExportY2Y.Visible = True
                ddlExport.Items(0).Enabled = False
                ddlExport.Items(1).Enabled = False
                ddlExport.Items(2).Enabled = False
                ddlExport.Items(3).Enabled = False
                ddlExport.Items(4).Enabled = True

                gvCriteria.Visible = False
                GVPriority.Visible = False
            Else
                btnY2YHelp.Visible = False
                lblMessage.Text = "Please chech whether invitee count is saved for current year."
            End If
        Catch ex As Exception
            lblMessage.Text = "Error in calculation." & ex.ToString
        End Try
    End Sub

    Protected Sub btnNewChapter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ListSelected.Items.Clear()
        BtnLoadCutOffScores.Visible = False
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        If listAll.Items.Count = 0 Then
            'listAll.Items.Clear()            
            dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select * from Chapter where status ='A' order by state Desc,chaptercode Desc")
            While dsRecords.Read
                listAll.Items.Insert(++i, New ListItem(dsRecords("ChapterCode"), dsRecords("ChapterId")))
            End While
        End If
        i = 0
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select * from Chapter where NewChapterFlag ='Y' order by state Desc,chaptercode Desc")
        While dsRecords.Read
            ListSelected.Items.Insert(++i, New ListItem(dsRecords("ChapterCode"), dsRecords("ChapterId")))
        End While
        pnlNewChapter.Visible = True

    End Sub
    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If listAll.SelectedIndex >= 0 Then
            Dim i As Integer
            For i = 0 To listAll.Items.Count - 1
                If listAll.Items(i).Selected Then
                    If Not ListSelected.Items.Contains(listAll.Items(i)) Then
                        ListSelected.Items.Add(listAll.Items(i))
                    End If
                End If
            Next i
        End If
    End Sub
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ListSelected.SelectedIndex >= 0 Then
            Dim i, j As Integer
            j = ListSelected.Items.Count - 1
            For i = 0 To j
                If ListSelected.Items(i).Selected Then
                    ListSelected.Items.Remove(ListSelected.Items(i))
                    j = j - 1
                    i = i - 1
                End If
                If i = j Then
                    Exit For
                End If
            Next i
        End If

    End Sub
    Protected Sub BtnDeleteAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ListSelected.Items.Clear()
    End Sub
    Protected Sub BtnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMessage.ForeColor = Color.Red
        'update Chapters Table
        If ListSelected.Items.Count > 0 Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update chapter set newchapterflag='N'")
            Dim i As Integer
            Dim Chapterid As String = "0"
            For i = 0 To ListSelected.Items.Count - 1
                'MsgBox(ListSelected.Items(i).Value)
                Chapterid = Chapterid & "," & ListSelected.Items(i).Value.ToString()
            Next i
            'MsgBox(Chapterid)
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update chapter set newchapterflag='Y', modifydate = GetDate() where ChapterID in (" & Chapterid & ")")
            lblMessage.ForeColor = Color.Blue
            lblMessage.Text = "New Chapter Flag Updated Successfully"
        Else
            lblMessage.Text = "New Chapter Flag is not updated"
        End If
        pnlNewChapter.Visible = False
    End Sub
    Protected Sub btnUpdateCnst_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblUpdateMsg.ForeColor = Color.Red
        lblerr.Text = ""
        lblUpdateMsg.Text = ""
        If ddlInvite.SelectedIndex = 0 Then
            lblUpdateMsg.Text = "Please select option"
            Exit Sub
        Else

            If ddlInvite.SelectedValue = "1" Then
                If lblContestants_Priority.Text.Length > 4 Then
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant Set PriorityInvite=null where ContestYear=" & Year & "; Update Contestant Set PriorityInvite = 1,ModifiedDate=GetDate() Where contestant_id in (" & lblContestants_Priority.Text & ") and ContestYear=" & Year) > 0 Then
                        lblUpdateMsg.ForeColor = Color.Blue
                        lblUpdateMsg.Text = "Updated Priority Invite column"
                    Else
                        lblUpdateMsg.Text = "Sorry Contestant table not updated for Priority List."
                    End If
                Else
                    lblUpdateMsg.Text = "Sorry Contestant table not updated for Priority List."
                End If

            ElseIf ddlInvite.SelectedValue = "2" Then
                If lblContestant.Text.Length > 4 Then
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant Set TotInvite=null where ContestYear=" & Year & "; Update Contestant Set TotInvite=1,ModifiedDate=GetDate() Where contestant_id in (" & lblContestant.Text & ") and ContestYear=" & Year) > 0 Then
                        lblUpdateMsg.ForeColor = Color.Blue
                        lblUpdateMsg.Text = "Updated Total Invite column"
                    Else
                        lblUpdateMsg.Text = "Sorry Contestant table not updated for Total List."
                    End If
                Else
                    lblUpdateMsg.Text = "Sorry Contestant table not updated for Total List."
                End If
            End If
        End If
    End Sub

    Private Sub ExportIviteeCount()
        BtnLoadCutOffScores.Visible = False
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet = Nothing
        Dim oSheet1 As IWorksheet = Nothing
        oSheet = oWorkbooks.Worksheets.Add()

        oSheet.Name = "NatSelCount"

        oSheet.Range("A1:K1").MergeCells = True
        oSheet.Range("A1").Value = "Total List "
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True
        oSheet.Range("A1").Font.Color = Color.Black

        oSheet.Range("M1:N1").MergeCells = True
        oSheet.Range("M1").Value = "Ratio"
        oSheet.Range("M1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("M1").Font.Bold = True
        oSheet.Range("M1").Font.Color = Color.Black

        oSheet.Range("P1:Z1").MergeCells = True
        oSheet.Range("P1").Value = "Priority List"
        oSheet.Range("P1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("P1").Font.Bold = True
        oSheet.Range("P1").Font.Color = Color.Black

        oSheet.Range("A2:Z2").Font.Color = Color.White
        oSheet.Range("A2:K2").Interior.Color = Color.Blue
        oSheet.Range("M2:N2").Interior.Color = Color.Blue
        oSheet.Range("P2:Z2").Interior.Color = Color.Blue

        oSheet.Range("A2:Z2").ColumnWidth = 10
        oSheet.Range("A2:Z2").RowHeight = 20
        oSheet.Range("A2:Z2").WrapText = True
        oSheet.Range("A2:Z2").Columns.VerticalAlignment = XlVAlign.xlVAlignCenter
        oSheet.Range("A2:Z2").Columns.HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A2:Z2").Font.Bold = True

        oSheet.Range("A2").Value = "Contest"
        oSheet.Range("B2").Value = "Grade"
        oSheet.Range("C2").Value = "Min Score"
        oSheet.Range("D2").Value = "Count"
        oSheet.Range("E2").Value = "Cumulative"
        oSheet.Range("F2").Value = "# of Contestants"
        oSheet.Range("G2").Value = "% Invited"
        oSheet.Range("H2").Value = "Incr. Rank 1"
        oSheet.Range("I2").Value = "Incr. New Chap"
        oSheet.Range("J2").Value = "Cum with Rank 1/New Chap"
        oSheet.Range("K2").Value = "Avg.Score"

        oSheet.Range("M2").Value = "Ratio1"
        oSheet.Range("N2").Value = "Ratio2"

        oSheet.Range("P2").Value = "Contest"
        oSheet.Range("Q2").Value = "Grade"
        oSheet.Range("R2").Value = "Min Score"
        oSheet.Range("S2").Value = "Count"
        oSheet.Range("T2").Value = "Cumulative"
        oSheet.Range("U2").Value = "# of Contestants"
        oSheet.Range("V2").Value = "% Invited"
        oSheet.Range("W2").Value = "Incr. Rank 1"
        oSheet.Range("X2").Value = "Incr. New Chap"
        oSheet.Range("Y2").Value = "Cum with Rank 1/New Chap"
        oSheet.Range("Z2").Value = "Avg.Score"

        Dim dr As DataRow
        Dim iRowIndex As Integer = 3
        Dim CRange As IRange = Nothing
        Dim dt As DataTable
        If Not (ViewState("gvOut") Is Nothing) Then
            dt = ViewState("gvOut")
            For Each dr In dt.Rows
                CRange = oSheet.Range("A" + iRowIndex.ToString())
                CRange.Value = dr("ProductCode")

                CRange = oSheet.Range("B" + iRowIndex.ToString())
                CRange.Value = dr("Grade")

                CRange = oSheet.Range("C" + iRowIndex.ToString())
                CRange.Value = dr("MinScore")

                CRange = oSheet.Range("D" + iRowIndex.ToString())
                CRange.Value = dr("Count")

                CRange = oSheet.Range("E" + iRowIndex.ToString())
                CRange.Value = dr("CumCount")

                CRange = oSheet.Range("F" + iRowIndex.ToString())
                CRange.Value = dr("Contestants")

                CRange = oSheet.Range("G" + iRowIndex.ToString())
                CRange.Value = dr("Invited")

                CRange = oSheet.Range("H" + iRowIndex.ToString())
                CRange.Value = dr("AddRank1")

                CRange = oSheet.Range("I" + iRowIndex.ToString())
                CRange.Value = dr("nwchcount")

                CRange = oSheet.Range("J" + iRowIndex.ToString())
                CRange.Value = dr("CumulWrank1")

                CRange = oSheet.Range("K" + iRowIndex.ToString())
                CRange.Value = dr("AvgScore")

                iRowIndex = iRowIndex + 1
            Next
        End If

        iRowIndex = 3
        If Not (ViewState("gvRatio") Is Nothing) Then
            dt = ViewState("gvRatio")
            For Each dr In dt.Rows
                CRange = oSheet.Range("M" + iRowIndex.ToString())
                CRange.Value = dr("Ratio1")

                CRange = oSheet.Range("N" + iRowIndex.ToString())
                CRange.Value = dr("Ratio2")

                iRowIndex = iRowIndex + 1
            Next
        End If
        iRowIndex = 3
        If Not (ViewState("gvOutPriority") Is Nothing) Then
            dt = ViewState("gvOutPriority")
            For Each dr In dt.Rows
                CRange = oSheet.Range("P" + iRowIndex.ToString())
                CRange.Value = dr("ProductCode")

                CRange = oSheet.Range("Q" + iRowIndex.ToString())
                CRange.Value = dr("Grade")

                CRange = oSheet.Range("R" + iRowIndex.ToString())
                CRange.Value = dr("MinScore")

                CRange = oSheet.Range("S" + iRowIndex.ToString())
                CRange.Value = dr("Count")

                CRange = oSheet.Range("T" + iRowIndex.ToString())
                CRange.Value = dr("CumCount")

                CRange = oSheet.Range("U" + iRowIndex.ToString())
                CRange.Value = dr("Contestants")

                CRange = oSheet.Range("V" + iRowIndex.ToString())
                CRange.Value = dr("Invited")

                CRange = oSheet.Range("W" + iRowIndex.ToString())
                CRange.Value = dr("AddRank1")

                CRange = oSheet.Range("X" + iRowIndex.ToString())
                CRange.Value = dr("nwchcount")

                CRange = oSheet.Range("Y" + iRowIndex.ToString())
                CRange.Value = dr("CumulWrank1")

                CRange = oSheet.Range("Z" + iRowIndex.ToString())
                CRange.Value = dr("AvgScore")
                iRowIndex = iRowIndex + 1
            Next
        End If


        iRowIndex = iRowIndex + 1

        oSheet.Range("A" & iRowIndex & ":I" & iRowIndex).MergeCells = True
        oSheet.Range("A" & iRowIndex).Value = "Rank1 Tie Contestants(Total List)"
        oSheet.Range("A" & iRowIndex).HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A" & iRowIndex).Font.Bold = True
        oSheet.Range("P" & iRowIndex & ":X" & iRowIndex).MergeCells = True
        oSheet.Range("P" & iRowIndex).Value = "Rank1 Tie Contestants (Priority List)"
        oSheet.Range("P" & iRowIndex).HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("P" & iRowIndex).Font.Bold = True

        'ChildNumber	contestant_id	TotalScore	Rank	BadgeNumber	GRADE	ChapterID	ProductID	ProductCode
        iRowIndex = iRowIndex + 1
        oSheet.Range("A" & iRowIndex).Value = "ChildNumber"
        oSheet.Range("B" & iRowIndex).Value = "contestant_id"
        oSheet.Range("C" & iRowIndex).Value = "TotalScore"
        oSheet.Range("D" & iRowIndex).Value = "Rank"
        oSheet.Range("E" & iRowIndex).Value = "BadgeNumber"
        oSheet.Range("F" & iRowIndex).Value = "GRADE"
        oSheet.Range("G" & iRowIndex).Value = "ChapterID"
        oSheet.Range("H" & iRowIndex).Value = "ProductID"
        oSheet.Range("I" & iRowIndex).Value = "ProductCode"

        oSheet.Range("P" & iRowIndex).Value = "ChildNumber"
        oSheet.Range("Q" & iRowIndex).Value = "contestant_id"
        oSheet.Range("R" & iRowIndex).Value = "TotalScore"
        oSheet.Range("S" & iRowIndex).Value = "Rank"
        oSheet.Range("T" & iRowIndex).Value = "BadgeNumber"
        oSheet.Range("U" & iRowIndex).Value = "GRADE"
        oSheet.Range("V" & iRowIndex).Value = "ChapterID"
        oSheet.Range("W" & iRowIndex).Value = "ProductID"
        oSheet.Range("X" & iRowIndex).Value = "ProductCode"
        iRowIndex = iRowIndex + 1
        Dim dstie As DataSet
        Dim iTieRowIndex As Integer = iRowIndex
        If Not Session("GVtieBreaker") Is Nothing Then
            dstie = Session("GVtieBreaker")
            dt = dstie.Tables(0)
            For Each dr In dt.Rows
                CRange = oSheet.Range("A" & iRowIndex)
                CRange.Value = dr("ChildNumber")
                CRange = oSheet.Range("B" & iRowIndex)
                CRange.Value = dr("contestant_id")
                CRange = oSheet.Range("C" & iRowIndex)
                CRange.Value = dr("TotalScore")
                CRange = oSheet.Range("D" & iRowIndex)
                CRange.Value = dr("Rank")
                CRange = oSheet.Range("E" & iRowIndex)
                CRange.Value = dr("BadgeNumber")
                CRange = oSheet.Range("F" & iRowIndex)
                CRange.Value = dr("GRADE")
                CRange = oSheet.Range("G" & iRowIndex)
                CRange.Value = dr("ChapterID")
                CRange = oSheet.Range("H" & iRowIndex)
                CRange.Value = dr("ProductID")
                CRange = oSheet.Range("I" & iRowIndex)
                CRange.Value = dr("ProductCode")
                iRowIndex = iRowIndex + 1
            Next

        End If

        iRowIndex = iTieRowIndex
        If Not Session("GVtieBreakerRank") Is Nothing Then
            dstie = Nothing
            dstie = Session("GVtieBreakerRank")
            dt = dstie.Tables(0)

            For Each dr In dt.Rows
                CRange = oSheet.Range("P" & iRowIndex)
                CRange.Value = dr("ChildNumber")
                CRange = oSheet.Range("Q" & iRowIndex)
                CRange.Value = dr("contestant_id")
                CRange = oSheet.Range("R" & iRowIndex)
                CRange.Value = dr("TotalScore")
                CRange = oSheet.Range("S" & iRowIndex)
                CRange.Value = dr("Rank")
                CRange = oSheet.Range("T" & iRowIndex)
                CRange.Value = dr("BadgeNumber")
                CRange = oSheet.Range("U" & iRowIndex)
                CRange.Value = dr("GRADE")
                CRange = oSheet.Range("V" & iRowIndex)
                CRange.Value = dr("ChapterID")
                CRange = oSheet.Range("W" & iRowIndex)
                CRange.Value = dr("ProductID")
                CRange = oSheet.Range("X" & iRowIndex)
                CRange.Value = dr("ProductCode")
                iRowIndex = iRowIndex + 1
            Next
        End If
        Dim T As DateTime = DateTime.Now
        Dim month As String = T.ToString("MMM")
        Dim day As String = T.ToString("dd")
        Dim yr As String = T.ToString("yyyy")
        Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
        Dim FileName__1 As String = "NationalSelectionCount"
        Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & yr) + ".xls"
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.[End]()

    End Sub
    Protected Sub ExportCriteria()
        BtnLoadCutOffScores.Visible = False
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet = Nothing
        Dim oSheet1 As IWorksheet = Nothing
        oSheet = oWorkbooks.Worksheets.Add()

        oSheet.Name = "National Selection Criteria"

        oSheet.Range("A1:M1").MergeCells = True
        oSheet.Range("A1").Value = "Criteria for National Invitees by Contest and Grade " & lblYear.Text '"National Selection Criteria"
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("A1").Font.Bold = True
        oSheet.Range("A1").Font.Color = Color.Black
        oSheet.Range("A1").Font.Size = 15

        oSheet.Range("A3:F3").MergeCells = True
        oSheet.Range("A3").Value = "Total List"
        oSheet.Range("A3").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A3").Font.Bold = True
        oSheet.Range("A3").Font.Color = Color.Black

        oSheet.Range("A4:F4").Interior.Color = Color.CadetBlue
        oSheet.Range("A4:F4").Font.Color = Color.White
        oSheet.Range("A4:F4").ColumnWidth = 10
        oSheet.Range("A4:F4").RowHeight = 25
        oSheet.Range("A4:F4").WrapText = True
        oSheet.Range("A4:F4").Columns.VerticalAlignment = XlVAlign.xlVAlignCenter
        oSheet.Range("A4:F4").Columns.HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A4:F4").Font.Bold = True

        oSheet.Range("H3:M3").MergeCells = True
        oSheet.Range("H3").Value = "Priority List"
        oSheet.Range("H3").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("H3").Font.Bold = True
        oSheet.Range("H3").Font.Color = Color.Black

        oSheet.Range("H4:M4").Interior.Color = Color.CadetBlue
        oSheet.Range("H4:M4").Font.Color = Color.White
        oSheet.Range("H4:M4").ColumnWidth = 10
        oSheet.Range("H4:M4").RowHeight = 25
        oSheet.Range("H4:M4").WrapText = True
        oSheet.Range("H4:M4").Columns.VerticalAlignment = XlVAlign.xlVAlignCenter
        oSheet.Range("H4:M4").Columns.HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("H4:M4").Font.Bold = True

        oSheet.Range("A4").Value = "Contest"
        oSheet.Range("B4").Value = "Grade"
        oSheet.Range("C4").Value = "Minimum Score"
        oSheet.Range("D4").Value = "Rank1Score"
        oSheet.Range("E4").Value = "New Chapter Score"
        oSheet.Range("F4").Value = "MaxScore"

        Dim dr As GridViewRow
        Dim iRowIndex As Integer = 5
        Dim CRange As IRange = Nothing
        For Each dr In gvCriteria.Rows

            CRange = oSheet.Range("A" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(0).FindControl("lblContest"), Label).Text

            CRange = oSheet.Range("B" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(1).FindControl("lblGrade"), Label).Text

            CRange = oSheet.Range("C" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(2).FindControl("TextBox2"), TextBox).Text
            CRange.Borders.Weight = 2
            CRange.Borders.Color = Color.DarkGray

            CRange = oSheet.Range("D" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(3).FindControl("lblRank1Score"), Label).Text

            CRange = oSheet.Range("E" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(4).FindControl("txtNewChScore"), TextBox).Text

            CRange = oSheet.Range("F" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(5).FindControl("txtMaxScore"), TextBox).Text

            oSheet.Range("E" & iRowIndex.ToString() & ":F" & iRowIndex.ToString()).Borders.Weight = 2

            oSheet.Range("E" & iRowIndex.ToString() & ":F" & iRowIndex.ToString()).Borders.Color = Color.DarkGray

            oSheet.Range("A" & iRowIndex.ToString() & ":F" & iRowIndex.ToString()).NumberFormat = "#,##0"
            oSheet.Range("A" & iRowIndex.ToString() & ":F" & iRowIndex.ToString()).HorizontalAlignment = XlHAlign.xlHAlignRight
            iRowIndex = iRowIndex + 1
        Next


        oSheet.Range("H4").Value = "Contest"
        oSheet.Range("I4").Value = "Grade"
        oSheet.Range("J4").Value = "Minimum Score"
        oSheet.Range("K4").Value = "Rank1Score"
        oSheet.Range("L4").Value = "New Chapter Score"
        oSheet.Range("M4").Value = "MaxScore"

        iRowIndex = 5
        For Each dr In GVPriority.Rows

            CRange = oSheet.Range("H" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(0).FindControl("lblContest_pr"), Label).Text

            CRange = oSheet.Range("I" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(1).FindControl("lblGrade_pr"), Label).Text

            CRange = oSheet.Range("J" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(2).FindControl("TextBox2_pr"), TextBox).Text
            CRange.Borders.Weight = 2
            CRange.Borders.Color = Color.DarkGray

            CRange = oSheet.Range("K" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(3).FindControl("lblRank1Score_pr"), Label).Text

            CRange = oSheet.Range("L" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(4).FindControl("txtNewChScore_pr"), TextBox).Text

            CRange = oSheet.Range("M" + iRowIndex.ToString())
            CRange.Value = CType(dr.Cells(5).FindControl("txtMaxScore_pr"), TextBox).Text

            oSheet.Range("L" & iRowIndex.ToString() & ":M" & iRowIndex.ToString()).Borders.Weight = 2
            oSheet.Range("L" & iRowIndex.ToString() & ":M" & iRowIndex.ToString()).Borders.Color = Color.DarkGray
            oSheet.Range("H" & iRowIndex.ToString() & ":M" & iRowIndex.ToString()).NumberFormat = "#,##0"
            oSheet.Range("H" & iRowIndex.ToString() & ":M" & iRowIndex.ToString()).HorizontalAlignment = XlHAlign.xlHAlignRight
            iRowIndex = iRowIndex + 1
        Next

        Dim dt As DateTime = DateTime.Now
        Dim month As String = dt.ToString("MMM")
        Dim day As String = dt.ToString("dd")
        Dim yr As String = dt.ToString("yyyy")
        Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
        Dim FileName__1 As String = "NationalSelectionCriteria"
        Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & yr) + ".xls"

        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.[End]()

    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlNewChapter.Visible = False
    End Sub

    Protected Sub ExportTotalInviteeList()

        BtnLoadCutOffScores.Visible = False
        Dim connectionString As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("NSFConnectionString").ToString()
        ' create and open the connection object
        Dim connection As SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
        connection.Open()
        Dim commandString As String = "select count(c.productcode) as cnstNo,c.productcode,c.Chapterid,ch.NewChapterFlag into #contestno from contestant c,Chapter Ch where c.contestyear=" & Year & " and (c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))>0 and Ch.Chapterid=c.Chapterid group by c.productcode,c.Chapterid,ch.NewChapterflag;Select C.contestant_id,C.ChildNumber,C.chapterid, Ch.chaptercode, c.productid, c.productcode,c.grade, (c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0)) as Score, C.rank, N.minscore, N.rank1score, N.newchscore, N.maxscore,tmpC.cnstNo as ContestantCount,tmpC.NewChapterFlag,(c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))-N.minscore as Score_MinSCore,(c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))-N.rank1score as Score_rank1score,(c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))-N.newchscore as Score_newchscore from contestant c,chapter Ch,NationalCriteria N,#contestno tmpC where c.contestYear=N.contestYear  AND N.Grade=C.Grade and N.ProductCode=C.ProductCode AND C.ChapterID=Ch.ChapterID AND tmpC.Chapterid=C.ChapterID AND tmpC.ProductCode=N.ProductCode AND exists(select contestant_id from contestant ct1 where ct1.contestant_id = c.contestant_id and  ct1.Contestant_id in  (" & lblContestant.Text & ") ) and N.List='Total' order by C.chapterID,C.GRADE,C.ProductID;Drop  table #contestno"
        '.contestant_id,C.ChildNumber,C.chapterid, Ch.chaptercode, c.productid, c.productcode,c.grade, Score, C.rank, N.minscore, N.rank1score, N.newchscore, N.maxscore,
        'tmpC.cnstNo as ContestantCount,tmpC.NewChapterFlag, Score_MinSCore, Score_rank1score, as Score_newchscore
        Dim daContests As SqlDataAdapter = New SqlDataAdapter(commandString, connection)
        Dim dsContests As DataSet = New DataSet()
        daContests.Fill(dsContests, "Contests")

        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet = Nothing
        Dim oSheet1 As IWorksheet = Nothing
        oSheet = oWorkbooks.Worksheets.Add()

        oSheet.Name = "TotalList"

        oSheet.Range("A1:M1").MergeCells = True
        oSheet.Range("A1").Value = "National Selection TotalList - " & lblYear.Text
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("A1").Font.Bold = True
        oSheet.Range("A1").Font.Color = Color.Black
        oSheet.Range("A1").Font.Size = 15

        oSheet.Range("A2").Value = "Id"
        oSheet.Range("B2").Value = "ChildNumber"
        oSheet.Range("C2").Value = "Chapter"
        oSheet.Range("D2").Value = "ProductId"
        oSheet.Range("E2").Value = "Product Code"
        oSheet.Range("F2").Value = "Grade"
        oSheet.Range("G2").Value = "Score"
        oSheet.Range("H2").Value = "rank"
        oSheet.Range("I2").Value = "minscore"

        oSheet.Range("J2").Value = "rank1score"
        oSheet.Range("K2").Value = "newchscore"
        oSheet.Range("L2").Value = "maxscore"

        oSheet.Range("M2").Value = "ContestantCount"
        oSheet.Range("N2").Value = "NewChapterFlag"
        oSheet.Range("O2").Value = "Score_MinSCore"

        oSheet.Range("P2").Value = "Score_rank1score"
        oSheet.Range("Q2").Value = "Score_newchscore"

        oSheet.Range("A2:Q2").Font.Bold = True

        Dim dr As DataRow
        Dim iRowIndex As Integer = 3
        Dim CRange As IRange = Nothing
        Dim dt As DataTable = dsContests.Tables(0)
        For Each dr In dt.Rows
            CRange = oSheet.Range("A" + iRowIndex.ToString())
            CRange.Value = dr("contestant_id")

            CRange = oSheet.Range("B" + iRowIndex.ToString())
            CRange.Value = dr("ChildNumber")

            CRange = oSheet.Range("C" + iRowIndex.ToString())
            CRange.Value = dr("ChapterCode")

            CRange = oSheet.Range("D" + iRowIndex.ToString())
            CRange.Value = dr("ProductId")

            CRange = oSheet.Range("E" + iRowIndex.ToString())
            CRange.Value = dr("ProductCode")

            CRange = oSheet.Range("F" + iRowIndex.ToString())
            CRange.Value = dr("Grade")

            CRange = oSheet.Range("G" + iRowIndex.ToString())
            CRange.Value = dr("Score")

            CRange = oSheet.Range("H" + iRowIndex.ToString())
            CRange.Value = dr("rank")

            CRange = oSheet.Range("I" + iRowIndex.ToString())
            CRange.Value = dr("minscore")

            CRange = oSheet.Range("J" + iRowIndex.ToString())
            CRange.Value = dr("rank1score")

            CRange = oSheet.Range("K" + iRowIndex.ToString())
            CRange.Value = dr("newchscore")

            CRange = oSheet.Range("L" + iRowIndex.ToString())
            CRange.Value = dr("maxscore")

            CRange = oSheet.Range("M" + iRowIndex.ToString())
            CRange.Value = dr("ContestantCount")

            CRange = oSheet.Range("N" + iRowIndex.ToString())
            CRange.Value = dr("NewChapterFlag")

            CRange = oSheet.Range("O" + iRowIndex.ToString())
            CRange.Value = dr("Score_MinSCore")

            CRange = oSheet.Range("P" + iRowIndex.ToString())
            CRange.Value = dr("Score_rank1score")

            CRange = oSheet.Range("Q" + iRowIndex.ToString())
            CRange.Value = dr("Score_newchscore")

            iRowIndex = iRowIndex + 1
        Next
        Dim T As DateTime = DateTime.Now
        Dim month As String = T.ToString("MMM")
        Dim day As String = T.ToString("dd")
        Dim yr As String = T.ToString("yyyy")
        Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
        Dim FileName__1 As String = "NationalSelection_TotalList"
        Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & yr) + ".xls"
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.[End]()

    End Sub
    Protected Sub ExportPriorityInviteeList()

        BtnLoadCutOffScores.Visible = False
        Dim connectionString As String = System.Web.Configuration.WebConfigurationManager.ConnectionStrings("NSFConnectionString").ToString()
        ' create and open the connection object
        Dim connection As SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
        connection.Open()
        Dim commandString As String = "select count(c.productcode) as cnstNo,c.productcode,c.Chapterid,ch.NewChapterFlag into #contestno from contestant c,Chapter Ch where c.contestyear=" & lblYear.Text & " and (c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))>0 and Ch.Chapterid=c.Chapterid group by c.productcode,c.Chapterid,ch.NewChapterflag;Select C.contestant_id,C.ChildNumber,C.chapterid, Ch.chaptercode, c.productid, c.productcode,c.grade, (c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0)) as Score, C.rank, N.minscore, N.rank1score, N.newchscore, N.maxscore,tmpC.cnstNo as ContestantCount,tmpC.NewChapterFlag,(c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))-N.minscore as Score_MinSCore,(c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))-N.rank1score as Score_rank1score,(c.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))-N.newchscore as Score_newchscore from contestant c,chapter Ch,NationalCriteria N,#contestno tmpC where c.contestYear=N.contestYear  AND N.Grade=C.Grade and N.ProductCode=C.ProductCode AND C.ChapterID=Ch.ChapterID AND tmpC.Chapterid=C.ChapterID AND tmpC.ProductCode=N.ProductCode AND exists(select contestant_id from contestant ct1 where ct1.contestant_id = c.contestant_id and  ct1.Contestant_id in  (" & lblContestants_Priority.Text & ")) and N.List='Priority' order by C.chapterID,C.GRADE,C.ProductID;Drop table #contestno"
        Dim daContests As SqlDataAdapter = New SqlDataAdapter(commandString, connection)
        Dim dsContests As DataSet = New DataSet()

        daContests.Fill(dsContests, "Contests")

        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet = Nothing
        Dim oSheet1 As IWorksheet = Nothing
        oSheet = oWorkbooks.Worksheets.Add()

        oSheet.Name = "PriorityList"

        oSheet.Range("A1:M1").MergeCells = True
        oSheet.Range("A1").Value = "National Selection PriorityList - " & lblYear.Text
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("A1").Font.Bold = True
        oSheet.Range("A1").Font.Color = Color.Black
        oSheet.Range("A1").Font.Size = 15

        '.contestant_id,C.ChildNumber,C.chapterid, Ch.chaptercode, c.productid, c.productcode,c.grade,
        '  as Score, C.rank, N.minscore, N.rank1score, N.newchscore, N.maxscore,
        'tmpC.cnstNo as ContestantCount,tmpC.NewChapterFlag, Score_MinSCore, Score_rank1score, as Score_newchscore

        oSheet.Range("A2").Value = "Id"
        oSheet.Range("B2").Value = "ChildNumber"
        oSheet.Range("C2").Value = "Chapter"
        oSheet.Range("D2").Value = "ProductId"
        oSheet.Range("E2").Value = "Product Code"
        oSheet.Range("F2").Value = "Grade"
        oSheet.Range("G2").Value = "Score"
        oSheet.Range("H2").Value = "rank"
        oSheet.Range("I2").Value = "minscore"

        oSheet.Range("J2").Value = "rank1score"
        oSheet.Range("K2").Value = "newchscore"
        oSheet.Range("L2").Value = "maxscore"

        oSheet.Range("M2").Value = "ContestantCount"
        oSheet.Range("N2").Value = "NewChapterFlag"
        oSheet.Range("O2").Value = "Score_MinSCore"

        oSheet.Range("P2").Value = "Score_rank1score"
        oSheet.Range("Q2").Value = "Score_newchscore"

        oSheet.Range("A2:Q2").Font.Bold = True

        Dim dr As DataRow
        Dim iRowIndex As Integer = 3
        Dim CRange As IRange = Nothing
        Dim dt As DataTable = dsContests.Tables(0)
        For Each dr In dt.Rows
            CRange = oSheet.Range("A" + iRowIndex.ToString())
            CRange.Value = dr("contestant_id")

            CRange = oSheet.Range("B" + iRowIndex.ToString())
            CRange.Value = dr("ChildNumber")

            CRange = oSheet.Range("C" + iRowIndex.ToString())
            CRange.Value = dr("ChapterCode")

            CRange = oSheet.Range("D" + iRowIndex.ToString())
            CRange.Value = dr("ProductId")

            CRange = oSheet.Range("E" + iRowIndex.ToString())
            CRange.Value = dr("ProductCode")

            CRange = oSheet.Range("F" + iRowIndex.ToString())
            CRange.Value = dr("Grade")

            CRange = oSheet.Range("G" + iRowIndex.ToString())
            CRange.Value = dr("Score")

            CRange = oSheet.Range("H" + iRowIndex.ToString())
            CRange.Value = dr("rank")

            CRange = oSheet.Range("I" + iRowIndex.ToString())
            CRange.Value = dr("minscore")

            CRange = oSheet.Range("J" + iRowIndex.ToString())
            CRange.Value = dr("rank1score")

            CRange = oSheet.Range("K" + iRowIndex.ToString())
            CRange.Value = dr("newchscore")

            CRange = oSheet.Range("L" + iRowIndex.ToString())
            CRange.Value = dr("maxscore")

            CRange = oSheet.Range("M" + iRowIndex.ToString())
            CRange.Value = dr("ContestantCount")

            CRange = oSheet.Range("N" + iRowIndex.ToString())
            CRange.Value = dr("NewChapterFlag")

            CRange = oSheet.Range("O" + iRowIndex.ToString())
            CRange.Value = dr("Score_MinSCore")

            CRange = oSheet.Range("P" + iRowIndex.ToString())
            CRange.Value = dr("Score_rank1score")

            CRange = oSheet.Range("Q" + iRowIndex.ToString())
            CRange.Value = dr("Score_newchscore")

            iRowIndex = iRowIndex + 1
        Next
        Dim T As DateTime = DateTime.Now
        Dim month As String = T.ToString("MMM")
        Dim day As String = T.ToString("dd")
        Dim yr As String = T.ToString("yyyy")
        Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
        Dim FileName__1 As String = "NationalSelection_PriorityList"
        Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & yr) + ".xls"
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.[End]()

    End Sub
    Protected Sub btnTies_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblMessage.ForeColor = Drawing.Color.Red
        lblMessage.Text = "Sorry! This feature is yet to be done."
        btnTies.Visible = False
    End Sub

    Protected Sub btnSaveInviteeCount_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Contest, Grade, MinScore, count, cumulative, Contestants_No, invited_Per, IncRank1, IncNewChap, CumRank1, AvgScore
        lblMessage.Text = ""
        lblUpdateMsg.Text = ""
        lblPriority.Text = ""
        Dim i As Integer
        Dim StrSQl As String = ""
        Dim ProductCode As String = ""
        Dim Grade As Integer
        Dim FlagIns As Boolean = False
        Dim FlagUpd As Boolean = False
        Try
            ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from InviteeCount where ContestYear=" & Year & " and List='Total'")
            For i = 0 To gvOut.Rows.Count - 1
                ProductCode = gvOut.Rows(i).Cells(0).Text.Trim
                Grade = Convert.ToInt32(gvOut.Rows(i).Cells(1).Text)
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from InviteeCount where ContestYear=" & Year & " and ProductCode='" & ProductCode & "' and Grade=" & Grade & " and List='Total'") = 0 Then
                    StrSQl = "Insert into Inviteecount(ProductCode, Grade, MinScore, count, cumCount, Contestants, invited, AddRank1, nwchcount, CumulWrank1, AvgScore,createDate,ContestYear,List) values"
                    StrSQl = StrSQl & "('" & gvOut.Rows(i).Cells(0).Text.Trim & "'," & gvOut.Rows(i).Cells(1).Text & "," & gvOut.Rows(i).Cells(2).Text & "," & gvOut.Rows(i).Cells(3).Text & "," & gvOut.Rows(i).Cells(4).Text & "," & gvOut.Rows(i).Cells(5).Text & ",'" & gvOut.Rows(i).Cells(6).Text.Trim & "'," & gvOut.Rows(i).Cells(7).Text & "," & gvOut.Rows(i).Cells(8).Text & "," & gvOut.Rows(i).Cells(9).Text & ",'" & gvOut.Rows(i).Cells(10).Text & "',GetDate()," & Year & ",'Total')"
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl) > 0 Then
                        FlagIns = True
                    End If

                Else
                    StrSQl = "Update Inviteecount set "
                    StrSQl = StrSQl & " MinScore=" & gvOut.Rows(i).Cells(2).Text & ",count=" & gvOut.Rows(i).Cells(3).Text & ",cumCount=" & gvOut.Rows(i).Cells(4).Text & ",Contestants=" & gvOut.Rows(i).Cells(5).Text & ",invited='" & gvOut.Rows(i).Cells(6).Text.Trim & "',AddRank1=" & gvOut.Rows(i).Cells(7).Text & ",nwchcount=" & gvOut.Rows(i).Cells(8).Text & ",CumulWrank1=" & gvOut.Rows(i).Cells(9).Text & ",AvgScore='" & gvOut.Rows(i).Cells(10).Text & "',createDate=GetDate(),ContestYear=" & Year & ",List='Total' "
                    StrSQl = StrSQl & " Where ProductCode='" & ProductCode & "' and Grade=" & Grade & " and ContestYear=" & Year & " and List='Total'"
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl) > 0 Then
                        FlagUpd = True
                    End If
                End If
            Next
            ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from InviteeCount where ContestYear=" & Year & " and List='Priority'")
            For i = 0 To gvOutPriority.Rows.Count - 1
                ProductCode = gvOutPriority.Rows(i).Cells(0).Text.Trim
                Grade = Convert.ToInt32(gvOutPriority.Rows(i).Cells(1).Text)
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from InviteeCount where ContestYear=" & Year & " and ProductCode='" & ProductCode & "' and Grade=" & Grade & " and List='Priority'") = 0 Then
                    StrSQl = "Insert into Inviteecount(ProductCode, Grade, MinScore, count, cumCount, Contestants, invited, AddRank1, nwchcount, CumulWrank1, AvgScore,createDate,ContestYear,List) values"
                    StrSQl = StrSQl & "('" & gvOutPriority.Rows(i).Cells(0).Text.Trim & "'," & gvOutPriority.Rows(i).Cells(1).Text & "," & gvOutPriority.Rows(i).Cells(2).Text & "," & gvOutPriority.Rows(i).Cells(3).Text & "," & gvOutPriority.Rows(i).Cells(4).Text & "," & gvOutPriority.Rows(i).Cells(5).Text & ",'" & gvOutPriority.Rows(i).Cells(6).Text.Trim & "'," & gvOutPriority.Rows(i).Cells(7).Text & "," & gvOutPriority.Rows(i).Cells(8).Text & "," & gvOutPriority.Rows(i).Cells(9).Text & ",'" & gvOutPriority.Rows(i).Cells(10).Text & "',GetDate()," & Year & ",'Priority' )"
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl) > 0 Then
                        FlagIns = True
                    End If

                Else
                    StrSQl = "Update Inviteecount set "
                    StrSQl = StrSQl & " MinScore=" & gvOutPriority.Rows(i).Cells(2).Text & ",count=" & gvOutPriority.Rows(i).Cells(3).Text & ",cumCount=" & gvOutPriority.Rows(i).Cells(4).Text & ",Contestants=" & gvOutPriority.Rows(i).Cells(5).Text & ",invited='" & gvOutPriority.Rows(i).Cells(6).Text.Trim & "',AddRank1=" & gvOutPriority.Rows(i).Cells(7).Text & ",nwchcount=" & gvOutPriority.Rows(i).Cells(8).Text & ",CumulWrank1=" & gvOutPriority.Rows(i).Cells(9).Text & ",AvgScore='" & gvOutPriority.Rows(i).Cells(10).Text & "',createDate=GetDate(),ContestYear=" & Year & ",List='Priority' "
                    StrSQl = StrSQl & " Where ProductCode='" & ProductCode & "' and Grade=" & Grade & " and ContestYear=" & Year & " and List='Priority'"
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl) > 0 Then
                        FlagUpd = True
                    End If
                End If
            Next
            If FlagIns = True Then
                lblMessage.ForeColor = Color.Blue
                lblMessage.Text = "Records inserted to Invitee Count succesfully."
            End If
            If FlagUpd = True Then
                lblMessage.Text = "Records Updated to Invitee Count succesfully."
            End If
            lblMessage.ForeColor = Color.Red
            btnSaveInviteeCount.Visible = False
        Catch ex As Exception
            lblMessage.Text = "Error In Saving Count"
        End Try
    End Sub

    Protected Sub btnLoadInviteeCount_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        getContestants()
        GetPriorityContestants()
        BtnLoadCutOffScores.Visible = False
        gvCriteria.DataSource = Nothing
        gvCriteria.DataBind()
        GVPriority.DataSource = Nothing
        GVPriority.DataBind()
        Dim dsInviteeCount As New DataSet
        Dim dsInviteePriority As New DataSet

        Dim tblInviteeCount() As String = {"InviteeCount"}
        Dim tblInviteePriorityCount() As String = {"PriorityInviteeCount"}

        Dim StrSQ As String
        If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from InviteeCount where contestyear=" & Year & " and List='Total'") > 0) Then
            StrSQ = "Select * from InviteeCount where contestyear=" & Year & " and List='Total'"
        Else
            StrSQ = "Select * from InviteeCount where contestyear=" & Val(Year) - 1 & " and List='Total'"
            lblMessage.Text = "InviteeCount for last year - " & Val(Year) - 1
        End If

        SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQ, dsInviteeCount, tblInviteeCount)
        ViewState("gvOut") = dsInviteeCount.Tables(0)
        gvOut.DataSource = dsInviteeCount
        gvOut.DataBind()
        gvOut.Visible = True
        gvOutPriority.Visible = True
        If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from InviteeCount where contestyear=" & Year & " and List='Priority'") > 0) Then
            StrSQ = "Select * from InviteeCount where contestyear=" & Year & " and List='Priority'"
        Else
            StrSQ = "Select * from InviteeCount where contestyear=" & Val(Year) - 1 & " and List='Priority'"
            lblMessage.Text = "InviteeCount for last year - " & Val(Year) - 1
        End If

        SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQ, dsInviteePriority, tblInviteePriorityCount)
        ViewState("gvOutPriority") = dsInviteePriority.Tables(0)
        gvOutPriority.DataSource = dsInviteePriority
        gvOutPriority.DataBind()
        gvOutPriority.Visible = True
        '  End If
        CalculateRatio()
        btnSave.Visible = False
        btnNewChapter.Visible = False
        btnCalCulate.Visible = False
        gvCriteria.Style.Add("display", "none")
        GVPriority.Style.Add("display", "none")
        btnUpdateCnst.Visible = False
        ddlInvite.Visible = False
        ' btnUpdateCnst1.Visible = False

        ddlNatInvite.Visible = True
        btnNatInvite.Visible = True
        'BtnExport.Visible = True
        ddlExport.Visible = True
        ddlExport.Items(0).Enabled = False
        ddlExport.Items(1).Enabled = True
        ddlExport.Items(2).Enabled = True
        ddlExport.Items(3).Enabled = True
        ddlExport.Items(4).Enabled = False
        'btnExport1.Visible = True
        'btnExportInvitee.Visible = True
        'btnExportInviteePriority.Visible = True
        btnSaveInviteeCount.Visible = False

    End Sub
    Private Sub CalculateRatio()
        Dim Ratio1, Ratio2 As Decimal
        Dim dt2 As DataTable = New DataTable()
        Dim dr As DataRow
        dt2.Columns.Add("Ratio1", Type.GetType("System.String"))
        dt2.Columns.Add("Ratio2", Type.GetType("System.String"))

        If gvOut.Rows.Count > 0 And gvOutPriority.Rows.Count > 0 Then

            For k As Integer = 0 To gvOut.Rows.Count - 1
                dr = dt2.NewRow()
                If gvOut.Rows(k).Cells(3).Text = 0 Or gvOutPriority.Rows(k).Cells(3).Text = 0 Then
                    Ratio1 = 0
                Else
                    Ratio1 = Convert.ToDecimal(gvOutPriority.Rows(k).Cells(3).Text / gvOut.Rows(k).Cells(3).Text)
                End If
                If gvOut.Rows(k).Cells(4).Text = 0 Or gvOutPriority.Rows(k).Cells(4).Text = 0 Then
                    Ratio2 = 0
                Else
                    Ratio2 = Convert.ToDecimal(gvOutPriority.Rows(k).Cells(4).Text / gvOut.Rows(k).Cells(4).Text)
                End If
                dr("Ratio1") = Math.Round(Ratio1 * 100, 2) & "%"
                dr("Ratio2") = Math.Round(Ratio2 * 100, 2) & "%"
                dt2.Rows.Add(dr)
            Next

            gvRatio.Visible = True
            ViewState("gvRatio") = dt2
            gvRatio.DataSource = dt2
            gvRatio.DataBind()
        Else
            ViewState("gvRatio") = Nothing
            gvRatio.DataSource = Nothing
            gvRatio.DataBind()
        End If
    End Sub
    Protected Sub ExportYearToYear()
        BtnLoadCutOffScores.Visible = False
        ' ProductCode,Grade,MinScore,Invitees,ExRegis,ExpShows,lastMinScore,LasInvitees,LastRegis,LastShows,RegisPers,ShowsPer
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet = Nothing
        Dim oSheet1 As IWorksheet = Nothing
        oSheet = oWorkbooks.Worksheets.Add()

        oSheet.Name = "Y2YComparison"

        oSheet.Range("A1:L1").MergeCells = True
        oSheet.Range("A1").Value = "National Selection YeartoYearComparison" & lblYear.Text
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("A1").Font.Bold = True
        oSheet.Range("A1").Font.Color = Color.Black
        oSheet.Range("A1").Font.Size = 15

        oSheet.Range("A2").Value = "ProductCode"
        oSheet.Range("B2").Value = "Grade"
        oSheet.Range("C2").Value = "MinScore"
        oSheet.Range("D2").Value = "Invitees"
        oSheet.Range("E2").Value = "ExRegis"
        oSheet.Range("F2").Value = "ExpShows"
        oSheet.Range("G2").Value = "lastMinScore"
        oSheet.Range("H2").Value = "LasInvitees"
        oSheet.Range("I2").Value = "LastRegis"
        oSheet.Range("J2").Value = "LastShows"
        oSheet.Range("K2").Value = "RegisPers"
        oSheet.Range("L2").Value = "ShowsPer"
        oSheet.Range("A2:L2").Font.Bold = True

        Dim dr As DataRow
        Dim iRowIndex As Integer = 3
        Dim CRange As IRange = Nothing
        If Not ViewState("gvY2Y") Is Nothing Then
            Dim dt As DataTable = ViewState("gvY2Y")
            For Each dr In dt.Rows
                CRange = oSheet.Range("A" + iRowIndex.ToString())
                CRange.Value = dr("ProductCode")
                CRange = oSheet.Range("B" + iRowIndex.ToString())
                CRange.Value = dr("Grade")
                CRange = oSheet.Range("C" + iRowIndex.ToString())
                CRange.Value = dr("MinScore")
                CRange = oSheet.Range("D" + iRowIndex.ToString())
                CRange.Value = dr("Invitees")
                CRange = oSheet.Range("E" + iRowIndex.ToString())
                CRange.Value = dr("ExRegis")
                CRange = oSheet.Range("F" + iRowIndex.ToString())
                CRange.Value = dr("ExpShows")
                CRange = oSheet.Range("G" + iRowIndex.ToString())
                CRange.Value = dr("lastMinScore")
                CRange = oSheet.Range("H" + iRowIndex.ToString())
                CRange.Value = dr("LasInvitees")
                CRange = oSheet.Range("I" + iRowIndex.ToString())
                CRange.Value = dr("LastRegis")
                CRange = oSheet.Range("J" + iRowIndex.ToString())
                CRange.Value = dr("LastShows")
                CRange = oSheet.Range("K" + iRowIndex.ToString())
                CRange.Value = dr("RegisPers")
                CRange = oSheet.Range("L" + iRowIndex.ToString())
                CRange.Value = dr("ShowsPer")

                iRowIndex = iRowIndex + 1
            Next
            Dim T As DateTime = DateTime.Now
            Dim month As String = T.ToString("MMM")
            Dim day As String = T.ToString("dd")
            Dim yr As String = T.ToString("yyyy")
            Dim monthDay As String = Convert.ToString(month & Convert.ToString("")) & day
            Dim FileName__1 As String = "YeartoYearComparison"
            Dim filename__2 As String = (Convert.ToString((Convert.ToString((Convert.ToString("") & FileName__1) + "_") & monthDay) + "_") & yr) + ".xls"
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", Convert.ToString("attachment;filename=") & filename__2)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.[End]()

        End If
    End Sub

    Private Sub CheckInviteeFlags()
        Dim TotalList As Integer
        Dim PriorityList, i As Integer

        Try
            TotalList = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Contestant where TotInvite is not null and ContestYear=" & Year & IIf(lblContestant.Text <> "", " and contestant_id in (" & lblContestant.Text & ")", ""))
            PriorityList = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Contestant where PriorityInvite is not null and ContestYear=" & Year & IIf(lblContestants_Priority.Text <> "", " and contestant_id in (" & lblContestants_Priority.Text & ")", ""))
            If TotalList > 0 Or PriorityList > 0 Then
                btnNatInvite.Visible = True
                ddlNatInvite.Visible = True
            End If
            If TotalList > 0 Then
                ddlNatInvite.Items(0).Enabled = True
            End If
            If PriorityList > 0 Then
                ddlNatInvite.Items(1).Enabled = True
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub btnNatInvite_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNatInvite.Click
        lblerr.Text = ""
        lblerr.ForeColor = Color.Red
        lblUpdateMsg.Text = ""
        'If ddlNatInvite.SelectedIndex = 0 Then
        '    lblerr.Text = "Please select option"
        '    Exit Sub
        'Else
        Dim strNatInvite As String = ""
        Dim total, priority As Integer
        For i As Integer = 0 To ddlNatInvite.Items.Count - 1
            If ddlNatInvite.Items(i).Selected = True Then
                If Len(strNatInvite) > 0 Then
                    strNatInvite = strNatInvite & "," & ddlNatInvite.Items(i).Value & ""
                Else
                    strNatInvite = "" & ddlNatInvite.Items(i).Value & ""
                End If
            End If
        Next

        Dim StrPriority As String = "Update Contestant Set NationalInvitee = PriorityInvite,ModifiedDate=GetDate() Where PriorityInvite is not null and ContestYear=" & Year & IIf(lblContestants_Priority.Text <> "", " and contestant_id in (" & lblContestants_Priority.Text & ")", "")
        Dim StrTotal As String = "Update Contestant Set NationalInvitee = TotInvite,ModifiedDate=GetDate() Where TotInvite is not null and ContestYear=" & Year & IIf(lblContestant.Text <> "", " and contestant_id in (" & lblContestant.Text & ")", "")
        If strNatInvite = "1" Then
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrPriority) > 0 Then
                lblerr.ForeColor = Color.Blue
                lblerr.Text = "National Invitee Flag updated for Priority list"
            Else
                lblerr.Text = "Priority Invite Flag not yet updated."
            End If
        ElseIf strNatInvite = "2" Then
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrTotal) > 0 Then
                lblerr.ForeColor = Color.Blue
                lblerr.Text = "National Invitee Flag updated for Total list"
            Else
                lblerr.Text = "Total Invite Flag not yet updated."
            End If
        ElseIf strNatInvite = "1,2" Then 'If ddlNatInvite.SelectedValue = "1" And ddlNatInvite.SelectedValue = "1" = 2 Then
            total = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrTotal)
            priority = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrPriority)
            If total > 0 And priority > 0 Then
                lblerr.ForeColor = Color.Blue
                lblerr.Text = "Priority Invite Flag and Total Invite Flag Updated."
            Else
                lblerr.Text = "Invite Flags not yet updated."
            End If
        End If
        'End If

    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlExport.SelectedItem.Text = "Export Invitee Count" Then
            ExportIviteeCount()
        ElseIf ddlExport.SelectedItem.Text = "Export Criteria" Then
            ExportCriteria()
        ElseIf ddlExport.SelectedItem.Text = "Export Total Invitee List" Then
            ExportTotalInviteeList()
        ElseIf ddlExport.SelectedItem.Text = "Export Priority Invitee List" Then
            ExportPriorityInviteeList()
        ElseIf ddlExport.SelectedItem.Text = "Export YearToYear" Then
            ExportYearToYear()
        End If
    End Sub

    Protected Sub btnY2YHelp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnY2YHelp.Click
        Response.Write("<script language='javascript'>window.open('NatCriteriaHelp.aspx','_blank','left=150,top=0,width=700,height=550,toolbar=0,location=0,scrollbars=1');</script> ")
    End Sub
    Protected Sub BtnLoadCutOffScores_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadCutOffScores()
    End Sub
    Sub getContestants()

        BtnLoadCutOffScores.Visible = False
        lblContestant.Text = "0"
        Dim CCount As New Hashtable()
        Dim CRank1 As New Hashtable()
        Dim reader1 As SqlDataReader
        Dim Rank1Tie(500, 3) As String
        Dim TieIndex As Integer
        TieIndex = 0
        Dim SQLTieQuery As String = " (EventId=1) "
        Dim overalltotal As Integer = 0
        Dim Rank1alltotal As Integer = 0
        Dim NewChapterCount As Integer = 0
        reader1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT ContestCode From ContestCategory WHERE contestyear = " & Year & "  and NationalSelectionCriteria ='I' and NationalFinalsStatus = 'Active' Group by ContestCode")
        While reader1.Read()
            CCount.Add(reader1("ContestCode"), 0)
            CRank1.Add(reader1("ContestCode"), 0)
        End While
        reader1.Close()
        Dim dt2 As DataTable = New DataTable()
        Dim dr As DataRow
        dt2.Columns.Add("ProductCode", Type.GetType("System.String"))
        dt2.Columns.Add("Grade", Type.GetType("System.Int32"))
        dt2.Columns.Add("MinScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("NewChScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
        dt2.Columns.Add("MaxScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Count", Type.GetType("System.Int32"))
        dt2.Columns.Add("CumCount", Type.GetType("System.Int32"))
        dt2.Columns.Add("Contestants", Type.GetType("System.Int32"))
        dt2.Columns.Add("Invited", Type.GetType("System.String"))
        dt2.Columns.Add("AddRank1", Type.GetType("System.Int32"))
        dt2.Columns.Add("nwchcount", Type.GetType("System.Int32"))
        dt2.Columns.Add("CumulWrank1", Type.GetType("System.Int32"))
        dt2.Columns.Add("AvgScore", Type.GetType("System.Decimal"))
        Dim i, invitecount, numCntestants, cntRank1, nwchcount, rank1Tiecount As Integer
        Dim AvgScore As Decimal = 0.0
        Dim LastContestName As String = ""

        Dim NwChapIDs As String = "0,"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT ChapterID FROM Chapter WHERE Newchapterflag='Y'")
        For i = 0 To ds.Tables(0).Rows.Count - 1
            NwChapIDs = NwChapIDs & ds.Tables(0).Rows(i)(0).ToString() & ","
        Next
        For i = 0 To gvCriteria.Rows.Count - 1
            dr = dt2.NewRow()
            invitecount = 0
            numCntestants = 0
            cntRank1 = 0
            nwchcount = 0
            rank1Tiecount = 0
            dr("ProductCode") = CType(gvCriteria.Rows(i).Cells(0).FindControl("lblContest"), Label).Text
            dr("Grade") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(1).FindControl("lblGrade"), Label).Text)
            dr("MinScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(2).FindControl("TextBox2"), TextBox).Text)
            dr("Rank1Score") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(3).FindControl("lblRank1Score"), Label).Text)
            dr("NewChScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(4).FindControl("txtNewChScore"), TextBox).Text)
            dr("MaxScore") = Convert.ToInt32(CType(gvCriteria.Rows(i).Cells(5).FindControl("txtMaxScore"), TextBox).Text)
            If LastContestName <> dr("ProductCode") Then
                LastContestName = dr("ProductCode")
                AvgScore = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Case COUNT(score1)  when  0 then 0 else Round(AVG(score1+ISNULL(score2,0)+ISNULL(score3,0)),2) end from contestant where contestyear=" & Year & " and score1+ISNULL(score2,0)+ISNULL(score3,0)>0 and productcode = '" & dr("ProductCode") & "' AND EventId=2")
            End If
            dr("AvgScore") = AvgScore
            Dim aScore As Integer = 0
            Dim gCount As Integer = 0
            Dim CumCount As Integer = 0
            Dim CumulWrank1 As Integer = 0

            If dr("MinScore") > 0 Then
                Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select ChildNumber, ContestCode, contestant_id, isnull(Score1,0) AS Score1, isnull( Score2,0) AS Score2,isnull(Score3,0) AS Score3, Rank,  BadgeNumber, ContestCategoryID, GRADE, ChapterID  from Contestant   Where ContestYear=" & Year & "  AND (Score1+ISNULL(score2,0)+ISNULL(score3,0))>0 AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND GRADE= " & dr("Grade") & "  ORDER BY ChapterID,Rank")
                While reader.Read()
                    aScore = reader("Score1") + reader("Score2") + reader("Score3")
                    If aScore >= dr("MinScore") Then
                        invitecount = invitecount + 1
                        lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                    ElseIf reader("Rank") = 1 Then '** aScore >= dr("MinScore") And 
                        'Rank 1 score consideration where total contestant is >= 5
                        If aScore >= dr("Rank1Score") And dr("Rank1Score") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(ChildNumber)  from Contestant  Where (Score1+ISNULL(score2,0)+ISNULL(score3,0)) >0  AND ContestYear=" & Year & "   AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND ChapterID =" & reader("chapterid") & "") >= 5 Then
                            cntRank1 = cntRank1 + 1
                            TieIndex = TieIndex + 1
                            lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                            SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                        ElseIf aScore >= dr("NewChScore") And dr("NewChScore") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(C.ChildNumber)  from Contestant C,Chapter Ch  Where (C.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))>0  AND C.ContestYear=" & Year & "  AND C.ProductCode = '" & dr("ProductCode") & "' AND C.EventId=2 and C.ChapterID = Ch.ChapterID and Ch.NewChapterFlag='Y' AND C.chapterid=" & reader("chapterid") & "") >= 5 Then
                            'New Chapter Contestant where Rank 1 and total contestant is >= 5
                            nwchcount = nwchcount + 1
                            lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                            SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                            TieIndex = TieIndex + 1
                        End If
                    ElseIf NwChapIDs.Contains("," + reader("chapterid").ToString() & ",") Then
                        aScore = aScore + 1
                        If aScore >= dr("MinScore") Then
                            'New Chapter Contestant by Adding 1 
                            nwchcount = nwchcount + 1
                            lblContestant.Text = lblContestant.Text & "," & reader("contestant_id")
                        End If
                    End If
                    numCntestants = numCntestants + 1
                End While
                reader.Close()
            End If
        Next
    End Sub
    Sub GetPriorityContestants()
        Dim CCount As New Hashtable()
        Dim CRank1 As New Hashtable()
        Dim reader1 As SqlDataReader
        Dim Rank1Tie(500, 3) As String
        lblContestants_Priority.Text = "0"
        Dim TieIndex As Integer
        TieIndex = 0
        Dim SQLTieQuery As String = " (EventId=1) "
        Dim overalltotal As Integer = 0
        Dim Rank1alltotal As Integer = 0
        Dim NewChapterCount As Integer = 0
        reader1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT ContestCode From ContestCategory WHERE contestyear = " & Year & "  and NationalSelectionCriteria ='I' and NationalFinalsStatus = 'Active' Group by ContestCode")
        While reader1.Read()
            CCount.Add(reader1("ContestCode"), 0)
            CRank1.Add(reader1("ContestCode"), 0)
        End While
        reader1.Close()
        Dim dt2 As DataTable = New DataTable()
        Dim dr As DataRow
        dt2.Columns.Add("ProductCode", Type.GetType("System.String"))
        dt2.Columns.Add("Grade", Type.GetType("System.Int32"))
        dt2.Columns.Add("MinScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("NewChScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Rank1Score", Type.GetType("System.Int32"))
        dt2.Columns.Add("MaxScore", Type.GetType("System.Int32"))
        dt2.Columns.Add("Count", Type.GetType("System.Int32"))
        dt2.Columns.Add("CumCount", Type.GetType("System.Int32"))
        dt2.Columns.Add("Contestants", Type.GetType("System.Int32"))
        dt2.Columns.Add("Invited", Type.GetType("System.String"))
        dt2.Columns.Add("AddRank1", Type.GetType("System.Int32"))
        dt2.Columns.Add("nwchcount", Type.GetType("System.Int32"))
        dt2.Columns.Add("CumulWrank1", Type.GetType("System.Int32"))
        dt2.Columns.Add("AvgScore", Type.GetType("System.Decimal"))
        Dim i, invitecount, numCntestants, cntRank1, nwchcount, rank1Tiecount As Integer
        Dim AvgScore As Decimal = 0.0
        Dim LastContestName As String = ""

        Dim NwChapIDs As String = "0,"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT ChapterID FROM Chapter WHERE Newchapterflag='Y'")
        For i = 0 To ds.Tables(0).Rows.Count - 1
            NwChapIDs = NwChapIDs & ds.Tables(0).Rows(i)(0).ToString() & ","
        Next
        For i = 0 To GVPriority.Rows.Count - 1
            dr = dt2.NewRow()
            invitecount = 0
            numCntestants = 0
            cntRank1 = 0
            nwchcount = 0
            rank1Tiecount = 0
            dr("ProductCode") = CType(GVPriority.Rows(i).Cells(0).FindControl("lblContest_pr"), Label).Text
            dr("Grade") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(1).FindControl("lblGrade_pr"), Label).Text)
            'CType(e.Item.FindControl("txtTransType"), TextBox).Text
            dr("MinScore") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(2).FindControl("TextBox2_pr"), TextBox).Text)
            dr("Rank1Score") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(3).FindControl("lblRank1Score_pr"), Label).Text)
            dr("NewChScore") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(4).FindControl("txtNewChScore_pr"), TextBox).Text)
            dr("MaxScore") = Convert.ToInt32(CType(GVPriority.Rows(i).Cells(5).FindControl("txtMaxScore_pr"), TextBox).Text)
            If LastContestName <> dr("ProductCode") Then
                LastContestName = dr("ProductCode")
                AvgScore = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Case COUNT(score1)  when  0 then 0 else Round(AVG(score1+ISNULL(score2,0)+ISNULL(score3,0)),2) end from contestant where contestyear=" & Year & " and score1+ISNULL(score2,0)+ISNULL(score3,0)>0 and productcode = '" & dr("ProductCode") & "' AND EventId=2")
            End If
            dr("AvgScore") = AvgScore
            Dim aScore As Integer = 0
            Dim gCount As Integer = 0
            Dim CumCount As Integer = 0
            Dim CumulWrank1 As Integer = 0
            If dr("MinScore") > 0 Then
                Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select ChildNumber, ContestCode, contestant_id, isnull(Score1,0) AS Score1, isnull( Score2,0) AS Score2,isnull(Score3,0) AS Score3, Rank,  BadgeNumber, ContestCategoryID, GRADE, ChapterID  from Contestant   Where ContestYear=" & Year & "  AND (Score1+ISNULL(score2,0)+ISNULL(score3,0))>0 AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND GRADE= " & dr("Grade") & "  ORDER BY ChapterID,Rank")
                While reader.Read()
                    aScore = reader("Score1") + reader("Score2") + reader("Score3")
                    If aScore >= dr("MinScore") Then
                        invitecount = invitecount + 1
                        lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                    ElseIf reader("Rank") = 1 Then '** aScore >= dr("MinScore") And 
                        'Rank 1 score consideration where total contestant is >= 5
                        If aScore >= dr("Rank1Score") And dr("Rank1Score") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(ChildNumber)  from Contestant  Where (Score1+ISNULL(score2,0)+ISNULL(score3,0)) >0  AND ContestYear=" & Year & "   AND ProductCode = '" & dr("ProductCode") & "' AND EventId=2 AND ChapterID =" & reader("chapterid") & "") >= 5 Then
                            cntRank1 = cntRank1 + 1
                            TieIndex = TieIndex + 1
                            lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                            SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                        ElseIf aScore >= dr("NewChScore") And dr("NewChScore") > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(C.ChildNumber)  from Contestant C,Chapter Ch  Where (C.Score1+ISNULL(C.score2,0)+ISNULL(C.score3,0))>0  AND C.ContestYear=" & Year & "  AND C.ProductCode = '" & dr("ProductCode") & "' AND C.EventId=2 and C.ChapterID = Ch.ChapterID and Ch.NewChapterFlag='Y' AND C.chapterid=" & reader("chapterid") & "") >= 5 Then
                            'New Chapter Contestant where Rank 1 and total contestant is >= 5
                            nwchcount = nwchcount + 1
                            lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                            SQLTieQuery = SQLTieQuery & " OR ((Score1+ISNULL(score2,0)+ISNULL(score3,0))=" & aScore.ToString() & " AND ProductCode = '" & dr("ProductCode") & "' AND ChapterID=" & reader("chapterid") & ")"
                            TieIndex = TieIndex + 1
                        End If
                    ElseIf NwChapIDs.Contains("," + reader("chapterid").ToString() & ",") Then
                        aScore = aScore + 1
                        If aScore >= dr("MinScore") Then
                            'New Chapter Contestant by Adding 1 
                            nwchcount = nwchcount + 1
                            lblContestants_Priority.Text = lblContestants_Priority.Text & "," & reader("contestant_id")
                        End If
                    End If
                    numCntestants = numCntestants + 1
                End While
                reader.Close()
            End If
        Next
    End Sub
End Class
