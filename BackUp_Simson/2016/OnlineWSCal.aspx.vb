Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

<Serializable()> Public Class OnlineWSCal
    Inherits System.Web.UI.Page

    Private Shared mintOWkShopID As Integer
    Private mintEventYear As Integer
    Private mintEventID As Integer
    Private mstrEventCode As String
    Private mintProductGroupID As Integer
    Private mstrProductGroupcode As String
    Private mintProductID As Integer
    Private mstrProductCode As String
    Private mRegFee As Double
    Private mLateFee As Double

    'Private mdtOWkshopDate2 As Date
    Private mstrEventDate As String 'Date
    Private mstrRegDLDate As String 'Date
    Private mstrLateDLDate As String 'Date
    Private mstrStartTime As String
    Private mstrTime As String
    Private mstrDuration As String
    Private mstrMaxCap As String
    Private strMaxCap As String

    Private mintEventFeesID As Integer
    Private mintTeacherID As Integer
    Protected strEventDate As String
    Protected strRegDLDate As String
    Protected strLateDLDate As String
    Protected strTime As String
    Private strDuration As String

    Private fUpload As FileUpload
    Dim cnTemp As SqlConnection
    Public ChapterID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Session("LoggedIn") = "true"
            'Session("LoginID") = 4240
            'Session("EventId") = 20

            lblerr.ForeColor = Color.Red
            cnTemp = New SqlConnection(Application("ConnectionString"))
            Page.MaintainScrollPositionOnPostBack = True
            If Not Page.IsPostBack = True Then
                'lstOWkshops.Attributes.Add("onchange", "EnableDisableEvents")

                If LCase(Session("LoggedIn")) <> "true" Then
                    Response.Redirect(".\login.aspx?entry=" & Session("entryToken"))
                End If
                LoadYear(ddlEventYear)
                Session("EventID") = 20
                LoadProductGroup(ddlProductGroup)
                'LoadProduct(ddlProduct)
                LoadDisplayTime(ddlDisplayTime)
                LoadTeacher(ddlTeacher, ddlEventYear.SelectedValue)
                LoadGrid()
            End If
        Catch ex As Exception
            lblerr.Text = "Invalid Exception.Please login again." & ex.ToString()
        End Try
    End Sub
    Private Sub LoadYear(ByVal ddlObject As DropDownList)
        Dim Year As Integer = Now.Year - 1
        For i As Integer = 0 To 4
            ddlObject.Items.Insert(i, New ListItem(Year + i, Year + i))
        Next
        If Session("EventYear") Is Nothing Then
            ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText(Now.Year))
        Else
            ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText(Session("EventYear")))
        End If
        'If Now.Month < 5 Then
        '    ddlObject.SelectedIndex = 0 'Year = Now.Year - 1
        'Else
        '    ddlObject.SelectedIndex = 1 'Year = Now.Year
        'End If
    End Sub

    Private Sub LoadProductGroup(ByVal ddlObject As DropDownList)
        Dim StrSQL As String = ""
        StrSQL = "Select Distinct P.ProductGroupId,P.ProductGroupCode,P.Name from ProductGroup P Inner Join EventFees EF on EF.EventID=P.EventID and EF.ProductGroupID =P.ProductGroupID  where EF.EventID =" & Session("EventID")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ddlObject.Items.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select ProductGroup", 0))
            ElseIf ddlObject.Items.Count = 1 Then
                LoadProduct(ddlProduct, ddlObject.SelectedValue)
            End If
        Else
            lblerr.Text = "No ProductGroups present for the Event."
        End If
    End Sub

    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ProductGroupID As Integer)
        Dim StrSQL As String = ""
        Try
            lblerr.Text = ""

            ddlObject.Items.Clear()
            ddlObject.Enabled = False

            StrSQL = "Select Distinct P.ProductId,P.ProductCode,P.Name from Product P Inner Join EventFees EF on EF.EventID=P.EventID and EF.ProductGroupID =P.ProductGroupID  and P.ProductID= EF.ProductID where EF.EventID =" & Session("EventID") & " and P.ProductGroupID =" & ProductGroupID
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlObject.DataSource = ds
                ddlObject.DataBind()
                If ddlObject.Items.Count > 1 Then
                    ddlObject.Enabled = True
                    ddlObject.Items.Insert(0, New ListItem("Select Product", 0))
                ElseIf ddlObject.Items.Count = 1 Then
                    Dim StringSQL As String = "Select Distinct EventFeesID from EventFees where EventYear =" & ddlEventYear.SelectedValue & " and EventID=" & Session("EventID") & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID = " & ddlProduct.SelectedValue
                    hdnEventFeesID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StringSQL)

                End If
            Else
                lblerr.Text = "No Products present for the Event."
            End If
        Catch ex As Exception
            'Response.Write(StrSQL)
        End Try
    End Sub

    Private Sub LoadDisplayTime(ByVal ddlObject As DropDownList)
        Try

            Dim dt As DataTable
            dt = New DataTable()
            Dim dr As DataRow
            Dim StartTime As DateTime = "8:00 AM " 'Now()
            dt.Columns.Add("ddlText", Type.GetType("System.String"))
            dt.Columns.Add("ddlValue", Type.GetType("System.String"))
            ' "8:00:00 AM " To "12:00:00 AM "
            While StartTime <= "8:00:00 PM "
                dr = dt.NewRow()

                dr("ddlText") = StartTime.ToString("h:mmtt").ToString()
                dr("ddlValue") = StartTime.ToString("h:mmtt").ToString()

                dt.Rows.Add(dr)
                StartTime = StartTime.AddHours(0.5) '.AddMinutes(60)
            End While

            If dt.Rows.Count > 0 Then
                ddlObject.DataSource = dt
                ddlObject.DataTextField = "ddlText".ToString()
                ddlObject.DataValueField = "ddlValue".ToString()
                ddlObject.DataBind()
            End If
            'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
            'ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
            ddlObject.Items.Insert(0, New ListItem("TBD", ""))

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        lblerr.Text = ""
        lblFileErr.Text = ""
        LoadProductGroup(ddlProductGroup)
        LoadGrid()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        If ddlProductGroup.SelectedValue > 0 Then
            LoadProduct(ddlProduct, ddlProductGroup.SelectedValue)
        Else
            lblerr.Text = "Please select Product."
            ddlProduct.Enabled = False
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'LoadTeacher(ddlTeacher, ddlEventYear.SelectedValue)
        Try
            lblerr.Text = ""
            Dim StrSQL As String = "Select Distinct EventFeesID from EventFees where EventYear =" & ddlEventYear.SelectedValue & " and EventID=" & Session("EventID") & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID = " & ddlProduct.SelectedValue
            hdnEventFeesID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL)
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    pIndSearch.Visible = True
    'End Sub

    'Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim firstName As String = String.Empty
    '    Dim lastName As String = String.Empty
    '    Dim email As String = String.Empty
    '    Dim strSql As New StringBuilder
    '    firstName = txtFirstName.Text
    '    lastName = txtLastName.Text
    '    email = txtEmail.Text
    '    Dim length As Integer = 0
    '    If firstName.Length > 0 Then
    '        strSql.Append("  I.firstName like '%" + firstName + "%'")
    '    End If
    '    If (lastName.Length > 0) Then
    '        length = strSql.ToString().Length
    '        If (length > 0) Then
    '            strSql.Append(" and I.lastName like '%" + lastName + "%'")
    '        Else
    '            strSql.Append("  I.lastName like '%" + lastName + "%'")
    '        End If
    '    End If
    '    If (email.Length > 0) Then
    '        length = strSql.ToString().Length
    '        If (length > 0) Then
    '            strSql.Append(" and I.Email like '%" + email + "%'")
    '        Else
    '            strSql.Append("  I.Email like '%" + email + "%'")
    '        End If
    '    End If
    '    If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
    '        strSql.Append(" order by I.lastname,I.firstname")
    '        SearchMembers(strSql.ToString())
    '    Else
    '        lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
    '        lblIndSearch.Visible = True
    '    End If

    'End Sub

    'Private Sub SearchMembers(ByVal sqlSt As String)
    '    Dim prmArray(1) As SqlParameter
    '    prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "[usp_SelectCalSignupVolunteers]", prmArray)
    '    Dim dt As DataTable = ds.Tables(0)
    '    Dim Count As Integer = dt.Rows.Count
    '    Dim dv As DataView = New DataView(dt)
    '    GridMemberDt.DataSource = dt
    '    GridMemberDt.DataBind()
    '    If (Count > 0) Then
    '        Panel4.Visible = True
    '        lblIndSearch.Text = ""
    '        lblIndSearch.Visible = False
    '    Else
    '        lblIndSearch.Text = "No Volunteer match found"
    '        lblIndSearch.Visible = True
    '        Panel4.Visible = False
    '    End If
    'End Sub
    'Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
    '    lblerr.Text = ""
    '    Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
    '    Dim row As GridViewRow = GridMemberDt.Rows(index)
    '    ' IncurredExpence name Search
    '    Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
    '    If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
    '        txtName.Text = row.Cells(1).Text + " " + row.Cells(2).Text
    '        hdnMemberID.Value = GridMemberDt.DataKeys(index).Value
    '        pIndSearch.Visible = False
    '        lblIndSearch.Visible = False
    '        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From OnlineWSCal Where TeacherID=" & hdnMemberID.Value & "") > 0 Then
    '            lblerr.Text = "The member already exists for Online Work shop Calendar."
    '        End If
    '    Else
    '        lblIndSearch.Visible = True
    '        lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
    '    End If
    'End Sub

    'Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    pIndSearch.Visible = False
    'End Sub

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Clear()
    'End Sub

    'Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    txtName.Text = ""

    'End Sub

    'Private Sub Clear()

    'End Sub

    'Protected Sub btnClear_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
    '    txtName.Text = ""
    'End Sub

    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click
        Dim StrInsert As String = ""
        lblerr.ForeColor = Color.Red
        lblFileErr.Text = ""
        Try
            If ddlProductGroup.SelectedItem.Value = 0 Then
                lblerr.Text = "Please Select Product Group"
                Exit Sub
            ElseIf ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Product."
                Exit Sub
            ElseIf ddlProduct.Items.Count < 1 Then
                lblerr.Text = "No Products present to add."
                Exit Sub
            ElseIf ddlDisplayTime.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Time."
                Exit Sub
            ElseIf ddlDuration.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Duration."
                Exit Sub
            ElseIf ddlTeacher.SelectedIndex = 0 Then 'txtName.Text = "" Then
                lblerr.Text = "Please Select Teacher"
                Exit Sub
            ElseIf txtMaxCap.Text = "" Then
                lblerr.Text = "Please enter max. capacity."
                Exit Sub
            End If
            If (txtStartDate.Text <> "" And txtStartDate.Text.Trim <> "TBD") Then
                If Convert.ToDateTime(txtStartDate.Text) < Now.Date() Then
                    lblerr.Text = "Start Date should be future date"
                    Exit Sub
                Else
                    lblerr.Text = ""
                End If
            End If
            If (txtRegDeadline.Text <> "" And txtRegDeadline.Text.Trim <> "TBD") And (txtLateRegDeadline.Text <> "" And txtLateRegDeadline.Text.Trim <> "TBD") Then 'ddlRegDeadline.SelectedIndex > 1 And ddlLateRegDL.SelectedIndex > 1 Then
                If Convert.ToDateTime(txtLateRegDeadline.Text) < Convert.ToDateTime(txtRegDeadline.Text) Then
                    lblerr.Text = "Late Reg Deadline Date must be greater than Deadline date"
                    Exit Sub
                    'ElseIf txtLateRegDeadline.Text <> "" And Convert.ToDateTime(txtStartDate.Text) < Convert.ToDateTime(txtLateRegDeadline.Text) Then
                    '    lblDataErr.Text = "Start Date must be greater than Late Reg Deadline Date"
                End If
            End If

            Dim StrCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "'")
            Dim StrTeachCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "' and ( ProductGroupId<>" & ddlProductGroup.SelectedValue & " Or ProductID<>" & ddlProduct.SelectedValue & ") and TeacherID=" & ddlTeacher.SelectedValue)
            If StrCount > 0 Then
                lblerr.Text = "Online Workshop already exists for the selected date and time."
                Exit Sub
            ElseIf StrTeachCount > 0 Then
                lblerr.Text = "Online Workshop already exists with the same date and time for different product for the selected volunteer."
                Exit Sub
            Else
                If hdnEventFeesID.Value.Length = 0 Then
                    lblerr.Text = "EventFees details has not been inserted for the year " & ddlEventYear.SelectedValue & "."
                    Exit Sub
                End If

                If (FileUpLoad1.HasFile) Then 
                    If IsValidFileName(ddlEventYear.SelectedValue.ToString, ddlProductGroup.SelectedValue, FileUpLoad1, txtStartDate.Text) = False Then
                        lblFileErr.Visible = True
                        Exit Sub
                        'error msg
                    End If
                End If
                StrInsert = StrInsert & "Insert into OnlineWSCal(TeacherID,EventYear,EventID,ProductGroupId,ProductGroupCode,ProductID,ProductCode,Date,Time,Duration,RegistrationDeadline,LateFeeDeadLine,EventFeesID,EventFee,CreatedDate,CreatedBy,MaxCap,FileName) Values"
                StrInsert = StrInsert & "(" & ddlTeacher.SelectedValue & "," & ddlEventYear.SelectedValue & "," & Session("EventID") & "," & ddlProductGroup.SelectedValue & ",(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=" & ddlProductGroup.SelectedValue & " and EventId =" & Session("EventID") & ")," & ddlProduct.SelectedValue & ",(Select Top 1 ProductCode from Product  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & "  and EventId =" & Session("EventID") & "),'" & txtStartDate.Text & "','" & ddlDisplayTime.Text & "'," & ddlDuration.SelectedValue & ",'" & txtRegDeadline.Text & "','" & txtLateRegDeadline.Text & "', " & hdnEventFeesID.Value & ",(Select RegFee from EventFees where EventFeesID=" & hdnEventFeesID.Value & "),GETDATE()," & Session("LoginID") & "," & txtMaxCap.Text & ","
                If sFileName.Length = 0 Then
                    StrInsert = StrInsert & " NULL)"
                Else
                    StrInsert = StrInsert & " '" & sFileName & "')"
                End If
                '" & sFileName & "')"
                '  Response.Write(StrInsert)
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrInsert) > 0 Then
                    If (FileUpLoad1.HasFile) Then
                        SaveFile(FileUpLoad1)
                    End If
                    lblerr.ForeColor = Color.Green
                    lblerr.Text = "Added Succesfully."
                    LoadGrid()
                End If



                End If
        Catch ex As Exception
            'Response.Write(ex.ToString() & StrInsert)
            lblerr.Text = "Not Added. " + ex.ToString()
        End Try
    End Sub
    Private Sub SaveFile(objFileUpload As FileUpload)

        If (objFileUpload.HasFile) Then
            Dim path As String = "~/IncludeOnlineWS"
            If Not Directory.Exists(Server.MapPath(path)) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            objFileUpload.SaveAs(Server.MapPath(path) + "/" + sFileName)
        Else
            lblFileErr.Text = "No uploaded file"
        End If
    End Sub
    Dim sFileName As String = ""
    Private Function IsValidFileName(yr As String, pgroupid As Integer, objfupload As FileUpload, evDT As Date) As Boolean
        Dim strPGCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=" & pgroupid)
        '  Dim ext As String = System.IO.Path.GetExtension(objfupload.FileName) '= FileUpLoad1.PostedFile.FileName.
        Dim strDt As String = evDT.Month & evDT.Day & evDT.Year
        Dim strDt2 As String = ""
        If evDT.Month < 10 Then
            strDt2 = "0" & evDT.Month & evDT.Day & evDT.Year
        End If

        sFileName = yr & "_" & strPGCode & "_OWorkShopBook_" & strDt & ".zip" ' + ext
        Dim sFName2 As String = yr & "_" & strPGCode & "_OWorkShopBook_" & strDt2 & ".zip"
        If objfupload.FileName.ToString() <> sFileName And objfupload.FileName.ToString() <> sFName2 Then
            lblFileErr.Visible = False
            lblFileErr.Text = " Workshop book name must be like '" & sFileName & "'"
            Return False
        End If

        Return True
    End Function
    Private Sub LoadGrid()
        Try
            Dim StrSql As String = " Select OnlineWSCalID,IsNull(EventFeesID,0) as EventFeesID,(Select FirstName + '' + LastName from IndSpouse where autoMemberID =TeacherID) as Teacher,TeacherID,EventYear,EventID,(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=OWS.ProductGroupId and EventId =" & Session("EventID") & ") as ProductGroupCode,(Select Top 1 ProductCode from Product where ProductGroupId=OWS.ProductGroupId and ProductId=OWS.ProductId and EventId =" & Session("EventID") & ") as ProductCode,ProductGroupId,ProductID,Date,ISNULL(CONVERT(VARCHAR(10),CAST(Time as Time),100),'TBD') as Time,Duration,RegistrationDeadline as RegDeadline,LateFeeDeadLine as LateRegDLDate,IsNull(MaxCap,0) as MaxCap,isnull(filename,'') FileName from OnlineWSCal OWS where EventYear =" & ddlEventYear.SelectedValue

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql) ' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductID" & ddlProduct.SelectedValue & "") ' and Date ='" & txtStartDate.Text & "' and Time='" & ddlDisplayTime.Text & "'" ")
            If ds.Tables(0).Rows.Count > 0 Then
                grdTarget.Visible = True
                grdTarget.DataSource = ds
                grdTarget.DataBind()

                grdTarget.Columns(1).Visible = False
                Session("dsOWkshop") = ds
            Else
                grdTarget.Visible = False
                grdTarget.DataSource = Nothing
                grdTarget.DataBind()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetEventDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If
            If Trim(mstrEventDate) <> "" And Trim(mstrEventDate) <> "TBD" Then
                txtTemp.Text = mstrEventDate
            ElseIf Trim(strEventDate) <> "" And Trim(strEventDate) <> "TBD" Then
                txtTemp.Text = strEventDate
            ElseIf dr.Item("Date").ToString() <> "" Then
                txtTemp.Text = dr.Item("Date")
            Else
                txtTemp.Text = "TBD"
            End If

            If txtTemp.Text <> "TBD" Then
                dr.Item("Date") = txtTemp.Text
            End If
            'Session("editRow")= dr
            Cache("editRow") = dr
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetRegDeadline_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            If Trim(mstrRegDLDate) <> "" And Trim(mstrRegDLDate) <> "TBD" Then
                txtTemp.Text = mstrRegDLDate
            ElseIf Trim(strRegDLDate) <> "" And Trim(strRegDLDate) <> "TBD" Then
                txtTemp.Text = strRegDLDate
            ElseIf dr.Item("RegDeadLine").ToString() <> "" Then
                txtTemp.Text = dr.Item("RegDeadLine")
            Else
                txtTemp.Text = "TBD"
            End If
            If txtTemp.Text <> "TBD" Then
                dr.Item("RegDeadLine") = txtTemp.Text
            End If
            'If strRegDLDate = "" And (dr.Item("RegDeadLine") = "" Or dr.Item("RegDeadLine") = "TBD") Then
            '    If dr.Item("Date") <> "" And dr.Item("Date") <> "TBD" Then
            '        txtTemp.Text = (Convert.ToDateTime(dr.Item("Date"))).AddDays(-14)
            '    End If
            'ElseIf dr.Item("RegDeadLine") <> "" Then
            '    txtTemp.Text = dr.Item("RegDeadLine")
            'Else
            '    txtTemp.Text = strRegDLDate 'dr.Item("RegDeadLine") '
            '    dr.Item("RegDeadLine") = txtTemp.Text
            'End If
            Cache("editRow") = dr 'Session("editRow")
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub SetLateRegDLDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            If Trim(mstrLateDLDate) <> "" And Trim(mstrLateDLDate) <> "TBD" Then
                txtTemp.Text = mstrLateDLDate
            ElseIf Trim(strLateDLDate) <> "" And Trim(strLateDLDate) <> "TBD" Then
                txtTemp.Text = strLateDLDate

            ElseIf dr.Item("LateRegDLDate").ToString() <> "" Then
                txtTemp.Text = dr.Item("LateRegDLDate")
            Else
                txtTemp.Text = "TBD"
            End If
            If txtTemp.Text <> "TBD" Then
                dr.Item("LateRegDLDate") = txtTemp.Text
            End If



            'If strLateDLDate = "" And (dr.Item("LateRegDLDate") = "" Or dr.Item("LateRegDLDate") = "TBD") Then
            '    If dr.Item("Date") <> "" And dr.Item("Date") <> "TBD" Then
            '        txtTemp.Text = (Convert.ToDateTime(dr.Item("Date"))).AddDays(-7)
            '    End If
            'ElseIf dr.Item("LateRegDLDate") <> "" Then
            '    txtTemp.Text = dr.Item("LateRegDLDate")
            'Else
            '    txtTemp.Text = strLateDLDate
            'End If
            Cache("editRow") = dr 'Session("editRow")

        Catch ex As Exception
            'Response.Write(ex.ToString())

        End Try
    End Sub

    Public Sub Calendar1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Try
            Dim myCalendar As WebControls.Calendar = CType(grdTarget.Items(CalRow.Value).FindControl("Calendar1"), WebControls.Calendar)   'Connect to the Calendar in the current row
            Dim myTextbox As TextBox = CType(grdTarget.Items(CalRow.Value).FindControl("txtEditEventDate"), TextBox)
            myTextbox.Text = myCalendar.SelectedDate
            myCalendar.Visible = False

            strEventDate = myTextbox.Text

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            dr.Item("Date") = myTextbox.Text

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub SetMaxCap_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtTemp As System.Web.UI.WebControls.TextBox
            txtTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            If Trim(mstrMaxCap) <> "" Then 'And Trim(mstrLateDLDate) <> "TBD" Then
                txtTemp.Text = mstrMaxCap
            ElseIf Trim(strMaxCap) <> "" Then  'And Trim(strLateDLDate) <> "TBD" Then
                txtTemp.Text = strMaxCap

            ElseIf dr.Item("MaxCap").ToString() <> "" Then
                txtTemp.Text = dr.Item("MaxCap")
            Else
                txtTemp.Text = ""
            End If

            Cache("editRow") = dr 'Session("editRow")

        Catch ex As Exception
            'Response.Write(ex.ToString())

        End Try
    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Connect to the calendar control
            Dim myCalendar As Calendar = grdTarget.Items(grdTarget.EditItemIndex).FindControl("Calendar1")
            'This code will change the state to opposite of what it is now.
            myCalendar.Visible = Not myCalendar.Visible
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Public Sub Calendar2_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim myCalendar As WebControls.Calendar = CType(grdTarget.Items(CalRow.Value).FindControl("Calendar2"), WebControls.Calendar)   'Connect to the Calendar in the current row
            Dim myTextbox As TextBox = CType(grdTarget.Items(CalRow.Value).FindControl("txtEditRegDeadLine"), TextBox)
            myTextbox.Text = myCalendar.SelectedDate
            myCalendar.Visible = False

            strRegDLDate = myTextbox.Text
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If
            dr.Item("RegDeadLine") = myTextbox.Text
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myCalendar As Calendar = grdTarget.Items(grdTarget.EditItemIndex).FindControl("Calendar2")
        myCalendar.Visible = Not myCalendar.Visible
    End Sub

    Public Sub Calendar3_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim myCalendar As WebControls.Calendar = CType(grdTarget.Items(CalRow.Value).FindControl("Calendar3"), WebControls.Calendar)   'Connect to the Calendar in the current row
            Dim myTextbox As TextBox = CType(grdTarget.Items(CalRow.Value).FindControl("txtEditLateFeeDeadLine"), TextBox)
            myTextbox.Text = myCalendar.SelectedDate
            myCalendar.Visible = False
            strLateDLDate = myTextbox.Text

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
            If (dr Is Nothing) Then
                Return
            End If

            dr.Item("LateRegDLDate") = myTextbox.Text
        Catch ex As Exception
            'Response.Write(ex.ToString())

        End Try
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim myCalendar As Calendar = grdTarget.Items(grdTarget.EditItemIndex).FindControl("Calendar3")
        myCalendar.Visible = Not myCalendar.Visible

    End Sub
    Public Sub SetDropDown_Time(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            LoadDisplayTime(ddlTemp)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If
            If ddlTemp.SelectedIndex <= 0 Then
                If dr.Item("Time") <> "" Then ' ddlTemp.SelectedIndex <= 0 Then
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Time")))
                ElseIf Trim(strTime).ToString() <> "TBD" Then
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(strTime))
                ElseIf Trim(mstrTime).ToString() <> "TBD" Then
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(mstrTime))
                Else
                    ddlTemp.Items(0).Selected = True
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetDropDown_Teacher(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If
            LoadTeacher(ddlTemp, dr.Item("EventYear"))

            If dr.Item("TeacherID") > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("TeacherID")))
            Else
                ddlTemp.Items(0).Selected = True

            End If

            'If mintTeacherID > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
            '    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(mintTeacherID))
            '   Else
            '    ddlTemp.Items(0).Selected = True

            'End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub SetDropDown_ProductGroup(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If
            LoadProductGroup(ddlTemp)

            'dr.Item("ProductGroupId")
            If dr.Item("ProductGroupId") > 0 Then 'mintProductGroupID > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("ProductGroupId"))) 'mintProductGroupID
            Else
                ddlTemp.Items(0).Selected = True
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub ddlDGProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        'Dim rowProdGroupId As Integer = 0
        'Dim rowProdGroupCode As String = ""

        'Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("ProductGroupCode") = ddlTemp.SelectedItem.Text
        dr.Item("ProductGroupId") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr

        'Session("editRow") = dr
    End Sub
    Public Sub ddlDGProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("ProductCode") = ddlTemp.SelectedItem.Text
        dr.Item("ProductId") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub
    Public Sub ddlDGDuration_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("Duration") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub
    Public Sub ddlDGTime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("Time") = ddlTemp.SelectedItem.Value
        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub

    Public Sub ddlDGTeacher_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If
        dr.Item("TeacherID") = ddlTemp.SelectedItem.Value
        dr.Item("Teacher") = ddlTemp.SelectedItem.Text

        Cache("editRow") = dr
        'Session("editRow") = dr
    End Sub


    Public Sub SetDropDown_Product(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim rowEventId As Integer = 0
        Dim rowProdGroupId As Integer = 0
        Dim rowProdGroupCode As String = ""
        Dim rowRoleId As Integer = 0
        Dim rowProductCode As String = ""
        Dim rowProductID As Integer = 0
        'Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow")
        If (dr Is Nothing) Then
            Return
        End If

        If (Not dr.Item("EventId") Is DBNull.Value) Then
            rowEventId = dr.Item("eventId")
        End If
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
            rowProdGroupCode = dr.Item("ProductGroupCode")
        End If
        If (Not dr.Item("ProductCode") Is DBNull.Value) Then
            rowProductCode = dr.Item("productCode")
        End If
        If (Not dr.Item("ProductId") Is DBNull.Value) Then
            rowProductID = dr.Item("ProductId")
        End If

        If (rowProdGroupId <= 0) Then
            LoadProduct(ddlTemp, rowProdGroupId)
        ElseIf (rowProdGroupId > 0) Then
            LoadProduct(ddlTemp, rowProdGroupId)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
        Else
            ddlTemp.Items.Clear()
        End If

        If dr.Item("ProductID") > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("ProductID")))
        ElseIf rowProductID > 0 Then
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
        Else
            ddlTemp.Items(0).Selected = True
        End If


    End Sub
    'Public Sub SetDropDown_Product(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
    '        ddlTemp = sender

    '        Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
    '        If (dr Is Nothing) Then
    '            Return
    '        End If
    '        LoadProduct(ddlTemp, dr.Item("ProductGroupID"))

    '        If mintProductID > 0 Then ' ddlTemp.SelectedIndex <= 0 Then
    '            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(mintProductID))
    '        Else
    '            ddlTemp.Items(0).Selected = True

    '        End If
    '    Catch ex As Exception
    '        ''Response.Write(ex.ToString())
    '    End Try
    'End Sub


    Private Sub LoadTeacher(ByVal ddlTeacher As DropDownList, ByVal EventYear As Integer)
        Dim dsTarget As DataSet
        dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, "Select I.AutoMemberID as TeacherID,I.FirstName + ' ' + I.LastName as Teacher from IndSpouse I Inner join Volunteer V on I.AutoMemberID =V.MemberID where V.RoleID =98")
        'AutoMemberID in "'(Select Distinct TeacherID from OnlineWSCal where EventYear =" & EventYear & ")")
        If Not ddlTeacher Is Nothing Then
            ddlTeacher.DataSource = dsTarget
            ddlTeacher.DataBind()
            ddlTeacher.DataValueField = "TeacherID"
            ddlTeacher.DataTextField = "Teacher"
            ddlTeacher.DataBind()

            If ddlTeacher.Items.Count > 1 Then
                ddlTeacher.Items.Insert(0, "Select Teacher")
            End If
        End If

    End Sub
    Public Sub SetDropDown_Duration(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender

            Dim dr As DataRow = CType(Cache("editRow"), DataRow) 'Session("editRow") 
            If (dr Is Nothing) Then
                Return
            End If

            If dr.Item("Duration").ToString() <> "TBD" Then
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Duration")))
            ElseIf strDuration Is Nothing Then
                ddlTemp.SelectedValue = strDuration
            ElseIf mstrDuration Is Nothing Then
                ddlTemp.SelectedValue = mstrDuration
            Else
                ddlTemp.Items(0).Selected = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub grdTarget_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.EditCommand

        Try
            'tblAddOWkshops.Visible = False
            'PnlAddOWkshops.Enabled = False
            lblerr.Text = ""
            lblerr.ForeColor = Color.Red
            grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
            mintOWkShopID = CInt(e.Item.Cells(1).Text)

            'Session("mintOWkShopID") = mintOWkShopID

            mintEventFeesID = CInt(e.Item.Cells(2).Text)
            mintEventYear = CInt(e.Item.Cells(3).Text)
            mintEventID = CInt(e.Item.Cells(4).Text)

            mintProductGroupID = CInt(e.Item.Cells(5).Text)
            mintProductID = CInt(e.Item.Cells(6).Text)
            mstrProductGroupcode = CStr(e.Item.Cells(7).Text)
            mstrProductCode = CStr(e.Item.Cells(8).Text)
            strEventDate = CType(e.Item.Cells(9).FindControl("lblEventDate"), Label).Text
            strTime = CType(e.Item.Cells(10).FindControl("lblTime"), Label).Text
            strDuration = CType(e.Item.Cells(11).FindControl("lblDuration"), Label).Text
            mintTeacherID = CInt(e.Item.Cells(12).Text)

            strRegDLDate = CType(e.Item.Cells(14).FindControl("lblRegDeadLine"), Label).Text
            strLateDLDate = CType(e.Item.Cells(15).FindControl("lblLateFeeDeadLine"), Label).Text
            strMaxCap = CType(e.Item.Cells(16).FindControl("lblMaxCap"), Label).Text


            'disenable delete and copy buttons
            Dim intcell As Integer = e.Item.Cells.Count - 2
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(intcell)

            Dim OWkshop_ds As DataSet = CType(Session("dsOWkshop"), DataSet)
            Dim currentRowIndex As Integer = e.Item.ItemIndex
            Dim OWkshop_Copy As DataSet
            Dim dr As DataRow
            If Not OWkshop_ds Is Nothing Then
                OWkshop_Copy = OWkshop_ds.Copy
                dr = OWkshop_Copy.Tables(0).Rows(currentRowIndex)
            End If
            Cache("editRow") = dr
            'Session("editRow")= dr

            CalRow.Value = e.Item.ItemIndex
            LoadGrid()
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub grdTarget_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.UpdateCommand
        Try
            Dim row As Integer = CInt(e.Item.ItemIndex)
            Dim txtEditRegDeadLine As TextBox, txtEditEventDate As TextBox, txtLateDLDate As TextBox, txtMaxCap As TextBox, ddlTemp As DropDownList, blnValidData As Boolean, lblText As Label
            Dim intSelStartTime As Integer, intSelEndTime As Integer, intSelCheckInTime As Integer

            'Venue
            lblerr.Text = ""
            lblGridError.Text = ""
            lblText = e.Item.FindControl("lblGridEditWSDate")
            '
            txtEditEventDate = e.Item.FindControl("txtEditEventDate")
            mstrEventDate = IIf(txtEditEventDate.Text = "" Or txtEditEventDate.Text.Trim = "TBD", "", txtEditEventDate.Text)
            'mstrEventDate = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedItem.Text)

            txtEditRegDeadLine = e.Item.FindControl("txtEditRegDeadLine")
            mstrRegDLDate = IIf(txtEditRegDeadLine.Text = "" Or txtEditRegDeadLine.Text.Trim = "TBD", "", txtEditRegDeadLine.Text)

            txtLateDLDate = e.Item.FindControl("txtEditLateFeeDeadLine")
            mstrLateDLDate = IIf(txtLateDLDate.Text = "" Or txtLateDLDate.Text = "TBD", "", txtLateDLDate.Text)

            ddlTemp = e.Item.FindControl("ddlTeacher")
            mintTeacherID = IIf(ddlTemp.SelectedValue <= 0, 0, ddlTemp.SelectedValue)

            ddlTemp = e.Item.FindControl("ddlTime")
            mstrTime = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedItem.Text)
            intSelEndTime = ddlTemp.SelectedIndex '3 +

            ddlTemp = e.Item.FindControl("ddlDuration")
            mstrDuration = IIf(ddlTemp.SelectedIndex <= 0, "", ddlTemp.SelectedValue)

            ddlTemp = e.Item.FindControl("ddlProductGroup")
            mstrProductGroupcode = IIf(ddlTemp.SelectedValue <= 0, "", ddlTemp.SelectedItem.Text)

            ddlTemp = e.Item.FindControl("ddlProductGroup")
            mintProductGroupID = IIf(ddlTemp.SelectedValue <= 0, 0, ddlTemp.SelectedValue)

            ddlTemp = e.Item.FindControl("ddlProduct")
            mstrProductCode = IIf(ddlTemp.SelectedValue <= 0, "", ddlTemp.SelectedItem.Text)

            ddlTemp = e.Item.FindControl("ddlProduct")
            mintProductID = IIf(ddlTemp.SelectedValue <= 0, 0, ddlTemp.SelectedValue)

            txtMaxCap = e.Item.FindControl("txtEditMaxCap")
            mstrMaxCap = IIf(txtMaxCap.Text = "" Or txtMaxCap.Text = "TBD", "", txtMaxCap.Text)

            fUpload = e.Item.FindControl("fupload")
            sFileName = CType(e.Item.FindControl("lblFileName"), Label).Text

            blnValidData = False
            lblGridError.Visible = False
            lblGridError.Text = ""

            If mstrEventDate <> "" Then 'And mstrRegDLDate <> "" Then
                If Convert.ToDateTime(mstrEventDate) < Now.Date() Then
                    lblGridError.Text = "Event Date must be Future date"
                End If
            End If
            If mstrRegDLDate <> "" And mstrLateDLDate <> "" Then
                If Convert.ToDateTime(mstrLateDLDate) < Convert.ToDateTime(mstrRegDLDate) Then
                    lblGridError.Text = "Late Reg Deadline Date must be greater than Deadline date"
                    'ElseIf mstrRegDLDate <> "" Then
                    '    If Convert.ToDateTime(mstrEventDate) < Convert.ToDateTime(mstrLateDLDate) Then
                    '        lblGridError.Text = "Start Date must be greater than Late Reg Deadline Date"
                    '    End If
                End If
            End If
            If mstrMaxCap = "" Then 'And mstrRegDLDate <> "" Then
                lblGridError.Text = "Event Date must be Future date"
            End If

            If UpdateWkShops() = False Then
                Exit Sub
            End If
            grdTarget.EditItemIndex = -1

            If lblGridError.Text <> "" Then
                lblGridError.Visible = True
                blnValidData = False
                GoTo FinalProcess
            End If
            'update routine
            'disenable delete buttons
            'Dim intcell As Integer = e.Item.Cells.Count - 2
            'Dim myTableCell As TableCell
            'myTableCell = e.Item.Cells(intcell)
            'Dim btnDelete As Button = myTableCell.Controls(0)
            'btnDelete.Enabled = True

            LoadGrid()
            Exit Sub

FinalProcess:
            grdTarget.EditItemIndex = CInt(e.Item.ItemIndex)
            LoadGrid()

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Function UpdateWkShops() As Boolean
        Try
            lblerr.ForeColor = Color.Red
            lblFileErr.Text = ""
            'Response.Write(StrUpdate)
            Dim StrCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & mintProductGroupID & " and ProductID=" & mintProductID & " and Date ='" & mstrEventDate & "' and Time='" & mstrTime & "' and OnlineWSCalID<>" & mintOWkShopID)
            Dim StrTeachCount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from OnlineWSCal where EventYear =" & ddlEventYear.SelectedValue & " and Date ='" & mstrEventDate & "' and Time='" & mstrTime & "' and ( ProductGroupId<>" & mintProductGroupID & " Or ProductID<>" & mintProductID & ") and TeacherID =" & mintTeacherID & " and OnlineWSCalID<>" & mintOWkShopID)
            If StrCount > 0 Then
                lblGridError.Text = " Online Workshop already exists for the selected date and Time. "

                Return True
            ElseIf StrTeachCount > 0 Then
                lblGridError.Text = " Online Workshop already exists with the same date and time for different product for the selected volunteer. "
                Return True
            Else
                If fUpload.HasFile Then
                    If IsValidFileName(ddlEventYear.SelectedValue, mintProductGroupID, fUpload, mstrEventDate) = False Then
                        lblFileErr.Visible = True
                        Return False
                        'error msg
                    End If
                End If
                Dim iEventFeesId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull (EventFeesId,0) from EventFees where EventId=" & Session("EventID") & " and EventYear =" & ddlEventYear.SelectedValue & " and ProductGroupId=" & mintProductGroupID & " and ProductId=" & mintProductID)

                Dim StrUpdate As String = " Update OnlineWSCal set EventFeesId=" & iEventFeesId & " , TeacherID=" & mintTeacherID & ",ProductGroupCode=(Select Top 1 ProductGroupCode from ProductGroup where ProductGroupId=" & mintProductGroupID & " and EventId =" & Session("EventID") & "),ProductGroupId=" & mintProductGroupID & ",ProductCode=(Select Top 1 ProductCode from Product  where ProductGroupId=" & mintProductGroupID & " and ProductId=" & mintProductID & "  and EventId =" & Session("EventID") & "),ProductID=" & mintProductID & ",Date='" & mstrEventDate & "',Time='" & mstrTime & "',Duration=" & mstrDuration & ",RegistrationDeadline='" & mstrRegDLDate & "',LateFeeDeadLine='" & mstrLateDLDate & "',ModifiedDate=GETDATE(),ModifiedBy=" & Session("LoginID") & ",MaxCap=" & mstrMaxCap & ", FileName="
                If sFileName.Length = 0 Then
                    StrUpdate = StrUpdate & "NULL"
                Else
                    StrUpdate = StrUpdate & "'" & sFileName & "'"
                End If
                StrUpdate = StrUpdate & " where OnlineWSCalID = " & mintOWkShopID
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate)
                If fUpload.HasFile Then
                    SaveFile(fUpload)
                End If
                lblerr.ForeColor = Color.Green
                lblerr.Text = "Updated successfully"
                lblGridError.Text = ""
                'tblAddPrepclubs.Visible = True
                PnlAddOWkshops.Enabled = True
                lblerr.Visible = True
                Return True

            End If
        Catch ex As SqlException
            'Response.Write(ex.ToString()) ' & " Update OnlineWSCal set TeacherID=" & mintTeacherID & ",ProductGroupId=" & mintProductGroupID & ",ProductID=" & mintProductID & ",Date='" & mstrEventDate & "',Time='" & mstrTime & "',Duration=" & mstrDuration & ",RegistrationDeadline='" & mstrRegDLDate & "',LateFeeDeadLine='" & mstrLateDLDate & "',ModifiedDate=GETDATE(),ModifiedBy=" & Session("LoginID") & " where OnlineWSCalID=" & mintOWkShopID)
            lblerr.Text = ex.Message
            lblerr.Visible = True
            Return False
        End Try
    End Function

    Protected Sub grdTarget_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdTarget.ItemDataBound
        Try

            Dim dsTarget As New DataSet

            ' First, make sure we're NOT dealing with a Header or Footer row
            If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
                'Now, reference the PushButton control that the Delete ButtonColumn has been rendered to
                Dim rownum As Integer
                rownum = e.Item.ItemIndex + 1
                Dim intOWkShopID As String = CStr(e.Item.Cells(1).Text)
                Dim ddlTeacherTemp As DropDownList
                ddlTeacherTemp = e.Item.Cells(13).FindControl("ddlTeacher")
                dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, "Select FirstName + ' ' + LastName as Teacher from IndSpouse where AutoMemberID in (Select Distinct TeacherID from OnlineWSCal where EventYear =" & e.Item.Cells(3).Text & ")")
                If Not ddlTeacherTemp Is Nothing Then
                    ddlTeacherTemp.DataSource = dsTarget
                    ddlTeacherTemp.DataBind()
                    If ddlTeacherTemp.Items.Count > 1 Then
                        ddlTeacherTemp.Items.Insert(0, "Select Teacher")
                    End If
                End If

                Dim ddlTimeTemp As DropDownList
                ddlTimeTemp = e.Item.Cells(10).FindControl("ddlTime")
                If Not ddlTimeTemp Is Nothing Then
                    LoadDisplayTime(ddlTimeTemp)
                End If

                Dim ddlDurationTemp As DropDownList
                ddlDurationTemp = e.Item.Cells(11).FindControl("ddlDuration")


                Dim intPrepClubIDCell As Integer = 1
                Dim PrepClubIDCell As TableCell = e.Item.Cells(intPrepClubIDCell)
                Dim strValue As String = PrepClubIDCell.Text
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub grdTarget_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdTarget.CancelCommand
        Try
            grdTarget.EditItemIndex = -1
            LoadGrid()
            lblGridError.Visible = False
            PnlAddOWkshops.Enabled = True
        Catch ex As Exception
            ''Response.Write(ex.ToString())
        End Try
    End Sub
End Class
