﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using System.Drawing;

public partial class Zoom_API_SetUpZoomSessions : System.Web.UI.Page
{

    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    protected void Page_Load(object sender, EventArgs e)
    {
        spnError.InnerText = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                LoadEvent(ddlEvent);
                fillMeetingGrid();
            }
            // 
            //if (!IsPostBack)
            //{
            // listMeetings();
            //}
        }
    }
    public int validateMeeting()
    {
        int retVal = 1;
        if (txtTopic.Text == "")
        {
            retVal = -1;
            spnError.InnerText = "Please enter Topic";
        }
        //if (txtStartTime.Text == "")
        //{
        //    retVal = -1;
        //    spnError.InnerText = "Please enter Start Time";
        //}
        if (txtDuration.Text == "")
        {
            retVal = -1;
            spnError.InnerText = "Please enter Duration";
        }

        if (txtMeetingPwd.Text == "")
        {
            retVal = -1;
            spnError.InnerText = "Please enter Meeting Password";
        }

        return retVal;
    }


    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            if (serviceType == "3")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnMeetingKey.Value + "");

                fillMeetingGrid();
                spnStatus.InnerText = "Zoom Session deleted successfully";
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);
            }

        }
        catch
        {
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }

    public void createZoomMeeting()
    {
        try
        {
            if (hdnIsUpdate.Value == "")
            {
                string URL = string.Empty;
                string service = "1";
                URL = "https://api.zoom.us/v1/meeting/create";
                string urlParameter = string.Empty;
                if (validateMeeting() == 1)
                {
                    if (ddlMeetingType.SelectedValue == "2")
                    {
                        string date = txtStartDate.Text;
                        string time = txtStartTime.Text;
                        string dateTime = date.Trim() + "T" + time.Trim() + ":00Z";
                        string test = "2016-5-18T12:00:00Z";
                        urlParameter += "api_key=" + apiKey + "";
                        urlParameter += "&api_secret=" + apiSecret + "";
                        urlParameter += "&data_type=JSON";
                        urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                        urlParameter += "&topic=" + txtTopic.Text + "";
                        urlParameter += "&password=" + txtMeetingPwd.Text + "";
                        urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";
                        urlParameter += "&start_time=" + dateTime + "";
                        urlParameter += "&duration=" + txtDuration.Text + "";
                        urlParameter += "&timezone=GMT+5:30";
                        urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                        urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                        urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                    }
                    else if (ddlMeetingType.SelectedValue == "1")
                    {
                        urlParameter += "api_key=" + apiKey + "";
                        urlParameter += " &api_secret=" + apiSecret + "";
                        urlParameter += " &data_type=JSON";
                        urlParameter += " &host_id=1BYjtcBlQKSXzwHzRqPaxg";
                        urlParameter += " &topic=" + txtTopic.Text + "";
                        urlParameter += " &password=" + txtMeetingPwd.Text + "";
                        urlParameter += " &type=" + ddlMeetingType.SelectedValue + "";


                        urlParameter += " &option_jbh=" + ddlJoinBH.SelectedValue + "";
                        urlParameter += " &option_host_video=" + ddlHostVideo.SelectedValue + "";
                        urlParameter += " &option_audio=" + ddlAudio.SelectedValue + "";
                    }
                    else if (ddlMeetingType.SelectedValue == "3")
                    {
                        urlParameter += "api_key=" + apiKey + "";
                        urlParameter += " &api_secret=" + apiSecret + "";
                        urlParameter += " &data_type=JSON";
                        urlParameter += " &host_id=1BYjtcBlQKSXzwHzRqPaxg";
                        urlParameter += " &topic=" + txtTopic.Text + "";
                        urlParameter += " &password=" + txtMeetingPwd.Text + "";
                        urlParameter += " &type=" + ddlMeetingType.SelectedValue + "";


                        urlParameter += " &option_jbh=" + ddlJoinBH.SelectedValue + "";
                        urlParameter += " &option_host_video=" + ddlHostVideo.SelectedValue + "";
                        urlParameter += " &option_audio=" + ddlAudio.SelectedValue + "";
                    }
                    makeZoomAPICall(urlParameter, URL, service);
                }
                else
                {
                    validateMeeting();
                }
            }
            else
            {
                updateZoomSession();
            }
        }
        catch
        {
        }
    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        createZoomMeeting();
    }
    protected void btnCreateZoomSession_Click(object sender, EventArgs e)
    {
        ScheduleTrainingCenterAll();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDuration.Text = "";
        txtMeetingPwd.Text = "";
        txtTopic.Text = "";
        txtStartTime.Text = "";
        ddlMeetingType.SelectedValue = "2";
        ddlJoinBH.SelectedValue = "true";
        ddlStartType.SelectedValue = "Video";
        ddlHostVideo.SelectedValue = "true";
        ddlAudio.SelectedValue = "voip";
    }

    public void listMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "2";
            URL = "https://api.zoom.us/v1/meeting/list";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            urlParameter += "&page_size=30";
            urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void deleteMeeting()
    {
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            urlParameter += "&id=" + hdnMeetingKey.Value + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void updateZoomSession()
    {
        try
        {
            string URL = string.Empty;
            string service = "1";
            URL = "https://api.zoom.us/v1/meeting/update";
            string urlParameter = string.Empty;
            if (validateMeeting() == 1)
            {
                if (ddlMeetingType.SelectedValue == "2")
                {
                    string date = txtStartTime.Text.Substring(0, 10);
                    string time = txtStartTime.Text.Substring(10, 5);
                    string dateTime = date.Trim() + "T" + time.Trim() + ":00Z";
                    dateTime = "2016-5-20T12:00:00Z";
                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += "&api_secret=" + apiSecret + "";
                    urlParameter += "&data_type=JSON";
                    urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                    urlParameter += "&id=" + hdnMeetingKey.Value + "";
                    urlParameter += "&topic=" + txtTopic.Text + "";
                    urlParameter += "&password=" + txtMeetingPwd.Text + "";
                    urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";
                    urlParameter += "&start_time=" + dateTime + "";
                    urlParameter += "&duration=" + txtDuration.Text + "";
                    urlParameter += "&timezone=GMT+5:30";
                    urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                    urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                    urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                }
                else if (ddlMeetingType.SelectedValue == "1")
                {
                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += " &api_secret=" + apiSecret + "";
                    urlParameter += " &data_type=JSON";
                    urlParameter += " &host_id=1BYjtcBlQKSXzwHzRqPaxg";
                    urlParameter += "&id=" + hdnMeetingKey.Value + "";
                    urlParameter += " &topic=" + txtTopic.Text + "";
                    urlParameter += " &password=" + txtMeetingPwd.Text + "";
                    urlParameter += " &type=" + ddlMeetingType.SelectedValue + "";


                    urlParameter += " &option_jbh=" + ddlJoinBH.SelectedValue + "";
                    urlParameter += " &option_host_video=" + ddlHostVideo.SelectedValue + "";
                    urlParameter += " &option_audio=" + ddlAudio.SelectedValue + "";
                }
                else if (ddlMeetingType.SelectedValue == "3")
                {
                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += " &api_secret=" + apiSecret + "";
                    urlParameter += " &data_type=JSON";
                    urlParameter += " &host_id=1BYjtcBlQKSXzwHzRqPaxg";
                    urlParameter += "&id=" + hdnMeetingKey.Value + "";
                    urlParameter += " &topic=" + txtTopic.Text + "";
                    urlParameter += " &password=" + txtMeetingPwd.Text + "";
                    urlParameter += " &type=" + ddlMeetingType.SelectedValue + "";


                    urlParameter += " &option_jbh=" + ddlJoinBH.SelectedValue + "";
                    urlParameter += " &option_host_video=" + ddlHostVideo.SelectedValue + "";
                    urlParameter += " &option_audio=" + ddlAudio.SelectedValue + "";
                }
                makeZoomAPICall(urlParameter, URL, service);
            }
            else
            {
                validateMeeting();
            }
        }
        catch
        {
        }
    }
    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        deleteMeeting();
    }
    protected void btnUpdateMeeting_Click(object sender, EventArgs e)
    {
        updateZoomSession();
    }

    public void LoadEvent(DropDownList ddlObject)
    {
        try
        {

            string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
            "here EF.EventYear ="
                        + (ddlYear.SelectedValue + "  and E.EventId in(13,19)"));
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.DataSource = ds;
                ddlObject.DataTextField = "EventCode";
                ddlObject.DataValueField = "EventId";
                ddlObject.DataBind();
                if ((ds.Tables[0].Rows.Count > 1))
                {
                    ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                    ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                    ddlObject.Enabled = false;

                    ddlChapter.SelectedValue = "112";
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                    {
                        fillCoach();
                    }
                    else
                    {
                        fillCoach();
                        fillProductGroup();
                    }
                }
                else
                {
                    ddlObject.Enabled = false;
                    fillProductGroup();
                }

            }
            else
            {

                spnError.InnerText = "No Events present for the selected year";
            }
        }
        catch
        {
        }
    }

    public void fillProductGroup()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddlEvent.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y')";
            }
            else
            {
                Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddlEvent.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y')";
            }


            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductGroup.DataSource = ds;
                ddlProductGroup.DataTextField = "ProductGroupCode";
                ddlProductGroup.DataValueField = "ProductGroupId";
                ddlProductGroup.DataBind();
                ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProductGroup.SelectedIndex = 1;
                    ddlProductGroup.Enabled = false;
                    fillProduct();
                }
                else
                {

                    ddlProductGroup.Enabled = true;
                }
            }
        }
        catch
        {
        }
    }
    public void fillProduct()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y')";
            }
            else
            {
                Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + ")";
            }



            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                ddlProduct.Enabled = true;
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "ProductCode";
                ddlProduct.DataValueField = "ProductID";
                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProduct.SelectedIndex = 1;
                    ddlProduct.Enabled = false;
                    loadLevel();
                    LoadSessionNo();
                    if (Session["RoleID"].ToString() == "88")
                    {
                    }

                }
                else
                {

                    ddlProduct.Enabled = true;
                }

            }

        }
        catch
        {
        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();
        //fillCoach();
    }

    public void fillCoach()
    {
        try
        {

            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' order by ID.FirstName ASC";


            }
            else
            {
                cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlCoach.DataSource = ds;
                ddlCoach.DataTextField = "Name";
                ddlCoach.DataValueField = "AutoMemberID";
                ddlCoach.DataBind();
                ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    ddlCoach.Enabled = true;
                }
                else
                {
                    ddlCoach.SelectedValue = Session["LoginID"].ToString();
                    ddlCoach.Enabled = false;
                }

            }

        }
        catch
        {
        }
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLevel();
    }


    public void loadLevel()
    {
        try
        {

            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            else
            {
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                ddlLevel.DataTextField = "Level";
                ddlLevel.DataValueField = "Level";
                ddlLevel.DataSource = ds;
                ddlLevel.DataBind();
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlLevel.Enabled = true;
                }
                else
                {
                    ddlLevel.Enabled = false;
                }
            }

        }
        catch
        {
        }
    }

    public void LoadSessionNo()
    {
        try
        {

            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            else
            {
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {

                ddlSessionNo.DataTextField = "SessionNo";
                ddlSessionNo.DataValueField = "SessionNo";
                ddlSessionNo.DataSource = ds;
                ddlSessionNo.DataBind();
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlSessionNo.Enabled = true;
                }
                else
                {
                    ddlSessionNo.Enabled = false;
                }
            }
        }
        catch
        {
        }

    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddlEvent);

    }
    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }

    public void ScheduleTrainingCenterAll()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;




            if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
            }
            else if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
            }
            else if (ddlCoach.SelectedValue == "All" && ddlProduct.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All")
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
            }
            else
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
            }



            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        count++;

                        string WebExID = "michael.simson@capestart.com";
                        string Pwd = "mxh894";
                        int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                        string ScheduleType = dr["ScheduleType"].ToString();

                        string year = dr["EventYear"].ToString();
                        string eventID = dr["EventID"].ToString();
                        string chapterId = ddlChapter.SelectedValue;
                        string ProductGroupID = dr["ProductGroupID"].ToString();
                        string ProductGroupCode = dr["ProductGroupCode"].ToString();
                        string ProductID = dr["ProductID"].ToString();
                        string ProductCode = dr["ProductCode"].ToString();
                        string Phase = dr["Phase"].ToString();
                        string Level = dr["Level"].ToString();
                        string Sessionno = dr["SessionNo"].ToString();
                        string CoachID = dr["MemberID"].ToString();
                        string CoachName = dr["CoachName"].ToString();

                        string MeetingPwd = "training";


                        string Time = dr["Time"].ToString();
                        string Day = dr["Day"].ToString();
                        string BeginTime = dr["Begin"].ToString();
                        string EndTime = dr["End"].ToString();
                        string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        if (BeginTime == "")
                        {
                            BeginTime = "20:00:00";
                        }
                        if (EndTime == "")
                        {
                            EndTime = "21:00:00";
                        }

                        if (txtDuration.Text != "")
                        {
                            int Duration = Convert.ToInt32(txtDuration.Text);
                        }
                        string timeZoneID = "7";

                        string TimeZone = "";
                        string SignUpId = dr["SignupID"].ToString();
                        double Mins = 0.0;
                        DateTime dFrom;
                        DateTime dTo;
                        string sDateFrom = BeginTime;
                        string sDateTo = EndTime;
                        if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                        {
                            TimeSpan TS = dTo - dFrom;
                            Mins = TS.TotalMinutes;

                        }
                        string durationHrs = Mins.ToString("0");
                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }

                        if (timeZoneID == "4")
                        {
                            TimeZone = "EST/EDT – Eastern";
                        }
                        else if (timeZoneID == "7")
                        {
                            TimeZone = "CST/CDT - Central";
                        }
                        else if (timeZoneID == "6")
                        {
                            TimeZone = "MST/MDT - Mountain";
                        }
                        string userID = Session["LoginID"].ToString();


                        if (hdnMeetingStatus.Value == "SUCCESS")
                        {
                            string meetingURL = (hdnHostURL.Value == "0" ? "" : hdnHostURL.Value);

                            string cmdText = "";

                            cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," + year + "," + eventID + "," + chapterId + "," + ProductGroupID + ",'" + ProductGroupCode + "'," + ProductID + ",'" + ProductCode + "','" + startDate + "','" + EndDate + "','" + BeginTime + "','" + EndTime + "'," + durationHrs + "," + timeZoneID + ",'" + TimeZone + "','" + SignUpId + "'," + CoachID + ",'" + Phase + "','" + Level + "','" + Sessionno + "','" + meetingURL + "','" + hdnSessionKey.Value + "','" + MeetingPwd + "','Active','" + Day + "','" + userID + "','" + WebExID + "','" + Pwd + "'";

                            DataSet objDs = new DataSet();
                            objDs = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);

                            if (null != objDs && objDs.Tables.Count > 0)
                            {
                                if (objDs.Tables[0].Rows.Count > 0)
                                {
                                    if (Convert.ToInt32(objDs.Tables[0].Rows[0]["Retval"].ToString()) > 0)
                                    {

                                        spnStatus.InnerText = "Zoom session Created successfully";
                                        string ChidName = string.Empty;
                                        string Email = string.Empty;
                                        string City = string.Empty;
                                        string Country = string.Empty;
                                        string ChildNumber = string.Empty;
                                        string CoachRegID = string.Empty;

                                        DataSet dsChild = new DataSet();

                                        string ChildText = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + CoachID + " and CR.EventYear=" + ddlYear.SelectedValue + " and CR.EventID=" + ddlEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddlYear.SelectedValue + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + CoachID + " and Approved='Y' and Level='" + Level + "') and CR.Level='" + Level + "'";



                                        dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                                        if (null != dsChild && dsChild.Tables.Count > 0)
                                        {
                                            if (dsChild.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow drChild in dsChild.Tables[0].Rows)
                                                {
                                                    ChidName = drChild["Name"].ToString();
                                                    Email = drChild["WebExEmail"].ToString();
                                                    if (Email.IndexOf(';') > 0)
                                                    {
                                                        Email = Email.Substring(0, Email.IndexOf(';'));
                                                    }
                                                    City = drChild["City"].ToString();
                                                    Country = drChild["Country"].ToString();
                                                    ChildNumber = drChild["ChildNumber"].ToString();
                                                    CoachRegID = drChild["CoachRegID"].ToString();

                                                    if (hdnMeetingStatus.Value == "SUCCESS")
                                                    {

                                                        string CmdChildUpdateText = "update CoachReg set AttendeeJoinURL='" + hdnJoinMeetingUrl.Value + "',Status='Active',ModifyDate=GetDate(), ModifiedBy='" + userID + "' where EventYear=" + ddlYear.SelectedValue + " and CMemberID=" + CoachID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + ProductGroupID + " and ProductID=" + ProductID + " and Approved='Y' and CoachRegID='" + CoachRegID + "'";

                                                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {

                                        spnStatus.InnerText = objDs.Tables[0].Rows[0]["Retval"].ToString();
                                    }
                                }
                            }


                            spnStatus.InnerText = "Zoom session Created successfully";
                        }
                        else
                        {
                            //spnError.InnerText = hdnException.Value;
                        }

                    }
                    fillMeetingGrid();
                }
                else
                {
                    spnError.InnerText = "No child is assigned for the selected Coach..";
                }


            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void fillMeetingGrid()
    {

        string cmdtext = "";
        DataSet ds = new DataSet();

        if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD, case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddlYear.SelectedValue + "' and VC.EventID=" + ddlEvent.SelectedValue + " and VC.ChapterID=" + ddlChapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.SessionType is null order by IP.LastName, IP.FirstName Asc";
        }
        else if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddlYear.SelectedValue + "' and VC.EventID=" + ddlEvent.SelectedValue + " and VC.ChapterID=" + ddlChapter.SelectedValue + " and VC.SessionType is null  order by IP.LastName, IP.FirstName Asc";
        }
        else if (ddlProduct.SelectedValue == "All" && ddlCoach.SelectedValue == "All")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddlYear.SelectedValue + "' and VC.EventID=" + ddlEvent.SelectedValue + " and VC.ChapterID=" + ddlChapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.SessionType is null order by IP.LastName, IP.FirstName Asc";
        }
        else if (ddlProduct.SelectedValue != "")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddlYear.SelectedValue + "' and VC.EventID=" + ddlEvent.SelectedValue + " and VC.ChapterID=" + ddlChapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " AND vc.MemberID=" + ddlCoach.SelectedValue + " and VC.SessionType is null  order by IP.LastName, IP.FirstName Asc";
        }
        else
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey)";
        }
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode, string Event, string level, string sessionNO, int year)
    {


        DataSet ds = new DataSet();
        string CmdText = string.Empty;




        try
        {

            CmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level, CR.SessionNo from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + year + " and CR.EventID=" + Event + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + year + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + MemberID + " and Approved='Y') and CR.Level='" + level + "' and CR.SessionNo=" + sessionNO + " order by C1.Last_Name, C1.First_Name Asc";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();

                }
                else
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;

                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;

                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;

                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;

                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;


                string SessionNo = GrdMeeting.Rows[selIndex].Cells[10].Text.Trim();

                fillStudentList(MemberID, ProductGroupCode, ProductCode, "13", Level, SessionNo, 2015);
            }
            else if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                string Time = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblOrgTime") as Label).Text;
                ddlYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                LoadEvent(ddlEvent);
                ddlEvent.SelectedValue = EventID;
                ddlChapter.SelectedValue = ChapterID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;

                ddlCoach.SelectedValue = MemberID;


                loadLevel();
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                ddlSessionNo.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;
                txtMeetingPwd.Text = GrdMeeting.Rows[selIndex].Cells[13].Text;

                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;

                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;

                btnCreateMeeting.Text = "Update Meeting";
                hdnSessionKey.Value = sessionKey;

                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;

            }
            else if (e.CommandName == "DeleteMeeting")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnMeetingKey.Value = sessionKey;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteMeeting()", true);
            }


        }
        catch (Exception ex)
        {

        }
    }
}