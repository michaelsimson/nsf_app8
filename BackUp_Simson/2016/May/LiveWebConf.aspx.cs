﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class LiveWebConf : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                findTrainingSessions();
                if (GrdMeeting.Rows.Count > 0)
                {
                    fillFollowupMeetingGrid();
                }


            }
        }
    }
    public void fillMeetingGrid(string SessionKey)
    {

        string cmdtext = "";
        DataSet ds = new DataSet();
        SessionKey = SessionKey.TrimEnd(',');
        int year = 0;
        year = DateTime.Now.Year;
        if (SessionKey != "")
        {
            cmdtext = "select CS.UserID,CS.PWD,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID) where VC.EventYear=" + year + " and SessionKey in (" + SessionKey + ") order by VC.ProductGroupCode";


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
    }
    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Terminate")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductCode = string.Empty;
                string LBeginDate = string.Empty;
                string Year = "";
                string LDay = string.Empty;

                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;

                sessionKey = GrdMeeting.Rows[selIndex].Cells[10].Text;
                hdnWebExID.Value = WebExID;
                hdnSessionKey.Value = sessionKey;
                hdnPwd.Value = WebExPwd;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[4].Text;
                LBeginDate = GrdMeeting.Rows[selIndex].Cells[13].Text;
                LDay = GrdMeeting.Rows[selIndex].Cells[12].Text;
                Year = GrdMeeting.Rows[selIndex].Cells[1].Text;

                string sEndTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlEndTime") as Label).Text;
                string sBeginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;
                DateTime dt = DateTime.Now;
                DateTime dtEnd = (DateTime.Now);
                DateTime.TryParse(sEndTime, out dtEnd);
                //dtEnd = dtEnd.AddMinutes(5);
                string Day = "";
                string status = "";
                string StartDate = "";
                string BeginTime = "";
                for (int i = 0; i < GrdFollowUpSessions.Rows.Count; i++)
                {
                    string UserID = GrdFollowUpSessions.Rows[i].Cells[16].Text;
                    string Pwd = GrdFollowUpSessions.Rows[i].Cells[17].Text;
                    if (UserID == WebExID && Pwd == WebExPwd)
                    {
                        status = "Y";
                        Day = GrdFollowUpSessions.Rows[i].Cells[11].Text;
                        StartDate = GrdFollowUpSessions.Rows[i].Cells[12].Text;
                        BeginTime = ((Label)GrdFollowUpSessions.Rows[selIndex].FindControl("hlTime1") as Label).Text;
                    }
                }

                if (status == "Y")
                {
                    DateTime dtBegin = (DateTime.Now);
                    DateTime.TryParse(BeginTime, out dtBegin);
                    TimeSpan ts = dtBegin - dt;
                    int min = ts.Minutes;
                    if ((StartDate == LBeginDate) && (Day == LDay) && (dtBegin > dtEnd) && (min < 10))
                    {
                        string terminatedTime = DateTime.Now.ToString("HH:mm:ss");
                        //sBeginTime = sBeginTime.Substring(0, 8);
                        sBeginTime = Convert.ToDateTime(sBeginTime).ToString("HH:mm:ss");
                        //sEndTime = sEndTime.Substring(0, 8);
                        sEndTime = Convert.ToDateTime(sEndTime).ToString("HH:mm:ss");
                        string cmdText = "insert into LiveWebConfTermLog (EventID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,SessionKey,MemberID,TerminatedDate,TerminationTime,TerminatedBy,ChapterID,EventYear,UserID,Password,Day,StartDate,BeginTime,EndTime,CreatedDate,CreatedBy) values(" + EventID + "," + ProductGroupID + ",'" + ProductGroupCode + "'," + ProductID + ",'" + ProductCode + "','" + sessionKey + "'," + MemberID + ",GetDate(),'" + terminatedTime + "'," + Session["LoginID"].ToString() + "," + ChapterID + "," + Year + ",'" + WebExID + "','" + WebExPwd + "','" + Day + "','" + StartDate + "','" + sBeginTime + "','" + sEndTime + "',GetDate()," + Session["LoginID"].ToString() + ")";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        //cmdText = "update WebConfLog set Status='Closed' where Sessionkey=" + sessionKey + "; update CalSignUp set Status='Closed' where MeetingKey=" + sessionKey + "";

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmKillMeeting();", true);

                    }

                    else
                    {
                        lblerr.Text = "You cannot terminate before...";
                    }
                }
                else
                {
                    lblerr.Text = "You cannot terminate before...";
                }
                //Response.Redirect(KillURL);
            }
        }
        catch (Exception ex)
        {
        }
    }
    private TimeSpan GetTimeFromString1(string timeString)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.TimeOfDay;
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid(hdnSessionKeys.Value);
    }


    public void GetTrainingSessions(string WebExID, string PWD, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingTestResponse(xmlReply);
        }

        if (result == "INPROGRESS")
        {
            //string cmdUpdateText = "update WebConfLog set Status='InProgress' where SessionKey='" + SessionKey + "'";
            //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);
            hdnSessionKeys.Value += SessionKey + ",";
        }

    }
    private string ProcessTrainingTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;

            }

        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }

    public void findTrainingSessions()
    {
        string cmdText = string.Empty;
        DataSet ds = new DataSet();
        string RoleID = Session["RoleID"].ToString();
        string MemberID = Session["LoginID"].ToString();
        string TeamLead = "";

        cmdText = "select TeamLead from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                TeamLead = ds.Tables[0].Rows[0]["TeamLead"].ToString();
            }
        }
        if (RoleID == "1" || RoleID == "2" || RoleID == "96")
        {
            cmdText = "select C.UserID,C.PWD,VC.SessionKey from WebConfLog VC inner join CalSignUp C on(VC.SessionKey=C.MeetingKey)";
        }

        else if (RoleID == "88" && TeamLead == "N")
        {
            cmdText = "select C.UserID,C.PWD,VC.SessionKey from WebConfLog VC inner join CalSignUp C on(VC.SessionKey=C.MeetingKey) where VC.MemberID=" + MemberID + "";
        }
        else if (RoleID == "88" && TeamLead == "Y")
        {
            string ProductGroupID = "";
            string ProductID = "";
            cmdText = "select ProductGroupID,ProductID from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProductGroupID = ds.Tables[0].Rows[0]["ProductGroupID"].ToString();
                    ProductID = ds.Tables[0].Rows[0]["ProductID"].ToString();
                }
            }
            cmdText = "select C.UserID,C.PWD,VC.SessionKey from WebConfLog VC inner join CalSignUp C on(VC.SessionKey=C.MeetingKey) where VC.ProductGroupID='" + ProductGroupID + "' and ProductID='" + ProductID + "'";

        }
        else if (RoleID == "88" && TeamLead == "")
        {
            cmdText = "select C.UserID,C.PWD,VC.SessionKey from WebConfLog VC inner join CalSignUp C on(VC.SessionKey=C.MeetingKey) where VC.MemberID=" + MemberID + "";
        }
        else if (RoleID == "89")
        {
            string ProductGroupID = "";
            string ProductID = "";
            cmdText = "select ProductGroupID,ProductID from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProductGroupID = ds.Tables[0].Rows[0]["ProductGroupID"].ToString();
                    ProductID = ds.Tables[0].Rows[0]["ProductID"].ToString();
                }
            }
            cmdText = "select C.UserID,C.PWD,VC.SessionKey from WebConfLog VC inner join CalSignUp C on(VC.SessionKey=C.MeetingKey) where VC.ProductGroupID='" + ProductGroupID + "' and ProductID='" + ProductID + "'";

        }
        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    GetTrainingSessions(dr["UserID"].ToString(), dr["Pwd"].ToString(), dr["SessionKey"].ToString());
                }
            }
        }
        fillMeetingGrid(hdnSessionKeys.Value);

    }

    public void fillFollowupMeetingGrid()
    {

        string cmdtext = "";
        DataSet ds = new DataSet();

        int Year = 0;
        Year = DateTime.Now.Year;

        string RoleID = Session["RoleID"].ToString();
        string MemberID = Session["LoginID"].ToString();
        string StartDate = "";
        string TeamLead = "";
        string LeadText = "";
        LeadText = "select TeamLead from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, LeadText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                TeamLead = ds.Tables[0].Rows[0]["TeamLead"].ToString();
            }
        }

        cmdtext = "select CS.UserID,CS.PWD,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID)";

        for (int i = 0; i < GrdMeeting.Rows.Count; i++)
        {
            string UserID = GrdMeeting.Rows[i].Cells[17].Text;
            string Pwd = GrdMeeting.Rows[i].Cells[18].Text;

            StartDate = GrdMeeting.Rows[i].Cells[13].Text;

        }

        if ((RoleID == "88" && TeamLead == "") || (RoleID == "88" && TeamLead == "N"))
        {
            cmdtext += " where VC.EventYear=" + Year + " and VC.StartDate='" + StartDate + "' and VC.MemberID=" + MemberID + " and VC.SessionKey not in(" + hdnSessionKeys.Value.TrimEnd(',') + ")  order by VC.ProductGroupCode";
        }

        else if (RoleID == "1" || RoleID == "2" || RoleID == "96")
        {
            cmdtext += " where VC.EventYear=" + Year + " and VC.StartDate='" + StartDate + "' and VC.SessionKey not in(" + hdnSessionKeys.Value.TrimEnd(',') + ")  order by VC.ProductGroupCode";
        }
        else if (RoleID == "89" || (RoleID == "88" && TeamLead == "Y"))
        {
            string ProductGroupID = "";
            string ProductID = "";
            string cmdText = "";
            cmdText = "select ProductGroupID,ProductID from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ProductGroupID = ds.Tables[0].Rows[0]["ProductGroupID"].ToString();
                    ProductID = ds.Tables[0].Rows[0]["ProductID"].ToString();
                }
            }

            cmdtext += " where VC.EventYear=" + Year + " and VC.StartDate='" + StartDate + "' and VC.ProductGroupID=" + ProductGroupID + " and ProductID=" + ProductID + " and VC.SessionKey not in(" + hdnSessionKeys.Value.TrimEnd(',') + ") order by VC.ProductGroupCode,VC.StartDate,VC.BeginTime,VC.EndTime";
        }




        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                GrdFollowUpSessions.DataSource = ds;
                GrdFollowUpSessions.DataBind();

            }
            else
            {
                GrdFollowUpSessions.DataSource = ds;
                GrdFollowUpSessions.DataBind();

            }
        }

    }
}