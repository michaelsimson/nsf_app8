<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" CodeFile="ScheduleNationalTechTeam.aspx.vb" Inherits="ScheduleNationalTechTeam" Title="NSF - Schedule National Technical Coordinator" %>
<asp:Content ID="cntScheduleTechCoordinators" ContentPlaceHolderID="Content_main" Runat="Server">
     <div align="center" >
         <h2>
                 Schedule National Tech Team Members 
         </h2>
</div>
<table border="0"  width="75%" align="center">
       <tr><td align="left">
            <asp:Label ID="Label2" runat="server" Text="Contest"></asp:Label>: 
            <asp:DropDownList ID="ddlProductGroup"  runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductGroupCode" Width="106px">
           </asp:DropDownList>
        </td>
        <td align="center">
            <asp:Label ID="lblNTTMember" runat="server" Text="NatTechTeamMember"></asp:Label>: 
            <asp:DropDownList ID="ddlNTTMember" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="MemberID"></asp:DropDownList>
        </td></tr>
        <tr align="center">
               <td colspan="3"><br /><asp:Button ID="BtnAddUpdate" runat="server" Text="Add" />&nbsp;
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel"   /></td>
        </tr>
        
        <tr><td align="center" colspan="3">
                            <asp:Label ID="lblError" runat="server" ForeColor="red" ></asp:Label>
       </td></tr>
       <tr><td align="center" colspan="3" visible="false">
                            <asp:Label ID="lblNTTID" runat="server" ForeColor="red"  Visible="false"></asp:Label>
       </td></tr>
    <tr><td colspan="3" align="center">
            <asp:DataGrid ID="DGNatTechTeam" runat="server" Width="99%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="NTTID">
			    <AlternatingItemStyle font-size="small"></AlternatingItemStyle>
			    <ItemStyle backcolor="white" Wrap="False" Font-Size="Small" ></ItemStyle>  
			    <HeaderStyle  BorderStyle="Solid" Font-Size="Small" Font-Bold="True" backcolor="Gainsboro"></HeaderStyle>
			    <FooterStyle BackColor="Gainsboro" ></FooterStyle>
			    <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                <Columns>
                        <asp:TemplateColumn>
                               <ItemTemplate><asp:LinkButton id="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
		                       </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:TemplateColumn>
                               <ItemTemplate><asp:LinkButton id="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
		                       </ItemTemplate>
		                </asp:TemplateColumn>
		                <asp:Boundcolumn DataField="NTTID"  HeaderText="NTTID" Visible="true" />
		                <asp:Boundcolumn DataField="ProductGroupID"  HeaderText="ProductGroupID" Visible="true" />
		                <asp:BoundColumn DataField="ProductGroupCode" headerText="ProductGroupCode"  ItemStyle-Width="20%"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ProductName" headerText="Product Name"  ItemStyle-Width="20%"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Name" headerText="NationalTechTeamMember"  ItemStyle-Width="45%"></asp:BoundColumn> 
                
                </Columns>
		    </asp:DataGrid>
		    </td></tr>
        </table>

</asp:Content>

 
 
 