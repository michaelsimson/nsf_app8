﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="MaxScoresByPhase.aspx.vb" Inherits="MaxScoresByPhase" Title="Maximum scores by phase" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script language="Javascript" type="text/javascript">

        function onlyNos(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    return false;
                }
                return true;

            }
            catch (err) {
                //  alert(err.Description);
            }
        }

        function GetMaxScores1() {

            var txt1 = document.getElementById("<% =txtPhase1.ClientID%>");
            var txt2 = document.getElementById("<% =txtPhase2.ClientID%>");
            var txt3 = document.getElementById("<% =txtMaxScores.ClientID%>");
            var pgCode = document.getElementById("<% =ddlProductGroupCode.ClientID%>");
            if (pgCode.value == "26" || pgCode.value == "14") {
                if ((txt1.value != "")) {
                    txt3.value = parseInt(txt1.value);
                }
            }
        }

        function GetMaxScores() {
            var txt1 = document.getElementById("<% =txtPhase1.ClientID%>");
            var txt2 = document.getElementById("<% =txtPhase2.ClientID%>");
            var txt3 = document.getElementById("<% =txtMaxScores.ClientID%>");

            if ((txt1.value != "") && (txt2.value != "")) {
                txt3.value = parseInt(txt1.value) + parseInt(txt2.value);
            }
        }
    </script>
    <asp:HyperLink ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink><br />

    <table cellpadding="0" cellspacing="0" border="0" align="left" width="1004px">
        <tr>
            <td align="center">
                <center>

                    <table cellpadding="3" cellspacing="0" border="0" width="400">
                        <tr>
                            <td colspan="2" align="center">
                                <h2>Maximum Scores By Phase</h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Event</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlEvent" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Finals</asp:ListItem>
                                    <asp:ListItem Value="2">Chapter Contests </asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Product Group Code</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlProductGroupCode" runat="server" Width="155px" Height="23px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Year </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlYear" runat="server" Width="155px" Height="23px">
                                </asp:DropDownList>


                            </td>
                        </tr>
                        <tr>
                            <td align="left">Phase1 </td>
                            <td align="left">
                                <asp:TextBox ID="txtPhase1" runat="server" Width="79px" onkeypress="return onlyNos(event,this);" onkeyup="GetMaxScores1()"></asp:TextBox>


                            </td>
                        </tr>
                        <tr>
                            <td align="left">Phase2 </td>
                            <td align="left">
                                <asp:TextBox ID="txtPhase2" runat="server" Width="79px" onkeypress="return onlyNos(event,this);" onkeyup="GetMaxScores()"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Max Score</td>
                            <td align="left">
                                <asp:TextBox ID="txtMaxScores" runat="server" Width="79px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblError" runat="server"></asp:Label>
                            </td>
                        </tr>

                    </table>

                    <asp:Panel runat="server" ID="pnlMaxScores" Visible="False">

                        <center>
                            <asp:Label ID="lblMsg" ForeColor="red" runat="server"></asp:Label>
                        </center>
                        <br />
                        <center>
                            <span style="font-weight: bold;">Table 1: Max Scores By Contest and Phase
                            </span>
                        </center>
                        <div style="clear: both; margin-top: 5px;"></div>
                        <center>
                            <asp:Label ID="lblStatus" ForeColor="red" runat="server" Visible="false"></asp:Label>
                        </center>
                        <asp:GridView ID="gvMaxScores" runat="server" DataKeyNames="MaxScoresId" AutoGenerateColumns="False" OnRowCommand="gvMaxScores_RowCommand" EnableModelValidation="True">
                            <Columns>
                                <asp:TemplateField Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                                <asp:BoundField DataField="MaxScoresId" HeaderText="MaxScoresId" />
                                <asp:BoundField DataField="EventID" HeaderText="EventID" />
                                <asp:BoundField DataField="Name" HeaderText="Event" />
                                <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" />
                                <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" ItemStyle-HorizontalAlign="Left">
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Year" HeaderText="Year" />
                                <asp:BoundField DataField="Phase1" HeaderText="Phase1" />
                                <asp:BoundField DataField="Phase2" HeaderText="Phase2" />
                                <asp:BoundField DataField="MaxScores" HeaderText="MaxScores" />
                            </Columns>
                        </asp:GridView>


                    </asp:Panel>
                </center>
            </td>
        </tr>
    </table>
    <input type="hidden" value="0" id="hdnEventID" runat="server" />
</asp:Content>

