﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;


public partial class MakeupSessions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblerr.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {

                LoadEvent(ddEvent);
                fillMeetingGrid();
                // TestCreateTrainingSession();
            }
        }
    }
    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y')";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y')";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + ")";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 1;
                ddlProduct.Enabled = false;
                loadLevel();
                LoadSessionNo();
                if (Session["RoleID"].ToString() == "88")
                {
                }

            }
            else
            {

                ddlProduct.Enabled = true;
            }

        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();
        //fillCoach();
    }

    public void fillCoach()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
            }

        }

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLevel();
    }

    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
            }
        }
        //if (ddlProductGroup.SelectedValue == "41")
        //{
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("Junior", "Junior"));
        //    ddlLevel.Items.Insert(2, new ListItem("Senior", "Senior"));
        //}
        //else if (ddlProductGroup.SelectedValue == "42")
        //{
        //    //ddlObject.Enabled = False
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("Junior", "Junior"));
        //    ddlLevel.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
        //    ddlLevel.Items.Insert(3, new ListItem("Senior", "Senior"));
        //}
        //else if (ddlProductGroup.SelectedValue == "33")
        //{
        //    //ddlObject.Enabled = False
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("One level only", "One level only"));

        //}

        //else
        //{
        //    ddlLevel.Items.Clear();
        //    ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlLevel.Items.Insert(1, new ListItem("Beginner", "Beginner"));
        //    ddlLevel.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
        //    ddlLevel.Items.Insert(3, new ListItem("Advanced", "Advanced"));
        //}
    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 1))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;

                ddchapter.SelectedValue = "112";
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    fillCoach();
                }
                else
                {
                    fillCoach();
                    fillProductGroup();
                }
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }

        }
        else
        {

            lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        if (validatemeeting() == "1")
        {
            ValidatMakeUpSession();
        }
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid();
        btnAddNewMeeting.Visible = false;
        tblAddNewMeeting.Visible = true;
    }
    protected void btnAddNewMeeting_Click(object sender, EventArgs e)
    {
        tblAddNewMeeting.Visible = true;
    }

    public void ValidatMakeUpSession()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;
            string SessionCreationDate = string.Empty;
            string CoachChange = string.Empty;
            //local
            //string CurrDate = DateTime.Today.ToString("dd/MM/yyyy");
            // Server
            String CurrDate = DateTime.Today.ToString("MM/dd/yyyy");
            DateTime dt = Convert.ToDateTime(CurrDate);
            CmdText = "select SessionCreationDate,CoachChange from WebConfControl where EventYear=" + ddYear.SelectedValue + "";
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "All")
            {
                CmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //local
                    //SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("dd/MM/yyyy");
                    //Server
                    SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("MM/dd/yyyy");
                    CoachChange = ds.Tables[0].Rows[0]["CoachChange"].ToString();
                }
            }
            if (SessionCreationDate != "")
            {
                DateTime dtSession = Convert.ToDateTime(SessionCreationDate);
                if (dt >= dtSession)
                {

                    CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null  and CS.Level='" + ddlLevel.SelectedValue + "' and CS.Phase='" + ddlPhase.SelectedValue + "'";

                    ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                count++;

                                int IsSameDay = 0;
                                int IsSameTime = 0;
                                string WebExID = dr["UserID"].ToString();
                                string Pwd = dr["PWD"].ToString();
                                int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                                string ScheduleType = dr["ScheduleType"].ToString();

                                string year = dr["EventYear"].ToString();
                                string eventID = dr["EventID"].ToString();
                                string chapterId = ddchapter.SelectedValue;
                                string ProductGroupID = dr["ProductGroupID"].ToString();
                                string ProductGroupCode = dr["ProductGroupCode"].ToString();
                                string ProductID = dr["ProductID"].ToString();
                                string ProductCode = dr["ProductCode"].ToString();
                                string Phase = dr["Phase"].ToString();
                                string Level = dr["Level"].ToString();
                                string Sessionno = dr["SessionNo"].ToString();
                                string CoachID = dr["MemberID"].ToString();
                                string CoachName = dr["CoachName"].ToString();

                                string MeetingPwd = "training";

                                string Date = txtDate.Text;
                                string Time = dr["Time"].ToString();
                                string Day = dr["Day"].ToString();
                                string BeginTime = dr["Begin"].ToString();
                                string EndTime = dr["End"].ToString();
                                string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                                string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                                if (BeginTime == "")
                                {
                                    BeginTime = "20:00:00";
                                }
                                if (EndTime == "")
                                {
                                    EndTime = "21:00:00";
                                }

                                if (txtDuration.Text != "")
                                {
                                    int Duration = Convert.ToInt32(txtDuration.Text);
                                }
                                string timeZoneID = string.Empty;

                                timeZoneID = ddlTimeZone.SelectedValue;

                                string TimeZone = ddlTimeZone.SelectedItem.Text;
                                string SignUpId = dr["SignupID"].ToString();
                                double Mins = 0.0;
                                DateTime dFrom;
                                DateTime dTo;


                                string durationHrs = Mins.ToString("0");
                                if (durationHrs.IndexOf("-") > -1)
                                {
                                    durationHrs = "188";
                                }

                                if (timeZoneID == "4")
                                {
                                    TimeZone = "EST/EDT – Eastern";
                                }
                                else if (timeZoneID == "7")
                                {
                                    TimeZone = "CST/CDT - Central";
                                }
                                else if (timeZoneID == "6")
                                {
                                    TimeZone = "MST/MDT - Mountain";
                                }
                                string userID = Session["LoginID"].ToString();


                                if (dr["MakeupMeetKey"].ToString() == "" && dr["MeetingKey"].ToString() != "")
                                {
                                    DateTime dStart;
                                    DateTime dEnd;

                                    string DateFrom = txtTime.Text;
                                    string DateTo = txtTime.Text;

                                    CmdText = "select * from CalSignup where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y'";
                                    ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                                    foreach (DataRow dr1 in ds.Tables[0].Rows)
                                    {
                                        DateTime dtFromS = new DateTime();
                                        DateTime dtEnds = new DateTime();
                                        string sDateFrom = dr1["Begin"].ToString();
                                        string sDateTo = dr1["End"].ToString();
                                        if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                                        {
                                            TimeSpan TS = dTo - dFrom;
                                            Mins = TS.TotalMinutes;
                                            dtFromS = dFrom;
                                            dtEnds = dTo;

                                        }

                                        if (DateTime.TryParse(DateFrom, out dStart) && DateTime.TryParse(DateTo, out dEnd))
                                        {
                                            DateTime dtStart = dStart;
                                            DateTime dtEnd = dEnd;

                                            if (dtStart > dtFromS && dtStart > dtEnds)
                                            {

                                            }
                                            else if (dtStart < dtFromS && dtStart < dtEnds && dtEnd < dtEnds && dtEnd < dtFromS)
                                            {

                                            }
                                            else
                                            {
                                                IsSameTime = 1;
                                            }
                                        }

                                        if (dr1["Day"].ToString() == Convert.ToDateTime(txtDate.Text).ToString("dddd"))
                                        {
                                            IsSameDay = 1;
                                        }

                                    }

                                    if (IsSameDay == 1 && IsSameTime == 1)
                                    {
                                        string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");

                                        lblerr.Text = "Time is not available...!";
                                    }
                                    else
                                    {
                                        IsSameTime = 0;
                                        IsSameDay = 0;
                                        string WebExUserID = string.Empty;
                                        string WebExUserPwd = string.Empty;
                                        string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                        CmdText = "select * from CalSignup where EventYear=" + ddYear.SelectedValue + " and Accepted='Y' and UserID='" + WebExID + "' and Pwd='" + Pwd + "'";
                                        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                                        foreach (DataRow dr1 in ds.Tables[0].Rows)
                                        {
                                            DateTime dtFromS = new DateTime();
                                            DateTime dtEnds = new DateTime();
                                            string sDateFrom = dr1["Begin"].ToString();
                                            string sDateTo = dr1["End"].ToString();
                                            if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                                            {
                                                TimeSpan TS = dTo - dFrom;
                                                Mins = TS.TotalMinutes;
                                                dtFromS = dFrom;
                                                dtEnds = dTo;

                                            }

                                            if (DateTime.TryParse(DateFrom, out dStart) && DateTime.TryParse(DateTo, out dEnd))
                                            {
                                                DateTime dtStart = dStart;
                                                DateTime dtEnd = dEnd;

                                                if (dtStart > dtFromS && dtStart > dtEnds)
                                                {
                                                    WebExUserID = dr1["UserID"].ToString();
                                                    WebExUserPwd = dr1["Pwd"].ToString();
                                                }
                                                else if (dtStart < dtFromS && dtStart < dtEnds && dtEnd < dtEnds && dtEnd < dtFromS)
                                                {
                                                    WebExUserID = dr1["UserID"].ToString();
                                                    WebExUserPwd = dr1["Pwd"].ToString();
                                                }
                                                else
                                                {
                                                    IsSameTime = 1;
                                                }
                                            }

                                            if (dr1["Day"].ToString() == Convert.ToDateTime(txtDate.Text).ToString("dddd"))
                                            {
                                                IsSameDay = 1;
                                            }

                                        }

                                        //if (IsSameTime != 1 && IsSameDay != 1)
                                        //{
                                        //TimeSpan EndSessTime = GetTimeFromString(txtTime.Text, Convert.ToInt32(txtDuration.Text));
                                        //string strEndTime = EndSessTime.ToString();
                                        //string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                        //CreatMakeUpSessions(WebExUserID, WebExUserPwd, "Term", 200, txtDate.Text, txtTime.Text, SignUpId, CoachID, strStartDay, strEndTime);
                                        // }
                                        //  else
                                        // {
                                        string strDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                        int sessDuration = Convert.ToInt32(txtDuration.Text) + 30;
                                        TimeSpan EndSessTime = GetTimeFromString(txtTime.Text, sessDuration);
                                        string strEndTime = EndSessTime.ToString();
                                        string strEndSessTime = EndSessTime.ToString();
                                        TimeSpan TsStartSessTime = GetTimeFromStringSubtract(txtTime.Text, -30);
                                        string strStartTime = TsStartSessTime.ToString();

                                        CmdText = "select distinct UserID,Pwd,VRoom from CalSignUp where EventYear=2015 and Accepted='Y' and [Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and [End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and UserID not in (select distinct UserID from CalSignUp where EventYear=2015 and Accepted='Y' and ([Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' or [End] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "') and UserID is not null and day='" + Day + "') order by Vroom ASC";

                                        // CmdText = "select * from CalSignup CS left join WebConfLog WC on (CS.MemberID=WC.MemberID)  where  CS.[Begin] not between '" + txtTime.Text + "' and '" + EndSessTime + "' and CS.day='" + day + "' and WC.BeginTime not between '" + txtTime.Text + "' and '" + EndSessTime + "' and CS.eventyear = '" + ddYear.SelectedValue + "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" + txtTime.Text + "' and '" + EndSessTime + "' and CS.day='" + strDay + "' and  eventyear = '" + ddYear.SelectedValue + "' and Accepted='Y') and CS.UserID<>'" + userID + "' and CS.PWD<>'" + Pwd + "'";

                                        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                                        DataSet ds1 = new DataSet();
                                        CmdText = "select WC.UserID,WC.PWD from WebConfLog WC where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' or wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and  WC.eventyear = '" + year + "' and (SessionType='Substitute' or SessionType='Makeup') and WC.MemberID in(select CMemberID from CoachReg where EventYear='" + year + "' and Approved='Y') order by PWD ASC";

                                        ds1 = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                                        IsSameTime = 0;
                                        IsSameDay = 0;
                                        string Uid = string.Empty;
                                        foreach (DataRow dr1 in ds.Tables[0].Rows)
                                        {
                                            if (ds1.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow dr2 in ds1.Tables[0].Rows)
                                                {
                                                    if (dr1["UserID"].ToString() != dr2["UserID"].ToString())
                                                    {
                                                        WebExUserID = dr1["UserID"].ToString();
                                                        WebExUserPwd = dr1["Pwd"].ToString();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                WebExUserID = dr1["UserID"].ToString();
                                                WebExUserPwd = dr1["Pwd"].ToString();
                                            }
                                        }
                                        if (WebExUserID != "" && WebExUserPwd != "")
                                        {
                                            string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
                                            CreatMakeUpSessions(WebExUserID, WebExUserPwd, "Term", 200, txtDate.Text, txtTime.Text, SignUpId, CoachID, strStartDay, strEndTime);
                                        }
                                        else
                                        {
                                            lblerr.Text = "Time is not available...!";
                                        }

                                        // }
                                    }

                                }
                                else
                                {
                                    if (dr["MeetingKey"].ToString() == "")
                                    {
                                        lblerr.Text = "Training sessions not created yet for the selected coach.!";
                                    }
                                    else if (dr["MakeupMeetKey"].ToString() != "")
                                    {
                                        lblerr.Text = "Please cancel the old makeup session before creating new makeup session.";
                                    }
                                }
                            }
                        }
                        else
                        {
                            lblerr.Text = "No child is assigned for the selected Coach..";
                        }
                    }
                }
                else
                {
                    if (dt < dtSession)
                    {
                        lblerr.Text = "This application can only be run on " + SessionCreationDate + "";
                    }
                    else if (CoachChange.Trim() == "Y")
                    {
                        lblerr.Text = "This application cannot be run since the coach change flag is not set to N";
                    }
                }
            }
            else
            {
                lblerr.Text = "This application can only be run on.....";
            }

        }
        catch (Exception ex)
        {
            //throw new Exception(ex.Message);
        }

    }



    public void fillMeetingGrid()
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;
        if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.MemberID=" + ddlCoach.SelectedValue + " ";
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdtext += " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdtext += " and VC.ProductID=" + ddlProduct.SelectedValue + "";
            }
            else if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
            {
                cmdtext += " and VC.MemberID=" + ddlCoach.SelectedValue + "";
            }


            cmdtext += "order by VC.ProductGroupCode";


        }
        else
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup cs on (cs.MakeupMeetkey=vc.Sessionkey) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + "";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdtext += " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdtext += " and VC.ProductID=" + ddlProduct.SelectedValue + "";
            }



            cmdtext += "order by VC.ProductGroupCode";
        }

        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }


    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }


        else if (ddlPhase.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Phase";
            retVal = "-1";
        }
        else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "")
        {
            lblerr.Text = "Please select Level";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (txtDate.Text == "")
        {
            lblerr.Text = "Please fill Date";
            retVal = "-1";
        }
        else if (txtTime.Text == "")
        {
            lblerr.Text = "Please fill Time";
            retVal = "-1";
        }
        else if (txtDuration.Text == "")
        {
            lblerr.Text = "Please fill Duration";
            retVal = "-1";
        }
        return retVal;
    }


    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                registerMeetingsAttendees(ChidName, Email.Trim(), City, Country);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {

                    GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

                    string CmdText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "', MakeUpRegID=" + hdsnRegistrationKey.Value + ", makeUpAttendeeID=" + hdnMeetingAttendeeID.Value + " where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y' and Level='" + Level + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblSuccess.Text = "Selected child registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                    fillStudentList(MemberID, ddlProductGroup.SelectedItem.Text, ddlProduct.SelectedItem.Text);
                }

            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string PMemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblPMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;

                string LoginSessionID = string.Empty;
                string LoginSessionChildID = string.Empty;
                hdnChildMeetingURL.Value = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblChildURL") as Label).Text;
                DataSet ds = new DataSet();
                string email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                int countEmail = 0;
                string ProductCode = string.Empty;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + email.Trim() + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
                //if (Session["LoginID"] != null)
                //{
                //    LoginSessionID = Session["LoginID"].ToString();
                //}
                //if (Session["StudentID"] != null)
                //{
                //    LoginSessionChildID = Session["StudentID"].ToString();
                //}
                //if (LoginSessionID == PMemberID)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                //else if (LoginSessionChildID == ChildNumber)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                if (countEmail > 0)
                {
                    string status = GetTrainingSessions(hdnWebExID.Value, hdnWebExPwd.Value, hdnSessionKey.Value);
                    if (status == "INPROGRESS")
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" + ProductCode + "','" + hdnCoachname.Value + "');", true);
                        //lblerr.Text = "Meeting Not In-Progress";

                        // System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                }
                else
                {
                    lblerr.Text = "Only authorized child can join ito the training session.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;

                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;
                txtMeetingPwd.Text = GrdMeeting.Rows[selIndex].Cells[13].Text;
                txtDate.Text = GrdMeeting.Rows[selIndex].Cells[15].Text;
                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;
                txtTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;
                txtEndTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlEndTime") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[11].Text;
                tblAddNewMeeting.Visible = true;

                btnCreateMeeting.Text = "Update Meeting";
                hdnSessionKey.Value = sessionKey;
                txtEndDate.Text = GrdMeeting.Rows[selIndex].Cells[16].Text;

                ddlDay.SelectedValue = (GrdMeeting.Rows[selIndex].Cells[14].Text).ToUpper();
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                fillStudentList(MemberID, ProductGroupCode, ProductCode);
            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                //hdnWebExMeetURL.Value = MeetingURl;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                TestGetHostUrlMeeting(WebExID, WebExPwd, hdnSessionKey.Value);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {
                    string MeetingURl = hdnHostURL.Value;
                    string URL = MeetingURl.Replace("&amp;", "&");
                    hdnWebExMeetURL.Value = URL;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
                }
                else
                {
                    lblerr.Text = hdnMeetingStatus.Value;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid();
    }

    protected void GrdMeeting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdMeeting.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        GrdMeeting.EditIndex = -1;
        string sessionKey = string.Empty;
        sessionKey = GrdMeeting.Rows[rowIndex].Cells[12].Text;
        string WebExID = string.Empty;
        string WebExPwd = string.Empty;
        WebExID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExID") as Label).Text;
        WebExPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExPwd") as Label).Text;
        hdnSessionKey.Value = sessionKey;
        hdnWebExID.Value = WebExID;
        hdnWebExPwd.Value = WebExPwd;
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);


    }


    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode)
    {

        trChildList.Visible = true;
        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + MemberID + " and Approved='Y') and CR.Level='" + HdnLevel.Value + "' order by C1.Last_Name, C1.First_Name Asc";




        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdStudentsList.DataSource = ds;
                GrdStudentsList.DataBind();
                lblChildMsg.Text = "";
                btnClosetable2.Visible = true;

                for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                {
                    if (((LinkButton)GrdStudentsList.Rows[i].FindControl("HlAttendeeMeetURL") as LinkButton).Text != "")
                    {
                        ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                    }
                    else
                    {
                        ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                    }
                }
            }
            else
            {
                GrdStudentsList.DataSource = ds;
                GrdStudentsList.DataBind();
                lblChildMsg.Text = "No record found";
                btnClosetable2.Visible = false;

            }
        }
    }

    public void CreatMakeUpSessions(string WebEXID, string PWD, string MeetingTitle, int Capacity, string StartDate, string Time, string SignupID, string CoachID, string Day, string EndTime)
    {
        MeetingTitle = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text;
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";

        strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<accessControl>\r\n";
        //strXML += "  <listing>PUBLIC</listing>\r\n";
        strXML += "  <sessionPassword>training</sessionPassword>\r\n";
        strXML += "</accessControl>\r\n";

        strXML += "<schedule>\r\n";
        strXML += "     <startDate>" + StartDate + " " + Time + "</startDate>\r\n";

        //strXML += "     <timeZone>GMT-12:00, Dateline (Eniwetok)</timeZone>\r\n";
        strXML += "     <duration>" + txtDuration.Text + "</duration>\r\n";
        //strXML += "     <duration>240</duration>\r\n";
        strXML += "     <timeZoneID>11</timeZoneID>\r\n";
        strXML += "     <openTime>20</openTime>\r\n";
        strXML += "</schedule>\r\n";

        strXML += "<metaData>\r\n";
        strXML += "     <confName>" + MeetingTitle + "</confName>\r\n";
        strXML += "     <agenda>agenda 1</agenda>\r\n";
        strXML += "     <description>Training</description>\r\n";
        strXML += "     <greeting>greeting</greeting>\r\n";
        strXML += "     <location>location</location>\r\n";
        strXML += "     <invitation>invitation</invitation>\r\n";
        strXML += "</metaData>\r\n";




        strXML += "<attendeeOptions>\r\n";
        strXML += "      <request>true</request>\r\n";
        strXML += "      <registration>true</registration>\r\n";
        strXML += "      <auto>true</auto>\r\n";
        strXML += "      <registrationPWD>training</registrationPWD>\r\n";
        strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        strXML += "      <registrationCloseDate>03/30/2016 12:00:00";
        strXML += "      </registrationCloseDate>\r\n";
        strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        strXML += "</attendeeOptions>";

        strXML += " <enableOptions>";
        strXML += "     <autoDeleteAfterMeetingEnd>true</autoDeleteAfterMeetingEnd>";
        strXML += "     <supportBreakoutSessions>true</supportBreakoutSessions>";
        strXML += "     <presenterBreakoutSession>true</presenterBreakoutSession>";
        // strXML += "     <attendeeBreakoutSession>true</attendeeBreakoutSession>";
        strXML += "  <trainingSessionRecord>false</trainingSessionRecord>";
        strXML += " </enableOptions>";

        strXML += " <telephony>";
        strXML += "<telephonySupport>CALLIN</telephonySupport>";
        strXML += " <numPhoneLines>1</numPhoneLines>";
        // strXML += "<extTelephonyURL>String</extTelephonyURL>";
        //<extTelephonyDescription>String</extTelephonyDescription>
        //<enableTSP>false</enableTSP>
        //<tspAccountIndex>1</tspAccountIndex>
        strXML += " </telephony>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingResponse(xmlReply);

        if (!string.IsNullOrEmpty(result))
        {
            if (hdnMeetingStatus.Value == "SUCCESS")
            {
                TestGetHostUrlMeeting(WebEXID, PWD, hdnSessionKey.Value);
                string cmdText = "";
                string meetingURL = hdnHostURL.Value;
                cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + CoachID + ",'" + hdnSessionKey.Value + "','" + txtDate.Text + "','" + txtDate.Text + "','" + txtTime.Text + "','" + EndTime + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "',1,'training'," + txtDuration.Text + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + WebEXID + "','" + PWD + "')";
                try
                {
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    cmdText = "Update CalSignup set MakeUpURL='" + meetingURL + "', MakeupMeetKey='" + hdnSessionKey.Value + "', MakeUpPwd='training',MakeUpDate='" + txtDate.Text + "',MakeUpTime='" + txtTime.Text + "',MakeUpDuration='" + txtDuration.Text + "' where SignupID=" + SignupID + "";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    string ChidName = string.Empty;
                    string Email = string.Empty;
                    string City = string.Empty;
                    string Country = string.Empty;
                    string ChildNumber = string.Empty;
                    string CoachRegID = string.Empty;
                    hdnWebExID.Value = WebEXID;
                    hdnWebExPwd.Value = PWD;

                    DataSet dsChild = new DataSet();

                    string ChildText = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and CR.ProductCode='" + ddlProduct.SelectedItem.Text + "' and CR.CMemberID=" + ddlCoach.SelectedValue + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and ProductCode='" + ddlProduct.SelectedItem.Text + "' and CMemberID=" + ddlCoach.SelectedValue + " and Approved='Y' and Level='" + ddlLevel.SelectedValue + "') and CR.Level='" + ddlLevel.SelectedValue + "'";



                    dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                    if (null != dsChild && dsChild.Tables.Count > 0)
                    {
                        if (dsChild.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow drChild in dsChild.Tables[0].Rows)
                            {
                                ChidName = drChild["Name"].ToString();
                                Email = drChild["WebExEmail"].ToString();
                                if (Email.IndexOf(';') > 0)
                                {
                                    Email = Email.Substring(0, Email.IndexOf(';'));
                                }
                                City = drChild["City"].ToString();
                                Country = drChild["Country"].ToString();
                                ChildNumber = drChild["ChildNumber"].ToString();
                                CoachRegID = drChild["CoachRegID"].ToString();
                                registerMeetingsAttendees(ChidName, Email, City, Country);
                                if (hdnMeetingStatus.Value == "SUCCESS")
                                {
                                    GetJoinUrlMeeting(ChidName, Email.Trim(), hdsnRegistrationKey.Value);

                                    string CmdChildUpdateText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "', MakeUpRegID=" + hdsnRegistrationKey.Value + ", MakeUpAttendeeID=" + hdnMeetingAttendeeID.Value + ",ModifyDate=GetDate(), ModifiedBy='" + Session["LoginID"].ToString() + "',MakeUpDate='" + txtDate.Text + "',MakeUpTime='" + txtTime.Text + "',MakeUpDuration='" + txtDuration.Text + "' where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + ddlCoach.SelectedValue + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Approved='Y' and CoachRegID='" + CoachRegID + "' and Level='" + ddlLevel.SelectedValue + "'";

                                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);
                                }
                            }
                        }
                    }

                    fillMeetingGrid();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                lblSuccess.Text = "Training session Created successfully";
            }
            else
            {
                lblerr.Text = hdnException.Value;
            }
        }
        else
        {
            lblerr.Text = hdnException.Value;
        }
    }

    public void registerMeetingsAttendees(string Name, string Email, string City, string Country)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.RegisterMeetingAttendee\">\r\n";//

        strXML += "<attendees>";
        strXML += "<person>";
        strXML += "<name>" + Name + "</name>";
        strXML += "<title>title</title>";
        strXML += "<company>microsoft</company>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += "<city>" + City + "</city>";
        strXML += "<country>" + Country + "</country>";
        strXML += "</address>";
        //strXML += "<phones>0</phones>";
        strXML += "<email>" + Email + "</email>";
        strXML += "<notes>notes</notes>";
        strXML += "<url>https://</url>";
        strXML += "<type>VISITOR</type>";
        strXML += "</person>";
        strXML += "<joinStatus>ACCEPT</joinStatus>";
        strXML += "<role>ATTENDEE</role>";
        // strXML += "<sessionNum>1</sessionNum>";
        // strXML += "<emailInvitations>true</emailInvitations>";
        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "</attendees>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingAttendeeResponse(xmlReply);
        ///lblMsg1.Text = result;
    }

    private string ProcessMeetingAttendeeResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Attendee Information</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText;

                string attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText;
                //xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).RemoveAll();

                //string meetingKey1 = (xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent", manager).LastChild).InnerText;
                //hdnSessionKey.Value = meetingKey;
                sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey + "<br/>");
                hdsnRegistrationKey.Value = meetingKey;
                hdnMeetingAttendeeID.Value = attendeeID;
                //hdsnRegistrationKey1.Value = meetingKey1;
                //sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey1 + "<br/>");

                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                //hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                lblerr.Text = error;
            }
            else
            {
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    public void GetJoinUrlMeeting(string Name, string email, string RegsiterID)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "<attendeeName>" + Name + "</attendeeName>";
        strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
        strXML += "<meetingPW>" + hdnMeetingPwd.Value + "</meetingPW>";
        strXML += "<RegID>" + RegsiterID + "</RegID>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg2.Text += result;
        //}
    }

    private string ProcessMeetingAttendeeJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {

                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");

                string meetingKey2 = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml;
                string URL = meetingKey2.Replace("&amp;", "&");



                hdnMeetingUrl.Value = URL;

                sb.Append("<a href=" + URL + " target='blank'>" + URL + "</a></br>");


            }
            else if (status == "FAILURE")
            {

                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    public void TestGetHostUrlMeeting(string WebExID, string Pwd, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "<email>webex.nsf.adm@gmail.com</email>";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GethosturlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        //strXML += "<meetingPW>meetme</meetingPW>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string Result = ProcessMeetingHostJoinURLResponse(xmlReply);
    }

    private string ProcessMeetingHostJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = status;

                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml;

                string URL = meetingKey.Replace("&amp;", "&");
                hdnHostURL.Value = URL;



            }
            else if (status == "FAILURE")
            {

                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {

                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    private string ProcessMeetingResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                string sessionKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:sessionkey", manager).InnerText;
                hdnSessionKey.Value = sessionKey;

            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";

                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                hdnException.Value = error;
            }
            else
            {

                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    protected void btnClosetable2_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
    }

    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        CancelMeeting(hdnSessionKey.Value, hdnWebExID.Value, hdnWebExPwd.Value);
    }
    public void CancelMeeting(string sessionKey, string WebExID, string Pwd)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.DelTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + sessionKey + "</sessionKey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        //string result = ProcessMeetingHostJoinURLResponse(xmlReply);
        //lblMsg3.Text = result;
        lblSuccess.Text = "Meeting Cancelled Successfully";
        try
        {
            string cmdText = "delete from WebConfLog where SessionKey='" + sessionKey + "'";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            cmdText = "update CalSignUp set status=null, MakeUpURL=null,MakeupMeetKey=null,MakeUpPwd=null,makeUpDate=null,MakeUpTime=null, MakeUpDuration=null where MakeupMeetKey='" + sessionKey + "'";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            cmdText = "update CoachReg set status=null, MakeUpURL=null,MakeUpAttendeeID=null,MakeUpRegID=null,MakeUpDate=null,MakeUpTime=null,MakeUpDuration=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y'";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        }
        catch (Exception ex)
        {

        }

        fillMeetingGrid();
    }
    public string GetTrainingSessions(string WebExID, string PWD, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;

        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingStatusTestResponse(xmlReply);
        }

        if (result == "INPROGRESS")
        {
            //string cmdUpdateText = "update WebConfLog set Status='InProgress' where SessionKey='" + SessionKey + "'";
            //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

        }
        else
        {
            // lblerr.Text = "Meeting Not In-Progress";

        }
        return result;

    }
    private string ProcessTrainingStatusTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            string ConfName = string.Empty;
            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;

                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText;
                day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText;
                startTime = startTime.Substring(10, 9);
                double Mins = 0.0;
                DateTime dFrom = DateTime.Now;
                DateTime dTo = DateTime.Now;
                string sDateFrom = startTime;

                string today = DateTime.Today.DayOfWeek.ToString();
                if (today.ToLower() == day.ToLower())
                {
                    string sDateTo = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);

                    //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    DateTime easternTime = DateTime.Now;
                    string strEasternTime = easternTime.ToShortDateString();
                    strEasternTime = strEasternTime + " " + sDateFrom;
                    DateTime dtEasternTime = Convert.ToDateTime(strEasternTime);
                    DateTime easternTimeNow = DateTime.Now.AddHours(1);

                    TimeSpan TS = dtEasternTime - easternTimeNow;
                    Mins = TS.TotalMinutes;


                }
                else
                {
                    for (int i = 1; i <= 7; i++)
                    {
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString();
                        if (today.ToLower() == day.ToLower())
                        {
                            dTo = DateTime.UtcNow.AddDays(i);
                            //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            DateTime easternTime = DateTime.Now.AddDays(i);
                            string sDateTo = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                            string targetDateTime = easternTime.ToShortDateString() + " " + sDateFrom;
                            //DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            DateTime easternTimeNow = DateTime.Now.AddHours(1);

                            DateTime dtTargetTime = Convert.ToDateTime(targetDateTime);
                            TimeSpan TS = dtTargetTime - easternTimeNow;
                            Mins = TS.TotalMinutes;


                        }
                    }
                }
                string DueMins = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."));
                hdnSessionStartTime.Value = startTime;
                hdnStartMins.Value = Convert.ToString(DueMins);
            }

        }
        catch (Exception e)
        {
            // sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }
    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }
}