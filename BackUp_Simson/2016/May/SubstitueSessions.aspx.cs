﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;

public partial class SubstitueSessions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblerr.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {

                LoadEvent(ddEvent);

                // TestCreateTrainingSession();
            }
        }
    }
    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ")";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + ")";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductGroup.DataSource = ds;
                ddlProductGroup.DataTextField = "ProductGroupCode";
                ddlProductGroup.DataValueField = "ProductGroupId";
                ddlProductGroup.DataBind();
                ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProductGroup.SelectedIndex = 1;
                    ddlProductGroup.Enabled = false;
                    fillProduct();
                    loadLevel();
                }
                else
                {
                    ddlProductGroup.Enabled = true;
                }

            }
        }
        catch (Exception ex)
        {
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ")";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + ")";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                ddlProduct.Enabled = true;
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "ProductCode";
                ddlProduct.DataValueField = "ProductID";
                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProduct.SelectedIndex = 1;
                    ddlProduct.Enabled = false;
                    fillSubstituteCoach();
                    loadLevel();
                    //populateRecClassWeekDate();

                    populateRecClassDate();
                    fillCoach();

                }
                else
                {
                    ddlProduct.Enabled = true;
                }

            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();

        //fillCoach();
    }

    public void fillCoach()
    {
        string cmdText = "";


        cmdText = "select (ID.FirstName+' '+ ID.LastName) as Name,ID.AutoMemberID from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "  and V.Accepted='Y'  order by ID.FirstName ASC";
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlCoach.DataSource = ds;
                ddlCoach.DataTextField = "Name";
                ddlCoach.DataValueField = "AutoMemberID";
                ddlCoach.DataBind();
                ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    ddlCoach.Enabled = true;
                }
                else
                {

                }

            }
            ddlCoach.Enabled = true;
        }
        catch (Exception ex)
        {
        }
    }

    public void fillSubstituteCoach()
    {
        string cmdText = "";


        cmdText = "select (ID.FirstName+' '+ ID.LastName) as Name,ID.AutoMemberID from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + " and V.ProductID=" + ddlProduct.SelectedValue + "  and V.Accepted='Y' order by ID.FirstName ASC";
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                DDLSubstituteCoach.DataSource = ds;
                DDLSubstituteCoach.DataTextField = "Name";
                DDLSubstituteCoach.DataValueField = "AutoMemberID";
                DDLSubstituteCoach.DataBind();
                DDLSubstituteCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    DDLSubstituteCoach.Enabled = true;
                }
                else
                {
                    DDLSubstituteCoach.SelectedValue = Session["LoginID"].ToString();
                    DDLSubstituteCoach.Enabled = false;
                    // populateRecClassDate();
                    //populateRecClassWeekDate();
                    //DDLSubstituteCoach.SelectedValue = Session["LoginID"].ToString();
                    //DDLSubstituteCoach.Enabled = true;
                }

            }

        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

        //fillCoach();
        loadLevel();
        fillSubstituteCoach();
        LoadSessionNo();
        //populateRecClassWeekDate();
        populateRecClassDate();
        fillCoach();

    }

    public void loadLevel()
    {


        if (ddlProductGroup.SelectedValue == "41")
        {
            ddlLevel.Items.Clear();
            // ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(0, new ListItem("Junior", "Junior"));
            ddlLevel.Items.Insert(1, new ListItem("Senior", "Senior"));
        }
        else if (ddlProductGroup.SelectedValue == "42")
        {
            //ddlObject.Enabled = False
            ddlLevel.Items.Clear();
            // ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(0, new ListItem("Junior", "Junior"));
            ddlLevel.Items.Insert(1, new ListItem("Intermediate", "Intermediate"));
            ddlLevel.Items.Insert(2, new ListItem("Senior", "Senior"));
        }
        else if (ddlProductGroup.SelectedValue == "33")
        {
            //ddlObject.Enabled = False
            ddlLevel.Items.Clear();
            // ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(0, new ListItem("One level only", "One level only"));

        }

        else
        {
            ddlLevel.Items.Clear();
            //ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(0, new ListItem("Beginner", "Beginner"));
            ddlLevel.Items.Insert(1, new ListItem("Intermediate", "Intermediate"));
            ddlLevel.Items.Insert(2, new ListItem("Advanced", "Advanced"));
        }
    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        try
        {

            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.DataSource = ds;
                ddlObject.DataTextField = "EventCode";
                ddlObject.DataValueField = "EventId";
                ddlObject.DataBind();
                if ((ds.Tables[0].Rows.Count > 1))
                {
                    ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                    ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                    ddlObject.Enabled = false;
                    fillProductGroup();
                    ddchapter.SelectedValue = "112";

                }
                else
                {
                    ddlObject.Enabled = false;
                    fillProductGroup();
                }

            }
            else
            {

                lblerr.Text = "No Events present for the selected year";
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        if (validatemeeting() == "1")
        {
            if (ddlCoach.SelectedValue != DDLSubstituteCoach.SelectedValue)
            {
                ValidatMakeUpSession();
            }
            else
            {
                lblerr.Text = "Coach name and Substitute Coach name should not be same.";
            }
        }
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {
        tblAddNewMeeting.Visible = false;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid();
        btnAddNewMeeting.Visible = false;
        tblAddNewMeeting.Visible = false;
    }
    protected void btnAddNewMeeting_Click(object sender, EventArgs e)
    {
        if (ValidateCreateSession() == "1")
        {
            //tblAddNewMeeting.Visible = true;
            string CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin], VRoom, [End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
            try
            {

                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                if (null != ds && ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {

                            string Phase = dr["Phase"].ToString();
                            string Level = dr["Level"].ToString();
                            string Sessionno = dr["SessionNo"].ToString();
                            string CoachID = dr["MemberID"].ToString();

                            string CoachName = dr["CoachName"].ToString();
                            string UserID = dr["UserID"].ToString();
                            string Pwd = dr["PWD"].ToString();
                            string VRoom = dr["VRoom"].ToString();

                            string Time = dr["Time"].ToString();

                            string BeginTime = dr["Begin"].ToString();
                            string EndTime = dr["End"].ToString();
                            string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                            string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                            if (BeginTime == "")
                            {
                                BeginTime = "20:00:00";
                            }
                            if (EndTime == "")
                            {
                                EndTime = "21:00:00";
                            }

                            if (txtDuration.Text != "")
                            {
                                int Duration = Convert.ToInt32(txtDuration.Text);
                            }
                            string timeZoneID = string.Empty;

                            timeZoneID = ddlTimeZone.SelectedValue;

                            string TimeZone = ddlTimeZone.SelectedItem.Text;
                            string SignUpId = dr["SignupID"].ToString();
                            double Mins = 0.0;
                            DateTime dFrom;
                            DateTime dTo;
                            string sDateFrom = BeginTime;
                            string sDateTo = EndTime;
                            if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                            {
                                TimeSpan TS = dTo - dFrom;
                                Mins = TS.TotalMinutes;

                            }
                            string durationHrs = Mins.ToString("0");
                            if (durationHrs.IndexOf("-") > -1)
                            {
                                durationHrs = "188";
                            }


                            txtDuration.Text = durationHrs;
                            ddlPhase.SelectedValue = Phase;
                            ddlLevel.SelectedValue = Level;
                            txtDate.Text = startDate;
                            txtEndDate.Text = EndDate;
                            txtTime.Text = BeginTime;
                            string userID = Session["LoginID"].ToString();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

    }

    public void ValidatMakeUpSession()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;
            string SessionCreationDate = string.Empty;
            string CoachChange = string.Empty;
            //local
            //string CurrDate = DateTime.Today.ToString("dd/MM/yyyy");
            // Server
            String CurrDate = DateTime.Today.ToString("MM/dd/yyyy");
            DateTime dt = Convert.ToDateTime(CurrDate);
            CmdText = "select SessionCreationDate,CoachChange from WebConfControl where EventYear=" + ddYear.SelectedValue + "";
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "All")
            {
                CmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            string Meetinkey = string.Empty;
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //local
                    //SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("dd/MM/yyyy");
                    //Server
                    SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("MM/dd/yyyy");
                    CoachChange = ds.Tables[0].Rows[0]["CoachChange"].ToString();
                }
            }
            if (SessionCreationDate != "")
            {
                DateTime dtSession = Convert.ToDateTime(SessionCreationDate);
                if (dt >= dtSession)
                {

                    CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.SignupID,CS.MeetingKey,CS.HostJoinURL,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.SubstituteMeetKey,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + DDLSubstituteCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null  and CS.Level='" + ddlLevel.SelectedValue + "' and CS.SessionNo='" + ddlSession.SelectedValue + "' and CS.Phase='" + ddlPhase.SelectedValue + "'";

                    ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                    if (ds.Tables[0] != null)
                    {




                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                count++;

                                int IsSameDay = 0;
                                int IsSameTime = 0;
                                string WebExID = dr["UserID"].ToString();
                                string Pwd = dr["PWD"].ToString();
                                int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                                string ScheduleType = dr["ScheduleType"].ToString();
                                Meetinkey = dr["meetingKey"].ToString();
                                string year = dr["EventYear"].ToString();
                                string eventID = dr["EventID"].ToString();
                                string chapterId = ddchapter.SelectedValue;
                                string ProductGroupID = dr["ProductGroupID"].ToString();
                                string ProductGroupCode = dr["ProductGroupCode"].ToString();
                                string ProductID = dr["ProductID"].ToString();
                                string ProductCode = dr["ProductCode"].ToString();
                                string Phase = dr["Phase"].ToString();
                                string Level = dr["Level"].ToString();
                                string Sessionno = dr["SessionNo"].ToString();
                                string CoachID = dr["MemberID"].ToString();
                                string CoachName = dr["CoachName"].ToString();

                                string MeetingPwd = "training";

                                string Date = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                                string Time = dr["Time"].ToString();
                                string Day = dr["Day"].ToString();
                                string BeginTime = dr["Begin"].ToString();
                                string EndTime = dr["End"].ToString();
                                string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                                string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                                string MeetingURL = dr["HostJoinURL"].ToString();
                                string signupID = dr["SignupID"].ToString();
                                if (BeginTime == "")
                                {
                                    BeginTime = "20:00:00";
                                }
                                if (EndTime == "")
                                {
                                    EndTime = "21:00:00";
                                }

                                if (txtDuration.Text != "")
                                {
                                    int Duration = Convert.ToInt32(txtDuration.Text);
                                }
                                string timeZoneID = string.Empty;

                                timeZoneID = ddlTimeZone.SelectedValue;

                                string TimeZone = ddlTimeZone.SelectedItem.Text;
                                string SignUpId = dr["SignupID"].ToString();
                                double Mins = 0.0;
                                DateTime dFrom;
                                DateTime dTo;
                                string sDateFrom = BeginTime;
                                string sDateTo = EndTime;
                                if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                                {
                                    TimeSpan TS = dTo - dFrom;
                                    Mins = TS.TotalMinutes;

                                }
                                string durationHrs = Mins.ToString("0");
                                if (durationHrs.IndexOf("-") > -1)
                                {
                                    durationHrs = "188";
                                }



                                if (durationHrs.IndexOf("-") > -1)
                                {
                                    durationHrs = "188";
                                }

                                if (timeZoneID == "4")
                                {
                                    TimeZone = "EST/EDT – Eastern";
                                }
                                else if (timeZoneID == "7")
                                {
                                    TimeZone = "CST/CDT - Central";
                                }
                                else if (timeZoneID == "6")
                                {
                                    TimeZone = "MST/MDT - Mountain";
                                }
                                string userID = Session["LoginID"].ToString();


                                if (DDlSubDate.SelectedValue == "0")
                                {
                                    CmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SubstituteCoachID, SessionType)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + DDLSubstituteCoach.SelectedValue + ",'" + Meetinkey + "','" + DDlSubDate.SelectedValue + "','" + DDlSubDate.SelectedValue + "','" + BeginTime + "','" + EndTime + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "',1,'training'," + durationHrs + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + MeetingURL + "','" + Day + "','" + WebExID + "','" + Pwd + "'," + ddlCoach.SelectedValue + ",'Substitute')";
                                }
                                else
                                {
                                    CmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SubstituteCoachID, SessionType)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + DDLSubstituteCoach.SelectedValue + ",'" + Meetinkey + "','" + DDlSubDate.SelectedValue + "','" + DDlSubDate.SelectedValue + "','" + BeginTime + "','" + EndTime + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "',1,'training'," + durationHrs + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + MeetingURL + "','" + Day + "','" + WebExID + "','" + Pwd + "'," + ddlCoach.SelectedValue + ",'Substitute')";
                                }


                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                                //cmdText = "Update CalSignup set SubstituteURL='" + meetingURL + "', SubstituteMeetKey='" + hdnSessionKey.Value + "', SubstitutePwd='training',SubstituteDate='" + txtSubstituteDate.Text + "',SubstituteTime='" + txtSubstituteTime.Text + "',SubstituteDuration='" + txtDuration.Text + "', SubstituteCoachID=" + ddlCoach.SelectedValue + " where SignupID=" + SignupID + "";

                                CmdText = "Update CalSignup set SubstituteURL='" + MeetingURL + "', SubstituteMeetKey='" + hdnSessionKey.Value + "', SubstitutePwd='training', SubstituteCoachID=" + ddlCoach.SelectedValue + ",SubstituteDate='" + DDlSubDate.SelectedValue + "',SubstituteTime='" + BeginTime + "',SubstituteDuration='" + durationHrs + "' where SignupID=" + signupID + "";

                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);


                                fillMeetingGrid();
                                lblSuccess.Text = "Substitute coach assigned for the session successfully";
                            }
                        }

                        //        if (dr["SubstituteMeetKey"].ToString() == "" && dr["MeetingKey"].ToString() != "")
                        //        {
                        //            string WebExUserID = string.Empty;
                        //            string WebExUserPwd = string.Empty;
                        //            string Vroom = "";

                        //            int sessDuration = Convert.ToInt32(durationHrs) + 30;
                        //            TimeSpan EndSessTime = GetTimeFromString(BeginTime, sessDuration);
                        //            string strEndTime = EndSessTime.ToString();
                        //            TimeSpan TsStartSessTime = GetTimeFromStringSubtract(EndTime, -30);
                        //            string strStartTime = TsStartSessTime.ToString();

                        //            CmdText = "select distinct UserID,Pwd,VRoom from CalSignUp where EventYear=2015 and Accepted='Y' and [Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and [End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and UserID not in (select distinct UserID from CalSignUp where EventYear=2015 and Accepted='Y' and ([Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' or [End] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "') and UserID is not null and day='" + Day + "') order by Vroom ASC";

                        //            //CmdText = "select distinct CS.UserID,CS.PWD,VRoom from CalSignup CS   where  CS.[Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and CS.[End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and CS.day='" + Day + "' and CS.eventyear = '" + ddYear.SelectedValue + "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and CS.day='" + Day + "' and  eventyear = '" + ddYear.SelectedValue + "' and Accepted='Y') ";

                        //            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                        //            DataSet ds1 = new DataSet();

                        //            CmdText = "select WC.UserID,WC.PWD from WebConfLog WC where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' or wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and  WC.eventyear = '" + year + "' and (SessionType='Substitute' or SessionType='Makeup') and WC.MemberID in(select CMemberID from CoachReg where EventYear='" + year + "' and Approved='Y') order by PWD ASC";

                        //            // CmdText = "select WC.UserID,WC.PWD from WebConfLog WC   where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndTime.Substring(0, 5) + "' and  WC.eventyear = '2015' and WC.MemberID in(select CMemberID from CoachReg where EventYear=2015 and Approved='Y')";
                        //            ds1 = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                        //            IsSameTime = 0;
                        //            IsSameDay = 0;
                        //            string Uid = string.Empty;
                        //            if (ds.Tables[0].Rows.Count > 0)
                        //            {
                        //                //ddlVRoom.Items.Clear()
                        //                if (ds1.Tables[0].Rows.Count > 0)
                        //                {
                        //                    foreach (DataRow dr1 in ds1.Tables[0].Rows)
                        //                    {
                        //                        int rowcount = ds.Tables[0].Rows.Count;
                        //                        for (int i = 0; i < rowcount; i++)
                        //                        {
                        //                            if (ds.Tables[0].Rows[i]["UserID"].ToString() != dr1["UserID"].ToString())
                        //                            {
                        //                                WebExUserID = ds.Tables[0].Rows[i]["UserID"].ToString();
                        //                                WebExUserPwd = ds.Tables[0].Rows[i]["Pwd"].ToString();

                        //                                Vroom = ds.Tables[0].Rows[i]["Vroom"].ToString();
                        //                            }
                        //                            else
                        //                            {
                        //                                ds.Tables[0].Rows[i].Delete();
                        //                                ds.Tables[0].AcceptChanges();
                        //                                rowcount = rowcount - 1;
                        //                            }

                        //                        }
                        //                        //foreach (DataRow dr2 in ds.Tables[0].Rows)
                        //                        //{


                        //                        //}
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    foreach (DataRow dr1 in ds.Tables[0].Rows)
                        //                    {
                        //                        WebExUserID = dr1["UserID"].ToString();
                        //                        WebExUserPwd = dr1["Pwd"].ToString();
                        //                        Vroom = dr1["Vroom"].ToString();
                        //                    }
                        //                }

                        //            }
                        //            if (WebExUserID != "" && WebExUserPwd != "")
                        //            {
                        //                int duration = 0;
                        //                if (durationHrs != "")
                        //                {
                        //                    duration = Convert.ToInt32(durationHrs);
                        //                }

                        //                RegisterAlternateHost(Meetinkey, EndTime, Day, SignUpId, WebExID, Pwd, Vroom, duration, WebExUserID, WebExUserPwd, BeginTime);

                        //            }
                        //            else
                        //            {
                        //                lblerr.Text = "Time is not available...!";
                        //            }


                        //        }
                        //        else
                        //        {
                        //            if (dr["SubstituteMeetKey"].ToString() != "")
                        //            {
                        //                lblerr.Text = "Duplicate exists..!";
                        //            }
                        //            else
                        //            {
                        //                lblerr.Text = "Training sessions not created yet for the selected coach.";
                        //            }
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    lblerr.Text = "No child is assigned for the selected Coach..";
                        //}
                    }
                }
                else
                {
                    if (dt < dtSession)
                    {
                        lblerr.Text = "This application can only be run on " + SessionCreationDate + "";
                    }
                    else if (CoachChange.Trim() == "Y")
                    {
                        lblerr.Text = "This application cannot be run since the coach change flag is not set to N";
                    }
                }
            }
            else
            {
                lblerr.Text = "This application can only be run on.....";
            }

        }
        catch (Exception ex)
        {
            //throw new Exception(ex.Message);
        }

    }



    public void fillMeetingGrid()
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;
        if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
        {
            cmdtext = "select Vc.VideoConfLogID,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as SubCoach,IP1.FirstName +' '+ IP1.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,cs.userID as WebUID,cs.Pwd as WebPwd,cs.Vroom from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.SubstituteCoachID) left join calsignup cs on (cs.SubstituteMeetKey=VC.SessionKey and VC.SubstituteCoachID=cs.SubstituteCoachID) left join IndSpouse ip1 on(IP1.AutoMemberID=VC.MemberID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.MemberID=" + ddlCoach.SelectedValue + " or VC.MemberID=" + DDLSubstituteCoach.SelectedValue + " or VC.SubstituteCoachID=" + DDLSubstituteCoach.SelectedValue + " or VC.SubstituteCoachID=" + ddlCoach.SelectedValue + " and VC.SubstituteCoachID is not null and Vc.SessionType='Substitute'  order by VC.ProductGroupCode";
        }
        else
        {
            cmdtext = "select Vc.VideoConfLogID, VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as SubCoach,IP1.FirstName +' '+ IP1.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day ,cs.userID as WebUID,cs.Pwd as WebPwd,cs.Vroom from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.SubstituteCoachID) left join IndSpouse ip1 on(IP1.AutoMemberID=VC.MemberID) left join calsignup cs on (cs.SubstituteMeetKey=vc.SessionKey) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.SubstituteCoachID is not null and  Vc.SessionType='Substitute' order by VC.ProductGroupCode";
        }

        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public string ValidateCreateSession()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }
        else if (DDLSubstituteCoach.SelectedValue == "0" || DDLSubstituteCoach.SelectedValue == "")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (ddlCoach.SelectedValue == "0" || ddlCoach.SelectedValue == "")
        {
            lblerr.Text = "Please select Substitute Coach";
            retVal = "-1";
        }
        return retVal;
    }

    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }


        else if (ddlPhase.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Phase";
            retVal = "-1";
        }
        else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "0")
        {
            lblerr.Text = "Please select Level";
            retVal = "-1";
        }

        else if (ddlSession.SelectedValue == "-1" || ddlSession.SelectedValue == "0")
        {
            lblerr.Text = "Please select Session No";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        //else if (txtDate.Text == "")
        //{
        //    lblerr.Text = "Please fill Date";
        //    retVal = "-1";
        //}
        //else if (txtTime.Text == "")
        //{
        //    lblerr.Text = "Please fill Time";
        //    retVal = "-1";
        //}
        //else if (txtSubstituteDate.Text == "")
        //{
        //    lblerr.Text = "Please fill substitute Date";
        //    retVal = "-1";
        //}
        //else if (txtSubstituteTime.Text == "")
        //{
        //    lblerr.Text = "Please fill substitute Time";
        //    retVal = "-1";
        //}
        //else if (txtDuration.Text == "")
        //{
        //    lblerr.Text = "Please fill Duration";
        //    retVal = "-1";
        //}
        return retVal;
    }


    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                registerMeetingsAttendees(ChidName, Email.Trim(), City, Country);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {

                    GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

                    string CmdText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "', MakeUpRegID=" + hdsnRegistrationKey.Value + ", makeUpAttendeeID=" + hdnMeetingAttendeeID.Value + " where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y' and Level='" + Level + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblSuccess.Text = "Selected child registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                    fillStudentList(MemberID, ddlProductGroup.SelectedItem.Text, ddlProduct.SelectedItem.Text);
                }

            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string PMemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblPMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string ProductCode = string.Empty;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                string LoginSessionID = string.Empty;
                string LoginSessionChildID = string.Empty;
                hdnChildMeetingURL.Value = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblChildURL") as Label).Text;
                DataSet ds = new DataSet();
                string email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                int countEmail = 0;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + email.Trim() + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
                //if (Session["LoginID"] != null)
                //{
                //    LoginSessionID = Session["LoginID"].ToString();
                //}
                //if (Session["StudentID"] != null)
                //{
                //    LoginSessionChildID = Session["StudentID"].ToString();
                //}
                //if (LoginSessionID == PMemberID)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                //else if (LoginSessionChildID == ChildNumber)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                if (countEmail > 0)
                {
                    string status = GetTrainingSessions(hdnWebExID.Value, hdnWebExPwd.Value, hdnSessionKey.Value);
                    if (status == "INPROGRESS")
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" + ProductCode + "','" + hdnCoachname.Value + "');", true);
                        //lblerr.Text = "Meeting Not In-Progress";

                        // System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                }
                else
                {
                    lblerr.Text = "Only authorized child can join ito the training session.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;

                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;

                fillSubstituteCoach();
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;
                txtMeetingPwd.Text = GrdMeeting.Rows[selIndex].Cells[13].Text;
                txtDate.Text = GrdMeeting.Rows[selIndex].Cells[15].Text;
                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;
                txtTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;
                txtEndTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlEndTime") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[11].Text;
                tblAddNewMeeting.Visible = true;

                btnCreateMeeting.Text = "Update Meeting";
                hdnSessionKey.Value = sessionKey;
                txtEndDate.Text = GrdMeeting.Rows[selIndex].Cells[16].Text;

                ddlDay.SelectedValue = (GrdMeeting.Rows[selIndex].Cells[14].Text).ToUpper();
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[14].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                fillStudentList(MemberID, ProductGroupCode, ProductCode);
            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                //hdnWebExMeetURL.Value = MeetingURl;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnSessionKey.Value = sessionKey;
                TestGetHostUrlMeeting(WebExID, WebExPwd, hdnSessionKey.Value);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {
                    string MeetingURl = hdnHostURL.Value;
                    string URL = MeetingURl.Replace("&amp;", "&");
                    hdnWebExMeetURL.Value = URL;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
                }
                else
                {
                    lblerr.Text = hdnMeetingStatus.Value;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid();
    }

    protected void GrdMeeting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {

            GrdMeeting.EditIndex = e.RowIndex;
            int rowIndex = e.RowIndex;
            GrdMeeting.EditIndex = -1;
            string sessionKey = string.Empty;
            sessionKey = GrdMeeting.Rows[rowIndex].Cells[13].Text;
            hdnSessionKey.Value = sessionKey;
            string WebExID = string.Empty;
            string WebExPwd = string.Empty;
            WebExID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExID") as Label).Text;
            WebExPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExPwd") as Label).Text;
            string WebUID = string.Empty;
            string WebUPwd = string.Empty;
            WebUID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebUID") as Label).Text;
            WebUPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("LabelblWebPwd") as Label).Text;
            string Vroom = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblVroom") as Label).Text;

            string WebConfLogID = string.Empty;
            WebConfLogID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebConfLogID") as Label).Text;
            hdnWebConflogID.Value = WebConfLogID;
            hdnSessionKey.Value = sessionKey;
            hdnWebExID.Value = WebUID;
            hdnWebExPwd.Value = WebUPwd;
            hdnVroom.Value = Vroom;
            hdnSubWebExID.Value = WebExID;
            hdnSubWebExPwd.Value = WebExPwd;

            //DelMeetingAttendee(sessionKey, WebUID, WebUPwd, "22");
            GetTrainingSessionsRegID(WebUID, WebUPwd, sessionKey);

            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);

        }
        catch (Exception ex)
        {
        }
    }


    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode)
    {

        trChildList.Visible = true;
        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level,CR.SubstituteURL,CR.SubstituteAttendeeID,CR.SubstituteRegID from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + MemberID + " and Approved='Y') and CR.Level='" + HdnLevel.Value + "' order by C1.Last_Name, C1.First_Name Asc";



        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "";
                    btnClosetable2.Visible = true;

                    for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                    {
                        if (((LinkButton)GrdStudentsList.Rows[i].FindControl("HlAttendeeMeetURL") as LinkButton).Text != "")
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                        }
                    }
                }
                else
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "No record found";
                    btnClosetable2.Visible = false;

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void CreatMakeUpSessions(string WebEXID, string PWD, string MeetingTitle, int Capacity, string StartDate, string Time, string SignupID, string CoachID, string Day, string EndTime)
    {
        MeetingTitle = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text + "(Substitute)";
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";

        strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<accessControl>\r\n";
        //strXML += "  <listing>PUBLIC</listing>\r\n";
        strXML += "  <sessionPassword>training</sessionPassword>\r\n";
        strXML += "</accessControl>\r\n";

        strXML += "<schedule>\r\n";
        strXML += "     <startDate>" + StartDate + " " + Time + "</startDate>\r\n";

        //strXML += "     <timeZone>GMT-12:00, Dateline (Eniwetok)</timeZone>\r\n";
        strXML += "     <duration>" + txtDuration.Text + "</duration>\r\n";
        //strXML += "     <duration>240</duration>\r\n";
        strXML += "     <timeZoneID>" + ddlTimeZone.SelectedValue + "</timeZoneID>\r\n";
        strXML += "     <openTime>20</openTime>\r\n";
        strXML += "</schedule>\r\n";

        strXML += "<metaData>\r\n";
        strXML += "     <confName>" + MeetingTitle + "</confName>\r\n";
        strXML += "     <agenda>agenda 1</agenda>\r\n";
        strXML += "     <description>Training</description>\r\n";
        strXML += "     <greeting>greeting</greeting>\r\n";
        strXML += "     <location>location</location>\r\n";
        strXML += "     <invitation>invitation</invitation>\r\n";
        strXML += "</metaData>\r\n";




        strXML += "<attendeeOptions>\r\n";
        strXML += "      <request>true</request>\r\n";
        strXML += "      <registration>true</registration>\r\n";
        strXML += "      <auto>true</auto>\r\n";
        strXML += "      <registrationPWD>training</registrationPWD>\r\n";
        strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        strXML += "      <registrationCloseDate>03/30/2016 12:00:00";
        strXML += "      </registrationCloseDate>\r\n";
        strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        strXML += "</attendeeOptions>";

        strXML += " <enableOptions>";
        strXML += "     <autoDeleteAfterMeetingEnd>true</autoDeleteAfterMeetingEnd>";
        strXML += "     <supportBreakoutSessions>true</supportBreakoutSessions>";
        strXML += "     <presenterBreakoutSession>true</presenterBreakoutSession>";
        // strXML += "     <attendeeBreakoutSession>true</attendeeBreakoutSession>";
        strXML += "  <trainingSessionRecord>false</trainingSessionRecord>";
        strXML += " </enableOptions>";

        strXML += " <telephony>";
        strXML += "<telephonySupport>CALLIN</telephonySupport>";
        strXML += " <numPhoneLines>1</numPhoneLines>";
        // strXML += "<extTelephonyURL>String</extTelephonyURL>";
        //<extTelephonyDescription>String</extTelephonyDescription>
        //<enableTSP>false</enableTSP>
        //<tspAccountIndex>1</tspAccountIndex>
        strXML += " </telephony>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingResponse(xmlReply);

        if (!string.IsNullOrEmpty(result))
        {
            if (hdnMeetingStatus.Value == "SUCCESS")
            {
                TestGetHostUrlMeeting(WebEXID, PWD, hdnSessionKey.Value);
                string cmdText = "";
                string meetingURL = hdnHostURL.Value;
                cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SubstituteCoachID)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + DDLSubstituteCoach.SelectedValue + ",'" + hdnSessionKey.Value + "','" + txtSubstituteDate.Text + "','" + txtSubstituteDate.Text + "','" + txtSubstituteTime.Text + "','" + EndTime + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "',1,'training'," + txtDuration.Text + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + WebEXID + "','" + PWD + "'," + ddlCoach.SelectedValue + ")";
                try
                {
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    cmdText = "Update CalSignup set SubstituteURL='" + meetingURL + "', SubstituteMeetKey='" + hdnSessionKey.Value + "', SubstitutePwd='training',SubstituteDate='" + txtSubstituteDate.Text + "',SubstituteTime='" + txtSubstituteTime.Text + "',SubstituteDuration='" + txtDuration.Text + "', SubstituteCoachID=" + ddlCoach.SelectedValue + " where SignupID=" + SignupID + "";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    string ChidName = string.Empty;
                    string Email = string.Empty;
                    string City = string.Empty;
                    string Country = string.Empty;
                    string ChildNumber = string.Empty;
                    string CoachRegID = string.Empty;
                    hdnWebExID.Value = WebEXID;
                    hdnWebExPwd.Value = PWD;

                    DataSet dsChild = new DataSet();

                    string ChildText = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and CR.ProductCode='" + ddlProduct.SelectedItem.Text + "' and CR.CMemberID=" + DDLSubstituteCoach.SelectedValue + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and ProductCode='" + ddlProduct.SelectedItem.Text + "' and CMemberID=" + DDLSubstituteCoach.SelectedValue + " and Approved='Y' and Level='" + ddlLevel.SelectedValue + "') and CR.Level='" + ddlLevel.SelectedValue + "'";



                    dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                    if (null != dsChild && dsChild.Tables.Count > 0)
                    {
                        if (dsChild.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow drChild in dsChild.Tables[0].Rows)
                            {
                                ChidName = drChild["Name"].ToString();
                                Email = drChild["WebExEmail"].ToString();
                                if (Email.IndexOf(';') > 0)
                                {
                                    Email = Email.Substring(0, Email.IndexOf(';'));
                                }
                                City = drChild["City"].ToString();
                                Country = drChild["Country"].ToString();
                                ChildNumber = drChild["ChildNumber"].ToString();
                                CoachRegID = drChild["CoachRegID"].ToString();
                                registerMeetingsAttendees(ChidName, Email, City, Country);
                                if (hdnMeetingStatus.Value == "SUCCESS")
                                {
                                    GetJoinUrlMeeting(ChidName, Email.Trim(), hdsnRegistrationKey.Value);

                                    string CmdChildUpdateText = "update CoachReg set SubstituteURL='" + hdnMeetingUrl.Value + "', SubstituteRegID=" + hdsnRegistrationKey.Value + ", SubstituteAttendeeID=" + hdnMeetingAttendeeID.Value + ",ModifyDate=GetDate(), ModifiedBy='" + Session["LoginID"].ToString() + "',SubstituteDate='" + txtSubstituteDate.Text + "',SubstituteTime='" + txtSubstituteTime.Text + "',SubstituteDuration='" + txtDuration.Text + "', SubstituteCoachID=" + ddlCoach.SelectedValue + " where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + DDLSubstituteCoach.SelectedValue + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Approved='Y' and CoachRegID='" + CoachRegID + "' and Level='" + ddlLevel.SelectedValue + "'";

                                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);
                                }
                            }
                        }
                    }

                    fillMeetingGrid();
                }
                catch (Exception ex)
                {
                }
                lblSuccess.Text = "Training session Created successfully";
            }
            else
            {
                lblerr.Text = hdnException.Value;
            }
        }
        else
        {
            lblerr.Text = hdnException.Value;
        }
    }

    public void registerMeetingsAttendees(string Name, string Email, string City, string Country)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.RegisterMeetingAttendee\">\r\n";//

        strXML += "<attendees>";
        strXML += "<person>";
        strXML += "<name>" + Name + "</name>";
        strXML += "<title>title</title>";
        strXML += "<company>microsoft</company>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += "<city>" + City + "</city>";
        strXML += "<country>" + Country + "</country>";
        strXML += "</address>";
        //strXML += "<phones>0</phones>";
        strXML += "<email>" + Email + "</email>";
        strXML += "<notes>notes</notes>";
        strXML += "<url>https://</url>";
        strXML += "<type>VISITOR</type>";
        strXML += "</person>";
        strXML += "<joinStatus>ACCEPT</joinStatus>";
        strXML += "<role>ATTENDEE</role>";
        // strXML += "<sessionNum>1</sessionNum>";
        // strXML += "<emailInvitations>true</emailInvitations>";
        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "</attendees>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingAttendeeResponse(xmlReply);
        ///lblMsg1.Text = result;
    }

    private string ProcessMeetingAttendeeResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Attendee Information</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText;

                string attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText;
                //xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).RemoveAll();

                //string meetingKey1 = (xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent", manager).LastChild).InnerText;
                //hdnSessionKey.Value = meetingKey;
                sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey + "<br/>");
                hdsnRegistrationKey.Value = meetingKey;
                hdnMeetingAttendeeID.Value = attendeeID;
                //hdsnRegistrationKey1.Value = meetingKey1;
                //sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey1 + "<br/>");

                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                //hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                lblerr.Text = error;
            }
            else
            {
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    public void GetJoinUrlMeeting(string Name, string email, string RegsiterID)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "<attendeeName>" + Name + "</attendeeName>";
        strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
        strXML += "<meetingPW>" + hdnMeetingPwd.Value + "</meetingPW>";
        strXML += "<RegID>" + RegsiterID + "</RegID>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg2.Text += result;
        //}
    }

    private string ProcessMeetingAttendeeJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {

                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");

                string meetingKey2 = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml;
                string URL = meetingKey2.Replace("&amp;", "&");



                hdnMeetingUrl.Value = URL;

                sb.Append("<a href=" + URL + " target='blank'>" + URL + "</a></br>");


            }
            else if (status == "FAILURE")
            {

                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    public void TestGetHostUrlMeeting(string WebExID, string Pwd, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "<email>webex.nsf.adm@gmail.com</email>";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GethosturlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        //strXML += "<meetingPW>meetme</meetingPW>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string Result = ProcessMeetingHostJoinURLResponse(xmlReply);
    }

    private string ProcessMeetingHostJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = status;

                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml;

                string URL = meetingKey.Replace("&amp;", "&");
                hdnHostURL.Value = URL;



            }
            else if (status == "FAILURE")
            {

                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {

                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    private string ProcessMeetingResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                string sessionKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:sessionkey", manager).InnerText;
                hdnSessionKey.Value = sessionKey;

            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";

                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                hdnException.Value = error;
            }
            else
            {

                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    protected void btnClosetable2_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
    }

    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        //CancelMeeting(hdnSessionKey.Value, hdnWebExID.Value, hdnWebExPwd.Value);

        DelMeetingAttendee(hdnSessionKey.Value, hdnSubWebExID.Value, hdnSubWebExPwd.Value, hdnVroom.Value);

    }
    public void CancelMeeting(string sessionKey, string WebExID, string Pwd)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.DelTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + sessionKey + "</sessionKey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        //string result = ProcessMeetingHostJoinURLResponse(xmlReply);
        //lblMsg3.Text = result;
        lblSuccess.Text = "Meeting Cancelled Successfully";
        try
        {
            string cmdText = "delete from WebConfLog where SessionKey='" + sessionKey + "'";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            cmdText = "update CalSignUp set status=null, substituteURL=null,substituteMeetKey=null,substitutePwd=null,substituteDate=null,SubstituteTime=null,substituteDuration=null where substituteMeetKey='" + sessionKey + "'";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            cmdText = "update CoachReg set status=null, substituteURL=null,SubstituteAttendeeID=null,SubstituteRegID=null,substituteDate=null,SubstituteTime=null,substituteDuration=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y'";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        fillMeetingGrid();
    }
    public string GetTrainingSessions(string WebExID, string PWD, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;

        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingStatusTestResponse(xmlReply);
        }

        if (result == "INPROGRESS")
        {
            //string cmdUpdateText = "update WebConfLog set Status='InProgress' where SessionKey='" + SessionKey + "'";
            //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

        }
        else
        {
            // lblerr.Text = "Meeting Not In-Progress";

        }
        return result;

    }
    private string ProcessTrainingStatusTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            string ConfName = string.Empty;
            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;

                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText;
                day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText;
                startTime = startTime.Substring(10, 9);
                double Mins = 0.0;
                DateTime dFrom = DateTime.Now;
                DateTime dTo = DateTime.Now;
                string sDateFrom = startTime;

                string today = DateTime.Today.DayOfWeek.ToString();
                if (today.ToLower() == day.ToLower())
                {
                    string sDateTo = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);

                    //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    DateTime easternTime = DateTime.Now;
                    string strEasternTime = easternTime.ToShortDateString();
                    strEasternTime = strEasternTime + " " + sDateFrom;
                    DateTime dtEasternTime = Convert.ToDateTime(strEasternTime);
                    DateTime easternTimeNow = DateTime.Now.AddHours(1);

                    TimeSpan TS = dtEasternTime - easternTimeNow;
                    Mins = TS.TotalMinutes;


                }
                else
                {
                    for (int i = 1; i <= 7; i++)
                    {
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString();
                        if (today.ToLower() == day.ToLower())
                        {
                            dTo = DateTime.UtcNow.AddDays(i);
                            //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            DateTime easternTime = DateTime.Now.AddDays(i);
                            string sDateTo = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                            string targetDateTime = easternTime.ToShortDateString() + " " + sDateFrom;
                            //DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            DateTime easternTimeNow = DateTime.Now.AddHours(1);

                            DateTime dtTargetTime = Convert.ToDateTime(targetDateTime);
                            TimeSpan TS = dtTargetTime - easternTimeNow;
                            Mins = TS.TotalMinutes;


                        }
                    }
                }
                string DueMins = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."));
                hdnSessionStartTime.Value = startTime;
                hdnStartMins.Value = Convert.ToString(DueMins);
            }

        }
        catch (Exception e)
        {
            // sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }


    public void RegisterAlternateHost(string MeetingKey, string EndTime, string Day, string SignupID, string WebExID, string WebExPwd, string Vroom, int Duration, string SubWebExID, string SubWebExPwd, string BeginTime)
    {
        string CoachName = string.Empty;
        string UserID = string.Empty;
        string Pwd = string.Empty;
        string VRoom = string.Empty;
        string email = string.Empty;

        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";

        if (Vroom == "24")
        {
            email = "webex.nsf.mct@gmail.com";
        }
        else if (Vroom == "23")
        {
            email = "webex.nsf.unv@gmail.com";
        }
        else if (Vroom == "22")
        {
            email = "webex.nsf.sat@gmail.com";
        }
        else if (Vroom == "21")
        {
            email = "webex.nsf.geo@gmail.com";
        }
        else if (Vroom == "20")
        {
            email = "webex.nsf.adm@gmail.com";
        }
        else
        {
            email = "webex.nsf." + Vroom + "@gmail.com";
        }

        CoachName = ddlCoach.SelectedItem.Text;
        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + WebExPwd + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.CreateMeetingAttendee\">\r\n";//



        strXML += "<person>";
        strXML += "<name>" + CoachName + "</name>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += " </address>";
        strXML += "<email>" + email + "</email>";
        strXML += "<type>MEMBER</type>";
        strXML += "</person>";
        strXML += "<role>HOST</role>";


        strXML += "<sessionKey>" + MeetingKey + "</sessionKey>";
        //strXML += "<status>NOT_INPROGRESS</status>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            ProcessTrainingAlterTestResponse(xmlReply);
        }
        hdnSessionKey.Value = MeetingKey;
        if (hdnMeetingStatus.Value == "SUCCESS")
        {
            TestGetHostUrlMeeting(WebExID, WebExPwd, hdnSessionKey.Value);
            string cmdText = "";
            string meetingURL = hdnHostURL.Value;

            //cmdText = "Update WebConfLog set SubstituteCoachID=" + ddlCoach.SelectedValue + " where SessionKey='" + hdnSessionKey.Value + "'";
            if (DDlSubDate.SelectedValue == "0")
            {
                cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SubstituteCoachID, SessionType)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + DDLSubstituteCoach.SelectedValue + ",'" + hdnSessionKey.Value + "','" + DDlSubDate.SelectedValue + "','" + DDlSubDate.SelectedValue + "','" + BeginTime + "','" + EndTime + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "',1,'training'," + Duration + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + SubWebExID + "','" + SubWebExPwd + "'," + ddlCoach.SelectedValue + ",'Substitute')";
            }
            else
            {
                cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SubstituteCoachID, SessionType)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + DDLSubstituteCoach.SelectedValue + ",'" + hdnSessionKey.Value + "','" + DDlSubDate.SelectedValue + "','" + DDlSubDate.SelectedValue + "','" + BeginTime + "','" + EndTime + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "',1,'training'," + Duration + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + SubWebExID + "','" + SubWebExPwd + "'," + ddlCoach.SelectedValue + ",'Substitute')";
            }

            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                //cmdText = "Update CalSignup set SubstituteURL='" + meetingURL + "', SubstituteMeetKey='" + hdnSessionKey.Value + "', SubstitutePwd='training',SubstituteDate='" + txtSubstituteDate.Text + "',SubstituteTime='" + txtSubstituteTime.Text + "',SubstituteDuration='" + txtDuration.Text + "', SubstituteCoachID=" + ddlCoach.SelectedValue + " where SignupID=" + SignupID + "";

                cmdText = "Update CalSignup set SubstituteURL='" + meetingURL + "', SubstituteMeetKey='" + hdnSessionKey.Value + "', SubstitutePwd='training', SubstituteCoachID=" + ddlCoach.SelectedValue + ",SubstituteDate='" + DDlSubDate.SelectedValue + "',SubstituteTime='" + BeginTime + "',SubstituteDuration='" + Duration + "' where SignupID=" + SignupID + "";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


                fillMeetingGrid();
                lblSuccess.Text = "Substitute coach assigned for the session successfully";
            }
            catch (Exception ex)
            {
            }

        }
        else
        {
            lblerr.Text = hdnException.Value;
        }
    }

    private void ProcessTrainingAlterTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            hdnMeetingStatus.Value = status;
            string ConfName = string.Empty;
            if (status == "SUCCESS")
            {
            }
            else
            {
                hdnException.Value = status;
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;

                hdnException.Value = error;
            }
        }
        catch
        {
        }
    }

    protected void DDLSubstituteCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubstituteCoach.SelectedValue != "0")
        {
            fillCoach();
            LoadSessionNo();
            // populateRecClassWeekDate();
            populateRecClassDate();
        }
    }

    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }

    public void populateRecClassDate()
    {

        string cmdText = string.Empty;
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term') where  CS.MemberID=" + DDLSubstituteCoach.SelectedValue + " and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.EventYear=" + ddYear.SelectedValue + " and CS.Accepted='Y' and CS.Level='" + ddlLevel.SelectedValue + "' and CS.SessionNo=" + ddlSession.SelectedValue + "";

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime dtStartDate = new DateTime();
                    DateTime dtEndDate = new DateTime();
                    dtStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString());
                    string strStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("MM/dd/yyyy");
                    string EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("MM/dd/yyyy");
                    dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString());
                    string day = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("dddd");
                    string classDay = ds.Tables[0].Rows[0]["day"].ToString();
                    DateTime dtClassDate = Convert.ToDateTime(dtStartDate);
                    DDlSubDate.Items.Clear();

                    if (day == classDay)
                    {

                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");

                            DDlSubDate.Items.Insert(0, new ListItem(date, date));

                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= 7; i++)
                        {
                            dtClassDate = dtClassDate.AddDays(1);
                            string strDay = Convert.ToDateTime(dtClassDate).ToString("dddd");
                            if (strDay == classDay)
                            {
                                break;
                            }
                        }
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");

                            DDlSubDate.Items.Insert(0, new ListItem(date, date));

                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    DDlSubDate.Items.Insert(0, new ListItem("Select", "0"));
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void populateRecClassWeekDate()
    {
        string cmdText = string.Empty;
        cmdText = "select Classes from EventFees  where  ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and EventID=13";

        //cmdText = "select StatDate,EndDate from CoachingDateCal  where  ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and EventID=13 and ScheduleType='Term'";

        int ClassCount = 1;
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ClassCount = Convert.ToInt32(ds.Tables[0].Rows[0]["Classes"].ToString());
                }
            }

            SqlParameter[] @params = {
                new SqlParameter("@MemberId", SqlDbType.Int),
                new SqlParameter("@EventYear", SqlDbType.Int),
                new SqlParameter("@ProductGroupId", SqlDbType.Int),
                new SqlParameter("@ProductId", SqlDbType.Int),
                new SqlParameter("@Phase", SqlDbType.Int),
                new SqlParameter("@Level", SqlDbType.VarChar, 50),
                new SqlParameter("@SessionNo", SqlDbType.Int),
                new SqlParameter("@Iterator", SqlDbType.Int),
                new SqlParameter("@ShowAll", SqlDbType.Int)
            };
            @params[0].Value = DDLSubstituteCoach.SelectedValue;
            @params[1].Value = ddYear.SelectedValue;
            @params[2].Value = ddlProductGroup.SelectedValue;
            @params[3].Value = ddlProduct.SelectedValue;
            @params[4].Value = 1;
            @params[5].Value = ddlLevel.SelectedValue;
            @params[6].Value = ddlSession.SelectedValue;
            @params[7].Value = 1;
            @params[8].Value = 1;
            //productgroupID, productID, phase, level, week#, DocType=Q, 
            //DDlSubDate.Items.Clear();

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.StoredProcedure, "usp_GetCoachDetailsByWeek", @params);

            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //DDlSubDate.DataValueField = "CurDate";
                    //DDlSubDate.DataTextField = "CurDate";
                    DataTable dtTable = ds.Tables[0];
                    DataView dv = dtTable.DefaultView;
                    dv.Sort = "curDate DESC";
                    dtTable = dv.ToTable();
                    foreach (DataRow row in dtTable.Rows)
                    {
                        string strCurDate = Convert.ToDateTime(row["CurDate"].ToString()).ToString("MM/dd/yyyy");
                        DDlSubDate.Items.Insert(0, new ListItem(strCurDate, strCurDate));
                    }
                    //ds.AcceptChanges();
                    // DDlSubDate.DataSource = ds;
                    //DDlSubDate.DataBind();
                }
            }
            DDlSubDate.Items.Insert(0, new ListItem("Select", "0"));
        }
        catch (Exception ex)
        {
        }
    }

    public void GetTrainingSessionsRegID(string WebExID, string PWD, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingTestResponseAlternateHost(xmlReply);
        }

        if (result == "INPROGRESS")
        {
            //string cmdUpdateText = "update WebConfLog set Status='InProgress' where SessionKey='" + SessionKey + "'";
            //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

        }

    }
    private string ProcessTrainingTestResponseAlternateHost(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;

            }

        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }

    public void DelMeetingAttendee(string sessionKey, string WebExID, string Pwd, string Vroom)
    {
        //string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        //string email = string.Empty;

        //if (WebExID.ToLower() == "nsftwentyfour")
        //{
        //    email = "webex.nsf.mct@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsftwentythree")
        //{
        //    email = "webex.nsf.unv@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsftwentytwo")
        //{
        //    email = "webex.nsf.sat@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsftwentyone")
        //{
        //    email = "webex.nsf.geo@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsftwenty")
        //{
        //    email = "webex.nsf.adm@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfone")
        //{
        //    email = "webex.nsf.1@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsftwo")
        //{
        //    email = "webex.nsf.2@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfthree")
        //{
        //    email = "webex.nsf.3@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsffour")
        //{
        //    email = "webex.nsf.4@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsffive")
        //{
        //    email = "webex.nsf.5@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfsix")
        //{
        //    email = "webex.nsf.6@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfseven")
        //{
        //    email = "webex.nsf.7@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfeight")
        //{
        //    email = "webex.nsf.8@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfnine")
        //{
        //    email = "webex.nsf.9@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsften")
        //{
        //    email = "webex.nsf.10@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfeleven")
        //{
        //    email = "webex.nsf.11@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsftwelve")
        //{
        //    email = "webex.nsf.12@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfthirteen")
        //{
        //    email = "webex.nsf.13@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsffourteen")
        //{
        //    email = "webex.nsf.14@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsffifteen")
        //{
        //    email = "webex.nsf.15@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfsixteen")
        //{
        //    email = "webex.nsf.16@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfseventeen")
        //{
        //    email = "webex.nsf.17@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfeighteen")
        //{
        //    email = "webex.nsf.18@gmail.com";
        //}
        //else if (WebExID.ToLower() == "nsfnineteen")
        //{
        //    email = "webex.nsf.19@gmail.com";
        //}
        //else if (Vroom == "nsftwentyfour")
        //{
        //    email = "webex.nsf.mct@gmail.com";
        //}
        //else if (Vroom == "nsftwentythree")
        //{
        //    email = "webex.nsf.unv@gmail.com";
        //}
        //else if (Vroom == "nsftwentytwo")
        //{
        //    email = "webex.nsf.sat@gmail.com";
        //}
        //else if (Vroom == "nsftwentyone")
        //{
        //    email = "webex.nsf.geo@gmail.com";
        //}
        //else if (Vroom == "nsftwenty")
        //{
        //    email = "webex.nsf.adm@gmail.com";
        //}
        //else
        //{
        //    email = "webex.nsf." + Vroom + "@gmail.com";
        //}

        ////else
        ////{
        ////    email = "webex.nsf.2@gmail.com";
        ////}

        //WebRequest request = WebRequest.Create(strXMLServer);

        //request.Method = "POST";

        //request.ContentType = "application/x-www-form-urlencoded";


        //string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        //strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        //strXML += "<header>\r\n";
        //strXML += "<securityContext>\r\n";
        //strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        //strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";
        //strXML += "<siteName>northsouth</siteName>\r\n";
        //strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "</securityContext>\r\n";
        //strXML += "</header>\r\n";
        //strXML += "<body>\r\n";
        //strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.DelMeetingAttendee\">\r\n";

        //strXML += "<attendeeEmail>\r\n";
        //strXML += "<email>" + email + "</email>\r\n";
        //strXML += "<sessionKey>" + sessionKey + "</sessionKey>\r\n";
        //strXML += "</attendeeEmail>\r\n";

        //strXML += "</bodyContent>\r\n";
        //strXML += "</body>\r\n";
        //strXML += "</serv:message>\r\n";
        //byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        //request.ContentLength = byteArray.Length;


        //Stream dataStream = request.GetRequestStream();

        //dataStream.Write(byteArray, 0, byteArray.Length);

        //dataStream.Close();

        //WebResponse response = request.GetResponse();


        //dataStream = response.GetResponseStream();
        //XmlDocument xmlReply = null;
        //if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        //{

        //    xmlReply = new XmlDocument();
        //    xmlReply.Load(dataStream);

        //}
        //ProcessTrainingAlterTestResponse(xmlReply);



        try
        {
            if (hdnMeetingStatus.Value.ToLower() == "success")
            {
                string cmdText = "delete from WebConfLog where VideoConfLogID='" + hdnWebConflogID.Value + "'";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                cmdText = "update CalSignUp set status=null, substituteURL=null,substituteMeetKey=null,substitutePwd=null,substituteDate=null,SubstituteTime=null,substituteDuration=null where substituteMeetKey='" + sessionKey + "'";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                cmdText = "update CoachReg set status=null, substituteURL=null,SubstituteAttendeeID=null,SubstituteRegID=null,substituteDate=null,SubstituteTime=null,substituteDuration=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y'";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                lblSuccess.Text = "Substitute Coach deleted Successfully";
            }
            else
            {
                lblerr.Text = hdnException.Value;
            }
        }

        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        fillMeetingGrid();

    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y'";
            if (DDLSubstituteCoach.SelectedValue != "" && DDLSubstituteCoach.SelectedValue != "0")
            {
                cmdText += " and V.MemberID='" + DDLSubstituteCoach.SelectedValue + "'";
            }

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }

}