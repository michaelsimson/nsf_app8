﻿<%--<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="SetUpZoomSessions.aspx.cs" Inherits="Zoom_API_SetUpZoomSessions" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="SetUpZoomSessions.aspx.cs" Inherits="Zoom_API_SetUpZoomSessions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <%--  <script src="../js/jquery-1.7.2.min.js"></script>--%>
        <script src="js/jquery-1.7.2.min.js"></script>
        <style type="text/css">
            .tblCell {
                border: 1px solid #999999;
                border-collapse: collapse;
            }
        </style>
        <script type="text/javascript">
            function StartMeeting(jsonData, type) {
                if (type == "1") {

                    var sessionKey = jsonData.id;
                    document.getElementById("<%=hdnSessionKey.ClientID%>").value = sessionKey;
                    var hostlink = jsonData.start_url
                    document.getElementById("<%=hdnHostURL.ClientID%>").value = hostlink;

                    var joinLink = jsonData.join_url;
                    document.getElementById("<%=hdnJoinMeetingUrl.ClientID%>").value = joinLink;

                    document.getElementById("<%=btnCreateZoomSession.ClientID%>").click();
                    $("#spnStatus").text("Meeting created successfully");
                }
                if (type == "3") {
                    $("#spnStatus").text("Meeting deleted successfully");

                }
                if (type == "4") {
                    $("#spnStatus").text("Meeting updated successfully");
                }
                bindMeetingList(jsonData);

            }

            function bindMeetingList(jsonData) {
                var tblHtml = "";

                tblHtml += "<tr style='background-color:#FFFFCC;'>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Ser#";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Action";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Meeting Title";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Meeting Key";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Meeting Type";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Star Date/Time";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Duration";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Host URL";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Join URL";
                tblHtml += "</td>";


                tblHtml += "</tr>";
                if (jsonData.meetings != "") {

                    $.each(jsonData.meetings, function (index, value) {

                        var meetingType = "";
                        if (value.type == "1") {
                            meetingType = "Instant Meeting";
                        } else if (value.type == "2") {
                            meetingType = "Scheduled Meeting";
                        } else if (value.type == "3") {
                            meetingType = "Recurring Meeting";
                        }
                        // $.each(value, function (meetIndex, meetVal) {

                        tblHtml += "<tr style='height:25px;'>";
                        tblHtml += "<td class='tblCell'>" + (index + 1) + "";
                        tblHtml += "</td>";

                        tblHtml += "<td class='tblCell'><input type='button' attr-meetingID=" + value.id + " class='btnUpdate' value='Update'/><input type='button' attr-meetingID=" + value.id + " class='btnDelete' value='Delete'/>";
                        tblHtml += "</td>";

                        tblHtml += "<td class='tblCell'>" + value.topic + "";
                        tblHtml += "</td>";

                        tblHtml += "<td class='tblCell'>" + value.id + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'>" + meetingType + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'>" + value.start_time + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'>" + value.duration + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'><a href=" + value.start_url + ">" + (value.start_url).substring(0, 20) + "...</a>";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'><a href=" + value.join_url + ">" + (value.join_url).substring(0, 20) + "...</a>";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        //});

                    });
                } else {
                    tblHtml += "<tr style='height:25px;'>";
                    tblHtml += "<td colspan='9' align='center'><span style='font-weight:bold; color:red;'>No meeting found</span>";
                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                }

                $("#tblMeetingList").html(tblHtml);

            }

            function deleteMeeting() {


                if (confirm("Are you sure want to delete meeting?")) {

                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }
            }

            $(document).on("click", ".btnDelete", function (e) {
                var meetingID = $(this).attr("attr-meetingID");
                document.getElementById("<%=hdnMeetingKey.ClientID%>").value = meetingID;
                 if (confirm("Are you sure want to delete meeting?")) {
                     document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }

             });

            $(document).on("click", ".btnUpdate", function (e) {
                var meetingID = $(this).attr("attr-meetingID");
                document.getElementById("<%=hdnMeetingKey.ClientID%>").value = meetingID;
                var topic = $(this).parent().next().html();
                var startTime = $(this).parent().next().next().next().next().html();
                var duration = $(this).parent().next().next().next().next().next().html();
                document.getElementById("<%=txtTopic.ClientID%>").value = topic;
                document.getElementById("<%=txtStartTime.ClientID%>").value = startTime;
                document.getElementById("<%=txtDuration.ClientID%>").value = duration;
                document.getElementById("<%=txtMeetingPwd.ClientID%>").value = "training";
                document.getElementById("<%=btnCreateMeeting.ClientID%>").value = "Update Session";
                document.getElementById("<%=hdnIsUpdate.ClientID%>").value = "1";
            });

        </script>
    </div>

    <div style="width: 1000px; margin-left: auto; margin-right: auto; min-height: 400px;">
        <div>
            <asp:Button ID="btnDeleteMeeting" runat="server" Text="Delete Meeting" Style="display: none;" OnClick="btnDeleteMeeting_Click" />
            <asp:Button ID="btnUpdateMeeting" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnCreateZoomSession_Click" />
            <asp:Button ID="btnCreateZoomSession" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnCreateZoomSession_Click" />
            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <h1 style="color: #46A71C;">Set up Zoom Sessions</h1>
        </div>
        <div style="clear: both; margin-bottom: 30px;"></div>
        <div style="width: 90%; margin: auto; position: relative; left: 10%;">
            <table style="margin-left: auto; margin-right: auto; float: left; width: 45%;">

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Year
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlYear" OnSelectedIndexChanged="ddYear_SelectedIndexChanged" runat="server" AutoPostBack="true">

                            <asp:ListItem Value="2016">2016</asp:ListItem>
                            <asp:ListItem Value="2015">2015</asp:ListItem>
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Chapter
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlChapter" runat="server">
                            <asp:ListItem Value=""></asp:ListItem>
                            <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Product Group
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlProductGroup" Style="width: 103px;" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>



                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Level
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlLevel" Style="width: 103px;" runat="server">
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>



                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Topic
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtTopic"></asp:TextBox>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>
                <tr class="ContentSubTitle" align="center" style="display: none;">
                    <td align="left">Start Date
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                        <span style="color: red; position: relative; left: 5px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>
                <tr class="ContentSubTitle" align="center">
                    <td align="left">Duration
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDuration" runat="server"></asp:TextBox>
                        <span style="color: red; position: relative; left: 5px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center">


                    <td align="left">Join Before Host
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlJoinBH" Style="width: 103px;" runat="server">

                            <asp:ListItem Value="true">True</asp:ListItem>
                            <asp:ListItem Value="false">False</asp:ListItem>

                        </asp:DropDownList>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>
                <tr class="ContentSubTitle" align="center">
                    <td align="left">Host Video
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlHostVideo" Style="width: 103px;" runat="server">

                            <asp:ListItem Value="true">True</asp:ListItem>
                            <asp:ListItem Value="false">False</asp:ListItem>

                        </asp:DropDownList>
                        <div style="margin-bottom: 5px;"></div>
                    </td>

                </tr>
                <tr class="ContentSubTitle" align="center">
                    <td align="left">
                        <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Session" OnClick="btnCreateMeeting_Click" />
                        <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                        <div style="clear: both; margin-bottom: 5px;"></div>
                        <span style="color: red; font-size: 11px;">* mandatory fields</span>
                    </td>
                    <td align="left"></td>
                </tr>
            </table>
            <table style="margin-left: auto; margin-right: auto; float: left; width: 40%;">
                <tr class="ContentSubTitle" align="center">
                    <td align="left">Event
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlEvent" Style="width: 103px;" runat="server">
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Coach
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlCoach" Style="width: 103px;" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>



                </tr>




                <tr class="ContentSubTitle" align="center">
                    <td align="left">Product
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlProduct" Style="width: 103px;" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>



                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Session No
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlSessionNo" Style="width: 103px;" runat="server">
                        </asp:DropDownList>
                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>



                </tr>


                <tr class="ContentSubTitle" align="center">
                    <td align="left">Meeting Type
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlMeetingType" Style="width: 103px;" runat="server">

                            <asp:ListItem Value="2">Scheduled Meeting</asp:ListItem>
                            <asp:ListItem Value="1">Instant Meeting</asp:ListItem>

                            <asp:ListItem Value="3">Recurring Meeting</asp:ListItem>

                        </asp:DropDownList>

                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center" style="display: none;">
                    <td align="left">Start Time
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtStartTime" runat="server"></asp:TextBox>
                        <span style="color: red; position: relative; left: 5px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Meeting Password
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtMeetingPwd" TextMode="Password" runat="server"></asp:TextBox><span style="color: red; position: relative; left: 5px;">*</span>
                        <div style="margin-bottom: 10px;"></div>
                    </td>
                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Start Type
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlStartType" Style="width: 103px;" runat="server">

                            <asp:ListItem Value="Video">Video</asp:ListItem>
                            <asp:ListItem Value="Scree_Share">Scree_Share</asp:ListItem>

                        </asp:DropDownList>
                        <div style="margin-bottom: 5px;"></div>
                    </td>

                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Audio
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlAudio" Style="width: 103px;" runat="server">
                            <asp:ListItem Value="Both">Both</asp:ListItem>
                            <asp:ListItem Value="Voip">Voip</asp:ListItem>
                            <asp:ListItem Value="Telephony">Telephony</asp:ListItem>


                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
        <div style="clear: both;"></div>
        <div align="center"><span id="spnError" runat="server" style="color: Red; font-weight: bold;"></span></div>
        <div style="clear: both;"></div>
        <div align="center"><span id="spnStatus" runat="server" style="color: green; font-weight: bold;"></span></div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <span style="font-weight: bold; font-size: 13px;">Table 1: Meeting List</span>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <table id="tblMeetingList" align="center" style="border: 1px solid #999999; border-width: 3px; border-collapse: collapse; width: 100%;">
        </table>

        <div style="clear: both;"></div>
        <div style="width: 1000px; overflow: scroll;">
            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" OnRowCommand="GrdMeeting_RowCommand" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                        <ItemTemplate>
                            <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                                <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                                <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                                <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                                <asp:Label ID="lblOrgTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'>'></asp:Label>
                            </div>
                            <asp:Button ID="btnModifyMeeting" runat="server" Text="Update" CommandName="Modify" />
                            <asp:Button ID="btnCancelMeeting" runat="server" Text="Delete" CommandName="DeleteMeeting" />
                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="SessionNo" HeaderText="Session"></asp:BoundField>
                    <asp:TemplateField HeaderText="Coach">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                    <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                    <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:TemplateField HeaderText="Begin Time">

                        <ItemTemplate>
                            <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Time">

                        <ItemTemplate>
                            <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                    <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>
                            <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>

                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <span style="font-weight: bold; font-size: 13px;">Table 2: Students List</span>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div style="width: 1000px; overflow: scroll;">
            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdStudentsList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1000px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" Visible="false" HeaderText="Action">

                        <ItemTemplate>


                            <%--  <asp:Button ID="btnSelect" runat="server" Text="Register" CommandName="Register" />
                            <asp:Button ID="BtnReRegister" runat="server" Text="Update Registration" CommandName="ReRegister" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>

                            <div style="display: none;">
                                <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblPMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>'>'></asp:Label>
                                <asp:Label ID="LblChildURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeJoinURL") %>'>'></asp:Label>
                                <asp:Label ID="lblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>
                                <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Child"></asp:BoundField>
                    <asp:BoundField DataField="ParentName" HeaderText="Parent"></asp:BoundField>
                    <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                    <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                    <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                    <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="AttendeeID" Visible="false" HeaderText="Attendee ID"></asp:BoundField>
                    <asp:BoundField DataField="RegisteredID" Visible="false" HeaderText="Registered ID"></asp:BoundField>
                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>

                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
            <div style="clear: both;"></div>
        </div>
    </div>

    <input type="hidden" id="hdnMeetingKey" value="" runat="server" />
    <input type="hidden" id="hdnIsUpdate" value="" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="" runat="server" />
    <input type="hidden" id="hdnHostURL" value="" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="" runat="server" />
    <input type="hidden" id="hdnJoinMeetingUrl" value="" runat="server" />

</asp:Content>
