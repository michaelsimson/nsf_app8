<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachClassContactList.aspx.vb" Inherits="CoachClassContactList" Title="Volunteer Contact List" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>
        <script language="javascript" type="text/javascript">


            function startChildMeeting() {
                var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
                $("#ancClick").target = "_blank";
                $("#ancClick").attr("href", url);
                $("#ancClick").attr("target", "_blank");
                document.getElementById("ancClick").click();

            }
            function joinChildMeeting() {
                startChildMeeting();
            }

            function showAlert(prdCode, coachName) {
                var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
                var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
                var dueMins = parseInt(startMins);
                var msg = "";
                if (dueMins > 0) {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                else if (dueMins >= -15) {
                    msg = "The coach has not started the class.";
                } else if (dueMins < -15) {
                    msg = "The coach has either not started or may have cancelled the class.";
                } else {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
                alert(msg);
            }

        </script>
    </div>

    <a id="ancClick" target="_blank" style="display: none;" href=""></a>
    <asp:SqlDataSource ID="ChapInZonesDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="usp_GetChapterWithinZone" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="DdlZonalCoordinator" Name="ZoneID" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="ChaWithinClustersDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="usp_GetChapterWithinCluster" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="DdlCluster" Name="ClusterID" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="ChapersDSet" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="usp_GetChapterAll" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <table border="0" width="1000px">
        <tr>
            <td>
                <b>
                    <asp:HyperLink ID="hlinkParentRegistration" runat="server" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:HyperLink>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:Label ForeColor="green" ID="lblLoginName" runat="server"></asp:Label>
                </b>
                &nbsp;&nbsp;&nbsp;&nbsp; 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>
                <asp:Label ForeColor="green" ID="lblLoginRole" runat="server"></asp:Label>
            </b>
            </td>
        </tr>
        <tr>

            <td align="center" id="tbl1">
                <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                    runat="server">
                    <strong>Students Contact List
                    </strong>
                </div>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="lblSessionTimeout" Visible="false" ForeColor="red" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table border="1" runat="server" id="tblAssignRoles" bgcolor="silver" width="30%">
                    <tbody>
                        <tr>
                            <td colspan="2" bgcolor="white">
                                <b>1. Select Role to get Contact List</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right" style="width: 103px; height: 25px;">&nbsp;Role Category:
                            </td>
                            <td style="width: 500px; height: 25px;">
                                <asp:DropDownList ID="ddlRoleCatSch" TabIndex="7" runat="server" CssClass="SmallFont" AutoPostBack="true" OnSelectedIndexChanged="ddlRoleCat_selectedIndexChanged" Width="160px">
                                    <asp:ListItem Text="Select a Category" Value="0" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblRoleCat" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="ItemLabel" valign="top" nowrap align="right" style="width: 103px">Role:
                            </td>
                            <td style="width: 347px">
                                <asp:ListBox ID="ddlRoleSch" SelectionMode="Multiple" TabIndex="7" DataTextField="selection" DataValueField="roleid" runat="server" CssClass="SmallFont" Width="160px"></asp:ListBox>
                                <br />
                                (Hold Ctrl key to select muplitple roles)
                            </td>
                        </tr>
                        <tr runat="server" id="tr1">
                            <td class="ItemLabel" valign="top" nowrap align="right" style="width: 103px; height: 25px;">&nbsp;Week of :
                            </td>
                            <td style="width: 500x; height: 25px;">
                                <asp:DropDownList ID="ddWeekOf" TabIndex="7" runat="server" CssClass="SmallFont" AutoPostBack="true" Enabled="false" Width="160px">
                                    <asp:ListItem Text="Select a Date" Value="0" Selected="True"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblWeekOf" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server" id="tr2">
                            <td class="ItemLabel" valign="top" nowrap align="right" style="width: 103px">Zone:
                            </td>
                            <td style="width: 347px">
                                <asp:SqlDataSource ID="ZoneDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                                    SelectCommand="usp_Getzones" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                <asp:DropDownList ID="DdlZonalCoordinator" runat="server" Width="160px" AppendDataBoundItems="True" OnSelectedIndexChanged="DdlZonalCoordinator_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr runat="server" id="tr3">
                            <td class="ItemLabel" valign="top" nowrap align="right" style="width: 103px">Cluster:
                            </td>
                            <td style="width: 347px">
                                <asp:DropDownList ID="DdlCluster" runat="server" Width="162px" OnSelectedIndexChanged="Cluster_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True">
                                    <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="ClusterDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                                    SelectCommand="usp_Getclusters" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr runat="server" id="tr4">
                            <td class="ItemLabel" valign="top" nowrap align="right" style="width: 103px">Chapter:
                            </td>
                            <td style="width: 347px">
                                <asp:DropDownList ID="ddlChapter" runat="server" AppendDataBoundItems="True">
                                    <asp:ListItem>Select Chapter</asp:ListItem>
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ChapterDS" runat="server" OldValuesParameterFormatString="original_{0}"
                                    SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter"></asp:ObjectDataSource>
                            </td>
                        </tr>
                        <tr runat="server" id="TrPrdGrp" visible="false">
                            <td class="ItemLabel" valign="top" nowrap align="right">Product Group
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlProductGroup" Width="170px" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr runat="server" id="TrPrd" visible="false">
                            <td class="ItemLabel" valign="top" nowrap align="right">Product 
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlProduct" Enabled="false" DataTextField="Name" DataValueField="ProductID" Width="170px" runat="server"></asp:DropDownList>
                                <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 4px; width: 103px;">
                                <asp:Label ID="lbllRoleIds" runat="server" Visible="false"></asp:Label>
                            </td>
                            <td style="width: 347px; height: 4px;">
                                <asp:Button ID="Find" runat="server" Text="Find" />
                                <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label>
                <table cellpadding="3" border="0" cellspacing="0">
                    <tr id="trviewChildren" runat="server" visible="false">
                        <td class="ItemLabel" style="vertical-align: middle" nowrap align="right">Select Coach
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCoach" AutoPostBack="true" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" DataTextField="Name"
                                DataValueField="ID" runat="server" Height="22px" Width="150px">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                        <td>Year :
                     <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlCyear_SelectedIndexChanged" ID="ddlCYear" runat="server">
                     </asp:DropDownList>
                        </td>
                        <td>Phase :</td>
                        <td>
                            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlCyear_SelectedIndexChanged" ID="ddlPhase" runat="server">
                                <asp:ListItem Value="1">One</asp:ListItem>
                                <asp:ListItem Value="2">Two</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>Product Group :</td>
                        <td>
                            <asp:DropDownList AutoPostBack="true" ID="DDLProductGroupCode" OnSelectedIndexChanged="DDLProductGroupCode_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>Product :</td>
                        <td>
                            <asp:DropDownList AutoPostBack="true" ID="DDLProductCode" OnSelectedIndexChanged="DDLProductCode_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>Level :</td>
                        <td>
                            <asp:DropDownList AutoPostBack="true" ID="DDLLevel" OnSelectedIndexChanged="DDLLevel_SelectedIndexChanged" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>Session :</td>
                        <td>
                            <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlSessionNo_SelectedIndexChanged" ID="ddlSessionNo" runat="server">
                                <asp:ListItem Value="0">All</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;&nbsp; 
                     <asp:Button ID="btnChildrenDetail" runat="server" Visible="false" OnClick="btnExport_Click" Text="Export Contacts" />
                            &nbsp; 
                     <asp:Button ID="btnChildrenDetWebex" runat="server" Visible="false" OnClick="btnExportWebex_Click" Text="Export Webex Format" />

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="TblSearchString" visible="false" runat="server" width="100%">
                    <tr>
                        <td>
                            <asp:Label ID="txtSearchCriteria" runat="server" Visible="False" ForeColor="blue">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tblMessage" visible="false" runat="server" width="100%">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lblMsg" runat="server" Visible="False" ForeColor="blue"></asp:Label>
                            <asp:Label ID="lblCoachMsg" ForeColor="Red" runat="server" Visible="false"></asp:Label>
                            <br />
                            <p></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table id="tblviewchldgrid" border="1" runat="server" visible="false">
                    <tr>
                        <td>
                            <asp:Label ID="lblCoaching" runat="server" Text="Contact List" ForeColor="Green"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GVCoaching" runat="server" AutoGenerateColumns="False"
                                CellPadding="4" OnRowCommand="GVCoaching_RowCommand" ForeColor="#333333" GridLines="None" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                                BorderColor="#336666">



                                <AlternatingRowStyle BackColor="WhiteSmoke" Font-Size="small"></AlternatingRowStyle>
                                <RowStyle BackColor="white" Wrap="False" Font-Size="Small"></RowStyle>
                                <HeaderStyle BorderStyle="Solid" Font-Bold="True" BackColor="White" ForeColor="Black"></HeaderStyle>
                                <FooterStyle BackColor="Gainsboro"></FooterStyle>

                                <%--  
                        <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#FFFBD6" ForeColor="Black" Font-Bold="True"  ></HeaderStyle>
                       <AlternatingRowStyle BackColor="White" />
                       <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />--%>
                                <Columns>
                                    <%-- <asp:BoundField HeaderText="UUID" ItemStyle-HorizontalAlign ="center" DataField="UUID" HeaderStyle-Font-Bold="true" />--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            Ser#
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSRNO" runat="server"
                                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="CoachName" HeaderStyle-Font-Bold="true" DataField="CoacherName" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="ChildName" HeaderStyle-Font-Bold="true" DataField="ChildName" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Email" DataField="Email" HeaderStyle-Font-Bold="true" />
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Parent_Name" DataField="FatherName" HeaderStyle-Font-Bold="true" />
                                    <%--<asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="JobTitle" DataField="JobTitle" HeaderStyle-Font-Bold="true"  />--%>
                                    <asp:BoundField HeaderText="Grade" DataField="Grade" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Approved" DataField="approved" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="HPhone" DataField="HPhone" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="CPhone" DataField="CPhone" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center" />
                                    <%-- <asp:BoundField HeaderText="Address1" DataField="Address1" HeaderStyle-Font-Bold="true"  />--%>
                                    <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="City" DataField="City" HeaderStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="State" DataField="State" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="center" />
                                    <%--  <asp:BoundField HeaderText="Zip" DataField="zip" HeaderStyle-Font-Bold="true"  />--%>
                                    <asp:BoundField HeaderText="Product_Name" DataField="ProductName" HeaderStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="CoachName" DataField="CoachName" Visible="false" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-Font-Bold="true" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Session" DataField="SessionNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="MaxCapacity" DataField="MaxCapacity" Visible="false" />
                                    <asp:BoundField HeaderText="CoachDay" DataField="Day" />
                                    <asp:BoundField HeaderText="Time" DataField="Time" />
                                    <asp:BoundField HeaderText="PaymentReference" DataField="PaymentReference" Visible="false" />
                                    <asp:BoundField HeaderText="PaymentDate" DataField="PaymentDate" Visible="true" DataFormatString="{0:d}" HeaderStyle-Font-Bold="true" />
                                    <asp:BoundField HeaderText="Status" DataField="Status" Visible="false" />
                                    <asp:BoundField HeaderText="ChildNumber" DataField="ChildNumber" Visible="false" />
                                    <asp:BoundField HeaderText="CoachRegID" DataField="CoachRegID" Visible="false" />
                                    <asp:BoundField HeaderText="SignUpID" DataField="SignUpID" Visible="false" />
                                    <asp:BoundField HeaderText="UserId" DataField="childEmail" Visible="true" HeaderStyle-Font-Bold="true" />
                                    <%-- <asp:BoundField HeaderText="Pwd" DataField="pwd" Visible="true" HeaderStyle-Font-Bold="true" />--%>
                                    <asp:TemplateField HeaderText="meeting URL">

                                        <ItemTemplate>

                                            <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>


                                            <%-- <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length)) %></asp:HyperLink>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="LblMeetingURL" runat="server" Text='<%# Eval("AttendeeJoinURL").ToString() %>'></asp:Label>
                                            <asp:Label ID="LblOnlineClassEmail" runat="server" Text='<%# Eval("OnlineClassEmail").ToString() %>'></asp:Label>
                                            <div style="display: none;">
                                                <asp:Label ID="lblWebExID" runat="server" Text='<%# Eval("UserID").ToString() %>'></asp:Label>
                                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%# Eval("PWD").ToString() %>'></asp:Label>
                                                <asp:Label ID="lblSessionKey" runat="server" Text='<%# Eval("MeetingKey").ToString() %>'></asp:Label>
                                                <asp:Label ID="lblPrdCode" runat="server" Text='<%# Eval("ProductCode").ToString() %>'></asp:Label>
                                                <asp:Label ID="lblCoachname" runat="server" Text='<%# Eval("CoachName").ToString() %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                <PagerStyle Wrap="False"></PagerStyle>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDistList" runat="server" Text="Distribution List" ForeColor="Green" Visible="false"></asp:Label></td>
                    </tr>
                    <%--<tr>
               <td>
              <asp:GridView ID="GVDistributionList" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" >
                            <AlternatingRowStyle backcolor="WhiteSmoke" font-size="small"></AlternatingRowStyle>
                           <RowStyle backcolor="white" Wrap="False" Font-Size="Small" ></RowStyle>
                           <HeaderStyle  BorderStyle="Solid" Font-Bold="True" backcolor="White" ForeColor="Black"></HeaderStyle>
                           <FooterStyle BackColor="Gainsboro" ></FooterStyle>
                    <Columns>
                           <asp:BoundField HeaderText="DUID" ItemStyle-HorizontalAlign ="center" DataField="DUID" HeaderStyle-Font-Bold="true"  />
                          <%-- <asp:BoundField HeaderText="SNo" ItemStyle-HorizontalAlign ="center" DataField="SNo" HeaderStyle-Font-Bold="true"  Visible="false"  />- -%> 
                          <asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="Distribution list name"   DataField="ListName" HeaderStyle-Font-Bold="true" />
                           <asp:BoundField HeaderText="Distribution list Description" DataField="ListDescription" HeaderStyle-Font-Bold="true"  />
                           <asp:BoundField HeaderText="Members" DataField="Members" HeaderStyle-Font-Bold="true"  />
                           
                         </Columns>
               </asp:GridView>
               </td>
               </tr>--%>
                </table>
                <table id="tblVolRoleResults" visible="false" runat="server" width="100%">
                    <tr>
                        <td align="left">
                            <strong>
                                <asp:Label ID="lblGrdVolResultsHdg" Visible="true" runat="server"></asp:Label>
                                <br />
                                <asp:Label ID="lblUpdateError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btn_ShowUniqueRecords" Width="180px" runat="server" Text="Show Unique Records" Visible="false"></asp:Button>
                            <asp:Button ID="btn_ExportUniqueList" Width="180px" runat="server" Text="Export Unique Contact List" Visible="false"></asp:Button>
                            <asp:Button ID="btnExport2" Visible="false" Width="180px" runat="server" Text="Export  Addresses to Excel"></asp:Button>
                            <asp:Button ID="btnExport1" Width="180px" runat="server" Text="Export Contacts to Excel"></asp:Button>
                            <asp:Button ID="btnEmailExport" runat="server" Text="Export Emails"></asp:Button>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:Panel ID="Panel_Unique" runat="server">
                                <asp:DataGrid ID="DG_Unique" runat="server" CssClass="announcement_text" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="MemberID"
                                    Height="14px" GridLines="Both" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                                    BorderColor="#336666" AllowPaging="true" PageSize="50" PagerStyle-Mode="NumericPages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small">
                                    <AlternatingItemStyle BackColor="WhiteSmoke" Font-Size="small"></AlternatingItemStyle>
                                    <ItemStyle BackColor="white" Wrap="False" Font-Size="Small"></ItemStyle>
                                    <HeaderStyle BorderStyle="Solid" Font-Size="Small" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
                                    <FooterStyle BackColor="Gainsboro"></FooterStyle>
                                    <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                                    <Columns>
                                        <asp:BoundColumn DataField="memberID" HeaderText="Member Id" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="name" HeaderText="Member Name" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <%--<asp:TemplateColumn HeaderText="RoleCode" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Selection")%>'></asp:Label>
                                    <asp:HiddenField ID="hfRoleId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.RoleId")%>' />
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                                        <asp:BoundColumn DataField="Email" HeaderText="Email" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn ItemStyle-Wrap="false" DataField="HPhone" HeaderText="Home Phone" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn ItemStyle-Wrap="false" DataField="CPhone" HeaderText="Cell Phone" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <%-- <asp:TemplateColumn HeaderText="TL" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblTeamLead" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeamLead")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                                        <asp:TemplateColumn HeaderText="Event" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEventCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--<asp:TemplateColumn HeaderText="Year" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                                        <%--<asp:TemplateColumn HeaderText="Product Group" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblProductGroupId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>'></asp:Label>
                                    <asp:Label ID="lblProductGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >
                              <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="10%">
                                 <ItemTemplate >
                                    <asp:Label ID="lblProductId" visible=false runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>'></asp:Label>
                                    <asp:Label ID="lblProduct" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                 </ItemTemplate>
                              </asp:TemplateColumn >--%>
                                        <asp:TemplateColumn HeaderText="State" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.State")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Chapter" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="YahooGroup" Visible="false" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblYahooGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.YahooGroup")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn ItemStyle-Wrap="false" DataField="WPhone" HeaderText="Work Phone" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <%--<asp:BoundColumn DataField="national"  HeaderText="National" readonly="true" Visible="false" ></asp:BoundColumn>
                              <asp:BoundColumn DataField="finals" HeaderText="finals" readonly="true"  Visible="false" ></asp:BoundColumn>--%>
                                        <asp:BoundColumn DataField="zoneId" HeaderText="zoneId" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="clusterId" HeaderText="clusterId" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="chapterId" HeaderText="Chapter Id" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="eventId" HeaderText="eventId" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="indiaChapter" HeaderText="India Chapter" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                                &nbsp;
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
                        <td colspan="2" class="ContentSubTitle" align="center" style="height: 103px">
                            <asp:Panel ID="Panel1" runat="server">
                                <asp:DataGrid ID="grdVolResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="volunteerid"
                                    Height="14px" GridLines="Both" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
                                    BorderColor="#336666" AllowPaging="true" PageSize="50" PagerStyle-Mode="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small">
                                    <AlternatingItemStyle BackColor="WhiteSmoke" Font-Size="small"></AlternatingItemStyle>
                                    <ItemStyle BackColor="white" Wrap="False" Font-Size="Small"></ItemStyle>
                                    <HeaderStyle BorderStyle="Solid" Font-Size="Small" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
                                    <FooterStyle BackColor="Gainsboro"></FooterStyle>
                                    <%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                                    <Columns>
                                        <asp:BoundColumn DataField="memberID" HeaderText="Member Id" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="name" HeaderText="Member Name" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="RoleCode" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Selection")%>'></asp:Label>
                                                <asp:HiddenField ID="hfRoleId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.RoleId")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Email" HeaderText="Email" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn ItemStyle-Wrap="false" DataField="HPhone" HeaderText="Home Phone" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn ItemStyle-Wrap="false" DataField="CPhone" HeaderText="Cell Phone" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="TL" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTeamLead" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeamLead")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Event" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEventCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Year" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Product Group" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductGroupId" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>'></asp:Label>
                                                <asp:Label ID="lblProductGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductId" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>'></asp:Label>
                                                <asp:Label ID="lblProduct" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="State" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.State")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Chapter" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="YahooGroup" Visible="false" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblYahooGroup" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.YahooGroup")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn ItemStyle-Wrap="false" DataField="WPhone" HeaderText="Work Phone" ReadOnly="true" Visible="true"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="national" HeaderText="National" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="finals" HeaderText="finals" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="zoneId" HeaderText="zoneId" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="clusterId" HeaderText="clusterId" ReadOnly="true" Visible="false"></asp:BoundColumn>

                                        <asp:BoundColumn DataField="chapterId" HeaderText="Chapter Id" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="eventId" HeaderText="eventId" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="indiaChapter" HeaderText="India Chapter" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                                &nbsp;
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
</asp:Content>

