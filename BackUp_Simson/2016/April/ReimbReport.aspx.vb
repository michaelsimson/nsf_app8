Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization
Imports Microsoft.ApplicationBlocks.Data

Partial Class Reports_ReimbReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadchapter()
            If Session("RoleId").ToString() = "5" Then
                Dim i As Integer
                'dgExpense.Columns(1).Visible = False
                For i = 8 To 18
                    dgExpense.Columns(i).Visible = False
                Next
                DGRevenue.Columns(11).Visible = False
                DGRevenue.Columns(12).Visible = False
                For i = 15 To 17
                    DGRevenue.Columns(i).Visible = False
                Next
            End If
            If Len(Session("LoginChapterID")) > 0 And Val(Session("RoleId")) > 5 Then
                ddlChapter.SelectedValue = Session("LoginChapterID")
                ddlChapter.Enabled = False
                getreportdate()
            End If
        End If
    End Sub
    Private Sub loadchapter()
        Dim dsEvents As New DataSet
        Dim liNull As New ListItem("Select A Chapter", "0")
        Dim strSQl As String = "select chapterId, Name, chapterCode from chapter order by state,chapterCode"
        Try
            dsEvents = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
        Catch se As SqlException
            lblerr.Text = se.ToString()
            Return
        End Try
        If dsEvents.Tables.Count > 0 Then
            ddlChapter.Items.Clear()
            ddlChapter.DataSource = dsEvents.Tables(0)
            ddlChapter.DataTextField = dsEvents.Tables(0).Columns("chapterCode").ToString
            ddlChapter.DataValueField = dsEvents.Tables(0).Columns("ChapterId").ToString
            ddlChapter.DataBind()
            ddlChapter.Items.Insert(0, liNull)
            If (Session("RoleId").ToString() = "5") Then
                ddlChapter.Items.FindByValue(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select ChapterID from Volunteer where roleID=5 AND  memberid=" & Session("LoginID") & IIf(Session("SelChapterID") = "", "", " and ChapterID=" & Session("SelChapterID")))).Selected = True
                getreportdate()
                ddlChapter.Enabled = False
            Else
                ddlChapter.Items.FindByValue("0").Selected = True
            End If
        End If
    End Sub

    Private Sub getreportdate()
        Try
            Dim flag As Boolean = False
            Dim StrSql As String = "SELECT DISTINCT CAST(Reportdate AS date) ,CONVERT(VARCHAR(10), CAST(Reportdate AS date), 101) as ReportDate  FROM ExpJournal  WHERE (ChapterID = " & ddlChapter.SelectedValue & " and EventID=" & IIf(ddlChapter.SelectedValue = 1, 1, 2) & ") AND (DATEDIFF(year, GETDATE(), ReportDate) < 10) GROUP By CAST(Reportdate AS date) Order by CAST(Reportdate AS date) DESC"
            Dim dsReportDate As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSql)
            If dsReportDate.Tables(0).Rows.Count > 0 Then
                lstReports.DataSource = dsReportDate
                lstReports.DataTextField = "ReportDate"
                lstReports.DataValueField = "ReportDate"
                lstReports.DataBind()
                lstReports.Items.Insert(0, New ListItem("Select Date", "-1"))
                lstReports.SelectedIndex = 0
                trreportdate.Visible = True
                btnReport.Enabled = True
                lblerr.Text = ""
            Else
                lblerr.Text = "No report Date for Selected Chapter"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
   
    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clearlstReport()
        getreportdate()
    End Sub

    Private Sub clearlstReport()
        lstReports.Items.Clear()
        trreportdate.Visible = False
        btnReport.Enabled = False
        btnRevExport.Visible = False
        btnExport.Visible = False
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        loadgrid(dgExpense, "E")
        If dgExpense.Rows.Count > 0 Then
            lblExp.Text = "Expense Details :"
            btnExport.Visible = True
        Else
            btnExport.Visible = False
            lblExp.Text = "No Expense Record found for selected report Date " '& dgExpense.Rows.Count
        End If
        loadgrid(DGRevenue, "R")
        If DGRevenue.Rows.Count > 0 Then
            lblRev.Text = "Revenue Details :"
            btnRevExport.Visible = True
        Else
            btnRevExport.Visible = False
            lblRev.Text = "No Revenue Record found for selected report Date"
        End If
    End Sub

    Protected Sub dgExpense_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        loadgrid(dgExpense, "E")
        dgExpense.PageIndex = e.NewPageIndex
        dgExpense.DataBind()
    End Sub
    Protected Sub dgRevenue_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        loadgrid(DGRevenue, "R")
        DGRevenue.PageIndex = e.NewPageIndex
        DGRevenue.DataBind()
    End Sub

    Function reportdates() As String
        Dim selectedreportdates As String = ""
        Dim i As Integer
        For i = 1 To lstReports.Items.Count - 1
            If lstReports.Items(i).Selected = True Then
                'Response.Write(lstReports.Items(i).Text.ToString() & "<BR>")
                If selectedreportdates = "" Then
                    selectedreportdates = "'" & lstReports.Items(i).Text & "'"
                Else
                    selectedreportdates = selectedreportdates & ",'" & lstReports.Items(i).Text & "'"
                End If
            End If
        Next
        Return selectedreportdates
    End Function

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=ReimbReportsExpense.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexportE As New GridView
        Try
            If reportdates() <> "" And reportdates() <> "Select Date" Then
                lblerr.Text = ""
                Dim dsExpJrnl As New DataSet
                Dim tblExpJrnl() As String = {"ExpJrnl"}
                Dim StrSQL As String
                If Session("RoleId").ToString() = "5" Then
                    StrSQL = "SELECT I.FirstName + ' ' + I.Lastname as IncurredBy,E.ExpCatCode,T.TreatDesc ,E.ReportDate,E.DateIncurred,E.ExpenseAmount as Amount,E.Providername as VendorName,E.ChapterApprovalFlag as ChApproval,E.Comments,Ev.Name As Event,Ch.ChapterCode as FromChapter, C.Chaptercode as ToChapter,R.FirstName + ' ' + R.Lastname as ReimburseTo, T.TreatCode FROM ExpJournal E INNER JOIN INDSPOUSE I ON I.AutoMemberID=E.IncMemberID INNER JOIN INDSPOUSE R ON R.AutoMemberID=E.ReimbMemberID INNER JOIN Treatment T ON T.TreatID=E.TreatID INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter C ON C.ChapterID = E.ToChapterID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType IN ('ChapterExp','FinalsExp') and E.ChapterID =" & ddlChapter.SelectedItem.Value & " and E.ReportDate in (" & reportdates() & ") order by E.reportdate"
                Else
                    StrSQL = "SELECT I.FirstName + ' ' + I.Lastname as IncurredBy,E.ExpCatCode,T.TreatDesc ,E.ReportDate,E.DateIncurred,E.ExpenseAmount as Amount,E.Providername as VendorName,E.ChapterApprovalFlag as ChApproval,E.Comments,E.ChapterApprover as ChApprover,E.NationalApprover as NatApprover,E.NationalApprovalFlag As NatApproval,E.Paid,E.DatePaid,E.ExpCatID,E.Account,E.ReimbMemberID,Ev.Name as Event,Ch.ChapterCode as FromChapter, C.Chaptercode as ToChapter,R.FirstName + ' ' + R.Lastname as ReimburseTo, T.TreatCode FROM ExpJournal E INNER JOIN INDSPOUSE I ON I.AutoMemberID=E.IncMemberID INNER JOIN INDSPOUSE R ON R.AutoMemberID=E.ReimbMemberID INNER JOIN Treatment T ON T.TreatID=E.TreatID INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter C ON C.ChapterID = E.ToChapterID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType IN ('ChapterExp','FinalsExp') and E.ChapterID =" & ddlChapter.SelectedItem.Value & " and E.ReportDate in (" & reportdates() & ") order by E.reportdate"
                End If
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dgexportE.DataSource = dsExpJrnl
                dgexportE.DataBind()
            Else
                lblerr.Text = "Report date is Not feeded"
            End If
        Catch ex As Exception
            lblerr.Text = reportdates()
            lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try

        dgexportE.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub btnExportRev_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=ReimbReportsRevenue.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexportE As New GridView
        Dim reportdate As String = reportdates()
        Try

            If reportdate <> "" And reportdate <> "Select Date" Then
                lblerr.Text = ""
                Dim dsExpJrnl As New DataSet
                Dim tblExpJrnl() As String = {"ExpJrnl"}
                Dim StrSQL As String
                If Session("RoleId").ToString() = "5" Then
                    StrSQL = "SELECT E.RevSource,E.RevCatCode as [From],E.SponsorCode as Given_For,E.ReportDate,E.DateIncurred,E.DonPurposeCode as DonPurpose,E.ExpenseAmount as Amount,E.FeesType,E.Comments,E.ProviderName as [Giver Name],E.SalesCatCode as SalesCat,E.CreatedDate,E.ChapterApprovalFlag as ChApproval,E.Account,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType='Revenue' and E.ChapterID =" & ddlChapter.SelectedItem.Value & "and E.ReportDate in (" & reportdate & ") order by E.reportdate"
                    'StrSQL = "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType='" & type & "' and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & "and ReportDate in (" & reportdate & ") order by reportdate"

                Else
                    StrSQL = "SELECT E.RevSource,E.RevCatCode as [From],E.SponsorCode as Given_For,E.ReportDate,E.DateIncurred,E.DonPurposeCode as DonPurpose,E.ExpenseAmount as Amount,E.FeesType,E.Comments,E.ProviderName as [Giver Name],E.SalesCatCode as SalesCat,E.PaymentMethod,E.CreatedBy,E.CreatedDate,E.ChapterApprovalFlag as ChApproval,E.ChapterApprover as ChApprover,E.NationalApprovalFlag as NatApproval,E.NationalApprover as NationalApprover,E.Account,Ev.Name as Event,Ch.ChapterCode FROM ExpJournal E  INNER JOIN EVENT Ev ON Ev.EventID=E.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = E.ChapterID where E.TransType='Revenue' and E.ChapterID =" & ddlChapter.SelectedItem.Value & "and E.ReportDate in (" & reportdate & ") order by E.reportdate"
                End If
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dgexportE.DataSource = dsExpJrnl
                dgexportE.DataBind()
            Else
                lblerr.Text = "Report date is Not feeded"
            End If
        Catch ex As Exception
            lblerr.Text = reportdate
            lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try
        dgexportE.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub loadgrid(ByVal dg As GridView, ByVal type As String)
        ' lblErr.Text = StrSQL.ToString()
        Dim reportdate As String = reportdates()
        Try
            If reportdate <> "" And reportdate <> "Select Date" Then
                lblerr.Text = ""
                Dim dsExpJrnl As New DataSet
                Dim tblExpJrnl() As String = {"ExpJrnl"}
                Dim StrSQL As String

                If type = "E" Then
                    StrSQL = "SELECT ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName,R.FirstName + ' ' + R.Lastname as ReimburseTo, T.TreatDesc,E.Name as EventName,Ch.Chaptercode as FromChapter, C.Chaptercode as ToChapter, ExpJournal.ExpCatCode, T.TreatCode"
                    StrSQL = StrSQL & " FROM ExpJournal INNER JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID Inner Join IndSpouse R on R.AutoMemberID =ExpJournal.ReimbMemberID "
                    StrSQL = StrSQL & " INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID "
                    StrSQL = StrSQL & " INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID "
                    StrSQL = StrSQL & " INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID "
                    StrSQL = StrSQL & " INNER JOIN Chapter C on C.ChapterID =ExpJournal.ToChapterID "
                    StrSQL = StrSQL & " where ExpJournal.TransType IN ('ChapterExp','FinalsExp') and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & " and ReportDate in (" & reportdate & ") order by reportdate"
                    'StrSQL = "SELECT ExpJournal.*,I.FirstName + ' ' + I.Lastname as IncName, T.TreatDesc,E.Name as EventName,Ch.Chaptercode FROM ExpJournal INNER JOIN INDSPOUSE I ON I.AutoMemberID=ExpJournal.IncMemberID INNER JOIN Treatment T ON T.TreatID=ExpJournal.TreatID INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType IN ('ChapterExp','FinalsExp') and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & " and ReportDate in (" & reportdate & ") order by reportdate"
                Else
                    StrSQL = "SELECT ExpJournal.*,E.Name as EventName,Ch.Chaptercode FROM ExpJournal  INNER JOIN EVENT E ON E.EventID=ExpJournal.EventID INNER JOIN Chapter Ch ON Ch.ChapterID = ExpJournal.ChapterID  where ExpJournal.TransType='Revenue' and ExpJournal.ChapterID =" & ddlChapter.SelectedItem.Value & " and ReportDate in (" & reportdate & ") order by reportdate"
                End If
                SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, StrSQL, dsExpJrnl, tblExpJrnl)
                dg.PageIndex = 0
                dg.DataSource = dsExpJrnl
                dg.DataBind()
            Else
                lblerr.Text = "Report date is Not feeded"
            End If
        Catch ex As Exception
            lblerr.Text = reportdate
            lblerr.Text = lblerr.Text & " <br> ******* " & ex.ToString()
        End Try
    End Sub
End Class
