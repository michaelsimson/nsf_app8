<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReimbReport.aspx.vb" Inherits="Reports_ReimbReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Reimbursement Reports</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:left">
    <br />
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl = "~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp;<br /></div>
    <div>
    <table border = "0" cellpadding = "0" width="900px" cellspacing="0"><tr><td align="center">
<table border = "0" cellpadding = "3" cellspacing="0">
<tr><td colspan = "2" align ="center" ><b> Reimbursement Report</b> </td></tr>
<tr><td>Chapter</td><td>
    <asp:DropDownList ID="ddlChapter" runat="server" AutoPostBack="true" OnSelectedIndexChanged = "ddlChapter_SelectedIndexChanged">
    </asp:DropDownList></td></tr>
<tr id="trreportdate" runat = "server"  visible="false"><td>Report Date</td><td>
    <asp:ListBox ID="lstReports" runat="server" SelectionMode="Multiple"></asp:ListBox></td></tr>
<tr><td colspan="2" align="center">
    <asp:Button ID="btnReport" runat="server" Text="Generate Report" OnClick="btnReport_Click" Enabled="false" /> 
    <asp:Button ID="btnExport" runat="server" Text="Export Expense Report" OnClick="btnExport_Click" visible = "false" /> 
    <asp:Button ID="btnRevExport" runat="server" Text="Export Revenue Report" OnClick="btnExportRev_Click" visible = "false" /> 

    
    </td></tr>
</table></td></tr></table>  
        <asp:Label ID="lblerr" runat="server"></asp:Label>  
        <br />
      <asp:Label ID="lblExp" runat="server" Text="" Font-Bold="true"  ></asp:Label>
        <br />
        <asp:GridView CssClass="SmallFont" ForeColor="Black"  ID="dgExpense" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False"  GridLines="Both" CellPadding="2" BackColor="White" BorderWidth="3px" BorderStyle="Double"
			    BorderColor="#336666" OnPageIndexChanging="dgExpense_PageIndexChanging" AllowPaging="true" PageSize="20"  PagerStyle-Mode ="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small"  AllowSorting="True">
        <Columns>          
            <asp:BoundField DataField="IncName"  HeaderText="IncurredBy" />
            <asp:BoundField DataField="ExpCatCode" HeaderText="ExpCatCode" />
            <asp:BoundField DataField="TreatDesc" HeaderText="TreatDesc" />
            <asp:BoundField DataField="ReportDate" HeaderText="ReportDate"  DataFormatString="{0:d}"/>
            <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="Providername" HeaderText="Vendor Name" />
            <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval"/>
            <asp:BoundField DataField="Comments" HeaderText="Comments" /> 
            <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover"/>
            <asp:BoundField DataField="NationalApprover" HeaderText="NatApprover"/>
            <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval"/>
            <asp:BoundField DataField="Paid" HeaderText="Paid"/>
            <asp:BoundField DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ExpCatID" HeaderText="ExpCatID" />
            <asp:BoundField DataField="Account" HeaderText="Account" />  
            <asp:BoundField DataField="TreatID" HeaderText="TreatID" />
            <asp:BoundField DataField="ReimbMemberID" HeaderText="ReMemberID"/>
            <asp:BoundField DataField="EventName" HeaderText="Event"/>
            <asp:BoundField DataField="FromChapter" HeaderText="From Chapter"/>
            <asp:BoundField DataField="ToChapter" HeaderText="ToChapter"/>
            <asp:BoundField DataField="ReimburseTo" HeaderText="ReimburseTo"/>
            <asp:BoundField DataField="TreatCode" HeaderText="TreatCode"/>
        </Columns>   
    </asp:GridView>
     <br />
        <asp:Label ID="lblRev" runat="server" Text="" Font-Bold="true" ></asp:Label>
        
        <asp:GridView ID="DGRevenue" CssClass="SmallFont" ForeColor="Black" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False"  GridLines=Both CellPadding=4 BackColor="White" BorderWidth="3px" BorderStyle="Double"
			    BorderColor="#336666"  OnPageIndexChanging="DGRevenue_PageIndexChanging" AllowPaging="true" PageSize=20  PagerStyle-Mode ="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small"  AllowSorting="True">
        <Columns>            
            <asp:BoundField DataField="RevSource"  HeaderText="RevSource" />
            <asp:BoundField DataField="RevCatCode" HeaderText="From"/>           
            <asp:BoundField DataField="SponsorCode" HeaderText="Given For" />
            <asp:BoundField DataField="ReportDate" HeaderText="ReportDate"  DataFormatString="{0:d}"/>
            <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="DonPurposeCode" HeaderText="DonPurpose"/>
            <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="FeesType" HeaderText="Fee Type"/>  
            <asp:BoundField DataField="Comments" HeaderText="Comments" /> 
            <asp:BoundField DataField="ProviderName" HeaderText="Giver's Name" /> 
            <asp:BoundField DataField="SalesCatCode" HeaderText="SalesCat"/>          
            <asp:BoundField DataField="PaymentMethod" HeaderText="Payment Method"/>
            <asp:BoundField DataField="CreatedBy" HeaderText="Created By"/>
            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date"  DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval"/>
            <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover"/>
            <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval"/>
            <asp:BoundField DataField="NationalApprover" HeaderText="NatApprover"/>
            <asp:BoundField DataField="Account" HeaderText="Account" /> 
            <asp:BoundField DataField="EventName" HeaderText="Event"/>
            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"/>       
       </Columns>
    </asp:GridView>
    </div>    
     <br />
          <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
