﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" MasterPageFile="~/NSFMasterPage.master" CodeFile="VolunteerSignupReport.aspx.cs" Inherits="VolunteerSignupReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx"
            runat="server"> Back to Volunteer Functions</asp:HyperLink>
        &nbsp&nbsp&nbsp&nbsp
         
          
    </div>

    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        <strong>Volunteer Signup Report

        </strong>
        <br />


    </div>
    <div align="center" style="margin-top: 10px;">
        <asp:Label ID="lbacces" Font-Bold="true" runat="server" Text="" ForeColor="Red"></asp:Label>

    </div>
    <br />

    <div id="IDContent" runat="server">

        <table style="margin-left: 0px;">
            <tr>
                <td style="width: 150px"></td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                    </asp:DropDownList>

                </td>

                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                <td style="width: 141px" align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                <td style="width: 141px" runat="server" align="left" id="ddchapterdrop">
                    <asp:DropDownList ID="ddchapter" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddchapter_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                </td>
                <td style="width: 141px" align="left">
                    <asp:DropDownList ID="ddTeams" runat="server" Width="155px" AutoPostBack="True" OnSelectedIndexChanged="ddTeams_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>







            </tr>

        </table>


        <table>
            <tr>
                <td></td>
                <td style="width: 50px"></td>

                <td>


                    <table>
                        <tr>
                            <td align="left" nowrap="nowrap">&nbsp;</td>
                            <td style="width: 141px" align="left">&nbsp;</td>
                        </tr>
                        <%-- <tr>
        <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product Group</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddproductgroup" runat="server" Width="150px" 
                    AutoPostBack="True" OnSelectedIndexChanged="ddproductgroup_SelectedIndexChanged" 
                    >
                    
                </asp:DropDownList>
            </td>
</tr>--%>
                        <%--     <tr>
         <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddproduct" runat="server" Width="150px" 
                    OnSelectedIndexChanged="ddproduct_SelectedIndexChanged" AutoPostBack="True" 
                    >
                    
                </asp:DropDownList>
            </td></tr>--%>
                    </table>

                </td>


            </tr>
        </table>

        <%--  </div>--%>


        <div id="Div1" align="center" runat="server">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:Button ID="btnExcel" runat="server" Visible="false" Text="Export to Excel" OnClick="btnExcel_Click" />
            <br />
            <br />

            <b>
                <div id="Div2" align="center" runat="server">
                    <asp:Label ID="Label1" Font-Bold="true" runat="server" Text=""></asp:Label>
            </b>
            <asp:Label ID="Label2" Font-Bold="true" runat="server" Text=""></asp:Label>
            <div id="Div4" align="center" runat="server" style='width: 1000px; overflow: auto;'>
                <asp:GridView ID="GvVolsignup" runat="server" DataKeyNames="VolsignupId"
                    AutoGenerateColumns="False" OnRowEditing="GvVolsignup_RowEditing" OnRowUpdating="GvVolsignup_RowUpdating" OnRowCancelingEdit="GvVolsignup_RowCancelingEdit1" EnableModelValidation="True" AllowPaging="True" OnPageIndexChanging="GvVolsignup_PageIndexChanging" PageSize="20">

                    <Columns>
                        <asp:TemplateField>

                            <ItemTemplate>
                                <asp:Button ID="btnEdit" Text="Modify" runat="server" CommandName="Edit" />

                            </ItemTemplate>

                            <EditItemTemplate>
                                <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" BackColor="SkyBlue" />

                                <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" BackColor="SkyBlue" />

                            </EditItemTemplate>


                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="MemberID">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblmemberId" Text='<%#Eval("MemberID") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="FirstName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="LastName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblastname" Text='<%#Eval("LastName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEmail" Text='<%#Eval("Email") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Hphone">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblhphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("hphone") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Cphone">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblcphone" Style="width: 80px; display: inline-block; text-align: center;" Text='<%#Eval("Cphone") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="TeamId">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="Team">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("TeamName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Year">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Eventname">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEventName" Style="width: 120px; display: inline-block; text-align: left;" Text='<%#Eval("Eventname") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ChapterName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblchapterName" Text='<%#Eval("ChapterName") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="ProductGroup">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproductGroup" Text='<%#Eval("ProductGroup") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HdnProductgroup" Value='<%# Bind("ProductGroupId")%>' runat="server" />
                                <asp:DropDownList ID="productGroup" runat="server" Width="120px" AutoPostBack="True" OnPreRender="DDproductGroupEdit" OnSelectedIndexChanged="ddday_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" Visible="false" ID="TxtprodGroup" Text='<%#Eval("ProductGroup") %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                        <asp:TemplateField HeaderText="Product">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproduct" Text='<%#Eval("product") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HdnProduct" Value='<%# Bind("ProductId")%>' runat="server" />
                                <asp:DropDownList ID="product" runat="server" Width="120px" AutoPostBack="false" OnPreRender="DDproduct">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" Visible="false" ID="TxtProduct" Text='<%#Eval("product") %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AvailHours">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:HiddenField ID="HdnAvailhrs" Value='<%# Bind("AvailHours")%>' runat="server" />
                                <asp:DropDownList ID="ddavailhrs" runat="server" Width="120px" AutoPostBack="false" OnPreRender="DDAvailhrs">
                                </asp:DropDownList>
                                <asp:TextBox runat="server" Visible="false" ID="Txthrs" Text='<%#Eval("AvailHours") %>' />
                            </EditItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="City">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>



                    </Columns>


                </asp:GridView>
            </div>
            <br />
            <asp:Label ID="lbnorec1" runat="server" ForeColor="Red"></asp:Label>
        </div>
    </div>

    <div id="Div3" align="center" runat="server">
        <br />
        <asp:Label ID="lblRoles" Font-Bold="true" runat="server" Text=""></asp:Label>
        </b>
   <asp:GridView ID="GridView1" runat="server" ItemStyle-HorizontalAlign="left"></asp:GridView>

        <br />
        <asp:Label ID="lbnorec" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>

    <asp:Label ID="lbevent" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbPrd" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lbEventId" runat="server" Visible="false"></asp:Label>
    </div> 
     <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="Label4" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
</asp:Content>
