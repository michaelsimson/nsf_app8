<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master"  AutoEventWireup="false" CodeFile="AddDonation.aspx.vb" Inherits="AddDonation" title="Untitled Page"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">    
    
    <script language="javascript" type="text/javascript">
        function numeric()
          {   
		  if((event.keyCode > 64 && event.keyCode<91) || (event.keyCode > 96 && event.keyCode<123)||(event.keyCode >= 32 && event.keyCode<=45) || (event.keyCode >= 58 && event.keyCode<=64) ||(event.keyCode >= 91 && event.keyCode<=96) ||(event.keyCode >= 123 && event.keyCode<=126)||(event.keyCode==47) ) 
			{
			event.keyCode=0;
			}
			else	
			 {
			 event.keyCode=event.keyCode;
			 }
        }        
    </script>
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
			    <tr bgcolor="#FFFFFF" align="middle" >
					<td colspan="2" class="Heading" align="middle">Add New Donation</td>
				</tr>			    
				<tr bgcolor="#FFFFFF" > 
				    <td colspan="2"><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/donorfunctions.aspx">Back to Donor Functions Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;
				    <asp:hyperlink id="hlnkSearch" runat="server" NavigateUrl="~/dbsearchresults.aspx">Back to Search Results</asp:hyperlink>
				    </td>
				</tr>
				<tr><td width="50%">
						<table id="tblIndividual" align="center" style="width: 871px">
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td  align="left" >Donation Amount:</td>
			        <td style="width: 320px;" ><asp:TextBox CssClass="inputBox" runat="server" ID="txtAmount" MaxLength="50" Width="92px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Amount" ControlToValidate="txtAmount" Width="87px"></asp:RequiredFieldValidator>
                        </td><td style="width: 200px">
                        <asp:Label ID="lbldonationtype" runat="server" Text="Donation Type: " Width="180px"></asp:Label></td>
                        <td>
                        <asp:DropDownList ID="drpdonationtype" runat="server" Width="120px" >
                        <asp:ListItem Value="1" Text="Unrestricted" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Perm Restricted"></asp:ListItem></asp:DropDownList></td>
			    </tr>
                <tr bgcolor="#FFFFFF" style="height: 25px">
                    <td >
                        Check/Transaction Number:</td>
                    <td style="width: 320px" >
                        <asp:TextBox CssClass="inputBox" ID="txtTransactionNo" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtTransactionNo"
                            ErrorMessage="Enter Data" Width="66px"></asp:RequiredFieldValidator></td><td style="width: 121px"><asp:Label ID="lblchapterid" runat="server" Text="ChapterId: " Width="75px"></asp:Label>
                       </td><td><asp:DropDownList ID="drpchapterid" runat="server"  Width="120px"></asp:DropDownList>
                        </td> 
                </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td >Donation Date:</td>
			        <td style="width: 320px;" ><asp:TextBox CssClass="inputBox" runat="server" ID="txtDonationDate" MaxLength="50" Width="110px"></asp:TextBox>
			        <asp:Label ID="lbldate" runat="server" Text="(MM/DD/YYYY)" ForeColor="Red"></asp:Label>
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDonationDate"
                            ErrorMessage="Enter Date" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDonationDate"
                            ErrorMessage="Enter Date" Width="66px"></asp:RequiredFieldValidator></td>--%>
                            <td style="width: 121px" >
                        Method:</td>
			        <td style="width: 277px; "><asp:DropDownList CssClass="inputBox" runat="server" id="ddlMethod"  Width="120px">
			            <asp:ListItem Text="Check" Value="Check"></asp:ListItem>
			            <asp:ListItem Text="Credit Card" Value="Credit Card"></asp:ListItem>
			            <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
			        </asp:DropDownList>
			        </td>
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			       <td >Anonymous:</td>
			        <td style="width: 320px;" ><asp:DropDownList CssClass="inputBox" runat="server" id="ddlAnonymous">			            
			            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			            <asp:ListItem Text="No" Value="No" Selected></asp:ListItem>
			        </asp:DropDownList>
			      <td style="width: 200px">  <asp:Label ID="lbldsn" runat="server" Text="Deposit Slip#" Width="180px"></asp:Label> 
			             
			       </td><td><asp:TextBox ID="txtdsn" runat="server" ></asp:TextBox>
                       &nbsp;<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtdsn"
                            ErrorMessage="Enter Deposit Slip Number"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtddate"
                            ErrorMessage="Enter Deposit Date"></asp:RequiredFieldValidator>--%>
			       <asp:Label ID="lblerr2" runat="server" ForeColor="red"></asp:Label></td>
			    </tr>
			   <tr bgcolor="#FFFFFF" style="height: 25px">
			       <td>
			       <asp:Label ID="lblddate" runat="server" Text="Deposit Date:"></asp:Label> 
			       </td>
			       <td  style="width: 320px">
			       <asp:TextBox ID="txtddate" runat="server" Width="100px" ></asp:TextBox>&nbsp;
			       <asp:Label ID="lblerr" runat="server" ForeColor="red" Text="(MM/DD/YYYY)"></asp:Label>&nbsp;
                     
                           <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtddate"
                            ErrorMessage="Enter Date(MM/DD/YYYY)" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtddate"
                            ErrorMessage="Enter Deposit Date"></asp:RequiredFieldValidator>--%></td>
                            <td >
                Bank :
            </td>
            <td style="width: 320px" >
            <asp:DropDownList ID="ddlBankId" DataTextField="BankCode" DataValueField ="BankID" runat="server"   Width="120px"/>
                       
            </td> 
                            
			    </tr>
			    <tr><td>
			  <asp:Label ID="lblProject" runat="server" Text="Project:"></asp:Label> 
			       </td>
			       <td > 
			       <asp:TextBox ID="txtProject" runat="server" Width="100px" ></asp:TextBox>&nbsp;
			       <asp:Label ID="lblErrPrjt" runat="server" ForeColor="red"></asp:Label>&nbsp;</td>
                    <td style="width: 200px" >Donation Event:</td>
                  
			        <td style="width: 277px" ><asp:DropDownList CssClass="inputBox" runat="server" id="ddlDonationEvent"  Width="180px">			            
			        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlDonationEvent"
                            ErrorMessage="Select Event"></asp:RequiredFieldValidator></td>
			   </tr>
			    
			 
			     
			    <tr bgcolor="#FFFFFF" style="height: 25px" >
			       <td >
                       Purpose:</td>
			        <td style="width: 320px;" ><asp:DropDownList CssClass="inputBox" runat="server" id="ddlPurpose" Width="190px">
			            
			        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Select Purpose" ControlToValidate="ddlPurpose"></asp:RequiredFieldValidator></td>
			    <%--</tr>
			    <tr bgcolor="#FFFFFF">--%>
			     <td style="width: 100px" >Supp Code:</td>
			        <td style="width: 320px" ><asp:DropDownList CssClass="inputBox" runat="server" id="DDSuppCode"  Width="180px">			            
			             <asp:ListItem Value="0" Selected="True">Select SuppCode</asp:ListItem>
                                <asp:ListItem>Advertising</asp:ListItem>
                                <asp:ListItem>Award</asp:ListItem>
                                <asp:ListItem>Employee Gift</asp:ListItem>
                                 <asp:ListItem>Matching Gift</asp:ListItem>
                                 <asp:ListItem>Sponsorship</asp:ListItem>
                                <asp:ListItem>Volunteer Hours</asp:ListItem>
                                 <asp:ListItem>Other</asp:ListItem>
                                  <asp:ListItem>None</asp:ListItem>
			        </asp:DropDownList></td>
			    </tr>
               
			    <tr   runat = "server" id="TrEndowment" bgcolor="#FFFFFF" style="height: 25px">
			       <td >
                       Endowment:</td>
			        <td style="width: 320px" ><asp:DropDownList CssClass="inputBox"  runat="server" id="ddlEndowment" Width="190px">
			            
			        </asp:DropDownList></td>
                    <td style="width: 200px" >In Memory Of:</td>
			        <td style="width: 277px" >
			            <asp:TextBox CssClass="inputBox" runat="server" ID="txtMemoryOf" MaxLength="50" Width="250px"></asp:TextBox>
			        </td>
            
			    </tr>
			  
               
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td >
                        Status:</td>
			        <td style="width: 320px" >
			            <asp:DropDownList CssClass="inputBox" runat="server" ID="ddlStatus" Width="110px">
			                <asp:ListItem Text="Select Status" Value=""></asp:ListItem>
			                <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
			                <asp:ListItem Text="Completed" Value="Completed"></asp:ListItem>
			            </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="ddlStatus" runat="server" ErrorMessage="Select Status"></asp:RequiredFieldValidator></td>
  <td style="width: 200px" >Matching Employer:</td>
			        <td style="width: 281px">
			            <!--<asp:TextBox  CssClass="inputBox" runat="server" ID="txtEmployer" MaxLength="50"></asp:TextBox>-->
			            <asp:DropDownList runat="server" ID="ddlEmployer" CssClass="inputBox"  Width="260px">
			                
			            </asp:DropDownList>
			            
                        </td>
                </tr>
			    <tr  runat = "server" id="Trmatch"  bgcolor="#FFFFFF" style="height: 25px">
			        <td >Matching Flag:</td>
			        <td style="width: 320px" >
			            <asp:DropDownList CssClass="inputBox" runat="server" ID="ddlMatchingFlag">
			                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
			                <asp:ListItem Text="No" Value="No" Selected=True></asp:ListItem>
			            </asp:DropDownList>
			            <asp:Label ID="lblerr3" runat="server" ForeColor="red"></asp:Label>
			        </td>
                      <td>
			      <asp:Label ID="lblEventYear" runat="server" Text="EventYear:"></asp:Label></td>
			      <td>
			        <asp:DropDownList runat="server" ID="ddlEventYear" CssClass="inputBox"  Width="130px">
			                
			            </asp:DropDownList>
			      </td>
			   <%-- </tr>
			    <tr bgcolor="#FFFFFF" style="height: 40px">
			   --%>     
			    </tr>
			    <tr bgcolor="#FFFFFF" style="height: 25px">
			        <td >
                        Remarks:</td>
			        <td style="width: 320px" >
			            <asp:TextBox CssClass="inputBox" runat="server"  TextMode="MultiLine"  ID="txtRemarks" MaxLength="150"></asp:TextBox>
			        </td>
			       
			    </tr>
                  
			    <tr  runat = "server" id="TrEduGame" bgcolor="#FFFFFF" style="height: 25px">
            <td>
                <asp:Label ID="lblDdlGame" runat="server" Text="Education Game:"></asp:Label>
            </td>
            <td style="width: 320px" >
                <asp:DropDownList runat="server" ID="dllEGames" Width="110px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredfieldvalidatorGame" runat="server" 
                    ControlToValidate="dllEGames" ErrorMessage="Select Education Games"
                    CssClass="mediumwording"></asp:RequiredFieldValidator>
            </td>
                    
        </tr>
          <tr  runat = "server" id="TrDesign"  bgcolor="#FFFFFF" style="height: 25px" >
			       <td >
                       Designation:</td>
			        <td style="width: 320px" ><asp:DropDownList CssClass="inputBox" runat="server" id="ddlDesignation"  Width="110px">
			             <asp:ListItem Text="None" Value="None" Selected></asp:ListItem>
			            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
			            <asp:ListItem Text="College" Value="College"></asp:ListItem>
			            <asp:ListItem Text="High School" Value="High School"></asp:ListItem>
			            <asp:ListItem Text="Primary School" Value="Primary School"></asp:ListItem>			            
			        </asp:DropDownList></td>
                
                  
			    <%--</tr>
			    <tr bgcolor="#FFFFFF" style="height: 40px">
			    --%>   
			    </tr>
			       <tr bgcolor="#FFFFFF" style="height: 25px">
					<td class="ItemCenter" align="center" colSpan="2" style="height: 22px">
					    <asp:button id="btnContinue" runat="server" CssClass="FormButton" Text="Save" ></asp:button><br />
                        <asp:Label ID="lblError" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
					  </td>
				</tr>
				  <tr bgcolor="#FFFFFF" align="middle" >
					<td colspan="2" class="Heading" align="middle" height="15"></td>
				</tr>	
			</table>
			</td>
			</tr>
			</table> 
</asp:Content>



 
 
 