﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Web.UI;
using System.Drawing;



public partial class VolunteerSignupReport : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    string FirstandLastName;
    bool IsValueCheck = false;
    bool IsEventProd = false;
    bool IsEvent = false;
    string productgroupid1;
    string productid1;
    string productGroup1;
    string product1;
    bool Isdropval = false;
    DataSet dsCheck;
    string StrQryCheck;
    bool Isupdate = false;
    protected void DDproductGroupEdit(object sender, System.EventArgs e)
    {
        try
        {
            lbacces.Text = "";
            if ((IsValueCheck != true))
            {
                string DDLstate;
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                if (lbEventId.Text != "")
                {
                    DDLstate = "select * from ProductGroup Pg left join Event E on E.EventId=Pg.EventId  where (E.EventCode='" + lbEventId.Text + "'  or E.Name='" + lbEventId.Text + "')";
                    // DDLstate = "select distinct Name,ProductGroupId from ProductGroup where eventid='" + lbEventId.Text + "'";
                }
                else
                {
                    DDLstate = "select distinct Name,ProductGroupId from ProductGroup";
                }
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                ddlTemp.DataSource = dsstate;
                ddlTemp.DataTextField = "Name";
                ddlTemp.DataValueField = "ProductGroupId";
                ddlTemp.DataBind();
                ddlTemp.Items.Insert(0, "Select ProductGroup");
            }
            else if (IsEventProd == true)
            {
                string DDLstate;
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                if (lbEventId.Text != "")
                {
                    DDLstate = "select * from ProductGroup Pg left join Event E on E.EventId=Pg.EventId  where (E.EventCode='" + lbEventId.Text + "'  or E.Name='" + lbEventId.Text + "')";
                    // DDLstate = "select distinct Name,ProductGroupId from ProductGroup where eventid='" + lbEventId.Text + "'";
                }
                else
                {
                    DDLstate = "select distinct Name,ProductGroupId from ProductGroup";
                }
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                if (dsstate.Tables[0].Rows.Count == 0)
                {
                    lbPrd.Text = "";
                }
                ddlTemp.DataSource = dsstate;
                ddlTemp.DataTextField = "Name";
                ddlTemp.DataValueField = "ProductGroupId";
                ddlTemp.DataBind();
                ddlTemp.Items.Insert(0, "Select ProductGroup");
            }

        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }
    protected void DDproduct(object sender, System.EventArgs e)
    {
        try
        {
            string DDLstate;
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            if ((lbPrd.Text != "") && (lbPrd.Text != "Select ProductGroup"))
            {
                DDLstate = "select distinct Name,productid from Product where productGroupID=" + lbPrd.Text + "";
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                ddlTemp.DataSource = dsstate;
                ddlTemp.DataTextField = "Name";
                ddlTemp.DataValueField = "productid";
                ddlTemp.DataBind();
            }
            ddlTemp.Items.Insert(0, "Select Product");
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void DDAvailhrs(object sender, System.EventArgs e)
    {
        try
        {
            if (IsValueCheck != true)
            {
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                ArrayList list = new ArrayList();

                for (int i = 1; i <= 20; i++)
                {
                    list.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlTemp.DataSource = list;
                ddlTemp.DataTextField = "Text";
                ddlTemp.DataValueField = "Value";
                ddlTemp.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["RoleId"] = "5";
        //Session["LoginID"] = "4240";


        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Session["Panel"] != null)
        {
            if (Session["Panel"].ToString().Equals("ChapterPanel"))
            {
                if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "3") || (Session["RoleId"].ToString() == "4") || (Session["RoleId"].ToString() == "5") || (Session["RoleId"].ToString() == "46"))
                {
                    if (!IsPostBack)
                    {
                        Events();
                        Yearscount();
                        ChapterDrop(ddchapter);
                        Team();
                    }
                }
                else
                {
                    lbacces.Text = "You do not have access to this application";
                    IDContent.Visible = false;
                }
            }
        }
        //else if ((Session["RoleId"].ToString() == "97") || (Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "5"))
        //{
        //    if (!IsPostBack)
        //    {
        //        Events();
        //        Yearscount();
        //        ChapterDrop(ddchapter);
        //    }
        //}
        else if (Array.IndexOf(new string[] { "89", "96", "97", "3", "4", "5", "46", "1", "2", "71" }, Session["RoleId"].ToString()) > -1)
        {
            if (!IsPostBack)
            {
                FilleEventByRole();
                Yearscount();
                ChapterDrop(ddchapter);
                Team();
            }
        }
        else
        {
            lbacces.Text = "You do not have access to this application";
            IDContent.Visible = false;
        }
    }
    protected void Events()
    {
        try
        {

            if (Session["Panel"] != null)
            {
                if (Session["Panel"].ToString().Equals("ChapterPanel"))
                {
                    if (Convert.ToInt32(Session["mailChapterID"].ToString()) > 1)
                    {


                        if (Array.IndexOf(new string[] { "1", "2" }, Session["RoleId"].ToString()) > -1)
                        {

                            try
                            {

                                ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                                ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                                ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                                ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                                ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                                ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                                ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                                ddEvent.SelectedValue = "2";

                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else if (Array.IndexOf(new string[] { "3", "4", "5" }, Session["RoleId"].ToString()) > -1)
                        {

                            try
                            {


                                ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                                ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                                ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                                ddEvent.SelectedValue = "2";

                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    if (Convert.ToInt32(Session["mailChapterID"].ToString()) == 1)
                    {
                        ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                        ddEvent.SelectedIndex = 0;
                        ddEvent.Enabled = false;
                    }
                }
            }
            else
            {
                //ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                //ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                //ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                //ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                //ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                //ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                //ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                //ddEvent.Items.Insert(0, new ListItem("All", "-1"));
                //ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));

                FilleEventByRole();
            }
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Team()
    {

        if ((ddEvent.SelectedItem.Text != "All") && (ddEvent.SelectedItem.Text != "Select Event"))
        {
            StrQryCheck = "select * from  VolTeamMatrix where " + ddEvent.SelectedItem.Text + "='Y'";
        }
        else
        {
            StrQryCheck = "select * from  VolTeamMatrix";
        }


        DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQryCheck);
        if (dsCheck.Tables[0].Rows.Count > 0)
        {
            Isdropval = true;
            ddTeams.DataSource = dsCheck;
            ddTeams.DataTextField = "TeamName";
            ddTeams.DataValueField = "TeamId";
            ddTeams.DataBind();
            ddTeams.Items.Insert(0, new ListItem("All", "-1"));
            ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
        }
        else
        {

        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = MaxYear; i <= DateTime.Now.Year + 1; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();
            ddYear.Items.Insert(0, new ListItem("All", "-1"));
            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {
        if ((Convert.ToInt32(Session["RoleID"].ToString()) == 1) || Convert.ToInt32(Session["RoleID"].ToString()) == 2)
        {
            string ddChapterstr = string.Empty;

            ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter Order by State, ChapterCode";

            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ddChapter.SelectedValue = "112";
                ddchapter.Enabled = false;
            }
            if (Session["Panel"] != null)
            {
                if (Session["Panel"].ToString().Equals("ChapterPanel"))
                {
                    if (Convert.ToInt32(Session["mailChapterID"].ToString()) > 1)
                    {
                        ddchapter.SelectedValue = Session["mailChapterID"].ToString();
                    }
                }
            }


        }
        else if (Convert.ToInt32(Session["RoleID"].ToString()) == 5)
        {
            string selectedchapterid = string.Empty;
            string ddChapterstr = string.Empty;
            if (Session["mailChapterID"] != null)
            {
                selectedchapterid = Session["mailChapterID"].ToString();

                ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ChapterID=" + selectedchapterid + " Order by State, ChapterCode";
            }
            else
            {
                ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter Order by State, ChapterCode";
            }
            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ddChapter.SelectedValue = "112";
                ddchapter.Enabled = false;
            }
        }
        else if (Convert.ToInt32(Session["RoleID"].ToString()) == 3)
        {
            string selectedchapterid = string.Empty;
            string ddChapterstr = string.Empty;
            if (Session["mailChapterID"] != null)
            {
                selectedchapterid = Session["mailChapterID"].ToString();

                ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ZoneID in (select ZoneID from Chapter where ChapterID=" + selectedchapterid + ") Order by State, ChapterCode";
            }
            else
            {
                ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ZoneID in (select ZoneID from Chapter) Order by State, ChapterCode";
            }

            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ddChapter.SelectedValue = "112";
                ddchapter.Enabled = false;
            }
        }
        else if (Convert.ToInt32(Session["RoleID"].ToString()) == 4)
        {

            string selectedchapterid = string.Empty;
            string ddChapterstr = string.Empty;
            if (Session["mailChapterID"] != null)
            {
                selectedchapterid = Session["mailChapterID"].ToString();

                ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ClusterId in (select ClusterId from Chapter where ChapterID=" + selectedchapterid + ") Order by State, ChapterCode";
            }
            else
            {
                ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ClusterId in (select ClusterId from Chapter) Order by State, ChapterCode";
            }

            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ddChapter.SelectedValue = "112";
                ddchapter.Enabled = false;
            }
        }
        else if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3")))
        {

            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();
                ddChapter.Items.Insert(0, new ListItem("All", "-1"));
                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }

        }
        else
        {
            ddchapter.Items.Clear();
            ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            ddChapter.Enabled = false;
        }


    }
    protected string genWhereConditons()
    {

        string iCondtions = string.Empty;
        if ((ddYear.SelectedItem.Text != "Select Year") && (ddYear.SelectedItem.Text != "All"))
        {
            iCondtions += " and  Vs.year=" + ddYear.SelectedValue;
        }
        if ((ddEvent.SelectedItem.Text != "Select Event") && (ddEvent.SelectedItem.Text != "All"))
        {
            iCondtions += " and Vs.EventID=" + ddEvent.SelectedValue;
        }

        if ((ddchapter.SelectedItem.Text != "Select Chapter") && (ddchapter.SelectedItem.Text != "All"))
        {
            iCondtions += " and Vs.ChapterId=" + ddchapter.SelectedValue;
        }
        if (ddTeams.Items.Count > 0)
        {
            if ((ddTeams.SelectedItem.Text != "Select Team") && (ddTeams.SelectedItem.Text != "All"))
            {
                iCondtions += " and Vs.TeamId=" + ddTeams.SelectedValue;
            }
        }
        if (Isdropval == true)
        {
            if ((ddTeams.SelectedItem.Text != "Select Team") && (ddTeams.SelectedItem.Text != "All"))
            {
                iCondtions += " and Vs.TeamId=" + ddTeams.SelectedValue;
            }
        }

        return iCondtions;

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        GvVolsignup.EditIndex = -1;
        if (validatGridDisplay() == "1")
        {
            GridDisply();
        }
        else
        {
            validatGridDisplay();
        }
    }
    protected void SignupDetails()
    {
        string StrQrySearchName = " select distinct FirstName+' '+ LastName as name,chapterid  from  IndSpouse where AutoMemberId=" + Session["LoginID"] + "";
        DataSet dsNmae = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchName);
        string strName = dsNmae.Tables[0].Rows[0]["name"].ToString();
        ViewState["NameOfUser"] = strName;
        ViewState["ChapterID"] = dsNmae.Tables[0].Rows[0]["chapterid"].ToString();
        string StrQrySearch = " select distinct V.Memberid,V.RoleID,V.RoleCode,I.FirstName,I.LastName,I.Email,I.HPhone,I.CPhone,I.City,I.State from  volunteer V left join"
        + " IndSpouse I on I.AutomemberId=V.memberId inner join VolSignup Vs on Vs.MemberId=I.AutomemberId  where V.RoleId is not null and V.RoleCode is not null and V.MemberId=" + Session["LoginID"] + " ";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);

        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
        }
        else
        {
            FirstandLastName = ds.Tables[0].Rows[0]["FirstName"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();
        }
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
            lblRoles.Text = "Table2: Existing Roles ";
            GridView1.Visible = false;
            btnExcel.Visible = false;
        }

        else
        {
            lbnorec.Text = "";
            lblRoles.Text = "Table2: Existing Roles";

            GridView1.Visible = true;
            GridView1.DataSource = ds;
            GridView1.DataBind();
            btnExcel.Visible = true;
        }
    }
    protected void GridDisply()
    {
        string memberIDs = "";
        string secMemberIDs = "";
        string StrQrySearch = "";

        StrQrySearch = " select I.State,I.City,Vs.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.VolsignupId,Vs.TeamId,VT.TeamName,Year,Vs.EventID,PG.Name as ProductGroup,"
     + "  P.Name as Product,Vs.AvailHours,Vs.ProductGroupId,Vs.ProductId,Vs.Eventname,C.Name as ChapterName from "
     + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
     + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null  " + genWhereConditons() + " "
     + " order by LastName, FirstName, memberid , year desc,eventid asc,TeamName";

        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {

            GvVolsignup.Visible = false;
            lbnorec1.Text = "No Records Found";
            Label1.Text = "Table1: Volunteer Sign Up";
            // btnExcel.Visible = false;

        }
        else
        {
            if (ddTeams.SelectedValue != "0" && ddTeams.SelectedValue != "-1" && ddTeams.SelectedValue != "")
            {
                if (ddEvent.SelectedValue != "0")
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr["MemberID"] != null && dr["MemberID"].ToString() != "")
                            {
                                if (secMemberIDs != dr["MemberID"].ToString())
                                {
                                    memberIDs += dr["MemberID"].ToString() + ",";
                                }
                                secMemberIDs = dr["MemberID"].ToString();
                            }
                        }
                        string ChapterID = "'";
                        if ((ddchapter.SelectedItem.Text != "Select Chapter") && (ddchapter.SelectedItem.Text != "All"))
                        {
                            ChapterID = ddchapter.SelectedValue;
                        }
                        string iCondtions = string.Empty;
                        if ((ddYear.SelectedItem.Text != "Select Year") && (ddYear.SelectedItem.Text != "All"))
                        {
                            iCondtions += " and  Vs.year=" + ddYear.SelectedValue;
                        }
                        if ((ddEvent.SelectedItem.Text != "Select Event") && (ddEvent.SelectedItem.Text != "All"))
                        {
                            iCondtions += " and Vs.EventID=" + ddEvent.SelectedValue;
                        }

                        if ((ddchapter.SelectedItem.Text != "Select Chapter") && (ddchapter.SelectedItem.Text != "All"))
                        {
                            iCondtions += " and Vs.ChapterId=" + ddchapter.SelectedValue;
                        }
                        if ((ddTeams.SelectedValue != "0") && (ddTeams.SelectedValue != "-1"))
                        {
                            iCondtions += " and Vs.TeamID=" + ddTeams.SelectedValue;
                        }
                        if (memberIDs != "")
                        {
                            memberIDs = memberIDs.Remove(memberIDs.Length - 1);
                            StrQrySearch = " select I.State,I.City,Vs.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.VolsignupId,Vs.TeamId,VT.TeamName,Year,Vs.EventID,PG.Name as ProductGroup,"
               + "  P.Name as Product,Vs.AvailHours,Vs.ProductGroupId,Vs.ProductId,Vs.Eventname,C.Name as ChapterName from "
               + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
               + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null " + iCondtions + " and Vs.MemberID in (" + memberIDs + ")  order by LastName, FirstName, memberid , year desc,eventid asc,TeamName";


                            //ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
                        }
                    }
                }
            }
            lbnorec1.Text = "";
            GvVolsignup.Visible = true;
            GvVolsignup.DataSource = ds;
            GvVolsignup.DataBind();
            SignupDetails();
            btnExcel.Visible = true;
            Label1.Text = "Table1: Volunteer Sign Up ";

        }


    }

    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedItem.Text != "Select Event")
        {
            Team();
        }
        ChapterDrop(ddchapter);
        //GridDisply();

    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GridDisply();
    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GridDisply();
    }
    protected void ddTeams_SelectedIndexChanged(object sender, EventArgs e)
    {
        Isdropval = true;
        // GridDisply();
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        //GvVolsignup.EditIndex = -1;

        //GridDisply();
        //Response.Clear();
        //Response.Buffer = true;
        //Response.ClearContent();
        //Response.ClearHeaders();
        //Response.Charset = "";
        //string FileName = "Volunteer Signup Report" + DateTime.Now + ".xls";
        //StringWriter strwritter = new StringWriter();
        //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.ContentType = "application/vnd.ms-excel";
        //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        //GvVolsignup.GridLines = GridLines.Both;
        //GvVolsignup.Columns[0].Visible = false;
        //GvVolsignup.HeaderStyle.Font.Bold = true;
        //Div2.RenderControl(htmltextwrtter);
        //Div3.RenderControl(htmltextwrtter);
        //Response.Write(strwritter.ToString());
        //Response.End();

        ExportToExcelAll();

    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    protected void GvVolsignup_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GvVolsignup.EditIndex = e.NewEditIndex;
        GridDisply();
        lbPrd.Text = "";
        lbEventId.Text = "";



        Label lblteamId = (Label)GvVolsignup.Rows[e.NewEditIndex].FindControl("lblTeamID");
        //  lbEventId.Text = lbEventNam.Text;


        string StrQrySearch = " select * from productTeamMatrix where TeamId= " + lblteamId.Text + "";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        DataTable dtcheckbox = new DataTable();
        dtcheckbox = ds.Tables[0];

        if (ds.Tables[0].Rows.Count > 0)
        {
            HiddenField kproductGroup = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnProductgroup");
            DropDownList ddProductgroup = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("productGroup");
            if (kproductGroup.Value == "")
            {
            }
            else
            {
                ddProductgroup.SelectedValue = kproductGroup.Value;
                lbPrd.Text = kproductGroup.Value;
            }

            HiddenField kproduct = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnProduct");
            DropDownList ddProduct = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("product");
            if (kproduct.Value == "")
            {
            }
            else
            {
                ddProduct.SelectedValue = kproduct.Value;
            }
            string st = Convert.ToString(dtcheckbox.Rows[0]["Product"]);
            string st1 = Convert.ToString(dtcheckbox.Rows[0]["productGroup"]);
            if (st1 != "Y")
            {
                ddProductgroup.Visible = false;
            }
            if (st != "Y")
            {
                ddProduct.Visible = false;
            }

        }
        else
        {
            DropDownList ddProductgroup1 = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("productGroup");
            ddProductgroup1.Visible = false;
            DropDownList ddProduct1 = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("product");
            ddProduct1.Visible = false;
        }
        HiddenField kAvailhrs = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnAvailhrs");
        DropDownList ddhrs = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("ddavailhrs");
        if (kAvailhrs.Value == "")
        {
        }
        else
        {
            ddhrs.SelectedValue = kAvailhrs.Value;
        }

        IsEvent = false;
    }
    protected void ddday_SelectedIndexChanged(object sender, EventArgs e)
    {
        IsValueCheck = true;
        this.lbPrd.Text = ((DropDownList)sender).SelectedValue;

    }
    protected void GvVolsignup_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string sDatakey;
            int TeamIDup;
            string teamnameup;
            int year;
            int Eventid;
            sDatakey = GvVolsignup.DataKeys[e.RowIndex].Value.ToString();
            Label lblmemberid = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblmemberId");
            Label lbTeamId = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblTeamID");
            Label lbTeamName = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblTeamName");
            Label lbEventName = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblEventName");
            Label lbYear = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblyear");
            DropDownList productGr = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("productGroup");
            DropDownList ddproduct = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("product");
            DropDownList ddavailhr = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("ddavailhrs");
            DropDownList ddEventName = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("ddEventName");

            ViewState["VolDatakey"] = sDatakey;

            string StrQrySearch = " select * from productTeamMatrix where TeamId= " + lbTeamId.Text + "";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            DataTable dtcheckbox = new DataTable();
            dtcheckbox = ds.Tables[0];

            if (ds.Tables[0].Rows.Count > 0)
            {

                string st = Convert.ToString(dtcheckbox.Rows[0]["Product"]);
                if ((productGr.Visible = true) || (ddproduct.Visible = true))
                {
                    if (st != "Y")
                    {
                        if (productGr.SelectedItem.Text != "Select ProductGroup")
                        {
                            Isupdate = true;
                            productid1 = "null";
                            productid1 = "null";
                            product1 = "null";
                            productGroup1 = "'" + productGr.SelectedItem.Text + "'";
                            productgroupid1 = productGr.SelectedValue;

                        }
                    }
                    else
                    {
                        if ((productGr.SelectedItem.Text != "Select ProductGroup") && (ddproduct.SelectedItem.Text != "Select Product"))
                        {
                            Isupdate = true;
                            product1 = "'" + ddproduct.SelectedItem.Text + "'";
                            productid1 = ddproduct.SelectedValue;
                            productGroup1 = "'" + productGr.SelectedItem.Text + "'";
                            productgroupid1 = productGr.SelectedValue;
                        }
                        else
                        {

                            Response.Write("<script>alert('Please Select ProductGroup and Product for this Team')</script>");
                            Isupdate = false;
                        }
                    }
                }
            }
            else
            {
                Isupdate = true;
                productgroupid1 = "null";
                productid1 = "null";
                productGroup1 = "null";
                product1 = "null";
            }

            if (Isupdate == true)
            {
                string ProductGroupDelVal1;
                string ProduCtDelval1;
                if (productgroupid1 == "null")
                {
                    ProductGroupDelVal1 = "productGroupid is null";
                }
                else
                {
                    ProductGroupDelVal1 = "productGroupid=" + productgroupid1 + "";
                }
                if (productid1 == "null")
                {
                    ProduCtDelval1 = "productId is null";
                }
                else
                {
                    ProduCtDelval1 = "productId=" + ddproduct.SelectedValue + "";
                }


                string qryUpdate = "update VolSignUp set TeamID= '" + lbTeamId.Text + "',TeamName='" + lbTeamName.Text + "',"
                   + "Availhours=" + ddavailhr.SelectedValue + ",ProductGroupID=" + productgroupid1 + ",ProductId="
                    + productid1 + ",productGroup=" + productGroup1 + ",ModifyDate=Getdate(),Modifiedby=" + Session["loginID"] + ","
                   + "product=" + product1 + " where volsignupID=" + sDatakey + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);

                string qryUpdate1 = "update VolSignUp set Availhours=" + ddavailhr.SelectedValue + " where year=" + lbYear.Text + " and MemberID=" + lblmemberid.Text + " ";


                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate1);

                Response.Write("<script>alert('Updated successfully')</script>");

                GvVolsignup.EditIndex = -1;
                GridDisply();




            }
        }
        catch (Exception ex)
        {

        }
    }


    protected void GvVolsignup_RowCancelingEdit1(object sender, GridViewCancelEditEventArgs e)
    {
        GvVolsignup.EditIndex = -1;
        GridDisply();
    }
    protected void GvVolsignup_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GvVolsignup.PageIndex = e.NewPageIndex;
        GvVolsignup.DataBind();
        GridDisply();
    }

    public void ExportToExcelAll()
    {
        string StrQrySearch = " select VS.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.TeamId,VT.TeamName,Year,Vs.Eventname,C.Name as ChapterName,PG.Name as ProductGroup,"
        + "  P.Name as Product,Vs.AvailHours, I.State,I.City from "
        + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
        + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null  " + genWhereConditons() + " "
        + " order by memberid , year desc,TeamName;";

        StrQrySearch += " select distinct V.Memberid,V.RoleID,V.RoleCode,I.FirstName,I.LastName,I.Email,I.HPhone,I.CPhone,I.City,I.State from  volunteer V left join"
        + " IndSpouse I on I.AutomemberId=V.memberId inner join VolSignup Vs on Vs.MemberId=I.AutomemberId  where V.RoleId is not null and V.RoleCode is not null and V.MemberId=" + Session["LoginID"] + "";

        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        if (ds.Tables[0] != null)
        {
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            ds.Tables[0].TableName = "Volunteer Signup Report";
            ds.Tables[1].TableName = "Existing Roles";
            string FileName = "Volunteer Signup Report" + DateTime.Now + ".xls";
            ExcelHelper.ToExcel(ds, FileName, Page.Response);
            //}
        }
    }
    public string validatGridDisplay()
    {
        string retval = "1";
        if (ddYear.SelectedValue == "0")
        {
            retval = "-1";
            lbacces.Text = "Please select Year";
            lbacces.ForeColor = Color.Red;
        }
        else if (ddEvent.SelectedValue == "0")
        {
            retval = "-1";
            lbacces.Text = "Please select Event";
            lbacces.ForeColor = Color.Red;
        }
        else if (ddchapter.Enabled == true)
        {
            if (ddchapter.SelectedValue == "0")
            {
                retval = "-1";
                lbacces.Text = "Please select Chapter";
                lbacces.ForeColor = Color.Red;
            }
        }
        else if (ddTeams.SelectedValue == "0" || ddTeams.SelectedValue == "")
        {
            retval = "-1";
            lbacces.Text = "Please select Team";
            lbacces.ForeColor = Color.Red;
        }
        return retval;
    }

    public void FilleEventByRole()
    {
        ddEvent.Items.Clear();
        string LoginRoleID = Session["RoleID"].ToString();
        if (Array.IndexOf(new string[] { "1", "2", "71" }, LoginRoleID) > -1)
        {
            String CmdText = string.Empty;
            CmdText = "select EventId,Name from Event where EventID in (select distinct(EventID) from VolSignUp)";
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                    ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                    ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                    ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                    ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                    ddEvent.Items.Insert(0, new ListItem("All", "-1"));
                    ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                }
            }
            catch (Exception ex)
            {

            }
        }
        else if (Array.IndexOf(new string[] { "89", "96", "97" }, LoginRoleID) > -1)
        {

            String CmdText = string.Empty;
            CmdText = "select EventId,Name from Event where EventID in (13,20)";
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                if (null != ds && ds.Tables.Count > 0)
                {

                    ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                    ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                    ddEvent.Items.Insert(0, new ListItem("All", "-1"));
                    ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                }
            }
            catch (Exception ex)
            {

            }
        }
        else if (Array.IndexOf(new string[] { "3", "4", "5", "46" }, LoginRoleID) > -1)
        {
            String CmdText = string.Empty;
            CmdText = "select EventId,Name from Event where EventID in (2,3,19)";
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                    ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                    ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                    ddEvent.Items.Insert(0, new ListItem("All", "-1"));
                    ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}