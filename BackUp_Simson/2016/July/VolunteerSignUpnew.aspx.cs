﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Net.Mail;


public partial class VolunteerSignUpnew : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    string WCNTVal;
    bool IsEvent = false;
    string WcTeamId;
    string WcTeamId1;
    string productGroup = string.Empty;
    string productStr = string.Empty;
    string productGroupText = string.Empty;
    string productStrText = string.Empty;
    string StrQryCheck = string.Empty;
    string WcText;
    string TeamNames;
    string FirstandLastName = string.Empty;
    Boolean listChecked = false;
    bool IsValue = false;
    bool IsValueCheck = false;
    bool Isupdate = false;
    bool IsEventProd = false;
    string productgroupid1;
    string productid1;
    string productGroup1;
    string product1;
    string confirmValue = string.Empty;
    bool IsAddTeam = false;
    string TeamIDValue = string.Empty;
    bool IsMessage = false;
    string TeamIDValue1 = string.Empty;
    bool checkBoxdisable = false;
    bool GridValidation = false;
    string Email;
    string Hphone;
    string Cphone;
    string MailStatus = string.Empty;
    string Details;
    string WCNTValog = string.Empty;
    string chapterName = string.Empty;
    string ToVolMailid;
    string ChapterUp = string.Empty;
    string StrChapter = string.Empty;
    string chapterGridVal = string.Empty;

    protected void DDproductGroupEdit(object sender, System.EventArgs e)
    {
        try
        {

            if ((IsValueCheck != true))
            {
                string DDLstate;
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                if (lbEventId.Text != "")
                {
                    DDLstate = "select distinct Pg.Name,pg.ProductGroupId from ProductGroup Pg left join Event E on E.EventId=Pg.EventId  left join product p on p.productgroupid=pg.productgroupid   where (E.EventCode='" + lbEventId.Text + "'  or E.Name='" + lbEventId.Text + "')  and p.status='o'";
                    // DDLstate = "select distinct Name,ProductGroupId from ProductGroup where eventid='" + lbEventId.Text + "'";
                }
                else
                {
                    DDLstate = "select distinct Pg.Name,pg.ProductGroupId from productgroup Pg left join product p on p.productgroupid=pg.productgroupid  where  p.status='o'";
                }
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                ddlTemp.DataSource = dsstate;
                ddlTemp.DataTextField = "Name";
                ddlTemp.DataValueField = "ProductGroupId";
                ddlTemp.DataBind();
                ddlTemp.Items.Insert(0, "Select ProductGroup");
            }
            else if (IsEventProd == true)
            {
                string DDLstate;
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                if (lbEventId.Text != "")
                {
                    DDLstate = "select * from ProductGroup Pg left join Event E on E.EventId=Pg.EventId  where (E.EventCode='" + lbEventId.Text + "'  or E.Name='" + lbEventId.Text + "')";
                    // DDLstate = "select distinct Name,ProductGroupId from ProductGroup where eventid='" + lbEventId.Text + "'";
                }
                else
                {
                    DDLstate = "select distinct Name,ProductGroupId from ProductGroup";
                }
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                if (dsstate.Tables[0].Rows.Count == 0)
                {
                    lbPrd.Text = "";
                }
                ddlTemp.DataSource = dsstate;
                ddlTemp.DataTextField = "Name";
                ddlTemp.DataValueField = "ProductGroupId";
                ddlTemp.DataBind();
                ddlTemp.Items.Insert(0, "Select ProductGroup");
            }

        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }

    protected void ChapterEdit(object sender, System.EventArgs e)
    {
        try
        {

            if ((IsValueCheck != true))
            {
                string DDLstate;
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                if (lbchaptr.Text != "")
                {
                    DDLstate = "select distinct ChapterID,chaptercode,[state]  from chapter order by [State],ChapterCode ";
                    // DDLstate = "select distinct Name,ProductGroupId from ProductGroup where eventid='" + lbEventId.Text + "'";


                    DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                    ddlTemp.DataSource = dsstate;
                    ddlTemp.DataTextField = "chaptercode";
                    ddlTemp.DataValueField = "ChapterID";
                    ddlTemp.DataBind();
                    ddlTemp.SelectedValue = lbchaptr.Text;
                }
                // ddlTemp.Items.Insert(0, "Select ProductGroup");
            }


        }
        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }

    protected void DDproduct(object sender, System.EventArgs e)
    {
        try
        {
            string DDLstate;
            DropDownList ddlTemp = null;
            ddlTemp = (DropDownList)sender;
            ddlTemp.Items.Clear();
            if ((lbPrd.Text != "") && (lbPrd.Text != "Select ProductGroup"))
            {
                DDLstate = "select distinct Name,productid from Product where productGroupID=" + lbPrd.Text + " and status='o'";
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
                ddlTemp.DataSource = dsstate;
                ddlTemp.DataTextField = "Name";
                ddlTemp.DataValueField = "productid";
                ddlTemp.DataBind();
            }
            ddlTemp.Items.Insert(0, "Select Product");
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void DDAvailhrs(object sender, System.EventArgs e)
    {
        try
        {
            if (IsValueCheck != true)
            {
                DropDownList ddlTemp = null;
                ddlTemp = (DropDownList)sender;
                ddlTemp.Items.Clear();
                ArrayList list = new ArrayList();

                for (int i = 1; i <= 20; i++)
                {
                    list.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlTemp.DataSource = list;
                ddlTemp.DataTextField = "Text";
                ddlTemp.DataValueField = "Value";
                ddlTemp.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = "4240";
        //Session["entryToken"] = "PARENT";
        //Session["LoginEmail"] = "sdasdas";
        if (Session["LoginId"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        } 
        if (!IsPostBack)
        {
           // Session["VolunteerLoginId"] = null;
            Events();
            Yearscount();
            Availhrs();
            ChapterDrop(ddchapter);
            linkButton();
        }
        try
        {
        if (Session["VolunteerLoginId"] != null )
        {
            if (Session["VolunteerLoginId"].ToString().Length == 0)
            {
                return;
            }
            lbEmail.Text = "";
            DataSet dsCount = new DataSet();
            int RoleCount = 0;
            string UserID = Session["VolunteerLoginId"].ToString();
            string cmdCountText = "select count(*)as Countset from Volunteer where MemberID=" + UserID + "";
            dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdCountText);
            if (null != dsCount && dsCount.Tables.Count > 0)
            {
                if (dsCount.Tables[0].Rows.Count > 0)
                {
                    RoleCount = Convert.ToInt32(dsCount.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            //if (!IsPostBack)
           // {
            divOption.Visible = false;
            divVolSignUp.Visible = true;
          
                SignupDetails();
                GridDisply();              
            //    if (RoleCount > 0)
            //    {
            //        lbnorec1.Visible = false;
            //    }
            //    else
            //    {
            //        lbnorec1.Visible = true;
            //    }
            //}
            //else
            //{
                for (int i = 0; i < CheckBoxList1.Items.Count; i++)
                {
                    if (CheckBoxList1.Items[i].Selected)
                    {
                        if (CheckBoxList1.Items[i].Enabled == true)
                        {
                            CheckBoxList1.Items[i].Attributes.Add("Style", "color: blue;");
                        }
                    }
                }
                if (RoleCount > 0)
                {
                    lbnorec1.Visible = false;
                }
                else
                {
                    lbnorec1.Visible = true;
                }
            //}
            
                lblChapter.Text = (SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Chapter c inner join indspouse i on c.ChapterId= i.ChapterId where i.automemberid= " + Session["VolunteerLoginId"])).ToString().Replace(",", "_");

        }
        else
        {
            // load ind spouse name
            string str = "";
            str = "select I.Firstname+' ' +I.LastName From IndSpouse I WHERE I.AutomemberId in(select case when Ind.donortype='IND' then isnull(Ind.Automemberid,'') else isnull(Relationship,'') end from indspouse Ind where automemberid=" + Session["LoginId"] + ")";
            str=SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, str).ToString();
            if (str.Trim().Length != 0)
                rbtnOption.Items[0].Text = str;
            else
                rbtnOption.Items[0].Attributes.Add("style", "display='none;'");
            str = "select I.Firstname+' ' +I.LastName From IndSpouse I WHERE I.AutomemberId in(select case when Ind.donortype='IND' then isnull((select Automemberid from indspouse Where RelationShip=" + Session["LoginId"] + "),'')  else  AutoMemberId END from indspouse Ind where Automemberid=" + Session["LoginId"] + ")";
            str = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, str).ToString();
            if (str.Trim().Length != 0)
                rbtnOption.Items[1].Text = str;
            else
                rbtnOption.Items[1].Attributes.Add("style", "display='none;'");
        }
        }
            catch (Exception ex)
            { }
    }
    protected void linkButton()
    {
        if (Session["entryToken"].ToString().ToUpper() == "PARENT")
        {
            lbtnVolunteerFunctions.Text = "Back to Parent Functions";
        }
        else if (Session["entryToken"].ToString().ToUpper() == "DONOR")
        {
            lbtnVolunteerFunctions.Text = "Back to Donor Functions";
        }
        else
        {
            lbtnVolunteerFunctions.Text = "Back to Volunteer Functions";
        }
    }

    protected void ChekBoxc()
    {
        try
        {
            ChecKboxCheked(false);
            foreach (ListItem li in CheckBoxList1.Items)
            {
                li.Selected = false;
                li.Enabled = true;
            }
            //if (IsEvent == true)
            //{

            string VolCol = string.Empty;
            if (ddEvent.SelectedItem.Text == "Chapter Contests")
            {
                VolCol = "ChapterContests";
            }
            else
            {
                VolCol = ddEvent.SelectedItem.Text;
            }

            StrQryCheck = "select * from  VolTeamMatrix where " + VolCol + "='Y'";

            DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQryCheck);
            if (null != dsCheck && dsCheck.Tables.Count > 0 && dsCheck.Tables[0].Rows.Count <= 0)
            {

                Chk.Visible = false;
                lblTeam.Visible = true;
                btnAdd.Visible = false;
                lblTeam.Text = "No Teams found ";
                CheckBoxList1.DataSource = dsCheck;
                CheckBoxList1.DataTextField = "TeamName";
                CheckBoxList1.DataValueField = "TeamID";
                CheckBoxList1.DataBind();
            }
            else
            {
                Chk.Visible = true;
                btnAdd.Visible = true;
                CheckBoxList1.DataSource = dsCheck;
                CheckBoxList1.DataTextField = "TeamName";
                CheckBoxList1.DataValueField = "TeamID";
                CheckBoxList1.DataBind();
            }
            //}
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void CancelOption()
    {
        ddEvent.SelectedIndex = 0;
        ddYear.SelectedIndex = 0;
        //ddproductgroup.SelectedIndex = 0;
        ddhrs.SelectedIndex = 0;
        // ddproduct.SelectedIndex = 0;
    }
    protected void Availhrs()
    {
        try
        {
            ddhrs.Items.Clear();

            ArrayList list = new ArrayList();

            for (int i = 1; i <= 20; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddhrs.DataSource = list;
            ddhrs.DataTextField = "Text";
            ddhrs.DataValueField = "Value";
            ddhrs.DataBind();
            ddhrs.Items.Insert(0, new ListItem("Select hours per week", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = MaxYear; i <= DateTime.Now.Year + 1; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();
            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }

    protected void Events()
    {
        try
        {
            ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
            ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
            ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
            ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
            ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
            ddEvent.Items.Insert(0, new ListItem("Chapter Contests", "2"));
            ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
            ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void GridDisply()
    {
        lnkHelpGuide.Enabled = true;
        string StrQrySearch;

        StrQrySearch = " select Vs.VolsignupId,Vs.TeamId,VT.TeamName,Vs.EventID,YEAR,PG.Name as ProductGroup,"
        + " P.Name as product,Vs.AvailHours,Vs.ProductGroupId,Vs.ProductId,Vs.Eventname,C.Name as Chapter,C.chapterid   from VolSignUp Vs left join"
        + " VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on Pg.ProductGroupId=Vs.ProductGroupId left join  "
        + " Product P on P.ProductId=Vs.ProductId  left join chapter c on c.Chapterid=Vs.chapterid where Vs.MemberId=" + Session["VolunteerLoginId"] + " order by year desc,eventid asc,chapter asc,TeamName";

        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
            GvVolsignup.Visible = false;
            lbnorec1.Text = "You have not signed up yet. Please select Year and Event for signup.";
            Label1.Text = "Table1: Volunteer Sign Up for";
            Label2.Text = ViewState["NameOfUser"].ToString();
            lnkHelpGuide.Enabled = false;
        }
        else
        {
            lbnorec1.Text = "";
            GvVolsignup.Visible = true;
            GvVolsignup.DataSource = ds;
            GvVolsignup.DataBind();
            SignupDetails();
            string tempName = ViewState["NameOfUser"].ToString();
            Label1.Text = "Table1: Volunteer Sign Up for";
            Label2.Text = tempName;
        }
    }
    protected void SignupDetails()
    {
        Email = "";
        string StrQrySearchName = " select distinct FirstName+' '+ LastName as name,chapterid  from  IndSpouse where AutoMemberId=" + Session["VolunteerLoginId"] + "";
        DataSet dsNmae = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchName);
        string strName = dsNmae.Tables[0].Rows[0]["name"].ToString();
        ViewState["NameOfUser"] = strName;
        lblUserName.Text = strName;
        ViewState["ChapterID"] = dsNmae.Tables[0].Rows[0]["chapterid"].ToString();
        string StrQrySearch = " select distinct V.Memberid,V.RoleID,V.RoleCode,I.FirstName,I.LastName,I.Email,I.HPhone,I.CPhone,I.Chapter  from  volunteer V left join"
        + " IndSpouse I on I.AutomemberId=V.memberId where V.RoleId is not null and V.RoleCode is not null and V.MemberId=" + Session["VolunteerLoginId"] + "";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
        }
        else
        {
            FirstandLastName = ds.Tables[0].Rows[0]["FirstName"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();
            ViewState["NameOfUser"] = strName;
            Email = ds.Tables[0].Rows[0]["Email"].ToString();
            ViewState["EmailID"] = Email;
            Cphone = ds.Tables[0].Rows[0]["CPhone"].ToString();
            Hphone = ds.Tables[0].Rows[0]["HPhone"].ToString();
            ViewState["Phone"] = Hphone + "/" + Cphone;

        }
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
            lblRoles.Text = "Table2: Existing Roles for   " + strName + "";
            lbnorec.Text = "No roles assigned to you";
            GridView1.Visible = false;
        }
        else
        {
            lbnorec.Text = "";
            lblRoles.Text = "Table2: Existing Roles for  ";
            lbRolesName.Text = FirstandLastName;
            ViewState["NameOfUser"] = strName;
            GridView1.Visible = true;
            GridView1.DataSource = ds;
            GridView1.DataBind();
        }
    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {

        IsEvent = true;
        lbevent.Text = "";
        lblErr.Text = "";
        CheckBoxList1.Enabled = true;
        if (ddEvent.SelectedItem.Text != "Select Event")
        {

            if (lbReqfield.Text == "Please Select Event")
            {
                lbReqfield.Text = "";
            }
            IsValue = true;
            lbevent.Text = ddEvent.SelectedItem.Text;

            lblErr1.Text = "";
        }

        Availhrs();
        GvVolsignup.EditIndex = -1;
        GridDisply();
        //ChekBoxc();
        //checkBoXChecked();

        if (ddEvent.SelectedItem.Text == "Select Event")
        {
            Chk.Visible = false;
        }
        ddhrs.SelectedIndex = 0;
        AvailHrsSelection();
        if (ViewState["AvailHours"] != "")
        {
            ddhrs.SelectedValue = ViewState["AvailHours"].ToString();
        }
        GridValText.Text = "";
        Gridval.Visible = false;
        ProductError.Text = "";
        lbnorec1.Text = "";

        // TblChapter.Visible = true;
        ChapterDrop(ddchapter);

        TblTeams.Visible = false;
        btnAdd.Visible = false;

        if (ddEvent.SelectedIndex != 0)
        {
            //string url = "VolunteerSignupGuide.aspx?value=" + ddEvent.SelectedItem.Value;
            //string s = "  alert(newWindow);  try{if(!(newWindow.Closed) ) { newWindow = window.open('" + url + "', 'VolunteerSignupGuide Help', 'width=970,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no'); }}catch(ex){alert('Err' + ex.toString());}";
            ClientScript.RegisterStartupScript(this.GetType(), "script", "PopupPickerByCond();", true);
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {
        if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3") || (ddEvent.SelectedValue == "1") || (ddEvent.SelectedValue == "13")) || (ddEvent.SelectedValue == "20"))
        {

            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();
                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }
            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ddChapter.SelectedValue = "112";
                ddchapter.Enabled = false;
            }
        }
        else
        {
            ddchapter.Items.Clear();
            ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));

            ddChapter.Enabled = false;
        }
    }
    protected void AvailHrsSelection()
    {
        ViewState["AvailHours"] = "";
        string TeamChecked1 = "Select distinct TeamName,Availhours from volsignup where  year=" + ddYear.SelectedValue + "  and  MemberId=" + Session["VolunteerLoginId"] + "";
        DataSet dsCheckbox1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, TeamChecked1);
        DataTable DtnCheck1 = dsCheckbox1.Tables[0];
        if (DtnCheck1.Rows.Count > 0)
        {
            ViewState["AvailHours"] = DtnCheck1.Rows[0]["Availhours"].ToString();
        }

    }
    protected void checkBoXChecked()
    {
        try
        {
            string TeamChecked;
            ViewState["AvailHours"] = "";
            if ((ddEvent.SelectedItem.Text != "Select Event") && (ddYear.SelectedItem.Text != "Select Year"))
            {
                ChecKboxCheked(false);
                string ProductGroupDelVal1 = string.Empty;
                string ProduCtDelval1 = string.Empty;
                if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3")))
                {
                    if (ddchapter.SelectedItem.Text != "Select Chapter")
                    {
                        TeamChecked = "Select distinct TeamName,Availhours from volsignup where EventName='" + ddEvent.SelectedItem.Text + "'  and year=" + ddYear.SelectedValue + " and chapterid=" + ddchapter.SelectedValue + " and  MemberId=" + Session["VolunteerLoginId"] + "";
                    }
                    else
                    {
                        TeamChecked = "Select distinct TeamName,Availhours from volsignup where EventName='" + ddEvent.SelectedItem.Text + "'  and year=" + ddYear.SelectedValue + "  and  MemberId=" + Session["VolunteerLoginId"] + "";
                    }
                }
                else
                {
                    TeamChecked = "Select distinct TeamName,Availhours from volsignup where EventName='" + ddEvent.SelectedItem.Text + "'  and year=" + ddYear.SelectedValue + "  and  MemberId=" + Session["VolunteerLoginId"] + "";
                }
                DataSet dsCheckbox = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, TeamChecked);
                DataTable DtnCheck = dsCheckbox.Tables[0];

                for (int i = 0; i <= DtnCheck.Rows.Count - 1; i++)
                {

                    for (int j = 0; j <= CheckBoxList1.Items.Count - 1; j++)
                    {
                        if (CheckBoxList1.Items[j].Text == DtnCheck.Rows[i]["TeamName"].ToString())
                        {
                            CheckBoxList1.Items[j].Selected = true;
                            CheckBoxList1.Items[j].Enabled = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                ChecKboxCheked(false);
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void AddedTeams()
    {
        lbReqfield.Text = "";
        string ProduCtDelval;
        string ProductGroupDelVal;
        int cnt = 0;
        lblErr.Text = "";
        int count = 0;
        if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3")))
        {
            chapterName = ddchapter.SelectedValue;
            chapterGridVal = "Chapterid=" + ddchapter.SelectedValue + "";

        }
        else
        {
            chapterName = "null";
            chapterGridVal = "Chapterid is null";
        }
        if (ddEvent.SelectedValue == "1")
        {
            chapterName = ddchapter.SelectedValue;
        }
        if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
        {
            chapterName = ddchapter.SelectedValue;
        }
        foreach (ListItem li in CheckBoxList1.Items)
        {
            if (li.Selected)
            {
                listChecked = true;
            }

        }
        if (listChecked == true)
        {
            foreach (ListItem li in CheckBoxList1.Items)
            {
                if (li.Selected)
                {
                    count = count + 1;
                    if (WCNTVal != string.Empty)
                    {
                        if (li.Enabled == true)
                        {
                            WcTeamId1 = WcTeamId1 + "," + li.Value + "";
                        }
                        WcTeamId = WcTeamId + "," + li.Value + "";
                        WcText = WcText + "," + li.Text + "";
                        TeamNames = TeamNames + "','" + li.Text;
                    }
                    else
                    {
                        if (li.Enabled == true)
                        {
                            WcTeamId1 = li.Value;
                        }
                        WcTeamId = li.Value;
                        WcText = li.Text;
                        TeamNames = li.Text;
                    }
                }
            }


            TeamIDValue = WcTeamId;
            TeamIDValue1 = WcTeamId1;
            TeamIDValue = TeamIDValue.TrimStart(',');
            if (TeamIDValue1 != null)
            {
                TeamIDValue1 = TeamIDValue1.TrimStart(',');
            }

            TeamIDValue = "(" + TeamIDValue + ")";
            TeamIDValue1 = "(" + TeamIDValue1 + ")";
            ViewState["VolSignuplogTeamIdval"] = TeamIDValue1;
            ViewState["VolSignuplogTeamId"] = TeamIDValue1;
            TeamNames = TeamNames.TrimStart(',');
            TeamNames = TeamNames + "'";
            string TeamNamesNew = TeamNames.Substring(2);
            TeamNamesNew = "(" + TeamNamesNew + ")";
            WcText = WcText.TrimStart(',');
            WcText = "(" + WcText + ")";
            //if (TeamIDValue1 != "()")
            //{
            //    string StrQrySearchDup = "select distinct TeamId from VolSignUp where TeamId in " + TeamIDValue + " and year=" + ddYear.SelectedValue + "  and EventName='" + ddEvent.SelectedItem.Text + "' and   MemberId=" + Session["LoginID"] + "";
            //    DataSet dscheckDup = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchDup);
            //    DataTable DtChecKdup = dscheckDup.Tables[0];
            //    if (DtChecKdup.Rows.Count == count)
            //    {
            //        lbReqfield.Text = "";
            //        lblErr.Text = "Please select new Team(s) to Add";
            //        IsAddTeam = false;
            //        Gridval.Visible = false;
            //        GridValText.Text = "";
            //        ProductError.Text = "";
            //    }
            //    else
            //    {
            //        IsAddTeam = true;
            //    }
            //}
            //else
            //{
            //    lbReqfield.Text = "";
            //    lblErr.Text = "Please select new Team(s) to Add";
            //    IsAddTeam = false;
            //    Gridval.Visible = false;
            //    GridValText.Text = "";
            //    ProductError.Text = "";
            //}

            IsAddTeam = true;
            string StrQrySearch = string.Empty;
            StrQrySearch = " select * from productTeamMatrix where TeamId in " + TeamIDValue + " and (productgroup is not null or product is not null)";

            //string StrQrySearch = " select * from productTeamMatrix where TeamId in " + TeamIDValue1 + " and (productgroup is not null or product is not null)";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            DataTable dtcheckbox = new DataTable();
            dtcheckbox = ds.Tables[0];
            int l1count = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    for (int j = 0; j <= Gridval.Rows.Count - 1; j++)
                    {
                        if (ds.Tables[0].Rows[i]["TeamId"].ToString() == Gridval.Rows[j].Cells[0].Text)
                        {
                            l1count = l1count + 1;
                        }
                    }


                }

                if (Gridval.Rows.Count != dtcheckbox.Rows.Count)
                {
                    TeamSelectionValidation();
                }
                else
                {
                    if (l1count != Gridval.Rows.Count)
                    {
                        TeamSelectionValidation();
                    }
                }

            }
            else
            {
                Gridval.Visible = false;
                GridValText.Text = "";
                ProductError.Text = "";

            }
            if (IsAddTeam == true)
            {
                if (Gridval.Visible == true)
                {

                    GridViewVal();

                }
                else
                {
                    //GridValidation = true;
                }

                if ((TeamIDValue1 != "()") && (Gridval.Visible == false))
                {
                    TeamSelectionValidation();
                }
                else
                {
                    IsAddTeam = true;
                    //GridValidation = true;
                }

                foreach (ListItem li in CheckBoxList1.Items)
                {
                    if (li.Selected)
                    {
                        ViewState["TeamLiValue"] = li.Value;
                        ViewState["TeamLiText"] = li.Text;
                        hdnTeamText.Value = li.Text;
                        string strSql = "select count(*) from VolSignUp where TeamId=" + li.Value + " and year=" + ddYear.SelectedValue + "  and EventName='" + ddEvent.SelectedItem.Text + "'  and " + chapterGridVal + "    and  MemberId=" + Session["VolunteerLoginId"] + "";
                        cnt = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql).ToString());

                        if (cnt == 0)
                        {
                            if (Gridval.Rows.Count > 0)
                            {
                                for (int i = 0; i <= Gridval.Rows.Count - 1; i++)
                                {

                                    if (Gridval.Rows[i].Cells[0].Text == li.Value)
                                    {
                                        DropDownList ddlPg = (DropDownList)Gridval.Rows[i].Cells[2].FindControl("ddlproductGroup");
                                        string valddproductGr = ddlPg.SelectedValue;
                                        DropDownList ddlProduct = (DropDownList)Gridval.Rows[i].Cells[2].FindControl("ddlproduct1");
                                        string valddproduc = ddlProduct.SelectedValue;

                                        if ((ddlPg.SelectedValue != "Select ProductGroup") || (ddlPg.SelectedValue != ""))
                                        {
                                            productGroup = ddlPg.SelectedValue;
                                            productStrText = "'" + ddlPg.SelectedItem.Text + "'";
                                            hdnProductGroupText.Value = ddlPg.SelectedItem.Text;
                                        }
                                        if ((ddlProduct.SelectedValue != "Select Product") || (ddlProduct.SelectedValue != ""))
                                        {
                                            if (ddlProduct.Visible == true)
                                            {
                                                productStr = ddlProduct.SelectedValue;
                                                productGroupText = "'" + ddlProduct.SelectedItem.Text + "'";
                                                hdnProductText.Value = ddlProduct.SelectedItem.Text;
                                            }
                                            else
                                            {
                                                productStr = "null";
                                                productGroupText = "null";
                                            }

                                        }
                                        break;
                                    }
                                    else
                                    {
                                        productGroup = "null";
                                        productGroupText = "null";
                                        productStr = "null";
                                        productStrText = "null";
                                    }

                                }
                                string StrQrySearchDup = string.Empty;


                                StrQrySearchDup = "select distinct TeamId from VolSignUp where TeamId = " + li.Value + " and year=" + ddYear.SelectedValue + "  and EventName='" + ddEvent.SelectedItem.Text + "' and   MemberId=" + Session["VolunteerLoginId"] + "";

                                if (productGroup != "null" && productStr != "null")
                                {
                                    StrQrySearchDup += "and ProductGroupID=" + productGroup + " and ProductID=" + productStr + "";
                                }
                                else if (productGroup == "null" && productStr == "null")
                                {
                                    StrQrySearchDup += "and ProductGroupID is null and ProductID is null";
                                }
                                else if (productGroup != "null" && productStr == "null")
                                {
                                    StrQrySearchDup += "and ProductGroupID =" + productGroup + " and ProductID is null";
                                }

                                DataSet dscheckDup = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchDup);
                                DataTable DtChecKdup = dscheckDup.Tables[0];
                                if (DtChecKdup.Rows.Count > 0)
                                {
                                    if (productGroup != "null" && productStr != "null")
                                    {
                                        lblErr.Text = "You have already signed up for this team, product group and product.  Please select a different team or different product group and product to add if you like.";

                                    }
                                    else if (productGroup == "null" && productStr == "null")
                                    {
                                        lblErr.Text = "You have already signed up for this team.  Please select a different team to add if you like.";
                                    }

                                }
                                else
                                {
                                    InserData();
                                }

                            }
                            else
                            {
                                productGroup = "null";
                                productGroupText = "null";
                                productStr = "null";
                                productStrText = "null";
                                string StrQrySearchDup = string.Empty;

                                StrQrySearchDup = "select distinct TeamId from VolSignUp where TeamId = " + li.Value + " and year=" + ddYear.SelectedValue + "  and EventName='" + ddEvent.SelectedItem.Text + "' and   MemberId=" + Session["VolunteerLoginId"] + "";



                                DataSet dscheckDup = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchDup);
                                DataTable DtChecKdup = dscheckDup.Tables[0];
                                if (DtChecKdup.Rows.Count > 0)
                                {

                                    lblErr.Text = "You have already signed up for this team.  Please select a different team to add if you like.";
                                    //break;
                                }
                                else
                                {
                                    InserData();
                                }
                                //InserData();
                            }

                        }


                    }
                }

                GridDisply();
                if (checkBoxdisable == true)
                {

                    MessageBody();

                    VolSignuplog();
                    ChekBoxc();
                    checkBoXChecked();
                    checkColor();
                    Gridval.Visible = false;

                }

            }

        }
        else
        {
            lblErr.Visible = true;
            lblErr.Text = "Please Select Any one of the Teams";
            listChecked = false;
        }
        if (hfIsRedirect.Value.Equals("True"))
        {
            redirectingPage();
        }
    }
    protected void MessageBody()
    {

        string UserMailmessage;
        string UserSubject;
        UserSubject = "Thank you signing up!";
        UserMailmessage = "Thank you for signing up to volunteer for the North South Foundation. We greatly appreciate. A volunteer will contact you soon.";

        string ToEMailid;
        ToEMailid = ViewState["EmailID"].ToString();
        // ToEMailid = "ahila.cs@capestart.com";

        string VolMailmessage = "";
        string VolUserSubject;
        VolUserSubject = "A Volunteer Signed up to volunteer for NSF!";

        if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
        {
            VolMailmessage = "<span style='width:100px; float :left; font-weight:bold;'>Event</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + ddEvent.SelectedItem.Text + "</span><br><br>";
            if (hdnProductGroupText.Value != "")
            {
                VolMailmessage += "<span style='width:100px; float :left; font-weight:bold;'>Product Group</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + hdnProductGroupText.Value + "</span><br><br>";
            }
            if (hdnProductText.Value != "")
            {
                VolMailmessage += "<span style='width:100px; float :left; font-weight:bold;'>Product</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + hdnProductText.Value + "</span><br><br>";
            }
            if (hdnTeamText.Value != "")
            {
                VolMailmessage += "<span style='width:100px; float :left; font-weight:bold;'>Team</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + hdnTeamText.Value + "</span><br><br>";
            }

            //VolMailmessage = "<b>Event : " + ddEvent.SelectedItem.Text + "</b><br><br><b>Product Group : " + hdnProductGroupText.Value + " </b><br><br><b>Product : " + hdnProductText.Value + "</b><br><br><b>Team : " + hdnTeamText.Value + "</b><br><br>";

            VolMailmessage += "A volunteer signed up to volunteer for NSF. It will be greatly appreciated if you can contact and follow up within a day or two.</br> Name:" + ViewState["NameOfUser"] + "</br> Email:" + ViewState["EmailID"] + "</br> Phone Number: " + ViewState["Phone"] + "";
        }
        else
        {
            //VolMailmessage = "A volunteer signed up to volunteer for NSF. It will be greatly appreciated if you can contact and follow up within a day or two.</br> Name:" + ViewState["NameOfUser"] + "</br> Email:" + ViewState["EmailID"] + "</br> Phone Number: " + ViewState["Phone"] + "";



            VolMailmessage = "<span style='width:100px; float :left; font-weight:bold;'>Event</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + ddEvent.SelectedItem.Text + "</span><br><br>";
            if (hdnProductGroupText.Value != "")
            {
                VolMailmessage += "<span style='width:100px; float :left; font-weight:bold;'>Product Group</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + hdnProductGroupText.Value + "</span><br><br>";
            }
            if (hdnProductText.Value != "")
            {
                VolMailmessage += "<span style='width:100px; float :left; font-weight:bold;'>Product</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + hdnProductText.Value + "</span><br><br>";
            }
            if (hdnTeamText.Value != "")
            {
                VolMailmessage += "<span style='width:100px; float :left; font-weight:bold;'>Team</span><span style='float:left; width:15px; font-weight:bold;'>:</span><span style='float:left; font-weight:bold;'> " + hdnTeamText.Value + "</span><br><br>";
            }

            //VolMailmessage = "<b>Event : " + ddEvent.SelectedItem.Text + "</b><br><br><b>Product Group : " + hdnProductGroupText.Value + " </b><br><br><b>Product : " + hdnProductText.Value + "</b><br><br><b>Team : " + hdnTeamText.Value + "</b><br><br>";

            VolMailmessage += "A volunteer signed up to volunteer for NSF. It will be greatly appreciated if you can contact and follow up within a day or two.</br> Name:" + ViewState["NameOfUser"] + "</br> Email:" + ViewState["EmailID"] + "</br> Phone Number: " + ViewState["Phone"] + "";

        }
        if ((ddchapter.Visible == true) || (ChapterUp != ""))
        {
            MailId();
        }
        else
        {
            //ToVolMailid = "chitturi9@gmail.com";
            // ToVolMailid = "ahila.cs@capestart.com";
            if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ToVolMailid = "praveengoli@gmail.com";
                //ToVolMailid = "michael.simson@capestart.com";
            }
            else
            {
                ToVolMailid = "chitturi9@gmail.com";
            }

        }

        //ToVolMailid = "ahila21nov@gmail.com";
        EmailContent(ViewState["NameOfUser"].ToString(), UserSubject, UserMailmessage, ToEMailid, "");
        if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
        {
            EmailContent("Praveen Goli", VolUserSubject, VolMailmessage, ToVolMailid, "Thanks");
        }
        else
        {
            EmailContent("Ratnam Chitturi", VolUserSubject, VolMailmessage, ToVolMailid, "Thanks");
        }


    }
    protected void MailId()
    {
        string Emaildetails;
        if (ChapterUp != "")
        {
            Emaildetails = ChapterUp;
        }
        else
        {
            Emaildetails = ddchapter.SelectedValue;
        }
        string StrQryMail = "Select email from IndSpouse I where exists (select * from Volunteer V where V.memberID=I.automemberID and V.RoleID=5 and V.chapterID = " + Emaildetails + ")";
        DataSet dsMail = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQryMail);
        DataTable DtMail = dsMail.Tables[0];
        if (DtMail.Rows.Count > 0)
        {
            ToVolMailid = DtMail.Rows[0]["email"].ToString();
            //ToVolMailid = "ahila.cs@capestart.com";
        }
        else
        {
            if (ddEvent.SelectedValue == "13" || ddEvent.SelectedValue == "20")
            {
                ToVolMailid = "praveengoli@gmail.com";

                //ToVolMailid = "michael.simson@capestart.com";
            }
            else
            {
                ToVolMailid = "chitturi9@gmail.com";
            }
            //ToVolMailid = "chitturi9@gmail.com";
            // ToVolMailid = "ahila.cs@capestart.com";
        }
    }
    protected void EmailContent(string Name, string StrSubject, string StrMailBody, string EmailToSend, string Thank)
    {
        string s = ViewState["VolSignuplogTeamId"].ToString();
        string MailBody;

        MailBody = "Dear " + Name + ",<br>Note : Do not reply to the email above.<br><br>" + StrMailBody + "<br><br>" + Thank + "";
        string subj;
        subj = StrSubject;
        SendEmail(subj, MailBody, EmailToSend);
    }
    protected void SendEmail(string sSubject, string sBody, string sMailTo)
    {
        string sFrom;
        sFrom = "nsfcontests@gmail.com";
        MailMessage mail = new MailMessage(sFrom, sMailTo, sSubject, sBody);
        SmtpClient client = new SmtpClient();
        mail.IsBodyHtml = true;
        Boolean ok;
        ok = true;
        try
        {
          //  client.Send(mail);
            MailStatus = "Sent";

            lbEmail.Visible = true;
            lbEmail.Text = "NSF Sent mail to you!";

        }
        catch (Exception ex)
        {
            ok = false;
            MailStatus = "Failed";
        }
    }
    protected void InserData()
    {
        if (GridValidation == true)
        {
            string sy = WcTeamId;
            ViewState["YearLog"] = ddYear.SelectedValue;
            ViewState["EventNamelog"] = ddEvent.SelectedItem.Text;
            ViewState["AvailHrs"] = ddhrs.SelectedValue;

            string StrQryInsert = "INSERT INTO VolSignUp (TeamID, Year,ProductGroupId,ProductId,Memberid,AvailHours,CreateDate,CreatedBy,TeamName,Product,ProductGroup,Eventname,EventId,ChapterId) VALUES (" + ViewState["TeamLiValue"] + "," + ddYear.SelectedValue + "," + productGroup + "," + productStr + ",'" + Session["VolunteerLoginId"] + "'," + ddhrs.SelectedValue + ",Getdate(), " + Session["VolunteerLoginId"] + " ,'" + ViewState["TeamLiText"] + "'," + productGroupText + "," + productStrText + ",'" + ddEvent.SelectedItem.Text + "'," + ddEvent.SelectedValue + "," + chapterName + ")";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryInsert);
            string qryUpdate1 = "update VolSignUp set Availhours=" + ddhrs.SelectedValue + " where year=" + ddYear.SelectedValue + " and MemberId=" + Session["VolunteerLoginId"] + "";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate1);
            lblErr.Visible = true;
            lbEmail.Text = "You have been added to the team(s) successfully, we will contact you soon.";
            lblErr.Text = "";
            ProductError.Text = "";
            GridValText.Text = "";
            checkBoxdisable = true;
            Session["Status"] = "Added";


        }
    }

    protected void ChecKboxCheked(bool status)
    {
        foreach (ListItem li in CheckBoxList1.Items)
        {
            if (li.Selected)
            {
                li.Selected = status;

            }
        }
    }

    protected void TeamSelectionValidation()
    {
        if (TeamIDValue != "()")
        {
            string StrQrySearch = string.Empty;

            StrQrySearch = "select * from productTeamMatrix where TeamId in " + TeamIDValue + " and (productgroup is not null or product is not null)";

            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            DataTable dtcheckbox = new DataTable();
            dtcheckbox = ds.Tables[0];

            if (ds.Tables[0].Rows.Count > 0)
            {
                GridValText.Text = "Teams-Product Details";
                Gridval.Visible = true;
                Gridval.DataSource = ds;
                Gridval.DataBind();
                lblErr.Text = "";
                lblErr.Text = "";
                GridValidation = false;
                IsAddTeam = false;
            }
            else
            {
                GridValText.Text = "";
                Gridval.Visible = false;
                IsAddTeam = true;
                GridValidation = true;
            }
        }
    }
    protected void GridViewVal()
    {
        for (int i = 0; i <= Gridval.Rows.Count - 1; i++)
        {
            DropDownList ddlPg = (DropDownList)Gridval.Rows[i].Cells[2].FindControl("ddlproductGroup");
            string valddproductGr = ddlPg.SelectedValue;
            DropDownList ddlProduct = (DropDownList)Gridval.Rows[i].Cells[2].FindControl("ddlproduct1");
            string valddproduc = ddlProduct.SelectedValue;
            if ((ddlPg.Visible == true) && (ddlProduct.Visible == true))
            {
                if ((ddlPg.SelectedValue == "Select ProductGroup") || (ddlProduct.SelectedValue == "Select Product"))
                {
                    ProductError.Text = "Please Select ProductGroup and Product from below Table";
                    GridValidation = false;
                    IsAddTeam = false;

                }
                else
                {
                    GridValidation = true;
                }

            }
            else if ((ddlProduct.Visible == false) && (ddlPg.Visible == true))
            {
                if ((ddlPg.SelectedValue == "Select ProductGroup") || (ddlPg.SelectedValue == ""))
                {
                    ProductError.Text = "Please Select ProductGroup and Product from below Table";
                    IsAddTeam = false;
                    GridValidation = false;

                }
                else
                {
                    GridValidation = true;
                }
            }
            else
            {
                IsAddTeam = true;
                GridValidation = true;
                ProductError.Text = "";
            }

        }

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {

            string StrQrySearch = "select Max(volsignupID) as PrimaryID from volsignup";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            DataTable dtcheckbox = ds.Tables[0];
            ViewState["PrimaryID"] = dtcheckbox.Rows[0]["PrimaryID"];

            string s = Session["LoginEmail"].ToString();
            lbReqfield.Text = "";
            lblErr.Text = "";


            AddedTeams();


        }

        catch (Exception ex)
        {

        }
    }

    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        IsEvent = false;
        TblTeams.Visible = false;

        if (ddYear.SelectedItem.Text != "Select Year")
        {

            if (lbReqfield.Text == "Please Select Year")
            {
                lbReqfield.Text = "";
            }
        }
        ChekBoxc();
        checkBoXChecked();
        GvVolsignup.EditIndex = -1;
        GridDisply();
        lblErr.Text = "";
        ddhrs.SelectedIndex = 0;
        AvailHrsSelection();
        if (ViewState["AvailHours"] != "")
        {
            ddhrs.SelectedValue = ViewState["AvailHours"].ToString();
        }
        checkColor();
        GridValText.Text = "";
        Gridval.Visible = false;
        ProductError.Text = "";
        lbnorec1.Text = "";
        btnAdd.Visible = false;

    }
    protected void GvVolsignup_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string sDatakey;
            int TeamIDup;
            string teamnameup;
            int year;
            int Eventid;
            sDatakey = GvVolsignup.DataKeys[e.RowIndex].Value.ToString();
            HiddenField hdchapterID = (HiddenField)GvVolsignup.Rows[e.RowIndex].FindControl("HdnChapId");
            ChapterUp = hdchapterID.Value;
            Label lbTeamId = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblTeamID");
            Label lbTeamName = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblTeamName");
            Label lbEventName = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblEventName");
            Label lbYear = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblyear");
            DropDownList productGr = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("productGroup");
            DropDownList ddproduct = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("product");
            DropDownList ddavailhr = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("ddavailhrs");
            DropDownList ddEventName = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("ddEventName");
            DropDownList ddChapterGrid = (DropDownList)GvVolsignup.Rows[e.RowIndex].FindControl("ddChaptergv");

            ViewState["VolSignuplogTeamId"] = lbTeamId.Text;
            ViewState["YearLog"] = lbYear.Text;
            ViewState["EventNamelog"] = lbEventName.Text;
            ViewState["AvailHrs"] = ddavailhr.SelectedValue;
            Session["Status"] = "Updated";

            ViewState["VolDatakey"] = sDatakey;

            if (ddChapterGrid.Visible != true)
            {
                chapterGridVal = "null";
            }
            else
            {
                chapterGridVal = ddChapterGrid.SelectedValue;
            }

            string StrQrySearch = " select * from productTeamMatrix where TeamId= " + lbTeamId.Text + " and (productgroup is not null or product is not null)";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
            DataTable dtcheckbox = new DataTable();
            dtcheckbox = ds.Tables[0];

            if (ds.Tables[0].Rows.Count > 0)
            {
                string st = Convert.ToString(dtcheckbox.Rows[0]["Product"]);
                if ((productGr.Visible = true) || (ddproduct.Visible = true))
                {
                    if (st != "Y")
                    {
                        if (productGr.SelectedItem.Text != "Select ProductGroup")
                        {
                            Isupdate = true;
                            productid1 = "null";
                            productid1 = "null";
                            product1 = "null";
                            productGroup1 = "'" + productGr.SelectedItem.Text + "'";
                            productgroupid1 = productGr.SelectedValue;

                        }
                    }
                    else
                    {
                        if ((productGr.SelectedItem.Text != "Select ProductGroup") && (ddproduct.SelectedItem.Text != "Select Product"))
                        {
                            Isupdate = true;
                            product1 = "'" + ddproduct.SelectedItem.Text + "'";
                            productid1 = ddproduct.SelectedValue;
                            productGroup1 = "'" + productGr.SelectedItem.Text + "'";
                            productgroupid1 = productGr.SelectedValue;
                        }
                        else
                        {
                            lblErr.Text = "Please Select ProductGroup and Product for this Team";
                            // Response.Write("<script>alert('Please Select ProductGroup and Product for this Team')</script>");
                            Isupdate = false;
                        }
                    }
                }
            }
            else
            {
                Isupdate = true;
                productgroupid1 = "null";
                productid1 = "null";
                productGroup1 = "null";
                product1 = "null";
            }

            if (Isupdate == true)
            {
                string ProductGroupDelVal1;
                string ProduCtDelval1;
                if (productgroupid1 == "null")
                {
                    ProductGroupDelVal1 = "productGroupid is null";
                }
                else
                {
                    ProductGroupDelVal1 = "productGroupid=" + productgroupid1 + "";
                }
                if (productid1 == "null")
                {
                    ProduCtDelval1 = "productId is null";
                }
                else
                {
                    ProduCtDelval1 = "productId=" + ddproduct.SelectedValue + "";
                }

                int cnt = 0;
                string strSql = "select count(*) from VolSignUp where volsignupId not in (" + sDatakey + ") and TeamId=" + lbTeamId.Text + " and year=" + lbYear.Text + "  and EventName='"
                    + lbEventName.Text + "'      and  MemberId=" + Session["VolunteerLoginId"] + "";
                cnt = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql).ToString());
                if (cnt > 0)
                {
                    lblErr.Text = "Record already exists";
                    //Response.Write("<script>alert('Record already exists')</script>");
                    GvVolsignup.EditIndex = -1;
                    GridDisply();
                }
                else
                {
                    string qryUpdate = "update VolSignUp set TeamID= '" + lbTeamId.Text + "',TeamName='" + lbTeamName.Text + "',"
                       + "Availhours=" + ddavailhr.SelectedValue + ",ProductGroupID=" + productgroupid1 + ",ProductId="
                        + productid1 + ",productGroup=" + productGroup1 + ",ModifyDate=Getdate(),Modifiedby=" + Session["VolunteerLoginId"] + ","
                       + "product=" + product1 + " ,Chapterid=" + chapterGridVal + " where volsignupID=" + sDatakey + "";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);

                    string qryUpdate1 = "update VolSignUp set Availhours=" + ddavailhr.SelectedValue + " where year=" + lbYear.Text + " and  MemberId=" + Session["VolunteerLoginId"] + "";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate1);


                    lbEmail.Text = "Updated successfully";
                    //Response.Write("<script>alert('Updated successfully')</script>");
                    MessageBody();
                    VolSignuplog();

                    lblErr.Text = "";
                    GvVolsignup.EditIndex = -1;
                    GridDisply();
                    AvailHrsSelection();
                    if (ViewState["AvailHours"] != "")
                    {
                        ddhrs.SelectedValue = ViewState["AvailHours"].ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void ddday_SelectedIndexChanged(object sender, EventArgs e)
    {
        IsValueCheck = true;
        this.lbPrd.Text = ((DropDownList)sender).SelectedValue;

    }
    protected void ddEventVal_SelectedIndexChanged(object sender, EventArgs e)
    {
        IsValueCheck = true;
        IsEventProd = true;
        this.lbEventId.Text = ((DropDownList)sender).SelectedValue;

    }
    protected void GvVolsignup_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GvVolsignup.EditIndex = e.NewEditIndex;
        GridDisply();
        lbPrd.Text = "";
        lbEventId.Text = "";
        lblErr.Text = "";
        lbchaptr.Text = "";
        HiddenField hdchapterID = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnChapId");
        DropDownList ddchap = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("ddChaptergv");
        if (hdchapterID.Value != "")
        {
            ddchap.Visible = true;
            lbchaptr.Text = hdchapterID.Value;
        }
        else
        {
            ddchap.Visible = false;
        }
        Label lbEventNam = (Label)GvVolsignup.Rows[e.NewEditIndex].FindControl("lblEventName");
        lbEventId.Text = lbEventNam.Text;
        HiddenField kYear = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnYear");
        Label ddyearDr = (Label)GvVolsignup.Rows[e.NewEditIndex].FindControl("lblyear");

        Label lblteamId = (Label)GvVolsignup.Rows[e.NewEditIndex].FindControl("lblTeamID");
        lbEventId.Text = lbEventNam.Text;


        string StrQrySearch = " select * from productTeamMatrix where TeamId= " + lblteamId.Text + " and (productgroup is not null or product is not null)";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        DataTable dtcheckbox = new DataTable();
        dtcheckbox = ds.Tables[0];

        if (ds.Tables[0].Rows.Count > 0)
        {
            HiddenField kproductGroup = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnProductgroup");
            DropDownList ddProductgroup = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("productGroup");
            if (kproductGroup.Value == "")
            {
            }
            else
            {
                ddProductgroup.SelectedValue = kproductGroup.Value;
                lbPrd.Text = kproductGroup.Value;
            }

            HiddenField kproduct = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnProduct");
            DropDownList ddProduct = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("product");
            if (kproduct.Value == "")
            {
            }
            else
            {
                ddProduct.SelectedValue = kproduct.Value;
            }
            string st = Convert.ToString(dtcheckbox.Rows[0]["Product"]);


            if (st != "Y")
            {
                ddProduct.Visible = false;
            }

        }
        else
        {
            DropDownList ddProductgroup1 = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("productGroup");
            ddProductgroup1.Visible = false;
            DropDownList ddProduct1 = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("product");
            ddProduct1.Visible = false;
        }
        HiddenField kAvailhrs = (HiddenField)GvVolsignup.Rows[e.NewEditIndex].FindControl("HdnAvailhrs");
        DropDownList ddhrs = (DropDownList)GvVolsignup.Rows[e.NewEditIndex].FindControl("ddavailhrs");
        if (kAvailhrs.Value == "")
        {
        }
        else
        {
            ddhrs.SelectedValue = kAvailhrs.Value;
        }

        IsEvent = false;


    }
    protected void GvVolsignup_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GvVolsignup.EditIndex = -1;
        GridDisply();
    }

    public void OnConfirm(object sender, EventArgs e)
    {
        confirmValue = Request.Form["confirm_value"];
    }
    protected void GvVolsignup_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string VolsignupId = GvVolsignup.DataKeys[e.RowIndex].Value.ToString();
        ViewState["VolDatakey"] = VolsignupId;
        if (confirmValue == "Yes")
        {
            Label lbTeamId = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblTeamID");
            Label lbYear = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblyear");
            Label lbEventname = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblEventName");
            Label lbAvaihrs = (Label)GvVolsignup.Rows[e.RowIndex].FindControl("lblhrs");
            ViewState["VolSignuplogTeamId"] = lbTeamId.Text;
            ViewState["YearLog"] = lbYear.Text;
            Session["Status"] = "Deleted";
            ViewState["EventNamelog"] = lbEventname.Text;
            ViewState["AvailHrs"] = lbAvaihrs.Text;
            VolSignuplog();
            string StrQryDel = "delete  from VolSignUp where VolSignupId=" + VolsignupId + "";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryDel);
            //Response.Write("<script>alert('Deleted successfully')</script>");
            lbEmail.Text = "Deleted successfully";
            lblErr.Text = "";
            GridDisply();
            SignupDetails();
            ChekBoxc();
            checkBoXChecked();
            checkColor();

            ddhrs.SelectedIndex = 0;
            if (ViewState["AvailHours"] != "")
            {
                ddhrs.SelectedValue = ViewState["AvailHours"].ToString();
            }

        }
        else
        {
        }

    }
    protected void ddhrs_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddhrs.SelectedItem.Text != "Select hours per week")
        {

            if (lbReqfield.Text == "Please select hours you can spend")
            {
                lbReqfield.Text = "";
            }
        }
        GvVolsignup.EditIndex = -1;
        GridDisply();
        lbnorec1.Text = "";
        GridValText.Text = "";
        Gridval.Visible = false;
        ProductError.Text = "";
        lblErr.Text = "";
    }

    protected void checkColor()
    {
        for (int i = 0; i < CheckBoxList1.Items.Count; i++)
        {
            if ((CheckBoxList1.Items[i].Enabled == false) || (CheckBoxList1.Items[i].Selected == false))
            {
                CheckBoxList1.Items[i].Attributes.Add("Style", "color: Black;");
            }
        }
    }

    protected void Gridval_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Find the DropDownList in the Row
            DropDownList ddlproductGroup = (e.Row.FindControl("ddlproductGroup") as DropDownList);
            DropDownList ddlproductDrop = (e.Row.FindControl("ddlproduct1") as DropDownList);
            string ddPGr = "select distinct Pg.Name,pg.ProductGroupId from productgroup Pg left join product p on p.productgroupid=pg.productgroupid  where pg.Eventid=" + ddEvent.SelectedValue + " and p.status='o'";
            DataSet ddPGrds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddPGr);
            ddlproductGroup.DataSource = ddPGrds;
            ddlproductGroup.DataTextField = "Name";
            ddlproductGroup.DataValueField = "ProductGroupId";
            ddlproductGroup.DataBind();
            ddlproductGroup.Items.Insert(0, "Select ProductGroup");
            HiddenField kproduct = (e.Row.FindControl("HdnProductval") as HiddenField);
            HiddenField kproductval = (e.Row.FindControl("HdnProduct") as HiddenField);
            if (kproduct.Value != "Y")
            {
                ddlproductDrop.Visible = false;
            }
            if (kproductval.Value != "Y")
            {
                ddlproductGroup.Visible = false;
            }
        }

    }
    protected void ddday1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)(((DropDownList)sender).NamingContainer);
        DropDownList ddlproductGroup = (DropDownList)gvr.FindControl("ddlproductGroup");
        HiddenField kproduct = (HiddenField)gvr.FindControl("HdnProductval");
        DropDownList ddlproduct = (DropDownList)gvr.FindControl("ddlproduct1");
        ddlproduct.Items.Clear();

        if ((ddlproductGroup.SelectedItem.Text != "Select ProductGroup") && (kproduct.Value == "Y"))
        {
            // ddlproduct.Enabled = true;
            string ddProduct = "select distinct Name,productid from Product where EventId=" + ddEvent.SelectedValue + " and ProductGroupId=" + ddlproductGroup.SelectedValue + " and status='o'";
            DataSet ddProduct1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddProduct);
            ddlproduct.DataSource = ddProduct1;
            ddlproduct.DataTextField = "Name";
            ddlproduct.DataValueField = "productid";
            ddlproduct.DataBind();
            ddlproduct.Items.Insert(0, "Select Product");
        }
        else
        {
            ddlproduct.Items.Clear();
            // ddlproduct.Enabled = false;
        }

    }
    protected void VolSignuplog()
    {
        string StrLog;
        string Chaptervdata = string.Empty;
        if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3")))
        {
            Chaptervdata = "'" + ddchapter.SelectedItem.Text + "'";

        }
        else if ((ddEvent.SelectedValue == "1") || (ddEvent.SelectedValue == "13"))
        {
            Chaptervdata = "'" + ddchapter.SelectedItem.Text + "'";
        }
        else
        {
            Chaptervdata = "null";
        }
        if (Session["Status"] == "Added")
        {
            if (ViewState["PrimaryID"].ToString() == "")
            {
                ViewState["PrimaryID"] = 0;
            }
            else
            {
            }

            StrLog = "select * from volsignup where volsignupId> " + ViewState["PrimaryID"] + " and TeamID in " + ViewState["VolSignuplogTeamId"] + "";

        }
        else
        {
            StrLog = "select * from volsignup where volsignupId=" + ViewState["VolDatakey"] + "";
        }

        DataSet dsLog = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrLog);
        DataTable Dtlog = dsLog.Tables[0];
        if (Dtlog.Rows.Count > 0)
        {
            for (int i = 0; i <= Dtlog.Rows.Count - 1; i++)
            {

                if (WCNTValog != string.Empty)
                {

                    WCNTValog = WCNTValog + " | " + Dtlog.Rows[i]["TeamName"] + "," + Dtlog.Rows[i]["ProductGroup"] + "," + Dtlog.Rows[i]["Product"];

                }
                else
                {

                    WCNTValog = Dtlog.Rows[i]["TeamName"] + "," + Dtlog.Rows[i]["ProductGroup"] + "," + Dtlog.Rows[i]["Product"];

                }
            }


        }
        string StrQryInsert = "INSERT INTO VolSignUpLog (Year,EventName,ChapterName,MemberId,Teams,Action,EmailId,MailStatus,AvailHours,CreateDate,CreatedBy) VALUES (" + ViewState["YearLog"] + ",'" + ViewState["EventNamelog"] + "'," + Chaptervdata + ","
            + Session["VolunteerLoginId"] + ",'" + WCNTValog + "','" + Session["Status"] + "','" + ViewState["EmailID"] + "','" + MailStatus + "'," + ViewState["AvailHrs"] + ",Getdate(), " + Session["VolunteerLoginId"] + ")";
        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryInsert);
    }
    protected void BtnContinue_Click(object sender, EventArgs e)
    {


        foreach (ListItem li in CheckBoxList1.Items)
        {
            if (li.Selected == true && li.Enabled == true)
            {
                listChecked = true;
                break;
            }

        }
        if (listChecked == true)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtoAddTeams();", true);

        }
        else
        {
            redirectingPage();
        }
    }
    protected void redirectingPage()
    {

        if (Session["entryToken"].ToString().ToUpper() == "VOLUNTEER")
        {
            Response.Redirect("VolunteerFunctions.aspx");
        }
        else if (Session["entryToken"].ToString().ToUpper() == "DONOR")
        {
            Response.Redirect("DonorFunctions.aspx");
        }
        else if (Session["entryToken"].ToString().ToUpper() == "PARENT")
        {
            Response.Redirect("MainChild.aspx");
        }
        else
        {
            Response.Redirect("~/Maintest.aspx");
        }
    }
    protected void lbtnVolunteerFunctions_Click(object sender, EventArgs e)
    {
        if (Session["entryToken"].ToString().ToUpper() == "DONOR")
        {
            Response.Redirect("DonorFunctions.aspx");
        }
        else if (Session["entryToken"].ToString().ToUpper() == "PARENT")
        {
            Response.Redirect("UserFunctions.aspx");
        }
        else
        {
            Response.Redirect("VolunteerFunctions.aspx");
        }
    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ChekBoxc();
        //checkBoXChecked();
        if (ddchapter.SelectedItem.Text != "Select Chapter")
        {
            lbReqfield.Text = "";
        }
        AvailHrsSelection();
        TblTeams.Visible = false;
        btnAdd.Visible = false;
        lblErr.Text = "";
    }
    protected void btncont_Click(object sender, EventArgs e)
    {

        if ((ddYear.SelectedItem.Text != "Select Year") && (ddEvent.SelectedItem.Text != "Select Event") && (ddhrs.SelectedItem.Text != "Select hours per week"))
        {
            if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3") || (ddEvent.SelectedValue == "13")))
            {
                if (ddchapter.SelectedItem.Text != "Select Chapter")
                {
                    btnAdd.Visible = true;
                    ChekBoxc();
                    checkBoXChecked();
                    TblTeams.Visible = true;
                }
                else
                {
                    lbReqfield.Text = "Please Select Chapter";
                }
            }
            else
            {
                ChekBoxc();
                checkBoXChecked();
                TblTeams.Visible = true;
                btnAdd.Visible = true;
            }
        }
        else
        {
            if (ddYear.SelectedItem.Text == "Select Year")
            {
                lbReqfield.Text = "Please Select Year";
                lbReqfield.Visible = true;
            }
            else if (ddEvent.SelectedItem.Text == "Select Event")
            {
                lbReqfield.Visible = true;
                lbReqfield.Text = "Please Select Event";
            }
            else if (ddhrs.SelectedItem.Text == "Select hours per week")
            {
                lbReqfield.Visible = true;
                lbReqfield.Text = "Please select hours you can spend";
            }
            else
            {
                lbReqfield.Text = "";
            }

        }

    }
    protected void GvVolsignup_PageIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GvVolsignup_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GvVolsignup.PageIndex = e.NewPageIndex;
        //DataSet dvEmployee = GridDisply();
        //GvVolsignup.DataSource = dvEmployee;
        //GvVolsignup.DataBind();
        GridDisply();

    }
    protected void lnkHelpGuide1_Click(object sender, EventArgs e)
    {
        //string url = "VolunteerSignupGuide.aspx?value=" + ddEvent.SelectedItem.Value ;
        //string s = "newWindow = window.open('" + url + "', 'VolunteerSignupGuide Help', 'width=970,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no');";
        ClientScript.RegisterStartupScript(this.GetType(), "script", "PopupPicker();", true);
    }
    protected void hiddenbtn_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Write("before update");

            redirectingPage();

        }
        catch (Exception ex)
        {

        }
    }
    protected void btnOptionContinue_Click(object sender, EventArgs e)
    {
        Session["VolunteerLoginId"] = null;
        spErr.InnerText = "";
        string strCmdText = "";
        if (rbtnOption.SelectedIndex == 0)
        {
            strCmdText = "select case when Ind.donortype='IND' then isnull(Ind.Automemberid,'') else isnull(Relationship,'') end from indspouse Ind where automemberid=" + Session["LoginId"];
        }
        else if (rbtnOption.SelectedIndex == 1)
        {
            strCmdText = "select case when Ind.donortype='IND' then isnull((select Automemberid from indspouse Where RelationShip=" + Session["LoginId"] +"),'')  else  AutoMemberId END from indspouse Ind where Automemberid=" + Session["LoginId"];
        }
        else if (rbtnOption.SelectedIndex == 2)
        {
            redirectingPage();
        }
        else
        {
            spErr.InnerText = "Please select one option";
            return;
        }
        string strLoginId="";
        strLoginId = Convert.ToString(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strCmdText));
        if (strLoginId.Trim().Length != 0)
        {
            Session["VolunteerLoginId"] = strLoginId;
            divOption.Visible = false;
            divVolSignUp.Visible = true;
            SignupDetails();
        }
        else
        {
            //warning msg
                        spErr.InnerText = "LoginId doesn't exists for the selected option";
        }
       
    }
}