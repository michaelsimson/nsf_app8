<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="SupportTracking.aspx.cs" Inherits="SupportTracking" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 520px;
            height: 200px;
            top: 55%;
            left: 50%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }
    </style>
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script>
        function submitform() {
            var chapter, EmailId, emailExp, event;
            event = document.getElementById("<%= ddlEvent.ClientID %>").value;
            chapter = document.getElementById("<%= ddlChapter.ClientID %>").value;
            EmailId = document.getElementById("<%= txtEmail.ClientID %>").value;
            emailExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([com\co\.\in])+$/; // to validate email id
            if (EmailId == '') {
                alert("Enter Email");
                return false;
            }
            if (chapter == -1) {
                alert("Select Chapter");
                return false;
            }
            if (event == -1) {
                alert("Select Event");
                return false;
            }

            if (EmailId != '') {
                if (!EmailId.match(emailExp)) {
                    alert("Invalid Email Id");
                    return false;
                }
            }
            return true;
        }

        function PopupPicker() {
            var PopupWindow = null;
            settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('SupportTrackingHelpGuide.aspx', 'CalSignUp Help', settings);
            PopupWindow.focus();
        }

        function ConfirmMessageToUpdate() {
            if (confirm("This appears to be a duplicate.  Do you still want to send it anyway.")) {

                document.getElementById('<%= btnUpdateTicket.ClientID%>').click();

            }

        }
        function ConfirmUserMessageToUpdate() {
            if (confirm("This appears to be a duplicate.  Do you still want to send it anyway.")) {


                document.getElementById('<%= btnUpdateUserTicket.ClientID%>').click();

            }

        }
        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }

        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvUpdateTicket").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvUpdateTicket").hide();
        }
        $(document).on("click", '#<%=hlBackToList.ClientID%>', function (e) {
            window.close();
        });
    </script>
    <asp:Label ID="lblerr" runat="server"></asp:Label>
    <asp:Button ID="btnUpdateTicket" Style="display: none;" runat="server" OnClick="btnUpdateTicket_Click" />

    <asp:Button ID="btnUpdateUserTicket" Style="display: none;" runat="server" OnClick="btnUpdateUserTicket_Click" />
    <%--<asp:GridView ID="gvEmails" runat="server" OnRowDataBound="OnRowDataBound" DataKeyNames="MessageNumber"
    AutoGenerateColumns="false">
   <Columns>
        <asp:BoundField HeaderText="From" DataField="From" HtmlEncode="false" />
        <asp:TemplateField HeaderText="Subject">
            <ItemTemplate>
                <asp:LinkButton ID="lnkView" runat="server" Text='<%# Eval("Subject") %>' />
                <span class="body" style="display: none">
                    <%# Eval("Body") %></span>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="Date" DataField="DateSent" />
        <asp:TemplateField ItemStyle-CssClass="Attachments">
            <ItemTemplate>
                <asp:Repeater ID="rptAttachments" runat="server">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkAttachment" runat="server" OnClick="Download" Text='<%# Eval("FileName") %>' />
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <br>
                    </SeparatorTemplate>
                </asp:Repeater>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>--%>
    <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>


    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Support Ticket Tracking
             <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table style="width: 100%" runat="server" id="tblrbtn">

        <tr>
            <td style="width: 50%">
                <asp:RadioButton ID="rbtn1" runat="server"
                    Text="New Ticket" GroupName="Ticket" AutoPostBack="true" Checked="true" OnCheckedChanged="rbtn_CheckedChanged" /></td>
            <td style="width: 50%">
                <asp:RadioButton ID="rbtn2" AutoPostBack="true" OnCheckedChanged="rbtn_CheckedChanged" runat="server" Text="Existing Ticket" GroupName="Ticket" /></td>
        </tr>
    </table>
    <asp:HyperLink ID="rptA" NavigateUrl="#bottom" CssClass="btn_02" Visible="false" runat="server">Go to the bottom to respond</asp:HyperLink>
    <%-- <asp:HyperLink ID="hlBackToList" Visible="false" runat="server" Style="cursor: pointer; float: right;" CssClass="btn_02" Text="Close this Pop-Up window"></asp:HyperLink>--%>
    <asp:LinkButton ID="hlBackToList" Visible="false" runat="server" Style="cursor: pointer; float: right;" Text="Close this Pop-Up window"></asp:LinkButton>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <asp:Repeater runat="server" ID="rptTicket">
        <ItemTemplate>
            <table cellspacing="0" cellpadding="5" width="90%" align="center" border="1">
                <!-- <asp:Label runat="server" ID="Label4" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.EntryEmail") %>'></asp:Label> -->
                <tr>
                    <td style="width: 100px">EntryEmail</td>
                    <td>
                        <asp:Label runat="server" ID="Label8" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.EntryEmail") %>'></asp:Label>
                    </td>
                    <td>First Name </td>
                    <td>
                        <asp:Label runat="server" ID="Label1" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.FirstName") %>'></asp:Label>
                    </td>
                    <td>Last Name</td>
                    <td>
                        <asp:Label runat="server" ID="Label2" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.LastName") %>'></asp:Label>
                    </td>
                    <td>HPhone</td>
                    <td>
                        <asp:Label runat="server" ID="Label20" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.HPhone") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">Chapter</td>
                    <td>
                        <asp:Label runat="server" ID="Label3" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Chapter") %>'></asp:Label>
                    </td>
                    <td>Date</td>
                    <td>
                        <asp:Label runat="server" ID="Label7" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.CreateDate") %>'></asp:Label>
                    </td>
                    <td>Event</td>
                    <td>
                        <asp:Label runat="server" ID="Label14" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Event") %>'></asp:Label>
                    </td>
                    <td>CPhone</td>
                    <td>
                        <asp:Label runat="server" ID="Label9" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.CPhone") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Subject</td>
                    <td colspan="7">
                        <asp:Label runat="server" ID="Label5" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Subject") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Message</td>
                    <td colspan="7">
                        <asp:Label runat="server" ID="Label6" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Message") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>To</td>
                    <td colspan="7">
                        <asp:Label runat="server" ID="LblToList" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.PName") %>'>; </asp:Label>
                        <asp:Label runat="server" ID="LblBMemberEmail" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.BName") %>'>; </asp:Label>
                        <asp:Label runat="server" ID="LblCMemberEmail" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.CName") %>'>; </asp:Label>
                           <asp:Label runat="server" ID="lblChapterCEmail" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCName") %>'>; </asp:Label>
                        <%--   <asp:Label runat="server" ID="lblUName" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.UName") %>'>; </asp:Label>--%>

                    </td>
                </tr>

                <tr>
                    <td>CCMails </td>
                    <td colspan="7">
                        <asp:Label runat="server" ID="lblCCEmails" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.CCEmails") %>'>; </asp:Label>
                    </td>
                </tr>

                <tr>
                    <td>Responded By</td>
                    <td colspan="7">
                        <asp:Label runat="server" ID="LblRespondedBy" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.RName") %>'></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td>Status</td>
                    <td colspan="7">
                        <asp:Label runat="server" ID="LblStatus" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.Status") %>'></asp:Label>
                    </td>
                </tr>



            </table>
            <br />
        </ItemTemplate>
    </asp:Repeater>

    <table runat="server" id="tblNew" border="0" cellpadding="1" cellspacing="0">

        <tr>
            <td style="width: 151px">Email</td>
            <td></td>
            <td>
                <table border="0" cellpadding="1" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtEmail" runat="server" Width="175px"></asp:TextBox>

                        </td>
                        <td>&nbsp;First Name</td>
                        <td></td>
                        <td>
                            <asp:TextBox ID="txtFName" runat="server" Width="125px"></asp:TextBox></td>
                        <td>&nbsp;Last Name</td>
                        <td></td>
                        <td>
                            <asp:TextBox ID="txtLName" runat="server" Width="125px"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 145px">Home Phone</td>
            <td></td>
            <td>
                <table border="0" cellpadding="1" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txtHPhone" runat="server" Width="175px"></asp:TextBox>
                        </td>
                        <td>&nbsp;Cell Phone</td>
                        <td></td>
                        <td>
                            <asp:TextBox ID="txtCPhone" runat="server" Width="125px"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
            <td>
                <a href="javascript:PopupPicker();">Help Guide</a>
            </td>
        </tr>
        <tr>
            <td style="width: 150px">Chapter</td>
            <td></td>
            <td>
                <asp:DropDownList ID="ddlChapter" runat="server" Width="150px"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>Type of Customer</td>
            <td></td>
            <td>
                <asp:DropDownList ID="ddlEntryCode" runat="server" Width="150px">
                    <asp:ListItem Value="P">Parent</asp:ListItem>
                    <asp:ListItem Value="D">Donor</asp:ListItem>
                    <asp:ListItem Value="S">Sponsor</asp:ListItem>
                    <asp:ListItem Value="V">Volunteer</asp:ListItem>
                    <asp:ListItem Value="S">Student</asp:ListItem>
                    <asp:ListItem Value="G">Game</asp:ListItem>
                    <asp:ListItem Value="O">Other</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 145px">Event</td>
            <td></td>
            <td>

                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server" Width="150px"
                                OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="-1">Select Event</asp:ListItem>
                                <asp:ListItem Value="2">Regional Contests</asp:ListItem>
                                <asp:ListItem Value="1">Finals</asp:ListItem>
                                <asp:ListItem Value="3">Workshop</asp:ListItem>
                                <asp:ListItem Value="13">Online Coaching</asp:ListItem>
                                <asp:ListItem Value="4">Game</asp:ListItem>
                                <asp:ListItem Value="19">PrepClub</asp:ListItem>
                                <asp:ListItem Value="20">Online workshop</asp:ListItem>
                                <asp:ListItem Value="0">Other</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>


                            <table runat="server" id="trproduct" border="0" cellpadding="1" cellspacing="0">
                                <tr>
                                    <td>
                                        <div>
                                            <div style="float: left;">
                                                <div style="float: left; margin-left: 5px;">Product Group</div>
                                                <div style="float: left; margin-left: 15px;">
                                                    <asp:DropDownList ID="ddlProductGroup" runat="server" Width="130px"
                                                        OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div style="float: left;">
                                                <div style="float: left; margin-left: 5px;">Product</div>
                                                <div style="float: left; margin-left: 20px;">
                                                    <asp:DropDownList ID="ddlProduct" runat="server" Width="130px"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <%-- <td style="">&nbsp;Product Group&nbsp;&nbsp;&nbsp;</td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList ID="ddlProductGroup" runat="server" Width="130px"
                                            OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList></td>

                                    <td style="">&nbsp;Product&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td></td>
                                    <td>
                                        <asp:DropDownList ID="ddlProduct" runat="server" Width="130px"></asp:DropDownList></td>--%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>Subject</td>
            <td></td>
            <td>
                <%-- <asp:DropDownList ID="ddlSubject" runat="server" Width=150px>
        <asp:ListItem Value="2">Regional Contests</asp:ListItem>
        <asp:ListItem Value="1">Finals</asp:ListItem>
        <asp:ListItem Value="3">Workshop</asp:ListItem>
        <asp:ListItem Value="13">Online Coaching</asp:ListItem>
        <asp:ListItem Value="-1">PrepClub</asp:ListItem>
         <asp:ListItem Value="0">Other</asp:ListItem>
    </asp:DropDownList>--%>
                <asp:TextBox ID="txtSubject" Width="400px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div style="width: 818px;">
                    <asp:Label ID="lblemailbody" Text="To get a quick response, please click on the <a href='javascript:PopupPicker();'>Help Guide</a>.  Otherwise, it may take 2 to 3 days for a volunteer to respond you.  In the space provided below, please explain in detail what the problem is, whom you talked to, how many times you talked, what the outcome is, and so on.  If relevant, please explain the documents, manuals, FAQ, etc., which you consulted."
                        runat="server"></asp:Label>
                </div>

                <CKEditor:CKEditorControl ID="txtEmailBody" runat="server" Width="800px"></CKEditor:CKEditorControl>
            </td>
        </tr>
        <tr>
            <td>Priority</td>
            <td></td>
            <td>
                <asp:DropDownList ID="ddlPriority" runat="server" Width="180px">
                    <asp:ListItem Value="Normal">Normal</asp:ListItem>
                    <asp:ListItem Value="High">High</asp:ListItem>
                    <asp:ListItem Value="Urgent">Urgent</asp:ListItem>

                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trVol" runat="server" visible="false">
            <td colspan="3">
                <table cellspacing="0">
                    <tr>
                        <td style="width: 150px">Assign To</td>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlAssignTo" runat="server" Width="150px"></asp:DropDownList></td>
                        <td></td>
                        <td>Email To "Assigned To:"</td>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlEmailAssignTo" runat="server" Width="100px">
                                <asp:ListItem>N</asp:ListItem>
                                <asp:ListItem>Y</asp:ListItem>

                            </asp:DropDownList></td>
                        <td></td>
                        <td><span id="spnEmailToCustomer" runat="server">Email To Customer</span></td>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlEmailToCustomer" runat="server" Width="100px" OnSelectedIndexChanged="ddlKeepRivate_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem>N</asp:ListItem>
                                <asp:ListItem>Y</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td><span id="spnKeepPrivate" runat="server">Keep Private</span></td>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlKeepRivate"
                                runat="server" Width="150px">
                                <asp:ListItem>Y</asp:ListItem>
                                <asp:ListItem>N</asp:ListItem>
                            </asp:DropDownList></td>
                        <td></td>
                        <td><span id="spnCC" runat="server">CC (',' Comma Separated): </span></td>
                        <td></td>
                        <td colspan="5" rowspan="2">
                            <asp:TextBox ID="txtcc" runat="server" TextMode="MultiLine" Height="50"
                                Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td></td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" Width="150px" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="0">Select Status</asp:ListItem>
                                <asp:ListItem>Resolved</asp:ListItem>
                                <asp:ListItem>Forwarded</asp:ListItem>
                                <asp:ListItem>Investigating</asp:ListItem>
                                <asp:ListItem>Out of town</asp:ListItem>
                                <asp:ListItem>Escalated</asp:ListItem>
                                <asp:ListItem>Pending Customer Input</asp:ListItem>
                                <asp:ListItem>Re-opened</asp:ListItem>
                                <asp:ListItem>Closed</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit"
                    OnClientClick="return submitform();" OnClick="btnSubmit_Click" />
                &nbsp;&nbsp;
        <asp:Button ID="btnVolSubmit" Visible="false" runat="server" Text="Submit"
            OnClick="btnVolSubmit_Click" />
                &nbsp;&nbsp;
        <asp:Button ID="btnCancel" runat="server" Text="Close"
            OnClick="btnCancel_Click" />

                <a name="bottom" id="bottom"></a>

                &nbsp;</td>
        </tr>

        <tr>
            <td colspan="3" align="center">
                <!-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
        ControlToValidate="txtEmail" ErrorMessage="Enter Email,"></asp:RequiredFieldValidator> 
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlChapter" ErrorMessage="Select Chapter" InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlEvent" ErrorMessage="Select Event" InitialValue="-1" ></asp:RequiredFieldValidator> -->
            </td>
        </tr>

        <tr>
            <td>Ticket # </td>
            <td></td>
            <td>
                <asp:Label ID="lblTicketNo" runat="server"></asp:Label>
                <asp:Label ID="Tnew" Visible="false" Text="Y" runat="server" />
                <asp:Label ID="ProfileFlag" Visible="false" Text="N" runat="server" />
                <asp:Label ID="TUser" Visible="false" Text="O" runat="server" />
                <asp:Label ID="lblLoginMemberid" Visible="false" runat="server" />

            </td>
        </tr>

    </table>

    <center>
        <table border="0" runat="server" id="trFilter" visible="false" cellpadding="3" cellspacing="0">
            <tr>
                <td colspan="2"></td>
                <td colspan="2">
                    <div id="divChp" runat="server" visible="false">
                        <center>
                            Chapter : 
                            <asp:DropDownList ID="ddlFChapter" runat="server">
                            </asp:DropDownList></center>
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Event</td>
                <td>
                    <asp:DropDownList ID="ddlFEvent" runat="server" Width="100px"
                        OnSelectedIndexChanged="ddlFEvent_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="-1">All</asp:ListItem>
                        <asp:ListItem Value="2">Regional Contests</asp:ListItem>
                        <asp:ListItem Value="1">Finals</asp:ListItem>
                        <asp:ListItem Value="3">Workshop</asp:ListItem>
                        <asp:ListItem Value="13">Online Coaching</asp:ListItem>
                        <asp:ListItem Value="4">Game</asp:ListItem>
                        <asp:ListItem Value="19">PrepClub</asp:ListItem>
                        <asp:ListItem Value="20">Online workshop</asp:ListItem>
                        <asp:ListItem Value="0">Other</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <table runat="server" id="trFproduct" border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>&nbsp; Product Group</td>

                            <td>
                                <asp:DropDownList ID="ddlFProductGroup" runat="server" Width="100px"
                                    OnSelectedIndexChanged="ddlFProductGroup_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList></td>

                            <td>Product</td>

                            <td>
                                <asp:DropDownList ID="ddlFProduct" runat="server" Width="100px"></asp:DropDownList></td>


                        </tr>
                    </table>

                </td>
                <td>
                    <table runat="server" id="Table1" border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td>Status</td>
                            <td>
                                <asp:DropDownList ID="DDLStatusFilter" runat="server" Width="100px" AutoPostBack="true">
                                    <asp:ListItem Value="0">Select Status</asp:ListItem>
                                    <asp:ListItem Value="-1">All</asp:ListItem>
                                    <asp:ListItem>Open</asp:ListItem>
                                    <asp:ListItem>Resolved</asp:ListItem>
                                    <asp:ListItem>Forwarded</asp:ListItem>
                                    <asp:ListItem>Investigating</asp:ListItem>
                                    <asp:ListItem>Out of town</asp:ListItem>
                                    <asp:ListItem>Escalated</asp:ListItem>
                                    <asp:ListItem>Pending Customer Input</asp:ListItem>
                                    <asp:ListItem>Re-opened</asp:ListItem>
                                    <asp:ListItem>Closed</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>Priority</td>

                            <td>
                                <asp:DropDownList ID="DDLPriorityFilter" runat="server" Width="100px">
                                    <asp:ListItem Value="0">Select Priority</asp:ListItem>
                                    <asp:ListItem Value="-1">All</asp:ListItem>
                                    <asp:ListItem Value="Normal">Normal</asp:ListItem>
                                    <asp:ListItem Value="High">High</asp:ListItem>
                                    <asp:ListItem Value="Urgent">Urgent</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:Button ID="btnContinue" runat="server" Text="Continue"
                        OnClick="btnContinue_Click" /></td>
            </tr>
        </table>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div id="dvSupportFilters" runat="server" visible="false" align="center">
            <table align="center">
                <tr>

                    <td>Event </td>
                    <td>
                        <asp:DropDownList ID="DDLEventFilter" runat="server" Width="130px"
                            AutoPostBack="true" OnSelectedIndexChanged="DDLEventFilter_SelectedIndexChanged">
                            <asp:ListItem Value="-1">All</asp:ListItem>
                            <asp:ListItem Value="2">Regional Contests</asp:ListItem>
                            <asp:ListItem Value="1">Finals</asp:ListItem>
                            <asp:ListItem Value="3">Workshop</asp:ListItem>
                            <asp:ListItem Value="13">Online Coaching</asp:ListItem>
                            <asp:ListItem Value="4">Game</asp:ListItem>
                            <asp:ListItem Value="19">PrepClub</asp:ListItem>
                            <asp:ListItem Value="20">Online workshop</asp:ListItem>
                            <asp:ListItem Value="0">Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>Product Group</td>
                    <td>
                        <asp:DropDownList ID="DDLProductGroupFilter" runat="server" Width="130px"
                            AutoPostBack="true" OnSelectedIndexChanged="DDLProductGroupFilter_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>Product</td>
                    <td>
                        <asp:DropDownList ID="DDLProductFilter" runat="server" Width="130px"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="BtnFilterSubmit" runat="server" Text="Submit" /></td>
                </tr>
            </table>
        </div>
        <asp:Label ID="lblError" runat="server" ForeColor="#FF6600"></asp:Label>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <asp:Label ID="lblTitle" Visible="false" runat="server" Style="font-weight: bold;">Table 1: Ticket History</asp:Label>
        </div>
        <div style="clear: both;"></div>
        <div id="dvSortingGrid" align="left" runat="server" visible="false">
            <div style="float: left; font-weight: bold; width: 35px;">Sort :</div>
            <asp:DropDownList ID="DDlGridSorting" runat="server" Width="150px"
                AutoPostBack="true" OnSelectedIndexChanged="DDlGridSorting_SelectedIndexChanged">
                <asp:ListItem Value="By Ticket Number" Selected="True">By Ticket Number</asp:ListItem>
                <asp:ListItem Value="By Status">By Status</asp:ListItem>
                <asp:ListItem Value="By Outstanding Time"> By Outstanding Time</asp:ListItem>
                <asp:ListItem Value="By Priority">By Priority</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>

        <div align="center">
            <center>

                <asp:GridView ID="GVSupport" Width="1000px" AutoGenerateColumns="False" runat="server"
                    AllowPaging="True" PageSize="100" OnPageIndexChanging="GVSupport_PageIndexChanging" HeaderStyle-BackColor="#ffffcc">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="Ticket_ID" HeaderText="Ticket_ID" />
                        <asp:BoundField DataField="OpenedDate" HeaderText="Opened Date" />
                        <asp:BoundField DataField="LastResponded" HeaderText="Last Responded" />
                        <asp:BoundField DataField="Priority" HeaderText="Priority" />
                        <asp:BoundField DataField="OFName" HeaderText="FirstName" />
                        <asp:BoundField DataField="OLName" HeaderText="LastName" />
                        <asp:TemplateField HeaderText="Details">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkViewDetails" Text='View' NavigateUrl='<%# GetUrl(Eval("Ticket_ID"))%>' runat="server"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Subject" HeaderText="Subject" />
                        <asp:BoundField DataField="CurrentStatus" HeaderText="Status" />
                        <asp:BoundField DataField="OEmail" HeaderText="EntryEmail" />
                        <asp:BoundField DataField="Thread" HeaderText="Total" />
                        <asp:BoundField DataField="Chapter" HeaderText="Chapter" />
                    </Columns>
                    <EmptyDataTemplate>
                        Ticket_ID
                    </EmptyDataTemplate>
                </asp:GridView>
            </center>
        </div>
    </center>

    <div id="dialog" style="display: none">
        <span id="body"></span>
        <br />
        <span id="attachments"></span>
    </div>
    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>

    <div id="dvUpdateTicket" class="web_dialog">
        <center>
            <div style="margin-top: 50px;">
                <asp:Label ID="lblUpdateOpt" runat="server" ForeColor="#FF6600">This appears to be a duplicate. Please select an option.</asp:Label>
            </div>
        </center>
        <center>
            <table id="TblUpdateTicket" runat="server" visible="true">
                <tr>
                    <td>
                        <asp:RadioButton ID="RbtnForward" AutoPostBack="true" Text="Not a duplicate. Please forward" runat="server" GroupName="Col" OnCheckedChanged="RbtnForward_CheckedChanged" /></td>
                    <td>
                        <asp:RadioButton ID="RbtnNoForward" AutoPostBack="true" Text="Replace the old one" runat="server" GroupName="Col" OnCheckedChanged="RbtnNoForward_CheckedChanged" /></td>
                    <td>
                        <asp:RadioButton ID="RbtnIgnore" AutoPostBack="true" Text="Discard this email" runat="server" GroupName="Col" OnCheckedChanged="RbtnIgnore_CheckedChanged" /></td>
                </tr>
            </table>
        </center>
    </div>

    <input type="hidden" id="hdnTicketID" value="0" runat="server" />
    <input type="hidden" id="hdnSupportID" value="0" runat="server" />
    <input type="hidden" id="hdnUMemberID" value="0" runat="server" />
    <input type="hidden" id="hdnUpdateFlag" value="0" runat="server" />

    <input type="hidden" id="hdnAssignTo" value="0" runat="server" />
    <input type="hidden" id="hdnAssignToFlag" value="0" runat="server" />

    <input type="hidden" id="hdnUpdateTicketID" value="0" runat="server" />

    <input type="hidden" id="hdnRMemberID" value="0" runat="server" />
    <input type="hidden" id="hdnIsViewParent" value="0" runat="server" />
    <input type="hidden" id="hdnStatus" value="0" runat="server" />

    <input type="hidden" id="hdnIsProduct" value="0" runat="server" />
    <input type="hidden" id="hdnIsProductGroup" value="0" runat="server" />

</asp:Content>

