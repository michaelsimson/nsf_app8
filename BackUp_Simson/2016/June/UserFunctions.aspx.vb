Imports System.Data.SqlClient
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Data

Namespace VRegistration

    Partial Class UserFunctions
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Session("LoggedIn") = "true"
            'Session("CustIndID") = 380

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            
            Try
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                If SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(ChildNumber) from Child Where MemberID = " & Session("CustIndID") & " and (select COUNT(eventID) from Event where EventId=18 and Status = 'O') = 1") > 0 Then 'Select count(ChildNumber) from Contestant Where ParentID = " & Session("CustIndID") & " and PaymentReference is not null and ContestYear = YEAR(GETDATE()) and EventId=1 and (select COUNT(eventID) from Event where EventId=18 and Status = 'O')
                    hlinkExcelathon1.Enabled = True
                    hlinkExcelathon.Enabled = True
                    hlinkExcelathonReg.Enabled = True
                End If
                If SqlHelper.ExecuteScalar(conn, CommandType.Text, "select COUNT(eventID) from Event where EventId=5 and Status = 'O'") > 0 Then 'Select count(ChildNumber) from Contestant Where ParentID = " & Session("CustIndID") & " and PaymentReference is not null and ContestYear = YEAR(GETDATE()) and EventId=1 and (select COUNT(eventID) from Event where EventId=18 and Status = 'O')
                    hlinkWalkathon.Enabled = True
                End If
                If SqlHelper.ExecuteScalar(conn, CommandType.Text, "select COUNT(ChildNumber) from Child WHERE Grade>=4 AND MemberID=" & Session("CustIndID")) > 0 Then 'Select count(ChildNumber) from Contestant Where ParentID = " & Session("CustIndID") & " and PaymentReference is not null and ContestYear = YEAR(GETDATE()) and EventId=1 and (select COUNT(eventID) from Event where EventId=18 and Status = 'O')
                    lbtnSATTesting.Visible = True
                End If
                conn.Close()
                If Not IsPostBack Then
                    GetCCDetails()
                End If
            Catch ex As Exception
                '   Response.Write(ex.ToString)
            End Try
        End Sub

        Sub GetCCDetails()
            Dim cmdText As String
            cmdText = "select C.State from Chapter C inner join IndSpouse I on I.Chapter=C.ChapterCode where I.Automemberid=" & Session("CustIndID")
            Dim strChapter As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
            ViewState("ParentChpState") = strChapter

            cmdText = "  select Chapter,FirstName + ' ' + LastName CCName,Email,isnull(HPhone,CPhone) Phone  from indspouse where AutoMemberId in ( select distinct MemberId from Volunteer V inner join Chapter C "
            cmdText = cmdText & " on V.ChapterId=C.ChapterId where V.RoleId=5 and C.ChapterCode= (select Chapter from IndSpouse where Automemberid=" & Session("CustIndID") & "))"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
            CCDetails.Visible = False
            If ds.Tables(0).Rows.Count > 0 Then
                CCDetails.Visible = True
                Dim dr As DataRow = ds.Tables(0).Rows(0)

                spChapter.InnerText = dr("Chapter")
                spCoordinator.InnerText = dr("CCName")
                spEmail.InnerText = dr("Email")
                spPhone.InnerText = dr("Phone")
            End If
        End Sub
        'Private Sub hlinkLogout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hlinkLogout.Click
        '    Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
        '    Session.Abandon()
        '    Response.Redirect(homeURL)
        'End Sub

        Protected Sub hlinkNationalRegistration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkNationalRegistration.Click
            trMessage.Visible = False
            Session("EventID") = 2
            Session("PanelFrom") = "RegCont"
            Dim eventid As Integer

            'Prasad change 12-22-08 
            eventid = EventIDCheck.GetEventIDFromDB(2) '   GetEventIDFromDB(2) ' SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEvent")

            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                Session("EventYear") = EventYear
                conn.Close()
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "Regional contests are not currently open for online registration."
            End If
        End Sub

        Protected Sub hlnkNationFinals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkNationFinals.Click
            trMessage.Visible = False
            Session("EventID") = 1
            Session("PanelFrom") = "Finals"
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(1)

            'Dim eventid As Integer
            'Dim conn As New SqlConnection(Application("ConnectionString"))
            'eventid = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEvent")

            'Prasad change 12-22-08
            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                Session("EventYear") = EventYear
                conn.Close()
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "National Finals are not currently open for online registration."
            End If
        End Sub
        Protected Sub lnkViewCCalendar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewCCalendar.Click
            If Not ViewState("ParentChpState") Is Nothing Then
                Response.Write("<script language='javascript'>window.open('http://www.northsouth.org/public/USContests/Regionals/calendar.aspx?state=" & ViewState("ParentChpState") & "','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=0');</script> ")

                '                Response.Redirect("http://www.northsouth.org/public/USContests/Regionals/calendar.aspx?state=" & ViewState("ParentChpState"))
            End If
        End Sub

        Protected Sub lnkWrkShopCalendar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkWrkShopCalendar.Click
            If Not ViewState("ParentChpState") Is Nothing Then
                Response.Write("<script language='javascript'>window.open('http://www.northsouth.org/public/uscontests/workshops/workshop.aspx?state=" & ViewState("ParentChpState") & "','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=0');</script> ")
                'Response.Redirect("http://www.northsouth.org/public/uscontests/workshops/workshop.aspx?state=" & ViewState("ParentChpState"))
            End If
        End Sub

        Protected Sub hlinkRegisterForWorkshop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkRegisterForWorkshop.Click
            trMessage.Visible = False
            Session("EventID") = 3
            Session("PanelFrom") = "Workshop"
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(3)

            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                Session("EventYear") = EventYear
                conn.Close()
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "Workshops are not currently open for online registration."
            End If
        End Sub


        Protected Sub hlinkRegisterForPrepClub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkRegisterForPrepClub.Click
            trMessage.Visible = False
            Session("EventID") = 19
            Session("PanelFrom") = "PrepClub"
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(19)

            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                Session("EventYear") = EventYear
                conn.Close()
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "Prep Clubs are not currently open for online registration."
            End If
        End Sub

        Protected Sub hlinkRegisForOnlneWksop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkRegisForOnlneWksop.Click
            trMessage.Visible = False
            Session("EventID") = 20
            Session("PanelFrom") = "OnlineWorkhop"
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(20)

            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                Session("EventYear") = EventYear
                conn.Close()
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "Online Workshops are not currently open for online registration."
            End If
        End Sub
        Protected Sub hlinkRegisterForGame_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkRegisterForGame.Click
            trMessage.Visible = False
            Session("EventID") = 4
            Session("PanelFrom") = "Games"
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(4)
            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                conn.Close()
                Session("EventYear") = EventYear
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "Games are not currently open for online registration."
            End If
        End Sub

        Protected Sub lnkDas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDas.Click
            Session("EventID") = 6
            Session("RoleID") = System.DBNull.Value
            Response.Redirect("Reg_DAS.aspx")
        End Sub

        Protected Sub hlinkdonate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkdonate.Click
            Session("EventID") = 11
            Response.Redirect("donor_donate.aspx")
        End Sub

        Protected Sub hlinkRegisterforcoaching_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkRegisterforcoaching.Click
            trMessage.Visible = False
            Session("EventID") = 13
            Session("PanelFrom") = "OnlineCoaching"
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(13)
            If Session("EventID") = eventid Then
                'get EventYear and store it as Session variable
                Dim EventYear As Integer
                Dim conn As New SqlConnection(Application("ConnectionString"))
                conn.Open()
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@EventID", Session("EventID"))
                EventYear = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear", param)
                conn.Close()
                Session("EventYear") = EventYear
                Response.Redirect("Registration.aspx?ParentUpdate=true")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                Session.Remove("PanelFrom")
                lblMessage.Text = "Coaching program is not currently open for online registration."
            End If
        End Sub

        Protected Sub hlinkShop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkShop.Click
            trMessage.Visible = False
            Session("EventID") = 10
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(10)
            If Session("EventID") = eventid Then
                Response.Redirect("ShoppingCatalog.aspx?id=1")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                lblMessage.Text = "Online shopping is not open now."
            End If

        End Sub
        Protected Sub lnkFundRaising_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFundRaising.Click
            trMessage.Visible = False
            Session("EventID") = 9
            Dim eventid As Integer
            eventid = EventIDCheck.GetEventIDFromDB(9)
            If Session("EventID") = eventid Then
                Response.Redirect("FundRReg.aspx")
            Else
                trMessage.Visible = True
                Session.Remove("EventID")
                lblMessage.Text = "Fund Raising is Not open now"
            End If
        End Sub

        Protected Sub lbtnSATTesting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnSATTesting.Click
            'Response.Write("<script language='javascript'>window.open('http://www.ipteccentral.com/ChildTestAnswers.aspx?MID=" & Session("CustIndID") & "','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=0');</script> ")
            Response.Redirect("ChildTestAnswers.aspx")
            'Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/ChildTestAnswers.aspx?MID=" & Session("CustIndID") & "','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ")
        End Sub
        Protected Sub bttnDownloadPapers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bttnDownloadPapers.Click
            Response.Redirect("DownloadCoachPapers.aspx")
        End Sub
        Protected Sub bttnCreateStudentLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bttnCreateStudentLogin.Click
            Response.Redirect("ChildAccess.aspx")
        End Sub
        Protected Sub btnupdateParentInfo_Click(sender As Object, e As EventArgs) Handles btnupdateParentInfo.Click
            Session("PanelFrom") = "General"
            Response.Redirect("Registration.aspx?ParentUpdate=True")
        End Sub
        Protected Sub lbtnchildinfo_Click(sender As Object, e As EventArgs) Handles lbtnchildinfo.Click
            Session("PanelFrom") = "General"
            Response.Redirect("MainChild.aspx")
        End Sub

        Protected Sub lbtnCreateChildLogin_Click(sender As Object, e As EventArgs) Handles lbtnCreateChildLogin.Click
            Session("EventID") = 4
            Session("PanelFrom") = "Games"
            Response.Redirect("GameAccess.aspx")
        End Sub
    End Class
End Namespace
