﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
//using OpenPop.Pop3;
//using OpenPop.Mime;
using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
//using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
//http://vivekcek.wordpress.com/2011/06/14/pop3-client-using-c/

//http://www.codeproject.com/Articles/188349/Read-Gmail-Inbox-Message-in-ASP-NET


public partial class SupportTracking : System.Web.UI.Page
{

    //Modified by: Michael Simson
    //Modified On: 02/19/2016
    //Reason: Change the error message for duplicate tickets

    //Purpose of this application:This application is mainly used for tracking the issues which are faced by the customer. 

    /* char Tnew = 'Y'; //New Ticket
     char ProfileFlag = 'N'; //Y=user in NSF db
     char TUser = 'O'; //O=Ticket Owner, R=Respondent */
    // char EntryCode = 'P'; //EntryCode p,d,v,s,g,o

    int LoginMemberiid = 0; //Used to Note the logged in MemberID as Parent
    String Ticket = "";
    int m_PageIndex = 0;
    String Email;
    String FName;
    String LName;
    String MemberID = "Null";
    String Status = "Open"; //Resolved, forwarded, investigating, out of town, escalated,Closed, Pending Customer Input, Reopened, Closed
    protected void Page_Load(object sender, EventArgs e)
    {

        lblError.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            manageShowHideTickets();
        }

    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        addTicket();
    }

    protected void btnVolSubmit_Click(object sender, EventArgs e)
    {
        respondTickets();
    }

    private void GetChapters(DropDownList ddlObject)
    {
        try
        {

            DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT ChapterCode,ChapterID  FROM Chapter WHERE State <>'US' and Status='A' Order By State,ChapterCode");
            ddlObject.DataSource = ds_Chapter;
            ddlObject.DataTextField = "ChapterCode";
            ddlObject.DataValueField = "ChapterId";
            ddlObject.DataBind();
            if (ddlObject.ID == ddlFChapter.ID)
            {
                ddlObject.Items.Insert(0, new ListItem("All", "0"));
                ddlObject.SelectedIndex = 0;
            }

            ddlObject.Items.Insert(0, new ListItem("Select Chapter", "-1"));
            ddlObject.SelectedIndex = 0;
        }
        catch
        {
        }
    }
    private void GetProductGroup(DropDownList ddlObject, DropDownList ddlObjectEvent)
    {
        try
        {

            int year = DateTime.Now.Year;

            string cmdtext = "select ProductGroupId,Name from ProductGroup where EventID=" + ddlObjectEvent.SelectedItem.Value;
            DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            ddlObject.DataSource = ds_Chapter;
            ddlObject.DataTextField = "Name";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
            ddlObject.SelectedIndex = 0;
            trproduct.Visible = true;
        }
        catch
        {
        }
    }
    private void GetProduct(DropDownList ddlObject, DropDownList ddlObjectProductGroup)
    {
        try
        {

            int year = DateTime.Now.Year;

            string cmdText = "select ProductId,Name from Product where ProductGroupId=" + ddlObjectProductGroup.SelectedItem.Value;
            DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            ddlObject.DataSource = ds_Chapter;
            ddlObject.DataTextField = "Name";
            ddlObject.DataValueField = "ProductId";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("Select One", "0"));
            ddlObject.SelectedIndex = 0;
        }
        catch
        {
        }
    }
    private void GetAssignee(DropDownList ddlObject)
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select (IP.FirstName+' '+ IP.LastName+' - '+Event+' '+ISNull(ST.ProductGroupCode,'')) as Name,ST.MemberID, IP.LastName,Ip.FirstName from Indspouse IP inner join SupportTicketContact ST on(IP.AutoMemberID=ST.MemberID) ";

            cmdText += "union";
            cmdText += " select (FirstName+' '+ LastName+' - Developer, India Scholarships') as Name,AutoMemberID,LastName,FirstName from IndSpouse where Email='dinold.jeeva@capestart.com'";
            cmdText += "union";
            cmdText += " select (FirstName+' '+ LastName+' - Developer, Coaching') as Name,AutoMemberID,LastName,FirstName from IndSpouse where Email='michael.simson@capestart.com'";
            cmdText += "union";
            cmdText += " select (FirstName+' '+ LastName+' - Developer, Contests') as Name,AutoMemberID,LastName,FirstName from IndSpouse where Email='bindhu.rajalakshmi@capestart.com'";
            cmdText += " order by LastName,FirstName ASC";

            DataSet ds_Assignee = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            ddlObject.DataSource = ds_Assignee;
            ddlObject.DataTextField = "Name";
            ddlObject.DataValueField = "MemberID";
            ddlObject.DataBind();

            ddlObject.Items.Insert(0, new ListItem("Select One", "0"));
            ddlObject.SelectedIndex = 0;
        }
        catch
        {
        }
    }
    protected void rbtn_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

            lblError.Text = "";
            if (rbtn1.Checked == true)
            {
                tblNew.Visible = true;
                GVSupport.Visible = false;

            }
            else
            {
                tblNew.Visible = false;
                if (Session["LoginID"] != null)
                    if (Session["entryToken"].ToString() == "Volunteer" && Request.QueryString["showmine"] == null)
                        GetGridValues(0, false);
                    else if (Request.QueryString["showmine"] != null)
                        GetGridValues(int.Parse(Session["LoginID"].ToString()), false);
                    else
                        GetGridValues(int.Parse(Session["CustIndID"].ToString()), false);

                dvSupportFilters.Visible = false;
            }
        }
        catch
        {
        }
    }
    public void GetGridValues(int MemberID, Boolean paging)
    {
        try
        {

            String sql = "SELECT s.Ticket_ID,IP1.FirstName as OFName,IP1.LastName as OLName,IP1.Email as OEmail,s.FirstName,s.LastName,s.EntryEmail,s.Subject,s.Chapter,s.Status,s.Priority,(select COUNT(*)from Support where Ticket_ID=s.Ticket_ID)as Thread,s.CreateDate,(select MIN(CreateDate) from Support where Ticket_ID=s.Ticket_ID) as OpenedDate,IP.FirstName +' '+IP.LastName as LastResponded,";

            if ((DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1") && (DDLPriorityFilter.SelectedValue == "0" && DDLPriorityFilter.SelectedValue == "-1"))
            {
                sql = sql + " (select Status from Support where Support_ID=(select Max(Support_ID) from Support where Ticket_ID=s.Ticket_ID) and Ticket_ID=s.Ticket_ID and status=s.status) as CurrentStatus,";
            }
            else if ((DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1") && (DDLPriorityFilter.SelectedValue != "0" || DDLPriorityFilter.SelectedValue != "-1"))
            {
                sql = sql + " (select Status from Support where Support_ID=(select Max(Support_ID) from Support where Ticket_ID=s.Ticket_ID) and Ticket_ID=s.Ticket_ID and status=s.status) as CurrentStatus,";
            }
            else if ((DDLStatusFilter.SelectedValue == "0" || DDLStatusFilter.SelectedValue == "-1") && (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1"))
            {
                sql = sql + " (select Status from Support where Support_ID=(select Max(Support_ID) from Support where Ticket_ID=s.Ticket_ID) and Ticket_ID=s.Ticket_ID and Priority=s.Priority) as CurrentStatus,";
            }
            else
            {
                sql = sql + " (select Status from Support where Support_ID=(select Max(Support_ID) from Support where Ticket_ID=s.Ticket_ID) and Ticket_ID=s.Ticket_ID) as CurrentStatus,";
            }

            sql = sql + "  case when s.priority = 'Normal' then '1. Normal' when s.priority = 'High' then '2. High' when s.Priority = 'Urgent' then '3. Urgent' end as sortPriority,case when s.Status = 'Open' then '1. Open' when s.Status = 'Investigating' then '2. Investigating' when s.Status = 'Forwarded' then '3. Forwarded' when s.Status = 'Escalated' then '4. Escalated' when s.Status = 'Out of town' then '5. Out of town' when s.Status = 'Re-opened' then '6. Re-opened'  when s.Status = 'Resolved' then '7. Resolved' when s.Status = 'Closed' then '8. Closed'    end as sortStatus  FROM Support s left join IndSpouse IP on ((select RMemberID from Support  where Support_ID=(select Max(Support_ID) from Support where Ticket_ID=s.Ticket_ID) and Ticket_ID=s.Ticket_ID)=IP.AutoMemberID)   left join IndSpouse IP1 on(s.UMember_ID=IP1.AutoMemberID)";

            string RoleID = string.Empty;
            try
            {
                RoleID = Session["RoleID"].ToString();
            }
            catch
            {
                RoleID = "";
            }
            if (RoleID == "1" || RoleID == "2" || RoleID == "3" || RoleID == "4" || RoleID == "5" || RoleID == "96")
            {
                if (divChp.Visible == true)
                {
                    if (Convert.ToInt32(ddlFChapter.SelectedItem.Value) == 0)
                    {
                        sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter ";
                    }
                    else if (Convert.ToInt32(ddlFChapter.SelectedItem.Value) > 0)
                    {
                        sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter and c.chapterID in (" + ddlFChapter.SelectedValue + ")";
                    }
                    else if (Session["SelChapterID"] != null && Request.QueryString["ch"] != null)
                    {
                        sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter and c.chapterID in (" + Session["SelChapterID"] + ")";
                    }
                }
                else if (Session["SelChapterID"] != null && Request.QueryString["ch"] != null)
                {
                    sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter and c.chapterID in (" + Session["SelChapterID"] + ")";
                }

                if (ddlFEvent.SelectedItem.Value != "-1")
                {
                    sql = sql + " WHERE s.Event_ID = " + ddlFEvent.SelectedItem.Value;
                    if (trproduct.Visible == true)
                    {
                        if (Int32.Parse(ddlFProductGroup.SelectedItem.Value) > 0)
                        {
                            sql = sql + " AND  s.ProductGroup_ID = " + ddlFProductGroup.SelectedItem.Value;
                            if (Int32.Parse(ddlFProduct.SelectedItem.Value) > 0)
                            {
                                sql = sql + " AND s.Product_ID = " + ddlFProduct.SelectedItem.Value;
                            }
                            else
                                sql = sql + " ";
                        }
                    }

                    if (DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1")
                    {
                        sql = sql + " AND  s.Status = '" + DDLStatusFilter.SelectedItem.Value + "'";
                    }
                    if (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1")
                    {
                        sql = sql + " AND  s.Priority = '" + DDLPriorityFilter.SelectedItem.Value + "'";
                    }
                }
                else
                {
                    if ((DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1") && (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1"))
                    {
                        sql += "where s.Status = '" + DDLStatusFilter.SelectedItem.Value + "' And s.Priority = '" + DDLPriorityFilter.SelectedItem.Value + "'";

                    }
                    else if ((DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1") && (DDLPriorityFilter.SelectedValue == "0" || DDLPriorityFilter.SelectedValue == "-1"))
                    {
                        sql += "where s.Status = '" + DDLStatusFilter.SelectedItem.Value + "'";
                    }

                    else if ((DDLStatusFilter.SelectedValue == "0" || DDLStatusFilter.SelectedValue == "-1") && (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1"))
                    {
                        sql += "where s.Priority = '" + DDLPriorityFilter.SelectedItem.Value + "'";
                    }


                }
            }

            else if (MemberID > 0)
            {

                if (Session["entryToken"].ToString() == "Volunteer")
                {
                    sql = sql + " WHERE (s.UMember_ID = " + MemberID.ToString() + " or S.AssignTo=" + MemberID.ToString() + " or S.BMemberID=" + MemberID.ToString() + " or S.CMemberID=" + MemberID.ToString() + ")";
                }
                else
                {
                    sql = sql + " WHERE s.UMember_ID = " + MemberID.ToString() + "";

                }




                if (DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1")
                {
                    sql = sql + " AND  s.Status = '" + DDLStatusFilter.SelectedItem.Value + "'";
                }
                if (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1")
                {
                    sql = sql + " AND  s.Priority = '" + DDLPriorityFilter.SelectedItem.Value + "'";
                }

                trFilter.Visible = false;
            }
            else if (Int32.Parse(ddlFEvent.SelectedItem.Value) > -1)
            {
                if (divChp.Visible == true)
                {
                    if (Convert.ToInt32(ddlFChapter.SelectedItem.Value) == 0)
                    {
                        sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter ";
                    }
                    else if (Convert.ToInt32(ddlFChapter.SelectedItem.Value) > 0)
                    {
                        sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter and c.chapterID in (" + ddlFChapter.SelectedValue + ")";
                    }
                    else if (Session["SelChapterID"] != null && Request.QueryString["ch"] != null)
                    {
                        sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter and c.chapterID in (" + Session["SelChapterID"] + ")";
                    }
                }
                else if (Session["SelChapterID"] != null && Request.QueryString["ch"] != null)
                {
                    sql = sql + " INNER JOIN Chapter c ON c.ChapterCode = s.Chapter and c.chapterID in (" + Session["SelChapterID"] + ")";
                }
                sql = sql + " WHERE s.Event_ID = " + ddlFEvent.SelectedItem.Value;
                if (ddlFEvent.SelectedItem.Value != "0")
                {
                    if (Int32.Parse(ddlFProductGroup.SelectedItem.Value) > 0)
                    {
                        sql = sql + " AND  s.ProductGroup_ID = " + ddlFProductGroup.SelectedItem.Value;
                        if (Int32.Parse(ddlFProduct.SelectedItem.Value) > 0)
                        {
                            sql = sql + " AND s.Product_ID = " + ddlFProduct.SelectedItem.Value;
                        }
                        else
                            sql = sql + " ";
                    }
                }

                if (DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1")
                {
                    sql = sql + " AND  s.Status = '" + DDLStatusFilter.SelectedItem.Value + "'";
                }
                if (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1")
                {
                    sql = sql + " AND  s.Priority = '" + DDLPriorityFilter.SelectedItem.Value + "'";
                }
            }
            else
            {

                if (DDLStatusFilter.SelectedValue != "0" && DDLStatusFilter.SelectedValue != "-1")
                {
                    sql = sql + " AND  s.Status = '" + DDLStatusFilter.SelectedItem.Value + "'";
                }
                if (DDLPriorityFilter.SelectedValue != "0" && DDLPriorityFilter.SelectedValue != "-1")
                {
                    sql = sql + " AND  s.Priority = '" + DDLPriorityFilter.SelectedItem.Value + "'";
                }
            }


            sql = sql + " GROUP BY s.Ticket_ID,s.FirstName,s.LastName,s.EntryEmail, s.Subject,s.Chapter,s.Status,s.CreateDate,IP.FirstName ,IP.LastName,s.Priority,IP1.FirstName,IP1.LastName,IP1.Email order by s.CreateDate DESC";

            DataSet ds_GetGrid = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sql);
            if (null != ds_GetGrid && ds_GetGrid.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr1 in ds_GetGrid.Tables[0].Rows)
                {
                    if (dr1["CurrentStatus"].ToString() == "")
                    {
                        dr1.Delete();
                    }
                }
                ds_GetGrid.Tables[0].AcceptChanges();
            }
            DataTable dt = new DataTable();
            dt = ds_GetGrid.Tables[0];
            ArrayList objTicketArr = new ArrayList();
            ArrayList objArrIndex = new ArrayList();

            string TicketID = string.Empty;
            int rowIndex = 0;

            foreach (DataRow dr in dt.Rows)
            {

                rowIndex++;
                for (int i = 0; i < objTicketArr.Count; i++)
                {
                    if (objTicketArr[i].ToString() == dr["Ticket_ID"].ToString())
                    {
                        objArrIndex.Add(rowIndex - 1);
                        //dr.Delete();
                    }
                }

                objTicketArr.Add(dr["Ticket_ID"].ToString());
            }

            for (int j = 0; j < objArrIndex.Count; j++)
            {
                dt.Rows[Convert.ToInt32(objArrIndex[j].ToString())].Delete();
            }
            Session["TicketList"] = dt;

            DataView dv = dt.DefaultView;
            if (DDlGridSorting.SelectedValue == "By Ticket Number")
            {
                dv.Sort = "Ticket_ID DESC";
            }
            else if (DDlGridSorting.SelectedValue == "By Status")
            {
                dv.Sort = "sortStatus ASC";
            }
            else if (DDlGridSorting.SelectedValue == "By Priority")
            {
                dv.Sort = "sortPriority DESC";
            }
            dt = dv.ToTable();

            GVSupport.DataSource = dt;

            if (paging == true)
                GVSupport.PageIndex = m_PageIndex;
            else
                GVSupport.PageIndex = 0;
            GVSupport.DataBind();
            if (ds_GetGrid.Tables[0].Rows.Count > 0)
            {
                GVSupport.Visible = true;
                dvSortingGrid.Visible = true;
            }
            else
            {
                GVSupport.Visible = false;
                dvSortingGrid.Visible = false;
                lblError.Text = "No record to display";
            }
        }
        catch
        {
        }
    }
    public void getUserDetails(string EntryType, String IDorEmail)
    {
        try
        {

            String sql = "SELECT Top 1 FirstName,lastName,Email,ChapterID,HPhone,CPhone  from IndSpouse WHERE ";
            if (EntryType == "P")
                sql = sql + " AutoMemberID = " + IDorEmail.ToString();
            else
                sql = sql + " Email = '" + IDorEmail.ToString() + "'";
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (EntryType == "P")
                        txtEmail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                    txtFName.Text = ds.Tables[0].Rows[0]["FirstName"].ToString();
                    txtLName.Text = ds.Tables[0].Rows[0]["LastName"].ToString();
                    txtHPhone.Text = ds.Tables[0].Rows[0]["HPhone"].ToString();
                    txtCPhone.Text = ds.Tables[0].Rows[0]["CPhone"].ToString();
                    ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(ds.Tables[0].Rows[0]["ChapterID"].ToString().ToString()));
                    Tnew.Text = "Y";
                    ProfileFlag.Text = "Y";
                    TUser.Text = "O";

                    txtEmail.Enabled = false;


                }
            }
            catch (Exception e)
            {

            }

        }
        catch
        {
        }
    }

    public void GetTicket(String Ticket)
    {
        try
        {


            string UMemberID = string.Empty;
            string CmdText = "select distinct UMember_ID from Support where Ticket_ID=" + Ticket + "";
            DataSet ds = new DataSet();
            try
            {
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                if (null != ds)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            UMemberID = ds.Tables[0].Rows[0]["UMember_ID"].ToString();
                        }
                    }
                }
            }
            catch
            {
            }

            String sql = "SELECT     s.Support_ID,s.Ticket_ID,s.RMemberID,s.TNew,s.TUser,s.EntryCode,s.ProfileFlag,s.UMember_ID,s.EntryEmail,s.FirstName,s.LastName,s.Chapter,s.Event_ID,ISNULL(e.Name,'Other') AS Event,s.Product_ID,s.ProductCode,s.ProductGroup_ID,s.ProductGroupCode,s.Subject,s.Message,s.Priority,s.Help,s.AssignTo,IP.FirstName +' '+IP.LastName as Name,s.SentEmailA,s.SentEmailC,s.Private,s.Status,s.CreateDate,s.CreatedBy,s.ModifyDate,s.ModifiedBy,s.CCedEmails,s.HPhone,s.CPhone, IP.Email +', '+IP.FirstName +' '+IP.LastName +';' as PName, IP1.Email +', '+IP1.FirstName +' '+IP1.LastName +';' as BName, IP2.Email +', '+IP2.FirstName +' '+IP2.LastName +';' as CName, IP3.Email +', '+IP3.FirstName +' '+IP3.LastName+';' as RName,IP4.Email +', '+IP4.FirstName +' '+IP4.LastName as UName,IP5.Email +', '+IP5.FirstName +' '+IP5.LastName as ChapterCName,s.CCEmails FROM Support s LEFT JOIN Event e ON s.Event_ID=e.EventID left join IndSpouse IP on (IP.AutoMemberID=s.AssignTo) left join IndSpouse IP1 on(S.BMemberID=IP1.AutoMemberID) left join IndSpouse IP2 on(S.CMemberID=IP2.AutoMemberID) left join IndSpouse IP3 on(S.RMemberID=IP3.AutoMemberID) left join IndSpouse IP4 on (S.UMember_ID=IP4.AutoMemberID) left join IndSpouse IP5 on (S.CCMemberID=IP5.AutoMemberID) WHERE s.Ticket_ID = '" + Ticket + "'";

            if (Session["LoginID"].ToString() == UMemberID)
            {
                sql += " and (S.Private='N' or S.Private is null)";
            }
            else
            {
                sql += " and (S.Private='N' or S.Private is null or S.Private='Y')";
            }



            if (Session["entryToken"].ToString() != "Volunteer")
            {

            }
            sql = sql + " ORDER BY s.CreateDate DESC";


            DataSet ds_GetGrid = new DataSet();
            try
            {
                ds_GetGrid = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sql);

                rptTicket.DataSource = ds_GetGrid;
                rptTicket.DataBind();

            }
            catch (Exception e)
            {

            }
            if (ds_GetGrid.Tables[0].Rows.Count > 0)
            {
                rptTicket.Visible = true;
                rptA.Visible = true;
                hlBackToList.Visible = true;
                txtEmail.Text = ds_GetGrid.Tables[0].Rows[0]["EntryEmail"].ToString();

                txtFName.Text = ds_GetGrid.Tables[0].Rows[0]["FirstName"].ToString();
                txtLName.Text = ds_GetGrid.Tables[0].Rows[0]["LastName"].ToString();
                txtHPhone.Text = ds_GetGrid.Tables[0].Rows[0]["HPhone"].ToString();
                txtCPhone.Text = ds_GetGrid.Tables[0].Rows[0]["CPhone"].ToString();
                ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByText(ds_GetGrid.Tables[0].Rows[0]["Chapter"].ToString()));
                ddlEntryCode.SelectedIndex = ddlEntryCode.Items.IndexOf(ddlEntryCode.Items.FindByValue(ds_GetGrid.Tables[0].Rows[0]["EntryCode"].ToString()));
                ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(ds_GetGrid.Tables[0].Rows[0]["Event_ID"].ToString()));

                try
                {
                    if (Int32.Parse(ddlEvent.SelectedItem.Value) > 0)
                    {
                        GetProductGroup(ddlProductGroup, ddlEvent);
                        ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(ds_GetGrid.Tables[0].Rows[0]["ProductGroup_ID"].ToString()));
                        if (Int32.Parse(ddlProductGroup.SelectedItem.Value) > 0)
                        {
                            GetProduct(ddlProduct, ddlProductGroup);
                            ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(ds_GetGrid.Tables[0].Rows[0]["Product_ID"].ToString()));
                        }
                        else
                        {
                            ddlProduct.Items.Clear();
                        }
                    }
                    else
                    {
                        ddlProductGroup.Items.Clear();
                        ddlProduct.Items.Clear();
                        trproduct.Visible = false;
                    }
                }
                catch (Exception e)
                {

                }

                txtSubject.Text = ds_GetGrid.Tables[0].Rows[0]["Subject"].ToString();
                lblTicketNo.Text = ds_GetGrid.Tables[0].Rows[0]["Ticket_ID"].ToString();
                hdnTicketID.Value = ds_GetGrid.Tables[0].Rows[0]["Ticket_ID"].ToString();
                lblLoginMemberid.Text = ds_GetGrid.Tables[0].Rows[0]["UMember_ID"].ToString();
                Tnew.Text = "N";
                ProfileFlag.Text = ds_GetGrid.Tables[0].Rows[0]["ProfileFlag"].ToString();
                hdnSupportID.Value = ds_GetGrid.Tables[0].Rows[0]["Support_ID"].ToString();
                Ticket = lblTicketNo.Text;

                txtEmail.Enabled = false;
                txtFName.Enabled = false;
                txtLName.Enabled = false;
                txtCPhone.Enabled = false;
                txtHPhone.Enabled = false;
                ddlChapter.Enabled = false;
                ddlEntryCode.Enabled = false;
                ddlEvent.Enabled = false;

                txtSubject.Enabled = false;
                hdnUMemberID.Value = ds_GetGrid.Tables[0].Rows[0]["UMember_ID"].ToString();

                string assignedTo = "";
                DataTable dt = new DataTable();
                DataView dv = new DataView();
                dt = ds_GetGrid.Tables[0];
                dv = dt.DefaultView;
                dv.Sort = "Support_ID Asc";
                dt = dv.ToTable();
                foreach (DataRow dr in dt.Rows)
                {
                    assignedTo = dr["AssignTo"].ToString();
                    if (dr["Status"].ToString() == "Resolved")
                    {
                        hdnStatus.Value = dr["Status"].ToString();
                    }
                }
                hdnAssignTo.Value = assignedTo;
                if (hdnAssignTo.Value == "")
                {
                    hdnAssignTo.Value = "0";
                }
                if (ds_GetGrid.Tables[0].Rows.Count > 1)
                {
                    hdnRMemberID.Value = ds_GetGrid.Tables[0].Rows[1]["RMemberID"].ToString();
                }


                if (hdnUMemberID.Value == Session["LoginID"].ToString())
                {
                    ddlEmailAssignTo.SelectedValue = "N";
                    ddlEmailAssignTo.Enabled = false;
                    ddlEmailToCustomer.SelectedValue = "N";
                    ddlEmailToCustomer.Visible = false;
                    ddlKeepRivate.SelectedValue = "N";
                    ddlKeepRivate.Visible = false;
                    txtcc.Visible = false;
                    spnEmailToCustomer.Visible = false;
                    spnKeepPrivate.Visible = false;
                    spnCC.Visible = false;
                }
                else
                {

                    ddlEmailAssignTo.Enabled = true;

                    ddlEmailToCustomer.Visible = true;

                    ddlKeepRivate.Visible = true;
                    txtcc.Visible = true;
                    spnEmailToCustomer.Visible = true;
                    spnKeepPrivate.Visible = true;
                    spnCC.Visible = true;
                }
            }
            else
            {
                rptTicket.Visible = false;
                lblError.Text = "No record to display";
            }
        }
        catch
        {
        }
    }

    protected string GetUrl(object id)
    {

        return "javascript:var w=window.open('SupportTracking.aspx?NSF_Ticket=" + encode(id.ToString()) + "','_blank','toolbar=no,width=1000,height=800,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=1,fullscreen=1;')";

    }

    protected string ClosePopUpWindow()
    {
        return "javascript:window.close();)";

    }




    protected void GVSupport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            hdnIsViewParent.Value = "Y";
            m_PageIndex = e.NewPageIndex;
            GetGridValues(LoginMemberiid, true);

        }
        catch (Exception ex) { }
    }
    protected string encode(String inputText)
    {
        byte[] bytesToEncode = Encoding.UTF8.GetBytes(inputText);
        string encodedText = Convert.ToBase64String(bytesToEncode);
        return encodedText;
    }
    protected string decode(String inputText)
    {
        try
        {
            byte[] decodedBytes = Convert.FromBase64String(inputText);
            String decodedText = Encoding.UTF8.GetString(decodedBytes);
            return decodedText;
        }
        catch
        {
            Response.Redirect("SupportTracking.aspx");
            return "";
        }
    }
    private void sentemail(string sSubject, string sBody, string strMailTo)
    {
        try
        {

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("nsfmailalerts@gmail.com");
            mail.Body = sBody;
            mail.Subject = sSubject;
            mail.CC.Add(new MailAddress("nsfmailalerts@gmail.com"));

            String[] strEmailAddressList;
            String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Match EmailAddressMatch;
            strEmailAddressList = strMailTo.Replace(',', ';').Split(';');
            foreach (object item in strEmailAddressList)
            {
                EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);
                if (EmailAddressMatch.Success)
                {
                    if (item.ToString().Trim() == "nsfprogramleads@gmail.com" && ddlEvent.SelectedValue == "20")
                    {
                        mail.To.Add(new MailAddress("nsfonlineworkshop@northsouth.org"));
                    }
                    else
                    {
                        mail.To.Add(new MailAddress(item.ToString().Trim()));
                    }
                }
            }

            SmtpClient client = new SmtpClient();
            // client.Host = host;
            mail.IsBodyHtml = true;
            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                //lblError.Text = ex.ToString();
            }
        }
        catch
        {
        }
    }

    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (Int32.Parse(ddlEvent.SelectedItem.Value) > 0)
            {
                GetProductGroup(ddlProductGroup, ddlEvent);
                ddlProduct.Items.Clear();
            }
            else
            {
                ddlProductGroup.Items.Clear();
                ddlProduct.Items.Clear();
                trproduct.Visible = false;
            }
        }
        catch
        {
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (Int32.Parse(ddlProductGroup.SelectedItem.Value) > 0)
                GetProduct(ddlProduct, ddlProductGroup);
            else
                ddlProduct.Items.Clear();
        }
        catch
        {
        }
    }
    protected void DDLProductGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (Int32.Parse(DDLProductGroupFilter.SelectedItem.Value) > 0)
                GetProduct(DDLProductFilter, DDLProductGroupFilter);
            else
                DDLProductFilter.Items.Clear();
        }
        catch
        {
        }
    }



    protected void ddlFEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            int iEventId = Int32.Parse(ddlFEvent.SelectedItem.Value);
            divChp.Visible = false;
            if (iEventId > 0)
            {
                if (iEventId == 2 || iEventId == 3 || iEventId == 19)
                {
                    divChp.Visible = true;
                    GetChapters(ddlFChapter);
                    int RoleID = Convert.ToInt32(Session["RoleId"]);
                    if (RoleID == 5)
                    {
                        string cmd;
                        cmd = "SELECT I.ChapterId ChapterId from Indspouse I inner join Volunteer V on I.Automemberid=V.MemberId and V.RoleId=5 where I.Automemberid=" + Session["LoginID"];
                        int iChapterId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmd));
                        ddlFChapter.SelectedIndex = ddlFChapter.Items.IndexOf(ddlFChapter.Items.FindByValue(iChapterId.ToString()));
                    }
                }

                GetProductGroup(ddlFProductGroup, ddlFEvent);
                ddlFProduct.Items.Clear();
                trFproduct.Visible = true;
            }
            else
            {
                ddlFProductGroup.Items.Clear();
                ddlFProduct.Items.Clear();
                trFproduct.Visible = false;
            }
        }
        catch
        {
        }
    }
    protected void DDLEventFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {


            if (Int32.Parse(DDLEventFilter.SelectedItem.Value) > 0)
            {
                GetProductGroup(DDLProductGroupFilter, DDLEventFilter);
                DDLProductFilter.Items.Clear();
            }
            else
            {
                DDLProductGroupFilter.Items.Clear();
                DDLProductGroupFilter.Items.Clear();
                trFproduct.Visible = false;
            }
        }
        catch
        {
        }
    }
    protected void ddlFProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (Int32.Parse(ddlFProductGroup.SelectedItem.Value) > 0)
                GetProduct(ddlFProduct, ddlFProductGroup);
            else
                ddlFProduct.Items.Clear();
        }
        catch
        {
        }
    }
    protected void ddlKeepRivate_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (ddlEmailToCustomer.SelectedItem.Text == "Y")
            {

                ddlKeepRivate.SelectedIndex = ddlKeepRivate.Items.IndexOf(ddlKeepRivate.Items.FindByValue("N"));
                btnVolSubmit.Attributes.Add("onclick", "return confirm('You are going to send emails to Customer?');");
                txtcc.Text = txtEmail.Text;
            }
            else
            {
                ddlKeepRivate.SelectedIndex = ddlKeepRivate.Items.IndexOf(ddlKeepRivate.Items.FindByValue("Y"));
                btnVolSubmit.Attributes.Remove("onclick");
            }
        }
        catch
        {
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Write("<script>self.close();</script>");
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        GetGridValues(0, false);
        lblTitle.Text = "Table 1: Ticket List";
        lblTitle.Visible = true;
    }
    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlStatus.SelectedValue == "Resolved")
        {
            ddlAssignTo.SelectedValue = "0";
        }
        if (ddlStatus.SelectedValue == "Re-opened")
        {
            if (hdnStatus.Value != "Resolved")
            {
                lblError.Text = "The ticket cannot Re-opened before Resolved.";
                ddlStatus.SelectedValue = "0";
            }
        }
    }

    protected void btnUpdateTicket_Click(object sender, EventArgs e)
    {
        hdnUpdateFlag.Value = "Vol";
        TblUpdateTicket.Visible = true;
        lblUpdateOpt.Text = "Please choose any one from below options.";
        RbtnForward.Checked = false;
        RbtnNoForward.Checked = false;
        RbtnIgnore.Checked = false;
    }

    public void UpdateVolticket()
    {
        try
        {

            if (ddlAssignTo.SelectedValue == "0")
            {
                if (hdnAssignTo.Value != "0" && hdnAssignTo.Value != "")
                {
                    try
                    {
                        ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                    }
                    catch
                    {
                        ddlAssignTo.SelectedValue = "0";
                    }

                }
            }

            lblError.Text = "";
            TUser.Text = "R";
            string Assignto = "NULL";
            if (ddlAssignTo.SelectedItem.Value != "0")
                Assignto = ddlAssignTo.SelectedItem.Value;
            if (lblTicketNo.Text.Length > 0)
            {
                Tnew.Text = "N";
                Ticket = lblTicketNo.Text;
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail == false)
                lblError.Text = "Please enter a valid email address";
            else if (ddlStatus.SelectedItem.Value == "0")
                lblError.Text = "Please select Status";
            else if (ddlStatus.SelectedValue != "Resolved" && ddlAssignTo.SelectedItem.Value == "0" && ddlKeepRivate.SelectedItem.Value == "Y")
                lblError.Text = "Please select AssignTo";
            else if (txtEmailBody.Text == "")
                lblError.Text = "The text box above cannot be empty.  Please describe the problem in detail.";
            else if (ddlEmailToCustomer.Text == "Y" && txtcc.Text == "")
            {
                lblError.Text = "The CC text box cannot be empty while Email To Customer value is 'Y'";
            }
            else if (ddlStatus.SelectedValue == "Re-opened" && hdnUMemberID.Value != Session["LoginID"].ToString())
            {
                lblError.Text = "You cannot Re-open the ticket....";
            }
            else
            {

                string sql = "";
                if (ProfileFlag.Text == "N")
                {
                    try
                    {
                        String getAutoMemberID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 ISNULL(AutoMemberID,0) as AutoMemberID FROM IndSpouse WHERE Email='" + txtEmail.Text.ToString() + "' ORDER BY Relationship").ToString();
                        if (getAutoMemberID.Length > 0)
                        {
                            ProfileFlag.Text = "Y";
                            lblLoginMemberid.Text = getAutoMemberID;
                        }
                    }
                    catch (Exception ex)
                    { }
                }

                string chapterCoordnator = string.Empty;
                if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                {
                    string ccText = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP.AutoMemberID FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                    SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, ccText);
                    if (dr.Read())
                    {
                        chapterCoordnator = dr["AutoMemberID"].ToString();
                    }
                }


                if (ddlProductGroup.Items.Count > 0)
                {

                    sql = sql + ",ProductGroup_ID,ProductGroupCode";
                }
                if (ddlProduct.Items.Count > 0)
                {

                    sql = sql + ",Product_ID,ProductCode";
                }



                if (lblLoginMemberid.Text.Length == 0)
                    sql = sql + MemberID;
                else
                    sql = sql + lblLoginMemberid.Text;

                string Ttype = string.Empty;
                string RMemberID = string.Empty;
                string AssignTo = string.Empty;
                if (hdnUMemberID.Value == RMemberID && ddlStatus.SelectedValue != "Forwarded")
                {
                    Ttype = "O";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                else if ((ddlAssignTo.SelectedValue == "0" || ddlAssignTo.SelectedValue == "") && ddlStatus.SelectedValue != "Forwarded")
                {
                    Ttype = "R";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                else
                {
                    Ttype = "F";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "'" + ddlAssignTo.SelectedValue + "'";
                }
                if (TUser.Text == "R" && ddlStatus.SelectedValue != "Forwarded" && ddlStatus.SelectedValue != "Pending Customer Input" && hdnUMemberID.Value != RMemberID)
                {
                    Ttype = "R";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                string ccMails = string.Empty;
                if (txtcc.Text == "")
                {
                    ccMails = "NULL";
                }
                else
                {
                    ccMails = "'" + txtcc.Text + "'";
                }
                if (ddlStatus.SelectedValue == "Resolved")
                {
                    AssignTo = "NULL";
                }

                if (chapterCoordnator != "")
                {
                    chapterCoordnator = "'" + chapterCoordnator + "'";
                }
                else
                {
                    chapterCoordnator = "null";
                }

                Status = ddlStatus.SelectedValue;
                sql = "Update Support set TNew='" + Tnew.Text + "', TUser='" + TUser.Text + "', EntryCode='" + ddlEntryCode.SelectedItem.Value + "', ProfileFlag='" + ProfileFlag.Text + "', FirstName='" + txtFName.Text + "', LastName='" + txtLName.Text + "', Chapter='" + ddlChapter.SelectedItem.Text + "', Event_ID='" + ddlEvent.SelectedValue + "', Subject='" + txtSubject.Text + "', Message='" + txtEmailBody.Text + "', Priority='" + ddlPriority.SelectedItem.Text + "',Status='" + Status + "',HPhone='" + txtHPhone.Text + "',CPhone='" + txtCPhone.Text + "', ModifyDate=GetDate(),RMemberID=" + RMemberID + ", AssignTo=" + AssignTo + ",Ttype='" + Ttype + "',CCEmails='" + ccMails + "',ModifiedBy=" + Session["LoginID"].ToString() + ", CCMemberID=" + chapterCoordnator + " where Support_ID=" + hdnSupportID.Value + "";

                try
                {
                    int suppId = 0;
                    if (Tnew.Text == "Y")
                    {
                        int Support_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID='" + Support_ID.ToString() + "' WHERE Support_ID=" + Support_ID.ToString());
                        lblTicketNo.Text = "NSF#" + Support_ID.ToString();
                        hdnTicketID.Value = Support_ID.ToString();
                    }
                    else
                    {
                        suppId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));
                    }



                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Status='" + ddlStatus.SelectedItem.Text + "' WHERE Support_ID='" + suppId + "'");
                    lblError.Text = "Ticket updated Successfully";

                    String SendToVol = "";
                    string UserName = string.Empty;
                    if (Int32.Parse(ddlAssignTo.SelectedItem.Value) != 0)
                    {

                        if (ddlStatus.SelectedValue == "Pending Customer Input")
                        {
                            string cmdText = "select * from Support where Ticket_ID=" + suppId + " and Status='Open'";
                            string UMemberID = string.Empty;
                            DataSet ds = new DataSet();
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                            if (null != ds && ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    UMemberID = ds.Tables[0].Rows[0]["UMember_ID"].ToString();
                                }
                            }
                            cmdText = "select Email, FirstName, LastName from Indspouse where AutoMemberID=" + UMemberID + "";
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                            if (null != ds && ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    SendToVol = ds.Tables[0].Rows[0]["Email"].ToString().Trim();
                                    UserName = ds.Tables[0].Rows[0]["FirstName"].ToString().Trim() + " " + ds.Tables[0].Rows[0]["LastName"].ToString().Trim();
                                }
                            }
                        }
                        else
                        {
                            string cmdTextVol = "SELECT IP.Email FROM SupportTicketContact ST inner join IndSpouse IP on(ST.MemberID=IP.AutoMemberID) WHERE EventID =" + ddlEvent.SelectedValue + "";
                            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                            {
                                cmdTextVol += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                            }
                            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                            {
                                cmdTextVol += " and ProductID=" + ddlProduct.SelectedValue;
                            }
                            DataSet dsVol = new DataSet();
                            dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                            if (null != dsVol && dsVol.Tables.Count > 0)
                            {
                                if (dsVol.Tables[0].Rows.Count > 0)
                                {
                                    SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                }
                            }
                            if (SendToVol == "")
                            {
                                cmdTextVol = "SELECT IP.Email FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                if (null != dsVol && dsVol.Tables.Count > 0)
                                {
                                    if (dsVol.Tables[0].Rows.Count > 0)
                                    {
                                        SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                    }
                                }
                            }
                        }
                    }

                    if (ddlKeepRivate.SelectedItem.Text == "N")
                    {
                        SendToVol = SendToVol + ";" + txtEmail.Text;
                    }
                    SendToVol = SendToVol + ";" + txtcc.Text.Replace(',', ';');
                    if (SendToVol.Length > 0)
                    {
                        if (ddlStatus.SelectedValue == "Pending Customer Input")
                        {
                            sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + UserName + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                        }
                        else
                        {
                            sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ddlAssignTo.SelectedItem.Text + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                        }
                    }

                    GetTicket(hdnTicketID.Value);
                }
                catch (Exception ex)
                {

                }
            }
        }
        catch
        {
        }
    }
    protected void btnUpdateUserTicket_Click(object sender, EventArgs e)
    {
        hdnUpdateFlag.Value = "User";
        TblUpdateTicket.Visible = true;
        lblUpdateOpt.Text = "Please choose any one from below options.";
        RbtnForward.Checked = false;
        RbtnNoForward.Checked = false;
        RbtnIgnore.Checked = false;

    }

    public void updateUserTicket()
    {
        try
        {

            lblError.Text = "";
            if (lblTicketNo.Text.Length > 0)
            {
                Tnew.Text = "N";
                Ticket = lblTicketNo.Text;
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail == false)
                lblError.Text = "Please enter a valid email address.";
            else if (Int32.Parse(ddlChapter.SelectedItem.Value) == -1)
                lblError.Text = "Please select Chapter.";
            else if (Int32.Parse(ddlEvent.SelectedItem.Value) == -1)
                lblError.Text = "Please select Event.";
            else if (txtEmailBody.Text == "")
                lblError.Text = "The text box above cannot be empty.  Please describe the problem in detail.";
            else if (ddlEmailToCustomer.Text == "Y" && txtcc.Text == "")
            {

                lblError.Text = "The CC text box cannot be empty while Email To Customer value is 'Y'";

            }
            else
            {

                string sql = "";
                if (ProfileFlag.Text == "N")
                {
                    try
                    {
                        String getAutoMemberID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 ISNULL(AutoMemberID,0) as AutoMemberID FROM IndSpouse WHERE Email='" + txtEmail.Text.ToString() + "' ORDER BY Relationship").ToString();
                        if (getAutoMemberID.Length > 0)
                        {
                            ProfileFlag.Text = "Y";
                            lblLoginMemberid.Text = getAutoMemberID;
                        }
                    }
                    catch (Exception ex)
                    { }
                }

                string cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue;
                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                {
                    cmdAssignTotext += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                }
                else
                {
                    cmdAssignTotext += " and ProductGroupId is null";
                }
                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                {
                    cmdAssignTotext += " and ProductID=" + ddlProduct.SelectedValue;
                }
                else
                {
                    cmdAssignTotext += " and ProductID is null";
                }
                string assignedTo = string.Empty;
                string BMemberID = string.Empty;
                string CMemberID = string.Empty;
                string chapterCoordnator = string.Empty;
                DataSet dsAssignto = new DataSet();
                dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                {
                    if (dsAssignto.Tables[0].Rows.Count > 0)
                    {
                        assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                        BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                        CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                    }
                    else
                    {
                        cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + "";
                        dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                        if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                        {
                            if (dsAssignto.Tables[0].Rows.Count > 0)
                            {
                                hdnIsProduct.Value = "yes";
                                assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                                BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                                CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                            }
                            else
                            {

                                cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue + " ";
                                dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                                if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                                {
                                    if (dsAssignto.Tables[0].Rows.Count > 0)
                                    {
                                        hdnIsProductGroup.Value = "Yes";
                                        hdnIsProduct.Value = "yes";
                                        assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                                        BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                                        CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                                    }
                                }

                            }
                        }
                    }
                }


                if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                {
                    string ccText = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP.AutoMemberID FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                    SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, ccText);
                    if (dr.Read())
                    {
                        chapterCoordnator = dr["AutoMemberID"].ToString();
                    }
                }

                if (chapterCoordnator != "")
                {
                    chapterCoordnator = "'" + chapterCoordnator + "'";
                }
                else
                {
                    chapterCoordnator = "null";
                }

                if (BMemberID != "")
                {
                    BMemberID = "'" + BMemberID + "'";
                }
                else
                {
                    BMemberID = "null";
                }
                if (CMemberID != "")
                {
                    CMemberID = "'" + CMemberID + "'";
                }
                else
                {
                    CMemberID = "null";
                }
                if (ProfileFlag.Text == "N")
                    sql = sql + MemberID;
                else
                    sql = sql + lblLoginMemberid.Text;
                Status = ddlStatus.SelectedItem.Text;

                if (Status.Trim() == "Select Status")
                {
                    Status = "Open";
                }

                sql = "Update Support set TNew='" + Tnew.Text + "', TUser='" + TUser.Text + "', EntryCode='" + ddlEntryCode.SelectedItem.Value + "', ProfileFlag='" + ProfileFlag.Text + "', FirstName='" + txtFName.Text + "', LastName='" + txtLName.Text + "', Chapter='" + ddlChapter.SelectedItem.Text + "', Event_ID='" + ddlEvent.SelectedValue + "', Subject='" + txtSubject.Text + "', Message='" + txtEmailBody.Text + "', Priority='" + ddlPriority.SelectedItem.Text + "',Status='" + Status + "',HPhone='" + txtHPhone.Text + "',CPhone='" + txtCPhone.Text + "', ModifyDate=GetDate(),ModifiedBy=" + Session["LoginID"].ToString() + ",TType='O',RMemberID=NULL, AssignTo='" + assignedTo + "', BMemberID=" + BMemberID + ",CMemberID=" + CMemberID + ", CCMemberID=" + chapterCoordnator + " ";

                if (ddlProductGroup.Items.Count > 0)
                {
                    if (Int32.Parse(ddlProductGroup.SelectedItem.Value) != 0 && Int32.Parse(ddlProductGroup.SelectedItem.Value) != -1)
                        sql = sql + ",ProductGroup_ID=" + ddlProductGroup.SelectedItem.Value + ",ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "'";
                    else
                        sql = sql + ",ProductGroup_ID=NULL,ProductGroupCode=NULL";
                }
                if (ddlProduct.Items.Count > 0)
                {
                    if (Int32.Parse(ddlProduct.SelectedItem.Value) != 0 && Int32.Parse(ddlProduct.SelectedItem.Value) != -1)
                        sql = sql + ",Product_ID=" + ddlProduct.SelectedItem.Value + ",ProductCode='" + ddlProduct.SelectedItem.Text + "'";
                    else
                        sql = sql + ",Product_ID=NULL,ProductCode=NULL";
                }





                sql = sql + " where Ticket_ID='" + hdnTicketID.Value + "'";
                //lblError.Text = sql;
                if (Tnew.Text == "Y")
                    sql = sql + " Select Scope_Identity() "; // to Generate Ticket#
                try
                {

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);

                    lblError.Text = "Ticket updated Successfully";
                    sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi Sir/Madam,<BR> We received your support ticket : " + lblTicketNo.Text + " will get back to you soon. <BR>" + txtEmailBody.Text, txtEmail.Text);



                    string SendToVol = string.Empty;
                    ArrayList ArrList = new ArrayList();
                    ArrayList ArrNameList = new ArrayList();

                    string Name = string.Empty;
                    string cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP1.Email as BEmail,IP1.FirstName +' '+IP1.LastName as BName,IP2.Email as CEmail,IP2.FirstName +' '+IP2.LastName as CName FROM SupportTicketContact ST inner join IndSpouse IP on(ST.MemberID=IP.AutoMemberID) left join Indspouse IP1 on(ST.BMemberID=IP1.AutoMemberID) left join Indspouse IP2 on(ST.CMemberID=IP2.AutoMemberID)  WHERE EventID =" + ddlEvent.SelectedValue + "";

                    if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                    {
                        if (hdnIsProductGroup.Value != "Yes")
                        {
                            cmdTextVol += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                        }
                    }
                    if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                    {
                        if (hdnIsProduct.Value != "yes")
                        {
                            cmdTextVol += " and ProductID=" + ddlProduct.SelectedValue;
                        }
                    }
                    DataSet dsVol = new DataSet();
                    dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                    if (null != dsVol && dsVol.Tables.Count > 0)
                    {
                        if (dsVol.Tables[0].Rows.Count > 0)
                        {
                            SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                            ArrList.Add(dsVol.Tables[0].Rows[0]["Email"].ToString());
                            if (dsVol.Tables[0].Rows[0]["BEmail"].ToString() != "")
                            {
                                ArrList.Add(dsVol.Tables[0].Rows[0]["BEmail"].ToString());
                            }
                            if (dsVol.Tables[0].Rows[0]["CEmail"].ToString() != "")
                            {
                                ArrList.Add(dsVol.Tables[0].Rows[0]["CEmail"].ToString());
                            }
                            Name = dsVol.Tables[0].Rows[0]["Name"].ToString();

                            ArrNameList.Add(dsVol.Tables[0].Rows[0]["Name"].ToString());
                            if (dsVol.Tables[0].Rows[0]["BName"].ToString() != "")
                            {
                                ArrNameList.Add(dsVol.Tables[0].Rows[0]["BName"].ToString());
                            }
                            if (dsVol.Tables[0].Rows[0]["CName"].ToString() != "")
                            {
                                ArrNameList.Add(dsVol.Tables[0].Rows[0]["CName"].ToString());
                            }
                        }
                    }
                    if (SendToVol == "")
                    {
                        cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                        dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                        if (null != dsVol && dsVol.Tables.Count > 0)
                        {
                            if (dsVol.Tables[0].Rows.Count > 0)
                            {
                                SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                ArrList.Add(dsVol.Tables[0].Rows[0]["Email"].ToString());
                                Name = dsVol.Tables[0].Rows[0]["Name"].ToString();
                                ArrNameList.Add(dsVol.Tables[0].Rows[0]["Name"].ToString());
                            }
                        }
                    }

                    if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                    {
                        cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                        SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                        if (dr.HasRows)
                        {
                            dr.Read();
                            ArrList.Add(dr["Email"].ToString());
                            ArrNameList.Add(dr["Name"].ToString());
                        }
                    }


                    for (int i = 0; i < ArrList.Count; i++)
                    {
                        sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ArrNameList[i].ToString() + ",<BR> We received a support ticket : " + lblTicketNo.Text + ". Please look into it. <BR>" + txtEmailBody.Text, ArrList[i].ToString());
                    }

                }
                catch (Exception ex)
                {

                }
            }
        }
        catch
        {
        }
    }
    //Add new tickets before 24 hours time lapse
    public void InsertNewTicketByUser()
    {
        try
        {

            lblError.Text = "";
            if (lblTicketNo.Text.Length > 0)
            {
                Tnew.Text = "N";
                Ticket = lblTicketNo.Text;
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail == false)
                lblError.Text = "Please enter a valid email address.";
            else if (Int32.Parse(ddlChapter.SelectedItem.Value) == -1)
                lblError.Text = "Please select Chapter.";
            else if (Int32.Parse(ddlEvent.SelectedItem.Value) == -1)
                lblError.Text = "Please select Event.";
            else if (txtEmailBody.Text == "")
                lblError.Text = "The text box above cannot be empty.  Please describe the problem in detail.";
            else if (ddlEmailToCustomer.Text == "Y" && txtcc.Text == "")
            {

                lblError.Text = "The CC text box cannot be empty while Email To Customer value is 'Y'";

            }
            else
            {

                String cmdDuplicateTicket = string.Empty;
                cmdDuplicateTicket = "select count(*) from Support where Subject='" + txtSubject.Text.Trim() + "' and UMember_ID=" + Session["LoginID"].ToString() + "";

                string count = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdDuplicateTicket).ToString();

                if (count == "0")
                {
                    string sql = "";
                    if (ProfileFlag.Text == "N")
                    {
                        try
                        {
                            String getAutoMemberID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 ISNULL(AutoMemberID,0) as AutoMemberID FROM IndSpouse WHERE Email='" + txtEmail.Text.ToString() + "' ORDER BY Relationship").ToString();
                            if (getAutoMemberID.Length > 0)
                            {
                                ProfileFlag.Text = "Y";
                                lblLoginMemberid.Text = getAutoMemberID;
                            }
                        }
                        catch (Exception ex)
                        { }
                    }

                    string cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue;
                    if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                    {
                        cmdAssignTotext += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                    }
                    else
                    {
                        cmdAssignTotext += " and ProductGroupId is null";
                    }
                    if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                    {
                        cmdAssignTotext += " and ProductID=" + ddlProduct.SelectedValue;
                    }
                    else
                    {
                        cmdAssignTotext += " and ProductID is null";
                    }

                    string assignedTo = string.Empty;
                    string BMemberID = string.Empty;
                    string CMemberID = string.Empty;
                    string chapterCoordnator = string.Empty;
                    DataSet dsAssignto = new DataSet();
                    dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                    if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                    {
                        if (dsAssignto.Tables[0].Rows.Count > 0)
                        {
                            assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                            BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                            CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                        }
                        else
                        {
                            cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + "";
                            dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                            if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                            {
                                if (dsAssignto.Tables[0].Rows.Count > 0)
                                {
                                    hdnIsProduct.Value = "yes";
                                    assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                                    BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                                    CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                                }
                                else
                                {

                                    cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue + " ";
                                    dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                                    if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                                    {
                                        if (dsAssignto.Tables[0].Rows.Count > 0)
                                        {
                                            hdnIsProductGroup.Value = "Yes";
                                            hdnIsProduct.Value = "yes";
                                            assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                                            BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                                            CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                    {
                        string ccText = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP.AutoMemberID FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                        SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, ccText);
                        if (dr.Read())
                        {
                            chapterCoordnator = dr["AutoMemberID"].ToString();
                        }
                    }


                    sql = "INSERT INTO Support (TNew, TUser, EntryCode, ProfileFlag, UMember_ID, EntryEmail, FirstName, LastName, Chapter, Event_ID, Subject, Message, Priority,Status,HPhone,CPhone, CreateDate,CreatedBy,TType,RMemberID";
                    if (Tnew.Text != "Y")
                        sql = sql + ",Ticket_ID ";
                    if (ddlProductGroup.Items.Count > 0)
                    {

                        sql = sql + ",ProductGroup_ID,ProductGroupCode";
                    }
                    if (ddlProduct.Items.Count > 0)
                    {

                        sql = sql + ",Product_ID,ProductCode";
                    }
                    sql = sql + ", AssignTo, BMemberID, CMemberID, CCMemberID";
                    sql = sql + ") VALUES ('" + Tnew.Text + "','" + TUser.Text + "','" + ddlEntryCode.SelectedItem.Value + "','" + ProfileFlag.Text + "',";

                    if (ProfileFlag.Text == "N")
                        sql = sql + MemberID;
                    else
                        sql = sql + lblLoginMemberid.Text;
                    Status = ddlStatus.SelectedItem.Text;
                    sql = sql + ",'" + txtEmail.Text + "','" + txtFName.Text + "','" + txtLName.Text + "','" + ddlChapter.SelectedItem.Text + "'," + ddlEvent.SelectedItem.Value + ",'" + txtSubject.Text + "','" + txtEmailBody.Text + "','" + ddlPriority.SelectedItem.Text + "','" + Status + "','" + txtHPhone.Text + "','" + txtCPhone.Text + "',GetDate()," + Session["LoginID"] + ",'O',NULL";
                    //)
                    if (Tnew.Text != "Y")
                        sql = sql + ",'" + hdnTicketID.Value + "' ";

                    if (ddlProductGroup.Items.Count > 0)
                    {
                        if (Int32.Parse(ddlProductGroup.SelectedItem.Value) != 0 && Int32.Parse(ddlProductGroup.SelectedItem.Value) != -1)
                            sql = sql + "," + ddlProductGroup.SelectedItem.Value + ",'" + ddlProductGroup.SelectedItem.Text + "'";
                        else
                            sql = sql + ",NULL,NULL";
                    }
                    if (ddlProduct.Items.Count > 0)
                    {
                        if (Int32.Parse(ddlProduct.SelectedItem.Value) != 0 && Int32.Parse(ddlProduct.SelectedItem.Value) != -1 && ddlProduct.SelectedValue != "")
                            sql = sql + "," + ddlProduct.SelectedItem.Value + ",'" + ddlProduct.SelectedItem.Text + "'";
                        else
                            sql = sql + ",NULL,NULL";
                    }
                    if (assignedTo != "")
                    {
                        sql = sql + "," + assignedTo + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }
                    if (BMemberID != "")
                    {
                        sql = sql + "," + BMemberID + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }
                    if (CMemberID != "")
                    {
                        sql = sql + "," + CMemberID + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }

                    if (chapterCoordnator != "")
                    {
                        sql = sql + "," + chapterCoordnator + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }

                    sql = sql + ")  ";

                    if (Tnew.Text == "Y")
                        sql = sql + " Select Scope_Identity() ";

                    if (Status.Trim() == "Select Status")
                    {
                        Status = "Open";
                    }
                    try
                    {

                        if (Tnew.Text == "Y")
                        {
                            int Support_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID='" + Support_ID.ToString() + "' WHERE Support_ID=" + Support_ID.ToString());

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID= (select isnull(max(Ticket_ID)+1,1) from Support) WHERE Support_ID=" + Support_ID.ToString());

                            lblTicketNo.Text = "NSF#" + Support_ID.ToString();
                            hdnTicketID.Value = Support_ID.ToString();
                            hdnSupportID.Value = Support_ID.ToString();
                            int TicketID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(Ticket_ID) from Support"));
                            hdnSupportID.Value = Support_ID.ToString();
                            lblTicketNo.Text = "NSF#" + TicketID.ToString();
                            hdnTicketID.Value = TicketID.ToString();
                        }
                        else
                        {
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
                        }
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Status='" + Status + "' WHERE Ticket_ID='" + hdnTicketID.Value + "'");
                        lblError.Text = "A new message was added to the Ticket.";
                        sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi Sir/Madam,<BR> We received your support ticket : " + lblTicketNo.Text + " will get back to you soon. <BR>" + txtEmailBody.Text, txtEmail.Text);




                        string SendToVol = string.Empty;
                        ArrayList ArrList = new ArrayList();
                        ArrayList ArrNameList = new ArrayList();

                        string Name = string.Empty;
                        string cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP1.Email as BEmail,IP1.FirstName +' '+IP1.LastName as BName,IP2.Email as CEmail,IP2.FirstName +' '+IP2.LastName as CName FROM SupportTicketContact ST inner join IndSpouse IP on(ST.MemberID=IP.AutoMemberID) left join Indspouse IP1 on(ST.BMemberID=IP1.AutoMemberID) left join Indspouse IP2 on(ST.CMemberID=IP2.AutoMemberID)  WHERE EventID =" + ddlEvent.SelectedValue + "";

                        if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                        {
                            if (hdnIsProductGroup.Value != "Yes")
                            {
                                cmdTextVol += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                            }
                        }
                        if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                        {
                            if (hdnIsProduct.Value != "yes")
                            {
                                cmdTextVol += " and ProductID=" + ddlProduct.SelectedValue;
                            }
                        }
                        DataSet dsVol = new DataSet();
                        dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                        if (null != dsVol && dsVol.Tables.Count > 0)
                        {
                            if (dsVol.Tables[0].Rows.Count > 0)
                            {
                                SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                ArrList.Add(dsVol.Tables[0].Rows[0]["Email"].ToString());
                                if (dsVol.Tables[0].Rows[0]["BEmail"].ToString() != "")
                                {
                                    ArrList.Add(dsVol.Tables[0].Rows[0]["BEmail"].ToString());
                                }
                                if (dsVol.Tables[0].Rows[0]["CEmail"].ToString() != "")
                                {
                                    ArrList.Add(dsVol.Tables[0].Rows[0]["CEmail"].ToString());
                                }
                                Name = dsVol.Tables[0].Rows[0]["Name"].ToString();

                                ArrNameList.Add(dsVol.Tables[0].Rows[0]["Name"].ToString());
                                if (dsVol.Tables[0].Rows[0]["BName"].ToString() != "")
                                {
                                    ArrNameList.Add(dsVol.Tables[0].Rows[0]["BName"].ToString());
                                }
                                if (dsVol.Tables[0].Rows[0]["CName"].ToString() != "")
                                {
                                    ArrNameList.Add(dsVol.Tables[0].Rows[0]["CName"].ToString());
                                }
                            }
                        }
                        if (SendToVol == "")
                        {
                            cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                            dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                            if (null != dsVol && dsVol.Tables.Count > 0)
                            {
                                if (dsVol.Tables[0].Rows.Count > 0)
                                {
                                    SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                    ArrList.Add(dsVol.Tables[0].Rows[0]["Email"].ToString());
                                    Name = dsVol.Tables[0].Rows[0]["Name"].ToString();
                                    ArrNameList.Add(dsVol.Tables[0].Rows[0]["Name"].ToString());
                                }
                            }
                        }

                        //Send email to the National Coordinator
                        if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                        {
                            cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                            SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                            if (dr.HasRows)
                            {
                                dr.Read();
                                ArrList.Add(dr["Email"].ToString());
                                ArrNameList.Add(dr["Name"].ToString());
                            }
                        }


                        for (int i = 0; i < ArrList.Count; i++)
                        {
                            sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ArrNameList[i].ToString() + ",<BR> We received a support ticket : " + lblTicketNo.Text + ". Please look into it. <BR>" + txtEmailBody.Text, ArrList[i].ToString());
                        }
                        GetGridValues(int.Parse(Session["LoginID"].ToString()), false);
                    }


                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    lblError.Text = "This seems to be a duplicate. Please wait until you get a response.  Please close the pop-up Window.";
                }
            }
        }
        catch
        {
        }
    }

    //add new response to the user's ticket before 24 hrs time lapse.
    public void InserNewTicketByVol()
    {
        try
        {

            if (ddlAssignTo.SelectedValue == "0")
            {
                if (hdnAssignTo.Value != "0" && hdnAssignTo.Value != "")
                {
                    try
                    {

                        ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                    }
                    catch
                    {
                        ddlAssignTo.SelectedValue = "0";
                    }
                }
            }
            hdnUpdateFlag.Value = "Vol";
            lblError.Text = "";
            TUser.Text = "R";
            string Assignto = "NULL";
            if (ddlAssignTo.SelectedItem.Value != "0")
            {
                Assignto = ddlAssignTo.SelectedItem.Value;
            }
            if (lblTicketNo.Text.Length > 0)
            {
                Tnew.Text = "N";
                Ticket = lblTicketNo.Text;
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail == false)
                lblError.Text = "Please enter a valid email address";
            else if (ddlStatus.SelectedItem.Value == "0")
                lblError.Text = "Please select Status";
            else if (ddlStatus.SelectedValue != "Resolved" && ddlAssignTo.SelectedItem.Value == "0" && ddlKeepRivate.SelectedItem.Value == "Y")
            {

                lblError.Text = "Please select AssignTo";
            }
            else if (txtEmailBody.Text == "")
            {
                lblError.Text = "The text box above cannot be empty.  Please describe the problem in detail.";
            }
            else if (ddlEmailToCustomer.Text == "Y" && txtcc.Text == "")
            {

                lblError.Text = "The CC text box cannot be empty while Email To Customer value is 'Y'";

            }
            else if (ddlStatus.SelectedValue == "Re-opened" && hdnUMemberID.Value != Session["LoginID"].ToString())
            {
                lblError.Text = "You cannot Re-open the ticket....";
            }

            else
            {

                string sql = "";
                if (ProfileFlag.Text == "N")
                {
                    try
                    {
                        String getAutoMemberID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 ISNULL(AutoMemberID,0) as AutoMemberID FROM IndSpouse WHERE Email='" + txtEmail.Text.ToString() + "' ORDER BY Relationship").ToString();
                        if (getAutoMemberID.Length > 0)
                        {
                            ProfileFlag.Text = "Y";
                            lblLoginMemberid.Text = getAutoMemberID;
                        }
                    }
                    catch (Exception ex)
                    { }
                }

                string chapterCoordnator = string.Empty;
                if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                {

                    string ccText = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP.AutoMemberID FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                    SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, ccText);
                    if (dr.Read())
                    {
                        chapterCoordnator = dr["AutoMemberID"].ToString();
                    }
                }

                sql = "INSERT INTO Support (TNew, TUser, EntryCode, ProfileFlag, UMember_ID, EntryEmail, FirstName, LastName, Chapter, Event_ID, Subject, Message, Priority,Status,HPhone,CPhone, CreateDate,CreatedBy, AssignTo, SentEmailA, SentEmailC, Private,CCedEmails,TType,RMemberID,CCEmails, CCMemberID";
                if (Tnew.Text != "Y")
                    sql = sql + ",Ticket_ID ";

                if (ddlProductGroup.Items.Count > 0)
                {

                    sql = sql + ",ProductGroup_ID,ProductGroupCode";
                }
                if (ddlProduct.Items.Count > 0)
                {

                    sql = sql + ",Product_ID,ProductCode";
                }


                sql = sql + ") VALUES ('" + Tnew.Text + "','" + TUser.Text + "','" + ddlEntryCode.SelectedItem.Value + "','" + ProfileFlag.Text + "',";

                if (lblLoginMemberid.Text.Length == 0)
                    sql = sql + hdnUMemberID.Value;
                else
                    sql = sql + hdnUMemberID.Value;

                string Ttype = string.Empty;
                string RMemberID = string.Empty;
                string AssignTo = string.Empty;
                RMemberID = Session["LoginID"].ToString();
                if (hdnUMemberID.Value == RMemberID && ddlStatus.SelectedValue != "Forwarded")
                {
                    Ttype = "O";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                else if ((ddlAssignTo.SelectedValue == "0" || ddlAssignTo.SelectedValue == "") && ddlStatus.SelectedValue != "Forwarded")
                {
                    Ttype = "R";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                else
                {
                    Ttype = "F";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "'" + ddlAssignTo.SelectedValue + "'";
                }
                if (TUser.Text == "R" && ddlStatus.SelectedValue != "Forwarded" && ddlStatus.SelectedValue != "Pending Customer Input" && hdnUMemberID.Value != RMemberID)
                {
                    Ttype = "R";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                string ccMails = string.Empty;
                if (txtcc.Text == "")
                {
                    ccMails = "NULL";
                }
                else
                {
                    ccMails = "'" + txtcc.Text + "'";
                }
                if (ddlStatus.SelectedValue == "Resolved")
                {
                    AssignTo = "NULL";
                }
                Status = ddlStatus.SelectedValue;
                sql = sql + ",'" + txtEmail.Text + "','" + txtFName.Text + "','" + txtLName.Text + "','" + ddlChapter.SelectedItem.Text + "'," + ddlEvent.SelectedItem.Value + ",'" + txtSubject.Text + "','" + txtEmailBody.Text + "','" + ddlPriority.SelectedItem.Text + "','" + ddlStatus.SelectedItem.Text + "','" + txtHPhone.Text + "','" + txtCPhone.Text + "',GetDate()," + Session["LoginID"].ToString() + ", " + AssignTo + ",'" + ddlEmailAssignTo.SelectedItem.Value + "','" + ddlEmailToCustomer.SelectedItem.Value + "','" + ddlKeepRivate.SelectedItem.Value + "','" + txtcc.Text + "','" + Ttype + "','" + RMemberID + "'," + ccMails + "";

                //)

                if (chapterCoordnator != "")
                {
                    sql = sql + "," + chapterCoordnator + "";
                }
                else
                {
                    sql = sql + ",Null";
                }
                if (Tnew.Text != "Y")
                    sql = sql + ",'" + Ticket + "' ";

                if (ddlProductGroup.Items.Count > 0)
                {
                    if (Int32.Parse(ddlProductGroup.SelectedItem.Value) != 0 && Int32.Parse(ddlProductGroup.SelectedItem.Value) != -1)
                        sql = sql + "," + ddlProductGroup.SelectedItem.Value + ",'" + ddlProductGroup.SelectedItem.Text + "'";
                    else
                        sql = sql + ",NULL,NULL";
                }
                if (ddlProduct.Items.Count > 0)
                {
                    if (Int32.Parse(ddlProduct.SelectedItem.Value) != 0 && Int32.Parse(ddlProduct.SelectedItem.Value) != -1)
                        sql = sql + "," + ddlProduct.SelectedItem.Value + ",'" + ddlProduct.SelectedItem.Text + "'";
                    else
                        sql = sql + ",NULL,NULL";
                }

                sql = sql + ")  ";

                if (Tnew.Text == "Y")
                    sql = sql + " Select Scope_Identity() ";
                try
                {
                    string TicketID = "0";
                    int suppId = 0;

                    if (Tnew.Text == "Y")
                    {
                        int Support_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));


                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID= (select isnull(max(Ticket_ID)+1,1) from Support) WHERE Support_ID=" + Support_ID.ToString());
                        lblTicketNo.Text = "NSF#" + Support_ID.ToString();
                        hdnTicketID.Value = Support_ID.ToString();
                        TicketID = Support_ID.ToString();
                    }
                    else
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Status='" + ddlStatus.SelectedItem.Text + "' WHERE Support_ID=(select max(Support_ID) from Support where Ticket_ID ='" + lblTicketNo.Text + "')");

                    lblError.Text = "A new message was added to the Ticket";

                    String SendToVol = "";
                    string UserName = string.Empty;
                    TicketID = lblTicketNo.Text;
                    if (Int32.Parse(ddlAssignTo.SelectedItem.Value) != 0)
                    {
                        if (ddlStatus.SelectedValue == "Pending Customer Input")
                        {
                            string cmdText = "select * from Support where Ticket_ID=" + TicketID + " and Status='Open'";
                            string UMemberID = string.Empty;
                            DataSet ds = new DataSet();
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                            if (null != ds && ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    UMemberID = ds.Tables[0].Rows[0]["UMember_ID"].ToString();
                                }
                            }
                            cmdText = "select Email, FirstName, LastName from Indspouse where AutoMemberID=" + UMemberID + "";
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                            if (null != ds && ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    SendToVol = ds.Tables[0].Rows[0]["Email"].ToString().Trim();
                                    UserName = ds.Tables[0].Rows[0]["FirstName"].ToString().Trim() + " " + ds.Tables[0].Rows[0]["LastName"].ToString().Trim();
                                }
                            }
                        }
                        else
                        {
                            string cmdTextVol = "SELECT IP.Email FROM SupportTicketContact ST inner join IndSpouse IP on(ST.MemberID=IP.AutoMemberID) WHERE EventID =" + ddlEvent.SelectedValue + "and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + "";
                            DataSet dsVol = new DataSet();
                            dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                            if (null != dsVol && dsVol.Tables.Count > 0)
                            {
                                if (dsVol.Tables[0].Rows.Count > 0)
                                {
                                    SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                }
                            }
                            if (SendToVol == "")
                            {
                                cmdTextVol = "SELECT IP.Email FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                if (null != dsVol && dsVol.Tables.Count > 0)
                                {
                                    if (dsVol.Tables[0].Rows.Count > 0)
                                    {
                                        SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                    }
                                }
                            }
                        }
                    }
                    if (ddlKeepRivate.SelectedItem.Text == "N")
                        SendToVol = SendToVol + ";" + txtEmail.Text;
                    SendToVol = SendToVol + ";" + txtcc.Text.Replace(',', ';');
                    if (SendToVol.Length > 0)
                    {
                        if (ddlStatus.SelectedValue == "Pending Customer Input")
                        {
                            sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + UserName + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                        }
                        else
                        {
                            sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ddlAssignTo.SelectedItem.Text + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                        }
                    }

                    GetTicket(TicketID);


                }
                catch (Exception ex)
                {

                }
            }
        }
        catch
        {
        }
    }
    protected void RbtnForward_CheckedChanged(object sender, EventArgs e)
    {
        if (hdnUpdateFlag.Value == "User")
        {
            InsertNewTicketByUser();

            RbtnForward.Checked = false;
            RbtnNoForward.Checked = false;
            RbtnIgnore.Checked = false;
        }
        else
        {
            InserNewTicketByVol();
            RbtnForward.Checked = false;
            RbtnNoForward.Checked = false;
            RbtnIgnore.Checked = false;

        }
    }
    protected void RbtnNoForward_CheckedChanged(object sender, EventArgs e)
    {
        if (hdnUpdateFlag.Value == "User")
        {
            updateUserTicket();
            RbtnForward.Checked = false;
            RbtnNoForward.Checked = false;
            RbtnIgnore.Checked = false;

        }
        else
        {
            UpdateVolticket();

            RbtnForward.Checked = false;
            RbtnNoForward.Checked = false;
            RbtnIgnore.Checked = false;
        }
    }
    protected void RbtnIgnore_CheckedChanged(object sender, EventArgs e)
    {

        RbtnForward.Checked = false;
        RbtnNoForward.Checked = false;
        RbtnIgnore.Checked = false;
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "closeConfirmationBox();", true);
    }


    protected void DDlGridSorting_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            DataTable dt = new DataTable();
            dt = (DataTable)Session["TicketList"];
            DataView dv = dt.DefaultView;
            if (DDlGridSorting.SelectedValue == "By Ticket Number")
            {
                dv.Sort = "Ticket_ID DESC";
            }
            else if (DDlGridSorting.SelectedValue == "By Status")
            {
                dv.Sort = "sortStatus ASC";
            }
            else if (DDlGridSorting.SelectedValue == "By Priority")
            {
                dv.Sort = "sortPriority DESC";
            }
            dt = dv.ToTable();

            GVSupport.DataSource = dt;
            GVSupport.DataBind();
        }
        catch
        {
        }
    }

    //Adding new Tickets by user
    public void addTicket()
    {

        try
        {

            hdnUpdateFlag.Value = "User";
            lblError.Text = "";
            if (lblTicketNo.Text.Length > 0)
            {
                Tnew.Text = "N";
                Ticket = lblTicketNo.Text;
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail == false)
                lblError.Text = "Please enter a valid email address.";
            else if (Int32.Parse(ddlChapter.SelectedItem.Value) == -1)
                lblError.Text = "Please select Chapter.";
            else if (Int32.Parse(ddlEvent.SelectedItem.Value) == -1)
                lblError.Text = "Please select Event.";
            else if (txtSubject.Text == "")
                lblError.Text = "Please enter subject";
            else if (txtEmailBody.Text == "")
                lblError.Text = "The text box above cannot be empty.  Please describe the problem in detail.";
            else
            {
                String cmdDuplicateTicket = string.Empty;
                cmdDuplicateTicket = "select count(*) from Support where Subject='" + txtSubject.Text.Trim() + "' and UMember_ID=" + Session["LoginID"].ToString() + "";

                string count = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdDuplicateTicket).ToString();

                if (count == "0")
                {


                    string sql = "";
                    if (ProfileFlag.Text == "N")
                    {
                        try
                        {
                            String getAutoMemberID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 ISNULL(AutoMemberID,0) as AutoMemberID FROM IndSpouse WHERE Email='" + txtEmail.Text.ToString() + "' ORDER BY Relationship").ToString();
                            if (getAutoMemberID.Length > 0)
                            {
                                ProfileFlag.Text = "Y";
                                lblLoginMemberid.Text = getAutoMemberID;
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                    string cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue;
                    if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                    {
                        cmdAssignTotext += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                    }
                    else
                    {
                        cmdAssignTotext += " and ProductGroupId is null";
                    }
                    if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                    {
                        cmdAssignTotext += " and ProductID=" + ddlProduct.SelectedValue;
                    }
                    else
                    {
                        cmdAssignTotext += " and ProductID is null";
                    }
                    string assignedTo = string.Empty;
                    string BMemberID = string.Empty;
                    string CMemberID = string.Empty;
                    string chapterCoordnator = string.Empty;
                    DataSet dsAssignto = new DataSet();
                    dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                    if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                    {
                        if (dsAssignto.Tables[0].Rows.Count > 0)
                        {
                            assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                            BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                            CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                        }
                        else
                        {
                            cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + "";
                            dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                            if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                            {
                                if (dsAssignto.Tables[0].Rows.Count > 0)
                                {
                                    hdnIsProduct.Value = "yes";
                                    assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                                    BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                                    CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                                }
                                else
                                {

                                    cmdAssignTotext = "select MemberID,BMemberID,CMemberID from SupportTicketContact where EventID=" + ddlEvent.SelectedValue + " and ProductGroupID is null and ProductID is null ";
                                    dsAssignto = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdAssignTotext);
                                    if (null != dsAssignto && dsAssignto.Tables.Count > 0)
                                    {
                                        if (dsAssignto.Tables[0].Rows.Count > 0)
                                        {
                                            hdnIsProductGroup.Value = "Yes";
                                            hdnIsProduct.Value = "yes";
                                            assignedTo = dsAssignto.Tables[0].Rows[0]["MemberID"].ToString();
                                            BMemberID = dsAssignto.Tables[0].Rows[0]["BMemberID"].ToString();
                                            CMemberID = dsAssignto.Tables[0].Rows[0]["CMemberID"].ToString();
                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                    {

                        string ccText = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP.AutoMemberID FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                        SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, ccText);
                        if (dr.Read())
                        {

                            chapterCoordnator = dr["AutoMemberID"].ToString();
                        }
                    }


                    sql = "INSERT INTO Support (TNew, TUser, EntryCode, ProfileFlag, UMember_ID, EntryEmail, FirstName, LastName, Chapter, Event_ID, Subject, Message, Priority,Status,HPhone,CPhone, CreateDate,CreatedBy,TType,RMemberID";
                    if (Tnew.Text != "Y")
                        sql = sql + ",Ticket_ID ";
                    if (ddlProductGroup.Items.Count > 0)
                    {

                        sql = sql + ",ProductGroup_ID,ProductGroupCode";
                    }
                    if (ddlProduct.Items.Count > 0)
                    {

                        sql = sql + ",Product_ID,ProductCode";
                    }
                    sql = sql + ", AssignTo, BMemberID, CMemberID, CCMemberID";
                    sql = sql + ") VALUES ('" + Tnew.Text + "','" + TUser.Text + "','" + ddlEntryCode.SelectedItem.Value + "','" + ProfileFlag.Text + "',";

                    if (ProfileFlag.Text == "N")
                        sql = sql + MemberID;
                    else
                        sql = sql + lblLoginMemberid.Text;

                    sql = sql + ",'" + txtEmail.Text + "','" + txtFName.Text + "','" + txtLName.Text + "','" + ddlChapter.SelectedItem.Text + "'," + ddlEvent.SelectedItem.Value + ",'" + txtSubject.Text + "','" + txtEmailBody.Text + "','" + ddlPriority.SelectedItem.Text + "','" + Status + "','" + txtHPhone.Text + "','" + txtCPhone.Text + "',GetDate()," + Session["LoginID"].ToString() + ",'O',NULL";
                    //)
                    if (Tnew.Text != "Y")
                        sql = sql + ",'" + Ticket + "' ";

                    if (ddlProductGroup.Items.Count > 0)
                    {
                        if (Int32.Parse(ddlProductGroup.SelectedItem.Value) != 0 && Int32.Parse(ddlProductGroup.SelectedItem.Value) != -1)
                            sql = sql + "," + ddlProductGroup.SelectedItem.Value + ",'" + ddlProductGroup.SelectedItem.Text + "'";
                        else
                            sql = sql + ",NULL,NULL";
                    }
                    if (ddlProduct.Items.Count > 0)
                    {
                        if (Int32.Parse(ddlProduct.SelectedItem.Value) != 0 && Int32.Parse(ddlProduct.SelectedItem.Value) != -1 && ddlProduct.SelectedValue != "")
                            sql = sql + "," + ddlProduct.SelectedItem.Value + ",'" + ddlProduct.SelectedItem.Text + "'";
                        else
                            sql = sql + ",NULL,NULL";
                    }
                    if (assignedTo != "")
                    {
                        sql = sql + "," + assignedTo + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }
                    if (BMemberID != "")
                    {
                        sql = sql + "," + BMemberID + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }
                    if (CMemberID != "")
                    {
                        sql = sql + "," + CMemberID + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }
                    if (chapterCoordnator != "")
                    {
                        sql = sql + "," + chapterCoordnator + "";
                    }
                    else
                    {
                        sql = sql + ",NULL";
                    }

                    sql = sql + ")  ";

                    if (Tnew.Text == "Y")
                        sql = sql + " Select Scope_Identity() "; // to Generate Ticket#
                    try
                    {

                        DataSet dsDubCheck = new DataSet();
                        string cmdDupCheck = string.Empty;
                        int dupCount = 0;
                        string status = string.Empty;
                        string createdDate = string.Empty;
                        DateTime dtCreatedDate;
                        DateTime dtNow = DateTime.Now;
                        string TicketNo = string.Empty;
                        double hours = 0;

                        if (Ticket != "")
                        {
                            cmdDupCheck = "select Ticket_ID,Status,CreateDate from Support where Ticket_ID='" + hdnTicketID.Value + "'";
                            dsDubCheck = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDupCheck);

                            if (null != dsDubCheck && dsDubCheck.Tables.Count > 0)
                            {
                                if (dsDubCheck.Tables[0].Rows.Count > 0)
                                {
                                    dupCount = dsDubCheck.Tables[0].Rows.Count;
                                    status = dsDubCheck.Tables[0].Rows[0]["Status"].ToString();
                                    createdDate = Convert.ToDateTime(dsDubCheck.Tables[0].Rows[0]["CreateDate"].ToString()).ToString();
                                    TicketNo = dsDubCheck.Tables[0].Rows[0]["Ticket_ID"].ToString();
                                }
                            }
                            dtCreatedDate = Convert.ToDateTime(createdDate);
                            TimeSpan Ts = dtNow - dtCreatedDate;
                            hours = Ts.TotalHours;
                        }
                        if (Ticket != "")
                        {
                            if (TicketNo == hdnTicketID.Value && hours < 24 && status == "Open")
                            {
                                txtEmailBody.Text = txtEmailBody.Text.Replace("<p>", "").Replace("</p>", "").Replace("\r\n", "");


                                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);

                            }
                            else
                            {
                            }

                        }
                        else
                        {
                            if (Tnew.Text == "Y")
                            {
                                int Support_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));

                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID= (select isnull(max(Ticket_ID)+1,1) from Support) WHERE Support_ID=" + Support_ID.ToString());

                                lblTicketNo.Text = "NSF#" + Support_ID.ToString();

                                int TicketID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(Ticket_ID) from Support"));
                                hdnSupportID.Value = Support_ID.ToString();
                                lblTicketNo.Text = "NSF#" + TicketID.ToString();
                                hdnTicketID.Value = TicketID.ToString();
                            }
                            else
                            {
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
                            }
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Status='" + Status + "' WHERE Ticket_ID='" + hdnTicketID.Value + "'");
                            lblError.Text = "Ticket Added Successfully";
                            sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi Sir/Madam,<BR> We received your support ticket : " + lblTicketNo.Text + " will get back to you soon. <BR>" + txtEmailBody.Text, txtEmail.Text);

                            string SendToVol = string.Empty;
                            ArrayList ArrList = new ArrayList();
                            ArrayList ArrNameList = new ArrayList();

                            string Name = string.Empty;
                            string cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP1.Email as BEmail,IP1.FirstName +' '+IP1.LastName as BName,IP2.Email as CEmail,IP2.FirstName +' '+IP2.LastName as CName FROM SupportTicketContact ST inner join IndSpouse IP on(ST.MemberID=IP.AutoMemberID) left join Indspouse IP1 on(ST.BMemberID=IP1.AutoMemberID) left join Indspouse IP2 on(ST.CMemberID=IP2.AutoMemberID)  WHERE EventID =" + ddlEvent.SelectedValue + "";

                            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "-1")
                            {
                                if (hdnIsProductGroup.Value != "Yes")
                                {
                                    cmdTextVol += " and ProductGroupId=" + ddlProductGroup.SelectedValue;
                                }
                            }
                            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "-1")
                            {
                                if (hdnIsProduct.Value != "yes")
                                {
                                    cmdTextVol += " and ProductID=" + ddlProduct.SelectedValue;
                                }
                            }
                            DataSet dsVol = new DataSet();
                            dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                            ArrList.Clear();
                            ArrNameList.Clear();
                            if (null != dsVol && dsVol.Tables.Count > 0)
                            {
                                if (dsVol.Tables[0].Rows.Count > 0)
                                {
                                    SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                    ArrList.Add(dsVol.Tables[0].Rows[0]["Email"].ToString().ToLower());

                                    if (dsVol.Tables[0].Rows[0]["BEmail"].ToString() != "")
                                    {
                                        ArrList.Add(dsVol.Tables[0].Rows[0]["BEmail"].ToString().ToLower());
                                    }
                                    if (dsVol.Tables[0].Rows[0]["CEmail"].ToString() != "")
                                    {
                                        ArrList.Add(dsVol.Tables[0].Rows[0]["CEmail"].ToString().ToLower());
                                    }

                                    ArrList.Add("chitturi9@gmail.com");
                                    Name = dsVol.Tables[0].Rows[0]["Name"].ToString();

                                    ArrNameList.Add(dsVol.Tables[0].Rows[0]["Name"].ToString());

                                    if (dsVol.Tables[0].Rows[0]["BName"].ToString() != "")
                                    {
                                        ArrNameList.Add(dsVol.Tables[0].Rows[0]["BName"].ToString());
                                    }
                                    if (dsVol.Tables[0].Rows[0]["CName"].ToString() != "")
                                    {
                                        ArrNameList.Add(dsVol.Tables[0].Rows[0]["CName"].ToString());
                                    }

                                    ArrNameList.Add(" ");
                                }
                            }
                            if (SendToVol == "")
                            {
                                cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                if (null != dsVol && dsVol.Tables.Count > 0)
                                {
                                    if (dsVol.Tables[0].Rows.Count > 0)
                                    {
                                        ArrList.Clear();
                                        ArrNameList.Clear();

                                        SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString();
                                        ArrList.Add(SendToVol);
                                        Name = dsVol.Tables[0].Rows[0]["Name"].ToString();
                                        ArrNameList.Add(Name);
                                    }
                                }
                            }

                            //Send email to the National Coordinator
                            if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                            {
                                cmdTextVol = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                                SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                if (dr.HasRows)
                                {
                                    dr.Read();
                                    ArrList.Add(dr["Email"].ToString());
                                    ArrNameList.Add(dr["Name"].ToString());
                                }
                            }


                            for (int i = 0; i < ArrList.Count; i++)
                            {

                                sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ArrNameList[i].ToString() + ",<BR> We received a support ticket : " + lblTicketNo.Text + ". Please look into it. <BR>" + txtEmailBody.Text, ArrList[i].ToString());
                            }


                        }
                        GetGridValues(int.Parse(Session["LoginID"].ToString()), false);
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {

                    lblError.Text = "This seems to be a duplicate. Please wait until you get a response.  Please close the pop-up Window.";
                }
            }
        }
        catch
        {
        }
    }

    //Responding to the user's ticket by volunteers
    public void respondTickets()
    {
        try
        {

            if (ddlAssignTo.SelectedValue == "0")
            {
                if (hdnAssignTo.Value != "0" && hdnAssignTo.Value != "")
                {
                    try
                    {

                        ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                    }
                    catch
                    {
                        ddlAssignTo.SelectedValue = "0";
                    }
                }
            }
            hdnUpdateFlag.Value = "Vol";
            lblError.Text = "";
            TUser.Text = "R";
            string Assignto = "NULL";
            if (ddlAssignTo.SelectedItem.Value != "0")
            {
                Assignto = ddlAssignTo.SelectedItem.Value;
            }
            if (lblTicketNo.Text.Length > 0)
            {
                Tnew.Text = "N";
                Ticket = lblTicketNo.Text;
            }
            bool isEmail = Regex.IsMatch(txtEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            if (isEmail == false)
                lblError.Text = "Please enter a valid email address";
            else if (ddlStatus.SelectedItem.Value == "0")
                lblError.Text = "Please select Status";
            else if (ddlStatus.SelectedValue != "Resolved" && ddlAssignTo.SelectedItem.Value == "0" && ddlKeepRivate.SelectedItem.Value == "Y")
            {

                lblError.Text = "Please select AssignTo";
            }
            else if (txtSubject.Text == "")
            {
                lblError.Text = "Please enter subject";
            }
            else if (txtEmailBody.Text == "")
            {
                lblError.Text = "The text box above cannot be empty.  Please describe the problem in detail.";
            }
            else if (ddlEmailToCustomer.Text == "Y" && txtcc.Text == "")
            {

                lblError.Text = "The CC text box cannot be empty while Email To Customer value is 'Y'";

            }
            else if (ddlStatus.SelectedValue == "Re-opened" && hdnUMemberID.Value != Session["LoginID"].ToString())
            {
                lblError.Text = "You cannot Re-open the ticket....";
            }


            else
            {

                string sql = "";
                if (ProfileFlag.Text == "N")
                {
                    try
                    {
                        String getAutoMemberID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 ISNULL(AutoMemberID,0) as AutoMemberID FROM IndSpouse WHERE Email='" + txtEmail.Text.ToString() + "' ORDER BY Relationship").ToString();
                        if (getAutoMemberID.Length > 0)
                        {
                            ProfileFlag.Text = "Y";
                            lblLoginMemberid.Text = getAutoMemberID;
                        }
                    }
                    catch (Exception ex)
                    { }
                }

                string chapterCoordnator = string.Empty;
                if (ddlChapter.SelectedIndex != -1 & (ddlEvent.SelectedValue.ToString() == "2" || ddlEvent.SelectedValue.ToString() == "3"))
                {

                    string ccText = "SELECT IP.Email,IP.FirstName +' '+IP.LastName as Name,IP.AutoMemberID FROM  IndSpouse IP Inner Join Volunteer V on V.MemberId=IP.Automemberid WHERE V.ChapterId=" + ddlChapter.SelectedValue + " and V.RoleId=5";
                    SqlDataReader dr = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, ccText);
                    if (dr.Read())
                    {
                        chapterCoordnator = dr["AutoMemberID"].ToString();
                    }
                }


                sql = "INSERT INTO Support (TNew, TUser, EntryCode, ProfileFlag, UMember_ID, EntryEmail, FirstName, LastName, Chapter, Event_ID, Subject, Message, Priority,Status,HPhone,CPhone, CreateDate,CreatedBy, AssignTo, SentEmailA, SentEmailC, Private,CCedEmails,TType,RMemberID,CCEmails, CCMemberID";
                if (Tnew.Text != "Y")
                    sql = sql + ",Ticket_ID ";

                if (ddlProductGroup.Items.Count > 0)
                {

                    sql = sql + ",ProductGroup_ID,ProductGroupCode";
                }
                if (ddlProduct.Items.Count > 0)
                {

                    sql = sql + ",Product_ID,ProductCode";
                }




                sql = sql + ") VALUES ('" + Tnew.Text + "','" + TUser.Text + "','" + ddlEntryCode.SelectedItem.Value + "','" + ProfileFlag.Text + "',";
                //+ MemberID +
                if (lblLoginMemberid.Text.Length == 0)
                    sql = sql + hdnUMemberID.Value;
                else
                    sql = sql + hdnUMemberID.Value;

                string Ttype = string.Empty;
                string RMemberID = string.Empty;
                string AssignTo = string.Empty;
                RMemberID = Session["LoginID"].ToString();
                if (hdnUMemberID.Value == RMemberID && ddlStatus.SelectedValue != "Forwarded")
                {
                    Ttype = "O";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                else if ((ddlAssignTo.SelectedValue == "0" || ddlAssignTo.SelectedValue == "") && ddlStatus.SelectedValue != "Forwarded")
                {
                    Ttype = "R";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                else
                {
                    Ttype = "F";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "'" + ddlAssignTo.SelectedValue + "'";
                }
                if (TUser.Text == "R" && ddlStatus.SelectedValue != "Forwarded" && ddlStatus.SelectedValue != "Pending Customer Input" && hdnUMemberID.Value != RMemberID)
                {
                    Ttype = "R";
                    RMemberID = Session["LoginID"].ToString();
                    AssignTo = "NULL";
                }
                string ccMails = string.Empty;
                if (txtcc.Text == "")
                {
                    ccMails = "NULL";
                }
                else
                {
                    ccMails = "'" + txtcc.Text + "'";
                }
                if (ddlStatus.SelectedValue == "Resolved")
                {
                    AssignTo = "NULL";
                }
                Status = ddlStatus.SelectedValue;
                sql = sql + ",'" + txtEmail.Text + "','" + txtFName.Text + "','" + txtLName.Text + "','" + ddlChapter.SelectedItem.Text + "'," + ddlEvent.SelectedItem.Value + ",'" + txtSubject.Text + "','" + txtEmailBody.Text + "','" + ddlPriority.SelectedItem.Text + "','" + ddlStatus.SelectedItem.Text + "','" + txtHPhone.Text + "','" + txtCPhone.Text + "',GetDate()," + Session["LoginID"].ToString() + ", " + AssignTo + ",'" + ddlEmailAssignTo.SelectedItem.Value + "','" + ddlEmailToCustomer.SelectedItem.Value + "','" + ddlKeepRivate.SelectedItem.Value + "','" + txtcc.Text + "','" + Ttype + "','" + RMemberID + "'," + ccMails + "";
                //)

                if (chapterCoordnator != "")
                {
                    sql = sql + "," + chapterCoordnator + " ";
                }
                else
                {
                    sql = sql + ",Null ";
                }

                if (Tnew.Text != "Y")
                    sql = sql + ",'" + Ticket + "' ";

                if (ddlProductGroup.Items.Count > 0)
                {
                    if (Int32.Parse(ddlProductGroup.SelectedItem.Value) != 0 && Int32.Parse(ddlProductGroup.SelectedItem.Value) != -1)
                        sql = sql + "," + ddlProductGroup.SelectedItem.Value + ",'" + ddlProductGroup.SelectedItem.Text + "'";
                    else
                        sql = sql + ",NULL,NULL";
                }
                if (ddlProduct.Items.Count > 0)
                {
                    if (Int32.Parse(ddlProduct.SelectedItem.Value) != 0 && Int32.Parse(ddlProduct.SelectedItem.Value) != -1)
                        sql = sql + "," + ddlProduct.SelectedItem.Value + ",'" + ddlProduct.SelectedItem.Text + "'";
                    else
                        sql = sql + ",NULL,NULL";
                }

                sql = sql + ")  ";

                if (Tnew.Text == "Y")
                    sql = sql + " Select Scope_Identity() "; // to Generate Ticket#
                try
                {
                    DataSet dsDubCheck = new DataSet();
                    string cmdDupCheck = string.Empty;
                    int dupCount = 0;
                    string status = string.Empty;
                    string createdDate = string.Empty;
                    DateTime dtCreatedDate;
                    DateTime dtNow = DateTime.Now;
                    string TicketNo = string.Empty;
                    double hours = 0;
                    int CurSuportID = 0;
                    int PrevSupportID = 0;
                    string CurRMemberID = string.Empty;

                    if (Ticket != "")
                    {
                        cmdDupCheck = "select Ticket_ID,Status,CreateDate,Support_ID,RMemberID from Support where Ticket_ID='" + hdnTicketID.Value + "'";
                        dsDubCheck = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdDupCheck);

                        if (null != dsDubCheck && dsDubCheck.Tables.Count > 0)
                        {
                            if (dsDubCheck.Tables[0].Rows.Count > 0)
                            {
                                dupCount = dsDubCheck.Tables[0].Rows.Count;
                                status = dsDubCheck.Tables[0].Rows[0]["Status"].ToString();
                                if (dupCount == 1)
                                {
                                    PrevSupportID = Convert.ToInt32(dsDubCheck.Tables[0].Rows[dupCount - 1]["Support_ID"].ToString());
                                }
                                else
                                {
                                    PrevSupportID = Convert.ToInt32(dsDubCheck.Tables[0].Rows[dupCount - 2]["Support_ID"].ToString());
                                }
                                foreach (DataRow dr in dsDubCheck.Tables[0].Rows)
                                {
                                    createdDate = Convert.ToDateTime(dr["CreateDate"].ToString()).ToString();
                                    if (dr["RMemberID"] != null && dr["RMemberID"].ToString() != "")
                                    {
                                        CurRMemberID = dr["RMemberID"].ToString();
                                        CurSuportID = Convert.ToInt32(dr["Support_ID"].ToString());
                                    }
                                }
                                TicketNo = dsDubCheck.Tables[0].Rows[0]["Ticket_ID"].ToString();
                            }
                        }
                        dtCreatedDate = Convert.ToDateTime(createdDate);
                        TimeSpan Ts = dtNow - dtCreatedDate;
                        hours = Ts.TotalHours;
                    }
                    string TicketID = "0";
                    int suppId = 0;
                    if (dupCount == 1)
                    {

                        if (Tnew.Text == "Y")
                        {
                            int Support_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID='" + Support_ID.ToString() + "' WHERE Support_ID=" + Support_ID.ToString());
                            lblTicketNo.Text = "NSF#" + Support_ID.ToString();
                            hdnTicketID.Value = Support_ID.ToString();
                            TicketID = Support_ID.ToString();
                        }
                        else
                        {
                            suppId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));
                        }
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Status='" + ddlStatus.SelectedItem.Text + "' WHERE Support_ID='" + suppId + "'");
                        lblError.Text = "A new message was added to the Ticket";
                        TicketID = lblTicketNo.Text;

                        String SendToVol = "";
                        string UserName = string.Empty;
                        if (Int32.Parse(ddlAssignTo.SelectedItem.Value) != 0)
                        {
                            if (ddlStatus.SelectedValue == "Pending Customer Input")
                            {
                                string cmdText = "select * from Support where Ticket_ID=" + TicketID + " and Status='Open'";
                                string UMemberID = string.Empty;
                                DataSet ds = new DataSet();
                                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                                if (null != ds && ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        UMemberID = ds.Tables[0].Rows[0]["UMember_ID"].ToString();
                                    }
                                }
                                cmdText = "select Email, FirstName, LastName from Indspouse where AutoMemberID=" + UMemberID + "";
                                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                                if (null != ds && ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        SendToVol = ds.Tables[0].Rows[0]["Email"].ToString().Trim();
                                        UserName = ds.Tables[0].Rows[0]["FirstName"].ToString().Trim() + " " + ds.Tables[0].Rows[0]["LastName"].ToString().Trim();
                                    }
                                }
                            }
                            else
                            {
                                string cmdTextVol = string.Empty;

                                cmdTextVol = "SELECT IP.Email FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                DataSet dsVol = new DataSet();
                                dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                if (null != dsVol && dsVol.Tables.Count > 0)
                                {
                                    if (dsVol.Tables[0].Rows.Count > 0)
                                    {
                                        SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString().Trim();
                                    }
                                }
                                if (SendToVol == "")
                                {
                                    cmdTextVol = "SELECT IP.Email FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                    dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                    if (null != dsVol && dsVol.Tables.Count > 0)
                                    {
                                        if (dsVol.Tables[0].Rows.Count > 0)
                                        {
                                            SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString().Trim();
                                        }
                                    }
                                }
                            }

                            if (ddlKeepRivate.SelectedItem.Text == "N")
                            {
                                SendToVol = SendToVol + ";" + txtEmail.Text;
                            }
                            SendToVol = SendToVol + ";" + txtcc.Text.Replace(',', ';');
                            if (SendToVol.Length > 0)
                            {
                                if (ddlStatus.SelectedValue == "Pending Customer Input")
                                {
                                    sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + UserName + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                                }
                                else
                                {
                                    sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ddlAssignTo.SelectedItem.Text + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (TicketNo == hdnTicketID.Value && hours < 24 && RMemberID != null && (CurSuportID > PrevSupportID))
                        {


                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);

                        }
                        else
                        {

                            if (Tnew.Text == "Y")
                            {
                                int Support_ID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sql));


                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Ticket_ID= (select isnull(max(Ticket_ID)+1,1) from Support) WHERE Support_ID=" + Support_ID.ToString());
                                lblTicketNo.Text = "NSF#" + Support_ID.ToString();
                                hdnTicketID.Value = Support_ID.ToString();
                                TicketID = Support_ID.ToString();
                            }
                            else
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE Support SET Status='" + ddlStatus.SelectedItem.Text + "' WHERE Support_ID=(select max(Support_ID) from Support where Ticket_ID ='" + lblTicketNo.Text + "')");

                            lblError.Text = "A new message was added to the Ticket";

                            String SendToVol = "";
                            string UserName = string.Empty;
                            TicketID = lblTicketNo.Text;
                            if (Int32.Parse(ddlAssignTo.SelectedItem.Value) != 0)
                            {
                                if (ddlStatus.SelectedValue == "Pending Customer Input")
                                {
                                    string cmdText = "select * from Support where Ticket_ID=" + TicketID + " and Status='Open'";
                                    string UMemberID = string.Empty;

                                    DataSet ds = new DataSet();
                                    ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                                    if (null != ds && ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            UMemberID = ds.Tables[0].Rows[0]["UMember_ID"].ToString();
                                        }
                                    }
                                    cmdText = "select Email, FirstName, LastName from Indspouse where AutoMemberID=" + UMemberID + "";
                                    ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                                    if (null != ds && ds.Tables.Count > 0)
                                    {
                                        if (ds.Tables[0].Rows.Count > 0)
                                        {
                                            SendToVol = ds.Tables[0].Rows[0]["Email"].ToString().Trim();
                                            UserName = ds.Tables[0].Rows[0]["FirstName"].ToString().Trim() + " " + ds.Tables[0].Rows[0]["LastName"].ToString().Trim();
                                        }
                                    }
                                }
                                else
                                {
                                    string cmdTextVol = "SELECT IP.Email FROM SupportTicketContact ST inner join IndSpouse IP on(ST.MemberID=IP.AutoMemberID) WHERE EventID =" + ddlEvent.SelectedValue + "and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + "";

                                    cmdTextVol = "SELECT IP.Email FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                    DataSet dsVol = new DataSet();
                                    dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                    if (null != dsVol && dsVol.Tables.Count > 0)
                                    {
                                        if (dsVol.Tables[0].Rows.Count > 0)
                                        {
                                            SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString().Trim();
                                        }
                                    }
                                    if (SendToVol == "")
                                    {
                                        cmdTextVol = "SELECT IP.Email FROM  IndSpouse IP WHERE IP.AutoMemberID =" + ddlAssignTo.SelectedValue + "";
                                        dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTextVol);
                                        if (null != dsVol && dsVol.Tables.Count > 0)
                                        {
                                            if (dsVol.Tables[0].Rows.Count > 0)
                                            {
                                                SendToVol = dsVol.Tables[0].Rows[0]["Email"].ToString().Trim();
                                            }
                                        }
                                    }
                                }
                            }
                            if (ddlKeepRivate.SelectedItem.Text == "N")
                                SendToVol = SendToVol + ";" + txtEmail.Text;
                            SendToVol = SendToVol + ";" + txtcc.Text.Replace(',', ';');
                            if (SendToVol.Length > 0)
                            {
                                if (ddlStatus.SelectedValue == "Pending Customer Input")
                                {
                                    sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + UserName + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                                }
                                else
                                {
                                    sentemail(lblTicketNo.Text + " " + txtSubject.Text, "Hi " + ddlAssignTo.SelectedItem.Text + ",<BR> Support ticket : " + lblTicketNo.Text + " <BR>" + txtEmailBody.Text, SendToVol);
                                }

                            }
                        }
                    }

                    GetTicket(TicketID);
                }
                catch (Exception ex)
                {

                }
            }
        }
        catch
        {
        }
    }

    //Show/hide tickets and panels according to the user roles
    public void manageShowHideTickets()
    {

        try
        {

            GetChapters(ddlChapter);
            GetProductGroup(ddlProductGroup, ddlEvent);
            if (Request.QueryString["NSF_Ticket"] == null)
            {
            }
            else
            {
                GetTicket(decode(Request.QueryString["NSF_Ticket"]));
                tblrbtn.Visible = false;
                hlnkMainMenu.Visible = false;

                if (Session["entryToken"].ToString() != "Volunteer")
                {
                    tblNew.Visible = false;
                    rptA.Visible = false;
                    hlBackToList.Visible = false;
                }
            }
            if (Session["LoginID"] != null)
            {
                lblTitle.Visible = false;
                if (Session["entryToken"].ToString() == "Volunteer" && Request.QueryString["NewTicket"] == null && (Request.QueryString["showmine"] == null))
                {
                    lblTitle.Visible = false;
                    trVol.Visible = true;
                    GetAssignee(ddlAssignTo);
                    try
                    {
                        ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                    }
                    catch
                    {
                        ddlAssignTo.SelectedValue = "0";
                    }
                    btnVolSubmit.Visible = true;
                    btnSubmit.Visible = false;
                    tblrbtn.Visible = false;

                    if (Request.QueryString["NSF_Ticket"] == null)
                    {
                        lblTitle.Visible = false;
                        tblNew.Visible = false;
                        trFilter.Visible = true;

                        ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("-1"));
                        if (Request.QueryString["NSFEvnt"] != null && Session["RoleId"] != null)
                        {
                            string EvntID = Request.QueryString["NSFEvnt"];
                            int RoleID = Convert.ToInt32(Session["RoleId"]);
                            if (RoleID == 1 || RoleID == 2 || RoleID == 29) //Customer Care all Events
                                ddlFEvent.Enabled = true;

                            if (EvntID == "1" && (RoleID == 1 || RoleID == 2 || RoleID == 5 || RoleID == 36)) // Finals
                                ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("1"));
                            else if (EvntID == "2" && (RoleID == 1 || RoleID == 2 || RoleID == 5)) // Chapter Co
                                ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("2"));
                            else if (EvntID == "3" && (RoleID == 1 || RoleID == 2 || RoleID == 5 || RoleID == 11)) // Workshop 
                                ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("3"));
                            else if (EvntID == "4" && (RoleID == 1 || RoleID == 2 || RoleID == 87)) // Game
                                ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("4"));
                            else if (EvntID == "13") // Coach Admin & Coach
                            {
                                if (RoleID == 1 || RoleID == 2 || RoleID == 89 || RoleID == 96) // Coach Admin - All Products
                                    ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("13"));
                                else if (RoleID == 88)
                                {
                                    string strSql = "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=88 and TeamLead='Y' and MemberID=" + Session["LoginID"].ToString();
                                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql)) > 0)
                                        ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("13"));
                                    else
                                    {
                                        btnContinue.Enabled = false;
                                        lblerr.Text = "You/Your Role not authorised to view this page";
                                    }
                                }
                                else
                                {
                                    btnContinue.Enabled = false;
                                    lblerr.Text = "You/Your Role not authorised to view this page";
                                }
                            }
                            else if (EvntID == "19" && (RoleID == 1 || RoleID == 2 || RoleID == 5 || RoleID == 94)) // Preclub Coordinator  
                                ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("19"));
                            else if (EvntID == "20" && (RoleID == 1 || RoleID == 2 || RoleID == 97 || RoleID == 96)) // Online WorkShop
                            {
                                ddlFEvent.SelectedIndex = ddlFEvent.Items.IndexOf(ddlFEvent.Items.FindByValue("20"));
                                if (RoleID == 96)
                                {
                                    ddlFEvent.Enabled = false;
                                }
                            }
                            else if (RoleID == 1 || RoleID == 2 || RoleID == 29)
                            {

                            }
                            else
                            {
                                btnContinue.Enabled = false;
                                lblerr.Text = "You/Your Role not authorised to view this page, Role ID =" + RoleID + ";";

                            }

                            if (Int32.Parse(ddlFEvent.SelectedItem.Value) > 0)
                            {
                                if (RoleID == 88 || RoleID == 89)

                                    GetProductGroup(ddlFProductGroup, ddlFEvent);
                                else
                                    GetProductGroup(ddlFProductGroup, ddlFEvent);
                            }
                        }
                    }

                }
                else if (((Session["entryToken"].ToString() == "Parent") || (Session["entryToken"].ToString() == "Donor") || (Request.QueryString["NewTicket"] == "true") || Request.QueryString["showmine"] != null) && (Session["entryToken"].ToString() != "Volunteer"))
                {

                    lblTitle.Visible = false;
                    trVol.Visible = false;

                    if (Request.QueryString["NewTicket"] == "true" || Request.QueryString["showmine"] == "true" || Request.QueryString["View"] == "yes")
                    {
                        LoginMemberiid = int.Parse(Session["LoginID"].ToString());
                    }
                    else
                    {
                        LoginMemberiid = int.Parse(Session["CustIndID"].ToString());
                    }
                    getUserDetails("P", LoginMemberiid.ToString());
                    lblLoginMemberid.Text = LoginMemberiid.ToString();
                    tblrbtn.Visible = false;
                    if (Session["entryToken"].ToString() == "Parent")
                    {
                        hlnkMainMenu.NavigateUrl = "UserFunctions.aspx";

                    }
                    else if (Session["entryToken"].ToString() == "Volunteer")
                    {
                        hlnkMainMenu.NavigateUrl = "VolunteerFunctions.aspx";
                    }
                    else
                    {
                        hlnkMainMenu.NavigateUrl = "DonorFunctions.aspx";
                    }
                    if (Request.QueryString["View"] != null || Request.QueryString["showmine"] == "true")
                    {
                        GetGridValues(LoginMemberiid, false);
                        lblTitle.Visible = true;
                        lblTitle.Text = "Table 1: Ticket List";

                        tblNew.Visible = false;
                        dvSupportFilters.Visible = false;

                    }
                    if (Request.QueryString["NewTicket"] != "true" && Request.QueryString["View"] == null || Request.QueryString["showmine"] != "true")
                    {
                        if (Request.QueryString["NewTicket"] != "true")
                        {
                            tblNew.Visible = true;
                            trVol.Visible = true;

                            GetAssignee(ddlAssignTo);
                            try
                            {

                                ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                            }
                            catch
                            {
                                ddlAssignTo.SelectedValue = "0";
                            }
                            btnVolSubmit.Visible = true;
                            btnSubmit.Visible = false;
                            tblrbtn.Visible = false;
                        }
                        else
                        {
                            GetGridValues(LoginMemberiid, false);
                            lblTitle.Visible = true;
                            lblTitle.Text = "Table 1: Ticket History";
                        }
                    }
                    else
                    {
                        tblNew.Visible = false;
                        trVol.Visible = false;
                        dvSupportFilters.Visible = false;
                        GetAssignee(ddlAssignTo);
                        try
                        {

                            ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                        }
                        catch
                        {
                            ddlAssignTo.SelectedValue = "0";
                        }
                        btnVolSubmit.Visible = false;
                        btnSubmit.Visible = false;
                        tblrbtn.Visible = false;
                    }

                    ddlEmailAssignTo.SelectedValue = "N";
                    ddlEmailAssignTo.Enabled = false;
                    ddlEmailToCustomer.SelectedValue = "N";
                    ddlEmailToCustomer.Visible = false;
                    ddlKeepRivate.SelectedValue = "N";
                    ddlKeepRivate.Visible = false;
                    txtcc.Visible = false;
                    spnEmailToCustomer.Visible = false;
                    spnKeepPrivate.Visible = false;
                    spnCC.Visible = false;

                }
                else if (((Session["entryToken"].ToString() == "Parent") || (Session["entryToken"].ToString() == "Donor") || (Session["entryToken"].ToString() == "Volunteer")) && (Request.QueryString["NewTicket"] == "true"))
                {
                    trVol.Visible = false;

                    if (Request.QueryString["NewTicket"] == "true" || Request.QueryString["showmine"] == "true")
                    {
                        LoginMemberiid = int.Parse(Session["LoginID"].ToString());
                    }
                    else
                    {
                        LoginMemberiid = int.Parse(Session["CustIndID"].ToString());
                    }
                    getUserDetails("P", LoginMemberiid.ToString());
                    lblLoginMemberid.Text = LoginMemberiid.ToString();
                    tblrbtn.Visible = false;
                    if (Session["entryToken"].ToString() == "Parent")
                    {
                        hlnkMainMenu.NavigateUrl = "UserFunctions.aspx";

                    }
                    else if (Session["entryToken"].ToString() == "Volunteer")
                    {
                        hlnkMainMenu.NavigateUrl = "VolunteerFunctions.aspx";
                    }
                    else
                    {
                        hlnkMainMenu.NavigateUrl = "DonorFunctions.aspx";
                    }
                    if (Request.QueryString["View"] != null || Request.QueryString["showmine"] == "true" || Request.QueryString["NewTicket"] == "true")
                    {
                        GetGridValues(LoginMemberiid, false);

                        lblTitle.Visible = true;
                        lblTitle.Text = "Table 1: Ticket History";
                    }

                }

                else if (Session["entryToken"].ToString() == "Volunteer" && Request.QueryString["NewTicket"] == null && (Request.QueryString["showmine"] == "true"))
                {
                    lblTitle.Visible = true;
                    lblTitle.Text = "Table 1: Ticket List";

                    trVol.Visible = false;

                    if (Request.QueryString["NewTicket"] == "true" || Request.QueryString["showmine"] == "true")
                    {
                        LoginMemberiid = int.Parse(Session["LoginID"].ToString());
                    }
                    else
                    {
                        LoginMemberiid = int.Parse(Session["CustIndID"].ToString());
                    }
                    getUserDetails("P", LoginMemberiid.ToString());
                    lblLoginMemberid.Text = LoginMemberiid.ToString();
                    tblrbtn.Visible = false;
                    if (Session["entryToken"].ToString() == "Parent")
                    {
                        hlnkMainMenu.NavigateUrl = "UserFunctions.aspx";

                    }
                    else if (Session["entryToken"].ToString() == "Volunteer")
                    {
                        hlnkMainMenu.NavigateUrl = "VolunteerFunctions.aspx";
                    }
                    else
                    {
                        hlnkMainMenu.NavigateUrl = "DonorFunctions.aspx";
                    }
                    if (Request.QueryString["View"] != null || Request.QueryString["showmine"] == "true")
                    {
                        GetGridValues(LoginMemberiid, false);
                        tblNew.Visible = false;
                        dvSupportFilters.Visible = false;
                    }
                    tblNew.Visible = false;
                    trVol.Visible = false;

                    GetAssignee(ddlAssignTo);
                    try
                    {

                        ddlAssignTo.SelectedValue = hdnAssignTo.Value;
                    }
                    catch
                    {
                        ddlAssignTo.SelectedValue = "0";
                    }
                    btnVolSubmit.Visible = false;
                    btnSubmit.Visible = false;
                    tblrbtn.Visible = false;
                }

                else
                {
                    lblTitle.Visible = false;
                    trVol.Visible = false;

                }
            }
            else
            {
                tblrbtn.Visible = false;
                hlnkMainMenu.Visible = false;
            }
            if (Request.QueryString["Action"] == null)
            {

            }
            else
            {
                trFilter.Visible = false;
                hlnkMainMenu.Visible = false;
                lblUpdateOpt.Text = "This appears to be a duplicate.";
                TblUpdateTicket.Visible = true;
            }
            ddlFEvent_SelectedIndexChanged(ddlFEvent, new EventArgs());
        }
        catch
        {
        }
    }
}
