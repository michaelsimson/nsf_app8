<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="ChangeCoaching.aspx.vb" Inherits="Admin_ChangeCoaching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <link href="../css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="../css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../js/jquery.qtip.min.js"></script>

        <script src="../js/ezmodal.js"></script>
        <script type="text/javascript">
            $(function (e) {


                $(".lnkCoachName").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {
                            return '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx?coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Calendar signup of that coach (all signups)</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachClassCalendar.aspx?coachID=' + $(this).attr("attr-coachid") + '"" target="_blank">Set up Class Calendar</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '" target="_blank" >Contact List of Students</a> </div>';
                        },

                        title: 'Online Coaching!',
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow'
                    },
                    show: {
                        solo: true
                    }
                })

                $(".lnkParentName").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {
                            return '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + $(this).attr("attr-parentId") + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx" target="_blank">Change Coach</a> </div>';
                        },

                        title: 'Online Coaching!',
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow'
                    },
                    show: {
                        solo: true
                    }
                })


            });

            $(document).on("mouseover", ".ancParentInfo", function (e) {

                getParentInfo($(this).attr("attr-parentID"));
            });

            $(document).on("click", ".ancParentInfo", function (e) {
                $("#btnPopUP").trigger("click");
            });

            function getParentInfo(pMemberID) {

                var jsonData = { PMemberID: pMemberID };
                $.ajax({
                    type: "POST",
                    url: "../SetupZoomSessions.aspx/ListParentInfo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {

                        var tblHtml = "";
                        tblHtml += " <thead>";
                        tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent1 Name</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Parent2 Name</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>Parent1 Email</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent2 Email</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child Name</td>";


                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child EMail</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"


                        tblHtml += "</tr>";
                        tblHtml += " </thead>";

                        $.each(data.d, function (index, value) {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndFirstName + " " + value.IndLastName + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseFirstName + " " + value.SpouseLastName + "";
                            tblHtml += "</td>";


                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndEMail + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseEmail + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildFirstName + " " + value.ChildLastName + "";
                            tblHtml += "</td>";



                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildEmail + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                            tblHtml += "</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                            tblHtml += "</td>";


                            tblHtml += "</tr>";
                        });

                        $("#tblParentInfo").html(tblHtml);


                    }
                })
            }
        </script>
    </div>

    <div>
        <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="900px" runat="server">
            <tr>
                <td></td>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1>Change Coach</h1>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr id="trvol" runat="server">
                <td class="ItemLabel" valign="top" nowrap align="right">Parent Email ID</td>
                <td>
                    <asp:TextBox ID="txtUserId" runat="server" CssClass="SmallFont" Width="300" MaxLength="50"></asp:TextBox>
                    <asp:Button ID="btnLoadChild" runat="server" CssClass="FormButtonCenter" Text="Load Child(ren)"></asp:Button>
                    <br>
                    &nbsp;
						<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
                        ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr id="trchild" runat="server" visible="false">
                <td align="right">Child </td>
                <td align="left">
                    <asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField="ChildNumber" Width="150px" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr id="trYear" runat="server" visible="false">
                <td align="right">Event Year</td>
                <td align="left">
                    <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="24px" Width="150px">
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
            <tr id="trMeetingPwd" runat="server" visible="false">
                <td align="right">Meeting Password</td>
                <td align="left">
                    <asp:TextBox ID="txtMeetingPwd" TextMode="Password" runat="server" Width="145" Wrap="False"></asp:TextBox>
                </td>
                <td></td>
            </tr>
            <tr id="trTimeZone" runat="server" visible="false">
                <td align="right">Time Zone</td>
                <td align="left">
                    <asp:DropDownList ID="ddlTimeZone" runat="server" Width="150px"
                        AutoPostBack="True">

                        <asp:ListItem Value="4" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                        <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                        <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                        <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>


            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPrd" runat="server" ForeColor="Red" Text="" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrdGrp" runat="server" ForeColor="Red" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center>
                        <asp:Label ID="LblTable1Title" runat="server" Style="font-weight: bold;" Text="Table 1: Change Coach is 'From'" Visible="false"></asp:Label>
                    </center>
                    <asp:DataGrid ID="dgselected" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                        Height="14px" GridLines="Horizontal" CellPadding="4" BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666" ForeColor="White" Font-Bold="True">
                        <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                        <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" BackColor="Navy"></HeaderStyle>
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SignUpID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductID" Visible="false"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Grade" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductGroupID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EventYear" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Change Coach" ItemStyle-Width="12%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnChange" runat="server" CausesValidation="false" CommandName="Select" Text="Change Coach"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="12%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachName" runat="server" CssClass="lnkCoachName" Style="cursor: pointer; color: blue;" attr-coachid='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>' Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Capacity" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn ItemStyle-HorizontalAlign="Center" HeaderText="ApprovedCount" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true" DataField="ApprovedCount" Visible="true"></asp:BoundColumn>

                            <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AttendeeID" HeaderStyle-Width="15%" ItemStyle-Width="15%" Visible="false">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAttendeeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "AttendeeID")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RegisteredID" HeaderStyle-Width="15%" ItemStyle-Width="15%" Visible="false">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblRegisteredID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RegisteredID")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SessionNo" HeaderText="Session#" HeaderStyle-ForeColor="white"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Email" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "OnlineClassEmail")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="WebExID" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "UserID")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PWD" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PWD")%>' Visible="True"></asp:Label>
                                    <asp:Label ID="LblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingKey")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                        <AlternatingItemStyle BackColor="LightBlue" />
                    </asp:DataGrid>

                </td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td colspan="2">
                    <asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr bgcolor="#FFFFFF">
                <td colspan="2">
                    <center>
                        <asp:Label ID="LblTable2Title" runat="server" Style="font-weight: bold;" Text="Table 2: Replace with denotes 'To'" Visible="false"></asp:Label>
                    </center>
                    <asp:DataGrid ID="dgCoachSelection" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                        Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666" ForeColor="White" Font-Bold="True">
                        <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                        <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:BoundColumn DataField="SignUpID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductGroupID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EventYear" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Replace with" ItemStyle-Width="12%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnChange" runat="server" CausesValidation="false" CommandName="Select" Text="Replace with"></asp:LinkButton>
                                </ItemTemplate>

                                <ItemStyle Width="12%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Product" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>

                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Coach Name" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachName" CssClass="lnkCoachName" Style="cursor: pointer; color: blue;" attr-coachid='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>' runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="10%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Capacity" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                                </ItemTemplate>

                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:BoundColumn ItemStyle-HorizontalAlign="Center" HeaderText="ApprovedCount" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true" DataField="ApprovedCount" Visible="true">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDayTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Meeting Key" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingKey")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Meeting Password" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingPassword" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingPwd")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Meeting Status" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Status")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTime1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SessionNo" HeaderText="Session#" HeaderStyle-ForeColor="white">
                                <HeaderStyle ForeColor="White"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Start Date" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" Visible="False">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="false"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Donation" HeaderStyle-Width="15%"
                                ItemStyle-Width="15%" Visible="False">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDonation" runat="server"
                                        Text='<%# DataBinder.Eval(Container.DataItem, "Regfee","{0:c}") %>'
                                        Visible="False"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="City" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="State" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <%--						<asp:LinkButton id="lblRemove" runat="server" CausesValidation="false" CommandName="Select" Text="Remove"></asp:LinkButton>
                                    --%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="UserID" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "UserID")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Password" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PWD")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Meeting Password" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMeetingPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MeetingPwd")%>' Visible="True"></asp:Label>
                                </ItemTemplate>

                                <ItemStyle Width="15%"></ItemStyle>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                        <AlternatingItemStyle BackColor="LightBlue" />
                    </asp:DataGrid>
                    <br />
                    <asp:Label ID="lblCoachRegID" runat="server" ForeColor="White"></asp:Label>
                    <asp:Label ID="lblSignUPID" runat="server" ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>

    </div>
    <input type="hidden" value="" id="hdnMeetingStatus" runat="server" />
    <input type="hidden" value="" id="hdnMeetingAttendeeID" runat="server" />
    <input type="hidden" value="" id="hdnAttendeeRegisteredID" runat="server" />
    <input type="hidden" value="" id="hdnAttendeename" runat="server" />
    <input type="hidden" value="" id="hdnAttendeeEmail" runat="server" />
    <input type="hidden" value="" id="hdnCity" runat="server" />
    <input type="hidden" value="" id="hdnState" runat="server" />
    <input type="hidden" value="" id="hdnWebExID" runat="server" />
    <input type="hidden" value="" id="hdnWebExPWD" runat="server" />
    <input type="hidden" value="" id="hdnMeetingURL" runat="server" />

    <input type="hidden" id="hdnTrainingSessionKey" value="" runat="server" />
    <input type="hidden" id="hdnHostMeetingURL" value="" runat="server" />

    <input type="hidden" id="HdnOnlineClassEmail" value="" runat="server" />
    <input type="hidden" id="hdnChildName" value="" runat="server" />



</asp:Content>

