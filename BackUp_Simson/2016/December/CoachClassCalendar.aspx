﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachClassCalendar.aspx.vb" Inherits="CoachClassCalendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 900px;
            top: 40%;
            left: 42.5%;
            margin-left: -350px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }

        .auto-style1 {
            height: 50px;
        }
    </style>
    <script language="javascript" src="js/Calendar.js" type="text/javascript"></script>
    <link href="css/Calendar.css" rel="stylesheet" type="text/css" />

    <script src="Scripts/jquery-1.9.1.js"></script>

    <link href="css/jquery.qtip.min.css" rel="stylesheet" />
    <link href="css/ezmodal.css" rel="stylesheet" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>

    <script src="js/ezmodal.js"></script>

    <script type="text/javascript">

        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }


        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvmakeupClass").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {

            $("#dvmakeupClass").hide();
            $("#overlay").hide();

        }
        $(document).on("click", '#btnClose', function (e) {
            HideDialog();
        });

        function StartMeeting(jsonData, type) {
            if (type == "1") {

                var sessionKey = jsonData.id;
                document.getElementById("<%=hdnTrainingSessionKey.ClientID%>").value = sessionKey;
                var hostlink = jsonData.start_url
                document.getElementById("<%=hdnHostMeetingURL.ClientID%>").value = hostlink;

                var joinLink = jsonData.join_url;
                document.getElementById("<%=hdnMeetingURL.ClientID%>").value = joinLink;
                document.getElementById("<%=hdnMeetingStatus.ClientID%>").value = "SUCCESS";


                document.getElementById("<%=btnCreateZoomSession.ClientID%>").click();

            }


        }

        function statusAlert() {
            alert("All Prior class status should be either 'ON' or 'Cancelled'. You cannot skip. Please edit in chronological order.");
        }

        function statusAlertValidation() {
            alert("You should update the status each week for the next upcoming class. In case you missed it, you can update the prior class(es).");
        }

        function showClassAlert() {

            var status = document.getElementById("<%=hdnStatus.ClientID%>").value
            var weekNo = document.getElementById("<%=hdnWeekNo.ClientID%>").value

            var msg = "Coach paper is missing for the week " + weekNo + " to the selected Product Group and Product. Do you still want to proceed and set the class status " + status + "?";
            if (status == "Makeup") {
                msg = "Coach paper is missing for the week " + weekNo + " to the selected Product Group and Product. Do you still want to schedule a Makeup class?";
            } else if (status == "Substitute") {
                msg = "Coach paper is missing for the week " + weekNo + " to the selected Product Group and Product. Do you still want to schedule a Substitute class?";
            }
            msg = "The Homework files were not yet uploaded to the NSF server for the week " + weekNo + ".  So the release dates cannot be set.  Please come back and set the release dates after the Home Work files are uploaded."
            if (confirm(msg)) {
                document.getElementById("<%=btnCreateClass.ClientID%>").click();
            }
        }

        function showAlert4Makeup() {
            if (confirm("Are you sure you want to cancel the Makeup Class?")) {

                document.getElementById("<%=btnCancelmakeup.ClientID%>").click();
            }
        }
        function showAlert4Substitute() {
            if (confirm("Are you sure you want to cancel the Substitute Class?")) {

                document.getElementById("<%=btnCancelSubstitute.ClientID%>").click();
            }
        }

        function showAlert4Practise() {
            if (confirm("Are you sure you want to cancel the Practice Class?")) {

                document.getElementById("<%=btnCancelPractice.ClientID%>").click();
            }
        }


        $(function (e) {

            var strContentHtml = "";

            $(".lnkCoachName").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {



                        var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;


                        var dvHtml = "";
                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx?coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Calendar signup of that coach (all signups)</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachClassCalendar.aspx?coachID=' + $(this).attr("attr-coachid") + '"" target="_blank">Set up Class Calendar</a></div>';
                        if (roleID == "88") {
                        } else {
                            dvHtml += ' <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div>';
                        }

                        dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '" target="_blank" >Contact List of Students</a> </div>';



                        return dvHtml;



                    },

                    title: 'Online Coaching!',
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow'
                },
                show: {
                    solo: true
                }
            })

        });
    </script>
    <asp:Button ID="btnCreateZoomSession" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnCreateZoomSession_Click" />
    <asp:Button ID="btnCreateClass" runat="server" Text="Create Class" Style="display: none;" OnClick="btnCreateClass_Click" />
    <asp:Button ID="btnCancelmakeup" runat="server" Text="Create Class" Style="display: none;" OnClick="btnCancelmakeup_Click" />
    <asp:Button ID="btnCancelSubstitute" runat="server" Text="Create Class" Style="display: none;" OnClick="btnCancelSubstitute_Click" />
    <asp:Button ID="btnCancelPractice" runat="server" Text="Create Class" Style="display: none;" OnClick="btnCancelPractice_Click" />
    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <h2>Coach Class Calendar</h2>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">

                <table width="100%" style="margin-left: auto; margin-right: auto; font-weight: bold; background-color: #ffffcc;">
                    <tr class="ContentSubTitle">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>CoachName</td>
                        <td>
                            <asp:DropDownList ID="ddlCoach" AutoPostBack="true" runat="server" DataTextField="CoachName" DataValueField="MemberId" Width="105px">
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td>
                            <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" Width="105px">
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="ProductCode" DataValueField="ProductId" Width="110px">
                            </asp:DropDownList>
                        </td>
                        <td>Level</td>
                        <td>
                            <asp:DropDownList ID="ddlLevel" Style="width: 90px;" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged">
                            </asp:DropDownList>

                        </td>
                        <td>Session</td>
                        <td>
                            <asp:DropDownList ID="ddlSessionNo" Style="width: 50px;" runat="server">
                            </asp:DropDownList>
                        </td>

                        <td>Phase</td>
                        <td>
                            <asp:DropDownList ID="ddlPhase" runat="server" Width="50px">
                            </asp:DropDownList>
                        </td>


                    </tr>

                    <tr>
                        <td colspan="16" class="auto-style1">
                            <br />
                            <center>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Style="text-align: center; height: 26px;" /></center>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <center>
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
                </center>
                <asp:Button ID="BtnAddUpdateMakeupSession" runat="server" Visible="false" Text="Add/Update Makeup Class" OnClick="BtnAddUpdateMakeupSession_Click" />
                <div id="dvPracticeSection" runat="server">
                    <div style="float: left;">
                        <asp:Button ID="btnAddUpdatePractiseSession" runat="server" Visible="false" Text="Add/Update Practice Class" OnClick="btnAddUpdatePractiseSession_Click" />
                    </div>
                    <div style="float: left; border: 1px solid green; margin-left: 10px;" id="dvPractiseInfo" visible="false" runat="server">
                        <div style="text-align: center;">
                            <span style="font-weight: bold;">Practice Session</span>
                        </div>
                        <div style="margin-bottom: 2px;">
                        </div>
                        <div style="float: left; margin-left: 5px;">
                            <span style="font-weight: bold;">Day: <span id="spnDay" style="font-weight: normal;" runat="server"></span></span>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <span style="font-weight: bold;">Date: <span id="spnDate" style="font-weight: normal;" runat="server"></span></span>
                        </div>
                        <div style="float: left; margin-left: 10px; margin-right: 5px;">
                            <span style="font-weight: bold;">Time: <span id="spnTime" style="font-weight: normal;" runat="server"></span></span>
                        </div>

                    </div>
                </div>

                <div runat="server" id="divTable1" visible="false">
                    <asp:HiddenField ID="hdMemberId" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdSelectedRowInx" runat="server" />
                    <asp:HiddenField ID="hdTable1Year" runat="server"></asp:HiddenField>

                    <asp:Repeater ID="rptCoachClass" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="rep_hdWeekCnt" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdProductGrp" runat="server" Value='<%# Eval("ProductGroupId")%>'></asp:HiddenField>
                            <asp:HiddenField ID="hdProduct" runat="server" Value='<%# Eval("ProductId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="hdHideModify" runat="server" Value="false" />
                            <asp:HiddenField ID="hdStartDate" runat="server" />
                            <asp:HiddenField ID="hdEndDate" runat="server" />
                            <asp:HiddenField ID="hdTotalClass" runat="server" />
                            <asp:HiddenField ID="hdHWRelDates" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdHWDueDate" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdARelDate" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdSRelDate" runat="server"></asp:HiddenField>
                            <table width="100%">
                                <tr>
                                    <td style="text-align: right; width: 50px"><b>Table
                                        <asp:Label ID="lblTableCnt" runat="server"></asp:Label>
                                        :</b></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><b>Coach Name: </b>
                                        <asp:Label ID="lblCoachname" CssClass="lnkCoachName" Style="color: blue; cursor: pointer;" attr-coachid='<%# Eval("MemberID")%>' runat="server" Text='<%# Eval("Coachname")%>'></asp:Label>
                                    </td>
                                    <td><b>Product Group: </b>
                                        <asp:Label ID="lblPrdGrp" runat="server" Text='<%# Eval("ProductGroupName")%>'></asp:Label>
                                    </td>
                                    <td><b>Product : </b>
                                        <asp:Label ID="lblProduct" runat="server" Text='<%# Eval("ProductName")%>'></asp:Label></td>
                                    <td><b>Phase : </b>
                                        <asp:Label ID="lblPhase" runat="server" Text='<%# Eval("Phase")%>'></asp:Label></td>
                                    <td><b>Level : </b>
                                        <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("Level")%>'></asp:Label></td>
                                    <td><b>Session :</b><asp:Label ID="lblSessionNo" runat="server" Text='<%# Eval("SessionNo")%>'></asp:Label></td>
                                    <td><b>VRoom :</b><asp:Label ID="Label1" runat="server" Text='<%# Eval("VRoom")%>'></asp:Label></td>
                                    <%-- <td><b>Pwd :</b><asp:Label ID="Label2" runat="server" Text='<%# Eval("PWD")%>'></asp:Label></td>--%>
                                    <td><%--<b>Time Zone : </b>
                                        <asp:Label ID="Label3" runat="server" Text='Eastern (EST)'></asp:Label>--%></td>
                                </tr>
                            </table>
                            <asp:GridView ID="rpt_grdCoachClassCal" runat="server" DataKeyNames="SignUpID" AutoGenerateColumns="False" EnableViewState="true" HeaderStyle-BackColor="#ffffcc"
                                OnRowDataBound="rpt_grdCoachClassCal_RowDataBound" OnRowCommand="rpt_grdCoachClassCal_RowCommand" HeaderStyle-Height="25px" AlternatingRowStyle-BackColor="#F6F6F6">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="70px" ItemStyle-Height="25px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" Enabled="false" />
                                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="UpdateCoach" Visible="false" />
                                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="CancelCoach" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>

                                            <%--  <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CurDate", "{0:dd/MM/yyyy}")%>'></asp:Label>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:dd/MM/yyyy}")%>' Visible="false"></asp:Label>--%>

                                            <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CurDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>


                                            <asp:Label ID="lblCoachClassCalID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachClassCalID")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSignUpId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SignUpID")%>' Visible="false"></asp:Label>

                                            <asp:Label ID="lblRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qreleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDueDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qdeadlinedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblARelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "areleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sreleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>

                                            <%-- <asp:Label ID="lblRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qreleasedate", "{0:dd/MM/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDueDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qdeadlinedate", "{0:dd/MM/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblARelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "areleasedate", "{0:dd/MM/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sreleasedate", "{0:dd/MM/yyyy}")%>' Visible="false"></asp:Label>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Day">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDaySub" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day").ToString()%>'></asp:Label>
                                            <asp:Label ID="lblDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time (EST)">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' DataFormatString="{0:hh:mm}"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ser#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerial" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SerNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeek" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "WeekNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdStatus" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Status")%>' />
                                            <asp:HiddenField ID="hdSignupID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "SignupID")%>' />
                                            <asp:DropDownList ID="ddlStatus" runat="server" Enabled="false" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem>Select</asp:ListItem>
                                                <asp:ListItem>Not set</asp:ListItem>
                                                <asp:ListItem>On</asp:ListItem>
                                                <asp:ListItem>Cancelled</asp:ListItem>
                                                <asp:ListItem>Substitute</asp:ListItem>
                                                <asp:ListItem>Makeup</asp:ListItem>
                                                <asp:ListItem>Makeup Cancelled</asp:ListItem>
                                                <asp:ListItem>Substitute Cancelled</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Substitute">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdSubstitute" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Substitute")%>' />
                                            <asp:DropDownList ID="ddlSubstitute" runat="server" Enabled="false" DataTextField="CoachName" DataValueField="MemberId" Width="120px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdReason" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Reason")%>' />
                                            <asp:DropDownList ID="ddlReason" runat="server" Enabled="false">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="Illness">Illness</asp:ListItem>
                                                <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                                                <asp:ListItem Value="Business Trip">Business Trip</asp:ListItem>
                                                <asp:ListItem Value="Work Related">Work Related</asp:ListItem>
                                                <asp:ListItem Value="Holiday">Holiday</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schedule Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheduleType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ScheduleType")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Homework">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHWRelDate" runat="server" EnableViewState="true" Text="__/__/____" dataformatstring="{0:MM/dd/yyyy}"></asp:Label>
                                            <%-- <asp:Label ID="lblHWRelDate" runat="server" EnableViewState="true" Text="__/__/____" dataformatstring="{0:dd/MM/yyyy}"></asp:Label>--%>



                                            <asp:ImageButton ID="imgHWRelDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HW Due Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHWDueDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                            <asp:ImageButton ID="imgHWDueDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:Label ID="lblARelDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                            <asp:ImageButton ID="imgARelDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SRelease">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSRelDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                            <asp:ImageButton ID="imgSRelDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="UserID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "UserID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pwd" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PWD")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week#" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeekNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "WeekNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>
                            </asp:GridView>
                            <br />
                            <asp:LinkButton ID="rep_lnkBtnShowNextWeek" runat="server" OnClick="rep_lnkBtnShowNextWeek_Click">Show Next Week</asp:LinkButton>
                            | <b>Go to : </b>
                            <asp:DropDownList ID="ddlGoTo" runat="server" OnSelectedIndexChanged="ddlGoTo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div id="dvColorStatus" runat="server" visible="false">
                    <div id="dvGreen" style="float: left;">
                        <div style="width: 30px; height: 20px; background-color: #58d68d; float: left;"></div>
                        <div style="float: left; font-weight: bold; margin-left: 10px;">Past</div>
                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div id="dvRed" style="float: left;">
                        <div style="width: 30px; height: 20px; background-color: #FF0000; float: left;"></div>
                        <div style="float: left; font-weight: bold; margin-left: 10px;">Regular Cancelled/Makeup Cancelled/Substitute Cancelled</div>
                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <%-- <div id="dvCurrent" style="float: left;">
                        <div style="width: 30px; height: 20px; background-color: #f4d03f; float: left;"></div>
                        <div style="float: left; font-weight: bold; margin-left: 10px;">Current Class with status On/Makeup/Substitute</div>
                    </div>--%>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div id="dvFuture" style="float: left;">
                        <div style="width: 30px; height: 20px; background-color: #05a9e7; float: left;"></div>
                        <div style="float: left; font-weight: bold; margin-left: 10px;">Upcoming</div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div style="clear: both;"></div>

    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>

    <div id="dvmakeupClass" class="web_dialog">
        <center>
            <div style="margin-top: 20px;">
                <asp:Label ID="lblUpdateOpt" runat="server" ForeColor="Green" Font-Bold="true">Makeup Class</asp:Label>
            </div>
        </center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <div style="margin-top: 5px;">
                <asp:Label ID="lblMakeupErrMsg" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </center>
        <center>
            <table id="TblMakeuPClasss" runat="server" visible="true">
                <tr>
                    <td>
                        <asp:Label ID="LblRecClassDate" Font-Bold="true" runat="server">Recurring Class Date</asp:Label></td>
                    <td>
                        <asp:DropDownList ID="ddlRecClassDate" runat="server">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList></td>

                    <td>
                        <asp:Label ID="LblMakeupClassDate" Font-Bold="true" runat="server">Makeup Class Date</asp:Label></td>
                    <td>
                        <asp:TextBox ID="TxtMkeupDate" runat="server"></asp:TextBox>
                        (MM:DD:YYYY)</td>

                    <td>
                        <asp:Label ID="lblMakeupTime" Font-Bold="true" runat="server">Time</asp:Label></td>
                    <td>
                        <%-- <asp:TextBox ID="TxtTime" runat="server"></asp:TextBox>
                        (HH:MM:SS)--%>

                        <asp:DropDownList ID="ddlMakeupTime" runat="server">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="08:00:00">08:00AM</asp:ListItem>
                            <asp:ListItem Value="09:00:00">09:00AM</asp:ListItem>
                            <asp:ListItem Value="10:00:00">10:00AM</asp:ListItem>
                            <asp:ListItem Value="11:00:00">11:00AM</asp:ListItem>
                            <asp:ListItem Value="12:00:00">12:00PM</asp:ListItem>
                            <asp:ListItem Value="13:00:00">01:00PM</asp:ListItem>
                            <asp:ListItem Value="14:00:00">02:00PM</asp:ListItem>
                            <asp:ListItem Value="15:00:00">03:00PM</asp:ListItem>
                            <asp:ListItem Value="16:00:00">04:00PM</asp:ListItem>
                            <asp:ListItem Value="17:00:00">05:00PM</asp:ListItem>
                            <asp:ListItem Value="18:00:00">06:00PM</asp:ListItem>
                            <asp:ListItem Value="19:00:00">07:00PM</asp:ListItem>
                            <asp:ListItem Value="20:00:00">08:00PM</asp:ListItem>
                            <asp:ListItem Value="21:00:00">09:00PM</asp:ListItem>
                            <asp:ListItem Value="22:00:00">10:00PM</asp:ListItem>
                            <asp:ListItem Value="23:00:00">11:00PM</asp:ListItem>
                            <asp:ListItem Value="24:00:00">12:00PM</asp:ListItem>
                        </asp:DropDownList>

                    </td>

                    <td>
                        <asp:Label ID="lblDuration" Font-Bold="true" runat="server" Visible="false">Duration</asp:Label></td>
                    <td>
                        <asp:TextBox ID="txtDuration" runat="server" Visible="false"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="8" align="center">
                        <br />
                        <asp:Button ID="BtnSaveMakeupClass" runat="server" Text="Save" OnClick="BtnSaveMakeupClass_Click" />
                        <input type="button" value="Close" id="btnClose" />
                    </td>
                </tr>
            </table>
            <div id="dvPractiseSession" runat="server" visible="false">
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center" style="font-weight: bold; color: #64A81C;">
                    <span id="spnPractiseTitle" runat="server" visible="true">Table 1 : Practise Sessions</span>
                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center" style="font-weight: bold; color: #64A81C;">
                    <span id="spnNorecPractise" runat="server" visible="true">No record exists</span>
                </div>
                <div style="clear: both;"></div>
                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdPractiseSession" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdPractiseSession_RowCommand">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                            <ItemTemplate>
                                <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                            <ItemTemplate>
                                <div style="display: none;">


                                    <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StartDate","{0:MM/dd/yyyy}") %>'>'></asp:Label>
                                    <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'>'></asp:Label>

                                    <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'>'></asp:Label>
                                    <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'>'></asp:Label>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'>'></asp:Label>

                                </div>
                                <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="UpdatePractice" />
                                <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="CancelPractise" />

                            </ItemTemplate>

                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--  <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>--%>
                        <asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>
                        <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                        <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>
                        <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                        <asp:BoundField DataField="StartDate" HeaderText="Class Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                        <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                        <asp:TemplateField HeaderText="Begin Time">

                            <ItemTemplate>
                                <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                        <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                        <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                        <asp:BoundField DataField="SessionType" HeaderText="SessionType"></asp:BoundField>

                    </Columns>

                    <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                    <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
                </asp:GridView>
            </div>
        </center>

    </div>
    <input type="hidden" id="hdnTrainingSessionKey" value="" runat="server" />
    <input type="hidden" id="hdnHostMeetingURL" value="" runat="server" />

    <input type="hidden" id="HdnOnlineClassEmail" value="" runat="server" />
    <input type="hidden" value="" id="hdnMeetingStatus" runat="server" />
    <input type="hidden" value="" id="hdnMeetingAttendeeID" runat="server" />
    <input type="hidden" value="" id="hdnAttendeeRegisteredID" runat="server" />
    <input type="hidden" value="" id="hdnAttendeename" runat="server" />
    <input type="hidden" value="" id="hdnAttendeeEmail" runat="server" />
    <input type="hidden" value="" id="hdnCity" runat="server" />
    <input type="hidden" value="" id="hdnState" runat="server" />
    <input type="hidden" value="" id="hdnWebExID" runat="server" />
    <input type="hidden" value="" id="hdnWebExPWD" runat="server" />
    <input type="hidden" value="" id="hdnMeetingURL" runat="server" />

    <input type="hidden" value="" id="hdnDay" runat="server" />
    <input type="hidden" value="" id="hdnDuration" runat="server" />
    <input type="hidden" value="" id="hdnStatus" runat="server" />
    <input type="hidden" value="" id="hdnSerNo" runat="server" />
    <input type="hidden" value="" id="hdnWeekNo" runat="server" />
    <input type="hidden" value="" id="hdnSignupID" runat="server" />
    <input type="hidden" value="" id="hdnCoachID" runat="server" />
    <input type="hidden" value="" id="hdnDate" runat="server" />
    <input type="hidden" value="" id="hdnTime" runat="server" />
    <input type="hidden" value="" id="hdnMeetingTitle" runat="server" />

    <input type="hidden" value="" id="hdnSubCoachID" runat="server" />
    <input type="hidden" value="" id="hdnProductGroupID" runat="server" />
    <input type="hidden" value="" id="hdnProductID" runat="server" />
    <input type="hidden" value="" id="hdnLevel" runat="server" />
    <input type="hidden" value="" id="hdnSessionNo" runat="server" />
    <input type="hidden" value="" id="hdnReason" runat="server" />
    <input type="hidden" value="" id="hdnTotalClass" runat="server" />
    <input type="hidden" value="" id="hdnIsUpdate" runat="server" />
    <input type="hidden" value="" id="hdnorgStatus" runat="server" />
    <input type="hidden" value="" id="hdnCoachClaassID" runat="server" />

    <input type="hidden" value="" id="hdnOrgStatusUpd" runat="server" />
    <input type="hidden" id="hdnRoleID" value="" runat="server" />
</asp:Content>

