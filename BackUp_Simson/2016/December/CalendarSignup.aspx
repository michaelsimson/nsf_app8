﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CalendarSignup.aspx.vb" Inherits="CalendarSignup" Title="CalendarSignup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <link href="css/jquery.qtip.min.css" rel="stylesheet" />
    <link href="css/ezmodal.css" rel="stylesheet" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>

    <script src="js/ezmodal.js"></script>

    <style type="text/css">
        .tblCell {
            border: 1px solid #999999;
            border-collapse: collapse;
        }
    </style>

    <style type="text/css">
        .ac-wrapper {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255,255,255,.6);
            z-index: 1001;
        }

        .popup {
            width: 480px;
            height: 250px;
            background: #FFFFFF;
            border: 2px solid #8CC403;
            border-radius: 15px;
            -moz-border-radius: 15px;
            -webkit-border-radius: 15px;
            box-shadow: #8CC403 0px 0px 3px 3px;
            -moz-box-shadow: #8CC403 0px 0px 3px 3px;
            -webkit-box-shadow: #8CC403 0px 0px 3px 3px;
            position: relative;
            top: 150px;
            left: 450px;
        }
    </style>
    <script language="javascript" type="text/javascript">

        function ConfirmUpdate(sessionno, dy, tm) {

            if (confirm("There is already an existing approved session " + sessionno + " on " + dy + " at " + tm + ". Do you want to change it?")) {
                //alert("There is already an existing approved session " + sessionno + " on " + dy + " at " + tm + ". Do you want to change it?");
                document.getElementById('<%= btnUpdate.ClientID%>').click();
            }
            else {
                document.getElementById('<%= btnCancelGrid.ClientID%>').click();
            }
        }

        function PopupPicker(ctl) {
            var PopupWindow = null;
            settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('CalSignHelp.aspx?ID=' + ctl, 'CalSignUp Help', settings);
            PopupWindow.focus();
        }
        function JoinMeeting1() {


            var url = document.getElementById("<%=hdnWebExMeetURL.ClientID%>").value;

            window.open(url, '_blank');

        }
        function StartMeeting1() {

            JoinMeeting();

        }

        function JoinMeeting() {


            var url = document.getElementById("<%=hdnHostMeetingURL.ClientID%>").value;

            window.open(url, '_blank');

        }

        function showmsg() {
            alert("Coaches can only join their class up to 30 minutes before class time");
        }
        function showAlert() {
            alert("Meeting attendees can only join their class up to 30 minutes before class time");
        }
        function PopUpConfirmBox(hideOrshow) {
            if (hideOrshow == 'hide') document.getElementById('wrapConfirmBox').style.display = "none";
            else document.getElementById('wrapConfirmBox').removeAttribute('style');
        }


        function JoinGuestMeeting() {

            var sessionkey = document.getElementById("<%=hdnSessionKey.ClientID%>").value;
            var url = "https://northsouth.zoom.us/j/" + sessionkey

            window.open(url, '_blank');

        }


        $(function (e) {

            var strContentHtml = "";

            $(".lnkCoachName").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {
                        var accepted = $(this).attr("attr-Accepted");
                        var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;
                        if (accepted == "Y") {

                            var dvHtml = "";
                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx?coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Calendar signup of that coach (all signups)</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachClassCalendar.aspx?coachID=' + $(this).attr("attr-coachid") + '"" target="_blank">Set up Class Calendar</a></div>';
                            if (roleID == "88") {
                            } else {
                                dvHtml += ' <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div>';
                            }

                            dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '" target="_blank" >Contact List of Students</a> </div>';



                            return dvHtml;

                        } else {
                            if (roleID == "88") {
                                return "";
                            } else {
                                return '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div>';
                            }
                        }
                    },

                    title: 'Online Coaching!',
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow'
                },
                show: {
                    solo: true
                }
            })

            $(".lnkParentName").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {
                        return '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + $(this).attr("attr-parentId") + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx" target="_blank">Change Coach</a> </div>';
                    },

                    title: 'Online Coaching!',
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow'
                },
                show: {
                    solo: true
                }
            })


        });

        //$(document).on("mouseover", ".lnkCoachName", function (e) {

        //    var accepted = $(this).attr("attr-Accepted");

        //    if (accepted == "Y") {
        //        strContentHtml = '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx" target="_blank">Calendar signup of that coach (all signups)</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachClassCalendar.aspx?coachID=' + $(this).attr("attr-coachid") + '"" target="_blank">Set up Class Calendar</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '" target="_blank" >Contact List of Students</a> </div>';
        //    } else {
        //        strContentHtml = '  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div>';
        //    }
        //});

        $(document).on("click", ".ancParentInfo", function (e) {
            $("#btnPopUP").trigger("click");
        });

        function getParentInfo(pMemberID) {

            var jsonData = { PMemberID: pMemberID };
            $.ajax({
                type: "POST",
                url: "SetupZoomSessions.aspx/ListParentInfo",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(jsonData),
                async: true,
                cache: false,
                success: function (data) {

                    var tblHtml = "";
                    tblHtml += " <thead>";
                    tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent1 Name</td>";
                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Parent2 Name</td>";

                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>Parent1 Email</td>";
                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent2 Email</td>";

                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child Name</td>";


                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child EMail</td>";
                    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                    tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"


                    tblHtml += "</tr>";
                    tblHtml += " </thead>";

                    $.each(data.d, function (index, value) {
                        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndFirstName + " " + value.IndLastName + "";
                        tblHtml += "</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseFirstName + " " + value.SpouseLastName + "";
                        tblHtml += "</td>";


                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndEMail + "";
                        tblHtml += "</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseEmail + "";
                        tblHtml += "</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildFirstName + " " + value.ChildLastName + "";
                        tblHtml += "</td>";



                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildEmail + "";
                        tblHtml += "</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                        tblHtml += "</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                        tblHtml += "</td>";


                        tblHtml += "</tr>";
                    });

                    $("#tblParentInfo").html(tblHtml);


                }
            })
        }

    </script>
    <div>
        <%-- <asp:ListItem Value="0">Beginner</asp:ListItem>
                                               <asp:ListItem Value="1">Intermediate</asp:ListItem>
                                                 <asp:ListItem Value="2">Advanced</asp:ListItem>--%>
        <div id="wrapConfirmBox" class="ac-wrapper" style="display: none;">
            <div class="popup">
                <div style="margin: 10px">

                    <table width="100%" cellspacing="10px">

                        <tr style="background-color: #ffffcc;">
                            <td colspan="2" style="color: green; font-weight: bold">Confirm Accepted Choice<span style="float: right">
                                <input type="submit" name="submitInd1" value="X" onclick="PopUpConfirmBox('hide')" />
                            </span></td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvConfirmList" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="#cedae6" Width="100%">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRow" runat="server" />
                                                <span style="display: none">
                                                    <asp:Label ID="lblSignUpId" runat="server" Text='<%# Eval("SignupId") %>'></asp:Label></span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ProductCode" HeaderText="Product" />

                                        <asp:BoundField DataField="Level" HeaderText="Level" />

                                        <asp:BoundField DataField="SessionNo" HeaderText="SessionNo" />

                                        <asp:BoundField DataField="Day" HeaderText="Day" />

                                        <asp:BoundField DataField="Time" HeaderText="Time" />

                                    </Columns>
                                </asp:GridView>


                                <%--<table cellpadding="5" width="100%" cellspacing="0" border="1" style="border-color:#f3e8e8">
                        <tr style="background-color:#cedae6"><th>Product</th><th>Level</th><th>SessionNo</th><th>Day</th><th>Time</th></tr>
                        <tr><td><asp:Label runat="server" ID="lblCProduct"></asp:Label> </td>
                            <td><asp:Label runat="server" ID="lblCLevel"></asp:Label> </td>
                            <td><asp:Label runat="server" ID="lblCSession"></asp:Label> </td>
                            <td><asp:Label runat="server" ID="lblCDay"></asp:Label> </td>
                            <td><asp:Label runat="server" ID="lblCTime"></asp:Label> </td></tr>
                    </table>--%>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <br />
                                <center>
                                    <asp:Button ID="btnConfirmToUpdate" runat="server" Text="  Yes  " OnClientClick="PopUpConfirmBox('hide')" />
                                    &nbsp;&nbsp;  
                                    <asp:Button ID="btnNotConfirm" runat="server" Text="  No  " OnClientClick="PopUpConfirmBox('hide')" />
                                    <%--    <input type="submit" name="submitInd2" value="  No  " onclick="PopUpConfirmBox('hide')" />--%>
                                </center>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>





        <span style="visibility: hidden">
            <asp:Button ID="btnUpdate" runat="server" Text="Update" />
            <asp:Button ID="btnCancelGrid" runat="server" Text="Cancel" />
            <asp:HiddenField ID="hdInAppr" runat="server" Value="" />
            <asp:HiddenField ID="hdToAppvId" runat="server" Value="" />
        </span>
        <table border="0" cellpadding="3" cellspacing="0" width="980">
            <tr>
                <td align="left">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>
                    &nbsp;&nbsp;
                </td>
            </tr>
        </table>
        <table cellpadding="3" cellspacing="0" align="center">
            <tr>
                <td></td>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1>Calendar Signup</h1>
                </td>
            </tr>
        </table>
        <%--OnSelectedIndexChanged="ddlDGCapacity_SelectedIndexChanged"--%>



        <table cellspacing="0" cellpadding="2" width="1200px" align="center" style="margin-left: auto; margin-right: auto; font-weight: bold; color: green; background-color: #ffffcc;" id="tblAddUpd" runat="server" visible="true">
            <tr class="ContentSubTitle" align="center">
                <td style="text-align: right; color: green;">Year</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="70px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Event</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" DataTextField="EventCode" DataValueField="EventID" AutoPostBack="true" runat="server" Height="20px" Width="80px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Phase</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged" Width="50px" Height="20px" runat="server" Enabled="false">
                        <asp:ListItem Value="1" Selected="True">One</asp:ListItem>
                        <asp:ListItem Value="2">Two</asp:ListItem>
                        <asp:ListItem Value="3">Three</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                            <a href="javascript:PopupPicker('Phase');">Help</a>
                </td>
                <td style="text-align: right; color: green;">Product Group</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="100px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Product</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlProduct" DataTextField="Name"
                        DataValueField="ProductID" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false"
                        runat="server" Height="20px" Width="100px">
                    </asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Level</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlLevel" runat="server" Width="70px" Height="20px" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Session</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlSession" runat="server" Width="40px" Height="20px">
                        <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                            <a href="javascript:PopupPicker('Session');">Help</a>

                    <br />
                    <div style="margin-bottom: 10px;"></div>
                </td>

            </tr>
            <tr class="ContentSubTitle">
                <td style="text-align: right; color: green;">Day</td>
                <td>
                    <asp:DropDownList ID="ddlWeekDays" runat="server" AutoPostBack="true" Height="20px" Width="80px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Time</td>
                <td>
                    <asp:DropDownList ID="ddlDisplayTime" runat="server" Height="20px" Width="50px"></asp:DropDownList>
                    <asp:Label ID="lblESTTime" runat="server" Text="EST"></asp:Label>
                </td>
                <td style="text-align: right; color: green;">Preference
                </td>
                <td>
                    <asp:DropDownList ID="ddlPref" runat="server" Height="20px" Width="50px">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                        <a href="javascript:PopupPicker('Pref');">Help</a>
                </td>
                <td style="text-align: right; color: green;">Max cap</td>
                <td>
                    <asp:DropDownList ID="ddlMaxCapacity" runat="server" AutoPostBack="true" Height="20px" Width="40px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;" bgcolor="#FF9966">Name</td>
                <td bgcolor="#FF9966">
                    <asp:TextBox ID="txtName" runat="server" Style="margin-left: 0px" Width="90px" Enabled="false" Visible="false"></asp:TextBox>
                    &nbsp;

                </td>
                <td bgcolor="#FF9966">
                    <asp:DropDownList ID="ddlVolName" DataTextField="Name" DataValueField="MemberID" runat="server" AutoPostBack="True" Height="20px" Width="100px" Visible="false"></asp:DropDownList>
                    <asp:HiddenField ID="hdnMemberID" runat="server" />
                </td>
                <td bgcolor="#FF9966">
                    <asp:Button ID="btnClear" runat="server" Text="Clear" Height="25px" Width="50px" />
                    &nbsp;
              <asp:Button ID="btnSearch" runat="server" Text="Search" Height="25px" Width="70px" Visible="false" />
                </td>

                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td align="center" colspan="14">
                    <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="14">
                    <asp:Button ID="btnSubmit" runat="server" Text="Add/Update" />
                    &nbsp;
                   <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                    &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="14">
                    <asp:Label CssClass="btn_02" ID="lblMesg" runat="server" Text="(Please signup for at least 3 alternate day/times, just in case)"></asp:Label>
                </td>
            </tr>
        </table>
        <table width="100%" align="center" style="margin-left: auto; margin-right: auto;">
            <tr>
                <td align="center">
                    <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>

        <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
            <b>Search NSF member</b>
            <div align="center">
                <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%" visible="true" bgcolor="silver">
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="Button1" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" /></td>
                        <td align="left">
                            <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" /></td>
                    </tr>
                </table>
                <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
            </div>
            <br />

            <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                <b>Search Result</b>
                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                    <Columns>
                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                        <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                        <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                        <asp:BoundField DataField="chapter" HeaderText="Chapter"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </asp:Panel>
        <div style="width: 1250px; overflow-x: scroll;">
            <table align="center">
                <tr>
                    <td align="center">
                        <span id="spnTableTitle" runat="server" visible="false" style="font-weight: bold;">Table 1 : Calendar Signup
                    <div style="float: left;">
                        <asp:Button ID="btnExport" runat="server" Text="Export" />
                    </div>
                            <div style="float: left;">
                                <asp:Label ID="lblUniqueCount" runat="server" Font-Bold="true"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 20px;" runat="server">
                                <asp:Label ID="lblAcceptedFilter" runat="server" Font-Bold="true">Accepted: </asp:Label>
                                <asp:DropDownList ID="ddlAcceptedFilter" runat="server" AutoPostBack="true">
                                    <asp:ListItem Value="B">Both</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="N">N</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div style="float: left; margin-left: 20px;">
                                <asp:Button ID="btnConfirm" runat="server" Text="Confirm Accepted Choice" Visible="false" />
                                <%--   <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("HostJoinURL")%>' runat="server" ToolTip='<%# Bind("HostJoinURL")%>'><%# Eval("HostJoinURL").ToString().Substring(0, Math.Min(20, Eval("HostJoinURL").ToString().Length)) + ""%></asp:HyperLink>--%>
                            </div>
                        </span>
                        <asp:DataGrid ID="DGCoach" runat="server" DataKeyField="SignUpID"
                            AutoGenerateColumns="False" Height="14px" CellPadding="2" BackColor="Navy"
                            BorderWidth="3px" BorderStyle="Double"
                            BorderColor="#336666" ForeColor="White" Font-Bold="True" AllowSorting="true" OnSortCommand="DGCoach_SortCommand">
                            <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                            <HeaderStyle Font-Size="X-Small" ForeColor="White" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                            <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <%-- <PagerStyle Font-Italic="True" Wrap="True" Mode="NextPrev" BackColor="White"></PagerStyle>--%>
                            <Columns>
                                <asp:EditCommandColumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
                                    <ItemStyle ForeColor="Blue"></ItemStyle>
                                </asp:EditCommandColumn>
                                <asp:ButtonColumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton" CommandName="Delete" Text="Delete">
                                    <ItemStyle ForeColor="Blue"></ItemStyle>
                                </asp:ButtonColumn>
                                <asp:TemplateColumn HeaderText="Ser#">
                                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSRNO" runat="server"
                                            Text='<%#Container.ItemIndex + 1%>'></asp:Label>

                                        <asp:Label ID="lblBeginTime" runat="server"
                                            Text='<%#DataBinder.Eval(Container, "DataItem.Begin")%>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblEndTime" runat="server"
                                            Text='<%#DataBinder.Eval(Container, "DataItem.End")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="SignUpID" ItemStyle-Width="50px" HeaderText="SignUp ID" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " ReadOnly="true" Visible="true">
                                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                                    <ItemStyle Width="50px"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Volunteer Name">
                                    <HeaderStyle Width="130px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMemberID" runat="server" CssClass="lnkCoachName" attr-coachId='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>' Style="color: blue; cursor: pointer;" attr-Accepted='<%#DataBinder.Eval(Container.DataItem, "Accepted")%>' Text='<%#DataBinder.Eval(Container, "DataItem.Name")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGMember" runat="server" DataTextField="Name" DataValueField="MemberId" OnPreRender="ddlDGMember_PreRender" OnSelectedIndexChanged="ddlDGMember_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </EditItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="ProductGroupCode" Visible="true" SortExpression="ProductGroupID">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                        <%-- <asp:HiddenField ID="hfProductGroupId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>' />--%>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGProductGroup" runat="server" DataTextField="Name" DataValueField="ProductGroupId" Enabled="false" OnPreRender="ddlDGProductGroup_PreRender">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Product" SortExpression="ProductId">
                                    <HeaderStyle Width="80px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                        <%--<asp:HiddenField ID="hfProductId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>' />--%>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGProduct" runat="server" DataTextField="Name" DataValueField="ProductId" OnPreRender="ddlDGProduct_PreRender" OnSelectedIndexChanged="ddlDGProduct_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Level">
                                    <HeaderStyle Width="100px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGLevel" runat="server" OnPreRender="ddlDGLevel_PreRender" AutoPostBack="false" Enabled="false">
                                            <%-- <asp:ListItem Value="0">Beginner</asp:ListItem>
                                               <asp:ListItem Value="1">Intermediate</asp:ListItem>
                                                 <asp:ListItem Value="2">Advanced</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </EditItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Session">
                                    <HeaderStyle Width="100px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SessionNo")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGSessionNo" OnPreRender="ddlDGSessionNo_PreRender" runat="server" AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="ddlDGSessionNo_SelectedIndexChanged">
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Day">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDay" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Day")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGDay" runat="server" OnPreRender="ddlDGDay_PreRender" OnSelectedIndexChanged="ddlDaytime_SelectedIndexChanged" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderText="Time (EST)">
                                    <HeaderStyle Width="400px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGTime" runat="server" OnPreRender="ddlDGTime_PreRender" AutoPostBack="true" Enabled="false" OnSelectedIndexChanged="ddlDGTime_SelectedIndexChanged"></asp:DropDownList>
                                    </EditItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderText="Accepted" SortExpression="Accepted">
                                    <HeaderStyle Width="400px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAccepted" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGAccepted" runat="server" OnPreRender="ddlDGAccepted_PreRender" AutoPostBack="false" Enabled="false">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="Y">Y</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderText="Confirm">
                                    <HeaderStyle Width="400px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblConfirm" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Confirm")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGConfirm" runat="server" OnPreRender="ddlDGConfirm_PreRender" AutoPostBack="false" Enabled="false">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="Y">Y</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Preferences">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPreferences" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Preference")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGPreferences" runat="server" OnPreRender="ddlDGPreferences_PreRender" AutoPostBack="false" Enabled="false">
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="2">3</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Max Cap">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCapacity")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGMaxCapacity" runat="server" OnPreRender="ddlDGMaxCapacity_PreRender" AutoPostBack="false" Enabled="false"></asp:DropDownList>
                                        <%--OnSelectedIndexChanged="ddlDGCapacity_SelectedIndexChanged"--%>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="#Of Students Approved">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblApprovedStudents" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.NStudents")%>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateColumn>


                                <asp:TemplateColumn HeaderText="Meeting URL">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <div style="display: none;">
                                            <asp:LinkButton runat="server" ID="lnkMeetingURL" Visible="false" Text='<%# Eval("HostJoinURL").ToString() + ""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("HostJoinURL").ToString()%>'></asp:LinkButton>

                                            <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MeetingKey")%>'></asp:Label>
                                            <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.HostID")%>'></asp:Label>
                                            <asp:Label ID="lblStProductID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductID")%>'></asp:Label>
                                        </div>
                                        <asp:Button ID="btnJoin" runat="server" Text="Join Meeting" CommandName="SelectMeetingURL" />


                                        <%--   <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("HostJoinURL")%>' runat="server" ToolTip='<%# Bind("HostJoinURL")%>'><%# Eval("HostJoinURL").ToString().Substring(0, Math.Min(20, Eval("HostJoinURL").ToString().Length)) + ""%></asp:HyperLink>--%>
                                    </ItemTemplate>


                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="VRoom">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGVRoom" runat="server" DataTextField="VRoom"
                                            DataValueField="VRoom" OnPreRender="ddlDGVRoom_PreRender" AutoPostBack="true" OnSelectedIndexChanged="ddlDGVRoom_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDGVRoom" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.VRoom") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="UserID" Visible="false">

                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtDGUserID" runat="server"
                                            OnPreRender="txtDGUserID_PreRender" Width="200px"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDGUserID" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.UserID") %>'></asp:Label>
                                    </ItemTemplate>

                                    <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Password" Visible="false">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtDGPWD" runat="server" OnPreRender="txtDGPWD_PreRender"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDGPWD" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.PWD") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Years">

                                    <ItemTemplate>
                                        <asp:Label ID="lblYears" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.Years") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Sessions">

                                    <ItemTemplate>
                                        <asp:Label ID="lblSessions" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.Sessions") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SessionKey">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSessionsKey" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.MeetingKey") %>'></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Meeting Pwd">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSessionsPwd" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.MeetingPwd") %>'></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateColumn>


                                <asp:TemplateColumn HeaderText="Makeup Meeting URL" Visible="false">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>

                                        <asp:LinkButton runat="server" ID="lnkMakeUpMeetingURL" Text='<%# Eval("MakeUpURL").ToString().Substring(0, Math.Min(20, Eval("MakeUpURL").ToString().Length)) + ""%>' CommandName="SelectMakeUpURL" ToolTip='<%# Eval("MakeUpURL").ToString()%>'></asp:LinkButton>
                                        <div style="display: none;">

                                            <asp:Label ID="lblMakeupKey" runat="server"
                                                Text='<%# DataBinder.Eval(Container, "DataItem.MakeUpMeetKey") %>'></asp:Label>
                                        </div>
                                        <%--   <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("HostJoinURL")%>' runat="server" ToolTip='<%# Bind("HostJoinURL")%>'><%# Eval("HostJoinURL").ToString().Substring(0, Math.Min(20, Eval("HostJoinURL").ToString().Length)) + ""%></asp:HyperLink>--%>
                                    </ItemTemplate>


                                </asp:TemplateColumn>


                                <asp:TemplateColumn HeaderText="Event Year">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGEventYear" runat="server" DataTextField="EventYear" DataValueField="EventYear" OnPreRender="ddlDGEventYear_PreRender" AutoPostBack="false" Enabled="false"></asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="EventCode">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEvent" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                        <asp:HiddenField ID="hfEventId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.EventId")%>' />

                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGEvent" runat="server" DataTextField="EventCode" DataValueField="EventID" OnPreRender="ddlDGEvent_PreRender" AutoPostBack="false" Enabled="false">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Phase">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhase" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Phase")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlDGPhase" runat="server" OnPreRender="ddlDGPhase_PreRender" AutoPostBack="true" Enabled="false">
                                            <asp:ListItem Value="1">One</asp:ListItem>
                                            <asp:ListItem Value="2">Two</asp:ListItem>
                                            <asp:ListItem Value="3">Three</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Phase" Visible="false">
                                    <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEndTime1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.End")%>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateColumn>


                            </Columns>

                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                            <AlternatingItemStyle BackColor="LightBlue" />
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </div>
        </ruby-span>
    <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="spnTable1Title" runat="server" visible="true">Table 2 : Makeup Sessions</span>


        </div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="SpnMakeupTitle" runat="server" visible="false" style="color: red;">No record exists</span>


        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 1250px; overflow-x: scroll;">
            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand">
                <Columns>


                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                    <asp:TemplateField HeaderText="Coach">

                        <ItemTemplate>

                            <asp:Label runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                    <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                    <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="Class Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                    <asp:TemplateField HeaderText="Begin Time">

                        <ItemTemplate>
                            <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Time" Visible="false">

                        <ItemTemplate>
                            <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                    <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString() + " "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>
                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "BeginTime")%>'><%# Eval("BeginTime").ToString()%></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>

                                <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SessionKey")%>'></asp:Label>
                                <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.HostID")%>'></asp:Label>
                                <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>

                            </div>
                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="SelectMeetingURL" />
                            <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VRoom" HeaderText="Vroom"></asp:BoundField>
                    <asp:BoundField DataField="Pwd" HeaderText="Pwd" Visible="false"></asp:BoundField>
                </Columns>

                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="Span1" runat="server" visible="true">Table 3 : Substitute Sessions</span>


        </div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="SpnSubstituteTitle" runat="server" visible="false" style="color: red;">No record exists</span>


        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div style="width: 1250px; overflow-x: scroll;">
            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GrdSubstituteSessions" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdSubstituteSessions_RowCommand">
                <Columns>


                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                    <asp:TemplateField HeaderText="Coach">

                        <ItemTemplate>

                            <asp:Label runat="server" ID="lblCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Substitute Coach">

                        <ItemTemplate>

                            <asp:Label runat="server" ID="Label2" Text='<%# Bind("SubCoach")%>' CommandName="SelectLink"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                    <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                    <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="Class Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                    <asp:BoundField DataField="SubstituteDate" HeaderText="Substitute Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:TemplateField HeaderText="Begin Time">

                        <ItemTemplate>
                            <asp:Label ID="lblBeginTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Time" Visible="false">

                        <ItemTemplate>
                            <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                    <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="lblMeetingURL" Text='<%# Eval("MeetingUrl").ToString()%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>

                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'><%# Eval("BeginTime").ToString()%></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>

                                <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SessionKey")%>'></asp:Label>
                                <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.HostID")%>'></asp:Label>
                                <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>

                            </div>
                            <asp:Button ID="btnJoin" runat="server" Text="Join Meeting" CommandName="Join" />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VRoom" HeaderText="VRoom"></asp:BoundField>
                    <asp:BoundField DataField="Pwd" HeaderText="Pwd" Visible="false"></asp:BoundField>
                </Columns>

                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center"><span style="font-weight: bold; color: #64a81c;">Table 4: Guest Attendee</span></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="spnGuestAttTitle" runat="server" visible="false" style="color: red;">No record exists</span>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center" style="width: 1250px; overflow-x: scroll;">
            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdGuestAttendee" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdGuestAttendee_RowCommand">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false" HeaderStyle-Width="70">

                        <ItemTemplate>
                            <div style="display: none;">

                                <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblGuestAttendID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"GuestAttendID") %>'>'></asp:Label>

                                <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="LblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pwd") %>'>'></asp:Label>
                                <asp:Label ID="LblMeetingKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'>'></asp:Label>
                                <asp:Label ID="LblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="LblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>
                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"WebExEmail") %>'>'></asp:Label>
                                <asp:Label ID="LblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>

                            </div>
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CommandName="DeleteAttendee" />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Guest Coach"></asp:BoundField>
                    <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="CoachName" HeaderText="Coach"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>

                    <asp:BoundField DataField="RegisteredID" HeaderText="Registered ID" Visible="false"></asp:BoundField>
                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("MeetingURL").ToString() + ""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingURL").ToString()%>'></asp:LinkButton>

                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>'><%# Eval("BeginTime").ToString()%></asp:Label>

                                <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'>'></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                            </div>
                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VRoom" HeaderText="VRoom"></asp:BoundField>
                </Columns>

            </asp:GridView>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <div align="center" style="font-weight: bold; color: #64A81C;">
                <span id="spnPractiseTitle" runat="server" visible="true">Table 5 : Practise Sessions</span>


            </div>
            <div align="center" style="font-weight: bold; color: #64A81C;">
                <span id="spnPractiseNoRecord" runat="server" visible="false" style="color: red;">No record exists</span>


            </div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="grdPractiseSession" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdPractiseSession_RowCommand">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                        <ItemTemplate>
                            <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                                <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                                <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                                <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                                <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StartDate") %>'>'></asp:Label>
                                <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'>'></asp:Label>

                                <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'>'></asp:Label>
                                <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'>'></asp:Label>

                            </div>
                            <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="Modify" />
                            <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="DeleteMeeting" />
                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>

                    <asp:TemplateField HeaderText="Coach">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                    <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                    <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="Class Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                    <asp:TemplateField HeaderText="Begin Time">

                        <ItemTemplate>
                            <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Time" Visible="false">

                        <ItemTemplate>
                            <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                    <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                    <asp:TemplateField HeaderText="meeting URL">
                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:Label runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString()+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:Label>

                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>

                                <asp:Label ID="lblStSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SessionKey")%>'></asp:Label>
                                <asp:Label ID="lblSthostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                                <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                            </div>
                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="VRoom" HeaderText="VRoom"></asp:BoundField>
                </Columns>
                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>
                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>

        </div>
        <input type="hidden" id="hdnTrainingSessionKey" value="" runat="server" />
        <input type="hidden" id="hdnHostMeetingURL" value="" runat="server" />
        <input type="hidden" id="hdnWebExMeetURL" value="" runat="server" />
        <input type="hidden" id="hdnUserID" value="" runat="server" />
        <input type="hidden" id="hdnPwd" value="" runat="server" />

        <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
        <input type="hidden" id="hdnHostURL" value="0" runat="server" />
        <input type="hidden" id="HdnVRoom" value="0" runat="server" />
        <input type="hidden" id="hdnTime" value="" runat="server" />

        <input type="hidden" id="HdnVroomUID" value="" runat="server" />
        <input type="hidden" id="hdnVroomPwd" value="" runat="server" />
        <input type="hidden" id="hdnSessionNo" value="" runat="server" />
        <input type="hidden" id="hdnBeginTime" value="" runat="server" />
        <input type="hidden" id="hdnDay" value="" runat="server" />
        <input type="hidden" id="hdnItemIndex" value="" runat="server" />
        <input type="hidden" id="hdnHostID" value="" runat="server" />
        <input type="hidden" id="hdnRoleID" value="" runat="server" />
    </div>
</asp:Content>
