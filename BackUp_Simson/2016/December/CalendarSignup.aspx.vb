Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml
Imports NativeExcel
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq

Partial Class CalendarSignup
    Inherits System.Web.UI.Page
    Public apiKey As String = "6sTMyAgRSpCmTSIJIWFP8w"
    Public apiSecret As String = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = "1"
        'Session("LoginID") = "4240"
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        ' btnConfirm.Visible = False
        If Not Page.IsPostBack Then
            Dim year As Integer = 0
            Dim roleID As String = String.Empty
            Try


                roleID = Session("RoleID").ToString()
                hdnRoleID.Value = roleID
            Catch ex As Exception
                roleID = 0
            End Try
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, Convert.ToString(year))
            ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
            If Now.Month <= 6 Then
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year - 1)))
            Else
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
            End If

            If Request.QueryString("Role") = "Coach" Then
                lblUniqueCount.Visible = False
                Dim StrSql As String = " select count(*) from CalSignUp where MemberID=" & Session("LoginID") & " and Accepted='Y' and Confirm is null and EventYear=" & ddlEventYear.SelectedValue
                Dim flag As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSql)
                If flag > 0 Then
                    btnConfirm.Visible = True
                End If
            End If

            ''Options for Volunteer Name Selection
            If Session("RoleID") = 88 Then 'Coach can schedule for his own Sessions.
                txtName.Visible = True
                btnSearch.Visible = False
                btnClear.Visible = False
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID from IndSpouse I inner join Volunteer V On V.MemberID = I.AutoMemberID where V.RoleId in (" & Session("RoleID") & ") and V.MemberID=" & Session("LoginID"))
                If ds.Tables(0).Rows.Count > 0 Then
                    txtName.Text = ds.Tables(0).Rows(0)("Name")
                    hdnMemberID.Value = ds.Tables(0).Rows(0)("MemberID")
                End If
            ElseIf (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 96 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches. 
                If Request.QueryString("Role") = "Coach" Then
                    txtName.Visible = False
                    btnSearch.Visible = False
                    btnClear.Visible = False
                Else
                    txtName.Visible = True
                    btnSearch.Visible = True
                    btnClear.Visible = True
                End If
                ddlVolName.Visible = True

                '********* To display all Coaches ****************'
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join Volunteer V On V.MemberID = I.AutoMemberID Where V.RoleId in (88) order by I.FirstName,I.LastName")

                'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.LastName,I.FirstName")
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlVolName.DataSource = ds
                    ddlVolName.DataBind()
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlVolName.Items.Insert(0, "Select Volunteer")
                        ddlVolName.SelectedIndex = 0
                    End If
                Else
                    lblerr.Text = "No Volunteers present for Calendar Sign up"
                End If

            Else
                txtName.Visible = True
                ddlVolName.Visible = False
                btnSearch.Visible = True
                btnClear.Visible = True
            End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            'If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
            'LoadProductGroup()
            'Else
            If Session("RoleId").ToString() = "89" Then 'Session("RoleId").ToString() = "88" Or
                'Commented By Bindhu on Aug 8_2016 To allow Coach admin to access all
                'If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductGroupId is not Null") > 1 Then
                'more than one 
                Dim strCmd As String = " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null "
                strCmd = strCmd & " UNION ALL select p.ProductGroupID, p.ProductID from product p inner join volunteer v on v.ProductGroupId =p.productgroupid  "
                strCmd = strCmd & " where Memberid = " & Session("LoginID") & " And RoleId = " & Session("RoleId") & " And v.ProductgroupId Is Not Null And v.ProductId Is null and v.ProductGroupId is not null"

                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strCmd)
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                'LoadProductGroup()
                'Else
                '    'only one
                '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                '    Dim prd As String = String.Empty
                '    Dim Prdgrp As String = String.Empty
                '    If ds.Tables(0).Rows.Count > 0 Then
                '        prd = ds.Tables(0).Rows(0)(1).ToString()
                '        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                '        lblPrd.Text = prd
                '        lblPrdGrp.Text = Prdgrp
                '    End If
                '    'LoadProductGroup()
                'End If
            ElseIf Session("RoleId").ToString() = "88" Then
                'Ferdine changing this on Sep 2nd
                'If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & Session("LoginID") & " and EventYear=" & Now.Year & "  and ProductId is not Null") > 1 Then
                'more than one 
                ''Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from CalSignUp where Memberid=" & Session("LoginID") & " and EventYear=" & Now.Year & " and ProductId is not Null ")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID FROM Product  WHERE EventID in (13,19) ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                'LoadProductGroup()
                ' Else
                ''    'only one
                ''    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from CalSignUp where Memberid=" & Session("LoginID") & "  and EventYear=" & Now.Year & " and ProductId is not Null ")
                ''    Dim prd As String = String.Empty
                ''    Dim Prdgrp As String = String.Empty
                ''    If ds.Tables(0).Rows.Count > 0 Then
                ''        prd = ds.Tables(0).Rows(0)(1).ToString()
                ''        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                ''        lblPrd.Text = prd
                ''        lblPrdGrp.Text = Prdgrp
                ''    End If
                ''    'LoadProductGroup()
                ''End If
            End If

            LoadEvent(ddlEvent)
            LoadWeekdays(ddlWeekDays)
            'LoadDisplayTime(ddlDisplayTime, False)
            LoadCapacity(ddlMaxCapacity)


            LoadGrid(ddlAcceptedFilter.SelectedValue)
            LoadMakeUpSessions()
            LoadSubstituteSessions()

            lblerr.Text = ""

            fillGuestAttendeeGrid()
            fillPractiseSession()

        End If

    End Sub
    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
                '*********Added on 26-11-2013 to disable Prepbclub for the current year
                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"))
                ddlObject.Enabled = False
                LoadProductGroup()
                '**************************************************************************'
            Else
                ddlObject.Enabled = False
                LoadProductGroup()
            End If
            'ddlObject.SelectedIndex = 0
        Else
            lblerr.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Public Sub LoadDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "8:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub
    Public Sub LoadWeekDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "6:00 PM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub

    Private Sub LoadWeekdays(ByVal ddlObject As DropDownList)
        'For i As Integer = 0 To 6
        'items.Add(new ListItem("Item 2", "Value 2"));
        ddlObject.Items.Add(New ListItem("Select Day", "-1"))
        ddlObject.Items.Add(New ListItem("Monday", "0"))
        ddlObject.Items.Add(New ListItem("Tuesday", "1"))
        ddlObject.Items.Add(New ListItem("Wednesday", "2"))
        ddlObject.Items.Add(New ListItem("Thursday", "3"))
        ddlObject.Items.Add(New ListItem("Friday", "4"))
        ddlObject.Items.Add(New ListItem("Saturday", "5"))
        ddlObject.Items.Add(New ListItem("Sunday", "6"))

        'ddlObject.Items.Add(New ListItem(WeekdayName(i + 1), i))
        ' ddlWeekDays.Items.Add(WeekdayName(i))
        'Next
    End Sub

    Private Sub LoadCapacity(ByVal ddltemp As DropDownList)
        Dim li As ListItem
        Dim i As Integer
        For i = 0 To 30 Step 1 '10 To 100 Step 5
            li = New ListItem(i)
            ddltemp.Items.Add(li)
        Next
        ddltemp.SelectedIndex = ddltemp.Items.IndexOf(ddltemp.Items.FindByValue(20))
    End Sub

    Private Sub LoadProductGroup()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblerr.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
                LoadLevel(ddlLevel, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue, ddlEvent.SelectedValue, ddlEventYear.SelectedValue)
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ddlProduct.Enabled = False
            Else
                Dim strSql As String
                strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & "  order by P.ProductID " 'and P.Status='O'
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, "Select Product")
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Items(0).Selected = True
                    ddlProduct_SelectedIndexChanged(ddlProduct, New EventArgs)
                    ddlProduct.Enabled = False
                    LoadGrid(ddlAcceptedFilter.SelectedValue)
                    LoadMakeUpSessions()
                    LoadSubstituteSessions()
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String, ByVal ProductID As String, ByVal EventID As String, ByVal EventYear As String)

        Try


            ddlObject.Items.Clear()
            ddlObject.Enabled = True

            Dim cmdText As String = String.Empty
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" & EventYear & " and ProductGroupID=" & ProductGroup & " and ProductID=" & ProductID & " and EventID=" & EventID & ""
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim level As SqlDataReader
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText)
            ddlObject.DataSource = level
            ddlObject.DataValueField = "LevelCode"
            ddlObject.DataTextField = "LevelCode"
            ddlObject.DataBind()
            Dim dt As DataTable = New DataTable()
            dt.Load(level)
            If (ddlObject.Items.Count > 1) Then
                ddlObject.Items.Insert(0, "Select Level")
            Else

            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
        'If ProductGroup.Contains("SAT") Then ' ddlProductGroup.SelectedItem.Text.Contains("SAT") Then
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("Junior", 1))
        '    ddlObject.Items.Insert(2, New ListItem("Senior", 2))
        'ElseIf ProductGroup.Contains("Universal Values") Then
        '    ''ddlObject.Enabled = False
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("Junior", 1))
        '    ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
        '    ddlObject.Items.Insert(3, New ListItem("Senior", 3))
        'ElseIf ProductGroup.Contains("Science") Then
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("One level only", 1))
        'Else
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("Beginner", 1))
        '    ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
        '    ddlObject.Items.Insert(3, New ListItem("Advanced", 3))
        'End If
        ddlWeekDays.SelectedIndex = 0
    End Sub

    Function CheckVolunteer() As Boolean
        lblerr.Text = ""
        If ddlVolName.Visible = False And txtName.Visible = True And txtName.Text = "" Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = False And ddlVolName.Visible = True And ddlVolName.SelectedIndex = 0 Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = True And ddlVolName.Visible = True Then
            If ddlVolName.SelectedIndex = 0 And txtName.Text.Trim.ToString() = "" Then
                lblerr.Text = "Please select Volunteer "
            ElseIf ddlVolName.SelectedIndex > 0 And txtName.Text <> "" Then
                lblerr.Text = "Please select one of the options for Volunteer"
            Else
                lblerr.Text = ""
            End If
        End If

        If lblerr.Text = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            lblError.Text = ""
            If CheckVolunteer() = False Then
                lblerr.Text = lblerr.Text '"Please select Volunteer"
            ElseIf ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblerr.Text = "Please select Event"
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblerr.Text = "No Product Group is present for selected Event."
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblerr.Text = "Please select Product Group"
            ElseIf ddlProduct.Items.Count = 0 Then
                lblerr.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblerr.Text = "Please select Product"
                'ElseIf ddlLevel.Enabled = True And ddlLevel.SelectedItem.Text = "Select" Then
            ElseIf ddlLevel.SelectedItem.Text = "Select Level" Then
                lblerr.Text = "Please select Level"
                'End If
            ElseIf ddlWeekDays.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Day"
            ElseIf ddlDisplayTime.SelectedIndex = 0 Then
                lblerr.Text = "Please Enter Coaching time"
            ElseIf ddlMaxCapacity.SelectedValue < 2 Then
                lblerr.Text = "Please select Maximum Capacity"

            Else
                lblerr.Text = ""
                Dim strSQL As String
                Dim Level As String = ""
                If ddlLevel.Enabled = True Then
                    If ddlLevel.SelectedItem.Text = "Select Level" Then
                        lblerr.Text = "Please select Level"
                        Exit Sub
                    End If
                    Level = ddlLevel.SelectedItem.Text
                End If

                If ddlSession.SelectedValue <> "" Then
                    Dim cmdtext As String = String.Empty

                    Dim pgID As String = ddlProductGroup.SelectedValue
                    Dim pID As String = ddlProduct.SelectedValue
                    Dim year As String = ddlEventYear.SelectedValue
                    Level = ddlLevel.SelectedValue
                    Dim sessionNo As String = ddlSession.SelectedValue

                    Dim iSessionNo As Integer = Convert.ToInt32(sessionNo) - 1

                    If sessionNo <> "1" Then


                        cmdtext = " select count(*) as sessioNoCount from CalSignup where EventYear=" & year & " and ProductGroupID=" & pgID & " and ProductID=" & pID & " and Level='" & Level & "' and SessionNo=" & iSessionNo & " and MemberID=" & ddlVolName.SelectedValue & ""
                        'Session("editRow") = dr
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdtext)
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Convert.ToInt32(ds.Tables(0).Rows(0)("sessioNoCount").ToString()) = 0 Then
                                lblerr.Text = "Higher Session # was selected without a lower session # selected earlier."

                                ddlSession.SelectedValue = 1
                                Exit Sub
                            Else

                            End If


                        End If
                    End If
                End If
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "Select Count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Eventyear=" & ddlEventYear.SelectedValue & "and EventID =" & ddlEvent.SelectedValue & " and Day='" & ddlWeekDays.SelectedItem.Text & "' and Time='" & ddlDisplayTime.SelectedItem.Text & "'") > 0 Then
                    lblerr.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
                ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue & IIf(Level <> "", " and Level = '" & Level & "'", "") & " AND Phase=" & ddlPhase.SelectedValue & " AND SessionNo=" & ddlSession.SelectedValue) < 3 Then '<3 Then

                    'strSQL = "INSERT INTO CalSignUp (MemberID, EventYear,EventID,EventCode, Phase,ProductGroupID, ProductGroupCode, ProductID, ProductCode, [Level],SessionNo, Day,Time,Preference,MaxCapacity, CreatedBy,CreateDate) VALUES ("
                    'strSQL = strSQL & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & "," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & ddlPhase.SelectedValue & "," & ddlProductGroup.SelectedValue & ",'" & getProductGroupcode(ddlProductGroup.SelectedValue) & "'," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "'," & IIf(Level <> "", "'" & Level & "'", "NULL") & "," & ddlSession.SelectedValue & ",'" & ddlWeekDays.SelectedItem.Text & "','" & ddlDisplayTime.SelectedValue & "'," & ddlPref.SelectedValue & "," & ddlMaxCapacity.SelectedValue & "," & Session("loginID") & ",Getdate())"


                    Dim strQuery As String = "INSERT INTO CalSignUp (MemberID, EventYear,EventID,EventCode, Phase,ProductGroupID, ProductGroupCode, ProductID, ProductCode, [Level],SessionNo, Day,Time,Preference,MaxCapacity, CreatedBy,CreateDate) VALUES (@MemberID,@EventYear,@EventID, @EventCode,@Phase,@ProductGroupID,@ProductGroupCode,@ProductID,@ProductCode,@Level,@SessionNo,@Day,@Time,@Preference,@MaxCapacity,@CreatedBy,@CreateDate)"

                    Dim cmd As New SqlCommand(strQuery)
                    cmd.Parameters.AddWithValue("@MemberID", IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue))
                    cmd.Parameters.AddWithValue("@EventYear", ddlEventYear.SelectedValue)
                    cmd.Parameters.AddWithValue("@EventID", ddlEvent.SelectedValue)
                    cmd.Parameters.AddWithValue("@EventCode", ddlEvent.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@Phase", ddlPhase.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductGroupID", ddlProductGroup.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductGroupCode", getProductGroupcode(ddlProductGroup.SelectedValue))
                    cmd.Parameters.AddWithValue("@ProductID", ddlProduct.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductCode", getProductcode(ddlProduct.SelectedValue))
                    cmd.Parameters.AddWithValue("@Level", IIf(Level <> "", "'" & Level & "'", "NULL"))
                    cmd.Parameters.AddWithValue("@SessionNo", ddlSession.SelectedValue)
                    cmd.Parameters.AddWithValue("@Day", ddlWeekDays.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@Time", ddlDisplayTime.SelectedValue)
                    cmd.Parameters.AddWithValue("@Preference", ddlPref.SelectedValue)
                    cmd.Parameters.AddWithValue("@MaxCapacity", ddlMaxCapacity.SelectedValue)
                    cmd.Parameters.AddWithValue("@CreatedBy", Session("LoginId"))
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToShortDateString())

                    Dim objNSF As NSFDBHelper = New NSFDBHelper()


                    If objNSF.InsertUpdateData(cmd) = True Then
                        'clear()
                        LoadGrid(ddlAcceptedFilter.SelectedValue)
                        lblerr.Text = "Inserted Successfully"
                    End If
                Else
                    lblerr.Text = "Coach with Same Product, Phase,Level and Session already found"
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Public Sub LoadGrid(ByVal Accepted As String)
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo) as NStudents, C.[Begin], C.[End], Vl.HostID FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID left join VirtualRoomLookUp VL on(Vl.Vroom=C.Vroom) where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + " "

                If ddlAcceptedFilter.SelectedValue = "Y" Then
                    StrSql = StrSql & " and C.Accepted='" & ddlAcceptedFilter.SelectedValue & "' "
                ElseIf ddlAcceptedFilter.SelectedValue = "N" Then
                    StrSql = StrSql & " and (C.Accepted='" & ddlAcceptedFilter.SelectedValue & "' or C.Accepted is null) "
                End If

                StrSql = StrSql & "group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,C.[Begin], C.[End], Vl.HostID order by I.LastName, I.FirstName Asc"

            Else
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo) as NStudents, C.[Begin], C.[End], Vl.HostID  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID left join VirtualRoomLookUp VL on (Vl.Vroom=C.Vroom) where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue <> "Select Product Group", " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Phase=" & ddlPhase.SelectedValue

                If ddlLevel.SelectedValue <> "Select Level" And ddlLevel.SelectedValue <> "" Then
                    StrSql = StrSql & " And C.Level = '" & ddlLevel.SelectedValue & "'"
                End If
                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")
                End If
                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " "
                End If
                If ddlAcceptedFilter.SelectedValue = "Y" Then
                    StrSql = StrSql & " and C.Accepted='" & ddlAcceptedFilter.SelectedValue & "'"
                ElseIf ddlAcceptedFilter.SelectedValue = "N" Then
                    StrSql = StrSql & " and (C.Accepted='" & ddlAcceptedFilter.SelectedValue & "' or C.Accepted is null)"
                End If

                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and C.MemberID=" & Request.QueryString("coachID") & ""
                End If

                StrSql = StrSql & " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD, C.[Begin], C.[End], VL.hostID order by I.LastName, I.FirstName Asc"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                Session("volDataSet") = ds
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim cmdUniqueCoachCount As String = String.Empty
                    cmdUniqueCoachCount = "select COUNT(distinct(MemberID)) from CalSignUp where EventYear=" & ddlEventYear.SelectedValue & ""
                    Dim uniqueCount As Integer = 0
                    Try
                        uniqueCount = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdUniqueCoachCount)
                    Catch ex As Exception
                        uniqueCount = 0
                    End Try
                    lblUniqueCount.Text = "Unique Coaches:" & uniqueCount
                    lblerr.Text = ""
                    spnTableTitle.Visible = True
                    DGCoach.Visible = True
                    DGCoach.DataSource = ds
                    DGCoach.DataBind()
                    ViewState("dtbl") = ds.Tables(0)
                    DGCoach.Visible = True
                    For i As Integer = 0 To DGCoach.Items.Count
                        Dim HostUrl As LinkButton = Nothing
                        Dim joinButton As Button = Nothing
                        HostUrl = CType(DGCoach.Items(i).FindControl("lnkMeetingURL"), LinkButton)
                        joinButton = CType(DGCoach.Items(i).FindControl("btnJoin"), Button)
                        If (HostUrl.Text <> "") Then
                            joinButton.Visible = True
                        Else
                            joinButton.Visible = False
                        End If
                    Next
                Else
                    DGCoach.Visible = False
                    spnTableTitle.Visible = False
                    If Page.IsPostBack = True Then
                        lblerr.Text = "No schedule available"
                    End If
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Private Function GetTimeFromString(timeString As String, addMinute As Integer) As TimeSpan
        Dim dateWithTime As DateTime = (DateTime.MinValue).AddMinutes(addMinute)
        DateTime.TryParse(timeString, dateWithTime)
        Return dateWithTime.AddMinutes(addMinute).TimeOfDay
    End Function
    Private Function GetTimeFromStringSubtract(timeString As String, addMinute As Integer) As TimeSpan
        Dim dateWithTime As DateTime = (DateTime.MinValue)
        DateTime.TryParse(timeString, dateWithTime)
        Return dateWithTime.AddMinutes(addMinute).TimeOfDay
    End Function

    'Public Sub loadGrid(ByVal blnReload As Boolean)
    '    'lblerr.Text = ""
    '    Dim ds As DataSet
    '    Try
    '        Dim StrSql As String = ""
    '        StrSql = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day, Time,C.Accepted,C.Preference FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedIndex > 0, " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")

    '        If blnReload = True Then
    '            Try
    '                If (ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0) Or (ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0) Then
    '                    StrSql = StrSql & " and C.MemberID= " & IIf(Request.QueryString("Role") = "Coach", IIf(ddlVolName.Visible = True, ddlVolName.SelectedValue, Session("LoginID")), IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue)) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                ElseIf ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
    '                    StrSql = StrSql & " AND C.EventID=" & ddlEvent.SelectedValue & " AND C.Phase=" & ddlPhase.SelectedValue & " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue & IIf(ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "") & IIf(Request.QueryString("Role") = "Coach", " and C.MemberID=" & Session("LoginID"), "") & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                End If

    '                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
    '                Session("volDataSet") = ds

    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    lblerr.Text = ""
    '                    DGCoach.Visible = True
    '                    DGCoach.DataSource = ds
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                    DGCoach.Visible = True
    '                    If Request.QueryString("Role") = "Admin" Then
    '                        DGCoach.Columns(13).Visible = True
    '                    Else
    '                        DGCoach.Columns(13).Visible = False
    '                    End If
    '                Else
    '                    DGCoach.Visible = False
    '                    lblerr.Text = "No schedule available"
    '                End If

    '            Catch se As SqlException
    '                lblerr.Text = StrSql 'se.ToString()
    '                Return
    '            End Try
    '        Else
    '            If Request.QueryString("Role") = "Coach" Or Request.QueryString("Role") = "Admin" Then
    '                StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
    '                Session("volDataSet") = ds
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    lblerr.Text = ""
    '                    DGCoach.Visible = True
    '                    DGCoach.DataSource = ds
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                    DGCoach.Visible = True
    '                    If Request.QueryString("Role") = "Admin" Then
    '                        DGCoach.Columns(13).Visible = True
    '                    Else
    '                        DGCoach.Columns(13).Visible = False
    '                    End If
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                Else
    '                    DGCoach.Visible = False
    '                    lblerr.Text = "No schedule available"
    '                End If
    '            Else
    '                ds = CType(Session("volDataSet"), DataSet)
    '                DGCoach.DataSource = Nothing 'ds
    '                DGCoach.DataBind()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.ToString())
    '    End Try
    'End Sub

    Protected Sub DGCoach_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim SignUpID As Integer
            SignUpID = CInt(e.Item.Cells(3).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM CalSignUp WHERE SignUpID =" + CStr(SignUpID))
                If rowsAffected > 0 Then
                    lblError.Text = "Deleted Successfully."
                End If
            Catch se As SqlException
                lblError.Text = "Error: while deleting the record"
                Return
            End Try

            LoadGrid(ddlAcceptedFilter.SelectedValue)

            DGCoach.EditItemIndex = -1
        ElseIf (cmdName = "SelectMeetingURL") Then

            Dim SessionKey As String = String.Empty
            Dim UserID As String = String.Empty
            Dim Pwd As String = String.Empty
            Dim HostUrl As LinkButton = Nothing
            SessionKey = CType(e.Item.FindControl("lblSessionsKey"), Label).Text
            UserID = CType(e.Item.FindControl("lblDGUserID"), Label).Text

            Dim hostID As String = CType(e.Item.FindControl("lblStHostID"), Label).Text

            Pwd = CType(e.Item.FindControl("lblDGPWD"), Label).Text
            HostUrl = CType(e.Item.FindControl("lnkMeetingURL"), LinkButton)
            ' hdnHostMeetingURL.Value = HostUrl.Text

            Dim coachName As String = CType(e.Item.FindControl("lblMemberID"), Label).Text
            Dim beginTime As String = CType(e.Item.FindControl("lblTime"), Label).Text
            Dim day As String = CType(e.Item.FindControl("lblDay"), Label).Text
            Dim prodcutID As String = CType(e.Item.FindControl("lblStProductID"), Label).Text
            Dim dtFromS As New DateTime()
            Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
            Dim mins As Double = 40.0
            If DateTime.TryParse(beginTime, dtFromS) Then
                Dim TS As TimeSpan = dtFromS - dtEnds

                mins = TS.TotalMinutes
            End If
            Dim today As String = DateTime.Now.DayOfWeek.ToString()

            Dim strLogmsg As String = String.Empty
            strLogmsg += DateTime.Now + "\n"
            strLogmsg += coachName + "\n"
            Dim strValidationStatus As String = String.Empty

            Dim iduration As Integer = 0
            Dim duration As String = String.Empty
            Dim cmdText As String = String.Empty
            cmdText = "select duration from EventFees where EventYear=" & ddlEventYear.SelectedValue & " and EventID=13 and ProductID=" & prodcutID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    iduration = Convert.ToDouble(ds.Tables(0).Rows(0)("Duration").ToString())
                    If iduration = 1 Then
                        duration = "-60"
                    ElseIf iduration = 1.5 Then
                        duration = "-90"
                    ElseIf iduration = 2 Then
                        duration = "-120"
                    ElseIf iduration = 2.5 Then
                        duration = "-150"
                    Else
                        duration = "-180"
                    End If
                End If
            End If

            If mins <= 30 AndAlso day = today Then
                Try


                    iduration = Convert.ToInt32(duration)
                Catch ex As Exception
                    CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                End Try
                If (mins < iduration) Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showmsg()", True)
                Else
                    hdnHostID.Value = hostID
                    Try
                        listLiveMeetings()
                    Catch ex As Exception

                    End Try
                    getmeetingIfo(SessionKey, hostID)
                    strValidationStatus = "Success"
                    strLogmsg += strValidationStatus + "\n"
                    writeToLogFile(strLogmsg)



                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", True)
                End If

            Else


                strValidationStatus = "Fails"
                strLogmsg += strValidationStatus + "\n"
                strLogmsg += DateTime.Now + "\n"
                writeToLogFile(strLogmsg)
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showmsg()", True)
            End If
            ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", True)

            'GenerateURL(SessionKey, Pwd, UserID)

        ElseIf (cmdName = "SelectMakeUpURL") Then

            Dim SessionKey As String = String.Empty
            Dim UserID As String = String.Empty
            Dim Pwd As String = String.Empty
            Dim HostUrl As LinkButton = Nothing
            SessionKey = CType(e.Item.FindControl("lblMakeupKey"), Label).Text
            UserID = CType(e.Item.FindControl("lblDGUserID"), Label).Text

            Pwd = CType(e.Item.FindControl("lblDGPWD"), Label).Text
            HostUrl = CType(e.Item.FindControl("lnkMakeUpMeetingURL"), LinkButton)
            GenerateURL(SessionKey, Pwd, UserID)

        End If
    End Sub

    Protected Sub DGCoach_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.EditCommand
        HdnVRoom.Value = ""
        DGCoach.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = CType(Session("volDataSet"), DataSet)
        Dim page As Integer = DGCoach.CurrentPageIndex
        Dim pageSize As Integer = DGCoach.PageSize
        Dim currentRowIndex As Integer
        lblerr.Text = ""
        lblError.Text = ""
        hdnItemIndex.Value = e.Item.ItemIndex
        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If

        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr



        'Modif CoachClass

        LoadGrid(ddlAcceptedFilter.SelectedValue)

        'Dim Level As String = String.Empty
        'Dim SessionNo As String = String.Empty
        'Dim Duration As Integer
        'Dim Day As String = String.Empty
        'Dim SerNo As String = "7"
        'Dim WeekNo As String = "7"
        ''Dim ddlVRoom As DropDownList = CType(e.Item.FindControl("ddlDGVRoom"), DropDownList)
        'Dim ddlVRoom As DropDownList = DirectCast(e.Item.FindControl("ddlDGVRoom"), DropDownList)

        'Level = CType(e.Item.FindControl("lblLevel"), Label).Text
        'SessionNo = CType(e.Item.FindControl("lblSessionNo"), Label).Text
        'Day = CType(e.Item.FindControl("lblDay"), Label).Text
        'Dim strBeginTime As String = String.Empty
        'Dim strEndTime As String = String.Empty
        'strBeginTime = CType(e.Item.FindControl("lblBeginTime"), Label).Text
        'strEndTime = CType(e.Item.FindControl("lblEndTime"), Label).Text

        'Dim year As String = CType(e.Item.FindControl("lblEventYear"), Label).Text
        'Dim EndSessTime As TimeSpan = GetTimeFromString(strEndTime, 30)

        'Dim strEndSessTime As String = EndSessTime.ToString()
        'Dim TsStartSessTime As TimeSpan = GetTimeFromStringSubtract(strBeginTime, -30)
        'Dim strStartTime As String = TsStartSessTime.ToString()

        'Dim CmdText As String = String.Empty
        'CmdText = "select distinct CS.UserID,CS.PWD,CS.Vroom from CalSignup CS   where  CS.[Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and CS.[End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and CS.day='" + Day + "' and CS.eventyear = '" + year + "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + year + " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and CS.day='" + Day + "' and  eventyear = '" + year + "' and Accepted='Y') "

        'Dim ds As DataSet = New DataSet()
        'ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

        'Dim ds1 As New DataSet()
        'CmdText = "select WC.UserID,WC.PWD from WebConfLog WC   where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and  WC.eventyear = '" + year + "' and WC.MemberID in(select CMemberID from CoachReg where EventYear='" + year + "' and Approved='Y')"
        'ds1 = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

        'Dim WebExUserID As String = String.Empty
        'Dim WebExUserPwd As String = String.Empty

        'Dim Uid As String = String.Empty

        'If ds.Tables(0).Rows.Count > 0 Then
        '    ddlVRoom.Items.Clear()

        '    For Each dr1 As DataRow In ds.Tables(0).Rows
        '        If ds1.Tables(0).Rows.Count > 0 Then
        '            For Each dr2 As DataRow In ds1.Tables(0).Rows
        '                If dr1("UserID").ToString() <> dr2("UserID").ToString() Then
        '                    WebExUserID = dr1("UserID").ToString()
        '                    WebExUserPwd = dr1("Pwd").ToString()

        '                    ddlVRoom.Items.Insert(0, New ListItem(dr1("Vroom").ToString(), dr1("Vroom").ToString()))
        '                End If
        '            Next
        '        Else
        '            WebExUserID = dr1("UserID").ToString()
        '            WebExUserPwd = dr1("Pwd").ToString()
        '            ddlVRoom.Items.Insert(0, New ListItem(dr1("Vroom").ToString(), dr1("Vroom").ToString()))
        '        End If
        '    Next
        'End If
        'If WebExUserID <> "" AndAlso WebExUserPwd <> "" Then

        'End If

    End Sub

    Private Sub DGCoach_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGCoach.ItemCreated
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            Dim Edtbtn As LinkButton = Nothing
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)


            If Session("RoleID") = 88 Then
                Dim flag As Object = DataBinder.Eval(e.Item.DataItem, "Accepted")
                If Not flag Is DBNull.Value Then
                    Edtbtn = CType(e.Item.Cells(0).Controls(0), LinkButton)
                    Edtbtn.Enabled = False
                    btn.Enabled = False
                Else
                    btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
                End If
            Else
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
            End If
        End If
    End Sub

    Protected Sub DGCoach_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.UpdateCommand
        Dim sqlStr As String
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim SignUpID As Integer
        Dim MemberID As Integer
        Dim EventID As Integer
        Dim EventYear As Integer
        Dim Capacity As Integer
        Dim SessionNo As Integer
        Dim ProductID As Integer
        Dim Level As String
        Dim Day As String
        Dim Time As String
        Dim Preference As Integer
        Dim Accepted As String
        Dim Confirm As String
        Dim UserID As String
        Dim PwD As String
        Dim Vroom As Integer
        Try
            SignUpID = CInt(e.Item.Cells(3).Text)
            'MemberID = CInt(CType(e.Item.FindControl("txtMemberID"), TextBox).Text)
            EventYear = CInt(CType(e.Item.FindControl("ddlDGEventYear"), DropDownList).SelectedValue)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                ProductID = dr.Item("productID")
                MemberID = dr.Item("MemberID")
                EventID = dr.Item("EventID")
            End If
            If CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Items.Count > 0 And CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Enabled = True Then
                Level = CStr(CType(e.Item.FindControl("ddlDGLevel"), DropDownList).SelectedItem.Text)
            Else
                Level = ""
            End If

            If (Level = "Select Level") Then
                lblerr.Text = "Please select Level"
                Exit Sub
            End If


            SessionNo = CInt(CType(e.Item.FindControl("ddlDGSessionNo"), DropDownList).SelectedItem.Value)
            Capacity = CInt(CType(e.Item.FindControl("ddlDGMaxCapacity"), DropDownList).SelectedItem.Value)
            Time = CStr(CType(e.Item.FindControl("ddlDGTime"), DropDownList).SelectedItem.Text)
            Day = CStr(CType(e.Item.FindControl("ddlDGDay"), DropDownList).SelectedItem.Text) 'Value)

            Preference = CInt(CType(e.Item.FindControl("ddlDGPreferences"), DropDownList).SelectedItem.Value)
            If CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).Enabled = True Then
                If hdInAppr.Value <> "" Then
                    CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedIndex = 1
                End If
                Accepted = CStr(CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedItem.Value)
                If Accepted = "" Then
                    Accepted = "NULL"
                End If
            Else
                Accepted = "NULL"
            End If

            If CType(e.Item.FindControl("ddlDGConfirm"), DropDownList).Enabled = True Then
                If hdInAppr.Value <> "" Then
                    CType(e.Item.FindControl("ddlDGConfirm"), DropDownList).SelectedIndex = 1
                End If
                Confirm = CStr(CType(e.Item.FindControl("ddlDGConfirm"), DropDownList).SelectedItem.Value)
                If Confirm = "" Then
                    Confirm = "NULL"
                End If
            Else
                Confirm = "NULL"
            End If


            UserID = CStr(CType(e.Item.FindControl("txtDGUserID"), TextBox).Text)
            PwD = CStr(CType(e.Item.FindControl("txtDGPWD"), TextBox).Text)
            Vroom = 0
            If CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).Items.Count > 0 Then
                If CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).SelectedItem.Text.ToLower <> "select" Then
                    Vroom = CInt(CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).SelectedItem.Value)
                End If
            End If

            Dim cmdText As String = String.Empty
            Dim iduration As Integer = 0
            cmdText = "select duration from EventFees where EventYear=" & EventYear & " and EventID=13 and ProductID=" & ProductID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    iduration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
                End If
            End If

            Dim dtCurrent As DateTime = DateTime.Now
            Dim strCurrentDate As String = DateTime.Now.ToShortDateString()

            strCurrentDate = strCurrentDate + " " + Time
            Dim dtStartTime As DateTime
            Dim dtEndTime As DateTime
            Dim strSTartTime As String = String.Empty
            Dim strEndTime As String = String.Empty
            dtStartTime = Convert.ToDateTime(strCurrentDate)
            dtStartTime = dtStartTime.AddMinutes(-30)
            dtEndTime = Convert.ToDateTime(strCurrentDate)
            dtEndTime = dtEndTime.AddHours(iduration)

            strSTartTime = dtStartTime.ToString("HH:mm:ss")
            strEndTime = dtEndTime.ToString("HH:mm:ss")

            'Accepted = CStr(CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedItem.Value)

            ',ProductGroupID = " & ProductGroupID & ",ProductGroupCode = '" & getProductGroupcode(ProductGroupID) & "'
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and Eventyear=" & EventYear & " and EventID=" & EventID & " and Day='" & Day & "' and Time='" & Time & "' and  SignUpID not in (" & SignUpID & ")") > 0 Then
                lblError.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
            ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ")") < 3 Then

                sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "',Accepted=" & IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL") & " ,Confirm=" & IIf(Confirm <> "NULL", "'" & Confirm & "'", "NULL") & " ,Preference=" & Preference & ",MaxCapacity =" & Capacity & ",UserID=" & IIf(UserID = "", "NULL", "'" & UserID & "'") & ",PWD=" & IIf(PwD = "", "NULL", "'" & PwD & "'") & ",Vroom=" & IIf(Vroom <= 0, "NULL", Vroom) & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & ",[Begin]='" & strSTartTime & "', [End]='" & strEndTime & "' WHERE SignUpID=" & SignUpID
                If (Accepted <> "NULL") Then

                    Dim dsAppr As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, "select signupid,day,time from CalSignup where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ") and Accepted='Y'")
                    If dsAppr.Tables(0).Rows.Count > 0 Then
                        'session
                        If hdInAppr.Value = "" Then
                            hdInAppr.Value = dsAppr.Tables(0).Rows(0).Item("SignupId")
                            hdToAppvId.Value = SignUpID
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmUpdate(" & SessionNo & ", '" & dsAppr.Tables(0).Rows(0).Item("Day").ToString() & "','" & dsAppr.Tables(0).Rows(0).Item("Time").ToString() & "');", True)
                            Exit Sub
                        Else
                            sqlStr = "UPDATE CalSignUp SET Accepted=null,ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE SignUpID=" & hdInAppr.Value
                            hdInAppr.Value = ""
                            hdToAppvId.Value = ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)
                        End If

                    End If
                End If




                ' sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "',Accepted=" & IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL") & ",Confirm=" & IIf(Confirm <> "NULL", "'" & Confirm & "'", "NULL") & ",Preference=" & Preference & ",MaxCapacity =" & Capacity & ",UserID=" & IIf(UserID = "", "NULL", "'" & UserID & "'") & ",PWD=" & IIf(PwD = "", "NULL", "'" & PwD & "'") & ",Vroom=" & IIf(Vroom <= 0, "NULL", Vroom) & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & ",[Begin]='" & strSTartTime & "', [End]='" & strEndTime & "' WHERE SignUpID=" & SignUpID

                Dim strQuery As String = "UPDATE CalSignUp SET MemberID = @MemberID,EventYear = @EventYEar,ProductID = @ProductID,ProductCode = @ProuctCode,[Level] =@Level ,SessionNo=@SessionNo,Day=@Day,Time = @Time,Accepted=@Accepted,Confirm=@Confirm,Preference=@Preference,MaxCapacity =@MaxCapacity,UserID=@UserID,PWD=@Pwd,Vroom=@Vroom,ModifiedDate = @ModifyDate,ModifiedBy = @LoginID,[Begin]=@BeginTime, [End]=@EndTime WHERE SignUpID=@SignupID"

                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@MemberID", MemberID)
                cmd.Parameters.AddWithValue("@EventYEar", EventYear)
                cmd.Parameters.AddWithValue("@ProductID", ProductID)
                cmd.Parameters.AddWithValue("@ProuctCode", getProductcode(ProductID))
                cmd.Parameters.AddWithValue("@Level", IIf(Level <> "", "'" & Level & "'", "NULL"))
                cmd.Parameters.AddWithValue("@SessionNo", SessionNo)
                cmd.Parameters.AddWithValue("@Day", Day)
                cmd.Parameters.AddWithValue("@Time", Time)
                cmd.Parameters.AddWithValue("@Accepted", IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL"))
                cmd.Parameters.AddWithValue("@Confirm", IIf(Confirm <> "NULL", "'" & Confirm & "'", "NULL"))
                cmd.Parameters.AddWithValue("@Preference", Preference)
                cmd.Parameters.AddWithValue("@MaxCapacity", Capacity)
                cmd.Parameters.AddWithValue("@UserID", IIf(UserID = "", "NULL", "'" & UserID & "'"))
                cmd.Parameters.AddWithValue("@Pwd", IIf(PwD = "", "NULL", "'" & PwD & "'"))
                cmd.Parameters.AddWithValue("@Vroom", IIf(Vroom <= 0, "NULL", Vroom))
                cmd.Parameters.AddWithValue("@ModifyDate", DateTime.Now.ToShortDateString())
                cmd.Parameters.AddWithValue("@LoginID", Session("LoginID"))
                cmd.Parameters.AddWithValue("@BeginTime", strSTartTime)
                cmd.Parameters.AddWithValue("@EndTime", strEndTime)
                cmd.Parameters.AddWithValue("@SignupID", SignUpID)
                Dim objNSF As NSFDBHelper = New NSFDBHelper()
                ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)

                If objNSF.InsertUpdateData(cmd) = True Then
                    lblError.Text = "Updated Successfully"
                End If
                DGCoach.EditItemIndex = -1
                LoadGrid(ddlAcceptedFilter.SelectedValue)

            Else
                lblError.Text = "Coach with same product, Phase, Level and Session are already found"
            End If
        Catch ex As SqlException
            'ex.message
            'Response.Write(ex.ToString())
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            Return
        End Try

    End Sub

    Protected Sub DGCoach_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.CancelCommand
        Dim ds As DataSet = CType(Session("volDataSet"), DataSet)
        lblerr.Text = ""
        lblError.Text = ""
        If (Not ds Is Nothing) Then
            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = DGCoach.CurrentPageIndex
            Dim pageSize As Integer = DGCoach.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            DGCoach.EditItemIndex = -1

            LoadGrid(ddlAcceptedFilter.SelectedValue)
        Else
            Return
        End If
    End Sub
    Protected Sub DGCoach_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGCoach.PageIndexChanged




        Dim ds As DataSet = New DataSet()
        ds = Session("volDataSet")
        DGCoach.DataSource = ds
        DGCoach.DataBind()
        DGCoach.CurrentPageIndex = e.NewPageIndex
        Page.EnableViewState = True

    End Sub
    Public Sub ddlDaytime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("Day") = ddlTemp.SelectedItem.Text

            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGEventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            ddlTemp.Items.Insert(0, Convert.ToString(Now.Year() + 1))
            ddlTemp.Items.Insert(1, Convert.ToString(Now.Year()))
            ddlTemp.Items.Insert(2, Convert.ToString(Now.Year() - 1))

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("EventYear")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If

    End Sub

    Public Sub ddlDGMember_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String
        If Request.QueryString("Role") = "Coach" Then
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.FirstName"
        Else
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName"
        End If

        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
        If Session("RoleID") = 88 Then 'Request.QueryString("Role") = "Coach"Then
            ddlTemp.Enabled = False
        End If
    End Sub

    Public Sub ddlDGMember_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGEvent_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim EventCode As String = ""
        Dim EventId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventCode") Is DBNull.Value) Then
                EventCode = dr.Item("EventCode")
                EventId = dr.Item("EventId")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadEvent(ddlTemp)
            Dim rowEventID As Integer = 0
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(EventId))
        Else
            Return
        End If

    End Sub

    Public Sub ddlDGPhase_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Phase As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Phase") Is DBNull.Value) Then
                Phase = dr.Item("Phase")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Phase))
        End If
    End Sub

    Public Sub ddlDGProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProductGroupID As Integer = 0
        Dim rowEventID As Integer = 0

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Try
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If

            'Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup where EventId=" & rowEventID & "  order by ProductGroupID"
            Dim strSql As String = String.Empty
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & rowEventID & "  order by P.ProductGroupID"
            Dim drproductGroupid As SqlDataReader
            drproductGroupid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductGroupid
            ddlTemp.DataBind()
            If (Not dr.Item("productGroupID") Is DBNull.Value) Then
                rowProductGroupID = dr.Item("productGroupID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductGroupID))
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub ddlDGProduct_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProdGroupId As Integer = 0
        Dim rowEventID As Integer = 0

        Try
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
                rowProdGroupId = dr.Item("productGroupId")
            End If
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            'Dim strSql As String = "Select ProductID, Name from Product where EventID in (" & rowEventID & ") and ProductGroupID =" & rowProdGroupId & " order by ProductID"
            Dim strSql As String = String.Empty
            strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & rowEventID & " and P.ProductGroupID =" & rowProdGroupId & " order by P.ProductID"

            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductid
            ddlTemp.DataBind()
            Dim rowProductID As Integer = 0
            If (Not dr.Item("productID") Is DBNull.Value) Then
                rowProductID = dr.Item("productID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub ddlDGProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("productID") = ddlTemp.SelectedValue
            dr.Item("ProductCode") = getProductcode(ddlTemp.SelectedValue)
            'Session("editRow") = dr
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGLevel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim level As String = ""
        Dim ProductGroupCode As String = ""
        Dim ProductGroupID As String
        Dim ProductID As String
        Dim EventID As String
        Dim EventYear As String


        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Level") Is DBNull.Value) Then
                level = dr.Item("Level")
            End If
            If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
                ProductGroupCode = dr.Item("ProductGroupCode")
                ProductGroupID = dr.Item("ProductGroupID")
            End If
            If (Not dr.Item("ProductID") Is DBNull.Value) Then
                ProductID = dr.Item("ProductID")
            End If
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                EventID = dr.Item("EventID")
            End If
            If (Not dr.Item("EventYear") Is DBNull.Value) Then
                EventYear = dr.Item("EventYear")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadLevel(ddlTemp, ProductGroupID, ProductID, EventID, EventYear)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(level))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            ElseIf ProductGroupCode <> "UV" Then
                ddlTemp.Enabled = True
            End If

        Else
            Return
        End If
    End Sub

    Public Sub ddlDGSessionNo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SessionNo As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("SessionNo") Is DBNull.Value) Then
                SessionNo = dr.Item("SessionNo")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(SessionNo))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
            If hdnSessionNo.Value <> "" Then
                ddlTemp.SelectedValue = hdnSessionNo.Value
            End If
        Else
            Return
        End If
    End Sub
    Public Sub ddlDGDay_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Day As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If
            LoadWeekdays(ddlTemp)
            'For i As Integer = 1 To 7
            '    ddlTemp.Items.Add(WeekdayName(i))
            'Next
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Day))
        If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If
        If hdnDay.Value <> "" Then
            ddlTemp.SelectedValue = hdnDay.Value
        End If

    End Sub

    Public Sub ddlDGTime_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Day As String = ""

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            'LoadDisplayTime(ddlTemp, False)
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If

            If Day = "Saturday" Or Day = "Sunday" Then
                LoadDisplayTime(ddlTemp, False)
            Else
                LoadWeekDisplayTime(ddlTemp, False)
            End If

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Time")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If


            If hdnBeginTime.Value <> "" Then
                ddlTemp.SelectedValue = hdnBeginTime.Value
            End If
        End If
    End Sub

    Public Sub ddlDGAccepted_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Accepted As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Accepted") Is DBNull.Value) Then
                Accepted = dr.Item("Accepted")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Accepted))
            If Session("RoleID") = 88 Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub


    Public Sub ddlDGConfirm_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Confirm As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Confirm") Is DBNull.Value) Then
                Confirm = dr.Item("Confirm")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Confirm))
            If Session("RoleID") = 88 Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGPreferences_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Preference As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Preference") Is DBNull.Value) Then
                Preference = dr.Item("Preference")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Preference))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGMaxCapacity_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            Dim li As ListItem
            Dim i As Integer
            For i = 0 To 35 Step 1 '10 To 100 Step 5
                li = New ListItem(i)
                ddlTemp.Items.Add(li)
            Next
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("MaxCapacity")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If
    End Sub
    Private Sub clear()
        Try


            lblerr.Text = ""
            lblError.Text = ""
            ddlEvent.SelectedValue = ""
            ddlProductGroup.Items.Clear()
            ddlProductGroup.Enabled = False
            ddlProduct.Items.Clear()
            ddlProduct.Enabled = False
            'ddlProductGroup.SelectedIndex = 0
            ddlPhase.SelectedIndex = 0
            ddlSession.SelectedIndex = 0
            ddlWeekDays.SelectedIndex = 0
            ddlDisplayTime.SelectedIndex = 0

            If ddlLevel.Enabled = True Then
                ddlLevel.SelectedIndex = ddlLevel.Items.IndexOf(ddlLevel.Items.FindByValue(0))
            End If
            ddlMaxCapacity.SelectedIndex = ddlMaxCapacity.Items.IndexOf(ddlMaxCapacity.Items.FindByText("20"))
            ddlPref.SelectedIndex = 0
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        DGCoach.Visible = False
        LoadEvent(ddlEvent)

        'If ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex <> 0 Then
        'LoadProductGroup()
        'Else
        '   LoadProductGroup()
        'End If
        'If ddlVolName.SelectedIndex <> 0 Then
        '    LoadGrid()
        'End If

        LoadGrid(ddlAcceptedFilter.SelectedValue)
        LoadMakeUpSessions()
        LoadSubstituteSessions()
    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
            lblerr.Text = "Please select Valid Product"
        Else
            LoadGrid(ddlAcceptedFilter.SelectedValue)
            LoadMakeUpSessions()
            LoadSubstituteSessions()
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        '  If ddlProductGroup.SelectedIndex <> 0 Then
        LoadProductID()
        'LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        '' Commented by Ferdine
        LoadGrid(ddlAcceptedFilter.SelectedValue)
        ' End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Then
            lblerr.Text = "Please select Valid Product"
        Else
            '' Commented by Ferdine
            LoadLevel(ddlLevel, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue, ddlEvent.SelectedValue, ddlEventYear.SelectedValue)
            LoadGrid(ddlAcceptedFilter.SelectedValue)
        End If
        LoadLevel(ddlLevel, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue, ddlEvent.SelectedValue, ddlEventYear.SelectedValue)
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        LoadGrid(ddlAcceptedFilter.SelectedValue)
        LoadMakeUpSessions()
        LoadSubstituteSessions()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  I.firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  I.lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "[usp_SelectCalSignupVolunteers]", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub
    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtName.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            hdnMemberID.Value = GridMemberDt.DataKeys(index).Value
            pIndSearch.Visible = False
            lblIndSearch.Visible = False
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignup Where MemberID=" & hdnMemberID.Value & "") > 0 Then
                lblerr.Text = "The member already exists for Calendar Signup"
            End If
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIndClose.Click
        pIndSearch.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clear()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtName.Text = ""
        If ddlVolName.Items.Count > 0 Then
            ddlVolName.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlDGVRoom_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim cmdText As String = "select Vroom from VirtualRoomLookup"
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlTemp.DataValueField = "Vroom"
                ddlTemp.DataTextField = "Vroom"
                ddlTemp.DataSource = ds
                ddlTemp.DataBind()
            End If
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            Dim Duration As Integer
            Dim Day As String = String.Empty
            Day = dr.Item("Day").ToString()
            Dim strBeginTime As String = String.Empty
            Dim strEndTime As String = String.Empty
            strBeginTime = dr.Item("Time").ToString()
            strEndTime = dr.Item("End").ToString()

            cmdText = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                Duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
                If Duration = 1 Then
                    Duration = 60
                ElseIf Duration = 1.5 Then
                    Duration = 90
                ElseIf Duration = 2 Then
                    Duration = 120
                Else
                    Duration = 180
                End If
            End If
            checkAvailableVrooms(Duration, strBeginTime, Day, ddlTemp, dr.Item("SignupID").ToString())
            'Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            'Dim Vroom As String
            'Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            'ddlTemp = sender
            'Dim Level As String = String.Empty
            'Dim SessionNo As String = String.Empty
            'Dim Duration As Integer
            'Dim Day As String = String.Empty
            'Dim SerNo As String = "7"
            'Dim WeekNo As String = "7"
            ''Dim ddlVRoom As DropDownList = CType(e.Item.FindControl("ddlDGVRoom"), DropDownList)
            'Dim ddlVRoom As String = dr.Item("VRoom").ToString()
            'Dim Accepted As String = String.Empty
            'Dim UserID As String = String.Empty
            'Dim Pwa As String = String.Empty
            'Accepted = dr.Item("Accepted").ToString()
            'UserID = dr.Item("UserID").ToString()
            'Pwa = dr.Item("Pwd").ToString()
            'ddlTemp.Items.Clear()
            'If (Accepted = "Y" And UserID = "" And Pwa = "") Then
            '    Level = dr.Item("level").ToString()
            '    SessionNo = dr.Item("SessionNo").ToString()
            '    Day = dr.Item("Day").ToString()
            '    Dim strBeginTime As String = String.Empty
            '    Dim strEndTime As String = String.Empty
            '    strBeginTime = dr.Item("Time").ToString()
            '    strEndTime = dr.Item("End").ToString()

            '    Dim year As String = dr.Item("EventYear").ToString()
            '    Dim EndSessTime As TimeSpan = GetTimeFromString(strEndTime, 30)

            '    Dim strEndSessTime As String = EndSessTime.ToString()
            '    Dim TsStartSessTime As TimeSpan = GetTimeFromStringSubtract(strBeginTime, -30)
            '    Dim strStartTime As String = TsStartSessTime.ToString()

            '    Dim tsSessEndLessTime As TimeSpan = GetTimeFromStringSubtract(strEndTime, -30)
            '    Dim strSessEndLessTime As String = tsSessEndLessTime.ToString()

            '    Dim CmdText As String = String.Empty
            '    'CmdText = "select distinct CS.UserID,CS.PWD,CS.Vroom from CalSignup CS   where  CS.[Begin] not between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "' and CS.[End] not between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "' and CS.day='" & Day & "' and CS.eventyear = '" & year & "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" & year & " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "' or [End] between '" & strSessEndLessTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "'  and CS.day='" & Day & "' and  eventyear = '" & year & "' and Accepted='Y') order by cs.vroom desc"

            '    CmdText = "  select distinct UserID,Pwd,VRoom from CalSignUp where EventYear=" & year & " and Accepted='Y' and [Begin] not between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "' and [End] not between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "' and UserID not in (select distinct UserID from CalSignUp where EventYear=" & year & " and Accepted='Y' and ([Begin] between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "' or [End] between '" & strStartTime.Substring(0, 5) & "' and '" & strEndSessTime.Substring(0, 5) & "') and UserID is not null and day='saturday') and DAY='saturday' order by Vroom DESC"

            '    Dim ds As DataSet = New DataSet()
            '    ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

            '    Dim ds1 As New DataSet()
            '    CmdText = "select WC.UserID,WC.PWD from WebConfLog WC   where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' or wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and  WC.eventyear = '" + year + "' and WC.MemberID in(select CMemberID from CoachReg where EventYear='" + year + "' and Approved='Y') order by PWD DESC"
            '    ds1 = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

            '    Dim WebExUserID As String = String.Empty
            '    Dim WebExUserPwd As String = String.Empty
            '    Dim Uid As String = String.Empty
            '    If ds.Tables(0).Rows.Count > 0 Then
            '        'ddlVRoom.Items.Clear()

            '        For Each dr1 As DataRow In ds.Tables(0).Rows
            '            If ds1.Tables(0).Rows.Count > 0 Then
            '                For Each dr2 As DataRow In ds1.Tables(0).Rows
            '                    If dr1("UserID").ToString() <> dr2("UserID").ToString() Then
            '                        WebExUserID = dr1("UserID").ToString()
            '                        WebExUserPwd = dr1("Pwd").ToString()

            '                        ddlTemp.Items.Insert(0, New ListItem(dr1("Vroom").ToString(), dr1("Vroom").ToString()))
            '                    End If
            '                Next
            '            Else
            '                WebExUserID = dr1("UserID").ToString()
            '                WebExUserPwd = dr1("Pwd").ToString()
            '                ddlTemp.Items.Insert(0, New ListItem(dr1("Vroom").ToString(), dr1("Vroom").ToString()))
            '            End If
            '        Next
            '    End If
            '    ddlTemp.Items.Insert(0, New ListItem("select", "select"))
            '    If (HdnVRoom.Value <> "") Then
            '        ddlTemp.SelectedValue = HdnVRoom.Value
            '    Else
            '        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(IIf(Vroom = "", "Select", dr.Item("VRoom"))))
            '    End If
            'Else
            '    If (Not dr Is Nothing) Then
            '        ddlTemp.Items.Clear()
            '        Dim li As ListItem
            '        Dim i As Integer
            '        For i = 1 To 100
            '            li = New ListItem(i)
            '            ddlTemp.Items.Add(li)
            '        Next
            '        ddlTemp.Items.Insert(0, New ListItem("Select", 0))
            '        Vroom = dr.Item("VRoom").ToString()
            '        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(IIf(Vroom = "", "Select", Vroom)))
            '        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value Then
            '            ddlTemp.Enabled = False
            '        Else
            '            ddlTemp.Enabled = True
            '        End If
            '    End If
            'End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Protected Sub ddlDGVRoom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            'Dim Vroom As String

            'Dim ArrayList As ArrayList = New ArrayList()
            'ArrayList.Add("WebExID")
            'ArrayList.Add("NsfOne")
            'ArrayList.Add("NsfTwo")
            'ArrayList.Add("NsfThree")
            'ArrayList.Add("NsfFour")
            'ArrayList.Add("NsfFive")
            'ArrayList.Add("NsfSix")
            'ArrayList.Add("NsfSeven")
            'ArrayList.Add("NsfEight")
            'ArrayList.Add("NsfNine")
            'ArrayList.Add("NsfTen")
            'ArrayList.Add("NsfEleven")
            'ArrayList.Add("NsfTwelve")
            'ArrayList.Add("NsfThirteen")
            'ArrayList.Add("NsfFourteen")
            'ArrayList.Add("NsfFifteen")
            'ArrayList.Add("NsfSixteen")
            'ArrayList.Add("NsfSeventeen")
            'ArrayList.Add("NsfEighteen")
            'ArrayList.Add("NsfNineteen")
            'ArrayList.Add("NsfTwenty")
            'ArrayList.Add("NsfTwentyOne")
            'ArrayList.Add("NsfTwentyTwo")
            'ArrayList.Add("NsfTwentyThree")
            'ArrayList.Add("NsfTwentyFour")
            'ArrayList.Add("NsfTwentyFive")
            'ArrayList.Add("NsfTwentySix")

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            HdnVRoom.Value = ddlTemp.SelectedValue

            Dim cmd As String = "SELECT VROOM,UserId,Pwd FROM VirtualRoomLookUp where VRoom=" & ddlTemp.SelectedValue
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmd)

            If ds.Tables(0).Rows.Count > 0 Then
                HdnVroomUID.Value = ds.Tables(0).Rows(0)("UserId").ToString
                hdnVroomPwd.Value = ds.Tables(0).Rows(0)("Pwd").ToString
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub txtDGUserID_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim txtTemp As System.Web.UI.WebControls.TextBox
        txtTemp = sender
        Dim Accepted As String = String.Empty
        Dim UserID As String = String.Empty
        Dim Pwa As String = String.Empty
        Accepted = dr.Item("Accepted").ToString()
        UserID = dr.Item("UserID").ToString()
        Pwa = dr.Item("Pwd").ToString()

        If (Accepted = "Y" And UserID = "" And Pwa = "") Then
            If (HdnVroomUID.Value <> "") Then
                txtTemp.Text = HdnVroomUID.Value
            Else
                txtTemp.Text = dr.Item("UserID").ToString()
            End If
        Else
            txtTemp.Text = dr.Item("UserID").ToString()
        End If

        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value
            txtTemp.Enabled = False
        Else
            txtTemp.Enabled = True
        End If
    End Sub

    Protected Sub txtDGPWD_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim txtTemp As System.Web.UI.WebControls.TextBox
        txtTemp = sender
        Dim Accepted As String = String.Empty
        Dim UserID As String = String.Empty
        Dim Pwa As String = String.Empty
        Accepted = dr.Item("Accepted").ToString()
        UserID = dr.Item("UserID").ToString()
        Pwa = dr.Item("Pwd").ToString()
        If (Accepted = "Y" And UserID = "" And Pwa = "") Then
            If (hdnVroomPwd.Value <> "") Then
                txtTemp.Text = hdnVroomPwd.Value
            Else
                txtTemp.Text = dr.Item("PWD").ToString()
            End If
        Else
            txtTemp.Text = dr.Item("PWD").ToString()
        End If
        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value
            txtTemp.Enabled = False
        Else
            txtTemp.Enabled = True
        End If
    End Sub

    Protected Sub ddlWeekDays_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWeekDays.SelectedIndexChanged
        If ddlWeekDays.SelectedValue = "5" Or ddlWeekDays.SelectedValue = "6" Then
            LoadDisplayTime(ddlDisplayTime, False)
        Else
            LoadWeekDisplayTime(ddlDisplayTime, False)
        End If
    End Sub
    Public Sub GetHostUrlMeeting(WebExID As String, Pwd As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"
        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf
        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GethosturlMeeting"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & hdnTrainingSessionKey.Value & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingHostURLResponse(xmlReply)
        'lblMsg3.Text = result;

    End Sub

    Private Function MeetingHostURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnHostMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try
        Return sb.ToString()
    End Function
    Public Sub GenerateURL(ByVal SessionKey As String, ByVal Pwd As String, ByVal UserID As String)
        hdnTrainingSessionKey.Value = SessionKey
        GetHostUrlMeeting(UserID, Pwd)
        Dim MeetinURL As String = hdnHostMeetingURL.Value
        If MeetinURL <> "" Then
            Dim URL = MeetinURL.Replace("&amp;", "&")
            hdnWebExMeetURL.Value = URL
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", True)
        End If

    End Sub

    Public Sub LoadMakeUpSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.VRoom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)  inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID)  where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.MemberID=" + Session("LoginID").ToString() + ""
            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)"
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & " AND VC.Phase=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql &= " where CS.EventYear=" & ddlEventYear.SelectedValue & " and VC.SessionType='Scheduled Meeting'"
                StrSql &= "order by IP.FirstName, IP.LastNAme ASC"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)

                If ds.Tables(0).Rows.Count > 0 Then
                    GrdMeeting.DataSource = ds
                    GrdMeeting.DataBind()
                    SpnMakeupTitle.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0
                    Dim sessionKey As String = String.Empty

                    For i = 0 To GrdMeeting.Rows.Count - 1
                        dateVal = GrdMeeting.Rows(i).Cells(13).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal <= dtTodayDate) Then
                            CType(GrdMeeting.Rows(i).FindControl("btnJoinMeeting"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            sessionKey = GrdMeeting.Rows(i).Cells(10).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    GrdMeeting.DataSource = ds
                    GrdMeeting.DataBind()
                    SpnMakeupTitle.Visible = True
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub LoadSubstituteSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select distinct VC.SessionKey, VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SubstituteDate,CS.Vroom,cs.Time,Vl.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachID) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) inner join virtualroomlookup VL on (VL.Vroom=cs.Vroom) where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.SubstituteCoachID=" + Session("LoginID").ToString() + " and VC.SubstituteCoachID is not null"
            Else
                StrSql = "select distinct   VC.SessionKey,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day, IP.FirstName,IP.LastName,CS.SubstituteDate,CS.Vroom,CS.Time, VL.HostID  from WebConfLog VC left join Event E on (E.EventId=VC.EventID) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on (VL.Vroom=CS.Vroom) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachID) "
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & " AND VC.Phase=" & ddlPhase.SelectedValue
                StrSql = StrSql & " where VC.SubstituteCoachID is not null and CS.EventYear=" & ddlEventYear.SelectedValue & ""

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and (VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " or VC.SubstituteCoachID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " and VC.SubstituteCoachID is not null)"
                End If
                StrSql &= " order by IP.FirstName, IP.LastName"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    GrdSubstituteSessions.DataSource = ds
                    GrdSubstituteSessions.DataBind()
                    SpnSubstituteTitle.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0
                    For i = 0 To GrdSubstituteSessions.Rows.Count - 1
                        dateVal = GrdSubstituteSessions.Rows(i).Cells(16).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal <= dtTodayDate) Then
                            CType(GrdSubstituteSessions.Rows(i).FindControl("btnJoin"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            Dim sessionKey As String = GrdMeeting.Rows(i).Cells(11).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    GrdSubstituteSessions.DataSource = ds
                    GrdSubstituteSessions.DataBind()
                    SpnSubstituteTitle.Visible = True
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub GrdMeeting_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "SelectMeetingURL" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                GrdMeeting.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                WebExID = GrdMeeting.Rows(selIndex).Cells(21).Text
                WebExPwd = GrdMeeting.Rows(selIndex).Cells(22).Text

                hdnSessionKey.Value = sessionKey
                Dim beginTime As String = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                    Else
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)

                        Dim CmdText As String = String.Empty
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)

                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub GrdSubstituteSessions_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

                Dim selIndex As Integer = row.RowIndex
                GrdSubstituteSessions.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                'string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                Dim beginTime As String = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                    Else
                        Dim CmdText As String = String.Empty
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Try


            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y') as NStudents,I.EMAIL,I.Hphone,I.CPhone,isnull(C.Confirm,'') Confirm  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,I.Email,I.Hphone,I.CPhone,C.Confirm order by I.LastName, I.FirstName Asc"
            Else
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y') as NStudents,I.Email,I.Hphone,I.CPhone,isnull(C.Confirm,'') Confirm  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue <> "Select Product Group", " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Phase=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql = StrSql & " group by C.Confirm,C.MeetingKey,C.MeetingPwd, C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,I.Email, I.Hphone,I.CPhone order by I.LastName, I.FirstName Asc"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                Session("volDataSet") = ds
            End If
            Dim dt As DataTable = ds.Tables(0)
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim FileName As String = "CalendarSignUp_" & Now.ToShortDateString & ".xls"

            oSheet.Range("A1:X1").MergeCells = True
            oSheet.Range("A1").Value = "Calendar SignUp"
            oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("A1").Font.Bold = True

            oSheet.Range("A2").Value = "Ser#"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("A2").Font.Bold = True

            oSheet.Range("B2").Value = "SignUp ID"
            oSheet.Range("B2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("b2").Font.Bold = True

            oSheet.Range("C2").Value = "Volunteer Name"
            oSheet.Range("C2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("C2").Font.Bold = True

            oSheet.Range("D2").Value = "Volunteer Email"
            oSheet.Range("D2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("D2").Font.Bold = True

            oSheet.Range("E2").Value = "Volunteer HPhone"
            oSheet.Range("E2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("E2").Font.Bold = True

            oSheet.Range("F2").Value = "Volunteer CPhone"
            oSheet.Range("F2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("F2").Font.Bold = True

            oSheet.Range("G2").Value = "ProductGroupCode"
            oSheet.Range("G2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("G2").Font.Bold = True

            oSheet.Range("H2").Value = "Product"
            oSheet.Range("H2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("H2").Font.Bold = True

            oSheet.Range("I2").Value = "Level"
            oSheet.Range("I2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("I2").Font.Bold = True

            oSheet.Range("J2").Value = "Session"
            oSheet.Range("J2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("J2").Font.Bold = True

            oSheet.Range("K2").Value = "Day"
            oSheet.Range("K2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("K2").Font.Bold = True

            oSheet.Range("L2").Value = "Time"
            oSheet.Range("L2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("L2").Font.Bold = True

            oSheet.Range("M2").Value = "Accepted"
            oSheet.Range("M2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("M2").Font.Bold = True

            oSheet.Range("N2").Value = "Confirm"
            oSheet.Range("N2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("N2").Font.Bold = True

            oSheet.Range("O2").Value = "Preferences"
            oSheet.Range("O2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("O2").Font.Bold = True

            oSheet.Range("P2").Value = "Max Cap"
            oSheet.Range("P2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("P2").Font.Bold = True

            oSheet.Range("Q2").Value = "#Of Students Approved"
            oSheet.Range("Q2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Q2").Font.Bold = True

            oSheet.Range("R2").Value = "VRoom"
            oSheet.Range("R2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("R2").Font.Bold = True

            oSheet.Range("S2").Value = "UserID"
            oSheet.Range("S2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("S2").Font.Bold = True

            oSheet.Range("T2").Value = "Password"
            oSheet.Range("T2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("T2").Font.Bold = True

            oSheet.Range("U2").Value = "Years"
            oSheet.Range("U2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("U2").Font.Bold = True

            oSheet.Range("V2").Value = "Sessions"
            oSheet.Range("V2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("V2").Font.Bold = True

            oSheet.Range("W2").Value = "SessionKey"
            oSheet.Range("W2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("W2").Font.Bold = True

            oSheet.Range("X2").Value = "Meeting Pwd"
            oSheet.Range("X2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("X2").Font.Bold = True

            oSheet.Range("Y2").Value = "Meeting URL"
            oSheet.Range("Y2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Y2").Font.Bold = True

            oSheet.Range("Z2").Value = "Event Year"
            oSheet.Range("Z2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Z2").Font.Bold = True

            oSheet.Range("AA2").Value = "EventCode"
            oSheet.Range("AA2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("AA2").Font.Bold = True

            oSheet.Range("AB2").Value = "Phase"
            oSheet.Range("AB2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("AB2").Font.Bold = True

            'dsPreYrFamilies Previous AND current year -Cur families
            Dim iRowIndex As Integer = 3, j As Integer
            Dim CRange As IRange
            For j = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(j)
                oSheet.Range("A" & Trim(Str(iRowIndex))).Value = j + 1
                oSheet.Range("B" & Trim(Str(iRowIndex))).Value = dr("SignUpID")
                oSheet.Range("C" & Trim(Str(iRowIndex))).Value = dr("Name")
                oSheet.Range("D" & Trim(Str(iRowIndex))).Value = dr("Email")
                oSheet.Range("E" & Trim(Str(iRowIndex))).Value = dr("HPhone")
                oSheet.Range("F" & Trim(Str(iRowIndex))).Value = dr("CPhone")

                oSheet.Range("G" & Trim(Str(iRowIndex))).Value = dr("ProductGroupCode")
                oSheet.Range("H" & Trim(Str(iRowIndex))).Value = dr("ProductCode")
                oSheet.Range("I" & Trim(Str(iRowIndex))).Value = dr("Level")
                oSheet.Range("J" & Trim(Str(iRowIndex))).Value = dr("SessionNo")
                oSheet.Range("K" & Trim(Str(iRowIndex))).Value = dr("Day")
                oSheet.Range("L" & Trim(Str(iRowIndex))).Value = dr("Time")
                oSheet.Range("M" & Trim(Str(iRowIndex))).Value = dr("Accepted")
                oSheet.Range("N" & Trim(Str(iRowIndex))).Value = dr("Confirm")
                oSheet.Range("O" & Trim(Str(iRowIndex))).Value = dr("Preference")
                oSheet.Range("P" & Trim(Str(iRowIndex))).Value = dr("MaxCapacity")
                oSheet.Range("Q" & Trim(Str(iRowIndex))).Value = dr("NStudents")
                oSheet.Range("R" & Trim(Str(iRowIndex))).Value = dr("VRoom")

                oSheet.Range("S" & Trim(Str(iRowIndex))).Value = dr("UserID")
                oSheet.Range("T" & Trim(Str(iRowIndex))).Value = dr("PWD")
                oSheet.Range("U" & Trim(Str(iRowIndex))).Value = dr("Years")
                oSheet.Range("V" & Trim(Str(iRowIndex))).Value = dr("Sessions")

                oSheet.Range("W" & Trim(Str(iRowIndex))).Value = dr("MeetingKey")
                oSheet.Range("X" & Trim(Str(iRowIndex))).Value = dr("MeetingPwd")

                oSheet.Range("Y" & Trim(Str(iRowIndex))).Value = dr("HostJoinURL").ToString().Substring(0, Math.Min(20, dr("HostJoinURL").ToString().Length))

                oSheet.Range("Z" & Trim(Str(iRowIndex))).Value = dr("EventYear")
                oSheet.Range("AA" & Trim(Str(iRowIndex))).Value = dr("EventCode")
                oSheet.Range("AB" & Trim(Str(iRowIndex))).Value = dr("Phase")

                iRowIndex = iRowIndex + 1
            Next
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Me.DGCoach_UpdateCommand(Me.DGCoach, New DataGridCommandEventArgs(Me.DGCoach.Items(Me.DGCoach.EditItemIndex), Me.DGCoach, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))
    End Sub

    Protected Sub btnCancelGrid_Click(sender As Object, e As EventArgs) Handles btnCancelGrid.Click
        hdInAppr.Value = ""
        hdToAppvId.Value = ""
        DGCoach_CancelCommand(Me.DGCoach, New DataGridCommandEventArgs(Me.DGCoach.Items(Me.DGCoach.EditItemIndex), Me.DGCoach, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))
    End Sub

    Public Sub fillGuestAttendeeGrid()
        Try
            Dim cmdText As String = String.Empty
            Dim ds As New DataSet()
            If Session("RoleID").ToString() = "88" Then
                cmdText = "select GA.MemberID,GA.RegisteredID,GA.GuestAttendID,GA.CMemberID,GA.MemberID,CS.UserID,CS.Pwd,CS.MeetingKey,cs.ProductGroupID,cs.ProductID,cs.SessionNo, cs.Level, IP.FirstName +' '+ IP.LastName as Name, IP1.FirstName +' '+ IP1.LastName as CoachName, IP.Email as WebExEmail, cs.ProductGroupCode,cs.ProductCode,cs.Level, GA.MeetingURL, GA.RegisteredID,CS.Vroom,CS.Time, CS.Day from GuestAttendance GA inner join CalSignup CS on (GA.SessionKey=CS.MeetingKey) inner join IndSpouse IP on(IP.AutoMemberID=GA.MemberID) inner join IndSpouse IP1 on (IP1.AutoMemberID=GA.CMemberID) Where GA.MemberID=" & Session("LoginID").ToString() & " and GA.EventYear=" & ddlEventYear.SelectedValue & ""
            Else
                cmdText = "select GA.MemberID,GA.RegisteredID,GA.GuestAttendID,GA.CMemberID,GA.MemberID,CS.UserID,CS.Pwd,CS.MeetingKey,cs.ProductGroupID,cs.ProductID,cs.SessionNo, cs.Level, IP.FirstName +' '+ IP.LastName as Name, IP1.FirstName +' '+ IP1.LastName as CoachName, IP.Email as WebExEmail, cs.ProductGroupCode,cs.ProductCode,cs.Level, GA.MeetingURL, GA.RegisteredID,CS.Vroom,CS.Time, CS.Day from GuestAttendance GA inner join CalSignup CS on (GA.SessionKey=CS.MeetingKey) inner join IndSpouse IP on(IP.AutoMemberID=GA.MemberID) inner join IndSpouse IP1 on (IP1.AutoMemberID=GA.CMemberID) where GA.EventYear=" & ddlEventYear.SelectedValue & ""
            End If
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    spnGuestAttTitle.Visible = False
                    GrdGuestAttendee.DataSource = ds
                    GrdGuestAttendee.DataBind()
                Else
                    spnGuestAttTitle.Visible = True

                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub fillPractiseSession()
        Dim cmdtext As String = ""
        Dim ds As New DataSet()
        spnTable1Title.Visible = True
        GrdMeeting.Visible = True
        cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) where "
        If Session("RoleID").ToString() = "88" Then
            cmdtext &= "VC.MemberID=" & Session("LoginID") & " and VC.SessionType='Practice'  order by VC.ProductGroupCode"
        Else
            cmdtext &= " VC.SessionType='Practice'  order by VC.ProductGroupCode"
        End If
        Try
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then

                    grdPractiseSession.DataSource = ds
                    grdPractiseSession.DataBind()
                    spnPractiseNoRecord.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0

                    For i = 0 To grdPractiseSession.Rows.Count - 1
                        dateVal = grdPractiseSession.Rows(i).Cells(13).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal <= dtTodayDate) Then
                            CType(grdPractiseSession.Rows(i).FindControl("btnJoinMeeting"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            Dim sessionKey As String = GrdMeeting.Rows(i).Cells(11).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    grdPractiseSession.DataSource = ds
                    grdPractiseSession.DataBind()
                    spnPractiseNoRecord.Visible = True
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Protected Sub grdPractiseSession_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                grdPractiseSession.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty

                WebExID = grdPractiseSession.Rows(selIndex).Cells(21).Text
                WebExPwd = grdPractiseSession.Rows(selIndex).Cells(22).Text
                sessionKey = grdPractiseSession.Rows(selIndex).Cells(10).Text

                sessionKey = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblStSessionKey"), Label), Label).Text
                Dim hostID As String = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblSthostID"), Label), Label).Text

                hdnSessionKey.Value = sessionKey
                Dim beginTime As String = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                    Else
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)
                        Dim CmdText As String = String.Empty
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub GrdGuestAttendee_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                Dim beginTime As String = TryCast(DirectCast(GrdGuestAttendee.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdGuestAttendee.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                Dim sessionKey As String = TryCast(DirectCast(GrdGuestAttendee.Rows(selIndex).FindControl("lblSessionKey"), Label), Label).Text
                hdnSessionKey.Value = sessionKey
                If mins <= 30 AndAlso day = today Then



                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinGuestMeeting()", True)
                Else
                    ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLevel.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        LoadGrid(ddlAcceptedFilter.SelectedValue)
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        If Not Request.QueryString("Role") Is Nothing Then
            Server.Transfer("CalendarSignup.aspx?Role=" + Request.QueryString("Role"))
        Else
            Server.Transfer("CalendarSignup.aspx")
        End If

    End Sub



    Protected Sub btnConfirmToUpdate_Click(sender As Object, e As EventArgs) Handles btnConfirmToUpdate.Click
        Dim strSignUpIds As String = ""
        Dim chkRow As CheckBox
        Dim r As GridViewRow
        For Each r In gvConfirmList.Rows
            chkRow = r.Cells(0).FindControl("chkRow")
            If chkRow.Checked Then
                If strSignUpIds.Length > 0 Then
                    strSignUpIds = strSignUpIds & "," & TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                Else
                    strSignUpIds = TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                End If
            End If
        Next


        If strSignUpIds.Length > 0 Then
            Dim strSql As String = "update CalSignup set Confirm='Y' where SignUpId in ( " & strSignUpIds & ") and EventID=13 and EventYear=" & ddlEventYear.SelectedValue
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            LoadGrid(ddlAcceptedFilter.SelectedValue)

            strSql = " select count(*) from CalSignUp where MemberID=" & Session("LoginID") & " and Accepted='Y' and Confirm is null and EventYear=" & ddlEventYear.SelectedValue
            Dim flag As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            If flag > 0 Then
                btnConfirm.Visible = True
            Else
                btnConfirm.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnNotConfirm_Click(sender As Object, e As EventArgs) Handles btnNotConfirm.Click
        Dim strSignUpIds As String = ""
        Dim chkRow As CheckBox
        Dim r As GridViewRow
        For Each r In gvConfirmList.Rows
            chkRow = r.Cells(0).FindControl("chkRow")
            If chkRow.Checked Then
                If strSignUpIds.Length > 0 Then
                    strSignUpIds = strSignUpIds & "," & TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                Else
                    strSignUpIds = TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                End If
            End If
        Next
        If strSignUpIds.Length > 0 Then
            Dim strSql As String = "update CalSignup set Confirm='N' where SignUpId in ( " & strSignUpIds & ") and EventID=13 and EventYear=" & ddlEventYear.SelectedValue
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            LoadGrid(ddlAcceptedFilter.SelectedValue)

            strSql = " select count(*) from CalSignUp where MemberID=" & Session("LoginID") & " and Accepted='Y' and Confirm is null and EventYear=" & ddlEventYear.SelectedValue
            Dim flag As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            If flag > 0 Then
                btnConfirm.Visible = True
            Else
                btnConfirm.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click
        Dim ds As DataSet
        Dim strSql As String = ""
        Try
            strSql = "SELECT  C.SignUpID,C.MemberID,C.EventYear,C.EventID, C.Phase,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Preference FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue
            strSql = strSql & " and C.MemberID=" & Session("LoginID") & " and C.Accepted='Y' and C.Confirm is null"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                gvConfirmList.DataSource = ds.Tables(0)
                gvConfirmList.DataBind()
                'Dim dr As DataRow = ds.Tables(0).Rows(0)
                'ViewState("SignupID") = dr("SignupId")
                'lblCProduct.Text = dr("ProductCode")
                'lblCLevel.Text = dr("Level")
                'lblCSession.Text = dr("SessionNo")
                'lblCDay.Text = dr("Day")
                'lblCTime.Text = dr("Time")
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "PopUpConfirmBox('show');", True)
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlAcceptedFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcceptedFilter.SelectedIndexChanged
        LoadGrid(ddlAcceptedFilter.SelectedValue)
    End Sub


    Protected Sub DGCoach_SortCommand(source As Object, e As DataGridSortCommandEventArgs)
        Dim strSQL As String
        Dim dt As DataTable

        dt = TryCast(ViewState("dtbl"), DataTable)

        If True Then
            Dim SortDir As String = String.Empty
            If dir = SortDirection.Ascending Then
                dir = SortDirection.Descending
                SortDir = "Desc"
            Else
                dir = SortDirection.Ascending
                SortDir = "Asc"
            End If
            Dim sortedView As New DataView(dt)
            sortedView.Sort = Convert.ToString(e.SortExpression + " ") & SortDir
            DGCoach.DataSource = sortedView
            DGCoach.DataBind()
        End If
    End Sub

    Protected Property dir() As SortDirection
        Get
            If ViewState("dirState") Is Nothing Then
                ViewState("dirState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("dirState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("dirState") = value
        End Set
    End Property

    Public Sub ddlDGSessionNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        lblError.Text = ""
        Try


            Dim dr As DataRow = CType(Cache("editRow"), DataRow)

            If (Not dr Is Nothing) Then

                Dim cmdText As String = String.Empty
                Dim pgID As String = dr.Item("productGroupID").ToString()
                Dim pID As String = dr.Item("productID").ToString()
                Dim year As String = dr.Item("EventYear").ToString()
                Dim Level As String = dr.Item("Level").ToString()
                Dim sessionNo As String = ddlTemp.SelectedValue

                Dim iSessionNo As Integer = Convert.ToInt32(sessionNo) - 1
                hdnSessionNo.Value = ddlTemp.SelectedValue
                If sessionNo <> "1" Then


                    cmdText = " select count(*) as sessioNoCount from CalSignup where EventYear=" & year & " and ProductGroupID=" & pgID & " and ProductID=" & pID & " and Level='" & Level & "' and SessionNo=" & iSessionNo & " and MemberID=" & dr.Item("MemberID").ToString() & ""
                    'Session("editRow") = dr
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                    If ds.Tables(0).Rows.Count > 0 Then
                        If Convert.ToInt32(ds.Tables(0).Rows(0)("sessioNoCount").ToString()) = 0 Then
                            lblError.Text = "Higher Session # was selected without a lower session # selected earlier."
                            ddlTemp.SelectedValue = 1
                            hdnSessionNo.Value = 1
                        Else

                        End If


                    End If
                End If
                Cache("editRow") = dr
            Else
                Return
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub


    Public Sub ddlDGDay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim day As String = ddlTemp.SelectedValue
        Dim startTime As String = dr.Item("Time").ToString()
        hdnDay.Value = day
        Dim cmdText As String = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        Dim Duration As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            Duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
            If Duration = 1 Then
                Duration = 60
            ElseIf Duration = 1.5 Then
                Duration = 90
            ElseIf Duration = 2 Then
                Duration = 120
            Else
                Duration = 180
            End If
        End If
        Try


            Dim ddlVRoom As DropDownList = CType(DGCoach.Items(hdnItemIndex.Value).FindControl("ddlDGVRoom"), DropDownList)
            checkAvailableVrooms(Duration, startTime, day, ddlVRoom, dr.Item("SignupID").ToString())
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Public Sub ddlDGTime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim day As String = dr.Item("Day").ToString()
        Dim startTime As String = ddlTemp.SelectedValue
        hdnBeginTime.Value = startTime
        Dim cmdText As String = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        Dim Duration As Integer
        If ds.Tables(0).Rows.Count > 0 Then
            Duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
            If Duration = 1 Then
                Duration = 60
            ElseIf Duration = 1.5 Then
                Duration = 90
            ElseIf Duration = 2 Then
                Duration = 120
            Else
                Duration = 180
            End If
        End If
        Try


            Dim ddlVRoom As DropDownList = CType(DGCoach.Items(hdnItemIndex.Value).FindControl("ddlDGVRoom"), DropDownList)
            checkAvailableVrooms(Duration, startTime, day, ddlVRoom, dr.Item("SignupID").ToString())
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub checkAvailableVrooms(ByVal Duration As Integer, ByVal strTime As String, ByVal day As String, ddlObject As DropDownList, ByVal SignupID As String)

        Dim cmdtext As String = String.Empty
        Dim ds As DataSet = New DataSet()
        Dim hostID As String = String.Empty

        Dim strStartTime As String = strTime
        Dim strStartDay As String = day
        Duration = Duration + 30
        Dim strEndTime As String = GetTimeFromString(strTime, Duration).ToString()
        strStartTime = GetTimeFromStringSubtract(strTime, -30).ToString()

        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd from CalSignup CS inner join WebConfLog VC on (CS.Meetingkey=VC.SessionKey or CS.makeupMeetKey=VC.Sessionkey) inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=2016 and ((VC.BeginTime between '" & strStartTime & "' and '" & strEndTime & "') or (VC.EndTime between '" & strStartTime & "' and '" & strEndTime & "')) and VC.Day ='" & strStartDay & "' and CS.Vroom is not null and CS.SignupID not in (" & SignupID & ")"

        Try
            Dim vRooms As String = String.Empty


            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        vRooms += dr("VRoom").ToString() + ","
                    Next
                End If
            End If

            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=2016 and ((VC.BeginTime between '" & strStartTime & "' and '" & strEndTime & "') and VC.Day='" & strStartDay & "' and  SessionType='Practice' and StartDate>= cast(GetDate() as date)) or ((VC.EndTime between '" & strStartTime & "' and '" & strEndTime & "') and VC.Day='" & strStartDay & "' and  SessionType='Practice' and StartDate>= cast(GetDate() as date)) and VC.Day ='" & strStartDay & "' and SessionType='Practice' and StartDate>= cast(GetDate() as date)"

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        vRooms += dr("VRoom").ToString() + ","
                    Next
                End If
            End If


            If vRooms <> "" Then
                vRooms = vRooms.TrimEnd(",")
            End If

            If vRooms <> "" Then
                cmdtext = "select HostID, USerID, PWD, Vroom from VirtualRoomLookUp where Vroom not in (" & vRooms & ")"
            Else
                cmdtext = "select HostID, USerID, PWD, Vroom from VirtualRoomLookUp"
            End If

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlObject.DataValueField = "Vroom"
                    ddlObject.DataTextField = "Vroom"
                    ddlObject.DataSource = ds
                    ddlObject.DataBind()
                Else
                    ddlObject.DataValueField = "Vroom"
                    ddlObject.DataTextField = "Vroom"
                    ddlObject.DataSource = ds
                    ddlObject.DataBind()
                    lblerr.Text = "No Vroom is available at this time. Please choose different time."
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try

    End Sub

    Public Sub writeToLogFile(logMessage As String)
        Dim strPath As String '= Server.MapPath(strLogFile)
        Dim strLogMessage As String = ""
        Dim strLogFile As String = String.Empty
        Try
            strLogFile = "~\ZoomMeeting_" & DateTime.Now.ToShortDateString() & ".txt"
            strPath = Server.MapPath(strLogFile)

            Dim swLog As StreamWriter
            strLogMessage = String.Format("{0}: {1}", Date.Now, logMessage)
            If (Not File.Exists(strLogFile)) Then
                swLog = New StreamWriter(Server.MapPath(strLogFile))

            Else
                swLog = File.AppendText(strLogFile)

            End If
            swLog.WriteLine(strLogMessage)
            swLog.WriteLine()
            swLog.Close()
        Catch ex As Exception
        End Try
    End Sub

    Public Sub getmeetingIfo(sessionkey As String, HostID As String)
        Try
            Dim URL As String = String.Empty
            Dim service As String = "5"
            URL = "https://api.zoom.us/v1/meeting/get"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += (Convert.ToString("&id=") & sessionkey) + ""
            urlParameter += (Convert.ToString("&host_id=") & HostID) + ""

            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try

    End Sub

    Public Sub makeZoomAPICall(urlParameters As String, URL As String, serviceType As String)
        Try


            Dim objRequest As HttpWebRequest = DirectCast(WebRequest.Create(URL), HttpWebRequest)
            objRequest.Method = "POST"
            objRequest.ContentLength = urlParameters.Length
            objRequest.ContentType = "application/x-www-form-urlencoded"

            ' post data is sent as a stream
            Dim myWriter As StreamWriter = Nothing
            myWriter = New StreamWriter(objRequest.GetRequestStream())
            myWriter.Write(urlParameters)
            myWriter.Close()

            ' returned values are returned as a stream, then read into a string
            Dim postResponse As String
            Dim objResponse As HttpWebResponse = DirectCast(objRequest.GetResponse(), HttpWebResponse)
            Using responseStream As New StreamReader(objResponse.GetResponseStream())
                postResponse = responseStream.ReadToEnd()

                responseStream.Close()
            End Using

            If serviceType = "5" Then

                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)


                hdnHostURL.Value = json("start_url").ToString()
                hdnHostMeetingURL.Value = json("start_url").ToString()

            ElseIf serviceType = "6" Then
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                Dim SessionKey As String = String.Empty

                For Each item As Object In json("meetings")
                    If (item("host_id").ToString() = hdnHostID.Value) Then

                        termianteMeeting(item("id").ToString(), hdnHostID.Value)

                    End If



                Next


            ElseIf serviceType = "7" Then
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                Dim SessionKey As String = String.Empty

            End If

        Catch
        End Try
        'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    End Sub

    Public Sub listLiveMeetings()
        Try
            Dim URL As String = String.Empty
            Dim service As String = "6"
            URL = "https://api.zoom.us/v1/meeting/live"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hdnHostID.Value + ""

            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Public Sub termianteMeeting(ByVal sessionKey As String, ByVal hostID As String)
        Try
            Dim URL As String = String.Empty
            Dim service As String = "7"
            URL = "https://api.zoom.us/v1/meeting/end"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hostID + ""
            urlParameter += "&id=" + sessionKey + ""


            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

End Class
