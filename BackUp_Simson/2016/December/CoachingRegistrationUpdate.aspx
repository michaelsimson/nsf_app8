﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingRegistrationUpdate.aspx.vb" Inherits="CoachingRegistrationUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div>
        <link href="css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>

        <script src="js/ezmodal.js"></script>
        <script type="text/javascript">
            $(function (e) {


                $(".lnkCoachName").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {
                            return '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx?coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Calendar signup of that coach (all signups)</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachClassCalendar.aspx?coachID=' + $(this).attr("attr-coachid") + '"" target="_blank">Set up Class Calendar</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1&coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Contact Information</a> </div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '" target="_blank" >Contact List of Students</a> </div>';
                        },

                        title: 'Online Coaching!',
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow'
                    },
                    show: {
                        solo: true
                    }
                })

                $(".lnkParentName").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {
                            return '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + $(this).attr("attr-parentId") + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx" target="_blank">Change Coach</a> </div>';
                        },

                        title: 'Online Coaching!',
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow'
                    },
                    show: {
                        solo: true
                    }
                })


            });

            $(document).on("mouseover", ".ancParentInfo", function (e) {

                getParentInfo($(this).attr("attr-parentID"));
            });

            $(document).on("click", ".ancParentInfo", function (e) {
                $("#btnPopUP").trigger("click");
            });

            function getParentInfo(pMemberID) {

                var jsonData = { PMemberID: pMemberID };
                $.ajax({
                    type: "POST",
                    url: "SetupZoomSessions.aspx/ListParentInfo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {

                        var tblHtml = "";
                        tblHtml += " <thead>";
                        tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent1 Name</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Parent2 Name</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>Parent1 Email</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent2 Email</td>";

                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child Name</td>";


                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child EMail</td>";
                        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
                        tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"


                        tblHtml += "</tr>";
                        tblHtml += " </thead>";

                        $.each(data.d, function (index, value) {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndFirstName + " " + value.IndLastName + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseFirstName + " " + value.SpouseLastName + "";
                            tblHtml += "</td>";


                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndEMail + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseEmail + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildFirstName + " " + value.ChildLastName + "";
                            tblHtml += "</td>";



                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildEmail + "";
                            tblHtml += "</td>";

                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                            tblHtml += "</td>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                            tblHtml += "</td>";


                            tblHtml += "</tr>";
                        });

                        $("#tblParentInfo").html(tblHtml);


                    }
                })
            }
        </script>
    </div>

    <div align="left">
        &nbsp;&nbsp;&nbsp;
      <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
    </div>
    <div style="text-align: center">
        <table border="0" cellpadding="3" cellspacing="0" style="width: 800px; text-align: center">
            <tr>
                <td align="center" style="color: #000000; font-family: Arial Rounded MT Bold; font-size: 14px;">Update Child Registration Data</td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr id="trvol" runat="server">
                            <td class="ItemLabel" valign="top" nowrap align="right">Parent Email ID</td>
                            <td>
                                <asp:TextBox ID="txtUserId" runat="server" CssClass="SmallFont" Width="300" MaxLength="50"></asp:TextBox>
                                <asp:Button ID="btnLoadChild" OnClick="btnLoadChild_Click" runat="server" CssClass="FormButtonCenter" Text="Load Child(ren)"></asp:Button>
                                <br>
                                &nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
                            Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
                                    ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnContinue" runat="server" CausesValidation="false" Text="Load All" OnClick="btnContinue_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trEdit" visible="false" runat="server">
                <td align="center">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="left">Approved </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlApproved" runat="server">
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Product </td>
                            <td align="left">
                                <asp:DropDownList Width="150px" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" ID="ddlProduct" DataTextField="ProductName" DataValueField="ProductID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Coach </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCoachName" Enabled="false" DataTextField="CoachName" DataValueField="SignUpID" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnUpdate" runat="server" CausesValidation="false" Text="Update" OnClick="btnUpdate_Click" />
                                &nbsp;&nbsp;&nbsp;   
                        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblApproved" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr id="trExcCapacity" runat="server" visible="false">
                <td>
                    <table align="center">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblExcCap" ForeColor="Red" Font-Bold="true" runat="server" Text="Capacity will be exceeded, do you still want to approve this case?"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="BtnYes" runat="server" Text="Yes" Style="width: 37px" />
                                &nbsp;
                        <asp:Button ID="BtnNo" runat="server" Text="No" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:DataGrid ID="dgselected" runat="server" OnItemCommand="dgselected_ItemCommand" OnPageIndexChanged="dgselected_PageIndexChanged" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
                        Height="14px" GridLines="Both" DataKeyField="CoachRegID" CellPadding="4" BackColor="#755200" BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666" PageSize="10" AllowPaging="true" ForeColor="White" Font-Bold="True">
                        <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                        <HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:BoundColumn DataField="CoachRegID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="childnumber" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductCode" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="SignUpID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductID" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Edit">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CausesValidation="false" CommandName="Select" Text="Edit"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChildName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Product" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Coach Name" ItemStyle-Width="12%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachName" CssClass="lnkCoachName" Style="cursor: pointer; color: blue;" attr-coachid='<%#DataBinder.Eval(Container.DataItem, "CMemberID")%>' runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Level" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Level")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "MaxCapacity")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ApprovedCount" ItemStyle-HorizontalAlign="Center" HeaderText="Approved Count" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApproved" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Approved")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Coach Day" HeaderStyle-Width="15%" Visible="true" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Time" HeaderStyle-Width="15%" Visible="true" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoachTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SessionNo" HeaderText="Session#" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Start Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblstartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "EndDate","{0:d}")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="FatherName" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFatherName" runat="server" CssClass="lnkParentName" Style="color: blue; cursor: pointer;" attr-parentID='<%#DataBinder.Eval(Container.DataItem, "PMemberID")%>' Text='<%#DataBinder.Eval(Container.DataItem, "FatherName")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <%--  <asp:BoundColumn DataField="FatherName" HeaderText="Father Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="EMail" HeaderText="Email" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="City" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblcity" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "city")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="State" HeaderStyle-Width="15%" ItemStyle-Width="15%">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "State")%>' Visible="True"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" />
                        <PagerStyle ForeColor="white" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PagerStyle>
                        <AlternatingItemStyle BackColor="LightBlue" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblCoachRegID" runat="server" ForeColor="White"></asp:Label>
                    <asp:Label ID="lblSignUpID" runat="server" ForeColor="White"></asp:Label>
                    <asp:Label ID="lblChildNumber" runat="server" ForeColor="White"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

    <button type="button" style="display: none;" id="btnPopUP" ezmodal-target="#demo">Open</button>

    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                Parent Information
            </div>

            <div class="ezmodal-content">


                <div>
                    <table align="center" id="tblParentInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px;">
                    </table>
                </div>

            </div>



            <div class="ezmodal-footer">

                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>

</asp:Content>

