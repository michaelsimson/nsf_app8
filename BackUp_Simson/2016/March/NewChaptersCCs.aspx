﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewChaptersCCs.aspx.cs" Inherits="NewChaptersCCs" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        New Chapters and CCs

                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div style="float: right;">
        <asp:Button ID="btnExportExcel" runat="server" Text="Export To Excel" OnClick="btnExportExcel_Click" />
    </div>
    <div align="center">

        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center">
            <span id="spnTable2Title" style="font-weight: bold;" runat="server" visible="true">Table 1: New Chapters
            </span>
        </div>

        <div style="clear: both;"></div>

        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdChpaterCodes" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 400px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
            <Columns>



                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter Code"></asp:BoundField>

            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>

        <div style="clear: both;"></div>
        <div align="center">
            <span id="spnStatus" runat="server" style="color: red;" visible="false">No record found</span>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div align="center">
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center">
            <span id="Span1" style="font-weight: bold;" runat="server" visible="true">Table 2: New Chapter Coordinators

            </span>
        </div>

        <div style="clear: both;"></div>

        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdChapterCoordinator" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 900px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
            <Columns>



                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter Code"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="First Name"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="HPhone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>

            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>

        <div style="clear: both;"></div>
        <div align="center">
            <span id="spnChapCordinator" runat="server" style="color: red;" visible="false">No record found</span>
        </div>
    </div>

</asp:Content>
