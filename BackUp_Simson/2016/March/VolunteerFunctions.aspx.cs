using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using IndSpouseTableAdapters;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

//Note: All Volunteers will access Personal and General Functions:
//Note: Any role > 5 who has a team lead Y can access Team Member panel.
//Note:Only roles 1, 2, and 9 should be able to access Technical Coordinator panel
//Note: Accounting panel should only be accessibly by roles 1, 2, 5, 37 and 38.  NO OTHER roles should be able to see or access it.


public partial class Admin_VolunteerFunctions : System.Web.UI.Page
{
    public String ChapterID;


    protected void Page_Load(object sender, EventArgs e)
    {
        Session["sSQL"] = "";
        Session["sSQLORG"] = "";
        Session["NavPath"] = "";
        lblChapterError.Text = "";
        Session["Facilities"] = "";
        Session["SmLogIDCluster"] = null;
        Session["SmLogIDZone"] = null;
        Session["SmLogIDChapter"] = null;
        Session["Panel"] = null;
        lnk_VolSignupReport.Attributes.Add("onclick", "return validate()");
        lnkSuppressionList.Attributes.Add("onclick", "return validate()");
        lnkContestCalender.Attributes.Add("onclick", "return validate()");
        lnkContestTiming.Attributes.Add("onclick", "return validate()");
        lnkEventContestDaySchedule.Attributes.Add("onclick", "return validateFacilities()");
        lnkContestSchedule.Attributes.Add("onclick", "return validate()");
        lnkChDonorList.Attributes.Add("onclick", "return validate()");
        lnkMedalList.Attributes.Add("onclick", "return validate()");
        lnkbtnSupportticket.Attributes.Add("onclick", "return validate()");
        lnkUploadSign.Attributes.Add("onclick", "return validate()");
        lnkMissingScoresChapter.Attributes.Add("onclick", "return validate()");
        lnkEthnicOrg.Attributes.Add("onclick", "return validate()");
        lbtnExp.Attributes.Add("onclick", "return validate()");
        lnkAssignRoles.Attributes.Add("onclick", "return validateRole()");
        // lnkviewbulletinboard.Attributes.Add("onclick", "return validateRole()");
        lnkGenerateBadgeNumber.Attributes.Add("onclick", "return validateRole()");
        Hyperlink58.Attributes.Add("onclick", "return validateRole()");
        HyperLink59.Attributes.Add("onclick", "return validateRole()");
        BtnContinue.Attributes.Add("onclick", "return validateSelection()");
        lnkAssignRolesCluster.Attributes.Add("onclick", "return validateCluster()");
        //lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");
        lnkClustDonorlist.Attributes.Add("onclick", "return validateCluster()");
        lnkAssignRolesZonal.Attributes.Add("onclick", "return validateZone()");
        lnkAssignRole.Attributes.Add("onclick", "return validateFinalCoordinator()");
        lnkChWebPageMgmt.Attributes.Add("onclick", "return validate()");
        lnkFundRaisingCal.Attributes.Add("onclick", "return validate()");

        lnkFundRaisingCalFunc.Attributes.Add("onclick", "return validateFundR()");
        LnkFundRContestRegReport.Attributes.Add("onclick", "return validateFundR()");
        lnkFundRPendingRegReport.Attributes.Add("onclick", "return validateFundR()");
        LnkFundRSendEmail.Attributes.Add("onclick", "return validateFundR()");
        LnkCheckinList.Attributes.Add("onclick", "return validateFundR()");
        LnkContestantList.Attributes.Add("onclick", "return validateFundR()");

        lnkEmail5.Attributes.Add("onclick", "return validate()");

        lnkTeamPlanning.Attributes.Add("onclick", "return validateFacilities()");


        LinkButton13.Attributes.Add("onclick", "return validate()");

        lnkEmail3.Attributes.Add("OnClick", "return validateZone()");
        lnkMissingScoreZonal.Attributes.Add("OnClick", "return validateZone()");
        lnkUnContactlst.Attributes.Add("OnClick", "return validateZone()");
        lnkContactlst.Attributes.Add("OnClick", "return validateZone()");
        lnkZoneDonorList.Attributes.Add("OnClick", "return validateZone()");
        lnkEmail4.Attributes.Add("OnClick", "return validateCluster()");
        lnkMissingScorecluster.Attributes.Add("Onclick", "return validateCluster()");
        lnkContactlist1.Attributes.Add("OnClick", "return validateCluster()");
        lnkUnassignedCntLst1.Attributes.Add("OnClick", "return validateCluster()");
        lnkContactlst2.Attributes.Add("onclick", "return validate()");
        lnkUnassignedCntLst2.Attributes.Add("onclick", "return validate()");
        LinkViewRegistrations.Attributes.Add("OnClick", "return validate()");
        lbtnSchedule.Attributes.Add("onclick", "return validateRole()");
        lnkWorkShopCal.Attributes.Add("onclick", "return validateWrkShop()");
        LinkViewWorkshopRegistrations.Attributes.Add("onclick", "return validateWrkShop()");
        lnkWkShopEmail.Attributes.Add("onclick", "return validateWrkShop()");
        lnkbtnWorkShopSupporttracking.Attributes.Add("onclick", "return validateWrkShop()");
        LinkTopRanksList.Attributes.Add("onclick", "return validate()");
        LinkTopRanksListFinals.Attributes.Add("onclick", "return validate()");
        lnkBtnFinalists.Attributes.Add("onclick", "return validate()");

        lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
        lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
        lnkRoomSchedule.Attributes.Add("onclick", "return validateFacilities()");
        lnkContestTeam.Attributes.Add("onclick", "return validateFacilities()");
        lnkRoomGuides.Attributes.Add("onclick", "return validateFacilities()");
        lnkGraders.Attributes.Add("onclick", "return validateFacilities()");
        lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");
        lnkPrepClub.Attributes.Add("onclick", "return validatePrepClub()");
        LinkViewPrepClub.Attributes.Add("onclick", "return validatePrepClub()");
        lnkPrepClubEmail.Attributes.Add("onclick", "return validatePrepClub()");
        lnkbtnPreclubSupporttracking.Attributes.Add("onclick", "return validatePrepClub()");
        //lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");
        lnkViewContestTeamSch.Attributes.Add("onclick", "return validateFacilities()");

        Session["LoginClusterID"] = "";
        Session["LoginChapterID"] = "";
        Session["LoginZoneID"] = "";

        Page.MaintainScrollPositionOnPostBack = true;


        if (this.IsPostBack == false)
        {
            ddlChapterFundR.DataSource = ChapersDSet;
            ddlChapterFundR.DataTextField = "ChapterCode";
            ddlChapterFundR.DataValueField = "ChapterID";
            ddlChapterFundR.DataBind();

            DdlCluster.DataSource = ClusterDS;
            DdlCluster.DataTextField = "ClusterCode";
            DdlCluster.DataValueField = "ClusterID";
            DdlCluster.DataBind();

            ddlChapter.DataSource = ChapersDSet;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();

            ddlChapter1.DataSource = ChapersDSet;
            ddlChapter1.DataTextField = "ChapterCode";
            ddlChapter1.DataValueField = "ChapterID";
            ddlChapter1.DataBind();

            fillChapterBasedOnRole(ddlChapter1);

            ddlChapterWkShop.DataSource = ChapersDSet;
            ddlChapterWkShop.DataTextField = "ChapterCode";
            ddlChapterWkShop.DataValueField = "ChapterID";
            ddlChapterWkShop.DataBind();

            ddlChapterPrepClub.DataSource = ChapersDSet;
            ddlChapterPrepClub.DataTextField = "ChapterCode";
            ddlChapterPrepClub.DataValueField = "ChapterID";
            ddlChapterPrepClub.DataBind();

            DdlZonalCoordinator.DataSource = ZoneDS;
            DdlZonalCoordinator.DataTextField = "ZoneCode";
            DdlZonalCoordinator.DataValueField = "ZoneID";
            DdlZonalCoordinator.DataBind();

            Session["SelZoneID"] = Session["SelClusterID"] = Session["SelChapterID"] = Session["SelCustIndID"] = null;

            //Remove the above 4 lines before installation
            trAdmin.Visible = false;
            trNationalCoordinator.Visible = false;
            trParent.Visible = false;
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trRentalNeeds.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trAccountingFunctions.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trBeeBook.Visible = false;
            Session["LoginRole"] = null;
            Session["LoginTeamLead"] = 0;
            Session["LoginTeamMember"] = 0;
            Session["LoginChapterID"] = null;
            Session["LoginEventID"] = null;
            if (Session["LoggedIn"] == null)
                Response.Redirect("MainTest.aspx");
            if ((Session["LoggedIn"] != null && Session["LoggedIn"].ToString() != "True") ||
                (Session["EntryToken"] != null && Session["EntryToken"].ToString() != "Volunteer"))
                Response.Redirect("MainTest.aspx");
            int ID = 0;
            if (Session["LoggedIn"] != null)
                ID = Convert.ToInt32(Session["LoginID"]);
            string Email = "";
            if (Session["LoginEmail"] != null)
                Email = Session["LoginEmail"].ToString();
            IndSpouseTableAdapters.IndSpouseTableAdapter it = new IndSpouseTableAdapter();
            DataTable dt = new DataTable();
            dt = it.GetData(ID);

            string VolEmail = null;
            if (dt.Rows.Count > 0)
                VolEmail = dt.Rows[0].ItemArray[22].ToString().Trim();
            if (VolEmail != null && VolEmail == Email)
                lblMessage.Text = "Found";
            else lblMessage.Text = "Volunteer Not Found";
            FindRole(ID);
            if (Session["RoleID"] != null)
            {
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96")
                {
                    PWebConfControl.Visible = true;
                }
                else
                {
                    PWebConfControl.Visible = false;
                }
            }
            //fillChapterBasedOnRole(ddlChapter1);
        }




        //else
        //{
        //    Hyperlink72.Enabled = false;
        //}

        int WalkCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(eventID) from Event where EventId=5 and Status = 'O'"));
        if (WalkCount > 0)
        {
            hlinkWalkathon.Enabled = true;
        }
        hidelinksbasedonRoleID();
        hideSBVBTestPaperLink();
        visibleVolSignReportlinks();

        if (Session["RoleID"] != null)
        {
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96")
            {
                PWebConfControl.Visible = true;
            }
            else
            {
                PWebConfControl.Visible = false;
            }
        }

        if (Session["LoginID"] != null)
        {
            if (Session["LoginID"].ToString() == "51336")
            {
                LinkButton2.Visible = true;
            }
            else
            {
                LinkButton2.Visible = false;
            }
        }

    }
    protected void hidelinksbasedonRoleID()
    {
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 46))
        {
            trVolRecruit.Visible = true;
        }
        else
        {
            trVolRecruit.Visible = false;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 92))
        {
            Hyperlink72.Enabled = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 88) || (Convert.ToInt32(Session["RoleId"]) == 89))
        {
            Hyperlink15.Enabled = false;
            Hyperlink71.Enabled = false;
        }

        // August 13 205
        //For enbaling Virtual Room Requirement link for RoleID=96 And TeamLead=Y also.

        if (Convert.ToInt32(Session["RoleId"]) == 1 || Convert.ToInt32(Session["RoleId"]) == 96)
        {
            PWebConfControl.Visible = true;

            // Disable Virtual Room Requirement (link)
            if (Convert.ToInt32(Session["RoleId"]) == 1)
            {
                lnkBtnVirtualRoomReq.Enabled = true;
            }
            else if (Convert.ToInt32(Session["RoleId"]) == 96)
            {
                pnlAddUpdateEventFees.Visible = true;
                string TeamLead = string.Empty;
                DataSet dsVol = new DataSet();
                string cmdTeamText = "select TeamLead from Volunteer where MemberID=" + Session["LoginID"].ToString() + " and RoleID=" + Session["RoleID"].ToString() + "";
                dsVol = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdTeamText);
                if (null != dsVol && dsVol.Tables != null)
                {
                    if (dsVol.Tables[0].Rows.Count > 0)
                    {
                        TeamLead = dsVol.Tables[0].Rows[0]["TeamLead"].ToString();
                    }
                }

                if (TeamLead == "Y")
                {
                    lnkBtnVirtualRoomReq.Enabled = true;

                }
                else
                {
                    lnkBtnVirtualRoomReq.Enabled = false;
                }
            }

        }
        else
        {
            PWebConfControl.Visible = false;
            pnlAddUpdateEventFees.Visible = false;
            lnkBtnVirtualRoomReq.Enabled = false;
        }


        //if (Convert.ToInt32(Session["RoleId"]) != 1)
        //{
        //    // Disable Virtual Room Requirement (link)
        //    lnkBtnVirtualRoomReq.Enabled = false;
        //}
        if ((Convert.ToInt32(Session["RoleID"]) == 1) || (Convert.ToInt32(Session["RoleID"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 46))
        {
            Hyperlink152.Enabled = true;
        }

        if (Convert.ToInt32(Session["RoleID"]) == 88)
        {
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            string strSql = "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=88 and TeamLead='Y' and MemberID=" + Session["LoginID"].ToString();
            int mcount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strSql));

            if (mcount > 0)
            {
                btnDownloadCoachPapers.Text = "Upload/Download Coach Papers";
            }
            else
            {
                btnDownloadCoachPapers.Text = "Download Coach Papers";
            }
        }


    }
    protected void visibleVolSignReportlinks()
    {
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 3) || (Convert.ToInt32(Session["RoleId"]) == 4) || (Convert.ToInt32(Session["RoleId"]) == 5))
        {
            lnk_VolSignupReport.Visible = true;
            HyperLink199.Visible = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 94))
        {
            HyperLink197.Visible = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 97))
        {
            HyperLink198.Visible = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || ((Convert.ToInt32(Session["RoleId"]) == 5) && Convert.ToInt32(Session["ChapterID"]) == 1))
        {
            HyperLink200.Visible = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96))
        {
            HyperLink202.Visible = true;
        }
    }
    protected void hideSBVBTestPaperLink()
    {
        if (Convert.ToInt32(Session["RoleId"]) == 30)
        {
            int memberid = Convert.ToInt32(Session["LoginID"]);
            DataSet dsProductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupCode from Volunteer where memberid =" + memberid + " and productGroupCode in('SB','VB')");
            if (dsProductgroup.Tables[0].Rows.Count == 0)
            {
                lnkSBVBSelMatrixCoreT.Visible = false;
                hylnkGenSBVBTestPaperCoreT.Visible = false;
            }
            else
            {
                lnkSBVBSelMatrixCoreT.Visible = true;
                hylnkGenSBVBTestPaperCoreT.Visible = true;
            }

        }
    }
    public string NavigateUrl
    {
        get
        {
            string text = (string)ViewState["NavigateUrl"];
            if (text != null)
                return text;
            else
                return string.Empty;
        }
        set
        {
            ViewState["NavigateUrl"] = value;
        }
    }


    private void FindRole(int ID)
    {
        DataTable dt = new DataTable();
        string ChapterCode = "";


        VolByMemIdTableAdapters.VolunteerTableAdapter vt = new VolByMemIdTableAdapters.VolunteerTableAdapter();

        dt = vt.GetData(ID);

        if (dt.Rows.Count == 1)
        {
            DdlRole.Visible = false;
            ArrayList al = new ArrayList();
            ArrayList al2 = new ArrayList();
            //get role code
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                al.Add(dt.Rows[i].ItemArray[3].ToString());
                // test roles here  Response.Write(dt.Rows[i].ItemArray[3].ToString()); 
                if (dt.Rows[i].ItemArray[9].ToString() != "")
                {
                    ChapterCode = dt.Rows[i].ItemArray[9].ToString() + "," + ChapterCode;
                }
            }

            if (ChapterCode.EndsWith(","))
            {
                ChapterCode = ChapterCode.Substring(0, ChapterCode.Length - 1);
            }

            //get teamlead

            for (int i = 0; i < dt.Rows.Count; i++)
                al2.Add(dt.Rows[i].ItemArray[4].ToString());

            if (al.Contains("Admin") || al.Contains("admin"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trAdmin.Visible = true;
                trCustomer.Visible = true;
                trNationalCoordinator.Visible = true;
                trChapterCoordinator.Visible = true;
                trRentalNeeds.Visible = true;
                trClusterCoordinator.Visible = true;
                trZonalCoordinator.Visible = true;
                trCustomer.Visible = true;
                trFinalCoordinator.Visible = true;
                trGeneralFunctions.Visible = true;
                trBeeFunction.Visible = true;
                trPersonal.Visible = true;
                trgamefunction.Visible = true;
                trTeamLead.Visible = true;
                trTeamMember.Visible = true;
                trTechnicalCoordinator.Visible = true;
                trBeeBook.Visible = true;
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = true;
                lnkPrepareFinals.Enabled = true;
                trCoachFunctions.Visible = true;
                trCoachFunctionsCoach.Visible = true;
                trWebPageMgmt.Visible = true;
                trMedalsCert.Visible = true;
                trExamReceiver.Visible = false;
                trFundRaising.Visible = true;
                trLaptopJudge.Visible = true;
                lnkRoomRequirement.Enabled = true;
                lnkRoomList.Enabled = true;
                lnkRentalNeeds.Enabled = true;
                lnkRoomSchedule.Enabled = true;
                lnkContestTeam.Enabled = true;
                lnkRoomGuides.Enabled = true;
                lnkGraders.Enabled = true;
                lnkAssignRolesFacilities.Enabled = true;
                TrNationalTechC.Visible = true;
                TrIndiaScholarships.Visible = true;
                Hyperlink87.Enabled = true;
                trWrkShop.Visible = true;
                trPrepClub.Visible = true;
                trSATFunctionsAdmin.Visible = true;
                trCoreTeamfunctions.Visible = true;
                //trSATFunctionsCoach.Visible = true;
                trExamDistributor.Visible = true;
                trOnlineWScal.Visible = true;
            }
            else if (al.Contains("CoachAdmin"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                trCoachFunctions.Visible = true;
                trCoachFunctionsCoach.Visible = true;
                trSATFunctionsAdmin.Visible = true;
                //trSATFunctionsCoach.Visible = true;
                trgamefunction.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trTeamLead.Visible = false;
            }
            else if (al.Contains("Coach"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                trCoachFunctionsCoach.Visible = true;
                trCoachFunctions.Visible = false;
                if (dt.Rows[0].ItemArray[4].ToString() == "Y")
                {
                    Hyperlink15.Enabled = true;
                    if (dt.Rows[0].ItemArray[10].ToString() == "Y")
                    {
                        trSATFunctionsAdmin.Visible = true;
                    }
                }
                else
                {
                    Hyperlink15.Enabled = false;
                    trSATFunctionsAdmin.Visible = false;
                }
                // 
                trgamefunction.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trTeamLead.Visible = false;
            }
            else if (al.Contains("CoachGlobal"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                trCoachFunctionsCoach.Visible = true;
                trCoachFunctions.Visible = true;
                trSATFunctionsAdmin.Visible = true;
              
                trgamefunction.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trTeamLead.Visible = false;
                trPersonal.Visible = true;
                trGeneralFunctions.Visible = true;
                trVolRecruit.Visible = false;
                Hyperlink15.Enabled = false;
                Hyperlink71.Enabled = false;
  trOnlineWScal.Visible = true;

            }
            else if (al.Contains("ITWeb") || al.Contains("WebAdmin"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                //Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                trWebPageMgmt.Visible = true;
                trgamefunction.Visible = false;
            }
            else if (al.Contains("Treasurer") || al.Contains("CoTreasurer"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trGeneralFunctions.Visible = false;
                trPersonal.Visible = true;
                trgamefunction.Visible = false;
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = true;
                Hyperlink87.Enabled = true;
            }
            else if (al.Contains("NationalC") || al.Contains("Nationalc") || al.Contains("nationalc"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trNationalCoordinator.Visible = true;
                trChapterCoordinator.Visible = true;
                trRentalNeeds.Visible = true;
                trClusterCoordinator.Visible = true;
                trZonalCoordinator.Visible = true;
                trCustomer.Visible = true;
                trFinalCoordinator.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = true;
                trTeamMember.Visible = true;
                trAccountingFunctions.Visible = true;
                trTechnicalCoordinator.Visible = true;
                trBeeBook.Visible = true;
                lnkPrepareFinals.Enabled = true;
                trCoachFunctions.Visible = true;
                trCoachFunctionsCoach.Visible = true;
                trMedalsCert.Visible = true;
                trFundRaising.Visible = true;
                trLaptopJudge.Visible = true;
                lnkRoomRequirement.Enabled = true;
                lnkRoomList.Enabled = true;
                lnkRentalNeeds.Enabled = true;
                lnkRoomSchedule.Enabled = true;
                lnkContestTeam.Enabled = true;
                lnkRoomGuides.Enabled = true;
                lnkGraders.Enabled = true;
                lnkAssignRolesFacilities.Enabled = true;
                TrNationalTechC.Visible = true;
                TrIndiaScholarships.Visible = true;
                Hyperlink87.Enabled = false;
                trWrkShop.Visible = true;
                trPrepClub.Visible = true;
                trSATFunctionsAdmin.Visible = true;
                // trSATFunctionsCoach.Visible = true;
                trExamDistributor.Visible = true;
                trOnlineWScal.Visible = true;
            }
            else if (al.Contains("Zonalc") || al.Contains("ZonalC") || al.Contains("zonalc"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                lblMessage.Text = "Go to ZonalC";
                trChapterCoordinator.Visible = true;
                trRentalNeeds.Visible = true;
                trClusterCoordinator.Visible = false;
                trZonalCoordinator.Visible = true;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                // updated by ferdine
                trTechnicalCoordinator.Visible = false;
                trgamefunction.Visible = false;
                lnkRoomList.Enabled = true;
                lnkRentalNeeds.Enabled = true;
                lnkRoomSchedule.Enabled = true;
                lnkContestTeam.Enabled = true;
                lnkRoomGuides.Enabled = true;
                lnkGraders.Enabled = true;
                lnkAssignRolesFacilities.Enabled = true;

                for (int i = 0; i <= DdlZonalCoordinator.Items.Count - 1; i++)
                {
                    if (DdlZonalCoordinator.Items[i].Text == dt.Rows[0].ItemArray[12].ToString())
                    {
                        DdlZonalCoordinator.SelectedIndex = i;
                    }
                }
                ddlChapterFundR.Items.Clear();
                ddlChapterFundR.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
                ddlChapterFundR.DataSource = ChapInZonesDS;
                ddlChapterFundR.DataTextField = "ChapterCode";
                ddlChapterFundR.DataValueField = "ChapterID";
                ddlChapterFundR.DataBind();

                ddlChapter.Items.Clear();
                ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
                ddlChapter.DataSource = ChapInZonesDS;
                ddlChapter.DataTextField = "ChapterCode";
                ddlChapter.DataValueField = "ChapterID";
                ddlChapter.DataBind();

                ddlChapter1.Items.Clear();
                ddlChapter1.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
                ddlChapter1.DataSource = ChapInZonesDS;
                ddlChapter1.DataTextField = "ChapterCode";
                ddlChapter1.DataValueField = "ChapterID";
                ddlChapter1.DataBind();

                fillChapterBasedOnRole(ddlChapter1);

                ddlChapterWkShop.Items.Clear();
                ddlChapterWkShop.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
                ddlChapterWkShop.DataSource = ChapInZonesDS;
                ddlChapterWkShop.DataTextField = "ChapterCode";
                ddlChapterWkShop.DataValueField = "ChapterID";
                ddlChapterWkShop.DataBind();

                ddlChapterPrepClub.Items.Clear();
                ddlChapterPrepClub.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
                ddlChapterPrepClub.DataSource = ChapInZonesDS;
                ddlChapterPrepClub.DataTextField = "ChapterCode";
                ddlChapterPrepClub.DataValueField = "ChapterID";
                ddlChapterPrepClub.DataBind();

                DdlZonalCoordinator.Enabled = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                //lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
                //lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
                //lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");

            }

            else if (al.Contains("FinalsCoreT") && al2.Contains("Y"))
            {
                DdlRoleCategory1.SelectedIndex = 1;
                DdlRoleCategory1.Enabled = false;
                Session["RoleCat"] = DdlRoleCategory1.SelectedItem.Value;
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trAdmin.Visible = true;
                trCustomer.Visible = true;
                trNationalCoordinator.Visible = true;
                trChapterCoordinator.Visible = true;
                trClusterCoordinator.Visible = true;
                trZonalCoordinator.Visible = true;
                trCustomer.Visible = true;
                trFinalCoordinator.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = true;
                trExamReceiver.Visible = false;
                trCoreTeamfunctions.Visible = false;
                trSATFunctionsAdmin.Visible = false;

            }
            else if (al.Contains("Clusterc") || al.Contains("clusterc") || al.Contains("ClusterC"))
            {

                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                lblMessage.Text = "Go to ClusterC";
                trChapterCoordinator.Visible = true;
                //trZonalCoordinator.Visible = false;
                trClusterCoordinator.Visible = true;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                Session["RoleId"] = 4;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                // Added on April 9th, 2012
                trAccountingFunctions.Visible = true;
                hlinkReqRefund.Enabled = false;
                Hyperlink72.Enabled = false;
                Hyperlink87.Enabled = false;
                Hyperlink34.Enabled = false;
                Hyperlink66.Enabled = false;
                Hyperlink56.Enabled = false;
                Hyperlink54.Enabled = false;
                hlinkReqRefund.Enabled = false;
                lnkDAS.Enabled = false;


                //Response.Write(Session["LoginRole"]);
                //Response.End();

                for (int i = 0; i <= DdlCluster.Items.Count - 1; i++)
                {
                    if (DdlCluster.Items[i].Text == dt.Rows[0].ItemArray[15].ToString().Trim())
                    {
                        DdlCluster.SelectedIndex = i;
                        Session["selClusterID"] = DdlCluster.SelectedItem.Value;
                    }

                }
                ddlChapter.Items.Clear();
                ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
                ddlChapter.DataSource = ChaWithinClustersDS;
                ddlChapter.DataTextField = "ChapterCode";
                ddlChapter.DataValueField = "ChapterID";
                ddlChapter.DataBind();

                DdlCluster.Enabled = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trExamReceiver.Visible = false;
                //Response.Write("Test");
            }
            else if (al.Contains("ChapterC") || al.Contains("chapterC") || al.Contains("chapterc"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                Session["SelChapterID"] = dt.Rows[0].ItemArray[8].ToString();

                trTechnicalCoordinator.Visible = false;
                trAccountingFunctions.Visible = true;
                trgamefunction.Visible = false;
                Hyperlink72.Enabled = false;
                Hyperlink87.Enabled = false;
                trZonalCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trChapterCoordinator.Visible = true;
                trRentalNeeds.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                lnkRoomList.Enabled = true;
                lnkRentalNeeds.Enabled = true;
                lnkRoomSchedule.Enabled = true;
                lnkContestTeam.Enabled = true;
                lnkRoomGuides.Enabled = true;
                lnkGraders.Enabled = true;
                lnkAssignRolesFacilities.Enabled = true;
                trWrkShop.Visible = true;
                trPrepClub.Visible = true;
                if (dt.Rows[0].ItemArray[8].ToString() == "1")
                {
                    trFinalCoordinator.Visible = true;
                    Session["EventID"] = 1;

                }
                else
                {
                    trFinalCoordinator.Visible = false;

                }

                //trWebPageMgmt.Visible = true;
                /*  for (int i = 0; i <= ddlChapter.Items.Count - 1; i++)
                  {
                      if (ChapterCode.IndexOf(ddlChapter.Items[i].Text) != -1)
                          ddlChapter.SelectedIndex = i;
                  }
                  for (int i = 0; i <= ddlChapter1.Items.Count - 1; i++)
                  {
                      if (ChapterCode.IndexOf(ddlChapter1.Items[i].Text) != -1)
                          ddlChapter1.SelectedIndex = i;
                  }*/

                for (int i = 0; i <= ddlChapter.Items.Count - 1; i++)
                {
                    if (ddlChapter.Items[i].Value == dt.Rows[0].ItemArray[8].ToString().Trim())
                        ddlChapter.SelectedIndex = i;
                }
                if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 46) || (Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
                {
                    fillChapterBasedOnRole(ddlChapter1);
                }
                else
                {
                    for (int i = 0; i <= ddlChapter1.Items.Count - 1; i++)
                    {
                        if (ddlChapter1.Items[i].Value == dt.Rows[0].ItemArray[8].ToString().Trim())
                            ddlChapter1.SelectedIndex = i;
                    }
                }
                for (int i = 0; i <= ddlChapterWkShop.Items.Count - 1; i++)
                {
                    if (ddlChapterWkShop.Items[i].Value == dt.Rows[0].ItemArray[8].ToString().Trim())
                        ddlChapterWkShop.SelectedIndex = i;
                }
                for (int i = 0; i <= ddlChapterPrepClub.Items.Count - 1; i++)
                {
                    if (ddlChapterPrepClub.Items[i].Value == dt.Rows[0].ItemArray[8].ToString().Trim())
                        ddlChapterPrepClub.SelectedIndex = i;
                }
                Session["selChapterName"] = ddlChapter.SelectedItem.Text;

                ddlChapter.Enabled = false;
                ddlChapter1.Enabled = false;
                ddlChapterWkShop.Enabled = false;
                ddlChapterPrepClub.Enabled = false;
                //Commented on 25-02-2013
                //string strSql;
                //strSql = "Select chapterid, chaptercode, state from chapter ";
                //strSql = strSql + " where clusterid in (Select clusterid from ";
                //strSql = strSql + " chapter where chapterid = " + Session["LoginChapterID"] + ")";
                //strSql = strSql + " order by state, chaptercode";
                //SqlConnection con = new SqlConnection(Application["ConnectionString"].ToString());
                //SqlDataReader drChapters = SqlHelper.ExecuteReader(con, CommandType.Text, strSql);
                //ddlChapter.Items.Clear();
                //ddlChapter1.Items.Clear();
                //while (drChapters.Read())
                //{
                //    ddlChapter.Items.Add(new ListItem(drChapters[1].ToString(), drChapters[0].ToString()));
                //    ddlChapter1.Items.Add(new ListItem(drChapters[1].ToString(), drChapters[0].ToString()));
                //}
                //ddlChapter.Enabled = false;
                // ddlChapter1.Enabled = false;

                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trExamReceiver.Visible = false;
                //lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
                //lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
                //lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");
            }
            else if (al.Contains("CustService"))
            {
                trCustomer.Visible = true;
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trCustomer.Visible = true;
                trZonalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trExamReceiver.Visible = false;
                trgamefunction.Visible = true;

            }
            else if (al.Contains("FundC"))
            {
                trFundRaising.Visible = true;
            }
            else if (al.Contains("ExamReceiver"))
            {

                Session["RoleId"] = 33;
                trZonalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trExamReceiver.Visible = false;
                //** Ferdine Modified 26/03/2010
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
            }
            else if (al.Contains("BeeBook"))
            {

                Session["RoleId"] = 41;
                trBeeBook.Visible = true;
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                trTechnicalCoordinator.Visible = false;
            }
            else if (al.Contains("ActgData"))// || al.Contains("ActgReview"))
            {
                trAccountingFunctions.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                //** Ferdine Modified 26/03/2010
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;

                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();

                if (dt.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(dt.Rows[0].ItemArray[8].ToString()) > 0 || dt.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (dt.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
            else if (al.Contains("ActgReview"))
            {
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                //** Ferdine Modified 26/03/2010
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;

                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();

                if (dt.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(dt.Rows[0].ItemArray[8].ToString()) > 0 || dt.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (dt.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
            else if (al.Contains("TechCGlobal"))
            {

                Session["RoleId"] = 8;
                trZonalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                //trTechnicalCoordinator.Visible = true;
                trExamReceiver.Visible = false;
                //** Ferdine Modified 27/03/2010
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;


            }
            else if (al.Contains("TechC"))
            {

                Session["RoleId"] = 9;
                trZonalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = true;
                trExamReceiver.Visible = false;
                trgamefunction.Visible = false;
                trLaptopJudge.Visible = false;
            }
            else if (al.Contains("RoomG"))
            {
                Session["RoleId"] = 23;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
            }
            else if (al.Contains("ExamDist"))
            {
                Session["RoleId"] = 32;
                trZonalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trExamReceiver.Visible = false;
                //trUploadTestPapers.Visible = true;
                trgamefunction.Visible = false;
                trExamDistributor.Visible = true;

            }
            else if (al.Contains("MedalsCert"))
            {
                Session["RoleId"] = 63;
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["SelChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                trZonalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trClusterCoordinator.Visible = false;
                trCustomer.Visible = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trExamReceiver.Visible = false;
                trBadgesCertificates.Visible = false;
                trgamefunction.Visible = false;
                trMedalsCert.Visible = true;
            }
            else if (al.Contains("MC"))
            {
                Session["RoleId"] = 22;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
            }
            else if (al.Contains("VenueC"))
            {
                //** Ferdine Modified 26/03/2010
                trTeamLead.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
            }
            else if (al.Contains("OWkshopC"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();

                trOnlineWScal.Visible = true;
                trPersonal.Visible = true;
                trGeneralFunctions.Visible = true;
            }
            else if (al.Contains("WkshopC"))
            {
                //** Ferdine Modified 19/01/2011
                Session["LoginRole"] = "11";
                Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                Session["RoleId"] = 11;
                txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                txtRoleCategory.Enabled = false;
                txtRoleCode.Text = "WkshopC";
                txtRoleCode.Enabled = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;// true;
                trTeamMember.Visible = false;
                trWrkShop.Visible = true;
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=11 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
                {
                    ddlChapterWkShop.Enabled = true;
                }
                else
                {
                    ddlChapterWkShop.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                    ddlChapterWkShop.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                    ddlChapterWkShop.Enabled = false;
                }
            }
            else if (al.Contains("PrepClubC"))
            {
                Session["LoginRole"] = "94";
                Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                Session["RoleId"] = 94;
                txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                txtRoleCategory.Enabled = false;
                txtRoleCode.Text = "PrepClubC";
                txtRoleCode.Enabled = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trWrkShop.Visible = false;
                trPrepClub.Visible = true;
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=94 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
                {
                    ddlChapterPrepClub.Enabled = true;
                }
                else
                {
                    ddlChapterPrepClub.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                    ddlChapterPrepClub.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                    ddlChapterPrepClub.Enabled = false;
                }
            }
            else if (al.Contains("Facilities"))
            {
                //** Added on 03/07/2012

                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                Session["SelChapterID"] = dt.Rows[0].ItemArray[8].ToString();

                trRentalNeeds.Visible = true;
                //Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();

                if (dt.Rows[0].ItemArray[8].ToString() != String.Empty)
                {
                    if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
                    {
                        fillChapterBasedOnRole(ddlChapter1);
                    }
                    else
                    {
                        ddlChapter1.SelectedItem.Text = dt.Rows[0].ItemArray[9].ToString();
                        ddlChapter1.SelectedValue = dt.Rows[0].ItemArray[8].ToString();
                        ddlChapter1.Enabled = false;
                    }
                }
                else
                {
                    int FacCount;
                    String str = "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1  and MemberID=" + Session["LoginID"] + "";// and RoleId=" + Session["LoginID"].ToString() + "";
                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                    FacCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, str));

                    if (FacCount > 0)
                    {
                        ddlChapter1.SelectedValue = "1";// DT.Rows[0].ItemArray[8].ToString();
                        ddlChapter1.Enabled = false;
                    }

                }
                trRentalNeeds.Visible = true;
                lnkRoomRequirement.Enabled = true;
                lnkRoomList.Enabled = true;
                lnkRentalNeeds.Enabled = true;
                lnkRoomSchedule.Enabled = true;
                lnkContestTeam.Enabled = true;
                lnkRoomGuides.Enabled = true;
                lnkGraders.Enabled = true;
                lnkAssignRolesFacilities.Enabled = true;
                lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");

                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trgamefunction.Visible = false;
                trTeamLead.Visible = false;
                trTechnicalCoordinator.Visible = false;

            }
            else if (al.Contains("JudgeLaptop")) //Laptop or Chief
            {
                trLaptopJudge.Visible = true;
                trgamefunction.Visible = false;
                trTeamMember.Visible = false;
                trTeamLead.Visible = false;
                trTechnicalCoordinator.Visible = false;
                Session["RoleId"] = 18;
            }
            else if (al.Contains("JudgeChief")) //Laptop or Chief
            {
                trLaptopJudge.Visible = true;
                trgamefunction.Visible = false;
                trTeamMember.Visible = false;
                trTeamLead.Visible = false;
                trTechnicalCoordinator.Visible = false;
                Session["RoleId"] = 16;
            }
            else if (al.Contains("GameCust")) //game
            {
                trgamefunction.Visible = true;
            }
            else if (al.Contains("CorpMatch"))
            {
                Session["RoleId"] = 92;
                trAccountingFunctions.Visible = true;
                Hyperlink72.Enabled = true;
                hlinkReqRefund.Enabled = false;
                Hyperlink87.Enabled = false;
                Hyperlink34.Enabled = false;
                Hyperlink66.Enabled = false;
                Hyperlink144.Enabled = false;
                Hyperlink136.Enabled = false;
                if (dt.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trTeamMember.Visible = false;
                    trTeamLead.Visible = false;
                    trTechnicalCoordinator.Visible = false;
                }
            }
            else if (al.Contains("NatTechT"))
            {
                //Session["RoleId"] = 93;
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                //Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                //Session["SelChapterID"] = dt.Rows[0].ItemArray[8].ToString();

                trAdmin.Visible = false;
                trCustomer.Visible = false;
                trNationalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trRentalNeeds.Visible = false;
                trClusterCoordinator.Visible = false;
                trZonalCoordinator.Visible = false;
                trCustomer.Visible = false;
                trFinalCoordinator.Visible = false;
                trgamefunction.Visible = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trBeeBook.Visible = false;
                trAccountingFunctions.Visible = false;
                trTreasuryFunctions.Visible = false;
                lnkPrepareFinals.Enabled = false;
                trCoachFunctions.Visible = false;
                trCoachFunctionsCoach.Visible = false;
                trWebPageMgmt.Visible = false;
                trMedalsCert.Visible = false;
                trExamReceiver.Visible = false;
                trFundRaising.Visible = false;
                trLaptopJudge.Visible = false;
                trRentalNeeds.Visible = false;

                TrNationalTechC.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trCoreTeamfunctions.Visible = true;
                lnkSBVBSelMatrixCoreT.Enabled = false;
                hylnkGenSBVBTestPaperCoreT.Enabled = false;
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Count(*) From Volunteer Where MemberID=" + Convert.ToInt32(Session["LoginID"]) + " and TeamLead='Y' and RoleId in(93)")) > 0)
                {
                    Hyperlink152.Enabled = true;
                }
                else //if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Count(*) From Volunteer Where MemberID=" + Convert.ToInt32(Session["LoginID"]) + " and TeamLead='N' ")) > 0)
                {
                    Hyperlink152.Enabled = false;
                }
            }
            else if (al.Contains("LiaisonIndia"))
            {
                TrIndiaScholarships.Visible = true;
            }
            else if (al.Contains("CollegeSch"))
            {
                TrIndiaScholarships.Visible = true;
            }
            else if (al.Contains("DesigSch"))
            {
                TrIndiaScholarships.Visible = true;
            }
            else if (al.Contains("CoreT") || al.Contains("coreT") || al.Contains("coret"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trNationalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trRentalNeeds.Visible = false;
                trClusterCoordinator.Visible = false;
                trZonalCoordinator.Visible = false;
                trCustomer.Visible = false;
                trFinalCoordinator.Visible = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trAccountingFunctions.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trBeeBook.Visible = false;
                lnkPrepareFinals.Enabled = false;
                trCoachFunctions.Visible = false;
                trCoachFunctionsCoach.Visible = false;
                trMedalsCert.Visible = false;
                trFundRaising.Visible = false;
                trLaptopJudge.Visible = false;
                lnkRoomRequirement.Enabled = false;
                lnkRoomList.Enabled = false;
                lnkRentalNeeds.Enabled = false;
                lnkRoomSchedule.Enabled = false;
                lnkContestTeam.Enabled = false;
                lnkRoomGuides.Enabled = false;
                lnkGraders.Enabled = false;
                lnkAssignRolesFacilities.Enabled = false;
                TrNationalTechC.Visible = false;
                TrIndiaScholarships.Visible = false;
                Hyperlink87.Enabled = false;
                trWrkShop.Visible = false;
                trPrepClub.Visible = false;

                trSATFunctionsAdmin.Visible = true;
                trGeneralFunctions.Visible = true;
                trPersonal.Visible = true;
                trCoreTeamfunctions.Visible = true;
                // trSATFunctionsCoach.Visible = true;
            }
            else if (al.Contains("ITTest"))
            {
                Session["LoginRole"] = dt.Rows[0].ItemArray[3].ToString();
                Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                trNationalCoordinator.Visible = true;
                trChapterCoordinator.Visible = true;
                trRentalNeeds.Visible = true;
                trClusterCoordinator.Visible = true;
                trZonalCoordinator.Visible = true;
                trCustomer.Visible = true;
                trFinalCoordinator.Visible = true;
                trTeamLead.Visible = true;
                trTeamMember.Visible = true;
                trAccountingFunctions.Visible = false;
                trTechnicalCoordinator.Visible = true;
                trBeeBook.Visible = true;
                lnkPrepareFinals.Enabled = true;
                trWebPageMgmt.Visible = true;
                trMedalsCert.Visible = true;
                trFundRaising.Visible = true;
                trLaptopJudge.Visible = true;
                lnkRoomRequirement.Enabled = true;
                lnkRoomList.Enabled = true;
                lnkRentalNeeds.Enabled = true;
                lnkRoomSchedule.Enabled = true;
                lnkContestTeam.Enabled = true;
                lnkRoomGuides.Enabled = true;
                lnkGraders.Enabled = true;
                lnkAssignRolesFacilities.Enabled = true;
                TrNationalTechC.Visible = true;
                TrIndiaScholarships.Visible = false;
                Hyperlink87.Enabled = false;
                trWrkShop.Visible = true;
                trPrepClub.Visible = true;
                trBeeFunction.Visible = true;
                trExamDistributor.Visible = true;
                trOnlineWScal.Visible = true;
                //trCoachFunctions.Visible = false;
                //trCoachFunctionsCoach.Visible = false;
                trgamefunction.Visible = true;
                trGeneralFunctions.Visible = false;
                trPersonal.Visible = false;

            }
            else
            {
                trNationalCoordinator.Visible = false;
                trChapterCoordinator.Visible = false;
                trRentalNeeds.Visible = false;
                trClusterCoordinator.Visible = false;
                trZonalCoordinator.Visible = false;
                trCustomer.Visible = false;
                trFinalCoordinator.Visible = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trAccountingFunctions.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trBeeBook.Visible = false;
                lnkPrepareFinals.Enabled = false;
                trCoachFunctions.Visible = false;
                trCoachFunctionsCoach.Visible = false;
                trMedalsCert.Visible = false;
                trFundRaising.Visible = false;
                trLaptopJudge.Visible = false;
                TrNationalTechC.Visible = false;
                TrIndiaScholarships.Visible = false;
                trWrkShop.Visible = false;
                trPrepClub.Visible = false;
                trSATFunctionsAdmin.Visible = false;
                trGeneralFunctions.Visible = false;
                trPersonal.Visible = false;
                trCoreTeamfunctions.Visible = false;

                if (string.IsNullOrEmpty(dt.Rows[0].ItemArray[2].ToString()))
                {
                    trPersonal.Visible = true;
                }
                else
                {
                    Session["RoleId"] = dt.Rows[0].ItemArray[2].ToString();
                    trGeneralFunctions.Visible = true;
                    trPersonal.Visible = true;
                }

                lblMessage.Text = "No Menus are Defined Yet"; //"No roles are defined for you, please contact at volunteer@northsouth.org for further assistance.";
                lblMessage.Visible = true;
            }

            ViewTeamLeadOrMember(al2.Contains("Y"));

        }
        if (dt.Rows.Count == 0)
        {

            //  trZonalCoordinator.Visible = false;

            //BtnContinue.Visible = true;
            table1.Visible = true;
            // Done on August 16, 2012 as per Dr.C's PPT by ferdine
            //

            lblMessage.Text = "No role was assigned to you.  Please contact  the person you are in contact with.  Otherwise, please send an email to <a href='mailto:nsfcontests@northsouth.org' target='_blank' >nsfcontests@gmail.com</a> with details.";
            //"No roles are defined for you, please contact at volunteer@northsouth.org for further assistance.";
            lblMessage.Visible = true;
            trGeneralFunctions.Visible = false;

            DdlRole.Visible = false;
        }

        if (dt.Rows.Count > 1)
        {
            DdlRole.Visible = true;

            //String StrSQ = "SELECT V.VolunteerID,CASE WHEN V.RoleId>5 THEN CASE WHEN V.[National] = 'Y' Or V.Finals = 'Y' THEN V.RoleCode+'_National' ELSE CASE WHEN Ch.ChapterCode is Null then V.RoleCode Else V.RoleCode+'_'+REPLACE(REPLACE(Ch.ChapterCode,',','_'),' ','') End END ELSE V.RoleCode END RoleCode";
            //String StrSQ = "SELECT V.VolunteerID,CASE WHEN V.RoleId>5 THEN CASE WHEN V.[National] = 'Y' Or V.Finals = 'Y' THEN V.RoleCode + CASE WHEN V.[National] ='Y' THEN '_National' ELSE '_Finals' END ELSE CASE WHEN Ch.ChapterCode is Null then V.RoleCode Else V.RoleCode+'_'+REPLACE(REPLACE(Ch.ChapterCode,',','_'),' ','') End END ELSE V.RoleCode END RoleCode";
            //StrSQ = StrSQ + " FROM Volunteer V Left Join Chapter Ch On V.ChapterID = Ch.ChapterID where V.MemberiD = " + ID;
            String StrSQ = "SELECT Distinct V.VolunteerID,CASE WHEN V.RoleId>2 THEN CASE WHEN V.[National] = 'Y' Or V.Finals = 'Y' THEN V.RoleCode + CASE WHEN V.[National] ='Y' THEN '_National' ";
            StrSQ = StrSQ + " ELSE '_Finals' END ELSE CASE WHEN Ch.ChapterCode is Null then CASE When V.ClusterCode is null Or V.ClusterCode ='' Then ";
            StrSQ = StrSQ + " CASE WHEN V.ZoneCode IS null Or V.ZoneCode ='' THEN V.RoleCode ELSE V.RoleCode +'_'+''+ RTRIM(LTRIM(V.ZoneCode)) +'' END	";
            StrSQ = StrSQ + " Else V.RoleCode +'_'+ V.ClusterCode End Else V.RoleCode+'_'+REPLACE(REPLACE(Ch.ChapterCode,',','_'),' ','') End END ELSE V.RoleCode END RoleCode";
            StrSQ = StrSQ + " FROM Volunteer V Left Join Chapter Ch On V.ChapterID = Ch.ChapterID where V.MemberiD =" + ID;

            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQ);
            DdlRole.DataSource = ds;
            DdlRole.DataTextField = "RoleCode";
            DdlRole.DataValueField = "VolunteerID";
            DdlRole.DataBind();
            BtnContinue.Visible = true;
            table1.Visible = false;
            if (Session["RoleCode"] != null)
            {
                DdlRole.SelectedValue = Session["RoleCode"].ToString();
            }
            // code changed by Michael... for the purpose of maintaining roles while redirecting pages
            if (Session["RoleCode"] != null)
            {
                int WalkCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(eventID) from Event where EventId=5 and Status = 'O'"));
                if (WalkCount > 0)
                {
                    hlinkWalkathon.Enabled = true;
                }
                RoleFunction();
                hidelinksbasedonRoleID();
                hideSBVBTestPaperLink();
                visibleVolSignReportlinks();

                if (Session["RoleID"] != null)
                {
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96")
                    {
                        PWebConfControl.Visible = true;
                    }
                    else
                    {
                        PWebConfControl.Visible = false;
                    }
                }
                BtnContinue.Visible = true;
                BtnContinue.Enabled = true;
                DdlRole.Enabled = true;
                table1.Visible = true;
            }
        }



    }

    protected void lbtnVol_Click(object sender, EventArgs e)
    {
        // Session["SelZoneID"] = Session["SelClusterID"] = Session["SelChapterID"] = Session["SelCustIndID"] = null;
        if (ddlChapter.SelectedIndex > 0)
        {
            Session["SelChapterID"] = Session["LoginChapterID"] = ddlChapter.SelectedValue.ToString();
            Response.Redirect("ChapterCoordinator/ChapterFunctions.aspx");
            // Response.Redirect("ChapVolunteerRoles.aspx");
        }
        else Response.Redirect("Admin/VolunteerSearch.aspx");
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdlCluster.SelectedIndex != 0)
        {
            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChaWithinClustersDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();

        }
        else
        {
            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChapInZonesDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();

        }
    }
    protected void DdlZonalCoordinator_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdlZonalCoordinator.SelectedIndex != 0)
        {

            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChapInZonesDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();

            ddlChapter1.Items.Clear();
            ddlChapter1.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter1.DataSource = ChapInZonesDS;
            ddlChapter1.DataTextField = "ChapterCode";
            ddlChapter1.DataValueField = "ChapterID";
            ddlChapter1.DataBind();

            fillChapterBasedOnRole(ddlChapter1);

            ddlChapterWkShop.Items.Clear();
            ddlChapterWkShop.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapterWkShop.DataSource = ChapInZonesDS;
            ddlChapterWkShop.DataTextField = "ChapterCode";
            ddlChapterWkShop.DataValueField = "ChapterID";
            ddlChapterWkShop.DataBind();

            ddlChapterPrepClub.Items.Clear();
            ddlChapterPrepClub.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapterPrepClub.DataSource = ChapInZonesDS;
            ddlChapterPrepClub.DataTextField = "ChapterCode";
            ddlChapterPrepClub.DataValueField = "ChapterID";
            ddlChapterPrepClub.DataBind();

        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        Session["EventID"] = 2;
        // get and store EventYear from database
        int EventYear = 0;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        SqlParameter param = new SqlParameter("@EventID", Session["EventID"]);
        EventYear = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear_Volunteer", param));
        Session["EventYear"] = EventYear;

        if (EventYear > 1 && DateTime.Today.Month >= 10)
        {
            Session["EventYear"] = DateTime.Today.Year + 1;
        }
        else if (EventYear < 1)
        {
            Session["EventYear"] = DateTime.Today.Year;
        }

        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
        //Response.Redirect("ChapterCoordinator/EventsAndProducts.aspx");
        Response.Redirect("AddUpdateContests.aspx");
    }

    protected void lnkContestTiming_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;

        Response.Redirect("ContestTimings.aspx");
    }
    protected void lnkContestSchedule_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;

        Response.Redirect("ContestSchedule.aspx");
    }
    protected void lnkTeamPlanning_Click(object sender, EventArgs e)
    {
        Session["LoginTPChapterID"] = ddlChapter1.SelectedItem.Value;

        Response.Redirect("TeamPlanning.aspx");
    }

    protected void lnkEventContestSchedule_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter1.SelectedItem.Value;

        Response.Redirect("ContestSchedule.aspx");
    }
    protected void lbtnExp_Click(object sender, EventArgs e)
    {

        Session["EventID"] = 2;
        // get and store EventYear from database
        int EventYear = 0;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        SqlParameter param = new SqlParameter("@EventID", Session["EventID"]);
        EventYear = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetOpenEventYear_Volunteer", param));
        Session["EventYear"] = EventYear;

        if (EventYear < 1 && DateTime.Today.Month > 11)
        {
            Session["EventYear"] = DateTime.Today.Year + 1;
        }

        else
        {
            Session["EventYear"] = DateTime.Today.Year;
        }

        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
        if ((Convert.ToInt32(Session["LoginChapterID"]) == 1) && ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2)))
            Session["EventID"] = 1;
        Response.Redirect("ExceptContestant.aspx");
    }
    protected void lnkAssignRoles_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;

        Response.Redirect("VolunteerAssignRoles.aspx");
    }
    protected void lnkAssignRolesZonal_Click(object sender, EventArgs e)
    {
        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;

        Response.Redirect("VolunteerAssignRoles.aspx");
    }
    protected void lnkMissingScoresZonal_Click(object sender, EventArgs e)
    {
        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;

        Response.Redirect("MissingScores.aspx");
    }
    protected void lnkAssignRolesCluster_Click(object sender, EventArgs e)
    {
        Session["selClusterID"] = DdlCluster.SelectedItem.Value;

        Response.Redirect("VolunteerAssignRoles.aspx");
    }
    protected void lnkMissingScoreCluster_Click(object sender, EventArgs e)
    {
        Session["selClusterID"] = DdlCluster.SelectedItem.Value;

        Response.Redirect("MissingScores.aspx");
    }
    protected void lnlEmail1_Click(object sender, EventArgs e)
    {
        RedirectToEmail();
    }
    protected void List2_SelectedIndexChanged(object sender, EventArgs e)
    {

        RedirectToEmail();
    }
    protected void lnkEmail4_Click(object sender, EventArgs e)
    {
        Session["Panel"] = "ClusterPanel";
        if (DdlCluster.SelectedIndex > 0)
        {
            //SqlHelper.ExecuteScalar
            Session["SmLogID"] = DdlCluster.SelectedItem.Value;
            try
            {
                string mChapterID = Convert.ToString(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Indspouse where automemberid=" + Session["LoginID"].ToString()));
                Session["mailChapterID"] = mChapterID;
            }
            catch (Exception ex) { }


        }
        RedirectToEmail();
    }
    protected void lnkEmail5_Click(object sender, EventArgs e)
    {
        Session["Panel"] = "ChapterPanel";
        if (ddlChapter.SelectedIndex > 0)
        {
            Session["mailChapterID"] = ddlChapter.SelectedItem.Value;
        }
        RedirectToEmail();
    }
    protected void AssignPanelName(object sender, EventArgs e)
    {
        Session["Panel"] = "ChapterPanel";
        if (ddlChapter.SelectedIndex > 0)
        {
            Session["mailChapterID"] = ddlChapter.SelectedItem.Value;
        }
        Response.Redirect("VolunteerSignupReport.aspx");
    }
    protected void lnkEmail3_Click1(object sender, EventArgs e)
    {
        Session["Panel"] = "ZonalPanel";
        if (DdlZonalCoordinator.SelectedIndex > 0)
        {

            try
            {
                string mChapterID = Convert.ToString(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Indspouse where automemberid=" + Session["LoginID"].ToString()));
                Session["mailChapterID"] = mChapterID;
            }
            catch (Exception ex) { }
        }
        RedirectToEmail();
    }
    protected void lnkZoneDonorList_Click(object sender, EventArgs e)
    {
        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;
        Response.Redirect("DonorList.aspx?id=3");
    }
    protected void lnkClustDonorlist_Click(object sender, EventArgs e)
    {
        Session["selClusterID"] = DdlCluster.SelectedItem.Value;
        Response.Redirect("DonorList.aspx?id=4");
    }
    protected void lnkChDonorList_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;
        Response.Redirect("DonorList.aspx?id=5");
    }
    protected void lnkMedalList_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;
        Response.Redirect("Reports/AllContestsWeeklyList.aspx?id=1");
    }
    protected void lnkSupList_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;
        Response.Redirect("SuppressionList.aspx");
    }

    protected void lnkSupListHandling_Click(object sender, EventArgs e)
    {
        Session["supListFromTreasury"] = "Y";

    }
    protected void lnkChWebPageMgmt_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;
        Response.Redirect("WebPageMgmtMain.aspx");
    }
    protected void lnkFundRaisingCal_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;
        Session["selChapterName"] = ddlChapter.SelectedItem.Text;
        Response.Redirect("FundRaisingCal.aspx");
    }
    protected void lnkFundRaisingCalFunc_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
        Response.Redirect("FundRaisingCal.aspx");
    }

    private void RedirectToEmail()
    {

        if (DdlZonalCoordinator.SelectedIndex >= 0)
        {
            Session["LoginZoneID"] = DdlZonalCoordinator.SelectedItem.Value;
            Session["LoginZoneName"] = DdlZonalCoordinator.SelectedItem.Text;
        }
        if (ddlChapter.SelectedIndex >= 0)
        {
            Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
            Session["LoginChapterName"] = ddlChapter.SelectedItem.Text;
        }
        if (DdlCluster.SelectedIndex >= 0)
        {
            Session["LoginClusterID"] = DdlCluster.SelectedItem.Value;
            Session["LoginClusterName"] = DdlCluster.SelectedItem.Text;
        }
        Response.Redirect("Email_CC.aspx");

    }
    protected void LinkButton1_Click1(object sender, EventArgs e)
    {
        RedirectToEmail();
    }
    protected void lnkAssignRole_Click(object sender, EventArgs e)
    {
        if (DdlRoleCategory1.SelectedItem.Value == "Finals")
        {
            Response.Redirect("VolunteerAssignRoles.aspx");
        }
        else
        {

        }
    }
    protected void lnkContactlst_Click(object sender, EventArgs e)
    {
        RedirectToContactList();
    }
    protected void lnkUnassignlnk_Click(object sender, EventArgs e)
    {
        RedirectToUnAssignedContactList();
    }
    protected void lnkContactlst1_Click(object sender, EventArgs e)
    {

        RedirectToContactList();
    }
    protected void lnkUnassignedCntLst1_Click(object sender, EventArgs e)
    {

        RedirectToUnAssignedContactList();
    }

    private void RedirectToContactList()
    {
        Response.Redirect("CoachClassContactList.aspx");
    }

    private void RedirectToUnAssignedContactList()
    {
        //set session variables for selected zone, cluster and chapter
        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;

        Session["selClusterID"] = DdlCluster.SelectedItem.Value;

        Session["selChapterID"] = ddlChapter.SelectedItem.Value;

        Response.Redirect("ContactListUnassigned.aspx");
    }


    private void RedirectToViewRegistrations()
    {
        //set session variables for selected zone, cluster and chapter
        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;

        Session["selClusterID"] = DdlCluster.SelectedItem.Value;

        Session["selChapterID"] = ddlChapter.SelectedItem.Value;

        Response.Redirect("./Reports/tmcList.aspx");
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {

    }

    public void RoleFunction()
    {


        table1.Visible = true;
        string RoleID = string.Empty;
        string TeamLead = string.Empty;
        //// VRegistration.NSFMasterPage nsfMaster =  (VRegistration.NSFMasterPage)this.Master;
        ////// nsfMaster.addMenuItemAt(2, new MenuItem("Back", "Back", "volunteerfunctions.aspx"));
        //// nsfMaster.addBackMenuItem("volunteerfunctions.aspx");
        GetVolunteerTableAdapters.VolunteerTableAdapter VT = new GetVolunteerTableAdapters.VolunteerTableAdapter();
        DataTable DT = new DataTable();
        DT = VT.GetVolunterData(Convert.ToInt32(DdlRole.SelectedItem.Value));
        RoleID = DT.Rows[0].ItemArray[2].ToString();
        TeamLead = DT.Rows[0].ItemArray[4].ToString();
        Session["TL"] = TeamLead.ToString();
        // Voluntter with CustomerService Role.

        if (DdlRole.SelectedItem.Text.Contains("CustService"))
        {
            trCustomer.Visible = true;
            trTeamMember.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = true;

        }
        // Voluntter with BeeBook Role.
        if (DdlRole.SelectedItem.Text.Contains("BeeBook"))
        {
            Session["RoleId"] = 41;
            trBeeBook.Visible = true;
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trTechnicalCoordinator.Visible = false;

        }


        if (DdlRole.SelectedItem.Text.Contains("ITWeb"))
        {

            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 43;
            //Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
            trWebPageMgmt.Visible = true;
            trgamefunction.Visible = false;
        }

        if (DdlRole.SelectedItem.Text.Contains("WebAdmin"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 90;
            //Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
            trWebPageMgmt.Visible = true;
            trgamefunction.Visible = false;
        }

        // Voluntter with BeeBook Role.
        if (DdlRole.SelectedItem.Text.Contains("ChapterC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["SelChapterID"] = DT.Rows[0].ItemArray[8].ToString();

            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = false;
            Hyperlink72.Enabled = false;
            Hyperlink87.Enabled = false;
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trAccountingFunctions.Visible = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            trWrkShop.Visible = true;
            trPrepClub.Visible = true;
            //trWebPageMgmt.Visible = true;

            if (DT.Rows[0].ItemArray[8].ToString() == "1")
            {
                trFinalCoordinator.Visible = true;
                Session["EventID"] = 1;
            }
            else
            {
                trFinalCoordinator.Visible = false;
            }
            for (int i = 0; i <= ddlChapter.Items.Count - 1; i++)
            {
                if (ddlChapter.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                    ddlChapter.SelectedIndex = i;
            }
            if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
            {
                fillChapterBasedOnRole(ddlChapter1);
            }
            else
            {
                for (int i = 0; i <= ddlChapter1.Items.Count - 1; i++)
                {

                    if (ddlChapter1.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                        ddlChapter1.SelectedIndex = i;
                }
            }
            for (int i = 0; i <= ddlChapterWkShop.Items.Count - 1; i++)
            {

                if (ddlChapterWkShop.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                    ddlChapterWkShop.SelectedIndex = i;
            }
            for (int i = 0; i <= ddlChapterPrepClub.Items.Count - 1; i++)
            {

                if (ddlChapterPrepClub.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                    ddlChapterPrepClub.SelectedIndex = i;
            }

            Session["selChapterName"] = ddlChapter.SelectedItem.Text;

            ddlChapter.Enabled = false;
            ddlChapter1.Enabled = false;
            ddlChapterWkShop.Enabled = false;
            ddlChapterPrepClub.Enabled = false;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;

            // lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
            // lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
            //  lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");
        }


        // Voluntter with  RoleID > 5  and if he is a Team Lead. 
        if (DT.Rows[0].ItemArray[4].ToString() == "Y" && Convert.ToInt32(RoleID) > 5)
        {

            if (DT.Rows[0].ItemArray[10].ToString() == "Y")
            {

                txtRoleCategory.Text = "National";
                txtRoleCategory.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[13].ToString() == "Y")
            {

                txtRoleCategory.Text = "Finals";
                txtRoleCategory.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[16].ToString() == "Y")
            {

                txtRoleCategory.Text = "IndiaChapter";
                txtRoleCategory.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[12].ToString() != "")
            {

                txtRoleCategory.Text = DT.Rows[0].ItemArray[12].ToString();
                txtRoleCategory.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[15].ToString() != "")
            {
                txtRoleCategory.Text = DT.Rows[0].ItemArray[15].ToString();
                txtRoleCategory.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[9].ToString() != "")
            {

                txtRoleCategory.Text = DT.Rows[0].ItemArray[9].ToString();
                txtRoleCategory.Enabled = false;
            }

            //RoleID > 5 can see Teammemeber panel

            txtRoleCode.Text = DT.Rows[0].ItemArray[3].ToString();
            txtRoleCode.Enabled = false;
            trTeamLead.Visible = true;
            trTeamMember.Visible = false;
            BtnContinue.Enabled = false;
            DdlRole.Enabled = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

            if (Convert.ToInt32(RoleID) != 29)
                trTechnicalCoordinator.Visible = true;
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();

            //Role ID 37 & 38 can only see Accounting Panel.
            if (DdlRole.SelectedItem.Text.Contains("ActgData"))// || DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                Hyperlink72.Enabled = false;
                //Hyperlink87.Enabled = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
            if (DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = false;
                //  Hyperlink72.Enabled = false;
                //Hyperlink87.Enabled = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trTreasuryFunctions.Visible = true;
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }
            }
        }
        if (Convert.ToInt32(RoleID) > 5 && DT.Rows[0].ItemArray[4].ToString() == "N")
        {
            if (DT.Rows[0].ItemArray[10].ToString() == "Y")
            {

                txtRoleCategory2.Text = "National";

                txtRoleCategory2.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[13].ToString() == "Y")
            {

                txtRoleCategory2.Text = "Finals";

                txtRoleCategory2.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[16].ToString() == "Y")
            {

                txtRoleCategory2.Text = "IndiaChapter";
                txtRoleCategory2.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[12].ToString() != "")
            {
                txtRoleCategory2.Text = DT.Rows[0].ItemArray[12].ToString();

                txtRoleCategory2.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[15].ToString() != "")
            {
                txtRoleCategory2.Text = DT.Rows[0].ItemArray[15].ToString();
                txtRoleCategory2.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[9].ToString() != "")
            {

                txtRoleCategory2.Text = DT.Rows[0].ItemArray[9].ToString();
                txtRoleCategory2.Enabled = false;
            }
            txtRoleCode2.Text = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            DdlRole.Enabled = false;
            trTechnicalCoordinator.Visible = true;
            if (Convert.ToInt32(Session["RoleId"]) == 16 || Convert.ToInt32(Session["RoleId"]) == 18)
            {
                trLaptopJudge.Visible = true;
            }
            if (Convert.ToInt32(Session["RoleId"]) == 18)
            {
                lbtnTechCDnloadTstpapers.Enabled = false;
            }
            else
            {
                lbtnTechCDnloadTstpapers.Enabled = true;
            }
            txtRoleCode2.Enabled = false;
            trTeamMember.Visible = true;
            BtnContinue.Enabled = false;

            //Role ID 37 & 38 can only see Accounting Panel.
            if (DdlRole.SelectedItem.Text.Contains("ActgData"))// || DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trTreasuryFunctions.Visible = false;

                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trTreasuryFunctions.Visible = true;
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
            if (DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = true;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;

                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
        }

        if (DdlRole.SelectedItem.Text.Contains("Treasurer") || DdlRole.SelectedItem.Text.Contains("Co-Treasurer"))
        {

            trGeneralFunctions.Visible = false;
            trgamefunction.Visible = false;
            trPersonal.Visible = true;
            trAccountingFunctions.Visible = true;
            trTreasuryFunctions.Visible = true;
            Hyperlink87.Enabled = true;
        }

        if (DdlRole.SelectedItem.Text.Contains("CustomerC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trCustomer.Visible = true;
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = true;
        }

        if ((DdlRole.SelectedItem.Text.Contains("Clusterc")) || (DdlRole.SelectedItem.Text.Contains("ClusterC")))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            lblMessage.Text = "Go to ClusterC";
            trChapterCoordinator.Visible = true;
            //trZonalCoordinator.Visible = false;
            trClusterCoordinator.Visible = true;
            trCustomer.Visible = false;

            trTeamLead.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            // Added on April 9th, 2012
            trAccountingFunctions.Visible = true;
            hlinkReqRefund.Enabled = false;
            Hyperlink72.Enabled = false;
            Hyperlink87.Enabled = false;
            Hyperlink34.Enabled = false;
            Hyperlink66.Enabled = false;
            Hyperlink56.Enabled = false;
            Hyperlink54.Enabled = false;
            hlinkReqRefund.Enabled = false;
            lnkDAS.Enabled = false;


            Hyperlink34.Enabled = false;
            hlinkReqRefund.Visible = false;
            Hyperlink72.Enabled = false;
            Hyperlink136.Enabled = false;
            HyperLink6.Enabled = false;
            Hyperlink66.Enabled = false;

            for (int i = 0; i <= DdlCluster.Items.Count - 1; i++)
            {
                if (DdlCluster.Items[i].Text.Trim() == DT.Rows[0].ItemArray[15].ToString().Trim())
                {
                    DdlCluster.SelectedIndex = i;
                    Session["selClusterID"] = DdlCluster.SelectedItem.Value;
                }
            }
            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChaWithinClustersDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();
            DdlCluster.Enabled = false;
            DdlRole.Enabled = false;
            trTeamMember.Visible = false;
            BtnContinue.Enabled = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = false;
        }

        if (DdlRole.SelectedItem.Text.Contains("FinalsCoreT") && DT.Rows[0].ItemArray[4].ToString() == "Y")
        {
            DdlRoleCategory1.SelectedIndex = 2;
            DdlRoleCategory1.Enabled = false;
            DDlRoleCode.SelectedIndex = 22;
            DDlRoleCode.Enabled = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trAdmin.Visible = false;
            trCustomer.Visible = false;
            trNationalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = true;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = true;
            trCoreTeamfunctions.Visible = false;
            trSATFunctionsAdmin.Visible = false;
        }


        if (DdlRole.SelectedItem.Text.Contains("ZonalC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            lblMessage.Text = "Go to ZonalC";
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = true;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;





            trAccountingFunctions.Visible = true;
            Hyperlink34.Enabled = false;
            hlinkReqRefund.Visible = false;

            Hyperlink72.Enabled = false;
            Hyperlink136.Enabled = false;
            HyperLink6.Enabled = false;
            Hyperlink66.Enabled = false;


            for (int i = 0; i <= DdlZonalCoordinator.Items.Count - 1; i++)
            {
                if (DdlZonalCoordinator.Items[i].Text == DT.Rows[0].ItemArray[12].ToString())
                {
                    DdlZonalCoordinator.SelectedIndex = i;
                }

            }

            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChapInZonesDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();

            ddlChapter1.Items.Clear();
            ddlChapter1.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter1.DataSource = ChapInZonesDS;
            ddlChapter1.DataTextField = "ChapterCode";
            ddlChapter1.DataValueField = "ChapterID";
            ddlChapter1.DataBind();

            fillChapterBasedOnRole(ddlChapter1);

            ddlChapterWkShop.Items.Clear();
            ddlChapterWkShop.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapterWkShop.DataSource = ChapInZonesDS;
            ddlChapterWkShop.DataTextField = "ChapterCode";
            ddlChapterWkShop.DataValueField = "ChapterID";
            ddlChapterWkShop.DataBind();

            ddlChapterPrepClub.Items.Clear();
            ddlChapterPrepClub.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapterPrepClub.DataSource = ChapInZonesDS;
            ddlChapterPrepClub.DataTextField = "ChapterCode";
            ddlChapterPrepClub.DataValueField = "ChapterID";
            ddlChapterPrepClub.DataBind();


            DdlZonalCoordinator.Enabled = false;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = false;
            //lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
            //lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
            //lnkRoomSchedule.Attributes.Add("onclick", "return validateFacilities()");
            //lnkContestTeam.Attributes.Add("onclick", "return validateFacilities()");
            //lnkRoomGuides.Attributes.Add("onclick", "return validateFacilities()");
            //lnkGraders.Attributes.Add("onclick", "return validateFacilities()");
            //lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");

        }

        if (DdlRole.SelectedItem.Text == "NationalC")
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trNationalCoordinator.Visible = true;
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trClusterCoordinator.Visible = true;
            trZonalCoordinator.Visible = true;
            trCustomer.Visible = true;
            trFinalCoordinator.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            DdlRole.Enabled = false;
            trBeeBook.Visible = true;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = true;
            trTechnicalCoordinator.Visible = true;
            trTeamMember.Visible = true;
            trAccountingFunctions.Visible = true;
            lnkPrepareFinals.Enabled = true;
            trCoachFunctions.Visible = true;
            trCoachFunctionsCoach.Visible = true;
            trMedalsCert.Visible = true;
            trFundRaising.Visible = true;
            trLaptopJudge.Visible = true;
            lnkRoomRequirement.Enabled = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            TrNationalTechC.Visible = true;
            TrIndiaScholarships.Visible = true;
            Hyperlink87.Enabled = false;
            trWrkShop.Visible = true;
            trPrepClub.Visible = true;
            trSATFunctionsAdmin.Visible = true;
            // trSATFunctionsCoach.Visible = true;
            trExamDistributor.Visible = true;
            trOnlineWScal.Visible = true;
        }

        if (DdlRole.SelectedItem.Text == "Admin")
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trAdmin.Visible = true;
            trCustomer.Visible = true;
            trNationalCoordinator.Visible = true;
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trClusterCoordinator.Visible = true;
            trZonalCoordinator.Visible = true;
            trCustomer.Visible = true;
            trFinalCoordinator.Visible = true;
            trGeneralFunctions.Visible = true;
            trBeeFunction.Visible = true;
            trPersonal.Visible = true;
            trBeeBook.Visible = true;
            trCoreTeamfunctions.Visible = true;
            DdlRole.Enabled = false;
            trTeamLead.Visible = true;
            BtnContinue.Enabled = false;
            trTechnicalCoordinator.Visible = true;
            trTeamMember.Visible = true;
            trAccountingFunctions.Visible = true;
            trTreasuryFunctions.Visible = true;
            lnkPrepareFinals.Enabled = true;
            trCoachFunctions.Visible = true;
            trCoachFunctionsCoach.Visible = true;
            trWebPageMgmt.Visible = true;
            trMedalsCert.Visible = true;
            trExamReceiver.Visible = false;
            trFundRaising.Visible = true;
            trLaptopJudge.Visible = true;
            trgamefunction.Visible = true;
            lnkRoomRequirement.Enabled = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            TrNationalTechC.Visible = true;
            TrIndiaScholarships.Visible = true;
            Hyperlink87.Enabled = true;
            trWrkShop.Visible = true;
            trPrepClub.Visible = true;
            trSATFunctionsAdmin.Visible = true;
            //trSATFunctionsCoach.Visible = true;
            trExamDistributor.Visible = true;
            Hyperlink72.Enabled = true;
            Hyperlink152.Enabled = true;
            lbtnScheduleCoaches.Visible = false;
            LinkButton1.Visible = false;
            HyperLink35.Visible = false;
            HyperLink49.Visible = false;
            LinkButton3.Visible = false;
            trOnlineWScal.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("TechCGlobal"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 8;
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trExamReceiver.Visible = false;
            // trTechnicalCoordinator.Visible = true;
            //** Ferdine Modified 27/03/2010
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            String str = "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1  and MemberID=" + Session["LoginID"] + " and RoleId=8";// and RoleId=" + Session["LoginID"].ToString() + "";
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            int fCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, str));
            if (fCount > 0)
            {
                trRentalNeeds.Visible = true;
                ddlChapter1.SelectedIndex = ddlChapter1.Items.IndexOf(ddlChapter1.Items.FindByValue("1"));
                ddlChapter1.Enabled = false;
            }

        }
        else if (DdlRole.SelectedItem.Text.Contains("TechC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trTechnicalCoordinator.Visible = true;
            trgamefunction.Visible = false;
            trLaptopJudge.Visible = false;
        }
        if (DdlRole.SelectedItem.Text.Contains("CoachAdmin"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trCoachFunctions.Visible = true;
            trCoachFunctionsCoach.Visible = true;
            trSATFunctionsAdmin.Visible = true;
            //trSATFunctionsCoach.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trTeamLead.Visible = false;
            trVolRecruit.Visible = false;
        }
        else if (DdlRole.SelectedItem.Text.Contains("Coach"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trCoachFunctions.Visible = false;
            trCoachFunctionsCoach.Visible = true;
            if (DT.Rows[0].ItemArray[4].ToString() == "Y")
            {
                Hyperlink15.Enabled = true;
                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trSATFunctionsAdmin.Visible = true;
                }
            }
            else
            {
                Hyperlink15.Enabled = false;
                trSATFunctionsAdmin.Visible = false;
            }

            //trSATFunctionsCoach.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trTeamLead.Visible = false;

            if (Convert.ToInt32(Session["RoleId"]) == 96)
            {
                trCoachFunctionsCoach.Visible = true;
                trCoachFunctions.Visible = true;
                trSATFunctionsAdmin.Visible = true;
                trVolRecruit.Visible = false;
            }
        }

        if (DdlRole.SelectedItem.Text.Contains("ExamDist"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 32;
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trExamReceiver.Visible = false;
            //trUploadTestPapers.Visible = true;
            trgamefunction.Visible = false;
            trExamDistributor.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("ExamReceiver"))
        {
            //** Ferdine Modified 26/03/2010
            trgamefunction.Visible = false;
            Session["RoleId"] = 33;
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trExamReceiver.Visible = false;
            //** Ferdine Modified 26/03/2010
            trgamefunction.Visible = false;
        }
        if (DdlRole.SelectedItem.Text.Contains("MedalsCert"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["SelChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = false;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trExamReceiver.Visible = false;
            trPersonal.Visible = false;
            trBadgesCertificates.Visible = false;
            trgamefunction.Visible = false;
            trMedalsCert.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("MC"))
        {
            //** Ferdine Modified 26/03/2010
            trTeamMember.Visible = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

        }
        if (DdlRole.SelectedItem.Text.Contains("VenueC"))
        {
            //** Ferdine Modified 26/03/2010
            trTeamLead.Visible = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

        }
        if (DdlRole.SelectedItem.Text.Contains("RoomG"))
        {
            //** Ferdine Modified 27/03/2010
            Session["RoleId"] = 23;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

        }
        //WrkshpC dont need Game Function & technical Coordinator row
        if (DdlRole.SelectedItem.Text.Contains("FundC"))
        {
            Session["RoleId"] = 12;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trFundRaising.Visible = true;
            trPersonal.Visible = true;
        }

        if (DdlRole.SelectedItem.Text.Contains("Facilities"))
        {
            //** Added on 03/07/2012
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["SelChapterID"] = DT.Rows[0].ItemArray[8].ToString();

            trRentalNeeds.Visible = true;

            //Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();

            if (DT.Rows[0].ItemArray[8].ToString() != String.Empty)
            {
                if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
                {
                    fillChapterBasedOnRole(ddlChapter1);
                }
                else
                {
                    ddlChapter1.SelectedItem.Text = DT.Rows[0].ItemArray[9].ToString();
                    ddlChapter1.SelectedValue = DT.Rows[0].ItemArray[8].ToString();
                    ddlChapter1.Enabled = false;
                }
            }
            else
            {
                int FacCount;
                String str = "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1  and MemberID=" + Session["LoginID"] + "";// and RoleId=" + Session["LoginID"].ToString() + "";
                SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                FacCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, str));

                if (FacCount > 0)
                {
                    ddlChapter1.SelectedValue = "1";// DT.Rows[0].ItemArray[8].ToString();
                    ddlChapter1.Enabled = false;
                }

            }
            //if (Convert.ToInt32(Session["RoleID"]) == 52)
            //{
            //    ddlChapter1.SelectedItem.Text = DT.Rows[0].ItemArray[9].ToString();
            //    ddlChapter1.SelectedValue = DT.Rows[0].ItemArray[8].ToString();
            //    ddlChapter1.Enabled = false;
            //}

            lnkRoomRequirement.Enabled = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");

            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trgamefunction.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;

            //Session["selChapterID"] = ddlChapter1.SelectedValue;
            // Session["selChapterName"] = ddlChapter1.SelectedItem.Text;
        }
        if (DdlRole.SelectedItem.Text.Contains("JudgeLaptop"))//Laptop  ") || al.Contains("JudgeChief
        {
            trLaptopJudge.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            Session["RoleId"] = 18;
        }
        if (DdlRole.SelectedItem.Text.Contains("JudgeChief"))//Laptop  ") || al.Contains("JudgeChief
        {
            trLaptopJudge.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            Session["RoleId"] = 16;
        }
        if (DdlRole.SelectedItem.Text.Contains("GameCust"))//Game 
        {

            trgamefunction.Visible = true;
        }
        if (Convert.ToInt32(RoleID) == 11)
        {
            //trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
        }
        if (DdlRole.SelectedItem.Text.Contains("CorpMatch"))
        {
            (Session["RoleId"]) = 92;
            trAccountingFunctions.Visible = true;
            hlinkReqRefund.Enabled = false;
            Hyperlink87.Enabled = false;
            Hyperlink34.Enabled = false;
            Hyperlink66.Enabled = false;
            Hyperlink144.Enabled = false;
            Hyperlink72.Enabled = true;
            Hyperlink136.Enabled = false;
            if (DT.Rows[0].ItemArray[10].ToString() == "Y")
            {
                trTeamMember.Visible = false;
                trTeamLead.Visible = false;
                trTechnicalCoordinator.Visible = false;
            }
        }
        else
        {
            //Hyperlink72.Enabled = false;
            hlinkReqRefund.Enabled = true;
            //Hyperlink87.Enabled = true;
            Hyperlink34.Visible = true;
            Hyperlink66.Visible = true;
            Hyperlink144.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("NatTechT"))
        {
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString(); // 93;
            trNationalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trRentalNeeds.Visible = false;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = false;
            DdlRole.Enabled = false;
            trBeeBook.Visible = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trTeamMember.Visible = false;
            trAccountingFunctions.Visible = false;
            lnkPrepareFinals.Enabled = false;
            trCoachFunctions.Visible = false;
            trCoachFunctionsCoach.Visible = false;
            trMedalsCert.Visible = false;
            trFundRaising.Visible = false;
            trLaptopJudge.Visible = false;
            lnkRoomRequirement.Enabled = false;
            lnkRoomList.Enabled = false;
            lnkRentalNeeds.Enabled = false;
            lnkRoomSchedule.Enabled = false;
            lnkContestTeam.Enabled = false;
            lnkRoomGuides.Enabled = false;
            lnkGraders.Enabled = false;
            lnkAssignRolesFacilities.Enabled = false;
            TrNationalTechC.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

            trCoreTeamfunctions.Visible = true;
            lnkSBVBSelMatrixCoreT.Enabled = false;
            hylnkGenSBVBTestPaperCoreT.Enabled = false;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Count(*) From Volunteer Where MemberID=" + Convert.ToInt32(Session["LoginID"]) + " and TeamLead='Y' and RoleID=93")) > 0)
            {
                Hyperlink152.Enabled = true;
            }
            else //if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Count(*) From Volunteer Where MemberID=" + Convert.ToInt32(Session["LoginID"]) + " and TeamLead='N' ")) > 0)
            {
                Hyperlink152.Enabled = false;
            }
        }

        if (DdlRole.SelectedItem.Text.Contains("LiaisonIndia"))
        {
            TrIndiaScholarships.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("CollegeSch"))
        {
            TrIndiaScholarships.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("DesigSch"))
        {
            TrIndiaScholarships.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("OWkshopC_National"))
        {
            trOnlineWScal.Visible = true;
            Session["LoginRole"] = "97";
            Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
            Session["RoleId"] = 97;
            txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
            txtRoleCategory.Enabled = false;
            txtRoleCode.Text = "OWkshopC";
            txtRoleCode.Enabled = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = false;
            trPersonal.Visible = false;
            trTeamLead.Visible = false;// true;
            trTeamMember.Visible = false;
            trWrkShop.Visible = false;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=97 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
            {
                ddlChapterWkShop.Enabled = true;
            }
            else
            {
                ddlChapterWkShop.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.Enabled = false;
            }
        }
        else if (DdlRole.SelectedItem.Text.Contains("WkshopC"))
        {
            Session["LoginRole"] = "11";
            Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
            Session["RoleId"] = 11;
            txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
            txtRoleCategory.Enabled = false;
            txtRoleCode.Text = "WkshopC";
            txtRoleCode.Enabled = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;// true;
            trTeamMember.Visible = false;
            trWrkShop.Visible = true;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=11 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
            {
                ddlChapterWkShop.Enabled = true;
            }
            else
            {
                ddlChapterWkShop.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.Enabled = false;
            }
        }
        if (DdlRole.SelectedItem.Text.Contains("PrepClubC"))
        {
            Session["LoginRole"] = "94";
            Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
            Session["RoleId"] = 94;
            txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
            txtRoleCategory.Enabled = false;
            txtRoleCode.Text = "PrepClubC";
            txtRoleCode.Enabled = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;// true;
            trTeamMember.Visible = false;
            trWrkShop.Visible = false;
            trPrepClub.Visible = true;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=94 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
            {
                ddlChapterPrepClub.Enabled = true;
            }
            else
            {
                ddlChapterPrepClub.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterPrepClub.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterPrepClub.Enabled = false;
            }
        }
        if (DdlRole.SelectedItem.Text.Contains("CoreT"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trNationalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trRentalNeeds.Visible = false;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trAccountingFunctions.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trBeeBook.Visible = false;
            lnkPrepareFinals.Enabled = false;
            trCoachFunctions.Visible = false;
            trCoachFunctionsCoach.Visible = false;
            trMedalsCert.Visible = false;
            trFundRaising.Visible = false;
            trLaptopJudge.Visible = false;
            lnkRoomRequirement.Enabled = false;
            lnkRoomList.Enabled = false;
            lnkRentalNeeds.Enabled = false;
            lnkRoomSchedule.Enabled = false;
            lnkContestTeam.Enabled = false;
            lnkRoomGuides.Enabled = false;
            lnkGraders.Enabled = false;
            lnkAssignRolesFacilities.Enabled = false;
            TrNationalTechC.Visible = false;
            TrIndiaScholarships.Visible = false;
            Hyperlink87.Enabled = false;
            trWrkShop.Visible = false;
            trPrepClub.Visible = false;

            trSATFunctionsAdmin.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trCoreTeamfunctions.Visible = true;


            // trSATFunctionsCoach.Visible = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 88) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96))
        {
            Hyperlink15.Enabled = false;
            Hyperlink71.Enabled = false;
        }
        if (Convert.ToInt32(Session["RoleId"]) == 97)
        {
            trOnlineWScal.Visible = true;
        }
        ViewTeamLeadOrMember(TeamLead.Contains("Y"));
        hidelinksbasedonRoleID();
        hideSBVBTestPaperLink();
        visibleVolSignReportlinks();
    }

    // Event Handler for a Volunteer with Multiple Roles
    protected void BtnContinue_Click(object sender, EventArgs e)
    {
        Session["RoleCode"] = DdlRole.SelectedValue;
        table1.Visible = true;
        string RoleID = string.Empty;
        string TeamLead = string.Empty;
        //// VRegistration.NSFMasterPage nsfMaster =  (VRegistration.NSFMasterPage)this.Master;
        ////// nsfMaster.addMenuItemAt(2, new MenuItem("Back", "Back", "volunteerfunctions.aspx"));
        //// nsfMaster.addBackMenuItem("volunteerfunctions.aspx");
        GetVolunteerTableAdapters.VolunteerTableAdapter VT = new GetVolunteerTableAdapters.VolunteerTableAdapter();
        DataTable DT = new DataTable();
        DT = VT.GetVolunterData(Convert.ToInt32(DdlRole.SelectedItem.Value));
        RoleID = DT.Rows[0].ItemArray[2].ToString();
        TeamLead = DT.Rows[0].ItemArray[4].ToString();
        Session["TL"] = TeamLead.ToString();
        // Voluntter with CustomerService Role.

        if (DdlRole.SelectedItem.Text.Contains("CustService"))
        {
            trCustomer.Visible = true;
            trTeamMember.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = true;


        }
        // Voluntter with BeeBook Role.
        if (DdlRole.SelectedItem.Text.Contains("BeeBook"))
        {
            Session["RoleId"] = 41;
            trBeeBook.Visible = true;
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trTechnicalCoordinator.Visible = false;

        }


        if (DdlRole.SelectedItem.Text.Contains("ITWeb"))
        {

            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 43;
            //Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
            trWebPageMgmt.Visible = true;
            trgamefunction.Visible = false;
        }

        if (DdlRole.SelectedItem.Text.Contains("WebAdmin"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 90;
            //Session["LoginChapterID"] = dt.Rows[0].ItemArray[8].ToString();
            trWebPageMgmt.Visible = true;
            trgamefunction.Visible = false;
        }

        // Voluntter with BeeBook Role.
        if (DdlRole.SelectedItem.Text.Contains("ChapterC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["SelChapterID"] = DT.Rows[0].ItemArray[8].ToString();

            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = false;
            Hyperlink72.Enabled = false;
            Hyperlink87.Enabled = false;
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trAccountingFunctions.Visible = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            trWrkShop.Visible = true;
            trPrepClub.Visible = true;
            //trWebPageMgmt.Visible = true;

            if (DT.Rows[0].ItemArray[8].ToString() == "1")
            {
                trFinalCoordinator.Visible = true;
                Session["EventID"] = 1;
            }
            else
            {
                trFinalCoordinator.Visible = false;
            }
            for (int i = 0; i <= ddlChapter.Items.Count - 1; i++)
            {
                if (ddlChapter.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                    ddlChapter.SelectedIndex = i;
            }
            if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
            {
                fillChapterBasedOnRole(ddlChapter1);
            }
            else
            {
                for (int i = 0; i <= ddlChapter1.Items.Count - 1; i++)
                {

                    if (ddlChapter1.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                        ddlChapter1.SelectedIndex = i;
                }
            }
            for (int i = 0; i <= ddlChapterWkShop.Items.Count - 1; i++)
            {

                if (ddlChapterWkShop.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                    ddlChapterWkShop.SelectedIndex = i;
            }
            for (int i = 0; i <= ddlChapterPrepClub.Items.Count - 1; i++)
            {

                if (ddlChapterPrepClub.Items[i].Value == DT.Rows[0].ItemArray[8].ToString().Trim())
                    ddlChapterPrepClub.SelectedIndex = i;
            }

            Session["selChapterName"] = ddlChapter.SelectedItem.Text;

            ddlChapter.Enabled = false;
            ddlChapter1.Enabled = false;
            ddlChapterWkShop.Enabled = false;
            ddlChapterPrepClub.Enabled = false;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;

            // lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
            // lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
            //  lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");
        }


        // Voluntter with  RoleID > 5  and if he is a Team Lead. 
        if (DT.Rows[0].ItemArray[4].ToString() == "Y" && Convert.ToInt32(RoleID) > 5)
        {

            if (DT.Rows[0].ItemArray[10].ToString() == "Y")
            {

                txtRoleCategory.Text = "National";
                txtRoleCategory.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[13].ToString() == "Y")
            {

                txtRoleCategory.Text = "Finals";
                txtRoleCategory.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[16].ToString() == "Y")
            {

                txtRoleCategory.Text = "IndiaChapter";
                txtRoleCategory.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[12].ToString() != "")
            {

                txtRoleCategory.Text = DT.Rows[0].ItemArray[12].ToString();
                txtRoleCategory.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[15].ToString() != "")
            {
                txtRoleCategory.Text = DT.Rows[0].ItemArray[15].ToString();
                txtRoleCategory.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[9].ToString() != "")
            {

                txtRoleCategory.Text = DT.Rows[0].ItemArray[9].ToString();
                txtRoleCategory.Enabled = false;
            }

            //RoleID > 5 can see Teammemeber panel

            txtRoleCode.Text = DT.Rows[0].ItemArray[3].ToString();
            txtRoleCode.Enabled = false;
            trTeamLead.Visible = true;
            trTeamMember.Visible = false;
            BtnContinue.Enabled = false;
            DdlRole.Enabled = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

            if (Convert.ToInt32(RoleID) != 29)
                trTechnicalCoordinator.Visible = true;
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();

            //Role ID 37 & 38 can only see Accounting Panel.
            if (DdlRole.SelectedItem.Text.Contains("ActgData"))// || DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                Hyperlink72.Enabled = false;
                //Hyperlink87.Enabled = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
            if (DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = false;
                //  Hyperlink72.Enabled = false;
                //Hyperlink87.Enabled = false;
                trTeamLead.Visible = false;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trTreasuryFunctions.Visible = true;
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }
            }
        }
        if (Convert.ToInt32(RoleID) > 5 && DT.Rows[0].ItemArray[4].ToString() == "N")
        {
            if (DT.Rows[0].ItemArray[10].ToString() == "Y")
            {

                txtRoleCategory2.Text = "National";

                txtRoleCategory2.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[13].ToString() == "Y")
            {

                txtRoleCategory2.Text = "Finals";

                txtRoleCategory2.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[16].ToString() == "Y")
            {

                txtRoleCategory2.Text = "IndiaChapter";
                txtRoleCategory2.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[12].ToString() != "")
            {
                txtRoleCategory2.Text = DT.Rows[0].ItemArray[12].ToString();

                txtRoleCategory2.Enabled = false;
            }

            else if (DT.Rows[0].ItemArray[15].ToString() != "")
            {
                txtRoleCategory2.Text = DT.Rows[0].ItemArray[15].ToString();
                txtRoleCategory2.Enabled = false;

            }
            else if (DT.Rows[0].ItemArray[9].ToString() != "")
            {

                txtRoleCategory2.Text = DT.Rows[0].ItemArray[9].ToString();
                txtRoleCategory2.Enabled = false;
            }
            txtRoleCode2.Text = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            DdlRole.Enabled = false;
            trTechnicalCoordinator.Visible = true;
            if (Convert.ToInt32(Session["RoleId"]) == 16 || Convert.ToInt32(Session["RoleId"]) == 18)
            {
                trLaptopJudge.Visible = true;
            }
            if (Convert.ToInt32(Session["RoleId"]) == 18)
            {
                lbtnTechCDnloadTstpapers.Enabled = false;
            }
            else
            {
                lbtnTechCDnloadTstpapers.Enabled = true;
            }
            txtRoleCode2.Enabled = false;
            trTeamMember.Visible = true;
            BtnContinue.Enabled = false;

            //Role ID 37 & 38 can only see Accounting Panel.
            if (DdlRole.SelectedItem.Text.Contains("ActgData"))// || DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;
                trTreasuryFunctions.Visible = false;

                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trTreasuryFunctions.Visible = true;
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
            if (DdlRole.SelectedItem.Text.Contains("ActgReview"))
            {
                //** Ferdine Modified 26/03/2010
                trAccountingFunctions.Visible = true;
                trTreasuryFunctions.Visible = true;
                trTeamMember.Visible = false;
                trgamefunction.Visible = false;
                trTechnicalCoordinator.Visible = false;

                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    Hyperlink87.Enabled = true;
                    Hyperlink72.Enabled = true;
                    HyperLink6.Enabled = true;
                }
                else if (Convert.ToInt32(DT.Rows[0].ItemArray[8].ToString()) > 0 || DT.Rows[0].ItemArray[15].ToString() == "Y")
                {
                    Hyperlink87.Enabled = false;
                    Hyperlink72.Enabled = false;
                }
                if (DT.Rows[0].ItemArray[10].ToString() != "Y")
                {
                    HyperLink6.Enabled = false;
                }

            }
        }

        if (DdlRole.SelectedItem.Text.Contains("Treasurer") || DdlRole.SelectedItem.Text.Contains("Co-Treasurer"))
        {

            trGeneralFunctions.Visible = false;
            trgamefunction.Visible = false;
            trPersonal.Visible = true;
            trAccountingFunctions.Visible = true;
            trTreasuryFunctions.Visible = true;
            Hyperlink87.Enabled = true;
        }

        if (DdlRole.SelectedItem.Text.Contains("CustomerC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trCustomer.Visible = true;
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = true;
        }

        if ((DdlRole.SelectedItem.Text.Contains("Clusterc")) || (DdlRole.SelectedItem.Text.Contains("ClusterC")))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            lblMessage.Text = "Go to ClusterC";
            trChapterCoordinator.Visible = true;
            //trZonalCoordinator.Visible = false;
            trClusterCoordinator.Visible = true;
            trCustomer.Visible = false;

            trTeamLead.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            // Added on April 9th, 2012
            trAccountingFunctions.Visible = true;
            hlinkReqRefund.Enabled = false;
            Hyperlink72.Enabled = false;
            Hyperlink87.Enabled = false;
            Hyperlink34.Enabled = false;
            Hyperlink66.Enabled = false;
            Hyperlink56.Enabled = false;
            Hyperlink54.Enabled = false;
            hlinkReqRefund.Enabled = false;
            lnkDAS.Enabled = false;


            Hyperlink34.Enabled = false;
            hlinkReqRefund.Visible = false;
            Hyperlink72.Enabled = false;
            Hyperlink136.Enabled = false;
            HyperLink6.Enabled = false;
            Hyperlink66.Enabled = false;

            for (int i = 0; i <= DdlCluster.Items.Count - 1; i++)
            {
                if (DdlCluster.Items[i].Text.Trim() == DT.Rows[0].ItemArray[15].ToString().Trim())
                {
                    DdlCluster.SelectedIndex = i;
                    Session["selClusterID"] = DdlCluster.SelectedItem.Value;
                }
            }
            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChaWithinClustersDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();
            DdlCluster.Enabled = false;
            DdlRole.Enabled = false;
            trTeamMember.Visible = false;
            BtnContinue.Enabled = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = false;
        }

        if (DdlRole.SelectedItem.Text.Contains("FinalsCoreT") && DT.Rows[0].ItemArray[4].ToString() == "Y")
        {
            DdlRoleCategory1.SelectedIndex = 2;
            DdlRoleCategory1.Enabled = false;
            DDlRoleCode.SelectedIndex = 22;
            DDlRoleCode.Enabled = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trAdmin.Visible = false;
            trCustomer.Visible = false;
            trNationalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = true;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = true;
            trCoreTeamfunctions.Visible = false;
            trSATFunctionsAdmin.Visible = false;
        }


        if (DdlRole.SelectedItem.Text.Contains("ZonalC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            lblMessage.Text = "Go to ZonalC";
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = true;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;





            trAccountingFunctions.Visible = true;
            Hyperlink34.Enabled = false;
            hlinkReqRefund.Visible = false;

            Hyperlink72.Enabled = false;
            Hyperlink136.Enabled = false;
            HyperLink6.Enabled = false;
            Hyperlink66.Enabled = false;


            for (int i = 0; i <= DdlZonalCoordinator.Items.Count - 1; i++)
            {
                if (DdlZonalCoordinator.Items[i].Text == DT.Rows[0].ItemArray[12].ToString())
                {
                    DdlZonalCoordinator.SelectedIndex = i;
                }

            }

            ddlChapter.Items.Clear();
            ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter.DataSource = ChapInZonesDS;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();

            ddlChapter1.Items.Clear();
            ddlChapter1.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapter1.DataSource = ChapInZonesDS;
            ddlChapter1.DataTextField = "ChapterCode";
            ddlChapter1.DataValueField = "ChapterID";
            ddlChapter1.DataBind();

            fillChapterBasedOnRole(ddlChapter1);

            ddlChapterWkShop.Items.Clear();
            ddlChapterWkShop.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapterWkShop.DataSource = ChapInZonesDS;
            ddlChapterWkShop.DataTextField = "ChapterCode";
            ddlChapterWkShop.DataValueField = "ChapterID";
            ddlChapterWkShop.DataBind();

            ddlChapterPrepClub.Items.Clear();
            ddlChapterPrepClub.Items.Insert(0, new ListItem("[Select Chapter]", String.Empty));
            ddlChapterPrepClub.DataSource = ChapInZonesDS;
            ddlChapterPrepClub.DataTextField = "ChapterCode";
            ddlChapterPrepClub.DataValueField = "ChapterID";
            ddlChapterPrepClub.DataBind();


            DdlZonalCoordinator.Enabled = false;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trgamefunction.Visible = false;
            //lnkRoomList.Attributes.Add("onclick", "return validateFacilities()");
            //lnkRentalNeeds.Attributes.Add("onclick", "return validateFacilities()");
            //lnkRoomSchedule.Attributes.Add("onclick", "return validateFacilities()");
            //lnkContestTeam.Attributes.Add("onclick", "return validateFacilities()");
            //lnkRoomGuides.Attributes.Add("onclick", "return validateFacilities()");
            //lnkGraders.Attributes.Add("onclick", "return validateFacilities()");
            //lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");

        }

        if (DdlRole.SelectedItem.Text == "NationalC")
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trNationalCoordinator.Visible = true;
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trClusterCoordinator.Visible = true;
            trZonalCoordinator.Visible = true;
            trCustomer.Visible = true;
            trFinalCoordinator.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            DdlRole.Enabled = false;
            trBeeBook.Visible = true;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = true;
            trTechnicalCoordinator.Visible = true;
            trTeamMember.Visible = true;
            trAccountingFunctions.Visible = true;
            lnkPrepareFinals.Enabled = true;
            trCoachFunctions.Visible = true;
            trCoachFunctionsCoach.Visible = true;
            trMedalsCert.Visible = true;
            trFundRaising.Visible = true;
            trLaptopJudge.Visible = true;
            lnkRoomRequirement.Enabled = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            TrNationalTechC.Visible = true;
            TrIndiaScholarships.Visible = true;
            Hyperlink87.Enabled = false;
            trWrkShop.Visible = true;
            trPrepClub.Visible = true;
            trSATFunctionsAdmin.Visible = true;
            // trSATFunctionsCoach.Visible = true;
            trExamDistributor.Visible = true;
            trOnlineWScal.Visible = true;
        }

        if (DdlRole.SelectedItem.Text == "Admin")
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trAdmin.Visible = true;
            trCustomer.Visible = true;
            trNationalCoordinator.Visible = true;
            trChapterCoordinator.Visible = true;
            trRentalNeeds.Visible = true;
            trClusterCoordinator.Visible = true;
            trZonalCoordinator.Visible = true;
            trCustomer.Visible = true;
            trFinalCoordinator.Visible = true;
            trGeneralFunctions.Visible = true;
            trBeeFunction.Visible = true;
            trPersonal.Visible = true;
            trBeeBook.Visible = true;
            trCoreTeamfunctions.Visible = true;
            DdlRole.Enabled = false;
            trTeamLead.Visible = true;
            BtnContinue.Enabled = false;
            trTechnicalCoordinator.Visible = true;
            trTeamMember.Visible = true;
            trAccountingFunctions.Visible = true;
            trTreasuryFunctions.Visible = true;
            lnkPrepareFinals.Enabled = true;
            trCoachFunctions.Visible = true;
            trCoachFunctionsCoach.Visible = true;
            trWebPageMgmt.Visible = true;
            trMedalsCert.Visible = true;
            trExamReceiver.Visible = false;
            trFundRaising.Visible = true;
            trLaptopJudge.Visible = true;
            trgamefunction.Visible = true;
            lnkRoomRequirement.Enabled = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            TrNationalTechC.Visible = true;
            TrIndiaScholarships.Visible = true;
            Hyperlink87.Enabled = true;
            trWrkShop.Visible = true;
            trPrepClub.Visible = true;
            trSATFunctionsAdmin.Visible = true;
            //trSATFunctionsCoach.Visible = true;
            trExamDistributor.Visible = true;
            Hyperlink72.Enabled = true;
            Hyperlink152.Enabled = true;
            lbtnScheduleCoaches.Visible = false;
            LinkButton1.Visible = false;
            HyperLink35.Visible = false;
            HyperLink49.Visible = false;
            LinkButton3.Visible = false;
            trOnlineWScal.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("TechCGlobal"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 8;
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trExamReceiver.Visible = false;
            // trTechnicalCoordinator.Visible = true;
            //** Ferdine Modified 27/03/2010
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            String str = "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1  and MemberID=" + Session["LoginID"] + " and RoleId=8";// and RoleId=" + Session["LoginID"].ToString() + "";
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            int fCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, str));
            if (fCount > 0)
            {
                trRentalNeeds.Visible = true;
                ddlChapter1.SelectedIndex = ddlChapter1.Items.IndexOf(ddlChapter1.Items.FindByValue("1"));
                ddlChapter1.Enabled = false;
            }

        }
        else if (DdlRole.SelectedItem.Text.Contains("TechC"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trTechnicalCoordinator.Visible = true;
            trgamefunction.Visible = false;
            trLaptopJudge.Visible = false;
        }
        if (DdlRole.SelectedItem.Text.Contains("CoachAdmin"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trCoachFunctions.Visible = true;
            trCoachFunctionsCoach.Visible = true;
            trSATFunctionsAdmin.Visible = true;
            //trSATFunctionsCoach.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trTeamLead.Visible = false;
            trVolRecruit.Visible = false;
        }
        else if (DdlRole.SelectedItem.Text.Contains("Coach"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trCoachFunctions.Visible = false;
            trCoachFunctionsCoach.Visible = true;
            if (DT.Rows[0].ItemArray[4].ToString() == "Y")
            {
                Hyperlink15.Enabled = true;
                if (DT.Rows[0].ItemArray[10].ToString() == "Y")
                {
                    trSATFunctionsAdmin.Visible = true;
                }
            }
            else
            {
                Hyperlink15.Enabled = false;
                trSATFunctionsAdmin.Visible = false;
            }

            //trSATFunctionsCoach.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trTeamLead.Visible = false;

            if (Convert.ToInt32(Session["RoleId"]) == 96)
            {
                trCoachFunctionsCoach.Visible = true;
                trCoachFunctions.Visible = true;
                trSATFunctionsAdmin.Visible = true;
                trVolRecruit.Visible = false;
                trOnlineWScal.Visible = true;
            }
        }



        if (DdlRole.SelectedItem.Text.Contains("ExamDist"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = 32;
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trExamReceiver.Visible = false;
            //trUploadTestPapers.Visible = true;
            trgamefunction.Visible = false;
            trExamDistributor.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("ExamReceiver"))
        {
            //** Ferdine Modified 26/03/2010
            trgamefunction.Visible = false;
            Session["RoleId"] = 33;
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trExamReceiver.Visible = false;
            //** Ferdine Modified 26/03/2010
            trgamefunction.Visible = false;
        }
        if (DdlRole.SelectedItem.Text.Contains("MedalsCert"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["SelChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            trZonalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trClusterCoordinator.Visible = false;
            trCustomer.Visible = false;
            trGeneralFunctions.Visible = false;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trExamReceiver.Visible = false;
            trPersonal.Visible = false;
            trBadgesCertificates.Visible = false;
            trgamefunction.Visible = false;
            trMedalsCert.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("MC"))
        {
            //** Ferdine Modified 26/03/2010
            trTeamMember.Visible = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

        }
        if (DdlRole.SelectedItem.Text.Contains("VenueC"))
        {
            //** Ferdine Modified 26/03/2010
            trTeamLead.Visible = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

        }
        if (DdlRole.SelectedItem.Text.Contains("RoomG"))
        {
            //** Ferdine Modified 27/03/2010
            Session["RoleId"] = 23;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;

        }
        //WrkshpC dont need Game Function & technical Coordinator row
        if (DdlRole.SelectedItem.Text.Contains("FundC"))
        {
            Session["RoleId"] = 12;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trFundRaising.Visible = true;
            trPersonal.Visible = true;
        }

        if (DdlRole.SelectedItem.Text.Contains("Facilities"))
        {
            //** Added on 03/07/2012
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();
            Session["SelChapterID"] = DT.Rows[0].ItemArray[8].ToString();

            trRentalNeeds.Visible = true;

            //Session["LoginChapterID"] = DT.Rows[0].ItemArray[8].ToString();

            if (DT.Rows[0].ItemArray[8].ToString() != String.Empty)
            {
                if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2) || (Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
                {
                    fillChapterBasedOnRole(ddlChapter1);
                }
                else
                {
                    ddlChapter1.SelectedItem.Text = DT.Rows[0].ItemArray[9].ToString();
                    ddlChapter1.SelectedValue = DT.Rows[0].ItemArray[8].ToString();
                    ddlChapter1.Enabled = false;
                }
            }
            else
            {
                int FacCount;
                String str = "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1  and MemberID=" + Session["LoginID"] + "";// and RoleId=" + Session["LoginID"].ToString() + "";
                SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                FacCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, str));

                if (FacCount > 0)
                {
                    ddlChapter1.SelectedValue = "1";// DT.Rows[0].ItemArray[8].ToString();
                    ddlChapter1.Enabled = false;
                }

            }
            //if (Convert.ToInt32(Session["RoleID"]) == 52)
            //{
            //    ddlChapter1.SelectedItem.Text = DT.Rows[0].ItemArray[9].ToString();
            //    ddlChapter1.SelectedValue = DT.Rows[0].ItemArray[8].ToString();
            //    ddlChapter1.Enabled = false;
            //}

            lnkRoomRequirement.Enabled = true;
            lnkRoomList.Enabled = true;
            lnkRentalNeeds.Enabled = true;
            lnkRoomSchedule.Enabled = true;
            lnkContestTeam.Enabled = true;
            lnkRoomGuides.Enabled = true;
            lnkGraders.Enabled = true;
            lnkAssignRolesFacilities.Enabled = true;
            lnkAssignRolesFacilities.Attributes.Add("onclick", "return validateFacilities()");

            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trgamefunction.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;

            //Session["selChapterID"] = ddlChapter1.SelectedValue;
            // Session["selChapterName"] = ddlChapter1.SelectedItem.Text;
        }
        if (DdlRole.SelectedItem.Text.Contains("JudgeLaptop"))//Laptop  ") || al.Contains("JudgeChief
        {
            trLaptopJudge.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            Session["RoleId"] = 18;
        }
        if (DdlRole.SelectedItem.Text.Contains("JudgeChief"))//Laptop  ") || al.Contains("JudgeChief
        {
            trLaptopJudge.Visible = true;
            trgamefunction.Visible = false;
            trTeamMember.Visible = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            Session["RoleId"] = 16;
        }
        if (DdlRole.SelectedItem.Text.Contains("GameCust"))//Game 
        {

            trgamefunction.Visible = true;
        }
        if (Convert.ToInt32(RoleID) == 11)
        {
            //trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
        }
        if (DdlRole.SelectedItem.Text.Contains("CorpMatch"))
        {
            (Session["RoleId"]) = 92;
            trAccountingFunctions.Visible = true;
            hlinkReqRefund.Enabled = false;
            Hyperlink87.Enabled = false;
            Hyperlink34.Enabled = false;
            Hyperlink66.Enabled = false;
            Hyperlink144.Enabled = false;
            Hyperlink72.Enabled = true;
            Hyperlink136.Enabled = false;
            if (DT.Rows[0].ItemArray[10].ToString() == "Y")
            {
                trTeamMember.Visible = false;
                trTeamLead.Visible = false;
                trTechnicalCoordinator.Visible = false;
            }
        }
        else
        {
            //Hyperlink72.Enabled = false;
            hlinkReqRefund.Enabled = true;
            //Hyperlink87.Enabled = true;
            Hyperlink34.Visible = true;
            Hyperlink66.Visible = true;
            Hyperlink144.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("NatTechT"))
        {
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString(); // 93;
            trNationalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trRentalNeeds.Visible = false;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = false;
            DdlRole.Enabled = false;
            trBeeBook.Visible = false;
            BtnContinue.Enabled = false;
            trTeamLead.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trTeamMember.Visible = false;
            trAccountingFunctions.Visible = false;
            lnkPrepareFinals.Enabled = false;
            trCoachFunctions.Visible = false;
            trCoachFunctionsCoach.Visible = false;
            trMedalsCert.Visible = false;
            trFundRaising.Visible = false;
            trLaptopJudge.Visible = false;
            lnkRoomRequirement.Enabled = false;
            lnkRoomList.Enabled = false;
            lnkRentalNeeds.Enabled = false;
            lnkRoomSchedule.Enabled = false;
            lnkContestTeam.Enabled = false;
            lnkRoomGuides.Enabled = false;
            lnkGraders.Enabled = false;
            lnkAssignRolesFacilities.Enabled = false;
            TrNationalTechC.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trCoreTeamfunctions.Visible = true;
            lnkSBVBSelMatrixCoreT.Enabled = false;
            hylnkGenSBVBTestPaperCoreT.Enabled = false;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Count(*) From Volunteer Where MemberID=" + Convert.ToInt32(Session["LoginID"]) + " and TeamLead='Y' and RoleID=93")) > 0)
            {
                Hyperlink152.Enabled = true;
            }
            else //if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Count(*) From Volunteer Where MemberID=" + Convert.ToInt32(Session["LoginID"]) + " and TeamLead='N' ")) > 0)
            {
                Hyperlink152.Enabled = false;
            }
        }

        if (DdlRole.SelectedItem.Text.Contains("LiaisonIndia"))
        {
            TrIndiaScholarships.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("CollegeSch"))
        {
            TrIndiaScholarships.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("DesigSch"))
        {
            TrIndiaScholarships.Visible = true;
        }
        if (DdlRole.SelectedItem.Text.Contains("OWkshopC_National"))
        {
            trOnlineWScal.Visible = true;
            Session["LoginRole"] = "97";
            Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
            Session["RoleId"] = 97;
            txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
            txtRoleCategory.Enabled = false;
            txtRoleCode.Text = "OWkshopC";
            txtRoleCode.Enabled = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = false;
            trPersonal.Visible = false;
            trTeamLead.Visible = false;// true;
            trTeamMember.Visible = false;
            trWrkShop.Visible = false;
            DdlRole.Enabled = false;
            BtnContinue.Enabled = false;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=97 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
            {
                ddlChapterWkShop.Enabled = true;
            }
            else
            {
                ddlChapterWkShop.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=97 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.Enabled = false;
            }
        }
        else if (DdlRole.SelectedItem.Text.Contains("WkshopC"))
        {
            Session["LoginRole"] = "11";
            Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
            Session["RoleId"] = 11;
            txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
            txtRoleCategory.Enabled = false;
            txtRoleCode.Text = "WkshopC";
            txtRoleCode.Enabled = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;// true;
            trTeamMember.Visible = false;
            trWrkShop.Visible = true;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=11 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
            {
                ddlChapterWkShop.Enabled = true;
            }
            else
            {
                ddlChapterWkShop.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=11 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterWkShop.Enabled = false;
            }
        }
        if (DdlRole.SelectedItem.Text.Contains("PrepClubC"))
        {
            Session["LoginRole"] = "94";
            Session["LoginChapterID"] = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
            Session["RoleId"] = 94;
            txtRoleCategory.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
            txtRoleCategory.Enabled = false;
            txtRoleCode.Text = "PrepClubC";
            txtRoleCode.Enabled = false;
            trgamefunction.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trTeamLead.Visible = false;// true;
            trTeamMember.Visible = false;
            trWrkShop.Visible = false;
            trPrepClub.Visible = true;
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select Count(*) from Volunteer where roleID=94 And Memberid=" + Convert.ToInt32(Session["LoginID"]) + " and [National]='Y'")) > 0)
            {
                ddlChapterPrepClub.Enabled = true;
            }
            else
            {
                ddlChapterPrepClub.SelectedItem.Value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterPrepClub.SelectedItem.Text = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterCode from Volunteer where roleID=94 And Memberid=" + Session["LoginID"] + "").ToString();
                ddlChapterPrepClub.Enabled = false;
            }
        }
        if (DdlRole.SelectedItem.Text.Contains("CoreT"))
        {
            Session["LoginRole"] = DT.Rows[0].ItemArray[3].ToString();
            Session["RoleId"] = DT.Rows[0].ItemArray[2].ToString();
            trNationalCoordinator.Visible = false;
            trChapterCoordinator.Visible = false;
            trRentalNeeds.Visible = false;
            trClusterCoordinator.Visible = false;
            trZonalCoordinator.Visible = false;
            trCustomer.Visible = false;
            trFinalCoordinator.Visible = false;
            trTeamLead.Visible = false;
            trTeamMember.Visible = false;
            trAccountingFunctions.Visible = false;
            trTechnicalCoordinator.Visible = false;
            trBeeBook.Visible = false;
            lnkPrepareFinals.Enabled = false;
            trCoachFunctions.Visible = false;
            trCoachFunctionsCoach.Visible = false;
            trMedalsCert.Visible = false;
            trFundRaising.Visible = false;
            trLaptopJudge.Visible = false;
            lnkRoomRequirement.Enabled = false;
            lnkRoomList.Enabled = false;
            lnkRentalNeeds.Enabled = false;
            lnkRoomSchedule.Enabled = false;
            lnkContestTeam.Enabled = false;
            lnkRoomGuides.Enabled = false;
            lnkGraders.Enabled = false;
            lnkAssignRolesFacilities.Enabled = false;
            TrNationalTechC.Visible = false;
            TrIndiaScholarships.Visible = false;
            Hyperlink87.Enabled = false;
            trWrkShop.Visible = false;
            trPrepClub.Visible = false;

            trSATFunctionsAdmin.Visible = true;
            trGeneralFunctions.Visible = true;
            trPersonal.Visible = true;
            trCoreTeamfunctions.Visible = true;

            if (Convert.ToInt32(Session["RoleId"]) == 36)
            {
                trSATFunctionsAdmin.Visible = false;
                trCoreTeamfunctions.Visible = false;
            }

            // trSATFunctionsCoach.Visible = true;
        }
        if ((Convert.ToInt32(Session["RoleId"]) == 88) || (Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96))
        {
            Hyperlink15.Enabled = false;
            Hyperlink71.Enabled = false;
        }
        if (Convert.ToInt32(Session["RoleId"]) == 97)
        {
            trOnlineWScal.Visible = true;
        }
        ViewTeamLeadOrMember(TeamLead.Contains("Y"));
        hidelinksbasedonRoleID();
        hideSBVBTestPaperLink();
        visibleVolSignReportlinks();
    }
    protected void ViewTeamLeadOrMember(Boolean tl)
    {
        if (!string.IsNullOrEmpty(Session["RoleId"].ToString()))
        {
            DataSet dsTeamID = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select * from Role where RoleID=" + Session["RoleId"].ToString() + " and TeamID>0");
            if (((Convert.ToInt32(Session["RoleId"].ToString())) > 5) && tl && dsTeamID.Tables[0].Rows.Count > 0)
            {
                trTeamLead.Visible = true;
                trTeamMember.Visible = true;
            }
            if ((Convert.ToInt32(Session["RoleId"].ToString())) > 5 && !tl && dsTeamID.Tables[0].Rows.Count > 0)
            {
                trTeamMember.Visible = true;
            }
        }

    }
    protected void LinkViewRegistrations_Click(object sender, EventArgs e)
    {
        RedirectToViewRegistrations();
    }

    protected void LinkViewWorkshopRegistrations_Click(object sender, EventArgs e)
    {
        //set session variables for selected zone, cluster and chapter
        Session["EventId"] = 3;

        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;

        Session["selClusterID"] = DdlCluster.SelectedItem.Value;

        Session["selChapterID"] = ddlChapterWkShop.SelectedItem.Value;

        Response.Redirect("./Reports/workshopTmcList.aspx");
    }
    //protected void lnkviewbulletinboard_Click(object sender, EventArgs e)
    //{
    //    Session["SelChapterID"] = ddlChapter.SelectedValue.ToString();
    //    Response.Redirect("https://www.northsouth.org/bb/bulletin.asp");
    //}
    protected void lnkGenerateBadgeNumber_Click(object sender, EventArgs e)
    {
        Session["SelChapterID"] = ddlChapter.SelectedValue.ToString();
        Response.Redirect("BadgeNumbers.aspx");
    }
    protected void Hyperlink58_Click(object sender, EventArgs e)
    {
        Session["SelChapterID"] = ddlChapter.SelectedValue.ToString();
        Response.Redirect("GenerateBadges.aspx");
    }
    protected void HyperLink59_Click(object sender, EventArgs e)
    {
        Session["SelChapterID"] = ddlChapter.SelectedValue.ToString();
        Session["Page"] = "ChapterCoOrdinator";
        Response.Redirect("GenerateParticipantCertificates.aspx");
    }
    protected void lbtnSchedule_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedValue.ToString();
        Session["selChapterName"] = ddlChapter.SelectedItem.Text;
        Response.Redirect("ScheduleTechCoordinator.aspx");
    }
    protected void lnkUploadSign_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedValue.ToString();
        Session["selChapterName"] = ddlChapter.SelectedItem.Text;
        Response.Redirect("SignatureUpload.aspx");
    }
    protected void lnkMissingScores_Click_chapter(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedValue.ToString();
        Session["selChapterName"] = ddlChapter.SelectedItem.Text;
        Response.Redirect("MissingScores.aspx");
    }
    protected void lnkEthnicOrg_Click_chapter(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedValue.ToString();
        Session["selChapterName"] = ddlChapter.SelectedItem.Text;
        Response.Redirect("EthnicOrg.aspx");
    }
    protected void lnkPrepareFinals_Click(object sender, EventArgs e)
    {
        Session["EventID"] = 1;
        Session["LoginChapterID"] = 1;
        Session["selChapterID"] = 1;
        Session["EventYear"] = DateTime.Today.Year;
        Response.Redirect("AddUpdateContests.aspx");
    }
    protected void lnkWorkShopCal_Click(object sender, EventArgs e)
    {
        Session["EventID"] = 3;
        Session["selChapterID"] = ddlChapterWkShop.SelectedValue.ToString();
        Session["LoginChapterID"] = ddlChapterWkShop.SelectedValue.ToString();
        Session["selChapterName"] = ddlChapterWkShop.SelectedItem.Text;
        Response.Redirect("ChapterCoordinator/WorkShopCalendar.aspx");

    }

    protected void lnkDAS_Click(object sender, EventArgs e)
    {
        int RoleID = Convert.ToInt32(Session["RoleID"]);

        Session["EventID"] = 6;
        if ((RoleID == 1) || (RoleID == 2))
        {
            Session["ChapterID"] = null;
        }
        else
        {
            string strSql;
            strSql = "Select ChapterId, ChapterCode, RoleID,  ISNULL(Finals,''), [National] from Volunteer ";
            strSql = strSql + " where memberid = " + Session["LoginID"] + " and RoleID in (" + Session["RoleId"].ToString() + ")";
            SqlConnection con = new SqlConnection(Application["ConnectionString"].ToString());
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(con, CommandType.Text, strSql);
            if (RoleID == 5)
            {
                Session["ChapterID"] = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else if (RoleID == 37)
            {
                string Finals = ds.Tables[0].Rows[0][3].ToString();
                string National = ds.Tables[0].Rows[0][4].ToString();
                int ChapterID = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                if (Finals == "Y")
                {
                    lblerr.Text = "You cannot access this application";
                }
                if ((National == "Y") && (ChapterID != null))
                {
                    Session["ChapterID"] = ChapterID;
                }
                else
                {
                    Session["ChapterID"] = ChapterID;
                }
            }
            else if (RoleID == 38)
            {
                string Finals = ds.Tables[0].Rows[0][3].ToString();
                string National = ds.Tables[0].Rows[0][4].ToString();
                int ChapterID = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                if (Finals == "Y")
                {
                    lblerr.Text = "You cannot access this application";
                }
                if ((National == "Y") && (ChapterID != null))
                {
                    Session["ChapterID"] = ChapterID;
                }
                else
                {
                    Session["ChapterID"] = ChapterID;
                }
            }
        }

        Response.Redirect("Reg_DAS.aspx");

    }
    protected void hlinkdonate_Click(object sender, EventArgs e)
    {

        Session["EventID"] = 11;
        Session["CustIndID"] = Session["LoginID"];
        Response.Redirect("donor_donate.aspx");
    }


    protected void lnkGame_Click(object sender, EventArgs e)
    {
        Session["EventID"] = 4;
        int RoleID = Convert.ToInt32(Session["RoleID"]);
        if ((RoleID == 1) || (RoleID == 2) || (RoleID == 87))
        {
            Response.Redirect("GameSearch.aspx");
        }
        else
        {
            lblerr1.Text = "You dont have access to this Function";
        }


    }

    protected void LinkTopRanksList_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
        //if (ddlChapter.SelectedValue != "1")
        //{
        Response.Redirect("Reports/TopRanksList.aspx");
        //}
        //else if (ddlChapter.SelectedValue == "1")
        //{
        //  Response.Redirect("Reports/TopRanksListFinals.aspx");
        //}
    }
    protected void LinkTopRanksListFinals_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
        Response.Redirect("Reports/TopRanksListFinals.aspx");
    }
    protected void lnkListOfFinalists_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter.SelectedItem.Value;
        Response.Redirect("Reports/FinalistsReport.aspx");
    }
    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    string filePath = Server.MapPath ("include\\NSF_Contest_Guide_Dec2010.pdf");
    //    System.IO.FileInfo file = new System.IO.FileInfo(filePath);
    //    if (file.Exists)
    //    {
    //        Response.Clear();
    //        Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
    //        Response.AddHeader("Content-Length", file.Length.ToString());
    //        Response.ContentType = "application/octet-stream";
    //        Response.WriteFile(file.FullName);
    //        Response.End(); //if file does not exist
    //    }
    //    else
    //        Response.Write(filePath);
    //}
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        string filePath = Server.MapPath("include\\NSF_Contest_Guide_Dec2010.doc");
        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
        if (file.Exists)
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/octet-stream";
            Response.WriteFile(file.FullName);
            Response.End(); //if file does not exist
        }
        else
            Response.Write(filePath);
    }
    protected void lbtnTechCDnloadTstpapers_Click(object sender, EventArgs e)
    {
        if (DdlRole.Items.Count > 1)
            Response.Redirect("ManageTestPapers.aspx?id=" + DdlRole.SelectedValue);
        else if ((Session["RoleId"].ToString() == "9") || (Session["RoleId"].ToString() == "33"))
            Response.Redirect("ManageTestPapers.aspx?id=" + SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1  Volunteerid from volunteer where memberid=" + Session["LoginID"].ToString()));
        else
            Response.Redirect("ManageTestPapers.aspx");
    }
    protected void lbtnNatTechCDnloadTstpapers_Click(object sender, EventArgs e)
    {
        if (DdlRole.Items.Count > 1)
            Response.Redirect("ManageTestPapers.aspx?id=" + DdlRole.SelectedValue);
        else if ((Session["RoleId"].ToString() == "9") || (Session["RoleId"].ToString() == "33"))
            Response.Redirect("ManageTestPapers.aspx?id=" + SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1  Volunteerid from volunteer where memberid=" + Session["LoginID"].ToString()));
        else
            Response.Redirect("ManageTestPapers.aspx");
    }
    protected void ltbnTechCManageScoresheet_Click(object sender, EventArgs e)
    {
        if (DdlRole.Items.Count > 1)
            Response.Redirect("ManageScoresheet.aspx?id=" + DdlRole.SelectedValue);
        else if ((Session["RoleId"].ToString() == "9") || (Session["RoleId"].ToString() == "33"))
            Response.Redirect("ManageScoresheet.aspx?id=" + SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1  Volunteerid from volunteer where memberid=" + Session["LoginID"].ToString()));
        else
            Response.Redirect("ManageScoresheet.aspx");
    }
    protected void lbtnJudgeScoreSheet_Click(object sender, EventArgs e)
    {
        if (DdlRole.Items.Count > 1)
            Response.Redirect("ManageScoresheet.aspx?id=" + DdlRole.SelectedValue);
        else if ((Session["RoleId"].ToString() == "16") || (Session["RoleId"].ToString() == "18"))
            Response.Redirect("ManageScoresheet.aspx?id=" + SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1  Volunteerid from volunteer where memberid=" + Session["LoginID"].ToString()));
        else
            Response.Redirect("ManageScoresheet.aspx");
    }

    //protected void lbtnAbsenteesList_Click(object sender, EventArgs e)
    //{
    //    if (DdlRole.Items.Count > 1)
    //        Response.Redirect("ViewAbsenteeList.aspx?id=" + DdlRole.SelectedValue);
    //    else if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2")|| (Session["RoleId"].ToString() == "9")|| (Session["RoleId"].ToString() == "16")|| (Session["RoleId"].ToString() == "18"))
    //        Response.Redirect("ViewAbsenteeList.aspx?id=" + SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1  Volunteerid from volunteer where memberid=" + Session["LoginID"].ToString()));
    //    else
    //        Response.Redirect("ViewAbsenteeList.aspx");
    // }
    protected void lnkFundraising_Click(object sender, EventArgs e)
    {
        Session["EventID"] = 9;
        Session["CustIndID"] = Session["LoginID"];
        Response.Redirect("FundRReg.aspx");
    }
    protected void ddlChapterFundR_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterID"] = ddlChapterFundR.SelectedValue;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
    }
    protected void ddlChapter1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter1.SelectedItem.Value;
        Session["selChapterID"] = ddlChapter1.SelectedValue;
        Session["selChapterName"] = ddlChapter1.SelectedItem.Text;
    }
    protected void ddlChapterWkShop_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapterWkShop.SelectedItem.Value;
        Session["selChapterID"] = ddlChapterWkShop.SelectedValue;
        Session["selChapterName"] = ddlChapterWkShop.SelectedItem.Text;
    }
    protected void ddlChapterPrepClub_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapterPrepClub.SelectedItem.Value;
        Session["selChapterID"] = ddlChapterPrepClub.SelectedValue;
        Session["selChapterName"] = ddlChapterPrepClub.SelectedItem.Text;
    }
    protected void lnkAssignRolesFacilities_Click(object sender, EventArgs e)
    {
        Session["LoginChapterID"] = ddlChapter1.SelectedItem.Value;
        Session["selChapterID"] = ddlChapter1.SelectedItem.Value;
        Response.Redirect("VolunteerAssignRoles.aspx?ID=1");
    }

    protected void hlinkWalkathon_Click(object sender, EventArgs e)
    {
        Session["EventID"] = 5;
        Session["CustIndID"] = Session["LoginID"];
        Response.Redirect("Don_athon_selection.aspx?Ev=5");
    }

    protected void lnkPrepClub_Click(object sender, EventArgs e)
    {
        //set session variables for selected zone, cluster and chapter

        Session["EventID"] = 19;
        Session["selChapterID"] = ddlChapterPrepClub.SelectedValue.ToString();
        Session["LoginChapterID"] = ddlChapterPrepClub.SelectedValue.ToString();
        Session["selChapterName"] = ddlChapterPrepClub.SelectedItem.Text;
        Response.Redirect("ChapterCoordinator/PrepClubCalendar.aspx");
    }

    protected void LinkViewPrepClub_Click(object sender, EventArgs e)
    {
        Session["EventId"] = 19;
        Session["selZoneID"] = DdlZonalCoordinator.SelectedItem.Value;
        Session["selClusterID"] = DdlCluster.SelectedItem.Value;
        Session["selChapterID"] = ddlChapterPrepClub.SelectedItem.Value;
        Response.Redirect("./Reports/workshopTmcList.aspx");
    }

    protected void lnkOnlineWkShop_Click(object sender, EventArgs e)
    {
        //set session variables for selected zone, cluster and chapter

        Session["EventID"] = 20;
        Response.Redirect("OnlineWSCal.aspx");
    }

    protected void LinkViewOnlineWkShop_Click(object sender, EventArgs e)
    {
        Session["EventId"] = 20;
        Response.Redirect("./Reports/xlReportRegByOnLineWorkshop.aspx");
    }

    protected void lbtnNatTechCoach_Click(object sender, EventArgs e)
    {
        if (DdlRole.Items.Count > 1)
            Response.Redirect("ManageCoachPapers.aspx?id=" + DdlRole.SelectedValue);
        else if ((Session["RoleId"].ToString() == "9") || (Session["RoleId"].ToString() == "33"))
            Response.Redirect("ManageCoachPapers.aspx?id=" + SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1  Volunteerid from volunteer where memberid=" + Session["LoginID"].ToString()));
        else
            Response.Redirect("ManageCoachPapers.aspx");
    }
    protected void lbtnAppSATAdminTests_Click(Object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/Admin/AdminTests.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");
    }
    protected void lbtnAppSATAdminTestSections_Click(Object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/Admin/AdminTestSections.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");
    }
    protected void lbtnAppSATAdminAnswerKeys_Click(Object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/Admin/AdminAnswerKeys.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");
    }
    protected void lbtnAppSATAdminStudTests_Click(Object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/Admin/AdminStudentTests.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");
    }
    protected void lbtnAppSATAdminTestStud_Click(Object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/Admin/AdminTestStudents.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");
    }
    protected void lbtnAppSATChildTestAns_Click(Object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/ChildTestAnswers.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");
    }
    protected void lbtnAppSATWebForm1_Click(Object sender, System.EventArgs e)
    {
        //Response.Write("<script language='javascript'>window.open('https://www.northsouth.org/AppSAT/WebForm1.aspx','_blank','left=0,top=0,width=1000,height=1200,toolbar=0,location=0,scrollbars=1');</script> ");

    }
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManageCoachPapers.aspx");
    }
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        Response.Redirect("CoachRelDates.aspx");
    }
    protected void btnDownloadCoachPapers_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManageCoachPapers.aspx");
    }

    protected void bttnCoachPapersReleaseDates_Click(object sender, EventArgs e)
    {
        Response.Redirect("CoachRelDates.aspx");
    }
    protected void lbtnChildupdate_Click(object sender, EventArgs e)
    {
        Session["CustIndID"] = Session["LoginID"];
        Response.Redirect("MainChild.aspx?parentUpdate=true");
    }
    protected void lnkActList_Click(object sender, EventArgs e)
    {
        Response.Redirect("Activitylist.aspx");
    }
    protected void lnkSBVBSelMatrix_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddUpdateSBVBSelMatrix.aspx");
    }
    protected void lnkInventory_Click(object sender, EventArgs e)
    {
        Response.Redirect("Inventory.aspx");
    }
    protected void lnkEndownment_Click(object sender, EventArgs e)
    {
        Response.Redirect("Endowment.aspx");
    }
    protected void lbtUSSAwards_Click(object sender, EventArgs e)
    {
        Response.Redirect("UssAwards.aspx");
    }
    protected void lnkRestrictedFund_Click(object sender, EventArgs e)
    {
        Response.Redirect("RestrictedFunds.aspx");
    }
    protected void lnkGameRegistrations_Click(object sender, EventArgs e)
    {
        Response.Redirect("Game_Reg.aspx");
    }
    protected void LinkButton11_Click(object sender, EventArgs e)
    {
        Session["SmLogIDZone"] = DdlZonalCoordinator.SelectedItem.Value;
        Response.Redirect("SendEmailLog.aspx");
    }
    protected void LinkButton12_Click(object sender, EventArgs e)
    {
        Session["SmLogIDCluster"] = DdlCluster.SelectedItem.Value;
        Response.Redirect("SendEmailLog.aspx");
    }
    protected void LinkButton13_Click(object sender, EventArgs e)
    {
        if (ddlChapter.SelectedIndex > 0)
        {
            if (DdlCluster.SelectedIndex > 0)
            {
                Session["SmLogIDCluster"] = DdlCluster.SelectedItem.Value;
            }
            if (DdlZonalCoordinator.SelectedIndex > 0)
            {
                Session["SmLogIDZone"] = DdlZonalCoordinator.SelectedItem.Value;
            }
            Session["SmLogIDChapter"] = ddlChapter.SelectedItem.Value;
            Response.Redirect("SendEmailLog.aspx");
        }
    }
    protected void lnkWkShopEmail_Click(object sender, EventArgs e)
    {
        Session["Panel"] = "ChapterPanel";
        //if (ddlChapterWkShop.SelectedIndex > 0)
        //'{
        Session["mailChapterID"] = ddlChapterWkShop.SelectedItem.Value;
        Session["LoginChapterID"] = ddlChapterWkShop.SelectedItem.Value;
        Session["LoginChapterName"] = ddlChapterWkShop.SelectedItem.Text;
        //}
        Session["RoleID"] = 11;
        Response.Redirect("Email_CC.aspx");
    }
    protected void lnkPrepClubEmail_Click(object sender, EventArgs e)
    {
        Session["Panel"] = "ChapterPanel";
        //  if (ddlChapterPrepClub.SelectedIndex > 0)
        // {
        Session["mailChapterID"] = ddlChapterPrepClub.SelectedItem.Value;
        Session["LoginChapterID"] = ddlChapterPrepClub.SelectedItem.Value;
        Session["LoginChapterName"] = ddlChapterPrepClub.SelectedItem.Text;
        // }
        Session["RoleID"] = 94;
        Response.Redirect("Email_CC.aspx");
    }

    protected void lnkOnlineWkShopEmail_Click(object sender, EventArgs e)
    {
        // Session["RoleID"] = 94;
        Response.Redirect("Email_OnLineWkShop.aspx");
    }
    string ConnectionString = "ConnectionString";
    public void fillChapterBasedOnRole(DropDownList ddlChapter)
    {
        string cmdText = "";
        if ((Convert.ToInt32(Session["RoleId"]) == 1) || (Convert.ToInt32(Session["RoleId"]) == 2))
        {
            cmdText = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        }

        else if ((Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52))
        {
            cmdText = "Select MemberID, ChapterId,ChapterCode from volunteer where MemberID=" + Session["LoginID"] + "";

        }
        else if ((Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
        {
            cmdText = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        }
        else
        {
            cmdText = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        }

        try
        {
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddlChapter1.Enabled = true;
                ddlChapter1.DataSource = dschapter;
                ddlChapter1.DataTextField = "chaptercode";
                ddlChapter1.DataValueField = "ChapterID";
                ddlChapter1.DataBind();

                ddlChapter1.Items.Insert(0, new ListItem("Select Chapter", "0"));
                string chapterId = "";
                if ((Convert.ToInt32(Session["RoleId"]) == 5) || (Convert.ToInt32(Session["RoleId"]) == 52))
                {
                    if (dschapter.Tables[0].Rows.Count == 1)
                    {
                        chapterId = dschapter.Tables[0].Rows[0]["ChapterId"].ToString();
                        ddlChapter1.SelectedIndex = ddlChapter1.Items.IndexOf(ddlChapter1.Items.FindByValue(chapterId));
                        ddlChapter1.Enabled = false;
                    }
                    else
                    {
                        if ((Convert.ToInt32(Session["RoleId"]) == 5))
                        {
                            String str = "Select Count(*) From Volunteer Where Finals='Y' and EventId = 1 and MemberID=" + Session["LoginID"] + " and RoleId=5";// and RoleId=" + Session["LoginID"].ToString() + "";
                            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                            int fCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, str));
                            if (fCount > 0)
                            {
                                ddlChapter1.SelectedIndex = ddlChapter1.Items.IndexOf(ddlChapter1.Items.FindByValue("1"));
                                ddlChapter1.Enabled = false;
                            }
                        }
                    }
                }
                else if ((Convert.ToInt32(Session["RoleId"]) == 89) || (Convert.ToInt32(Session["RoleId"]) == 96) || (Convert.ToInt32(Session["RoleId"]) == 97))
                {
                    ddlChapter1.SelectedValue = "112";
                    ddlChapter1.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }



    protected void lnkbtnSupportticket_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapter.SelectedItem.Value;
        if (ddlChapter.SelectedItem.Value == "1")
            Response.Redirect("SupportTracking.aspx?NSFEvnt=1");
        else
            Response.Redirect("SupportTracking.aspx?NSFEvnt=2&ch=" + ddlChapter.SelectedItem.Value);
    }
    protected void lnkbtnPreclubSupporttracking_Click(object sender, EventArgs e)
    {
        //protected void LinkViewPrepClub_Click(object sender, EventArgs e)
        Session["selChapterID"] = ddlChapterPrepClub.SelectedItem.Value;
        Response.Redirect("SupportTracking.aspx?NSFEvnt=19&ch=" + ddlChapterPrepClub.SelectedItem.Value);
    }
    protected void lnkbtnWorkShopSupporttracking_Click(object sender, EventArgs e)
    {
        Session["selChapterID"] = ddlChapterWkShop.SelectedItem.Value;
        Response.Redirect("SupportTracking.aspx?NSFEvnt=3&ch=" + ddlChapterWkShop.SelectedItem.Value);
    }
    protected void LinkButton14_Click(object sender, EventArgs e)
    {
        Session["Panel"] = "ChapterPanel";
        if (ddlChapter1.SelectedIndex > 0)
        {
            Session["mailChapterID"] = ddlChapter1.SelectedItem.Value;
            Response.Redirect("SetUpTeams.aspx");
        }

    }

    protected void LnkFundRContestRegReport_Click(object sender, EventArgs e)
    {
        string chapterID = ddlChapterFundR.SelectedValue;
        Session["selChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
        Response.Redirect("Reports/FundRContestRegReport.aspx?Chap=" + chapterID + "");
    }
    protected void lnkFundRPendingRegReport_Click(object sender, EventArgs e)
    {
        string chapterID = ddlChapterFundR.SelectedValue;
        Session["selChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
        Response.Redirect("Reports/FundraisingPendReg.aspx?Chap=" + chapterID + "");
    }
    protected void LnkFundRSendEmail_Click(object sender, EventArgs e)
    {
        string chapterID = ddlChapterFundR.SelectedValue;
        Session["selChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
        Response.Redirect("EmailFundRaising.aspx");
    }
    protected void LnkCheckinList_Click(object sender, EventArgs e)
    {
        string chapterID = ddlChapterFundR.SelectedValue;
        Session["selChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
        Response.Redirect("FundRCheckinList.aspx?Chap=" + chapterID + "");
    }
    protected void LnkContestantList_Click(object sender, EventArgs e)
    {
        string chapterID = ddlChapterFundR.SelectedValue;
        Session["selChapterID"] = ddlChapterFundR.SelectedItem.Value;
        Session["selChapterName"] = ddlChapterFundR.SelectedItem.Text;
        Response.Redirect("FundRContestantList.aspx?Chap=" + chapterID + "");
    }
}



