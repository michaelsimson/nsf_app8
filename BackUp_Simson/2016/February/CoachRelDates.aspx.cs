﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Globalization;
using System.Drawing;

public partial class CoachRelDates : System.Web.UI.Page
{
    int cpId;
    int Eyear;
    string PGC;
    string PC;
    string sLevel;
    string mId;
    EntityTestPaper m_objETP = new EntityTestPaper();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = 4240;
        //Session["RoleId"] = "1";

        lblNoPermission.Visible = false;

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            //Label16.Text = Session["LoginID"].ToString();
        }
        if (!Page.IsPostBack)
        {
            try
            {
                GetDropDownChoice(dllfileChoice, true);
                dllfileChoice.SelectedIndex = 0;
                if (Session["RoleId"].ToString() == "88")
                {
                    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                    string strSql = "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=88 and TeamLead='Y' and MemberID=" + Session["LoginID"].ToString();
                    int mcount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strSql));

                    if (mcount > 0)
                    {
                        hdntlead.Value = "Y";
                    }
                    else
                    {
                        hdntlead.Value = "N";
                    }
                }
            }
            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
        }
    }

    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (dllfileChoice.SelectedIndex == 1)
            {

                GetFlrContextYear(ddlFlrYear);
                LoadAddRelDates();
                fillCoach(ddlFlrYear.SelectedValue, ddlFlrEvent.SelectedValue, DDLMCoach);

                //Label9.Text = "";
                //Label15.Text = "";
                //lblQR.Text = "";
                //lblQD.Text = "";
                //lblAR.Text = "";
                //lblAD.Text = "";

                //tboxQrelease.Text = "";
                //tboxQdeadline.Text = "";
                //tboxArelease.Text = "";
                //tboxSrelease.Text = "";
                //tboxQrelease.Enabled = false;
                //tboxQdeadline.Enabled = false;
                //tboxArelease.Enabled = false;
                //tboxSrelease.Enabled = false;
                //ddlSession.Enabled = false;

                //GetFlrContextYear(ddlFlrYear);
                //PopulateEvents(ddlFlrEvent, false);
                //GetProductGroupCodes(ddlFlrProductGroup, true);

                //GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true,ddlFlrYear );
                ////GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);

                //PopulatePaperType(ddlFlrPaperType, true);
                //GetWeek(ddlFlrWeek, true);
                //PopulateLevel(ddlFlrLevel, "NA", true);
                //PopulateSets(ddlFlrSet, true);

                //Panel1.Visible = true;
                //Panel3.Visible = false;
                //Panel6.Visible = false;
                //Panel7.Visible = false;
                //gvEdit.Visible = false;
                //gvTestPapers.DataSource = null;
                //gvTestPapers.DataBind();
                //gvTestPapers.Visible = true;

                //if (Session["RoleId"].ToString() == "88")
                //{
                //    PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup),ddlFlrYear);
                //}
                //else
                //{
                //    PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true);
                //}

            }
            else
            {
                GetFlrContextYear(ddlFlrvYear);
                LoadModifyRelDates();
                fillCoach(ddlFlrvYear.SelectedValue, ddlFlrvEvent.SelectedValue, DDLACoach);
                //Label9.Text = "";
                //Label15.Text = "";
                //Label11.Text = "";
                //Label12.Text = "";
                //Label13.Text = "";
                //Label14.Text = "";
                //txtQR.Text = "";
                //txtQD.Text = "";
                //txtSD.Text = "";
                //txtAD.Text = "";
                //txtQR.Enabled = false;
                //txtQD.Enabled = false;
                //txtAD.Enabled = false;
                //txtSD.Enabled = false;
                //ddlSessionM.Enabled = false;

                //GetFlrContextYear(ddlFlrvYear);
                //PopulateEvents(ddlFlrvEvent, false);
                //GetProductGroupCodes(ddlFlrvProductGroup, true);
                //GetProductCodes(PGId(ddlFlrvProductGroup), ddlFlrvProduct, true,ddlFlrvYear );

                ////GetProductGroupCodes(ddlFlrvProductGroup, true);
                ////GetProductCodes(int.Parse(ddlFlrvProductGroup.SelectedValue), ddlFlrvProduct, true);
                //PopulatePaperType(ddlFlrvPaperType, true);
                //GetWeek(ddlFlrvWeek, true);
                //PopulateSets(ddlFlrvSet, true);
                //PopulateLevel(ddlFlrvLevel, "NA", true);
                //lblError.Text = "";
                //SearchCoachRelDate(true);
                //Panel1.Visible = false;
                //Panel3.Visible = true;
                //Panel7.Visible = false;
                //Panel6.Visible = true;
                //gvEdit.DataSource = null;
                //gvEdit.DataBind();
                //gvEdit.Visible = true;
                //gvTestPapers.Visible = false;

                //if (Session["RoleId"].ToString() == "88")
                //{
                //    PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlFlrvProductGroup),ddlFlrvYear );
                //}
                //else
                //{
                //    PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);
                //}
                //// PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);

                //GetCoachNames(ddlNameM, Int32.Parse(Session["RoleID"].ToString()), Int32.Parse(Session["LoginID"].ToString()), PGCode(ddlFlrvProductGroup), 2012, true);
                //GetSessionNumbers(ddlSessionM, Int32.Parse(ddlNameM.SelectedValue.ToString()), -1, false,ddlFlrvYear );
                //ddlSessionM.Items.Insert(0, new ListItem(" ", "-1"));
                //ddlSessionM.SelectedIndex = 0;
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void LoadAddRelDates()
    {
        lblError.Text = "";
        lblSearchErr.Text = "";
        Label9.Text = "";
        Label15.Text = "";
        lblQR.Text = "";
        lblQD.Text = "";
        lblAR.Text = "";
        lblAD.Text = "";

        tboxQrelease.Text = "";
        tboxQdeadline.Text = "";
        tboxArelease.Text = "";
        tboxSrelease.Text = "";
        tboxQrelease.Enabled = false;
        tboxQdeadline.Enabled = false;
        tboxArelease.Enabled = false;
        tboxSrelease.Enabled = false;
        ddlSession.Enabled = false;

        Panel1.Visible = true;
        Panel3.Visible = false;

        // GetFlrContextYear(ddlFlrYear);
        PopulateEvents(ddlFlrEvent, false);
        // GetProductGroupCodes(ddlFlrProductGroup, true, int.Parse(ddlFlrYear.SelectedValue), DDLMCoach);

        //GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true, ddlFlrYear);
        //GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);

        PopulatePaperType(ddlFlrPaperType, true);
        GetWeek(ddlFlrWeek, true);
        //PopulateLevel(ddlFlrLevel, "NA", true);
        // loadlevel();
        PopulateSets(ddlFlrSet, true);

        //Panel1.Visible = true;
        //Panel3.Visible = false;
        Panel6.Visible = false;
        Panel7.Visible = false;
        gvEdit.Visible = false;
        gvTestPapers.DataSource = null;
        gvTestPapers.DataBind();
        gvTestPapers.Visible = true;

        if (Session["RoleId"].ToString() == "88")
        {
            //PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);
        }
        else
        {
            //PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true);
        }

    }
    protected void LoadModifyRelDates()
    {

        lblError.Text = "";
        lblSearchErr.Text = "";
        Label9.Text = "";
        Label15.Text = "";
        Label11.Text = "";
        Label12.Text = "";
        Label13.Text = "";
        Label14.Text = "";
        txtQR.Text = "";
        txtQD.Text = "";
        txtSD.Text = "";
        txtAD.Text = "";
        txtQR.Enabled = false;
        txtQD.Enabled = false;
        txtAD.Enabled = false;
        txtSD.Enabled = false;
        ddlSessionM.Enabled = false;

        Panel1.Visible = false;
        Panel3.Visible = true;

        //GetFlrContextYear(ddlFlrvYear);

        PopulateEvents(ddlFlrvEvent, false);
        // GetProductGroupCodes(ddlFlrvProductGroup, true, int.Parse(ddlFlrvYear.SelectedValue), DDLACoach);
        //GetProductCodes(PGId(ddlFlrvProductGroup), ddlFlrvProduct, true, ddlFlrvYear);

        //GetProductGroupCodes(ddlFlrvProductGroup, true);
        //GetProductCodes(int.Parse(ddlFlrvProductGroup.SelectedValue), ddlFlrvProduct, true);
        PopulatePaperType(ddlFlrvPaperType, true);
        GetWeek(ddlFlrvWeek, true);
        PopulateSets(ddlFlrvSet, true);
        // PopulateLevel(ddlFlrvLevel, "NA", true);
        lblError.Text = "";
        SearchCoachRelDate(true);
        //Panel1.Visible = false;
        //Panel3.Visible = true;
        Panel7.Visible = false;
        Panel6.Visible = true;
        gvEdit.DataSource = null;
        gvEdit.DataBind();
        gvEdit.Visible = true;
        gvTestPapers.Visible = false;

        if (Session["RoleId"].ToString() == "88")
        {
            //PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlFlrvProductGroup), ddlFlrvYear);
        }
        else
        {
            //PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);
        }
        // PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);

        GetCoachNames(ddlNameM, Int32.Parse(Session["RoleID"].ToString()), Int32.Parse(Session["LoginID"].ToString()), PGCode(ddlFlrvProductGroup), Convert.ToInt32(ddlFlrvYear.SelectedItem.Text), true);
        GetSessionNumbers(ddlSessionM, Int32.Parse(ddlNameM.SelectedValue.ToString()), -1, false, ddlFlrvYear);
        ddlSessionM.Items.Insert(0, new ListItem(" ", "-1"));
        ddlSessionM.SelectedIndex = 0;
    }


    #region Modify Records

    protected void gvEdit_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow row = gvEdit.SelectedRow;
            //  CoaID = Int32.Parse(row.Cells[0].Text);
            ddlSessionM.Items.Clear();
            ddlSessionM.Items.Insert(0, new ListItem(row.Cells[13].Text, row.Cells[13].Text));
            ddlSessionM.SelectedIndex = 0;
            //ddlSessionM.Text = row.Cells[13].Text;
            txtQR.Text = row.Cells[21].Text.Replace("-", "/");
            txtQD.Text = row.Cells[22].Text.Replace("-", "/");
            txtAD.Text = row.Cells[23].Text.Replace("-", "/");
            txtSD.Text = row.Cells[24].Text.Replace("-", "/");
            btnUpdate.Enabled = true;
            txtQR.Enabled = true;
            txtQD.Enabled = true;
            txtAD.Enabled = true;
            txtSD.Enabled = true;
            ddlSessionM.Enabled = false;// true;
            //if (!IsPostBack == true)
            //{
            Page.RegisterStartupScript("myScript", "<script language=JavaScript> $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });</script>");
            //}
            //CoaID = Int32.Parse(gvTestPapers.DataKeys[0].Values["CoachPaperId"].ToString());
            btnDelete.Enabled = true;
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    protected void gvEdit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        e.Row.Cells[0].Enabled = false;
        if (Session["RoleID"].ToString() == "88" || Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96")
        {
            e.Row.Cells[0].Enabled = true;
        }
    }
    protected void gvEdit_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument.ToString());
        GridViewRow row = gvEdit.Rows[index];

        int CoachPaperID = Convert.ToInt32(gvEdit.DataKeys[index].Value);
        Session["UpdCoachPaperID"] = CoachPaperID;

        string CoachID = ((Label)gvEdit.Rows[index].FindControl("lblCMemberID") as Label).Text;
        DDLACoach.SelectedValue = CoachID;
       // ddlNameM.SelectedValue = CoachID;

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        bool EFlag = false;
        int CoaID;
        //int sNumber;
        string[] formats = new string[] { "MM/dd/yyyy" };
        Label15.Text = "";
        Label11.Text = "";
        Label12.Text = "";
        Label13.Text = "";
        Label14.Text = "";
        Label16.Text = "";
        DateTime QRDate;
        DateTime QDDate;
        DateTime ARDate;
        DateTime SRDate;

        if (!DateTime.TryParseExact(txtQR.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QRDate))
        {
            Label11.Text = "Invalid Q-Release Date";
            EFlag = true;
        }

        if (!DateTime.TryParseExact(txtQD.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QDDate))
        {
            Label12.Text = "Invalid Q-Deadline Date";
            EFlag = true;
        }
        else if (DateTime.Compare(QDDate, QRDate) < 1)
        {
            Label12.Text = "Q-Deadline Date must be greater than Q-Release Date";
            EFlag = true;
        }
        if (!DateTime.TryParseExact(txtAD.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out ARDate))
        {
            Label13.Text = "Invalid A-Release Date";
            EFlag = true;
        }
        else if (DateTime.Compare(ARDate, QDDate) < 1)
        {
            Label13.Text = "A-Release Date must be greater than Q-Deadline Date";
            EFlag = true;
        }

        if (!DateTime.TryParseExact(txtSD.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out SRDate))
        {
            Label14.Text = "Invalid S-Release Date";
            EFlag = true;
        }
        else if ((QRDate - SRDate).Days > 7)//(DateTime.Compare(QRDate, SRDate) > 7)//, DateTime.Today.Date) < 0)
        {
            lblAD.Text = "S-Release Date must be less than or equal to Q-Release by 7 days";
            EFlag = true;
        }
        //else if (DateTime.Compare(SRDate, DateTime.Today.Date) < 0)
        //{
        //    Label14.Text = "Date can't be in the Past";
        //    EFlag = true;
        //}
        //else if (DateTime.Compare(SRDate, QDDate) < 1)
        //{
        //    Label14.Text = "S-Release Date must be greater than Q-Deadline Date";
        //    EFlag = true;
        //}


        //bool result = Int32.TryParse(txtSession.Text, out sNumber);

        if (ddlSessionM.SelectedItem.Text == "")
        {
            Label16.Text = "Invalid Session Number";
            EFlag = true;
        }

        if (EFlag == true)
        {
            return;
        }

        //GridViewRow row = gvEdit.SelectedRow;
        CoaID = Convert.ToInt32(Session["UpdCoachPaperID"]);// Int32.Parse(gvEdit.DataKeys[0].Values["CoachRelID"].ToString());
        UpdateCoachRelDate(CoaID);
        //SearchCoachRelDate(false);
        populateRelDates();
    }

    protected void ddlFlrvProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GetProductCodes(int.Parse(ddlFlrvProductGroup.SelectedValue), ddlFlrvProduct, true);
        GetProductCodes(PGId(ddlFlrvProductGroup), ddlFlrvProduct, true, ddlFlrvYear);
        GetCoachNames(ddlNameM, Int32.Parse(Session["RoleID"].ToString()), Int32.Parse(Session["LoginID"].ToString()), PGCode(ddlFlrvProductGroup), Convert.ToInt32(ddlFlrvYear.SelectedItem.Text), true);
        //  PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);
        PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlFlrvProductGroup), ddlFlrvYear);
    }

    protected void btnvReset_Click(object sender, EventArgs e)
    {
        ddlFlrvProduct.SelectedIndex = 0;
        ddlFlrvProductGroup.SelectedIndex = 0;
        ddlFlrvWeek.SelectedIndex = 0;
        ddlFlrvSet.SelectedIndex = 0;
        ddlFlrvPaperType.SelectedIndex = 1;// 0;
        ddlFlrvEvent.SelectedIndex = 0;
        ddlFlrvLevel.SelectedIndex = 0;

        SearchCoachRelDate(false);
        txtAD.Text = "";
        txtQR.Text = "";
        txtQD.Text = "";
        txtSD.Text = "";
        txtAD.Enabled = false;
        txtSD.Enabled = false;
        txtQR.Enabled = false;
        txtQD.Enabled = false;
        ddlSessionM.Enabled = false;
        Label11.Text = "";
        Label12.Text = "";
        Label13.Text = "";
        Label14.Text = "";
        Label15.Text = "";
        Label16.Text = "";
        btnUpdate.Enabled = false;
        btnDelete.Enabled = false;

    }

    protected void btnvSerarch_Click(object sender, EventArgs e)
    {
        GetSessionNumbers(ddlSessionM, Int32.Parse(DDLACoach.SelectedValue.ToString()), -1, true, ddlFlrvYear);
        if (dllfileChoice.SelectedIndex == 1)
        {
        }
        else if (dllfileChoice.SelectedIndex == 2)
        {
            if (DDLACoach.SelectedValue == "0")
            {
                SearchCoachRelDate(false);
            }
            else
            {
                populateRelDates();

            }
        }

        txtAD.Text = "";
        txtQR.Text = "";
        txtQD.Text = "";
        txtSD.Text = "";
        Label15.Text = "";
        Label9.Text = "";
        btnUpdate.Enabled = false;
        btnDelete.Enabled = false;
    }

    #endregion

    #region "Add Records

    protected void Button1_Click2(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        Label15.Text = "";
        string[] formats = new string[] { "MM/dd/yyyy" };
        lblsession.Text = "";
        lblQR.Text = "";
        lblQD.Text = "";
        lblAR.Text = "";
        lblAD.Text = "";
        DateTime QRDate;
        DateTime QDDate;
        DateTime ARDate;
        DateTime SRDate;

        if ((Session["LoginId"] != null))
        {
            mId = Session["LoginId"].ToString();
        }

        if (DDLMCoach.Enabled = true && DDLMCoach.Items.Count > 0)
        {
            mId = DDLMCoach.SelectedValue.ToString();
        }
        GridViewRow row = gvTestPapers.SelectedRow;
        cpId = Int32.Parse(row.Cells[2].Text);
        Eyear = Int32.Parse(row.Cells[3].Text);
        PGC = row.Cells[9].Text;
        PC = row.Cells[10].Text;
        sLevel = row.Cells[11].Text;
        string Strsql = "select count (CoachRelID) from CoachRelDates where  CoachPaperId =" + cpId + " and MemberID =" + mId + " and EventYear = " + Eyear + " and ProductGroupCode = '" + PGC + "' and ProductCode = '" + PC + "' and [Level] = '" + sLevel + "' and [session] = " + ddlSession.SelectedItem.Text;// +" and [QReleaseDate] = '" + QRDate.ToString() + "' and [QDeadlineDate] ='" + QDDate.ToString() + "' and [AReleaseDate] ='" + ARDate.ToString() + "' and [SReleaseDate] ='" + SRDate.ToString() + "'";

        if (ddlSession.Enabled = true & ddlSession.SelectedValue.ToString() == "-1")
        {
            lblsession.Text = "Invalid Session ";
            ErrFlag = true;
        }
        else
        {
            ddlSession.Enabled = true;
        }
        if (!DateTime.TryParseExact(tboxQrelease.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QRDate))
        {
            lblQR.Text = "Invalid Q-Release Date";
            ErrFlag = true;
        }
        else if (DateTime.Compare(QRDate, DateTime.Today.Date) < 0)
        {
            lblQR.Text = "Date Must be in future";
            ErrFlag = true;
        }

        if (!DateTime.TryParseExact(tboxQdeadline.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QDDate))
        {
            lblQD.Text = "Invalid Q-Deadline Date";
            ErrFlag = true;
        }
        else if (DateTime.Compare(QDDate, QRDate) < 1)
        {
            lblQD.Text = "Q-Deadline Date must be greater than Q-Release Date";
            ErrFlag = true;
        }

        if (!DateTime.TryParseExact(tboxArelease.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out ARDate))
        {
            lblAR.Text = "Invalid A-Release Date";
            ErrFlag = true;
        }
        else if (DateTime.Compare(ARDate, QDDate) < 1)
        {
            lblAR.Text = "A-Release Date must be greater than Q-Deadline Date";
            ErrFlag = true;
        }

        if (!DateTime.TryParseExact(tboxSrelease.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out SRDate))
        {
            lblAD.Text = "Invalid S-Release Date";
            ErrFlag = true;
        }
        // else if (DateTime.Compare(SRDate, DateTime.Today.Date) < 0)
        else if ((QRDate - SRDate).Days > 7)// (DateTime.Compare(QRDate, SRDate)> 7)//, DateTime.Today.Date) < 0)
        {
            lblAD.Text = "S-Release Date must be less than or equal to Q-Release by 7 days";
            ErrFlag = true;
        }

        if (ErrFlag == false)
        {
            int cnt = Convert.ToInt16(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Strsql));
            if (cnt > 0)
            {
                Label9.ForeColor = System.Drawing.Color.Red;
                Label9.Text = "Record Already exists with similar entries";
                return;
            }
        }

        if (ErrFlag == true)
        {
            return;
        }

        Label9.Text = "";
        int CoaID = -1;
        CoaID = Int32.Parse(row.Cells[2].Text);
        //CoaID = Int32.Parse(gvTestPapers.DataKeys[0].Values["CoachPaperId"].ToString());
        InsrtCoachRelDate(CoaID);
        btnSearch_Click(null, null);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {
            ddlFlrProduct.SelectedIndex = 0;
            ddlFlrProductGroup.SelectedIndex = 0;
            m_objETP.ProductId = PGId(ddlFlrProduct);
            m_objETP.ProductCode = PGCode(ddlFlrProduct);
            m_objETP.ProductGroupId = PGId(ddlFlrProductGroup);
            m_objETP.ProductGroupCode = PGCode(ddlFlrProductGroup);

            ddlFlrWeek.SelectedIndex = 0;
            ddlFlrSet.SelectedIndex = 0;
            ddlFlrPaperType.SelectedIndex = 1;// 0;
            ddlFlrEvent.SelectedIndex = 0;
            ddlFlrLevel.SelectedIndex = 0;
            m_objETP.DocType = "Q";
            lblQR.Text = "";
            lblQD.Text = "";
            lblAR.Text = "";
            lblAD.Text = "";
            tboxArelease.Text = "";
            tboxQrelease.Text = "";
            tboxQdeadline.Text = "";
            tboxSrelease.Text = "";
            lblsession.Text = "";
            Label9.Text = "";

            if (ddlFlrLevel.SelectedValue != "-1")
            {
                m_objETP.Level = ddlFlrLevel.SelectedItem.Text;
            }

            GetTestPapers(m_objETP);
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());

        }
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {
        Label9.Text = "";
        tboxQrelease.Text = string.Empty;
        tboxQdeadline.Text = string.Empty;
        tboxArelease.Text = string.Empty;
        tboxSrelease.Text = string.Empty;
        ddlSession.SelectedIndex = 0;
        Button1.Enabled = true;
        tboxQrelease.Enabled = true;
        tboxQdeadline.Enabled = true;
        tboxArelease.Enabled = true;
        tboxSrelease.Enabled = true;
        GridViewRow row = gvTestPapers.SelectedRow;
        Page.RegisterStartupScript("myScript", "<script language=JavaScript> $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });</script>");
        ddlSession.Enabled = true;
        //GetSessionNumbers(ddlSession, Int32.Parse(ddlName.SelectedValue.ToString()), -1, true);

    }

    protected void gvTestPapers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        // if (Session["RoleID"].ToString() == "88" || Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96")
        //{
        //    e.Row.Cells[0].Enabled = true;
        //}
        e.Row.Cells[0].Enabled = false;
        if (Session["RoleID"].ToString() == "88" || Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96")
        {
            e.Row.Cells[0].Enabled = true;
        }
        else
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string strSql = "";
                int cellText = Convert.ToInt32(e.Row.Cells[2].Text);
                int EventYear = Convert.ToInt32(e.Row.Cells[3].Text);

                strSql = "SELECT DISTINCT count(SessionNo) as sCount  FROM CoachReg WHERE CoachReg.EventYear = " + EventYear + " AND CoachReg.CMemberID = " + Int32.Parse(Session["LoginId"].ToString()) + " AND coachreg.SessionNo not in (select [session] from CoachRelDates where EventYear = " + EventYear + " and MemberID = " + Session["LoginId"].ToString() + " and CoachPaperId = " + cellText.ToString() + ")"; // DateTime.Now.Year.ToString() // 
                int cnt = Convert.ToInt16(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql));
                if (cnt == 0)
                {
                    e.Row.Cells[0].Enabled = false;
                }
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        m_objETP.ProductId = PGId(ddlFlrProduct);
        m_objETP.ProductGroupId = PGId(ddlFlrProductGroup);
        m_objETP.ProductCode = PGCode(ddlFlrProduct);
        m_objETP.ProductGroupCode = PGCode(ddlFlrProductGroup);
        m_objETP.WeekId = int.Parse(ddlFlrWeek.SelectedValue);
        m_objETP.SetNum = int.Parse(ddlFlrSet.SelectedValue);
        m_objETP.DocType = "Q";
        m_objETP.EventId = int.Parse(ddlFlrEvent.SelectedValue);
        lblQR.Text = "";
        lblQD.Text = "";
        lblAR.Text = "";
        lblAD.Text = "";
        tboxArelease.Text = "";
        tboxQrelease.Text = "";
        tboxQdeadline.Text = "";
        tboxSrelease.Text = "";
        lblsession.Text = "";
        //Label9.Text = "";
        //Response.Write(PGId(ddlFlrProduct) + PGCode(ddlFlrProduct));

        if (ddlFlrPaperType.SelectedValue != "-1")
        {
            m_objETP.PaperType = ddlFlrPaperType.SelectedValue; //Modified 27/07/2013
        }
        m_objETP.Description = string.Empty;// tbxFlrDescription.Text;
        m_objETP.TestFileName = string.Empty;// tbxFlrTestFileName.Text;
        if (ddlFlrLevel.SelectedValue != "-1" && ddlFlrLevel.SelectedValue != "")
        {
            m_objETP.Level = ddlFlrLevel.SelectedItem.Text;
        }
        GetSessionNumbers(ddlSession, Int32.Parse(DDLMCoach.SelectedValue.ToString()), -1, true, ddlFlrYear);
        GetTestPapers(m_objETP);
        ddlSession.Enabled = false;

    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        int CoaID = -1;
        GridViewRow row = gvTestPapers.SelectedRow;
        CoaID = Int32.Parse(row.Cells[0].Text);
        InsrtCoachRelDate(CoaID);
    }

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
    }
    protected void gvTestPapers_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument.ToString());
        GridViewRow row = gvTestPapers.Rows[index];

        int CoachPaperID = Convert.ToInt32(gvTestPapers.DataKeys[index].Value);
        //GetSessionNumbers(ddlSession, Int32.Parse(ddlName.SelectedValue.ToString()), CoachPaperID, true, ddlFlrYear);
    }
    protected void ddlFlrProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //GetProductCodes(int.Parse(ddlFlrProductGroup.SelectedValue), ddlFlrProduct, true);
            GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true, ddlFlrYear);
            GetCoachNames(ddlName, Int32.Parse(Session["RoleID"].ToString()), Int32.Parse(Session["LoginID"].ToString()), PGCode(ddlFlrProductGroup), Int32.Parse(ddlFlrYear.SelectedValue), true);//DateTime.Now.Year.ToString()
            GetSessionNumbers(ddlSession, Int32.Parse(ddlName.SelectedValue.ToString()), -1, true, ddlFlrYear);
            // PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true);
            if (Session["RoleId"].ToString() == "88")
            {
                PopulateLevelNew(ddlFlrLevel, true, PGId(ddlFlrProductGroup), ddlFlrYear);

            }
            else
            {
                //PopulateLevel(ddlFlrLevel, ddlFlrProductGroup.SelectedItem.Text, true);
                loadlevel();
            }
            //PopulateLevelNew(ddlFlrLevel, true); 
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    #endregion

    #region "Private Methods"
    private void SearchCoachRelDate(bool FT)
    {

        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_CoachRelDate_Search";

        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@ProductId", PGId(ddlFlrvProduct));//  Int32.Parse(ddlFlrvProduct.SelectedValue));
        param[1] = new SqlParameter("@ProductGroupId", PGId(ddlFlrvProductGroup)); // Int32.Parse(ddlFlrvProductGroup.SelectedValue));
        param[2] = new SqlParameter("@EventYear", Int32.Parse(ddlFlrvYear.SelectedValue));
        param[3] = new SqlParameter("@WeekId", Int32.Parse(ddlFlrvWeek.SelectedValue));
        param[4] = new SqlParameter("@SetNum", Int32.Parse(ddlFlrvSet.SelectedValue));

        param[6] = new SqlParameter("@MemberId", Int32.Parse(Session["LoginID"].ToString()));
        param[6] = new SqlParameter("@MemberId", null);
        param[7] = new SqlParameter("@RoleId", Int32.Parse(Session["RoleId"].ToString()));

        if (ddlFlrvLevel.SelectedValue != "-1" && ddlFlrvLevel.SelectedValue != "")
        {
            param[8] = new SqlParameter("@Level", ddlFlrvLevel.SelectedItem.Text);
        }
        else
        {
            param[8] = new SqlParameter("@Level", DBNull.Value);
        }

        if (ddlFlrvPaperType.SelectedValue != "-1")
        {
            param[5] = new SqlParameter("@PaperType", ddlFlrvPaperType.SelectedValue);
        }
        else
        {
            param[5] = new SqlParameter("@PaperType", DBNull.Value);
        }

        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvEdit.DataSource = ds;
                gvEdit.DataBind();
                //   gvEdit.SelectedIndex = -1;
                lblError.Text = "";

                Panel7.Visible = true;


            }
            else
            {
                gvEdit.DataSource = null;
                gvEdit.DataBind();
                Panel7.Visible = false;
                if (FT == false)
                {
                    lblError.Text = "Sorry your selection criteria didn't match with any record.";
                }
            }

        }
        catch (SqlException ex)
        {
            lblError.ForeColor = System.Drawing.Color.Red;
            //Label15.ForeColor = System.Drawing.Color.Red;
            // Label15.Text = ex.Message.ToString();
            lblError.Text = ex.Message.ToString();
        }


    }

    private void SearchModify()
    {
        String StrSQL = "SELECT DISTINCT CP.CoachPaperId, CP.ProductId, CP.EventId, CP.PaperType, CP.DocType,";
        StrSQL = StrSQL + " CP.ProductCode, CP.ProductGroupCode, CP.WeekId, CP.SetNum, CR.QReleaseDate,";
        StrSQL = StrSQL + " CR.QDeadlineDate, CR.AReleaseDate, CR.SReleaseDate, CR.CoachRelID";
        StrSQL = StrSQL + " FROM CoachPapers CP INNER JOIN CoachRelDates CR ON CR.CoachPaperId=CP.CoachPaperId";
        StrSQL = StrSQL + " ORDER BY CP.CoachPaperId";

        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvEdit.DataSource = ds;
            gvEdit.DataBind();
        }
        else
        {
            //LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
        }
    }

    private void FillModifyGrid()
    {

        String StrSQL = "SELECT DISTINCT CP.CoachPaperId, CP.ProductId, CP.EventId, CP.PaperType, CP.DocType,";
        StrSQL = StrSQL + " CP.ProductCode, CP.ProductGroupCode, CP.WeekId, CP.SetNum, CR.QReleaseDate,";
        StrSQL = StrSQL + " CR.QDeadlineDate, CR.AReleaseDate, CR.SReleaseDate, CR.CoachRelID";
        StrSQL = StrSQL + " FROM CoachPapers CP INNER JOIN CoachRelDates CR ON CR.CoachPaperId=CP.CoachPaperId";
        StrSQL = StrSQL + " ORDER BY CP.CoachPaperId";

        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvEdit.DataSource = ds;
            gvEdit.DataBind();
        }
        else
        {
            //LblexamRecErr.Text = "Sorry no records to Display. You can download only 10 days before contest.";
        }

    }

    private void InsrtCoachRelDate(int CoId)
    {
        string[] formats = { "MM/dd/yyyy" };
        int CoachId = CoId;
        try
        {
            DateTime QRDate = DateTime.ParseExact(tboxQrelease.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            DateTime QDDate = DateTime.ParseExact(tboxQdeadline.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            DateTime ARDate = DateTime.ParseExact(tboxArelease.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            DateTime SRDate = DateTime.ParseExact(tboxSrelease.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            string sqlCommand = "usp_Insert_CoachRelDate";
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@CoachPaperId", CoachId);
            param[1] = new SqlParameter("@Phase", "1");
            param[2] = new SqlParameter("@SessionNo", ddlSession.SelectedValue);
            param[3] = new SqlParameter("@QReleaseDate", QRDate);
            param[4] = new SqlParameter("@QDeadlineDate", QDDate);
            param[5] = new SqlParameter("@AReleaseDate", ARDate);
            param[6] = new SqlParameter("@SReleaseDate", SRDate);
            param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
            param[8] = new SqlParameter("@CreatedBy", Int32.Parse(Session["LoginID"].ToString()));
            param[9] = new SqlParameter("@MemberId", Int32.Parse(DDLMCoach.SelectedItem.Value));

            SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            Label9.ForeColor = System.Drawing.Color.Green;
            Label9.Text = "Record Added Successfully ";
        }
        catch (Exception ex)
        {
            Label9.ForeColor = System.Drawing.Color.Red;
            Label9.Text = ex.Message.ToString();
        }

    }

    public class EntityTestPaper
    {
        public Int32 TestPaperId = -1;
        public Int32 ProductId = -1;
        public string ProductCode = DBNull.Value.ToString();
        public string Event = DBNull.Value.ToString();
        public Int32 EventId = -1;
        public Int32 ProductGroupId = -1;
        public string ProductGroupCode = DBNull.Value.ToString();
        public string EventCode = DBNull.Value.ToString();
        public Int32 WeekId = -1;
        public Int32 SetNum = -1;
        public string PaperType = DBNull.Value.ToString(); //Modified on 22/07/2013
        public Int32 Sections = -1;
        public string TestFileName = DBNull.Value.ToString();
        public string Description = DBNull.Value.ToString();
        public string Password = DBNull.Value.ToString();
        public DateTime CreateDate = new System.DateTime(1900, 1, 1);
        public Int32 CreatedBy = -1;
        public DateTime? ModifyDate = null;
        public Int32? ModifiedBy = null;
        public string ContestYear = DBNull.Value.ToString();
        public string DocType = DBNull.Value.ToString();
        public string WeekOf = DBNull.Value.ToString();
        public string ReceivedBy = DBNull.Value.ToString();
        public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
        public string Level = DBNull.Value.ToString();

        public string getCompleteFTPFilePath(String TestFileName)
        {
            return String.Format("{0}/{1}",
                      System.Configuration.ConfigurationManager.AppSettings["FTPCoachPapersPath"], TestFileName);
        }
        public EntityTestPaper()
        {
        }
    }

    private void GetCoachNames(DropDownList ddlObject, int RoleId, int MemberId, string grpCode, int EYear, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_GetCoachNames";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@RoleId", RoleId);
        param[1] = new SqlParameter("@MemberId", MemberId);
        param[2] = new SqlParameter("@ProductGroupCode", grpCode);
        param[3] = new SqlParameter("@EventYear", EYear);

        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            if (ds.Tables.Count > 0)
            {
                ddlObject.DataSource = ds.Tables[0];
                ddlObject.DataTextField = "NameOfCoach";
                ddlObject.DataValueField = "automemberId";
                ddlObject.DataBind();
            }
        }
        catch (SqlException ex)
        {
            Response.Write(ex.ToString());
        }

        if (ddlObject.Items.Count == 1)
        {
            ddlObject.Enabled = false;
            blnCreateEmptyItem = false;
        }
        else if (ddlObject.Items.Count == 0)
        {
            ddlObject.Enabled = false;
            blnCreateEmptyItem = true;
        }

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Coach Name]", "-1"));
            ddlObject.SelectedIndex = 0;
        }


    }
    private void GetTestPapers(EntityTestPaper objETP)
    {

        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_Conditional_CoachPapers_GetByCriteria";// "CoachPapers_GetByCriteria";
        SqlParameter[] param = new SqlParameter[18];
        param[0] = new SqlParameter("@ProductId", objETP.ProductId);
        param[1] = new SqlParameter("@ProductCode", objETP.ProductCode);
        param[2] = new SqlParameter("@ProductGroupId", objETP.ProductGroupId);
        param[3] = new SqlParameter("@ProductGroupCode", objETP.ProductGroupCode);
        if (ddlFlrYear.SelectedValue == DateTime.Now.Year.ToString())
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        else
            param[4] = new SqlParameter("@WeekId", objETP.WeekId);
        param[5] = new SqlParameter("@SetNum", objETP.SetNum);
        param[6] = new SqlParameter("@PaperType", objETP.PaperType);// Modified 22/07/2013
        // param[6] = new SqlParameter("@NoOfContestants", objETP.NoOfContestants);
        param[7] = new SqlParameter("@TestFileName", objETP.TestFileName);
        param[8] = new SqlParameter("@Description", objETP.Description);
        param[9] = new SqlParameter("@CreateDate", objETP.CreateDate);
        param[10] = new SqlParameter("@CreatedBy", objETP.CreatedBy);
        param[11] = new SqlParameter("@EventYear", ddlFlrYear.SelectedValue);
        param[12] = new SqlParameter("@DocType", objETP.DocType);
        param[13] = new SqlParameter("@Sections", objETP.Sections);
        param[14] = new SqlParameter("EventId", objETP.EventId);
        param[15] = new SqlParameter("@MemberId", Int32.Parse(Session["LoginID"].ToString()));
        param[16] = new SqlParameter("@RoleId", Int32.Parse(Session["RoleID"].ToString()));
        param[17] = new SqlParameter("@Level", objETP.Level);
        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count == 0)
        {
            DataView dv = new DataView(dt);
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();
            lblSearchErr.Text = "Sorry your selection criteria didn't match with any record";
            Panel3.Visible = false;
        }
        else
        {

            lblSearchErr.Text = string.Empty;
            Panel3.Visible = true;
            DataView dv = new DataView(dt);
            //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);
            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();
            GetCoachNames(ddlName, Int32.Parse(Session["RoleID"].ToString()), Int32.Parse(Session["LoginID"].ToString()), PGCode(ddlFlrProductGroup), Convert.ToInt32(ddlFlrYear.SelectedItem.Text), true);
            //GetSessionNumbers(ddlSession, Int32.Parse(ddlName.SelectedValue.ToString()), -1, true, ddlFlrYear);

        }

    }

    private void UpdateCoachRelDate(int CoId)
    {
        string[] formats = { "MM/dd/yyyy" };
        int CoachId = CoId;
        try
        {
            DateTime QRDate = DateTime.ParseExact(txtQR.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            DateTime QDDate = DateTime.ParseExact(txtQD.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            DateTime ARDate = DateTime.ParseExact(txtAD.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            DateTime SRDate = DateTime.ParseExact(txtSD.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None);

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            string sqlCommand = "usp_Update_CoachRelDate";
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CoachRelID", CoachId);
            param[1] = new SqlParameter("@QReleaseDate", QRDate);
            param[2] = new SqlParameter("@QDeadlineDate", QDDate);
            param[3] = new SqlParameter("@AReleaseDate", ARDate);
            param[4] = new SqlParameter("@SReleaseDate", SRDate);
            param[5] = new SqlParameter("@SessionNO", ddlSessionM.Text);
            param[6] = new SqlParameter("@ModifiedBy", Int32.Parse(Session["LoginID"].ToString()));
            param[7] = new SqlParameter("@ModifyDate", System.DateTime.Now);
            SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            Label15.ForeColor = System.Drawing.Color.Green;
            Label15.Text = "Record Updated Successfully ";
        }
        catch (Exception ex)
        {
            Label15.ForeColor = System.Drawing.Color.Red;
            Label15.Text = "Error Updating Data.";//ex.Message.ToString(); 
        }
    }

    #endregion

    #region "Private Functions"
    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        string[] Choice = { "Add ReleaseDate", "Modify ReleaseDate" };
        ddlObject.DataSource = Choice;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
        }
        ddlObject.SelectedIndex = 0;

    }

    private void GetFlrContextYear(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        //if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "32"))
        //{
        int Minyear, MaxYear;
        try
        {
            Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(contestyear) from testpapers"));
            MaxYear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(contestyear) from testpapers"));

            int year = DateTime.Now.Year;
            int j = 0;
            for (int i = Minyear; i <= year; i++)
            {
                ddlObject.Items.Insert(j, new ListItem(i.ToString()));
                j = j + 1;
            }
            ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText((MaxYear).ToString()));
        }
        catch (SqlException se)
        {
            //lblMessage.Text = se.Message;
            //Response.Write(se.ToString());

            return;
        }

        //}
        //else if (Session["RoleId"].ToString() == "2")
        //{
        //    int year = DateTime.Now.Year;
        //    ddlFlrYear.Items.Insert(0, new ListItem((year - 1).ToString()));
        //    ddlFlrYear.Items.Insert(1, new ListItem(year.ToString()));
        //    ddlFlrYear.SelectedIndex = 1;
        //}
        //else
        //{
        //    ddlFlrYear.Items.Insert(0, new ListItem(DateTime.Now.Year.ToString()));
        //    ddlFlrYear.Enabled = false;
        //}
    }

    private void PopulateEvents(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Coaching", "13"));

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Event]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private void GetProductGroupCodesold(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsproductgroup;
            //if (Convert.ToInt32(Session["RoleId"]) == 93)
            //{
            //    //     dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(1,2) and A.ProductGroupCode in (Select distinct C.ProductGroupCode From ContestCategory C Left Join NatTechTeam N ON N.ProductGroupCode = C.ProductGroupCode where (C.RegionalStatus = 'Active' OR C.NationalFinalsStatus = 'Active') AND C.ContestYear = " + System.DateTime.Now.Year.ToString() + " and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]) + ") Order by A.EventCode");
            //    dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupCode in (Select distinct C.ProductGroupCode From ContestCategory C Left Join NatTechTeam N ON N.ProductGroupCode = C.ProductGroupCode where (C.RegionalStatus = 'Active' OR C.NationalFinalsStatus = 'Active') AND C.ContestYear = " + System.DateTime.Now.Year.ToString() + " and N.NTTMemberID=" + Convert.ToInt32(Session["LoginID"]) + ") Order by A.EventCode");

            //}
            //else
            //{
            // dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(1,2) and A.ProductGroupCode in (Select distinct ProductGroupCode From ContestCategory where (RegionalStatus = 'Active' OR NationalFinalsStatus = 'Active') AND ContestYear = " + System.DateTime.Now.Year.ToString() + ") Order by A.EventCode");
            // dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupCode in (Select distinct ProductGroupCode From ContestCategory where (RegionalStatus = 'Active' OR NationalFinalsStatus = 'Active') AND ContestYear = " + System.DateTime.Now.Year.ToString() + ") Order by A.EventCode");
            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct A.[ProductGroupId], A.[EventCode] + ' - ' + A.[ProductGroupCode] AS EventCodeAndProductGroupCode, A.EventCode FROM dbo.[Product] A Where A.EventID in(13) Order by A.EventCode");

            //}

            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "EventCodeAndProductGroupCode";
            ddlObject.DataValueField = "ProductGroupId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            //lblMessage.Text = se.Message;
            //Response.Write(se.ToString());

            return;
        }
    }

    private void GetProductCodesold(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            if (ProductGroupId != -1)
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ProductGroupId", ProductGroupId);
                DataSet dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "sp_GetByProductGroupId", param);
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "ProductCode";
                ddlObject.DataValueField = "ProductId";
                ddlObject.DataBind();
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            // lblMessage.Text = se.Message;
            //Response.Write(se.ToString());

            return;
        }
    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem, int EventYear, DropDownList ddlCoach)
    {
        string R_ID = string.Empty;
        string M_ID = string.Empty;
        if (Session["RoleID"] != null)
        {
            R_ID = Session["RoleID"].ToString();
        }
        if (Session["LoginID"] != null)
        {
            M_ID = Session["LoginID"].ToString();
        }
        try
        {
            string SMemberID = ddlCoach.SelectedValue;
            string conn = Application["ConnectionString"].ToString();
            DataSet dsproductgroup;
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@RoleId", R_ID);
            param[1] = new SqlParameter("@MemberID", M_ID);
            param[2] = new SqlParameter("@TLead", hdntlead.Value.ToString());
            param[3] = new SqlParameter("@EventYear", EventYear);
            param[4] = new SqlParameter("@SMemberID", SMemberID);

            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetProductGroupByRoleID", param);
            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "Name";
            ddlObject.DataValueField = "IDandCode";
            ddlObject.DataBind();
            ddlObject.Enabled = true;

            if (dsproductgroup.Tables[0].Rows.Count == 1)
            {
                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;
                ddlObject.SelectedIndex = 0;

                if (dllfileChoice.SelectedIndex == 1)
                {
                    GetProductCodes(PGId(ddlFlrProductGroup), ddlFlrProduct, true, ddlFlrYear);
                }
                else if (dllfileChoice.SelectedIndex == 2)
                {
                    GetProductCodes(PGId(ddlFlrvProductGroup), ddlFlrvProduct, true, ddlFlrvYear);
                }

                //if (Session["RoleId"].ToString() == "88")
                //{
                //    PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlObject));
                //}
                //else
                //{
                //    PopulateLevel(ddlFlrvLevel, ddlObject.SelectedItem.Text, true);
                //    ////Response.Write(ddlFlrProduct.SelectedValue.ToString()); 
                //}
            }
            else if (dsproductgroup.Tables[0].Rows.Count < 1)
            {
                ddlObject.SelectedIndex = -1;
            }
            else
            {
                ddlObject.SelectedIndex = -1;
            }


            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }

        }
        catch (Exception se)
        {
            //  lblMessage.Text = se.Message;
            //Response.Write(se.ToString());
            return;
        }

    }

    private void GetProductCodes(int ProductGroupId, DropDownList ddlObject, bool blnCreateEmptyItem, DropDownList ddlYear)
    {
        string R_ID = string.Empty;
        string M_ID = string.Empty;
        if (Session["RoleID"] != null)
        {
            R_ID = Session["RoleID"].ToString();
        }
        if (Session["LoginID"] != null)
        {
            M_ID = Session["LoginID"].ToString();
        }
        DataSet dsproduct;
        try
        {
            string SMemberID = string.Empty;
            if (dllfileChoice.SelectedIndex == 1)
            {
                SMemberID = DDLMCoach.SelectedValue;
            }
            else if (dllfileChoice.SelectedIndex == 2)
            {

                SMemberID = DDLACoach.SelectedValue;
            }

            if (ProductGroupId != -1)
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter("@RoleId", R_ID);
                param[1] = new SqlParameter("@MemberId", M_ID);
                param[2] = new SqlParameter("@ProductGroupId", ProductGroupId);
                param[3] = new SqlParameter("@EventYear", Int32.Parse(ddlYear.SelectedValue));//DateTime.Now.Year.ToString ()
                param[4] = new SqlParameter("@TLead", hdntlead.Value.ToString());
                param[5] = new SqlParameter("@SMemberID", SMemberID);
                //Response.Write(R_ID + "*****" + M_ID + "*****" + ProductGroupId + "*****" + hdntlead.Value.ToString());
                dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetProductCodeByRoleID", param);
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "Name";
                ddlObject.DataValueField = "IDandCode";
                ddlObject.DataBind();
                ddlObject.Enabled = true;

                if (dsproduct.Tables[0].Rows.Count < 2)
                {
                    blnCreateEmptyItem = false;
                    ddlObject.Enabled = false;
                }
                if (dsproduct.Tables[0].Rows.Count == 1)
                {
                    if (Session["RoleId"].ToString() == "88")
                    {
                        PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlFlrvProductGroup), ddlFlrvYear);

                    }
                    else
                    {
                        //PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);
                        loadlevel();
                    }

                }
            }
            else
            {
                ddlObject.Items.Clear();
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            // ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            //lblMessage.Text = se.Message;
            //Response.Write(se.ToString());
            return;
        }
    }

    private void PopulateLevelNew(DropDownList ddlObject, bool blnCreateEmptyItem, int ProductGroupId, DropDownList ddlYear)
    {
        try
        {

            ddlObject.Enabled = true;
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT DISTINCT Level FROM CalSignUp WHERE Accepted='Y' and  MemberID =" + Session["LoginId"].ToString() + " AND EventYear =" + Int32.Parse(ddlYear.SelectedValue) + " and ProductGroupId=" + ProductGroupId); //DateTime.Now.Year.ToString()
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "Level";
            ddlObject.DataValueField = "Level";
            ddlObject.DataBind();
            ddlObject.Enabled = true;

            if (ds.Tables[0].Rows.Count < 2)
            {
                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;
            }
            if (ds.Tables[0].Rows.Count == 0)
            {
                blnCreateEmptyItem = true;
                ddlObject.Enabled = false;
            }

            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
                ddlObject.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            //Response.Write(ex.ToString());
        }
    }

    private void PopulateLevel(DropDownList ddlObject, string iCondition, bool blnCreateEmptyItem)
    {


        ArrayList list = new ArrayList();
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        if (iCondition.ToLower() == "math" || iCondition.ToLower() == "geography")
        {
            list.Add(new ListItem("Beginner", "BEG"));
            list.Add(new ListItem("Intermediate", "INT"));
            list.Add(new ListItem("Advanced", "ADV"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition.ToLower() == "sat")
        {
            list.Add(new ListItem("Junior", "JR"));
            list.Add(new ListItem("Senior", "SR"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition.ToLower() == "universal values")
        {
            list.Add(new ListItem("Junior", "JUV"));
            list.Add(new ListItem("Intermediate", "IUV"));
            list.Add(new ListItem("Senior", "SUV"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition == "NA")
        {
            ddlObject.Enabled = false;
        }
        else
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = false;
            blnCreateEmptyItem = false;
        }


        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
        }

        //ddlObject.SelectedIndex = 0;



    }

    private void PopulatePaperType(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        //int[] Nos = { 5, 10, 15, 20, 25, 30, 35 };
        ////string[] Itm = { "Homework", "Test" };

        ////ddlObject.DataSource = Itm;
        ////ddlObject.DataBind();
        ddlObject.Items.Clear();
        ddlObject.Items.Add(new ListItem("Homework", "HW"));
        ddlObject.Items.Add(new ListItem("PreTest", "PT"));
        ddlObject.Items.Add(new ListItem("RegTest", "RT"));
        ddlObject.Items.Add(new ListItem("FinalTest", "FT"));
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Paper Type]", "-1"));
        }
        ddlObject.SelectedIndex = 1;
    }

    private void GetWeek(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Week#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private void PopulateSets(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int[] Nos = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40 };

        ddlObject.DataSource = Nos;
        ddlObject.DataBind();
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Set#]", "-1"));
        }
        ddlObject.SelectedIndex = 0;
    }

    private int PGId(DropDownList ddlObject)
    {
        int index;
        int PGI = -1;
        index = ddlObject.SelectedValue.ToString().IndexOf("-");
        if (index > 0)
        {
            PGI = Int32.Parse(ddlObject.SelectedValue.ToString().Trim().Substring(0, index));
        }
        return PGI;
    }

    private string PGCode(DropDownList ddlObject)
    {
        int index = -1;
        string PGC = string.Empty;
        index = ddlObject.SelectedValue.ToString().Trim().LastIndexOf("-");
        if (index > 0)
        {
            index++;
            PGC = ddlObject.SelectedValue.ToString().Substring(index);
        }
        return PGC;
    }

    #endregion
    protected void tboxQrelease_TextChanged(object sender, EventArgs e)
    {
        Datevalidate(tboxQrelease, tboxQrelease, lblQR, 1);
        CallJavaScript();

    }
    protected void tboxQdeadline_TextChanged(object sender, EventArgs e)
    {
        Datevalidate(tboxQdeadline, tboxQrelease, lblQD, 2);
        CallJavaScript();
    }
    protected void tboxArelease_TextChanged(object sender, EventArgs e)
    {
        Datevalidate(tboxArelease, tboxQdeadline, lblAR, 3);
        CallJavaScript();
    }
    protected void tboxSrelease_TextChanged(object sender, EventArgs e)
    {
        //Datevalidate(tboxSrelease , tboxQdeadline,lblAD , 4);
        Datevalidate(tboxSrelease, tboxQrelease, lblAD, 4);

        CallJavaScript();
    }
    private void CallJavaScript()
    {
        Page.RegisterStartupScript("myScript", "<script language=JavaScript> $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });</script>");

    }
    private void Datevalidate(TextBox tboxS, TextBox tboxV, Label errorLabel, int fval)
    {
        string[] formats = new string[] { "MM/dd/yyyy" };
        DateTime QRDate;
        DateTime QDDate;
        DateTime ARDate;
        DateTime SRDate;
        if (fval == 1)
        {
            Label15.Text = "";
            errorLabel.Text = "";
            if (!DateTime.TryParseExact(tboxS.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QRDate))
            {
                errorLabel.Text = "Invalid Q-Release Date";
            }
            //else if (DateTime.Compare(QRDate, DateTime.Today.Date) < 0)
            //{
            //    errorLabel.Text = "Date can't be in the Past";
            //}
        }
        if (fval == 2)
        {

            errorLabel.Text = "";
            DateTime.TryParseExact(tboxV.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QRDate);
            if (!DateTime.TryParseExact(tboxS.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QDDate))
            {
                errorLabel.Text = "Invalid Q-Deadline Date";
            }
            else
                if (DateTime.Compare(QDDate, QRDate) < 1)
                {
                    errorLabel.Text = "Q-Deadline Date must be greater than Q-Release Date";
                }
                else
                {
                    if (tboxV.Text == string.Empty)
                    {
                        errorLabel.Text = "Invalid Q-Release Date";
                    }
                }
        }

        if (fval == 3)
        {
            errorLabel.Text = "";
            DateTime.TryParseExact(tboxV.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QDDate);
            if (!DateTime.TryParseExact(tboxS.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out ARDate))
            {
                errorLabel.Text = "Invalid A-Release Date";
            }
            else
            {
                if (DateTime.Compare(ARDate, QDDate) < 1)
                {
                    errorLabel.Text = "A-Release Date must be greater than Q-Deadline Date";
                }
                else
                {
                    if (tboxV.Text == string.Empty)
                    {
                        errorLabel.Text = "Invalid Q-Deadline Date";
                    }
                }
            }
        }
        if (fval == 4)
        {
            errorLabel.Text = "";
            DateTime.TryParseExact(tboxV.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QRDate);
            if (!DateTime.TryParseExact(tboxS.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out SRDate))
            {
                errorLabel.Text = "Invalid S-Release Date";
            }
            else
            {
                DateTime.TryParseExact(tboxV.Text.Trim(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out QRDate);
                //TimeSpan diff = QRDate - SRDate ;
                if ((QRDate - SRDate).Days > 7)
                {
                    errorLabel.Text = "S-Release Date must be less than or equal to Q-Release by 7 days";
                    tboxV.Focus();
                }
                //if (DateTime.Compare(SRDate, QDDate) < 1)
                //{
                //    errorLabel.Text = "S-Release Date must be greater than Q-Deadline Date";
                //    tboxV.Focus();
                //}
                else
                {
                    if (tboxV.Text == string.Empty)
                    {
                        errorLabel.Text = "Invalid Q-Deadline Date";
                    }
                }

            }
        }

    }
    protected void txtQR_TextChanged(object sender, EventArgs e)
    {
        Datevalidate(txtQR, txtQR, Label11, 1);
    }
    protected void txtQD_TextChanged(object sender, EventArgs e)
    {
        Datevalidate(txtQD, txtQR, Label12, 2);
    }
    protected void txtAD_TextChanged(object sender, EventArgs e)
    {
        Datevalidate(txtAD, txtQD, Label13, 3);
    }
    protected void txtSD_TextChanged(object sender, EventArgs e)
    {
        //Datevalidate(txtSD, txtQD, Label14, 4);
        Datevalidate(txtSD, txtQR, Label14, 4);

    }
    private void GetSessionNumbers(DropDownList ddlObject, int MemberId, int cpid, bool blnCreateEmptyItem, DropDownList ddlYear)
    {
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        try
        {
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@CoachPaperID", cpid);
            param[1] = new SqlParameter("@MemberId", MemberId);
            param[2] = new SqlParameter("@EventYear", ddlYear.SelectedValue);//Int32.Parse(DateTime.Now.Year.ToString()));

            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_Select_Session_By_ID", param);

            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "SessionNo";
            ddlObject.DataValueField = "SessionNo";
            ddlObject.DataBind();


            if (ddlObject.Items.Count == 1)
            {
                ddlObject.Enabled = false;
                blnCreateEmptyItem = false;
            }
            else if (ddlObject.Items.Count == 0)
            {
                ddlObject.Enabled = false;
                blnCreateEmptyItem = true;
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Session#]", "-1"));
                ddlObject.SelectedIndex = 0;
            }
        }
        catch (SqlException ex)
        {
            //Response.Write(ex.ToString());
        }

    }

    protected void ddlName_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetSessionNumbers(ddlSession, Int32.Parse(ddlName.SelectedValue.ToString()), -1, true, ddlFlrYear);
    }

    protected void btnDelete_Click1(object sender, EventArgs e)
    {
        Label15.ForeColor = System.Drawing.Color.Green;
        if (gvEdit.SelectedIndex > -1)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                try
                {
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "DELETE FROM CoachRelDates WHERE CoachRelDates.CoachRelID =" + gvEdit.SelectedDataKey.Value.ToString());
                    btnDelete.Enabled = false;
                    //ddlSessionM.Text="";
                    Label15.Text = "Record Deleted. ";
                    btnvSerarch_Click(null, null);

                }
                catch (Exception ex)
                {
                    Label15.ForeColor = System.Drawing.Color.Red;
                    Label15.Text = ex.Message.ToString();
                }
            }
        }

    }

    protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSession.SelectedItem.Value.ToString() != "-1")
        {
            lblsession.Text = "";
        }
        else
        {
            lblsession.Text = "Invalid Session";
        }

    }

    protected void ddlFlrvProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (Session["RoleId"].ToString() == "88")
        {
            PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlFlrvProductGroup), ddlFlrvYear);

        }
        else
        {
            //PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);
            loadlevel();
        }

    }
    protected void ddlFlrvProductGroup_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //GetProductCodes(int.Parse(ddlFlrvProductGroup.SelectedValue), ddlFlrvProduct, true);
        GetProductCodes(PGId(ddlFlrvProductGroup), ddlFlrvProduct, true, ddlFlrvYear);
        GetCoachNames(ddlNameM, Int32.Parse(Session["RoleID"].ToString()), Int32.Parse(Session["LoginID"].ToString()), PGCode(ddlFlrvProductGroup), Int32.Parse(ddlFlrvYear.SelectedValue), true);//DateTime.Now.Year.ToString()
        //  PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);

        if (Session["RoleId"].ToString() == "88")
        {
            PopulateLevelNew(ddlFlrvLevel, true, PGId(ddlFlrvProductGroup), ddlFlrvYear);

        }
        else
        {
            //PopulateLevel(ddlFlrvLevel, ddlFlrvProductGroup.SelectedItem.Text, true);
            loadlevel();
        }

        // PopulateLevelNew(ddlFlrvLevel, true);
    }
    protected void ddlNameM_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetSessionNumbers(ddlSessionM, Int32.Parse(ddlNameM.SelectedValue.ToString()), -1, true, ddlFlrvYear);

        string cmdText = string.Empty;
        cmdText = " SELECT DISTINCT CP.[CoachPaperId] ,CP.[ProductId]  ,CP.[EventYear]  ,CP.[EventId] ,CP.[PaperType] ,CP.[DocType] ,CP.[ProductCode] ,CP.[ProductGroupCode] ,CR.[Phase] ,CR.[Level] ,CR.[Session] ,CP.[WeekId] ,CP.[SetNum] ,CP.[Sections] ,CR.[QReleaseDate] ,CR.[QDeadlineDate] ,CR.[AReleaseDate] ,CR.[SReleaseDate] ,CR.[CoachRelID],CR.[MemberID],ISP.FirstName +  '  '+ ISP.[LastName] AS [NameOfCoach] FROM [CoachPapers] CP INNER JOIN [CoachRelDates] CR  ON CP.[CoachPaperId]=CR.[CoachPaperId] INNER JOIN IndSpouse ISP ON CR.MemberID = ISP.AutoMemberID  and CR.MemberID = " + ddlNameM.SelectedValue + "  WHERE CP.[EventYear] = " + ddlFlrvYear.SelectedValue + "";

        if (ddlFlrvProductGroup.SelectedValue != "-1" && ddlFlrvProductGroup.SelectedValue != "")
        {
            cmdText += " and CP.ProductGroupId = " + PGId(ddlFlrvProductGroup) + "";
        }

        if (ddlFlrvProduct.SelectedValue != "-1" && ddlFlrvProduct.SelectedValue != "")
        {
            cmdText += " and CP.ProductId = " + PGId(ddlFlrvProduct) + "";
        }
        if (ddlFlrvLevel.SelectedValue != "-1" && ddlFlrvLevel.SelectedValue != "")
        {
            cmdText += " and CP.Level = '" + ddlFlrvLevel.SelectedItem.Text + "'";
        }

        if (ddlFlrvWeek.SelectedValue != "-1" && ddlFlrvWeek.SelectedValue != "")
        {
            cmdText += " and CP.WeekId = " + ddlFlrvWeek.SelectedValue + "";
        }
        if (ddlFlrvSet.SelectedValue != "-1" && ddlFlrvSet.SelectedValue != "")
        {
            cmdText += " and CP.SetNum = " + ddlFlrvSet.SelectedValue + "";
        }
        if (ddlFlrvPaperType.SelectedValue != "-1" && ddlFlrvPaperType.SelectedValue != "")
        {
            cmdText += " and CP.PaperType = '" + ddlFlrvPaperType.SelectedValue + "'";
        }
        cmdText += " and CR.MemberID = " + ddlNameM.SelectedValue + "";

        cmdText += " order by CP.[CoachPaperId]";

        try
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvEdit.DataSource = ds;
                    gvEdit.DataBind();
                    txtQR.Text = "";
                    txtQD.Text = "";
                    txtAD.Text = "";
                    txtSD.Text = "";
                    lblNoPermission.Visible = false;
                    lblNoPermission.Text = "";
                }
                else
                {
                    gvEdit.DataSource = ds;
                    gvEdit.DataBind();
                    txtQR.Text = "";
                    txtQD.Text = "";
                    txtAD.Text = "";
                    txtSD.Text = "";
                    lblNoPermission.Visible = true;
                    lblNoPermission.Text = "No record exists.";
                }
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void ddlFlrProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlFlrvWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFlrvWeek.SelectedValue != "-1")
        {
            ddlFlrvSet.SelectedIndex = ddlFlrvSet.Items.IndexOf(ddlFlrvSet.Items.FindByValue(ddlFlrvWeek.SelectedValue));
            ddlFlrvSet.Enabled = false;
        }
    }
    protected void ddlFlrWeek_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFlrWeek.SelectedValue != "-1")
        {
            ddlFlrSet.SelectedIndex = ddlFlrSet.Items.IndexOf(ddlFlrSet.Items.FindByValue(ddlFlrWeek.SelectedValue));
            ddlFlrSet.Enabled = false;
        }

    }
    protected void ddlFlrvYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadModifyRelDates();
    }
    protected void ddlFlrYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadAddRelDates();
    }


    public void fillCoach(string Year, string Event, DropDownList ddlCoach)
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + Year + " and V.EventID=" + Event + " and V.Accepted='Y' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("[Select Coach]", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
            }

        }

    }

    protected void DDLACoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroupCodes(ddlFlrvProductGroup, true, int.Parse(ddlFlrvYear.SelectedValue), DDLACoach);
    }

    protected void DDLMCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProductGroupCodes(ddlFlrProductGroup, true, int.Parse(ddlFlrYear.SelectedValue), DDLMCoach);
    }

    public void loadlevel()
    {
        DropDownList ddlCoach = DDLMCoach;
        DropDownList ddlYear = ddlFlrYear;
        DropDownList ddlProductID = ddlFlrProduct;
        DropDownList DDLProductGroupID = ddlFlrProductGroup;
        DropDownList ddlEvent = ddlFlrEvent;
        DropDownList ddlLevel = ddlFlrvLevel;
        if (dllfileChoice.SelectedIndex == 1)
        {
            ddlCoach = DDLMCoach;
            ddlYear = ddlFlrYear;
            ddlProductID = ddlFlrProduct;
            DDLProductGroupID = ddlFlrProductGroup;
            ddlEvent = ddlFlrEvent;
            ddlLevel = ddlFlrLevel;
        }
        else if (dllfileChoice.SelectedIndex == 2)
        {
            ddlCoach = DDLACoach;
            ddlYear = ddlFlrvYear;
            ddlProductID = ddlFlrvProduct;
            DDLProductGroupID = ddlFlrvProductGroup;
            ddlEvent = ddlFlrvEvent;
            ddlLevel = ddlFlrvLevel;
        }

        string cmdText = string.Empty;
        cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and V.ProductGroupID=" + PGId(DDLProductGroupID) + " and V.ProductID=" + PGId(ddlProductID) + " and V.MemberID=" + ddlCoach.SelectedValue + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
            }
            //ddlLevel.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
        }

    }

    public void populateRelDates()
    {
        string cmdText = string.Empty;
        cmdText = " SELECT DISTINCT CP.[CoachPaperId] ,CP.[ProductId]  ,CP.[EventYear]  ,CP.[EventId] ,CP.[PaperType] ,CP.[DocType] ,CP.[ProductCode] ,CP.[ProductGroupCode] ,CR.[Phase] ,CR.[Level] ,CR.[Session] ,CP.[WeekId] ,CP.[SetNum] ,CP.[Sections] ,CR.[QReleaseDate] ,CR.[QDeadlineDate] ,CR.[AReleaseDate] ,CR.[SReleaseDate] ,CR.[CoachRelID],CR.[MemberID],ISP.FirstName +  '  '+ ISP.[LastName] AS [NameOfCoach] FROM [CoachPapers] CP INNER JOIN [CoachRelDates] CR  ON CP.[CoachPaperId]=CR.[CoachPaperId] INNER JOIN IndSpouse ISP ON CR.MemberID = ISP.AutoMemberID  and CR.MemberID = " + DDLACoach.SelectedValue + "  WHERE CP.[EventYear] = " + ddlFlrvYear.SelectedValue + "";

        if (ddlFlrvProductGroup.SelectedValue != "-1" && ddlFlrvProductGroup.SelectedValue != "")
        {
            cmdText += " and CP.ProductGroupId = " + PGId(ddlFlrvProductGroup) + "";
        }

        if (ddlFlrvProduct.SelectedValue != "-1" && ddlFlrvProduct.SelectedValue != "")
        {
            cmdText += " and CP.ProductId = " + PGId(ddlFlrvProduct) + "";
        }
        if (ddlFlrvLevel.SelectedValue != "-1" && ddlFlrvLevel.SelectedValue != "")
        {
            cmdText += " and CP.Level = '" + ddlFlrvLevel.SelectedItem.Text + "'";
        }

        if (ddlFlrvWeek.SelectedValue != "-1" && ddlFlrvWeek.SelectedValue != "")
        {
            cmdText += " and CP.WeekId = " + ddlFlrvWeek.SelectedValue + "";
        }
        if (ddlFlrvSet.SelectedValue != "-1" && ddlFlrvSet.SelectedValue != "")
        {
            cmdText += " and CP.SetNum = " + ddlFlrvSet.SelectedValue + "";
        }
        if (ddlFlrvPaperType.SelectedValue != "-1" && ddlFlrvPaperType.SelectedValue != "")
        {
            cmdText += " and CP.PaperType = '" + ddlFlrvPaperType.SelectedValue + "'";
        }
        cmdText += " and CR.MemberID = " + DDLACoach.SelectedValue + "";

        cmdText += " order by CP.[CoachPaperId]";

        try
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvEdit.DataSource = ds;
                    gvEdit.DataBind();
                    txtQR.Text = "";
                    txtQD.Text = "";
                    txtAD.Text = "";
                    txtSD.Text = "";
                    lblNoPermission.Visible = false;
                    lblNoPermission.Text = "";
                    Panel7.Visible = true;
                }
                else
                {
                    gvEdit.DataSource = ds;
                    gvEdit.DataBind();
                    txtQR.Text = "";
                    txtQD.Text = "";
                    txtAD.Text = "";
                    txtSD.Text = "";
                    lblNoPermission.Visible = true;
                    Panel7.Visible = false;
                    lblNoPermission.Text = "No record exists.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
}
