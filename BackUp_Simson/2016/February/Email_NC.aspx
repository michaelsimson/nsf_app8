<%@ Page Language="VB" Debug="true" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Email_NC.aspx.vb" Inherits="Email_NC" title="National Coordinator's - Send EMail" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

    <table style="width: 680px">
<tr><td style="width: 680px;">
<asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="284px" ></asp:HyperLink>
</td></tr></table><table><tr><td align="center" style="width: 542px">
        <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
<asp:Label ID="lblerr" runat="server" ForeColor="Red" Text="You are not allowed to use this application." Visible="False" Width="471px"></asp:Label>
  </td></tr></table>
            <table id="tabletarget" runat="server" width="100%" border="0" cellpadding="0" cellspacing="0">
     <tr style="height :25px">
        <td align="right" style="height: 25px; width:6px" nowrap="nowrap"><asp:Label ID="lbltarget" runat="server" Text="Target:" Width="136px" Font-Bold="True"></asp:Label></td>
        <td style="width: 64px; height: 25px;" nowrap="nowrap"> <asp:RadioButton ID="rbtall" runat="server" Text="All" GroupName="Group1" AutoPostBack="true" OnCheckedChanged="rbtall_CheckedChanged" />&nbsp;&nbsp;&nbsp; </td>
        <td style="width: 25%; height: 25px;" colspan="2"> <asp:RadioButton ID="btnregisteredparetns" runat="server" Text="Registered Parents" AutoPostBack="true" OnCheckedChanged="btnregisteredparetns_CheckedChanged" Checked="true" GroupName="Group1" Width="148px" /> </td>
        <td style="height: 25px;" colspan="2"> <asp:RadioButton ID="rbtassigndvolunteer" runat="server" Text="Assigned Volunteers" AutoPostBack="true" OnCheckedChanged="rbtassigndvolunteer_CheckedChanged" GroupName="Group1" /></td> 
        <td style="height: 25px; width:25%" colspan="2"> <asp:RadioButton ID="rbtunassigndvolunteer" runat="server" Text="UnAssigned Volunteers" AutoPostBack="true" OnCheckedChanged="rbtunassigndvolunteer_CheckedChanged" GroupName="Group1" /></td>  
        <td style="height: 25px; width:15%" nowrap="nowrap"> <asp:RadioButton ID="rbtownlist" runat="server" Text="Own List" AutoPostBack="true" OnCheckedChanged="rbtownlist_CheckedChanged" GroupName="Group1" /></td></tr>  
     <tr style="height:25px"><td style="width: 6px" dir="ltr" nowrap="nowrap">&nbsp;<span style="font-size: small"><strong># of Years: </strong></span>&nbsp;<asp:DropDownList ID="ddlnfYears" runat="server" Enabled="False" Width="60px">
                              <asp:ListItem Selected="True" Value="0">Years</asp:ListItem>
                              <asp:ListItem Value="-1">1</asp:ListItem>
                              <asp:ListItem Value="-2">2</asp:ListItem>
                              <asp:ListItem Value="-3">3</asp:ListItem>
                              <asp:ListItem Value="-4">4</asp:ListItem>
                              <asp:ListItem Value="-5">5</asp:ListItem>
                              <asp:ListItem Value="-6">6</asp:ListItem>
                              <asp:ListItem Value="-7">7</asp:ListItem>
                              <asp:ListItem Value="-8">8</asp:ListItem>
                              <asp:ListItem Value="-9">9</asp:ListItem>
                              <asp:ListItem Value="-10">10</asp:ListItem>
                              <asp:ListItem Value="-11">11</asp:ListItem>
                              <asp:ListItem Value="-12">12</asp:ListItem>
                              <asp:ListItem Value="-13">13</asp:ListItem>
                              <asp:ListItem Value="-14">14</asp:ListItem>
                              <asp:ListItem Value="-15">15</asp:ListItem>
                          </asp:DropDownList>
         </td></tr>
     <tr><td style="width:6px; height: 28px;" nowrap="nowrap"></td>
        <td style="width:64px; height: 28px;"></td>
         <td style="width: 84px; height: 28px;">
                 <asp:Label ID="lblevent" runat="server" Text="Event:" Font-Bold="true"></asp:Label>
                 </td><td style="width: 332px; height: 28px;">
                          <asp:DropDownList id="drpevent" Width="140px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpevent_SelectedIndexChanged">
                          <asp:ListItem Value="2" Text="Chapter Contest" Selected="True"></asp:ListItem>
                              <asp:ListItem Value="2,13" Text="Contests & Coaching"></asp:ListItem>
                          <asp:ListItem Value="3" Text="Workshop"></asp:ListItem>
                          <asp:ListItem Value="1" Text="National Finals"></asp:ListItem>
                          <asp:ListItem Value="13" Text="Coaching"></asp:ListItem>
                          </asp:DropDownList> 
         </td>
        
       <td style="width: 35px; text-align:right" rowspan="2"><asp:Label ID="lblassigncategory" Text="Category:" runat="server" Font-Bold="true"></asp:Label></td>
       <td rowspan="2" style="width: 11px"><asp:ListBox id="lstAssigncategory" Height="100px" Enabled="false" DataValueField="RoleID" DataTextField="Name" SelectionMode="Multiple" Width="200px" runat="server" OnSelectedIndexChanged="lstAssigncategory_SelectedIndexChanged" AutoPostBack="true" >
                       <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                       <asp:ListItem Text="National" Value="1" ></asp:ListItem>
                       <asp:ListItem Text="Zonal" Value="2"></asp:ListItem>
                       <asp:ListItem Text="Cluster" Value="3"></asp:ListItem>
                       <asp:ListItem Text="Chapter" Value="4"></asp:ListItem>
                       <asp:ListItem Text="Finals" Value="5"></asp:ListItem>
                       <asp:ListItem Text="India Chapters" Value="6"></asp:ListItem>
                       </asp:ListBox>
      </td> <td style="width: 40px;text-align:right" rowspan="2"><asp:Label ID="lblunassigncategory" Text="Category:" runat="server" Font-Bold="true"  ></asp:Label></td>
       <td rowspan="2"><asp:ListBox id="lstUnassigncategory" DataValueField="VolunteerTaskID" DataTextField="TaskDescription" Enabled="false" SelectionMode="Multiple" width="200px" Height="100px" runat="server" OnSelectedIndexChanged="lstUnassigncategory_SelectedIndexChanged" AutoPostBack="true" >
                       <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                       <asp:ListItem Text="Finals" Value="1"></asp:ListItem>
                       <asp:ListItem Text="National" Value="2"></asp:ListItem>
                       <asp:ListItem Text="Chapter" Value="3"></asp:ListItem>
                       <asp:ListItem Text="India Chapters" Value="4"></asp:ListItem>
                       </asp:ListBox>    </td>              
                  </tr>
                
     <tr><td style="width:6px; height: 30px;"></td>
        <td style="width:64px; height: 30px;"></td>
         <td style="width: 84px; height: 30px;"><asp:Label ID="lblregistrationtype" runat="server" Text="Type of Registration:" Width="140px" Font-Bold="true"></asp:Label> 
                 </td><td style="width: 332px; height: 30px;">
                          <asp:DropDownList ID="drpregistrationtype" runat="server" >
                               </asp:DropDownList> </td>
        
                  </tr>

                  <tr style="height:25px"><td style="height: 95px; width: 6px;"></td><td style="width: 64px; height: 95px;"></td>
                 <td style="height: 95px; width: 84px"><asp:Label ID="lblproductgroup" Text="Product Group:" Width="140px" runat="server" Font-Bold="true"></asp:Label>
        </td><td style="height: 95px"><asp:ListBox id="lstProductGroup" DataValueField="ProductGroupID" DataTextField="Name" SelectionMode="Multiple"  Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" >
                                               </asp:ListBox>    </td>
                      <td colspan="4" align="right"><div id="divchooseexcelownlist" runat="server" visible="false">
                          <br />
                          <br />
                          <asp:Label ID="lblattfile" ForeColor="Red" Text="Upload Excel file with email list in column A:" runat="server"></asp:Label> <asp:FileUpload ID="AttachmentFile" runat="server"/>

                                                    </div></td>

                          </tr>
               <%-- <tr></tr>--%>
                 <tr style="height:25px"><td style="width: 6px"></td><td style="width: 64px"></td><td style="width: 84px"><asp:Label ID="lblProductid" Text="Product:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
                 </td><td style="width: 332px"><asp:ListBox id="lstProductid" Enabled="false" DataValueField="ProductCode" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductid_SelectedIndexChanged">
                                                </asp:ListBox>    <br /><br />
                 </td><td style="height: 24px; width: 40px;text-align:right" rowspan="2"><br /><asp:Label ID="lblassignrole" Text="Role: " runat="server" Font-Bold="true"></asp:Label></td>
       <td rowspan="2" style="width: 11px"><br /><asp:ListBox id="lstAssignRole" Height="100px" Enabled="false" DataValueField="RoleID" DataTextField="Name" SelectionMode="Multiple" Width="200px" runat="server" OnSelectedIndexChanged="lstAssignRole_SelectedIndexChanged" AutoPostBack="true" >
                       </asp:ListBox></td>
                       <td style="height: 24px; width: 40px;text-align:right" rowspan="2"><br /><asp:Label ID="lblunassignRole" Text="Role:" runat="server" Font-Bold="true"  ></asp:Label></td>
       <td rowspan="2"><br /><asp:ListBox id="lstUnassignRole" DataValueField="VolunteerTaskID" DataTextField="TaskDescription" Enabled="false" SelectionMode="Multiple" width="200px" Height="100px" runat="server" OnSelectedIndexChanged="lstUnassignRole_SelectedIndexChanged" AutoPostBack="true" >
                       </asp:ListBox>    </td> </tr>
                 
                 <tr style="height:25px"><td style="width: 6px"></td><td style="width: 64px"></td><td style="width: 84px">
                     <asp:Label ID="lblyear" Text="Event Year:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
                  </td><td style="width: 332px">
                 <asp:ListBox id="lstyear" SelectionMode="Multiple" Width="140px" Height="75px" runat="server" OnSelectedIndexChanged="lstyear_SelectedIndexChanged" AutoPostBack="true">
                 </asp:ListBox>
                         <br /><br /></td></tr>
                  <tr style="height:25px"><td style="width: 6px"></td><td style="width: 64px"></td><td style="width: 84px">&nbsp;</td><td style="width: 332px">
                      &nbsp;</td><td></td>
                  <td colspan="2">
                      <asp:Label ID="lblNote" runat="server" ForeColor="Red" 
                          Text="**Note:  Sends email if and only if <br /> a) Valid Email flag is NULL and  <br /> b) Newsletter is not in 2 or 3" 
                          Visible="true"></asp:Label>
                      </td></tr>
                 <tr style="height:25px"><td style="width: 6px"></td></tr>
                  <tr>
                      <td style="width:6px; height: 30px;"></td>
        <td style="width:64px; height: 30px;"></td>
         <td style="width: 84px; height: 30px;">  <asp:Label ID="Label1" Text="Exclude Paid Registrations:" runat="server" Width="140px" Font-Bold="true"></asp:Label> 
                 </td><td style="width: 332px; height: 30px;">
                         <asp:DropDownList ID="ddlPaid" runat="server">
                          <asp:ListItem Value="-1">Select one
</asp:ListItem>
                          <asp:ListItem Value="1">Chapter Contest</asp:ListItem>
                          <asp:ListItem Value="2">Finals</asp:ListItem>
                          <asp:ListItem Value="3">Workshop</asp:ListItem>
                          <asp:ListItem Value="4">Coaching</asp:ListItem>
                          <asp:ListItem Value="5">PrepClub</asp:ListItem>
                          <asp:ListItem Value="20">Online Workshop</asp:ListItem>
                      </asp:DropDownList>
                          </td>
                      </tr>
                 <tr><td style="height: 24px; width: 6px;"></td><td style="height: 24px; width: 64px;"></td><td style="width: 253px; height: 24px;" colspan="2">
                 <asp:Button ID="btnselectmail" runat="server" Text="Continue" />
                 </td></tr>
  </table>

   </asp:Content>

