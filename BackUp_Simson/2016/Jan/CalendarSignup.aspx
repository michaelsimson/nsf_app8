﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CalendarSignup.aspx.vb" Inherits="CalendarSignup" Title="CalendarSignup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script language="javascript" type="text/javascript">
        function ConfirmUpdate(sessionno, dy, tm) {

            if (confirm("There is already an existing approved session " + sessionno + " on " + dy + " at " + tm + ". Do you want to change it?")) {
                //alert("There is already an existing approved session " + sessionno + " on " + dy + " at " + tm + ". Do you want to change it?");
                document.getElementById('<%= btnUpdate.ClientID%>').click();
            }
            else {
                document.getElementById('<%= btnCancelGrid.ClientID%>').click();
            }
        }

        function PopupPicker(ctl) {
            var PopupWindow = null;
            settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('CalSignHelp.aspx?ID=' + ctl, 'CalSignUp Help', settings);
            PopupWindow.focus();
        }
        function JoinMeeting() {

            var url = document.getElementById("<%=hdnWebExMeetURL.ClientID%>").value;

            window.open(url, '_blank');

        }
        function StartMeeting() {

            JoinMeeting();

        }
    </script>
    <div>
        <span style="visibility: hidden">
            <asp:Button ID="btnUpdate" runat="server" Text="Update" />
            <asp:Button ID="btnCancelGrid" runat="server" Text="Cancel" />
            <asp:HiddenField ID="hdInAppr" runat="server" Value="" />
            <asp:HiddenField ID="hdToAppvId" runat="server" Value="" />
        </span>
        <table border="0" cellpadding="3" cellspacing="0" width="980">
            <tr>
                <td align="left">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>&nbsp;&nbsp;
                </td>
            </tr>
        </table>
        <table cellpadding="3" cellspacing="0" align="center">
            <tr>
                <td></td>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1>Calendar Signup</h1>
                </td>
            </tr>
        </table>
        <%--        <table border="1" cellpadding="3" cellspacing="0" align="center">
            <tr>
                <td align="center" colspan="2" style="font-size: 16px; font-weight: bold; font-family: Calibri">Calendar Signup</td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Name</td>
                <td style="width: 133px">
                    <asp:TextBox ID="txtName" runat="server" Style="margin-left: 0px" Width="150px" Enabled="false" Visible="false"></asp:TextBox>
                    <asp:DropDownList ID="ddlVolName" DataTextField="Name" DataValueField="MemberID" runat="server" AutoPostBack="True" Height="20px" Width="150px" Visible="false"></asp:DropDownList>
                    <asp:HiddenField ID="hdnMemberID" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center"></td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Event Year</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Event</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" DataTextField="EventCode" DataValueField="EventID" AutoPostBack="true" runat="server" Height="20px" Width="150px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Phase</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged" Width="100px" Height="20px" runat="server">
                        <asp:ListItem Value="1">One</asp:ListItem>
                        <asp:ListItem Value="2">Two</asp:ListItem>
                        <asp:ListItem Value="3">Three</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                            <a href="javascript:PopupPicker('Phase');">Help</a>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Product Group</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Product</td>
                <td align="left" style="width: 150px">
                    <asp:DropDownList ID="ddlProduct" DataTextField="Name"
                        DataValueField="ProductID" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false"
                        runat="server" Height="20px" Width="150px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Level</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlLevel" runat="server" Width="150px" Height="20px">
                        <%--<asp:ListItem Value="0" Selected="True">Beginner</asp:ListItem>
                         <asp:ListItem Value="1">Intermediate</asp:ListItem>
                           <asp:ListItem Value="2">Advanced</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Session #</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlSession" runat="server" Width="100px" Height="20px">
                        <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                            <a href="javascript:PopupPicker('Session');">Help</a>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Day</td>
                <td style="width: 133px">
                    <asp:DropDownList ID="ddlWeekDays" runat="server" AutoPostBack="true" Height="20px" Width="150px"></asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Time</td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlDisplayTime" runat="server" Height="20px" Width="120px"></asp:DropDownList>
                    <asp:Label ID="lblESTTime" runat="server" Text="EST"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Preferences</td>
                <td style="width: 133px">
                    <asp:DropDownList ID="ddlPref" runat="server" Height="20px" Width="100px">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                        <a href="javascript:PopupPicker('Pref');">Help</a>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 107px">Maximum Capacity </td>
                <td align="left" style="width: 133px">
                    <asp:DropDownList ID="ddlMaxCapacity" runat="server" AutoPostBack="true" Height="20px" Width="150px"></asp:DropDownList></td>
            </tr>

        </table>--%>



        <table width="1200px" align="center" style="margin-left: auto; margin-right: auto; font-weight: bold; color: green; background-color: #ffffcc;" id="tblAddUpd" runat="server" visible="true">
            <tr class="ContentSubTitle" align="center">
                <td style="text-align: right; color: green;">Year</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="70px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Event</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" DataTextField="EventCode" DataValueField="EventID" AutoPostBack="true" runat="server" Height="20px" Width="80px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Phase</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged" Width="50px" Height="20px" runat="server" Enabled="false">
                        <asp:ListItem Value="1" Selected="True">One</asp:ListItem>
                        <asp:ListItem Value="2">Two</asp:ListItem>
                        <asp:ListItem Value="3">Three</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                            <a href="javascript:PopupPicker('Phase');">Help</a>
                </td>
                <td style="text-align: right; color: green;">Product Group</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="100px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Product</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlProduct" DataTextField="Name"
                        DataValueField="ProductID" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false"
                        runat="server" Height="20px" Width="100px">
                    </asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Level</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlLevel" runat="server" Width="70px" Height="20px">
                        <%--<asp:ListItem Value="0" Selected="True">Beginner</asp:ListItem>
                         <asp:ListItem Value="1">Intermediate</asp:ListItem>
                           <asp:ListItem Value="2">Advanced</asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Session</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlSession" runat="server" Width="40px" Height="20px">
                        <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                            <a href="javascript:PopupPicker('Session');">Help</a>

                    <br />
                    <div style="margin-bottom: 10px;"></div>
                </td>

            </tr>
            <tr class="ContentSubTitle">
                <td style="text-align: right; color: green;">Day</td>
                <td>
                    <asp:DropDownList ID="ddlWeekDays" runat="server" AutoPostBack="true" Height="20px" Width="80px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Time</td>
                <td>
                    <asp:DropDownList ID="ddlDisplayTime" runat="server" Height="20px" Width="50px"></asp:DropDownList>
                    <asp:Label ID="lblESTTime" runat="server" Text="EST"></asp:Label></td>
                <td style="text-align: right; color: green;">Preference
                </td>
                <td>
                    <asp:DropDownList ID="ddlPref" runat="server" Height="20px" Width="50px">
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                        <a href="javascript:PopupPicker('Pref');">Help</a>
                </td>
                <td style="text-align: right; color: green;">Max cap</td>
                <td>
                    <asp:DropDownList ID="ddlMaxCapacity" runat="server" AutoPostBack="true" Height="20px" Width="40px"></asp:DropDownList>
                </td>
                <td style="text-align: right; color: green;">Name</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" Style="margin-left: 0px" Width="90px" Enabled="false" Visible="false"></asp:TextBox>
                    &nbsp;

                </td>
                <td>
                    <asp:DropDownList ID="ddlVolName" DataTextField="Name" DataValueField="MemberID" runat="server" AutoPostBack="True" Height="20px" Width="100px" Visible="false"></asp:DropDownList>
                    <asp:HiddenField ID="hdnMemberID" runat="server" />
                </td>
                <td>
                    <asp:Button ID="btnClear" runat="server" Text="Clear" Height="25px" Width="50px" />&nbsp;
              <asp:Button ID="btnSearch" runat="server" Text="Search" Height="25px" Width="70px" Visible="false" />
                </td>

                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td align="center" colspan="14">
                    <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="14">
                    <asp:Button ID="btnSubmit" runat="server" Text="Add/Update" />&nbsp;
                   <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="14">
                    <asp:Label CssClass="btn_02" ID="lblMesg" runat="server" Text="(Please signup for at least 3 alternate day/times, just in case)"></asp:Label>
                </td>
            </tr>
        </table>
        <table width="100%" align="center" style="margin-left: auto; margin-right: auto;">
            <tr>
                <td align="center">
                    <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>

        <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
            <b>Search NSF member</b>
            <div align="center">
                <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%" visible="true" bgcolor="silver">
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="Button1" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" /></td>
                        <td align="left">
                            <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" /></td>
                    </tr>
                </table>
                <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
            </div>
            <br />

            <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                <b>Search Result</b>
                <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                    <Columns>
                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                        <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                        <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                        <asp:BoundField DataField="chapter" HeaderText="Chapter"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </asp:Panel>
        <table align="center">
            <tr>
                <td align="center">
                    <span id="spnTableTitle" runat="server" visible="false" style="font-weight: bold;">Table 1 : Calendar Signup
                    <div style="float: left;">
                        <asp:Button ID="btnExport" runat="server" Text="Export" />
                    </div>
                    </span>
                    <asp:DataGrid ID="DGCoach" runat="server" DataKeyField="SignUpID"
                        AutoGenerateColumns="False" Height="14px" CellPadding="2" BackColor="Navy"
                        BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666" ForeColor="White" Font-Bold="True">
                        <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
                        <HeaderStyle Font-Size="X-Small" ForeColor="White" Font-Names="Verdana" Font-Bold="True"></HeaderStyle>
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:EditCommandColumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit">
                                <ItemStyle ForeColor="Blue"></ItemStyle>
                            </asp:EditCommandColumn>
                            <asp:ButtonColumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton" CommandName="Delete" Text="Delete">
                                <ItemStyle ForeColor="Blue"></ItemStyle>
                            </asp:ButtonColumn>
                            <asp:TemplateColumn HeaderText="Ser#">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSRNO" runat="server"
                                        Text='<%#Container.ItemIndex + 1%>'></asp:Label>

                                    <asp:Label ID="lblBeginTime" runat="server"
                                        Text='<%#DataBinder.Eval(Container, "DataItem.Begin")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblEndTime" runat="server"
                                        Text='<%#DataBinder.Eval(Container, "DataItem.End")%>' Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="SignUpID" ItemStyle-Width="50px" HeaderText="SignUp ID" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " ReadOnly="true" Visible="true">
                                <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                                <ItemStyle Width="50px"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Volunteer Name">
                                <HeaderStyle Width="130px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Name")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGMember" runat="server" DataTextField="Name" DataValueField="MemberId" OnPreRender="ddlDGMember_PreRender" OnSelectedIndexChanged="ddlDGMember_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </EditItemTemplate>

                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="ProductGroupCode" Visible="true">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                    <asp:HiddenField ID="hfProductGroupId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGProductGroup" runat="server" DataTextField="Name" DataValueField="ProductGroupId" Enabled="false" OnPreRender="ddlDGProductGroup_PreRender">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Product">
                                <HeaderStyle Width="80px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                    <asp:HiddenField ID="hfProductId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGProduct" runat="server" DataTextField="Name" DataValueField="ProductId" OnPreRender="ddlDGProduct_PreRender" OnSelectedIndexChanged="ddlDGProduct_SelectedIndexChanged" AutoPostBack="true" Enabled="false">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Level">
                                <HeaderStyle Width="100px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGLevel" runat="server" OnPreRender="ddlDGLevel_PreRender" AutoPostBack="false" Enabled="false">
                                        <%-- <asp:ListItem Value="0">Beginner</asp:ListItem>
                                               <asp:ListItem Value="1">Intermediate</asp:ListItem>
                                                 <asp:ListItem Value="2">Advanced</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </EditItemTemplate>

                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Session">
                                <HeaderStyle Width="100px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SessionNo")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGSessionNo" OnPreRender="ddlDGSessionNo_PreRender" runat="server" AutoPostBack="false" Enabled="false">
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>

                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Day">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDay" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Day")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGDay" runat="server" OnPreRender="ddlDGDay_PreRender" OnSelectedIndexChanged="ddlDaytime_SelectedIndexChanged" AutoPostBack="true" Enabled="false"></asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderText="Time">
                                <HeaderStyle Width="400px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGTime" runat="server" OnPreRender="ddlDGTime_PreRender" AutoPostBack="false" Enabled="false"></asp:DropDownList>
                                </EditItemTemplate>

                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderText="Accepted">
                                <HeaderStyle Width="400px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAccepted" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGAccepted" runat="server" OnPreRender="ddlDGAccepted_PreRender" AutoPostBack="false" Enabled="false">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>

                                <ItemStyle HorizontalAlign="Left"></ItemStyle>

                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Preferences">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPreferences" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Preference")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGPreferences" runat="server" OnPreRender="ddlDGPreferences_PreRender" AutoPostBack="false" Enabled="false">
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="2">3</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Max Cap">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCapacity")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGMaxCapacity" runat="server" OnPreRender="ddlDGMaxCapacity_PreRender" AutoPostBack="false" Enabled="false"></asp:DropDownList>
                                    <%--OnSelectedIndexChanged="ddlDGCapacity_SelectedIndexChanged"--%>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="#Of Students Approved">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblApprovedStudents" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.NStudents")%>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateColumn>


                            <asp:TemplateColumn HeaderText="VRoom">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGVRoom" runat="server" DataTextField="VRoom"
                                        DataValueField="VRoom" OnPreRender="ddlDGVRoom_PreRender" AutoPostBack="true" OnSelectedIndexChanged="ddlDGVRoom_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDGVRoom" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.VRoom") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="UserID">

                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDGUserID" runat="server"
                                        OnPreRender="txtDGUserID_PreRender" Width="200px"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDGUserID" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.UserID") %>'></asp:Label>
                                </ItemTemplate>

                                <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Password">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDGPWD" runat="server" OnPreRender="txtDGPWD_PreRender"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDGPWD" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.PWD") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Years">

                                <ItemTemplate>
                                    <asp:Label ID="lblYears" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.Years") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Sessions">

                                <ItemTemplate>
                                    <asp:Label ID="lblSessions" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.Sessions") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="150px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SessionKey">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSessionsKey" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.MeetingKey") %>'></asp:Label>
                                </ItemTemplate>


                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Meeting Pwd">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSessionsPwd" runat="server"
                                        Text='<%# DataBinder.Eval(Container, "DataItem.MeetingPwd") %>'></asp:Label>
                                </ItemTemplate>


                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Meeting URL">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>

                                    <asp:LinkButton runat="server" ID="lnkMeetingURL" Text='<%# Eval("HostJoinURL").ToString().Substring(0, Math.Min(20, Eval("HostJoinURL").ToString().Length)) + ""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("HostJoinURL").ToString()%>'></asp:LinkButton>

                                    <%--   <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("HostJoinURL")%>' runat="server" ToolTip='<%# Bind("HostJoinURL")%>'><%# Eval("HostJoinURL").ToString().Substring(0, Math.Min(20, Eval("HostJoinURL").ToString().Length)) + ""%></asp:HyperLink>--%>
                                </ItemTemplate>


                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Makeup Meeting URL" Visible="false">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>

                                    <asp:LinkButton runat="server" ID="lnkMakeUpMeetingURL" Text='<%# Eval("MakeUpURL").ToString().Substring(0, Math.Min(20, Eval("MakeUpURL").ToString().Length)) + ""%>' CommandName="SelectMakeUpURL" ToolTip='<%# Eval("MakeUpURL").ToString()%>'></asp:LinkButton>
                                    <div style="display: none;">

                                        <asp:Label ID="lblMakeupKey" runat="server"
                                            Text='<%# DataBinder.Eval(Container, "DataItem.MakeUpMeetKey") %>'></asp:Label>
                                    </div>
                                    <%--   <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("HostJoinURL")%>' runat="server" ToolTip='<%# Bind("HostJoinURL")%>'><%# Eval("HostJoinURL").ToString().Substring(0, Math.Min(20, Eval("HostJoinURL").ToString().Length)) + ""%></asp:HyperLink>--%>
                                </ItemTemplate>


                            </asp:TemplateColumn>


                            <asp:TemplateColumn HeaderText="Event Year">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGEventYear" runat="server" DataTextField="EventYear" DataValueField="EventYear" OnPreRender="ddlDGEventYear_PreRender" AutoPostBack="false" Enabled="false"></asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EventCode">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEvent" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventCode")%>'></asp:Label>
                                    <asp:HiddenField ID="hfEventId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.EventId")%>' />

                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGEvent" runat="server" DataTextField="EventCode" DataValueField="EventID" OnPreRender="ddlDGEvent_PreRender" AutoPostBack="false" Enabled="false">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Phase">
                                <HeaderStyle Width="50px" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPhase" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Phase")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDGPhase" runat="server" OnPreRender="ddlDGPhase_PreRender" AutoPostBack="true" Enabled="false">
                                        <asp:ListItem Value="1">One</asp:ListItem>
                                        <asp:ListItem Value="2">Two</asp:ListItem>
                                        <asp:ListItem Value="3">Three</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateColumn>


                        </Columns>

                        <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>
                        <AlternatingItemStyle BackColor="LightBlue" />
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        </ruby-span>
    <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="spnTable1Title" runat="server" visible="true">Table 2 : Makeup Sessions</span>


        </div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="SpnMakeupTitle" runat="server" visible="false" style="color: red;">No record exists</span>


        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand">
            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                <asp:TemplateField HeaderText="Coach">

                    <ItemTemplate>

                        <asp:Label runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                <asp:TemplateField HeaderText="Begin Time">

                    <ItemTemplate>
                        <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="End Time" Visible="false">

                    <ItemTemplate>
                        <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                <asp:TemplateField HeaderText="meeting URL">

                    <ItemTemplate>

                        <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>
                        <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserID" HeaderText="User ID"></asp:BoundField>
                <asp:BoundField DataField="Pwd" HeaderText="Pwd"></asp:BoundField>
            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="Span1" runat="server" visible="true">Table 3 : Substitute Sessions</span>


        </div>
        <div align="center" style="font-weight: bold; color: #64A81C;">
            <span id="SpnSubstituteTitle" runat="server" visible="false" style="color: red;">No record exists</span>


        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GrdSubstituteSessions" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdSubstituteSessions_RowCommand">
            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                <asp:BoundField DataField="Phase" HeaderText="Phase"></asp:BoundField>
                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                <asp:TemplateField HeaderText="Coach">

                    <ItemTemplate>

                        <asp:Label runat="server" ID="lblCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Substitute Coach">

                    <ItemTemplate>

                        <asp:Label runat="server" ID="Label2" Text='<%# Bind("SubCoach")%>' CommandName="SelectLink"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                <asp:TemplateField HeaderText="Begin Time">

                    <ItemTemplate>
                        <asp:Label ID="lblBeginTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="End Time" Visible="false">

                    <ItemTemplate>
                        <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                <asp:TemplateField HeaderText="meeting URL">

                    <ItemTemplate>

                        <asp:LinkButton runat="server" ID="lblMeetingURL" Text='<%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>
                        <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UserID" HeaderText="User ID"></asp:BoundField>
                <asp:BoundField DataField="Pwd" HeaderText="Pwd"></asp:BoundField>
            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center"><span style="font-weight:bold;">Table 3: Guest Attendee</span></div>
        <div align="center">
            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdGuestAttendee" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 100%; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" HeaderStyle-Width="70">

                        <ItemTemplate>
                            <div style="display: none;">

                                <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblGuestAttendID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"GuestAttendID") %>'>'></asp:Label>

                                <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="LblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pwd") %>'>'></asp:Label>
                                <asp:Label ID="LblMeetingKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'>'></asp:Label>
                                <asp:Label ID="LblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="LblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>
                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"WebExEmail") %>'>'></asp:Label>
                                <asp:Label ID="LblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>

                            </div>
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" CommandName="DeleteAttendee" />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Guest Coach"></asp:BoundField>
                    <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="CoachName" HeaderText="Coach"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>

                    <asp:BoundField DataField="RegisteredID" HeaderText="Registered ID"></asp:BoundField>
                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("MeetingURL").ToString().Substring(0,Math.Min(20,Eval("MeetingURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingURL").ToString()%>'></asp:LinkButton>

                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
        <input type="hidden" id="hdnTrainingSessionKey" value="" runat="server" />
        <input type="hidden" id="hdnHostMeetingURL" value="" runat="server" />
        <input type="hidden" id="hdnWebExMeetURL" value="" runat="server" />
        <input type="hidden" id="hdnUserID" value="" runat="server" />
        <input type="hidden" id="hdnPwd" value="" runat="server" />

        <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
        <input type="hidden" id="hdnHostURL" value="0" runat="server" />
        <input type="hidden" id="HdnVRoom" value="0" runat="server" />
        <input type="hidden" id="hdnTime" value="" runat="server" />

        <input type="hidden" id="HdnVroomUID" value="" runat="server" />
        <input type="hidden" id="hdnVroomPwd" value="" runat="server" />

    </div>
</asp:Content>
