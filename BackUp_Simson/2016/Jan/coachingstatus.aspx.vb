﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports RKLib.ExportData
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections

Imports System.Net

Imports System.Xml
Partial Class coachingstatus
    Inherits System.Web.UI.Page
    Dim Year As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("entryToken") = "Parent"
        'Session("LoggedIn") = "true"
        'Session("CustIndID") = 67479

        '**Debug 
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If Now.Month <= 5 Then
                Session("Year") = Now.Year - 1
            Else
                Session("Year") = Now.Year
            End If
            If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
                hlinkParentRegistration.Visible = True
                hlinkStudent.Visible = False
            ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                hlinkParentRegistration.Visible = False
                hlinkStudent.Visible = True

            End If
            LoadSelectedCoaching()
            LoadMakeUpSessions()
            LoadSubstituteSessions()
            If (hdMemberId.Value = "51336") Then
                lblCoachName.Visible = True
                Span2.Visible = True
                BindCoachClassDetailsInGrid(rpt_grdCoachClassCal, 0)
            Else
                Span2.Visible = False
                lblCoachName.Visible = False
            End If


        End If
    End Sub
    Private Sub LoadSelectedCoaching()
        Dim SQLStr As String = String.Empty
        Dim year As Integer
        'year = 2015
        year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select max(EventYear) from CalSignup")

        SQLStr = "select CR.CoachRegID,CR.RegisteredID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+' EST' as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, C.UserID, C.Pwd as WebExPwd, Ch.OnlineClassEmail "
        SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	+ ' hr(s)' as Duration, P.ProductGroupID,P.ProductID,CR.Phase,CR.SessionNo,CMemberID "
        SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Phase = C.Phase Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID"
        If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
            SQLStr = SQLStr & " where CR.PMemberid=" & Session("LoginID") & " and CR.EventYear =" & Year & " and C.Accepted='Y'"
        Else
            SQLStr = SQLStr & " where CR.ChildNumber=" & Session("StudentId") & " and CR.EventYear =" & Year & " and C.Accepted='Y'"
        End If

        Try
           
            Dim conn As New SqlConnection(Application("ConnectionString"))



            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            dgselected.DataSource = drCoaching
            dgselected.DataBind()
            If dgselected.Items.Count > 0 Then
                dgselected.Visible = True
            Else
                lblErr.Text = "No coaching Registration"
                dgselected.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
            If ds.Tables(0).Rows.Count > 0 Then


                For Each dr As DataRow In ds.Tables(0).Rows

                    'If dr("AttendeeJoinURL").ToString() <> "" Then

                    If (dr("CMemberID").ToString() = "51336") Then


                        hdnProductGroupID.Value = dr("ProductGroupID").ToString()
                        hdnProductID.Value = dr("ProductID").ToString()
                        hdnPhase.Value = dr("Phase").ToString()
                        hdnSessionNo.Value = dr("SessionNo").ToString()
                        hdMemberId.Value = dr("CMemberID").ToString()
                        hdnLevel.Value = dr("Level").ToString()
                        hdTable1Year.Value = DateTime.Now.Year
                    End If

                    'End If
                Next
            End If
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            lblErr.Text = SQLStr + "" + ex.Message
        End Try
    End Sub
    Dim hasNotApprFlag As Boolean

    Protected Sub dgselected_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgselected.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            ' Retrieve the text of the Approved status from the DataGridItem
            Dim ApprovedStatus As String = CType(e.Item.FindControl("lblApproved"), Label).Text
            If ApprovedStatus.ToLower.Trim <> "y" Then
                hasNotApprFlag = True
            End If
        End If

    End Sub


    Private Sub LoadMakeUpSessions()
        Try
            Dim year As Integer
            ' year = DateTime.Now.Year
            year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select max(EventYear) from CalSignup")
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            SQLStr = "select CR.CoachRegID,CR.MakeupRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,WC.Day, CONVERT(varchar(15),CAST(WC.BeginTIME AS TIME),100)+' EST' as Time,WC.StartDate,WC.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, WC.UserID, WC.Pwd as WebExPwd, Ch.OnlineClassEmail, C.MakeUpmeetKey "
            SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	+ ' hr(s)' as Duration"
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Phase = C.Phase  inner join WebConfLog WC on (C.MemberID=WC.MemberID and C.MakeupURL=wc.MeetingURL) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & year & " and C.Accepted='Y'"
            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            DgMakeupSession.DataSource = drCoaching
            DgMakeupSession.DataBind()
            If DgMakeupSession.Items.Count > 0 Then
                DgMakeupSession.Visible = True
                SpnMakeupTitle.Visible = False
            Else
                SpnMakeupTitle.Visible = True
                DgMakeupSession.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadSubstituteSessions()
        Try
            Dim year As Integer
            year = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select max(EventYear) from CalSignup")
            Dim SQLStr As String
            SQLStr = "select CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,IP.FirstName + ' ' + IP.LastName as SubCoachName,Case when CR.Approved ='Y' then I.Email Else '' End as CoachEmail,C.Level,C.MaxCapacity,CR.ChildNumber,CR.AttendeeJoinURL,CR.MakeUpURL,WC.Day, CONVERT(varchar(15),CAST(WC.BeginTIME AS TIME),100)+' EST' as Time,WC.StartDate,WC.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then case when CR.Approved='Y' then 'Adjusted' Else 'Pending' END Else 'Paid' End as Status,CR.Approved,CR.SessionNo, C.MeetingKey, C.UserID, C.Pwd as WebExPwd, Ch.OnlineClassEmail "
            SQLStr = SQLStr & ",Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	+ ' hr(s)' as Duration, CR.SubstituteURL,C.SubstituteMeetKey"
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and CR.Phase = C.Phase  inner join WebConfLog WC on (C.MemberID=WC.MemberID and C.SubstituteURL=wc.MeetingURL) Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID Inner Join IndSpouse IP ON C.SubstituteCoachID=IP.AutoMemberID  where  CR.PMemberid=" & Session("CustIndID") & IIf(Session("StudentId") > 0, " and CR.ChildNumber=" & Session("StudentId"), "") & " and CR.EventYear =" & year & " and C.Accepted='Y'"
            '" & Session("Year") & "
            ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ' Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
            SQLStr = SQLStr & " Order by Ch.Childnumber,P.ProductGroupId,P.ProductId"
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            hasNotApprFlag = False
            lblNote.Visible = False
            DataGrdSubstituteSessions.DataSource = drCoaching
            DataGrdSubstituteSessions.DataBind()
            If DataGrdSubstituteSessions.Items.Count > 0 Then
                DataGrdSubstituteSessions.Visible = True
                SpnSubstituteTitle.Visible = False
            Else
                SpnSubstituteTitle.Visible = True
                DataGrdSubstituteSessions.Visible = False
            End If
            If hasNotApprFlag = True Then
                lblNote.Visible = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgselected.ItemCommand
        Dim cmdName As String = e.CommandName
        If e.CommandName = "SelectMeetingURL" Then
            hdnIsCheck.Value = "Yes"
            Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
            hdnChildMeetingURL.Value = MeetingURL
            Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

            Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
            Dim ChildName As String = CType(e.Item.FindControl("lblChildName"), Label).Text
            HdnChildName.Value = ChildName
            Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
            Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
            Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
            Dim Coachname As String = CType(e.Item.FindControl("lblCoach"), Label).Text
            Dim RegID As String = String.Empty
            RegID = CType(e.Item.FindControl("lblRegisteredID"), Label).Text
            Dim ds As DataSet = New DataSet()
            Dim CmdText As String = String.Empty

            HdnWebExID.Value = WebExID
            HdnWebExPwd.Value = WebExPwd
            hdnSessionKey.Value = SessionKey
            hdnOnlineClassEmail.Value = OnlineClassEmail
            hdnAttendeeRegisteredID.Value = RegID

            CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then
                        Dim status As String = GetTrainingSessions(WebExID, WebExPwd, SessionKey)

                        If status = "INPROGRESS" Then
                            GetJoinMeetingURL(hdnSessionKey.Value, HdnWebExID.Value, HdnWebExPwd.Value, hdnMeetingAttendeeID.Value, ChildName, OnlineClassEmail, "training")
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", True)
                        Else

                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" & ProductCode & "','" & Coachname & "');", True)
                        End If

                    Else
                        lblErr.Text = "Only authorized child can join into the training session."
                    End If

                End If
            End If

        End If
    End Sub



    Public Function GetTrainingSessions(WebExID As String, Pwd As String, SessionKey As String) As String
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.training.GetTrainingSession"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & SessionKey & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = ProcessTrainingStatusTestResponse(xmlReply)
        'lblMsg3.Text = result;
        Return result

    End Function
    Private Function ProcessTrainingStatusTestResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Dim MeetingStatus As String = String.Empty
        Dim startTime As String = String.Empty
        Dim Day As String = String.Empty
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)

            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession")
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText
            Dim ExpirationDate As String = String.Empty
            If status = "SUCCESS" Then
                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText
                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText
                Try
                    Day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText
                    Dim Attendees As String = String.Empty
                    Attendees = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:attendees/sess:participants/sess:participant/sess:person/com:name", manager).InnerText
                Catch ex As Exception
train:              ExpirationDate = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:expirationDate", manager).InnerText
                    Dim dtExpDate As DateTime = Convert.ToDateTime(ExpirationDate).ToString("MM/dd/yyyy")
                    Day = dtExpDate.ToString("dddd")
                End Try

                startTime = startTime.Substring(10, 9)

                hdnSessionStartTime.Value = startTime
                Dim Mins As Double = 0.0
                Dim dFrom As DateTime = DateTime.Now
                Dim dTo As DateTime = DateTime.Now
                Dim sDateFrom As String = startTime

                Dim today As String = DateTime.Today.DayOfWeek.ToString()
                If today.ToLower() = Day.ToLower() Then
                    Dim sDateTo As String = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)

                    'TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    'DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    Dim easternTime As DateTime = DateTime.Now
                    Dim strEasternTime As String = easternTime.ToShortDateString()
                    strEasternTime = Convert.ToString(strEasternTime & Convert.ToString(" ")) & sDateFrom
                    Dim dtEasternTime As DateTime = Convert.ToDateTime(strEasternTime)
                    Dim easternTimeNow As DateTime = DateTime.Now.AddHours(1)

                    Dim TS As TimeSpan = dtEasternTime - easternTimeNow


                    Mins = TS.TotalMinutes
                Else
                    For i As Integer = 1 To 7
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString()
                        If today.ToLower() = Day.ToLower() Then
                            dTo = DateTime.UtcNow.AddDays(i)
                            'TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            'DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            Dim easternTime As DateTime = DateTime.Now.AddDays(i)
                            Dim sDateTo As String = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                            Dim targetDateTime As String = Convert.ToString(easternTime.ToShortDateString() + " ") & sDateFrom
                            'DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            Dim easternTimeNow As DateTime = DateTime.Now.AddHours(1)

                            Dim dtTargetTime As DateTime = Convert.ToDateTime(targetDateTime)
                            Dim TS As TimeSpan = dtTargetTime - easternTimeNow


                            Mins = TS.TotalMinutes
                        End If
                    Next
                End If
                Dim DueMins As String = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."))
                hdnSessionStartTime.Value = startTime
                hdnStartMins.Value = Convert.ToString(DueMins)
            End If
            If MeetingStatus = "INPROGRESS" Then


                Dim Attendees As String = String.Empty
                Dim isExist As Integer = 0
                Try
                    For i As Integer = 1 To 100

                        Attendees = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:attendees/sess:participants/sess:participant[" & i & "]/sess:person/com:name", manager).InnerText
                        If Attendees = HdnChildName.Value Then
                            isExist = 1
                        Else

                        End If

                    Next
                Catch ex As Exception

                End Try
                If hdnIsCheck.Value = "Yes" Then


                    If isExist <> 1 Then
                        RegisterMeetingAttendee(hdnSessionKey.Value, HdnWebExID.Value, HdnWebExPwd.Value, "", HdnChildName.Value, "", hdnOnlineClassEmail.Value, "US")

                    Else

                    End If
                End If
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return MeetingStatus
    End Function

    Protected Sub DgMakeupSession_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        If e.CommandName = "SelectMeetingURL" Then
            hdnIsCheck.Value = "No"
            Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
            hdnChildMeetingURL.Value = MeetingURL
            Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

            Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
            Dim ChildName As String = CType(e.Item.FindControl("lblChildName"), Label).Text
            HdnChildName.Value = ChildName
            Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
            Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
            Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
            Dim Coachname As String = CType(e.Item.FindControl("lblCoach"), Label).Text
            Dim RegID As String = String.Empty
            RegID = CType(e.Item.FindControl("lblRegisteredID"), Label).Text
            Dim ds As DataSet = New DataSet()
            Dim CmdText As String = String.Empty

            HdnWebExID.Value = WebExID
            HdnWebExPwd.Value = WebExPwd
            hdnSessionKey.Value = SessionKey
            hdnOnlineClassEmail.Value = OnlineClassEmail
            hdnAttendeeRegisteredID.Value = RegID


            CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then
                        Dim status As String = GetTrainingSessions(WebExID, WebExPwd, SessionKey)
                        If status = "INPROGRESS" Then
                            GetJoinMeetingURL(hdnSessionKey.Value, HdnWebExID.Value, HdnWebExPwd.Value, hdnMeetingAttendeeID.Value, ChildName, OnlineClassEmail, "training")
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", True)
                        Else

                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" & ProductCode & "','" & Coachname & "');", True)
                        End If

                    Else
                        lblErr.Text = "Only authorized child can join ito the training session."
                    End If

                End If
            End If

        End If
    End Sub
    Protected Sub DataGrdSubstituteSessions_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        If e.CommandName = "SelectMeetingURL" Then
            hdnIsCheck.Value = "No"
            Dim MeetingURL As String = CType(e.Item.FindControl("LblMeetingURL"), Label).Text
            hdnChildMeetingURL.Value = MeetingURL
            Dim OnlineClassEmail As String = CType(e.Item.FindControl("LblOnlineClassEmail"), Label).Text

            Dim WebExID As String = CType(e.Item.FindControl("lblWebExID"), Label).Text
            Dim WebExPwd As String = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
            Dim SessionKey As String = CType(e.Item.FindControl("lblSessionKey"), Label).Text
            Dim ProductCode As String = CType(e.Item.FindControl("lblPrdCode"), Label).Text
            Dim Coachname As String = CType(e.Item.FindControl("lblExistCoachName"), Label).Text
            Dim ds As DataSet = New DataSet()
            Dim CmdText As String = String.Empty
            CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'"
            ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If (Convert.ToInt32(ds.Tables(0).Rows(0)("Countset").ToString()) > 0) Then
                        Dim status As String = GetTrainingSessions(WebExID, WebExPwd, SessionKey)
                        If status = "INPROGRESS" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", True)
                        Else
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" & ProductCode & "','" & Coachname & "');", True)
                        End If

                    Else
                        lblErr.Text = "Only authorized child can join ito the training session."
                    End If

                End If
            End If

        End If
    End Sub

    Public Sub RegisterMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, City As String, Email As String, Country As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.RegisterMeetingAttendee"">" & vbCr & vbLf

        strXML += "<attendees>" & vbCr & vbLf
        strXML += "<person>" & vbCr & vbLf
        strXML += "<name>" & Name & "</name>" & vbCr & vbLf
        strXML += "<title>title</title>" & vbCr & vbLf
        strXML += "<company>microsoft</company>" & vbCr & vbLf
        strXML += "<address>" & vbCr & vbLf
        strXML += "<addressType>PERSONAL</addressType>" & vbCr & vbLf
        'strXML += "<city>" & City & "</city>" & vbCr & vbLf
        strXML += "<country>US</country>" & vbCr & vbLf
        strXML += "</address>" & vbCr & vbLf

        strXML += "<email>" & Email & "</email>" & vbCr & vbLf
        strXML += "<notes>notes</notes>" & vbCr & vbLf
        strXML += "<url>https://</url>" & vbCr & vbLf
        strXML += "<type>VISITOR</type>" & vbCr & vbLf
        strXML += "</person>" & vbCr & vbLf
        strXML += "<joinStatus>ACCEPT</joinStatus>" & vbCr & vbLf
        strXML += "<role>ATTENDEE</role>" & vbCr & vbLf

        strXML += "<sessionKey>" & sessionKey & "</sessionKey>" & vbCr & vbLf
        strXML += "</attendees>" & vbCr & vbLf


        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = RegisterMeetingAttendeeResponse(xmlReply)
    End Sub
    Private Function RegisterMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim RegID As String
                RegID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText
                Dim attendeeID As String

                attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText

                hdnMeetingAttendeeID.Value = attendeeID
                hdnAttendeeRegisteredID.Value = RegID
                GetJoinMeetingURL(hdnSessionKey.Value, HdnWebExID.Value, HdnWebExPwd.Value, "", HdnChildName.Value, hdnOnlineClassEmail.Value, "training")
            ElseIf status = "FAILURE" Then


            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
    Public Sub GetJoinMeetingURL(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, Email As String, MeetingPassword As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf

        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID.Trim() & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd.Trim() & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GetjoinurlMeeting"">" & vbCr & vbLf

        strXML += "<sessionKey>" + sessionKey.Trim() + "</sessionKey>" & vbCr & vbLf
        strXML += "<attendeeName>" + Name.Trim() + "</attendeeName>" & vbCr & vbLf
        strXML += "<attendeeEmail>" + Email.Trim() + "</attendeeEmail>" & vbCr & vbLf
        strXML += "<meetingPW>" + MeetingPassword.Trim() + "</meetingPW>" & vbCr & vbLf
        strXML += "<RegID>" + hdnAttendeeRegisteredID.Value.Trim() + "</RegID>" & vbCr & vbLf

        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingAttendeeURLResponse(xmlReply)
    End Sub
    Private Function MeetingAttendeeURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnChildMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
    Protected Sub rpt_grdCoachClassCal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lbl As Label = CType(e.Row.FindControl("lblSerial"), Label)
                lbl.Text = Convert.ToInt16(e.Row.RowIndex) + 1
                Dim lblWeek As Label = CType(e.Row.FindControl("lblWeek"), Label)
                Dim lblStartDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblStartDate"), Label)


                Dim lblPhase As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblPhase"), Label)
                Dim lblSessionNo As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblSessionNo"), Label)
                Dim rep_hdWeekCnt As HiddenField = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_hdWeekCnt"), HiddenField)

                Dim lblDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblDate"), Label)
                Dim lblCoachClassCalID As Label = DirectCast(e.Row.Cells(1).FindControl("lblCoachClassCalID"), Label)



                'FillCoachByCondition(hdProductGrp.Value, hdProduct.Value, ddlSubstitute)


                Dim lblHWRelDate As Label = DirectCast(e.Row.Cells(10).FindControl("lblHWRelDate"), Label)
                Dim lblHWDueDate As Label = DirectCast(e.Row.Cells(11).FindControl("lblHWDueDate"), Label)
                Dim lblARelDate As Label = DirectCast(e.Row.Cells(12).FindControl("lblARelDate"), Label)
                Dim lblSRelDate As Label = DirectCast(e.Row.Cells(13).FindControl("lblSRelDate"), Label)
                Dim lblDay As Label = DirectCast(e.Row.Cells(2).FindControl("lblDay"), Label)
                Dim lblStatus As Label = DirectCast(e.Row.Cells(7).FindControl("lblStatus"), Label)
                Dim lblProdGroupID As Label = DirectCast(e.Row.Cells(7).FindControl("lblProductGroupID"), Label)
                Dim lblProdID As Label = DirectCast(e.Row.Cells(7).FindControl("lblProductID"), Label)
                Dim lblScheduleType As Label = DirectCast(e.Row.Cells(10).FindControl("lblScheduleType"), Label)
                'If lblCoachClassCalID.Text = "" Or ddlStatus.SelectedIndex = 0 Then
                '    lblHWRelDate.Text = lblDate.Text
                '    lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                '    lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                'Else
                '    lblHWRelDate.Text = lblDate.Text
                '    lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                '    lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                'End If

                Dim lblRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblRelDt"), Label)
                If lblRelDT.Text.Trim = "" Then
                    If lblStatus.Text <> "Cancelled" Then


                        lblHWRelDate.Text = lblDate.Text
                        lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                        lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                        If lblProdGroupID.Text = "31" Then
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(8)).Date.ToString("MM/dd/yyyy")
                        ElseIf lblProdGroupID.Text = "33" Then
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(8)).Date.ToString("MM/dd/yyyy")
                        ElseIf lblProdGroupID.Text = "41" Then
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(6)).Date.ToString("MM/dd/yyyy")
                        Else
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                        End If

                    End If
                Else

                    Dim lblDueDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblDueDt"), Label)
                    Dim lblARelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblARelDt"), Label)
                    Dim lblSRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblSRelDt"), Label)
                    lblHWRelDate.Text = lblRelDT.Text
                    lblHWDueDate.Text = DateTime.ParseExact(lblDueDT.Text, "MM/dd/yyyy", Nothing)
                    lblARelDate.Text = DateTime.ParseExact(lblARelDT.Text, "MM/dd/yyyy", Nothing)
                    lblSRelDate.Text = DateTime.ParseExact(lblSRelDT.Text, "MM/dd/yyyy", Nothing)

                End If

                'Dim curWeek As Integer, TotalWeek As Integer
                'Dim hdEndDate As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdEndDate"), HiddenField)
                'Dim stWeek As Date, endWeek As Date, curDate As Date, endDate As Date
                'curDate = DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing)
                'stWeek = DateAdd("d", 0 - curDate.DayOfWeek, curDate)
                'endWeek = DateAdd("d", 6 - curDate.DayOfWeek, curDate)
                'endDate = DateTime.ParseExact(hdEndDate.Value, "MM/dd/yyyy", Nothing)
                'If endDate >= stWeek And endDate <= endWeek Then
                '    Dim lnkBtnShowNxt As LinkButton = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
                '    lnkBtnShowNxt.Enabled = False
                'End If

                'If lblCoachClassCalID.Text = 0 Or ddlStatus.SelectedItem.Text = "Not set" Or ddlStatus.SelectedItem.Text = "Cancelled" Then
                '    cmdText = "select isnull(max(weekno),0) as weekno from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Phase=" + lblPhase.Text + " and SessionNo=" + lblSessionNo.Text + " and Day='" + lblDay.Text + "' and [date] <'" & lblDate.Text & "' and Status in ('On','Substitute') "
                '    Dim dsClass As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                '    If dsClass.Tables(0).Rows.Count > 0 Then
                '        curWeek = dsClass.Tables(0).Rows(0).Item("weekno")
                '        TotalWeek = hdTotalClass.Value
                '        If curWeek >= TotalWeek Then
                '            Dim lnkBtnShowNxt As LinkButton = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
                '            lnkBtnShowNxt.Enabled = False
                '        End If
                '    End If
                '    lblWeek.Text = curWeek
                'End If
                'Disable Modify Button

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Private Sub BindCoachClassDetailsInGrid(grCtrl As GridView, iShowAll As Integer)
        'Dim dt As Date = Now.Date.ToString("yyyy-MM-dd")  ' "2014-12-02"
        'Dim rpItem As RepeaterItem = grCtrl.NamingContainer
        'Dim iProductId As Integer = DirectCast(rpItem.FindControl("hdProduct"), HiddenField).Value
        'Dim iProductGrpId As Integer = DirectCast(rpItem.FindControl("hdProductGrp"), HiddenField).Value
        'Dim iPhase As Integer = DirectCast(rpItem.FindControl("lblPhase"), Label).Text
        'Dim strLevel As String = DirectCast(rpItem.FindControl("lblLevel"), Label).Text
        'Dim iSessionNo As Integer = DirectCast(rpItem.FindControl("lblSessionNo"), Label).Text
        'Dim rep_hdWeekCnt As HiddenField = TryCast(rpItem.FindControl("rep_hdWeekCnt"), HiddenField)

        'If Len(Trim(rep_hdWeekCnt.Value)) = 0 Then
        '    rep_hdWeekCnt.Value = "1"
        'End If
        '@MemberId int, @EventYear int, @ProductGroupId int, @ProductId int, @Level varchar(50), @SessionNo int , @Iterator int
        Dim params() As SqlParameter = {New SqlParameter("@MemberId", SqlDbType.Int), New SqlParameter("@EventYear", SqlDbType.Int), New SqlParameter("@ProductGroupId", SqlDbType.Int), New SqlParameter("@ProductId", SqlDbType.Int), New SqlParameter("@Phase", SqlDbType.Int), New SqlParameter("@Level", SqlDbType.VarChar, 50), New SqlParameter("@SessionNo", SqlDbType.Int), New SqlParameter("@Iterator", SqlDbType.Int), New SqlParameter("@ShowAll", SqlDbType.Int)}
        params(0).Value = hdMemberId.Value
        params(1).Value = hdTable1Year.Value
        params(2).Value = hdnProductGroupID.Value
        params(3).Value = hdnProductID.Value
        params(4).Value = hdnPhase.Value
        params(5).Value = hdnLevel.Value
        params(6).Value = hdnSessionNo.Value
        params(7).Value = 16
        params(8).Value = 0
        'productgroupID, productID, phase, level, week#, DocType=Q, 
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.StoredProcedure, "usp_GetCoachDetailsByWeek", params)
        If ds.Tables(0).Rows.Count = 0 Then
            'rpItem.Visible = False
        Else
            grCtrl.DataSource = ds.Tables(0)
            grCtrl.DataBind()
        End If
    End Sub

End Class
