﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="xlReportRegByOnLineWorkshop.aspx.cs" Inherits="xlReportRegByOnLineWorkshop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     


    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
         <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>
        </tr>
        <tr>

            <td>
                   <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong> Online Workshop Registration Report</strong> </div>
            </td>
        </tr>
       
      <tr>

            <td align="right">
                 <asp:Button ID="excel" runat="server"  Text="Export To Excel" 
                onclick="excel_Click" enabled="false" />
            </td>
        </tr>
       <tr>
           <td colspan="2" align="center">
               

                            <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
 <table width="100%" style="margin-left: auto; margin-right: auto; font-weight: bold" runat="server" id="tblCoach" visible="true" >
                     
                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        
                        <td>ProductGroup</td>
                        <td>
                            <asp:DropDownList ID="ddlProductGroup" AutoPostBack="True" runat="server" DataTextField="Name" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Workshop Date</td>
                        <td>
                             <asp:DropDownList ID="ddlworkshopDate" runat="server" AutoPostBack="True"  DataValueField="EventDate" Width="110px" OnSelectedIndexChanged="ddlworkshopDate_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                       

                    </tr>
     <tr>
         <td colspan="8" align="center"><asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="Button1_Click" />
                        <br />
                        <asp:Label ID="lblErr" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"></asp:Label>
                     </td>
         

     </tr>
     <tr align="center">

         <td colspan="8"><div style="width:1000px;height:400px; overflow:auto;">
             
             <asp:GridView ID="gvOnlineWorkshopRegReport" runat="server" width="100%" AutoGenerateColumns="False" EnableModelValidation="True">
                 <Columns>
                     <asp:BoundField DataField="RegistrationDate" HeaderText="RegistrationDate" />
                     <asp:BoundField DataField="WkshopDate" HeaderText="WkshopDate" />
                     <asp:BoundField DataField="Wkshop" HeaderText="Wkshop" />
                     <asp:BoundField DataField="ChildLastName" HeaderText="ChildLastName" />
                     <asp:BoundField DataField="ChildFirstName" HeaderText="ChildFirstName" />
                     <asp:BoundField DataField="Grade" HeaderText="Grade" />
                     <asp:BoundField DataField="DOB" HeaderText="DOB" />
                     <asp:BoundField DataField="GenderChild" HeaderText="GenderChild" />
                     <asp:BoundField DataField="CPhoneFather" HeaderText="CPhoneFather" />
                     <asp:BoundField DataField="HomePhone" HeaderText="HomePhone" />
                     <asp:BoundField DataField="LastNameFather" HeaderText="LastNameFather" />
                     <asp:BoundField DataField="FirstNameFather" HeaderText="FirstNameFather" />
                     <asp:BoundField DataField="EmailFather" HeaderText="EmailFather" />
                     <asp:BoundField DataField="Status" HeaderText="Status" />
                     <asp:BoundField DataField="SchoolName" HeaderText="SchoolName" />
                     <asp:BoundField DataField="ProductID" HeaderText="ProductID" />
                     <asp:BoundField DataField="LastNameMother" HeaderText="LastNameMother" />
                     <asp:BoundField DataField="FirstNameMother" HeaderText="FirstNameMother" />
                     <asp:BoundField DataField="EmailMother" HeaderText="EmailMother" />
                     <asp:BoundField DataField="CPhoneMother" HeaderText="CPhoneMother" />
                     <asp:BoundField DataField="WkshopName" HeaderText="WkshopName" />
                     <asp:BoundField DataField="RegID" HeaderText="RegID" />
                     <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                     <asp:BoundField DataField="MemberID" HeaderText="MemberID" />
                 </Columns>

             </asp:GridView></div></td>

     </tr>


                  
                  
                </table>
               </td>

       </tr>

  
        
                </table>
       </asp:Content>
    