﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachClassCalendar.aspx.vb" Inherits="CoachClassCalendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script language="javascript" src="js/Calendar.js" type="text/javascript"></script>
    <link href="css/Calendar.css" rel="stylesheet" type="text/css" />

    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <h2>Coach Class Calendar</h2>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">

                <table width="100%" style="margin-left: auto; margin-right: auto; font-weight: bold; background-color: #ffffcc;">
                    <tr class="ContentSubTitle">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>CoachName</td>
                        <td>
                            <asp:DropDownList ID="ddlCoach" AutoPostBack="true" runat="server" DataTextField="CoachName" DataValueField="MemberId" Width="105px">
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td>
                            <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductGroupId" Width="125px">
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductId" Width="110px">
                            </asp:DropDownList>
                        </td>
                        <td>Phase</td>
                        <td>
                            <asp:DropDownList ID="ddlPhase" runat="server" Width="105px">
                            </asp:DropDownList>
                        </td>


                    </tr>

                    <tr>
                        <td colspan="12">
                            <br />
                            <center>
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Style="text-align: center; height: 26px;" /></center>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <center>
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </center>
                <div runat="server" id="divTable1" visible="false">
                    <asp:HiddenField ID="hdMemberId" runat="server"></asp:HiddenField>
                    <asp:HiddenField ID="hdSelectedRowInx" runat="server" />
                    <asp:HiddenField ID="hdTable1Year" runat="server"></asp:HiddenField>

                    <asp:Repeater ID="rptCoachClass" runat="server">
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:HiddenField ID="rep_hdWeekCnt" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdProductGrp" runat="server" Value='<%# Eval("ProductGroupId")%>'></asp:HiddenField>
                            <asp:HiddenField ID="hdProduct" runat="server" Value='<%# Eval("ProductId") %>'></asp:HiddenField>
                            <asp:HiddenField ID="hdHideModify" runat="server" Value="false" />
                            <asp:HiddenField ID="hdStartDate" runat="server" />
                            <asp:HiddenField ID="hdEndDate" runat="server" />
                            <asp:HiddenField ID="hdTotalClass" runat="server" />
                            <asp:HiddenField ID="hdHWRelDates" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdHWDueDate" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdARelDate" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdSRelDate" runat="server"></asp:HiddenField>
                            <table width="100%">
                                <tr>
                                    <td style="text-align: right; width: 50px"><b>Table
                                        <asp:Label ID="lblTableCnt" runat="server"></asp:Label>
                                        :</b></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td><b>Product Group: </b>
                                        <asp:Label ID="lblPrdGrp" runat="server" Text='<%# Eval("ProductGroupName")%>'></asp:Label>
                                    </td>
                                    <td><b>Product : </b>
                                        <asp:Label ID="lblProduct" runat="server" Text='<%# Eval("ProductName")%>'></asp:Label></td>
                                    <td><b>Phase : </b>
                                        <asp:Label ID="lblPhase" runat="server" Text='<%# Eval("Phase")%>'></asp:Label></td>
                                    <td><b>Level : </b>
                                        <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("Level")%>'></asp:Label></td>
                                    <td><b>Session :</b><asp:Label ID="lblSessionNo" runat="server" Text='<%# Eval("SessionNo")%>'></asp:Label></td>
                                    <td><b>UserID :</b><asp:Label ID="Label1" runat="server" Text='<%# Eval("UserID")%>'></asp:Label></td>
                                    <td><b>Pwd :</b><asp:Label ID="Label2" runat="server" Text='<%# Eval("PWD")%>'></asp:Label></td>
                                </tr>
                            </table>
                            <asp:GridView ID="rpt_grdCoachClassCal" runat="server" DataKeyNames="SignUpID" AutoGenerateColumns="False" EnableViewState="true" HeaderStyle-BackColor="#ffffcc"
                                OnRowDataBound="rpt_grdCoachClassCal_RowDataBound" OnRowCommand="rpt_grdCoachClassCal_RowCommand" HeaderStyle-Height="25px" AlternatingRowStyle-BackColor="#F6F6F6">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="70px" ItemStyle-Height="25px">
                                        <ItemTemplate>
                                            <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" Enabled="false" />
                                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="UpdateCoach" Visible="false" />
                                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="CancelCoach" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CurDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                                            <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblCoachClassCalID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "CoachClassCalID")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSignUpId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SignUpID")%>' Visible="false"></asp:Label>

                                            <asp:Label ID="lblRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qreleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDueDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "qdeadlinedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblARelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "areleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblSRelDt" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sreleasedate", "{0:MM/dd/yyyy}")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Day">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDaySub" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day").ToString().Substring(0, 3)%>'></asp:Label>
                                            <asp:Label ID="lblDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Day")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Time")%>' DataFormatString="{0:hh:mm}"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Duration")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ser#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSerial" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SerNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week#">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeekNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "SerNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdStatus" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Status")%>' />
                                            <asp:DropDownList ID="ddlStatus" runat="server" Enabled="false" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem>Select</asp:ListItem>
                                                <asp:ListItem>Not set</asp:ListItem>
                                                <asp:ListItem>On</asp:ListItem>
                                                <asp:ListItem>Cancelled</asp:ListItem>
                                                <asp:ListItem>Substitute</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Substitute">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdSubstitute" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Substitute")%>' />
                                            <asp:DropDownList ID="ddlSubstitute" runat="server" Enabled="false" DataTextField="CoachName" DataValueField="MemberId" Width="120px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdReason" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Reason")%>' />
                                            <asp:DropDownList ID="ddlReason" runat="server" Enabled="false">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                <asp:ListItem Value="Illness">Illness</asp:ListItem>
                                                <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                                                <asp:ListItem Value="Business Trip">Business Trip</asp:ListItem>
                                                <asp:ListItem Value="Work Related">Work Related</asp:ListItem>
                                                <asp:ListItem Value="Holiday">Holiday</asp:ListItem>
                                                <asp:ListItem Value="Other">Other</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schedule Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheduleType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ScheduleType")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Homework">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHWRelDate" runat="server" EnableViewState="true" Text="__/__/____" dataformatstring="{0:MM/dd/yyyy}"></asp:Label>
                                            <asp:ImageButton ID="imgHWRelDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="HW Due Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHWDueDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                            <asp:ImageButton ID="imgHWDueDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:Label ID="lblARelDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                            <asp:ImageButton ID="imgARelDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SRelease">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSRelDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                            <asp:ImageButton ID="imgSRelDate" runat="server" ImageUrl="~/Images/Calendar.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="UserID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "UserID")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pwd" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "PWD")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Week#" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblWeek" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "WeekNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>
                            </asp:GridView>
                            <br />
                            <asp:LinkButton ID="rep_lnkBtnShowNextWeek" runat="server" OnClick="rep_lnkBtnShowNextWeek_Click">Show Next Week</asp:LinkButton>
                            | <b>Go to : </b>
                            <asp:DropDownList ID="ddlGoTo" runat="server" OnSelectedIndexChanged="ddlGoTo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

