﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Collections.Generic


Partial Class CoachClassCalendar
    Inherits System.Web.UI.Page
    Dim cmdText As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 89
        'Session("LoginID") = 51336
        'Session("LoggedIn") = "true"
        Try
            If LCase(Session("LoggedIn")) <> "true" Or Session("LoginID").ToString = String.Empty Then
                Response.Redirect("~/maintest.aspx")
            End If

            If Page.IsPostBack = False Then
                Dim year As Integer = 0
                year = Convert.ToInt32(DateTime.Now.Year)
                Dim i, j As Integer
                j = 0
                For i = 0 To 4
                    ddlYear.Items.Insert(i, Convert.ToString(year - j))
                    j = j + 1
                Next
                If Now.Month <= 3 Then
                    ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(year - 1)))
                Else
                    ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(year)))
                End If
                'ddlPhase.Items.Add(New ListItem("Select Phase", -1))
                For i = 1 To 4
                    ddlPhase.Items.Add(i)
                Next
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Parent Functions"
                    hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
                ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Response.Redirect("~/login.aspx?entry=v")
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "88") Then
                    FillEvent()
                    If Session("RoleId").ToString() <> "88" Then
                        FillCoach(ddlCoach)
                    Else
                        FillProductGroup()
                    End If

                Else
                    Response.Redirect("~/maintest.aspx")
                End If
            Else
                FillDate()
            End If
        Catch ex As Exception
            Response.Redirect("~/maintest.aspx")
        End Try
    End Sub

    ' Add events in dropdown for adding and updating
    Private Sub FillEvent()
        ddlEvent.Items.Add(New ListItem("Coaching", "13"))
    End Sub

    ' Get product group details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProductGroup()

        Dim dsProductGrp As DataSet
        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then
            cmdText = " select distinct p.ProductGroupID,p.ProductGroupCode,p.Name from calsignup c inner join ProductGroup p on c.ProductGroupID=p.ProductGroupID where c.eventid =13 and c.EventYear=" & ddlYear.SelectedItem.Value & " and c.MemberID='" & ddlCoach.SelectedValue & "' and c.ProductId is not Null Order by ProductGroupId"
        Else 'If Session("RoleId") = 89 Then
            cmdText = " select distinct p.ProductGroupID,p.ProductGroupCode,p.Name from calsignup c inner join ProductGroup p on c.ProductGroupID=p.ProductGroupID where c.eventid =13 and c.EventYear=" & ddlYear.SelectedItem.Value & " and c.Memberid=" & Session("LoginID") & "  Order by ProductGroupId"

        End If
        dsProductGrp = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ViewState("PrdGroup") = dsProductGrp.Tables(0)

        ddlProductGroup.DataSource = dsProductGrp.Tables(0)
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Enabled = True
            ddlProductGroup.Items.Insert(0, "Select Product Group")
        Else
            ddlProductGroup.SelectedIndex = 0
            ddlProductGroup.Enabled = False
        End If
        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
    End Sub
    ' Get product details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProduct()
        If ddlProductGroup.SelectedValue <> "Select Product Group" Then
            If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or (Session("RoleId").ToString() = "89") Then
                cmdText = " select distinct p.ProductID, p.ProductCode, p.Name from calsignup c inner join Product p on c.ProductID=p.ProductID where c.eventid =13 and c.eventyear=" & ddlYear.SelectedItem.Value & " and c.ProductGroupId =" & ddlProductGroup.SelectedItem.Value & " and C.MemberID='" & ddlCoach.SelectedValue & "' and c.ProductId is not Null Order by ProductId"
            Else
                cmdText = " select distinct p.ProductID, p.ProductCode, p.Name from calsignup c inner join Product p on c.ProductID=p.ProductID where c.eventid =13 and c.eventyear=" & ddlYear.SelectedItem.Value & " and c.Memberid=" & Session("LoginID") & " and c.ProductGroupId=" & ddlProductGroup.SelectedValue & " and c.ProductId is not Null  Order by ProductId"
            End If
            Dim dsProduct As DataSet
            dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ddlProduct.DataSource = dsProduct.Tables(0)
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 1 Then
                ddlProduct.Enabled = True
                ddlProduct.Items.Insert(0, "Select Product")
            Else
                ddlProduct.SelectedIndex = 0
                ddlProduct.Enabled = False
            End If
        End If
    End Sub
    Private Sub FillCoachByCondition(PrdGroupId As Integer, PrdId As Integer, ctrl As DropDownList)
        Dim dsCoach As DataSet
        cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C "
        cmdText = cmdText & " inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & PrdGroupId & " and C.ProductId=" & PrdId & " and C.EventYear=" & ddlYear.SelectedValue & "  and I.AutomemberId<>" & ddlCoach.SelectedItem.Value & " order by lastname,firstname"
        'End If
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)

        If dsCoach.Tables(0).Rows.Count > 0 Then
            ctrl.DataSource = dsCoach.Tables(0)
            ctrl.DataBind()
            ctrl.Items.Insert(0, New ListItem("[Coach Name]", "Null"))
            ctrl.SelectedIndex = 0
        End If
    End Sub
    Private Sub FillCoach(ddl As DropDownList)
        ddl.Items.Clear()
        Dim dsCoach As DataSet
        If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or Session("RoleId") = 89 Then
            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.Accepted='Y' and C.EventYear=" & ddlYear.SelectedValue & " order by lastname,firstname"
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If dsCoach.Tables(0).Rows.Count > 0 Then
                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                If ddl.Items.Count > 0 Then
                    ddl.Items(0).Selected = True
                    If ddl.Items.Count = 1 Then
                        ddl.Enabled = False
                    Else
                        ddl.Items.Insert(0, New ListItem("[Coach Name]", "Null"))
                        ddl.SelectedIndex = 0
                        ddl.Enabled = True
                    End If
                End If
            End If
        Else
            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" & Session("LoginID")
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If dsCoach.Tables(0).Rows.Count > 0 Then
                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                ddl.Enabled = False
            End If
        End If
    End Sub
    Private Sub BindCoachClassDetailsInRepeater()
        cmdText = "select count(*) from calsignup  where eventyear=" & hdTable1Year.Value & " and eventid=13 and Accepted='y' and MemberId= " & hdMemberId.Value & " and ProductGroupId= " & ddlProductGroup.SelectedItem.Value & " and  Productid=" & ddlProduct.SelectedItem.Value & " and Phase=" & ddlPhase.SelectedItem.Value
        If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
            Dim ds As DataSet
            cmdText = "select ProductGroupId,ProductGroupCode,UserID,PWD,(SELECT Name FROM PRODUCTGROUP pg WHERE pg.ProductGroupId=c.ProductGroupId) ProductGroupName,Productid, (SELECT Name FROM PRODUCT p WHERE p.ProductId=c.ProductId) ProductName ,ProductCode, phase,level,sessionno from calsignup c where eventyear=" & hdTable1Year.Value & " and eventid=13 and Accepted='y' and MemberId= " & hdMemberId.Value & " group by ProductGroupId,ProductGroupCode,UserID,PWD,Productid,ProductCode, phase,level,sessionno "
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                rptCoachClass.DataSource = ds.Tables(0)
                rptCoachClass.DataBind()
            End If
        Else
            rptCoachClass.DataSource = Nothing
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "No record exists"
        End If
    End Sub
    'Fill product for each selected product group
    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Try
            ddlProduct.Items.Clear()
            FillProduct()
            If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or Session("RoleId") = 89 Then
            Else
                FillCoach(ddlCoach)
            End If

        Catch ex As Exception
        End Try
    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged
        Try
            ddlCoach.Items.Clear()
            If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or Session("RoleId") = 89 Then
            Else
                FillCoach(ddlCoach)
            End If
            'FillCoach(ddlCoach)
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            ViewState("Date") = Nothing
            Reset()
            rptCoachClass.DataSource = Nothing
            If ddlYear.SelectedValue = String.Empty Then
                ShowMessage("Select Year", True)
            ElseIf ddlProductGroup.Items.Count = 0 Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlProduct.Items.Count = 0 Then
                ShowMessage("Select Product", True)
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                ShowMessage("Select Product", True)
            ElseIf ddlPhase.SelectedItem.Text = "Select Phase" Then
                ShowMessage("Select Phase", True)
            ElseIf ddlCoach.Items.Count = 0 Then
                ShowMessage("Select Coach", True)
            ElseIf ddlCoach.SelectedItem.Text = "[Coach Name]" Then
                ShowMessage("Select Coach", True)
            Else
                rptCoachClass.DataSource = Nothing
                rptCoachClass.DataBind()
                hdMemberId.Value = ddlCoach.SelectedItem.Value
                hdTable1Year.Value = ddlYear.SelectedItem.Value
                BindCoachClassDetailsInRepeater()
                divTable1.Visible = True
                Dim indx As Integer, iVisible As Integer = 0
                For indx = 0 To rptCoachClass.Items.Count - 1
                    If rptCoachClass.Items(indx).Visible = True Then
                        iVisible = 1
                        Exit For
                    End If
                Next
                If iVisible = 0 Then
                    lblMsg.ForeColor = Color.Red
                    lblMsg.Text = "No record exists"
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Private Sub Reset()
        ' divUpdate.Visible = False
        lblMsg.Text = ""
    End Sub
    'Error or Success message function
    Private Sub ShowMessage(msg As String, bIsError As Boolean)
        lblMsg.Text = msg
        lblMsg.ForeColor = Color.Blue
        If bIsError = True Then
            lblMsg.ForeColor = Color.Red
        End If
    End Sub
    Private Function GetDates(ByVal StartDate As DateTime, ByVal EndDate As DateTime, ByVal Day As String) As List(Of DateTime)
        Dim dates As New List(Of DateTime)
        Dim SelectedDays As New List(Of DayOfWeek)
        Select Case Day.ToLower
            Case "su"
                SelectedDays.Add(DayOfWeek.Sunday)
            Case "mo"
                SelectedDays.Add(DayOfWeek.Monday)
            Case "tu"
                SelectedDays.Add(DayOfWeek.Tuesday)
            Case "we"
                SelectedDays.Add(DayOfWeek.Wednesday)
            Case "th"
                SelectedDays.Add(DayOfWeek.Thursday)
            Case "fr"
                SelectedDays.Add(DayOfWeek.Friday)
            Case "sa"
                SelectedDays.Add(DayOfWeek.Saturday)
            Case Else
                ' ...unexpected day of week...
                ' do something here?
        End Select
        Dim dt As DateTime = StartDate.Date
        While dt <= EndDate
            If SelectedDays.Contains(dt.DayOfWeek) Then
                dates.Add(dt)
                Exit While
            End If
            dt = dt.AddDays(1)
        End While
        Return dates
    End Function
    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs)
        lblMsg.Text = ""
        'Casting sender to Dropdown
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)
        'Row of the Grid from where the SelectedIndex change event is fired.
        Dim row As GridViewRow = TryCast(ddl.NamingContainer, GridViewRow)
        ' Get object of Repeater control 
        Dim rptItem As RepeaterItem = TryCast(ddl.NamingContainer.NamingContainer.NamingContainer, RepeaterItem)
        Dim hdProductGrp As HiddenField = TryCast(rptItem.FindControl("hdProductGrp"), HiddenField)
        Dim hdProduct As HiddenField = TryCast(rptItem.FindControl("hdProduct"), HiddenField)
        Dim lblPhase As Label = TryCast(rptItem.FindControl("lblPhase"), Label)
        Dim lblSessionNo As Label = TryCast(rptItem.FindControl("lblSessionNo"), Label)
        Dim lblDay As Label = TryCast(row.FindControl("lblDay"), Label)
        Dim ddlSubstitute As DropDownList = TryCast(row.FindControl("ddlSubstitute"), DropDownList)
        Dim ddlReason As DropDownList = TryCast(row.FindControl("ddlReason"), DropDownList)
        Dim ddlStatus As DropDownList = TryCast(row.FindControl("ddlStatus"), DropDownList)
        'If Len(Trim(lblCoachClassCalID.Text)) = 0 Then
        Dim hdStatus As HiddenField = TryCast(row.FindControl("hdStatus"), HiddenField)

        Dim lblHWDate As Label = TryCast(row.FindControl("lblHWDueDate"), Label)
        Dim lblRelDate As Label = TryCast(row.FindControl("lblARelDate"), Label)
        Dim lblSRelDate As Label = TryCast(row.FindControl("lblSRelDate"), Label)
        Dim lblHomeDate As Label = TryCast(row.FindControl("lblHWRelDate"), Label)

        Dim imgHWDate As ImageButton = TryCast(row.FindControl("imgHWRelDate"), ImageButton)
        Dim imgRelDate As ImageButton = TryCast(row.FindControl("imgHWDueDate"), ImageButton)
        Dim imgSRelDate As ImageButton = TryCast(row.FindControl("imgARelDate"), ImageButton)
        Dim imgHomeDate As ImageButton = TryCast(row.FindControl("imgSRelDate"), ImageButton)

        cmdText = "select isnull( max(weekno),0) from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Phase=" + lblPhase.Text + " and SessionNo=" + lblSessionNo.Text + " and Day='" + lblDay.Text + "' and [Status]  in ('On','Substitute')"
        Dim cntWeek As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        Dim lblWeek As Label = CType(row.FindControl("lblWeek"), Label)
        If hdStatus.Value <> "On" And hdStatus.Value <> "Substitute" Then
            lblWeek.Text = cntWeek
            If ddl.SelectedItem.Text.ToLower = "on" Or ddl.SelectedItem.Text.ToLower = "substitute" Then
                lblWeek.Text = cntWeek + 1
            End If
            '' validate
            cmdText = "select classes from EventFees Where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId=" & hdProduct.Value
            Dim iCntClasses As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If iCntClasses < CInt(lblWeek.Text) Then
                lblMsg.ForeColor = Color.Red
                lblMsg.Text = "Week # cannot exceed the limitation"
            End If

        End If

        If ddlStatus.SelectedItem.Text = "Cancelled" Or ddlStatus.SelectedItem.Text = "Not set" Then
            lblHWDate.Visible = False
            lblRelDate.Visible = False
            lblSRelDate.Visible = False
            lblHomeDate.Visible = False
            imgHWDate.Visible = False
            imgRelDate.Visible = False
            imgSRelDate.Visible = False
            imgHomeDate.Visible = False


        Else
            lblHWDate.Visible = True
            lblRelDate.Visible = True
            lblSRelDate.Visible = True
            lblHomeDate.Visible = True

            imgHWDate.Visible = True
            imgRelDate.Visible = True
            imgSRelDate.Visible = True
            imgHomeDate.Visible = True
        End If
        If ddlStatus.SelectedItem.Text = "On" Or ddlStatus.SelectedItem.Text = "Not set" Then
            ddlReason.SelectedValue = ""
        End If

        If ddl.SelectedItem.Value = "Substitute" Then : ddlSubstitute.Enabled = True : Else : ddlSubstitute.Enabled = False : End If
        If ddl.SelectedItem.Value = "Substitute" Or ddl.SelectedItem.Value = "Cancelled" Then : ddlReason.Enabled = True : Else : ddlReason.Enabled = False : End If
    End Sub
    Protected Sub ddlGoTo_SelectedIndexChanged(sender As Object, e As EventArgs)
        'Row of the Grid from where the SelectedIndex change event is fired.
        Dim rptItem As RepeaterItem = TryCast(sender.NamingContainer, RepeaterItem)
        Dim ddlGoTo As DropDownList = TryCast(sender, DropDownList)
        Dim grd As GridView = TryCast(rptItem.FindControl("rpt_grdCoachClassCal"), GridView)
        Dim rep_hdWeekCnt As HiddenField = TryCast(rptItem.FindControl("rep_hdWeekCnt"), HiddenField)
        Dim rep_lnkBtnShowNextWeek As LinkButton = TryCast(rptItem.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
        If ddlGoTo.SelectedItem.Value <> -1 Then
            rep_hdWeekCnt.Value = ddlGoTo.SelectedItem.Value
            BindCoachClassDetailsInGrid(grd, 0)
            If rep_hdWeekCnt.Value = (ddlGoTo.Items.Count - 1) Then : rep_lnkBtnShowNextWeek.Enabled = False
            Else : rep_lnkBtnShowNextWeek.Enabled = True
            End If
        End If
    End Sub
    Private Sub FillDate()
        Try
            If Not ViewState("Date") Is Nothing Then
                Dim list As New Dictionary(Of String, String)
                list = ViewState("Date")
                For Each pair As KeyValuePair(Of String, String) In list
                    ' Get key.
                    Dim key As String = pair.Key
                    ' Get value.
                    Dim value As String = pair.Value
                    ' Display.
                    TryCast(Page.FindControl(key), Label).Text = TryCast(Page.FindControl(value), HiddenField).Value
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub rpt_grdCoachClassCal_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try


            If e.CommandName <> "Modify" And e.CommandName <> "UpdateCoach" And e.CommandName <> "CancelCoach" Then Exit Sub
            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            Dim ddlStatus As DropDownList = DirectCast(gvRow.Cells(7).FindControl("ddlStatus"), DropDownList)
            Dim ddlSubstitute As DropDownList = DirectCast(gvRow.Cells(8).FindControl("ddlSubstitute"), DropDownList)
            Dim ddlReason As DropDownList = DirectCast(gvRow.Cells(9).FindControl("ddlReason"), DropDownList)

            Dim btnModify As Button = DirectCast(gvRow.Cells(0).FindControl("btnModify"), Button)
            Dim btnUpdate As Button = DirectCast(gvRow.Cells(0).FindControl("btnUpdate"), Button)
            Dim btnCancel As Button = DirectCast(gvRow.Cells(0).FindControl("btnCancel"), Button)

            Dim lblSignUpId As Label = DirectCast(gvRow.Cells(1).FindControl("lblSignUpId"), Label)
            Dim lblCoachClassCalID As Label = DirectCast(gvRow.Cells(1).FindControl("lblCoachClassCalID"), Label)
            Dim lblDate As Label = DirectCast(gvRow.Cells(1).FindControl("lblDate"), Label)

            Dim SerNo As Integer = DirectCast(gvRow.Cells(5).FindControl("lblSerial"), Label).Text
            Dim WeekSerNo As Integer = DirectCast(gvRow.Cells(6).FindControl("lblWeekNo"), Label).Text
            Dim WeekNo As Integer = DirectCast(gvRow.Cells(15).FindControl("lblWeek"), Label).Text

            Dim lblHWRelDate As Label = DirectCast(gvRow.Cells(10).FindControl("lblHWRelDate"), Label)
            Dim lblHWDueDate As Label = DirectCast(gvRow.Cells(11).FindControl("lblHWDueDate"), Label)
            Dim lblARelDate As Label = DirectCast(gvRow.Cells(12).FindControl("lblARelDate"), Label)
            Dim lblSRelDate As Label = DirectCast(gvRow.Cells(12).FindControl("lblSRelDate"), Label)

            Dim hdHWRelDates As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdHWRelDates"), HiddenField)
            Dim hdHWDueDate As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdHWDueDate"), HiddenField)
            Dim hdARelDate As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdARelDate"), HiddenField)
            Dim hdSRelDate As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdSRelDate"), HiddenField)
            Dim hdHideModify As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdHideModify"), HiddenField)
            Dim imgHWRelDate As ImageButton = DirectCast(gvRow.Cells(10).FindControl("imgHWRelDate"), ImageButton)
            Dim imgHWDueDate As ImageButton = DirectCast(gvRow.Cells(11).FindControl("imgHWDueDate"), ImageButton)
            Dim imgARelDate As ImageButton = DirectCast(gvRow.Cells(12).FindControl("imgARelDate"), ImageButton)
            Dim imgSRelDate As ImageButton = DirectCast(gvRow.Cells(12).FindControl("imgSRelDate"), ImageButton)

            If e.CommandName = "Modify" Then
                Dim list As New Dictionary(Of String, String)
                If Not ViewState("Date") Is Nothing Then : list = ViewState("Date") : End If
                list.Add(lblHWRelDate.UniqueID, hdHWRelDates.UniqueID)
                list.Add(lblHWDueDate.UniqueID, hdHWDueDate.UniqueID)
                list.Add(lblARelDate.UniqueID, hdARelDate.UniqueID)
                list.Add(lblSRelDate.UniqueID, hdSRelDate.UniqueID)
                hdHWRelDates.Value = lblHWRelDate.Text
                hdHWDueDate.Value = lblHWDueDate.Text
                hdARelDate.Value = lblARelDate.Text
                hdSRelDate.Value = lblSRelDate.Text

                ViewState("Date") = list
                ddlStatus.Enabled = True
                btnModify.Visible = False
                btnUpdate.Visible = True
                btnCancel.Visible = True
                imgHWRelDate.Visible = True
                imgHWDueDate.Visible = True
                imgARelDate.Visible = True
                imgSRelDate.Visible = True

                imgHWRelDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblHWRelDate.ClientID & "','" & hdHWRelDates.ClientID & "'); return false;")
                imgHWDueDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblHWDueDate.ClientID & "','" & hdHWDueDate.ClientID & "'); return false;")
                imgARelDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblARelDate.ClientID & "','" & hdARelDate.ClientID & "'); return false;")
                imgSRelDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblSRelDate.ClientID & "','" & hdSRelDate.ClientID & "'); return false;")
                If ddlStatus.SelectedItem.Text = "Cancelled" Then
                    ddlReason.Enabled = True

                    lblHWRelDate.Visible = False
                    lblHWDueDate.Visible = False
                    lblARelDate.Visible = False
                    lblSRelDate.Visible = False

                    imgHWRelDate.Visible = False
                    imgHWDueDate.Visible = False
                    imgARelDate.Visible = False
                    imgSRelDate.Visible = False
                Else

                    lblHWRelDate.Visible = True
                    lblHWDueDate.Visible = True
                    lblARelDate.Visible = True
                    lblSRelDate.Visible = True

                    imgHWRelDate.Visible = True
                    imgHWDueDate.Visible = True
                    imgARelDate.Visible = True
                    imgSRelDate.Visible = True

                End If
            ElseIf e.CommandName = "UpdateCoach" Then
                lblMsg.ForeColor = Color.Red
                lblMsg.Text = ""
                If ddlStatus.SelectedIndex = 0 Then
                    lblMsg.Text = "Select Status "
                ElseIf ddlStatus.SelectedItem.Text.ToLower = "cancelled" Or ddlStatus.SelectedItem.Text.ToLower = "substitute" Then
                    ' Reason is mandatory if status is cancelled or Substitute
                    If ddlReason.SelectedIndex = 0 Then
                        ' show error msg
                        lblMsg.Text = "Reason is mandatory if status is Cancelled or Substitute"
                    ElseIf ddlSubstitute.SelectedIndex = 0 And ddlStatus.SelectedItem.Text.ToLower = "substitute" Then
                        lblMsg.Text = "Select coach name from substitute "
                    End If
                ElseIf ddlStatus.SelectedItem.Text.ToLower = "on" Or ddlStatus.SelectedItem.Text.ToLower = "substitute" Then
                    If lblHWRelDate.Text = "__/__/____" Then
                        ' show error msg
                        lblMsg.Text = "Home work date is mandatory if status is on or Substitute"
                    ElseIf lblHWDueDate.Text = "__/__/____" Then
                        ' show error msg
                        lblMsg.Text = "Home work due date is mandatory if status is on or Substitute"
                    ElseIf lblARelDate.Text = "__/__/____" Then
                        ' show error msg
                        lblMsg.Text = "Answer release date is mandatory if status is on or Substitute"
                    ElseIf lblSRelDate.Text = "__/__/____" Then
                        ' show error msg
                        lblMsg.Text = "SRelease date is mandatory if status is on or Substitute"
                    Else

                        'server
                        Dim dtHWRel As DateTime = DateTime.ParseExact(lblHWRelDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                        Dim dtHWDue As Date = Date.ParseExact(lblHWDueDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                        Dim dtARel As Date = Date.ParseExact(lblARelDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                        Dim dtSRel As Date = Date.ParseExact(lblSRelDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)



                        If dtHWRel >= dtHWDue Then
                            ' show error msg
                            lblMsg.Text = "Due date should be greater than the Homework date"
                        ElseIf dtARel <= dtHWDue Then
                            ' show error msg
                            lblMsg.Text = " Answers date should be greater than both Due Date and Homework Date"
                        ElseIf dtSRel <= dtHWDue Then
                            ' show error msg
                            lblMsg.Text = " SRelease date should be greater than both Due Date and Homework Date"
                        End If
                    End If
                End If

                '    ' validate hw release date
                '    'Home work release date, HW Due date and Answer Release date are mandatory when Status is On or Substitute
                '    'Defaults: Homework is Date of the Class, Due date is 5 days after the Homework release date and Answers is 7th day after the HW release date. 

                Dim hdProductGrp As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdProductGrp"), HiddenField)
                Dim hdProduct As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdProduct"), HiddenField)
                Dim lblPhase As Label = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("lblPhase"), Label)
                Dim lblLevel As Label = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("lblLevel"), Label)
                Dim lblSessionNo As Label = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("lblSessionNo"), Label)

                If Len(Trim(lblMsg.Text)) = 0 Then
                    lblMsg.ForeColor = Color.DarkBlue
                    If Len(lblCoachClassCalID.Text.Trim) = 0 Or lblCoachClassCalID.Text = 0 Then
                        'insert
                        If ddlStatus.SelectedItem.Text <> "Cancelled" Or ddlStatus.SelectedItem.Text <> "Not set" Then


                            cmdText = "select CoachPaperId from CoachPapers where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Level='" & lblLevel.Text & "' and WeekId=" & WeekSerNo & " and DocType='Q'"
                            Dim CoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            If CoachPaperId > 0 Then


                                cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Phase,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Phase,[Level],SessionNo, '" & lblDate.Text & "', Day, Time, Duration," & SerNo & ", " & WeekSerNo & ", '" & ddlStatus.SelectedItem.Text & "'," & ddlSubstitute.SelectedItem.Value & ", '" & ddlReason.SelectedItem.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                lblMsg.Text = "Inserted Successfully"
                            Else
                                lblMsg.Text = "Coach paper is missing for the week " & WeekSerNo & " to the selected Product Group and Product."
                            End If
                        Else
                            cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Phase,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                            cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Phase,[Level],SessionNo, '" & lblDate.Text & "', Day, Time, Duration," & SerNo & ", " & WeekSerNo & ", '" & ddlStatus.SelectedItem.Text & "'," & ddlSubstitute.SelectedItem.Value & ", '" & ddlReason.SelectedItem.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            lblMsg.Text = "Inserted Successfully"
                        End If
                    Else
                        'update
                        If ddlStatus.SelectedItem.Text <> "Cancelled" Or ddlStatus.SelectedItem.Text <> "Not set" Then


                            cmdText = "select CoachPaperId from CoachPapers where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Level='" & lblLevel.Text & "' and WeekId=" & WeekSerNo & " and DocType='Q'"
                            Dim CoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            If CoachPaperId > 0 Then


                                cmdText = "UPDATE CoachClassCal SET SerNo=" & SerNo & ", WeekNo = " & WeekSerNo & " ,Status='" & ddlStatus.SelectedItem.Text & "',Substitute=" & ddlSubstitute.SelectedItem.Value & ", Reason='" & ddlReason.SelectedItem.Value & "' ,ModifyDate=GETDATE(), ModifiedBy=" & Session("LoginID")
                                cmdText = cmdText + " where CoachClassCalId=" + lblCoachClassCalID.Text
                                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                lblMsg.Text = "Updated Successfully"
                            Else
                                lblMsg.Text = "Coach paper is missing for the week " & WeekSerNo & " to the selected Product Group and Product."
                            End If
                        Else
                            cmdText = "UPDATE CoachClassCal SET SerNo=" & SerNo & ", WeekNo = " & WeekSerNo & " ,Status='" & ddlStatus.SelectedItem.Text & "',Substitute=" & ddlSubstitute.SelectedItem.Value & ", Reason='" & ddlReason.SelectedItem.Value & "' ,ModifyDate=GETDATE(), ModifiedBy=" & Session("LoginID")
                            cmdText = cmdText + " where CoachClassCalId=" + lblCoachClassCalID.Text
                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            lblMsg.Text = "Updated Successfully"
                        End If
                    End If


                    ' update in coachreldates
                    cmdText = "select CoachPaperId from CoachPapers where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Level='" & lblLevel.Text & "' and WeekId=" & WeekSerNo & " and DocType='Q'"
                    Dim iCoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If ddlStatus.SelectedItem.Text <> "Cancelled" Or ddlStatus.SelectedItem.Text <> "Not set" Then


                        If iCoachPaperId > 0 Then
                            If ddlStatus.SelectedItem.Text = "Cancelled" Or ddlStatus.SelectedItem.Text = "Not set" Then
                                cmdText = "update CoachRelDates Set qreleasedate=null, qdeadlinedate=null, areleasedate=null,sreleasedate=null WHERE CoachPaperId=" & iCoachPaperId & " and MemberId= " & hdMemberId.Value & " and  EventYear=" & hdTable1Year.Value & " and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Phase =" & lblPhase.Text & " and Level='" & lblLevel.Text & "' and Session =" & lblSessionNo.Text


                            Else
                                cmdText = "update CoachRelDates Set qreleasedate='" & lblHWRelDate.Text & "', qdeadlinedate='" & lblHWDueDate.Text & "', areleasedate='" & lblARelDate.Text & "',sreleasedate='" & lblSRelDate.Text & "' WHERE CoachPaperId=" & iCoachPaperId & " and MemberId= " & hdMemberId.Value & " and  EventYear=" & hdTable1Year.Value & " and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Phase =" & lblPhase.Text & " and Level='" & lblLevel.Text & "' and Session =" & lblSessionNo.Text
                            End If

                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                        End If
                    End If
                    hdHideModify.Value = "false"
                    ddlStatus.Enabled = False
                    ddlReason.Enabled = False
                    ddlSubstitute.Enabled = False
                    btnModify.Visible = True
                    btnUpdate.Visible = False
                    btnCancel.Visible = False
                    imgHWRelDate.Visible = False
                    imgHWDueDate.Visible = False
                    imgARelDate.Visible = False
                    imgSRelDate.Visible = False
                    If Not ViewState("Date") Is Nothing Then
                        Dim list As New Dictionary(Of String, String)
                        list = ViewState("Date")
                        If list.ContainsKey(lblHWRelDate.UniqueID) Then
                            list.Remove(lblHWRelDate.UniqueID)
                        End If
                        If list.ContainsKey(lblHWDueDate.UniqueID) Then
                            list.Remove(lblHWDueDate.UniqueID)
                        End If
                        If list.ContainsKey(lblARelDate.UniqueID) Then
                            list.Remove(lblARelDate.UniqueID)
                        End If
                        If list.ContainsKey(lblSRelDate.UniqueID) Then
                            list.Remove(lblSRelDate.UniqueID)
                        End If
                    End If
                    BindCoachClassDetailsInGrid(sender, 0)
                End If
            ElseIf e.CommandName = "CancelCoach" Then
                Reset()
                hdHideModify.Value = "false"
                ddlStatus.Enabled = False
                ddlReason.Enabled = False
                ddlSubstitute.Enabled = False
                btnModify.Visible = True
                btnUpdate.Visible = False
                btnCancel.Visible = False
                imgHWRelDate.Visible = False
                imgHWDueDate.Visible = False
                imgARelDate.Visible = False
                imgSRelDate.Visible = False
                If Not ViewState("Date") Is Nothing Then
                    Dim list As New Dictionary(Of String, String)
                    list = ViewState("Date")
                    If list.ContainsKey(lblHWRelDate.UniqueID) Then
                        list.Remove(lblHWRelDate.UniqueID)
                    End If
                    If list.ContainsKey(lblHWDueDate.UniqueID) Then
                        list.Remove(lblHWDueDate.UniqueID)
                    End If
                    If list.ContainsKey(lblARelDate.UniqueID) Then
                        list.Remove(lblARelDate.UniqueID)
                    End If
                    If list.ContainsKey(lblSRelDate.UniqueID) Then
                        list.Remove(lblSRelDate.UniqueID)
                    End If
                End If
                BindCoachClassDetailsInGrid(sender, 0)
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub



    Protected Sub rep_lnkBtnShowNextWeek_Click(sender As Object, e As EventArgs)
        Reset()
        'Row of the Grid from where the SelectedIndex change event is fired.
        Dim rptItem As RepeaterItem = TryCast(sender.NamingContainer, RepeaterItem)
        Dim grd As GridView = TryCast(rptItem.FindControl("rpt_grdCoachClassCal"), GridView)
        Dim rep_hdWeekCnt As HiddenField = TryCast(rptItem.FindControl("rep_hdWeekCnt"), HiddenField)
        Dim hdHideModify As HiddenField = TryCast(rptItem.FindControl("hdHideModify"), HiddenField)
        hdHideModify.Value = "false"
        rep_hdWeekCnt.Value = (CInt(rep_hdWeekCnt.Value) + 1).ToString
        Dim ddlGoTo As DropDownList = TryCast(rptItem.FindControl("ddlGoTo"), DropDownList)
        ddlGoTo.SelectedIndex = ddlGoTo.Items.IndexOf(ddlGoTo.Items.FindByText(Convert.ToString(rep_hdWeekCnt.Value)))

        ddlGoTo_SelectedIndexChanged(ddlGoTo, New EventArgs)
        'BindCoachClassDetailsInGrid(grd)
    End Sub

    Private Sub BindCoachClassDetailsInGrid(grCtrl As GridView, iShowAll As Integer)
        Dim dt As Date = Now.Date.ToString("yyyy-MM-dd")  ' "2014-12-02"
        Dim rpItem As RepeaterItem = grCtrl.NamingContainer
        Dim iProductId As Integer = DirectCast(rpItem.FindControl("hdProduct"), HiddenField).Value
        Dim iProductGrpId As Integer = DirectCast(rpItem.FindControl("hdProductGrp"), HiddenField).Value
        Dim iPhase As Integer = DirectCast(rpItem.FindControl("lblPhase"), Label).Text
        Dim strLevel As String = DirectCast(rpItem.FindControl("lblLevel"), Label).Text
        Dim iSessionNo As Integer = DirectCast(rpItem.FindControl("lblSessionNo"), Label).Text
        Dim rep_hdWeekCnt As HiddenField = TryCast(rpItem.FindControl("rep_hdWeekCnt"), HiddenField)

        If Len(Trim(rep_hdWeekCnt.Value)) = 0 Then
            rep_hdWeekCnt.Value = "1"
        End If
        '@MemberId int, @EventYear int, @ProductGroupId int, @ProductId int, @Level varchar(50), @SessionNo int , @Iterator int
        Dim params() As SqlParameter = {New SqlParameter("@MemberId", SqlDbType.Int), New SqlParameter("@EventYear", SqlDbType.Int), New SqlParameter("@ProductGroupId", SqlDbType.Int), New SqlParameter("@ProductId", SqlDbType.Int), New SqlParameter("@Phase", SqlDbType.Int), New SqlParameter("@Level", SqlDbType.VarChar, 50), New SqlParameter("@SessionNo", SqlDbType.Int), New SqlParameter("@Iterator", SqlDbType.Int), New SqlParameter("@ShowAll", SqlDbType.Int)}
        params(0).Value = hdMemberId.Value
        params(1).Value = hdTable1Year.Value
        params(2).Value = iProductGrpId
        params(3).Value = iProductId
        params(4).Value = iPhase
        params(5).Value = strLevel
        params(6).Value = iSessionNo
        params(7).Value = rep_hdWeekCnt.Value
        params(8).Value = iShowAll
        'productgroupID, productID, phase, level, week#, DocType=Q, 
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.StoredProcedure, "usp_GetCoachDetailsByWeek", params)
        If ds.Tables(0).Rows.Count = 0 Then
            rpItem.Visible = False
        Else
            grCtrl.DataSource = ds.Tables(0)
            grCtrl.DataBind()
        End If
    End Sub
    Protected Sub rptCoachClass_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCoachClass.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim hdProductGrp As HiddenField = DirectCast(e.Item.FindControl("hdProductGrp"), HiddenField)
            Dim hdProduct As HiddenField = DirectCast(e.Item.FindControl("hdProduct"), HiddenField)
            Dim hdTotalClass As HiddenField = DirectCast(e.Item.FindControl("hdTotalClass"), HiddenField)
            Dim lblPhase As Label = DirectCast(e.Item.FindControl("lblPhase"), Label)
            cmdText = "select classes from EventFees Where EventID=13 and EventYear= " & hdTable1Year.Value & " and ProductGroupId=" & hdProductGrp.Value & " and ProductId=" & hdProduct.Value
            Dim iTotalClass As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            'cmdText = "select StartDate, EndDate from CoachingDateCal Where EventId=13 and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Phase=" + lblPhase.Text + " and ScheduleType='term'"

            cmdText = "select StartDate, EndDate from CoachingDateCal Where EventId=13 and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Phase=" + lblPhase.Text + ""

            Dim dsClass As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If dsClass.Tables(0).Rows.Count = 0 Then
                e.Item.Visible = False
                Exit Sub
            End If
            Dim hdEndDate As HiddenField = DirectCast(e.Item.FindControl("hdEndDate"), HiddenField)
            hdEndDate.Value = Convert.ToDateTime(dsClass.Tables(0).Rows(0)("EndDate")).ToString("MM/dd/yyyy")
            hdTotalClass.Value = 0
            If iTotalClass > 0 Then
                e.Item.Visible = True
                hdTotalClass.Value = iTotalClass
                Dim grid As GridView = DirectCast(e.Item.FindControl("rpt_grdCoachClassCal"), GridView)
                Dim rep_hdWeekCnt As HiddenField = DirectCast(e.Item.FindControl("rep_hdWeekCnt"), HiddenField)
                rep_hdWeekCnt.Value = "1"
                BindCoachClassDetailsInGrid(grid, 1)
                Dim lblTableCnt As Label = DirectCast(e.Item.FindControl("lblTableCnt"), Label)
                lblTableCnt.Text = CInt(e.Item.ItemIndex) + 1
                Dim ddlGoTo As DropDownList = DirectCast(e.Item.FindControl("ddlGoTo"), DropDownList)
                Dim rpt_grdCoachClassCal As GridView = DirectCast(e.Item.FindControl("rpt_grdCoachClassCal"), GridView)
                rep_hdWeekCnt.Value = rpt_grdCoachClassCal.Rows.Count
                Dim k As Integer
                ddlGoTo.Items.Clear()
                ddlGoTo.Items.Add(New ListItem("Select", -1))
                For k = 1 To iTotalClass
                    ddlGoTo.Items.Add(New ListItem(k, k))
                Next
                ddlGoTo.SelectedIndex = ddlGoTo.Items.IndexOf(ddlGoTo.Items.FindByText(Convert.ToString(rep_hdWeekCnt.Value)))
            Else
                e.Item.Visible = False
            End If
        End If
    End Sub
    Protected Sub rpt_grdCoachClassCal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lbl As Label = CType(e.Row.FindControl("lblSerial"), Label)
                lbl.Text = Convert.ToInt16(e.Row.RowIndex) + 1
                Dim lblWeekNo As Label = CType(e.Row.FindControl("lblWeekNo"), Label)
                lblWeekNo.Text = Convert.ToInt16(e.Row.RowIndex) + 1

                Dim lblWeek As Label = CType(e.Row.FindControl("lblWeek"), Label)
                Dim lblStartDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblStartDate"), Label)
                Dim hdProductGrp As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdProductGrp"), HiddenField)
                Dim hdProduct As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdProduct"), HiddenField)
                Dim hdTotalClass As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdTotalClass"), HiddenField)

                Dim lblPhase As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblPhase"), Label)
                Dim lblSessionNo As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblSessionNo"), Label)
                Dim rep_hdWeekCnt As HiddenField = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_hdWeekCnt"), HiddenField)
                Dim hdHideModify As HiddenField = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("hdHideModify"), HiddenField)

                Dim lblDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblDate"), Label)
                Dim lblCoachClassCalID As Label = DirectCast(e.Row.Cells(1).FindControl("lblCoachClassCalID"), Label)
                Dim ddlStatus As DropDownList = DirectCast(e.Row.Cells(7).FindControl("ddlStatus"), DropDownList)
                Dim hdStatus As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdStatus"), HiddenField)
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(Convert.ToString(hdStatus.Value)))
                Dim ddlSubstitute As DropDownList = DirectCast(e.Row.Cells(8).FindControl("ddlSubstitute"), DropDownList)
                Dim hdSubstitute As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdSubstitute"), HiddenField)
                FillCoachByCondition(hdProductGrp.Value, hdProduct.Value, ddlSubstitute)
                ddlSubstitute.SelectedIndex = ddlSubstitute.Items.IndexOf(ddlSubstitute.Items.FindByValue(Convert.ToString(hdSubstitute.Value)))
                Dim ddlReason As DropDownList = DirectCast(e.Row.Cells(9).FindControl("ddlReason"), DropDownList)
                Dim hdReason As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdReason"), HiddenField)
                ddlReason.SelectedIndex = ddlReason.Items.IndexOf(ddlReason.Items.FindByText(Convert.ToString(hdReason.Value)))
                Dim lblHWRelDate As Label = DirectCast(e.Row.Cells(10).FindControl("lblHWRelDate"), Label)
                Dim lblHWDueDate As Label = DirectCast(e.Row.Cells(11).FindControl("lblHWDueDate"), Label)
                Dim lblARelDate As Label = DirectCast(e.Row.Cells(12).FindControl("lblARelDate"), Label)
                Dim lblSRelDate As Label = DirectCast(e.Row.Cells(13).FindControl("lblSRelDate"), Label)
                Dim lblDay As Label = DirectCast(e.Row.Cells(2).FindControl("lblDay"), Label)
                Dim lblScheduleType As Label = DirectCast(e.Row.Cells(10).FindControl("lblScheduleType"), Label)
                'If lblCoachClassCalID.Text = "" Or ddlStatus.SelectedIndex = 0 Then
                '    lblHWRelDate.Text = lblDate.Text
                '    lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                '    lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                'Else
                '    lblHWRelDate.Text = lblDate.Text
                '    lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                '    lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                'End If

                Dim lblRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblRelDt"), Label)
                If lblRelDT.Text.Trim = "" Then
                    If hdStatus.Value <> "Cancelled" And hdStatus.Value <> "Not set" Then


                        lblHWRelDate.Text = lblDate.Text
                        lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                        lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                        If hdProductGrp.Value = "31" Then
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(8)).Date.ToString("MM/dd/yyyy")
                        ElseIf hdProductGrp.Value = "33" Then
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(8)).Date.ToString("MM/dd/yyyy")
                        ElseIf hdProductGrp.Value = "41" Then
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(6)).Date.ToString("MM/dd/yyyy")
                        Else
                            lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                        End If

                    End If
                Else

                    Dim lblDueDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblDueDt"), Label)
                    Dim lblARelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblARelDt"), Label)
                    Dim lblSRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblSRelDt"), Label)
                    lblHWRelDate.Text = lblRelDT.Text
                    lblHWDueDate.Text = DateTime.ParseExact(lblDueDT.Text, "MM/dd/yyyy", Nothing)
                    lblARelDate.Text = DateTime.ParseExact(lblARelDT.Text, "MM/dd/yyyy", Nothing)
                    lblSRelDate.Text = DateTime.ParseExact(lblSRelDT.Text, "MM/dd/yyyy", Nothing)

                End If

                Dim curWeek As Integer, TotalWeek As Integer
                Dim hdEndDate As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdEndDate"), HiddenField)
                Dim stWeek As Date, endWeek As Date, curDate As Date, endDate As Date
                curDate = DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing)
                stWeek = DateAdd("d", 0 - curDate.DayOfWeek, curDate)
                endWeek = DateAdd("d", 6 - curDate.DayOfWeek, curDate)
                endDate = DateTime.ParseExact(hdEndDate.Value, "MM/dd/yyyy", Nothing)
                If endDate >= stWeek And endDate <= endWeek Then
                    Dim lnkBtnShowNxt As LinkButton = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
                    lnkBtnShowNxt.Enabled = False
                End If

                If lblCoachClassCalID.Text = 0 Or ddlStatus.SelectedItem.Text = "Not set" Or ddlStatus.SelectedItem.Text = "Select" Or ddlStatus.SelectedItem.Text = "Cancelled" Then
                    cmdText = "select isnull(max(weekno),0) as weekno from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Phase=" + lblPhase.Text + " and SessionNo=" + lblSessionNo.Text + " and Day='" + lblDay.Text + "' and [date] <'" & lblDate.Text & "' and Status in ('On','Substitute') "
                    Dim dsClass As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If dsClass.Tables(0).Rows.Count > 0 Then
                        curWeek = dsClass.Tables(0).Rows(0).Item("weekno")
                        TotalWeek = hdTotalClass.Value
                        If curWeek >= TotalWeek Then
                            Dim lnkBtnShowNxt As LinkButton = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
                            lnkBtnShowNxt.Enabled = False
                        End If
                    End If
                    lblWeek.Text = curWeek
                End If
                'Disable Modify Button
                If hdHideModify.Value = "false" Then
                    Dim btnModify As Button = DirectCast(e.Row.Cells(0).FindControl("btnModify"), Button)
                    If ddlStatus.SelectedItem.Text = "Select" Then
                        btnModify.Enabled = True
                        btnModify.Text = "Edit"
                        'Modified by Simson
                        hdHideModify.Value = "true"
                        hdHideModify.Value = "false"
                    Else
                        btnModify.Enabled = True
                        btnModify.Text = "Modify"

                    End If
                    cmdText = "select isnull(max(weekno),0) as weekno from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Phase=" + lblPhase.Text + " and SessionNo=" + lblSessionNo.Text + " and Day='" + lblDay.Text + "' and [date] >'" & lblDate.Text & "' and Status in ('On','Substitute') "
                    Dim iMaxWeekCnt As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                    'Modified by Simson
                    'If curWeek < iMaxWeekCnt Then
                    '    btnModify.Enabled = False
                    'End If
                    'Server
                    Dim todayDate As DateTime = DateTime.Now.ToString("MM/dd/yyyy")


                    If lblARelDate.Text <> "__/__/____" Then

                        Dim RelDate As DateTime = Convert.ToDateTime(lblARelDate.Text).ToString("MM/dd/yyyy")
                        If RelDate < todayDate And curWeek < iMaxWeekCnt Then
                            btnModify.Enabled = False
                        End If
                    Else
                        If curWeek < iMaxWeekCnt Then
                            btnModify.Enabled = False
                        Else
                            btnModify.Enabled = True
                        End If

                    End If
                    If hdStatus.Value = "Cancelled" Then
                        Dim lblDueDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblDueDt"), Label)
                        Dim lblARelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblARelDt"), Label)
                        Dim lblSRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblSRelDt"), Label)

                        lblHWDueDate.Text = "__/__/____"
                        lblARelDate.Text = "__/__/____"
                        lblSRelDate.Text = "__/__/____"
                        lblHWRelDate.Text = "__/__/____"
                    End If
                    'If lblScheduleType.Text = "Thanksgiving" Or lblScheduleType.Text = "Christmas" Then
                    '    btnModify.Enabled = False
                    'End If

                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        FillProductGroup()
    End Sub
    'Public Sub ValidatSubstituteSession(Level As String, SubstituteCoachID As String, phase As String)
    '    Try
    '        Dim CmdText As String = String.Empty
    '        Dim ds As New DataSet()
    '        Dim count As Integer = 0
    '        Dim SessionCreationDate As String = String.Empty
    '        Dim CoachChange As String = String.Empty
    '        'local
    '        Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
    '        ' Server
    '        ' String CurrDate = DateTime.Today.ToString("MM/dd/yyyy");
    '        Dim dt As DateTime = Convert.ToDateTime(CurrDate)
    '        CmdText = "select SessionCreationDate,CoachChange from WebConfControl where EventYear=" + ddYear.SelectedValue + ""
    '        If ddlProductGroup.SelectedValue <> "0" AndAlso ddlProductGroup.SelectedValue <> "All" Then
    '            CmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue + ""
    '        End If

    '        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)
    '        Dim Meetinkey As String = String.Empty
    '        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                'local
    '                SessionCreationDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("SessionCreationDate").ToString()).ToString("dd/MM/yyyy")
    '                'Server
    '                'SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("MM/dd/yyyy");
    '                CoachChange = ds.Tables(0).Rows(0)("CoachChange").ToString()
    '            End If
    '        End If
    '        If SessionCreationDate <> "" Then
    '            Dim dtSession As DateTime = Convert.ToDateTime(SessionCreationDate)
    '            If dt >= dtSession AndAlso CoachChange.Trim() = "N" Then

    '                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.SubstituteMeetKey,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + DateTime.Now.Year + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + DateTime.Now.Year + "' and CS.MemberID=" + SubstituteCoachID + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + DateTime.Now.Year + " and Approved='Y') and CS.UserID is not null  and CS.Level='" + Level + "' and CS.Phase='" + phase + "'"

    '                ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

    '                If ds.Tables(0) IsNot Nothing Then
    '                    If ds.Tables(0).Rows.Count > 0 Then
    '                        For Each dr As DataRow In ds.Tables(0).Rows
    '                            count += 1

    '                            Dim IsSameDay As Integer = 0
    '                            Dim IsSameTime As Integer = 0
    '                            Dim WebExID As String = dr("UserID").ToString()
    '                            Dim Pwd As String = dr("PWD").ToString()
    '                            Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
    '                            Dim ScheduleType As String = dr("ScheduleType").ToString()
    '                            Meetinkey = dr("meetingKey").ToString()
    '                            Dim year As String = dr("EventYear").ToString()
    '                            Dim eventID As String = dr("EventID").ToString()
    '                            Dim chapterId As String = "112"
    '                            Dim ProductGroupID As String = dr("ProductGroupID").ToString()
    '                            Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
    '                            Dim ProductID As String = dr("ProductID").ToString()
    '                            Dim ProductCode As String = dr("ProductCode").ToString()
    '                            'Dim Phase As String = dr("Phase").ToString()
    '                            'Dim Level As String = dr("Level").ToString()
    '                            Dim Sessionno As String = dr("SessionNo").ToString()
    '                            Dim CoachID As String = dr("MemberID").ToString()
    '                            Dim CoachName As String = dr("CoachName").ToString()

    '                            Dim MeetingPwd As String = "training"

    '                            Dim [Date] As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
    '                            Dim Time As String = dr("Time").ToString()
    '                            Dim Day As String = dr("Day").ToString()
    '                            Dim BeginTime As String = dr("Begin").ToString()
    '                            Dim EndTime As String = dr("End").ToString()
    '                            Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
    '                            Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
    '                            If BeginTime = "" Then
    '                                BeginTime = "20:00:00"
    '                            End If
    '                            If EndTime = "" Then
    '                                EndTime = "21:00:00"
    '                            End If

    '                            'If txtDuration.Text <> "" Then
    '                            '    Dim Duration As Integer = Convert.ToInt32(txtDuration.Text)
    '                            'End If
    '                            Dim timeZoneID As String = String.Empty

    '                            timeZoneID = "112"


    '                            Dim SignUpId As String = dr("SignupID").ToString()
    '                            Dim Mins As Double = 0.0
    '                            Dim dFrom As DateTime
    '                            Dim dTo As DateTime



    '                            Dim userID As String = Session("LoginID").ToString()


    '                            If dr("SubstituteMeetKey").ToString() = "" Then

    '                                ' RegisterAlternateHost(Meetinkey, EndTime, Day, SignUpId, WebExID, Pwd)
    '                            Else
    '                                'If dr("MakeupMeetKey").ToString() = "" Then
    '                                '    lblerr.Text = "Duplicate exists..!"
    '                                'Else
    '                                '    lblerr.Text = "Training sessions not created yet for the selected coach."
    '                                'End If
    '                            End If
    '                        Next
    '                    Else
    '                        'lblerr.Text = "No child is assigned for the selected Coach.."
    '                    End If
    '                End If
    '            Else
    '                If dt < dtSession Then
    '                    'lblerr.Text = (Convert.ToString("This application can only be run on ") & SessionCreationDate) + ""
    '                ElseIf CoachChange.Trim() = "Y" Then
    '                    'lblerr.Text = "This application cannot be run since the coach change flag is not set to N"
    '                End If
    '            End If
    '        Else
    '            'lblerr.Text = "This application can only be run on....."

    '        End If
    '        'throw new Exception(ex.Message);
    '    Catch ex As Exception

    '    End Try

    'End Sub



    Protected Sub ddlCoach_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoach.SelectedIndexChanged
        FillProductGroup()
    End Sub
End Class

