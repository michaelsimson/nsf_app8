Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml
Imports NativeExcel

Partial Class CalendarSignup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = "1"
        'Session("LoginID") = "4240"
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not Page.IsPostBack Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, Convert.ToString(year))
            ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
            If Now.Month <= 3 Then
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year - 1)))
            Else
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
            End If

            ''Options for Volunteer Name Selection
            If Session("RoleID") = 88 Then 'Coach can schedule for his own Sessions.
                txtName.Visible = True
                btnSearch.Visible = False
                btnClear.Visible = False
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID from IndSpouse I inner join Volunteer V On V.MemberID = I.AutoMemberID where V.RoleId in (" & Session("RoleID") & ") and V.MemberID=" & Session("LoginID"))
                If ds.Tables(0).Rows.Count > 0 Then
                    txtName.Text = ds.Tables(0).Rows(0)("Name")
                    hdnMemberID.Value = ds.Tables(0).Rows(0)("MemberID")
                End If
            ElseIf (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 96 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches. 
                If Request.QueryString("Role") = "Coach" Then
                    txtName.Visible = False
                    btnSearch.Visible = False
                    btnClear.Visible = False
                Else
                    txtName.Visible = True
                    btnSearch.Visible = True
                    btnClear.Visible = True
                End If
                ddlVolName.Visible = True

                '********* To display all Coaches ****************'
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join Volunteer V On V.MemberID = I.AutoMemberID Where V.RoleId in (88) order by I.LastName,I.FirstName")

                'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.LastName,I.FirstName")
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlVolName.DataSource = ds
                    ddlVolName.DataBind()
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlVolName.Items.Insert(0, "Select Volunteer")
                        ddlVolName.SelectedIndex = 0
                    End If
                Else
                    lblerr.Text = "No Volunteers present for Calendar Sign up"
                End If

            Else
                txtName.Visible = True
                ddlVolName.Visible = False
                btnSearch.Visible = True
                btnClear.Visible = True
            End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            'If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
            'LoadProductGroup()
            'Else
            If Session("RoleId").ToString() = "89" Then 'Session("RoleId").ToString() = "88" Or
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                    'more than one 
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                    Dim i As Integer
                    Dim prd As String = String.Empty
                    Dim Prdgrp As String = String.Empty
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If prd.Length = 0 Then
                            prd = ds.Tables(0).Rows(i)(1).ToString()
                        Else
                            prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                        End If

                        If Prdgrp.Length = 0 Then
                            Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                        Else
                            Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                        End If
                    Next
                    lblPrd.Text = prd
                    lblPrdGrp.Text = Prdgrp
                    'LoadProductGroup()
                Else
                    'only one
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                    Dim prd As String = String.Empty
                    Dim Prdgrp As String = String.Empty
                    If ds.Tables(0).Rows.Count > 0 Then
                        prd = ds.Tables(0).Rows(0)(1).ToString()
                        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                    End If
                    'LoadProductGroup()
                End If
            ElseIf Session("RoleId").ToString() = "88" Then
                'Ferdine changing this on Sep 2nd
                'If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & Session("LoginID") & " and EventYear=" & Now.Year & "  and ProductId is not Null") > 1 Then
                'more than one 
                ''Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from CalSignUp where Memberid=" & Session("LoginID") & " and EventYear=" & Now.Year & " and ProductId is not Null ")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID FROM Product  WHERE EventID in (13,19) ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                'LoadProductGroup()
                ' Else
                ''    'only one
                ''    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from CalSignUp where Memberid=" & Session("LoginID") & "  and EventYear=" & Now.Year & " and ProductId is not Null ")
                ''    Dim prd As String = String.Empty
                ''    Dim Prdgrp As String = String.Empty
                ''    If ds.Tables(0).Rows.Count > 0 Then
                ''        prd = ds.Tables(0).Rows(0)(1).ToString()
                ''        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                ''        lblPrd.Text = prd
                ''        lblPrdGrp.Text = Prdgrp
                ''    End If
                ''    'LoadProductGroup()
                ''End If
            End If

            LoadEvent(ddlEvent)
            LoadWeekdays(ddlWeekDays)
            'LoadDisplayTime(ddlDisplayTime, False)
            LoadCapacity(ddlMaxCapacity)
            LoadGrid()
            LoadMakeUpSessions()
            LoadSubstituteSessions()
            lblerr.Text = ""
        End If
    End Sub
    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
                '*********Added on 26-11-2013 to disable Prepbclub for the current year
                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"))
                ddlObject.Enabled = False
                LoadProductGroup()
                '**************************************************************************'
            Else
                ddlObject.Enabled = False
                LoadProductGroup()
            End If
            'ddlObject.SelectedIndex = 0
        Else
            lblerr.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Public Sub LoadDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "8:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub
    Public Sub LoadWeekDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "6:00 PM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub

    Private Sub LoadWeekdays(ByVal ddlObject As DropDownList)
        'For i As Integer = 0 To 6
        'items.Add(new ListItem("Item 2", "Value 2"));
        ddlObject.Items.Add(New ListItem("Select Day", "-1"))
        ddlObject.Items.Add(New ListItem("Monday", "0"))
        ddlObject.Items.Add(New ListItem("Tuesday", "1"))
        ddlObject.Items.Add(New ListItem("Wednesday", "2"))
        ddlObject.Items.Add(New ListItem("Thursday", "3"))
        ddlObject.Items.Add(New ListItem("Friday", "4"))
        ddlObject.Items.Add(New ListItem("Saturday", "5"))
        ddlObject.Items.Add(New ListItem("Sunday", "6"))

        'ddlObject.Items.Add(New ListItem(WeekdayName(i + 1), i))
        ' ddlWeekDays.Items.Add(WeekdayName(i))
        'Next
    End Sub

    Private Sub LoadCapacity(ByVal ddltemp As DropDownList)
        Dim li As ListItem
        Dim i As Integer
        For i = 0 To 30 Step 1 '10 To 100 Step 5
            li = New ListItem(i)
            ddltemp.Items.Add(li)
        Next
        ddltemp.SelectedIndex = ddltemp.Items.IndexOf(ddltemp.Items.FindByValue(20))
    End Sub

    Private Sub LoadProductGroup()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblerr.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
                LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ddlProduct.Enabled = False
            Else
                Dim strSql As String
                strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & "  order by P.ProductID " 'and P.Status='O'
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, "Select Product")
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    LoadGrid()
                    LoadMakeUpSessions()
                    LoadSubstituteSessions()
                End If
            End If
        Catch ex As Exception
            lblerr.Text = lblerr.Text & "<br>" & ex.ToString
        End Try
    End Sub
    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String)
        ddlObject.Items.Clear()
        ddlObject.Enabled = True
        If ProductGroup.Contains("SAT") Then ' ddlProductGroup.SelectedItem.Text.Contains("SAT") Then
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("Junior", 1))
            ddlObject.Items.Insert(2, New ListItem("Senior", 2))
        ElseIf ProductGroup.Contains("Universal Values") Then
            ''ddlObject.Enabled = False
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("Junior", 1))
            ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
            ddlObject.Items.Insert(3, New ListItem("Senior", 3))
        ElseIf ProductGroup.Contains("Science") Then
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("One level only", 1))
        Else
            ddlObject.Items.Insert(0, New ListItem("Select", 0))
            ddlObject.Items.Insert(1, New ListItem("Beginner", 1))
            ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
            ddlObject.Items.Insert(3, New ListItem("Advanced", 3))
        End If
        ddlWeekDays.SelectedIndex = 0
    End Sub

    Function CheckVolunteer() As Boolean
        lblerr.Text = ""
        If ddlVolName.Visible = False And txtName.Visible = True And txtName.Text = "" Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = False And ddlVolName.Visible = True And ddlVolName.SelectedIndex = 0 Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = True And ddlVolName.Visible = True Then
            If ddlVolName.SelectedIndex = 0 And txtName.Text.Trim.ToString() = "" Then
                lblerr.Text = "Please select Volunteer "
            ElseIf ddlVolName.SelectedIndex > 0 And txtName.Text <> "" Then
                lblerr.Text = "Please select one of the options for Volunteer"
            Else
                lblerr.Text = ""
            End If
        End If

        If lblerr.Text = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            lblError.Text = ""
            If CheckVolunteer() = False Then
                lblerr.Text = lblerr.Text '"Please select Volunteer"
            ElseIf ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblerr.Text = "Please select Event"
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblerr.Text = "No Product Group is present for selected Event."
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblerr.Text = "Please select Product Group"
            ElseIf ddlProduct.Items.Count = 0 Then
                lblerr.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblerr.Text = "Please select Product"
                'ElseIf ddlLevel.Enabled = True And ddlLevel.SelectedItem.Text = "Select" Then
            ElseIf ddlLevel.SelectedItem.Text = "Select" Then
                lblerr.Text = "Please select Level"
                'End If
            ElseIf ddlWeekDays.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Day"
            ElseIf ddlDisplayTime.SelectedIndex = 0 Then
                lblerr.Text = "Please Enter Coaching time"
            ElseIf ddlMaxCapacity.SelectedValue < 2 Then
                lblerr.Text = "Please select Maximum Capacity"
            Else
                lblerr.Text = ""
                Dim strSQL As String
                Dim Level As String = ""
                If ddlLevel.Enabled = True Then
                    If ddlLevel.SelectedItem.Text = "Select" Then
                        lblerr.Text = "Please select Level"
                        Exit Sub
                    End If
                    Level = ddlLevel.SelectedItem.Text
                End If

                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "Select Count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Eventyear=" & ddlEventYear.SelectedValue & "and EventID =" & ddlEvent.SelectedValue & " and Day='" & ddlWeekDays.SelectedItem.Text & "' and Time='" & ddlDisplayTime.SelectedItem.Text & "'") > 0 Then
                    lblerr.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
                ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue & IIf(Level <> "", " and Level = '" & Level & "'", "") & " AND Phase=" & ddlPhase.SelectedValue & " AND SessionNo=" & ddlSession.SelectedValue) < 3 Then '<3 Then
                    strSQL = "INSERT INTO CalSignUp (MemberID, EventYear,EventID,EventCode, Phase,ProductGroupID, ProductGroupCode, ProductID, ProductCode, [Level],SessionNo, Day,Time,Preference,MaxCapacity, CreatedBy,CreateDate) VALUES ("
                    strSQL = strSQL & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & "," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & ddlPhase.SelectedValue & "," & ddlProductGroup.SelectedValue & ",'" & getProductGroupcode(ddlProductGroup.SelectedValue) & "'," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "'," & IIf(Level <> "", "'" & Level & "'", "NULL") & "," & ddlSession.SelectedValue & ",'" & ddlWeekDays.SelectedItem.Text & "','" & ddlDisplayTime.SelectedValue & "'," & ddlPref.SelectedValue & "," & ddlMaxCapacity.SelectedValue & "," & Session("loginID") & ",Getdate())"
                    If SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strSQL) > 0 Then
                        'clear()
                        LoadGrid()
                        lblerr.Text = "Inserted Successfully"
                    End If
                Else
                    lblerr.Text = "Coach with Same Product, Phase,Level and Session already found"
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub LoadGrid()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo) as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc"
            Else
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo) as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue <> "Select Product Group", " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Phase=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql = StrSql & " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                Session("volDataSet") = ds

                If ds.Tables(0).Rows.Count > 0 Then
                    lblerr.Text = ""
                    spnTableTitle.Visible = True
                    DGCoach.Visible = True
                    DGCoach.DataSource = ds
                    DGCoach.DataBind()
                    DGCoach.CurrentPageIndex = 0
                    DGCoach.Visible = True
                    'If Request.QueryString("Role") = "Admin" Then
                    '    DGCoach.Columns(13).Visible = True
                    'Else
                    '    DGCoach.Columns(13).Visible = False
                    'End If
                Else
                    DGCoach.Visible = False
                    spnTableTitle.Visible = False
                    If Page.IsPostBack = True Then
                        lblerr.Text = "No schedule available"
                    End If
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    'Public Sub loadGrid(ByVal blnReload As Boolean)
    '    'lblerr.Text = ""
    '    Dim ds As DataSet
    '    Try
    '        Dim StrSql As String = ""
    '        StrSql = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day, Time,C.Accepted,C.Preference FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedIndex > 0, " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")

    '        If blnReload = True Then
    '            Try
    '                If (ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0) Or (ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0) Then
    '                    StrSql = StrSql & " and C.MemberID= " & IIf(Request.QueryString("Role") = "Coach", IIf(ddlVolName.Visible = True, ddlVolName.SelectedValue, Session("LoginID")), IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue)) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                ElseIf ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
    '                    StrSql = StrSql & " AND C.EventID=" & ddlEvent.SelectedValue & " AND C.Phase=" & ddlPhase.SelectedValue & " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue & IIf(ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "") & IIf(Request.QueryString("Role") = "Coach", " and C.MemberID=" & Session("LoginID"), "") & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                End If

    '                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
    '                Session("volDataSet") = ds

    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    lblerr.Text = ""
    '                    DGCoach.Visible = True
    '                    DGCoach.DataSource = ds
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                    DGCoach.Visible = True
    '                    If Request.QueryString("Role") = "Admin" Then
    '                        DGCoach.Columns(13).Visible = True
    '                    Else
    '                        DGCoach.Columns(13).Visible = False
    '                    End If
    '                Else
    '                    DGCoach.Visible = False
    '                    lblerr.Text = "No schedule available"
    '                End If

    '            Catch se As SqlException
    '                lblerr.Text = StrSql 'se.ToString()
    '                Return
    '            End Try
    '        Else
    '            If Request.QueryString("Role") = "Coach" Or Request.QueryString("Role") = "Admin" Then
    '                StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " Order By C.Day,C.Time,C.SignupID,I.LastName,I.FirstName"
    '                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
    '                Session("volDataSet") = ds
    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    lblerr.Text = ""
    '                    DGCoach.Visible = True
    '                    DGCoach.DataSource = ds
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                    DGCoach.Visible = True
    '                    If Request.QueryString("Role") = "Admin" Then
    '                        DGCoach.Columns(13).Visible = True
    '                    Else
    '                        DGCoach.Columns(13).Visible = False
    '                    End If
    '                    DGCoach.DataBind()
    '                    DGCoach.CurrentPageIndex = 0
    '                Else
    '                    DGCoach.Visible = False
    '                    lblerr.Text = "No schedule available"
    '                End If
    '            Else
    '                ds = CType(Session("volDataSet"), DataSet)
    '                DGCoach.DataSource = Nothing 'ds
    '                DGCoach.DataBind()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex.ToString())
    '    End Try
    'End Sub

    Protected Sub DGCoach_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim SignUpID As Integer
            SignUpID = CInt(e.Item.Cells(3).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM CalSignUp WHERE SignUpID =" + CStr(SignUpID))
                If rowsAffected > 0 Then
                    lblError.Text = "Deleted Successfully."
                End If
            Catch se As SqlException
                lblError.Text = "Error: while deleting the record"
                Return
            End Try

            LoadGrid()

            DGCoach.EditItemIndex = -1
        ElseIf (cmdName = "SelectMeetingURL") Then

            Dim SessionKey As String = String.Empty
            Dim UserID As String = String.Empty
            Dim Pwd As String = String.Empty
            Dim HostUrl As LinkButton = Nothing
            SessionKey = CType(e.Item.FindControl("lblSessionsKey"), Label).Text
            UserID = CType(e.Item.FindControl("lblDGUserID"), Label).Text

            Pwd = CType(e.Item.FindControl("lblDGPWD"), Label).Text
            HostUrl = CType(e.Item.FindControl("lnkMeetingURL"), LinkButton)
            GenerateURL(SessionKey, Pwd, UserID)
        ElseIf (cmdName = "SelectMakeUpURL") Then

            Dim SessionKey As String = String.Empty
            Dim UserID As String = String.Empty
            Dim Pwd As String = String.Empty
            Dim HostUrl As LinkButton = Nothing
            SessionKey = CType(e.Item.FindControl("lblMakeupKey"), Label).Text
            UserID = CType(e.Item.FindControl("lblDGUserID"), Label).Text

            Pwd = CType(e.Item.FindControl("lblDGPWD"), Label).Text
            HostUrl = CType(e.Item.FindControl("lnkMakeUpMeetingURL"), LinkButton)
            GenerateURL(SessionKey, Pwd, UserID)

        End If
    End Sub

    Protected Sub DGCoach_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.EditCommand
        DGCoach.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = CType(Session("volDataSet"), DataSet)
        Dim page As Integer = DGCoach.CurrentPageIndex
        Dim pageSize As Integer = DGCoach.PageSize
        Dim currentRowIndex As Integer
        lblerr.Text = ""
        lblError.Text = ""
        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If

        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr

        LoadGrid()
    End Sub

    Private Sub DGCoach_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGCoach.ItemCreated
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            Dim Edtbtn As LinkButton = Nothing
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)


            If Session("RoleID") = 88 Then
                Dim flag As Object = DataBinder.Eval(e.Item.DataItem, "Accepted")
                If Not flag Is DBNull.Value Then
                    Edtbtn = CType(e.Item.Cells(0).Controls(0), LinkButton)
                    Edtbtn.Enabled = False
                    btn.Enabled = False
                Else
                    btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
                End If
            Else
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
            End If



        End If
    End Sub

    Protected Sub DGCoach_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.UpdateCommand
        Dim sqlStr As String
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim SignUpID As Integer
        Dim MemberID As Integer
        Dim EventID As Integer
        Dim EventYear As Integer
        Dim Capacity As Integer
        Dim SessionNo As Integer
        Dim ProductID As Integer
        Dim Level As String
        Dim Day As String
        Dim Time As String
        Dim Preference As Integer
        Dim Accepted As String
        Dim UserID As String
        Dim PwD As String
        Dim Vroom As Integer
        Try
            SignUpID = CInt(e.Item.Cells(3).Text)
            'MemberID = CInt(CType(e.Item.FindControl("txtMemberID"), TextBox).Text)
            EventYear = CInt(CType(e.Item.FindControl("ddlDGEventYear"), DropDownList).SelectedValue)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                ProductID = dr.Item("productID")
                MemberID = dr.Item("MemberID")
                EventID = dr.Item("EventID")
            End If
            If CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Items.Count > 0 And CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Enabled = True Then
                Level = CStr(CType(e.Item.FindControl("ddlDGLevel"), DropDownList).SelectedItem.Text)
            Else
                Level = ""
            End If
            SessionNo = CInt(CType(e.Item.FindControl("ddlDGSessionNo"), DropDownList).SelectedItem.Value)
            Capacity = CInt(CType(e.Item.FindControl("ddlDGMaxCapacity"), DropDownList).SelectedItem.Value)
            Time = CStr(CType(e.Item.FindControl("ddlDGTime"), DropDownList).SelectedItem.Text)
            Day = CStr(CType(e.Item.FindControl("ddlDGDay"), DropDownList).SelectedItem.Text) 'Value)

            Preference = CInt(CType(e.Item.FindControl("ddlDGPreferences"), DropDownList).SelectedItem.Value)
            If CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).Enabled = True Then
                If hdInAppr.Value <> "" Then
                    CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedIndex = 1
                End If
                Accepted = CStr(CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedItem.Value)
                If Accepted = "" Then
                    Accepted = "NULL"
                End If
            Else
                Accepted = "NULL"
            End If
            UserID = CStr(CType(e.Item.FindControl("txtDGUserID"), TextBox).Text)
            PwD = CStr(CType(e.Item.FindControl("txtDGPWD"), TextBox).Text)
            Vroom = CInt(CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).SelectedValue)
            'Accepted = CStr(CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedItem.Value)

            ',ProductGroupID = " & ProductGroupID & ",ProductGroupCode = '" & getProductGroupcode(ProductGroupID) & "'
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and Eventyear=" & EventYear & " and EventID=" & EventID & " and Day='" & Day & "' and Time='" & Time & "' and  SignUpID not in (" & SignUpID & ")") > 0 Then
                lblError.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
            ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ")") < 3 Then
                sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "',Accepted=" & IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL") & " ,Preference=" & Preference & ",MaxCapacity =" & Capacity & ",UserID=" & IIf(UserID = "", "NULL", "'" & UserID & "'") & ",PWD=" & IIf(PwD = "", "NULL", "'" & PwD & "'") & ",Vroom=" & IIf(Vroom <= 0, "NULL", Vroom) & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE SignUpID=" & SignUpID
                If (Accepted <> "NULL") Then
                    Dim dsAppr As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, "select signupid,day,time from CalSignup where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Phase=" & ddlPhase.SelectedValue & " and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ") and Accepted='Y'")
                    If dsAppr.Tables(0).Rows.Count > 0 Then
                        'session
                        If hdInAppr.Value = "" Then
                            hdInAppr.Value = dsAppr.Tables(0).Rows(0).Item("SignupId")
                            hdToAppvId.Value = SignUpID
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmUpdate(" & SessionNo & ", '" & dsAppr.Tables(0).Rows(0).Item("Day").ToString() & "','" & dsAppr.Tables(0).Rows(0).Item("Time").ToString() & "');", True)
                            Exit Sub
                        Else
                            sqlStr = "UPDATE CalSignUp SET Accepted=null,ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE SignUpID=" & hdInAppr.Value
                            hdInAppr.Value = ""
                            hdToAppvId.Value = ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)
                        End If

                    End If
                End If

                sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "',Accepted=" & IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL") & " ,Preference=" & Preference & ",MaxCapacity =" & Capacity & ",UserID=" & IIf(UserID = "", "NULL", "'" & UserID & "'") & ",PWD=" & IIf(PwD = "", "NULL", "'" & PwD & "'") & ",Vroom=" & IIf(Vroom <= 0, "NULL", Vroom) & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE SignUpID=" & SignUpID
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr) > 0 Then
                    lblError.Text = "Updated Successfully"
                End If
                DGCoach.EditItemIndex = -1
                LoadGrid()

            Else
                lblError.Text = "Coach with same product, Phase, Level and Session are already found"
            End If
        Catch ex As SqlException
            'ex.message
            'Response.Write(ex.ToString())
            lblError.Text = "<br> Error:updating the record" + ex.ToString
            Return
        End Try

    End Sub

    Protected Sub DGCoach_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.CancelCommand
        Dim ds As DataSet = CType(Session("volDataSet"), DataSet)
        lblerr.Text = ""
        lblError.Text = ""
        If (Not ds Is Nothing) Then
            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = DGCoach.CurrentPageIndex
            Dim pageSize As Integer = DGCoach.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            DGCoach.EditItemIndex = -1

            LoadGrid()
        Else
            Return
        End If
    End Sub
    Protected Sub DGCoach_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGCoach.PageIndexChanged
        DGCoach.CurrentPageIndex = e.NewPageIndex
        DGCoach.EditItemIndex = -1
        LoadGrid()
    End Sub
    Public Sub ddlDaytime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("Day") = ddlTemp.SelectedItem.Text

            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGEventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            ddlTemp.Items.Insert(0, Convert.ToString(Now.Year() + 1))
            ddlTemp.Items.Insert(1, Convert.ToString(Now.Year()))
            ddlTemp.Items.Insert(2, Convert.ToString(Now.Year() - 1))

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("EventYear")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If

    End Sub

    Public Sub ddlDGMember_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String
        If Request.QueryString("Role") = "Coach" Then
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.FirstName"
        Else
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName"
        End If

        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
        If Session("RoleID") = 88 Then 'Request.QueryString("Role") = "Coach"Then
            ddlTemp.Enabled = False
        End If
    End Sub

    Public Sub ddlDGMember_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGEvent_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim EventCode As String = ""
        Dim EventId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventCode") Is DBNull.Value) Then
                EventCode = dr.Item("EventCode")
                EventId = dr.Item("EventId")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadEvent(ddlTemp)
            Dim rowEventID As Integer = 0
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(EventId))
        Else
            Return
        End If

    End Sub

    Public Sub ddlDGPhase_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Phase As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Phase") Is DBNull.Value) Then
                Phase = dr.Item("Phase")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Phase))
        End If
    End Sub

    Public Sub ddlDGProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProductGroupID As Integer = 0
        Dim rowEventID As Integer = 0

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Try
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If

            'Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup where EventId=" & rowEventID & "  order by ProductGroupID"
            Dim strSql As String = String.Empty
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & rowEventID & "  order by P.ProductGroupID"
            Dim drproductGroupid As SqlDataReader
            drproductGroupid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductGroupid
            ddlTemp.DataBind()
            If (Not dr.Item("productGroupID") Is DBNull.Value) Then
                rowProductGroupID = dr.Item("productGroupID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductGroupID))
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub ddlDGProduct_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProdGroupId As Integer = 0
        Dim rowEventID As Integer = 0

        Try
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
                rowProdGroupId = dr.Item("productGroupId")
            End If
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            'Dim strSql As String = "Select ProductID, Name from Product where EventID in (" & rowEventID & ") and ProductGroupID =" & rowProdGroupId & " order by ProductID"
            Dim strSql As String = String.Empty
            strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & rowEventID & " and P.ProductGroupID =" & rowProdGroupId & " order by P.ProductID"

            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductid
            ddlTemp.DataBind()
            Dim rowProductID As Integer = 0
            If (Not dr.Item("productID") Is DBNull.Value) Then
                rowProductID = dr.Item("productID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub ddlDGProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("productID") = ddlTemp.SelectedValue
            dr.Item("ProductCode") = getProductcode(ddlTemp.SelectedValue)
            'Session("editRow") = dr
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGLevel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim level As String = ""
        Dim ProductGroupCode As String = ""

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Level") Is DBNull.Value) Then
                level = dr.Item("Level")
            End If
            If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
                ProductGroupCode = dr.Item("ProductGroupCode")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadLevel(ddlTemp, ProductGroupCode)
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(level))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            ElseIf ProductGroupCode <> "UV" Then
                ddlTemp.Enabled = True
            End If

        Else
            Return
        End If
    End Sub

    Public Sub ddlDGSessionNo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SessionNo As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("SessionNo") Is DBNull.Value) Then
                SessionNo = dr.Item("SessionNo")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(SessionNo))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub
    Public Sub ddlDGDay_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Day As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If
            LoadWeekdays(ddlTemp)
            'For i As Integer = 1 To 7
            '    ddlTemp.Items.Add(WeekdayName(i))
            'Next
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Day))
        If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If
    End Sub

    Public Sub ddlDGTime_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Day As String = ""

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            'LoadDisplayTime(ddlTemp, False)
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If

            If Day = "Saturday" Or Day = "Sunday" Then
                LoadDisplayTime(ddlTemp, False)
            Else
                LoadWeekDisplayTime(ddlTemp, False)
            End If

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Time")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If
    End Sub

    Public Sub ddlDGAccepted_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Accepted As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Accepted") Is DBNull.Value) Then
                Accepted = dr.Item("Accepted")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Accepted))
            If Session("RoleID") = 88 Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGPreferences_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Preference As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Preference") Is DBNull.Value) Then
                Preference = dr.Item("Preference")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Preference))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGMaxCapacity_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            Dim li As ListItem
            Dim i As Integer
            For i = 0 To 35 Step 1 '10 To 100 Step 5
                li = New ListItem(i)
                ddlTemp.Items.Add(li)
            Next
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("MaxCapacity")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If
    End Sub
    Private Sub clear()
        lblerr.Text = ""
        lblError.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlProductGroup.Items.Clear()
        ddlProductGroup.Enabled = False
        ddlProduct.Items.Clear()
        ddlProduct.Enabled = False
        'ddlProductGroup.SelectedIndex = 0
        ddlPhase.SelectedIndex = 0
        ddlSession.SelectedIndex = 0
        ddlWeekDays.SelectedIndex = 0
        ddlDisplayTime.SelectedIndex = 0

        If ddlLevel.Enabled = True Then
            ddlLevel.SelectedIndex = ddlLevel.Items.IndexOf(ddlLevel.Items.FindByValue(0))
        End If
        ddlMaxCapacity.SelectedIndex = ddlMaxCapacity.Items.IndexOf(ddlMaxCapacity.Items.FindByText("20"))
        ddlPref.SelectedIndex = 0

    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        DGCoach.Visible = False
        LoadEvent(ddlEvent)

        'If ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex <> 0 Then
        'LoadProductGroup()
        'Else
        '   LoadProductGroup()
        'End If
        'If ddlVolName.SelectedIndex <> 0 Then
        '    LoadGrid()
        'End If

        LoadGrid()
        LoadMakeUpSessions()
        LoadSubstituteSessions()
    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
            lblerr.Text = "Please select Valid Product"
        Else
            LoadGrid()
            LoadMakeUpSessions()
            LoadSubstituteSessions()
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProductGroup.SelectedIndex <> 0 Then
            LoadProductID()
            LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
            '' Commented by Ferdine
            LoadGrid()
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Then
            lblerr.Text = "Please select Valid Product"
        Else
            '' Commented by Ferdine
            LoadGrid()
        End If
        LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        LoadGrid()
        LoadMakeUpSessions()
        LoadSubstituteSessions()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  I.firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  I.lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "[usp_SelectCalSignupVolunteers]", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub
    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtName.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            hdnMemberID.Value = GridMemberDt.DataKeys(index).Value
            pIndSearch.Visible = False
            lblIndSearch.Visible = False
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignup Where MemberID=" & hdnMemberID.Value & "") > 0 Then
                lblerr.Text = "The member already exists for Calendar Signup"
            End If
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIndClose.Click
        pIndSearch.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clear()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtName.Text = ""
        If ddlVolName.Items.Count > 0 Then
            ddlVolName.SelectedIndex = 0
        End If
    End Sub

    Protected Sub ddlDGVRoom_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            Dim Vroom As String
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            If (Not dr Is Nothing) Then
                ddlTemp.Items.Clear()
                Dim li As ListItem
                Dim i As Integer
                For i = 1 To 100
                    li = New ListItem(i)
                    ddlTemp.Items.Add(li)
                Next
                ddlTemp.Items.Insert(0, New ListItem("Select", 0))
                Vroom = dr.Item("VRoom").ToString()

                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(IIf(Vroom = "", "Select", dr.Item("VRoom"))))
                If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value Then
                    ddlTemp.Enabled = False
                Else
                    ddlTemp.Enabled = True
                End If
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub txtDGUserID_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim txtTemp As System.Web.UI.WebControls.TextBox
        txtTemp = sender
        txtTemp.Text = dr.Item("UserID").ToString()
        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value
            txtTemp.Enabled = False
        Else
            txtTemp.Enabled = True
        End If
    End Sub

    Protected Sub txtDGPWD_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim txtTemp As System.Web.UI.WebControls.TextBox
        txtTemp = sender
        txtTemp.Text = dr.Item("PWD").ToString()
        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value
            txtTemp.Enabled = False
        Else
            txtTemp.Enabled = True
        End If
    End Sub

    Protected Sub ddlWeekDays_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWeekDays.SelectedIndexChanged
        If ddlWeekDays.SelectedValue = "5" Or ddlWeekDays.SelectedValue = "6" Then
            LoadDisplayTime(ddlDisplayTime, False)
        Else
            LoadWeekDisplayTime(ddlDisplayTime, False)
        End If
    End Sub
    Public Sub GetHostUrlMeeting(WebExID As String, Pwd As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GethosturlMeeting"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & hdnTrainingSessionKey.Value & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingHostURLResponse(xmlReply)
        'lblMsg3.Text = result;

    End Sub

    Private Function MeetingHostURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnHostMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then


            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return sb.ToString()
    End Function
    Public Sub GenerateURL(ByVal SessionKey As String, ByVal Pwd As String, ByVal UserID As String)
        hdnTrainingSessionKey.Value = SessionKey
        GetHostUrlMeeting(UserID, Pwd)
        Dim MeetinURL As String = hdnHostMeetingURL.Value
        If MeetinURL <> "" Then
            Dim URL = MeetinURL.Replace("&amp;", "&")
            hdnWebExMeetURL.Value = URL
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", True)
        End If

    End Sub

    Public Sub LoadMakeUpSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID) where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.MemberID=" + Session("LoginID").ToString() + "  order by VC.ProductGroupCode"

            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)"

                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & " AND VC.Phase=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "")
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If

            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)

                If ds.Tables(0).Rows.Count > 0 Then
                    GrdMeeting.DataSource = ds
                    GrdMeeting.DataBind()
                    SpnMakeupTitle.Visible = False
                Else
                    GrdMeeting.DataSource = ds
                    GrdMeeting.DataBind()
                    SpnMakeupTitle.Visible = True
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub LoadSubstituteSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP.FirstName +' '+ IP.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.SubstituteMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachID) where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.MemberID=" + Session("LoginID").ToString() + "  order by VC.ProductGroupCode"

            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP.FirstName +' '+ IP.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachID) inner join calsignup cs on (cs.SubstituteMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)"

                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & " AND VC.Phase=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "")
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If

            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)

                If ds.Tables(0).Rows.Count > 0 Then
                    GrdSubstituteSessions.DataSource = ds
                    GrdSubstituteSessions.DataBind()
                    SpnSubstituteTitle.Visible = False
                Else
                    GrdSubstituteSessions.DataSource = ds
                    GrdSubstituteSessions.DataBind()
                    SpnSubstituteTitle.Visible = True
                End If
            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub GrdMeeting_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "SelectMeetingURL" Then

                row = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

                Dim selIndex As Integer = row.RowIndex
                GrdMeeting.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                'string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                'hdnWebExMeetURL.Value = MeetingURl;
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty

                WebExID = GrdMeeting.Rows(selIndex).Cells(21).Text

                WebExPwd = GrdMeeting.Rows(selIndex).Cells(22).Text
                sessionKey = GrdMeeting.Rows(selIndex).Cells(10).Text
                hdnSessionKey.Value = sessionKey
                GenerateURL(sessionKey, WebExPwd, WebExID)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Protected Sub GrdSubstituteSessions_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "SelectMeetingURL" Then

                row = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

                Dim selIndex As Integer = row.RowIndex
                GrdSubstituteSessions.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                'string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                'hdnWebExMeetURL.Value = MeetingURl;
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty

                WebExID = GrdSubstituteSessions.Rows(selIndex).Cells(22).Text

                WebExPwd = GrdSubstituteSessions.Rows(selIndex).Cells(23).Text
                sessionKey = GrdSubstituteSessions.Rows(selIndex).Cells(11).Text
                hdnSessionKey.Value = sessionKey
                GenerateURL(sessionKey, WebExPwd, WebExID)
            End If

        Catch ex As Exception
        End Try
    End Sub


    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim ds As DataSet
        Dim StrSql As String = ""
        If Session("RoleID") = "88" Then
            StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y') as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc"
        Else
            StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y') as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue <> "Select Product Group", " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
            StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Phase=" & ddlPhase.SelectedValue

            If Session("RoleId").ToString() <> "88" Then
                StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")
            End If

            If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
            End If
            StrSql = StrSql & " group by C.MeetingKey,C.MeetingPwd, C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc"
        End If
        'Response.Write(StrSql)
        If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
            Session("volDataSet") = ds
        End If
        Dim dt As DataTable = ds.Tables(0)
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        Dim FileName As String = "CalendarSignUp_" & Now.ToShortDateString & ".xls"

        oSheet.Range("A1:X1").MergeCells = True
        oSheet.Range("A1").Value = "Calendar SignUp"
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True
         
        oSheet.Range("A2").Value = "Ser#"
        oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A2").Font.Bold = True

        oSheet.Range("B2").Value = "SignUp ID"
        oSheet.Range("B2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("b2").Font.Bold = True

        oSheet.Range("C2").Value = "Volunteer Name"
        oSheet.Range("C2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("C2").Font.Bold = True

        oSheet.Range("D2").Value = "Event Year"
        oSheet.Range("D2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("D2").Font.Bold = True

        oSheet.Range("E2").Value = "EventCode"
        oSheet.Range("E2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("E2").Font.Bold = True

        oSheet.Range("F2").Value = "Phase"
        oSheet.Range("F2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("F2").Font.Bold = True

        oSheet.Range("G2").Value = "ProductGroupCode"
        oSheet.Range("G2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("G2").Font.Bold = True

        oSheet.Range("H2").Value = "Product"
        oSheet.Range("H2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("H2").Font.Bold = True

        oSheet.Range("I2").Value = "Level"
        oSheet.Range("I2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("I2").Font.Bold = True

        oSheet.Range("J2").Value = "Session"
        oSheet.Range("J2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("J2").Font.Bold = True

        oSheet.Range("K2").Value = "Day"
        oSheet.Range("K2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("K2").Font.Bold = True

        oSheet.Range("L2").Value = "Time"
        oSheet.Range("L2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("L2").Font.Bold = True

        oSheet.Range("M2").Value = "Accepted"
        oSheet.Range("M2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("M2").Font.Bold = True

        oSheet.Range("N2").Value = "Preferences"
        oSheet.Range("N2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("N2").Font.Bold = True

        oSheet.Range("O2").Value = "Max Cap"
        oSheet.Range("O2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("O2").Font.Bold = True

        oSheet.Range("P2").Value = "#Of Students Approved"
        oSheet.Range("P2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("P2").Font.Bold = True

        oSheet.Range("Q2").Value = "VRoom"
        oSheet.Range("Q2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("Q2").Font.Bold = True

        oSheet.Range("R2").Value = "UserID"
        oSheet.Range("R2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("R2").Font.Bold = True

        oSheet.Range("S2").Value = "Password"
        oSheet.Range("S2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("S2").Font.Bold = True

        oSheet.Range("T2").Value = "Years"
        oSheet.Range("T2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("T2").Font.Bold = True

        oSheet.Range("U2").Value = "Sessions"
        oSheet.Range("U2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("U2").Font.Bold = True

        oSheet.Range("V2").Value = "SessionKey"
        oSheet.Range("V2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("V2").Font.Bold = True

        oSheet.Range("W2").Value = "Meeting Pwd"
        oSheet.Range("W2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("W2").Font.Bold = True

        oSheet.Range("X2").Value = "Meeting UR"
        oSheet.Range("X2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("X2").Font.Bold = True

        'dsPreYrFamilies Previous AND current year -Cur families
        Dim iRowIndex As Integer = 3, j As Integer
        Dim CRange As IRange
        For j = 0 To dt.Rows.Count - 1
            Dim dr As DataRow = dt.Rows(j)
            oSheet.Range("A" & Trim(Str(iRowIndex))).Value = j + 1
            oSheet.Range("B" & Trim(Str(iRowIndex))).Value = dr("SignUpID")
            oSheet.Range("C" & Trim(Str(iRowIndex))).Value = dr("Name")
            oSheet.Range("D" & Trim(Str(iRowIndex))).Value = dr("EventYear")
            oSheet.Range("E" & Trim(Str(iRowIndex))).Value = dr("EventCode")
            oSheet.Range("F" & Trim(Str(iRowIndex))).Value = dr("Phase")
            oSheet.Range("G" & Trim(Str(iRowIndex))).Value = dr("ProductGroupCode")
            oSheet.Range("H" & Trim(Str(iRowIndex))).Value = dr("ProductCode")
            oSheet.Range("I" & Trim(Str(iRowIndex))).Value = dr("Level")
            oSheet.Range("J" & Trim(Str(iRowIndex))).Value = dr("SessionNo")
            oSheet.Range("K" & Trim(Str(iRowIndex))).Value = dr("Day")
            oSheet.Range("L" & Trim(Str(iRowIndex))).Value = dr("Time")
            oSheet.Range("M" & Trim(Str(iRowIndex))).Value = dr("Accepted")

            oSheet.Range("N" & Trim(Str(iRowIndex))).Value = dr("Preference")
            oSheet.Range("O" & Trim(Str(iRowIndex))).Value = dr("MaxCapacity")
            oSheet.Range("P" & Trim(Str(iRowIndex))).Value = dr("NStudents")
            oSheet.Range("Q" & Trim(Str(iRowIndex))).Value = dr("VRoom")

            oSheet.Range("R" & Trim(Str(iRowIndex))).Value = dr("UserID")
            oSheet.Range("S" & Trim(Str(iRowIndex))).Value = dr("PWD")
            oSheet.Range("T" & Trim(Str(iRowIndex))).Value = dr("Years")
            oSheet.Range("U" & Trim(Str(iRowIndex))).Value = dr("Sessions")

            oSheet.Range("V" & Trim(Str(iRowIndex))).Value = dr("MeetingKey")
            oSheet.Range("W" & Trim(Str(iRowIndex))).Value = dr("MeetingPwd")
            oSheet.Range("X" & Trim(Str(iRowIndex))).Value = dr("HostJoinURL").ToString().Substring(0, Math.Min(20, dr("HostJoinURL").ToString().Length))
            iRowIndex = iRowIndex + 1
        Next
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()
    End Sub
    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Me.DGCoach_UpdateCommand(Me.DGCoach, New DataGridCommandEventArgs(Me.DGCoach.Items(Me.DGCoach.EditItemIndex), Me.DGCoach, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))

    End Sub

    Protected Sub btnCancelGrid_Click(sender As Object, e As EventArgs) Handles btnCancelGrid.Click
        hdInAppr.Value = ""
        hdToAppvId.Value = ""
        DGCoach_CancelCommand(Me.DGCoach, New DataGridCommandEventArgs(Me.DGCoach.Items(Me.DGCoach.EditItemIndex), Me.DGCoach, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))
    End Sub
End Class
