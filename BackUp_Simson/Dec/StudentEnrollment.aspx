﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StudentEnrollment.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="StudentEnrollment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>
        <script language="javascript" type="text/javascript">

            function JoinMeeting() {

                var url = document.getElementById("<%=hdnWebExMeetURL.ClientID%>").value;

                window.open(url, '_blank');

            }
            function StartMeeting() {

                JoinMeeting();

            }

            function startChildMeeting() {
                var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
                $("#ancClick").target = "_blank";
                $("#ancClick").attr("href", url);
                $("#ancClick").attr("target", "_blank");
                document.getElementById("ancClick").click();

            }
            function joinChildMeeting() {
                startChildMeeting();
            }
            function showAlert(prdCode, coachName) {
                var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
                var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
                var dueMins = parseInt(startMins);
                var msg = "";
                if (dueMins > 0) {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
                }
                else if (dueMins >= -15) {
                    msg = "The coach has not started the class.";
                } else if (dueMins < -15) {
                    msg = "The coach has either not started or may have cancelled the class.";
                } else {
                    msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins from the current time of Central Standard Time and/or " + coachName + " has not started the class.";
                }
                //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
                alert(msg);
            }
        </script>
    </div>
    <a id="ancClick" target="_blank" href=""></a>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Student Enrollment
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table align="center" style="width: auto;">
        <tr>

            <td align="left" nowrap="nowrap" style="font-weight: bold;">Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 140px;">
                <asp:DropDownList ID="ddlYear" runat="server" Width="100px">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                    <asp:ListItem Value="2014">2014</asp:ListItem>
                    <asp:ListItem Value="2013">2013</asp:ListItem>
                    <asp:ListItem Value="2012">2012</asp:ListItem>
                    <asp:ListItem Value="2011">2011</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Type Of Report&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="ddlCoachType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCoachType_SelectedIndexChanged">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">By Coach By Product</asp:ListItem>
                    <asp:ListItem Value="2">By Coach</asp:ListItem>

                </asp:DropDownList>
            </td>
            <td id="tdProductGroupTitle" runat="server" visible="false" align="left" nowrap="nowrap" style="font-weight: bold;">Product Group&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td id="tdproductGroup" runat="server" visible="false" align="left">
                <asp:DropDownList ID="DDlProductGroup" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DDlProductGroup_SelectedIndexChanged" Width="120">
                </asp:DropDownList>
            </td>
            <td id="tdProductTitle" runat="server" visible="false" align="left" nowrap="nowrap" style="font-weight: bold;">Product&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td id="tdProduct" runat="server" visible="false" align="left">
                <asp:DropDownList ID="DDLProduct" runat="server" Width="100">
                </asp:DropDownList>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsubmit" runat="server"
                    Text="Submit" OnClick="btnsubmit_Click" />
            </td>


        </tr>

    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="LblMsg" runat="server" ForeColor="Red"></asp:Label>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <center>
            <span id="spnMemberTitle" style="font-family: Trebuchet MS; font-weight: bold;" runat="server"></span>
        </center>
        <div align="left">
            <asp:Button ID="BtnExportToExcel" runat="server" OnClick="BtnExportToExcel_Click" Visible="false" Text="Export To Excel" />
            <asp:Button ID="BtnExportExcelAll" runat="server" OnClick="BtnExportExcelAll_Click" Visible="false" Text="Export To Excel All" />
        </div>
        <div style="clear: both;"></div>
        <center>

            <asp:GridView ID="GrdCoachSignUp" runat="server" AutoGenerateColumns="false" EnableViewState="true" Style="margin-left: auto; margin-right: auto; margin-top: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdCoachSignUp_RowCommand" OnPageIndexChanging="GrdCoachSignUp_PageIndexChanging" AllowPaging="true" PageSize="50" Width="100%">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="LblSelect" runat="server" Text="Select" CommandName="Select"></asp:LinkButton>

                            <div style="display: none;">
                                <asp:Label runat="server" ID="lblHdnDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPhase" Text='<%#DataBinder.Eval(Container.DataItem,"Phs") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnLevel" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPreference" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnUserID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblhdnAutoMemberID" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblSessionKey" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Member ID" SortExpression="MemberID">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSignUpID" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Coach Name" SortExpression="Name">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVolunteerName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" SortExpression="Email">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVolunteerEmail" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Year" SortExpression="EventYear">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Code" SortExpression="EventCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventCode" Text='<%#DataBinder.Eval(Container.DataItem,"EventCode") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phase" SortExpression="Phase">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPhase" Text='<%#DataBinder.Eval(Container.DataItem,"Phase") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ProductGroup" SortExpression="ProductGroupCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPgCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" SortExpression="ProductCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level" SortExpression="Level">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblLevel" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Session" SortExpression="SessionNo">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Day" SortExpression="Day">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time" SortExpression="Time">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accepted" SortExpression="Accepted">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="#Of Students" SortExpression="Preference">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPreferences" Text='<%#DataBinder.Eval(Container.DataItem,"NStudents") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Cap" SortExpression="MaxCapacity">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VRoom" SortExpression="VRoom">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Meeting URL">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("HostJoinURL").ToString().Substring(0,Math.Min(20,Eval("HostJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("HostJoinURL").ToString()%>'></asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UserID" SortExpression="UserID">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblUID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Password" SortExpression="PWD">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Years" SortExpression="Years">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblYears" Text='<%#DataBinder.Eval(Container.DataItem,"Years") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sessions" SortExpression="Sessions">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSessions" Text='<%#DataBinder.Eval(Container.DataItem,"Sessions") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </center>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" id="dvChildrenList" runat="server" visible="false">
        <center>
            <span id="spnStudentGrid" style="font-family: Trebuchet MS; font-weight: bold;" runat="server" visible="false">Table 2: Students Enrolled</span>
        </center>
        <div style="clear: both;"></div>
        <div style="float: right;">
            <asp:Button ID="btnChildrenDetail" OnClick="btnChildrenDetail_Click" runat="server" Visible="false" Text="Export Contacts" />
            <asp:Button ID="btnChildrenDetWebex" OnClick="btnChildrenDetWebex_Click" runat="server" Visible="false" Text="Export Webex Format" />
        </div>
        <div style="clear: both;"></div>
        <asp:GridView ID="GVCoaching" OnRowCommand="GVCoaching_RowCommand" runat="server" AutoGenerateColumns="False" Width="1200px"
            CellPadding="4" ForeColor="#333333" GridLines="None" BackColor="White" BorderWidth="3px" BorderStyle="Double"
            BorderColor="#336666">



            <AlternatingRowStyle BackColor="WhiteSmoke" Font-Size="small"></AlternatingRowStyle>
            <RowStyle BackColor="white" Wrap="False" Font-Size="Small"></RowStyle>
            <HeaderStyle BorderStyle="Solid" Font-Bold="True" BackColor="#FFFFCC" ForeColor="Black"></HeaderStyle>
            <FooterStyle BackColor="Gainsboro"></FooterStyle>

            <%--  
                        <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                        <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#FFFBD6" ForeColor="Black" Font-Bold="True"  ></HeaderStyle>
                       <AlternatingRowStyle BackColor="White" />
                       <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />--%>
            <Columns>
                <%-- <asp:BoundField HeaderText="UUID" ItemStyle-HorizontalAlign ="center" DataField="UUID" HeaderStyle-Font-Bold="true" />--%>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="ChildName" HeaderStyle-Font-Bold="true" DataField="ChildName" />
                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Email" DataField="Email" HeaderStyle-Font-Bold="true" />
                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="Parent_Name" DataField="FatherName" HeaderStyle-Font-Bold="true" />
                <%--<asp:BoundField ItemStyle-HorizontalAlign ="Left" HeaderText="JobTitle" DataField="JobTitle" HeaderStyle-Font-Bold="true"  />--%>
                <asp:BoundField HeaderText="Grade" DataField="Grade" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="Approved" DataField="approved" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="HPhone" DataField="HPhone" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="CPhone" DataField="CPhone" HeaderStyle-Font-Bold="true" />
                <%-- <asp:BoundField HeaderText="Address1" DataField="Address1" HeaderStyle-Font-Bold="true"  />--%>
                <asp:BoundField ItemStyle-HorizontalAlign="Left" HeaderText="City" DataField="City" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="State" DataField="State" HeaderStyle-Font-Bold="true" />
                <%--  <asp:BoundField HeaderText="Zip" DataField="zip" HeaderStyle-Font-Bold="true"  />--%>
                <asp:BoundField HeaderText="Product_Name" DataField="ProductName" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="CoachName" DataField="CoachName" Visible="false" />
                <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="Session" DataField="SessionNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="MaxCapacity" DataField="MaxCapacity" Visible="false" />
                <asp:BoundField HeaderText="CoachDay" DataField="Day" />
                <asp:BoundField HeaderText="Time" DataField="Time" />
                <asp:BoundField HeaderText="PaymentReference" DataField="PaymentReference" Visible="false" />
                <asp:BoundField HeaderText="PaymentDate" DataField="PaymentDate" Visible="true" DataFormatString="{0:d}" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="Status" DataField="Status" Visible="false" />
                <asp:BoundField HeaderText="ChildNumber" DataField="ChildNumber" Visible="false" />
                <asp:BoundField HeaderText="CoachRegID" DataField="CoachRegID" Visible="false" />
                <asp:BoundField HeaderText="SignUpID" DataField="SignUpID" Visible="false" />
                <asp:BoundField HeaderText="UserId" DataField="childEmail" Visible="true" HeaderStyle-Font-Bold="true" />
                <asp:BoundField HeaderText="Pwd" DataField="pwd" Visible="true" HeaderStyle-Font-Bold="true" />
                <asp:TemplateField HeaderText="meeting URL">

                    <ItemTemplate>

                        <%--  <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>--%>

                        <%--  <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length)) %></asp:HyperLink>--%>

                        <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>
                        <div style="display: none;">
                            <asp:Label ID="LblMeetingURL" runat="server" Text='<%# Eval("AttendeeJoinURL").ToString() %>'></asp:Label>
                            <asp:Label ID="LblOnlineClassEmail" runat="server" Text='<%# Eval("OnlineClassEmail").ToString() %>'></asp:Label>
                            <asp:Label ID="lblWebExID" runat="server" Text='<%# Eval("UserID").ToString() %>'></asp:Label>
                            <asp:Label ID="lblWebExPwd" runat="server" Text='<%# Eval("WebExPwd").ToString() %>'></asp:Label>
                            <asp:Label ID="lblSessionKey" runat="server" Text='<%# Eval("MeetingKey").ToString() %>'></asp:Label>
                            <asp:Label ID="lblPrdCode" runat="server" Text='<%# Eval("ProductCode").ToString() %>'></asp:Label>
                            <asp:Label ID="lblCoachname" runat="server" Text='<%# Eval("CoachName").ToString() %>'></asp:Label>
                        </div>


                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False"></PagerStyle>
        </asp:GridView>
        <div style="clear: both;"></div>
        <div runat="server" visible="false" id="dvStatus" align="center">
            <span style="color: red;">No record exists</span>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Button ID="BtnCloseTable2" Visible="false" runat="server" Text="Close Table 2" OnClick="BtnCloseTable2_Click" />
    </div>
    <input type="hidden" id="hdnCoachID" value="0" runat="server" />
    <input type="hidden" id="hdnPhase" value="0" runat="server" />
    <input type="hidden" id="hdnYear" value="0" runat="server" />
    <input type="hidden" id="hdnSession" value="0" runat="server" />
    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />
    <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
</asp:Content>
