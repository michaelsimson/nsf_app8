Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class Admin_ChangeCoaching
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        End If
        If Page.IsPostBack = False Then
            Dim CoachChange As String = "Y"
            Dim SessionCreationDate As String = Nothing
            Dim cmdText As String = String.Empty
            Dim dsCH As DataSet
            Dim dtSessionCreationDate As New DateTime()
            Dim dtTodayDate As New DateTime()
            btnLoadChild.Enabled = True
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, Convert.ToString(year))
            ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))

            If Now.Month <= 3 Then
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year - 1)))
            Else
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
            End If

            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                Loadchild(Session("CustIndID"))
                trvol.Visible = False
                RequiredFieldValidator1.EnableClientScript = False
                RequiredFieldValidator1.Enabled = False
                hlnkMainMenu.Text = "Back to Parent Functions"
                hlnkMainMenu.NavigateUrl = "../UserFunctions.aspx"
            ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("../login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then
                If Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                        'more than one 
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim i As Integer
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            If prd.Length = 0 Then
                                prd = ds.Tables(0).Rows(i)(1).ToString()
                            Else
                                prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                            End If

                            If Prdgrp.Length = 0 Then
                                Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                            Else
                                Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                            End If
                        Next
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        'LoadProductGroup()
                    Else
                        'only one
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        If ds.Tables(0).Rows.Count > 0 Then
                            prd = ds.Tables(0).Rows(0)(1).ToString()
                            Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                            lblPrd.Text = prd
                            lblPrdGrp.Text = Prdgrp
                        End If
                        'LoadProductGroup()
                    End If
                End If

            Else
                Response.Redirect("../maintest.aspx")
            End If

        End If



    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer)
        Dim SQLStr As String
        SQLStr = "select distinct CR.ChildNumber, Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS Name  "

        SQLStr = SQLStr & "  from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.EventYear=C.EventYear and CR.ProductID=C.ProductID and  CR.Level =C.Level and CR.Phase=C.Phase and CR.SessionNo=C.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        '' If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
        SQLStr = SQLStr & "where " ' C.Enddate>Getdate() and "
        '' End If
        If Session("EntryToken").ToString.ToUpper() <> "PARENT" Then
            If (Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89") Then
                If lblPrdGrp.Text <> "" Then
                    SQLStr = SQLStr & " P.ProductGroupid in (" & lblPrdGrp.Text & ") and"
                End If
                If lblPrd.Text <> "" Then
                    SQLStr = SQLStr & " P.ProductID in (" & lblPrd.Text & ") and "
                End If
            End If
        End If
        SQLStr = SQLStr & " C.Accepted='Y' and CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " and CR.PMemberId=" & ParentID & " ORDER BY CR.ChildNumber, Name "


        Dim drChild As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr)

        ddlChild.DataSource = drChild
        ddlChild.DataBind()
        LblTable1Title.Visible = True
        ddlChild.Enabled = True
        If ddlChild.Items.Count > 0 Then
            lblError.Text = ""
            trchild.Visible = True
            LoadSelectedCoaching()
            If ddlChild.Items.Count > 1 Then
                ddlChild.Enabled = True
            Else
                ddlChild.Enabled = False
            End If
        Else
            lblError.Text = "No Child Found"
        End If
    End Sub

    Private Sub LoadSelectedCoaching()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "select C.ProductGroupID,C.EventYear,C.MeetingKey,CR.CoachRegID,C.UserID,C.PWD,case when CH.Email IS NULL then I.Email else CH.Email end as Email,CR.Grade,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName,Ch.OnlineClassEmail, Case when P.CoachName is null then P.Name Else P.CoachName End as ProductName,P.ProductID,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day ,CR.AttendeeID,CR.RegisteredID, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.SessionNo,C.StartDate,C.Enddate,I.City,I.State,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID  and CR1.Phase=C1.Phase and CR1.SessionNo=C1.SessionNo and CR1.Level=C1.Level  and CR1.EventYear=C1.EventYear where CR1.Approved='Y' AND C1.Accepted='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount "
        'and ((Case when C1.ProductGroupCode not in('UV') then CR1.Level end)=C1.Level Or (Case when C1.ProductGroupCode in('UV') then CR1.Level end)Is null)'
        '**************Commented on 26-09-2013 as EndDate is removed from the Code*********************'
        'If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
        '    SQLStr = SQLStr & ",'False' As ChangeFlag"
        'Else
        '    SQLStr = SQLStr & ",'False' As ChangeFlag" ',Case WHEN C.Enddate>Getdate() Then 'False' Else 'True' END As ChangeFlag"
        'End If
        SQLStr = SQLStr & ",'False' As ChangeFlag"
        SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber and CR.ChildNumber = " & ddlChild.SelectedValue & " Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.EventYear=C.EventYear and CR.ProductID=C.ProductID and  CR.Level =C.Level and CR.Phase=C.Phase and CR.SessionNo=C.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID " '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        '' If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
        SQLStr = SQLStr & "where " ' C.Enddate>Getdate() and "
        '' End If
        If Session("EntryToken").ToString.ToUpper() <> "PARENT" Then
            If (Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89") Then
                If lblPrdGrp.Text <> "" Then
                    SQLStr = SQLStr & " P.ProductGroupid in (" & lblPrdGrp.Text & ") and"
                End If
                If lblPrd.Text <> "" Then
                    SQLStr = SQLStr & " P.ProductID in (" & lblPrd.Text & ") and "
                End If
            End If
        End If
        SQLStr = SQLStr & " C.[Begin] Is not null and C.[End] is not null and CR.Approved='Y'and C.Accepted='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " ORDER BY CR.ChildNumber,LevelID "
        Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
        dgselected.DataSource = drCoaching
        dgselected.DataBind()
        If dgselected.Items.Count > 0 Then
            lblError.Text = ""
            dgselected.Visible = True
            LblTable1Title.Visible = True
        Else
            dgselected.Visible = False
            LblTable1Title.Visible = True
            lblError.Text = "No Coaching found for selected Child"
        End If
    End Sub

    Protected Sub btnLoadChild_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadChild.Click
        ddlChild.Items.Clear()
        trchild.Visible = False
        clear()
        dgselected.DataSource = Nothing
        dgselected.DataBind()
        If txtUserId.Text.Length > 0 Then
            getMemberid()
        End If
    End Sub

    Private Sub getMemberid()
        Try
            Dim memberid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where EMAIL<>'' AND Email = '" & txtUserId.Text & "'")
            If memberid > 0 Then
                Loadchild(memberid)
            Else
                lblError.Text = "Sorry EMail Doesn't Match with our database"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgselected_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgselected.ItemDataBound
        If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
            If e.Item.DataItem("ChangeFlag") = "True" Then
                Dim btn As LinkButton = Nothing
                btn = CType(e.Item.FindControl("lbtnChange"), LinkButton)
                btn.Attributes.Add("onclick", "return confirm('Coaching End Date has already passed. Do you still want to change?');")
            End If
        End If
    End Sub

    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgselected.ItemCommand
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnChange"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim CoachRegID As Integer = CInt(e.Item.Cells(0).Text)
            Dim SignUpID As Integer = CInt(e.Item.Cells(3).Text)
            Dim ProductID As Integer = CInt(e.Item.Cells(4).Text)
            Dim ProductGroupID As Integer = CInt(e.Item.Cells(6).Text)
            Dim Year As Integer = CInt(e.Item.Cells(7).Text)
            Dim Grade As Integer = CInt(e.Item.Cells(5).Text)
            Dim Level As String = CType(e.Item.FindControl("lblLevel"), Label).Text

            HdnOnlineClassEmail.Value = CType(e.Item.FindControl("lblEmail"), Label).Text
            hdnTrainingSessionKey.Value = CType(e.Item.FindControl("LblSessionKey"), Label).Text
            e.Item.BackColor = Color.FromName("#D4D4D2")
            Dim Childname As String
            Childname = CType(e.Item.FindControl("lblChildName"), Label).Text
            Dim ChildEmail As String
            ChildEmail = CType(e.Item.FindControl("lblEmail"), Label).Text

            Dim Childnumber As Integer
            Childnumber = CInt(e.Item.Cells(1).Text)

            Dim AttendeeID As String
            AttendeeID = CType(e.Item.FindControl("lblAttendeeID"), Label).Text

            hdnAttendeename.Value = Childname
            hdnAttendeeEmail.Value = ChildEmail
            hdnMeetingAttendeeID.Value = AttendeeID

            Dim City As String
            City = CType(e.Item.FindControl("lblcity"), Label).Text
            Dim State As String
            State = CType(e.Item.FindControl("lblState"), Label).Text
            hdnCity.Value = City
            hdnState.Value = State

            Dim WebExID As String
            WebExID = CType(e.Item.FindControl("lblWebExID"), Label).Text
            Dim Pwd As String
            Pwd = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
            hdnWebExID.Value = WebExID
            hdnWebExPWD.Value = Pwd

            lblCoachRegID.Text = CoachRegID
            lblSignUPID.Text = SignUpID

            Dim cmdText As String = String.Empty
            Dim CoachChange As String = String.Empty
            Dim dsCH As DataSet
            Dim SessionCreationDate As String = String.Empty
            Dim dtSessionCreationDate As New DateTime()
            Dim dtTodayDate As New DateTime()
            'Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
            'dtTodayDate = Convert.ToDateTime(CurrDate).ToString("dd/MM/yyyy")
            Dim CurrDate As String = DateTime.Today.ToString("MM/dd/yyyy")
            dtTodayDate = Convert.ToDateTime(CurrDate).ToString("MM/dd/yyyy")
            cmdText = "select CoachChange,SessionCreationDate from WebConfControl where EventID=13 and EventYear=" & Year & " and ProductGroupID=" & ProductGroupID & ""
            dsCH = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If dsCH.Tables.Count > 0 Then
                ' Condition to check if dataset tables contains data or not
                If dsCH.Tables(0).Rows.Count > 0 Then
                    CoachChange = dsCH.Tables(0).Rows(0)("CoachChange").ToString()
                    SessionCreationDate = dsCH.Tables(0).Rows(0)("SessionCreationDate").ToString()
                    dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("MM/dd/yyyy")
                    'dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("dd/MM/yyyy")
                End If
            Else
            End If
            If (dsCH.Tables(0).Rows.Count > 0) Then
                If dtTodayDate >= dtSessionCreationDate And CoachChange.Trim = "N" Then
                    lblError.Text = "Change Coach is currently not allowed. Change cannot be executed for next one hour."
                Else
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from CalSignup where EventYear =" & ddlEventYear.SelectedValue & " and ProductID=" & ProductID & " and SignupId not in (" & SignUpID & ")") > 1 Then
                        LoadCoaching(SignUpID, ProductID, Grade, Level)
                    Else
                        lblError.Text = "No other Coach is available for the session."
                        dgCoachSelection.Visible = False
                    End If
                End If
            Else
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from CalSignup where EventYear =" & ddlEventYear.SelectedValue & " and ProductID=" & ProductID & " and SignupId not in (" & SignUpID & ")") > 1 Then
                    LoadCoaching(SignUpID, ProductID, Grade, Level)
                Else
                    lblError.Text = "No other Coach is available for the session."
                    dgCoachSelection.Visible = False

                End If

            End If

        End If
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
        'LoadProduct()
        LoadSelectedCoaching()
    End Sub

    Private Sub LoadCoaching(ByVal SignUpID As Integer, ByVal ProductID As Integer, ByVal Grade As Integer, ByVal Level As String)
        Try

            lblError.Text = ""
            Dim ParFlag As Boolean = False
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            SQLStr = "select C.EventYear,C.ProductGroupID,C.SignUpID,C.MeetingPwd,Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,EF.RegFee,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.SessionNo,C.StartDate,C.Enddate,I.City,I.State, C.MeetingKey,C.MeetingPwd,C.Status,C.UserID,C.PWD, CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,  (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID  and CR1.Phase=C1.Phase  and CR1.SessionNo=C1.SessionNo  and CR1.EventYear=C1.EventYear where CR1.Approved='Y' AND C1.Accepted='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount  from  Child Ch"

            'and ((Case when C1.ProductGroupCode not in('UV') then CR1.Level end)=C1.Level Or (Case when C1.ProductGroupCode in('UV') then CR1.Level end)Is null)'
            SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=13  and Ef.EventYear =" & ddlEventYear.SelectedValue
            SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
            SQLStr = SQLStr & " Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear and C.Accepted='Y' and P.ProductID=" & ProductID
            If ProductID = 106 Or ProductID = 107 Then 'Only for SAT
                SQLStr = SQLStr & " AND C.Level= '" & Level & "'"
            End If
            SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
            'Not needed here by ferdine
            ' ''If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Count(*) From ChildCoachLevel Where ChildNumber = " & ddlChild.SelectedValue & " and ProductID =" & ProductID) > 0 Then
            ' ''    SQLStr = SQLStr & " Inner Join ChildCoachLevel CL ON C.Level = CL.Level and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y'"
            ' ''    'CoachFlag = True
            ' ''End If

            SQLStr = SQLStr & " where ch.ChildNumber = " & ddlChild.SelectedValue '& " and " & Grade & " Between EF.GradeFrom and Ef.GradeTo "
            If (Session("EntryToken").ToString.ToUpper() = "PARENT") Or (Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" And Session("RoleId") <> "1" And Session("RoleId") <> "89") Then
                SQLStr = SQLStr & " and GETDATE() < DATEADD(dd,1,EF.ChangeCoachDL) "
                ParFlag = True
            Else
                ParFlag = False
            End If
            SQLStr = SQLStr & " and C.SignUpID not IN (" & SignUpID & ") ORDER BY LEVELID" 'and GETDATE()< C.Enddate
            'Response.Write(SQLStr)
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)

            dgCoachSelection.DataSource = drCoaching
            dgCoachSelection.DataBind()
            LblTable2Title.Visible = True
            If dgCoachSelection.Items.Count > 0 Then
                dgCoachSelection.Visible = True
            Else
                dgCoachSelection.Visible = False
                If ParFlag = True Then
                    lblError.Text = "Deadline to Change Coach for the Selected Contest has Passed."
                End If
                'lblError.Text = "No other Eligible Coaching session Available"
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub dgCoachSelection_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCoachSelection.ItemCommand
        Try

            Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnChange"), LinkButton)

            Dim SessionKey As String = String.Empty
            SessionKey = CType(e.Item.FindControl("lblMeetingKey"), Label).Text
            Dim WebExID As String
            WebExID = CType(e.Item.FindControl("lblWebExID"), Label).Text
            Dim Pwd As String
            Pwd = CType(e.Item.FindControl("lblWebExPwd"), Label).Text
            Dim MeetingPwd As String
            MeetingPwd = CType(e.Item.FindControl("lblMeetingPwd"), Label).Text


            Dim UserID As String = Nothing
            UserID = Session("LoginID").ToString()
            e.Item.BackColor = Color.FromName("#D4D4D2")
            If (Not (lbtn) Is Nothing) Then
                Dim SignUpID As Integer = CInt(e.Item.Cells(0).Text)
                Dim ProductGroupID As Integer = CInt(e.Item.Cells(1).Text)
                Dim EventYear As Integer = CInt(e.Item.Cells(2).Text)
                Dim cmdCountText As String = String.Empty
                Dim cmdCntText As String = String.Empty
                cmdCountText = "select COUNT(CR.CoachRegID) from CoachReg CR Inner JOin CalSignUp C ON CR.ProductID  = C.ProductID and CR.Level =C.Level and CR.EventYear = C.EventYear  and CR.Phase=C.Phase  and CR.SessionNo=C.SessionNo  and CR.ChildNumber = " & ddlChild.SelectedValue & " where CR.CoachRegID not in (" & lblCoachRegID.Text & ") and C.SignUpID =" & SignUpID & " and CR.Approved='Y' AND C.Accepted='Y'"
                cmdCntText = "select CASE WHEN count(CR.CoachRegID) <(select MaxCapacity from CalSignUp where SignUpID=" & SignUpID & ") THEN 'TRUE' ELSE 'FALSE' END from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level and CR.EventYear = C.EventYear and CR.Phase=C.Phase  and CR.SessionNo=C.SessionNo  where CR.Approved='Y' AND C.Accepted='Y' AND C.SignUpID=" & SignUpID & ""
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdCountText) > 0 Then ' ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
                    lblError.Text = "* You cannot select this coach, since you already have a coach for the same level you desire."
                ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdCntText) = "TRUE" Then '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)  ''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT

                    Dim CoachChange As String = "Y"
                    Dim SessionCreationDate As String = Nothing
                    Dim cmdText As String = String.Empty
                    Dim dsCH As DataSet
                    Dim dtSessionCreationDate As New DateTime()
                    Dim dtTodayDate As New DateTime()
                    'Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
                    'dtTodayDate = Convert.ToDateTime(CurrDate).ToString("dd/MM/yyyy")

                    Dim CurrDate As String = DateTime.Today.ToString("MM/dd/yyyy")
                    dtTodayDate = Convert.ToDateTime(CurrDate).ToString("MM/dd/yyyy")

                    cmdText = "select CoachChange,SessionCreationDate from WebConfControl where EventID=13 and EventYear=" & EventYear & " and ProductGroupID=" & ProductGroupID & ""
                    dsCH = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If dsCH.Tables.Count > 0 Then
                        ' Condition to check if dataset tables contains data or not
                        If dsCH.Tables(0).Rows.Count > 0 Then
                            CoachChange = dsCH.Tables(0).Rows(0)("CoachChange").ToString()
                            SessionCreationDate = dsCH.Tables(0).Rows(0)("SessionCreationDate").ToString()
                            'dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("dd/MM/yyyy")
                            dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("MM/dd/yyyy")
                        End If
                    Else
                    End If
                    If dsCH.Tables(0).Rows.Count > 0 Then
                        If dtTodayDate >= dtSessionCreationDate And CoachChange.Trim = "Y" Then
                            trMeetingPwd.Visible = False
                            trYear.Visible = True
                            trTimeZone.Visible = False
                            If SessionKey = "" Or SessionKey = "0" Then
                                If hdnMeetingAttendeeID.Value <> "" Then
                                    DelMeetingAttendee("", hdnWebExID.Value, hdnWebExPWD.Value, hdnMeetingAttendeeID.Value)
                                    If hdnMeetingStatus.Value = "Success" Then
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=null,CR.AttendeeJoinURL=null, CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")

                                        SwitchStudentAndCreateTrainingSession(UserID, SignUpID)
                                    End If
                                Else
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=null,CR.AttendeeJoinURL=null, CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")
                                    SwitchStudentAndCreateTrainingSession(UserID, SignUpID)

                                End If
                            ElseIf SessionKey <> "" And SessionKey <> "0" Then
                                If hdnMeetingAttendeeID.Value <> "" Then
                                    DelMeetingAttendee("", hdnWebExID.Value, hdnWebExPWD.Value, hdnMeetingAttendeeID.Value)
                                    If hdnMeetingStatus.Value = "Success" Then
                                        RegisterMeetingAttendee(SessionKey, WebExID, Pwd, hdnMeetingAttendeeID.Value, hdnAttendeename.Value, hdnCity.Value, hdnAttendeeEmail.Value, hdnState.Value)
                                        If hdnMeetingStatus.Value = "Success" Then
                                            GetJoinMeetingURL(SessionKey, WebExID, Pwd, "", hdnAttendeename.Value, hdnAttendeeEmail.Value, MeetingPwd)
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=" & hdnAttendeeRegisteredID.Value & ",CR.AttendeeJoinURL='" & hdnMeetingURL.Value & "', CR.AttendeeID=" & hdnMeetingAttendeeID.Value & " ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")
                                        End If
                                    Else
                                        RegisterMeetingAttendee(SessionKey, WebExID, Pwd, hdnMeetingAttendeeID.Value, hdnAttendeename.Value, hdnCity.Value, hdnAttendeeEmail.Value, hdnState.Value)
                                        If hdnMeetingStatus.Value = "Success" Then
                                            GetJoinMeetingURL(SessionKey, WebExID, Pwd, "", hdnAttendeename.Value, hdnAttendeeEmail.Value, MeetingPwd)
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=" & hdnAttendeeRegisteredID.Value & ",CR.AttendeeJoinURL='" & hdnMeetingURL.Value & "', CR.AttendeeID=" & hdnMeetingAttendeeID.Value & " ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")
                                        End If
                                    End If
                                Else
                                    RegisterMeetingAttendee(SessionKey, WebExID, Pwd, hdnMeetingAttendeeID.Value, hdnAttendeename.Value, hdnCity.Value, hdnAttendeeEmail.Value, hdnState.Value)
                                    If hdnMeetingStatus.Value = "Success" Then
                                        GetJoinMeetingURL(SessionKey, WebExID, Pwd, "", hdnAttendeename.Value, hdnAttendeeEmail.Value, MeetingPwd)
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=" & hdnAttendeeRegisteredID.Value & ",CR.AttendeeJoinURL='" & hdnMeetingURL.Value & "', CR.AttendeeID=" & hdnMeetingAttendeeID.Value & " ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")
                                    End If
                                End If
                            End If
                        ElseIf dtTodayDate >= dtSessionCreationDate And CoachChange.Trim = "N" Then
                            lblError.Text = "Change Coach is currently not allowed.  Change cannot be executed for next one hour."
                        ElseIf dtTodayDate < dtSessionCreationDate And CoachChange.Trim = "Y" Then
                            trMeetingPwd.Visible = False
                            trYear.Visible = False
                            trTimeZone.Visible = False
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=null,CR.AttendeeJoinURL=null, CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")
                        End If
                    Else
                        trMeetingPwd.Visible = False
                        trYear.Visible = False
                        trTimeZone.Visible = False
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.RegisteredID=null,CR.AttendeeJoinURL=null, CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpID & " and CR.CoachRegID = " & lblCoachRegID.Text & "")
                    End If

                    'send emails to coachs & CoachAdmin
                    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select I.Email,C.SignUpID,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.level,C.ProductID, CASE WHEN C.StartDate<GetDate() THEN 'Y' Else 'N' END as Status,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail FROM IndSpouse I INNER JOIN CalSignUp C ON I.AutoMemberID = C.MemberID  Inner JOIN Product P ON C.ProductID=P.ProductID INNER JOIN Child Ch ON Ch.ChildNumber=" & ddlChild.SelectedValue & " INNER JOIN IndSpouse I2 ON Ch.MEMBERID = I2.AutoMemberID  WHERE C.SignUpID IN(" & lblSignUPID.Text & "," & SignUpID & ")")
                    Dim SignUPID1 As Integer, ProductID As Integer, FrmEMailid, ToEMailid, FromStatus, ToStatus, MailBody, MailBody1, MailBody2, subj, ProductCode, CoachAdminEmail As String
                    MailBody = ""
                    subj = "A student is switching"
                    CoachAdminEmail = ""
                    While reader.Read()
                        SignUPID1 = reader("SignUpID")
                        ProductCode = reader("ProductCode")
                        ProductID = reader("ProductID")
                        CoachAdminEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 I.Email from IndSpouse I Inner JOIN Volunteer V ON V.MemberID=I.AutomemberID and V.RoleID=89 and v.ProductID=" & ProductID)
                        MailBody = "Dear Coach, <br><br>Note : Do not reply to the email above."
                        MailBody = MailBody & "<br><br>Student: " & reader("ChildName") & ", Parent Name: " & reader("ParentName") & ", Email: " & reader("ParentEMail") & ", " & ProductCode & ","
                        If SignUPID1.ToString() = lblSignUPID.Text Then
                            'Old Coach
                            FromStatus = reader("Status")
                            FrmEMailid = reader("Email")
                            MailBody1 = " Switching from: " & FrmEMailid & ", Level: " & reader("level") & ", CoachDay:" & reader("Day") & " , Time : " & reader("Time") & "<br> To: "
                        Else
                            'New Coach
                            ToStatus = reader("Status")
                            ToEMailid = reader("Email")
                            MailBody2 = ToEMailid & ", Level: " & reader("level") & ", CoachDay:" & reader("Day") & ", Time : " & reader("Time") & ",Switch Date : " & Now.ToString()
                        End If
                    End While
                    MailBody = MailBody & MailBody1 & MailBody2
                    If ToStatus.Trim() = "Y" Then
                        'SendEmail(subj, MailBody, ToEMailid)
                    End If
                    If FromStatus.Trim() = "Y" Then
                        ' SendEmail(subj, MailBody, FrmEMailid)
                    End If
                    If CoachAdminEmail <> "" Then
                        ' SendEmail(subj, MailBody, CoachAdminEmail)
                    End If

                    clear()
                    LoadSelectedCoaching()
                    lblError.Text = "Change was made successfully"
                Else
                    lblError.Text = "There is no more room for this coach.  Please replace with another coach."
                End If
            End If
        Catch ex As Exception
            SendEmail("Error in Change Coach", ex.ToString(), "michael.simson@capestart.com")
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim sFrom As String = "nsfcontests@northsouth.org" ' "nsfcontests@gmail.com" 'Updated on Jan 30 2015
        'Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        Dim ok As Boolean = True
        Try
            client.Send(mail)
        Catch e As Exception
            ok = False
        End Try
    End Sub
    Private Sub clear()
        dgCoachSelection.DataSource = Nothing
        dgCoachSelection.DataBind()
        lblError.Text = ""
        lblCoachRegID.Text = String.Empty
        lblSignUPID.Text = String.Empty
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        '  If ddlChild.Items.Count > 0 Then
        clear()
        btnLoadChild_Click(btnLoadChild, New EventArgs)
        'LoadSelectedCoaching()
        ' End If
    End Sub

    Protected Sub dgselected_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgselected.SelectedIndexChanged

    End Sub

    Public Sub DelMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"

        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        request.Method = "POST"
        request.ContentType = "application/x-www-form-urlencoded"
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf
        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf
        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.DelMeetingAttendee"">" & vbCr & vbLf
        strXML += "<attendeeEmail>" & vbCr & vbLf
        strXML += "<email>" & HdnOnlineClassEmail.Value & "</email>" & vbCr & vbLf
        strXML += "<sessionKey>" & hdnTrainingSessionKey.Value & "</sessionKey>" & vbCr & vbLf
        strXML += "</attendeeEmail>" & vbCr & vbLf

        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = ProcessMeetingAttendeeResponse(xmlReply)
    End Sub
    Public Sub RegisterMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, City As String, Email As String, Country As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.RegisterMeetingAttendee"">" & vbCr & vbLf

        strXML += "<attendees>" & vbCr & vbLf
        strXML += "<person>" & vbCr & vbLf
        strXML += "<name>" & Name & "</name>" & vbCr & vbLf
        strXML += "<title>title</title>" & vbCr & vbLf
        strXML += "<company>microsoft</company>" & vbCr & vbLf
        strXML += "<address>" & vbCr & vbLf
        strXML += "<addressType>PERSONAL</addressType>" & vbCr & vbLf
        strXML += "<city>" & City & "</city>" & vbCr & vbLf
        strXML += "<country>US</country>" & vbCr & vbLf
        strXML += "</address>" & vbCr & vbLf

        strXML += "<email>" & Email & "</email>" & vbCr & vbLf
        strXML += "<notes>notes</notes>" & vbCr & vbLf
        strXML += "<url>https://</url>" & vbCr & vbLf
        strXML += "<type>VISITOR</type>" & vbCr & vbLf
        strXML += "</person>" & vbCr & vbLf
        strXML += "<joinStatus>ACCEPT</joinStatus>" & vbCr & vbLf
        strXML += "<role>ATTENDEE</role>" & vbCr & vbLf

        strXML += "<sessionKey>" & sessionKey & "</sessionKey>" & vbCr & vbLf
        strXML += "</attendees>" & vbCr & vbLf


        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = RegisterMeetingAttendeeResponse(xmlReply)
    End Sub
    Public Sub GetJoinMeetingURL(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, Email As String, MeetingPassword As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf

        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GetjoinurlMeeting"">" & vbCr & vbLf

        strXML += "<sessionKey>" + sessionKey + "</sessionKey>" & vbCr & vbLf
        strXML += "<attendeeName>" + Name + "</attendeeName>" & vbCr & vbLf
        strXML += "<attendeeEmail>" + Email + "</attendeeEmail>" & vbCr & vbLf
        strXML += "<meetingPW>" + MeetingPassword + "</meetingPW>" & vbCr & vbLf
        strXML += "<RegID>" + hdnAttendeeRegisteredID.Value + "</RegID>" & vbCr & vbLf

        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingAttendeeURLResponse(xmlReply)
    End Sub
    Private Function ProcessMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
    Private Function RegisterMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"
                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText
                Dim attendeeID As String

                attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText

                hdnMeetingAttendeeID.Value = attendeeID
                hdnAttendeeRegisteredID.Value = meetingKey

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function

    Private Function MeetingAttendeeURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"
                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function

    Public Sub CreateTrainingSession(WebEXID As String, PWD As String, MeetingTitle As String, Capacity As Integer, StartDate As String, Time As String, _
  Day As String, BeginTime As String, EndTime As String, EndDate As String, Duration As String, CoachName As String, ProductCode As String)
        Dim MTitle As String = CoachName & " - " & ProductCode
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"

        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        request.Method = "POST"
        request.ContentType = "application/x-www-form-urlencoded"
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf
        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf

        strXML &= "<webExID>" & WebEXID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & PWD & "</password>" & vbCr & vbLf
        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.training.CreateTrainingSession"">" & vbCr & vbLf

        strXML &= "<accessControl>" & vbCr & vbLf
        'strXML &= "  <listing>PUBLIC</listing>\r\n";
        strXML &= "  <sessionPassword>training</sessionPassword>" & vbCr & vbLf
        strXML &= "</accessControl>" & vbCr & vbLf

        strXML &= "<schedule>" & vbCr & vbLf
        strXML &= "<startDate>" & StartDate & " " & Time & "</startDate>" & vbCr & vbLf
        strXML &= "<duration>" & Duration & "</duration>" & vbCr & vbLf

        strXML &= "     <timeZoneID>11</timeZoneID>" & vbCr & vbLf
        strXML &= "     <openTime>20</openTime>" & vbCr & vbLf
        strXML &= "</schedule>" & vbCr & vbLf

        strXML &= "<metaData>" & vbCr & vbLf
        strXML &= "<confName>" & MTitle & "</confName>" & vbCr & vbLf
        strXML &= "     <agenda>agenda 1</agenda>" & vbCr & vbLf
        strXML &= "     <description>Training</description>" & vbCr & vbLf
        strXML &= "     <greeting>greeting</greeting>" & vbCr & vbLf
        strXML &= "     <location>location</location>" & vbCr & vbLf
        strXML &= "     <invitation>invitation</invitation>" & vbCr & vbLf
        strXML &= "</metaData>" & vbCr & vbLf

        strXML &= "<repeat>"
        strXML &= "     <repeatType>MULTIPLE_SESSION</repeatType>"
        strXML &= "     <dayInWeek>"
        strXML &= "         <day>" & Day.ToUpper & "</day>"
        strXML &= "     </dayInWeek>"
        strXML &= "     <occurenceType>WEEKLY</occurenceType>"
        strXML &= "     <expirationDate>" & EndDate & " " & EndTime & "</expirationDate>"
        strXML &= "</repeat>"

        strXML &= "<attendeeOptions>" & vbCr & vbLf
        strXML &= "      <request>true</request>" & vbCr & vbLf
        strXML &= "      <registration>true</registration>" & vbCr & vbLf
        strXML &= "      <auto>true</auto>" & vbCr & vbLf
        strXML &= "      <registrationPWD>training</registrationPWD>" & vbCr & vbLf
        strXML &= "      <maxRegistrations>200</maxRegistrations>" & vbCr & vbLf
        strXML &= "      <registrationCloseDate>03/30/2016 12:00:00"
        strXML &= "      </registrationCloseDate>" & vbCr & vbLf
        strXML &= "      <emailInvitations>true</emailInvitations>" & vbCr & vbLf
        strXML &= "</attendeeOptions>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf

        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)
        request.ContentLength = byteArray.Length
        Dim dataStream As Stream = request.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As WebResponse = request.GetResponse()
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then
            xmlReply = New XmlDocument()
            xmlReply.Load(dataStream)
        End If
        Dim Result As String = Nothing
        Result = ProcessCreatedTrainingSessionsResponse(xmlReply)
    End Sub
    Private Function ProcessCreatedTrainingSessionsResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Dim status As String = Nothing
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession")
            Dim MeetingKey As String = Nothing
            status = (xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText)
            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = status

                hdnTrainingSessionKey.Value = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:sessionkey", manager).InnerText
            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = status
                hdnTrainingSessionKey.Value = ""
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        'Return sb.ToString()
        Return status.ToString()
    End Function

    Public Sub SwitchStudentAndCreateTrainingSession(UserID As String, SignUpId As String)

        Dim cmdText As String = Nothing
        cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],UserID,PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='2015' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and SignUpId=" & SignUpId & ""

        Dim ds As New DataSet()
        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

        If ds.Tables(0) IsNot Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows

                    Dim WebExID As String = dr("UserID").ToString()
                    Dim WebExPwd As String = dr("PWD").ToString()
                    Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
                    Dim ScheduleType As String = dr("ScheduleType").ToString()

                    Dim year As String = dr("EventYear").ToString()
                    Dim eventID As String = dr("EventID").ToString()

                    Dim ProductGroupID As String = dr("ProductGroupID").ToString()
                    Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
                    Dim ProductID As String = dr("ProductID").ToString()
                    Dim ProductCode As String = dr("ProductCode").ToString()
                    Dim Phase As String = dr("Phase").ToString()
                    Dim Level As String = dr("Level").ToString()
                    Dim Sessionno As String = dr("SessionNo").ToString()
                    Dim CoachID As String = dr("MemberID").ToString()
                    Dim MeetingPwd As String = "training"

                    Dim Time As String = dr("Time").ToString()
                    Dim Day As String = dr("Day").ToString()
                    Dim STime As String = dr("Begin").ToString()
                    Dim ETime As String = dr("End").ToString()

                    Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
                    Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
                    'string endTime = "01:00 AM";
                    'TimeSpan time1 = DateTime.Parse(endTime).Subtract(DateTime.Parse(Time));
                    'TimeSpan time2 = GetTimeFromString1(dr["End"].ToString());
                    'double hours = (time1 - time2).TotalHours;

                    Dim timeZoneID As String = ddlTimeZone.SelectedValue
                    Dim TimeZone As String = ddlTimeZone.SelectedItem.Text
                    SignUpId = dr("SignupID").ToString()
                    Dim Mins As Double = 0.0
                    Dim dFrom As DateTime
                    Dim dTo As DateTime

                    Dim sDateFrom As String = STime
                    Dim sDateTo As String = ETime
                    If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                        Dim TS As TimeSpan = dTo - dFrom

                        Mins = TS.TotalMinutes
                    End If
                    Dim durationHrs As String = Mins.ToString("0")
                    If durationHrs.IndexOf("-") > -1 Then
                        durationHrs = "188"
                    End If

                    If timeZoneID = "4" Then
                        TimeZone = "EST/EDT � Eastern"
                    ElseIf timeZoneID = "7" Then
                        TimeZone = "CST/CDT - Central"
                    ElseIf timeZoneID = "6" Then
                        TimeZone = "MST/MDT - Mountain"
                    End If

                    Dim CoachName As String = String.Empty

                    CoachName = dr("CoachName").ToString()
                    If dr("MeetingKey").ToString() = "" Or dr("MeetingKey").ToString() = "0" Then
                        CreateTrainingSession(WebExID, WebExPwd, ScheduleType, Capacity, startDate.Replace("-", "/"), Time, _
                            Day, STime, ETime, EndDate.Replace("-", "/"), durationHrs, CoachName, ProductCode)

                        If hdnMeetingStatus.Value = "SUCCESS" Then
                            GetHostUrlMeeting(WebExID, WebExPwd)

                            Dim meetingURL As String = hdnHostMeetingURL.Value
                            cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "','" & startDate & "','" & EndDate & "','" & STime & "','" & ETime & "'," & durationHrs & "," & timeZoneID & ",'" & TimeZone & "','" & SignUpId & "'," & CoachID & ",'" & Phase & "','" & Level & "','" & Sessionno & "','" & meetingURL & "','" & hdnTrainingSessionKey.Value & "','" & MeetingPwd & "','Active','" & Day & "','" & UserID & "','" & WebExID & "','" & WebExPwd & "'"

                            Dim objDs As New DataSet()
                            objDs = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                            If objDs IsNot Nothing AndAlso objDs.Tables.Count > 0 Then
                                If objDs.Tables(0).Rows.Count > 0 Then
                                    If Convert.ToInt32(objDs.Tables(0).Rows(0)("Retval").ToString()) > 0 Then

                                        Dim dsChild As New DataSet()
                                        Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ProductGroupID & "' and CR.ProductID='" & ProductID & "' and CR.CMemberID=" & CoachID & " and CR.EventYear=" & ddlEventYear.SelectedValue & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & ddlEventYear.SelectedValue & " and ProductGroupID='" & ProductGroupID & "' and ProductID='" & ProductID & "' and CMemberID=" & CoachID & " and Approved='Y' and Level = '" & Level & "' and SessionNo=" & Sessionno & ") and CR.Level='" & Level & "' and CR.SessionNo=" & Sessionno & ""
                                        dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
                                        Dim ChidName As String = String.Empty
                                        Dim Email As String = String.Empty
                                        Dim City As String = String.Empty
                                        Dim Country As String = String.Empty
                                        Dim ChildNumber As String = String.Empty
                                        Dim CoachRegID As String = String.Empty

                                        If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                                            If dsChild.Tables(0).Rows.Count > 0 Then
                                                For Each drChild As DataRow In dsChild.Tables(0).Rows
                                                    ChidName = drChild("Name").ToString()
                                                    Email = drChild("Email").ToString()
                                                    City = drChild("City").ToString()
                                                    Country = drChild("Country").ToString()
                                                    ChildNumber = drChild("ChildNumber").ToString()
                                                    CoachRegID = drChild("CoachRegID").ToString()

                                                    RegisterMeetingAttendee(hdnTrainingSessionKey.Value, WebExID, WebExPwd, "", ChidName, City, Email, Country)
                                                    If hdnMeetingStatus.Value = "Success" Then

                                                        GetJoinMeetingURL(hdnTrainingSessionKey.Value, WebExID, WebExPwd, hdnMeetingAttendeeID.Value, ChidName, Email, MeetingPwd)
                                                        Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', RegisteredID=" & hdnAttendeeRegisteredID.Value & ", AttendeeID=" & hdnMeetingAttendeeID.Value & ", Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where CoachRegID='" & CoachRegID & "'"
                                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                                                    End If
                                                Next
                                            End If
                                        End If
                                    Else

                                    End If
                                End If
                            End If


                        Else
                        End If
                    Else

                    End If
                Next

            End If
        End If
        txtMeetingPwd.Text = ""
    End Sub
    Public Sub GetHostUrlMeeting(WebExID As String, Pwd As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GethosturlMeeting"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & hdnTrainingSessionKey.Value & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingHostURLResponse(xmlReply)
        'lblMsg3.Text = result;

    End Sub

    Private Function MeetingHostURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"
                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnHostMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return sb.ToString()
    End Function
End Class
