﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.IO;


using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;



public partial class StudentEnrollment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
    }

    public void fillProductGroup()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            cmdText = "select ProductGroupId,Name from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + year + ")";

            //cmdText = "select ProductGroupID,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            DDlProductGroup.DataSource = ds;
            DDlProductGroup.DataTextField = "Name";
            DDlProductGroup.DataValueField = "ProductGroupId";
            DDlProductGroup.DataBind();
            DDlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
        }
        catch (Exception ex)
        {
        }
    }
    public void fillProduct()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            //year = 2014;
            cmdText = "select ProductId,Name from Product where ProductGroupId=" + DDlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + DDlProductGroup.SelectedItem.Value + " and EventYear=" + year + ")";

            //cmdText = "select ProductID,ProductCode from Product where ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            DDLProduct.DataSource = ds;
            DDLProduct.DataTextField = "Name";
            DDLProduct.DataValueField = "ProductId";
            DDLProduct.DataBind();
            DDLProduct.Items.Insert(0, new ListItem("Select", "0"));
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlCoachType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCoachType.SelectedValue == "1")
        {
            fillProductGroup();
            tdProductGroupTitle.Visible = true;
            tdproductGroup.Visible = true;
            tdProductTitle.Visible = true;
            tdProduct.Visible = true;
        }
        else
        {
            tdProductGroupTitle.Visible = false;
            tdproductGroup.Visible = false;
            tdProductTitle.Visible = false;
            tdProduct.Visible = false;
        }
    }
    protected void DDlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
    }


    public void LoadCoachCalendarSignup()
    {
        try
        {
            int CurrentYear = DateTime.Now.Year;
            string Year = ddlYear.SelectedValue;
            string cmdText = string.Empty;
            DataSet ds = new DataSet();
            string FromVal = string.Empty;


            if (Session["RoleID"].ToString() == "88")
            {
                cmdText = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email,C.HostJoinURL,C.MeetingKey, C.EventYear,C.EventID, C.EventCode,c.Phase as Phs,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 and C.MemberID=" + Session["LoginID"].ToString() + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.HostJoinURL,C.MeetingKey,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
            }
            else if (Session["RoleID"].ToString() != "89")
            {
                if (ddlCoachType.SelectedValue == "1")
                {

                    if (DDlProductGroup.SelectedValue != "0" && (DDLProduct.SelectedValue == "0" || DDLProduct.SelectedValue == ""))
                    {
                        cmdText = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email,C.HostJoinURL,C.MeetingKey, C.EventYear,C.EventID,c.Phase as Phs, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 and C.ProductGroupID=" + DDlProductGroup.SelectedValue + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.MeetingKey,C.HostJoinURL,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
                    }
                    else if (DDLProduct.SelectedValue != "0")
                    {
                        cmdText = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email,C.HostJoinURL,C.MeetingKey, C.EventYear,C.EventID,c.Phase as Phs, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 and C.ProductGroupID=" + DDlProductGroup.SelectedValue + " and C.ProductID=" + DDLProduct.SelectedValue + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.HostJoinURL,C.MeetingKey,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
                    }
                    spnMemberTitle.InnerText = "Table 1: " + DDLProduct.SelectedItem.Text + "";
                }
                else
                {
                    spnMemberTitle.InnerText = "Table 1: List of Coaches";
                    cmdText = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email,C.HostJoinURL,C.MeetingKey, C.EventYear,C.EventID,c.Phase as Phs, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as NStudents  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.HostJoinURL,C.MeetingKey,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
                }
            }



            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdCoachSignUp.DataSource = ds;
                    GrdCoachSignUp.DataBind();

                    BtnExportToExcel.Visible = true;
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96")
                    {
                        BtnExportExcelAll.Visible = true;
                    }
                    else
                    {
                        BtnExportExcelAll.Visible = false;
                    }

                }
                else
                {
                    GrdCoachSignUp.DataSource = ds;
                    GrdCoachSignUp.DataBind();
                    BtnExportToExcel.Visible = false;
                    BtnExportExcelAll.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void ExportToExcelCoach()
    {
        try
        {
            int CurrentYear = DateTime.Now.Year;
            string Year = ddlYear.SelectedValue;
            string cmdText = string.Empty;
            DataSet ds = new DataSet();
            string FromVal = string.Empty;
            string tableName = string.Empty;


            if (Session["RoleID"].ToString() == "88")
            {
                cmdText = "SELECT C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as '#Of Students', C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 and C.MemberID=" + Session["LoginID"].ToString() + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
            }
            else if (Session["RoleID"].ToString() != "89")
            {
                if (ddlCoachType.SelectedValue == "1")
                {

                    if (DDlProductGroup.SelectedValue != "0" && (DDLProduct.SelectedValue == "0" || DDLProduct.SelectedValue == ""))
                    {
                        cmdText = "SELECT C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as '#Of Students' , C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 and C.ProductGroupID=" + DDlProductGroup.SelectedValue + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
                    }
                    else if (DDLProduct.SelectedValue != "0")
                    {
                        cmdText = "SELECT C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as '#Of Students', C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 and C.ProductGroupID=" + DDlProductGroup.SelectedValue + " and C.ProductID=" + DDLProduct.SelectedValue + " group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
                    }
                    tableName = DDLProduct.SelectedItem.Text;
                }
                else
                {
                    spnMemberTitle.InnerText = "Table 1: List of Coaches";
                    cmdText = "SELECT C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and SessionNo=C.SessionNo) as '#Of Students', C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + Year + " and C.Accepted='Y' AND C.EventID=13 group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD order by I.LastName, I.FirstName Asc";
                    tableName = "List of Coaches";
                }
            }



            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ds.Tables[0].Rows[i]["Ser#"] = i + 1;

                    }
                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;
                    ds.Tables[0].TableName = tableName;

                    string filename = "StudentEnrollment_CoachList_" + monthDay + "_" + year + ".xls";
                    ExcelHelper.ToExcel(ds, filename, Page.Response);
                }
                else
                {

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void BtnExportToExcel_Click(object sender, EventArgs e)
    {
        ExportToExcelCoach();
    }
    protected void GrdCoachSignUp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = null;
        if (e.CommandName == "Select")
        {
            row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

            int selIndex = row.RowIndex;
            GrdCoachSignUp.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

            string CoachID = string.Empty;
            string Year = string.Empty;
            string Phase = string.Empty;
            string Session = string.Empty;
            string ProductGroupID = string.Empty;
            string ProductID = string.Empty;

            CoachID = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblhdnAutoMemberID") as Label).Text;
            Year = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblHdnEventYear") as Label).Text;
            Phase = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblHdnPhase") as Label).Text;
            Session = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblHdnSession") as Label).Text;
            ProductGroupID = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
            ProductID = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblProductID") as Label).Text;
            hdnCoachID.Value = CoachID;
            hdnSession.Value = Session;
            hdnYear.Value = Year;
            hdnPhase.Value = Phase;
            hdnProductGroupID.Value = ProductGroupID;
            hdnProductID.Value = ProductID;

            string cmdText = string.Empty;
            DataSet ds = new DataSet();

            cmdText = "select ROW_NUMBER() Over (ORDER BY P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, I1.Email as Email ,I1.Career as JobTitle,Ch.Email as ChildEmail,Ch.OnlineClassEmail,ch.pwd,CR.Approved,CR.SessionNo, CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName,Ch.FIRST_NAME, Ch.LAST_NAME ,CR.Grade, I1.FirstName +' '+ I1.LastName as FatherName,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,AttendeeJoinURL,C.MeetingKey, C.UserID, C.Pwd as WebExPwd  ";

            cmdText = cmdText + " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON";

            cmdText = cmdText + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" + Year + " where  CR.CMemberid=" + CoachID + "";

            cmdText = cmdText + " AND CR.Phase=" + Phase + " and C.SessionNo=" + Session + " AND CR.Approved='Y' and C.Accepted='Y' and CR.ProductGroupID=" + ProductGroupID + " and CR.ProductID=" + ProductID + "   ORDER BY CR.ProductGroupID,CR.ProductID,Ch.LAST_NAME,Ch.FIRST_NAME ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GVCoaching.DataSource = ds;
                    GVCoaching.DataBind();
                    dvChildrenList.Visible = true;
                    spnStudentGrid.Visible = true;
                    dvStatus.Visible = false;
                    btnChildrenDetail.Visible = true;
                    btnChildrenDetWebex.Visible = true;
                    BtnCloseTable2.Visible = true;
                }
                else
                {
                    GVCoaching.DataSource = ds;
                    GVCoaching.DataBind();
                    dvChildrenList.Visible = true;
                    spnStudentGrid.Visible = true;
                    dvStatus.Visible = true;
                    btnChildrenDetail.Visible = false;
                    btnChildrenDetWebex.Visible = false;
                    BtnCloseTable2.Visible = false;
                }
            }

        }
        else if (e.CommandName == "SelectMeetingURL")
        {
            row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

            int selIndex = row.RowIndex;
            GrdCoachSignUp.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
            //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
            //hdnWebExMeetURL.Value = MeetingURl;
            string WebExID = string.Empty;
            string WebExPwd = string.Empty;
            string sessionKey = string.Empty;
            WebExID = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblUID") as Label).Text;
            WebExPwd = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblPWD") as Label).Text;
            sessionKey = ((Label)GrdCoachSignUp.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
            hdnSessionKey.Value = sessionKey;
            GetHostUrlMeeting(WebExID, WebExPwd);
            if (hdnMeetingStatus.Value == "SUCCESS")
            {
                string MeetingURl = hdnHostURL.Value;
                string URL = MeetingURl.Replace("&amp;", "&");
                hdnWebExMeetURL.Value = URL;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
            }
            else
            {

                LblMsg.Text = hdnMeetingStatus.Value;
            }
        }

    }

    public void GetHostUrlMeeting(string WebExID, string Pwd)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "<email>webex.nsf.adm@gmail.com</email>";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GethosturlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        //strXML += "<meetingPWD>" + hdnMeetingPwd.Value + "</meetingPWD>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = ProcessMeetingHostJoinURLResponse(xmlReply);
        //lblMsg3.Text = result;

    }
    private string ProcessMeetingHostJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            hdnMeetingStatus.Value = status;
            if (status == "SUCCESS")
            {
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml;


                //hdnHostURL.Value = meetingKey;
                string URL = meetingKey.Replace("&amp;", "&");
                hdnHostURL.Value = URL;

                //hdnSessionKey.Value = meetingKey;
                //sb.Append("<a href=" + meetingKey + " target='blank'>" + meetingKey + "</a></br>");
                sb.Append("<a href=" + URL + " target='blank'>" + URL + "</a></br>");

                //sb.Append("<a href=" + meetingKey2 + " target='blank'>" + meetingKey2 + "</a></br>");
                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {

                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (ValidateCoachByProduct() == "1")
        {
            LoadCoachCalendarSignup();
        }
        else
        {
            ValidateCoachByProduct();
        }
    }

    public string ValidateCoachByProduct()
    {
        string Retval = "1";
        if (ddlYear.SelectedValue == "0")
        {

            LblMsg.Text = "Please select Year";
            Retval = "-1";
        }

        else if (ddlCoachType.SelectedValue == "0")
        {
            LblMsg.Text = "Please select Type Of Report";
            Retval = "-1";
        }
        else if (ddlCoachType.SelectedValue == "1")
        {
            if (DDlProductGroup.SelectedValue == "0")
            {
                LblMsg.Text = "Please select Product Group";
                Retval = "-1";
            }

            else if (DDLProduct.SelectedValue == "0")
            {
                LblMsg.Text = "Please select Product";
                Retval = "-1";
            }
        }
        return Retval;
    }

    protected void GrdCoachSignUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GrdCoachSignUp.PageIndex = e.NewPageIndex;
            LoadCoachCalendarSignup();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }

    public void ExportToExcel()
    {
        string cmdText = string.Empty;
        DataSet ds = new DataSet();

        cmdText = "select ROW_NUMBER() Over (ORDER BY P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, I1.Email as Email,I1.FirstName +' '+ I1.LastName as ParentName,CR.Grade,CR.Approved,I1.HPhone,I1.CPhone,I1.City,I1.State,Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,C.Level,CR.SessionNo,C.Day as Day,CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,CR.PaymentDate,Ch.Email as ChildEmail,ch.pwd ";


        cmdText = cmdText + " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON";

        cmdText = cmdText + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" + hdnYear.Value + " where  CR.CMemberid=" + hdnCoachID.Value + "";

        cmdText = cmdText + " AND CR.Phase=" + hdnPhase.Value + " and C.SessionNo=" + hdnSession.Value + " AND CR.Approved='Y' and C.Accepted='Y'   ORDER BY Ch.LAST_NAME,Ch.FIRST_NAME ASC";

        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;
                ds.Tables[0].TableName = "StudentsEnrolled";
                string filename = "StudentsEnrolled_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }
    }

    protected void btnChildrenDetail_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    protected void BtnCloseTable2_Click(object sender, EventArgs e)
    {
        dvChildrenList.Visible = false;
        BtnCloseTable2.Visible = false;
    }
    protected void btnChildrenDetWebex_Click(object sender, EventArgs e)
    {
        ExportWebExFormat();
    }

    public void ExportWebExFormat()
    {
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;
        string filename = "CoachingChildrenDetails_Webex_" + monthDay + "_" + year + ".xls";
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
        Response.Charset = "";

        Response.ContentType = "application/vnd.xls";
        StringWriter stringWrite = new StringWriter();
        HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        Response.Write("<br>Contacts<br>");

        string SQLStr = string.Empty;

        DataSet ds = new DataSet();

        SQLStr = "Select Distinct '' as UUID,Ch.LAST_NAME, Ch.FIRST_NAME,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS Name,I1.Email as Email,'' as Company,'' as JobTitle,'' as URL,1 as OffCntry,'' as OffLocal,'' as CellCntry,'' as CellLocal,'' as	FaxCntry,'' as	FaxLocal,'' as	Address1,'' as	Address2,'' as	City,'' as	State_Or_Province,'' as Zip_Or_Postal,'' as Country,'' as	TimeZone,'' as	Language,'' as Locale,'' as UserName,'' as	Notes";
        SQLStr = SQLStr + " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON ";

        SQLStr = SQLStr + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" + hdnYear.Value + " where  CR.CMemberid=" + hdnCoachID.Value + "";
        SQLStr = SQLStr + " AND CR.SessionNo= " + hdnSession.Value + "";
        SQLStr = SQLStr + " AND CR.Phase=" + hdnPhase.Value + " AND CR.Approved='Y' and C.Accepted='Y'";

        SQLStr = SQLStr + " UNION ALL ";

        SQLStr = SQLStr + "select  Distinct '' as UUID,Ch.LAST_NAME, Ch.FIRST_NAME,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS Name,Ch.Email as Email,'' as Company,'' as JobTitle,'' as URL,1 as OffCntry,'' as OffLocal,'' as CellCntry,'' as CellLocal,'' as	FaxCntry,'' as	FaxLocal,'' as	Address1,'' as	Address2,'' as	City,'' as	State_Or_Province,'' as Zip_Or_Postal,'' as Country,'' as	TimeZone,'' as	Language,'' as Locale,'' as UserName,'' as	Notes";
        SQLStr = SQLStr + " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON ";
        SQLStr = SQLStr + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" + hdnYear.Value + " where  CR.CMemberid=" + hdnCoachID.Value + "";
        SQLStr = SQLStr + " AND CR.SessionNo= " + hdnSession.Value + "";
        SQLStr = SQLStr + " AND CR.Phase=" + hdnPhase.Value + " AND CR.Approved='Y' and C.Accepted='Y' and Ch.Email is not null ORDER BY LAST_NAME, FIRST_NAME";
        GridView GVCoachingExp = new GridView();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ds.Tables[0].Columns.RemoveAt(2);
                ds.Tables[0].Columns.RemoveAt(1);
                GVCoachingExp.DataSource = ds;
                GVCoachingExp.DataBind();

            }
        }

        string SQLStrList = string.Empty;
        SQLStrList = " select '' as DUID ,ROW_NUMBER() Over (ORDER BY CR.ProductCode, Ch.LAST_NAME,Ch.FIRST_NAME,Ch.ChildNumber) as SNo,I1.Email  as Email, ISNull(Ch.Email,'') as ChildEmail,Ch.FIRST_NAME + ' ' + Ch.Last_Name as ChildName,CR.ProductCode,Ch.LAST_NAME,ch.FIRST_NAME,Ch.ChildNumber,I.FirstName +' ' + (Select top 1 Name from Product where ProductID =CR.ProductID)+ ' ' + CR.Level + ' ' + 'List'  as ListName,I.FirstName +' ' + (Select top 1 Name from Product where ProductID =CR.ProductID)+ ' ' + CR.Level+ ' ' + 'List'   as ListDescription,'' as Members ";
        SQLStrList = SQLStrList + " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON ";

        SQLStrList = SQLStrList + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" + hdnYear.Value + " where  CR.CMemberid=" + hdnCoachID.Value + " ";
        SQLStrList = SQLStrList + " AND CR.SessionNo= " + hdnSession.Value + " ";

        SQLStrList = SQLStrList + " AND CR.Phase=" + hdnPhase.Value + " AND CR.Approved='Y' and C.Accepted='Y'  ORDER BY ProductCode, LAST_NAME,FIRST_NAME,ChildNumber ";

        DataSet dsList = new DataSet();
        dsList = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStrList);

        string StrList = string.Empty;
        string ListName = string.Empty;
        string ListDescription = string.Empty;
        if (null != dsList && dsList.Tables.Count > 0)
        {
            if (dsList.Tables[0].Rows.Count > 0)
            {
                ListName = dsList.Tables[0].Rows[0]["ListName"].ToString();
                ListDescription = dsList.Tables[0].Rows[0]["ListDescription"].ToString();
                for (int i = 0; i < dsList.Tables[0].Rows.Count; i++)
                {
                    StrList = StrList + dsList.Tables[0].Rows[i]["ChildName"] + "(" + dsList.Tables[0].Rows[i]["Email"] + ");";
                    if (dsList.Tables[0].Rows[0]["ChildEmail"].ToString() != "")
                    {
                        StrList = StrList + dsList.Tables[0].Rows[i]["ChildName"] + "(" + dsList.Tables[0].Rows[i]["ChildEmail"] + ");";
                    }

                }
            }

        }

        dsList.Tables[0].Rows[0]["ListName"] = ListName;
        dsList.Tables[0].Rows[0]["ListDescription"] = ListDescription;
        dsList.Tables[0].Rows[0]["Members"] = StrList;

        for (int k = 1; k <= 8; k++)
        {
            dsList.Tables[0].Columns.RemoveAt(1);
        }

        GridView GVDistributionList = new GridView();

        GVDistributionList.DataSource = dsList;
        GVDistributionList.DataBind();

        for (int j = 1; j < GVDistributionList.Rows.Count; j++)
        {
            GVDistributionList.Rows[j].Enabled = false;
            GVDistributionList.Rows[j].Visible = false;
        }

        GVCoachingExp.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        StringWriter stringWrite1 = new StringWriter();
        HtmlTextWriter htmlWrite1 = new HtmlTextWriter(stringWrite1);
        Response.Write("<br>Distribution List<br>");



        GVDistributionList.RenderControl(htmlWrite1);
        Response.Write(stringWrite1.ToString());
        Response.End();

    }

    protected void GVCoaching_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                hdnChildMeetingURL.Value = ((Label)GVCoaching.Rows[selIndex].FindControl("LblMeetingURL") as Label).Text;
                string OnlineClassEmail = string.Empty;
                OnlineClassEmail = ((Label)GVCoaching.Rows[selIndex].FindControl("LblOnlineClassEmail") as Label).Text;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string SessionKey = string.Empty;
                WebExID = ((Label)GVCoaching.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GVCoaching.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                SessionKey = ((Label)GVCoaching.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                string ProductCode = ((Label)GVCoaching.Rows[selIndex].FindControl("lblPrdCode") as Label).Text;
                string CoachName = ((Label)GVCoaching.Rows[selIndex].FindControl("lblCoachname") as Label).Text;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + OnlineClassEmail.Trim() + "'";
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                int countEmail = 0;
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
                if (countEmail > 0)
                {
                    string status = GetTrainingSessions(WebExID, WebExPwd, SessionKey);
                    if (status == "INPROGRESS")
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" + ProductCode + "','" + CoachName + "');", true);
                    }
                }
                else
                {

                    LblMsg.Text = "Only authorized child can join into the training session.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ExportToExcelAll()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        CmdText = "SELECT C.MemberID,I.FirstName +' '+ I.LastName as Name,I.Email, C.EventYear,C.EventID, C.EventCode,Case When C.Phase =1 Then 'One' Else Case When C.Phase=2 then 'Two' Else 'Three' End End as Phase ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level) as '#Of Students', C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions, C.HostJoinURL  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + ddlYear.SelectedValue + " and C.Accepted='Y' AND C.EventID=13 group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.EventYear,C.EventID, C.EventCode,C.Phase ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,C.HostJoinURL order by I.LastName, I.FirstName Asc; ";

        CmdText += "select ROW_NUMBER() Over (ORDER BY P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, I1.Email as Email ,I1.Career as JobTitle,Ch.Email as ChildEmail,Ch.OnlineClassEmail,ch.pwd,CR.Approved,CR.SessionNo, CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName,Ch.FIRST_NAME, Ch.LAST_NAME ,CR.Grade, I1.FirstName +' '+ I1.LastName as FatherName,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status,AttendeeJoinURL  ";

        CmdText = CmdText + " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber  INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON";

        CmdText = CmdText + " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" + ddlYear.SelectedValue + " where ";

        CmdText = CmdText + "  CR.Approved='Y' and C.Accepted='Y'  ORDER BY CR.ProductGroupID,CR.ProductID,Ch.LAST_NAME,Ch.FIRST_NAME ASC;";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["Ser#"] = i + 1;

                }
                ds.Tables[1].Columns.Add("Ser#").SetOrdinal(0);
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    ds.Tables[1].Rows[i]["Ser#"] = i + 1;

                }

                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;
                ds.Tables[0].TableName = "CoachList";
                ds.Tables[1].TableName = "StudentList";
                string filename = "CoachStudentList_" + monthDay + "_" + year + ".xls";
                ExcelHelper.ToExcel(ds, filename, Page.Response);
            }
        }

    }
    protected void BtnExportExcelAll_Click(object sender, EventArgs e)
    {
        ExportToExcelAll();
    }

    public string GetTrainingSessions(string WebExID, string PWD, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingStatusTestResponse(xmlReply);
        }

        if (result == "INPROGRESS")
        {
            //string cmdUpdateText = "update WebConfLog set Status='InProgress' where SessionKey='" + SessionKey + "'";
            //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

        }
        else
        {
            LblMsg.Text = "Meeting Not In-Progress";


        }
        return result;

    }
    private string ProcessTrainingStatusTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;
                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText;
                day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText;
                startTime = startTime.Substring(10, 9);
                double Mins = 0.0;
                DateTime dFrom = DateTime.Now;
                DateTime dTo = DateTime.Now;
                string sDateFrom = startTime;

                string today = DateTime.Today.DayOfWeek.ToString();
                if (today.ToLower() == day.ToLower())
                {
                    string sDateTo = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);

                    //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    DateTime easternTime = DateTime.Now;
                    string strEasternTime = easternTime.ToShortDateString();
                    strEasternTime = strEasternTime + " " + sDateFrom;
                    DateTime dtEasternTime = Convert.ToDateTime(strEasternTime);
                    DateTime easternTimeNow = DateTime.Now.AddHours(1);

                    TimeSpan TS = dtEasternTime - easternTimeNow;
                    Mins = TS.TotalMinutes;


                }
                else
                {
                    for (int i = 1; i <= 7; i++)
                    {
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString();
                        if (today.ToLower() == day.ToLower())
                        {
                            dTo = DateTime.UtcNow.AddDays(i);
                            //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            DateTime easternTime = DateTime.Now.AddDays(i);
                            string sDateTo = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                            string targetDateTime = easternTime.ToShortDateString() + " " + sDateFrom;
                            //DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            DateTime easternTimeNow = DateTime.Now.AddHours(1);

                            DateTime dtTargetTime = Convert.ToDateTime(targetDateTime);
                            TimeSpan TS = dtTargetTime - easternTimeNow;
                            Mins = TS.TotalMinutes;


                        }
                    }
                }
                string DueMins = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."));
                hdnSessionStartTime.Value = startTime;
                hdnStartMins.Value = Convert.ToString(DueMins);

            }

        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }
}