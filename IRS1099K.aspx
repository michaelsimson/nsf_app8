﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="IRS1099K.aspx.cs" Inherits="IRS1099K" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
     <script language="Javascript" type="text/javascript">

         function onlyNos(e, t) {
             try {
                 if (window.event) {
                     var charCode = window.event.keyCode;
                 }
                 else if (e) {
                     var charCode = e.which;
                 }
                 else { return true; }
                 if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                     return false;
                 }
                 
                 return true;
              
             }
             catch (err) {
                 //  alert(err.Description);
             }
         }
         </script>
       <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>

    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong> 
      IRS 1099K Data




    </strong>
             <br />
       
    </div>


    <br />
   

<table  margin-left: 0px;">
    
        <tr>
            <td style="width:130px"></td>
      <td align="left" nowrap="nowrap" style="width: 46px" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year  </b></td>
            
            <td align="left" nowrap="nowrap" >
                 <asp:DropDownList ID="ddlYear" runat="server" Width="150px" 
                      AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" 
                   >
                </asp:DropDownList>
              
            </td>
        
          
            <td style="width: 141px" align="right">
                <b>CC Type</b>&nbsp;</td><td>  <asp:DropDownList ID="ddlCCType" runat="server" Width="150px" 
                      AutoPostBack="True">
                    <asp:ListItem Value="type">Select CC Type</asp:ListItem>
                   <asp:ListItem Value="VisaMCDisc">Visa,MC,Discover</asp:ListItem>
                   <asp:ListItem Value="AmEx">American Express</asp:ListItem>
                </asp:DropDownList></td><td>
         <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
            </td>
            <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td style="width: 131px" align="left">
                &nbsp;</td>
            
           
        </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td>&nbsp;</td>
        <td>  
            &nbsp;</td><td align="center">
             &nbsp;</td><td></td><td></td>
           </tr></table>
    <div id="enterinputFields" visible="false" runat="server">
    <table>
        <tr><td></td><td ><b>#of Transactions for the year</b></td><td>
                <asp:TextBox ID="tbNumberofYears" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
            </td><td>
                &nbsp;</td><td></td><td></td>

        </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td><b>January</b></td>
        <td>  
            <asp:TextBox ID="tbJanuaryData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td><td></td><td><b>July</b></td><td>
         <asp:TextBox ID="tbJulyData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td><b>February</b></td>
        <td>  
            <asp:TextBox ID="tbFebruaryData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td><td>&nbsp;</td><td><b>August</b></td><td>
         <asp:TextBox ID="tbAugustData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td><b>March</b></td>
        <td>  
            <asp:TextBox ID="tbMarchData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td><td>&nbsp;</td><td><b>September</b></td><td>
         <asp:TextBox ID="tbSeptemberData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td><b>April</b></td>
        <td>  
            <asp:TextBox ID="tbAprilData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td><td>&nbsp;</td><td><b>October</b></td><td>
         <asp:TextBox ID="tbOctoberData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td><b>May</b></td>
        <td>  
            <asp:TextBox ID="tbMayData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td><td>&nbsp;</td><td><b>November</b></td><td>
         <asp:TextBox ID="tbNovemberData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td><b>June</b></td>
        <td>  
            <asp:TextBox ID="tbJuneData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td><td>&nbsp;</td><td><b>December</b></td><td>
         <asp:TextBox ID="tbDecemberData" runat="server" Width="73px" onkeypress="return onlyNos(event,this);"></asp:TextBox>
         </td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td>&nbsp;</td>
        <td>  
            &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>
         &nbsp;</td>
           </tr>
    <tr>
         <td style="width:131px">&nbsp;</td><td>&nbsp;</td>
        <td>  
     <asp:Button ID="btnSaveUpdate" runat="server" Text="" OnClick="btnSaveUpdate_Click"/>
         </td><td>
       <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClick="btncancel_Click" style="margin-top: 0px"  />
         </td><td>&nbsp;</td><td>
         &nbsp;</td>
           </tr>
    </table></div>
   
  
   


 <%--  </div>--%>

 <br/>
 <div  align="center"><asp:Label ID="lblError" runat="server" ForeColor="red" style="font-weight: 700"></asp:Label></div>
  <div id="Div1" align="center" runat="server">

        
        
         <br />
        <asp:GridView ID="gvIRS1099K" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" OnRowUpdating="gvIRS1099K_RowUpdating" OnRowEditing="gvIRS1099K_RowEditing">
            <Columns>
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:HiddenField ID="RevID" Value='<%# Bind("RevID")%>' runat="server" />
                        <asp:Button ID="Button1" runat="server" Text="Edit"  CommandName="edit" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblYear" runat="server" Text='<%#Eval("Year") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CCType">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblCCType" runat="server" Text='<%#Eval("CCType") %>' ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Count">
                    <ItemTemplate>
                        <asp:Label ID="lblCount" runat="server" Text='<%#Eval("Count") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="January">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblJanuary" runat="server" Text='<%#Eval("January") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="February">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblFebruary" runat="server" Text='<%#Eval("February") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="March">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblMarch" runat="server" Text='<%#Eval("March") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="April">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblApril" runat="server" Text='<%#Eval("April") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="May">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblMay" runat="server" Text='<%#Eval("May") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="June">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblJune" runat="server" Text='<%#Eval("June") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="July">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblJuly" runat="server" Text='<%#Eval("July") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="August">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblAugust" runat="server" Text='<%#Eval("August") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="September">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSeptember" runat="server" Text='<%#Eval("September") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="October">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblOctober" runat="server" Text='<%#Eval("October") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="November">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblNovember" runat="server" Text='<%#Eval("November") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="December">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDecember" runat="server" Text='<%#Eval("December") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CreatedBy">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%#Eval("CreatedBy") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CreatedDate">
                    <ItemTemplate>
                        <asp:Label ID="Label16" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ModifiedBy">
                    <ItemTemplate>
                        <asp:Label ID="Label17" runat="server" Text='<%#Eval("ModifiedBy") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ModifyDate">
                    <ItemTemplate>
                        <asp:Label ID="Label18" runat="server" Text='<%#Eval("ModifyDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        
      
             
   </div>
   <br />
    
    
  <br />
   
    
          

</asp:Content>

