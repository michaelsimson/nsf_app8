﻿Imports Microsoft.ApplicationBlocks.Data

Partial Class CoachClassCalReport
    Inherits System.Web.UI.Page


    Dim cmdText As String

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 1 '89
        'Session("LoginID") = 4240 '22214       
        'Session("LoggedIn") = "true"

        Try
            If LCase(Session("LoggedIn")) <> "true" Or Session("LoginID").ToString = String.Empty Then

                Response.Redirect("~/maintest.aspx")

            End If

            If Page.IsPostBack = False Then

                Session("WeekCnt") = 0
                Dim year As Integer = 0
                year = Convert.ToInt32(DateTime.Now.Year)
                Dim i, j As Integer
                j = 4
                For i = 0 To 4
                    ' ddlYear.Items.Insert(i, Convert.ToString(year - j))
                    ddlYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))
                    j = j - 1
                Next
                ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                ' ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(year)))

                ddlPhase.Items.Add(New ListItem("Select Phase", -1))

                For i = 1 To 4
                    ddlPhase.Items.Add(i)
                Next

                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Parent Functions"
                    hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
                ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Response.Redirect("~/login.aspx?entry=v")
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "88") Then
                    FillEvent()
                    FillProductGroup()
                Else
                    Response.Redirect("~/maintest.aspx")
                End If
            End If
        Catch ex As Exception
            ' Response.Write("Err:" & ex.ToString)
        End Try
    End Sub

    ' Add events in dropdown for adding and updating
    Private Sub FillEvent()
        ddlEvent.Items.Add(New ListItem("Coaching", "13"))
    End Sub

    ' Get product group details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProductGroup()

        Dim dsProductGrp As DataSet
        If Session("RoleId") = 89 Then
            cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null  Order by ProductGroupId"
        Else
            cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" & ddlEvent.SelectedValue & " and ProductId is not Null Order by ProductGroupId"
        End If

        ' "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] AS ProductGroupCode  FROM dbo.[Product] A Where A.EventID in(13) Order by A.ProductGroupCode")
        dsProductGrp = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ViewState("PrdGroup") = dsProductGrp.Tables(0)

        ddlProductGroup.DataSource = dsProductGrp.Tables(0)
        ddlProductGroup.DataBind()

        If ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Enabled = True
            ddlProductGroup.Items.Insert(0, "Select Product Group")
        Else
            ddlProductGroup.SelectedIndex = 0
            ddlProductGroup.Enabled = False

        End If

        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)

    End Sub

    ' Get product details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProduct()

        If ddlProductGroup.SelectedValue <> "Select Product Group" Then

            If Session("RoleId") = 89 Then
                cmdText = " select distinct ProductID,ProductCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId is not Null  Order by ProductId"
            Else
                cmdText = " select distinct ProductID,ProductCode from volunteer where eventid =" & ddlEvent.SelectedValue & " and ProductGroupId =" & ddlProductGroup.SelectedValue & " and ProductId is not Null Order by ProductId"
            End If
            '"SELECT distinct A.[ProductId] as ProductId, A.[ProductCode] AS ProductCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupId= " & ddlProductGroup.SelectedValue & " Order by A.ProductCode"
            Dim dsProduct As DataSet
            dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ddlProduct.DataSource = dsProduct.Tables(0)
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 1 Then
                ddlProduct.Enabled = True
                ddlProduct.Items.Insert(0, "Select Product")
            Else
                ddlProduct.SelectedIndex = 0
                ddlProduct.Enabled = False
            End If
        End If

    End Sub
    Private Sub FillCoach(ddl As DropDownList)
        Dim dsCoach As DataSet
        If Session("RoleId") = 88 Then

            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" & Session("LoginID")
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)

            If dsCoach.Tables(0).Rows.Count > 0 Then
                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                ddl.SelectedIndex = 0
                ddl.Enabled = False
            End If

        Else
            ' If Len(strCond) = 0 Then
            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " order by lastname,firstname"
            '    Else
            '    cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " " & strCond & " order by lastname,firstname"
            'End If
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)

            If dsCoach.Tables(0).Rows.Count > 0 Then

                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                If ddl.Items.Count > 0 Then
                    ddl.Items(0).Selected = True
                    If ddl.Items.Count = 1 Then
                        ddl.Enabled = False
                    Else
                        ddl.Enabled = True
                    End If

                End If


            End If

        End If
    End Sub


    'Fill product for each selected product group
    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Try
            ddlProduct.Items.Clear()
            FillProduct()
        Catch ex As Exception
            '   Response.Write("ERR: " & ex.ToString())
        End Try
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged
        Try
            ddlCoach.Items.Clear()
            FillCoach(ddlCoach)
        Catch ex As Exception
            '  Response.Write("ERR: " & ex.ToString())
        End Try
    End Sub

    Protected Sub rdoCalendar_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoCalendar.SelectedIndexChanged

        lblCoachClassCalendar.Text = rdoCalendar.SelectedItem.Text
        If rdoCalendar.SelectedIndex = 0 Then ' Class Calendar by Coach
            tblCoach.Visible = True
            gvCoachClassCal.Visible = False


        Else
            Dim ds As DataSet
            tblCoach.Visible = False
            gvCoachClassCal.Visible = True
            cmdText = cmdText + " select cc.EventYear, 'Coaching' EventCode, cc.ProductGroup ProductGroupCode, cc.Product ProductCode , cc.Phase, cc.[Date], cc.[Day],cc.[Time], cc.Duration, cc.SerNo, cc.WeekNo,(select I1.Firstname + ' '+ I1.LastName   from indspouse I1"
            cmdText = cmdText + " where I1.automemberid=cc.MemberId) as Coach, [Status], (select I2.FirstName + ' ' + I2.LastName from indspouse I2 where I2.automemberid=cc.Substitute) as SubstituteName,  Reason,"
            cmdText = cmdText + " rel.QReleaseDate,rel.QDeadlineDate, rel.AReleaseDate , cs.UserId, cs.PWD from CoachClassCal cc left join CoachPapers cp on cp.eventyear=cc.eventyear and cp.EventId=13 and cp.productid=cc.productid and cp.level=cc.level and cp.DocType='Q' and cp.WeekId = cc.WeekNo "
            cmdText = cmdText + " left join coachreldates rel on  rel.memberid=cc.memberid and rel.eventyear=cc.eventyear and rel.productid=cc.productid and rel.phase=cc.phase and rel.level=cc.level and rel.session=cc.sessionno and rel.CoachPaperId= cp.CoachPaperId  "
            cmdText = cmdText + " left join calsignup cs on cs.MemberId=cc.MemberId and cs.EventId=13 and cs.EventYear=cc.EventYear and cs.ProductGroupId= cc.ProductGroupId and cs.ProductId=cc.ProductId and cs.phase=cc.phase and cs.level=cc.level and cs.sessionno=cc.sessionno and cs.Accepted='y'"
            'cmdText = cmdText + " where (cc.[date]>=getdate() and cc.[date]<= dateadd(dd,8, getdate()))  "
            cmdText = cmdText + " where cc.[date]>=getdate()  "
            Select Case rdoCalendar.SelectedIndex
                Case 1 ' Missing Calendar Report 
                    cmdText = cmdText + " and cc.status is null" ' and cs.MemberId = " & ddlCoach.SelectedItem.Value
                Case 2 'Cancelled Calendar Report
                    cmdText = cmdText + " and cc.status ='Cancelled' " ' and cs.MemberId = " & ddlCoach.SelectedItem.Value
                Case 3 'Substitute Calendar Report
                    cmdText = cmdText + " and cc.status='Substitute'" ' and cs.MemberId = " & ddlCoach.SelectedItem.Value
            End Select
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            gvCoachClassCal.DataSource = ds.Tables(0)
            gvCoachClassCal.DataBind()
        End If
    End Sub

    Private Sub BindCoachClassDetailsInRepeater()
        'and productgroupid<>" + hdTable1PrdGrp.Value + " and productid <>" + hdTable1Prd.Value + "
        cmdText = "select count(*) from calsignup  where eventyear=" & ddlYear.SelectedItem.Value & " and eventid=13 and Accepted='y' and MemberId= " & ddlCoach.SelectedItem.Value & " and ProductGroupId= " & ddlProductGroup.SelectedItem.Value & " and  Productid=" & ddlProduct.SelectedItem.Value & " and Phase=" & ddlPhase.SelectedItem.Value

        If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
            Dim ds As DataSet
            cmdText = "select ProductGroupId,ProductGroupCode,Productid,ProductCode, phase,level,sessionno from calsignup  where eventyear=" & ddlYear.SelectedItem.Value & " and eventid=13 and Accepted='y' and MemberId= " & ddlCoach.SelectedItem.Value & " group by ProductGroupId,ProductGroupCode,Productid,ProductCode, phase,level,sessionno "
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                rptCoachClass.DataSource = ds.Tables(0)
                rptCoachClass.DataBind()
            End If
        End If

    End Sub
    Private Sub BindCoachClassDetailsInGrid(grCtrl As GridView)
        Try
            Dim dt As Date = Now.Date.ToString("yyyy-MM-dd")  ' "2014-12-02"
            Dim rpItem As RepeaterItem = grCtrl.NamingContainer

            Dim iProductId As Integer = DirectCast(rpItem.FindControl("hdProduct"), HiddenField).Value
            Dim iProductGrpId As Integer = DirectCast(rpItem.FindControl("hdProductGrp"), HiddenField).Value
            Dim iPhase As Integer = DirectCast(rpItem.FindControl("lblPhase"), Label).Text
            Dim strLevel As String = DirectCast(rpItem.FindControl("lblLevel"), Label).Text
            Dim iSessionNo As Integer = DirectCast(rpItem.FindControl("lblSessionNo"), Label).Text


            Dim rep_hdWeekCnt As HiddenField = TryCast(rpItem.FindControl("rep_hdWeekCnt"), HiddenField)

            If Len(Trim(rep_hdWeekCnt.Value)) = 0 Then
                rep_hdWeekCnt.Value = "0"
            End If
            ' '" + dt.ToString("yyyy-MM-dd") + "'
            cmdText = "select cs.SignupID,isnull(cc.CoachClassCalID, '') CoachClassCalID, cs.MemberId, cs.EventYear,cs.EventID, cs.SessionNo,cs.Level,cs.ProductGroupId,cs.ProductGroupCode,cs.ProductId,cs.Productcode, isnull(CONVERT(char(10),cdc.startdate,126),'')  as startdate,"
            cmdText = cmdText + " cs.EndDate,cs.Duration,cs.Phase,cc.[date],cs.[Day] [Day],cs.[Time] [Time],cs.UserId,cs.Pwd,cc.SerNo,isnull(cc.WeekNo,0) as WeekNo, cc.[Status], cc.Substitute, cc.Reason, rel.qreleasedate, rel.qdeadlinedate, rel.areleasedate , (select i1.FirstName + ' ' + i1.LastName from indspouse i1 where i1.automemberid=cc.Substitute) as SubstituteName "
            cmdText = cmdText + "  from calsignup cs "
            cmdText = cmdText + " left join CoachingDateCal cdc on cs.EventId=cdc.EventId and cdc.EventYear=cs.EventYear and cdc.ProductGroupId= cs.ProductGroupId and cdc.ProductId=cs.ProductId "
            cmdText = cmdText + " left join coachclasscal cc on cs.MemberId=cc.MemberId and (cc.[date]>= cdc.startdate and cc.[date]<= cdc.enddate)  and cs.eventyear=cc.eventyear and "
            cmdText = cmdText + " cs.productid=cc.productid and cs.phase=cc.phase and cs.level=cc.level and cs.sessionno=cc.sessionno  left join coachreldates rel on "
            cmdText = cmdText + " rel.memberid=cs.memberid  and cs.eventyear=rel.eventyear and cs.productid=rel.productid and cs.phase=rel.phase and cs.level=rel.level and cs.sessionno=rel.session "

            cmdText = cmdText + " where cdc.StartDate is not null and cs.MemberId = " & ddlCoach.SelectedItem.Value
            cmdText = cmdText + " and cs.eventyear=" & ddlYear.SelectedItem.Value & " and  cs.ProductId=" & iProductId & " and cs.Level='" & strLevel & "' and cs.sessionno=" & iSessionNo & " and cs.Accepted='y'  order by SignUpID"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'If ds.Tables.Count > 0 Then
            '    'Row of the Grid from where the SelectedIndex change event is fired.
            '     Dim rp As RepeaterItem = TryCast(grCtrl.NamingContainer, RepeaterItem)
            '    'Dim lblPrdGrp As Label = DirectCast(rp.FindControl("lblPrdGrp"), Label)
            '    'lblPrdGrp.Text = ds.Tables(0).Rows(0).Item("ProductGroupCode")
            '    'Dim hdProductGrp As HiddenField = DirectCast(rp.FindControl("hdProductGrp"), HiddenField)
            '    'hdProductGrp.Value = ds.Tables(0).Rows(0).Item("ProductGroupId")
            '    'Dim lblProduct As Label = DirectCast(rp.FindControl("lblProduct"), Label)
            '    'lblProduct.Text = ds.Tables(0).Rows(0).Item("ProductCode")
            '    'Dim lblPhase As Label = DirectCast(rp.FindControl("lblPhase"), Label)
            '    'lblPhase.Text = ds.Tables(0).Rows(0).Item("Phase")
            '    'Dim lblLevel As Label = DirectCast(rp.FindControl("lblLevel"), Label)
            '    'lblLevel.Text = ds.Tables(0).Rows(0).Item("Level")
            '    'Dim lblSession As Label = DirectCast(rp.FindControl("lblSessionNo"), Label)
            '    'lblSession.Text = ds.Tables(0).Rows(0).Item("SessionNo")
            'End If

            grCtrl.DataSource = ds.Tables(0)
            grCtrl.DataBind()
        Catch ex As Exception
            '   Response.Write("ERR: " & ex.ToString())
        End Try


    End Sub

    Protected Sub rptCoachClass_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCoachClass.ItemDataBound
        Try
            If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then

                Dim grid As GridView = DirectCast(e.Item.FindControl("rpt_grdCoachClassCal"), GridView)

                'Dim iProductId As Integer = DirectCast(e.Item.FindControl("hdProduct"), HiddenField).Value
                'Dim iPhase As Integer = DirectCast(e.Item.FindControl("hdProduct"), HiddenField).Value
                'Dim iProductId As Integer = DirectCast(e.Item.FindControl("hdProduct"), HiddenField).Value
                BindCoachClassDetailsInGrid(grid)

                Dim lblTableCnt As Label = DirectCast(e.Item.FindControl("lblTableCnt"), Label)
                lblTableCnt.Text = CInt(e.Item.ItemIndex) + 1


            End If
        Catch ex As Exception
            'Response.Write("ERR: " & ex.ToString())
        End Try
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindCoachClassDetailsInRepeater()
    End Sub
End Class
