
Imports System.Text
Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.IO
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net

Namespace VRegistration


    Partial Class VolUniqueEmail
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region



        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=v")
            End If
            If Not Page.IsPostBack Then
                lblMessage.Text = "Thank you for being a volunteer for North South Foundation."
                'lblMessage.Text = "Thank you for your offer to volunteer. The email address you used to login needs to be linked to your profile, if it exists.  By profile, we mean your name, address, contact information, etc.  Only you know if it exists.  If you are married, your spouse may have entered your  profile, but we don�t know that at this point.  If you know for certain that your profile  does not exist in our system, please select No below and proceed to enter your profile.  If you already have a profile, select Yes.  Thank you for your kind assistance."
                Dim conn As New SqlConnection(Application("ConnectionString"))
                'Dim conn1 As New SqlConnection(Application("ConnectionString"))
                'Dim conn2 As New SqlConnection(Application("ConnectionString"))
                Dim cnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from IndSpouse where email = '" & Session("userid") & "'")
                If cnt = 0 Then
                    PnlNo.Visible = True
                    lblMessage.Text = "Thank you for your offer to volunteer. The email address you used to login needs to be linked to your profile, if it exists.  By profile, we mean your name, address, contact information, etc.  Only you know if it exists.  If you are married, your spouse may have entered your  profile, but we don�t know that at this point.  If you know for certain that your profile  does not exist in our system, please select No below and proceed to enter your profile.  If you already have a profile, select Yes.  Thank you for your kind assistance."
                ElseIf cnt > 2 Then

                    Dim SMailFrom As String
                    Dim sMailTo As String
                    Dim sSubject As String
                    Dim sBody As String
                    SMailFrom = "nsfcontests@gmail.com"
                    sMailTo = "nsfcontests@gmail.com"
                    sSubject = Session("UserID") & "is Having " & cnt.ToString() & " Records"
                    Dim mm As New MailMessage(SMailFrom, sMailTo)
                    mm.Subject = sSubject

                    sBody = Server.HtmlEncode("")
                    sBody = sBody & "Dear Sir, <br />"
                    sBody = sBody & " A volunteer is trying to access the system with the email address:" & Session("UserID")
                    sBody = sBody & "<br /> But the system found that the email exists in more than two profiles. This is not permissible in the system.  Please escalate this issue for further investigation."
                    mm.Body = sBody
                    mm.IsBodyHtml = True

                    '(3) Create the SmtpClient object
                    Dim client As New SmtpClient()

                    '(4) Send the MailMessage (will use the Web.config settings)
                    'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

                    'client.Host = host
                    'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
                    'client.Timeout = 20000


                    Try
                        client.Send(mm)
                    Catch ex As Exception
                        ' MsgBox(ex.ToString)
                        'lblMessage.Text = ex.Message
                    End Try
                    trContinue.Visible = True
                    btncontinue.Visible = True
                Else
                    Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & Session("UserID") & "' AND b.Email = '" & Session("UserID") & "' AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
                    Dim i As Integer = 0
                    While readr.Read
                        i = 1
                        Session("IndId") = readr("INDID")
                        Session("SpouseID") = readr("SpouseID")
                    End While
                    readr.Close()
                    If i = 0 Then
                        Dim SMailFrom As String
                        Dim sMailTo As String
                        Dim sSubject As String
                        Dim sBody As String
                        SMailFrom = "nsfcontests@gmail.com"
                        sMailTo = "nsfcontests@gmail.com"
                        sSubject = Session("UserID") & "is Having " & cnt.ToString() & " Records"
                        Dim mm As New MailMessage(SMailFrom, sMailTo)
                        mm.Subject = sSubject

                        sBody = Server.HtmlEncode("")
                        sBody = sBody & "Dear Sir <br>"
                        sBody = sBody & " A volunteer is trying to access the system with the email address:" & Session("UserID")
                        sBody = sBody & "But the system found that the email exists in more than two profiles. This is not permissible in the system.  Please escalate this issue for further investigation."
                        mm.Body = sBody
                        mm.IsBodyHtml = True

                        '(3) Create the SmtpClient object
                        Dim client As New SmtpClient()

                        '(4) Send the MailMessage (will use the Web.config settings)
                        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

                        'client.Host = host
                        'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
                        'client.Timeout = 20000


                        Try
                            client.Send(mm)
                        Catch ex As Exception
                            ' MsgBox(ex.ToString)
                            'lblMessage.Text = ex.Message
                        End Try
                        trContinue.Visible = True
                        btncontinue.Visible = True
                    Else
                        PnlNew.Visible = True
                        Dim indRw As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS Name, b.user_pwd FROM IndSpouse AS a CROSS JOIN login_master AS b WHERE b.user_email = '" & Session("UserID") & "' AND a.AutoMemberID = " & Session("IndId") & "")
                        While indRw.Read()
                            lblIndName.Text = indRw("Name")
                            lblIndEmail.Text = Session("UserID")
                            lblIndPwd.Text = indRw("user_pwd")
                        End While
                        indRw.Close()
                        Dim indSpRw As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS Name, b.user_pwd FROM IndSpouse AS a CROSS JOIN login_master AS b WHERE b.user_email = '" & Session("UserID") & "' AND a.AutoMemberID = " & Session("SpouseID") & "")
                        While indSpRw.Read()
                            lblIndSpName.Text = indSpRw("Name")
                            LblIndSpEmail.Text = Session("UserID")
                            lblIndSpPwd.Text = indSpRw("user_pwd")
                        End While
                        indSpRw.Close()
                    End If
                End If
            End If
        End Sub

        Protected Sub RBtnYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBtnYes.CheckedChanged
            If RBtnYes.Checked = True Then
                trMaill.Visible = True
                btncontinue.Visible = True
                PnlNo.Visible = False
            End If
        End Sub

        Protected Sub btncontinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncontinue.Click
            Response.Redirect("http://www.northsouth.org")
        End Sub

        Protected Sub RBtnNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBtnNo.CheckedChanged
            If RBtnNo.Checked = True Then
                Session("LoggedIn") = "True"
                Session("LoginEmail") = Session("UserID")
                Response.Redirect("Registration.aspx")
            End If
        End Sub

        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim NewEmail, NewPwd, NewName, chapter As String
            Dim NewId, chapterID As Integer
            If (txtIndSpEmail.Text.Length > 0 And txtIndSpEmail.Text.Length > 0) Or (txtIndEmail.Text.Length > 0 And txtIndPwd.Text.Length > 0) Then
                If Not Page.IsValid Then
                    lblError.Text = "Please Enter Valid Email and Password"
                    Exit Sub
                End If
                If txtIndEmail.Text.Length < 2 And txtIndSpPwd.Text.Length > 0 And txtIndSpPwd.Text = txtIndSpCPwd.Text Then
                    NewEmail = txtIndSpEmail.Text
                    NewPwd = txtIndSpPwd.Text
                    NewId = Session("SpouseID")
                    NewName = lblIndSpName.Text
                ElseIf txtIndEmail.Text.Length > 0 And txtIndSpEmail.Text.Length < 2 And txtIndCPwd.Text = txtIndPwd.Text Then
                    NewEmail = txtIndEmail.Text
                    NewPwd = txtIndPwd.Text
                    NewId = Session("IndId")
                    NewName = lblIndName.Text
                Else
                    lblError.Text = "password Mismatch"
                    Exit Sub
                End If

                Dim Strsql1 As String = "Select count(*) from child where email = '" & NewEmail & "'"
                Dim dupChild As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql1)

                Dim Strsql As String = "Select count(*) from Indspouse where email = '" & NewEmail & "' AND automemberID NOT IN (" & Session("IndId") & "," & Session("SpouseID") & ")"
                Dim Dupcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql)
                If Dupcnt > 0 Or dupChild > 0 Then
                    lblError.Text = "This Email already Exist in our system"
                Else
                    Dim strsq As String = "UPDATE IndSpouse SET Email='" & NewEmail & "' WHERE automemberID =" & NewId
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsq)
                    Strsql = "Select count(*) from Login_Master where User_email = '" & NewEmail & "'"
                    Dim DupLcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql)
                    If DupLcnt = 1 Then
                        Strsql = "Update Login_Master set user_pwd ='" & NewPwd & "', ModifyDate ='" & DateTime.Now & "', ModifiedBy=" & Session("IndId") & " WHERE User_Email='" & NewEmail & "'"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsq)
                    ElseIf DupLcnt = 0 Then
                        Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT Chapter, ChapterID FROM IndSpouse WHERE Email='" & NewEmail & "'")
                        While readr.Read()
                            chapter = readr("Chapter")
                            chapterID = readr("ChapterID")
                        End While
                        readr.Close()
                        Strsql = "Insert Into Login_Master(user_email, user_pwd, date_added,  user_role, user_name, chapter, active, ChapterID, CreateDate, CreatedBy) Values('"
                        Strsql = Strsql & NewEmail & "','" & NewPwd & "','" & DateTime.Now & "','Volunteer','" & NewName & "','" & chapter & "','Yes'," & chapterID & ",'" & DateTime.Now & "'," & Session("IndId") & ")"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, Strsql)
                        Response.Redirect("login.aspx?entry=v")
                    Else
                        lblError.Text = "E-Mail Has Changed in Profile but You have error in Login kindly mail to nsfcontests@gmail.com"
                    End If
                End If
            Else
                lblError.Text = "Please Enter Valid E-Mail"
            End If
        End Sub
    End Class

End Namespace

