﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BeeBook_BeeBookSchedule : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginId"] != null)
        {
            if (!IsPostBack)
            {
                ChapterDrop(ddchapter);
                //BindBeeBookSchedule();
            }
        }
        else
        {
            Response.Redirect("~/maintest.aspx");
        }
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        tblAddUpdateTaskActivities.Visible = true;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        tblAddUpdateTaskActivities.Visible = false;
    }
    protected void btnAddNewtask_Click(object sender, EventArgs e)
    {
        trAddNewTask.Style.Add("display", "table-row");
    }
    protected void btnTaskCancel_Click(object sender, EventArgs e)
    {
        trAddNewTask.Style.Add("display", "none");
    }
    protected void btnAddUpdate_Click(object sender, EventArgs e)
    {
        string task = ddlTask.SelectedItem.Text;
        int taskID = ddlTask.SelectedIndex;

    }
    protected void btnAddtask_Click(object sender, EventArgs e)
    {
        string taskName = txtTask.Text;
        string userID = Session["LoginId"].ToString();
        int eventID = Convert.ToInt32(ddEvent.SelectedValue);
        string eventName = ddEvent.SelectedItem.Text;
        int chapterID = Convert.ToInt32(ddchapter.SelectedValue);
        string chapterName = ddchapter.SelectedItem.Text;
        //string cmdtext = "Insert into BeeBookSchedule ([Event],EventID,Chapter,ChapterID,Components,CreatedDate,CreatedBy) values('" + eventName + "'," + eventID + ",'" + chapterName + "'," + chapterID + ",'" + taskName + "',GETDATE()," + userID + ")";
        //SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
        //lblMsg.Text = "Task added successfully";
        //lblMsg.ForeColor = Color.Blue;
    }
    string ConnectionString = "ConnectionString";
    protected void BindBeeBookSchedule()
    {
        string cmdtext = "select BL.ID,BS.Event,BS.Chapter,BS.Components,BL.SubComponents,BL.Year,BL.Duration,BL.EndDate,BS.EventID,BS.ChapterID,BS.ComponentID,BL.ID from BeeBookScheduleList BL inner join BeeBookSchedule BS on BS.ComponentID=BL.ComponentID";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
        grdBeeBookSchedule.DataSource = ds.Tables[0];
        grdBeeBookSchedule.DataBind();
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {

        string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
        if (dschapter.Tables[0].Rows.Count > 0)
        {
            ddChapter.Enabled = true;
            ddChapter.DataSource = dschapter;
            ddChapter.DataTextField = "chaptercode";
            ddChapter.DataValueField = "ChapterID";
            ddChapter.DataBind();
            ddChapter.Items.Insert(0, new ListItem("All", "-1"));
            ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
        }


    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {

        ChapterDrop(ddchapter);

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string cmdText = "";
        int eventID = Convert.ToInt32(ddEvent.SelectedValue);

        int chapterID = Convert.ToInt32(ddchapter.SelectedValue);
        if (ddEvent.SelectedValue == "1" && ddchapter.SelectedValue == "1")
        {
            if (ddYear.SelectedValue == "-1")
            {
                cmdText = "select BL.ID,BS.Event,BS.Chapter,BS.Components,BL.SubComponents,BL.Year,BL.Duration,BL.EndDate,BS.EventID,BS.ChapterID,BS.ComponentID,BL.ID from BeeBookScheduleList BL inner join BeeBookSchedule BS on BS.ComponentID=BL.ComponentID where BS.EventID=" + eventID + " and BS.ChapterID=" + chapterID + "";
            }
            else
            {
                cmdText = "select BL.ID,BS.Event,BS.Chapter,BS.Components,BL.SubComponents,BL.Year,BL.Duration,BL.EndDate,BS.EventID,BS.ChapterID,BS.ComponentID,BL.ID from BeeBookScheduleList BL inner join BeeBookSchedule BS on BS.ComponentID=BL.ComponentID where BS.EventID=" + eventID + " and BS.ChapterID=" + chapterID + " and BL.Year='" + ddYear.SelectedItem.Text + "'";
            }

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            grdBeeBookSchedule.DataSource = ds.Tables[0];
            grdBeeBookSchedule.DataBind();
            lblMsg.Text = "";
        }
        else
        {
            grdBeeBookSchedule.DataSource = null;
            lblMsg.Text = "No record found";
            lblMsg.ForeColor = Color.Red;
        }
    }
}