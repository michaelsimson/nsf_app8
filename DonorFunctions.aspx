<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="DonorFunctions.aspx.vb" Inherits="DonorFunctions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="right">
        <p>
            <asp:hyperlink cssclass="btn_02" id="Hyperlink23" runat="server" navigateurl="SupportTracking.aspx">File Support Ticket</asp:hyperlink>
            &nbsp;
        </p>
    </div>
    <div align="center" class="title02">
        Donor Functions
    </div>
    <table id="table1" class="tableclass" runat="server" cellpadding="1" width="100%" border="2" style="border-right: #ffff99 thin solid; border-top: #ffff99 thin solid; border-left: #ffff99 thin solid; border-bottom: #ffff99 thin solid; background-color: #ffffcc;">
        <tr>
            <th style="height: 24px; width: 225px;">Application</th>
            <th style="height: 24px; width: 179px;">Main Option</th>
            <th style="height: 24px; width: 105px;">Reports</th>
            <th style="height: 24px; width: 97px;">Additional</th>
        </tr>
         

        <tr>

            <td style="width: 225px; height: 233px;" class="title04">Donor Functions</td>
            <td style="width: 179px; height: 233px;">
                <p>
                    <asp:hyperlink cssclass="btn_02" id="hlinkViewDonations" runat="server" navigateurl="~/Registration.aspx?ParentUpdate=true">Update Profile</asp:hyperlink>
                </p>
                <%--<P><asp:hyperlink id="hlinkAddDonation" runat="server" NavigateUrl="~/donor_donate.aspx">Donate</asp:hyperlink></P>	--%>
                <p>
                    <asp:linkbutton cssclass="btn_02" id="hlinkdonate" runat="server" text="Donate"></asp:linkbutton>
                </p>
                <p>
                    <asp:hyperlink cssclass="btn_02" id="Hyperlink2" runat="server" navigateurl="~/MainChild.aspx">Add/Update ChildInfo</asp:hyperlink>
                </p>
                <p>
                    <asp:hyperlink cssclass="btn_02" id="hlinkPerRef" runat="server" navigateurl="PersonalPref.aspx?id=3" enabled="true">Personal Preferences</asp:hyperlink>
                    &nbsp;
                </p>
                <p>
                    <asp:hyperlink cssclass="btn_02" id="Hyperlink4" runat="server" navigateurl="RemoveFromSuppressionList.aspx" enabled="true">Remove from Suppression List</asp:hyperlink>
                    &nbsp;
                </p>
                <p>
                    <asp:linkbutton cssclass="btn_02" id="lnkFundRaising" runat="server" text="Register for Fundraising"></asp:linkbutton>
                </p>
                <%--<p><asp:hyperlink CssClass="btn_02" id="hlinkWalkathon" runat="server" Enabled="false" NavigateUrl="Don_athon_selection.aspx?Ev=5">Register for Walk-a-thon</asp:hyperlink><p>--%>
                <p>
                    <asp:linkbutton cssclass="btn_02" id="hlinkWalkathon" runat="server" enabled="false" text="Register for Walk-a-thon"></asp:linkbutton>
                </p>
                <p>
                    <asp:hyperlink cssclass="btn_02" id="hlinkSpons_athon" runat="server" navigateurl="Sponsor_athon.aspx" enabled="true" width="250px">Sponsor for Walk-a-thon/Marathon/Excel-a-thon</asp:hyperlink>
                </p>

            </td>
            <td style="width: 105px;">
                <p>
                    <asp:hyperlink cssclass="btn_02" id="Hyperlink3" runat="server" navigateurl="~/ViewDonations.aspx">View Donations</asp:hyperlink>
                </p>
                <p>
                    <asp:hyperlink cssclass="btn_02" id="Hyperlink1" runat="server" navigateurl="~/donationreceipt.aspx">Donation Receipt</asp:hyperlink>
                </p>
                <p>
                    <asp:hyperlink cssclass="btn_02" id="HyperLink9" navigateurl="~/FundRStatus.aspx?id=1" runat="server">View Fundraising Status </asp:hyperlink>
                </p>
            </td>
            <td style="width: 97px;"></td>
        </tr>

    </table>
</asp:Content>




