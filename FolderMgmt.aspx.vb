﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Partial Class FolderMgmt
    Inherits System.Web.UI.Page
    Dim ServerPath As String = "d:\inetpub\wwwroot\northsouth\"
    Dim urlPath As String = "http://www.northsouth.org/"

    '*********************  FERDINE SILVA  Oct 22, 2010 ***************************
    '*  All RoleID with 1,2,3,4,5 have permission to access the webpagemgmt	      *
    '*  RoleID 1 has access to all public, Private, etc (Root)              	  *
    '*  RoleID 2 - 5 has the access to chapter Panel selection they come through  *
    '*  RoleID 1-5 should not have any assignmant in VolDocAccess table    	      *
    '*  Assigned volunteer cant be let to delete from Volunteer table.       	  *
    '******************************************************************************

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            loadMemberAccess()
            'loadLevel(ddlLevel1, 1, "")
            BtnDelete.Attributes.Add("onclick", "return confirm('Are sure you want to delete the folder?');")
        End If
    End Sub
    Protected Sub ddlselectFolder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlselectFolder.SelectedValue = "0" Then
            lblVErr.Text = ""
            DisableDdl("1")
            selectlevels(13, "select FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID") & " AND FolderListID=" & ddlselectFolder.SelectedValue & " ")
            trdrp.Visible = True
        Else
            lblVErr.ForeColor = Color.Red
            lblVErr.Text = "Please select a folder"
        End If
    End Sub
    Private Sub loadMemberAccess()
        Dim level As Integer
        Dim SQLQuery As String = ""
        If Session("RoleId").ToString() = "1" Then 'Or (Session("RoleId").ToString() = "90")
            level = 0
        ElseIf (Session("RoleId").ToString() = "90") Or (Session("RoleId").ToString() = "43") Then
            'This query is for roleID 90 and 43
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") > 1 Then
                trdrp.Visible = False
                trmultiple.Visible = True
                Dim dsV As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select F.FolderListID,F.DisplayName  from VolDocAccess V Inner Join FolderTree F ON V.FolderListID = F.FolderListID Where V.MemberID =" & Session("LoginID") & "")
                ddlselectFolder.DataSource = dsV
                ddlselectFolder.DataBind()
                If dsV.Tables(0).Rows.Count > 0 Then
                    ddlselectFolder.Items.Insert(0, New ListItem("Select Folder", "0"))
                    ddlselectFolder.Items(0).Selected = True
                End If
                lblVErr.Text = "Please select a Folder"
                Exit Sub
            Else
                SQLQuery = "select FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID")
            End If
        ElseIf (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Then
            'This query is for roleID 2,3,4,5
            SQLQuery = "select FT.FolderListID,FT.Level,dbo.ufn_getFolderName(FT.FolderListID) as Folders from FolderTree FT Inner Join Chapter C ON FT.FName=C.WebFolderName WHERE C.ChapterID = " & Session("selChapterID")
        Else
            level = 13
        End If
        selectlevels(level, SQLQuery)
        '** Have to make default location chapter co-ordinators  ************
        
    End Sub

    Private Sub selectlevels(ByVal level As Integer, ByVal SQLQuery As String)
        Dim arrstr As String()
        Dim folderID As Integer = 0
        Dim selectedDDL As String()
        If SQLQuery.Length > 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLQuery)
            If ds.Tables(0).Rows.Count > 0 Then
                level = ds.Tables(0).Rows(0)("Level")
                If IsDBNull(ds.Tables(0).Rows(0)("FolderListID")) = False Then
                    folderID = ds.Tables(0).Rows(0)("FolderListID")
                    selectedDDL = ds.Tables(0).Rows(0)("Folders").Split("|")
                End If
            Else
                level = 13
            End If
        End If
        Select Case (level)
            Case 1
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                lblLevel.Text = "2"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 2
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                lblLevel.Text = "3"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 3
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                lblLevel.Text = "4"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 4
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                lblLevel.Text = "5"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 5
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                lblLevel.Text = "6"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 6
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                lblLevel.Text = "7"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 7
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                lblLevel.Text = "8"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 8
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                lblLevel.Text = "9"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 9
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                lblLevel.Text = "10"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 10
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                lblLevel.Text = "11"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 11
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                ddlLevel11.Items.RemoveAt(ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByValue("0")))
                ddlLevel11.SelectedIndex = ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByText(selectedDDL(10)))
                ddlLevel11.Enabled = False
                arrstr = ddlLevel11.SelectedValue.Split("|")
                loadLevel(ddlLevel12, 12, arrstr(0))
                lblLevel.Text = "12"
                lblParentFID.Text = arrstr(0)
                constructpath(lblLevel.Text)
            Case 12
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                ddlLevel11.Items.RemoveAt(ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByValue("0")))
                ddlLevel11.SelectedIndex = ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByText(selectedDDL(10)))
                ddlLevel11.Enabled = False
                arrstr = ddlLevel11.SelectedValue.Split("|")
                loadLevel(ddlLevel12, 12, arrstr(0))
                ddlLevel12.Items.RemoveAt(ddlLevel12.Items.IndexOf(ddlLevel12.Items.FindByValue("0")))
                ddlLevel12.SelectedIndex = ddlLevel12.Items.IndexOf(ddlLevel12.Items.FindByText(selectedDDL(11)))
                ddlLevel12.Enabled = False
                arrstr = ddlLevel12.SelectedValue.Split("|")
                lblLevel.Text = "13"
                lblParentFID.Text = arrstr(0)
                constructpath("12")
            Case 0
                loadLevel(ddlLevel1, 1, "")
                lblLevel.Text = "1"
                lblParentFID.Text = "Null"
                constructpath(lblLevel.Text)
            Case Else
                'Response.Redirect("WebPageMgmtMain.aspx")
                trdrp.Visible = False
                lblNoaccess.Visible = True
        End Select

    End Sub

    Protected Sub ddlLevel1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        If Not ddlLevel1.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel1.SelectedValue.Split("|")
            lblLevel.Text = "2"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel2, 2, arrstr(0).Trim)
        Else
            lblLevel.Text = "1"
            lblParentFID.Text = "Null"
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel2.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel2.SelectedValue.Split("|")
            lblLevel.Text = "3"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel3, 3, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel1.SelectedValue.Split("|")
            lblLevel.Text = "2"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel3.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel3.SelectedValue.Split("|")
            lblLevel.Text = "4"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel4, 4, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel2.SelectedValue.Split("|")
            lblLevel.Text = "3"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel4.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel4.SelectedValue.Split("|")
            lblLevel.Text = "5"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel5, 5, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel3.SelectedValue.Split("|")
            lblLevel.Text = "4"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel5.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel5.SelectedValue.Split("|")
            lblLevel.Text = "6"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel6, 6, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel4.SelectedValue.Split("|")
            lblLevel.Text = "5"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel6.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel6.SelectedValue.Split("|")
            lblLevel.Text = "7"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel7, 7, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel5.SelectedValue.Split("|")
            lblLevel.Text = "6"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel7.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel7.SelectedValue.Split("|")
            lblLevel.Text = "8"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel8, 8, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel6.SelectedValue.Split("|")
            lblLevel.Text = "7"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel8_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel8.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel8.SelectedValue.Split("|")
            lblLevel.Text = "9"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel9, 9, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel7.SelectedValue.Split("|")
            lblLevel.Text = "8"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel9_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel9.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel9.SelectedValue.Split("|")
            lblLevel.Text = "10"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel10, 10, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel8.SelectedValue.Split("|")
            lblLevel.Text = "9"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel10_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel10.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel10.SelectedValue.Split("|")
            lblLevel.Text = "11"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel11, 11, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel9.SelectedValue.Split("|")
            lblLevel.Text = "10"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel11.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel11.SelectedValue.Split("|")
            lblLevel.Text = "12"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel12, 12, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel10.SelectedValue.Split("|")
            lblLevel.Text = "11"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel12_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' not needed now
    End Sub
    
    Private Sub loadLevel(ByVal ddl As DropDownList, ByVal level As Integer, ByVal ParentFID As String)
        Dim whereContn As String = ""
        If level > 1 Then
            whereContn = " AND ParentFID=" & ParentFID
        Else
            lblLevel.Text = "1"
            lblParentFID.Text = "Null"
        End If
        Dim strSql As String
        Try
            strSql = "SELECT  Case WHEN Level=1 THEN CONVERT(Varchar,FolderListID) + '|' + FName Else  CONVERT(Varchar,FolderListID) + '|' + FName + '|' + CONVERT(Varchar,ParentFID) END   AS Value, DisplayName FROM FolderTree WHERE Level = " & level & whereContn & " ORDER BY DisplayName"
        Catch ex As Exception
            lblErr.Text = strSql
        End Try
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        ddl.DataSource = ds
        ddl.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            ddl.Items.Insert(0, New ListItem("Select Folder", "0"))
            ddl.Items(0).Selected = True
            ddl.Enabled = True
        Else
            ddl.Enabled = False
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        If TxtFname.Text.Length < 1 Then
            lblErr.Text = "Please Enter Folder Name"
        ElseIf txtDisplayName.Text.Length < 1 Then
            lblErr.Text = "Please Enter Folder Display Name"
        ElseIf btnSubmit.Text = "Add" Then
            Dim strSQl As String = "INSERT INTO  FolderTree(FName, DisplayName, Level, ParentFID, CreateDate, CreatedBy) VALUES ('" & TxtFname.Text & "','" & txtDisplayName.Text & "'," & lblLevel.Text & "," & lblParentFID.Text & ",GetDate()," & Session("LoginID") & ")"
            Try
                If CreateDirectory(lblPath.Text & TxtFname.Text) Then
                    Dim str As String = "SELECT count(*) from FolderTree where  FName = '" & TxtFname.Text & "' AND Level=" & lblLevel.Text & " AND  " & IIf(lblLevel.Text = "1", "ParentFID is Null", "ParentFID=" & lblParentFID.Text)
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, str) < 1 Then
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
                        clearall()
                        tblNew.Visible = False
                        lblError.Text = lblError.Text & "Inserted Successfully"
                    Else
                        lblErr.Text = lblError.Text & "Folder Already exist in Database."
                    End If
                Else
                    lblError.Text = "Folder already Exist in Disk <br>"
                    Exit Sub
                End If
            Catch ex As Exception
                lblErr.Text = strSQl & ex.ToString()
            End Try
        ElseIf btnSubmit.Text = "Update" Then

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(F1.FolderListID) from FolderTree F1, FolderTree F2 WHERE ((F1.ParentFID = F2.ParentFID) OR (F1.ParentFID IS NULL and F2.ParentFID IS NULL)) AND  F2.FName='" & TxtFname.Text & "' and F1.FolderListID = " & lblRenameFolder.Text & " AND F1.FolderListID <> F2.FolderListID") < 1 Then
                If Rename() Then
                    Dim strSQl1 As String = "UPDATE FolderTree SET FName='" & TxtFname.Text & "', DisplayName='" & txtDisplayName.Text & "',ModifyDate=GetDate(),ModifiedBy=" & Session("LoginID") & " WHERE FolderListID=" & lblRenameFolder.Text
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl1)
                    clearall()
                    lblError.Text = "Folder Renamed Successfully"
                Else
                    lblError.Text = "Folder with same Name already Exist in Disk"
                End If
            Else
                lblError.Text = "Folder with same Name already Exist"
            End If

        End If
    End Sub
    Function CreateDirectory(ByVal path As String) As Boolean
        Dim flag As Boolean = False
        Try
            If Directory.Exists(path) Then
                Response.Write("This Folder already Exist")
            Else
                Directory.CreateDirectory(path)
                flag = True
            End If
        Catch ex As Exception
            Response.Write(path)
            Response.Write(ex.ToString)
        End Try
        Return flag
    End Function
    Function Rename() As Boolean
        Dim flag As Boolean = False
        Dim Spath As String = lblRenamePath.Text.Substring(0, lblRenamePath.Text.Length - 1)
        Dim NwPath As String = Spath.Substring(0, Spath.LastIndexOf("\")) & "\" & TxtFname.Text
        NwPath = NwPath.Replace("\", "\\")
        Try
            Directory.Move(Spath, NwPath)
            flag = True
        Catch ex As Exception
            flag = False
        End Try
        Return flag
    End Function
    Private Sub constructpath(ByVal level As String)
        Select Case level
            Case "1"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                DisableDdl(level)
            Case "2"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                DisableDdl(level)
            Case "3"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                DisableDdl(level)
            Case "4"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                DisableDdl(level)
            Case "5"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                DisableDdl(level)
            Case "6"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                DisableDdl(level)
            Case "7"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                DisableDdl(level)
            Case "8"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                DisableDdl(level)
            Case "9"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                DisableDdl(level)
            Case "10"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                DisableDdl(level)
            Case "11"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                DisableDdl(level)
            Case "12"
                lblPath.Text = ServerPath
                lblUrl.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                setPath(ddlLevel11)
                DisableDdl(level)
        End Select
    End Sub
    Private Sub DisableDdl(ByVal level As String)
        Select Case level
            Case "11"
                ddldisable(ddlLevel12)
            Case "10"
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "9"
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "8"
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "7"
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "6"
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "5"
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "4"
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "3"
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "2"
                ddldisable(ddlLevel3)
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "1"
                ddldisable(ddlLevel2)
                ddldisable(ddlLevel3)
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
        End Select
    End Sub
    Private Sub ddldisable(ByVal ddl As DropDownList)
        ddl.Items.Clear()
        ddl.Enabled = False
    End Sub
    Private Sub setPath(ByVal ddl As DropDownList)
        Dim arrstr As String() = ddl.SelectedValue.Split("|")
        lblPath.Text = lblPath.Text & arrstr(1).Trim & "\"
        lblUrl.Text = lblUrl.Text & arrstr(1).Trim & "/"
    End Sub
    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TxtFname.Text = String.Empty
        txtDisplayName.Text = String.Empty
        lblErr.Text = ""
    End Sub
    Protected Sub btnRename_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select  DisplayName,Fname from FolderTree where FolderListID = " & lblParentFID.Text & "")
        If ds.Tables(0).Rows.Count > 0 Then
            txtDisplayName.Text = ds.Tables(0).Rows(0)(0)
            TxtFname.Text = ds.Tables(0).Rows(0)(1)
            lblRenameFolder.Text = lblParentFID.Text
            lblRenamePath.Text = lblPath.Text
            'popoulate valuse in textbox
            tblNew.Visible = True
            btnSubmit.Text = "Update"
            lblrname.Visible = True
        Else
            lblError.Text = "Please select Folder to Rename"
        End If
    End Sub
    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblrname.Visible = False
        btnSubmit.Text = "Add"
        tblNew.Visible = True
    End Sub
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        tblNew.Visible = False
        If Delete() Then
            'Check for Files & Sub Folders inside the Folder

            '"Delete from FolderTree where FolderListID = " & lblParentFID.Text & " and FolderListID not in (select ParentFID from FolderTree WHERE ParentFID is not null and Level=" & Val(lblLevel.Text) & ")
            Dim i As Integer = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DelFolder (FolderListID, FName, DisplayName, [Level], ParentFID, CreateDate, CreatedBy) SELECT FolderListID, FName, DisplayName, [Level], ParentFID, GetDate()," & Session("LoginID") & " FROM FolderTree WHERE FolderListID = " & lblParentFID.Text & "; DELETE FROM FolderTree WHERE FolderListID =" & lblParentFID.Text & "")
            If i = 0 Then
                lblError.Text = lblError.Text & "Not Deleted since it has Sub folders"
            Else
                clearall()
                '***** Delete in all other tables *********
                lblError.Text = "Deleted Successfully"
            End If
        End If
    End Sub
    Protected Sub BtnClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clearall()
    End Sub
    Private Sub clearall()
        lblError.Text = ""
        lblErr.Text = ""
        TxtFname.Text = String.Empty
        txtDisplayName.Text = String.Empty
        tblNew.Visible = False
        'loadLevel(ddlLevel1, 1, "")
        DisableDdl("1")
        constructpath("1")
        lblRenameFolder.Text = String.Empty
        lblRenamePath.Text = String.Empty
        btnSubmit.Text = "Add"
        lblrname.Visible = False
        loadMemberAccess()
    End Sub
    Function Delete() As Boolean
        Dim flag As Boolean = False
        Try
            Directory.Delete(lblPath.Text)
            flag = True
        Catch ex As Exception
            lblError.Text = " The directory in disk is not empty. <br>" 'ex.ToString()
        End Try
        Return flag
    End Function
End Class
