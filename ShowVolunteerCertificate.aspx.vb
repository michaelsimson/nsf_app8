Imports NorthSouth.BAL

Partial Class ShowVolunteerCertificate
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateParticipantCertificates

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadCertificates()
        End If
    End Sub
    Private Sub LoadCertificates()
        Dim dsCertificates As New DataSet
        Dim badge As New Badge()

        generateBadge = CType(Context.Handler, GenerateParticipantCertificates)
        dsCertificates = badge.GetVolunteerCertificates(Application("ConnectionString"), Session("SelChapterID"))
        If dsCertificates.Tables(0).Rows.Count > 0 Then
            rptCertificate.Visible = True
            pnlMessage.Visible = False
            rptCertificate.DataSource = dsCertificates
            rptCertificate.DataBind()
        Else
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found."
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            Response.ContentType = "application/vnd.word"
            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Certificates_Volunteers_" & strChapterName & ".doc")
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            rptCertificate.RenderControl(htmlWrite)
            Response.Write("<html>")
            Response.Write("<head>")
            Response.Write("<style>")
            Response.Write("@page Section1 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("div.Section1 {page:Section1;}")
            Response.Write("@page Section2 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("div.Section2 {page:Section2;}")
            Response.Write("</style>")
            Response.Write("</head>")
            Response.Write("<body>")
            Response.Write("<div class='Section2'>")
            Response.Write(stringWrite.ToString())
            Response.Write("</div>")
            Response.Write("</body>")
            Response.Write("</html>")
            Response.End()
        End If
    End Sub
    Protected Function GetProductName(ByVal ChildNumber As Double) As String
        Dim strProductName As String
        strProductName = ""
        Dim dsProductNames As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, generateBadge.ContestDates, ChildNumber)
        If dsProductNames.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsProductNames.Tables(0).Rows.Count - 1
                If Len(strProductName) > 0 Then
                    If i = dsProductNames.Tables(0).Rows.Count - 1 Then
                        strProductName = strProductName & " and " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    Else
                        strProductName = strProductName & ", " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    End If
                Else
                    strProductName = dsProductNames.Tables(0).Rows(i)(0).ToString()
                End If
            Next
        Else
            strProductName = ""
        End If
        Return strProductName
    End Function

End Class
