<%@ Page Language="C#" AutoEventWireup="false" CodeFile="ShowVolunteerCertificatePDF.aspx.cs" Inherits="ShowVolunteerCertificatePDF" %>
<%@ Reference Page="~/GenerateParticipantCertificates.aspx" %>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
      <style type="text/css" media="screen" >
        <!-- 
        p.MsoNormal, li.MsoNormal, div.MsoNormal
	    {
	        margin:0in;
	        margin-bottom:.0001pt;
	        text-autospace:none;
	        font-size:10.0pt;
	        font-family:"Times New Roman","serif";
	    }  
        @page Section1
	        {
	            size:11.0in 8.5in;
	            margin:.5in 1.0in .5in 1.0in;
	        }
        div.Section1
	        {
	            page:Section1;
	        }
        -->
        </style>
        <script language="javascript" type="text/javascript">
            function printdoc() {
                document.getElementById('btnPrint').style.visibility = "hidden";
                document.getElementById('hlnkMainMenu').style.visibility = "hidden";
                window.print();
                document.getElementById('btnPrint').style.visibility = "visible";
                document.getElementById('hlnkMainMenu').style.visibility = "visible";
                return false;
            }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />
             <asp:Panel ID="Panel1" runat="server">
            <asp:Repeater runat="server" ID="rptCertificate">
                <ItemTemplate>
         <div class="Section1" style="page-break-before:always"> 
               <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0" >                
                 <tr>
                  <td colspan="8">
                  <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0">
                  <tr>
                  <td rowspan="3" align="left" width="20%">
                  
                  <asp:Image runat="server" ID="imgThinkingMan" ImageUrl="http://www.northsouth.org/app8/Images/nsf.jpg"/>                            
                  </td>
                  
                  <td align="left" width="80%">
                  <asp:Image runat="server" ID="imgHeader"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg1.jpg" Width="100%" /><br />
                  </td>
                  </tr>

                  <tr>
                  <td>
              <%--      <% If Session("SelChapterID") = 1 Then%>--%>
                            <asp:Image runat="server" ID="imgTitleNational"  ImageUrl="http://www.northsouth.org/app8/Images/image007_National.gif" Width="90%"/>
                 <%--       <%else %>
                            <asp:Image runat="server" ID="imgTitle"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg2.jpg" Width="90%"/>
                            <%end if %>--%>
                      
                  </td>
                  </tr>
                  </table>
                  </td>
                  </tr>
                              
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                        <br />
                            <asp:Label runat="server" ID="lblTitle" ForeColor="#988600" Text="Certificate of Appreciation" Font-Names="Script MT Bold" Font-Size="36"></asp:Label>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="Label1" ForeColor="#988600" Text="awarded to" Font-Names="Script MT Bold" Font-Size="26"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr> 
                   <%-- <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblName" ForeColor="Blue" Font-Bold="true" Font-Size="22" Text='<%# DataBinder.Eval(Container,"DataItem.FirstName").ToString() & " " & DataBinder.Eval(Container,"DataItem.LastName").ToString() %>'></asp:Label>
                        </td>
                    </tr>--%>
                     <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr> 
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                   <%-- <tr>
                        <td colspan="8" style="text-align:justify;">
                            <b>
                            <font face="Arial">for volunteering and providing exceptional service to the foundation 
                           <asp:Label runat="server" ID="lblContestYear"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.contestyear") %>'></asp:Label>
                            <% If Session("SelChapterID") = 1 Then%>
                            in the National Championship Finals held during  <asp:Label runat="server" ID="Label3"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ContestDate") %>'></asp:Label> at
                            <%Else%>
                            during the 2013 Regional contests held at the
                            <%end if %>
                            <asp:Label runat="server" ID="lblLocation" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label> 
                              <% If Session("SelChapterID") = 1 Then%>
                              .
                               <%Else%>
                               Chapter.
                               <% end if %>
                            
                            </font>
                            </b>
                        </td>
                    </tr>--%>
                     
                    
                     <tr> 
                        <td colspan="8" align="center">
              <asp:Label runat="server" ID="Label4" ForeColor="#988600" Text="We couldn't do it without you!" Font-Names="Georgia" Font-Size="18"></asp:Label>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                            
                            <font face="Arial Narrow"  size="3" style="font-style: italic;" > North South Foundation (NSF) is a non-profit organization involved in implementing educational programs for children in North America and India. The Foundation believes that this world can be a better place to live if the children of today are better prepared to be good citizens of tomorrow. Toward this end, the Foundation encourages children to endeavor to become the best they can be, by giving their best. Further, while it is self-evident that all humans are created equal, it is education that is paramount to actually realizing the rights of equality including life, liberty and the pursuit of happiness as the Founding Fathers of this Nation envisaged more than two hundred years ago.</font>
                        </td>
                    </tr>
                   
                     <tr>
                        <td colspan="8">&nbsp;</td>
                    </tr>
          
                    <tr>
                        <td colspan="8" >
                             <table cellspacing="0" cellpadding="0" width="98%"  align="left" border="0" >                
                                <tr >
                                    <td colspan="3">
                           
                                    </td> 
                                
                                    <td colspan="3" align="center" rowspan="7" valign="top">
                              <asp:Image runat="server" ID="Image1"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg3.jpg" Width="139px" Height="192px"/>

                                    </td> 
                                
                                    <td colspan="3" width="33%" >
                                    <br />
                                    </td> 
                                </tr>
                                  <tr >
                                    <td colspan="3">
                                    <br />
                                    </td> 
                                
                                    <td colspan="3">
                                   
                                    </td> 
                                </tr>        
                        <tr>
                          <td colspan="3" align="left" valign="bottom">
                                    <br />
                                        <hr  />
                                    </td> 
                                      <td colspan="3" align="left" valign="bottom">
                                    <br />
                                        <hr  />
                                    </td> 
                        </tr>
                               <%-- <tr>
                                    <td colspan="3" align="left" valign="top"> 
                                        <asp:Label runat="server" ID="lblLeftTitle" Font-Names="Arial" Font-Size="12" Font-Bold="true"  Text='<%# Session("LeftTitle") %>'></asp:Label>&nbsp;
                                        <asp:Label runat="server" ID="lblLeftSignature" Font-Names="Arial" Font-Size="12" Font-Bold="true" Text='<%# Session("LeftSignatureName") %>'></asp:Label>                                        
       
                                    </td>
                          
                                    <td  colspan="3" align="left" valign="top">
                                        <asp:Label runat="server" ID="lblRightTitle" Font-Size="12"  Font-Names="Arial" Font-Bold="true" Text='<%# Session("RightTitle") %>'></asp:Label>&nbsp;
                                        <asp:Label runat="server" ID="lblRightSignature" Font-Names="Arial" Font-Size="12"  Font-Bold="true" Text='<%# Session("RightSignatureName") %>'></asp:Label>
                             
                                        </td>
                               
                                </tr> --%>
                               <%-- <tr>
                                    <td colspan="3" align="left" valign="top">
                                    <asp:Label runat="server" Font-Names="Arial"  ID="lblSigTitle" Text='<%# Session("LeftSignatureTitle") %>'></asp:Label>

                                    </td>
                                    <td colspan="3" align="left" valign="top">
                                   <asp:Label runat="server" ID="lblRightSigTitle" Font-Names="Arial"  Text='<%# Session("RightSignatureTitle") %>'></asp:Label>

                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="3" ><br /></td>
                                    <td colspan="3" ><br /></td>
                                </tr>
                            </table>
                        </td> 
                    </tr>                   
		       </table> 
	
		       </ItemTemplate>	   
		       </asp:Repeater >
		 </asp:Panel>
		<asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" class="tblMain" cellpadding="0" width="100%"  align="left" border="0" >
                <tr >
                    <td class="Heading">
                        <asp:Label runat="server" ID="lblMessage" ></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
    </form>
</body>
</html>


 
 
 