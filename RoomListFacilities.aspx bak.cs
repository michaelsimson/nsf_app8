﻿
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using NativeExcel;

public partial class RoomListFacilities : System.Web.UI.Page
{

    protected System.Web.UI.WebControls.Button btnAddRow;
    protected System.Web.UI.WebControls.Button btnInsert;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            GetContestYear(ddlContestYear);
            GetEvent(ddlEvent, "1,2");
            GetChapter(ddlChapter);
            CheckEvent();
            int RoleID = Convert.ToInt32(Session["RoleID"]);

            if ((RoleID == 3) || (RoleID == 5))
            {
                ddlChapter.SelectedValue = Session["selChapterID"].ToString();
                ddlChapter.SelectedItem.Text = Session["selChapterName"].ToString();
                ddlChapter.Enabled = false;
            }
            else
            {
                ddlChapter.Enabled = true;
                GetChapter(ddlChapter);
            }
            //GetVendor();
            LoadTables();
            //LoadGrid();
         }
    }

    public void LoadTables()
    {  
        DataTable DT = new DataTable();
        DT.Columns.Add("LabelName");
        DataRow DR;
        DR = DT.NewRow();

        DT.Columns.Add("Col1");
        DT.Columns.Add("Col2");
        DT.Columns.Add("Col3");
        DT.Columns.Add("Col4");
        DT.Columns.Add("Col5");
        DT.Columns.Add("Col6");
        DT.Columns.Add("Col7");
        DT.Columns.Add("Col8");
        DT.Columns.Add("Col9");
        DT.Columns.Add("Col10");
        DT.Columns.Add("Col11");
        DT.Columns.Add("Col12");
        DT.Columns.Add("Col13");
        DT.Columns.Add("Col14");
        DT.Columns.Add("Col15");

        DT.Rows.Add("Room Number");
        DT.Rows.Add("Floor");
        DT.Rows.Add("Capacity");
        DT.Rows.Add("Gallery");
        DT.Rows.Add("Flippers to write");
        DT.Rows.Add("Tables to write");
        DT.Rows.Add("Fixed Chairs");
        DT.Rows.Add("Podium");
        DT.Rows.Add("Podium Mic");
        DT.Rows.Add("Podium Laptop");
        DT.Rows.Add("Projector");
        DT.Rows.Add("Judge Table");
        DT.Rows.Add("Judge Chairs");
        DT.Rows.Add("Chief Judge Mic");
        DT.Rows.Add("Child Mic");
        DT.Rows.Add("Audio Mixer");
        DT.Rows.Add("Child Mic Stand");
        DT.Rows.Add("Desk Mic Stand");
        DT.Rows.Add("Audio Wires");
        DT.Rows.Add("Extra Mic");
        DT.Rows.Add("Internet Access");
        DT.Rows.Add("Laptop");
        DT.Rows.Add("Table");
        DT.Rows.Add("Chairs");
        DT.Rows.Add("MicStand");

        Session.Add("DT", DT);      
        DataGridRoom.DataSource = DT;
        DataGridRoom.DataBind();
        
    }
   
    private void CheckEvent()
    {
        if (ddlEvent.SelectedValue == "1")
        {
            ddlChapter.SelectedValue = "1";
            ddlChapter.SelectedItem.Text = "Home";
            ddlChapter.Enabled = false;
        }
        else
        {
            ddlChapter.Enabled = true;
        }
    }
   
    private void GetContestYear(DropDownList ddlObject)
    {
        int[] year = new int[5];
        year[0] = DateTime.Now.Year;
        year[1] = DateTime.Now.AddYears(-1).Year;
        year[2] = DateTime.Now.AddYears(-2).Year;
        year[3] = DateTime.Now.AddYears(-3).Year;
        year[4] = DateTime.Now.AddYears(-4).Year;
        ddlObject.DataSource = year;
        ddlObject.DataBind();
        //ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetEvent(DropDownList ddlObject, String EvntIDs)
    {
        DataSet dsEvent = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Name,EventId  from Event where EventID in (" + EvntIDs + ") Group by Name,EventId");
        ddlObject.DataSource = dsEvent;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "EventId";
        ddlObject.DataBind();
        if (dsEvent.Tables[0].Rows.Count > 1)
        {
            ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
        }
        else
            ddlObject.Enabled = false;
    }
    private void GetChapter(DropDownList ddlObject)
    {
        DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetChapterAll");
        ddlObject.DataSource = ds_Chapter;
        ddlObject.DataTextField = "ChapterCode";
        ddlObject.DataValueField = "ChapterId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select Chapter", "-1"));
        ddlObject.SelectedIndex = 0;
    }
     
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetChapter(ddlChapter);
        CheckEvent();
        LoadGrid();
    }
    protected void DataGridRoom_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        DataTable dt, dt1, dt2;
        dt = new DataTable();

        dt.Columns.Add("ddlText", Type.GetType("System.String"));
        dt.Columns.Add("ddlValue", Type.GetType("System.Int32"));
        dt.Rows.Add("Select", 0);
        for (int l = 1; l <= 5; l++)
        {
            dt.Rows.Add(l, l);
        }

        dt1 = new DataTable();
        dt1.Columns.Add("ddlText", Type.GetType("System.String"));
        dt1.Columns.Add("ddlValue", Type.GetType("System.Int32"));
        for (int j = 0; j < 2; j++)
        {
             dt1.Rows.Add(j , j);
        }

        dt2 = new DataTable();
        dt2.Columns.Add("ddlText", Type.GetType("System.String"));
        dt2.Columns.Add("ddlValue", Type.GetType("System.String"));
        dt2.Rows.Add('N', 'N');
        dt2.Rows.Add('Y', 'Y');
        
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            //string[] optionsCount = { "Select", "1", "2", "3", "4", "5" };
            //string[] optCount ={"0", "1"};
            //string[] optionsBool = { "Y", "N" };
          
            Label LabelRow = new Label();
            LabelRow = (Label)e.Item.FindControl("LabelName");
                      
             if (LabelRow.Text == "Floor" || LabelRow.Text == "Podium" || LabelRow.Text == "Podium Mic" || LabelRow.Text == "Podium Laptop" || LabelRow.Text == "Projector" ||
                 LabelRow.Text == "Judge Table" || LabelRow.Text == "Judge Chairs" ||  LabelRow.Text == "Audio Wires" || LabelRow.Text == "Extra Mic" ||
                 LabelRow.Text == "Laptop" || LabelRow.Text == "MicStand")
            {
                   for (int i = 1; i <= 15; i++)
                    {
                        
                            DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                            list.DataSource = dt; //optionsCount;
                            list.DataBind();
                     }
                  
            }
             else if (LabelRow.Text == "Chief Judge Mic" || LabelRow.Text == "Child Mic" || LabelRow.Text == "Audio Mixer" ||
                      LabelRow.Text == "Audio Mixer" || LabelRow.Text == "Child Mic Stand" || LabelRow.Text == "Desk Mic Stand" || LabelRow.Text == "Internet Access")
             {
                 for (int i = 1; i <= 15; i++)
                 {
                         DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                         list.DataSource = dt1;// optCount;
                         list.DataBind();
                 }
             }

            else if (LabelRow.Text == "Gallery" || LabelRow.Text == "Flippers to write" || LabelRow.Text == "Tables to write" || LabelRow.Text == "Fixed Chairs") 
            {
                for (int i = 1; i <= 15; i++)
                {
                        DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                        list.DataSource = dt2;// optionsBool;
                        list.DataBind();
                }
            }
           else if (LabelRow.Text == "Room Number" || LabelRow.Text == "Capacity" || LabelRow.Text == "Table" || LabelRow.Text == "Chairs" )
           {
               for (int i = 1; i <= 15; i++)
               {
                       DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                       TextBox txtbx = (TextBox)e.Item.FindControl("TextCol" + i);
                       txtbx.Visible = true;
                       list.Visible = false;
                       txtbx.DataBind();
                }
            }
        }
    }

   
    protected void BtnAddUpdate_Click(object sender, EventArgs e)
    {
        String SQLInsertCmd = "";
        String SQLUpdateCmd = "";
        String SQLUpdateValue = "";
        String SQLVal = "";
        String SQLExec = "";
        String SQLUpdatExec = "";
        String SQLEvalStr = "";
        Boolean AddUpdateFlag = false;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        LblError.Text = " ";
        LblAddUpdate.Text ="";

        if (ddlEvent.SelectedValue == "-1")
        {
            LblError.Text = " Please Select Event"; //LblError.Text +
            goto Execute;
        }
        else if (ddlChapter.SelectedValue == "-1")
        {
            LblError.Text = " Please Select Chapter";
            goto Execute;
        }
        else if (TextVendor.Text == "")
        {
            LblError.Text = " Enter Vendor Name";
            goto Execute;
        }
        else if (TextBldgID.Text == "")
        {
            LblError.Text = " Enter Building(Bldg) ID";
            goto Execute;
        }
        else if (TextBldgName.Text == "")
        {
            LblError.Text = " Enter Building(Bldg) Name";
            goto Execute;
        }

        String RoomNumber;//= new String[16];
        RoomNumber = "";
        String CurrRoomNumber = "";
        int ColumnCount = DataGridRoom.Columns.Count;

        for (int i = 1; i < ColumnCount; i++)
        {
             SQLInsertCmd = "Insert into RoomListFacilities (ContestYear,EventID,Event,ChapterID,Chapter,Vendor,BldgID,BldgName,RoomNumber,Floor,Capacity,Gallery,Flipperstowrite,Tablestowrite,FixedChairs,Podium,PodMic,PodLT,Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,MicStand,CreatedBy,CreatedDate) Values ( " + ddlContestYear.SelectedValue + "," + ddlEvent.SelectedValue + ",'" + ddlEvent.SelectedItem.Text+ "'," + ddlChapter .SelectedValue + ",'" + ddlChapter.SelectedItem.Text + "','" + TextVendor.Text + "','" + TextBldgID.Text + "','" + TextBldgName.Text + "'," ;
             SQLUpdateCmd = "Update RoomListFacilities Set ContestYear=" + ddlContestYear.SelectedValue + ",EventID=" + ddlEvent.SelectedValue + ",Event='" + ddlEvent.SelectedItem.Text + "',ChapterID=" + ddlChapter.SelectedValue + ",Chapter='" + ddlChapter.SelectedItem.Text + "',Vendor='" + TextVendor.Text + "',BldgID='" + TextBldgID.Text + "',BldgName='" + TextBldgName.Text + "',";
             SQLUpdateValue = "";
             SQLVal = "";
             
              foreach (DataGridItem dg in DataGridRoom.Items)
              {
                  Label label = (Label)(dg.Cells[0].Controls[1]);//.FindControl(("Room Number")));
                  String GridRowValue = "";
                  String textValue = "";
                  TextBox textVal = ((TextBox)(dg.FindControl(("TextCol") + i))) ;
                  String ddlValue = ((DropDownList)dg.Cells[i].Controls[1]).SelectedValue;
                  
                  textValue =  textVal.Text ;
                  
                  GridRowValue= "'" + textValue + ddlValue + "',";

                  SQLVal = SQLVal + GridRowValue ;//"'" + textValue + ddlValue + "'," ;// +","; // textval + dropdown + ",";//
           
                  if (label.Text == "Room Number")
                  {
                      if (textValue != "")
                      {
                          bool inRooms = (RoomNumber).Contains("'" + textValue + "'");
                          if (inRooms == true)
                          {
                              LblError.Text = "Room Number must be Unique";
                              goto Execute;
                          }
                          else
                          {
                              RoomNumber = RoomNumber + "'" + textValue + "',";//check + ","; // 
                          }
                      }
                      else if (textValue=="")
                      {
                          //LblError.Text = "Missing Room Number in Column" + i;
                          goto Execute;
                      }
                     SQLUpdateValue = SQLUpdateValue + "RoomNumber=" + GridRowValue;
                     SQLEvalStr = "Select Count(*) From RoomListFacilities Where ChapterID=" + ddlChapter.SelectedValue + " And ContestYear=" + ddlContestYear.SelectedValue + " And BldgID='" + TextBldgID.Text + "' AND RoomNumber='" + textValue + "'";
                     CurrRoomNumber = textValue;
                  }
                  else if (label.Text == "Floor")
                  { 
                        SQLUpdateValue = SQLUpdateValue + "Floor=" + GridRowValue; 
                  }
                  else if (label.Text == "Capacity")
                  {
                      SQLUpdateValue = SQLUpdateValue + "Capacity=" + GridRowValue; 
                  }
                  else if (label.Text =="Gallery")
                  {
                      SQLUpdateValue = SQLUpdateValue + "Gallery=" + GridRowValue; 
                  }
                  else if (label.Text =="Flippers to write")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Flipperstowrite="+ GridRowValue; 
                  }
                  else if (label.Text =="Tables to write")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Tablestowrite=" + GridRowValue;           
                  }
                  else if (label.Text =="Fixed Chairs")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "FixedChairs="+ GridRowValue;                
                  }
                  else if (label.Text =="Podium")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Podium=" + GridRowValue;       
                  }
                  else if (label.Text =="Podium Mic")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "PodMic=" + GridRowValue;        
                  }
                  else if (label.Text =="Podium Laptop")
                  {
                          SQLUpdateValue = SQLUpdateValue + "PodLT=" + GridRowValue;        
                  }
                  else if (label.Text =="Projector")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Projector=" + GridRowValue;        
                  }
                  else if (label.Text =="Judge Table")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "JTable=" + GridRowValue;       
                  }
                  else if (label.Text =="Judge Chairs")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "JChair=" + GridRowValue;       
                  }
                  else if (label.Text =="Chief Judge Mic")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "CJMic=" + GridRowValue;       
                  }
                  else if (label.Text =="Child Mic")
                  { 
                           SQLUpdateValue = SQLUpdateValue +"ChildMic=" + GridRowValue;      
                  }
                  else if (label.Text =="Audio Mixer")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "AMixer=" + GridRowValue;        
                  }
                  else if (label.Text =="Child Mic Stand")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "ChMicStand=" + GridRowValue;       
                  }
                  else if (label.Text =="Desk Mic Stand")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "DeskMicStand=" + GridRowValue;      
                  }
                  else if (label.Text =="Audio Wires")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "AWires=" + GridRowValue;     
                  }
                  else if (label.Text =="Extra Mic")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "ExtraMic=" + GridRowValue;   
                  }
                  else if (label.Text =="Internet Access")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "IntAccess="+ GridRowValue;      
                  }
                  else if (label.Text =="Laptop")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "Laptop="+ GridRowValue;      
                  }
                  else if (label.Text =="Table")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "Tables="+ GridRowValue;      
                  }
                  else if (label.Text =="Chairs")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "Chair=" + GridRowValue;       
                  }
                  else if (label.Text =="MicStand")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "MicStand=" + GridRowValue;      
                  }

            }

              if (Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, SQLEvalStr)) > 0)
                  SQLUpdatExec = SQLUpdatExec + SQLUpdateCmd + SQLUpdateValue + "ModifiedBy=" + Session["LoginID"] + ",ModifiedDate=GETDATE() Where ChapterID=" + ddlChapter.SelectedValue + " And ContestYear=" + ddlContestYear.SelectedValue + " And BldgID='" + TextBldgID.Text + "' AND RoomNumber='" + CurrRoomNumber + "'";
              else  
                  SQLExec = SQLExec + SQLInsertCmd + SQLVal + Session["LoginID"] + ",GETDATE())";              
         }
        

   Execute:
        
        if (LblError.Text == " ")
        {
            try
            {
                conn.Open();
                if (SQLExec.Length > 20)
                {
                    SqlCommand cmdIns = new SqlCommand(SQLExec, conn);
                    cmdIns.ExecuteNonQuery();
                    AddUpdateFlag =true;
                }
                if (SQLUpdatExec.Length > 20)
                {
                    SqlCommand cmdIns = new SqlCommand(SQLUpdatExec, conn);
                    cmdIns.ExecuteNonQuery();
                    AddUpdateFlag = true;
                }

                if (AddUpdateFlag == true)
                {
                    LblAddUpdate.Text = "Added/Updated Successfully";
                    LoadGrid();
                }
                conn.Close(); 
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        else
        {
            LblAddUpdate.Text = "No Records Added";
        }
    }
    protected void BtnUpdateCancel_Click(object sender, EventArgs e)
    {
        GetContestYear(ddlContestYear);
        GetEvent(ddlEvent, "1,2");
        GetChapter(ddlChapter);
        CheckEvent();
        TextVendor.Text = "";
        TextBldgID.Text = "";
        TextBldgName.Text = "";
        LblAddUpdate.Text = "";
        LblError.Text = "";
        DGRoomList.Visible = false;

        int ColumnCount = DataGridRoom.Columns.Count;
        for (int i = 1; i < ColumnCount; i++)
        {
            foreach (DataGridItem dg in DataGridRoom.Items)
            {
                TextBox textboxClear = ((TextBox)(dg.FindControl(("TextCol") + i))) ;
                textboxClear.Text = "";
                ((DropDownList)dg.Cells[i].Controls[1]).ClearSelection();
            }
        }
    }

    private void LoadGrid()
    {
            DGRoomList.Visible = true;
            String strSQL ="";
            
            strSQL = "SELECT Chapter, ContestYear, BldgName, BldgID, RoomNumber,Floor ,Capacity,Gallery,Flipperstowrite,Tablestowrite,FixedChairs,Podium,PodMic,PodLT,";
            strSQL =  strSQL+" Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,MicStand ";
            strSQL =  strSQL+" FROM RoomListFacilities WHERE ContestYear=" + ddlContestYear.SelectedValue +" And EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "";

            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strSQL);
            DGRoomList.DataSource = ds;
            DGRoomList.DataBind();

            if (ds.Tables[0].Rows.Count <1)
                LblRecError.Text = "No records available.";
            else
                LblRecError.Text = "";


            //for (int i = 0; i < ds.Tables[0].Rows.Count;i++ )
            //{
            //    foreach (DataGridItem dg in DataGridRoom.Items)
            //    {
            //        TextBox textboxClear = ((TextBox)(dg.FindControl(("TextCol") + i)));

            //       ((TextBox)(dg.FindControl(("TextCol") + i))).Text 
            //       // textboxClear.Text = "";
            //        ((DropDownList)dg.Cells[i].Controls[1]).ClearSelection();
            //    }
            //    ddlBldgName.SelectedIndex = ddlBldgName.Items.IndexOf(ddlBldgName.Items.FindByValue("ddlBldgName"));

            //}






        
    //        SqlConnection connection = new SqlConnection(Application["ConnectionString"].ToString());
    //        connection.Open();
    //        SqlCommand myCommand = new SqlCommand(strSQL, connection);
    //        SqlDataAdapter RoomAdapter = new SqlDataAdapter(myCommand);
    //        DataTable dt= new DataTable ();
    //        RoomAdapter.Fill(dt);
    //        int Count = dt.Rows.Count;
    //        DataView dv = new DataView(dt);
    //        DGRoomList.DataSource = dt;
    //        DGRoomList.DataBind();
           //if  (Count < 1)
            //    LblRecError.Text = "No records available.";
            //else
            //    LblRecError.Text = "";
  
     }


    protected void ddlChapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadGrid();
        LoadVendor(ddlVendor);
        
     }
    private void LoadVendor(DropDownList ddlVendor)
    {
        String StrSQL = "Select ORGANIZATION_NAME,MEMBERID From OrganizationInfo Where  ChapterId=" + ddlChapter.SelectedValue + "Order By ORGANIZATION_NAME";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);

        ddlVendor.DataSource = ds;
        ddlVendor.DataTextField = "ORGANIZATION_NAME";
        ddlVendor.DataValueField = "MEMBERID";
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem("Select Vendor", "-1"));
        ddlVendor.SelectedIndex = 0;
    }
    private void LoadBuildingID(DropDownList ddlBldgID)
    {
        Response.Write("Enters");
        try
        {
            String StrSQL = "Select BldgID,BldgName From RoomListFacilities Where  ContestYear= " + ddlContestYear.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + " and Vendor='" + ddlVendor.SelectedItem.Text.Trim() + "'";
            DataSet ds = new DataSet();
            Response.Write(StrSQL);
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        
        ddlBldgID.DataSource = ds;
        ddlBldgID.DataTextField = "BldgID";
        ddlBldgID.DataValueField = "BldgID";
        ddlBldgID.DataBind();

        ddlBldgName.DataSource = ds;
        ddlBldgName.DataTextField = "BldgName";
        ddlBldgName.DataValueField = "BldgName";
        ddlBldgName.DataBind();

        //if (ds.Tables[0].Rows.Count == 1)
        //{
        //    ddlBldgName.SelectedIndex = ddlBldgName.Items.IndexOf(ddlBldgName.Items.FindByValue("ddlBldgName"));
        //}
  ddlBldgID.Items.Insert(ds.Tables[0].Rows.Count , new ListItem("Other"));
  ddlBldgName.Items.Insert(ds.Tables[0].Rows.Count , new ListItem("Other"));
       


       // ddlBldgID.Items.Add("TextBox");// ert(0, new ListItem("Select Vendor", "-1"));
      //  ddlBldgID.SelectedIndex = 0;
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }

    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)  
    {
        LoadBuildingID(ddlBldgID);
    }

    protected void  ddlBldgID_SelectedIndexChanged(object sender, EventArgs e)
    { 
        //String StrSQL = "Select BldgId,BldgName From RoomListFacilities Where  ContestYear= " + ddlContestYear.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + " and Vendor='" + ddlVendor.SelectedItem.Text.Trim() + "' and BldgID='" + ddlBldgID.SelectedItem.Text + "'";
        //DataSet ds = new DataSet();
        //Response.Write(StrSQL);
        //ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);

        //ddlBldgName.DataSource = ds;
        //ddlBldgName.DataTextField = "BldgName";
        //ddlBldgName.DataValueField = "BldgName";
        //ddlBldgName.DataBind();
  
      
        //if (ds.Tables[0].Rows.Count == 1)
        //{
        //    ddlBldgName.SelectedIndex = ddlBldgName.Items.IndexOf(ddlBldgName.Items.FindByValue("BldgName"));//
        //}
        ddlBldgName.SelectedIndex = ddlBldgID.SelectedIndex;

        AddTextBox();
        

    }
    protected void ddlBldgName_SelectedIndexChanged(object sender, EventArgs e)
    {
        AddTextBox();
    }
    private void AddTextBox()
    {
        if (ddlBldgID.SelectedItem.Text == "Other")
        {
            TextBldgID.Visible = true;
        }
        else
        {
            TextBldgID.Visible = false;
        }
        if (ddlBldgName.SelectedItem.Text == "Other")
        {
            TextBldgName.Visible = true;
        }
        else
        {
            TextBldgName.Visible = false;
        }
    }
}
