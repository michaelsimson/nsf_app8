Imports NorthSouth.BAL
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data

Partial Class ShowRankCertificate
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateParticipantCertificates

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadCertificates()
        End If
    End Sub
    Private Sub LoadCertificates()
        Dim dsCertificates As New DataSet
        Dim badge As New Badge()
        Try
            generateBadge = CType(Context.Handler, GenerateParticipantCertificates)
            Response.Write(generateBadge)
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
        Try
            dsCertificates = badge.GetRankCertificates(Application("ConnectionString"), Session("SelChapterID"), Now.Year - 1, generateBadge.ContestDates)
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try

        If dsCertificates.Tables(0).Rows.Count > 0 Then
            rptCertificate.Visible = True
            pnlMessage.Visible = False
            rptCertificate.DataSource = dsCertificates
            rptCertificate.DataBind()
        Else
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found."
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            Response.ContentType = "application/vnd.word"
            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Certificates_Rank_" & strChapterName & ".doc")
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            rptCertificate.RenderControl(htmlWrite)
            Response.Write("<html>")
            Response.Write("<head>")
            Response.Write("<style>")
            Response.Write("@page Section1 {size:11.69in 8.27in;mso-page-orientation:landscape;margin:0.7in 1.5in 0.5in 1.5in;mso-header-margin:0.5in;mso-footer-margin:0.25in;mso-paper-source:0;}")
            'Response.Write("@page Section1 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("div.Section1 {page:Section1;}")
            Response.Write("@page Section2 {size:11.69in 8.27in;mso-page-orientation:landscape;margin:0.7in 1.5in 0.5in 1.5in;mso-header-margin:0.5in;mso-footer-margin:0.25in;mso-paper-source:0;}")
            'Response.Write("@page Section2 {size:11.69in 8.5in;margin:1.0in 1.5in 1.0in 1.0in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:0;}")
            Response.Write("div.Section2 {page:Section2;}")
            Response.Write("</style>")
            Response.Write("</head>")
            Response.Write("<body>")
            Response.Write("<div class='Section2'>")
            Response.Write(stringWrite.ToString())
            Response.Write("</div>")
            Response.Write("</body>")
            Response.Write("</html>")
            Response.End()
        End If
    End Sub
    Protected Function GetLeftSignatureName(ByVal ProductCode As String) As String
        Dim strLeftSignatureName As String
        strLeftSignatureName = ""
        Dim dsLeftSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsLeftSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsLeftSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsLeftSignatureList.Tables(0).Rows.Count - 1
                strLeftSignatureName = dsLeftSignatureList.Tables(0).Rows(i)(6).ToString() & " " & dsLeftSignatureList.Tables(0).Rows(i)(0).ToString() & " " & dsLeftSignatureList.Tables(0).Rows(i)(1).ToString()
            Next
        Else
            strLeftSignatureName = ""
        End If
        Return strLeftSignatureName
    End Function
    Protected Function GetLeftSignatureTitle(ByVal ProductCode As String) As String
        Dim strLeftSignatureTitle As String
        strLeftSignatureTitle = ""
        Dim dsLeftSignatureTitle As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsLeftSignatureTitle = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsLeftSignatureTitle.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsLeftSignatureTitle.Tables(0).Rows.Count - 1
                strLeftSignatureTitle = dsLeftSignatureTitle.Tables(0).Rows(i)(2).ToString()
            Next
        Else
            strLeftSignatureTitle = ""
        End If
        Return strLeftSignatureTitle
    End Function
    Protected Function GetRightSignatureName(ByVal ProductCode As String) As String
        Dim strRightSignatureName As String
        strRightSignatureName = ""
        Dim dsRightSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureList.Tables(0).Rows.Count - 1
                strRightSignatureName = dsRightSignatureList.Tables(0).Rows(i)(7).ToString() & " " & dsRightSignatureList.Tables(0).Rows(i)(3).ToString() & " " & dsRightSignatureList.Tables(0).Rows(i)(4).ToString()
            Next
        Else
            strRightSignatureName = ""
        End If
        Return strRightSignatureName
    End Function

    Protected Function GetLeftSignature(ByVal ProductCode As String) As String
        Dim strRightSignatureName As String
        strRightSignatureName = ""
        Dim dsRightSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureList.Tables(0).Rows.Count - 1
                strRightSignatureName = dsRightSignatureList.Tables(0).Rows(i)(8).ToString()
            Next
        Else
            strRightSignatureName = ""
        End If
        Return strRightSignatureName
    End Function

    Protected Function GetRightSignature(ByVal ProductCode As String) As String
        Dim strRightSignatureName As String
        strRightSignatureName = ""
        Dim dsRightSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureList.Tables(0).Rows.Count - 1
                strRightSignatureName = dsRightSignatureList.Tables(0).Rows(i)(9).ToString()
            Next
        Else
            strRightSignatureName = ""
        End If
        Return strRightSignatureName
    End Function

    Protected Function GetRightSignatureTitle(ByVal ProductCode As String) As String
        Dim strRightSignatureTitle As String
        strRightSignatureTitle = ""
        Dim dsRightSignatureTitle As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureTitle = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureTitle.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureTitle.Tables(0).Rows.Count - 1
                strRightSignatureTitle = dsRightSignatureTitle.Tables(0).Rows(i)(5).ToString()
            Next
        Else
            strRightSignatureTitle = ""
        End If
        Return strRightSignatureTitle
    End Function

    Public Function ShowImage(ByVal imageURL As String, ByVal spacerURL As String, ByVal imagePath As String)

        Dim f As New IO.FileInfo(Server.MapPath(imagePath))
        If f.Exists Then

            Return imageURL
        Else
            Return spacerURL
        End If

    End Function

 
End Class
