﻿using System;
using System.Data;

using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;


public partial class VocabularyTestPapers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            //Session["LoginID"] = 4240;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

                
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= (DateTime.Now.Year-5); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }



          
           
        }


       
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }







    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEvent.SelectedIndex == 1)
        {
            ddlSet.Items[0].Attributes.Add("style", "display:none");
            ddlSet.Items[1].Attributes.Add("style", "display:none");
            ddlSet.Items[2].Attributes.Add("style", "display:none");
            ddlSet.Items[3].Attributes.Add("style", "display:none");
            ddlSet.SelectedIndex = 4;
            ddlSet.Enabled = false;
        }
         else if(ddlEvent.SelectedIndex==2)
        {
            ddlSet.Enabled = true ;
            ddlSet.SelectedIndex = 1;
            ddlPhase.Items[2].Attributes.Add("style", "display: none");
            ddlSet.Items[4].Attributes.Add("style", "display:none");
            
        }
       
    
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //|| ddlAction.SelectedIndex == 0 || ddlProduct.SelectedIndex == 0 || ddlSet.SelectedIndex == 0)
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            gvVocabwords_new.Visible = false;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            gvVocabwords_new.Visible = false;
        }
        else if (ddlSet.SelectedIndex == 0)
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Set";
            gvVocabwords_new.Visible = false;
        }
        else if (ddlAction.SelectedIndex == 0)
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Action";
            gvVocabwords_new.Visible = false;
        }
        else
        {
            if (ddlYear.SelectedIndex != 0 && ddlAction.SelectedIndex == 1)
            {
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
                lblErr.Text = "Not allowed";
            }
            else
            {
                lblErr.Text = "";
                if (ddlAction.SelectedIndex == 1)
                {
                    if (ddlPhase.SelectedIndex == 0)  //For Phase: I
                    {
                        gvVocabwords_new.Columns[0].Visible = false;
                        string strqry = "";
                        string productstr = "";
                        if (ddlProduct.SelectedItem.Value.Equals("JVB"))
                        {
                            productstr = " and vn.level=1";
                        }
                        else if (ddlProduct.SelectedItem.Value.Equals("IVB"))
                        {
                            productstr = " and vn.level=2";
                        }
                        int j = SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "delete VocabTPWords where Year=" + ddlYear.SelectedValue+" and Event='"+ddlEvent.SelectedItem.Text+"' and ProductCode='"+ddlProduct.SelectedValue+"' and Phase=1");
                        if (ddlEvent.SelectedIndex == 2)
                        {
                            // Getting Published records from Vocabwords_new and insert into Temporary Table

                            strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempVocabTPWords')) begin drop table TempVocabTPWords end create table TempVocabTPWords(InitialWordSeqID int identity(1,1) primary key,Word nvarchar(50),RandomSeqID int) insert into TempVocabTPWords(Word) ((select Word from Vocabwords_new vn where vn.Year=" + ddlYear.SelectedValue + " " + productstr + " and Regional ='Y'))";
                            int i = SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, strqry);
                            //Randomizing the set of records in TempTable and feeded into same table
                            DataSet dsEasy = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select [InitialWordSeqID],[Word],RandomSeqID from TempVocabTPWords");
                            DataTable dtEasy = dsEasy.Tables[0];
                            DataTable dtnew = (DataTable)dtEasy.Clone();
                            //dtnew.Columns.Add("RandomizedSeqID");
                            int k = 0;
                            string updateqry="";
                            List<int> a = GetRandomNumbers(dtEasy.Rows.Count);
                            if (dtEasy.Rows.Count >= 0)
                            {
                                for (k = 0; k < dtEasy.Rows.Count; k++)
                                {
                                    dtnew.ImportRow(dtEasy.Rows[a[k]]);
                                    dtnew.Rows[k]["RandomSeqID"] = k + 1;

                                    updateqry = updateqry + "update TempVocabTPWords set RandomSeqID ="

                                          + dtnew.Rows[k]["RandomSeqID"].ToString().Trim() + " where InitialWordSeqID="
                                          + dtnew.Rows[k]["InitialWordSeqID"].ToString().Trim() + " ";
                                }
                            }
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);

                            //strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempVocabTPWords')) begin drop table TempVocabTPWords end create table TempVocabTPWords(TempVocabWordsID int identity(1,1) primary key,Word nvarchar(50),Unpub nvarchar(25), Level int, SubLevel int,Source nvarchar(50), Year int, EventID int,Event nvarchar(20), ProductGroupID int, ProductGroupCode nvarchar(20), ProductID int, ProductCode nvarchar(20), SetNo int, Phase int, CreatedBy int, CreateDate date) insert into TempVocabTPWords(Word,Unpub, Level, SubLevel,Source, Year, EventID,Event, ProductGroupID, ProductGroupCode, ProductID, ProductCode, SetNo, Phase, CreatedBy, CreateDate) ((select Word, case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as UnPub, Level, [Sub-level],Source, Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID, (select EventCode from event where Eventid=" + ddlEvent.SelectedValue + ") as Event,( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode," + ddlSet.SelectedValue + " as SetNo, case " + ddlPhase.SelectedValue + " when 1 then 1 when 2 then 2 when 3 then 3 end as Phase," + Session["LoginID"] + " as CreatedBy, convert(varchar(20),getdate(),101) as CreateDate from Vocabwords_new vn where vn.Year=" + ddlYear.SelectedValue + " " + productstr + " and Regional ='Y'))";
                           // strqry="insert into VocabTPWords(Word,Unpub, Level, SubLevel,Source, Year, EventID,Event, ProductGroupID, ProductGroupCode, ProductID, ProductCode, SetNo, Phase, CreatedBy, CreateDate) ((select Word, case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as UnPub, Level, [Sub-level],Source, Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID, (select EventCode from event where Eventid=" + ddlEvent.SelectedValue + ") as Event,( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode," + ddlSet.SelectedValue + " as SetNo, case " + ddlPhase.SelectedValue + " when 1 then 1 when 2 then 2 when 3 then 3 end as Phase," + Session["LoginID"] + " as CreatedBy, convert(varchar(20),getdate(),101) as CreateDate from Vocabwords_new vn where vn.Year=" + ddlYear.SelectedValue + " " + productstr + " and Regional ='Y' union select Word, case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as UnPub, Level, [Sub-level],Source, Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID, (select EventCode from event where Eventid=" + ddlEvent.SelectedValue + ") as Event,( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode," + ddlSet.SelectedValue + " as SetNo, case " + ddlPhase.SelectedValue + " when 1 then 1 when 2 then 2 when 3 then 3 end as Phase," + Session["LoginID"] + " as CreatedBy, convert(varchar(20),getdate(),101) as CreateDate from Vocabwords_new vn where vn.Year=" + ddlYear.SelectedValue + " " + productstr + " and Reg_Reserved ='Y' )) ";



                        }
                        else if (ddlEvent.SelectedIndex == 1)
                        {
                            //strqry = "insert into VocabTPWords(Word,Unpub, Level, SubLevel,Source, Year, EventID,Event, ProductGroupID, ProductGroupCode, ProductID, ProductCode, SetNo, Phase, CreatedBy, CreateDate) ((select Word, case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as UnPub, Level, [Sub-level],Source, Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID, (select EventCode from event where Eventid=" + ddlEvent.SelectedValue + ") as Event,( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode," + ddlSet.SelectedValue + " as SetNo, case " + ddlPhase.SelectedValue + " when 1 then 1 when 2 then 2 when 3 then 3 end as Phase," + Session["LoginID"] + " as CreatedBy, convert(varchar(20),getdate(),101) as CreateDate from Vocabwords_new vn where vn.Year=" + ddlYear.SelectedValue + " " + productstr + " and [National] ='Y' union select Word, case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as UnPub, Level, [Sub-level],Source, Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID, (select EventCode from event where Eventid=" + ddlEvent.SelectedValue + ") as Event,( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode," + ddlSet.SelectedValue + " as SetNo, case " + ddlPhase.SelectedValue + " when 1 then 1 when 2 then 2 when 3 then 3 end as Phase," + Session["LoginID"] + " as CreatedBy, convert(varchar(20),getdate(),101) as CreateDate from Vocabwords_new vn where vn.Year=" + ddlYear.SelectedValue + " " + productstr + " and National_Reserved ='Y' )) ";
                        }


                       // inserting set of records into VocabTPWords SQL Table
                       

                       // //Phase:1-> Getting 10 PUBLISHED words from Source 'JVBList' 
                       //     // initially 5 from Sub-level=1 ie., EASY
                       //         DataSet dsEasy = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select [VocabTPID],[Word],[Unpub],[Level],[SubLevel],[Year],[EventID],[Event],[ProductGroupID],[ProductGroupCode],[ProductID],[ProductCode],[SetNo],[Phase],[CreatedBy],convert(varchar(20),[CreateDate],101) [CreateDate],[ModifiedBy],convert(varchar(20),[ModifyDate],101) [ModifyDate] from VocabTPWords where Year=" + ddlYear.SelectedValue + " and Event='" + ddlEvent.SelectedItem.Text + "' and ProductCode='" + ddlProduct.SelectedValue + "' and SetNo=" + ddlSet.SelectedValue + " and Phase=1 and Unpub is null and Source='JVBList' and Sublevel=1");
                       //         DataTable dtEasy = dsEasy.Tables[0];
                       //         DataTable dtnew = (DataTable)dtEasy.Clone();
                       //         int k = 0;
                       //         List<int> a = GetRandomNumbers(dtEasy.Rows.Count);
                       //         if (dtEasy.Rows.Count >= 5)
                       //         {
                       //             for (k = 0; k < 5; k++)
                       //             {
                       //                 dtnew.ImportRow(dtEasy.Rows[a[k]]);
                       //             }
                       //         }
                       //      // next 5 from Sub-level=2 ie., Difficult

                       //     DataSet dsDiff = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select [VocabTPID],[Word],[Unpub],[Level],[SubLevel],[Year],[EventID],[Event],[ProductGroupID],[ProductGroupCode],[ProductID],[ProductCode],[SetNo],[Phase],[CreatedBy],convert(varchar(20),[CreateDate],101) [CreateDate],[ModifiedBy],convert(varchar(20),[ModifyDate],101) [ModifyDate] from VocabTPWords where Year=" + ddlYear.SelectedValue + " and Event='" + ddlEvent.SelectedItem.Text + "' and ProductCode='" + ddlProduct.SelectedValue + "' and SetNo=" + ddlSet.SelectedValue + " and Phase=1 and Unpub is null and Source='JVBList' and Sublevel=2");
                       //     DataTable dtDiff = dsDiff.Tables[0];
                       //     if (dtDiff.Rows.Count >= 5)
                       //     {
                       //         a = GetRandomNumbers(dtDiff.Rows.Count);
                       //         for (k = 0; k < 5; k++)
                       //         {
                       //             dtnew.ImportRow(dtDiff.Rows[a[k]]);
                       //         }
                       //     }
                       ////Getting 15 UN-PUBLISHED words from Source except 'JVBList' 
                       //     string strqryUnpubDiff = "select [VocabTPID],[Word],[Unpub],[Level],[SubLevel],[Year],[EventID],[Event],[ProductGroupID],[ProductGroupCode],[ProductID],[ProductCode],[SetNo],[Phase],[CreatedBy],convert(varchar(20),[CreateDate],101) [CreateDate],[ModifiedBy],convert(varchar(20),[ModifyDate],101) [ModifyDate] from VocabTPWords where Year=" + ddlYear.SelectedValue + " and Event='" + ddlEvent.SelectedItem.Text + "' and ProductCode='" + ddlProduct.SelectedValue + "' and SetNo=" + ddlSet.SelectedValue + " and Phase=1 and Unpub='Y' and Source!='JVBList' and Sublevel=2";
                       //     DataSet dsUnpubDiff = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, strqryUnpubDiff);
                       //     DataTable dtUnpubDiff = dsUnpubDiff.Tables[0];
                       //     if (dtUnpubDiff.Rows.Count >= 15)
                       //     {
                       //         a = GetRandomNumbers(dtUnpubDiff.Rows.Count);
                       //         for (k = 0; k < 15; k++)
                       //         {
                       //             dtnew.ImportRow(dtUnpubDiff.Rows[a[k]]);
                       //         }
                       //     }
                       // // display to gird for binding
                       // DataSet dsnew = new DataSet();
                       // dsnew.Tables.Add(dtnew);

                       //     if (dsnew.Tables[0].Rows.Count > 0)
                       //     {
                       //         gvVocabwords_new.DataSource = dsnew;
                       //         //ddlContestDate.DataValueField=''
                       //         gvVocabwords_new.DataBind();
                       //         gvVocabwords_new.Visible = true;

                       //     }
                       //     else
                       //     {
                       //         lblErr.Visible = true;
                       //         lblErr.ForeColor = Color.Red;
                       //         lblErr.Text = "Not Record Exists!";
                       //     }
                    }
                    else if (ddlPhase.SelectedIndex == 1) // For Phase: II
                    {
                        
                    }
                    else
                    {
                        gvVocabwords_new.Visible = false;
                    }

                }
                else if (ddlAction.SelectedIndex == 4)
                {

                    DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select [VocabTPID],[Word],[Unpub],[Level],[SubLevel],[Year],[EventID],[Event],[ProductGroupID],[ProductGroupCode],[ProductID],[ProductCode],[SetNo],[Phase],[CreatedBy],convert(varchar(20),[CreateDate],101) [CreateDate],[ModifiedBy],convert(varchar(20),[ModifyDate],101) [ModifyDate] from VocabTPWords where Year=" + ddlYear.SelectedValue+ " and EventID="+ddlEvent.SelectedValue+" and ProductCode='"+ddlPhase.SelectedValue+"' and SetNo="+ddlSet.SelectedValue+" and Phase="+ddlPhase.SelectedValue+" ");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvVocabwords_new.DataSource = ds;
                        //ddlContestDate.DataValueField=''
                        gvVocabwords_new.DataBind();
                        gvVocabwords_new.Visible = true;
                        gvVocabwords_new.Columns[0].Visible = true;

                    }
                    else
                    {
                        lblErr.Visible = true;
                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = "Not Record Exists!";
                    }
                    //gvVocabwords_new.Visible = true;
                    
                }


            }
        }
    }
    public static List<int> GetRandomNumbers(int count)
    {
        List<int> randomNumbers = new List<int>();

        for (int i = 0; i < count; i++)
        {
            int number;

            do number = (new Random()).Next(count);
            while (randomNumbers.Contains(number));

            randomNumbers.Add(number);
        }

        return randomNumbers;
    }
   
    protected void gvVocabwords_new_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void gvVocabwords_new_RowEditing(object sender, GridViewEditEventArgs e)
    {
      
    }
}