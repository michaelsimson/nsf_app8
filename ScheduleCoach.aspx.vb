Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ScheduleCoach
    Inherits System.Web.UI.Page

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    If LCase(Session("LoggedIn")) <> "true" Then
    '        Server.Transfer("maintest.aspx")
    '    ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
    '        Server.Transfer("login.aspx?entry=v")
    '    End If

    '    If Not Page.IsPostBack Then
    '        Dim year As Integer = 0
    '        year = Convert.ToInt32(DateTime.Now.Year)
    '        ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
    '        ddlEventYear.Items.Insert(1, Convert.ToString(year))
    '        ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
    '        ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
    '        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
    '            LoadProductGroup()
    '        ElseIf Session("RoleId").ToString() = "89" Then
    '            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null") > 1 Then
    '                'more than one 
    '                'Response.Write("select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null")
    '                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null ")
    '                Dim i As Integer
    '                Dim prd As String = String.Empty
    '                Dim Prdgrp As String = String.Empty
    '                For i = 0 To ds.Tables(0).Rows.Count - 1
    '                    If prd.Length = 0 Then
    '                        prd = ds.Tables(0).Rows(i)(1).ToString()
    '                    Else
    '                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
    '                    End If

    '                    If Prdgrp.Length = 0 Then
    '                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
    '                    Else
    '                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
    '                    End If
    '                Next
    '                lblPrd.Text = prd
    '                lblPrdGrp.Text = Prdgrp
    '                LoadProductGroup()
    '            Else
    '                'only one
    '                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=89 and ProductId is not Null ")
    '                Dim prd As String = String.Empty
    '                Dim Prdgrp As String = String.Empty
    '                prd = ds.Tables(0).Rows(0)(1).ToString()
    '                Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
    '                lblPrd.Text = prd
    '                lblPrdGrp.Text = Prdgrp
    '                LoadProductGroup()
    '            End If

    '        Else
    '            Server.Transfer("maintest.aspx")
    '        End If
    '        LoadCoach()
    '        LoadCapacity(ddlCapacity)
    '        LoadMinCapacity(ddlMinCap)

    '    End If
    'End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Private Sub LoadCapacity(ByVal ddltemp As DropDownList)
        Dim li As ListItem
        Dim i As Integer
        For i = 10 To 100 Step 5
            li = New ListItem(i)
            ddltemp.Items.Add(li)
        Next
    End Sub
    Private Sub LoadMinCapacity(ByVal ddltemp As DropDownList)
        Dim li As ListItem
        Dim i As Integer
        For i = 1 To 25 Step 1
            li = New ListItem(i)
            ddltemp.Items.Add(li)
        Next
    End Sub
    Private Sub LoadCoach()
        Dim strSql As String = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as ID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (88) and v.MemberID = I.AutoMemberID  order by I.FirstName"
        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlCoachName.DataSource = drcoach
        ddlCoachName.DataBind()
        ddlCoachName.Items.Insert(0, "Select Coach Name")
        ddlCoachName.Items(0).Selected = True
    End Sub
    Private Sub LoadProductGroup()
        Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=13 " & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
      
        ddlProductGroup.DataSource = drproductgroup
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count < 1 Then
            lblerr.Text = "No Product is opened. Please Contact admin and Get Product Opened in EventFees table"
        ElseIf ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Items.Insert(0, "Select Product Group")
            ddlProductGroup.Items(0).Selected = True
            ddlProductGroup.Enabled = True
        Else
            ddlProductGroup.Enabled = False
            LoadProductID()
        End If
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ddlProduct.Enabled = False
        Else
            Dim strSql As String
            Try
                strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=13 " & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & " order by P.ProductID "
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, "Select Product")
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    loadGrid(True)
                End If
            Catch ex As Exception
                lblerr.Text = lblerr.Text & "<br>" & ex.ToString
            End Try
        End If
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadProductID()
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If ddlProductGroup.Items.Count = 0 Then
            lblerr.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened in EventFees table"
        ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            lblerr.Text = "Please select Product Group"
        ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
            lblerr.Text = "Please select Product"
        ElseIf ddlCoachName.Items(0).Selected = True Then
            lblerr.Text = "Please select Coach name"
        ElseIf txtCoachingTime.Text.Length < 1 Then
            lblerr.Text = "Please Enter Coaching time"
            'ElseIf ddlMinCap.SelectedValue < 2 Then
            '    lblerr.Text = "Please select Minimum Capacity"
        ElseIf ddlCapacity.SelectedValue < 2 Then
            lblerr.Text = "Please select Maximum Capacity"
        ElseIf CInt(ddlMinCap.SelectedValue) >= CInt(ddlCapacity.SelectedValue) Then
            lblerr.Text = "Maximum Capacity must be greater than Minimum Capacity"
        ElseIf txtStartDate.Text.Length < 1 Then
            lblerr.Text = "Please Select Start Date"
        ElseIf Not IsDate(txtStartDate.Text) = True Then
            lblerr.Text = "Please Enter Start Date in mm/dd/yyyy format"
        ElseIf txtEndDate.Text.Length < 1 Then
            lblerr.Text = "Please Select End Date"
        ElseIf Not IsDate(txtEndDate.Text) = True Then
            lblerr.Text = "Please enter End Date  in mm/dd/yyyy format"
        ElseIf Date.Parse(txtEndDate.Text) < Date.Parse(txtStartDate.Text) Then
            lblerr.Text = "Please correct End date greater then Start date"
        Else
            lblerr.Text = ""
            Dim strSQL As String
            Try
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from Coachcal where Memberid=" & ddlCoachName.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and eventyear=" & ddlEventYear.SelectedValue & " and Level = '" & ddlLevel.SelectedItem.Text & "' AND Phase=" & ddlPhase.SelectedValue & " AND SessionNo=" & ddlSession.SelectedValue) < 1 Then
                    strSQL = "INSERT INTO CoachCal (MemberID, EventYear, MaxCap, MinCap, ProductID, ProductCode, [Level],SessionNo, ProductGroupID, ProductGroupCode, CoachDayTime, StartDate, EndDate,CreatedBy,CreateDate,Phase) VALUES (" & ddlCoachName.SelectedValue & "," & ddlEventYear.SelectedValue & "," & ddlCapacity.SelectedValue & "," & ddlMinCap.SelectedValue & "," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "','" & ddlLevel.SelectedItem.Text & "'," & ddlSession.SelectedValue & "," & ddlProductGroup.SelectedValue & ",'" & getProductGroupcode(ddlProductGroup.SelectedValue) & "','" & txtCoachingTime.Text & "','" & txtStartDate.Text & "','" & txtEndDate.Text & "'," & Session("loginID") & ",Getdate()," & ddlPhase.SelectedValue & ")"
                    SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strSQL)
                    clear()
                    loadGrid(True)
                    lblerr.Text = "Inserted Successfully"
                Else
                    lblerr.Text = "Coach with same product and Level and Session already found"
                End If
            Catch ex As Exception
                lblerr.Text = strSQL
                lblerr.Text = lblerr.Text & ex.ToString()
            End Try
        End If
    End Sub
    Public Sub loadGrid(ByVal blnReload As Boolean)
        lblerr.Text = ""
        Dim ds As DataSet
        If blnReload = True Then
            Try
                ' ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT   C.CoachCalID, C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear, C.MaxCap, C.MinCap, C.ProductID, P.ProductCode as ProductCode, C.[Level], C.ProductGroupID, PG.Name as ProductGroupCode, C.CoachDayTime, C.StartDate, C.EndDate,I.City,I.State,CASE WHEN C.CoachDayTime LIKE '%EST%' THEN 1 ELSE CASE WHEN C.CoachDayTime LIKE '%CST%' THEN 2 ELSE CASE WHEN C.CoachDayTime LIKE '%MST%' THEN 3 ELSE 4 END END END AS TIMEZone FROM CoachCal C,IndSpouse I,ProductGroup PG,Product P where C.MemberID=I.AutoMemberID and C.ProductID=P.ProductID and C.ProductGroupID=PG.ProductGroupId and C.ProductID=" & ddlProduct.SelectedValue & "  Order By TimeZone,I.LastName,I.FirstName")
                'MsgBox("SELECT   C.CoachCalID, C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear, C.MaxCap, C.MinCap, C.ProductID, P.ProductCode as ProductCode, C.[Level], C.ProductGroupID, PG.Name as ProductGroupCode, C.CoachDayTime, C.StartDate, C.EndDate,I.City,I.State,CASE WHEN C.CoachDayTime LIKE '%EST%' THEN 1 ELSE CASE WHEN C.CoachDayTime LIKE '%CST%' THEN 2 ELSE CASE WHEN C.CoachDayTime LIKE '%MST%' THEN 3 ELSE 4 END END END AS TIMEZone,case when CR.Cnt IS Null then 0 else CR.Cnt End as Count FROM CoachCal C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID LEFT JOIN (Select COUNT(CoachRegID) as Cnt, CMemberID,ProductID,[Level],EventYear from CoachReg group by  CMemberID,ProductID,[Level],EventYear) as CR ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear where C.ProductID=" & ddlProduct.SelectedValue & "  Order By TimeZone,I.LastName,I.FirstName")
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT   C.CoachCalID, C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear, C.MaxCap, C.MinCap, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.CoachDayTime, C.StartDate, C.EndDate,I.City,I.State,CASE WHEN C.CoachDayTime LIKE '%EST%' THEN 1 ELSE CASE WHEN C.CoachDayTime LIKE '%CST%' THEN 2 ELSE CASE WHEN C.CoachDayTime LIKE '%MST%' THEN 3 ELSE 4 END END END AS TIMEZone,case when CR.Cnt IS Null then 0 else CR.Cnt End as Count FROM CoachCal C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID LEFT JOIN (Select COUNT(CoachRegID) as Cnt, CMemberID,ProductID,[Level],EventYear,Phase,SessionNo from CoachReg group by  CMemberID,ProductID,[Level],EventYear,Phase,SessionNo) as CR ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  where C.EventYear=" & ddlEventYear.SelectedValue & " AND C.Phase=" & ddlPhase.SelectedValue & " AND C.ProductID=" & ddlProduct.SelectedValue & "  Order By TimeZone,I.LastName,I.FirstName")
                Session("volDataSet") = ds

                If ds.Tables(0).Rows.Count > 0 Then
                    DGCoach.Visible = True
                Else
                    DGCoach.Visible = False
                    lblerr.Text = "No schedule available"
                End If
                DGCoach.DataSource = ds
                DGCoach.DataBind()
                DGCoach.CurrentPageIndex = 0
            Catch se As SqlException
                lblerr.Text = se.ToString()
                'Response.Write("SELECT   C.CoachCalID, C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear, C.MaxCap, C.MinCap, C.ProductID, P.ProductCode as ProductCode, C.[Level], C.ProductGroupID, PG.Name as ProductGroupCode, C.CoachDayTime, C.StartDate, C.EndDate,I.City,I.State,CASE WHEN C.CoachDayTime LIKE '%EST%' THEN 1 ELSE CASE WHEN C.CoachDayTime LIKE '%CST%' THEN 2 ELSE CASE WHEN C.CoachDayTime LIKE '%MST%' THEN 3 ELSE 4 END END END AS TIMEZone,case when CR.Cnt IS Null then 0 else CR.Cnt End as Count FROM CoachCal C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID LEFT JOIN (Select COUNT(CoachRegID) as Cnt, CMemberID,ProductID,[Level],EventYear,Phase from CoachReg group by  CMemberID,ProductID,[Level],EventYear,Phase) as CR ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase where C.EventYear=" & ddlEventYear.SelectedValue & " AND C.Phase=" & ddlPhase.SelectedValue & " AND C.ProductID=" & ddlProduct.SelectedValue & "  Order By TimeZone,I.LastName,I.FirstName")
                Return
            End Try
        Else
            ds = CType(Session("volDataSet"), DataSet)
            DGCoach.DataSource = ds
            DGCoach.DataBind()
        End If
    End Sub

    Protected Sub DGCoach_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim CoachCalID As Integer
            CoachCalID = CInt(e.Item.Cells(2).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM coachcal WHERE CoachCalID =" + CStr(CoachCalID))
            Catch se As SqlException
                lblerr.Text = "Error: while deleting the record"
                Return
            End Try
            loadGrid(True)
            DGCoach.EditItemIndex = -1
        End If
    End Sub

    Protected Sub DGCoach_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.EditCommand
        DGCoach.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = CType(Session("volDataSet"), DataSet)
        Dim page As Integer = DGCoach.CurrentPageIndex
        Dim pageSize As Integer = DGCoach.PageSize
        Dim currentRowIndex As Integer

        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If

        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr
        loadGrid(True)
    End Sub

    Private Sub DGCoach_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGCoach.ItemCreated
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)
            Dim count As Integer = CType(DataBinder.Eval(e.Item.DataItem, "count"), Integer)
            If count < 1 Then
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
            Else
                btn.Visible = False
            End If
        End If
    End Sub

    Protected Sub DGCoach_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.UpdateCommand
        Dim sqlStr As String
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim CoachCalID As Integer
        Dim MemberID As Integer
        Dim EventYear As Integer
        Dim Capacity As Integer
        Dim MinCap As Integer
        Dim SessionNo As Integer
        Dim ProductID As Integer
        Dim Level As String
        Dim CoachDayTime As String
        Dim StartDate As String
        Dim EndDate As String

        Try
            CoachCalID = CInt(e.Item.Cells(2).Text)
            'MemberID = CInt(CType(e.Item.FindControl("txtMemberID"), TextBox).Text)
            EventYear = CInt(CType(e.Item.FindControl("ddlEventYear"), DropDownList).SelectedValue)
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                ProductID = dr.Item("productID")
                MemberID = dr.Item("MemberID")
                Capacity = dr.Item("MaxCap")
                MinCap = dr.Item("MinCap")
            End If
            Level = CStr(CType(e.Item.FindControl("ddlLevel"), DropDownList).SelectedItem.Text)
            SessionNo = CInt(CType(e.Item.FindControl("ddlSessionNo"), DropDownList).SelectedItem.Value)
            'Capacity = CInt(CType(e.Item.FindControl("txtCapacity"), TextBox).Text)
            CoachDayTime = CType(e.Item.FindControl("txtCoachDayTime"), TextBox).Text
            StartDate = CType(e.Item.FindControl("txtStartDate"), TextBox).Text
            EndDate = CType(e.Item.FindControl("txtEndDate"), TextBox).Text

            ',ProductGroupID = " & ProductGroupID & ",ProductGroupCode = '" & getProductGroupcode(ProductGroupID) & "'
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from Coachcal where Memberid=" & MemberID & " and ProductID=" & ProductID & " and eventyear=" & EventYear & " and Level = '" & Level & "' and  CoachCalID not in (" & CoachCalID & ") AND Phase=" & ddlPhase.SelectedValue & " AND SessionNo=" & SessionNo) < 1 Then
                sqlStr = "UPDATE CoachCal SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",MaxCap =" & Capacity & ",MinCap =" & MinCap & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] = '" & Level & "',CoachDayTime = '" & CoachDayTime & "',StartDate = '" & StartDate & "',SessionNo=" & SessionNo & ",EndDate = '" & EndDate & "' ,ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE CoachCalID=" & CoachCalID
                SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlStr)
                lblerr.Text = "Updated Successfully"
            Else
                lblerr.Text = "Coach with same product and Level already found"
            End If
        Catch ex As SqlException
            'ex.message
            lblerr.Text = sqlStr & "<br> Error:updating the record" + ex.ToString
            Return
        End Try
        DGCoach.EditItemIndex = -1
        loadGrid(True)
    End Sub

    Protected Sub DGCoach_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGCoach.CancelCommand
        Dim ds As DataSet = CType(Session("volDataSet"), DataSet)
        If (Not ds Is Nothing) Then
            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = DGCoach.CurrentPageIndex
            Dim pageSize As Integer = DGCoach.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If

            'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            DGCoach.EditItemIndex = -1
            loadGrid(False)
        Else

            Return
        End If
    End Sub
    Protected Sub DGCoach_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGCoach.PageIndexChanged
        DGCoach.CurrentPageIndex = e.NewPageIndex
        DGCoach.EditItemIndex = -1
        loadGrid(False)
    End Sub

    Public Sub ddlDGCapacity_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim start As Integer
        If (Not dr Is Nothing) Then
            start = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(CR.CoachRegID) from CoachReg CR Inner Join coachcal C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level] and CR.EventYear=C.EventYear and C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  where CR.Approved='Y' AND C.CoachCalID=" & dr.Item("CoachCalID") & " AND C.EventYear=" & dr.Item("EventYear") & "")
            'start = ((convert.ToInt32(start / 10) + 1) * 10
            ''** Sept 2011, 22 start = IIf(start Mod 5 = 0, start, (Convert.ToInt32(start / 5) + 1) * 5)
            Dim li As ListItem
            Dim i As Integer
            For i = start To 100 Step 1
                li = New ListItem(i)
                ddlTemp.Items.Add(li)
            Next
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("MaxCap")))
        End If
    End Sub

    Public Sub ddlDGCapacity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If ddlTemp.SelectedValue <= dr.Item("MinCap") Then
                lblError.Text = "Maximum Capacity must be greater than Minimum Capacity"
            Else
                lblError.Text = ""
                dr.Item("MaxCap") = ddlTemp.SelectedValue
                Cache("editRow") = dr
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGMinCap_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim start As Integer
        If (Not dr Is Nothing) Then
            start = dr.Item("MinCap")
            Dim li As ListItem
            Dim i As Integer
            For i = 1 To 25 Step 1
                li = New ListItem(i)
                ddlTemp.Items.Add(li)
            Next
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(start.ToString()))
        End If
    End Sub

    Public Sub ddlDGMinCap_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MinCap") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlLevel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim level As String = ""
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Level") Is DBNull.Value) Then
                level = dr.Item("Level")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowEventYear")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(level))
        Else
            Return
        End If
    End Sub

    Public Sub ddlSessionNo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SessionNo As Integer = 0
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("SessionNo") Is DBNull.Value) Then
                SessionNo = dr.Item("SessionNo")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowEventYear")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(SessionNo))
        Else
            Return
        End If
    End Sub

    Public Sub EventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim rowEventYear As Integer = 0
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventYear") Is DBNull.Value) Then
                rowEventYear = dr.Item("EventYear")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("rowEventYear")))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(rowEventYear))
        Else
            Return
        End If
    End Sub
    Public Sub Member_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (88) and v.MemberID = I.AutoMemberID order by I.FirstName"
        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
    End Sub
    
    Public Sub Member_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub Product_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProdGroupId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowProdGroupId = dr.Item("productGroupId")
        End If
        Dim strSql As String = "Select ProductID, Name from Product where EventID in (13) and ProductGroupID =" & rowProdGroupId & " order by ProductID"
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drproductid
        ddlTemp.DataBind()
        Dim rowProductID As Integer = 0
        If (Not dr.Item("productID") Is DBNull.Value) Then
            rowProductID = dr.Item("productID")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
    End Sub


    Public Sub Product_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        'Session("rowRoleId") = ddlTemp.SelectedValue
        'Session("rowRoleCode") = ddlTemp.SelectedItem.Text
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            dr.Item("productID") = ddlTemp.SelectedValue
            dr.Item("ProductCode") = getProductcode(ddlTemp.SelectedValue)
            'Session("editRow") = dr
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProductGroupID As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup where EventId=13  order by ProductGroupID"
        Dim drproductGroupid As SqlDataReader
        drproductGroupid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drproductGroupid
        ddlTemp.DataBind()
        If (Not dr.Item("productGroupID") Is DBNull.Value) Then
            rowProductGroupID = dr.Item("productGroupID")
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductGroupID))
    End Sub


    Private Sub clear()
        ddlCoachName.SelectedIndex = 0
        ' ddlProduct.Items.Clear()
        'ddlProduct.Enabled = False
        'ddlProductGroup.SelectedIndex = 0
        ddlLevel.SelectedIndex = ddlLevel.Items.IndexOf(ddlLevel.Items.FindByText("Beginner"))
        txtCoachingTime.Text = String.Empty
        txtStartDate.Text = String.Empty
        txtEndDate.Text = String.Empty
        ddlCapacity.SelectedIndex = ddlCapacity.Items.IndexOf(ddlCapacity.Items.FindByText("1"))
        ddlMinCap.SelectedIndex = ddlMinCap.Items.IndexOf(ddlMinCap.Items.FindByText("1"))
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlProduct.SelectedValue = "Select Product" Then
            lblerr.Text = "Please select Valid Product"
        Else
            loadGrid(True)
        End If

    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadProductGroup()
        If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
            lblerr.Text = "Please select Valid Product"
        Else
            loadGrid(True)
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
            lblerr.Text = "Please select Valid Product"
        Else
            loadGrid(True)
        End If
    End Sub
End Class
