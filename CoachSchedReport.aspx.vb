﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports VRegistration
Partial Class CoachSchedReport
    Inherits System.Web.UI.Page
    Dim dtColor As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not Page.IsPostBack Then

            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            'ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            'ddlEventYear.Items.Insert(1, Convert.ToString(year))
            'ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))

            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year + 1) + "-" + Convert.ToString(year + 2).Substring(2, 2), Convert.ToString(year + 1)))
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)))
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year - 1) + "-" + Convert.ToString(year).Substring(2, 2), Convert.ToString(year - 1)))

            ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
            loadPhase()
            ''Options for Volunteer Name Selection

            'If (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches. 
            Dim ds_Vol As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID  " & IIf(Session("RoleID") = 89, " Where I.AutoMemberID=" & Session("LoginID"), "") & " order by I.FirstName")
            If ds_Vol.Tables(0).Rows.Count > 0 Then
                ddlVolName.DataSource = ds_Vol
                ddlVolName.DataBind()
                If ds_Vol.Tables(0).Rows.Count > 1 Then
                    ddlVolName.Items.Insert(0, "Select Volunteer")
                    ddlVolName.SelectedIndex = 0
                End If
            Else
                lblError.Text = "No Volunteers present for Calendar Sign up"
            End If

            'End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Then
                LoadProductGroup()
            Else
                If Session("RoleId").ToString() = "89" Then
                    Dim ds As DataSet
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                        'more than one 
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim i As Integer
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            If prd.Length = 0 Then
                                prd = ds.Tables(0).Rows(i)(1).ToString()
                            Else
                                prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                            End If

                            If Prdgrp.Length = 0 Then
                                Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                            Else
                                Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                            End If
                        Next
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        LoadProductGroup()
                    Else
                        'only one
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        If ds.Tables(0).Rows.Count > 0 Then
                            prd = ds.Tables(0).Rows(0)(1).ToString()
                            Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                            lblPrd.Text = prd
                            lblPrdGrp.Text = Prdgrp
                        End If
                        LoadProductGroup()
                    End If
                End If
            End If

            LoadRole()
            LoadVolunteer()
            LoadEvent(ddlEvent)
            LoadWeekdays()
            'LoadGrid(DGSchedule)
            lblError.Text = ""
            oDiv.Attributes.Add("onscroll", "setonScroll(this,'" + DGSchedule.ClientID + "');")
        End If
    End Sub
    Public Sub LoadRole()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct RoleID,RoleCode From Volunteer Where RoleID in (" & Session("RoleID") & ")")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRole.DataSource = ds
            ddlRole.DataBind()
        End If

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(control As Control)

    End Sub

    Public Sub LoadVolunteer()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.FirstName +''+I.LastName as Name,C.MemberID From CalSignUp C Inner Join IndSpouse I on I.AutoMemberID=C.MemberID " & IIf(Session("RoleID") = 89, " where C.MemberID=" & Session("LoginID"), ""))
        If ds.Tables(0).Rows.Count > 0 Then
            ddlVolName.DataSource = ds
            ddlVolName.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlVolName.Items.Insert(0, New ListItem("Select Event", "-1"))
            Else
                ddlVolName.Enabled = False
            End If
            ddlVolName.SelectedIndex = 0
        Else
            lblError.Text = "No Events present for the selected year"
        End If
    End Sub

    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub
        ' "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
                ddlObject.Enabled = True
            Else
                ddlObject.Enabled = False
                LoadProductGroup()
            End If
            ddlObject.SelectedIndex = 1
            LoadProductGroup()
            LoadProductID()
        Else
            lblError.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Private Sub LoadWeekdays()
        For i As Integer = 1 To 7
            ddlWeekDay.Items.Add(WeekdayName(i))
        Next
        ddlWeekDay.SelectedIndex = ddlWeekDay.Items.IndexOf(ddlWeekDay.Items.FindByText("Saturday"))
    End Sub

    Private Sub LoadProductGroup()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " and EF.Semester='" + ddlPhase.SelectedValue + "' AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblError.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            '  If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            'ddlProduct.Enabled = False
            '   Else
            Dim strSql As String
            strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " and EF.Semester='" + ddlPhase.SelectedValue + "' AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & "" 'and P.Status='O'
            If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                strSql = strSql + " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & ""
            End If
            strSql = strSql + "  order by P.ProductID "
            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlProduct.DataSource = drproductid
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 1 Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
                ddlProduct.Enabled = True
            ElseIf ddlProduct.Items.Count < 1 Then
                ddlProduct.Enabled = False
            Else
                ddlProduct.Enabled = False
                LoadGrid(DGSchedule, 1)
                LoadGrid(DGScheduleNotAccepted, 2)
            End If
            '  End If
        Catch ex As Exception
            lblError.Text = lblError.Text & "<br>" & ex.ToString
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        DisplayEvent()

    End Sub
    Protected Sub DisplayEvent()
        Try
            TimeConflict()
            lblError.Text = ""
            'If ddlVolName.SelectedItem.Text = "" Then 'Session("RoleID") = "89" And
            '    lblError.Text = "Please select Volunteer"
            'Else
            If ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblError.Text = "Please select Event"
                clearGrid()
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblError.Text = "No Product Group is present for selected Event."
                clearGrid()
                ' ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblError.Text = "Please select Product Group"
                clearGrid()
            ElseIf ddlProduct.Items.Count = 0 Then
                lblError.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
                '  ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblError.Text = "Please select Product"
                clearGrid()
            Else
                lblError.Text = ""
                Dim str As String = "select count(*) from CalSignUp where Semester='" & ddlPhase.SelectedValue & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue
                Dim StrCmd As String = ""
                StrCmd = "select count(*) from CalSignUp where Semester='" & ddlPhase.SelectedValue & "' and Eventyear=" & ddlEventYear.SelectedValue & ""
                If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                    StrCmd = StrCmd + " and ProductGroupID=" & ddlProductGroup.SelectedValue & " "
                End If
                If (ddlProduct.SelectedValue <> "Select Product") Then
                    StrCmd = StrCmd + "  and ProductID=" & ddlProduct.SelectedValue & " "
                End If
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, StrCmd) > 0 Then
                    '< 1 Then '" & IIf(Session("RoleID") = "89", " Memberid=" & ddlVolName.SelectedValue & " and ", "") & "
                    btnExport.Visible = True
                    dvSort.Visible = True
                    'LoadGrid(DGSchedule)
                    LoadGrid(DGSchedule, 1)
                    LoadGrid(DGScheduleNotAccepted, 2)
                    GridviewRowColor()

                Else
                    DGSchedule.Visible = False
                    btnExport.Enabled = False
                    Table1.Visible = False
                    Table2.Visible = False
                    DGScheduleNotAccepted.Visible = False
                    lblError.Text = "No record Exists."
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Function WeekDayNumber(ByVal weekName As String) As Integer
        Dim weekNames As New Dictionary(Of String, Integer)
        For i As Integer = 1 To 7
            weekNames.Add(WeekdayName(i), i)
        Next
        Return weekNames(weekName)
    End Function

    Public Sub LoadGrid(ByVal DGSchedule, ByVal mode)
        Try
            Table2.Visible = True
            Table1.Visible = True
            Dim StrSQl As String = ""
            Dim ds As DataSet
            Dim st As String
            st = "Select Count(*) From CalSignUp Where EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "'"
            If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                st = st + " and ProductGroupID=" & ddlProductGroup.SelectedValue & " "
            End If
            If (ddlProduct.SelectedValue <> "Select Product") Then
                st = st + "  and ProductID=" & ddlProduct.SelectedValue & " "
            End If

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, st) > 0 Then
                '" & IIf(Session("RoleID") = 89, " MemberID=" & Session("LoginID") & " and ", "") & "
                Dim pgId As String = "0"
                If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
                    pgId = ddlProductGroup.SelectedValue
                End If

                Dim pId As String = "0"
                If (ddlProduct.SelectedValue <> "Select Product") Then
                    pId = ddlProduct.SelectedValue
                End If

                Dim prmArray(13) As SqlParameter
                prmArray(0) = New SqlParameter("@mode", mode)
                prmArray(1) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
                prmArray(2) = New SqlParameter("@EventID", ddlEvent.SelectedValue)
                prmArray(3) = New SqlParameter("@Semester", ddlPhase.SelectedValue)
                prmArray(4) = New SqlParameter("@ProductGroupID", pgId)
                prmArray(5) = New SqlParameter("@ProductID", pId)

                'prmArray(5) = New SqlParameter("@MemberID", IIf(Session("RoleID") = 89, Session("LoginID"), "NULL"))

                Dim j As Integer = 0 '' Set to assign weekday names for Headers in the DataGris at appropriate Columns.
                Dim WeekDayVal As Integer
                Try
                    WeekDayVal = WeekDayNumber(ddlWeekDay.SelectedItem.Text)
                Catch ex As Exception
                    WeekDayVal = WeekDayNumber("Saturday")
                End Try
              
                '***********Assigning Weekday parameters for the SP*************'
                For i As Integer = 6 To 12
                    j = j + 4
                    If WeekDayVal > 7 Then
                        WeekDayVal = WeekDayVal - 7
                    End If

                    prmArray(i) = New SqlParameter("@Day" & i - 5, WeekdayName(WeekDayVal))
                    DGSchedule.Columns(j).HeaderText = WeekdayName(WeekDayVal) '.ToString.Substring(0, 3) ' "Sunday"

                    WeekDayVal = WeekDayVal + 1
                Next

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_PrepSchCalSignupAccepted", prmArray)
                Dim dttab As DataTable = ds.Tables(0)
                ViewState("dtgrid") = dttab
                If ds.Tables(0).Rows.Count > 0 Then

                    DGSchedule.DataSource = ds
                    DGSchedule.DataBind()


                    '************************Enable only the filled Cells in Grid****************************'

                    '******************************************************************************************'
                    DGSchedule.Visible = True
                    btnExport.Enabled = True
                    lb2.Text = ""
                    lbT1.Text = ""
                Else
                    If mode = 2 Then
                        lb2.Visible = True
                        DGScheduleNotAccepted.Visible = False
                        lb2.Text = "No Data Exists for the selections made."
                    Else
                        lbT1.Visible = True
                        DGSchedule.Visible = False
                        lbT1.Text = "No Data Exists for the selections made."
                    End If

                End If

            Else
                lblError.Text = "No Data Exists."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub GridviewRowColor()
        Dim StrsqlColor As String
        StrsqlColor = "select  distinct C.MemberID,I.firstname+' '+I.Lastname as name from CalSignUp C inner join "
        StrsqlColor = StrsqlColor & " indspouse I on I.automemberId=C.memberid where accepted='Y' and    EventYear=" & ddlEventYear.SelectedValue & " "
        StrsqlColor = StrsqlColor & " and EventID=" & ddlEvent.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "'"

        If (ddlProductGroup.SelectedValue <> "Select Product Group") Then
            StrsqlColor = StrsqlColor + " and ProductGroupID=" & ddlProductGroup.SelectedValue & " "
        End If
        If (ddlProduct.SelectedValue <> "Select Product") Then
            StrsqlColor = StrsqlColor + "  and ProductID=" & ddlProduct.SelectedValue & " "
        End If

        StrsqlColor = StrsqlColor & " group by C.MemberID,I.firstname,I.Lastname  having COUNT(C.memberid)>1"
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrsqlColor)
        dtColor = ds1.Tables(0)

        For i As Integer = 0 To dtColor.Rows.Count
            For j As Integer = 0 To DGSchedule.Items.Count - 1
                'Dim st As Object = DGSchedule
                Dim str As String = DGSchedule.Items(j).Cells(2).Text
                If dtColor.Rows(i)("name") = DGSchedule.Items(j).Cells(2).Text Then
                    ' DGSchedule.Items(j).BackColor = Color.FromArgb(235, 237, 236)
                    DGSchedule.Items(j).BackColor = Color.SkyBlue
                End If

            Next

        Next
    End Sub

    Private Sub clear()
        lblError.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlProductGroup.Items.Clear()
        ddlProductGroup.Enabled = False
        ddlProduct.Items.Clear()
        ddlProduct.Enabled = False
        'ddlProductGroup.SelectedIndex = 0
        ddlPhase.SelectedIndex = 0
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        lblError.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        clearGrid()
        LoadEvent(ddlEvent)

    End Sub
    Protected Sub clearGrid()
        DGSchedule.Visible = False
        DGScheduleNotAccepted.Visible = False
        GriConflict.Visible = False
        Table1.Visible = False
        Table2.Visible = False
        Table3.Visible = False
    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblError.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        LoadProductGroup()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If ddlProductGroup.SelectedIndex <> 0 Then
            LoadProductID()
        End If
        DisplayEvent()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        lblError.Text = ""
        lblError.Text = ""
        '  If ddlProduct.SelectedValue = "Select Product" Then
        ' lblError.Text = "Please select Valid Product"
        ' Else
        Dim strSql As String = "select productgroupid from product where productid=" & ddlProduct.SelectedValue & ""
        Dim PgId As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, strSql))
        ddlProductGroup.SelectedValue = PgId
        DisplayEvent()
        ' LoadGrid(DGSchedule)
        ' End If
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblError.Text = ""
    End Sub


    Protected Sub DGSchedule_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGSchedule.PageIndexChanged
        DGSchedule.CurrentPageIndex = e.NewPageIndex
        DGSchedule.EditItemIndex = -1
        LoadGrid(DGSchedule, 1)
        'LoadGrid(DGScheduleNotAccepted, 2)
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click


        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Prepare_Schedule_" & ddlEventYear.SelectedItem.Value & "_" & ddlEvent.SelectedItem.Text & "_" & ddlProduct.SelectedItem.Text & ".xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        Dim dgexportAccept As New DataGrid
        Dim dt As DataTable = LoadTable(DGSchedule)
        Dim drow As DataRow
        drow = dt.NewRow()
        dt.Rows.Add(drow)
        dgexport.DataSource = dt
        dgexport.DataBind()
        Dim dt1 As DataTable = LoadTable(DGScheduleNotAccepted)
        dgexportAccept.DataSource = dt1
        dgexportAccept.DataBind()
        Table1.RenderControl(htmlWrite)
        dgexport.RenderControl(htmlWrite)
        Table2.RenderControl(htmlWrite)
        dgexportAccept.RenderControl(htmlWrite)
        Table3.RenderControl(htmlWrite)
        GriConflict.RenderControl(htmlWrite)
        Response.Write("<table><tr><td align='center' colspan='4'><font face='Arial' size='4'>Schedule Report</font></td></tr><tr><td align='right' colspan='4'>")
        Response.Write(stringWrite.ToString())

        Response.End()
    End Sub

    Function LoadTable(ByVal Dgview) As DataTable
        Dim dt1 As DataTable = New DataTable()
        DGSchedule.AllowPaging = False
        DGSchedule.DataBind()
        ' LoadGrid(DGSchedule)
        LoadGrid(DGSchedule, 1)
        LoadGrid(DGScheduleNotAccepted, 2)
        DGSchedule.AllowPaging = False
        DGSchedule.DataBind()
        DGScheduleNotAccepted.AllowPaging = False
        DGScheduleNotAccepted.DataBind()
        'With dt1.Columns
        dt1 = New DataTable()
        dt1.Columns.Add("Ser#", Type.GetType("System.String"))

        dt1.Columns.Add("CoachName", Type.GetType("System.String"))
        dt1.Columns.Add("Level", Type.GetType("System.String"))
        dt1.Columns.Add(DGSchedule.Columns(4).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S1", Type.GetType("System.String"))
        dt1.Columns.Add("P1", Type.GetType("System.String"))
        dt1.Columns.Add("A1", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(8).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S2", Type.GetType("System.String"))
        dt1.Columns.Add("P2", Type.GetType("System.String"))
        dt1.Columns.Add("A2", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(12).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S3", Type.GetType("System.String"))
        dt1.Columns.Add("P3", Type.GetType("System.String"))
        dt1.Columns.Add("A3", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(16).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S4", Type.GetType("System.String"))
        dt1.Columns.Add("P4", Type.GetType("System.String"))
        dt1.Columns.Add("A4", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(20).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S5", Type.GetType("System.String"))
        dt1.Columns.Add("P5", Type.GetType("System.String"))
        dt1.Columns.Add("A5", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(24).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S6", Type.GetType("System.String"))
        dt1.Columns.Add("P6", Type.GetType("System.String"))
        dt1.Columns.Add("A6", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(28).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S7", Type.GetType("System.String"))
        dt1.Columns.Add("P7", Type.GetType("System.String"))
        dt1.Columns.Add("A7", Type.GetType("System.String"))

        ' End With
        Dim i As Integer = 0
        Dim row As DataGridItem
        Dim drow As DataRow
        For Each row In Dgview.Items  '.Rows
            ' For i As Integer = 0 To DGSchedule.Items.Count - 1
            drow = dt1.NewRow()
            i += 1
            drow("Ser#") = i
            drow("CoachName") = row.Cells(2).Text
            drow("Level") = row.Cells(3).Text 'row.Cells("chk").Value
            drow(DGSchedule.Columns(4).HeaderText) = row.Cells(4).Text
            drow("S1") = row.Cells(5).Text
            drow("P1") = row.Cells(6).Text
            drow("A1") = row.Cells(7).Text

            drow(DGSchedule.Columns(8).HeaderText) = row.Cells(8).Text
            drow("S2") = row.Cells(9).Text
            drow("P2") = row.Cells(10).Text
            drow("A2") = row.Cells(11).Text

            drow(DGSchedule.Columns(12).HeaderText) = row.Cells(12).Text
            drow("S3") = row.Cells(13).Text
            drow("P3") = row.Cells(14).Text
            drow("A3") = row.Cells(15).Text

            drow(DGSchedule.Columns(16).HeaderText) = row.Cells(16).Text
            drow("S4") = row.Cells(17).Text
            drow("P4") = row.Cells(18).Text
            drow("A4") = row.Cells(19).Text

            drow(DGSchedule.Columns(20).HeaderText) = row.Cells(20).Text
            drow("S5") = row.Cells(21).Text
            drow("P5") = row.Cells(22).Text
            drow("A5") = row.Cells(23).Text

            drow(DGSchedule.Columns(24).HeaderText) = row.Cells(24).Text
            drow("S6") = row.Cells(25).Text
            drow("P6") = row.Cells(26).Text
            drow("A6") = row.Cells(27).Text

            drow(DGSchedule.Columns(28).HeaderText) = row.Cells(28).Text
            drow("S7") = row.Cells(29).Text
            drow("P7") = row.Cells(30).Text
            drow("A7") = row.Cells(31).Text

            dt1.Rows.Add(drow)
        Next

        Return dt1

    End Function

    Protected Sub DGScheduleNotAccepted_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGScheduleNotAccepted.PageIndexChanged
        DGScheduleNotAccepted.CurrentPageIndex = e.NewPageIndex
        DGScheduleNotAccepted.EditItemIndex = -1
        LoadGrid(DGScheduleNotAccepted, 2)
    End Sub
    Protected Sub TimeConflict()
        Try
            Dim dtnew As DataTable
            Dim temptable As DataTable
            Dim stryear As String = ddlEventYear.SelectedItem.Value
            Dim Strsql As String = "select  distinct MemberID,day from CalSignUp where  EventYear=" + stryear + "  and Accepted='y' and Semester='" & ddlPhase.SelectedValue & "' group by MemberID,day having COUNT(memberid)>1"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
            Dim dt As DataTable
            dt = ds.Tables(0)
            Dim builder As New StringBuilder
            If dt.Rows.Count > 0 Then
                If dt.Rows.Count > 0 Then
                    For i As Integer = 0 To dt.Rows.Count - 1
                        ''Dim DataType() As String = myTableData.Rows(i).Item(1)
                        builder.AppendLine(dt.Rows(i).Item(0))
                        If i <= dt.Rows.Count - 2 Then
                            builder.Append(",")
                        End If
                    Next
                End If

                Dim strs As String = builder.ToString()

                Dim StrsqlNew As String = "select Signupid,I.FirstName+' '+I.LastName as CoachName,C.MemberID,I.Email,I.CPhone,I.HPhone,C.ProductGroupCode as ProdGroup,C.productcode as Product,C.DAY,C.time,C.level, C.Semester,I.City,I.State from CalSignUp C left join IndSpouse I on I.AutoMemberID=c.MemberID where C.EventYear=" + stryear + " and C.MemberID in (" + strs.ToString() + ") and C.Accepted='Y' and Semester='" & ddlPhase.SelectedValue & "'  order by time"
                Dim dsnew As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrsqlNew)
                dtnew = dsnew.Tables(0)
                Dim datacopy As DataTable
                datacopy = dtnew.Clone()

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim strval As String = dt.Rows(i).Item("MemberId")
                    Dim strvalday As String = dt.Rows(i).Item("day")
                    Dim result() As DataRow
                    result = dtnew.Select("Memberid='" + strval + "' And day='" + strvalday + "'")
                    'For Each row As DataRow In result
                    '    datacopy.ImportRow(row)
                    'Next
                    For j As Integer = 0 To result.Length - 1
                        For k As Integer = 0 To result.Length - 1
                            Dim starttime As TimeSpan
                            Dim endtime As TimeSpan
                            Dim minustime As TimeSpan
                            If k <> result.Length - 1 Then

                                If result(j)("Product") <> result(k)("Product") Or result(j)("level") <> result(k)("level") Or result(j)("Signupid") <> result(k)("Signupid") Then
                                    minustime = TimeSpan.Parse("3:00:00")
                                    starttime = result(j)("Time")
                                    endtime = result(k)("Time")
                                    If starttime - endtime < minustime Then
                                        datacopy.ImportRow(result(k))
                                        datacopy.ImportRow(result(j))
                                    End If
                                End If

                            End If

                        Next

                    Next
                Next
                Dim data As DataTable
                data = datacopy.Clone()
                ViewState("dt") = datacopy
                Dim dt1 As DataTable = DirectCast(ViewState("dt"), DataTable)
                dt1 = dt1.DefaultView.ToTable(True, "Signupid", "CoachName", "Memberid", "Email", "CPhone", "HPhone", "ProdGroup", "Product", "DAY", "time", "level", "Semester", "City", "State")
                If dt1.Rows.Count > 0 Then
                    Table3.Visible = True
                    GriConflict.Visible = True
                    GriConflict.DataSource = dt1
                    GriConflict.DataBind()
                End If

            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Private Function ddlAccept11() As Object
        Throw New NotImplementedException
    End Function

    Private Function DataRow() As DataRow
        Throw New NotImplementedException
    End Function

    Protected Sub DGSchedule_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles DGSchedule.ItemDataBound
    End Sub

    Protected Sub DGSchedule_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles DGSchedule.ItemCommand

    End Sub

    Private Sub loadPhase()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2
            ddlPhase.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlPhase.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue)
    End Sub

    Protected Sub ddlWeekDay_SelectedIndexChanged(sender As Object, e As EventArgs)
        DisplayEvent()
    End Sub
End Class

