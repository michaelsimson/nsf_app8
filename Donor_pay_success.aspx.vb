
Imports System
Imports System.Web
Imports LinkPointTransaction
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports NorthSouth.BAL

Partial Class Donor_pay_success
    Inherits System.Web.UI.Page
    Dim strSql As String
    Protected order As String
    Protected resp As String
    Protected fIE5 As Boolean
    Public nRegFee As Decimal = 0
    Dim sbContests As New StringBuilder

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Session("LoggedIn") Is Nothing) Then
            If Session("LoggedIn") <> "True" Then
                Dim entryToken As String = Nothing
                If (Not Session("entryToken") Is Nothing) Then
                    entryToken = Session("entryToken").ToString().Substring(0, 1)
                    Server.Transfer("login.aspx?entry=" + entryToken)
                Else
                    Server.Transfer(System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL"))
                End If
            End If
        End If
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        Dim isTestMode As Boolean = False
        isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)

        If Not Session("CustIndID") = Nothing Then
            Session("ParentID") = Session("CustIndID")
        End If

        ' Put user code to initialize the page here
        If Not IsPostBack Then
            Dim sb As New StringBuilder
            Dim bc As HttpBrowserCapabilities = Request.Browser
            Dim re As StreamReader
            Dim nDonationAmt As Decimal = 0
            Dim nTaxDeductibleAmount As Decimal = 0
            Dim nregfee As Decimal = Session("RegFee")
            Dim nTaxDeductibleRegFee = CType(Session("RegFee"), Decimal) * (2 / 3)
            Dim emailBody As String = ""
            Dim screenConfirmText As String = ""
            Dim subMail As String = "Receipt for your NSF Donation"
            Dim approved As String = ""
            Dim paymentReference As String = ""

            If (isTestMode = True) Then
                subMail = "Test Email(no real credit card Transactions):" + "Confirmation received for your Donation"
            End If

            If (Not Session("R_APPROVED") Is Nothing) Then
                approved = Session("R_APPROVED").ToString
            End If

            If (Not Session("PaymentReference") Is Nothing) Then
                paymentReference = Session("PaymentReference").ToString
            End If

            If (approved = "APPROVED") Then
                DisplayContests()


                fIE5 = ((bc.Browser = "IE") _
                            AndAlso (bc.MajorVersion > 4))
                order = CType(Session("outXml"), String)

                If (Not (Session("Donation")) Is Nothing) Then
                    nDonationAmt = CType(Session("Donation"), Decimal)
                End If

                nTaxDeductibleAmount = (nDonationAmt + (nTaxDeductibleRegFee))

                Dim eventType As String
                If (Session("EventID") = 1) Then
                    eventType = "Finals"
                Else
                    eventType = "Regional"
                End If

                Dim eventYear As String
                eventYear = System.Configuration.ConfigurationManager.AppSettings.Get("Contest_Year")
                GetDonationReceipt()
                If Session("EventID") = "1" Then
                    re = File.OpenText(Server.MapPath("ConfirmingEmailParents.htm"))
                Else
                    re = File.OpenText(Server.MapPath("Donor_email.htm"))
                End If

                emailBody = re.ReadToEnd
                re.Close()
                If Session("Address2") = "#" Then
                    Session("Address2") = ""
                End If
                emailBody = emailBody.Replace("[NAME]", Session("Name"))
                emailBody = emailBody.Replace("[ADDRESS1]", Session("Address1"))
                emailBody = emailBody.Replace("[ADDRESS2]", Session("Address2"))
                emailBody = emailBody.Replace("[CITY]", Session("City"))
                emailBody = emailBody.Replace("[STATE]", Session("State"))
                emailBody = emailBody.Replace("[ZIP]", Session("ZIP"))
                emailBody = emailBody.Replace("[Date]", Session("Date"))
                emailBody = emailBody.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                emailBody = emailBody.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                emailBody = emailBody.Replace("[PAYMENTREFERENCE]", paymentReference)

                ''***Screen Population logic

                lbltxmsg.Text = "Your tax-deductible contribution is: " & FormatCurrency(nTaxDeductibleAmount)
                lblpayment.Text = "Payment Reference: " & paymentReference
                lblcontribution.Text = "Date of Contribution: " & Session("Date")

                're = File.OpenText(Server.MapPath("Donor_Screen.htm"))
                'screenConfirmText = re.ReadToEnd
                're.Close()
                'screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", paymentReference)
                'screenConfirmText = screenConfirmText.Replace("[NAME]", Session("Name"))
                'screenConfirmText = screenConfirmText.Replace("[Date]", Session("Date"))
                'screenConfirmText = screenConfirmText.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                'lblDonationMessage.Text = screenConfirmText.ToString
                'screenConfirmText = Nothing

                '*************************************************
                Dim Emailto As String = ""
                If (Not Session("CustIndID") Is Nothing) Then
                    Emailto = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Email from Indspouse where automemberid=" & Session("CustIndID") & "")
                Else
                    Emailto = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Email from Indspouse where automemberid=" & Session("LoginID") & "")
                End If
 
                If SendEmail(subMail, emailBody.ToString, Emailto) Then
                    lblEmailStatus.Text = "A copy of the donor receipt has been sent to your e-mail address for your permanent records."
                    If Not Session("EventID") Is Nothing Then
                        If Session("EventID") = 18 Then
                            Dim CEmail As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Email from WalkMarathon where WalkMarID = " & Session("WalkMarathonID") & "")
                            SendEmail("Receipt for NSF Donation", emailBody.ToString, CEmail)
                        End If
                    End If
                Else
                    lblEmailStatus.Text = "There was an error sending email. Please print/save details of this page for your records."
                End If

                ' Need coding to eliminate the custIndId if its volunteer login                
            End If
        End If
    End Sub
    Protected Overrides Sub OnInit(ByVal e As EventArgs)        '
        ' CODEGEN: This call is required by the ASP.NET Web Form Designer.        '
        InitializeComponent()
        MyBase.OnInit(e)
    End Sub

    ' <summary>
    ' Required method for Designer support - do not modify
    ' the contents of this method with the code editor.
    ' </summary>

    Private Sub InitializeComponent()

    End Sub
    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
        ' sMailTo = "bindhu.rajalakshmi@capestart.com"
        'Build Email Message
        GetDonationReceipt()
        Dim email As New MailMessage
        email.From = New MailAddress("nsffundraising@gmail.com")
        'If emailvalidateion(sMailTo) = True Then
        '    email.To.Add(sMailTo)
        'End If
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Dim ok As Boolean = True
        Try
            email.To.Add(sMailTo)
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
            'client.Timeout = 20000
            client.Send(email)
        Catch e As Exception
            'Response.Write(e.ToString())
            lblMessage.Text = e.Message.ToString
            ok = False
        End Try
        Return ok
    End Function

    Private Sub DisplayContests()
        Dim sb As New StringBuilder

        Dim rowcount As Int32 = 0

        Dim connContest As New SqlConnection(Application("ConnectionString"))

        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}

        Dim prmArray(4) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@paymentreference"
        prmArray(2).Value = Session("PaymentReference")
        prmArray(2).Direction = ParameterDirection.Input

        prmArray(3) = New SqlParameter
        prmArray(3).ParameterName = "@EventID"
        prmArray(3).Value = Session("EventID")
        prmArray(3).Direction = ParameterDirection.Input


        SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContestsAndPaymentInfo", dsContestant, tblConestant, prmArray)
        connContest = Nothing
    End Sub

    Private Sub GetDonationReceipt()
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        lblCurrDate.Text = DateTime.Now.ToString("MMMM dd, yyyy")

        'Chapter CoOrdinator
        Dim strChapterID As String
        strChapterID = ""
        If Session("RoleID") = "5" Then
            strSql = "Select chapterid, chaptercode, state from chapter where "
            strSql = strSql & " clusterid in (Select clusterid from chapter where "
            strSql = strSql & " chapterid = " & Session("LoginChapterID") & ") order by state, chaptercode"
            Dim con As New SqlConnection(Application("ConnectionString"))

            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
            While (drNSFChapters.Read())
                If Len(strChapterID) > 0 Then
                    strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                Else
                    strChapterID = drNSFChapters(0).ToString()
                End If
            End While
            drNSFChapters.Close()
        End If

        'Name and address of the Donor
        strSql = " Select automemberid, chapterid, "
        strSql = strSql & " donortype, email, firstname, "
        strSql = strSql & " lastname, address1, ISNull(Address2,'') as address2,"
        strSql = strSql & " city, state, zip from "
        strSql = strSql & " indspouse where "
        If (Not Session("CustIndID") Is Nothing) Then
            strSql = strSql & " automemberid = " & Session("CustIndID")
        Else
            strSql = strSql & " automemberid = " & Session("LoginID")
        End If
        strSql = strSql & "  AND DonorType IN ('IND','SPOUSE')"

        Dim drIndividual As SqlDataReader
        drIndividual = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
        If drIndividual.Read() Then
            lblName.Text = Trim(drIndividual("FirstName") & " " & drIndividual("LastName"))
            lblAddress1.Text = Trim(drIndividual("Address1"))
            If Len(drIndividual("Address2")) > 1 Then
                lblAddress1.Text = lblAddress1.Text & ","
                lblAddress2.Text = "#" & Trim(drIndividual("Address2"))
            Else
                lblAddress2.Text = Trim(drIndividual("Address2"))
            End If
            lblCity.Text = Trim(drIndividual("City"))
            lblState.Text = Trim(drIndividual("State"))
            lblZip.Text = Trim(drIndividual("Zip"))
        End If
        drIndividual.Close()
        Dim dtCurrDate As DateTime
        dtCurrDate = DateTime.Now
        Session("ADDRESS1") = lblAddress1.Text
        Session("ADDRESS2") = lblAddress2.Text
        Session("City") = lblCity.Text
        Session("State") = lblState.Text
        Session("Zip") = lblZip.Text
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" And Not Session("CustIndID") Is Nothing Then
            Session("ParentID") = Nothing
            Session("CustIndID") = Nothing
            Response.Redirect("VolunteerFunctions.aspx")
        Else
            Response.Redirect("DonorFunctions.aspx")
        End If
    End Sub

    Private Function emailvalidateion(ByVal mailstring As String) As Boolean
        ''Added for testing 20-09-2013
        Dim Valid As Boolean
        Try
            Valid = Regex.IsMatch(mailstring, "\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase)
        Catch ex As Exception

        End Try
        If Not Valid Then
            Return False
        Else
            Return True
        End If

    End Function
End Class
