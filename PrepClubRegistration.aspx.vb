'Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL

Partial Class PrepClubRegistration
    Inherits System.Web.UI.Page

    Public nRegFee As Decimal = 0
    Dim conn As SqlConnection


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Debug 
        'Session("CustIndID") = 9210  '4
        'Session("LoggedIn") = "True"
        'Session("CustSpouseID") = 9210
        'Session("EventID") = 3
        'Session("LoginChapterID") = 48
        Session("LATEFEE") = 0   'this is a variable that is going to be used in the future
        nRegFee = 0
        conn = New SqlConnection(Application("ConnectionString"))
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=p")
        End If
        If Not Page.IsPostBack Then
            LoadChildren()
            LoadPaidList()
            LoadAvailableEventsList()
            'Response.Write("Delete From Registration WHERE RegID IN (Select Distinct R.RegID FROM Registration R Inner Join EventCalendar EC On R.ECalendarId = EC.ECalendarId where  EC.EventYear >= Year(GetDate()) and   R.PaymentReference is null and GetDate() > DATEADD(day, 1, EC.RegDeadline) AND R.MemberID =" & Session("CustIndID") & ")")
            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "Delete From Registration_PrepClub WHERE RegID IN (Select Distinct R.RegID FROM Registration_PrepClub R Inner Join PrepClubCal EC On R.PrepClubCalId = EC.PrepClubCalId where  EC.EventYear >= Year(GetDate()) and R.PaymentReference is null and GetDate() > DATEADD(day, 1, EC.RegDeadline) AND R.MemberID =" & Session("CustIndID") & ")")
        Else
            'LoadPaidList()
            'LoadAvailableEventsList()

        End If
        tdPrepclub.InnerHtml = "PrepClub selection for " & " <font style='color:Maroon'>" & ddlChildren.SelectedItem.Text & "</font>"

        lblWarning.Visible = False


    End Sub


    Private Sub LoadAvailableEventsList()
        conn = New SqlConnection()
        conn.ConnectionString = Application("ConnectionString")

        Dim cmd As New SqlCommand
        Dim dsPaidList As New DataSet
        Dim da As New SqlDataAdapter


        cmd.Connection = conn
        conn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_GetSelections_PrepClubCalendar"
        cmd.Parameters.Add(New SqlParameter("@ChildNumber", ddlChildren.SelectedValue))
        'Following ChapterId 48 has to be brought

        cmd.Parameters.Add(New SqlParameter("@ChapterID", Session("CustIndChapterID")))
        cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
        cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
        da.SelectCommand = cmd
        da.Fill(dsPaidList)
        If dsPaidList.Tables(0).Rows.Count > 0 Then
            dgWkSelectionList.Visible = True
            lblWkSelectionList.Visible = False
            dgWkSelectionList.DataSource = dsPaidList.Tables(0)
            dgWkSelectionList.DataBind()

        Else
            dgWkSelectionList.Visible = False
            lblWkSelectionList.Text = "No Transactions to List"
            lblWkSelectionList.Visible = True
        End If

        enableDisableCheckBox()
        DisableGroups()
        If conn.State <> ConnectionState.Closed Then conn.Close()
    End Sub

    Private Sub enableDisableCheckBox()

        Dim dgItem As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim Lbl3 As Label
        Dim totalAmt As Double
        totalAmt = 0.0
        btnPayNow.Enabled = False
        tdSubmit.Style.Add("display", "none")
        tdWant.Style.Add("display", "none")
        Session("ContestsSelected") = Nothing
        For Each dgItem In dgWkSelectionList.Items

            selectRadioButton = dgItem.FindControl("chkEvent")
            Lbl1 = dgItem.FindControl("lblStatus")

            If Lbl1.Text.Equals("Pending") Then
                Lbl2 = dgItem.FindControl("lblFee")
                totalAmt = totalAmt + CInt(Lbl2.Text)
                Lbl3 = dgItem.FindControl("lblProductCode")
                '************************************************************************************************************************************
                '** This Session("ContestsSelected") is not used now, We are using Session("ContestsSelected") from SummarySelections.aspx.vb page **
                '************************************************************************************************************************************
                Session("ContestsSelected") = Session("ContestsSelected") & Lbl3.Text & "(" & ddlChildren.SelectedValue & ")(" & Session("CustIndChapterID") & ")(" & Lbl2.Text & ")"
                Lbl1.Style.Add("color", "Red")
                selectRadioButton.Text = "Remove"
                selectRadioButton.Attributes.Add("style", "color:Red")
                btnPayNow.Enabled = True
                tdSubmit.Style.Add("display", "block")
                tdWant.Style.Add("display", "block")
            End If
            lblTotalFee.Text = totalAmt.ToString()
        Next
    End Sub
    Private Sub LoadPaidList()

        conn = New SqlConnection()
        conn.ConnectionString = Application("ConnectionString")

        Dim cmd As New SqlCommand
        Dim dsPaidList As New DataSet
        Dim da As New SqlDataAdapter


        cmd.Connection = conn
        conn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_GetPaidPrepClubTran_Registration "
        cmd.Parameters.Add(New SqlParameter("@ChildNumber", ddlChildren.SelectedValue))
        'Following ChapterId 48 has to be brought
        cmd.Parameters.Add(New SqlParameter("@ChapterID", Session("CustIndChapterID")))
        cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
        cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
        da.SelectCommand = cmd
        da.Fill(dsPaidList)
        If dsPaidList.Tables(0).Rows.Count > 0 Then
            dgPaidList.Visible = True
            lblContestInfo.Visible = False
            dgPaidList.DataSource = dsPaidList.Tables(0)
            dgPaidList.DataBind()

        Else

            dgPaidList.DataSource = Nothing
            dgPaidList.Visible = False
            lblContestInfo.Text = "No Transactions to List"
            lblContestInfo.Visible = True
        End If


        If conn.State <> ConnectionState.Closed Then conn.Close()

    End Sub

    Private Sub LoadChildren()
        Dim objChild As New Child
        Dim dsChild As New DataSet
        Dim strWhere As String

        conn = New SqlConnection()
        conn.ConnectionString = Application("ConnectionString")
        strWhere = "MemberId='" & Session("CustIndID") & "' or SpouseID='" & Session("CustIndID") & "'"
        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, strWhere)

        If dsChild.Tables.Count > 0 Then
            If dsChild.Tables(0).Rows.Count > 0 Then
                Dim dvChildren As New DataView
                dvChildren = dsChild.Tables(0).DefaultView
                Dim dtMdate As Date = FormatDateTime(("10/1/" & (Application("ContestYear") - 1)), DateFormat.ShortDate)
                Dim strFilter As String = "((ModifyDate >= '" & dtMdate & "') or (CreateDate >= '" & dtMdate & "' and  ModifyDate is Null ))"
                dvChildren.RowFilter = strFilter

                ddlChildren.DataSource = dvChildren
                ddlChildren.DataBind()
                Session("ChildCount") = dsChild.Tables(0).Rows.Count
                ddlChildren.SelectedIndex = 0
                Session("SelectedChildID") = ddlChildren.SelectedValue

            End If
        End If

        If conn.State <> ConnectionState.Closed Then conn.Close()

        'lblDebug.Text = strWhere
        'lblDebug.Visible = True
    End Sub

    Protected Sub chkEvent_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)


        Dim objCheckBox As CheckBox
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lblFee As Label
        Dim lbl3 As Label
        Dim strSelected As String
        Dim totalFee As Double
        totalFee = 0.0

        objCheckBox = sender
        strSelected = ""
        If objCheckBox.Checked = True Then
            For Each dgItem In dgWkSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked Then
                    lblFee = dgItem.FindControl("lblFee")
                    If lblFee.Text.Trim().Length > 0 Then
                        totalFee = totalFee + Double.Parse(lblFee.Text)
                    End If

                    Lbl1 = dgItem.FindControl("lblProductCode")
                    For Each dgItem1 In dgWkSelectionList.Items
                        lbl3 = dgItem.FindControl("lblStatus")
                        If lbl3.Text <> "Pending" Then
                            If dgWkSelectionList.DataKeys(dgItem.ItemIndex) <> dgWkSelectionList.DataKeys(dgItem1.ItemIndex) Then
                                Lbl2 = dgItem1.FindControl("lblProductCode")
                                If Lbl2.Text.Equals(Lbl1.Text) Then
                                    selectRadioButton1 = dgItem1.FindControl("chkEvent")
                                    selectRadioButton1.Checked = False
                                    selectRadioButton1.Enabled = False
                                End If
                            End If
                        End If


                    Next

                End If

            Next





        ElseIf objCheckBox.Checked = False Then

            strSelected = ""
            For Each dgItem In dgWkSelectionList.Items

                selectRadioButton = dgItem.FindControl("chkEvent")


                If selectRadioButton.Checked = True Then
                    lblFee = dgItem.FindControl("lblFee")
                    If lblFee.Text.Trim().Length > 0 Then
                        totalFee = totalFee + Double.Parse(lblFee.Text)
                    End If
                    Lbl1 = dgItem.FindControl("lblProductCode")
                    strSelected = strSelected + "," + Lbl1.Text + "-" + dgWkSelectionList.DataKeys(dgItem.ItemIndex).ToString()
                End If
                '    Lbl1 = dgItem.FindControl("ItemType")
                '    For Each dgItem1 In DataGrid1.Items
                '        If DataGrid1.DataKeys(dgItem.ItemIndex) <> DataGrid1.DataKeys(dgItem1.ItemIndex) Then
                '            Lbl2 = dgItem1.FindControl("ItemType")
                '            If Lbl2.Text.Equals(Lbl1.Text) Then
                '                selectRadioButton1 = dgItem1.FindControl("SelectCheck")
                '                selectRadioButton1.Enabled = True
                '            End If
                '        End If

                '    Next

                'End If

            Next
            Dim strAr() As String
            Dim str1() As String
            Dim i As Integer
            Dim itmType As String
            Dim itmIndex As String
            strAr = strSelected.Split(",")
            If strAr.Length > 1 Then
                For i = 0 To strAr.Length - 1
                    str1 = strAr(i).Split("-")
                    If str1.Length = 2 Then
                        itmType = str1(0)
                        itmIndex = str1(1)
                        For Each dgItem In dgWkSelectionList.Items

                            selectRadioButton = dgItem.FindControl("chkEvent")
                            Lbl1 = dgItem.FindControl("lblProductCode")
                            lbl3 = dgItem.FindControl("lblStatus")
                            If lbl3.Text <> "Pending" Then
                                If Lbl1.Text.Equals(itmType) And dgWkSelectionList.DataKeys(dgItem.ItemIndex) <> itmIndex Then
                                    selectRadioButton.Enabled = False
                                Else
                                    selectRadioButton.Enabled = True
                                End If
                            End If


                        Next
                    End If

                Next
            Else

                For Each dgItem In dgWkSelectionList.Items

                    Lbl2 = dgItem.FindControl("lblStatus")
                    If Lbl2.Text <> "Pending" Then
                        selectRadioButton = dgItem.FindControl("chkEvent")
                        selectRadioButton.Enabled = True
                    End If

                Next

            End If


        End If
        totalFee = 0.0
        For Each dgItem In dgWkSelectionList.Items
            Lbl2 = dgItem.FindControl("lblStatus")
            Lbl1 = dgItem.FindControl("lblFee")
            selectRadioButton = dgItem.FindControl("chkEvent")
            If selectRadioButton.Checked = True And Lbl2.Text.Equals("Select") Then
                totalFee = totalFee + Double.Parse(Lbl1.Text)
            ElseIf selectRadioButton.Checked = False And Lbl2.Text.Equals("Pending") Then
                totalFee = totalFee + Double.Parse(Lbl1.Text)
            End If


        Next
        lblTotalFee.Text = totalFee.ToString()


    End Sub

    Public Sub DisableGroups()

        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lblIt1 As Label
        Dim lblStatus As Label



        Dim strSelected As String

        strSelected = ""

        For Each dgItem In dgWkSelectionList.Items

            selectRadioButton = dgItem.FindControl("chkEvent")
            Lbl1 = dgItem.FindControl("lblStatus")
            If Lbl1.Text.Equals("Pending") Then
                Lbl2 = dgItem.FindControl("lblProductCode")
                For Each dgItem1 In dgWkSelectionList.Items

                    lblIt1 = dgItem1.FindControl("lblProductCode")
                    lblStatus = dgItem1.FindControl("lblStatus")

                    If lblStatus.Text <> "Pending" And lblIt1.Text.Equals(Lbl2.Text) Then
                        selectRadioButton1 = dgItem1.FindControl("chkEvent")
                        selectRadioButton1.Enabled = False
                    End If
                Next

            End If
        Next

    End Sub


    Protected Sub ddlChildren_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        LoadPaidList()
        LoadAvailableEventsList()


    End Sub


    Protected Sub btnSubmitSelections_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitSelections.Click

        'insert into Registration table

        Dim dgItem As DataGridItem

        Dim selectRadioButton As CheckBox
        Dim intKeyValue As Integer
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim bolSelected As Boolean


        Dim totalFee As Double
        totalFee = 0.0

        Try
            bolSelected = False
            For Each dgItem In dgWkSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked = True Then
                    bolSelected = True
                End If

            Next

            If bolSelected = False Then

                'Display Warning message.
                lblWarning.Text = "No Event selected, please select at least one event."
                lblWarning.Visible = True
                Exit Sub
            End If


            conn = New SqlConnection()
            conn.ConnectionString = Application("ConnectionString")
            conn.Open()
            For Each dgItem In dgWkSelectionList.Items
                totalFee = 0.0
                selectRadioButton = dgItem.FindControl("chkEvent")
                Lbl1 = dgItem.FindControl("lblStatus")
                If selectRadioButton.Checked = True And Lbl1.Text <> "Pending" Then
                    ' Insert Record Into Registration
                    'GetEcal Id ECalendarID

                    intKeyValue = dgWkSelectionList.DataKeys(dgItem.ItemIndex)



                    Dim cmd As New SqlCommand
                    Dim dsEventCaldata As New DataSet
                    Dim da As New SqlDataAdapter


                    cmd.Connection = conn

                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.CommandText = "usp_getEventDataByPrepClubCalId "
                    cmd.Parameters.Add(New SqlParameter("@PrepClubCalId", intKeyValue))
                    Response.Write(intKeyValue)
                    da.SelectCommand = cmd
                    da.Fill(dsEventCaldata)
                    If dsEventCaldata.Tables(0).Rows.Count > 0 Then
                        'MsgBox(dsEventCaldata.Tables(0).Rows(0)("ProductCode"))
                        Dim param(14) As SqlParameter
                        param(0) = New SqlParameter("@ChapterID", dsEventCaldata.Tables(0).Rows(0)("ChapterID"))
                        param(1) = New SqlParameter("@ChapterCode", dsEventCaldata.Tables(0).Rows(0)("ChapterCode"))
                        param(2) = New SqlParameter("@EventID", dsEventCaldata.Tables(0).Rows(0)("EventId"))
                        param(3) = New SqlParameter("@EventCode", dsEventCaldata.Tables(0).Rows(0)("EventCode"))
                        param(4) = New SqlParameter("@ProductGroupID", dsEventCaldata.Tables(0).Rows(0)("ProductGroupID"))
                        param(5) = New SqlParameter("@ProductGroupCode", dsEventCaldata.Tables(0).Rows(0)("ProductGroupCode"))
                        param(6) = New SqlParameter("@ProductID", dsEventCaldata.Tables(0).Rows(0)("ProductID"))
                        param(7) = New SqlParameter("@ProductCode", dsEventCaldata.Tables(0).Rows(0)("ProductCode"))
                        param(8) = New SqlParameter("@MemberID", Session("CustIndID"))
                        param(9) = New SqlParameter("@ChildNumber", ddlChildren.SelectedValue)
                        param(10) = New SqlParameter("@EventDate", dsEventCaldata.Tables(0).Rows(0)("EventDate"))
                        param(11) = New SqlParameter("Fee", dsEventCaldata.Tables(0).Rows(0)("EventFee"))
                        param(12) = New SqlParameter("@CreatedBy", Session("LoginID"))
                        param(13) = New SqlParameter("@PrepClubCalId", intKeyValue)
                        param(14) = New SqlParameter("@RegID", SqlDbType.Int)
                        param(14).Direction = ParameterDirection.Output
                        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_InsertNew_Registration_PrepClub", param)

                    End If
                ElseIf selectRadioButton.Checked = True And Lbl1.Text.Equals("Pending") Then
                    intKeyValue = dgWkSelectionList.DataKeys(dgItem.ItemIndex)
                    Lbl2 = dgItem.FindControl("lblRegId")
                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_DeletePending_Registration_PrepClub", New SqlParameter("@RegId", Integer.Parse(Lbl2.Text)))
                End If

            Next
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            LoadAvailableEventsList()
            ' Server.Transfer("WkShopREgistration.aspx")
            lblWarning.Text = " Submitted Successfully"
            lblWarning.Visible = True
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            Response.Write(ex.ToString())
            'lblDataErr.Text = ex.Message
            'lblDataErr.Visible = True
            'Return False
        End Try

    End Sub

    Protected Sub btnPayNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayNow.Click
        'Session("ContestsSelected") = Session("ContestsSelected")  & "RegFee:" & lblTotalFee.Text
        Session("RegFee") = lblTotalFee.Text
        Session("Mealsamount") = 0
        Session("LateFee") = 0
        Page.Response.Redirect("SummarySelections_PrepClub.aspx")
    End Sub
End Class
