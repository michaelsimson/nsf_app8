﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.Sql;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class CoachingRegisCount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            int year = Convert.ToInt32(DateTime.Now.Year);
            lstYear.Items.Insert(0, new ListItem(Convert.ToString(year - 1)));
            lstYear.Items.Insert(1, new ListItem(Convert.ToString(year)));
            lstYear.Items.Insert(2, new ListItem(Convert.ToString(year + 1)));
            lstYear.Items[1].Selected = true;
            GetProductGroup(lstProductGroup);
            //if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "88") || (Session["RoleId"].ToString() == "89")))
            //{
                
            //}
            //else
            //    Response.Redirect("~/VolunteerFunctions.aspx");
        }

    }
   
    private void GetProductGroup(ListBox ddlObject)
    {
        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where   P.EventId=13  order by P.ProductGroupID"); //EF.EventYear>=YEAR(GETDATE()) AND
        ddlObject.DataSource = dsProductGroup;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "ProductGroupID";
        ddlObject.DataBind();
        if (ddlObject.Items.Count < 1)
        {
            lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";

        }
        else if (ddlObject.Items.Count > 0)
        {
            ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
            GetProduct();
            lblerr.Text = "";
        }

    }
    private void GetProduct()
    {
        string prodGrp = "";
        if (lstProductGroup.Items[0].Selected == false)
        {
            prodGrp = " and P.ProductGroupID in (0";
            foreach (ListItem i in lstProductGroup.Items)
            {
                if (i.Selected)
                {
                    prodGrp = prodGrp + "," + i.Value;

                }
            }
            prodGrp = prodGrp + ") ";
        }

        DataSet dsProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select Distinct P.ProductID, Case when P.CoachName is Not Null THEN P.CoachName Else P.Name END  as Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where P.EventID=13 " + prodGrp + " order by P.ProductID"); // EF.EventYear>=YEAR(GETDATE()) AND
        lstProduct.DataSource = dsProduct;
        lstProduct.DataTextField = "Name";
        lstProduct.DataValueField = "ProductID";
        lstProduct.DataBind();
        if (lstProduct.Items.Count < 1)
        {
            lblerr.Text = "No Product is open. Please Contact admin and Get Product Opened in EventFees table";
        }
        else if (lstProduct.Items.Count > 0)
        {
            lstProduct.Items.Insert(0, new ListItem("All", "0"));
            lstProduct.SelectedIndex = 0;
            lstProduct.Enabled = true;
            lblerr.Text = "";
            if (lstProductGroup.Items[0].Selected == true)
                lstProduct.Enabled = false;
        }
    }

    protected void lstProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetProduct();
    }
    protected void btnGetCount_Click(object sender, EventArgs e)
    {

        try { 
            string wherecntn = "";
        lblerr.Text = "";
       
        if (lstProductGroup.Items[0].Selected != true)
        {
            if (lstProduct.Items[0].Selected == true)
            {

                wherecntn = wherecntn + " AND CR.ProductID in (0";
                foreach (ListItem i in lstProduct.Items)
                {
                    wherecntn = wherecntn + "," + i.Value;
                }
                wherecntn = wherecntn + ") ";
            }
            else
            {

                wherecntn = wherecntn + " AND CR.ProductID in (0";
                foreach (ListItem i in lstProduct.Items)
                {
                    if (i.Selected)
                    {
                        wherecntn = wherecntn + "," + i.Value;

                    }
                }
                wherecntn = wherecntn + ") ";
            }
        }
        wherecntn = wherecntn + " AND CR.EventYear in (0";
        foreach (ListItem i in lstYear.Items)
        {
            if (i.Selected)
            {
                wherecntn = wherecntn + "," + i.Value;

            }
        }
        wherecntn = wherecntn + ") ";
        wherecntn = wherecntn + " AND CR.Phase=" + ddlPhase.SelectedValue + " and C.Accepted='Y' ";
        //string StrSQL = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND' AND (newsletter not in ('5') OR (Newsletter is null)) and automemberid in (select PMemberid from CoachReg where " + wherecntn + "))) or (donortype = 'SPOUSE' AND (newsletter not in ('5') OR (Newsletter is null))  and relationship in (select pMemberid from CoachReg  where " + wherecntn + ")) group by Email";
        string StrSQL = " Select FirstName,LastName,ProductCode,Level,SUM(Paid) as Paid, Sum(Pending) as Pending, Sum (Approved) as Approved, SUM(PaidAmt) as PaidAmt,MaxCapacity as Capacity,ProductCode,SessionNo from";
        StrSQL = StrSQL + " ((select I.FirstName,I.LastName,CR.Level,count(CR.CoachRegID) as Paid, 0 as Pending, 0 as Approved, count(CR.CoachRegID) * EF.RegFee as PaidAmt ,C.MaxCapacity,C.SessionNo ,cr.ProductCode,cr.CMemberID  from CoachReg CR Inner Join CalSignup C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join (SELECT DISTINCT E.EventID ,E.eventYear ,E.ProductID, CASE WHEN E.ProductLevelPricing = 'N' THEN RegFee/COUNT(P.ProductGroupId) ELSE E.RegFee END as RegFee  FROM EventFees E INNER JOIN Product P ON P.ProductGroupId = E.ProductGroupID Group By E.EventID ,E.eventYear ,E.ProductID,RegFee,ProductLevelPricing ) EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID Inner Join IndSpouse I ON C.MemberID =I.AutoMemberID   where CR.PaymentReference is Not NUll " + wherecntn + " Group by cr.CMemberID,I.FirstName,I.LastName,cr.Level,cr.ProductCode ,c.MaxCapacity,EF.RegFee,C.SessionNo) Union All "; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null) //Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + " (select I.FirstName,I.LastName,CR.Level,0 as Paid, count(CR.CoachRegID) as Pending, 0 as Approved, 0.00 as PaidAmt ,C.MaxCapacity,C.SessionNo ,cr.ProductCode,cr.CMemberID from CoachReg CR Inner Join CalSignup C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID =I.AutoMemberID   where CR.PaymentReference is NUll " + wherecntn + " Group by cr.CMemberID,I.FirstName,I.LastName,cr.Level,cr.ProductCode ,c.MaxCapacity,C.SessionNo) Union All"; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + " select I.FirstName,I.LastName,CR.Level,0 as Paid, 0 as Pending, count(CR.CoachRegID) as Approved, 0.00 as PaidAmt ,C.MaxCapacity,C.SessionNo ,cr.ProductCode,cr.CMemberID from CoachReg CR Inner Join CalSignup C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level and CR.EventYear=C.EventYear and C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID =I.AutoMemberID   where CR.Approved='Y' " + wherecntn + " Group by cr.CMemberID,I.FirstName,I.LastName,cr.Level,cr.ProductCode ,c.MaxCapacity,C.SessionNo ) temp"; //((Case when C.ProductCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductCode in('UV') then CR.Level end)Is null)//Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
        StrSQL = StrSQL + " Group by temp.ProductCode,Level,CMemberID,ProductCode,FirstName,LastName,MaxCapacity,SessionNo order By temp.ProductCode,LastName,FirstName";
        string[] tblEmails = new string[] { "EmailContacts" };
        StringBuilder sbEmailList = new StringBuilder();
         //Response.Write(StrSQL.ToString ());
        DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
         
        if (dsCount.Tables[0].Rows.Count > 0)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("SNo", typeof(string));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("LastName", typeof(string));
            dt.Columns.Add("ProductCode", typeof(string));
            dt.Columns.Add("Level", typeof(string));
            dt.Columns.Add("SessionNo", typeof(string));
            dt.Columns.Add("Paid", typeof(int));
            dt.Columns.Add("Pending", typeof(int));
            dt.Columns.Add("Approved", typeof(int));
            dt.Columns.Add("Capacity", typeof(int));
            dt.Columns.Add("PaidAmt", typeof(double));
            DataRow dr;
            int Paid=0, Pending=0, Approved=0, Capacity=0;
            double PaidAmt=0.00;
            for (int i = 0; i < dsCount.Tables[0].Rows.Count; i++)
            {
                dr = dt.NewRow();
                dr["SNo"] = (i + 1).ToString();
                dr["FirstName"] = dsCount.Tables[0].Rows[i]["FirstName"].ToString();
                dr["LastName"] = dsCount.Tables[0].Rows[i]["LastName"].ToString();
                dr["ProductCode"] = dsCount.Tables[0].Rows[i]["ProductCode"].ToString();
                dr["Level"] = dsCount.Tables[0].Rows[i]["Level"].ToString();
                dr["SessionNo"] =dsCount.Tables[0].Rows[i]["SessionNo"].ToString();
                dr["Paid"] = Convert.ToInt32 (dsCount.Tables[0].Rows[i]["Paid"].ToString());
                dr["Pending"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Pending"].ToString());
                dr["Approved"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Approved"].ToString());
                dr["Capacity"] = Convert.ToInt32(dsCount.Tables[0].Rows[i]["Capacity"].ToString());
                dr["PaidAmt"] = Convert.ToDouble(dsCount.Tables[0].Rows[i]["PaidAmt"].ToString());
                Paid = Paid + Convert.ToInt32(dr["Paid"].ToString ());
                Pending = Pending + Convert.ToInt32(dr["Pending"].ToString());
                Approved = Approved + Convert.ToInt32(dr["Approved"].ToString());
                Capacity = Capacity + Convert.ToInt32(dr["Capacity"].ToString());
                PaidAmt = PaidAmt + Convert.ToDouble(dr["PaidAmt"].ToString());
                dt.Rows.Add(dr);
            }
            dr = dt.NewRow();
            dr["SNo"] = "";
            dr["FirstName"] = "Total ";
            dr["LastName"] = "";
            dr["ProductCode"] = "";
            dr["Level"] = "";
            dr["SessionNo"] = "";
            dr["Paid"] = Paid;
            dr["Pending"] = Pending;
            dr["Approved"] = Approved;
            dr["Capacity"] = Capacity;
            dr["PaidAmt"] = PaidAmt;
            dt.Rows.Add(dr);
            grdCoaching.Visible = true;
            grdCoaching.DataSource = dt;
            grdCoaching.DataBind();
        }
        else
        {
            lblerr.Text = "Sorry, No Coaching Registration Found";
            grdCoaching.Visible = false;
        }
        }
        catch(Exception ex) {
           lblerr.Text =  ex.ToString() ;
        }
    }
}
