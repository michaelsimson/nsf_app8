﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb


Partial Class nsfupload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'check user permissions

    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImport.Click
        If fulCardFile.HasFile Then
            Try
                lblMessage.ForeColor = Drawing.Color.Black
                'get server path to store transaction files with date stamp
                Dim TransFilePath As String = AppDomain.CurrentDomain.SetupInformation.ApplicationBase & "UploadFiles\"
                Dim TransFile As String = "CardTrans_" & Format(Date.Now, "yyyyMMdd_hhmm") & ".csv"

                'check if .csv file is selected or not

                'save file on the server
                fulCardFile.SaveAs(TransFilePath & TransFile)
                lblMessage.Text = "File name: " & fulCardFile.PostedFile.FileName & " has been imported. <br>"

                'import csv file to database
                Call ImportCSV_Database(TransFilePath & TransFile)

            Catch ex As Exception
                lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Text = "ERROR: " & ex.Message.ToString()
            End Try
        Else
            lblMessage.ForeColor = Drawing.Color.Red
            lblMessage.Text = "You have not specified a file."
        End If

    End Sub

    Private Sub ImportCSV_Database(ByVal TransFileWithPath As String)

        Dim dbConn As SqlConnection
        Try
            'open NSF database connection
	    dbConn  = New SqlConnection(Application("ConnectionString"))
            dbConn.Open()  
        Catch ex As Exception
		lblMessage.ForeColor = Drawing.Color.Red
                lblMessage.Text = "ERROR in Opening the database: " & ex.Message.ToString()
        End Try


        'open imported file as dataset
        Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
        Dim dsFile As DataSet = OpenFileConnection(TransFile)

        Dim RowsImported As Integer = 0
        For Each TranRow As DataRow In dsFile.Tables(0).Rows
            If TranRow(0).ToString.Trim <> "" And TranRow(0).ToString.Trim.Contains("Date") = False Then
                Dim Tdate As String = TranRow(0).ToString.Replace("""", "").Replace(",", "").Trim
                Dim CheckNum As String = TranRow(1).ToString.Replace("""", "").Replace(",", "").Trim
                Dim Description As String = TranRow(2).ToString.Replace("""", "").Replace(",", "").Trim
                Dim WithdrawalAmount As String = TranRow(3).ToString.Replace("""", "").Replace(",", "").Trim
                Dim DepositAmount As String = TranRow(4).ToString.Replace("""", "").Replace(",", "").Trim
                Dim AdditionalInfo As String = TranRow(5).ToString.Replace("""", "").Replace(",", "").Trim
                If Len(WithdrawalAmount) = 0 Then
                    WithdrawalAmount = 0
                End If

                If Len(CheckNum) = 0 Then
                    CheckNum = 0
                End If

                If Len(DepositAmount) = 0 Then
                    DepositAmount = 0
                End If
                If Len(AdditionalInfo) = 0 Then
                    AdditionalInfo = Nothing
                End If

                If Len(Description) = 0 Then
                    Description = String.Empty
                End If

                'create insert statement
                Try
                    Dim SqlInsert As String = "INSERT INTO HBT2Temp(Tdate, CheckNum, Description,WithdrawalAmount, DepositAmount,AdditionalInfo) VALUES("
                    SqlInsert = SqlInsert & "'" & Tdate & "'," & CheckNum & ",'" & Description & "'," & WithdrawalAmount & "," & DepositAmount & ",'" & AdditionalInfo & "')"
               
                    lblMessage.Text = lblMessage.Text & "<br>" & SqlInsert
                    'create SQL command 
                    Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                    RowsImported += cmd.ExecuteNonQuery()
                Catch ex As Exception
                    lblMessage.ForeColor = Drawing.Color.Red
                    lblMessage.Text = lblMessage.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
                End Try
            End If
        Next


        'display number of rows imported
        lblMessage.Text = lblMessage.Text & "<br> Rows Imported Successfully: " & RowsImported

        
    End Sub

    Private Function OpenFileConnection(ByVal TransFileTable As FileInfo) As DataSet
        Dim dsFile As New DataSet
        Try

            Dim ConnCSV As String = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=Yes;FMT=Delimited';Data Source=" & TransFileTable.DirectoryName.ToString()
            Dim sqlSelect As String
            Dim objOleDBConn As OleDbConnection
            Dim objOleDBDa As OleDbDataAdapter
            objOleDBConn = New OleDbConnection(ConnCSV)
            objOleDBConn.Open()
            sqlSelect = "select * from [" + TransFileTable.Name.ToString() + "]"
            objOleDBDa = New OleDbDataAdapter(sqlSelect, objOleDBConn)
            objOleDBDa.Fill(dsFile)
            objOleDBConn.Close()
        Catch ex As Exception
            lblMessage.Text = lblMessage.Text & "<br> ERROR in OpenFileConnection: " & ex.Message.ToString()
        End Try
        Return dsFile
    End Function

End Class
