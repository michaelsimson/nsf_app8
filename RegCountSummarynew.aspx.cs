﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;


public partial class RegCountSummarynew : System.Web.UI.Page
{
    string StaYear = System.DateTime.Now.Year.ToString();
    string calc;
    int Start;
    int endall;
    string Qry;
    string Qrycondition;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["RoleId"] = "2";
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }
            else
            {
                if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || (Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5")))
                {
                    Iddonation.Visible = true;
                    DDlfrontchoice(DDchoice);
                    Event();
                    Yearscount();
                    years();
                    Pnldisp.Visible = false;
                    LBbacktofront.Visible = true;
                }
                if ((Session["RoleId"].ToString() == "5"))
                {
                    Chapter();
                    Cluster();
                    Event();
                    Yearscount();
                    years();
                }
                else if ((Session["RoleId"].ToString() == "3"))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct zoneid from volunteer where MemberID='" + Session["LoginID"] + "' and TeamLead='" + Session["TL"] + "'  and zoneid is not null");
                    DataTable dt = ds.Tables[0];
                    Txthidden.Text = dt.Rows[0]["zoneid"].ToString();
                    if (Txthidden.Text != "")
                    {
                        Zone();
                        Cluster();
                        Chapter();
                        Event();
                        Yearscount();
                        years();
                    }
                    else
                    {
                        Pnldisp.Visible = false;
                        lblNoPermission.Text = "Sorry, you don't have ZoneCode ";
                    }
                }
                else if ((Session["RoleId"].ToString() == "4"))
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct clusterid from volunteer where MemberID='" + Session["LoginID"] + "' and clusterid is not null");
                    DataTable dt = ds.Tables[0];
                    Txthidden.Text = dt.Rows[0]["clusterid"].ToString();
                    if (Txthidden.Text != "")
                    {
                        Cluster();
                        Zone();
                        Chapter();
                        Event();
                        Yearscount();
                        years();
                    }
                    else
                    {
                        Pnldisp.Visible = false;
                        lblNoPermission.Text = "Sorry, you don't have Cluster ";
                    }
                }
                else
                {
                    Zone();
                    Event();
                    Yearscount();
                    years();
                }
            }
        }

    }

    protected void DDlfrontchoice(DropDownList DDobject)
    {
        DDobject.Items.Clear();
        DDobject.Items.Insert(0, new ListItem("By Contest Group", "3"));
        DDobject.Items.Insert(0, new ListItem("By Contest", "4"));
        DDobject.Items.Insert(0, new ListItem("By Event", "2"));
        DDobject.Items.Insert(0, new ListItem("By Chapter", "1"));
        DDobject.Items.Insert(0, new ListItem("[Select Report]", "0"));
    }
    protected void Event()
    {
        ddevent.Items.Clear();
        if ((Session["RoleId"].ToString() == "1") || ((Session["RoleId"].ToString() == "2")))
        {
            ddevent.Items.Insert(0, new ListItem("Coaching", "13"));
            ddevent.Items.Insert(0, new ListItem("Game", "4"));
        }
        ddevent.Items.Insert(0, new ListItem("PrepClub", "19"));
        ddevent.Items.Insert(0, new ListItem("Workshop", "3"));
        ddevent.Items.Insert(0, new ListItem("Finals", "1"));
        ddevent.Items.Insert(0, new ListItem("Chapter", "2"));
        ddevent.Items.Insert(0, new ListItem("All", "-1"));
        ddevent.Items.Insert(0, new ListItem("[Select Event]", "0"));
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    #region Zone
    private string GetZoneQuery()
    {
        string ZoneQuery = "Select distinct Name,ZoneId from Zone where Status='A'";
        string ZoneFields = " Select ZoneCode,ZoneId from ";
        switch (Session["RoleId"].ToString())
        {
            case "5":
                return ZoneFields + " chapter where ChapterID='" + ddchapter.SelectedValue + "'";
            case "3":
                return ZoneFields + " Zone where  ZoneId='" + Txthidden.Text + "'  and ZoneCode is not null";
            case "4":
                return ZoneFields + " Cluster where ClusterId='" + Txthidden.Text + "'";
            case "1":
            case "2":
            case "84":
            case "85":
            default:
                return ZoneQuery;
        }
        return ZoneQuery;
    }
    private void InsertZoneDropDown(string Caption, string Value, bool IsEnabled)
    {
        ddZone.Items.Insert(0, new ListItem(Caption, Value));
        ddZone.Enabled = IsEnabled;
    }
    private void Zone()
    {
        try
        {
            ddZone.Items.Clear();
            switch (ddevent.SelectedItem.Text)
            {
                case "All":
                    InsertZoneDropDown("All", "-1", true);
                    ddCluster.Items.Insert(0, "All");
                    return;
                case "Finals":
                    InsertZoneDropDown(ddevent.SelectedItem.Text, "1", false);
                    return;
                case "Coaching":
                    InsertZoneDropDown(ddevent.SelectedItem.Text, "13", false);
                    return;
                case "Game":
                    InsertZoneDropDown(ddevent.SelectedItem.Text, "9", false);
                    return;

            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, GetZoneQuery());
            ddZone.DataSource = ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                ddZone.DataValueField = "ZoneId";
                ddZone.DataBind();
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddZone.Enabled = false;
                }
                else
                {
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        ddZone.Items.Insert(0, "All");
                    }
                    else
                    {
                        ddZone.Items.Insert(0, "All");
                        ddZone.Items.Insert(0, new ListItem("[Select Zone]", "-1"));

                    }
                    ddZone.Enabled = true;
                }
            }
            else
            {
                ddZone.Items.Insert(0, new ListItem("[Select Zone]", "-1"));
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    #endregion
    protected void Cluster()
    {
        try
        {
            ddCluster.Enabled = true;
            ddCluster.Items.Clear();
            string ClusterQuery = "select ClusterId,clustercode from ";
            switch (Session["RoleId"].ToString())
            {
                case "5":
                    ClusterQuery = ClusterQuery + " chapter where ChapterID='" + ddchapter.SelectedValue + "'";
                    break;
                case "4":
                    ClusterQuery = ClusterQuery + " Cluster  where clusterId='" + Txthidden.Text + "'";
                    break;
                case "3":
                    ClusterQuery = ClusterQuery + " Cluster  where clusterId='" + Txthidden.Text + "'";
                    break;
                default:
                    if (ddZone.SelectedItem.Text == "All")
                    {
                        ClusterQuery = "select  distinct clustercode,clusterid from cluster where Status='A' ";
                    }
                    else
                    {
                        ClusterQuery = "select distinct Name as clustercode,ClusterID from Cluster where Status='A' and ZoneID='" + ddZone.SelectedValue + "'";
                    }
                    break;
            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, ClusterQuery);
            ddCluster.DataSource = ds;
            ddCluster.DataTextField = "clustercode";
            ddCluster.DataValueField = "ClusterId";
            ddCluster.DataBind();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
            {

            }
            else
            {
                ddCluster.Enabled = true;
            }

            if (ddZone.SelectedItem.Text == "All")
            {

                ddCluster.Items.Insert(0, new ListItem("All", "-1"));
            }
            else if (ddevent.SelectedItem.Text == "Finals")
            {

                ddCluster.Items.Insert(0, new ListItem("Finals", "1"));
                ddCluster.Enabled = false;
            }
            else if (ddevent.SelectedItem.Text == "Coaching")
            {

                ddCluster.Items.Insert(0, new ListItem("Coaching", "13"));
                ddCluster.Enabled = false;
            }
            else if (ddevent.SelectedItem.Text == "Game")
            {

                ddCluster.Items.Insert(0, new ListItem("Game", "58"));
                ddCluster.Enabled = false;
            }
            else
            {
                ddCluster.Items.Insert(0, "[Select Cluster]");

            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Chapter()
    {
        try
        {
            ddchapter.Enabled = true;
            if ((Session["RoleId"].ToString() == "5"))
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode from volunteer where  MemberID='" + Session["LoginID"] + "' and ChapterId is not null");
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddchapter.Items.Clear();
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    Cluster();
                    ddchapter.Enabled = false;
                }
                else
                {
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode,[State] from chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        Cluster();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "All")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterid,chaptercode,[state] from chapter where clusterId='" + ddCluster.SelectedValue + "' and ChapterId is not null order by [State],ChapterCode");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        ddchapter.Enabled = true;
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }
            else
            {
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    if (ddCluster.SelectedItem.Text == "All")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, "All");
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Finals, US", "1"));
                        ddchapter.Enabled = false;
                    }
                    else if (ddevent.SelectedItem.Text == "Coaching")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Coaching,US", "112"));
                        ddchapter.Enabled = false;

                    }
                    else if (ddevent.SelectedItem.Text == "Game")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Game,US", "117"));
                        ddchapter.Enabled = false;
                    }
                    else
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ChapterID,[state],chaptercode from Chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "' order by [State],ChapterCode");
                        ddchapter.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddchapter.DataTextField = "Name";
                            ddchapter.DataValueField = "ChapterID";
                            ddchapter.DataBind();
                            ddchapter.Items.Insert(0, "All");
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                        else
                        {
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                    }
                }
                else
                {
                    ddchapter.Items.Clear();
                    ddchapter.Items.Insert(0, "[Select Chapter]");
                }
            }
            if (ddCluster.SelectedItem.Text == "All")
            {
                string ClusterFlqry;
                string WhereCluster;
                ClusterFlqry = "select distinct chapterid,chaptercode,[State] from chapter where  ChapterId is not null";
                WhereCluster = ClusterFlqry + Filterdropdown();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, WhereCluster);
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                ddchapter.Enabled = true;
                ddchapter.DataTextField = "chaptercode";
                ddchapter.DataValueField = "ChapterID";
                ddchapter.DataBind();
                ddchapter.Items.Insert(0, "All");
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected void Yearscount()
    {
        ddNoyear.Items.Clear();
        ddNoyear.Items.Add("[Select No of Years]");

        for (int i = 1; i <= 10; i++)
        {
            ddNoyear.Items.Add(i.ToString());
        }
    }
    protected void years()
    {
        DDyear.Items.Clear();
        DDyear.Items.Add("Year");
        DDyear.Items.Add("Calendar Year");
        DDyear.Items.Add("Fiscal Year");
    }
    protected string Filterdropdown()
    {
        string iCondtions = string.Empty;
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ZoneId=" + ddZone.SelectedValue;
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ClusterId=" + ddCluster.SelectedValue;
            }
        }
        return iCondtions + "  order by [State],ChapterCode";
    }
    protected void ddevent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddevent.SelectedItem.Value.ToString() == "13")
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                Lbreport.Visible = true;
                DDReport.Enabled = true;
                DDReport.Visible = true;
            }
        }
        else
        {
            Lbreport.Visible = false;
            DDReport.Visible = false;
        }
        Zone();
        Cluster();
        Chapter();
    }
    protected void ddZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cluster();
        Chapter();
    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void DDchoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {
            IDchapter.Visible = true;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "3")
        {
            Divevent.Visible = false;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
            Divcontest.Visible = false;
            Divcontestgroup.Visible = true;
        }
        else if (DDchoice.SelectedItem.Value.ToString() == "4")
        {
            Divevent.Visible = false;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
            Divcontestgroup.Visible = false;
            Divcontest.Visible = true;
        }
        else
        {
            Divevent.Visible = true;
            Iddonation.Visible = false;
            Pnldisp.Visible = true;
            Divchoice.Visible = false;
        }

    }
    protected void ddCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        Chapter();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        if ((ddevent.SelectedValue.ToString() == "13"))
        {
            if (DDchoice.SelectedItem.Value.ToString() == "1")
            {
                if (DDReport.SelectedItem.Text != "Select Report")
                {
                    buttonclick();
                }
                else
                {
                    lblall.Visible = true;
                    Gridcontestant.Visible = false;
                }
            }
            else
            {
                buttonclick();
            }

        }
        else
        {
            buttonclick();
        }
    }
    protected string genWhereConditons()
    {
        string iCondtions = string.Empty;
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and cs.EventID=" + ddevent.SelectedValue;
            }
        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    if (ddevent.SelectedItem.Text != "All")
                    {
                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Coaching") && (ddevent.SelectedItem.Text != "Game"))
                    {
                        if (ddevent.SelectedItem.Text != "All")
                        {
                            iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                        }
                    }
                }
            }
        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    if (ddevent.SelectedItem.Text != "All")
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }
                else
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Coaching") && (ddevent.SelectedItem.Text != "Game"))
                    {
                        if (ddevent.SelectedItem.Text != "All")
                        {
                            iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                        }
                    }
                }
            }
        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                if ((Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5"))
                {
                    if (ddevent.SelectedItem.Text != "All")
                    {
                        iCondtions += " and cs.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                    }
                }
                else
                {
                    if ((ddchapter.SelectedItem.Text != "Coaching,US") && ((ddchapter.SelectedItem.Text != "Game,US")))
                    {
                        if ((ddevent.SelectedItem.Value.ToString() == "2"))
                        {
                            if (ddchapter.SelectedItem.Value.ToString() != "1")
                            {
                                iCondtions += " and cs.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                            }
                        }
                        else
                        {
                            if (ddevent.SelectedItem.Text != "All")
                            {
                                iCondtions += " and cs.ChapterID=" + ddchapter.SelectedItem.Value.ToString();
                            }
                        }
                    }
                }
            }
        }
       
    }
        //if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        //{
        //    calc = ddNoyear.SelectedItem.Text;
        //    Start = Convert.ToInt32(StaYear);
        //    endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
        //}
        //else
        //{
        //    Start = Convert.ToInt32(StaYear);
        //    endall = Convert.ToInt32(StaYear) - 5;
        //}
        //int sta = Convert.ToInt32(Start);
        //string WCNT = string.Empty;
        //string am = string.Empty;
        //string Tot = string.Empty;
        //string strtot = string.Empty;
        //string Totwhere = string.Empty;
        //for (int i = endall; i <= sta; i++)
        //{
        //    if (WCNT != string.Empty)
        //    {
        //        WCNT = WCNT + ",[" + i.ToString() + "]";
        //        am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
        //        Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
        //    }
        //    else
        //    {
        //        WCNT = "[" + i.ToString() + "]";
        //        Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
        //        am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
        //    }
        //}
        //Totwhere = "(" + Tot + ")!=0";
        if (DDchoice.SelectedItem.Value.ToString() == "1")
        {

            return iCondtions + " group by  cs.ChapterID,ch.ChapterCode,cs.contestyear) select ChapterID,ChapterCode " + am + ",'total'=" + Tot + " from tbl pivot(sum(amt) for [Year] in (" + WCNT + "))As PVTTBL where" + Totwhere + "order by total desc";

        }
        else
        {
        }
   
}
   
    protected void buttonclick()
    {
         int numval;
            //chaptercondition = false;
            if ((ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
            {
                lblall.Visible = false;
                if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
                {
                    calc = ddNoyear.SelectedItem.Text;
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);
                }
              
                if (DDyear.SelectedItem.Text != "Fiscal Year")
                {
                    if (DDchoice.SelectedItem.Value.ToString() == "1")
                    {
                       if ((ddevent.SelectedItem.Value.ToString() == "1"))
                       {
                      FunctionsForChapter("cs.EventId=1 and cs.chapterid=1");
                      }

                   Qry = Qrycondition + genWhereConditons();
                    }
                }
            }
    }
  
    private void FunctionsForChapter(string EventVAL)
    {
        Qrycondition = "with tbl as (select   cs.ChapterID,ch.ChapterCode,cs.contestyear as year ,Count(*)as amt from Contestant CS  left join chapter ch on ch.ChapterID=CS.ChapterId where   cs.contestyear   between '"
            + endall + "' and '" + Start + "' and " + EventVAL + " and PaymentReference is not null ";
    }
   
    protected void Button2_Click(object sender, EventArgs e)
    {

    }

  
   
}