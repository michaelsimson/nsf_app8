Imports System.Data
Imports System.Data.SqlClient
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Partial Class AddOrganization
    Inherits System.Web.UI.Page
    Dim strSqlQuery As String
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("LoginID"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("entryToken"))) = 0 Then
                Server.Transfer("Maintest.aspx")
            End If

            strSqlQuery = "SELECT AutoMemberID FROM OrganizationInfo where upper(Organization_Name) ='" & txtOrgName.Text.ToUpper() & "'"
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim drOrganizations As SqlDataReader
            drOrganizations = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
            If drOrganizations.Read() Then
                lblMessage.Text = "Organization Name already exists."
            ElseIf Page.IsValid = False Then
                lblMessage.Text = "Please fill/Select all data."
            Else    'Response.End()

                Dim organization As New Organization()
                If Request.QueryString("Id") Is Nothing Then
                    organization.AutoMemberID = organization.GetMaxOrganizationID(Application("ConnectionString"))
                Else
                    organization.AutoMemberID = Request.QueryString("Id")
                End If
                organization.ChapterID = ddlNSFChapter.SelectedItem.Value
                organization.CreatedDate = CDate(Date.Now.ToShortDateString)
                organization.ModifyDate = CDate(Date.Now.ToShortDateString)
                organization.MemberSince = CDate(Date.Now.ToShortDateString)
                'organization.MemberID = Session("CustIndID")
                Dim MemberID As String = "OWN" & Year(CDate(Date.Now)) & "-" & Right("0" & Month(CDate(Date.Now)), 2) & "-" & Right("0" & Day(CDate(Date.Now)), 2) & "-" & Right("0" & Hour(CDate(Date.Now)), 2) & "-" & Right("0" & Minute(CDate(Date.Now)), 2) & "-" & Right("0" & Second(CDate(Date.Now)), 2)
                organization.MemberID = MemberID
                organization.Address1 = txtAddress1.Text
                organization.Address2 = txtAddress2.Text
                organization.BusType = ddlBusinessType.SelectedItem.Text
                organization.City = txtCity.Text
                organization.Country = ddlCountry.SelectedItem.Value
                organization.CreatedBy = Session("LoginID")
                organization.MemberSince = CDate(Date.Now.ToShortDateString)
                organization.DeletedFlag = "No"
                organization.DeleteReason = ""
                organization.Email = txtEmail.Text
                organization.Email2 = txtEmail2.Text
                organization.Fax = txtFax.Text
                organization.FirstName = txtFirstName.Text
                organization.Initial = txtMiddleName.Text
                organization.IRSCat = ddlIRSCat.SelectedItem.Value
                organization.LastName = txtLastName.Text
                organization.LiasonPerson = txtLiasonPerson.Text
                organization.MailingLabel = ddlMailingLabel.SelectedItem.Value
                organization.MatchingGift = ddlMatchingGift.SelectedItem.Value
                organization.ModifiedBy = 0

                'organization.NSFChapter = Right(ddlNSFChapter.SelectedItem.Text, Len(ddlNSFChapter.SelectedItem.Text) - InStr(1, ddlNSFChapter.SelectedItem.Text, ",", CompareMethod.Text))
                organization.NSFChapter = ddlNSFChapter.SelectedItem.Text
                organization.OrganizationName = txtOrgName.Text
                organization.Phone = txtPhone.Text
                organization.ReferredBy = txtReferredBy.Text
                organization.SendMail = ddlSendMail.SelectedItem.Value
                organization.SendNewsLetter = ddlSendNewsLetter.SelectedItem.Value
                organization.SendReceipt = ddlSendReceipt.SelectedItem.Value
                organization.Sponsor = ddlSponsor.SelectedItem.Value
                organization.State = ddlState.SelectedItem.Value
                organization.Title = ddlTitle.SelectedItem.Value
                organization.Venue = ddlVenue.SelectedItem.Value
                organization.WebSite = txtWebsite.Text
                organization.Zip = txtZip.Text
                organization.Matching_Gift_URL = IIf(TrGiftURL.Visible = True, txtGiFtURL.Text, "")
                organization.Matching_Gift_Account_Number = IIf(TrGiftAccNo.Visible = True, txtGiftAccNo.Text, "")
                organization.Matching_Gift_User_Id = IIf(TrGiftUserID.Visible = True, txtGiftUserId.Text, "")
                organization.Matching_Gift_Password = IIf(TrGiftPwd.Visible = True, txtGiftPwd.Text, "")

                organization.AddOrganization(Application("ConnectionString"))
                lblMessage.Text = "Saved Successfully"

                If Not Request.QueryString("Id") Is Nothing Then
                    Response.Redirect("dbsearchresults.aspx")
                ElseIf Session("NavPath") = "Sponsor" Then
                    Response.Redirect("Search_Sponsor.aspx")
                Else
                    Dim Org_MemId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MAX(AutoMemberID) from organizationinfo")
                    Response.Redirect("Vieworganizations.aspx?id=" & Org_MemId)
                End If
            End If
        Catch ex As Exception
            'Response.Redirect("CustomErrorPage.aspx")
            lblMessage.Text = "Not Saved. Error code : " & ex.ToString()
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Session("RoleID") = 1 Or Session("RoleID") = 92 Then
            TrGiftURL.Visible = True
            TrGiftAccNo.Visible = True
            TrGiftUserID.Visible = True
            TrGiftPwd.Visible = True
        Else
            TrGiftURL.Visible = False
            TrGiftAccNo.Visible = False
            TrGiftUserID.Visible = False
            TrGiftPwd.Visible = False
        End If
        If Not IsPostBack Then
            LoadStates()
            LoadNSFChapters()
            hlnkSearch.Visible = True
            If Session("NavPath") = "Sponsor" Then
                hlnkSearch.NavigateUrl = "Search_Sponsor.aspx"
            Else
                hlnkSearch.NavigateUrl = "dbsearchResults.aspx"
            End If
        End If
    End Sub
    Private Sub LoadStates()
        strSqlQuery = "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME"
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        While drStates.Read()
            ddlState.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlState.Items.Insert(0, New ListItem("Select State", "0"))
        ddlState.SelectedIndex = 0
    End Sub

    Private Sub LoadIndiaStates()
        strSqlQuery = "SELECT Distinct StateCode,StateName FROM StateMaster_India ORDER BY StateName"
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        While drStates.Read()
            ddlState.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlState.Items.Insert(0, New ListItem("Select State", "0"))
        ddlState.SelectedIndex = 0
    End Sub

    Private Sub LoadNSFChapters()
        strSqlQuery = "SELECT ChapterID,	ChapterCode,	Name,	State FROM Chapter WHERE Status='A' ORDER BY State,Name"
        Dim drChapters As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drChapters = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
        While drChapters.Read()
            ddlNSFChapter.Items.Add(New ListItem(drChapters(1).ToString(), drChapters(0).ToString()))
        End While
        ddlNSFChapter.Items.Insert(0, New ListItem("Select Chapter", "0"))
        ddlNSFChapter.SelectedIndex = 0
        If Len(Session("LoginChapterID")) > 0 Then
            ddlNSFChapter.SelectedValue = Session("LoginChapterID")
            ddlNSFChapter.Enabled = False
        End If
        If Session("RoleID") = "4" Or Session("RoleID") = "5" Then
            ddlNSFChapter.Items.Clear()
            Dim strSql As String
            strSql = "Select chapterid, chaptercode, state from chapter "
            If Session("RoleID") = "4" Then
                strSql = strSql & " where clusterid in (Select clusterid from "
                strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
            ElseIf Session("RoleID") = "5" Then
                strSql = strSql & " where chapterid = " + Session("LoginChapterID") & ""
            End If
            strSql = strSql & " order by state, chaptercode"
            Dim con As New SqlConnection(Application("ConnectionString"))
            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
            While (drNSFChapters.Read())
                ddlNSFChapter.Items.Add(New ListItem(drNSFChapters(1).ToString(), drNSFChapters(0).ToString()))
            End While
            If ddlNSFChapter.Items.Count > 1 Then
                ddlNSFChapter.Items.Insert(0, New ListItem("Select Chapter", "0"))
                ddlNSFChapter.Enabled = True
            ElseIf ddlNSFChapter.Items.Count = 1 Then
                ddlNSFChapter.Enabled = False
            End If
        End If
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        If ddlCountry.SelectedValue = "India" Then
            ddlState.Items.Clear()
            LoadIndiaStates()
            RangeValidator1.MaximumValue = 999999
            RangeValidator1.ErrorMessage = "Enter Numeric & Six Digits with no space only"
            ddlNSFChapter.SelectedIndex = ddlNSFChapter.Items.IndexOf(ddlNSFChapter.Items.FindByValue("1"))
            ddlNSFChapter.Enabled = False
        Else
            RangeValidator1.MaximumValue = 99999
            RangeValidator1.ErrorMessage = "Enter Numeric & Five Digits only"
            ddlState.Items.Clear()
            LoadStates()
            LoadNSFChapters()
            ddlNSFChapter.Enabled = True
        End If
    End Sub
End Class
