Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization

Namespace VRegistration

    ' <summary>
    ' Summary description for regnlregnet_Donate.
    ' </summary>
    Partial Class Donate
        Inherits System.Web.UI.Page

        '    Private us As CultureInfo = New CultureInfo("en-US")
        Protected lbBack As System.Web.UI.WebControls.LinkButton
        Protected RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
        Protected Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents CustomValidator1 As System.Web.UI.WebControls.CustomValidator

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            If Session("LoggedIn") <> "True" Then
                Server.Transfer("login.aspx?entry=" + Session("entryToken").ToString().Substring(0, 1))
            End If

            'Add back link
            Dim nsfMaster As NSFMasterPage = Me.Master
            nsfMaster.addBackMenuItem("termsAndConditions.aspx")
            'nsfMaster.addMenuItem("Back", "reg_contestants.aspx")

            If Not IsPostBack Then
                ddlChapterLiaison.SelectedIndex = 0
                ddlDonationFor.SelectedIndex = 0
                'ddlVolunteer.SelectedIndex = 0

                cbSupport1.Checked = False
                cbSupport2.Checked = False

                If (Not Session("CustIndID") Is Nothing) Then
                    Dim dsDonation As DataSet
                    Dim conn As New SqlConnection(Application("ConnectionString"))
                    dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select * from DonationPurpose")
                    ddlDonationFor.DataSource = dsDonation.Tables(0)
                    ddlDonationFor.DataValueField = "PurposeCode"
                    ddlDonationFor.DataTextField = "PurposeDesc"
                    ddlDonationFor.DataBind()
                    'ddlDonationFor.SelectedIndex = -1
                    ddlDonationFor.SelectedIndex = 3
                    'ddlDonationFor.SelectedItem.Text = "India scholarships"

                    '*** Silva 
                    If Session("EventID") = 13 Then
                        rngDonationAmount.EnableClientScript = False
                        rfvDonationAmount.EnableClientScript = False
                        rngDonationAmount.Enabled = False
                        rfvDonationAmount.Enabled = False
                        'Dim Donfee As Integer = CType(Session("Donation"), Integer)
                        'txtDonation.Text = Donfee.ToString("c", New CultureInfo("en-US"))
                        'txtDonation.Text.Replace("$", "")
                        ''** txtDonation.Text = CType(Session("Donation"), Integer)
                        '***************Modifieid on 23-09-2013 to make the default Donation Amount as 0 for Coaching****************'
                        'If Session("ProductType") = "SAT" Then
                        txtDonation.Text = "0.00"
                        'Else
                        'txtDonation.Text = "250.00"
                        'End If

                        '***********************************************************************************************************'

                        'txtDonation.Text = CType(Session("Donation"), Decimal)
                        'nRegFee = CType(Session("RegFee"), Integer)
                        'lblRegFee.Text = nRegFee.ToString("c", New CultureInfo("en-US"))
                        'nTotalAmt = nRegFee
                        'txtDonation.Text = Session("Donation").ToString("c")
                        ''**txtDonation.Enabled = False
                        txtDonation.Width = 75
                        ''**lbltxt.Visible = False

                        trv1.Visible = False
                        trv2.Visible = False
                        trv3.Visible = False
                    ElseIf Session("EventID") = 4 Then
                        txtDonation.Text = "0.00"
                    ElseIf Session("EventID") = 12 Then
                        ' rngDonationAmount.EnableClientScript = False
                        ' rfvDonationAmount.EnableClientScript = False
                        ' rngDonationAmount.Enabled = False
                        ' rfvDonationAmount.Enabled = False
                        'Dim Donfee As Integer = CType(Session("Donation"), Integer)
                        'txtDonation.Text = Donfee.ToString("c", New CultureInfo("en-US"))
                        'txtDonation.Text.Replace("$", "")
                        txtDonation.Text = Session("Donation")

                        'txtDonation.Text = CType(Session("Donation"), Decimal)
                        'nRegFee = CType(Session("RegFee"), Integer)
                        'lblRegFee.Text = nRegFee.ToString("c", New CultureInfo("en-US"))
                        'nTotalAmt = nRegFee
                        'txtDonation.Text = Session("Donation").ToString("c")
                        'txtDonation.Enabled = False
                        txtDonation.Width = 75
                        lbltxt.Visible = False

                        trv1.Visible = False
                        trv2.Visible = False
                        trv3.Visible = False
                    Else
                        Dim prmArray(6) As SqlParameter

                        prmArray(0) = New SqlParameter
                        prmArray(0).ParameterName = "@ParentID"
                        'old 'prmArray(0).Value = Convert.ToInt32(Session("ParentID").ToString)
                        prmArray(0).Value = Convert.ToInt32(Session("CustIndID").ToString)
                        prmArray(0).Direction = ParameterDirection.Input

                        Dim eventyear As Integer
                        Try
                            eventyear = Session("EventYear")
                        Catch ex As Exception
                            eventyear = Now.Year
                        End Try


                        prmArray(1) = New SqlParameter
                        prmArray(1).ParameterName = "@ContestYear"
                        prmArray(1).Value = eventyear
                        prmArray(1).Direction = ParameterDirection.Input

                        Dim ds As DataSet = Nothing
                        Try
                            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetContestCharity", prmArray)
                        Catch se As SqlException
                            lblMessage.Text = se.Message
                            Return
                        End Try
                        If ds Is Nothing Then
                            Return
                        End If
                        If (ds.Tables.Count <= 0) Then
                            Return
                        End If
                        If (ds.Tables(0).Rows.Count <= 0) Then
                            Return
                        End If
                        If (ds.Tables(0).Rows(0)("PaymentDate").ToString <> "") Then
                            Session("Donation") = CType(0, Decimal)
                            Session("DONATIONFOR") = ""
                            'Response.Redirect("reg_Pay.aspx")
                        Else
                            Dim nDonation As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("Donationamount").ToString)
                            txtDonation.Text = nDonation.ToString("0.00")
                            If (Not (ddlDonationFor.Items.FindByValue(ds.Tables(0).Rows(0)("DonationFor").ToString)) Is Nothing) Then
                                'ddlDonationFor.SelectedIndex = -1
                                'ddlDonationFor.Items.FindByValue(ds.Tables(0).Rows(0)("DonationFor").ToString).Selected = True
                            Else
                                'ddlDonationFor.Items.FindByValue("INScholar").Selected = True
                                txtDonation.Text = "250.00"
                            End If
                            'If (ds.Tables(0).Rows(0)("SupportFor1").ToString <> "False") Then
                            '    cbSupport1.Checked = True
                            'End If
                            'If (ds.Tables(0).Rows(0)("SupportFor2").ToString <> "False") Then
                            '    cbSupport2.Checked = True
                            'End If
                            If (Not (ddlChapterLiaison.Items.FindByValue(ds.Tables(0).Rows(0)("NSFINDIACHAPTERINFO").ToString)) Is Nothing) Then
                                ddlChapterLiaison.SelectedIndex = -1
                                ddlChapterLiaison.Items.FindByValue(ds.Tables(0).Rows(0)("NSFINDIACHAPTERINFO").ToString).Selected = True
                            End If
                            '*** Commented by venkat ambati because it is no longer needed from 2006 Registration
                            'If (Not (ddlVolunteer.Items.FindByValue(ds.Tables(0).Rows(0)("VOLUNTEERINTERESTAREA").ToString)) Is Nothing) Then
                            '    ddlVolunteer.SelectedIndex = -1
                            '    ddlVolunteer.Items.FindByValue(ds.Tables(0).Rows(0)("VOLUNTEERINTERESTAREA").ToString).Selected = True
                            'End If
                        End If
                        '****
                    End If
                Else
                    lblMessage.Text = (lblMessage.Text + "<BR>Error: The required session vars CustIndID  not available")
                    Return
                End If

            End If

        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()

        End Sub
        Dim IsConfirmed As Boolean = False
        Private Sub btnNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNo.Click
            Try
                IsConfirmed = False
                pnlConfirm.Visible = False
                txtDonation.Focus()
            Catch ex As Exception

            End Try
        End Sub
        Private Sub btnYes_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnYes.Click
            Try
                IsConfirmed = True
                lbContinue_Click(Nothing, Nothing)
                pnlConfirm.Visible = False
                Session("DoPay") = "regDonate"
                Response.Redirect("reg_Pay.aspx?eid=" + Session("EventID").ToString())
            Catch ex As Exception

            End Try
        End Sub

        Private Sub lbContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbContinue.Click
            'If (Panel1.Visible _
            '            AndAlso (ddlDonationFor.SelectedIndex = 0)) Then
            If String.IsNullOrEmpty(txtDonation.Text.Trim()) Then
                txtDonation.Text = 0
            End If

            If ((CInt(Me.txtDonation.Text) > 0) AndAlso (ddlDonationFor.SelectedIndex = 0)) Then
                DonationForLabel.Visible = True
                DonationForLabel.ForeColor = Color.Red
                DonationForLabel.Text = "Please select an option."
                'ElseIf String.IsNullOrEmpty(txtDonation.Text) Then
                '    DonationForLabel.Text = "Please enter donation amount."
            Else
                'get member's chapterid
                Dim chapterId As Integer = 0
                Dim currentDate As Date = Now
                Dim conn As New SqlConnection(Application("ConnectionString"))

                If ((Not Session("CustIndID") Is Nothing)) Then
                    Dim memberId As Integer = CInt(Session("CustIndID"))
                    Dim eventId As Integer = 0
                    If (Not Session("EventID") Is Nothing) Then
                        eventId = CInt(Session("EventID").ToString())
                    End If

                    Try
                        Dim ret As Object = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_getIndspouseChapterId", New SqlParameter("@autoMemberId", memberId))
                        If (Not ret Is Nothing) Then
                            chapterId = CType(ret, Integer)
                        End If
                        Session("CustIndChapterID") = chapterId
                    Catch se As SqlException
                        '? if there is no chapterid
                        lblMessage.Text = se.Message
                        lblMessage.Text = (lblMessage.Text + "<BR>Error:There is no Chapter associated with this loginid")
                        Return
                    End Try

                    'DonationForLabel.Visible = False
                    Dim strquery As String = "DELETE FROM Contest_Charity WHERE MEMBERID=<MEMBERID> and ContestYear=<CONTESTYEAR>;"
                    strquery = (strquery + " INSERT INTO Contest_Charity(MEMBERID, ContestYear, DonationAmount, DonationFor, SupportFor1, Support" & _
                    "For2, NSFIndiaChapterInfo, VolunteerInterestArea,EventId,ChapterId,createdDate)")
                    strquery = (strquery + " VALUES(<MEMBERID>, <CONTESTYEAR>, <DONATIONAMOUNT>, '<DONATIONFOR>', <SUPPORTFOR1>, <SUPPORTFOR2>, '" & _
                    "<NSFINDIACHAPTERINFO>', '<VOLUNTEERINTERESTAREA>',<EVENTID>,<CHAPTERID>,<CREATEDDATE>)")
                    Dim eventyear As Integer
                    Try
                        eventyear = Session("EventYear")
                    Catch ex As Exception
                        eventyear = Now.Year
                    End Try
                    strquery = strquery.Replace("<MEMBERID>", memberId.ToString)
                    strquery = strquery.Replace("<CONTESTYEAR>", eventyear.ToString)
                    strquery = strquery.Replace("<DONATIONAMOUNT>", txtDonation.Text.ToString)
                    Session("DONATIONFOR") = ""
                    If (ddlDonationFor.SelectedIndex >= 0) Then
                        strquery = strquery.Replace("<DONATIONFOR>", ddlDonationFor.SelectedItem.Value.ToString)
                        Session("DONATIONFOR") = ddlDonationFor.SelectedItem.Value.ToString
                    Else
                        strquery = strquery.Replace("<DONATIONFOR>", "")
                    End If
                    If cbSupport1.Checked Then
                        strquery = strquery.Replace("<SUPPORTFOR1>", "1")
                    Else
                        strquery = strquery.Replace("<SUPPORTFOR1>", "0")
                    End If
                    If cbSupport2.Checked Then
                        strquery = strquery.Replace("<SUPPORTFOR2>", "1")
                    Else
                        strquery = strquery.Replace("<SUPPORTFOR2>", "0")
                    End If
                    If (ddlChapterLiaison.SelectedIndex >= 0) Then
                        strquery = strquery.Replace("<NSFINDIACHAPTERINFO>", ddlChapterLiaison.SelectedItem.Value.ToString)
                    Else
                        strquery = strquery.Replace("<NSFINDIACHAPTERINFO>", "")
                    End If
                    strquery = strquery.Replace("<VOLUNTEERINTERESTAREA>", "")
                    strquery = strquery.Replace("<EVENTID>", eventId.ToString())
                    strquery = strquery.Replace("<CHAPTERID>", chapterId.ToString())
                    'strquery = strquery.Replace("<CREATEDDATE>", "'" + System.DateTime.Today.ToShortDateString + "'")
                    strquery = strquery.Replace("<CREATEDDATE>", "getdate()")
                    Try
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strquery)
                    Catch se As SqlException
                        'lblMessage.Text = strquery
                        lblMessage.Text = se.Message
                        lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                        Return
                    End Try

                    Try
                        Session("Donation") = Convert.ToDecimal(txtDonation.Text.ToString)
                    Catch ex As Exception
                        lblMessage.Text = ex.Message
                        Return
                    End Try
                    '  If Application("EventID") = "1" Then
                    'Response.Redirect("TermsAndConditions.aspx")
                    'Else
                    '*********** Added/Updated on 09-09-2014 To reduce the number of refunds for new Parents
                    If Not String.IsNullOrEmpty(txtDonation.Text) And Not IsConfirmed Then
                        Try
                            Dim DonationAmount As Decimal = Convert.ToDecimal(txtDonation.Text)
                            If ((Not Session("CustIndID") Is Nothing)) Then
                                'Dim memberId As Integer = CInt(Session("CustIndID"))
                                'Dim eventId As Integer = 0
                                If (Not Session("EventID") Is Nothing) Then
                                    eventId = CInt(Session("EventID").ToString())
                                End If
                                Try
                                    'Query updated on Nov 25,2014 to add validation of 12 months for each MemberID from NFG_Transactions table
                                    Dim TransactionCount As Object = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) from NFG_Transactions where memberid =" + memberId.ToString() & "  and DateDiff(mm,(Select top 1 [Payment Date] from NFG_Transactions where MemberID=" + memberId.ToString() & " order by [Payment Date] desc),GETDATE())<12 ")
                                    If (Not TransactionCount Is Nothing) Then
                                        If (CType(TransactionCount, Integer) = 0 And DonationAmount >= 250.0) Then
                                            'ask confirmation
                                            pnlConfirm.Visible = True
                                        Else
                                            Session("DoPay") = "regDonate"
                                            Response.Redirect("reg_Pay.aspx?eid=" + Session("EventID").ToString())
                                        End If
                                    Else
                                        Session("DoPay") = "regDonate"
                                        Response.Redirect("reg_Pay.aspx?eid=" + Session("EventID").ToString())
                                    End If
                                Catch se As SqlException
                                    '? if there is no chapterid
                                    lblMessage.Text = se.Message
                                    ''lblMessage.Text = (lblMessage.Text + "<BR>Invalid Amount")
                                    Return
                                End Try
                            End If
                        Catch ex As Exception
                        End Try
                    End If
                    '''''Response.Redirect("reg_Pay.aspx") 'Commented on 09-09-2014 To reduce the number of refunds for new Parents
                    '**************************************************************************************************************'
                    'End If
                Else
                    lblMessage.Text = (lblMessage.Text + "<BR>Error: The required session var CustIndID is not available")
                    Return
                End If
            End If
        End Sub

        Private Sub txtDonation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDonation.TextChanged
            lblMessage.ForeColor = Color.Blue
            lblMessage.Text = ""
            txtDonation.Text = txtDonation.Text.Trim
            Dim fDonation As Double = 0
            Try
                fDonation = Convert.ToDouble(txtDonation.Text)
                rfvDonationFor.Enabled = (fDonation > 0)
                Panel1.Visible = (fDonation > 0)
            Catch exception As System.Exception
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "Invalid number, please enter 0 or an amount of your choice"
            End Try
        End Sub

        Protected Sub cbSupport1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSupport1.CheckedChanged
            If cbSupport1.Checked = True Then
                cbSupport2.Checked = False
            End If

        End Sub
        Protected Sub cbSupport2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbSupport2.CheckedChanged
            If cbSupport2.Checked = True Then
                cbSupport1.Checked = False
            End If

        End Sub

    End Class

End Namespace

