<%@ Page Title="" Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ScanFinDoc.aspx.vb" Inherits="ScanFinDoc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
  



    <div>
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
       <tr bgcolor="#FFFFFF" >  
             <td align="left">
                    <asp:LinkButton ID="hlnkMainPage" OnClick="hlnkMainPage_Click" CausesValidation="false"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:LinkButton>
             </td>
       </tr>
         <tr bgcolor="#FFFFFF" >  
             <td align="left">
                    <asp:LinkButton ID="lbprevious" Visible="false"  CausesValidation="false"  CssClass="btn_02" runat="server">Back To FrontPage</asp:LinkButton>
             </td>
       </tr>
       <tr bgcolor="#FFFFFF" >
         
             <td align="center" class="Heading">Scanned Financial Documents</td>
             <td align="Right"></td> 
        </tr>
        <tr bgcolor="#FFFFFF" >
             <td align="center" class="txt01"><asp:Label ID="LblMessage" runat="server" ></asp:Label><br />
             </td>
       </tr>
       <tr bgcolor="#FFFFFF">
            <td align="center" class="txt01">
                <table cellspacing="0" cellpadding="2"  align="center" border="0" >
                     <tr><td align="left">Category  </td>
                         <td align="left">: 
                              <asp:DropDownList AutoPostBack="true" ID="ddlCategory" OnSelectedIndexChanged = "ddlCategory_SelectedIndexChanged" runat="server">
                                    <asp:ListItem Value="0">Select Category </asp:ListItem>
                                  <asp:ListItem Value="1">Cash Receipts - Donation Checks</asp:ListItem>
                                    <asp:ListItem Value="3">Cash Receipts - Credit Card Revenues</asp:ListItem>
                                 <asp:ListItem Value="2">Cash Receipts - Other Deposits</asp:ListItem>
                                    <asp:ListItem Value="4">Cash Receipts � Investment Income</asp:ListItem>
                                   <asp:ListItem Value="5">Vouchers - Expense checks</asp:ListItem>
                                  <asp:ListItem Value="6">Cash Receipts - ACH/EFT  Donations</asp:ListItem>
                                   <%--   <asp:ListItem Value="7">Vouchers - Bank Service Charges / Credit Card Fees</asp:ListItem>--%>
                                   <asp:ListItem Value="8">Vouchers - TransferOut</asp:ListItem>
                                   <asp:ListItem Value="9">Vouchers - Buy Transactions</asp:ListItem>


                                   <%--  <asp:ListItem Value="10">Vouchers- Sell Transaction</asp:ListItem>--%>
                               </asp:DropDownList>
           
                        </td>
                        <td></td>
                     </tr> 
                     <tr runat ="server" id="trbanks" visible = "false">
                            <td align="left">Account  </td><td align="left">:<asp:ListBox ID="lstBank" SelectionMode="Multiple" runat="server"></asp:ListBox>
                            </td> 
                            <td></td>
                      </tr> 
                      <tr><td align="left">From </td><td align="left"> : 
                            <asp:TextBox ID="TxtFrom" runat="server"></asp:TextBox></td> <td>
                                <asp:RequiredFieldValidator ControlToValidate="TxtFrom" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                      </tr>
                      <tr><td align="left">To </td><td align="left" > : 
                           <asp:TextBox ID="txtTo" runat="server"></asp:TextBox></td> <td>
                           <asp:RequiredFieldValidator ControlToValidate="txtTo" ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                           </td>
                      </tr>
                      <tr><td align="center" colspan="3">
                             <asp:Button ID="BtnSubmit" OnClick="BtnSubmit_Click" runat="server" Text="Submit" />
                           <table border ="1" style ="margin-top:20px;border:none;" cellpadding="0" cellspacing="0">
                             <%-- Newly placed Start --%>
                       <tr> 
                       <td>

                       <asp:Panel runat="server" ID="Pnlupload" Visible="false"> 
              


       <asp:FileUpload ID="FileUpLoad1" runat="server"   />
       
 
   <asp:Button ID="BtnSubmit1" OnClick="BtnSubmit1_Click" ClientIDMode="Static"  runat="server" Text="Upload" />

                    <asp:Label ID="Label1" runat="server" Text="Label1" Visible="false" ForeColor="Red" Font-Size="Larger"></asp:Label><br />
                       <asp:Label ID="Label2" runat="server" Text="Label1" Visible="False" ForeColor="Red" ></asp:Label><br />
          
                 <asp:Label ID="Label4" runat="server" Text="Label1" Visible="false" ForeColor="Red"></asp:Label><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                    ErrorMessage="  *Required File " ControlToValidate="FileUpLoad1">
                    </asp:RequiredFieldValidator>
                
              
                           <asp:TextBox ID="TextBox6" runat="server" Visible="false"></asp:TextBox>
                             <asp:Label ID="Label3" runat="server" Text="Label" Visible="false" ForeColor="Red"></asp:Label>
            
            </asp:Panel>


            </td>
            </tr>
                       
                        <%-- Newly placed End --%>
                      
                          
                          </table>
                         </td> 
                      </tr>
                       
                       
                    
             
             
             
                      <tr><td align="center" colspan="3">
                            <table id="tblDupRec" runat="server" visible="false">
                                <tr><td>
                                    <asp:Label ID="lblRecDup" runat="server" Text="The filename is NOT correct,�Do you want the system to correct the filename for you and upload?" ForeColor="Red"> </asp:Label>
                                </td></tr>
                                <tr><td align="center" colspan="3"><asp:Label ID="Label7" runat="server"  ForeColor="Red"> </asp:Label></td></tr>
                                <tr><td align="center" colspan="3">
                                        <asp:Button ID="BtnYes" runat="server" ForeColor="Red" Text="Yes" /> 
                                        <asp:Button ID="BtnNo" runat="server" ForeColor="Red" Text="No" />
                                </td></tr>
                                
                            </table>
                          </td>
                      </tr> 

      

                      <tr><td align="center" colspan="3">
                            <table id="Tbldup" runat="server" visible="false">
                                <tr><td>
                                    <asp:Label ID="Lbdup" runat="server" Text="This is a duplicate.  Do you want to replace the current file?" ForeColor="Red"> </asp:Label>
                                </td></tr>
                                <tr><td align="center" colspan="3">
                                        <asp:Button ID="yesdup" runat="server" ForeColor="Red" Text="Yes" /> 
                                        <asp:Button ID="nodup" runat="server" ForeColor="Red" Text="No" />
                                </td></tr>
                            </table>
                          </td>
                      </tr> 
                   
                      <tr><td align="center" colspan="2"><asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label></td></tr>
                </table>
            </td>
       </tr>
       
        <asp:TextBox ID="Txtstring" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBoxval" runat="server" Visible="false"></asp:TextBox>
        <asp:TextBox ID="TextBox1" runat="server"  Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox2" runat="server"  Visible="false"></asp:TextBox>
                <asp:TextBox ID="TextBox3" runat="server"  Visible="false"></asp:TextBox>
                    <asp:TextBox ID="TextBox4" runat="server"  Visible="false"></asp:TextBox>
                        <asp:TextBox ID="TextBox5" runat="server"  Visible="false"></asp:TextBox>
                          <asp:TextBox ID="TextBox7" runat="server"  Visible="false"></asp:TextBox>
            <asp:TextBox ID="TextBox8" runat="server"  Visible="false"></asp:TextBox>
          <asp:TextBox ID="TextBox10" runat="server"  Visible="false"></asp:TextBox>
         <asp:TextBox ID="TextDoccat" runat="server"  Visible="false"></asp:TextBox>
         <asp:TextBox ID="Textdocid" runat="server"  Visible="false"></asp:TextBox>
         <asp:TextBox ID="Textchoice" runat="server"  Visible="false"></asp:TextBox>
            
            <tr>
             <%--<asp:Panel runat="server" ID="Pnlupload" Visible="false"> 
              
                <asp:FileUpload ID="FileUpLoad1" runat="server"  />
                    <asp:Button ID="BtnSubmit1" OnClick="BtnSubmit1_Click" runat="server" Text="Submit" />
                    <%--<asp:FileUpload ID="FileUpLoad1" runat="server" Height="20px" Width="467px" />&nbsp;--%>
                   
                   
                  <%-- <asp:Button ID="UploadButton" runat="server" Text="Upload" />--%>
                 <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                    ErrorMessage="  *Required File " ControlToValidate="FileUpLoad1">
                    </asp:RequiredFieldValidator>--%>
                
                <td></td>
                <td></td>
                <td></td>
           
            
        </tr>
       <tr runat="server" id="TrDetailView" visible="false" bgcolor="#FFFFFF">
          <td  align="center">   
             <div align="center" ><span class="title04">Panel 2</span> </div>
                <br />
                
                <asp:DataGrid  ID="grdEditVoucher" runat="server"   AutoGenerateColumns="False" AllowSorting="True" DataKeyField="DonationID"
					 GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4"  BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666">
					<HeaderStyle Font-Bold="true" ForeColor="White"  />
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>
                                 <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                                 <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="DonationID" ReadOnly="true" DataField="DonationID"></asp:BoundColumn>
                                 <asp:BoundColumn  HeaderStyle-ForeColor="White" HeaderText="DepositSlip"  ReadOnly="true"  DataField="DepositSlip"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="Name"  ReadOnly="true"  DataField="DName"></asp:BoundColumn>
                                  <asp:TemplateColumn  HeaderText="Donationdate"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate>
                                         <asp:Label ID="lblDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donationdate")%>'></asp:Label>
                                      </ItemTemplate>
                                     <%-- <EditItemTemplate>
                                       <asp:TextBox ID="txtDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Depositdate")%>'></asp:TextBox>
                                      </EditItemTemplate>--%>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn  HeaderText="Depositdate"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate>
                                         <asp:Label ID="lblDepositdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositDate")%>'></asp:Label>
                                      </ItemTemplate>
                                     <%-- <EditItemTemplate>
                                       <asp:TextBox ID="txtDepositdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Depositdate")%>'></asp:TextBox>
                                      </EditItemTemplate>--%>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="DonorType"  ItemStyle-Width="10%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblDonorType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                      <asp:DropDownList ID="ddlDonorType" runat="server"  OnPreRender="SetDropDown_DonorType" AutoPostBack="false">
                                      <asp:ListItem>OWN</asp:ListItem>
                                       <asp:ListItem>IND</asp:ListItem>
                                        <asp:ListItem>SPOUSE</asp:ListItem>
	                                    </asp:DropDownList>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="true" />
                                 </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="BusType" >
                                      <ItemTemplate>
                                          <asp:Label ID="lblBusType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.BusType")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                  <asp:DropDownList runat="server" ID="ddlBusType" OnPreRender="SetDropDown_BusType" AutoPostBack="false">
					               <asp:ListItem Text="Select One" Value="0" Selected="True"></asp:ListItem>
					                <asp:ListItem Text="NRI,Business" Value="NRI,Business"></asp:ListItem>
					                <asp:ListItem Text="NRI, Ethnic" Value="NRI, Ethnic"></asp:ListItem>
					                <asp:ListItem Text="NRI, Professional" Value="NRI, Professional"></asp:ListItem>
					                <asp:ListItem Text="NRI, Other" Value="NRI, Other"></asp:ListItem>
					                <asp:ListItem Text="Press, India" Value="Press, India"></asp:ListItem>
					                <asp:ListItem Text="Press, NRI" Value="Press, NRI"></asp:ListItem>
					                <asp:ListItem Text="Press, US" Value="Press, US"></asp:ListItem>
					                <asp:ListItem Text="Religious, Temples" Value="Religious, Temples"></asp:ListItem>
					                <asp:ListItem Text="Religious, Other" Value="Religious, Other"></asp:ListItem>
					                <asp:ListItem Text="University_College_School" Value="University_College_School"></asp:ListItem>
					                <asp:ListItem Text="Corporation_LLC" Value="Corporation_LLC"></asp:ListItem>
					                <asp:ListItem Text="United Way" Value="United Way"></asp:ListItem>
					                <asp:ListItem Text="Other Non-Profit" Value="Other Non-Profit"></asp:ListItem>
					                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
					                <asp:ListItem></asp:ListItem>
					              </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="IRSCat"  ItemStyle-Width="14%" HeaderStyle-Width="14%">
                                      <ItemTemplate><asp:Label ID="lblIRSCat" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.IRSCat")%>'></asp:Label>
                                      </ItemTemplate>
                                     <EditItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlIRSCat"  OnPreRender="SetDropDown_IRSCat" AutoPostBack="false">
	                                <asp:ListItem Text="Profit" Value="Profit"></asp:ListItem>
			                        <asp:ListItem Text="Non-proft,501(c)(3)" Value="Non-proft,501(c)(3)"></asp:ListItem>
			                        <asp:ListItem Text="Non-proft,PAC" Value="Non-proft,PAC"></asp:ListItem>
			                        <asp:ListItem Text="Non-proft,Other" Value="Non-proft,Other"></asp:ListItem>
                                    <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList></EditItemTemplate>
                                    <HeaderStyle Width="14%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DonationType"  ItemStyle-Width="5%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblDonationType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonationType")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                         <asp:DropDownList ID="ddldonationtype" runat="server"  OnPreRender="SetDropDown_donationtype" AutoPostBack="false">
                        <asp:ListItem Value="1" Text="Unrestricted" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Perm Restricted"></asp:ListItem></asp:DropDownList> 
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ChapterCode"  ItemStyle-Width="5%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblChapterCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList  DataTextField="WebFolderName" DataValueField="ChapterID"   ID="ddlChapterCode"  runat="server" OnPreRender="SetDropDown_Chapter"  AutoPostBack="false">
					                 <asp:ListItem>SPOUSE</asp:ListItem>
					                      </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Amount","{0:C}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Amount","{0:F}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false"  HeaderText="Memberid" >
                                      <ItemTemplate>
                                         <asp:Label ID="lblMemberid" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>'></asp:Label>
                                      </ItemTemplate>                                     
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                </Columns>
  				</asp:DataGrid>
                &nbsp; &nbsp;&nbsp;
               <asp:Button ID="btnClose" Visible="false" OnClick="btnClose_Click"  runat="server" Text="Close Panel 2" />
               <br />
               <br />
            </td>
       </tr>
       <tr  runat="server" id="TrDonation" bgcolor="#FFFFFF" ><%--id="trEdit" --%>
          <td  align="center"> 
            <asp:GridView Width="1000px"  Visible="false" DataKeyNames="DepositSlip"  ID="gvDonation"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                <Columns>                
                    <asp:BoundField DataField ="DepositSlip"  HeaderText="DepositSlip #" />
                     <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField> 
                     <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                    <asp:BoundField DataField="EventYear" HeaderText="EventYear" />
                     <asp:BoundField DataField="Transcat" HeaderText="Transcat" />
                    <asp:BoundField DataField="DepositDate" HeaderText="DepositDate" DataFormatString="{0:d}" />
                    <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                    <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                    <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />--%>
                     <asp:BoundField DataField="BankID" ItemStyle-HorizontalAlign="Center" HeaderText="BankID" />
                    <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                    
                    <asp:BoundField  ItemStyle-Wrap="true"  DataField="DocName" HeaderText="DocName" />
                          <asp:BoundField  ItemStyle-Wrap="true"  DataField="banktransid" HeaderText="TransId" />
                       <asp:BoundField  ItemStyle-Wrap="true"  DataField="DoCID" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                    
                   
                </Columns>
                <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="1000px"  Visible="false" DataKeyNames="DepositSlip"  ID="GVTDA"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                <Columns>                
                    <asp:BoundField DataField ="DepositSlip"  HeaderText="TDA A/C" />
                    <asp:BoundField DataField="StartDate" HeaderText="StartDate" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="EndDate" HeaderText="EndDate" DataFormatString="{0:d}" />
                    <asp:HyperLinkField DataNavigateUrlFields="StartDate,EndDate" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DType=CC&SDt={0:MM/dd/yyyy}&EDt={1:MM/dd/yyyy}"  HeaderText="Voucher" Text="View/Print" />                               
                    <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                    <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>
                    <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                    <asp:BoundField DataField="DepositDate" HeaderText="DepositDate" DataFormatString="{0:d}" />
                    <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                </Columns>
                <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" DataKeyNames="CheckNumber"  ID="gvExp"  runat="server" AllowPaging="false" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns> 
                         <asp:BoundField DataField ="Eventyear"  HeaderText="Eventyear" />
                         <asp:BoundField DataField ="transcat"  HeaderText="Transcat" />          
                        <asp:BoundField DataField ="CheckNumber"  HeaderText="Check#" />
                       <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField>                             
                        <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                        <asp:BoundField DataField="BankTransID" HeaderText="BankTransID" />
                        <asp:BoundField DataField="DatePaid" HeaderText="DateIssued" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="DateCashed" HeaderText="DateCashed" DataFormatString="{0:d}" /><%--Added on 29-04-2013 to display all checks--%>
                        <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                        <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                        <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />--%>
                        <asp:BoundField DataField="BankID" HeaderText="BankID" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                 <asp:BoundField  HeaderText="DocName" DataField="DocName" ItemStyle-HorizontalAlign="Center" />
                         <asp:BoundField  ItemStyle-Wrap="true"  DataField="CheckNumber" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                       <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false"   ID="GVCCRevenues"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        
                        <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField>                             
                        <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                         <%--<asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="banktransid" HeaderText="CheckID" />
                         <asp:BoundField DataField="transcat" HeaderText="Transcat" />
                           <asp:BoundField DataField="EventYear" HeaderText="Event Year" />
                           <asp:BoundField DataField="bankid" HeaderText="Bankid" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate" HeaderText="End_Date" DataFormatString="{0:d}" />
                        <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                        <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                        <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />--%>
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                       <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                     <asp:BoundField DataField="DocName" HeaderText="DocName" />
                                <asp:BoundField  ItemStyle-Wrap="true"  DataField="BankTransID" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" ID="GVACT_EFTDon"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>   
                     <asp:BoundField DataField ="Eventyear"  HeaderText="Eventyear" />             
                       <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField> 
                     <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                        <asp:BoundField DataField="BankID" HeaderText="BankID"  Visible="True"/>
                        <asp:BoundField DataField="BankTransID" HeaderText="CheckID" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate"  Visible="True" DataFormatString="{0:d}"/>
                        <asp:BoundField DataField="TransCat" HeaderText="TransCat" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate"  HeaderText="End_Date" DataFormatString="{0:d}" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransType" HeaderText="Trans Type"  Visible="False" />
                        <asp:BoundField DataField="VendCust" HeaderText="VendCust"  Visible="False" />
                        <asp:BoundField DataField="Reason" HeaderText="Reason"  Visible="False" />
                        <asp:BoundField DataField="Description" HeaderText="Description"  Visible="False" />
                        <asp:BoundField DataField="Addinfo" HeaderText="Addinfo"  Visible="False" />
                        <asp:BoundField DataField="DonorType" HeaderText="DonorType"  Visible="False" />
                        <asp:BoundField DataField="BankTransID" HeaderText="BankTransID"  Visible="True" />
                          <asp:BoundField DataField="Docname" HeaderText="Doc Name"  Visible="True" />
                                <asp:BoundField  ItemStyle-Wrap="true"  DataField="BankTransID" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                      <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" ID="GVBnk_CCFees"  runat="server" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                         <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField> 
                     <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                          <asp:BoundField DataField="BankID" ItemStyle-HorizontalAlign="Center" HeaderText="BankID"/>
                            <asp:BoundField DataField="Eventyear" HeaderText="Event Year" Visible="True" />
                          <asp:BoundField DataField="TransType" HeaderText="TransType" Visible="False"/>
                          <%--<asp:BoundField DataField="BankTransID" HeaderText="BankTransID" Visible="False"/>--%>
                          <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate" DataFormatString="{0:d}" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                       <asp:BoundField DataField="AccountName" HeaderText="AccountName" />
                  
                      <asp:BoundField DataField="Transcat" HeaderText="Transcat" />
                      <asp:BoundField DataField="banktransid" HeaderText="Banktransid" />
                      <asp:BoundField DataField="AddInfo" HeaderText="AddInfo"/>
                          <asp:BoundField DataField="docname" HeaderText="Doc Name"/>
                   
                      <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>  
            <asp:GridView Width="800px"  Visible="false" ID="GVTransfers"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField> 
                     <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                       <%-- <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="BankID" HeaderText="BankID"  Visible="True"/>
                        <asp:BoundField DataField="BrokTransID" HeaderText="BrokTransID"  />
                        <asp:BoundField DataField="BankCode" HeaderText="BankCode"  Visible="True"/>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate"  Visible="True" DataFormatString="{0:d}"/>
                        <asp:BoundField DataField="EventYear" HeaderText="Event Year" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate"  HeaderText="End_Date" DataFormatString="{0:d}"  />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                      
                        <asp:BoundField DataField="transcat" HeaderText="Transcat"  />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity"/>
                       
                        <asp:BoundField DataField="ToBankID" HeaderText="ToBankID" Visible="True" />
                        <asp:BoundField DataField="docname" HeaderText="DocName" />
                                 <asp:BoundField  ItemStyle-Wrap="true"  DataField="BrokTransID" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                      <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" ID="GVBuyTrans"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField>                             
                        <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                       <%-- <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="BankID" HeaderText="BankID"  Visible="True"/>
                        <asp:BoundField DataField="BankCode" HeaderText="BankCode"  Visible="True"/>
                         <asp:BoundField DataField="transcat" HeaderText="Transcat"  Visible="True"/>
                         <asp:BoundField DataField="BrokTransID" HeaderText="BankTransID"  Visible="True"/>
                         <asp:BoundField DataField="EventYear" HeaderText="Event Year"  Visible="True"/>
                        <asp:BoundField DataField="AccNo" HeaderText="CheckID" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate"  Visible="True" DataFormatString="{0:d}"/>
                        <asp:BoundField DataField="TransCat" HeaderText="TransCat"  Visible="False"/>
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate"  HeaderText="End_Date" DataFormatString="{0:d}"  />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransType" HeaderText="Trans Type"  Visible="False" />
                        <asp:BoundField DataField="AssetClass" HeaderText="AssetClass"  />
                       <%-- <asp:BoundField DataField="Ticker" HeaderText="Ticker"  />--%>
                   
                        <asp:BoundField DataField="docname" HeaderText="DocName" />
                                   <asp:BoundField  ItemStyle-Wrap="true"  DataField="BrokTransID" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                        </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false"   ID="GVTDAIncome"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        
                        <asp:ButtonField CommandName="Upload"  Text="Upload" headerText="Upload" ></asp:ButtonField>                             
                        <asp:ButtonField CommandName="DownLoad"  Text="DownLoad" headerText="DownLoad" ></asp:ButtonField>
                         <%--<asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Cash Receipt#" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate" HeaderText="End_Date" DataFormatString="{0:d}" />
                         <asp:BoundField DataField="BankCode" HeaderText="BankCode"  />
                       <asp:BoundField DataField="transcat" HeaderText="Transcat" />
                        <asp:BoundField DataField="Eventyear" HeaderText="Event Year" />
                        <asp:BoundField DataField="BrokTransID" HeaderText="BankTransID" />

                        <asp:BoundField DataField="BankID" ItemStyle-HorizontalAlign="Center" HeaderText="BankID"   />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                         <asp:BoundField DataField="transdate" HeaderText="Transdate" />
                          <asp:BoundField DataField="docname" HeaderText="Doc Name" />
                       <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                       <asp:BoundField  ItemStyle-Wrap="true"  DataField="BrokTransID" HeaderText="DoCID" />
                   
           <asp:ButtonField CommandName="DOCID"  Text="DOCID" headerText="DOCIDLink" ></asp:ButtonField>
                         </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            &nbsp; &nbsp;&nbsp;
            <br /><br />
            <asp:GridView Width="800px"  Visible="false"   ID="GVGLTemp"  OnPageIndexChanging  ="GVGLTemp_PageIndexChanging"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:BoundField DataField="VoucherNo" HeaderText="VoucherNo" />
                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString ="{0:d}" />
                        <asp:BoundField DataField="TransType" HeaderText="TransType" />
                        <asp:BoundField DataField="BankDate" HeaderText="BankDate" DataFormatString ="{0:d}" />
                        <asp:BoundField DataField="Account" HeaderText="Account" />
                         <asp:BoundField DataField="Description" HeaderText="Description" />
                        <asp:BoundField DataField="Class" HeaderText="Class" />
                        <asp:BoundField DataField="Ticker" HeaderText="Ticker" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="BankName" HeaderText="BankName" />
                        <asp:BoundField DataField="CheckNumber" HeaderText="CheckNumber" />
                        <asp:BoundField DataField="PayeeName" HeaderText="Payee Name" />
                         <asp:BoundField DataField="StartDate" HeaderText="StartDate" DataFormatString ="{0:d}" />
                         <asp:BoundField DataField="Enddate" HeaderText="Enddate" DataFormatString ="{0:d}" />
                       </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               </asp:GridView>
             <asp:GridView runat="server" ID="Gridpopup" Visible="true"></asp:GridView>

         </td>
       </tr>   
      
    </table>
   </div>

    <div>
        
          
    
        <table>
        
          <tr>
              <td>
            <table>
                <tr> <td>  <asp:Label ID="Label6" runat="server" Text="The latest 25 records from the ScanFinDoc  table" Visible="false" class="Heading"></asp:Label> </td></tr>
                <tr>
                    <td>
                       <asp:GridView runat="server" ID="gridsanfindoc" Visible="true"></asp:GridView></td>
          
                </tr>   

            </table>
</td>
        </tr>
        
         <tr>
        <td>
            <table>    <tr> <td>  <asp:Label ID="Label5" runat="server" Text="The list of files in the /scanfindoc folder" Visible="false" class="Heading"></asp:Label> </td></tr>

                <tr> <td><asp:ListBox ID="ListBox1" runat="server" Visible="false"></asp:ListBox> </td></tr>

            </table>

        
         </td>
           </tr> 
      
       
                   <tr>
        <td>
          
        
         </td>
           </tr> 
             
      
      </table></div>
</asp:Content>