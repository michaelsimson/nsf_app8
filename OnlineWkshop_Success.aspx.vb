Imports System
Imports System.Web
Imports LinkPointTransaction
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports NorthSouth.BAL
Partial Class OnlineWkshop_Success
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection
    Public AccessToken As String = "ZR8zO6GhdSoGiKIXXgd5Ic3z4xQA"
    Public Organizerkey As String = "5516732880916201477"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            'Session("LoggedIn") = "true"
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
        End If

        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet
        'Session("LoginEmail") = "sai.tumuluru@gmail.com"
        'Session("CustIndID") = 37590
        'Session("EventID") = 20


        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"
        hdnMemberId.Value = Session("CustIndID")
        hdnEventyear.Value = DateTime.Now.Year
        hdnAccessToken.Value = AccessToken
        hdnOrganizerKey.Value = Organizerkey
        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") &
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                lblAddress2.Text = drIndSpouse.Item("Address2")
                If lblAddress2.Text.Trim().Length = 0 Then
                    trAddress2.Visible = False
                End If
                lblStateZip.Text = drIndSpouse.Item("City") & ", " & drIndSpouse.Item("state") & " " & drIndSpouse.Item("zip")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If

            End If
        End If
        Dim dsChild As New DataSet

        Dim dsInvitees As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        If conn.State = ConnectionState.Closed Then conn.Open()

        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_Get_Paid_OnlineWSRegistrationByReference" ' "usp_Get_Paid_TransRegistrationByReference"
        cmd.Connection = conn

        cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
        cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
        'Used for testing by Sims
        '' Session("PaymentReference") = "8d8y803a-5001490783"
        If Session("PaymentReference") = Nothing Then
            cmd.Parameters.Add(New SqlParameter("@paymentreference", ""))
        Else
            cmd.Parameters.Add(New SqlParameter("@paymentreference", Session("PaymentReference")))
        End If

        da.SelectCommand = cmd
        da.Fill(dsInvitees)
        dgChildList.DataSource = dsInvitees.Tables(0)
        dgChildList.DataBind()

        Try
            Dim i As Integer = 0
            Dim onlinWsCalId As String = String.Empty
            For i = 0 To dsInvitees.Tables(0).Rows.Count - 1
                onlinWsCalId += dsInvitees.Tables(0).Rows(i)("OnlineWSCalId").ToString() + ","

            Next
            onlinWsCalId = onlinWsCalId.TrimEnd(",")
            hdnOnlineWsCalId.Value = onlinWsCalId
        Catch ex As Exception

        End Try

        Try

            'for testing purpose
            'Session("PaymentReference") = "TempRef01"
            lblReference.Text = Session("PaymentReference")
            lblPaymentDate.Text = Date.Parse(Today().ToString()).ToString("MM/dd/yyyy")  'Today().ToString("{}")
            lblRegFee.Text = Session("RegFee")
            lblLateFee.Text = Session("LateFee")

            lblDonation.Text = Session("Donation")
            '        lblMeals.Text = Session("Mealcharges")
            lblDonationTax.Text = lblDonation.Text
            lblRegFeeTax.Text = CType(Session("RegFee"), Decimal) * (2 / 3).ToString()
            lblLateFeeTax.Text = CType(Session("LateFee"), Decimal) * (2 / 3).ToString()

            lblTotAmt.Text = (Double.Parse(Session("RegFee")) + Double.Parse(Session("Donation")) + Double.Parse(Session("LateFee"))).ToString()

            lblTotAmtTax.Text = Double.Parse(lblRegFeeTax.Text) + Double.Parse(lblDonationTax.Text) + Double.Parse(lblLateFeeTax.Text)

            lblRegFee.Text = Double.Parse(lblRegFee.Text).ToString("c", New CultureInfo("en-US"))
            lblLateFee.Text = Double.Parse(lblLateFee.Text).ToString("c", New CultureInfo("en-US"))

            If lblDonation.Text <> "" And lblDonation.Text <> Nothing Then
                lblDonation.Text = Double.Parse(lblDonation.Text).ToString("c", New CultureInfo("en-US"))
            End If

            If lblDonationTax.Text <> "" And lblDonationTax.Text <> Nothing Then
                lblDonationTax.Text = Double.Parse(lblDonationTax.Text).ToString("c", New CultureInfo("en-US"))
            End If


            If lblRegFeeTax.Text <> "" And lblRegFeeTax.Text <> Nothing Then
                lblRegFeeTax.Text = Double.Parse(lblRegFeeTax.Text).ToString("c", New CultureInfo("en-US"))
            End If

            If lblLateFeeTax.Text <> "" And lblLateFeeTax.Text <> Nothing Then
                lblLateFeeTax.Text = Double.Parse(lblLateFeeTax.Text).ToString("c", New CultureInfo("en-US"))
            End If

            If lblTotAmt.Text <> "" And lblTotAmt.Text <> Nothing Then
                lblTotAmt.Text = Double.Parse(lblTotAmt.Text).ToString("c", New CultureInfo("en-US"))
            End If

            If lblTotAmtTax.Text <> "" And lblTotAmtTax.Text <> Nothing Then
                lblTotAmtTax.Text = Double.Parse(lblTotAmtTax.Text).ToString("c", New CultureInfo("en-US"))
            End If

            If Session("Donation") > 0 Then
                lblNoteMsg.Text = "Thank you for your generous donation. The donation plus the tax-deductible portion of the registration fees is used to provide college scholarships to poor students in India. Please obtain a matching gift form from your (or your spouse�s) employer, fill out the top portion (" & lblTotAmtTax.Text & ") and mail it to the North South Foundation at 2 Marissa Ct, Burr Ridge, IL 60527. By doing this, you double your contribution to the Foundation."
                lblNoteMsg.Visible = True
            Else
                lblNoteMsg.Text = "Thank you for your online registration. The tax-deductible portion is used to provide college scholarships to poor students in India. Please obtain a matching gift form from your (or your spouse�s) employer, fill out the top portion (" & lblTotAmtTax.Text & ") and mail it to the North South Foundation at 2 Marissa Ct, Burr Ridge, IL 60527. By doing this, you double your contribution to the Foundation."
                lblNoteMsg.Visible = True
            End If

            DisplayContests()

        Catch ex As Exception

        End Try
    End Sub


    Private Sub DisplayContests()
        Dim sb As New StringBuilder
        Dim sbOnlineWSs As New StringBuilder
        Dim re As StreamReader
        Dim emailBody As String = ""
        Dim screenConfirmText As String = ""
        Dim rowcount As Int32 = 0
        Dim strDonationMessage As String = ""
        Dim connContest As New SqlConnection(Application("ConnectionString"))
        Dim nDonationAmt As Double
        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}
        Dim nTaxDeductibleAmount As Double
        Dim prmArray(4) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@EventYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input
        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@paymentreference"
        prmArray(2).Value = Session("PaymentReference")
        prmArray(2).Direction = ParameterDirection.Input

        prmArray(3) = New SqlParameter
        prmArray(3).ParameterName = "@EventID"
        prmArray(3).Value = Session("EventID")
        prmArray(3).Direction = ParameterDirection.Input

        'sandhya - 6//8/2007
        'added this parameter to correct the sql for finals


        'If Session("EventID") = "1" Then   'Regional Contact Information
        '    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArray)
        'ElseIf Session("EventID") = "2" Then   'Regional Contact Information
        '    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetRegionalContests", dsContestant, tblConestant, prmArray)
        'End If
        SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetOnlineWSRegistrationPaymentInfo", dsContestant, tblConestant, prmArray)
        'If dsContestant.Tables.Count > 0 Then
        '    dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
        '    dgSelectedContests.DataBind()
        'End If

        sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=0>")
        sb.Append("<tr bgcolor=lightblue forecolor=white>")
        sb.Append("<td width=20%><b>Contestant Name</b></td>")
        sb.Append("<td width=20%>Contest Desc</td>")
        sb.Append("<td width=20%>Contest Location / Contact info</td>")
        sb.Append("<td width=20%>Contest Date Time</td>")
        sb.Append("<td width=20%>Payment Info</td>")
        sb.Append("</tr>")
        If dsContestant.Tables.Count > 0 Then
            If dsContestant.Tables(0).Rows.Count > 0 Then
                For rowcount = 0 To dsContestant.Tables(0).Rows.Count - 1
                    sb.Append("<tr>")
                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestantName").ToString() + "</td>")

                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("EventDesc").ToString() + "</td>")
                    If Application("EventID") = "1" Then
                        sb.Append("<td width=20%>")
                        sb.Append(Application("NationalFinalsCity") + "<BR>")
                    Else
                        sb.Append("<td width=20%>")
                        'sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("Building").ToString() + ", ")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChapterCity").ToString() + ", ")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChapterState").ToString() + "<BR>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("CoordinatorName").ToString() + "</td>")

                    End If

                    sb.Append("<td width=20%>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("EventDate").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("Time").ToString() + "</td>")

                    sb.Append("<td width=20%>")
                    sb.Append(FormatCurrency(dsContestant.Tables(0).Rows(rowcount).Item("Fee")).ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentDate").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentReference").ToString() + "<BR>")
                    sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentMode").ToString() + "</td></tr>")
                Next
            End If
        End If
        sb.Append("</table>")

        re = File.OpenText(Server.MapPath("2008_Regional_ConfirmingEmail.htm"))

        'sbOnlineWSs.Append(sb.ToString)

        emailBody = re.ReadToEnd
        re.Close()
        Dim nTaxDeductibleRegFee = CType(Session("RegFee"), Decimal) * (2 / 3)
        Dim nregfee As Decimal = Session("RegFee")
        Dim nLatefee As Decimal = Session("LateFee")

        If (Not (Session("Donation")) Is Nothing) Then
            nDonationAmt = CType(Session("Donation"), Decimal)
        End If
        '    Session("PaymentReference") = R_OrderNum

        If (nDonationAmt > 0) Then
            'strDonationMessage = "Thank you also for your generous donation of " & FormatCurrency(nDonationAmt) & ".<BR> This will help NSF�s goal of providing scholarships ($250 each) to 500 poor but meritorious students in India for the year 2006-2007."
            strDonationMessage = "We also thank you for your generous donation of " & FormatCurrency(nDonationAmt) & ".This will help in reaching our goal of providing 500 scholarships ($250 each) to those who excel among the poor go to college in India for the upcoming academic year."
        End If
        'Your tax-deductible contribution is:  ( donation amount + 2/3 * AMNT)
        nTaxDeductibleAmount = (nDonationAmt + (nTaxDeductibleRegFee))

        'emailBody = emailBody.Replace("[PAYMENTREFERENCE]", R_OrderNum)
        emailBody = emailBody.Replace("[PAYMENTREFERENCE]", Session("PaymentReference"))
        emailBody = emailBody.Replace("[DONATIONAMOUNT]", FormatCurrency(Double.Parse(Session("Donation"))))
        emailBody = emailBody.Replace("[DATAGRID]", sb.ToString)
        emailBody = emailBody.Replace("[DONATIONTEXT]", strDonationMessage)
        emailBody = emailBody.Replace("[EVENTYEAR]", Application("ContestYear"))
        emailBody = emailBody.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(Double.Parse(Session("RegFee"))))
        emailBody = emailBody.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
        ' SendDasMessage("Donation to North South Foundation", emailBody, CType(Session("LoginEmail"), String))
        'Response.Write(sbContests.ToString())

        '*************************************************

        'If SendEmail("Confirmation received for PrepClub registrations", emailBody.ToString, CType(Session("LoginEmail"), String)) Then
        '    'If SendEmail(subMail, emailBody.ToString, CType("chitturi9@gmail.com", String)) Then
        '    lblEmailStatus.Text = "A receipt was sent to you by email for your records."
        'Else
        '    lblEmailStatus.Text = "There was an error sending email. Please print/save details of this page for your records."
        'End If



        'connContest = Nothing
    End Sub
    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
        'Build Email Message
        Dim email As New MailMessage
        email.From = New MailAddress(New GlobalVariable().EmailAddress("workshop")) '""nsfcontests@northsouth.org")
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host

        Dim ok As Boolean = True
        Try
            client.Send(email)
        Catch e As Exception
            'lblEmailStatus.Text = e.Message.ToString
            ok = False
        End Try
        Return ok
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)

        Dim sb As New StringBuilder()
        Dim sw As New StringWriter(sb)
        Dim hWriter As New HtmlTextWriter(sw)
        Dim pos, pos1 As Integer
        Dim emailBody As String
        MyBase.Render(hWriter)
        'base.Render(hWriter);
        Dim PageResult As String
        PageResult = sb.ToString()

        writer.Write(PageResult)
        '// *** store to a string

        'string PageResult = sb.ToString();
        pos = PageResult.IndexOf("tabId0")

        emailBody = PageResult.Substring(pos)
        '<html>
        '<body>
        '<table width="100%">

        emailBody = emailBody.Replace("tabId0'>", "")
        emailBody = "<html> <body><table width='100%'>" + emailBody
        emailBody = emailBody.Replace("Back to Parent Functions Page", "")

        '// *** Write it back to the server

        'writer.Write(PageResult);
        'Commented on 11-20-2014 for testing in App8
        If SendEmail("Confirmation received for Online Workshop registrations", emailBody.ToString, CType(Session("LoginEmail"), String)) Then ')
            'If SendEmail(subMail, emailBody.ToString, CType("chitturi9@gmail.com", String)) Then
            lblEmailStatus.Text = "A receipt was sent to you by email for your records."
        Else
            lblEmailStatus.Text = "There was an error sending email. Please print/save details of this page for your records."
        End If
    End Sub

End Class
