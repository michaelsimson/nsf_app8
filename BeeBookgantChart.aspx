﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NSFMasterPage.master" CodeFile="BeeBookgantChart.aspx.vb" Inherits="BeeBook_BeeBookgantChart" %>

<%@ Register Assembly="ScheduleControl" Namespace="EventCalendar" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="../Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript">
        //$(function (e) {
        //    var tablehtml = ;
        //    for (var i = 0; i < 4; i++) {
        //        tablehtml += "<tr>";
        //        tablehtml += "<td style='border:1px solid black;'>Recruiting</td>";
        //        for (var j = 0; j < 10; j++) {
        //            tablehtml += "<td  style='border:1px solid black;' colspn='4'>Software</td>";
        //        }
        //        tablehtml += "</tr>";
        //    }
        //    $("#tblTestChart").html(tablehtml);
        //});

    </script>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; margin-bottom: 20px; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        <strong>Bee Book Schedule

        </strong>
        <br />

    </div>
    <div id="IDContent" style="display: none;">
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
        <table style="margin-left: 0px;">

            <tr>
                <td style="width: 150px"></td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddYear" runat="server" Width="150px"
                        AutoPostBack="True">
                        <asp:ListItem Value="0">Select year</asp:ListItem>
                        <asp:ListItem Value="-1">All</asp:ListItem>
                        <asp:ListItem Value="2014">2014</asp:ListItem>
                        <asp:ListItem Value="2015">2015</asp:ListItem>
                    </asp:DropDownList>

                </td>

                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                <td style="width: 141px" align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True">
                        <asp:ListItem Value="0">Select Event</asp:ListItem>
                        <asp:ListItem Value="19">Prepclub</asp:ListItem>
                        <asp:ListItem Value="21">[National]</asp:ListItem>
                        <asp:ListItem Value="13">Coaching</asp:ListItem>
                        <asp:ListItem Value="3">Workshop</asp:ListItem>
                        <asp:ListItem Value="2">ChapterContests</asp:ListItem>
                        <asp:ListItem Value="1">Finals</asp:ListItem>
                        <asp:ListItem Value="-1">All</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                <td style="width: 141px" runat="server" align="left" id="ddchapterdrop">
                    <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" DataValueField="ChapterID" runat="server" Width="150px" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                </td>
                <td style="width: 141px" align="left">
                    <asp:DropDownList ID="ddTeams" DataTextField="TeamName" DataValueField="TeamID" runat="server" Width="155px" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                </td>

                <td>

                    <asp:Button ID="btnExcel" runat="server" Text="Submit Chart" />
                </td>




            </tr>

        </table>


        <table>
            <tr>
                <td></td>
                <td style="width: 50px"></td>

                <td>


                    <table>
                        <tr>
                            <td align="left" nowrap="nowrap">&nbsp;</td>
                            <td style="width: 141px" align="left">&nbsp;</td>
                        </tr>
                        <%-- <tr>
        <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product Group</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddproductgroup" runat="server" Width="150px" 
                    AutoPostBack="True" OnSelectedIndexChanged="ddproductgroup_SelectedIndexChanged" 
                    >
                    
                </asp:DropDownList>
            </td>
</tr>--%>
                        <%--     <tr>
         <td align="left" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product</b> </td>
            <td style="width: 141px" align="left">
                <asp:DropDownList ID="ddproduct" runat="server" Width="150px" 
                    OnSelectedIndexChanged="ddproduct_SelectedIndexChanged" AutoPostBack="True" 
                    >
                    
                </asp:DropDownList>
            </td></tr>--%>
                    </table>

                </td>


            </tr>
        </table>

        <%--  </div>--%>
        <div align="center" id="Div5" visible="false" style="font-size: 26px; font-weight: bold; font-family: Calibri; margin-bottom: 20px; color: rgb(73, 177, 23);"
            runat="server">
            <strong>Bee Book Schedule Components
            </strong>
            <br />

        </div>
        <table width="90%" style="margin-left: 40px; margin-right: auto; font-weight: bold; background-color: #ffffcc;" id="tblAddUpdateComponents" runat="server" visible="false">
            <tr class="ContentSubTitle" align="center">
                <td style="text-align: right;">Component</td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtComponent" runat="server"></asp:TextBox>
                </td>
                <td style="text-align: center" colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnAddUpdate" runat="server" Text="Add/Update" Style="text-align: center" />
                    &nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                </td>

            </tr>



        </table>
        <br />
        <div id="dvAddComponents" visible="false" align="left" runat="server" style="margin-bottom: 10px;">
            <asp:Button ID="btnAddComponents" runat="server" Text="AddComponents"
                Enabled="true" />

        </div>
        <asp:GridView ID="grdBeeBookSchedule" runat="server" AutoGenerateColumns="False" EnableViewState="true" Width="100%" HeaderStyle-BackColor="#ffffcc">
            <Columns>
                <asp:TemplateField HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                        <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                        <div style="display: none">
                            <asp:Label ID="lblComponentID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ComponentID")%>'></asp:Label>

                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Event" Visible="True" HeaderText="Event" HeaderStyle-Width="100px"></asp:BoundField>
                <asp:BoundField DataField="Chapter" Visible="true" HeaderText="Chapter"></asp:BoundField>

                <asp:BoundField DataField="Components" Visible="true" HeaderText="Components"></asp:BoundField>


            </Columns>
        </asp:GridView>
        <div align="center" id="Div6" style="font-size: 26px; display: none; font-weight: bold; margin-top: 20px; margin-bottom: 20px; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            <strong>Bee Book Schedule Sub Components
            </strong>


        </div>
        <br />
        <table width="90%" style="margin-left: 40px; margin-right: auto; display: none; font-weight: bold; background-color: #ffffcc;" id="tblAddUpdateSubComponents" runat="server" visible="false">
            <tr class="ContentSubTitle" align="center">
                <td style="text-align: right;">Component</td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlComponents" DataValueField= DataTextField= runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
                <td style="text-align: right;">Sub Component</td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtSubComponents" runat="server"></asp:TextBox>
                </td>
                <td style="text-align: right;">Duration</td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtDuration" runat="server"></asp:TextBox>
                </td>
                <td style="text-align: right;">EndDate</td>
                <td style="text-align: left;">
                    <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                </td>

                <td style="text-align: center" colspan="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnAddUpdateSubComponents" runat="server" Text="Add/Update" Style="text-align: center" />
                    &nbsp;&nbsp;
                                <asp:Button ID="btnCancelSubComponents" runat="server" Text="Cancel" />
                </td>

            </tr>



        </table>
        <br />
        <div id="dvAddSubComponents" align="left" runat="server" style="margin-bottom: 10px; display: none;">
            <asp:Button ID="btnAddSubComponents" runat="server" Text="AddSubComponents"
                Enabled="true" />

        </div>
        <br />
        <asp:GridView ID="grdBeeBookScheduleList" runat="server" AutoGenerateColumns="False" EnableViewState="true" Width="100%" Visible="false" HeaderStyle-BackColor="#ffffcc">
            <Columns>

                <asp:BoundField DataField="Year" Visible="True" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="Event" Visible="True" HeaderText="Event"></asp:BoundField>
                <asp:BoundField DataField="Chapter" Visible="true" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="Components" Visible="true" HeaderText="Components"></asp:BoundField>
                <asp:BoundField DataField="SubComponents" Visible="true" HeaderText="Sub Components"></asp:BoundField>
                <asp:BoundField DataField="Duration" Visible="true" HeaderText="Duration"></asp:BoundField>
                <asp:BoundField DataField="EndDate" Visible="true" HeaderText="EndDate"></asp:BoundField>





            </Columns>
        </asp:GridView>


    </div>

    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="1250px" runat="server">


        <tr>
            <td colspan="2">

                <div>
                    <center>
                        <div runat="server" id="div1">
                            <cc1:ScheduleControl ID="EventCalendarControl1" runat="server" Height="91px" Width="300px" />
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </div>
                    </center>
                </div>



            </td>
        </tr>
        <tr>
            <td colspan="2">

                <div>
                    <center>
                        <div runat="server" id="div7" visible="false">
                            <cc1:ScheduleControl ID="EventCalendarControlRecruiting" runat="server" Height="91px" Width="300px" />
                            <asp:Label ID="Label4" runat="server"></asp:Label>
                        </div>
                    </center>
                </div>



            </td>
        </tr>
        <tr>
            <td colspan="2">

                <div>
                    <center>
                        <div runat="server" id="div2" visible="false">
                            <cc1:ScheduleControl ID="EventCalendarControlPhotos" runat="server" Height="91px" Width="300px" />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                        </div>
                    </center>
                </div>



            </td>
        </tr>
        <tr>
            <td colspan="2">

                <div>
                    <center>
                        <div runat="server" id="div3" visible="false">
                            <cc1:ScheduleControl ID="EventCalendarControlArticle1" runat="server" Height="91px" Width="300px" />
                            <asp:Label ID="Label2" runat="server"></asp:Label>
                        </div>
                    </center>
                </div>



            </td>
        </tr>
        <tr>
            <td colspan="2">

                <div>
                    <center>
                        <div runat="server" id="div4" visible="false">
                            <cc1:ScheduleControl ID="EventCalendarControlArticle2" runat="server" Height="91px" Width="300px" />
                            <asp:Label ID="Label3" runat="server"></asp:Label>
                        </div>
                    </center>
                </div>



            </td>
        </tr>
    </table>
    <%--<div style="float: left;">
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:200px; text-align:left;">Articles 2</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">Start Date</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">End Date</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #e94c6f;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Invite Material</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #542733;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Deadline For Material</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">28-06-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #ff0000;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left">Review Material</div>
            <div style="float: left; margin-left: 2px; width: 150px;">28-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">12-07-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #db3340;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Edit And Placements</div>
            <div style="float: left; margin-left: 2px; width: 150px;">12-07-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">19-07-2015</div>
        </div>
    </div>
    <div style="float: left; margin-left: 50px;">
         <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:200px; text-align:left;">Articles 1</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">Start Date</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">End Date</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #e8b71a;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Invite Articles</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #fa6900;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Deadline For Articles</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">05-07-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #354458;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Review Articles</div>
            <div style="float: left; margin-left: 2px; width: 150px;">05-07-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">19-07-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #982395;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Edit And Placements</div>
            <div style="float: left; margin-left: 2px; width: 150px;">19-07-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">26-07-2014</div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 50px;"></div>
    <div style="float: left;">
         <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:200px; text-align:left;">Photos</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">Start Date</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">End Date</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #260126;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Last Day of Regionals</div>
            <div style="float: left; margin-left: 2px; width: 150px;">26-04-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">26-04-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #35404f;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">All Scores Are In</div>
            <div style="float: left; margin-left: 2px; width: 150px;">26-04-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">03-05-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #83bf17;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Select List Of Invites</div>
            <div style="float: left; margin-left: 2px; width: 150px;">03-05-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">17-05-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #1352a2;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Open Registration</div>
            <div style="float: left; margin-left: 2px; width: 150px;"> 17-05-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">24-05-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #e94c6f;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Priority List</div>
            <div style="float: left; margin-left: 2px; width: 150px;">24-05-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #542733;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Wait List</div>
            <div style="float: left; margin-left: 2px; width: 150px;">07-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">21-06-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #ff0000;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Photo Deadline</div>
            <div style="float: left; margin-left: 2px; width: 150px;">21-06-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">05-07-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #db3340;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Photo Cleanup</div>
            <div style="float: left; margin-left: 2px; width: 150px;">05-07-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">19-07-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #e8b71a;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">First Draft Of Bee Book</div>
            <div style="float: left; margin-left: 2px; width: 150px;">19-07-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">26-07-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #fa6900;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Final Draft</div>
            <div style="float: left; margin-left: 2px; width: 150px;">26-07-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">09-08-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #354458;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Printing Job</div>
            <div style="float: left; margin-left: 2px; width: 150px;">09-08-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">23-08-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #982395;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Shipping</div>
            <div style="float: left; margin-left: 2px; width: 150px;">23-08-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">28-08-2015</div>
        </div>
    </div>
    <div style="float: left; margin-left: 50px;">
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:200px; text-align:left;">Recruiting</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">Start Date</div>
        <div style="float: left; font-weight: bold; font-size: 14px; margin-top: 10px; width:150px;">End Date</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #260126;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Recruit team Lead</div>
            <div style="float: left; margin-left: 2px; width: 150px;">01-11-2014</div>
            <div style="float: left; margin-left: 2px; width: 150px;">01-11-2014</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #35404f;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Recruit Editors</div>
            <div style="float: left; margin-left: 2px; width: 150px;">01-11-2014</div>
            <div style="float: left; margin-left: 2px; width: 150px;">15-11-2014</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #83bf17;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Secure Software</div>
            <div style="float: left; margin-left: 2px; width: 150px;">15-11-2014</div>
            <div style="float: left; margin-left: 2px; width: 150px;">29-11-2014</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #1352a2;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Learning Software</div>
            <div style="float: left; margin-left: 2px; width: 150px;">29-11-2014</div>
            <div style="float: left; margin-left: 2px; width: 150px;">27-12-2014</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #e94c6f;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Load Previous Bee Book</div>
            <div style="float: left; margin-left: 2px; width: 150px;">27-12-2014</div>
            <div style="float: left; margin-left: 2px; width: 150px;">10-01-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #542733;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Prepare Schedules</div>
            <div style="float: left; margin-left: 2px; width: 150px;">10-01-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">24-01-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #ff0000;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Front Cover</div>
            <div style="float: left; margin-left: 2px; width: 150px;">24-01-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">21-02-2015</div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div>
            <div style="float: left; height: 20px; width: 20px; background-color: #db3340;"></div>
            <div style="float: left; margin-left: 2px; width: 175px; text-align:left;">Back Cover</div>
            <div style="float: left; margin-left: 2px; width: 150px;">21-02-2015</div>
            <div style="float: left; margin-left: 2px; width: 150px;">21-03-2015</div>
        </div>
        
    </div>--%>
</asp:Content>
