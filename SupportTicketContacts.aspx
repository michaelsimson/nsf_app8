﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SupportTicketContacts.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="SupportTicketContacts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 1120px;
            height: 150px;
            top: 55%;
            left: 50%;
            margin-left: -561px;
            margin-top: -200px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }
    </style>

    <link href="css/jquery.qtip.min.css" rel="stylesheet" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script>
        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }
        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvMemberDetails").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvMemberDetails").hide();
        }

        $(function (e) {


            $(".lnkBmember").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {
                        var strHtml = "";


                        strHtml += "<div>";
                        strHtml += '<div style="font-size: 14px; float: left; width:50px;"><span style="font-weight: bold;">Name </span></div>';
                        strHtml += "<div style='float:left;'>:</div>"
                        strHtml += '<div style="font-size: 14px; float: left; margin-left:1px;"><span >' + $(this).attr("attr-name") + '</span></div>';
                        strHtml += "</div>";

                        strHtml += "<div style='clear:both; margin-bottom:5px;'></div>";

                        strHtml += "<div>";
                        strHtml += '<div style="font-size: 14px; float: left; width:50px;"><span style="font-weight: bold;">Email </span></div>';
                        strHtml += "<div style='float:left;'>:</div>"
                        strHtml += '<div style="font-size: 14px; float: left; margin-left:1px;"><span >' + $(this).attr("attr-email") + ' </span></div>';
                        strHtml += "</div>";

                        strHtml += "<div style='clear:both; margin-bottom:5px;'></div>";

                        strHtml += "<div>";
                        strHtml += '<div style="font-size: 14px; float: left; width:50px;"><span style="font-weight: bold;">CPhone </span></div>';
                        strHtml += "<div style='float:left;'>:</div>"
                        strHtml += '<div style="font-size: 14px; float: left; margin-left:1px;"><span >' + $(this).attr("attr-cPhone") + '</span></div>';
                        strHtml += "</div>";


                        strHtml += "<div style='clear:both; margin-bottom:5px;'></div>";

                        return strHtml;
                    },

                    title: 'Backup Contact',
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow'
                },
                show: {
                    solo: true
                }
            })
        });

        function getConfirmfromUser(type, cType) {


            if (confirm("Are you sure want to delete " + type + "?")) {

                if (cType == 1) {
                    document.getElementById("<%=btnDeleteP.ClientID%>").click();
                } else if (cType == 2) {
                    document.getElementById("<%=btnDeleteB.ClientID%>").click();
                } else if (cType == 3) {
                    document.getElementById("<%=btnDeleteC.ClientID%>").click();
                }
    }
}


    </script>

    <asp:Button ID="btnDeleteP" runat="server" Text="Delete Contact" Style="display: none;" OnClick="btnDeleteP_Click" />
    <asp:Button ID="btnDeleteB" runat="server" Text="Delete Contact" Style="display: none;" OnClick="btnDeleteB_Click" />
    <asp:Button ID="btnDeleteC" runat="server" Text="Delete Contact" Style="display: none;" OnClick="btnDeleteC_Click" />


    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <table id="tblSupportTicketContacts" border="0" cellpadding="3" cellspacing="0" width="1200px" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Support Ticket Contatcs</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td>
                <div style="text-align: center">
                    <asp:Label Style="color: red;" ID="lblErrMsg" runat="server"></asp:Label>
                </div>
                <br />


                <table style="margin-left: auto; margin-right: auto; width: 65%;">

                    <tr>



                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                        <td style="width: 100px" align="left">
                            <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product Group </b></td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddlProductGroup" runat="server" Width="100px"
                                AutoPostBack="True" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                            </asp:DropDownList>

                        </td>
                        <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Product</b> </td>
                        <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                            <asp:DropDownList ID="ddlProduct" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="false" OnClick="btnSubmit_Click" />
                        </td>

                        <td>

                            <asp:Button ID="btnAddNew" runat="server" Text="AddNew" OnClick="btnAddNew_Click" />
                        </td>

                    </tr>
                </table>
                <div style="text-align: center">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>


            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center>
                    <div style="background-color: #FFFFCC; width: 90%; margin-bottom: 10px;">
                        <div style="background-color: #FFFFCC; width: 100%;">
                            <table width="100%" id="tblContactGroup" runat="server" visible="false" style="margin-left: auto; margin-bottom: 10px; margin-right: auto; font-weight: bold; background-color: #ffffcc;">

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Primary
                                    </td>
                                    <td>Backup 1</td>
                                    <td>Backup 2
                                    </td>
                                </tr>
                                <tr class="ContentSubTitle">
                                    <td>Volunteer In Charge</td>
                                    <td>
                                        <asp:TextBox ID="txtVolunteerInCharge" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnVolunteerFind" OnClick="btnVolunteerFind_onClick" runat="server" Text="Search" Width="60" />
                                        <asp:Button ID="btnDeletePMember" OnClick="btnDeletePMember_onClick" runat="server" Text="Delete" Width="60" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBMemberID" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnBMemberID" OnClick="btnBMemberID_onClick" runat="server" Text="Search" Width="60" />
                                        <asp:Button ID="BtnDeleteBmemberID" OnClick="BtnDeleteBmemberID_onClick" runat="server" Text="Delete" Width="60" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCMemberID" Enabled="false" runat="server" Width="150px" Height="18px"></asp:TextBox>
                                        <asp:Button ID="btnCMemberID" OnClick="btnCMemberID_onClick" runat="server" Text="Search" Width="60" />
                                        <asp:Button ID="btnDeleteCMemberID" OnClick="btnDeleteCMemberID_onClick" runat="server" Text="Delete" Width="60" />
                                    </td>
                                </tr>

                                <tr align="center">

                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnSaveConatct" runat="server" Text="Add" Width="60" OnClick="btnSaveConatct_Click" />
                                        <asp:Button ID="btnCancelContact" runat="server" Text="Cancel" Width="60" OnClick="btnCancelContact_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </center>
                <div style="clear: both; margin-top: 10px;"></div>
                <center>
                    <asp:Panel ID="pIndSearch" runat="server" Width="1100px" Visible="false" HorizontalAlign="Center">
                        <b>Search NSF member</b>
                        <div align="center" style="width: 1100px;">
                            <table border="1" runat="server" id="tblIndSearch" style="text-align: center; margin-bottom: 10px;" width="30%" visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Donor Type:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server"
                                            OnSelectedIndexChanged="ddlDonorType_SelectedIndexChanged">
                                            <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                                            <asp:ListItem Value="Organization">Organization</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Organization Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>

                            <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                                <b>Search Result</b>
                                <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1100px; margin-bottom: 10px;" OnRowCommand="GridMemberDt_RowCommand" HeaderStyle-BackColor="#ffffcc">
                                    <Columns>
                                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                        <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                        <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                        <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="2">


                <div id="dvSupportContactList" runat="server" visible="false" style="width: 20%; margin-left: auto; margin-right: auto;"><span style="font-weight: bold;">Table 1 : </span><span style="font-weight: bold;">Support Contact List</span></div>

                <div style="clear: both;"></div>
                <asp:GridView ID="grdSupportContactList" runat="server" OnRowCommand="grdSupportContactList_RowCommand" AutoGenerateColumns="false" Width="1200px" Style="margin-left: auto; margin-right: auto;" HeaderStyle-BackColor="#ffffcc">
                    <Columns>


                        <asp:TemplateField HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                            <ItemTemplate>
                                <asp:Button ID="btnModifyGroup" runat="server" Text="Modify" CommandName="Modify" />
                                <div style="display: none;">
                                    <asp:Label ID="lblSupportContID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SupTicketContID") %>'></asp:Label>
                                    <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                    <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                                    <asp:Label ID="lblBackUpName1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BackUpName1") %>'></asp:Label>
                                    <asp:Label ID="lblBackUpName2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BackUpName2") %>'></asp:Label>
                                    <asp:Label ID="lblBMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BMemberID") %>'></asp:Label>
                                    <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'></asp:Label>
                                </div>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:BoundField DataField="MemberID" Visible="true" HeaderText="Member ID" HeaderStyle-Width="50px" />

                        <asp:BoundField DataField="Pname" Visible="true" HeaderText="Name" HeaderStyle-Width="100px" />
                        <asp:BoundField DataField="LastName" Visible="false" HeaderText="Last Name" HeaderStyle-Width="90px"></asp:BoundField>

                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" HeaderText="BMember ID">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBmemberID" CssClass="lnkBmember" attr-name='<%#DataBinder.Eval(Container.DataItem,"BackUpName1") %>' attr-email='<%#DataBinder.Eval(Container.DataItem,"BMail") %>' attr-cphone='<%#DataBinder.Eval(Container.DataItem,"BPhone") %>' runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BMemberID") %>' CommandName="BMember"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:ButtonField DataTextField="BMemberID" HeaderText="BMember ID" CommandName="BMember" HeaderStyle-Width="50px"></asp:ButtonField>--%>
                        <asp:BoundField DataField="BackUpName1" HeaderText="BMember" HeaderStyle-Width="50px"></asp:BoundField>
                        <%--  <asp:ButtonField DataTextField="CMemberID" HeaderText="CMember ID" CommandName="CMember" HeaderStyle-Width="50px"></asp:ButtonField>--%>
                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" HeaderText="CMember ID">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCmemberID" CssClass="lnkBmember" attr-name='<%#DataBinder.Eval(Container.DataItem,"BackUpName2") %>' attr-email='<%#DataBinder.Eval(Container.DataItem,"CMail") %>' attr-cphone='<%#DataBinder.Eval(Container.DataItem,"BCPhone") %>' runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>' CommandName="BMember"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField DataField="BackupName2" HeaderText="CMember" HeaderStyle-Width="50px"></asp:BoundField>

                        <asp:BoundField DataField="Event" Visible="true" HeaderText="Event" HeaderStyle-Width="100px"></asp:BoundField>

                        <asp:BoundField DataField="ProductGroup" Visible="true" HeaderText="Product Group" HeaderStyle-Width="100px"></asp:BoundField>
                        <asp:BoundField DataField="Product" Visible="true" HeaderText="Product" HeaderStyle-Width="125px" />
                        <asp:BoundField DataField="Email" Visible="true" HeaderText="Email" HeaderStyle-Width="125px" />

                        <asp:BoundField DataField="HPhone" Visible="true" HeaderText="Home Phone" HeaderStyle-Width="150px"></asp:BoundField>
                        <asp:BoundField DataField="CPhone" Visible="true" HeaderText="Cell Phone" HeaderStyle-Width="125px"></asp:BoundField>
                        <asp:BoundField DataField="WPhone" Visible="true" HeaderText="Work Phone" HeaderStyle-Width="125px"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>
    <div id="dvMemberDetails" class="web_dialog">
        <center>
            <span id="spnMemberTitle" style="font-family: Trebuchet MS; font-weight: bold;" runat="server"></span>
        </center>
        <center>

            <asp:GridView ID="grdMemberInfo" runat="server" AutoGenerateColumns="false" Width="1100px" Style="margin-left: auto; margin-right: auto; margin-top: 5px;" HeaderStyle-BackColor="#ffffcc">
                <Columns>

                    <asp:BoundField DataField="MemberID" Visible="true" HeaderText="Member ID" HeaderStyle-Width="60px" />

                    <asp:BoundField DataField="FirstName" Visible="true" HeaderText="First Name" HeaderStyle-Width="80px" />
                    <asp:BoundField DataField="LastName" Visible="true" HeaderText="Last Name" HeaderStyle-Width="80px"></asp:BoundField>

                    <asp:BoundField DataField="Event" Visible="true" HeaderText="Event" HeaderStyle-Width="90px"></asp:BoundField>

                    <asp:BoundField DataField="ProductGroup" Visible="true" HeaderText="Product Group" HeaderStyle-Width="90px"></asp:BoundField>
                    <asp:BoundField DataField="Product" Visible="true" HeaderText="Product" HeaderStyle-Width="90px" />
                    <asp:BoundField DataField="Email" Visible="true" HeaderText="Email" HeaderStyle-Width="125px" />

                    <asp:BoundField DataField="HPhone" Visible="true" HeaderText="Home Phone" HeaderStyle-Width="150px"></asp:BoundField>
                    <asp:BoundField DataField="CPhone" Visible="true" HeaderText="Cell Phone" HeaderStyle-Width="150px"></asp:BoundField>
                    <asp:BoundField DataField="WPhone" Visible="true" HeaderText="Work Phone" HeaderStyle-Width="150px"></asp:BoundField>
                </Columns>
            </asp:GridView>
        </center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <input type="button" id="btnClose" onclick="HideDialog()" value="Close" />
        </center>
    </div>


    <input type="hidden" id="hdnAutoMemberID" runat="server" value="" />
    <input type="hidden" id="hdnSupportContactID" runat="server" value="" />
    <input type="hidden" id="hdnMemberFlag" runat="server" value="" />
    <input type="hidden" id="hdnBackupMemberID1" runat="server" value="" />
    <input type="hidden" id="hdnBackupMemberID2" runat="server" value="" />
    <input type="hidden" id="hdnRowIndex" runat="server" value="" />

    <div>
    </div>
</asp:Content>
