<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="NationalFinals_EmailInvite.aspx.vb" Inherits="VRegistration.NationalFinals_EmailInvite" %>
 
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    <LINK href="Styles/PreviewStyle.css" type="text/css" rel="stylesheet">
 <script language="javascript" src="JScript/Preview.js" type="text/javascript"></script>
     <div style="text-align: center">  

			<table width="100%">
				<tr>
					<td class="Heading" noWrap align="center" colSpan="2">
						National&nbsp;Invitees List
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right">Contest&nbsp;Year</td>
					<td vAlign="top" noWrap align="left"><asp:dropdownlist id="ddlEventYear" runat="server" CssClass="SmallFont" AutoPostBack="True">
							<asp:ListItem>Select Contest Year</asp:ListItem>
							<asp:ListItem Value="2008">2008</asp:ListItem>
							<asp:ListItem Value="2009">2009</asp:ListItem>
							<asp:ListItem Value="2010">2010</asp:ListItem>
							<asp:ListItem Value="2011">2011</asp:ListItem>
							<asp:ListItem Value="2012">2012</asp:ListItem>
							<asp:ListItem Value="2013">2013</asp:ListItem>
							<asp:ListItem Value="2014">2014</asp:ListItem>
							<asp:ListItem Value="2015">2015</asp:ListItem>
							<asp:ListItem Value="2016">2016</asp:ListItem>
							<asp:ListItem Value="2017">2017</asp:ListItem>
							<asp:ListItem Value="2018">2018</asp:ListItem>
							<asp:ListItem Value="2019">2019</asp:ListItem>
							<asp:ListItem Value="2020">2020</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td colSpan="2" class="ContentSubTitle"><asp:datagrid Visible="false" id="dgInviteeList" runat="server" CssClass="GridStyle" Width="100%" AllowSorting="True"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="email">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Parent Email">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblEMail" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.email") %>'>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
						</asp:datagrid></td>
				</tr>
				<tr><td>
				<asp:Button id="btnEmailPreview" runat="server" CssClass="FormButtonCenter" Text="Email Preview"></asp:Button>
				
				</td>
				<td colspan="1" align="left"> 
				<asp:RadioButtonList 
                                    id="rblSendEmail" 
                                    runat="server">   
                            <asp:ListItem value="All" Selected>Send email to all invitees</asp:ListItem>
                            <asp:ListItem value="NotRegistered">Send email to invitees who have not yet registered</asp:ListItem>
                            
                </asp:RadioButtonList></td>
				</tr>
				<tr><td>
				</td>
					<td colspan="1" align="left"><br />
						
							
						<div>
						<asp:Label Runat="server" id="Label1" CssClass="SmallFont">Type Subject for the Email in the below Text Box </asp:Label>
						
							<asp:TextBox Runat="server" ID="txtSubject" CssClass="SmallFont" Width="80%"></asp:TextBox></div>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center" class="ItemCenter">
						<asp:Button id="btnSendEmail" runat="server" Text="Send Email Invite" CssClass="FormButtonCenter"></asp:Button>
					</td>
				</tr>
				<tr>
					<td align="left" colSpan="2"><asp:hyperlink id="hlinkUserFunctions" runat="server" NavigateUrl="">Back to Main Page</asp:hyperlink></td>
				</tr>

			</table>
			<asp:Label ID="lblError" Runat="server" ForeColor="red" Font-Bold="True"></asp:Label>
			
			<div id="Preview_area" style="display:none;" runat="server">
			
			</div>
			<br>
			<br>
			<br>
			<br>
        </div>
        </asp:Content>
 

 
 
 