Imports System
Imports System.Text
Imports System.IO
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Namespace VRegistration
    Public Class FinalsSelectionCriteria
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'Protected WithEvents ddlEventYear As System.Web.UI.WebControls.DropDownList
        'Protected WithEvents rblContestType As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents rfvContestType As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
        'Protected WithEvents rblJSB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents rblSSB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMarksGradeSSB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblJVB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents rblIVB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMarksGradeIVB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblSVB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMarksGradeSVB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblMB1 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMB1 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblMB2 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMB2 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblMB3 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMB3 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblMB4 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtMB4 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblJGB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtJGB As System.Web.UI.WebControls.TextBox
        Protected WithEvents rblSJB As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents txtSJB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblEW1 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtEW1 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblEW2 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtEW2 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblEW3 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtEW3 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblPS1 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtPS1 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblPS2 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtPS2 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblPS3 As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtPS3 As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblBB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtBB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rblSGB As System.Web.UI.WebControls.RadioButtonList
        'Protected WithEvents txtSGB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents txtMarksGradeJSB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents txtMarksGradeJVB As System.Web.UI.WebControls.TextBox
        'Protected WithEvents rfvRBLJSB As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents RequiredFieldValidator18 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRBLSSB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvrblJVB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvrblIVB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvrblSVB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvrblMB1 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvrblMB2 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvrblMB3 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblMB4 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblJGB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblSGB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblEW1 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblEW2 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblEW3 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblPS1 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblPS2 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvRblPS3 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtJSB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvtxtSSB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtJVB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtIVB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtSVB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtMB1 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtMB2 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtMB3 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtMB4 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtJGB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtSGB As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtEW1 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtEW2 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtEW3 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtPSJ As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtPS2 As System.Web.UI.WebControls.RequiredFieldValidator
        'Protected WithEvents rfvTxtPS3 As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents hlinkUserFunctions As System.Web.UI.WebControls.HyperLink

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Session("LoggedIn") <> True Then
                Response.Redirect("..\login.aspx")
                'Server.Transfer("")
            End If

            If Not Page.IsPostBack Then
                ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
                rblContestType.Items.FindByValue(Application("ContestType")).Selected = True
                ddlEventYear.Enabled = False
                rblContestType.Enabled = False
                txtBB.Text = "0"

                LoadPageData()

                Dim redirectURL As String
                If Request.ServerVariables("Server_Name") <> "localhost" Then
                    redirectURL = "/app6/UserFunctions.aspx"
                Else
                    redirectURL = "/VRegistration/UserFunctions.aspx"
                End If
                hlinkUserFunctions.NavigateUrl = redirectURL

            End If
        End Sub
        Private Sub LoadPageData()
            Dim dsContestCategory As DataSet
            Dim drContestCategory As DataRow

            Dim prmArray(1) As SqlParameter

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ContestYear"
            prmArray(0).Value = Application("ContestYear")
            prmArray(0).Direction = ParameterDirection.Input
            Dim ctr As Integer
            dsContestCategory = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestCategoryAll", prmArray)

            If dsContestCategory.Tables.Count > 0 Then
                If dsContestCategory.Tables(0).Rows.Count > 0 Then
                    For ctr = 0 To dsContestCategory.Tables(0).Rows.Count - 1
                        drContestCategory = dsContestCategory.Tables(0).Rows(ctr)
                        Select Case drContestCategory("ContestCode").ToString
                            Case "JSB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblJSB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMarksGradeJSB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "SSB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblSSB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMarksGradeSSB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "JVB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblJVB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMarksGradeJVB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "IVB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblIVB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMarksGradeIVB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "SVB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblSVB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMarksGradeSVB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "MB1"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblMB1.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMB1.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "MB2"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblMB2.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMB2.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "MB3"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblMB3.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMB3.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "MB4"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblMB4.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtMB4.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "JGB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblJGB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtJGB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "SGB"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblSGB.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtSGB.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "EW1"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblEW1.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtEW1.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "EW2"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblEW2.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtEW2.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "EW3"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblEW3.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtEW3.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "PS1"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblPS1.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtPS1.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "PS2"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblPS2.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtPS2.Text = drContestCategory("CriteriaValue").ToString
                                End If
                            Case "PS3"
                                If Not IsDBNull(drContestCategory("NationalSelectionCriteria")) Then
                                    rblPS3.Items.FindByValue(drContestCategory("NationalSelectionCriteria")).Selected = True
                                    txtPS3.Text = drContestCategory("CriteriaValue").ToString
                                End If
                        End Select
                    Next
                End If
            End If

        End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim prmArray(2) As SqlParameter

            Dim sbContest As New StringBuilder
            sbContest.Append("JSB," & rblJSB.SelectedItem.Value & "," & txtMarksGradeJSB.Text & "~")
            sbContest.Append("SSB," & rblSSB.SelectedItem.Value & "," & txtMarksGradeSSB.Text & "~")

            sbContest.Append("JVB," & rblJVB.SelectedItem.Value & "," & txtMarksGradeJVB.Text & "~")
            sbContest.Append("IVB," & rblIVB.SelectedItem.Value & "," & txtMarksGradeIVB.Text & "~")
            sbContest.Append("SVB," & rblSVB.SelectedItem.Value & "," & txtMarksGradeSVB.Text & "~")

            sbContest.Append("MB1," & rblMB1.SelectedItem.Value & "," & txtMB1.Text & "~")
            sbContest.Append("MB2," & rblMB2.SelectedItem.Value & "," & txtMB2.Text & "~")
            sbContest.Append("MB3," & rblMB3.SelectedItem.Value & "," & txtMB3.Text & "~")
            sbContest.Append("MB4," & rblMB4.SelectedItem.Value & "," & txtMB4.Text & "~")

            sbContest.Append("JGB," & rblJGB.SelectedItem.Value & "," & txtJGB.Text & "~")
            sbContest.Append("SGB," & rblSGB.SelectedItem.Value & "," & txtSGB.Text & "~")

            sbContest.Append("EW1," & rblEW1.SelectedItem.Value & "," & txtEW1.Text & "~")
            sbContest.Append("EW2," & rblEW2.SelectedItem.Value & "," & txtEW2.Text & "~")
            sbContest.Append("EW3," & rblEW3.SelectedItem.Value & "," & txtEW3.Text & "~")

            sbContest.Append("PS1," & rblPS1.SelectedItem.Value & "," & txtPS1.Text & "~")
            sbContest.Append("PS2," & rblPS2.SelectedItem.Value & "," & txtPS2.Text & "~")
            sbContest.Append("PS3," & rblPS3.SelectedItem.Value & "," & txtPS3.Text & "~")

            sbContest.Append("BB ," & rblBB.SelectedItem.Value & "," & txtBB.Text & "~")

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ContestYear"
            prmArray(0).Value = ddlEventYear.SelectedValue
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@NationalDetails"
            prmArray(1).Value = sbContest.ToString
            prmArray(1).Direction = ParameterDirection.Input

            SqlHelper.ExecuteNonQuery(conn.ConnectionString, CommandType.StoredProcedure, "usp_UpdateNationalSelectionCriteria", prmArray)
        End Sub
    End Class
End Namespace

