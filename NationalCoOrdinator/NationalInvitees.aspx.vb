Imports System.Data.SqlClient
Namespace VRegistration
    Partial Class NationalInvitees
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            'Response.Write("Page Under Construction!.")
            'Response.End()

            If Not Page.IsPostBack Then
                ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
                ddlEventYear.Enabled = False

                If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If

                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim cmd As New SqlCommand
                Dim dsInvitees As New DataSet
                Dim da As New SqlDataAdapter

                cmd.Connection = conn
                conn.Open()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "usp_GetNationalFinalsInvitees"
                cmd.Parameters.Add(New SqlParameter("@ContestYear", Application("ContestYear")))

                da.SelectCommand = cmd
                da.Fill(dsInvitees)

                Dim dvJSB As DataView = dsInvitees.Tables(0).DefaultView
                dvJSB.RowFilter = "ContestCategoryID = 85"
                lblJSBTotal.Text = dvJSB.Count
                dgJSBInvitees.DataSource = dvJSB
                dgJSBInvitees.DataBind()

                Dim dvSSB As DataView = dsInvitees.Tables(0).DefaultView
                dvSSB.RowFilter = "ContestCategoryID = 86"
                lblSSBTotal.Text = dvSSB.Count
                dgSSBInvitees.DataSource = dvSSB
                dgSSBInvitees.DataBind()

                Dim dvJVB As DataView = dsInvitees.Tables(0).DefaultView
                dvJVB.RowFilter = "ContestCategoryID = 36"
                lblJVBTotal.Text = dvJVB.Count
                dgJVBInvitees.DataSource = dvJVB
                dgJVBInvitees.DataBind()

                Dim dvIVB As DataView = dsInvitees.Tables(0).DefaultView
                dvIVB.RowFilter = "ContestCategoryID = 37"
                lblIVBTotal.Text = dvIVB.Count
                dgIVBInvitees.DataSource = dvIVB
                dgIVBInvitees.DataBind()

                Dim dvSVB As DataView = dsInvitees.Tables(0).DefaultView
                dvSVB.RowFilter = "ContestCategoryID = 38"
                lblSVBTotal.Text = dvSVB.Count
                dgSVBInvitees.DataSource = dvSVB
                dgSVBInvitees.DataBind()


                Dim dvMB1 As DataView = dsInvitees.Tables(0).DefaultView
                dvMB1.RowFilter = "ContestCategoryID = 39"
                lblMB1Total.Text = dvMB1.Count
                dgMB1Invitees.DataSource = dvMB1
                dgMB1Invitees.DataBind()

                Dim dvMB2 As DataView = dsInvitees.Tables(0).DefaultView
                dvMB2.RowFilter = "ContestCategoryID = 40"
                lblMB2Total.Text = dvMB2.Count
                dgMB2Invitees.DataSource = dvMB2
                dgMB2Invitees.DataBind()

                Dim dvMB3 As DataView = dsInvitees.Tables(0).DefaultView
                dvMB3.RowFilter = "ContestCategoryID = 41"
                lblMB3Total.Text = dvMB3.Count
                dgMB3Invitees.DataSource = dvMB3
                dgMB3Invitees.DataBind()

                Dim dvJGB As DataView = dsInvitees.Tables(0).DefaultView
                dvJGB.RowFilter = "ContestCategoryID = 43"
                lblJGBTotal.Text = dvJGB.Count
                dgJGBInvitees.DataSource = dvJGB
                dgJGBInvitees.DataBind()

                Dim dvSGB As DataView = dsInvitees.Tables(0).DefaultView
                dvSGB.RowFilter = "ContestCategoryID = 44"
                lblSGBTotal.Text = dvSGB.Count
                dgSGBInvitees.DataSource = dvSGB
                dgSGBInvitees.DataBind()

                'Dim redirectURL As String
                'If Request.ServerVariables("Server_Name") <> "localhost" Then
                '    redirectURL = "/app6/UserFunctions.aspx"
                'Else
                '    redirectURL = "/VRegistration/UserFunctions.aspx"
                'End If
                'hlinkUserFunctions.NavigateUrl = redirectURL
            End If
        End Sub
    End Class
End Namespace

