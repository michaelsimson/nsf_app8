<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="NationalInvitees.aspx.vb" Inherits="VRegistration.NationalInvitees" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    
     <div style="text-align: center">  
    
		<table width="100%">
					<tr>
						<td class="Heading" noWrap align="center" colSpan="2">National&nbsp;Invitees List
						</td>
					</tr>
					<tr>
						<td class="ItemLabel" vAlign="top" noWrap align="right">Contest&nbsp;Year</td>
						<td vAlign="top" noWrap align="left"><asp:dropdownlist id="ddlEventYear" runat="server" AutoPostBack="True" CssClass="SmallFont">
								<asp:ListItem>Select Contest Year</asp:ListItem>
								<asp:ListItem Value="1995">1995</asp:ListItem>
								<asp:ListItem Value="1996">1996</asp:ListItem>
								<asp:ListItem Value="1997">1997</asp:ListItem>
								<asp:ListItem Value="1998">1998</asp:ListItem>
								<asp:ListItem Value="1999">1999</asp:ListItem>
								<asp:ListItem Value="2000">2000</asp:ListItem>
								<asp:ListItem Value="2001">2001</asp:ListItem>
								<asp:ListItem Value="2002">2002</asp:ListItem>
								<asp:ListItem Value="2003">2003</asp:ListItem>
								<asp:ListItem Value="2004">2004</asp:ListItem>
								<asp:ListItem Value="2005">2005</asp:ListItem>
								<asp:ListItem Value="2006">2006</asp:ListItem>
								<asp:ListItem Value="2007">2007</asp:ListItem>
								<asp:ListItem Value="2008">2008</asp:ListItem>
								<asp:ListItem Value="2009">2000</asp:ListItem>
								<asp:ListItem Value="2010">2010</asp:ListItem>
								<asp:ListItem Value="2011">2011</asp:ListItem>
								<asp:ListItem Value="2012">2012</asp:ListItem>
								<asp:ListItem Value="2013">2013</asp:ListItem>
								<asp:ListItem Value="2014">2014</asp:ListItem>
								<asp:ListItem Value="2015">2015</asp:ListItem>
								<asp:ListItem Value="2016">2016</asp:ListItem>
								<asp:ListItem Value="2017">2017</asp:ListItem>
								<asp:ListItem Value="2018">2018</asp:ListItem>
								<asp:ListItem Value="2019">2019</asp:ListItem>
								<asp:ListItem Value="2020">2020</asp:ListItem>
							</asp:dropdownlist></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">JSB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgJSBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="lblCity" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="lblContestDescription" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id=lblContestantName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="lblScore1" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="lblScore2" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="lblScore3" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="lblRank" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblGrade" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblEMail" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label90" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentSubHeading">Total Invitees:</td>
						<td><asp:Label ID="lblJSBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">SSB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgSSBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label1" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label2" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label4" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label5" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label6" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label7" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label8" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label11" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label91" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblSSBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">JVB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgJVBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label12" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label13" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label14" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label15" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label16" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label17" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label18" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label19" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label22" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label92" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblJVBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">IVB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgIVBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label24" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label25" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label26" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label27" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label28" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label29" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label30" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label31" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label34" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label93" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblIVBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">SVB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgSVBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label79" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label80" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label81" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label82" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label83" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label84" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label85" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label86" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label89" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label78" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblSVBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">MB1 Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgMB1Invitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label23" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label35" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label36" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label37" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label38" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label39" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label40" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label41" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label44" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label94" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblMB1Total" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">MB2 Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgMB2Invitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label46" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label47" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label48" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label49" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label50" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label51" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label52" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label53" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label56" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label95" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblMB2Total" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">MB3 Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgMB3Invitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label58" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label59" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label60" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label61" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label62" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label63" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label64" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label65" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label68" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label96" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblMB3Total" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					
					
					<tr>
						<td class="ContentTitle" align="center" colspan="2">JGB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgJGBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label45" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label57" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label69" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label70" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label71" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label72" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label73" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label74" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label77" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblJGBTotal" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									</Columns>
								</asp:datagrid>
								</td>
							</tr>
												<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblJGBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
					<tr>
						<td class="ContentTitle" align="center" colspan="2">SGB Invitees</td>
					</tr>
					<tr>
						<td class="ContentSubTitle" colSpan="2"><asp:datagrid id="dgSGBInvitees" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
								CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
								<FooterStyle CssClass="GridFooter"></FooterStyle>
								<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
								<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
								<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
								<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Chapter Name">
										<ItemTemplate>
											<asp:Label id="Label45" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.City") & " " & DataBinder.Eval(Container, "DataItem.State") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="ContestDescription">
										<ItemTemplate>
											<asp:Label id="Label57" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDesc") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Type">
										<ItemTemplate>
											<asp:Label id="Label69" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.First_Name") & " " & DataBinder.Eval(Container, "DataItem.Last_Name")%>' CssClass=SmallFont>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 1">
										<ItemTemplate>
											<asp:Label id="Label70" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score1") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 2">
										<ItemTemplate>
											<asp:Label id="Label71" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score2") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Score 3">
										<ItemTemplate>
											<asp:Label id="Label72" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Score3") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Rank">
										<ItemTemplate>
											<asp:Label id="Label73" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Rank") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contestant School Grade">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label74" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Parent Email">
										<ItemTemplate>
											<asp:Label runat="server" ID="Label77" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.EMail") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Total Score">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblSGBTotal" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.TotalScore") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
								<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
							</asp:datagrid></td>
					</tr>
					<tr>
						<td class="ContentHeading">Total Invitees:</td>
						<td><asp:Label ID="lblSGBTotal" CssClass="SmallFont" Runat="server"></asp:Label></td>
					</tr>
			</table>
			</div>
			</asp:Content>

 

 
 
 