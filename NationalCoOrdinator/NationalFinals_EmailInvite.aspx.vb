Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Namespace VRegistration
    Partial Class NationalFinals_EmailInvite
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Not Page.IsPostBack Then
                ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
                ddlEventYear.Enabled = False
                'If LCase(Session("LoggedIn")) <> "true" And Session("LoggedIn") <> "LoggedIn" Then
                '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                'End If

                'Srini (vasu99@gmail.com) Commented out ( actual email list in Prod is long
                'Dim conn As New SqlConnection(Application("ConnectionString"))
                'Dim cmd As New SqlCommand
                'Dim dsInvitees As New DataSet
                'Dim da As New SqlDataAdapter

                'cmd.Connection = conn
                'conn.Open()
                'cmd.CommandType = CommandType.StoredProcedure
                'cmd.CommandText = "usp_GetNationalInviteesEmailList"
                'cmd.Parameters.Add(New SqlParameter("@ContestYear", Application("ContestYear")))

                'da.SelectCommand = cmd
                'da.Fill(dsInvitees)

                'If dsInvitees.Tables.Count > 0 Then
                '    dgInviteeList.DataSource = dsInvitees.Tables(0)
                '    dgInviteeList.DataBind()
                'End If

                hlinkUserFunctions.NavigateUrl = "~/VolunteerFunctions.aspx"


            End If
            btnEmailPreview.Attributes.Add("Onclick", "getPreview('Preview_area');")


            Dim re_preview As StreamReader
            Dim emailbody As String
            re_preview = File.OpenText(Server.MapPath(Application("ContestYear") & "NationalInvitation.htm"))
            emailbody = re_preview.ReadToEnd
            re_preview.Close()
            Preview_area.InnerHtml = emailbody
        End Sub

        Private Sub btnSendEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim dsParents As New DataSet
            Dim tblParents() As String = {"Parents"}
            Dim emailbody As String
            Dim re As StreamReader
            Dim strInvalidEmails As New StringBuilder
            Dim strEmailFailure As New StringBuilder
            Dim strEmailDuplicate As New StringBuilder
            Dim prmArray(1) As SqlParameter


            If txtSubject.Text = "" Then
                lblError.Text = "Email Subject is not entered. Please Enter the Email Subject before sending the Email."
                Exit Sub
            End If

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ContestYear"
            prmArray(0).Value = Application("ContestYear")
            prmArray(0).Direction = ParameterDirection.Input
            '*** Actual
            'SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetNationalFinalsInvitees", dsParents, tblParents, prmArray)
            '*** Second Round
            'Srini 6/3
            If (rblSendEmail.SelectedValue = "All") Then
                SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetNationalInviteesEmailList", dsParents, tblParents, prmArray)

            Else
                SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_2007NationalInvitees", dsParents, tblParents, prmArray)

            End If
           
            '   usp_2007NationalInvitees()
            If dsParents.Tables.Count > 0 Then
                For Each drParent As DataRow In dsParents.Tables(0).Rows
                    If drParent("email") <> "" Then
                        Dim objReg As Regex

                        re = File.OpenText(Server.MapPath(Application("ContestYear") & "NationalInvitation.htm"))
                        emailbody = re.ReadToEnd
                        re.Close()

                        If SendEmail(txtSubject.Text, emailbody, drParent("EMail").ToString) = False Then
                            strEmailFailure.Append("Email Not Send to:" & drParent("EMail") & ", " & drParent("Last_Name").ToString & ", " & drParent("First_Name").ToString & ", " & drParent("ChapterID").ToString & vbCrLf)
                        Else
                            Response.Write(drParent("email").ToString & "<BR>")
                        End If
                    Else
                        strInvalidEmails.Append("Invalid Email" & drParent("EMail") & ", " & drParent("Last_Name").ToString & ", " & drParent("First_Name").ToString & ", " & drParent("ChapterID").ToString & vbCrLf)
                    End If
                Next
            End If
            Response.Write("Email Invitation Sent to all the Contests successfully!.")
            Response.End()

            strInvalidEmails = Nothing
            strEmailFailure = Nothing
            strEmailDuplicate = Nothing

        End Sub
        Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, Optional ByVal sCC As String = "") As Boolean
            Dim sFrom As String = "nsfcontests@gmail.com"
            Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
            Dim client As New SmtpClient()
            Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            client.Host = host

            mail.IsBodyHtml = True
            Dim ok As Boolean = True
            Try
                client.Send(mail)
            Catch e As Exception
                ok = False
            End Try
            Return ok
        End Function

        
    End Class
End Namespace

