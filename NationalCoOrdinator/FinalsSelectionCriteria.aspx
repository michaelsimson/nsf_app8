<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="FinalsSelectionCriteria.aspx.vb" Inherits="VRegistration.FinalsSelectionCriteria" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
     <div style="text-align: center">  
			<table width="100%" align="center">
				<tr>
					<td class="Heading" align="right" colSpan="2">Finals invitation Criteria
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" style="HEIGHT: 20px" vAlign="top" noWrap align="right">Event 
						Year</td>
					<td style="HEIGHT: 20px" vAlign="top" noWrap align="left"><asp:dropdownlist id="ddlEventYear" runat="server" CssClass="SmallFont" AutoPostBack="True">
							<asp:ListItem>Select Event Year</asp:ListItem>
							<asp:ListItem Value="2005">2005</asp:ListItem>
							<asp:ListItem Value="2006">2006</asp:ListItem>
							<asp:ListItem Value="2007">2007</asp:ListItem>
							<asp:ListItem Value="2008">2008</asp:ListItem>
							<asp:ListItem Value="2009">2000</asp:ListItem>
							<asp:ListItem Value="2010">2010</asp:ListItem>
							<asp:ListItem Value="2011">2011</asp:ListItem>
							<asp:ListItem Value="2012">2012</asp:ListItem>
							<asp:ListItem Value="2013">2013</asp:ListItem>
							<asp:ListItem Value="2014">2014</asp:ListItem>
							<asp:ListItem Value="2015">2015</asp:ListItem>
							<asp:ListItem Value="2016">2016</asp:ListItem>
							<asp:ListItem Value="2017">2017</asp:ListItem>
							<asp:ListItem Value="2018">2018</asp:ListItem>
							<asp:ListItem Value="2019">2019</asp:ListItem>
							<asp:ListItem Value="2020">2020</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Contest&nbsp;Type:
					</td>
					<td noWrap align="left"><asp:radiobuttonlist id="rblContestType" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal">
							<asp:ListItem Value="1">National</asp:ListItem>
							<asp:ListItem Value="2">Chapter</asp:ListItem>
						</asp:radiobuttonlist><asp:requiredfieldvalidator id="rfvContestType" runat="server" CssClass="SmallFont" InitialValue="0" ErrorMessage="Contest Type Need to be Selected"
							ControlToValidate="rblContestType" Display="Dynamic"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" noWrap align="left">Contests:
					</td>
					<td noWrap align="left">
						<table>
							<tr>
								<td>
									<p>1. Spelling Bee</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Junior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblJSB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRBLJSB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblJSB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMarksGradeJSB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtJSB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtMarksGradeJSB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Senior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblSSB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRBLSSB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblSSB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMarksGradeSSB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvtxtSSB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtMarksGradeSSB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<p>2. Vocabulary Bee</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Junior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblJVB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvrblJVB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblJVB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMarksGradeJVB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtJVB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtMarksGradeJVB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Intermediate
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblIVB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvrblIVB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblIVB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMarksGradeIVB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtIVB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtMarksGradeIVB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Senior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblSVB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvrblSVB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblSVB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMarksGradeSVB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtSVB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtMarksGradeSVB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<p>3. Math Bee</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level 1
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblMB1" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvrblMB1" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblMB1" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMB1" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtMB1" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="rblMB1" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level 2
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblMB2" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvrblMB2" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblMB2" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMB2" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtMB2" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="rblMB2" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level 3
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblMB3" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvrblMB3" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblMB3" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMB3" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtMB3" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="rblMB3" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level 4
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblMB4" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblMB4" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblMB4" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtMB4" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtMB4" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="rblMB4" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<p>4. Geography Bee</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Junior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblJGB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblJGB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblJGB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtJGB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtJGB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtJGB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Senior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblSGB" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblSGB" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblSGB" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtSGB" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtSGB" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtSGB" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<p>5. Essay</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Junior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblEW1" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblEW1" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblEW1" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtEW1" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtEW1" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtEW1" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Intermediate
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblEW2" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblEW2" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblEW2" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtEW2" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtEW2" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtEW2" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Senior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblEW3" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblEW3" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblEW3" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtEW3" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtEW3" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtEW3" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<p>6. Public Speaking</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Junior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblPS1" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblPS1" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblPS1" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtPS1" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtPSJ" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtPS1" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Intermediate
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblPS2" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblPS2" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblPS2" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtPS2" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtPS2" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtPS3" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Senior
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblPS3" RepeatDirection="Horizontal" Runat="server">
													<asp:ListItem Value="0">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
												<asp:RequiredFieldValidator id="rfvRblPS3" runat="server" CssClass="SmallFont" ErrorMessage="Selection Required"
													ControlToValidate="rblPS3" Display="Dynamic"></asp:RequiredFieldValidator></td>
											<td width="20%"><asp:textbox id="txtPS3" runat="server" CssClass="SmallFont"></asp:textbox>
												<asp:RequiredFieldValidator id="rfvTxtPS3" runat="server" CssClass="SmallFont" ErrorMessage="Data Entry Required"
													ControlToValidate="txtPS3" Display="Dynamic"></asp:RequiredFieldValidator></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<p>7. Brain Bee</p>
								</td>
							</tr>
							<tr>
								<td>
									<table borderColorLight="#ff9966" border="1">
										<tr>
											<td width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brain Bee
											</td>
											<td width="50%"><asp:radiobuttonlist id="rblBB" RepeatDirection="Horizontal" Runat="server" Enabled="False">
													<asp:ListItem Value="0" Selected="True">Open</asp:ListItem>
													<asp:ListItem Value="M">Marks</asp:ListItem>
													<asp:ListItem Value="G">Grade</asp:ListItem>
												</asp:radiobuttonlist>
											</td>
											<td width="20%"><asp:textbox id="txtBB" runat="server" CssClass="SmallFont" Enabled="False"></asp:textbox></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="ItemCenter" noWrap colSpan="2"><asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Create "></asp:button></td>
				</tr>
			</table>
    </div>
    </asp:Content>
    
 

 
 
 