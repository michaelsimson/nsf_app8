﻿// JScript File

	//Generating Pop-up  Preview page
		function getPreview(Preview_area)
		{	
		//alert(document.getElementById("_ctl0_Content_main_Preview_area").innerHTML);
			//Creating new page
			var pp = window.open();
			//Adding HTML opening tag with <HEAD> … </HEAD> portion 
			pp.document.writeln('<HTML><HEAD><title>Email Preview</title><LINK href=../Styles.css  type="text/css" rel="stylesheet">')
			pp.document.writeln('<LINK href=PrintStyle.css  type="text/css" rel="stylesheet" media="print"><base target="_self"></HEAD>')
			//Adding Body Tag
			pp.document.writeln('<body MS_POSITIONING="GridLayout" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">');
			//Adding form Tag
			pp.document.writeln('<form  method="post">');
		  //Writing print area of the calling page
			pp.document.writeln(document.getElementById("_ctl0_Content_main_Preview_area").innerHTML);
			
			//Creating two buttons Print and Close within a table
			pp.document.writeln('<hr><TABLE width=100%><TR><TD></TD></TR><TR><TD align=center><INPUT ID="PRINT" type="button" value="Print" onclick="javascript:location.reload(true);window.print();"><INPUT ID="CLOSE" type="button" value="Close" onclick="window.close();"></TD></TR><TR><TD></TD></TR></TABLE>');
			
			//Ending Tag of </form>, </body> and </HTML>
			pp.document.writeln('</form></body></HTML>');			
			
		}		