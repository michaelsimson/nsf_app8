<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.reg_Success_donation" Trace="false" CodeFile="reg_success_donation.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<TR>
					<TD class="largewordingbold" style="height: 16px"><asp:label id="lblDonationMessage" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"><asp:label id="lblMessage" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"><asp:label id="lblEmailStatus" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"><FONT color="#ff0000" size="4">Please Do Not use Back 
							Button on Browser to avoid Duplicate Charges to your Credit Card.</FONT></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"></td>
				</tr>
				<tr>
				    <td>
				        Once again, we thank you for your contribution. 
                        <br />
                        <br />
                        North South Foundation
                        <br />
                        2 Marissa Ct, Burr Ridge, IL 60527
                        <br />
                        Tax-ID: 36-3659998
                        <br />
                        Non-profit Organization under the 501(c)(3) Code.<br />
                        <a href="http://www.northsouth.org">http://www.northsouth.org</a>

				    </td>
				</tr>
			</table>
			
			<table width="100%">
				<tr>
					<TD class="largewordingbold" align="center"><FONT color="#cc3300" size="4"><STRONG></STRONG></FONT></TD>
					<td class="Content">
					</td>
				</tr>
				<tr>
					<td>
						<asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Main Page</asp:hyperlink>
					</td>
				</tr>
			</table>
</asp:content>
		


 

 
 
 