﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.IO

Partial Class Don_athon_custom
    Inherits System.Web.UI.Page
    Public i As Integer = 0
    'usp_SelectWalkMarathon
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack = True Then
            ' ltlTitle.Text = "North South Foundation - Excel-a-Thon : Noble Cause Through Brilliant Minds!"
            Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            addMenuItemAt(0, "Home", homeURL)
            If (Session("LoggedIn") = "True") Then
                If Session("entryToken").ToString() = "Parent" Then
                    hlinkParentRegistration.Visible = True
                ElseIf Session("entryToken").ToString() = "Volunteer" Then
                    hlinkVolunteerFunctions.Visible = True
                ElseIf Session("entryToken").ToString() = "Donor" Then
                    hlinkDonorFunctions.Visible = True
                End If
                addLogoutMenuItem()
            Else
                If (loginURL <> Nothing And Session("entryToken") <> Nothing) Then
                    loginURL = loginURL + Session("entryToken").ToString().Substring(0, 1)
                    addMenuItem("Login", loginURL)
                End If
            End If
                If Request.QueryString("NSFDONATE") Is Nothing Then
                    Response.Redirect("/public/main/donate.aspx")
                End If

                Dim GetID As String = decode(Request.QueryString("NSFDONATE"))
                If IsNumeric(GetID) Then
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWalkMarathon", New SqlParameter("@WalkMarID", GetID))
                    ' MsgBox(ds.Tables(0).Rows.Count)
                    If ds.Tables(0).Rows.Count > 0 Then
                        HlblWalkMarathonID.Value = GetID
                        Session("EventID") = ds.Tables(0).Rows(0)("EventID").ToString()
                        hdnEventID.Value = ds.Tables(0).Rows(0)("EventID").ToString()
                        lblHeading.Text = ds.Tables(0).Rows(0)("EventName").ToString()
                        GetDonorDetails()
                        HlblWMmemberid.Value = Convert.ToInt32(ds.Tables(0).Rows(0)("MemberID").ToString())
                        ltlTitle.Text = "North South Foundation - " & ds.Tables(0).Rows(0)("HeaderText").ToString() & " " & ds.Tables(0).Rows(0)("WName").ToString() & "'s Fundraising page"
                        lblName.Text = ds.Tables(0).Rows(0)("WName").ToString()
                        If ds.Tables(0).Rows(0)("ChapterID") > 1 Then
                            lblChapter.Text = ds.Tables(0).Rows(0)("ChapterCode")
                            trChapt.Visible = True
                        End If
                        lblPageCaption.Text = ds.Tables(0).Rows(0)("HeaderText").ToString()
                        showimage(ds.Tables(0).Rows(0)("ImageFileName"))
                        'ltlEvents.Text = "Event Name : " & ds.Tables(0).Rows(0)("EventName").ToString() & "  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Event Starts date : "

                        ltlCustomMsg.Text = IIf(ds.Tables(0).Rows(0)("MemberContent").ToString().Length > 5, ds.Tables(0).Rows(0)("MemberContent").ToString(), "<p align='justify' style='font-family:Calibri; font-size:12px;'> Hello! I am excited to inform you that I have cleared the regional contest and made it to the national spelling bee organized by North South Foundation (NSF). Winners in the national round have been featured in the popular media and awarded scholarships.<br /><br />NSF was started over 22 years ago with the purpose of providing educational scholarships to needy children who display academic excellence. NSF funds these scholarships by raising donations in the US through spelling bees and direct donations. The NSF Scholarship program is designed to encourage excellence among the poor children who excel academically but need financial help to attend college. Each scholarship is $250 per student per year. NSF has distributed more than 4,500 scholarships to students who need financial support to pursue their quest for knowledge in engineering, medicine, polytechnic, science and other fields. The scholarship is an annual award and not a one-time payment. The student is eligible for the scholarship until graduation as long as he/she maintains high academic standards. The local chapters in various states of India invite applications from students, screen them and select the neediest students who eventually become NSF scholarship recipients. Scholarship amounts range from INR 5,000 to INR 12,000 per student per year.<br /><br />I have pledged to seek sponsorship to further the noble mission of this charity. My personal goal is to raise a minimum of $250 to provide scholarship for one student for one year. If you would please consider making a tax-deductible donation (Tax ID: 36-3659998) toward my goal of $250 it would be greatly appreciated. 100% of the money goes towards educational scholarships and any amount of money will help. Please help me make a difference in life of a deserving student who aspires to rise from poverty through education.<br /><br />If you have any questions about the contest, NSF or your donation, please feel free to contact our parents or visit the NSF website. Thank you for your kindness.</p>")
                        lblTargetamt.Text = FormatCurrency(Convert.ToDecimal(ds.Tables(0).Rows(0)("TargetAmount")))
                        lblRaisedamt.Text = FormatCurrency(ds.Tables(0).Rows(0)("PledgeAmount"))
                        If Convert.ToDecimal(ds.Tables(0).Rows(0)("PledgeAmount")) > 0 Then
                            i = (Convert.ToDecimal(ds.Tables(0).Rows(0)("PledgeAmount")) / Convert.ToDecimal(ds.Tables(0).Rows(0)("TargetAmount"))) * 100
                        Else
                            i = 0
                        End If
                        If Not ds.Tables(0).Rows(0)("DonateAThonCalID") Is DBNull.Value Then
                            getDoanteAThon(ds.Tables(0).Rows(0)("DonateAThonCalID"))

                        End If
                        If Session("EventID") = "5" Then
                            Literal1.Text = Literal1.Text.Replace("**##**", lblName.Text)
                            Literal1.Text = Literal1.Text & "<div align='left'><b>Message from " & lblName.Text & ": </b></div>"
                            divchldname.Visible = False
                            btnSponsor.Visible = False
                            divWalkathon.Visible = True
                            divbelow.Visible = False
                        End If

                    Else
                        Response.Redirect("/public/main/donate.aspx")
                    End If
                Else
                    Response.Redirect("/public/main/donate.aspx")
                End If
            End If
    End Sub

    Public Sub getDoanteAThon(ByVal DonateAThonCalID As String)
        Dim ds_WalkaThon As New DataSet
        Try
            ds_WalkaThon = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select D.DonateAThonID, D.EventID, D.EventYear, D.ClusterID, D.VenueAndTime, D.Description, CONVERT(VARCHAR(12),D.StartDate, 107) as StartDate, CONVERT(VARCHAR(12),D.Eventdate, 107) as EventDate,D.EndDate as CheckEDate,CONVERT(VARCHAR(12),D.EndDate, 107) as EndDate,C.ClusterCode from  DonateAThonCal D Inner Join Cluster C ON C.ClusterID= D.ClusterID where DonateAThonID = " & DonateAThonCalID & "")
            If ds_WalkaThon.Tables(0).Rows.Count > 0 Then
                If Session("EventID") = "5" Then
                    Literal1.Text = "<br><br><Table><tr><td align='center' colspan='2' style='font-family:Arial; font-size:16px;'><b>Welcome to the North South Foundation Walk-a-Thon Donations Page</b></td></tr>"
                    Literal1.Text = Literal1.Text & "<tr><td align='center' Colspan=2> <b> For **##**  </b></td></tr>"
                    Literal1.Text = Literal1.Text & "<tr><td align='left'> <b> Details of Walk-a-thon :</b> </td><td align='left'> " & ds_WalkaThon.Tables(0).Rows(0)("VenueAndTime").ToString() & "<br></td></tr>"
                    Literal1.Text = Literal1.Text & "</table><br><br>"
                End If
                If Convert.ToDateTime(ds_WalkaThon.Tables(0).Rows(0)("CheckEDate").ToString()) < Now.Date Then
                    lblErr.Text = "End date to Sponsor passed."
                    btnSponsor.Enabled = False
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
            'Response.Write("Select D.DonateAThonID, D.EventID, D.EventYear, D.ClusterID, D.VenueAndTime, D.Description, D.StartDate, D.EventDate,D.EndDate,C.ClusterCode from  DonateAThonCal D Inner Join Cluster C ON C.ClusterID= D.ClusterID where DonateAThonID = " & ddlWalkaThon.SelectedValue & "")
        End Try

    End Sub

    Function decode(ByVal inputText As String) As String
        Dim decodedBytes As Byte()
        decodedBytes = Convert.FromBase64String(inputText)
        Dim decodedText As String
        decodedText = Encoding.UTF8.GetString(decodedBytes)
        Return decodedText
    End Function

    Public Sub addMenuItem(ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addMenuItem(ByVal itemText As String, ByVal href As String)
        addMenuItem(New MenuItem("&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal itemText As String, ByVal href As String)
        addMenuItemAt(index, New MenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addLogoutMenuItem()
        addMenuItemAt(NavLinks.Items.Count, "Logout", "logout.aspx") 'add to the end
    End Sub

    Private Sub showimage(ByVal Imgname As String)
        'System .Drawing .Image.FromStream (
        Dim filepath As String
        Dim file As FileInfo
        filepath = Server.MapPath("walkmarathon/" & Imgname)
        file = New FileInfo(filepath)
        If file.Exists Then
            Dim fs As FileStream = New FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read)
            Dim Image As System.Drawing.Image = System.Drawing.Image.FromStream(fs)
            If Image.Width > 250 Then
                img1.Width = 250
            End If
            If Image.Height > 250 Then
                img1.Height = 250
            End If
            img1.ImageUrl = "~/walkmarathon/" & Imgname
        End If
    End Sub

    Private Sub GetDonorDetails()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select CASE WHEN Anonymous='Y' then 'Anonymous' else I.FirstName+' '+I.LastName END as Name,WM.CreateDate,CASE WHEN DonAnonymous='Y' then Null else WM.PledgeAmount End as PledgeAmount,WM.Comment from  WMSponsor WM Inner Join IndSpouse I ON I.AutoMemberID = WM.MemberID  where ((WM.PaymentMode ='Credit Card' and WM.PaymentReference is not null) OR (WM.PaymentMode <>'Credit Card')) AND WM.WalkMarID = " & HlblWalkMarathonID.Value & "")
        If ds.Tables(0).Rows.Count > 0 Then
            lblSponsor.Visible = True
            GVwList.DataSource = ds
            GVwList.DataBind()
        Else
            GVwList.Visible = False
            lblSponsor.Visible = False
        End If

    End Sub

    'Protected Sub RegType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Session("WMMemberID") = HlblWMmemberid.Value
    '    'If RegType.SelectedValue = 3 Then
    '    '    If Session("loginID") = Nothing Then
    '    '        Response.Redirect("login.aspx?entry=wd")
    '    '    Else
    '    '        Session("EventID") = 12
    '    '        Response.Redirect("Don_athonDonorDetails.aspx?id=3")
    '    '    End If
    '    'Else

    '    'End If
    'End Sub

    Protected Sub btnSponsor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSponsor.Click
        If Not Request.QueryString("MId") Is Nothing Then
            Session("MId") = decode(Request.QueryString("MId"))
        End If
        If Session("EventID") Is Nothing Then Session("EventID") = hdnEventID.Value
        Session("WMMemberID") = HlblWMmemberid.Value
        Session("WalkMarathonID") = HlblWalkMarathonID.Value
        Response.Redirect("Don_athonDonorDetails.aspx")
    End Sub
End Class
