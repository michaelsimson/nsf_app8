﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master"  %>
<%@ Import NameSpace="Microsoft.ApplicationBlocks.Data" %>

<script runat="server">
    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <p align="center" class="pageHeading">
      <SPAN class="title02">About NSF Online Games </span>
      </p>
        
      
      <div align="left" class="title04">Spelling Bee </div>
      <div align="left" class="style11">
        <ul>
	        <li><A class="btn_02" href="#intro">Introduction to Spelling Bee Game </A></li>
			<li><A class="btn_02" href="#access">How do I get access?</A></li>
			<li><A class="btn_02" href="#faq">FAQ  </A></li>
	        <li><A class="btn_02" href="#instr">Instructions for Online Spelling Game </A></li>
         </ul>
      </div>
      <hr align="center" color="#0099FF" width="100%"> </hr>
      
      
      <div align="left" class="title04">Vocabulary Bee</div>
      <div align="left" class="style11">
		<ul>
			<li><A class="btn_02" href="#introvocab">Introduction to Vocabulary Bee Gam </A></li>
			<li><A class="btn_02" href="#access">How do I get access?</A></li>
			<li><A class="btn_02" href="#faq">FAQ </A></li>
	        <li><A class="btn_02" href="#instrvocab">Instructions Online Vocabulary Game</A></li>

		</ul>
      </div>     
        
      <hr align="center" color="#0099FF" width="100%"> </hr>
      <div align="center" class="title04"><A name="intro">Introduction to the Spelling Bee Game</A> </div>
      <div align="left" class="style11">
      <p  class="txt01" align="Justify">
     	As per the old adage,  "Practice makes one perfect". <br> 
		Practice words are accessible on the site.  To learn them, you have to consult a dictionary.  
		Further, in order to test your spelling skill, you have to silently spell each word to yourself or spell 
		in front of your parents.  Most parents print the words and ask their children to practice.  
		While many children find it exciting initially, they pretty soon find it laborious and monotonous.  
		Many continue to practice them grudgingly under pressure from their parents. 
		
      </p>
      <p  class="txt01"  align="Justify">
        To overcome this barrier, North South Foundation has developed an Online Spelling Bee Word Game to improve 
		English skills of children of the Indian American community.  This is an interactive game providing a 
		challenging and rewarding experience.  The objective is to make it a fun in learning.  

      </p>
     <p  class="txt01"  align="Justify"> 
      <div align="left"><b>The game has the following unique features: </b></div>
      <div align="left"  class="txt01">
        <ol>
	        <li>It emulates the actual spelling bee environment:  computer pronounces a word;
	         you can ask it to repeat the word; alternatively, you can ask for a definition, root, or sentence.</li>
	         
			<li>It allows you to choose words among six categories ranging from beginners to advanced.</li>
			
			<li>Words are given randomly so that you don&#8217;t get the same sequence of words in each session. 
			 This will eliminate monotony and boredom.</li>
			 
	        <li>As you learn words, the computer will suppress them so that you will not be annoyed with 
	        	the words you already know.</li>
	        	
	        <li>You will see your scores and the improvement you make over time. 
	            As a result, you will gain more confidence in your own capabilities. </li>
         </ol>
         
      </div>
      
     </p>
     <hr align="center" color="#0099FF" width="100%"> </hr>
      
      <div align="center" class="title04"><A name="access">How do I get access to the game?</A></div>
      <div align="left" class="txt01">
        
           <p  class="txt01"  align="Justify">Please read each step very carefully to ensure that getting access to the game is smooth and not onerous.<br> 
        <ol>
	        <li>If you make a tax-deductible donation of $50 or more, you are eligible to access the game.  Please note that the registration
	          fees for contests do not provide access to the game.

                      <li>If your child participates in the NSF Dollar-a-Square (DAS) program and collects $100 or more, you are also
			 eligible to use the game.

	          <li>Once the payment is made, log in as a parent and then click on Register for Game on the Parent
	          Functions Page.  
                       <ul>  <p  class="txt01"> a) First select the child and then the game you want.  Press Continue.<br>
                                  <p  class="txt01"> b) Next click on New/Renew Access to Game.  You will now see a popup screen where you can enter loginID and password, 
                                          which your child needs, to access the game.  The loginID has to be unique in our system.  Keep loginID and password in a safe place.  <br>
                                    <p  class="txt01"> c) After finishing data entry,  click on Save button. You will receive an email confirmation stating that you have successfully registered your                                        child.  An email will also go to the customer service volunteers to activate your access to the game.  This can take a day or two to take effect. <br>                                  </p>
                                   </ul>

                     <li>Please note that the $50 donation provides access
	          to one game for one child for one year.  Each child should have separate access, since each child is 
	          tracked separately for his/her performance.

                       <li>Renewals can be requested by the same process by clicking on Register for Game.  If there are 
                        any problems with registration or game access, please send an email to 
	          nsfgame@gmail.com with detailed explanation of the problem you encountered.
	         
			<li>If the suggested tax-deductible donation is a hardship for you, please send a detailed explanation of your 
			hardship to nsfgame@gmail.com. You might be eligible for getting access to the game for free or at a lower price. </li>
			
					 	        	        
         </ol>
        </div>
        <hr align="center" color="#0099FF" width="100%"> </hr>
      
        
        <div align="center" class="title04"><A name="faq">FAQ &#8211; Online Games</A> </div>

        <p>
        <div align="left"><b>Can I see a demo version before I sign up? </b> </div>
        <div align="left" class="style11">
	        Yes, you can see a demo version of the game on the game login page.          
        </div>
        </p>
        <p>
        <div align="left" ><b>What do I need on my computer to use the game?</b></div>
        <div align="left" class="style11">
	        You need a good set of speakers and recent versions of Real Player & Internet Explorer.
		          We did not test the software with other browsers like FireFox.
         
        </div>
        </p>
        <p>
        <div align="left"><b>I can't hear the words?</b> </div>
        <div align="left" class="style11">
	        
		        Make certain that your speakers are turned on and the volume is properly set. 
		        We have done our best to get audio files for most of the words. If you have problem 
		        with a specific word, submit your feedback using Comments button. We will take your 
		        feedback and take appropriate action to improve the game.   
		   
         
        </div>
        
        </p>
        <p>
        <div align="left"><b>Do I get the same words I use in Game in the contests conducted by NSF? 
        Are all the practice words there in the game? </b></div>
        <div align="left" class="style11">
	        
		        Most of the practice words are available in the game. Further, we have added a lot more to enhance your vocabulary. This is 
		        a learning tool to improve your English Language skills irrespective of whether you participate in the contests.      
		    
         
        </div>
</p>
<hr align="center" color="#0099FF" width="100%"> </hr>
      
	<div align="center" class="title04"><A name="instr"> Instructions for Playing Spelling Bee Game <A> </div>
	<div align="left" class="style11">
	        <ol>
		        <li> Go to the home page:  www.northsouth.org</li>
				<li>Click on Play Online Games under the Quick Links under Kids Corner</li>
				<li>Enter Login id</li>
				<li> Enter Password </li>
				<li> Now you will get a Menu. Select the appropriate category you want to play.</li>
				<li> After you hear the word, you can type the spelling or ask for more information (Definition, Sentence,
				 Language Origin or Parts of Speech.)</li>
				<li> Once you type the word, click Submit Word.</li>
				<li> The result will be shown on the next screen.</li>  
				<li> To move on, click Next Word or if you are done, click End Session. </li>
				</ol>
         
	</div>
	
	<div align="left" ><b> Do&#8217;s and Don&#8217;ts </b></div>
	<div align="left" class="style11">
		 <ul>
		        <li>DO NOT use your browser's BACK and FORWARD buttons to navigate. You may get wrong results. </li>
		
				<li>Your results for a session will not be saved if you attempt less than 10 words. 
				 You can always try more words (no maximum): 20, 40, 50, 100 or more! </li> 
		
				<li>To End a Game click End Session button, otherwise your results for this session will be lost. 
					This button will appear once you spell a word. Once you finish a session, you can start another session or quit. </li>
	</ul>
	</div>
 <br>
 
 <div align="left" ><b>  Problems </b></div>

	<div align="left" class="style11">If you couldn&#8217;t make out a word the first time, you can click on Repeat Word, 
	which can usually take a few seconds to replay the word.
	If the word is hard to hear, you can click Comments/Feedback and report the problem. 
	</div>

      <br>
      
      
      <hr align="center" color="#0099FF" width="100%"> </hr>
      <div align="center" class="title04"><A name="introvocab">Introduction to the Vocabulary Bee Game</A> </div>
      <div align="left" class="txt01">
      <p  class="txt01"  align="Justify">
     	As per the old adage,  "Practice makes one perfect". <br> 
		English vocabulary is vast.  Merriam-Webster&#8217;s Dictionary boasts 450,000 words.  Nobody can learn vocabulary overnight. 
		 It takes years of learning and practice.  Context and parts of speech are extremely important in English.  
		 Many words are unique in the English language, and even a good synonym will not convey the same meaning. 
		  For example, even simple words like simple, lucid, elicit, corral and dormant do not possess good replacements in 
		  certain contexts.  Most of the Indian Americans who were born and educated in India have limited vocabulary,
		   because English is not their native language. Naturally, it is extremely hard for them to improve the vocabulary 
		   of their children. Thus special effort is required on the part of their children to overcome this limitation. 
      </p>
      <p  class="txt01"  align="Justify">
        Practice words are accessible on the site.  To learn them, you have to consult a dictionary. 
         Further, in order to test your vocabulary skill, you have to recite the meaning to yourself 
         and compare to the one in the dictionary.  Most parents print the words and ask their children to practice.  
         While many children find it exciting initially, they pretty soon find it laborious and monotonous.  
         Many continue to practice them grudgingly under pressure from their parents.   

      </p>
      
      <p class="txt01">
        To overcome this barrier, North South Foundation has developed an Online Vocabulary Game to improve English vocabulary skills of children of the Indian American community. 
         This is an interactive game providing a challenging and rewarding experience.  The objective is to make it a fun in learning.   

      </p>

     <p> 
      <div align="left"><b>The game has the following unique features: </b></div>
      <div align="left" class="style11">
        <ol>
	        <li>It emulates the actual vocabulary contest environment:  
	        computer gives you a word along with multiple meanings;
	         you are asked to choose the one that is closest in meaning. </li>
	         
			<li>It allows you to choose words among three categories ranging from beginners to advanced.</li>
			
			<li>Words are given randomly so that you don&#8217;t get the same sequence of words in each session.
			  This will eliminate monotony and boredom. </li>
			 
	        <li>)  As you learn words, the computer will suppress them so that you will not be annoyed with the words you already know. </li>
	        	
	        <li>YYou will see your scores and the improvement you make over time. As a result, you will gain more confidence in your own capabilities. . </li>
         </ol>
         
      </div>
      
     </p>
     <hr align="center" color="#0099FF" width="100%"> </hr>
      
	<div align="center" class="title04"><A name="instrvocab"> Instructions for Playing Vocabulary Bee Game <A> </div>
	<div align="left" class="style11">
	        <ol>
		        <li> Go to the home page:  www.northsouth.org</li>
				<li>Click on Play Online Games under the Quick Links under Kids Corner</li>
				<li>Enter Login id</li>
				<li> Enter Password </li>
				<li> Now you will get a Menu. Select the appropriate category you want to play.</li>
				<li> You will get a word and a number of choices.  You need to check the one choice that has the closest fit to the word given</li>
				<li> Once you make your choice, click Submit Word.</li>
				<li> The result will be shown on the next screen.  You will also see the meaning of the word.  
				It is a good practice to study the meaning and make up a sentence in your own words to solidify your understanding of the meaning. </li>  
				<li> To move on, click Next Word or if you are done, click End Session. </li>
				</ol>
         
	</div>
	
	<div align="left" ><b> Do&#8217;s and Don&#8217;ts </b></div>
	<div align="left" class="style11">
		 <ul>
		        <li>DO NOT use your browser's BACK and FORWARD buttons to navigate. You may get wrong results. </li>
		
				<li>Your results for a session will not be saved if you attempt less than 10 words. 
				 You can always try more words (no maximum): 20, 40, 50, 100 or more! </li> 
		
				<li>To End a Game click End Session button, otherwise your results for this session will be lost. 
					This button will appear once you spell a word. Once you finish a session, you can start another session or quit. </li>
	</ul>
	</div>
 <br>
 
 <div align="left" ><b>  Problems </b></div>

	<div align="left" class="style11">If you couldn&#8217;t make out a word the first time, you can click on Repeat Word, 
	which can usually take a few seconds to replay the word.
	If the word is hard to hear, you can click Comments/Feedback and report the problem. 
	</div>

      <br>
      
</asp:Content>

