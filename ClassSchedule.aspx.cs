﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using VRegistration;


public partial class ClassSchedule : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //Session["LoginID"] = 22214;
                //Session["RoleId"] = 1;
                if (Session["LoginID"] == null)
                {
                    Response.Redirect("~/Maintest.aspx");
                }

                if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
                {
                    Response.Redirect("~/login.aspx?entry=p");
                }
                if (Convert.ToInt32(Session["RoleId"]) != 1)
                {
                    Response.Redirect("~/VolunteerFunctions.aspx");
                }

                int MaxYear = DateTime.Now.Year;
                //ArrayList list = new ArrayList();

                for (int i = MaxYear; i >= (DateTime.Now.Year - 3); i--)
                {
                    ddlYear.Items.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString(), i.ToString()));
                }
                ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

                ddlEvent.Items.Add(new ListItem("Coaching", "13"));
                loadPhase(ddlPhase);
                FillProductGroup();
                FillProduct();

                //fillddlphase();
                //fillddlCoach();
            }



        }
        catch (Exception ex)
        {
            //Response.Write("Err :" + ex.ToString());
        }
    }
    private void loadPhase(DropDownList ddlObject)
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue);
    }
    private void FillCoach()
    {
        //ddlProductGroup
        string ddlCoachqry;
        try
        {
            if (Convert.ToInt32(Session["RoleId"]) == 89)
            {
                ddlCoachqry = "select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from  indspouse I inner join CalSignup c on I.AutoMemberId=c.MemberId where c.EventYear='" + ddlYear.SelectedItem.Value + "' and c.ProductGroupID='" + ddlProductGroup.SelectedItem.Value + "' and c.ProductID='" + ddlProduct.SelectedItem.Value + "' and c.Semester='" + ddlPhase.SelectedItem.Text + "' and I.AutoMemberID=" + Session["LoginID"] + " and c.EventID=13";
            }
            else
            {
                ddlCoachqry = "select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from  indspouse I inner join CalSignup c on I.AutoMemberId=c.MemberId where c.EventYear='" + ddlYear.SelectedItem.Value + "' and c.ProductGroupID='" + ddlProductGroup.SelectedItem.Value + "' and c.ProductID='" + ddlProduct.SelectedItem.Value + "' and c.Semester='" + ddlPhase.SelectedItem.Text + "' and c.EventID=13";
            }
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlCoachqry);
            //DataSet myDataSet = new DataSet();
            ddlCoach.DataSource = dsstate;
            //ddlCoach.DataTextField = "CoachName";
            //ddlCoach.DataValueField = "MemberId";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select Coach", "-1"));
        }
        catch (Exception e)
        { }
    }
    private void FillPhase()
    {
        ddlPhase.Items.Clear();
        //ddlPhase.Items.Insert(0, new ListItem("Select Semester", "-1"));
        for (int i = 1; i < 5; i++)
        {
            // ddlPhase.Items.Insert(i - 1, i.ToString());
        }

    }

    private void FillProduct()
    {
        string ddlproductqry;
        try
        {
            if (Convert.ToInt32(Session["RoleId"]) == 89)
            {
                ddlproductqry = "select distinct p.Name,v.ProductID,v.ProductCode from CalSignup c inner join volunteer v on c.ProductID=v.ProductID inner join Product p on v.ProductID=p.ProductID where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and C.Semester='" + ddlPhase.SelectedValue + "' and v.ProductId is not Null  ";
                if (ddlProductGroup.SelectedValue != "-1")
                {
                    ddlproductqry += "  and v.ProductGroupId=" + ddlProductGroup.SelectedValue + " ";
                }
                ddlproductqry += " Order by v.ProductId";
            }
            else
            {
                ddlproductqry = "select  distinct c.ProductCode,c.ProductID,p.Name from Calsignup c inner join Product p on c.ProductID=p.ProductId where c.EventYear=" + ddlYear.SelectedValue + " and c.EventId=13 and C.Semester='" + ddlPhase.SelectedValue + "' and c.ProductId is not Null ";
                if (ddlProductGroup.SelectedValue != "-1")
                {
                    ddlproductqry += "  and c.ProductGroupId=" + ddlProductGroup.SelectedValue + " ";
                }
                ddlproductqry += " Order by c.ProductId";
            }
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
            DataSet myDataSet = new DataSet();
            ddlProduct.DataSource = dsstate;
            //ddlProduct.DataTextField = "Name";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));


        }
        catch (Exception e)
        { }
    }
    private void FillProductGroup()
    {
        //ddlProductGroup
        string ddlproductgroupqry;

        if (Convert.ToInt32(Session["RoleId"]) == 89)
        {
            ddlproductgroupqry = "select distinct p.Name,v.ProductGroupID,v.ProductGroupCode from CalSignup c inner join volunteer v on c.ProductGroupID=v.ProductGroupID inner join ProductGroup p on v.ProductGroupID=p.ProductGroupID where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and C.Semester='" + ddlPhase.SelectedValue + "' and v.ProductId is not Null  Order by v.ProductGroupId";
        }
        else
        {
            ddlproductgroupqry = "select  distinct c.ProductGroupCode,c.ProductGroupID,p.Name from Calsignup c inner join ProductGroup p on c.ProductGroupID=p.ProductGroupId where c.EventYear=" + ddlYear.SelectedValue + " and c.Semester='" + ddlPhase.SelectedValue + "' and c.EventId=" + ddlEvent.SelectedValue + " and c.ProductId is not Null Order by c.ProductGroupId";
            // ddlproductgroupqry = "select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" + ddlEvent.SelectedValue + " and ProductId is not Null Order by ProductGroupId";
        }
        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductgroupqry);

        ddlProductGroup.DataSource = dsstate;
        //ddlProductGroup.DataTextField = "Name";
        ddlProductGroup.DataValueField = "ProductGroupID";
        ddlProductGroup.DataBind();
        ddlProductGroup.Items.Insert(0, new ListItem("Select ProductGroup", "-1"));



    }

    protected void rdoCalendar_SelectedIndexChanged(object sender, EventArgs e)
    {
        //ddlProductGroup.SelectedIndex = 0;
        //ddlProduct.SelectedIndex = -1;
        //ddlPhase.SelectedIndex = -1;
        //ddlCoach.SelectedIndex = -1;

        if (rdoCalendar.SelectedIndex == 0)
        {

            lblClassSchedule.Text = "Close a Current Class";
            tblCoach.Visible = true;

            lblsuccTable.Text = "";
            //GridView1.Visible = true;

            tdCoach.Visible = true;
            ddlCoach.Visible = true;
            lblErr.Text = "";

        }
        else if (rdoCalendar.SelectedIndex == 1)
        {
            lblClassSchedule.Text = "Open a Closed Class";
            tblCoach.Visible = true;

            lblsuccTable.Text = "";

            tdCoach.Visible = false;
            ddlCoach.Visible = false;
            lblErr.Text = "";

        }
        else
        {
            lblClassSchedule.Text = "Open a New Class";
            tblCoach.Visible = true;

            lblsuccTable.Text = "";

            tdCoach.Visible = true;
            ddlCoach.Visible = true;
            lblErr.Text = "";

        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillProduct();

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblErr.Text = "";
        string strSql = "select productgroupid from product where productid=" + ddlProduct.SelectedValue + "";
        int PgId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, strSql));
        ddlProductGroup.SelectedValue = Convert.ToString(PgId);
        FillCoach();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        fillGridviewCoachClassTable();
    }
    private void fillGridviewCoachClassTable()
    {
        string str;
        try
        {
            lblTableName2.Text = "";
            GVScheduledClasses.Visible = false;
            if (ddlProductGroup.SelectedIndex == 0)
            {
                lblErr.Text = "Select ProductGroup";
                lblTableName.Text = "";
                GridViewCoachClassTable.Visible = false;

            }
            else if (ddlProduct.SelectedIndex == 0)
            {
                lblErr.Text = "Select Product";
                lblTableName.Text = "";
                GridViewCoachClassTable.Visible = false;
            }
            else
            {
                lblErr.Text = "";
                lblErrTable2.Text = "";
                hyear.Value = ddlYear.SelectedItem.Value;
                hproductgroupname.Value = ddlProductGroup.SelectedItem.Value;
                hproductname.Value = ddlProduct.SelectedItem.Value;
                hphase.Value = ddlPhase.SelectedItem.Value;
                if (rdoCalendar.SelectedIndex == 0)
                {
                    GVScheduledClasses.Visible = false;
                    if (ddlCoach.SelectedIndex == 0)
                    {
                        lblErr.Text = "Select Coach Name";
                        GridViewCoachClassTable.Visible = false;
                        lblTableName.Text = "";
                    }
                    else
                    {

                        str = "select CONVERT(VARCHAR(5), Time, 108) as Time,VRoom,convert(varchar(5),[Begin],108) as [Begin],convert(varchar(5),[End],108) as [End],MemberID,'Name' as Name,SignupID,EventYear,EventCode,ProductGroupCode,ProductCode,Semester,Level,SessionNo,Accepted,substring(Day,1,3) as Days,Day,StartDate,EndDate,UserID,PWD,UpFlag from CalSignUp where EventID=13 and ProductGroupID='" + ddlProductGroup.SelectedItem.Value + "' and ProductID='" + ddlProduct.SelectedItem.Value + "' and EventYear='" + ddlYear.SelectedItem.Value + "' and MemberID=" + ddlCoach.SelectedItem.Value + " and Semester='" + hphase.Value + "' and Accepted='Y' and (UpFlag is null or UpFlag='Open') ";
                        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, str);
                        if (dsstate.Tables[0].Rows.Count > 0)
                        {
                            //lblsuccTable.Text = "";
                            lblTableName.Text = "Table 1: Classes Conducted By Coach";
                            GridViewCoachClassTable.DataSource = dsstate.Tables[0];
                            GridViewCoachClassTable.DataBind();
                            //actionName.Text = "Close";
                            lblErrTable.Text = "";
                            GridViewCoachClassTable.Visible = true;
                        }
                        else
                        {
                            // lblsuccTable.Text = "";
                            lblErrTable.Text = "No Record Exists!";
                            GridViewCoachClassTable.Visible = false;
                        }
                    }
                }
                else if (rdoCalendar.SelectedIndex == 1)
                {
                    GVScheduledClasses.Visible = false;
                    str = "select CONVERT(VARCHAR(5), Time, 108) as Time,VRoom,convert(varchar(5),[Begin],108) as [Begin],convert(varchar(5),[End],108) as [End],MemberID,(select Firstname + ' '+LastName from Indspouse I1 where I1.AutoMemberID=c.MemberId ) as Name,SignupID,EventYear,EventCode,ProductGroupCode,ProductCode,Semester,Level,SessionNo,Accepted,substring(Day,1,3) as Days,Day,StartDate,EndDate,UserID,PWD,UpFlag from CalSignUp c where EventID=13 and ProductGroupID='" + ddlProductGroup.SelectedItem.Value + "' and ProductID='" + ddlProduct.SelectedItem.Value + "' and EventYear='" + ddlYear.SelectedItem.Value + "' and Semester='" + hphase.Value + "' and Accepted='Y' and UpFlag='Closed' ";
                    DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, str);
                    if (dsstate.Tables[0].Rows.Count > 0)
                    {
                        //lblsuccTable.Text = "";
                        lblTableName.Text = "Table 1: Classes Conducted By Coach";
                        GridViewCoachClassTable.DataSource = dsstate.Tables[0];
                        GridViewCoachClassTable.DataBind();
                        lblErrTable.Text = "";
                        GridViewCoachClassTable.Visible = true;
                    }
                    else
                    {
                        // lblsuccTable.Text = "";
                        lblErrTable.Text = "No Record Exists!";
                        GridViewCoachClassTable.Visible = false;
                    }
                }
                else
                {
                    if (ddlCoach.SelectedIndex == 0)
                    {
                        lblErr.Text = "Select Coach Name";
                        GridViewCoachClassTable.Visible = false;
                        lblTableName.Text = "";
                    }
                    else
                    {
                        str = "select CONVERT(VARCHAR(5), Time, 108) as Time,VRoom,convert(varchar(5),[Begin],108) as [Begin],convert(varchar(5),[End],108) as [End],MemberID,(select Firstname + ' '+LastName from Indspouse I1 where I1.AutoMemberID=c.MemberId ) as Name,SignupID,EventYear,EventCode,ProductGroupCode,ProductCode,Semester,Level,SessionNo,Accepted,substring(Day,1,3) as Days,Day,StartDate,EndDate,UserID,PWD,UpFlag from CalSignUp c where EventID=13 and ProductGroupID='" + ddlProductGroup.SelectedItem.Value + "' and ProductID='" + ddlProduct.SelectedItem.Value + "' and EventYear='" + ddlYear.SelectedItem.Value + "' and MemberID=" + ddlCoach.SelectedItem.Value + " and Accepted='Y' and Semester='" + hphase.Value + "' ";
                        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, str);
                        if (dsstate.Tables[0].Rows.Count > 0)
                        {
                            //lblsuccTable.Text = "";
                            lblTableName.Text = "Table 1: Classes Signedup By Coach";
                            GridViewCoachClassTable.DataSource = dsstate.Tables[0];
                            GridViewCoachClassTable.DataBind();
                            lblErrTable.Text = "";
                            GridViewCoachClassTable.Visible = true;
                        }
                        else
                        {
                            // lblsuccTable.Text = "";
                            lblErrTable.Text = "No Record Exists!";
                            GridViewCoachClassTable.Visible = false;
                        }
                    }
                }



            }
        }
        catch (Exception ex) { }
    }

    protected void GridViewCoachClassTable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        lblErr.Text = "";
        GridViewCoachClassTable.PageIndex = e.NewPageIndex;
        fillGridviewCoachClassTable();

    }
    protected void GridViewCoachClassTable_RowEditing(object sender, GridViewEditEventArgs e)
    {




    }

    protected void GridViewCoachClassTable_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            string id;
            string hlevel = string.Empty;
            string hsessionno = string.Empty;
            string hmemberid = string.Empty;
            string hday = string.Empty;
            //string hsdate = string.Empty;
            //string hedate = string.Empty;
            string hstarttime = string.Empty;
            string hendtime = string.Empty;
            HiddenField hfId = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("SignupID") as HiddenField);
            id = hfId.Value;

            //HiddenField hfsessionno = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("SessionNo") as HiddenField);
            //hsessionno = hfsessionno.Value;

            //HiddenField hflevel = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("Level") as HiddenField);
            //hlevel = hflevel.Value;

            Label hslabel = (Label)(GridViewCoachClassTable.Rows[e.RowIndex].Cells[7].FindControl("lblSession") as Label);
            hsessionno = hslabel.Text;

            Label hllabel = (Label)(GridViewCoachClassTable.Rows[e.RowIndex].Cells[6].FindControl("lblLevel") as Label);
            hlevel = hllabel.Text;
            Label hdaylabel = (Label)(GridViewCoachClassTable.Rows[e.RowIndex].Cells[11].FindControl("lblDay") as Label);
            hday = hdaylabel.Text;

            HiddenField hfstarttime = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("StartTime") as HiddenField);
            hstarttime = hfstarttime.Value;
            HiddenField hfendtime = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("EndTime") as HiddenField);
            hendtime = hfendtime.Value;
            //Label hstarttimelabel = (Label)(GridViewCoachClassTable.Rows[e.RowIndex].Cells[12].FindControl("lblsdate") as Label);
            //hsdate = hstarttimelabel.Text;

            //Label hendtimelabel = (Label)(GridViewCoachClassTable.Rows[e.RowIndex].Cells[13].FindControl("lbledate") as Label);
            //hedate = hendtimelabel.Text;


            HiddenField hfmemberid = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("MemberID") as HiddenField);
            hmemberid = hfmemberid.Value;
            Button buttonText = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("btnAction") as Button);
            string str;
            str = "Select count(*) from CoachReg where EventYear=" + hyear.Value + " and EventID=13 and ProductGroupID=" + hproductgroupname.Value + " and ProductID=" + hproductname.Value + " and Semester = '" + hphase.Value + "' and Level='" + hlevel + "' and  SessionNo=" + hsessionno + " and PMemberID=" + hmemberid + " and Approved='Y'";
            //DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, str);
            int iCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, str));

            if (buttonText.Text == "Select")
            {


                //fill Table 2:

                fillTableGVScheduledClasses(id, hday, hstarttime, hendtime);


            }
            else
            {
                if (iCount > 0)
                {
                    lblErr.Text = "This class cannot be closed since there are children registered in it";
                }
                else
                {
                    //Response.Write("<script>ConfirmToUpdate();</script>");
                    //Page.RegisterStartupScript("myScript", "<script language=JavaScript>  ConfirmToUpdate();</script>");
                    //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Confirm", "<script language=JavaScript>  ConfirmToUpdate();</script>");
                    string strupdateqry = string.Empty;

                    if (buttonText.Text == "Close")
                    {


                        strupdateqry = "update CalSignUp set UpFlag='Closed',modifieddate=Getdate(),modifiedby=" + Session["loginID"] + " where SignupID=" + hfId.Value + "";
                        DataSet dsupdate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strupdateqry);
                        lblsuccTable.Text = "Updated Successfully";
                    }
                    else if (buttonText.Text == "Open")
                    {
                        strupdateqry = "update CalSignUp set UpFlag='Open',modifieddate=Getdate(),modifiedby=" + Session["loginID"] + " where SignupID=" + hfId.Value + "";
                        DataSet dsupdate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strupdateqry);
                        lblsuccTable.Text = "Updated Successfully";
                    }




                    fillGridviewCoachClassTable();
                }
            }


        }
        catch (Exception ex)
        { }

    }
    private void fillTableGVScheduledClasses(string id, string day, string starttime, string endtime)
    {

        string strfillquery = string.Empty;
        lblTableName2.Text = "Table 2:  Scheduled Classes ";
        strfillquery = "select CONVERT(VARCHAR(5), Time, 108) as Time,VRoom,convert(varchar(5),[Begin],108) as [Begin],convert(varchar(5),[End],108) as [End],MemberID,(select Firstname + ' '+LastName from Indspouse I1 where I1.AutoMemberID=c.MemberId ) as Name,SignupID,EventYear,EventCode,ProductGroupCode,ProductCode,Semester,Level,SessionNo,Accepted,substring(Day,1,3) as Days,Day,StartDate,EndDate,UserID,PWD,UpFlag from CalSignUp c where EventYear=" + hyear.Value + " and Day='" + day + "' and Accepted='Y' and ([Begin]>=(DATEADD(hour, -2, CONVERT(time,'" + starttime + "',114))) and [End]<=(DATEADD(hour, 2, CONVERT(time,'" + endtime + "',114)))) and SignupID<>" + id + "  Order by  [Begin],VRoom,UserID ";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strfillquery);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            //lblsuccTable.Text = "";
            lblErrTable2.Text = "";
            GVScheduledClasses.DataSource = dsstate.Tables[0];
            GVScheduledClasses.DataBind();
            //actionName.Text = "Close";
            lblErrTable.Text = "";

            GVScheduledClasses.Visible = true;
        }
        else
        {
            // lblsuccTable.Text = "";
            lblErrTable2.Text = "No Record Exists!";
            GVScheduledClasses.Visible = false;
        }
    }
    protected void Button3_Click(object sender, EventArgs e)
    {

    }
    protected void GridViewCoachClassTable_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
        {
            if (rdoCalendar.SelectedIndex == 0)
            {
                e.Row.Cells[3].Visible = false;
                e.Row.Cells[4].Visible = false;
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button ActionButton = (e.Row.FindControl("btnAction") as Button);

            if (rdoCalendar.SelectedIndex == 0)
            {
                ActionButton.Text = "Close";

            }
            else if (rdoCalendar.SelectedIndex == 1)
            {
                ActionButton.Text = "Open";
            }
            else
            {
                ActionButton.Text = "Select";
            }
        }

        // If e.Row.RowType = DataControlRowType.DataRow Then
        //     Button ActionButton = (GridViewCoachClassTable.Rows[e.RowIndex].Cells[0].FindControl("btnAction") as Button);
        //ActionButton.Text = "Close";
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillProductGroup();
        FillProduct();
    }
    //protected void GVScheduledClasses_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GVScheduledClasses.PageIndex = e.NewPageIndex;
    //    fillTableGVScheduledClasses();
    //}
    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillProductGroup();
        FillProduct();
    }
}