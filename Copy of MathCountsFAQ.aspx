﻿<!--#include file="../uscontests_header.aspx"-->
<script type="text/javascript">
    document.getElementById("dvMathsCoaching").style.display = "block";

</script>
<div class="title02">NSF Coaching : FAQ – Mathcounts</div>

<div align="justify" class="txt01">

    <div class="title02">MATHCOUNTS Competition:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q1">1. What is MATHCOUNTS?</a></li>
        <li><a class="btn_02" href="#Q2">2.	How can my child participate in MATHCOUNTS competition?</a></li>
        <li><a class="btn_02" href="#Q3">3.	My child's school does not participate in MATHCOUNTS, what can be done about it?</a></li>
        <li><a class="btn_02" href="#Q4">4.	My child is home schooled, can he participate in MATHCOUNTS?</a></li>

    </ul>
    <div class="title02">NSF MathCounts Coaching:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q5">5. How does the online coaching work?</a></li>
        <li><a class="btn_02" href="#Q6">6.	What materials do we need to have in order to attend the online coaching?</a></li>
        <li><a class="btn_02" href="#Q7">7.	Is it mandatory for my child to participate in MATHCOUNTS competition if he/she attends coaching?</a></li>
        <li><a class="btn_02" href="#Q8">8.	What is the objective of NSF MathCounts coaching?</a></li>
        <li><a class="btn_02" href="#Q9">9.	For what grade levels is NSF MathCounts coaching available?</a></li>
        <li><a class="btn_02" href="#Q10">10. What is Pre-MathCounts?</a></li>
        <li><a class="btn_02" href="#Q11">11. What are the various NSF MathCounts coaching levels and the criteria to join those levels?</a></li>
        <li><a class="btn_02" href="#Q12">12. How long are the NSF MathCounts and Pre-MathCounts coaching?</a></li>
        <li><a class="btn_02" href="#Q13">13. What do they actually learn in the coaching classes?</a></li>
    </ul>

    <div class="title02">MathCounts Coaching Registration Process:</div>
    <ul>
        <a name="top"></a>

        <li><a class="btn_02" href="#Q14">14. How do I register my child for the coaching classes?</a></li>
        <li><a class="btn_02" href="#Q15">15. How do I choose the coaching level and class for my child?</a></li>
        <li><a class="btn_02" href="#Q16">16. What is the cost of NSF coaching?</a></li>
        <li><a class="btn_02" href="#Q17">17. My child is in 5th grade.  Can I register for MathCounts coaching?</a></li>
        <li><a class="btn_02" href="#Q18">18. I cannot afford the registration fee for the course at this time.  How can I register for coaching?</a></li>
        <li><a class="btn_02" href="#Q19">19. The classes have already started.  Can I still register? </a></li>
        <li><a class="btn_02" href="#Q20">20. If I choose a class for my child but not pay, am I reserving a spot in that class? </a></li>

    </ul>

    <div class="title02">Registration Changes:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q21">21. I registered my child, but I haven't yet received any communication...</a></li>
        <li><a class="btn_02" href="#Q22">22. How can I change class to a more convenient day and time?</a></li>
        <li><a class="btn_02" href="#Q23">23. Can I change the class level after I am registered?</a></li>
        <li><a class="btn_02" href="#Q24">24. Change coach is grayed out.  How can I change coach?</a></li>
        <li><a class="btn_02" href="#Q25">25. Can I get a refund if I cancel my coaching course? </a></li>

    </ul>

    <div class="title02">Coaching Classes Related:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q26">26. What do I do if my child is going to miss a class?</a></li>
        <li><a class="btn_02" href="#Q27">27. How does class participation work in the online class??</a></li>
        <li><a class="btn_02" href="#Q28">28. Can I get answers along with homework problems?</a></li>
        <li><a class="btn_02" href="#Q29">29. Is it okay if I help my child with the homework?</a></li>
        <li><a class="btn_02" href="#Q30">30. I am too busy with my work.  Can I rely on the coach to take care of my child?</a></li>

    </ul>

    <div class="title02">Parents Contribution:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q31">31. As a parent, how can I help my child?</a></li>


    </ul>

    <hr />
    <div class="title02">MATHCOUNTS Competition:</div>
    <p>
        <a name="Q1"></a><span class="btn_02">1. What is MATHCOUNTS?    
        </span>
        <p>
            MATHCOUNTS is a National Math Competition open for students in 6th, 7th and 8th grades. MATHCOUNTS reaches over 250,000 US middle school students with more than 125,000 competing in the MATHCOUNTS competition series. In the most recent MATHCOUNTS alumni survey from the respondents who competed in MATHCOUNTS at state level, the average SAT score for the three sections was 2194, which shows students who compete at this level not only excel at mathematics.
            <br />
            There are 4 rounds of MATHCOUNTS: School, Chapter, State and National. First the student participates in the School round and if gets selected advances to Chapter. From Chapter round, the top scoring team and the individuals advance to State round and from State, 4 top individuals will compete in Nationals representing their state. Generally, the School Competition takes place in January, Chapter in February, State in March, and the National Competition in May. More information about MATHCOUNTS competitions can be obtained from www.mathcounts.org
        </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q2"></a><span class="btn_02">2. How can my child participate in MATHCOUNTS competition?
        </span>
        <p>
            To participate in MATHCOUNTS, your child needs to start from the school. Check with your school if there is a coach who conducts the school level competition. From there he needs to advance to Chapter, State and Nationals.
        </p>

    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q3"></a><span class="btn_02">3. My child's school does not participate in MATHCOUNTS, what can be done about it?
        </span>
        <p>
            You can talk to your school and conduct the MATHCOUNTS School Competition and take the top scoring students to the Chapter Competition. Sometimes, it may not be easy to convince the school management. Many schools don’t like to participate in just one specific competition as only very few kids can participate. Schools want to give opportunity to a large set of students. One way to approach this is to start a math club which meets once a week. This will allow any kid to participate in a weekly math learning activity. The club in addition to helping learn can expose the kids to many competitions including MATHCOUNTS, AMC 8, Math League etc. 
        </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q4"></a><span class="btn_02">4. My child is home schooled, can he participate in MATHCOUNTS?
        </span>
        <p>Yes, your child can still participate in MATHCOUNTS. An affidavit is available in the website www.mathcounts.org for the homeschool children. Prior to the Chapter Competition, it has to be filled and given to the chapter coordinator and if your child advances to the State Competition, it is forwarded to the state coordinator by the chapter coordinator. If he/she advances to national Competition, the affidavit is forwarded to the national office by the state coordinator. </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">NSF MathCounts Coaching:</div>

    <p>
        <a name="Q5"></a><span class="btn_02">5. How does the online coaching work?
        </span>
    </p>
    <p>We will send you an invitation every week to join the weekly online meeting.  Students will just click on the link to join the meeting and dial in to the phone number provided for your class.  The first hour of the class will be dedicated to going over difficult homework problems from previous week followed by discussion of new problems. </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q6"></a><span class="btn_02">6. What materials do we need to have in order to attend the online coaching?
        </span>
    </p>
    <p>
        This is an online class. You need a Computer, Computer Audio, Microphone and Telephone (with ability to mute).  
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q7"></a><span class="btn_02">7. Is it mandatory for my child to participate in MATHCOUNTS competition if he/she attends coaching?
        </span>
    </p>
    <p>No, it is not compulsory to participate in the competition. If your child is interested to participate, he/she may register through their school. If not, your child can attend coaching just for math enrichment and improve accuracy and speed.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q8"></a><span class="btn_02">8. What is the objective of NSF MathCounts coaching?
        </span>
    </p>
    <p>Several NSF students are winning top places in the Spelling Bee, Geography Bee, Brain Bee competitions year after year. Though there are many kids highly talented and skilled in Math, we want to see more NSF students representing and winning the MATHCOUNTS competition. So, we started a systematic coaching for this competition in 2009-10 and we saw few kids advancing to the National Competition and placing in top 50 places. We are hoping to see more NSF kids in the Nationals and winning the championship.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q9"></a><span class="btn_02">9. For what grade levels is NSF MathCounts coaching available?
        </span>
    </p>
    <p>
        There are two streams of coaching:
        <ul>
            <li>NSF MathCounts coaching (6th - 8th  grades, but high school students have taken the coaching in the past to sharpen their math skills and to practice speed) </li>
            <li>NSF Pre-MathCounts coaching (4th  and 5th grades)</li>
        </ul>

    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q10"></a><span class="btn_02">10. What is Pre-MathCounts?
        </span>
    </p>
    <p>
        Pre-MathCounts is to generate enthusiasm and passion for math in kids.  Another objective is to prepare 4th and 5th grade students in math concepts and problem solving skills needed to compete and excel in MATHCOUNTS National competition during their 6th, 7th and 8th grades.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q11"></a><span class="btn_02">11. What are the various NSF MathCounts coaching levels and the criteria to join those levels?
        </span>
    </p>
    <p>Please refer to Mathcounts – Level Determination document.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q12"></a><span class="btn_02">12. How long are the NSF MathCounts and Pre-MathCounts coaching?
        </span>
    </p>
    <p>
        The coaching is 15 weeks long with 2 hours session each week (with the exception of holidays) starting around October every year. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q13"></a><span class="btn_02">13. What do they actually learn in the coaching classes?
        </span>
    </p>
    <p>MATHCOUNTS releases handbooks every year. The students will solve the problems from those books and other similar problems. They may learn the underlying concepts of these problems and additional concepts that will be helpful to solve MATHCOUNTS problems.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">MathCounts Coaching Registration Process:</div>

    <p>
        <a name="Q14"></a><span class="btn_02">14. How do I register my child for the coaching classes?
        </span>
    </p>
    <p>
        Go to our website <a href="www.northsouth.org">www.northsouth.org</a> and login as a Parent. Click on “Register for Coaching”. It will take you step by step. You can pick a coach based on your child’s level and the time that best suits you.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q15"></a><span class="btn_02">15. How do I choose the coaching level and class for my child?
        </span>
    </p>
    <p>
        Again, please refer to Mathcounts – Level Determination document for NSF Mathcounts coaching level recommendations. There may be more than one coach available for your child’s level. You can pick a coach with most convenient time that will work for you.. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q16"></a><span class="btn_02">16. What is the cost of NSF coaching?
        </span>
    </p>
    <p>
        The registration fee for this coaching is $150, 2/3rd of which is tax deductible. In financial hardship cases, a waiver will be considered upon request.  
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q17"></a><span class="btn_02">17. My child is in 5th grade.  Can I register for MathCounts coaching?
        </span>
    </p>
    <p>
        No, only students in 6thgrade and up can join this coaching. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q18"></a><span class="btn_02">18. I cannot afford the registration fee for the course at this time.  How can I register for coaching?
        </span>
    </p>
    <p>
        Please send an email to <a>nsfmathcounts@hotmail.com</a> describing the financial difficulty. We will go over your situation and let you know if your fee will be waived.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q19"></a><span class="btn_02">19. The classes have already started.  Can I still register? 
        </span>
    </p>
    <p>
        No, once the registration is closed, you cannot register. It is first come, first served, so register at the earliest to ensure a seat for your child.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q20"></a><span class="btn_02">20. If I choose a class for my child but not pay, am I reserving a spot in that class?  
        </span>
    </p>
    <p>
        No, your registration is not complete until you pay. You will receive a confirmation email after you pay and complete the registration. Just choosing the appropriate class will not guarantee a spot for your child in that class.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">Registration Changes:</div>
    <p>
        <a name="Q21"></a><span class="btn_02">21. I registered my child, but I haven't yet received any communication...
        </span>
    </p>
    <p>
        Your child's coach will contact you few days before the beginning of the class. If you have to contact us on any important issue, send an email to <a>nsfmathcounts@hotmail.com.</a> Someone will contact you.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q22"></a><span class="btn_02">22. How can I change class to a more convenient day and time?
        </span>
    </p>
    <p>
        Parents can change the classes themselves from the Parent Functions page, using “Change Coach” option, for up to two weeks after the coaching session starts, provided space is available. If you find another class for the level you want with a convenient day and time, you can go ahead and change yourself. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q23"></a><span class="btn_02">23. Can I change the class level after I am registered?
        </span>
    </p>
    <p>
        Please read the answer for the last question. If space is available, you will be able to change in first two weeks.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q24"></a><span class="btn_02">24. Change coach is grayed out.  How can I change coach?
        </span>
    </p>
    <p>
        You are allowed to change the coach from the Parent Functions page up to two weeks after the coaching starts. Subsequently, this option is grayed out.  
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q25"></a><span class="btn_02">25. Can I get a refund if I cancel my coaching course? 
        </span>
    </p>
    <p>
        Two-thirds of the registration fee you pay is tax-deductible and is used as a scholarship to help a poor child go to college in India.  Coaching is done by all volunteers who give their time and effort for this noble cause.  If you insist on getting a refund of your registration fee, NSF will do so only before the second coaching class.  You can request a refund on the “Parent Functions” page after you do Parent Login in <a href="www.northsouth.org">www.northsouth.org. </a>
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">Coaching Classes Related:</div>
    <p>
        <a name="Q26"></a><span class="btn_02">26. What do I do if my child is going to miss a class?
        </span>
    </p>
    <p>
        Please email your coach and get the materials that will be covered during that class. You can work them out at home. If you have any question in it, you can forward them to your coach and get it clarified.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q27"></a><span class="btn_02">27. How does class participation work in the online class?
        </span>
    </p>
    <p>
        Parents are requested to ensure that their child participates in every class and is respectful of other students.  Students should be discouraged from disruptive behavior such as writing on the board or chat window while the coach is teaching.  Their phone should be set to mute setting unless they are called upon to speak. Different students are at different levels so don’t let your child get overwhelmed or discouraged if some concepts are new to them.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q28"></a><span class="btn_02">28. Can I get answers along with homework problems?
        </span>
    </p>
    <p>
        No, the coaches give just the problems for homework. In the next class, these problems are discussed and the answers are given. This will give an opportunity for the kids to try the problems on their own.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q29"></a><span class="btn_02">29. Is it okay if I help my child with the homework?
        </span>
    </p>
    <p>
        No, the coaches give just the problems for homework. In the next class, these problems are discussed and the answers are given. This will give an opportunity for the kids to try the problems on their own.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q30"></a><span class="btn_02">30. I am too busy with my work.  Can I rely on the coach to take care of my child?
        </span>
    </p>
    <p>
        No. NSF Coaching is a good starting point in the right direction to prepare your child for the MATHCOUNTS competition. As we said earlier, coaches are volunteers and each coach has about 20 children to handle. So don’t rely solely on the NSF coaching for your child to win the competition.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">Parents Contribution:</div>
    <p>
        <a name="Q30"></a><span class="btn_02">31. As a parent, how can I help my child?
        </span>
    </p>
    <p>
        You can provide a quiet environment for your child to work every day especially during the online coaching session. Kids will be on the phone and on online session during the class, so a quiet, undisturbed place will help your child a lot. To fare well in MATHCOUNTS competition speed and accuracy are important skills to acquire. So every day practice in solving math problems will be helpful. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>
</div>

<!--#include file="../uscontests_footer.aspx"-->
