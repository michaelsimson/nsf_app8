﻿<!--#include file="../uscontests_header.aspx"-->
<script type="text/javascript">
    document.getElementById("dvMathsCoaching").style.display = "block";

</script>
<div class="title02">NSF Coaching : FAQ – Pre-Mathcounts</div>

<div align="justify" class="txt01">

    <div class="title02">PRE-MATHCOUNTS Coaching:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q1">1. What is the Objective of Pre-MathCounts?</a></li>
        <li><a class="btn_02" href="#Q2">2.	Who can register for Pre-MathCounts Coaching?</a></li>
        <li><a class="btn_02" href="#Q3">3.	What is the Pre-MathCounts coaching Plan?</a></li>
        <li><a class="btn_02" href="#Q4">4.	What are some of the topics covered in Pre-MathCounts?</a></li>
        <li><a class="btn_02" href="#Q5">5. How does class participation work in the online class?</a></li>
        <li><a class="btn_02" href="#Q6">6.	How does class participation work in the online class?</a></li>
        <li><a class="btn_02" href="#Q7">7.	Can the coaches email us the notes?</a></li>
        <li><a class="btn_02" href="#Q8">8.	Is there Homework in the Coaching?</a></li>
        <li><a class="btn_02" href="#Q9">9.	How does the online coaching work?</a></li>
        <li><a class="btn_02" href="#Q10">10. How many weeks are classes are held during the year?</a></li>
        <li><a class="btn_02" href="#Q11">11. What can I do to help my child from home?</a></li>
        <li><a class="btn_02" href="#Q12">12. My child is not able to understand and is finding the topics very challenging.?</a></li>
        <li><a class="btn_02" href="#Q13">13. If we miss a class, are there any makeup classes?</a></li>

    </ul>
    <div class="title02">Pre-MathCounts Coaching Registration Process:</div>
    <ul>
        <a name="top"></a>
        <li><a class="btn_02" href="#Q14">14. How do I enroll my kid in Pre-MathCounts coaching classes?</a></li>
        <li><a class="btn_02" href="#Q15">15. How do I choose the coaching level and class for my child?</a></li>
        <li><a class="btn_02" href="#Q16">16. What is the cost of NSF coaching?</a></li>
        <li><a class="btn_02" href="#Q17">17. My child is in 3rd grade.  Can I register for Pre-MathCounts coaching?</a></li>
        <li><a class="btn_02" href="#Q18">18. I cannot afford the registration fee for the course at this time.  How can I register for coaching?</a></li>
        <li><a class="btn_02" href="#Q19">19. The classes have already started.  Can I still register? Can I get make-up classes? </a></li>
    </ul>

    <div class="title02">Registration Changes:</div>
    <ul>
        <a name="top"></a>


        <li><a class="btn_02" href="#Q20">20. I registered my child, but I haven't yet received any communication... </a></li>
        <li><a class="btn_02" href="#Q21">21. How can I change coach to a more convenient day and time?</a></li>
        <li><a class="btn_02" href="#Q22">22. Change coach option is grayed out.  How can I change coach?</a></li>
        <li><a class="btn_02" href="#Q23">23. Can I get a refund if I cancel my coaching course?</a></li>
    </ul>

    <div class="title02">Coaching Classes Related:</div>
    <ul>
        <a name="top"></a>

        <li><a class="btn_02" href="#Q24">24. My child finds the material too difficult.  Should I drop my child from the classes?</a></li>
        <li><a class="btn_02" href="#Q25">25. My child finds the material too easy.  How can he get more challenged? </a></li>
        <li><a class="btn_02" href="#Q26">26. What do I do if my child is going to miss a class?</a></li>
        <li><a class="btn_02" href="#Q27">27. Can I get answers along with homework problems?</a></li>
        <li><a class="btn_02" href="#Q28">28. Is it ok, if I help with my child with the homework?</a></li>
        <li><a class="btn_02" href="#Q29">29. I am too busy with my work.  Can I rely on the coach to take care of my child?</a></li>
    </ul>

    <div class="title02">Parent Contribution:</div>
    <ul>
        <a name="top"></a>

        <li><a class="btn_02" href="#Q30">30. As a parent, how can I help my child?</a></li>
        <li><a class="btn_02" href="#Q31">31. How can I help in NSF Mathcounts and Pre-Mathcounts coaching?</a></li>

    </ul>



    <hr />
    <div class="title02">PRE-MATHCOUNTS Coaching:</div>
    <p>
        <a name="Q1"></a><span class="btn_02">1. What is the Objective of Pre-MathCounts?    
        </span>
        <p>
            The primary objective of Pre-MathCounts coaching is to lay solid foundation of math concepts in the kids so they retain them throughout their life.  During this course NSF kids will be exposed to important concepts and topics they will need to solve MATHCOUNTS type problems.  
        </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q2"></a><span class="btn_02">2. Who can register for Pre-MathCounts Coaching?
        </span>
        <p>
            Pre-MathCounts coaching is designed for 4th and 5th graders interested in learning advanced topics required to do well in MATHCOUNTS competition in Middle School.
        </p>

    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q3"></a><span class="btn_02">3. What is the Pre-MathCounts coaching Plan?
        </span>
        <p>
            There are three levels Beginner, Intermediate and Advanced. For details, refer to <a href="http://sdrv.ms/Q8KX4N">http://sdrv.ms/Q8KX4N </a>
        </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q4"></a><span class="btn_02">4. What are some of the topics covered in Pre-MathCounts?
        </span>
        <p>
            Coaching material will be based on (but not limited to) the following broad topics:
            <ul>
                <li>(a) Number theory (integers, order of operation, prime factors, LCM, GCF, fractions, simple interest, etc.)</li>
                <li>(b) Algebra (pattern recognition, exponents, combining like terms, solving one and two-step equations, direct and inverse relationship and their relationship to word problems, etc.)</li>
                <li>(c) Geometry (angles, parallel lines, Pythagoras theorem, similar triangles, polygons, solids, area, volumes, graphs, translation, rotation, reflection etc.)</li>
                <li>(d) Probability (counting principles, probability, application to word problems).</li>
            </ul>

        </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>



    <p>
        <a name="Q5"></a><span class="btn_02">5. How does class participation work in the online class?
        </span>
    </p>
    <p>Online teaching has its limitations.  Unlike face-to-face classes we are not able to see your child and have the teacher-student interaction as in a regular classroom.  So, we rely on parents for encouraging the child to participate.  Also, we would like to limit parent involvement in class discussions due to technical difficulties.  We are happy to answer any of your questions after class session or by email-but questions related to the material taught should be asked by the students.  </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q6"></a><span class="btn_02">6. How does class participation work in the online class?
        </span>
    </p>
    <p>
        Parents are requested to ensure that their child participates in every class and is respectful of other students.  Students should be discouraged from disruptive behavior such as writing on the board or chat window while the coach is teaching.  Their phone should be set to mute setting unless they are called upon to speak.
        <br />
        Different students are at different levels so don’t let your child get overwhelmed or discouraged if some concepts are new to them.
  
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q7"></a><span class="btn_02">7. Can the coaches email us the notes?
        </span>
    </p>
    <p>It is not our policy to email notes.  However, important pages may be sent along with the homework worksheets.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q8"></a><span class="btn_02">8. Is there Homework in the Coaching?
        </span>
    </p>
    <p>There is homework given every week to reinforce learning of concepts covered in class.  It is very important for kids to complete homework on time. If homework is not submitted for three weeks your child will receive a warning and may be dropped from class.  Please ensure they try the problems even if they do not get the right answer.   During Thanksgiving and Christmas Break, your child will be receiving extra homework.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q9"></a><span class="btn_02">9. How does the online coaching work?
        </span>
    </p>
    <p>
        We will send you an invitation every week to join the weekly online meeting.  You will join meeting using the web-address and dial in to the phone number provided for your class.  The first hour of the class will be dedicated to going over difficult homework problems from previous week followed by discussion of new problems. 

    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q10"></a><span class="btn_02">10. How many weeks are classes are held during the year?
        </span>
    </p>
    <p>
        We will have online meeting for 15 weeks including a final exam on all topics discussed during the year. The 15 weeks will be spread out with some holidays such as  Thanksgiving, Christmas and New Year. Your coach may also have to take some personal holidays. The coaching schedule will be emailed to you.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q11"></a><span class="btn_02">11. What can I do to help my child from home?
        </span>
    </p>
    <p>Ensure your child attends each and every class.  Also spend some time with your child while he/she is completing the assigned homework each week showing the method he/she is using.  Also, use online videos and other examples to reinforce topics.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q12"></a><span class="btn_02">12. My child is not able to understand and is finding the topics very challenging?
        </span>
    </p>
    <p>
        The topics are carefully chosen based on prior years’ MATHCOUNTS tests.  These topics are precursors to more advanced topics they will encounter in 6th, 7th, 8th grades.  Hence you will have to work with them with more examples to help them learn.  Different children learn differently.  Use a variety of techniques to help your child such as videos, pictures, patterns, simpler problems etc.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q13"></a><span class="btn_02">13. If we miss a class, are there any makeup classes?
        </span>
    </p>
    <p>There are no scheduled makeup classes, however please contact your coach for homework, classwork and specific help with any questions.  Also, please notify your coach if your student is going to miss any classes.</p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">MathCounts Coaching Registration Process:</div>

    <p>
        <a name="Q14"></a><span class="btn_02">14. How do I enroll my kid in Pre-MathCounts coaching classes?
        </span>
    </p>
    <p>
        Go to our website <a href="http://www.northsouth.org">http://www.northsouth.org</a> Parent Login.  Login with your parent ID.  In the “Parent Functions” page, click on “Register for coaching”.  You may pick a coach based on your child’s level and the time that best suits you.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q15"></a><span class="btn_02">15. How do I choose the coaching level and class for my child?
        </span>
    </p>
    <p>
        There are three levels Beginner, Intermediate and Advanced. For details, refer to Pre-Mathcounts – Level Determination document.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q16"></a><span class="btn_02">16. What is the cost of NSF coaching?
        </span>
    </p>
    <p>
        The registration fee for this coaching is $150, 2/3rd of which is tax deductible. In financial hardship cases, a waiver will be considered upon request.  
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q17"></a><span class="btn_02">17. My child is in 3rd grade.  Can I register for Pre-MathCounts coaching?
        </span>
    </p>
    <p>
        No. Since we have limited coaches and all of our coaches are volunteers, we can’t accommodate children from 3rd grade for Pre-MathCounts coaching. We are only considering students who are registered in 4th or 5th grade in the current year. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q18"></a><span class="btn_02">18. I cannot afford the registration fee for the course at this time.  How can I register for coaching?
        </span>
    </p>
    <p>
        Please send an email to <a>nsfpremathcounts@hotmail.com</a> describing the financial difficulty. We will go over your situation and let you know if we could register your child for coaching.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q19"></a><span class="btn_02">19. The classes have already started.  Can I still register? Can I get make-up classes? 
        </span>
    </p>
    <p>
        Yes. If there are still spaces available, you can register your child for the classes. Since all of our coaches are parent volunteers, they may not be able to give make-up classes. However, you may contact your coach to request the materials that have been covered so far and work on them at home.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <div class="title02">Registration Changes:</div>
    <p>
        <a name="Q20"></a><span class="btn_02">20. I registered my child, but I haven't yet received any communication...
        </span>
    </p>
    <p>
        If you have not received any communication one week prior to start date, please send an email to <a>nsfpremathcounts@hotmail.com</a>. Someone will contact you.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q21"></a><span class="btn_02">21. How can I change coach to a more convenient day and time?
        </span>
    </p>
    <p>
        Parents can change the coaches themselves from the Parent Functions page for up to two weeks after the coaching session starts. If you find another coach for the same level with a convenient day and time, you can go ahead and change the coach yourself. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q22"></a><span class="btn_02">22. Change coach option is grayed out.  How can I change coach??
        </span>
    </p>
    <p>
        You are allowed to change the coach from the Parent Functions page up to two weeks after the coaching starts. Subsequently, this option is grayed out. Please send an email to <a>nsfpremathcounts@hotmail.com</a> with the details and if the situation permits, we will accommodate your request. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q23"></a><span class="btn_02">23. Can I get a refund if I cancel my coaching course?
        </span>
    </p>
    <p>
        The registration fee you pay is used as a scholarship to help a poor child go to college in India.  Coaching is done by all volunteers who give their time and effort for this noble cause.  If you insist on getting a refund of your registration fee, NSF will do so only before the 2nd coaching class.  You can request a refund on the parent functions page after you do parent login.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>
    <div class="title02">Coaching Classes Related:</div>
    <p>
        <a name="Q24"></a><span class="btn_02">24. My child finds the material too difficult.  Should I drop my child from the classes?
        </span>
    </p>
    <p>
        Talk to your child’s coach and share your concerns. If there is a gap between your child’s level and the material and if your child finds it hard to catch up with the material in the first few weeks, he/she may be moved to a lower level. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q25"></a><span class="btn_02">25. My child finds the material too easy.  How can he get more challenged? 
        </span>
    </p>
    <p>
        Repetition of topics only helps reinforce those.  Again, talk to your child’s coach.  Initially a child may find a level easy, but subsequent classes may cover harder topics in-depth. The coach may be able to evaluate your child and guide him/her accordingly either to stick with the same level or to move to a higher level.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q26"></a><span class="btn_02">26. What do I do if my child is going to miss a class?
        </span>
    </p>
    <p>
        Please email your coach and get the materials that will be covered during that class. You can work them out at home. If you have any questions in it, you can forward them to your coach and get them clarified.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q27"></a><span class="btn_02">27. Can I get answers along with homework problems?
        </span>
    </p>
    <p>
        No, the coaches give just the problems for homework. In the next class, these problems are discussed and the answer is given. This will give an opportunity for the kids to try the problems on their own.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q28"></a><span class="btn_02">28. Is it ok, if I help with my child with the homework?
        </span>
    </p>
    <p>
        Yes and No. No, do not jump right in and do the problems for your child. This will not give a chance for your child to do some deep thinking and be challenged. Yes, when a child is stuck with a problem you can give some hints for the child to think in the right direction and arrive at the answer.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>

    <p>
        <a name="Q29"></a><span class="btn_02">29. I am too busy with my work.  Can I rely on the coach to take care of my child?
        </span>
    </p>
    <p>
        No. NSF Coaching is a good starting point in the right direction to prepare your child for the MATHCOUNTS competition. As we said earlier, coaches are volunteers and each coach has about 20 children to handle. So don’t rely solely on the NSF coaching for your child to win the competition.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>
    <div class="title02">Parents Contribution:</div>
    <p>
        <a name="Q30"></a><span class="btn_02">30. As a parent, how can I help my child?
        </span>
    </p>
    <p>
        You can provide a quiet environment for your child to work every day especially during the online coaching session. Kids will be on the phone and on online session during the class, so a quiet, undisturbed place will help your child a lot. To fare well in MATHCOUNTS competitions, speed and accuracy are important skills to acquire. So every day practice in solving math problems will be helpful.
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>


    <p>
        <a name="Q30"></a><span class="btn_02">31. How can I help in NSF Mathcounts and Pre-Mathcounts coaching?
        </span>
    </p>
    <p>
        You can volunteer to be a coach. You can send an email to nsfpremathcounts@hotmail.com telling us your background, class timings you can teach and why you want to become a coach. Based on the needs and your background we will evaluate and assign you with a specific level. 
    </p>
    <p><span class="btn_02"><a href="#top">Top</a></span></p>
</div>

<!--#include file="../uscontests_footer.aspx"-->
