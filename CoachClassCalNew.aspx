﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CoachClassCalNew.aspx.cs" Inherits="CoachClassCalNew" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link href="css/ClassCal.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <link href="css/HtmlGridTable.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <%-- <link href="css/BeatPicker.min.css" rel="stylesheet" />
    <script src="js/BeatPicker.min.js"></script>--%>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/Loader.css" rel="stylesheet" />
    <script src="js/jquery.toast.js"></script>
    <link href="css/jquery.toast.css" rel="stylesheet" />

    <%--  <link href="css/Loader.css" rel="stylesheet" />--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <div>
        <%-- <link href="css/dcalendar.picker.css" rel="stylesheet" />
        <script src="js/dcalendar.picker.js"></script>--%>
        <style type="text/css">
            @-webkit-keyframes invalid {
                from {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            @-moz-keyframes invalid {
                from {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            @-o-keyframes invalid {
                from; {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            @keyframes invalid {
                from {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            .invalid {
                -webkit-animation: invalid 1s infinite; /* Safari 4+ */
                -moz-animation: invalid 1s infinite; /* Fx 5+ */
                -o-animation: invalid 1s infinite; /* Opera 12+ */
                animation: invalid 1s infinite; /* IE 10+ */
            }
        </style>
        <script type="text/javascript">


            var jsonClassSchedule;
            var glCoachId = getParameterByName("coachID");
            var glSemester = getParameterByName("Semester");
            var glPgId = getParameterByName("pgId");
            var glPid = getParameterByName("pId");
            var glSessionNo = getParameterByName("SessionNo");
            var gllevel = getParameterByName("level");
            $(function (e) {

                $('#dvHWDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                });

                $('#dvDueDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                });
                $('#dvARelDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                });
                $('#dvSRelDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                });
                $('#dvClassDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                });
                getYear();

                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;
                //if (roleID == "88") {
                //    $("#spnUserTitle").text("Coach Perspective");
                //} else { $("#spnUserTitle").text("Admin Perspective"); }
                // showPage();
                $(".beatpicker-clearButton").hide();
                getYearAndSemester();



            });

            function getParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }
            $(document).on("click", "#btnSubmit", function (e) {


                if (validateSubmit() == "1") {

                    $("#spnGridTitle").show();

                    loadSchedule(0);
                    //testWeekNoReset();
                }
            });

            function showLoader() {
                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");
            }

            function hideLoader() {
                $("#fountainTextG").css("display", "none");
                $("#overlay").css("display", "none");
            }

            function loadSchedule(coachClassCalID) {
                showLoader();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();

                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, Semester: semester, Level: level, SessionNo: sessionNo, CoachClassCalID: coachClassCalID } });


                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListSchedule",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#btnAddNextClass").show();
                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Date</th>";
                        tblHtml += "<th>Day</th>";
                        tblHtml += " <th>Time</th>";
                        tblHtml += "<th>Class Stype</th>";
                        tblHtml += "<th>Week#</th>";
                        tblHtml += "<th>Status</th>";
                        tblHtml += "<th>Substitute</th>";

                        tblHtml += "<th>Home work</th>";
                        tblHtml += "<th>HWDueDate</th>";

                        tblHtml += "<th>ARelDate</th>";
                        tblHtml += "<th>SRelDate</th>";
                        tblHtml += "</tr>";
                        tblHtml += "</thead>";
                        var responseCount = 0;
                        var acceptedCount = 0;
                        var i = 0;
                        var surveyTitle = "";
                        var color = "";
                        var strClass = "";
                        var cursor = "";
                        jsonClassSchedule = data.d;
                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {

                                if (value.IsDisabled == "Yes") {
                                    //color = "grey";
                                    //strClass = "modifyDisabled";
                                    //cursor = "normal";

                                    color = "blue";
                                    strClass = "modify";
                                    cursor = "pointer";

                                } else {
                                    color = "blue";
                                    strClass = "modify";
                                    cursor = "pointer";
                                }

                                tblHtml += "<tr>";
                                var deleteClass = "delete";
                                var deleteColor = "red";
                                var deleteCurso = "pointer";
                                var deleteTitle = "Delete";

                                if (value.CoachClassCalID == "") {
                                    value.CoachClassCalID = "0";
                                    deleteClass = "DeleteDisabled";
                                    deleteColor = "grey";
                                    deleteCurso = "normal";
                                    deleteTitle = "";

                                }

                                tblHtml += "<tr class='tr" + value.WeekNo + "'>";
                                if (roleID == "88") {
                                    tblHtml += '<td><div style="float:left;"><a attr-CType=' + value.ClassType + ' attr-CoachClassID=' + value.CoachClassCalID + ' attr-Date=' + value.Date + ' attr-Time=' + value.Time + ' class=' + strClass + ' WeekNo=' + value.WeekNo + '  title="Modify" style="cursor:' + cursor + ';"><i style="color:' + color + ';" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div></td>';
                                } else {
                                    tblHtml += '<td><div style="float:left;"><a attr-CType=' + value.ClassType + ' attr-CoachClassID=' + value.CoachClassCalID + ' attr-Date=' + value.Date + ' WeekNo=' + value.WeekNo + '  attr-Time=' + value.Time + ' class="modify" title="Modify" style="cursor:pointer;"><i style="color:blue;" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div><div style="float:left; position:relative; left:10px;"><a style="cursor:' + deleteCurso + ';" attr-CoachClassID=' + value.CoachClassCalID + ' attr-ClassType=' + value.ClassType + ' attr-ClassDate=' + value.Date + ' WeekNo=' + value.WeekNo + ' class=' + deleteClass + ' title=' + deleteTitle + '><i class="fa fa-trash-o fa-2x" style="color:' + deleteColor + ';" aria-hidden="true"></i></a></div></td>';
                                }
                                tblHtml += "<td>" + value.Date + "</td>";
                                tblHtml += "<td>" + value.Day + "</td>";
                                tblHtml += "<td>" + value.Time + "</td>";
                                tblHtml += "<td>" + value.ClassType + "</td>";
                                tblHtml += "<td>" + value.WeekNo + "</td>";
                                tblHtml += "<td>" + value.Status + "</td>";
                                tblHtml += "<td>" + value.SubstituteCoachName + "</td>";

                                if (value.Status == "On" && value.ClassType != "Extra") {

                                    tblHtml += "<td>" + (value.HWDeadlineDate == null ? "" : value.HWDeadlineDate) + "</td>";
                                    tblHtml += "<td>" + (value.HWDueDate == null ? "" : value.HWDueDate) + "</td>";

                                    tblHtml += "<td>" + (value.ARelDate == null ? "" : value.ARelDate) + "</td>";
                                    tblHtml += "<td>" + (value.SRelDate == null ? "" : value.SRelDate) + "</td>";
                                } else {
                                    tblHtml += "<td></td>";
                                    tblHtml += "<td></td>";

                                    tblHtml += "<td></td>";
                                    tblHtml += "<td></td>";
                                }

                                tblHtml += "</tr>";


                            });
                            hideLoader();

                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";


                            hideLoader();
                        }
                        tblHtml += "</tbody>";
                        $("#table").html(tblHtml);

                    },
                    failure: function (response) {
                        alert(response.d);
                        hideLoader();
                    }
                });
            }

            $(document).on("click", "#btnAddNextClass", function (e) {
                // clear();
                $("#hdnCoachClassCalID").val("0");
                $("#hdnSubstituteCoachId").val("0");
                $("#selClassType").val("0");
                $("#txtWeekNo").val("");
                $("#txtClassDate").val("");
                $("#selTime").val("0");
                $("#txtHwRelDate").val("");
                $("#txtSRelDate").val("");
                $("#txtDueDate").val("");
                $("#txtAnsDate").val("");

                $("#dvAddClass").show();
                $("#selClassType").removeAttr("disabled");
                $("#txtWeekNo").attr("disabled", "disabled");
                $("#txtClassDate").attr("disabled", "disabled");
                $("#selTime").attr("disabled", "disabled");

                $("#selStatus").val("On");
                $("#fieldAddClass").width(850);
                clear();

            });

            $(document).on("click", "#btnCancel", function (e) {
                $("#dvAddClass").hide();
            });
            $(document).on("click", ".modify", function (e) {
                //  $("#table tbody tr").css("background-color", "F5F5F5");
                // $(this).closest("tr").prev("tr").css("background-color", "F5F5F5");
                //$(this).closest("tr").prev().css("background-color", "F5F5F5");
                // $(this).closest("tr").css("background-color", "blue");
                $("#dvAddClass").show();
            });

            function postNewClass() {
                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();
                var status = $("#selStatus").val();
                var hrelDate = $("#txtHwRelDate").val();
                var dueDate = $("#txtDueDate").val();
                var sRelDate = $("#txtSRelDate").val();
                var aRelDate = $("#txtAnsDate").val();
                var weekNo = $("#txtWeekNo").val();
                var day = "Thursday";
                var duration = "120";
                var substituteCoachId = 0;
                var makeUpCoachID = 0;
                var coachClassCalId = $("#hdnCoachClassCalID").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                substituteCoachId = $("#selSubCoach").val();
                if (classType == "Substitute") {
                    substituteCoachId = $("#selSubCoach").val();

                }
                if (classType == "Makeup") {
                    makeUpCoachID = $("#selSubCoach").val();
                }

                showLoader();
                var jsonData = JSON.stringify({ objCoachClass: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, Date: date, Day: day, Time: time, Duration: duration, WeekNo: weekNo, Status: status, ClassType: classType, Substitute: substituteCoachId, CreatedBy: loginID, Makeup: makeUpCoachID, CoachClassCalID: coachClassCalId } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/PostNewClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            var retval = 0;
                            var coachRelId = 0;
                            var coachPaperId = 0;
                            $.each(data.d, function (index, value) {
                                retval = value.RetVal;
                                coachRelId = value.CoachRelID;
                                coachPaperId = value.CoachpaperID;


                            });


                            if (parseInt(retval) > 0) {
                                if (parseInt(coachPaperId) > 0 && classType != "Extra") {
                                    if (parseInt(coachRelId) > 0) {
                                        var hDate = $("#txtHwRelDate").val();
                                        var hDueDate = $("#txtDueDate").val();
                                        var aReldate = $("#txtAnsDate").val();
                                        var sRelDate = $("#txtSRelDate").val();
                                        if (hDate != "" && hDueDate != "" && aReldate != "" && sRelDate != "") {
                                            UpdateReleaseDates(coachRelId);
                                        }
                                    } else {
                                        var hDate = $("#txtHwRelDate").val();
                                        var hDueDate = $("#txtDueDate").val();
                                        var aReldate = $("#txtAnsDate").val();
                                        var sRelDate = $("#txtSRelDate").val();
                                        if (hDate != "" && hDueDate != "" && aReldate != "" && sRelDate != "") {
                                            inserReleaseDates(coachPaperId);
                                        }
                                    }
                                }
                                loadSchedule(0);
                                $("#spnGridTitle").show();
                                statusMessage("Class added successfully!", "success");
                                clear();
                                $("#dvAddClass").hide();
                            }
                            $("#hdnCoachClassCalID").val("0")
                        }
                        hideLoader();

                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            $(document).on("click", "#btnSave", function (e) {
                var classType = $("#selClassType").val();

                $(this).attr("disabled", "disabled");
                if (validateAddClass() == "1") {

                    if (classType == "Substitute") {

                        if ($("#hdnCoachClassCalID").val() != "0") {

                            var status = $("#selStatus").val();
                            //if (status == "Cancelled") {

                            getConfirmationForWeekNo();
                            // } else {
                            //    updateCoachClassSchedule("No");
                            // }

                        } else {

                            checkDuplicateClass();
                        }
                        // verifyRegularClasCancelled(); 
                    } else if (classType == "Makeup") {
                        $("#txtClassDate").removeAttr("disabled");
                        $("#selTime").removeAttr("disabled");
                        var mode = 1;
                        if ($("#hdnCoachClassCalID").val() != "0") {
                            mode = "2";
                        }
                        validateMakeupSessions(mode);
                    } else if (classType == "Extra") {
                        $("#txtClassDate").removeAttr("disabled");
                        $("#selTime").removeAttr("disabled");
                        var mode = 1;
                        if ($("#hdnCoachClassCalID").val() != "0") {
                            mode = "2";
                        }
                        //if (mode == "1") {
                        //    validateExtraSessions(mode);
                        //} else {
                        //    updateCoachClassSchedule("No");
                        //}
                        validateExtraSessions(mode);

                    } else if (classType == "Makeup Substitute") {
                        $("#txtClassDate").removeAttr("disabled");
                        $("#selTime").removeAttr("disabled");
                        var mode = 1;
                        if ($("#hdnCoachClassCalID").val() != "0") {
                            mode = "2";
                        }
                        if (mode == 1) {
                            validateMakeupSubstituteSessions();
                        } else {
                            getConfirmationForWeekNo()
                        }
                    } else if (classType == "Holiday") {

                        $("#txtClassDate").attr("disabled", "disabled");
                        $("#selTime").attr("disabled", "disabled");
                        if (validateAddClass() == "1") {
                            if ($("#hdnCoachClassCalID").val() != "0") {
                                var status = $("#selStatus").val();
                                //if (status == "Cancelled") {
                                // var consecutiveWeekNo = getConfirmationForWeekNo();
                                getConfirmationForWeekNo();
                                // } else {
                                //     updateCoachClassSchedule("No");
                                // }

                            } else {
                                verifyHolidayCLass();
                                // postNewClass();
                            }
                        }
                    } else {
                        $("#txtClassDate").attr("disabled", "disabled");
                        $("#selTime").attr("disabled", "disabled");
                        if (validateAddClass() == "1") {
                            if ($("#hdnCoachClassCalID").val() != "0") {
                                var status = $("#selStatus").val();
                                //if (status == "Cancelled") {
                                // var consecutiveWeekNo = getConfirmationForWeekNo();
                                getConfirmationForWeekNo();
                                // } else {
                                //     updateCoachClassSchedule("No");
                                // }

                            } else {
                                postNewClass();
                            }
                        }
                    }
                }
                $(this).removeAttr("disabled");
            });

            function showHideDateTime(mode) {
                var classType = $("#selClassType").val();

                if (classType == "Substitute") {

                    $("#dvSubstitute").show();
                    $("#selTime").attr("disabled", "disabled");
                    $("#txtClassDate").attr("disabled", "disabled");
                    $("#fieldAddClass").width(950);
                    if (mode == 1) {
                        $("#txtWeekNo").removeAttr("disabled");
                    }
                    $("#lblSubtituteCoach").text("Substitute Coach:");
                    listMakeupSubstituteCoaches();
                } else if (classType == "Makeup") {
                    $("#dvSubstitute").hide();
                    $("#selTime").removeAttr("disabled");
                    $("#txtClassDate").removeAttr("disabled");
                    if (mode == 1) {
                        $("#fieldAddClass").width(850);
                        $("#txtWeekNo").removeAttr("disabled");
                    } else if (mode == 2) {
                        $("#dvSubstitute").show();
                        listMakeupSubstituteCoaches();
                        $("#fieldAddClass").width(950);
                    }
                    $("#lblSubtituteCoach").text("Substitute Coach:");



                    //listMakeupSubstituteCoaches();
                } else if (classType == "Extra") {
                    $("#selTime").removeAttr("disabled");
                    $("#txtClassDate").removeAttr("disabled");
                    if (mode == 1) {
                        $("#txtWeekNo").removeAttr("disabled");
                    }
                    $("#dvSubstitute").hide();
                    $("#fieldAddClass").width(850);
                    if (mode == 2) {
                        $("#dvSubstitute").show();
                        listMakeupSubstituteCoaches();
                        $("#fieldAddClass").width(950);
                    }
                    $("#lblSubtituteCoach").text("Substitute Coach:");
                } else if (classType == "Makeup Substitute") {
                    $("#fieldAddClass").width(950);
                    $("#dvSubstitute").show();
                    $("#selTime").removeAttr("disabled");
                    $("#txtClassDate").removeAttr("disabled");
                    if (mode == 1) {
                        $("#txtWeekNo").removeAttr("disabled");
                    }
                    $("#lblSubtituteCoach").text("Makeup Substitute Coach:");

                    listMakeupSubstituteCoaches();
                } else if (classType == "Holiday") {
                    $("#fieldAddClass").width(850);
                    $("#dvSubstitute").hide();
                    $("#selTime").attr("disabled", "disabled");
                    $("#txtClassDate").attr("disabled", "disabled");

                    $("#txtWeekNo").attr("disabled", "disabled");
                    if (mode == 1) {
                        getWeekNo();
                    } else if (mode == 2) {

                        $("#dvSubstitute").show();
                        $("#lblSubtituteCoach").text("Substitute Coach:");
                        $("#fieldAddClass").width(950);
                        listMakeupSubstituteCoaches();
                    }
                } else {
                    $("#fieldAddClass").width(850);
                    $("#dvSubstitute").hide();
                    $("#selTime").attr("disabled", "disabled");
                    $("#txtClassDate").attr("disabled", "disabled");

                    $("#txtWeekNo").attr("disabled", "disabled");
                    if (mode == 1) {
                        getWeekNo();
                    } else if (mode == 2) {

                        $("#dvSubstitute").show();
                        $("#lblSubtituteCoach").text("Substitute Coach:");
                        $("#fieldAddClass").width(950);
                        listMakeupSubstituteCoaches();
                    }
                }
            }

            $(document).on("change", "#selClassType", function (e) {

                var classType = $(this).val();
                if (classType != "0") {
                    if (classType == "Makeup") {
                        $("#selSubCoach").val("0");
                        $("#selStatus").val("On");
                        $("#txtWeekNo").val("");
                        $("#txtHwRelDate").val("");
                        $("#txtSRelDate").val("");
                        $("#txtDueDate").val("");
                        $("#txtAnsDate").val("");
                        $("#selTime").val("0");
                        $("#txtClassDate").val("");
                        $("#dvClassDate").show();
                        $("#dvClassTime").show();
                    } else if (classType == "Extra") {
                        $("#selSubCoach").val("0");
                        $("#selStatus").val("On");
                        $("#txtWeekNo").val("");
                        $("#txtHwRelDate").val("");
                        $("#txtSRelDate").val("");
                        $("#txtDueDate").val("");
                        $("#txtAnsDate").val("");
                        $("#selTime").val("0");
                        $("#txtClassDate").val("");
                        $("#dvClassDate").show();
                        $("#dvClassTime").show();
                        $("#dvSubstitute").hide();
                    } else if (classType == "Makeup Substitute") {
                        $("#selSubCoach").val("0");
                        $("#selStatus").val("On");
                        $("#txtWeekNo").val("");
                        $("#txtHwRelDate").val("");
                        $("#txtSRelDate").val("");
                        $("#txtDueDate").val("");
                        $("#txtAnsDate").val("");
                        $("#selTime").val("0");
                        $("#txtClassDate").val("");
                        $("#dvClassDate").show();
                        $("#dvClassTime").show();
                    }
                    showHideDateTime(1);
                } else {
                    clear();
                }
            });

            function verifyRegularClasCancelled() {
                var classType = $("#selClassType").val();
                var status;
                showLoader();
                if (classType == "Substitute") {
                    var memberID = $("#selCoach").val();
                    var eventId = $("#selEvent").val();
                    var eventyear = $("#selEventyear").val();
                    var pgId = $("#selProductGroup").val();
                    var pgcode = $("#selProductGroup option:selected").text();
                    var pId = $("#selProduct").val();
                    var pCode = $("#selProduct option:selected").text();
                    var level = $("#selLevel").val();
                    var sessionNo = $("#selSession").val();
                    var semester = $("#selSemester").val();
                    var weekNo = $("#txtWeekNo").val();
                    var classType = "Regular";

                    var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, WeekNo: weekNo, ClassType: classType } });


                    $.ajax({
                        type: "POST",
                        url: "CoachClassCalNew.aspx/VerifyRegularClassStatus",
                        data: jsonData,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            hideLoader();
                            if (JSON.stringify(data.d.length) > 0) {
                                $.each(data.d, function (index, value) {
                                    status = value.Status;
                                    $("#hdnStatus").val(status);

                                    if (status == "Cancelled") {
                                        postNewClass()
                                    } else {

                                        statusMessage("Substitute class can only be created if the regular class with the regular coach is cancelled or the makeup class is cancelled.", "error");
                                    }
                                });
                            }
                        }, failure: function (e) {
                            hideLoader();
                        }
                    });

                }

                return status;
            }

            $(document).on("click", "#btnCancel", function (e) {

                clear();

            });

            function clear() {

                $("#selClassType").val("0");
                $("#selSubCoach").val("0");
                $("#selStatus").val("On");
                $("#txtWeekNo").val("");
                $("#txtHwRelDate").val("");
                $("#txtSRelDate").val("");
                $("#txtDueDate").val("");
                $("#txtAnsDate").val("");
                $("#selTime").val("0");
                $("#txtClassDate").val("");
                $("#dvClassDate").show();
                $("#dvClassTime").show();
                $("#dvSubstitute").hide();
            }
            function loadScheduleBaseonId(coachClassCalID) {
                showLoader();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();


                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, Semester: semester, Level: level, SessionNo: sessionNo, CoachClassCalID: coachClassCalID } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListScheduleBasedOnId",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {

                                $("#selClassType").val(value.ClassType);
                                $("#hdnClassType").val(value.ClassType);

                                $("#selStatus").val(value.Status);
                                $("#hdnPriorStatus").val(value.Status);
                                $("#txtWeekNo").val(value.WeekNo);
                                $("#txtClassDate").val(value.Date);
                                $("#selTime").val(value.Time);
                                var substituteId = value.Substitute;
                                $("#hdnSubstituteCoachId").val(substituteId);

                                $("#dvSubstitute").show();
                                // $("#fieldAddClass").css("width", "940");
                                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;

                                if (roleID == "88") {
                                    $("#selClassType").attr("disabled", "disabled");
                                }


                                $("#txtWeekNo").attr("disabled", "disabled");
                                $("#txtClassDate").attr("disabled", "disabled");
                                $("#selTime").attr("disabled", "disabled");
                                if (value.ClassType != "Extra") {
                                    //$("#txtHwRelDate").val(value.HWDeadlineDate);
                                    //$("#txtDueDate").val(value.HWDueDate);
                                    //$("#txtSRelDate").val(value.SRelDate);
                                    //$("#txtAnsDate").val(value.ARelDate);

                                    $("#dvHWDate").datepicker("update", value.HWDeadlineDate);
                                    $("#dvDueDate").datepicker("update", value.HWDueDate);
                                    $("#dvARelDate").datepicker("update", value.ARelDate);
                                    $("#dvSRelDate").datepicker("update", value.SRelDate);
                                }
                                showHideDateTime(2);

                            });
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            $(document).on("click", ".modify", function (e) {
                var coachClassCalID = $(this).attr("attr-coachclassid");

                var classType = $(this).attr("attr-ctype");
                $('#hdnClassType').val(classType);

                var classDate = $(this).attr("attr-date");
                var classTime = $(this).attr("attr-time");


                $("#hdnDate").val(classDate);
                $("#hdnTime").val(classTime);
                var weekNo = $(this).attr("WeekNo");
                $("#hdnCoachClassCalID").val(coachClassCalID)
                $("#hdnWeekNo").val(weekNo);
                if (classType != "Holiday" && coachClassCalID != "0") {
                    $("#hdnCoachClassCalID").val(coachClassCalID);

                    loadScheduleBaseonId(coachClassCalID);
                } else if (classType == "Holiday" && coachClassCalID > 0) {
                    $("#hdnCoachClassCalID").val(coachClassCalID);

                    loadScheduleBaseonId(coachClassCalID);

                } else {
                    $("#hdnCoachClassCalID").val(coachClassCalID);
                    $("#hdnClassType").val("Holiday");
                    var date = $(this).attr("attr-Date");
                    var time = $(this).attr("attr-Time");
                    getWeekNoForHolidayCLass(date, time);
                }
            });


            function statusMessage(message, type) {

                $.toast({
                    // heading: 'Can I add <em>icons</em>?',
                    text: '<b>' + message + '</b>',
                    icon: type,
                    position: 'top-center',
                    hideAfter: 12000,
                    stack: 1

                })


            }

            function validateAddClass() {
                var retVal = "1";
                var msg = "";
                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();
                var status = $("#selStatus").val();
                var weekNo = $("#txtWeekNo").val();
                var substitute = $("#selSubCoach").val();


                if (eventyear == "0") {
                    retVal = "-1";
                    msg = "Please select Event year";
                } else if (semester == "0") {
                    retVal = "-1";
                    msg = "Please select Semester";
                } else if (memberID == "0") {
                    retVal = "-1";
                    msg = "Please select Coach";
                } else if (pgId == "0") {
                    retVal = "-1";
                    msg = "Please select ProductGroup";
                } else if (pId == "0") {
                    retVal = "-1";
                    msg = "Please select Product";
                } else if (level == "0") {
                    retVal = "-1";
                    msg = "Please select Level";
                } else if (sessionNo == "0") {
                    retVal = "-1";
                    msg = "Please select sessionNo";
                } else if (classType == "0") {
                    retVal = "-1";
                    msg = "Please select Class Type";
                } else if (classType == "Regular" || classType == "Makeup" || classType == "Extra") {
                    if (date == "") {
                        retVal = "-1";
                        msg = "Please enter Date";
                    }
                } else if (classType == "Regular" || classType == "Makeup" || classType == "Extra") {
                    if (time == "0") {
                        retVal = "-1";
                        msg = "Please enter Time";
                    }
                } else if (weekNo == "") {
                    retVal = "-1";
                    msg = "Please enter Week No";
                } else if (status == "0") {
                    retVal = "-1";
                    msg = "Please select Status";
                } else if (classType == "Substitute" && substitute == "0") {

                    retVal = "-1";
                    msg = "Please select Substitute Coach";
                } else if (classType == "Makeup Substitute" && substitute == "0") {
                    retVal = "-1";
                    msg = "Please select Makeup Substitute Coach";
                }
                if (msg != "") {
                    statusMessage(msg, "error");
                }
                return retVal;

            }

            function updateCoachClassSchedule(isWeekNoReset) {
                var coachClassCalID = $("#hdnCoachClassCalID").val();
                var status = $("#selStatus").val();
                var sessioNo = $("#selSession").val();
                var hrelDate = $("#txtHwRelDate").val();
                var dueDate = $("#txtDueDate").val();
                var sRelDate = $("#txtSRelDate").val();
                var aRelDate = $("#txtAnsDate").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();
                var session = $("#selSession").val();
                var memberId = $("#selCoach").val();
                var substitute = $("#selSubCoach").val();
                var classType = $("#selClassType").val();

                if (status == "Cancelled") {
                    hrelDate = null;
                    dueDate = null;
                    sRelDate = null;
                    aRelDate = null;
                }
                if (substitute > 0) {
                    substitute = $("#selSubCoach").val();
                } else {
                    substitute = 0;
                }
                var makeUpCoachID = 0;

                //if (classType == "Makeup") {
                //    makeUpCoachID = $("#selSubCoach").val();
                //}
                var weekNoStimulation = "";
                var priorStatus = $("#hdnPriorStatus").val();

                if (priorStatus == "Cancelled" && status == "On") {
                    weekNoStimulation = "increment";
                } else if (priorStatus == "On" && status == "Cancelled") {
                    weekNoStimulation = "decrement";
                }
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                showLoader();
                var jsonData = JSON.stringify({ objCoachClass: { CoachClassCalID: coachClassCalID, Status: status, EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, WeekNo: weekNo, Level: level, Semester: semester, SessionNo: session, MemberId: memberId, Substitute: substitute, Makeup: makeUpCoachID, Date: date, Time: time, ClassType: classType, WeekNoReset: isWeekNoReset, WeekNoStimulation: weekNoStimulation, Date: date, CreatedBy: loginID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/UpdateCoachClassSchedule",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                var coachPaperId = value.CoachpaperID;
                                var coachRelId = value.CoachRelID;

                                //if (classType == "Makeup" || classType == "Extra") {
                                //    checkVRoomAvailability();
                                //}

                                if (value.CoachpaperID > 0) {
                                    if (isWeekNoReset == "Yes") {
                                    } else {

                                        if (parseInt(coachPaperId) > 0 && classType != "Extra") {
                                            if (parseInt(coachRelId) > 0) {
                                                UpdateReleaseDates(coachRelId);
                                            } else {

                                                inserReleaseDates(coachPaperId);
                                            }
                                        }

                                        loadSchedule(0);
                                        $("#spnGridTitle").show();
                                        statusMessage("Class added successfully!", "success");
                                        clear();
                                        $("#dvAddClass").hide();

                                        //UpdateReleaseDates(value.CoachRelID);
                                    }
                                } else {
                                    loadSchedule(0);
                                    $("#spnGridTitle").show();
                                    statusMessage("Class updated successfully!", "success");
                                    $("#dvAddClass").hide();
                                }
                            });

                            loadSchedule(0);
                            $("#dvAddClass").hide();
                            $.each(jsonClassSchedule, function (index, value) {
                                $('.tr' + value.WeekNo + '').removeClass("invalid");
                            });
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function UpdateReleaseDates(coachRelID) {

                var coachClassCalID = $("#hdnCoachClassCalID").val();
                var status = $("#selStatus").val();
                var sessioNo = $("#selSession").val();
                var hrelDate = $("#txtHwRelDate").val();
                var dueDate = $("#txtDueDate").val();
                var sRelDate = $("#txtSRelDate").val();
                var aRelDate = $("#txtAnsDate").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;


                showLoader();
                var jsonData = JSON.stringify({ objCoachClass: { SessionNo: sessioNo, HWDeadlineDate: hrelDate, HWDueDate: dueDate, ARelDate: aRelDate, SRelDate: sRelDate, LoginID: loginID, CoachRelID: coachRelID } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/UpdateCoachRelDate",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d) == 1) {

                            $("#spnGridTitle").show();
                            statusMessage("Class updated successfully!", "success");
                            loadSchedule(0);
                            clear();
                            $("#dvAddClass").hide();
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function getClassStartDateAndEndDate() {

                var classType = $("#selClassType").val();
                var status;

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                showLoader();
                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, WeekNo: weekNo, ClassType: classType, CreatedBy: loginID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetClassStartDateAndEndDate",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                var day = value.Day;
                                var startDate = value.ClassStartDate;
                                var endDate = value.ClassEndDate;
                                var hDueDate = value.HWDueDate;
                                var aRelDate = value.ARelDate;
                                var sRelDate = value.SRelDate;
                                $("#txtClassDate").val(startDate);
                                $("#selTime").val(value.Time);
                                $("#hdnDay").val(day);

                                $("#dvHWDate").datepicker("update", startDate);
                                $("#dvDueDate").datepicker("update", hDueDate);
                                $("#dvARelDate").datepicker("update", aRelDate);
                                $("#dvSRelDate").datepicker("update", sRelDate);

                            });
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });

                return status;
            }


            $(document).on("keyup", "#txtWeekNo", function (e) {
                if ($(this).val().length > 0) {

                    if ($("#selClassType").val() == "Substitute" || $("#selClassType").val() == "Makeup" || $("#selClassType").val() == "Extra" || $("#selClassType").val() == "Makeup Substitute") {

                        checkWeekNoExists();

                    }
                }
            });

            function getWeekNo() {
                var classType = $("#selClassType").val();
                var status = "On";

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                showLoader();

                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, ClassType: classType, Status: status } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetWeekNo",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                var weekNo = parseInt(value.WeekNo) + 1;
                                // alert(weekNo);
                                $("#txtWeekNo").val(weekNo);
                                getClassStartDateAndEndDate();
                            });
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function inserReleaseDates(coachPaperID) {

                var classType = $("#selClassType").val();
                var status = "On";

                var memberID = $("#selCoach").val();

                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var hwRelDate = $("#txtHwRelDate").val();
                var hwDueDate = $("#txtDueDate").val();
                var aRelDate = $("#txtAnsDate").val();
                var sRelDate = $("#txtSRelDate").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;

                showLoader();

                var jsonData = JSON.stringify({ objCoachClass: { CoachpaperID: coachPaperID, SessionNo: sessionNo, Semester: semester, HWDeadlineDate: hwRelDate, HWDueDate: hwDueDate, ARelDate: aRelDate, SRelDate: sRelDate, LoginID: loginID, MemberId: memberID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/InsertCoachRelDate",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            statusMessage("Class added successfully!", "success");

                            clear();

                            loadSchedule(0);
                            $("#spnGridTitle").show();
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });

            }

            function listCoaches() {


                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;

                if (roleID != "88") {
                    loginID = 0;
                }

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: loginID } });
                $("#selCoach").empty();
                $("#selCoach").html("<option value='0'>Select</option")
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListCoaches",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        var coachId;
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {

                                $("#selCoach").append($("<option></option>").val
                                    (value.MemberID).html(value.Coachname));
                                coachId = value.MemberID;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selCoach").removeAttr("disabled");
                                if (glCoachId != null) {
                                    $("#selCoach").val(glCoachId);
                                    listProductGroups();
                                }
                            } else {

                                $("#selCoach").val(coachId);

                                $("#selCoach").attr("disabled", "disabled");
                                listProductGroups();
                            }
                        } else {
                            $("#selCoach").html("<option value='0'>Select</option>");
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });

            }
            function listProductGroups() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                $("#selProductGroup").empty();

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID } });

                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListproductGroups",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {
                            var productGroupID;
                            $.each(data.d, function (index, value) {
                                $("#selProductGroup").append($("<option></option>").val
                                    (value.ProductGroupId).html(value.ProductGroup));
                                productGroupID = value.ProductGroupId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selProductGroup").removeAttr("disabled");
                                if (glPgId != null) {
                                    $("#selProductGroup").val(glPgId);
                                }
                                listProducts();
                            } else {
                                $("#selProductGroup").val(productGroupID);
                                $("#selProductGroup").attr("disabled", "disabled");
                                listProducts();
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }
            function listProducts() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                $("#selProduct").empty();
                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID } });

                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListProducts",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {
                            var productId;
                            $.each(data.d, function (index, value) {
                                $("#selProduct").append($("<option></option>").val
                                    (value.ProductId).html(value.Product));
                                productId = value.ProductId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selProduct").removeAttr("disabled");
                                if (glPid != null) {
                                    $("#selProduct").val(glPid);
                                }
                                listlevels();
                            } else {
                                $("#selProduct").val(productId);
                                $("#selProduct").attr("disabled", "disabled");
                                listlevels();
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }
            function listlevels() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                var productId = $("#selProduct").val();
                showLoader();
                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID, ProductId: productId } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListLevel",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {
                            var level;
                            $.each(data.d, function (index, value) {
                                $("#selLevel").append($("<option></option>").val
                                    (value.Level).html(value.Level));
                                level = value.Level;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selLevel").removeAttr("disabled");
                                if (gllevel != null) {
                                    $("#selLevel").val(gllevel);
                                }
                                listSessionNO();
                            } else {
                                $("#selLevel").val(level);
                                $("#selLevel").attr("disabled", "disabled");
                                listSessionNO();
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }
            function listSessionNO() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                var productId = $("#selProduct").val();
                var level = $("#selLevel").val();
                showLoader();
                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID, ProductId: productId, Level: level } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListSessionNo",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {
                            var sessionNo;
                            $.each(data.d, function (index, value) {
                                $("#selSession").append($("<option></option>").val
                                    (value.SessionNo).html(value.SessionNo));
                                sessionNo = value.SessionNo;
                            });

                            if (JSON.stringify(data.d.length) > 1) {
                                if (glSessionNo != null) {
                                    $("#selSession").val(glSessionNo);
                                }
                                $("#selSession").removeAttr("disabled");
                                if (glPgId != null && glPid != null && gllevel != null && glSessionNo != null) {
                                    loadSchedule(0);
                                }
                            } else {
                                $("#selSession").val(sessionNo);
                                $("#selSession").attr("disabled", "disabled");
                                loadSchedule(0);
                                if (glPgId != null && glPid != null && gllevel != null && glSessionNo != null) {
                                    loadSchedule(0);
                                }
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            $(document).on("change", "#selCoach", function (e) {
                $("#dvAddClass").hide();
                listProductGroups();
            });
            $(document).on("change", "#selProductGroup", function (e) {
                $("#dvAddClass").hide();
                listProducts();
            });
            $(document).on("change", "#selProduct", function (e) {
                $("#dvAddClass").hide();
                listlevels();
            });
            $(document).on("change", "#selLevel", function (e) {
                $("#dvAddClass").hide();
                listSessionNO();
            });
            $(document).on("change", "#selSession", function (e) {
                $("#dvAddClass").hide();
                loadSchedule(0);
            });

            function showPage() {
                document.getElementById("loader").style.display = "none";
                document.getElementById("myDiv").style.display = "block";
            }

            function validateSubmit() {

                var retval = "1";

                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                if (eventyear == "0") {
                    retval = "-1";
                    msg = "Please select Event year";
                } else if (semester == "0") {
                    retval = "-1";
                    msg = "Please select Semester";
                } else if (memberID == "0") {
                    retval = "-1";
                    msg = "Please select Coach";
                } else if (pgId == "0") {
                    retval = "-1";
                    msg = "Please select ProductGroup";
                } else if (pId == "0") {
                    retval = "-1";
                    msg = "Please select Product";
                } else if (level == "0") {
                    retval = "-1";
                    msg = "Please select Level";
                } else if (sessionNo == "0") {
                    retval = "-1";
                    msg = "Please select sessionNo";
                }
                if (retval == "-1") {
                    statusMessage(msg, "error");
                }
                return retval;

            }

            function checkWeekNoExists() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                var productId = $("#selProduct").val();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var weekNo = $("#txtWeekNo").val();
                var classType = $("#selClassType").val();

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID, ProductId: productId, Level: level, SessionNo: sessionNo, WeekNo: weekNo, ClassType: classType } });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/CheckWeekNoExists",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d) > 0) {
                            if (classType != "Extra" && classType != "Makeup") {
                                getClassStartDateAndEndDate();
                            }
                        } else {
                            var weekNo = $("#txtWeekNo").val();
                            if (classType == "Makeup Substitute") {
                                statusMessage("There is no Makeup class for the week #" + weekNo + "", "error");
                            } else {
                                statusMessage("Regular class not yet added for the week #" + weekNo + "", "error");
                            }

                            $("#txtWeekNo").val("");
                        }

                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function checkDuplicateClass() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                var productId = $("#selProduct").val();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var weekNo = $("#txtWeekNo").val();
                var classType = $("#selClassType").val();
                var status = $("#selStatus").val();
                var classDate = $("#txtClassDate").val();
                showLoader();
                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID, ProductId: productId, Level: level, SessionNo: sessionNo, WeekNo: weekNo, ClassType: classType, Status: status, ClassDate: classDate } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/CheckDuplicateClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        var msg = "";
                        if (JSON.stringify(data.d) > 0) {

                            msg = "Duplicate exists";

                            statusMessage(msg, "error");

                        } else {
                            if (JSON.stringify(data.d) == -2) {

                                msg = "You cannot schedule substitute session since the recurring class date is already passed.";
                                statusMessage(msg, "error");

                            } else {

                                verifyRegularClasCancelled();
                            }

                        }

                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function deleteClass(coachClassCalID, hostId, sessionKey, weekNo) {

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                var session = $("#selSession").val();
                var memberId = $("#selCoach").val();
                var substitute = $("#selSubCoach").val();
                var classType = $("#selClassType").val();

                var jsonData = JSON.stringify({ objCoachClass: { CoachClassCalID: coachClassCalID, EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, WeekNo: weekNo, Level: level, Semester: semester, SessionNo: session, MemberId: memberId, ClassType: classType } });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/DeleteClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            statusMessage("Deleted successfully", "success");
                            if (hostId != "" && sessionKey != "") {
                                deleteZoomSessions(sessionKey, hostId);
                            }
                            loadSchedule(0);
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            $(document).on("click", ".delete", function (e) {
                var coachClassCalID = $(this).attr("attr-CoachClassID");
                var classType = $(this).attr("attr-ClassType");
                var classDate = $(this).attr("attr-ClassDate");
                var weekNo = $(this).attr("WeekNo");

                if (confirm("Are you sure want to delete?")) {

                    if (classType == "Makeup" || classType == "Extra") {
                        getHostAndSessionKey(coachClassCalID, classType, classDate, weekNo);
                    } else {
                        deleteClass(coachClassCalID, "", "", weekNo);
                    }
                }
            });

            function getHostAndSessionKey(coachClassCalID, classType, classDate, weekNo) {
                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                var jsonData = JSON.stringify({ EventYear: eventyear, Semester: semester, CoachId: memberID, PgId: pgId, PId: pId, Level: level, Session: sessionNo, ClassType: classType, ClassDate: classDate });

                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetHostIdAndSessionKey",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {
                            var sessionKey = "";
                            var hostId = "";
                            $.each(data.d, function (index, value) {
                                sessionKey = value.SessionKey;
                                hostId = value.HostID;
                            });
                            deleteClass(coachClassCalID, hostId, sessionKey, weekNo);
                        }
                    }
                });
            }

            function validateMakeupSessions(mode) {

                var classDate = $("#txtClassDate").val();
                var classTime = $("#selTime").val();


                var classType = $("#selClassType").val();
                var status;

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();


                var jsonData = JSON.stringify({ objCoachClass: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, Date: classDate, Time: classTime, WeekNo: weekNo, ClassType: classType, Mode: mode } });


                showLoader();

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ValidatemakeupExtraClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.RetVal == "1") {

                                    if ($("#hdnCoachClassCalID").val() != "0") {
                                        if (validateAddClass() == "1") {
                                            var status = $("#selStatus").val();
                                            // if (status == "Cancelled") {
                                            getConfirmationForWeekNo();
                                            // } else {
                                            //     updateCoachClassSchedule("No");
                                            // }

                                        }
                                    } else {
                                        if (validateAddClass() == "1") {

                                            checkVRoomAvailability();
                                            //if (checkVRoomAvailability() != "") {
                                            //    postNewClass();

                                            //}
                                        }
                                    }

                                } else if (value.RetVal == "-1") {
                                    if (classType == "Makeup") {
                                        statusMessage("Makeup class date should be greater than or equal to current date and Makeup class time should be greater than current time.", "error");
                                    } else {
                                        statusMessage("Extra class date should be greater than or equal to current date and Extra class time should be greater than current time.", "error");

                                    }
                                } else if (value.RetVal == "-2") {
                                    if (classType == "Makeup") {
                                        statusMessage("Makeup class time and Regular class time should not be same.", "error");
                                    } else {
                                        statusMessage("Extra class time and Regular class time should not be same.", "error");
                                    }
                                } else if (value.RetVal == "-3") {
                                    statusMessage("There cannot be two makeups for the same week.", "error");
                                } else if (value.RetVal == "-4") {
                                    statusMessage("Makeup class can be created only if there is a Regular/Extra/Subsitute class for the week.", "error");
                                } else if (value.RetVal == "-5") {
                                    statusMessage("Makeup class can only be created if the regular class with the regular coach is cancelled or the subtitute class is cancelled.", "error");
                                }
                            });
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });


            }

            function listMakeupSubstituteCoaches() {

                $("#selSubCoach").empty();
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;
                var memberID = $("#selCoach").val();


                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID } });

                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListMakeupSubstituteCoaches",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        var coachId;
                        if (JSON.stringify(data.d.length) > 0) {

                            $("#selSubCoach").append($("<option></option>").val
                                ("0").html("Select"));
                            $.each(data.d, function (index, value) {

                                $("#selSubCoach").append($("<option></option>").val
                                    (value.MemberID).html(value.Coachname));

                            });

                            var submakeupId = $("#hdnSubstituteCoachId").val();
                            if (submakeupId != "0" && submakeupId != "") {
                                $("#selSubCoach").val(submakeupId);
                            } else {
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });

            }

            function validateExtraSessions(mode) {

                var classDate = $("#txtClassDate").val();
                var classTime = $("#selTime").val();


                var classType = $("#selClassType").val();
                var status;

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();


                var jsonData = JSON.stringify({ objCoachClass: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, Date: classDate, Time: classTime, WeekNo: weekNo, ClassType: classType, Mode: mode } });


                showLoader();

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ValidateExtraClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.RetVal == "1") {

                                    if ($("#hdnCoachClassCalID").val() != "0") {
                                        if (validateAddClass() == "1") {
                                            var status = $("#selStatus").val();
                                            //// if (status == "Cancelled") {
                                            //getConfirmationForWeekNo();
                                            //// } else {
                                            ////    updateCoachClassSchedule();
                                            ////}

                                            if (($("#hdnDate").val() != classDate || $("#hdnTime").val() != classTime) && status == "On") {

                                                deleteMakeupClass("Extra");
                                            } else {
                                                updateCoachClassSchedule("No");
                                            }
                                        }
                                    } else {
                                        if (validateAddClass() == "1") {
                                            checkVRoomAvailability();
                                            //if (checkVRoomAvailability() != "") {
                                            //    postNewClass();

                                            //}
                                            // postNewClass();
                                        }
                                    }

                                } else if (value.RetVal == "-1") {
                                    if (classType == "Makeup") {
                                        statusMessage("Makeup class date should be greater than or equal to current date and Makeup class time should be greater than current time.", "error");
                                    } else {
                                        statusMessage("Extra class date should be greater than or equal to current date and Extra class time should be greater than current time.", "error");

                                    }
                                } else if (value.RetVal == "-2") {
                                    if (classType == "Makeup") {
                                        statusMessage("Makeup class time and Regular class time should not be same.", "error");
                                    } else {
                                        statusMessage("Extra class time and Regular class time should not be same.", "error");
                                    }
                                } else if (value.RetVal == "-4") {
                                    statusMessage("Extra class can be created only if there is a Regular/Makeup/Subsitute class for the week.", "error");
                                } else if (value.RetVal == "-5") {
                                    statusMessage("Extra class already exists on the same date and time.", "error");
                                }

                            });
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });


            }

            function makeZoomSessions(hostId, userId, pwd, duration) {
                var classDate = $("#txtClassDate").val();
                var classTime = $("#selTime").val();

                var topic = "";
                var apiKey = "";
                var apiSecret = "";


                var coachName = $("#selCoach option:selected").text();


                var pgcode = $("#selProductGroup option:selected").text();

                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                topic = coachName + "-" + pCode + "-" + level + "-" + sessionNo;

                var jsonData = JSON.stringify({ objZoomSess: { StartDate: classDate, StartTime: classTime, Duration: duration, Topic: topic, APIKey: apiKey, APISecret: apiSecret, HostID: hostId } });


                showLoader();



                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/makeZoomSessions",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.Retval == 1) {


                                    saveMakeupSessions(value.SessionKey, value.HostURL, value.JoinURl, hostId, userId, pwd, duration);

                                }
                            });
                        }
                        // clear();
                    }
                });

            }

            function saveMakeupSessions(sessionKey, hostURL, joinURL, hostId, userId, pwd, duration) {
                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();
                //var userId = userId;
                //var pwd = pwd;



                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;


                if ($("#selSubCoach").val() != "0") {
                    makeUpCoachID = $("#selSubCoach").val();
                } else {
                    makeUpCoachID = memberID;
                }

                showLoader();
                var jsonData = JSON.stringify({ objCoachClass: { EventYear: eventyear, EventID: eventId, ChapterID: 13, ProductGroupID: pgId, ProductGroup: pgcode, ProductID: pId, Product: pCode, MemberID: memberID, SessionKey: sessionKey, StartDate: date, EndDate: date, StartTime: time, StartTime: time, Semester: semester, Level: level, SessionNo: sessionNo, Duration: duration, HostURL: hostURL, UserID: userId, Pwd: pwd, JoinURl: joinURL, HostID: hostId, Date: date } });
                // var classType = $("#selClassType").val();
                var url = "";
                if (classType == "Makeup") {
                    url = "SaveMakeupSessions";
                } else if (classType == "Extra") {
                    url = "SaveExtraSessions";
                }
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/" + url + "",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.RetVal == 1) {

                                }
                            });
                            postNewClass();
                        }
                        //  clear();
                    }
                });

            }

            function deleteZoomSessions(sessionKey, hostId) {

                var jsonData = JSON.stringify({ objZoomSess: { SessionKey: sessionKey, HostID: hostId } });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/DeleteZoomSessions",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        $.each(data.d, function (index, value) {
                            if (value.Retval == 1) {
                                statusMessage("Deleted successfully", "success");
                            }
                        });

                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function checkVRoomAvailability() {
                var eventYear = $("#selEventyear").val();
                var startTime = $("#selTime").val();
                var endTime = $("#selTime").val();
                var pgId = $("#selProductGroup").val();
                var pId = $("#selProduct").val();
                var date = $("#txtClassDate").val();
                var hostId = "";
                var userId = "";
                var pwd = "";
                var duration = 120;
                var jsonData = JSON.stringify({ objCoachClass: { EventYear: eventYear, StrStartTime: startTime, ProductGroupId: pgId, ProductId: pId, Date: date } });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ValidateVroomValidation",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        var x = JSON.stringify(data.d);
                        if (x.length > 0) {
                            $.each(data.d, function (index, value) {

                                hostId = value.HostID;
                                userId = value.UserID;
                                pwd = value.Pwd;
                                duration = value.Duration;
                                $("#hdnDuration").val(value.Duration);
                                $("#hdnUserID").val(value.UserID);
                                $("#hdnPwd").val(value.Pwd);

                            });

                            makeZoomSessions(hostId, userId, pwd, duration);
                        } else {
                            statusMessage("No vroom is available for the time slot. Please choose different time.", "error");
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });

                return hostId;
            }

            function getYearAndSemester() {


                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetyearAndSemester",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (index, value) {
                            $("#selEventyear").val(value.EventYear);
                            $("#selSemester").val(value.Semester);
                        });
                        var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;
                        if (roleID == "88") {
                            var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                            getCurrenyearAndSemester(loginID);
                        } else {

                            listCoaches();
                        }
                    }
                });
            }

            $(document).on("change", "#selSemester", function (e) {

                listCoaches();;
            });

            $(document).on("change", "#selEventyear", function (e) {

                listCoaches();;
            });

            function getConfirmationForWeekNo() {
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();
                var memberId = $("#selCoach").val();
                var status = $("#selStatus").val();
                var priorStatus = $("#hdnPriorStatus").val();
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();

                var jsonData = JSON.stringify({ objCoachClass: { EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, WeekNo: weekNo, Level: level, Semester: semester, SessionNo: sessionNo, MemberId: memberId, Status: status } });

                var consecutiveWeekNo = 0;
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetWeekNoConfirmation",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();

                        consecutiveWeekNo = JSON.stringify(data.d)

                        if (consecutiveWeekNo > 0 && priorStatus != status) {
                            $.each(jsonClassSchedule, function (index, value) {

                                if (value.WeekNo != "") {
                                    $('.tr' + value.WeekNo + '').addClass("invalid");
                                }
                            });
                            if (confirm("Week numbers will be reset if out of sequence class cancellations/On happen. Are you sure to continue?")) {


                                if (classType == "Makeup" && ($("#hdnDate").val() != date || $("#hdnTime").val() != time) && status == "On") {
                                    deleteMakeupClass("Scheduled Meeting");

                                } else {
                                    updateCoachClassSchedule("Yes");
                                }
                            } else {
                                $.each(jsonClassSchedule, function (index, value) {
                                    $('.tr' + value.WeekNo + '').removeClass("invalid");
                                });
                            }
                        } else if (consecutiveWeekNo == -2) {
                            statusMessage("Two active classes should not be scheduled for a single week.", "error");

                        } else {

                            var isWeekNoRest = "No";

                            if (classType == "Makeup" && ($("#hdnDate").val() != date || $("#hdnTime").val() != time) && status == "On") {

                                deleteMakeupClass("Scheduled Meeting");
                            } else {
                                updateCoachClassSchedule("No");
                            }
                        }
                    }

                });


            }

            function deleteMakeupClass(classType) {

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                //classType = "Scheduled Meeting";
                var classDate = $("#txtClassDate").val();
                var coachClassCalId = $("#hdnCoachClassCalID").val();

                var jsonData = JSON.stringify({ EventYear: eventyear, Semester: semester, CoachId: memberID, PgId: pgId, PId: pId, Level: level, Session: sessionNo, ClassType: classType, ClassDate: $("#hdnDate").val() });

                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetHostIdAndSessionKey",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {
                            var sessionKey = "";
                            var hostId = "";
                            $.each(data.d, function (index, value) {
                                sessionKey = value.SessionKey;
                                hostId = value.HostID;
                            });
                            deleteMakeupfromZoom(coachClassCalId, sessionKey, hostId);
                        }
                    }
                });
            }

            function deleteMakeupfromZoom(coachclasscalId, SessionKey, hostId) {

                var jsonData = JSON.stringify({ objZoomSess: { SessionKey: SessionKey, HostID: hostId } });
                showLoader();
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/DeleteZoomSessions",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();

                        deleteMakeupfromNSF(coachclasscalId, SessionKey);

                    }, failure: function (e) {
                        hideLoader();
                    }
                });

            }

            function deleteMakeupfromNSF(coachclasscalId, SessionKey) {

                var jsonData = JSON.stringify({ SessionKey: SessionKey, CoachClassCalID: coachclasscalId });
                showLoader();

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/DeletemakeupFromNSF",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        hideLoader();
                        checkVRoomAvailability();

                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function validateMakeupSubstituteSessions(mode) {

                var classDate = $("#txtClassDate").val();
                var classTime = $("#selTime").val();


                var classType = $("#selClassType").val();
                var status;

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();


                var jsonData = JSON.stringify({ objCoachClass: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, Date: classDate, Time: classTime, WeekNo: weekNo, ClassType: classType, Mode: mode } });


                showLoader();

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ValidateMakeupSubstitute",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.RetVal == "1") {
                                    postNewClass();
                                } else if (value.RetVal == "-2") {
                                    statusMessage("Makeup Substitute class can only be created if the Makeup class with the regular coach is cancelled.", "error");
                                }
                            });
                        }
                    }
                });
            }


            function getWeekNoForHolidayCLass(date, time) {
                var classType = $("#selClassType").val();
                var status = "On";

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                showLoader();

                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, ClassType: classType, Status: status, Date: date } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetWeekNoForHolidayCLass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {
                            var retVal = 0;
                            $.each(data.d, function (index, value) {
                                retVal = parseInt(value.Retval);
                            });

                            if (retVal > 0) {
                                $.each(data.d, function (index, value) {
                                    var weekNo = parseInt(value.WeekNo) + 1;

                                    $("#selClassType").val("Holiday");
                                    $("#selStatus").val("Cancelled");
                                    $("#hdnPriorStatus").val("Cancelled");
                                    $("#txtWeekNo").val(weekNo);
                                    $("#txtClassDate").val(date);
                                    $("#selTime").val(time);
                                    $("#dvSubstitute").hide();
                                    $("#selClassType").attr("disabled", "disabled");
                                    $("#txtWeekNo").attr("disabled", "disabled");
                                    $("#txtClassDate").attr("disabled", "disabled");
                                    $("#selTime").attr("disabled", "disabled");
                                    $("#txtHwRelDate").val(value.HWDeadlineDate);
                                    $("#txtDueDate").val(value.HWDueDate);
                                    $("#txtSRelDate").val(value.SRelDate);
                                    $("#txtAnsDate").val(value.ARelDate);
                                    showHideDateTime(2);
                                });
                            } else {
                                if (retVal == -2) {
                                    statusMessage("Holiday could not be active until the prior class of the holiday is scheduled. Please schedule the prior class of the holiday.", "error");
                                    $("#dvAddClass").hide();
                                }
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            $(document).on("change", "#selSubCoach", function (e) {
                var substituteCoachId = $(this).val();
                if (parseInt(substituteCoachId) > 0) {
                    if (confirm("You are scheduling a substitute coach.  Please ensure the substitute coach is able to conduct the class on that day/time prior to making this change.")) {

                    } else {
                        $(this).val("0")
                    }
                }
            });


            function verifyHolidayCLass() {

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var date = $("#txtClassDate").val();

                showLoader();

                var jsonData = JSON.stringify({ CoachClsCal: { EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Date: date } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/VerifyHolidayClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {
                            var retVal = 0;
                            $.each(data.d, function (index, value) {
                                retVal = parseInt(value.RetVal);
                            });
                            if (retVal == 1) {
                                postNewClass();
                            } else if (retVal == -1) {
                                statusMessage("Holiday class could not be scheduled since there is no holidays in this date", "error");
                            } else if (retVal == -2) {
                                statusMessage("Holiday class could not be scheduled since there is no holidays in this date", "error");
                            }
                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            $(document).on("click", ".day", function (e) {
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                if (classType == "Makeup" && date != "") {
                    getReleaseDatesForMakeup();
                }
            });

            function getReleaseDatesForMakeup() {


                var date = $("#txtClassDate").val();
                showLoader();

                var jsonData = JSON.stringify({ Date: date });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetReleaseDatesForMakeup",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        hideLoader();
                        if (JSON.stringify(data.d.length) > 0) {


                            $.each(data.d, function (index, value) {

                                $("#txtHwRelDate").val(value.HWDeadlineDate);
                                $("#txtDueDate").val(value.HWDueDate);
                                $("#txtSRelDate").val(value.SRelDate);
                                $("#txtAnsDate").val(value.ARelDate);

                            });

                        }
                    }, failure: function (e) {
                        hideLoader();
                    }
                });
            }

            function testWeekNoReset() {
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var jsonData = JSON.stringify({ objCoachClass: { EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, Level: level, Semester: semester, SessionNo: sessionNo, MemberId: memberID } });

                var consecutiveWeekNo = 0;
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/WeekNoResetting",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                    }
                });
            }

            function changeCLassType() {
                var status = $("#selStatus").val();
                var classType = $("#hdnClassType").val();
                if (status == "On" && classType == "Holiday") {
                    $("#selClassType").val("Regular");
                } else if (status == "Cancelled" && classType == "Holiday") {
                    $("#selClassType").val("Holiday");
                }
            }
            $(document).on("change", "#selStatus", function (e) {
                // changeCLassType();
            });
            function getCurrenyearAndSemester() {


                var consecutiveWeekNo = 0;
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetCurrentyearAndSemester",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                    }
                });
            }
            function getYear() {
                var d = new Date();
                var year = d.getFullYear();
                var Years = parseInt(year) + 1;
                var yearFormat = year + "-" + Years.toString().substr(2, 2);
                var prevyear = parseInt(year) - 1;
                var prevYears = parseInt(prevyear) + 1;
                var prevyearFormat = prevyear + "-" + prevYears.toString().substr(2, 2);
                var futureYear = parseInt(year) + 1;
                var futureYears = parseInt(futureYear) + 1;
                var futureyearFormat = futureYear + "-" + futureYears.toString().substr(2, 2);
                $("#selEventyear").append($("<option></option>").val
                    (futureYear).html(futureyearFormat));
                $("#selEventyear").append($("<option></option>").val
                    (year).html(yearFormat));
                $("#selEventyear").append($("<option></option>").val
                    (prevyear).html(prevyearFormat));
            }

            function getCurrenyearAndSemester(memberId) {

                var consecutiveWeekNo = 0;
                var jsonData = JSON.stringify({ MemberId: memberId });
                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetCoachEventYearAndSemester",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {

                                $("#selEventyear").val(value.EventYear);
                                $("#selSemester").val(value.Semester);
                                
                            });
                            listCoaches();
                        }
                    }
                });
            }

        </script>
    </div>

    <div style="float: left;">

        <a href="VolunteerFunctions.aspx">Back to Volunteer Functions</a>
    </div>
    <div style="clear: both; margin-bottom: 5px;"></div>
    <div>
        <center>
            <h1 style="color: #00a0b0;">Coach Class Calendar</h1>
        </center>
    </div>

    <div style="clear: both; margin-bottom: 5px;"></div>
    <div style="width: 1000px; margin-left: auto; margin-right: auto;">
        <center>
            <fieldset style="padding: 0px;">

                <div style="float: left;">
                    <label for="name">Event Year:</label>
                    <select id="selEventyear">
                        <option value="0">Select</option>
                        <%-- <option value="2017">2017-18</option>
                        <option value="2016" selected="selected">2016-17</option>--%>
                    </select>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <label for="mail">Event:</label>
                    <select id="selEvent">

                        <option value="13" selected="selected">Coaching</option>
                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Semester:</label>
                    <select id="selSemester" style="width: 100px;">
                        <option value="Fall" selected="selected">Fall</option>
                        <option value="Spring">Spring</option>
                        <option value="Summer">Summer</option>
                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="">Coach:</label>
                    <select id="selCoach" style="width: 150px;">

                        <option value="0">Select</option>


                    </select>
                </div>



                <div style="float: left; margin-left: 20px;">
                    <label for="selPgGroup">Product Group:</label>
                    <select id="selProductGroup" style="width: 100px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Product:</label>
                    <select id="selProduct" style="width: 100px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Level:</label>
                    <select id="selLevel" style="width: 150px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Session:</label>
                    <select id="selSession">
                        <option value="0">Select</option>

                    </select>
                </div>

            </fieldset>
        </center>
    </div>

    <div style="clear: both;"></div>
    <div align="center">
        <center>
            <input type="button" id="btnSubmit" value="Submit" class="btnClass" style="cursor: pointer;" />

        </center>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="left">


        <input type="button" class="btnClass" id="btnAddNextClass" value="Add New Class" style="cursor: pointer; display: none;" />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvAddClass" align="center" style="margin-left: auto; margin-right: auto; padding: 10px; border: 1px solid #c6cccd; display: none;">
        <center>
            <h2>Add New Class</h2>
            <fieldset id="fieldAddClass" style="padding: 0; width: 850px;">

                <div style="float: left;">
                    <label for="name">Class Type</label>
                    <select id="selClassType">
                        <option value="0">Select</option>
                        <option value="Regular">Regular</option>
                        <option value="Makeup">Makeup</option>
                        <%--   <option value="Substitute">Substitute</option>
                        <option value="Makeup Substitute">Makeup Substitute</option>--%>
                        <option value="Extra">Extra</option>
                        <option value="Holiday">Holiday</option>
                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Week #:</label>
                    <input type="text" id="txtWeekNo" style="padding: 6px;" />
                </div>
                <div style="float: left; margin-left: 20px;" id="dvClassDate" class="input-append date">
                    <label for="mail">Class Date:</label>
                    <input type="text" style="padding: 6px;" disabled="disabled" id="txtClassDate" placeholder="mm/dd/yyyy" />
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>

                <div style="float: left; margin-left: 20px;" id="dvClassTime">
                    <label for="password">Class Time:</label>
                    <select id="selTime" disabled="disabled" style="width: 120px;">
                        <option value="0">Select</option>
                        <option value="08:00">8:00 AM</option>
                        <option value="09:00">9:00 AM</option>
                        <option value="10:00">10:00 AM</option>
                        <option value="11:00">11:00 AM</option>
                        <option value="12:00">12:00 PM</option>
                        <option value="13:00">01:00 PM</option>
                        <option value="14:00">02:00 PM</option>
                        <option value="15:00">03:00 PM</option>
                        <option value="16:00">04:00 PM</option>
                        <option value="17:00">05:00 PM</option>
                        <option value="18:00">06:00 PM</option>
                        <option value="19:00">07:00 PM</option>
                        <option value="20:00">08:00 PM</option>
                        <option value="21:00">09:00 PM</option>
                        <option value="22:00">10:00 PM</option>
                        <option value="23:00">11:00 PM</option>

                    </select>
                </div>



                <div style="float: left; margin-left: 20px; display: none;" id="dvSubstitute">
                    <label for="" id="lblSubtituteCoach">Substitute Coach:</label>
                    <select id="selSubCoach" style="width: 150px;">
                        <option value="0">Select</option>


                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Status:</label>
                    <select id="selStatus" style="width: 120px;">
                        <option value="On">On</option>
                        <option value="Cancelled">Cancelled</option>
                    </select>
                </div>


            </fieldset>
            <fieldset style="padding: 0; width: 850px;">



                <div style="float: left;" id="dvHWDate" class="input-append date">
                    <label for="mail">Home Work:</label>
                    <input type="text" style="padding: 6px;" id="txtHwRelDate" placeholder="mm/dd/yyyy" />

                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <div style="float: left; margin-left: 20px;" id="dvDueDate" class="input-append date">
                    <label for="mail">Home Work Due Date:</label>
                    <input type="text" style="padding: 6px;" id="txtDueDate" placeholder="mm/dd/yyyy" />
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <div style="float: left; margin-left: 20px;" id="dvARelDate" class="input-append date">
                    <label for="mail">Answer Release Date:</label>
                    <input type="text" style="padding: 6px;" id="txtAnsDate" placeholder="mm/dd/yyyy" />
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <div style="float: left; margin-left: 20px;" id="dvSRelDate" class="input-append date">
                    <label for="mail">Student Release Date:</label>
                    <input type="text" style="padding: 6px;" id="txtSRelDate" placeholder="mm/dd/yyyy" />
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>


                <%-- <div style="float: left; margin-left: 20px;" class="input-append date">
                    <input type="text" name="date" id="txtDOB" placeholder="dd/mm/yyyy">
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>--%>
            </fieldset>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <center>
                <input type="button" class="btnClass" id="btnSave" value="Save" style="cursor: pointer;" />
                <input type="button" class="btnClass" id="btnCancel" value="Cancel" style="cursor: pointer;" />
                <%--  <button id="btnSave" style="font-size: 16px; cursor: pointer;">Save</button>

                <button id="btnCancel" style="font-size: 16px; cursor: pointer;">Cancel</button>--%>
            </center>
        </center>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <span id="spnGridTitle" style="display: none; font-weight: bold;">Table 1: Coach Class Calendar</span>
    </center>
    <div style="clear: both; margin-bottom: 1px;"></div>
    <div>
        <table id="table">
            <%--  <thead>
                <tr>
                    <th>Action</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Class Stype</th>
                    <th>Week#</th>
                    <th>Status</th>
                    <th>Makeup</th>
                    <th>Substitute</th>
                    <th>Reason</th>
                    <th>HwRelDate</th>
                    <th>HWDueDate</th>
                    <th>SRelDate</th>
                    <th>ARelDate</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <button id="Button1" style="padding: 5px; font-size: 16px; cursor: pointer;">Modify</button></td>
                    <td>03/25/2017</td>
                    <td>6:00 PM</td>
                    <td>Regular</td>
                    <td>1</td>
                    <td>On</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>--%>
        </table>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
            <div class="sk-fading-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
        </div>
        <div id="overlay"></div>

        <div style="display: none;" id="myDiv" class="animate-bottom">
        </div>
        <input type="hidden" value="0" id="hdnCoachClassCalID" />
        <input type="hidden" value="0" id="hdnStatus" />
        <input type="hidden" value="0" id="hdnDay" />
        <input type="hidden" value="0" id="hdnLoginID" runat="server" />
        <input type="hidden" value="0" id="hdnRoleID" runat="server" />
        <input type="hidden" value="120" id="hdnDuration" runat="server" />

        <input type="hidden" value="nsf.zoom.24@gmail.com" id="hdnUserID" runat="server" />
        <input type="hidden" value="TrainerVroom!24" id="hdnPwd" runat="server" />
        <input type="hidden" value="" id="hdnPriorStatus" />
        <input type="hidden" value="0" id="hdnMakeupSubtituteId" />
        <input type="hidden" value="0" id="hdnSubstituteCoachId" />
        <input type="hidden" value="0" id="hdnClassType" />
        <input type="hidden" value="0" id="hdnWeekNo" />
        <input type="hidden" value="0" id="hdnDate" />
        <input type="hidden" value="0" id="hdnTime" />
    </div>
</asp:Content>
