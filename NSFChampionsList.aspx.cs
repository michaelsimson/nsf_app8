﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class NSFChampionsList : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}

        if (!IsPostBack)
        {
            Yearscount(FromYear);
            Yearscount(ToYear);
        }
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        load();
       
       
    }
    public void load()
    {
        string YearRange = FromYear.SelectedValue + "-" + ToYear.SelectedValue;
        string ProductGroupQry = "select distinct USSAward.ProductGroupCode,USSAward.ProductGroupId,Productgroup.Name +' '+'Champions " + YearRange + "' as Name from USSAward left join" +
        " Productgroup on  Productgroup.ProductGroupCode=USSAward.ProductGroupCode ";
        string PrductQryByOrder = ProductGroupQry + " where USSAward.ProductGroupID in(1,2,3) and USSAward.ContestYear  between " + FromYear.SelectedValue + " and " + ToYear.SelectedValue 
            + "  union  All " + ProductGroupQry + "where USSAward.ProductGroupID in(25) and USSAward.ContestYear  between   " 
            + FromYear.SelectedValue + " and " + ToYear.SelectedValue + "  order by USSAward.ProductGroupId asc";
        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, PrductQryByOrder);
        string MergeOrder = ProductGroupQry + "where USSAward.ProductGroupID >3 and USSAward.ProductGroupID<>25  and USSAward.ContestYear  between   "
            + FromYear.SelectedValue + " and " + ToYear.SelectedValue + "   order by USSAward.ProductGroupId asc";
        DataSet dsProductGroupMerge = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, MergeOrder);
        dsProductGroup.Merge(dsProductGroupMerge);

        if (dsProductGroup.Tables[0].Rows.Count > 0)
        {
            DataLists.DataSource = dsProductGroup.Tables[0];
            DataLists.DataBind();
        }
    }
    protected void Yearscount(DropDownList DDYear)
    {
        try
        {
            DDYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = 1993; i <= DateTime.Now.Year; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            DDYear.DataSource = list;
            DDYear.DataTextField = "Text";
            DDYear.DataValueField = "Value";
            DDYear.DataBind();
            DDYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //SessionExp();
        }

    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label lblyear = (Label)e.Item.FindControl("lblyear");
        Label lblprod = (Label)e.Item.FindControl("lblprod");
        Repeater rp = (Repeater)e.Item.FindControl("rp_Postings");


        string DDListItem = "select  RANK,FIRST_NAME,LAST_NAME, I.City,I.State from USSAward A inner join dbo.Child C on"+
        "   C.ChildNumber=A.ChildNumber left join IndSpouse I on I.AutoMemberID=c.MEMBERID  where ContestYear='" + lblyear.Text + "'   AND ProductCode='" + lblprod.Text + "' ORDER bY USSAwardID ASC";
            //"select * from USSAward where ContestYear='"+lblyear.Text+"'  AND ProductCode='"+lblprod.Text+"' ORDER bY USSAwardID ASC";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDListItem);

        if (dsstate.Tables[0].Rows.Count > 0)
        {
            rp.DataSource = dsstate.Tables[0];
            rp.DataBind();
        }

    }
    protected void btnsearchChild0_Click(object sender, EventArgs e)
    {
        if ((FromYear.SelectedItem.Text != "Select Year") && (ToYear.SelectedItem.Text != "Select Year"))
        {
            if (Convert.ToInt32(ToYear.SelectedValue) >= Convert.ToInt32(FromYear.SelectedValue))
            {
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=Champions List.doc");
                Response.Charset = "";
                Response.ContentType = "application/vnd.ms-word ";
                StringWriter sw = new StringWriter();

                HtmlTextWriter hw = new HtmlTextWriter(sw);
                load();

                DataLists.DataBind();
                DataLists.RenderControl(hw);
                //TblBorder.RenderControl(hw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            else
            {
                Response.Write("<script>alert('To year should be greater than From Year')</script>");
            }
        }
        else
        {
            Response.Write("<script>alert('Please Select From year and To Year')</script>");
        }
    }
    protected void DataLists_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        string levelName;
        HiddenField hdnProdcode = (HiddenField)e.Item.FindControl("hdn");
        if (hdnProdcode.Value == "MB")
        {
            levelName = "p.ProductCode";
        }
        else
        {
            levelName = "LEFT(p.Name, CHARINDEX(' ', p.Name) )";
        }
        DataList DataList1 = (DataList)e.Item.FindControl("DataList1");
        string DDLstate = "select distinct ContestYear, p.ProductCode,'Name'="+levelName+"  from USSAward U inner"+
        " join Product P on p.ProductCode=U.ProductCode  where P.ProductGroupCode='" + hdnProdcode.Value + "' and p.EventId=1  and contestyear between " + FromYear.SelectedValue + " and "
        + ToYear.SelectedValue + "  order by ContestYear desc";

        DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);

        if (dsstate.Tables[0].Rows.Count > 0)
        {
            DataList1.DataSource = dsstate.Tables[0];
            DataList1.DataBind();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        load();
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        load();
    }
}