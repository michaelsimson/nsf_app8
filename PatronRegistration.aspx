﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="PatronRegistration.aspx.vb" Inherits="PatronRegistration" title="Untitled Page" %>
 <%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
		<script language="javascript" type="text/javascript">
			window.history.forward(1);
		</script>
		
		
		</div>
		<div>
	    
			<table cellspacing="1" cellpadding="3"   border="0" class="tableclass" >                
			    <tr>
			        <td>
                        <asp:LinkButton CssClass="btn_02"  ID="hlnkMainMenu" runat="server"></asp:LinkButton>
			           &nbsp;&nbsp;&nbsp;
                        </td>
			    </tr>				<tr bgcolor="#FFFFFF" align="center" >
					<TD class="title02" colspan="2" align="center" ><b>Personal Information</b>
					</TD>       
				</tr>
				</table>
				<div align="center">
						<table id="tblIndividual"   class="tableclass"  cellspacing="1" cellpadding="3">
								<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Title:</TD>
								<td noWrap align="left"><asp:dropdownlist id="ddlTitleInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Title</asp:ListItem>
										<asp:ListItem Value="Mr">Mr.</asp:ListItem>
										<asp:ListItem Value="Mrs">Mrs.</asp:ListItem>
										<asp:ListItem Value="Miss">Miss.</asp:ListItem>
										<asp:ListItem Value="Dr">Dr.</asp:ListItem>
										<asp:ListItem Value="Ms">Ms.</asp:ListItem>
										<asp:ListItem Value="Prof">Prof.</asp:ListItem>
                                    <asp:ListItem>Late</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvTitleInd" runat="server" ControlToValidate="ddlTitleInd" Display="Dynamic"
										ErrorMessage="Title Should be Selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">First Name:</td>
								<td noWrap align="left"><asp:textbox id="txtFirstNameInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvFirstName" runat="server" ControlToValidate="txtFirstNameInd" ErrorMessage="First Name is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Last Name:</td>
								<td noWrap align="left"><asp:textbox id="txtLastNameInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvLastName" runat="server" ControlToValidate="txtLastNameInd" ErrorMessage="Last Name is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Address1:</td>
								<td noWrap align="left"><asp:textbox id="txtAddress1Ind" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvAddress1" runat="server" ControlToValidate="txtAddress1Ind" ErrorMessage="Address is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">City:</td>
								<td noWrap align="left"><asp:textbox id="txtCityInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:requiredfieldvalidator id="rfvCity" runat="server" ControlToValidate="txtCityInd" ErrorMessage="City is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   align="right">
                                    State:</td>
								<td  align="left"><asp:dropdownlist id="ddlStateInd" runat="server" CssClass="SmallFont"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvStateInd" runat="server" ControlToValidate="ddlStateInd" Display="Dynamic"
										ErrorMessage="State should be selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">ZIP/Postal 
									Code:</td>
								<td  noWrap align="left">
								    <asp:textbox id="txtZipInd" runat="server" CssClass="SmallFont"></asp:textbox>
								        <asp:requiredfieldvalidator id="rfvZip" runat="server" ControlToValidate="txtZipInd" ErrorMessage="Zip is required."></asp:requiredfieldvalidator>
                                    <br />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtZipInd"
                                        ErrorMessage="Zip code should contain 5 digits" ValidationExpression="\d{5}(-\d{4})?"></asp:RegularExpressionValidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  style="HEIGHT: 20px" noWrap align="right">Country:</td>
								<td style="HEIGHT: 20px" noWrap align="left"><asp:dropdownlist id="ddlCountryInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" " Selected="True">Select Country</asp:ListItem>
										<asp:ListItem Value="US">United States</asp:ListItem>
										<asp:ListItem Value="CA">Canada</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvCountry" runat="server" ControlToValidate="ddlCountryInd" Display="Dynamic"
										ErrorMessage="Country is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right" style="height: 10px">Gender:</td>
								<td  noWrap align="left" style="height: 10px"><asp:dropdownlist id="ddlGenderInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Gender</asp:ListItem>
										<asp:ListItem Value="Male">Male</asp:ListItem>
										<asp:ListItem Value="Female">Female</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="rfvGenderInd" runat="server" ControlToValidate="ddlGenderInd" Display="Dynamic"
										ErrorMessage="Gender should be Selected"></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right" style="height: 14px">Home Phone:</td>
								<td noWrap align="left" style="height: 14px"><asp:textbox id="txtHomePhoneInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revHomePhoneInd" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic"
										ErrorMessage="Home Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator>
                                    <br />
										<asp:requiredfieldvalidator id="rfvHomePhoneInd" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic" ErrorMessage="Home Phone Number is required."></asp:requiredfieldvalidator></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Cell Phone:</td>
								<td noWrap align="left"><asp:textbox id="txtCellPhoneInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revCellPhoneInd" runat="server" ControlToValidate="txtCellPhoneInd" Display="Dynamic"
										ErrorMessage="Cell Phone No should be in xxx-xxx-xxxx format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:regularexpressionvalidator>&nbsp;
								</td>
							</tr>
							
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Primary E-mail:</td>
								<td noWrap align="left"><asp:textbox id="txtPrimaryEmailInd" runat="server" CssClass="SmallFont"></asp:textbox><asp:regularexpressionvalidator id="revPrimaryEmailInd" runat="server" ControlToValidate="txtPrimaryEmailInd" Display="Dynamic"
										ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator><br />
                                    <asp:requiredfieldvalidator id="rfvEmail" runat="server" ControlToValidate="txtPrimaryEmailInd" Display="Dynamic"
										ErrorMessage="Email is required."></asp:requiredfieldvalidator></td>
							</tr>
							
							<tr bgcolor="#FFFFFF">
								<td  noWrap align="right">Referred By:</td>
								<td noWrap align="left" colSpan="3"><asp:dropdownlist id="ddlReferredByInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value="">Select Referred By</asp:ListItem>
										<asp:ListItem Value="None">None</asp:ListItem>
										<asp:ListItem Value="Advertisement">Advertisement</asp:ListItem>
										<asp:ListItem Value="Email">Email</asp:ListItem>
										<asp:ListItem Value="Flyer">Flyer</asp:ListItem>
										<asp:ListItem Value="Friend">Friend</asp:ListItem>
										<asp:ListItem Value="Newspaper">Newspaper</asp:ListItem>
										<asp:ListItem Value="Relative">Relative</asp:ListItem>
										<asp:ListItem Value="Other">Other</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr bgcolor="#FFFFFF">
								<td   noWrap align="right">NSF 
									Chapter:</td>
								<td  noWrap align="left" colSpan="3"><asp:dropdownlist id="ddlChapterInd" runat="server" CssClass="SmallFont"></asp:dropdownlist><asp:requiredfieldvalidator id="rfvChapterInd" runat="server" ControlToValidate="ddlChapterInd" Display="Dynamic"
										ErrorMessage="NSF Chapter should be selected"></asp:requiredfieldvalidator></td>
							</tr>						
							
						</table>
						<asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Submit"  ></asp:button>
						
          <recaptcha:RecaptchaControl
              ID="recaptcha"
              runat="server"
              Theme="red"
              PublicKey="6Ld8xMUSAAAAAPhq9qKd7ZTzxM_pByJI7J6qWFPj"
              PrivateKey="6Ld8xMUSAAAAAOyJUVei17z3NWZbEPvAWYDvOotm"
              />
<asp:Label  ID="lblResult" runat="server" />
          <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="btnChkSubmit_Click" />

					</div>
</div>
</asp:Content>

