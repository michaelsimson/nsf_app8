﻿Imports System.Data.SqlClient
Imports System.Net.Mail

Partial Class Emailafs
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.QueryString("id") = Nothing Then
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("ferdin448@gmail.com")
            email.To.Add(Request.QueryString("id"))
            email.Subject = "Thanks for Registering - North South Foundation"
            email.IsBodyHtml = True
            'email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = "Hi Friend,<br> Thanks for Visiting My Site<br><br> Silvaa.co.cc"
            'leave blank to use default SMTP server
            Dim client As New SmtpClient()
            Try
                client.Send(email)
            Catch ex As Exception
                Response.Write(ex.ToString())
                Exit Sub
            End Try
            Dim strscript As String = "<script language=javascript>window.top.close();</script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterStartupScript(Page.GetType(), "clientScript", strscript)
            End If
        End If
    End Sub
End Class
