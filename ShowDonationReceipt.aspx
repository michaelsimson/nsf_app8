<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowDonationReceipt.aspx.vb" Inherits="ShowDonationReceipt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Donation Receipt</title>
	<link href="Styles.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript">
	    function printdoc()
	    {
	        document.getElementById('btnPrint').style.visibility = "hidden";
	        document.getElementById('hlinkParentRegistration').style.visibility = "hidden";
	        document.getElementById('hlnkHome').style.visibility = "hidden";
	        document.getElementById('txtHidden').style.visibility = "hidden";
	        document.getElementById('hlnkDownload').style.visibility = "hidden";
	        if (document.getElementById('hdnAttach').value != "") {
	            document.getElementById('hlnkSearch').style.visibility = "hidden";
	            document.getElementById('lbtnSend').style.visibility = "hidden";
	        }
	           
	        window.print();
	        document.getElementById('btnPrint').style.visibility = "visible";
	        document.getElementById('txtHidden').style.visibility = "visible";
	        document.getElementById('hlinkParentRegistration').style.visibility = "visible";
	        document.getElementById('hlnkHome').style.visibility = "visible";
	        document.getElementById('hlnkDownload').style.visibility = "visible";
	        if (document.getElementById('hdnAttach').value != "") {
	            document.getElementById('hlnkSearch').style.visibility = "visible";
	            document.getElementById('lbtnSend').style.visibility = "visible";
	        }
	        
	        return false;
	    }	   
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>        
          <table cellspacing="1" class="tblMain" cellpadding="3" width="750px"  align="center" border="0" >
                <tr>
                    <td>
                        <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/DonorFunctions.aspx" >Back to Main Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                        <asp:hyperlink id="hlnkSearch" runat="server" NavigateUrl="~/DonorFunctions.aspx" Visible="false">Back to Search</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                        <asp:HyperLink ID="hlnkDownload" runat="server" Enabled="false">Download Receipt [Right click and Save]</asp:HyperLink>
                    &nbsp;<asp:LinkButton ID="lbtnSend" Visible="false" OnClick="lbtnSend_Click" runat="server">Send Mail</asp:LinkButton></td>
                </tr>
				<tr bgcolor="#FFFFFF">
					<td  class="ItemCenter" colspan="2" align="center"><font face="Arial" size="5">North South Foundation<br />	</font> 				       
					   <font face="Arial" size="2"> 2 Marissa Ct<br />
                        Burr Ridge, IL 60527-6864<br />
                        Tax Id:  36-3659998</font>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
				    <td colspan="2" class="Heading" align="center">
				        <font face="Arial" size="4">
				       Donation Receipt: From <asp:Label runat="server" ID="lblFromDate"></asp:Label> to <asp:Label runat="server" ID="lblToDate"></asp:Label> 
				       </font>
				    </td>
				</tr>
				<asp:Panel runat="server" ID="pnlMessage" Visible="false">				    
			        <tr bgcolor="#FFFFFF">
				        <td class="ItemCenter" align="center" colspan="2"><asp:label id="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red" Font-Size="Medium"></asp:label></td>
			         </tr>				    
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlData" >
				<tr>
				    <td colspan="2">&nbsp;</td>
				</tr>
				<tr>
				    <td align="left"  colspan="2">
				    <asp:Label Font-Size="11" runat="server" ID="lblCurrDate" CssClass="ItemCenter"></asp:Label>     
                        <asp:TextBox ID="txtHidden" OnTextChanged="txtHidden_TextChanged" AutoPostBack="true"  runat="server"></asp:TextBox>
</td>
				</tr>
				<tr>
				    <td>&nbsp;</td>
				</tr>
				<tr>
				    <td align="left" colspan="2"><b></b>
				   </td>
				</tr>
				<tr>
				    <td align="left" colspan="2">
                     <asp:Label Font-Size="11" runat="server" ID="lblName" CssClass="ItemCenter"></asp:Label><br />
				        <asp:Label Font-Size="11" runat="server" ID="lblAddress1"></asp:Label>
				        <asp:Label Font-Size="11" runat="server" ID="lblAddress2" ></asp:Label><br />				        
				        <asp:Label Font-Size="11" runat="server" ID="lblCity" CssClass="ItemCenter"></asp:Label>,
				        <asp:Label Font-Size="11" runat="server" ID="lblState" CssClass="ItemCenter"></asp:Label>
				        <asp:Label  Font-Size="11" runat="server" ID="lblZip" CssClass="ItemCenter"></asp:Label><br />
				     </td>
				</tr>
			
                <tr>
                    <td class="ItemCenter" colspan="2" >
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lblTaxDeductibleAmt" Text="1) Tax-deductible Amount from Fees"  Font-Size="11" ></asp:Label></b>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:DataGrid ShowFooter="true" runat="server" ID="dgFess"  
							            AutoGenerateColumns="False" Width="450px" BorderWidth="1px" CellPadding="4"  AllowSorting="True">					            							            
							         
							             <ItemStyle ForeColor="#000066" Width="10" />
                                        <Columns>                                            
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderText="Payment Date"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDFeePaymentDate" Text='<%# DataBinder.Eval(Container, "DataItem.donationdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderText="Fee" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right" >
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblFee" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.RegFee"),2) %>'></asp:Label>
                                                     </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Percent Deductible" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblFeePercentDeductible" Text='<%# GetTaxablePercentage(DataBinder.Eval(Container,"DataItem.EventID")) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn  ItemStyle-Width="150" HeaderStyle-Wrap="false" HeaderText="Tax Deductible Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblFeeTaxDeductibleAmount" Text='<%# FormatNumber(GetDonationAmount(DataBinder.Eval(Container,"DataItem.EventID"),DataBinder.Eval(Container,"DataItem.RegFee")),2) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>	
               
                <tr>
                    <td class="ItemCenter" colspan="2" >
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lblTaxDonation" Text="2) Tax-deductible Amount from Donation(s)"  Font-Size="11" ></asp:Label></b>                                   
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:DataGrid ShowFooter="true" runat="server" ID="dgDonation" 
							            AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4"  AllowSorting="True">
							            <AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
							            <ItemStyle Wrap="False" ></ItemStyle>
                                        <Columns>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Donation date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10"  runat="server" ID="lblPaymentDate" Text='<%# DataBinder.Eval(Container, "DataItem.donationdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Donation" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblDonation" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Percent Deductible" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblPercentDeductible" Text="100"></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="150" HeaderStyle-Wrap="false" HeaderText="Tax Deductible Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblTaxDeductibleAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr >
                    <td><font size="4"><b>Total Tax-deductible Contribution:</b> &nbsp;&nbsp;&nbsp;
                    $</font><asp:Label runat="server" Font-Bold="true" Font-Size="12" ID="lblGrandTotal" ></asp:Label></td>
                </tr>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                       <Div runat ="server" id="divall" align="justify">
                         No goods or services were provided to you in exchange for this tax-deductible contribution.  North 
                        South Foundation is a tax-exempt organization under the U.S. Internal Revenue Code 501(c)(3).  We 
                        further confirm that no direct, tangible benefit will accrue to the donor, to the donor's family, nor to any
                        related third party as a result of this gift.  <br /><br /></Div>
                        <Div runat ="server" id="divCorp" visible="false"  align="justify"> 
                         No goods or services were provided to you in exchange for this tax-deductible contribution.  
                         North South Foundation is a tax-exempt organization under the U.S. Internal Revenue Code 501(c)(3).  
                         We  further confirm that no direct, tangible benefit will accrue to the donor, to the donor's employees or management, 
                         or to any related third party as a result of this gift.  Thank you for your generous contribution.  <br /><br /></Div>
                        
                            Thank you for your generous contribution.  Please retain this receipt for your records. 
                       
                    </td>
                </tr>
                <tr>
                    <td><asp:HiddenField runat="server" ID="txtRegFee" /></td>
                    <td><asp:HiddenField runat="server" ID="txtDonation" />
                        <asp:HiddenField runat="server" ID="txtToken" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" runat="server"  onclick="return printdoc();" value="Print" id="btnPrint" class="FormButton" />
                    </td>
                </tr>
                <tr>
                    <td><asp:HyperLink runat="server" ID="hlnkHome" NavigateUrl="Maintest.aspx">Home</asp:HyperLink> </td>
                </tr>
            </asp:Panel>
            
		</table> 
        <asp:HiddenField ID="hdnAttach" runat="server" />
        <asp:HiddenField ID="hdnMailBody" runat="server" />
        <asp:HiddenField ID="hdnEmail" runat="server" />
    </div>
    </form>
</body>
</html>


 
 
 