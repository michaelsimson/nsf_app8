<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowAllDonationReceipts.aspx.vb" Inherits="ShowAllDonationReceipts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
   	<link href="Styles.css" type="text/css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function printdoc()        {
            document.getElementById('btnPrint').style.visibility="hidden";
            document.getElementById('hlinkParentRegistration').style.visibility="hidden";
            document.getElementById('hlnkSearch').style.visibility="hidden";
            window.print();
            document.getElementById('btnPrint').style.visibility="visible";
            document.getElementById('hlinkParentRegistration').style.visibility="visible";
            document.getElementById('hlnkSearch').style.visibility="visible";
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div>        
        <table cellspacing="1" class="tblMain" cellpadding="3" width="90%"  align="center" border="0" >
            <tr>
                    <td>
                        <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/DonorFunctions.aspx" >Back to Main Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                        <asp:hyperlink id="hlnkSearch" runat="server" NavigateUrl="~/DonorFunctions.aspx" >Back to Search</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                        <asp:Panel runat="server" ID="pnlChapters" Visible="false">
                            <asp:DropDownList runat="server" ID="ddlChapter"></asp:DropDownList>
                        </asp:Panel>
                    </td>
                    <td align="right">
                        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />
                    </td>
             </tr>             
        </table>
        <asp:Panel runat="server" ID="pnlData">
        <asp:Repeater runat="server" ID="rptReceipt">       
            <ItemTemplate>       
             <div style="page-break-before:always">     
                 <table cellspacing="1" class="tblMain" cellpadding="2" width="90%"  align="center" border="0" >                
				<tr bgcolor="#FFFFFF">
					<td  class="ItemCenter" colspan="2" align="center"><font face="Arial" size="4"><b>North South Foundation</b></font><br />					       
					    <font size="3"> 2 Marissa Ct<br />
                        Burr Ridge, IL 60527-6864<br />
                        Tax Id:  36-3659998</font> 
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
				    <td colspan="2" class="Heading" align="center">
				        <font face="Arial" size="3">
				      <b> Donation Receipt:</b> From  <b> <asp:Label runat="server" ID="lblFromDate" Text='<%# GetFromDate() %>'></asp:Label></b> to  <b> <asp:Label runat="server" ID="lblToDate" Text='<%#  GetToDate() %>'></asp:Label> </b>
				       </font>
				    </td>
				</tr>	
                     <tr>
				    <td colspan="2" >&nbsp;</td>
				</tr>
                      <tr>
				    <td colspan="2" >&nbsp;</td>
				</tr>						 				 
				<tr>
				    <td align="left"  colspan="2">
				    <font size="3"> <b><asp:Label runat="server" ID="lblCurrDate" CssClass="ItemCenter" Text='<%# GetDate() %>' ></asp:Label></b></font></td>
				</tr>
                <tr>
				    <td colspan="2" >&nbsp;</td>
				</tr>				
				<tr>
				    <td align="left" colspan="2">
				    <asp:Label Font-Size="11" runat="server" ID="lblName" CssClass="ItemCenter" Text='<%# GetDonorName(DataBinder.Eval(Container,"DataItem.FirstName"),DataBinder.Eval(Container,"DataItem.LastName")) %>'></asp:Label></td>
				</tr>
				<tr>
				    <td align="left" colspan="2"><font size="2">
				        <asp:Label Font-Size="11" runat="server" ID="lblAddress1" CssClass="ItemCenter" Text='<%# DataBinder.Eval(Container,"DataItem.Address1")%>'></asp:Label>
				        <asp:Label Font-Size="11" runat="server" ID="lblAddress2" CssClass="ItemCenter" Text='<%# DataBinder.Eval(Container,"DataItem.Address2")%>'></asp:Label><br />				        
				        <asp:Label Font-Size="11" runat="server" ID="lblCity" CssClass="ItemCenter" Text='<%# DataBinder.Eval(Container,"DataItem.City")%>'></asp:Label>,
				        <asp:Label Font-Size="11" runat="server" ID="lblState" CssClass="ItemCenter" text='<%# DataBinder.Eval(Container,"DataItem.State")%>'></asp:Label>
				        <asp:Label  Font-Size="11" runat="server" ID="lblZip" CssClass="ItemCenter" Text='<%# DataBinder.Eval(Container,"DataItem.Zip")%>'></asp:Label> 
                         </font>
				     </td>
				</tr>				 
                <tr>
                    <td class="ItemCenter" colspan="2" >
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b><asp:Label runat="server" ID="lblTaxDeductibleAmt"  Font-Bold="true" Text="1) Tax-deductible Amount from Fees"  Font-Size="11" ></asp:Label></b>
                                </td>
                            </tr>
                            <tr>
                                <td>                                   
                                    <asp:DataGrid ShowFooter="true" runat="server" ID="dgFess" DataSource='<%# GetRegistrationDataSource(DataBinder.Eval(Container,"DataItem.AutoMemberID")) %>' OnItemDataBound="dgFess_ItemDataBound"
							            AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4"  AllowSorting="True">							            							            
							            <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White"  />
							             <ItemStyle ForeColor="#000066" Width="10" />
                                        <Columns>                                            
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderText="Payment Date"  HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9" >
                                                <ItemTemplate  >
                                                    <asp:Label Font-Size="10" runat="server" ID="lblDFeePaymentDate" Text='<%# DataBinder.Eval(Container, "DataItem.donationdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderText="Fee" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right" >
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblFee" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.RegFee"),2) %>'></asp:Label>
                                                     </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Percent Deductible" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblFeePercentDeductible" Text='<%# GetTaxablePercentage(DataBinder.Eval(Container,"DataItem.EventID")) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn  ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Tax Deductible Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblFeeTaxDeductibleAmount" Text='<%# FormatNumber(GetDonationAmount(DataBinder.Eval(Container,"DataItem.EventID"),DataBinder.Eval(Container,"DataItem.RegFee")),2) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>                                        
                                </td>
                            </tr>                            
                        </table>
                    </td>
                </tr>               
                <tr>
                    <td class="ItemCenter" colspan="2">
                        <table width="100%">
                            <tr>
                                <td align="left">
                                    <b><asp:Label runat="server" ID="lblTaxDonation" Font-Bold="true"  Text="2) Tax-deductible Amount from Donation(s)"  Font-Size="11" ></asp:Label></b>                                   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DataGrid ShowFooter="true" runat="server" ID="dgDonation"  DataSource='<%# GetDonationDataSource(DataBinder.Eval(Container,"DataItem.AutoMemberID")) %>' OnItemDataBound="dgDonation_ItemDataBound"
							            AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4"  AllowSorting="True">
							            <AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
							            <ItemStyle Wrap="False" ></ItemStyle>
                                        <Columns>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Payment Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <asp:Label Font-Size="10"  runat="server" ID="lblPaymentDate" Text='<%# DataBinder.Eval(Container, "DataItem.donationdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Donation" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblDonation" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Percent Deductible" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10"  runat="server" ID="lblPercentDeductible" Text="100"></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn ItemStyle-Width="100" HeaderStyle-Wrap="false" HeaderText="Tax Deductible Amount" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="9">
                                                <ItemTemplate>
                                                    <div style="text-align:right">
                                                        <asp:Label Font-Size="10" runat="server" ID="lblTaxDeductibleAmount" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>
                                                    </div> 
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><font size="3"><b>Total Tax-deductible Contribution:</b></font> &nbsp;&nbsp;&nbsp;
                    $<asp:Label runat="server" Font-Bold="true" Font-Size="12" ID="lblGrandTotal" ></asp:Label></td>
                </tr>                
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <font face="Arial" size="2"> 
                        No goods or services were provided to you in exchange for this tax-deductible contribution.  North 
                        South Foundation is a tax-exempt organization under the U.S. Internal Revenue Code 501(c)(3).  We 
                        further confirm that no direct, tangible benefit will accrue to the donor, to the donor's family, nor to any 
                        related third party as a result of this gift.&nbsp;<br />
                        <br />                        
                            Thank you for your generous contribution.  Please retain this receipt for your records. 
                        </font>
                    </td>
                </tr>  
		    </table> 
		    </div>    
		</ItemTemplate>		
		
		</asp:Repeater>
		
		</asp:Panel>
		<asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="1" class="tblMain" cellpadding="3" width="90%"  align="center" border="0" >
            <tr >
                    <td class="Heading">
                        <asp:Label runat="server" ID="lblMessage" ></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
    </div>
    </div>
    </form>
</body>
</html>


 
 
 