<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Reg_Game.aspx.vb" Inherits="Reg_Game" title="NorthSouthGameRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<script language="JavaScript">
var nav = window.Event ? true : false;
if (nav) {
   window.captureEvents(Event.KEYDOWN);
   window.onkeydown = NetscapeEventHandler_KeyDown;
} else {
   document.onkeydown = MicrosoftEventHandler_KeyDown;
}

function NetscapeEventHandler_KeyDown(e) {
  if (e.which == 13 && e.target.type != 'textarea' && e.target.type != 'submit') { return false; }
  return true;
}

function MicrosoftEventHandler_KeyDown() {
  if (event.keyCode == 13 && event.srcElement.type != 'textarea' && event.srcElement.type != 'submit')
    return false;
  return true; 
}
</script>


<%--<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0">
  <tr bgcolor="#ffffff" >
       <td class="Heading" align="center" colspan="3">Online Registration - Games</td></tr>
       <tr><td ></td></tr>
		<tr><td></td></tr>	
		<tr><td style="height: 22px; width:25%" ><asp:LinkButton ID="lnkmain1" runat="server" CausesValidation="false"></asp:LinkButton></td>&nbsp;&nbsp;&nbsp
		<td style="width:30%"><asp:LinkButton ID="lnkGS1" runat="server" Text="Back to Game Search" CausesValidation="false" Visible="false"></asp:LinkButton> </td>&nbsp;&nbsp;&nbsp
		 	<td style="width:30%">
		 	<asp:HyperLink ID="Hyphelp1" NavigateUrl="http://www.northsouth.org/st/game.asp#access" runat="server" onclick="window.open(this.href,'popupwindow','width=800,height=400,scrollbars,resizable');return false;">Help</asp:HyperLink></td>
		</tr>
		<tr><td></td></tr>
		<tr><td></td></tr>	
		
		</table>
		--%>
		<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0" >
            <tr >
                <td colspan="3" align="center">
                <asp:Label ID="lblmainheader" runat="server" Text="Online Registration - Games" Font-Size="Large" ForeColor="DarkBlue"></asp:Label>
                 </td>
            </tr >
            <tr >
                <td colspan="1" style="width:30%"><asp:LinkButton ID="lnkmain" runat="server" CausesValidation="false"></asp:LinkButton></td>
                <td colspan="1" style="width:30%"><asp:LinkButton ID="lnkGS" runat="server" CausesValidation="false" Text="Back to Game Search" ></asp:LinkButton></td>
                <td colspan="1" style="width:30%"><asp:HyperLink ID="Hyphelp" NavigateUrl="http://www.northsouth.org/st/game.asp#access" runat="server" onclick="window.open(this.href,'popupwindow','width=800,height=400,scrollbars,resizable');return false;">Help</asp:HyperLink></td>
            </tr>
            <tr>
            </tr>
            </table>
		
	<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0" id="tablec" runat="server">	
  <tr>
	<td align="right" style="width: 331px; height: 28px;"><asp:Label ID="lblChild" runat="server" Text="Child"></asp:Label></td>
	<td style="height: 28px"><asp:DropDownList ID="drpChild" runat="server" ValidationGroup="VG1" CausesValidation="True"></asp:DropDownList></td>
	<td style="height: 28px"><asp:Label ID="lblchildselection" runat="server" ForeColor="red"></asp:Label> &nbsp
	<asp:RequiredFieldValidator ID="rfvchild" runat="server" ControlToValidate ="drpChild" ErrorMessage="select a child" InitialValue="0" ValidationGroup="VG1"></asp:RequiredFieldValidator></td></tr>
    <tr>
	<td align="right" style="width: 331px"><asp:Label ID="lblgame" runat="server" Text="Game"></asp:Label></td>
	<td><asp:DropDownList ID="drpgame" runat="server" ValidationGroup="VG1" CausesValidation="True"></asp:DropDownList></td>
	<td><asp:Label ID="lblgameselection" runat="server" ForeColor="red"></asp:Label> &nbsp <asp:RequiredFieldValidator ID="Rfvgame" runat="server" ControlToValidate ="drpgame" InitialValue="0" ErrorMessage="select a Game" ValidationGroup ="VG1"></asp:RequiredFieldValidator></td></tr>
   </table>
  <br />
  <br />
    <table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0" id="table1" runat="server">
    <tr>
	<td colspan="1" style="width: 232px"><asp:Button ID="Btnnewrenew" runat="server" Text="New/Renew Access to Game" ValidationGroup="VG1" Enabled="false"  />
	</td>
	<td style="width:243px" align="center" ><asp:Button ID="btncontinue" runat="server" Text="Continue" CausesValidation="true" ValidationGroup="VG1" />
	<asp:ValidationSummary ID="vdd" runat="server" ValidationGroup="VG1" ShowMessageBox="true" /></td>
	<td ><asp:Button ID="btnemail" runat="server" Text="Send Email to Parents" /> </td>
	</tr>
  <tr><td colspan="2" style="width:50%"><asp:Label ID="lblerror" runat="server" ForeColor="Red" Width="736px" ></asp:Label>
 </td></tr>
    </table>
  <br />
  <br />
  <br />
<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0">

<tr>
<td  align="left" style=" height: 30px; width: 112px;"><asp:Label ID="lblAmountused" runat="server" Text="Amount used up:" Visible="false"></asp:Label></td> 
<td style="width: 130px; height: 30px"><asp:TextBox Enabled="false" ID="lblAmountused1" runat="server" Visible="False" Width="50px" ></asp:TextBox></td>
<td  align="right" style="width: 125px; height: 30px"><asp:Label ID="lblAmountunused" runat="server" Text="Unused/Available:" Visible="false"></asp:Label> </td>
<td style="height: 30px; width: 116px;"><asp:TextBox Enabled="false"   ID="txtamountunused1" runat="server"  Visible="false" Width="50px"></asp:TextBox></td>
<td align="right" style="height: 30px; width: 90px;"><asp:Label ID="lblshortfall" runat="server" Text="Shortfall:" Visible="false"></asp:Label> </td>
<td style="height: 30px"><asp:TextBox Enabled="false"  ID="txtshortfall1" runat="server" Visible="false" Width="50px" ></asp:TextBox> </td>
</tr>
<tr>
<td colspan="6" style="height: 22px"><asp:Label ID="lblnote" Visible="false" Text="Note: Only donations and DAS money contributed during the last 12 months will be considered for game access" runat="server"></asp:Label></td>
</tr>
</table >
<br />
<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0">
<tr>
<td colspan="4"><asp:Label ID="lbl1" Text="Panel 1: Game records for the selected Child" Font-Size="medium" Font-Bold="true" runat="server" Visible="false"></asp:Label></td></tr>
<tr><td colspan="6"><asp:Label ID="lblerr" runat="server" ForeColor="red" ></asp:Label></td></tr>
</table>

<table cellspacing="1" cellpadding="3" width="70%" id="table154"  align="center" border="3" runat="server" visible="false">
<tr><td colspan="6" style="height: 24px"><asp:Label ID="lblheader" runat="server" Font-Bold="true" ></asp:Label></td></tr>
<tr><td style="width: 113px; height: 27px"><asp:Label ID="lblcname" runat="server" Text="Child Name"></asp:Label></td>
<td style="height: 27px"><asp:TextBox ID="txtcname" runat="server" ReadOnly="true"></asp:TextBox></td>
<td style="width: 95px; height: 27px;"><asp:Label ID="lblPName" runat="server" Text="Parent Name"></asp:Label></td>
<td style="height: 27px"><asp:TextBox ID="txtPName" runat="server" ReadOnly="true" ></asp:TextBox></td>
<td style="width: 106px; height: 27px;"><asp:Label ID="lblDateRegistered" runat="server" Text="Date Registered" Width="96px"></asp:Label></td>
<td style="height: 27px; width: 195px;"><asp:TextBox ID="txtDateRegistered" runat="server" ReadOnly="true"></asp:TextBox></td>
</tr><tr>
<td style="width: 113px; height: 22px;"><asp:Label ID="lblClogin" runat="server" Text="Child Login ID" Width="84px"></asp:Label></td>
<td style="height: 22px"><asp:TextBox ID="txtClogin" runat="server" CausesValidation="True" ValidationGroup="VG2"></asp:TextBox></td>
<td style="width: 95px; height: 22px;"><asp:Label ID="lblCPwd" runat="server" Text="Child Password" Width="90px"></asp:Label></td>
<td style="height: 22px"><asp:TextBox ID="txtCPwd" runat="server" ValidationGroup="VG2" CausesValidation="true"></asp:TextBox>
</td>
<td style="width: 106px; height: 22px;"><asp:Label ID="lblGameName" runat="server" Text="Game Name"></asp:Label></td>
<td style="width: 195px; height: 22px;"><asp:TextBox ID="txtgname" runat="server" ReadOnly="true "></asp:TextBox></td>
<%--<td><asp:DropDownList ID="drpgamename" runat="server" ></asp:DropDownList></td>--%>
</tr>
<tr><td style="width: 113px"><asp:Label ID="lblSdate" runat="server" Text="Start Date"></asp:Label></td>
<td><asp:TextBox ID="txtSdate" runat="server"></asp:TextBox>
 </td>
<td style="width: 95px"><asp:Label ID="lblEdate" runat="server" Text="End Date"></asp:Label></td>
<td><asp:TextBox ID="txtEdate" runat="server" ReadOnly="true"></asp:TextBox></td>
<td style="width: 106px"><asp:Label ID="lblApporved" runat="server" Text="Approved"></asp:Label></td>
<td style="width: 195px"><asp:DropDownList ID="drpapporved" runat="server" >
<asp:ListItem Text="Select approval" Value="0" Selected="True" ></asp:ListItem>
<asp:ListItem Text="No" Value="No"></asp:ListItem>
<asp:ListItem Text="Yes" Value="Yes"></asp:ListItem></asp:DropDownList>
</td>
</tr>
<tr><td style="width: 113px"><asp:Label ID="lblchapter" runat="server" Text="Chapter"></asp:Label></td>
<td><asp:TextBox ID="txtchapter" runat="server" ReadOnly="true"></asp:TextBox></td>
<td style="width: 95px"><asp:Label ID="lblPhone" runat="server" Text="Phone"></asp:Label></td>
<td><asp:TextBox ID="txtPhone" runat="server"  ReadOnly="true"></asp:TextBox></td>
<td style="width: 106px"><asp:Label ID="lblemail" runat="server" Text="EMail"></asp:Label></td>
<td style="width: 195px"><asp:TextBox ID="txtemail" runat="server"  ReadOnly="true"></asp:TextBox></td>
</tr>

<tr><td colspan="2"><asp:Button ID="btnsave" runat="server" Text="Save" CausesValidation="true" ValidationGroup="VG2"/>
<asp:Button ID="btncancel" runat="server" Text="Cancel" Width="92px" CausesValidation="false"  /></td>
<td colspan="2"><asp:RequiredFieldValidator ID="RFclogin" runat="server" ControlToValidate="txtClogin" ErrorMessage="Enter child login" Width="127px" ValidationGroup="VG2"></asp:RequiredFieldValidator>
 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCPwd" ErrorMessage="Enter child password" Width="129px" ValidationGroup="VG2"></asp:RequiredFieldValidator></td> 
 <td colspan="2"><asp:Label ID="lblvalidations" runat="server" Visible="false"></asp:Label></td>
<%--<td><asp:RequiredFieldValidator ID="rfapp" runat="server" InitialValue="Select approval" ControlToValidate="drpapporved" ErrorMessage="Select Approval" ValidationGroup="VG2"></asp:RequiredFieldValidator></td>--%></tr>
<tr><td colspan="6" style="height: 38px"><asp:RegularExpressionValidator ID="reclogin" runat="server" ControlToValidate="txtClogin" ValidationExpression="^[\s\S]{4,20}$" ErrorMessage="LoginID should be between 4 and 20  characters. " ValidationGroup="VG2" Width="294px"></asp:RegularExpressionValidator>
<asp:RegularExpressionValidator ID="reclogin1" runat="server" ControlToValidate="txtClogin" ValidationExpression="^[a-zA-Z0-9][\w\.-@\-]*[a-zA-Z0-9]$" ErrorMessage="LoginID allows a letter, number or a few special charters (. _ - @)" Width="383px" ValidationGroup="VG2"></asp:RegularExpressionValidator>
<asp:RegularExpressionValidator ID="revdate" runat="server" ValidationGroup="VG2" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ControlToValidate="txtSdate" ErrorMessage="Enter date format (MM/DD/YYY)" ></asp:RegularExpressionValidator>&nbsp;
<asp:RegularExpressionValidator ID="Reccpwd" runat="server" ControlToValidate="txtCPwd" ValidationExpression="^[\s\S]{4,20}$" ErrorMessage=" Password should be between 4 and 20  characters." Width="291px" ValidationGroup="VG2">
</asp:RegularExpressionValidator><asp:RegularExpressionValidator ID="Reccpwd1" runat="server" ControlToValidate="txtCPwd" ValidationExpression="^[a-zA-Z0-9][\w\.-@\-]*[a-zA-Z0-9]$" ErrorMessage="Password allows a letter, number or a few special charters (. _ - @). " Height="17px" Width="396px" ValidationGroup="VG2"></asp:RegularExpressionValidator>
<asp:Label ID="lbldateerr" runat="server" ForeColor="red"></asp:Label>&nbsp;
</td></tr><%--<td><asp:RequiredFieldValidator ID="RFgamename" runat="server" ControlToValidate="drpgamename" ErrorMessage="Select a Game"></asp:RequiredFieldValidator></td>--%></table>
<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0"><%--<tr><td>
<asp:Label id="lberror" runat="server" ForeColor="red"></asp:Label>
</td></tr>--%>
</table>
<br />
<asp:GridView ID="grd1" runat="server"  style="margin-left: 125px" OnRowEditing="grd1_RowEditing" OnRowCancelingEdit="grd1_RowCancelingEdit" OnRowUpdating="grd1_RowUpdating" AutoGenerateColumns="false" >
<Columns>
<asp:TemplateField>
<ItemTemplate >
<asp:LinkButton ID="lnkgrd1" runat="server" Text="Edit"  CommandName="Edit" Visible="false" CausesValidation="false"></asp:LinkButton>
</ItemTemplate>
<EditItemTemplate>
      <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update" ValidationGroup="VG4"  />
      <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false"/>
      </EditItemTemplate>
      <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Width="150px" Font-Size="Small" ForeColor="Black" />
     </asp:TemplateField>
     
    <asp:TemplateField HeaderText="Game ID">
    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small"  ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblgmid" runat="server" Text='<%#Eval("GameID")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Child Name" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="200px" Height="10px" />
     <ItemTemplate>
              <asp:Label ID="lblcname" runat="server" Text='<%#Eval("ChildName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
    
     <asp:TemplateField HeaderText="Game Name" ItemStyle-Width="400px" ItemStyle-Height="10px" >
     <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblgname" runat="server" Text='<%#Eval("name")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Create Date" ItemStyle-HorizontalAlign="Center">
        <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblcdate" runat="server" Text='<%#Eval("CreateDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
             
          <asp:TemplateField HeaderText="Child Login ID" >
          <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black"/>
     <ItemTemplate>
              <asp:Label ID="lblcid" runat="server" Text='<%#Eval("childLoginID")%>'></asp:Label>
           </ItemTemplate>
          <EditItemTemplate>
               <asp:TextBox runat="server" ID="txtcid"  Text='<%#Eval("ChildLoginID")%>' Width="65px" />
               <asp:RequiredFieldValidator ID="rfcl" runat="server" ControlToValidate="txtcid" ErrorMessage="Enter ChildID" ValidationGroup="VG4"></asp:RequiredFieldValidator>
               <asp:RegularExpressionValidator ID="reclogin" runat="server" ControlToValidate="txtcid" ValidationGroup="VG4" ValidationExpression="^[\s\S]{4,20}$" ErrorMessage="LoginID should be between 4 and 20 characters"></asp:RegularExpressionValidator>
               <asp:RegularExpressionValidator ID="reclogin1" runat="server" ControlToValidate="txtCid" ValidationGroup="VG4" ValidationExpression="^[a-zA-Z0-9][\w\.-@\-]*[a-zA-Z0-9]$" ErrorMessage="LoginID allows a letter, number or a few special charters (. _ - @)"></asp:RegularExpressionValidator>

                </EditItemTemplate>
          </asp:TemplateField>
          
          <asp:TemplateField HeaderText="Child Password" >
          <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="150px" />
             <ItemTemplate>
              <asp:Label ID="lblcpd" runat="server" Text='<%#Eval("ChildPWD")%>'></asp:Label>
           </ItemTemplate>
                   <EditItemTemplate>
                   
               <asp:TextBox runat="server" ID="txtcpwd1"  Text='<%#Eval("ChildPWD")%>' Width="65px" />
               <asp:RequiredFieldValidator ID="lblcpd" runat="server" ControlToValidate="txtcpwd1" ValidationGroup="VG4" ErrorMessage="Enter Password"></asp:RequiredFieldValidator>
               <asp:RegularExpressionValidator ID="recpswd" runat="server" ControlToValidate="txtcpwd1" ValidationGroup="VG4" ValidationExpression="^[\s\S]{4,20}$" ErrorMessage="password should be between 4 and 20 characters"></asp:RegularExpressionValidator>
<asp:RegularExpressionValidator ID="recpswd1" runat="server" ControlToValidate="txtcpwd1" ValidationGroup="VG4" ValidationExpression="^[a-zA-Z0-9][\w\.-@\-]*[a-zA-Z0-9]$" ErrorMessage="Password allows a letter, number or a few special charters (. _ - @)"></asp:RegularExpressionValidator>

               <%--<asp:RegularExpressionValidator  ID="REV1" runat="server" ControlToValidate="txtamtcol" ErrorMessage="Enter Valid Amount Format (nnn.nn)" ValidationExpression="^\$?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?$"></asp:RegularExpressionValidator> --%>
          </EditItemTemplate>
</asp:TemplateField>

        <asp:TemplateField HeaderText="Approved" >
<HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black"  Width="200px" />
     <ItemTemplate>
              <asp:Label ID="lblapp" runat="server" Text='<%#Eval("Approved")%>'></asp:Label>
           </ItemTemplate>
           <EditItemTemplate>
           <asp:DropDownList ID="drpapp" runat="server" DataTextFormatString='<%#Eval("Approved")%>'>
           <asp:ListItem Text="Select Approval" Value=""></asp:ListItem>
           <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
           <asp:ListItem Text="No" Value = "No"></asp:ListItem>
           </asp:DropDownList>
           <asp:Label ID="lblapproved" runat="server"  ForeColor="red"></asp:Label>
           <%--<asp:RequiredFieldValidator ID="rfapp1" runat="server" InitialValue="Select Approval" ControlToValidate="drpapp" ErrorMessage="Select Approval" ValidationGroup="VG4"></asp:RequiredFieldValidator>
           --%>
           </EditItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="250px" />
     <ItemTemplate>
              <asp:Label ID="lblsd" runat="server" Text='<%#Eval("StartDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           <EditItemTemplate>
           <asp:TextBox ID="txtSdate" runat="server" Text='<%#Eval("StartDate","{0:MM/dd/yyyy}")%>'></asp:TextBox>
           <asp:Label ID="lblstdate" runat="server"  ForeColor="red"></asp:Label>
         <%--<asp:RequiredFieldValidator ID="tfsdate" runat="server" ValidationGroup="VG4" ControlToValidate="txtSdate" ErrorMessage="Enter Start Date" ></asp:RequiredFieldValidator>--%>
           <asp:RegularExpressionValidator ID="revdate" runat="server" ValidationGroup="VG4" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ControlToValidate="txtSdate" ErrorMessage="Enter date format (MM/DD/YYY)" ></asp:RegularExpressionValidator>
           
           </EditItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="End Date"  ItemStyle-HorizontalAlign="Center" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="150px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lbled" runat="server" Text='<%#Eval("EndDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Parent Name" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="200px" ForeColor="Black" Height="10px" />
     <ItemTemplate>
              <asp:Label ID="lblpname" runat="server" Text='<%#Eval("ParentName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblac" runat="server" Text='<%#Eval("Active")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Phone" ItemStyle-Width="500px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblhp" runat="server" Text='<%#Eval("hphone")%>' Height="10px"></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Email" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="200px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblem" runat="server" Text='<%#Eval("email")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter ID" ItemStyle-Width="100px" ItemStyle-Height="10px" ItemStyle-HorizontalAlign="Center">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblci" runat="server" Text='<%#Eval("ChapterID")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter" ItemStyle-Width="400px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="10px" />
     <ItemTemplate>
              <asp:Label ID="lblc" runat="server" Text='<%#Eval("Chapter")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="City" ItemStyle-Width="400px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="10px" />
     <ItemTemplate>
              <asp:Label ID="lblcity" runat="server" Text='<%#Eval("City")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="State" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblstate" runat="server" Text='<%#Eval("State")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           
           
</Columns>
</asp:GridView>
<br />
<table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0">
<tr><td colspan="2">
<asp:Label ID="lbl2" runat="server" Text=" Panel 2: Game records for other children in the family " Font-Size="Medium" Font-Bold="True" Visible="False"></asp:Label>
</td></tr>
<tr>
<td align="left" style="height:10px"><asp:Label ID="lblAU" runat="server" Text="Amount used up:" Visible="False" Width="145px"></asp:Label>
<asp:TextBox Enabled="false" ID="txtAU1" runat="server" Visible="false" Width="50px" ></asp:TextBox></td> </tr>
<tr><td style="width: 129px" colspan="2"><asp:Label ID="lblerr3" runat="server" ForeColor="red" ></asp:Label></td></tr></table>
<br />
    <asp:GridView ID="grd2" runat="server" AutoGenerateColumns="false"  style="margin-left: 125px" HeaderStyle-BackColor="#CCCC99" HeaderStyle-Font-Size="XX-Small"  HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="black">
    
    <Columns>
    <asp:TemplateField HeaderText="Game ID" >
    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small"  ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblgmid" runat="server" Text='<%#Eval("GameiD")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Child Name" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="200px" />
     <ItemTemplate>
              <asp:Label ID="lblcname" runat="server" Text='<%#Eval("childName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
    
     <asp:TemplateField HeaderText="Game Name" ItemStyle-Width="400px" ItemStyle-Height="10px" >
     <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="150px" />
     <ItemTemplate>
              <asp:Label ID="lblgname" runat="server" Text='<%#Eval("name")%>'></asp:Label>
           </ItemTemplate>
                </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Create Date"  ItemStyle-HorizontalAlign="Center">
        <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblcdate" runat="server" Text='<%#Eval("CreateDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
             
          <asp:TemplateField HeaderText="Child Login ID" >
          <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="150px" />
     <ItemTemplate>
              <asp:Label ID="lblcid" runat="server" Text='<%#Eval("childLoginID")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
          
          <asp:TemplateField HeaderText="Child Password" >
          <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="150px" />
             <ItemTemplate>
              <asp:Label ID="lblcpd" runat="server" Text='<%#Eval("ChildPWD")%>'></asp:Label>
           </ItemTemplate>
                   </asp:TemplateField>

        <asp:TemplateField HeaderText="Approved" >
<HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblapp" runat="server" Text='<%#Eval("Approved")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="150px" />
     <ItemTemplate>
              <asp:Label ID="lblsd" runat="server" Text='<%#Eval("StartDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="End Date" ItemStyle-HorizontalAlign="Center" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="150px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lbled" runat="server" Text='<%#Eval("EndDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Parent Name" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="200px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblpname" runat="server" Text='<%#Eval("ParentName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Active"  ItemStyle-HorizontalAlign="Center">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblac" runat="server" Text='<%#Eval("Active")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Phone" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblhp" runat="server" Text='<%#Eval("hphone")%>' Width="100px"></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Email" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="200px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblem" runat="server" Text='<%#Eval("email")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter ID" ItemStyle-HorizontalAlign="Center">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblci" runat="server" Text='<%#Eval("ChapterID")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter" ItemStyle-Width="400px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblc" runat="server" Text='<%#Eval("Chapter")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="City" ItemStyle-Width="400px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblcity" runat="server" Text='<%#Eval("City")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="State" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblstate" runat="server" Text='<%#Eval("State")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           
           
</Columns>
    </asp:GridView>
    <br />
    <table  cellspacing="1" cellpadding="3" width="80%"  align="center" border="0">
    <tr><td colspan="4"><asp:Label ID="lbl3" runat="server" Text="Panel 3: DAS Collections " Font-Size="medium" Font-Bold="True" Visible="False" Width="199px"></asp:Label></td></tr>
    <tr><td  align="left" style="width: 148px">
<asp:Label ID="lblDAS" runat="server" Text="Cumulative DAS Amount:" Visible="false"></asp:Label></td>
<td style="width: 159px"><asp:TextBox Enabled="false" ID="txtDAS1" runat="server" Visible="false" Width="50px"></asp:TextBox> </td> 
<td  align="right" style="width: 117px"><asp:Label ID="lblDAS50" runat="server" Text="DAS at 50%:" Visible="false"></asp:Label></td>
<td><asp:TextBox Enabled="false"  ID="txtDAS501" runat="server" Visible="false" Width="50px"></asp:TextBox></td></tr>
<tr><td style="width: 148px" colspan="4"><asp:Label ID="lblerr4" runat="server" ForeColor="red" ></asp:Label></td></tr></table>
    <br /><asp:GridView ID="grd3" runat="server" AutoGenerateColumns="false"  style="margin-left: 125px" HeaderStyle-BackColor="#CCCC99" HeaderStyle-Font-Size="XX-Small"  HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="black" >
    <Columns>
    <asp:TemplateField HeaderText = "DASID" HeaderStyle-ForeColor="Black" >
     <ItemTemplate>
              <asp:Label ID="lbldasid" runat="server" Text='<%#Eval("DASID")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Child Name" HeaderStyle-ForeColor="Black" ItemStyle-Width="100px" ItemStyle-Height="10px" >
    <ItemTemplate>
              <asp:Label ID="lblcname" runat="server" Text='<%#Eval("ChildName")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Created Date" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="Center" >
    <ItemTemplate>
              <asp:Label ID="lblcdate" runat="server" Text='<%#Eval("CreateDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Amount Collected" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="right">
    <ItemTemplate>
              <asp:Label ID="lblAmtcol" runat="server" Text='<%#Eval("AmountCol","{0:n2}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Date Mailed" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="Center" >
            <ItemTemplate>
              <asp:Label ID="lbldmail" runat="server" Text='<%#Eval("DateMailed","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Amount Received" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="right">
    <ItemTemplate>
              <asp:Label ID="lblarec" runat="server" Text='<%#Eval("AmountRec","{0:n2}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField  HeaderText = "Date Recieved" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="Center">
    <ItemTemplate>
              <asp:Label ID="lbldrec" runat="server" Text='<%#Eval("DateReceived","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Parent Name" HeaderStyle-ForeColor="Black" ItemStyle-Width="100px" ItemStyle-Height="10px" >
    <ItemTemplate>
              <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PName")%>' Width="200px"></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
     </Columns>
    </asp:GridView><br />
    <table cellspacing="1" cellpadding="3" width="80%"  align="center" border="0">
    <tr><td style="width: 152px" colspan="6">
<asp:Label ID="lbldonation" runat="server" Font-Size="medium" Font-Bold="true" Text="Panel 4: Donations" Visible="false"></asp:Label></td></tr>
    <tr><td style="width: 152px">
    <asp:Label ID="lblCdonations" runat="server" Text="Cumulative Donations:" Visible="false"></asp:Label></td>
<td style="width: 155px"><asp:TextBox Enabled="false" ID="txtCdonations1" runat="server" Visible="false" Width="50px"></asp:TextBox> </td> 
<td style="width: 237px">
    <asp:Label ID="lbltaxdeductible" runat="server" Text="Cumulative Tax-deductible Donations:" Visible="false"></asp:Label></td>
<td><asp:TextBox Enabled="false" ID="txttaxdeductible" runat="server" Visible="false" Width="50px"></asp:TextBox> </td></tr>
<tr><td style="width: 152px" colspan="6">
<asp:Label ID="lblerr5" runat="server" ForeColor="red" ></asp:Label></td></tr></table>
    <br /><asp:GridView ID="grd4" runat="server" AutoGenerateColumns="false" style="margin-left: 125px" HeaderStyle-BackColor="#CCCC99" HeaderStyle-Font-Size="XX-Small"  HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="black" >
        <Columns>
        
        <asp:TemplateField HeaderText = "Donation ID" HeaderStyle-ForeColor="Black" >
            <ItemTemplate>
              <asp:Label ID="lbldid" runat="server" Text='<%#Eval("DonationID")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Parent Name" HeaderStyle-ForeColor="Black" ItemStyle-Width="100px" ItemStyle-Height="10px" >
    <ItemTemplate>
              <asp:Label ID="lblpname" runat="server" Text='<%#Eval("Parentname")%>' ></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Amount" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="right" >
    <ItemTemplate>
              <asp:Label ID="lblamt" runat="server" Text='<%#Eval("Amount","{0:n2}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "TaxDeduction" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="right">
    <ItemTemplate>
              <asp:Label ID="lblTaxDeduction" runat="server" Text='<%#Eval("TaxDeduction","{0:n2}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText = "Donation Date" HeaderStyle-ForeColor="Black" ItemStyle-HorizontalAlign="Center" >
            <ItemTemplate>
              <asp:Label ID="lbldd" runat="server" Text='<%#Eval("DonationDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
        </asp:TemplateField>
        
        
        
        </Columns>
    </asp:GridView><br /> 
    <asp:SqlDataSource ID="ChapterDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="Select  ProductID ,Name  from Product where eventid=4 and status='O'" SelectCommandType="Text"></asp:SqlDataSource>

</asp:Content>

