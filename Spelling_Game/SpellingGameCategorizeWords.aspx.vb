﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.UI.Page
Imports System.Text.RegularExpressions
Imports System.Data

Partial Class Spelling_Game_SpellingGameCategorizeWords
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Sql As String
        If ddlEvent.SelectedValue = "Regional" Then
            Sql = "SELECT Distinct W.Word, W.Category_Root,SB.RegContest FROM word_master_new  W INNER JOIN SBPubCriteria SB ON SB.Level = W.Level_NSF AND SB.SubLevel = W.[Sub-Level_NSF] WHERE W.Regional_Flag ='Y' Order By SB.RegContest,W.Category_Root,W.Word"
        Else
            Sql = "SELECT  Distinct W.Word, W.Category_Root,SB.NatContest  FROM word_master_new  W INNER JOIN SBPubCriteria SB ON SB.Level = W.Level_NSF AND SB.SubLevel = W.[Sub-Level_NSF] WHERE W.Nat_Flag  ='Y' Order By SB.RegContest,W.Category_Root,W.Word"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Sql)
        Dim i As Integer
        Dim FileExPath, FilenewPath, alphabet As String
        FilenewPath = Server.MapPath(ddlEvent.SelectedValue & "SBLanguage")
        If Directory.Exists(FilenewPath) = True Then
            Dim di As New System.IO.DirectoryInfo(FilenewPath)
            di.Delete(True)
        End If
        CreateDirectory(FilenewPath)
        CreateDirectory(Server.MapPath(ddlEvent.SelectedValue & "SBLanguage\JSB"))
        CreateDirectory(Server.MapPath(ddlEvent.SelectedValue & "SBLanguage\SSB"))
        Dim SucWordCount As Integer = 0
        Dim FailWordCount As Integer = 0
        For i = 0 To ds.Tables(0).Rows.Count - 1

            If ds.Tables(0).Rows(i)(2).ToString().Trim() = "SSB" Then
                FilenewPath = Server.MapPath(ddlEvent.SelectedValue & "SBLanguage\SSB\" & ds.Tables(0).Rows(i)(1).ToString().Trim())
            Else
                FilenewPath = Server.MapPath(ddlEvent.SelectedValue & "SBLanguage\JSB\" & ds.Tables(0).Rows(i)(1).ToString().Trim())
            End If
            If Directory.Exists(FilenewPath) = False Then
                CreateDirectory(FilenewPath)
            End If
            If ds.Tables(0).Rows(i)(2).ToString().Trim() = "SSB" Then
                FilenewPath = ddlEvent.SelectedValue & "SBLanguage/SSB/" & ds.Tables(0).Rows(i)(1).ToString().Trim()
            Else
                FilenewPath = ddlEvent.SelectedValue & "SBLanguage/JSB/" & ds.Tables(0).Rows(i)(1).ToString().Trim()
            End If

            alphabet = ds.Tables(0).Rows(i)("Word").ToString().Trim().Substring(0, 1)
            Select Case (alphabet)
                Case "a", "b"
                    FileExPath = "Bee_audio_AB/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "c"
                    FileExPath = "Bee_audio_C/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "d", "e"
                    FileExPath = "Bee_audio_DE/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "f", "g", "h"
                    FileExPath = "Bee_audio_FH/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "i", "j", "k", "l"
                    FileExPath = "Bee_audio_IL/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "m", "n", "o"
                    FileExPath = "Bee_audio_MO/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "p"
                    FileExPath = "Bee_audio_P/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "q", "r"
                    FileExPath = "Bee_audio_QR/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "s"
                    FileExPath = "Bee_audio_S/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
                    Exit Select

                Case "t", "u", "v", "w", "x", "y", "z"
                    FileExPath = "Bee_audio_TZ/" & ds.Tables(0).Rows(i)("Word").ToString().Trim()
            End Select

            If File.Exists(Server.MapPath(FileExPath & ".wav")) = True Then
                SucWordCount = SucWordCount + 1
                ' Response.Write(FilenewPath & "/" & ds.Tables(0).Rows(i)("Word").ToString().Trim() & ".wav" & "<br>")
                File.Copy(Server.MapPath(FileExPath & ".wav"), Server.MapPath(FilenewPath & "\" & ds.Tables(0).Rows(i)("Word").ToString().Trim() & ".wav"))
            Else
                FailWordCount = FailWordCount + 1
                'Response.Write(Server.MapPath(FileExPath & ".wav") & "<br>")
            End If
            ''Session["audio"] = FileExPath + ".wav";
        Next
        lblVErr.Text = "Total Files moved successfully : " & SucWordCount & "<br> Total Files unavailable : " & FailWordCount
    End Sub

    Function CreateDirectory(ByVal path As String) As Boolean
        Dim flag As Boolean = False
        Try
            If Directory.Exists(path) Then
                Response.Write("This Folder already Exist")
            Else
                Directory.CreateDirectory(path)
                flag = True
            End If
        Catch ex As Exception
            Response.Write(path)
            Response.Write(ex.ToString)
        End Try
        Return flag
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblVErr.Text = ""
    End Sub
End Class
