using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Media;

public partial class Spelling_Demo_Demo_SpellingGame_DisplayWord : System.Web.UI.Page
{
    private static string strNextClicked;

    private static int endResultCorrect;
    private static int endResultWrong;

    private static int questionCount;

    public int QuestionCount
    {
        get { return questionCount; }
        set { questionCount = value; }
    }


    private int level;
    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    private int subLevel;
    public int SubLevel
    {
        get { return subLevel; }
        set { subLevel = value; }
    }

    private static DataSet wordsDataSet;
    public DataSet dsDemoWords
    {
        get { return wordsDataSet; }
        set { wordsDataSet = value; }
    }

      protected void Page_Load(object sender, EventArgs e)
    {
          // User code to initialize the page here
        if (!IsPostBack)
        {
            if (Request.QueryString["category"] != null)
            {
                int category = int.Parse(Request.QueryString["category"].ToString());
                Level = category / 10;
                SubLevel = category % 10;
                SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                dsDemoWords = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "vusp_GetDemoSpellingGameDetails", new SqlParameter("@Level", Level), new SqlParameter("@SubLevel", SubLevel));
                dsDemoWords.Tables[0].Columns.Add("Spelt");
                frmViewDemoWords.DataSource = dsDemoWords;
                frmViewDemoWords.DataBind();
                QuestionCount = 1;
                strNextClicked = string.Empty;
                endResultCorrect = 0;
                endResultWrong = 0;
            }
            else
            {
                Response.Redirect("Demo_SpellingGame.aspx");
            }
        }

        StringBuilder strShowControlScript = new StringBuilder();
        strShowControlScript.Append("<script language=JavaScript>");
        strShowControlScript.Append("function ShowControl(controlID){ var displayControl = document.getElementById(controlID); displayControl.style.display = 'block';  return false;}");
        strShowControlScript.Append("</script>");
        RegisterClientScriptBlock("ShowControlScript", strShowControlScript.ToString());

        if (strNextClicked == "NextClicked")
        {
            frmViewDemoWords.DataSource = dsDemoWords;
            frmViewDemoWords.DataBind();
            frmViewDemoWords.PageIndex = QuestionCount;
            strNextClicked = string.Empty;
        }
    }

    protected void frmViewDemoWords_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {
            // The ItemCommand event is raised when any button within the FormView control is clicked. 
            // Use the CommandName property to determine which button was clicked. 

            // Use the Row property to retrieve the data row.
            FormViewRow row = frmViewDemoWords.Row;

            if (e.CommandName == "RepeatWord")
            {
                frmViewDemoWords.DataSource = dsDemoWords;
                frmViewDemoWords.DataBind();
            }
            else if (e.CommandName == "SubmitWord")
            {
                Label lblWord = (Label)row.FindControl("lblWord");
                TextBox txtWord = (TextBox)row.FindControl("txtWord");
                Label lblDefinition = (Label)row.FindControl("lblDefinition");
                Label lblSentence = (Label)row.FindControl("lblSentence");
                Label lblLanguageOrigin = (Label)row.FindControl("lblLanguageOrigin");
                Label lblPartofSpeech = (Label)row.FindControl("lblPartofSpeech");

                if ((lblWord != null) && (txtWord != null) && (lblDefinition != null) && (lblSentence != null) && (lblLanguageOrigin != null) && (lblPartofSpeech != null))
                {
                    frmViewDemoWords.Visible = false;
                    lblWrongAttemptCount.Text = QuestionCount.ToString();

                    if (lblWord.Text != txtWord.Text.Trim())
                    {
                        panelWrongAnswer.Visible = true;
                        panelCorrectAnswer.Visible = false;
                        panelControls.Visible = true;
                        panelEndResult.Visible = false;

                        lblWrongGivenAnswer.Text = txtWord.Text.Trim();
                        lblWrongCorrectWord.Text = lblWord.Text;
                        lblWrongDefinition.Text = lblDefinition.Text;
                        lblWrongSentence.Text = lblSentence.Text.Replace("___________", lblWord.Text);
                        lblWrongPartofSpeech.Text = lblPartofSpeech.Text;
                        lblWrongAttemptCount.Text = QuestionCount.ToString();

                        dsDemoWords.Tables[0].Rows[QuestionCount - 1]["Spelt"] = "Wrong";
                        endResultWrong++;
                    }
                    else
                    {
                        panelWrongAnswer.Visible = false;
                        panelCorrectAnswer.Visible = true;
                        panelControls.Visible = true;
                        panelEndResult.Visible = false;

                        lblCorrectCorrectWord.Text = lblWord.Text; ;
                        lblCorrectAttemptCount.Text = QuestionCount.ToString();

                        dsDemoWords.Tables[0].Rows[QuestionCount - 1]["Spelt"] = "Correct";
                        endResultCorrect++;
                    }
                }
            }
         
            //Page.MaintainScrollPositionOnPostBack = true;
        }
        catch
        {
        }
    }

    protected void btnNextWord_Click(object sender, EventArgs e)
    {
        if (QuestionCount < dsDemoWords.Tables[0].Rows.Count)
        {
            //QuestionCount++;
            strNextClicked = "NextClicked";

            frmViewDemoWords.DataSource = dsDemoWords;
            frmViewDemoWords.DataBind();
            frmViewDemoWords.PageIndex = QuestionCount;

            QuestionCount++;

            frmViewDemoWords.Visible = true;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = false;
            Page_Load(sender, e);
        }
        else
        {
            frmViewDemoWords.Visible = false;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = true;
            lblEndResultAttempt.Text = QuestionCount.ToString();
            lblEndResultCorrect.Text = endResultCorrect.ToString();
            lblEndResultWrong.Text = endResultWrong.ToString();
        }
    }

    protected void frmViewDemoWords_DataBound(object sender, EventArgs e)
    {
        FormViewRow row = frmViewDemoWords.Row;

        TextBox txtWord = (TextBox)row.FindControl("txtWord");
        Button btnSubmitWord = (Button)row.FindControl("btnSubmitWord");
        if ((txtWord != null) && (btnSubmitWord != null))
        {
            Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + txtWord.ClientID + "').focus();</script>");
            txtWord.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSubmitWord.UniqueID + "').click();return false;}} else {return true}; ");
        }

        Label lblWord = (Label)row.FindControl("lblWord");
        Label lblSentence = (Label)row.FindControl("lblSentence");

        if ((lblWord != null) && (lblSentence != null))
        {
            lblSentence.Text = lblSentence.Text.Replace(lblWord.Text, "___________");
        }

        Button btnDefinition = (Button)row.FindControl("btnDefinition");
        Label lblDefinition = (Label)row.FindControl("lblDefinition");
        if ((btnDefinition != null) && (lblDefinition != null))
        {
            btnDefinition.OnClientClick = "javascript:return ShowControl('" + lblDefinition.ClientID + "')";
        }

        Button btnSentence = (Button)row.FindControl("btnSentence");
        if ((btnSentence != null) && (lblSentence != null))
        {
            btnSentence.OnClientClick = "javascript:return ShowControl('" + lblSentence.ClientID + "')";
        }

        Button btnLanguageOrigin = (Button)row.FindControl("btnLanguageOrigin");
        Label lblLanguageOrigin = (Label)row.FindControl("lblLanguageOrigin");
        if ((btnLanguageOrigin != null) && (lblLanguageOrigin != null))
        {
            btnLanguageOrigin.OnClientClick = "javascript:return ShowControl('" + lblLanguageOrigin.ClientID + "')";
        }

        Button btnPartofSpeech = (Button)row.FindControl("btnPartofSpeech");
        Label lblPartofSpeech = (Label)row.FindControl("lblPartofSpeech");
        if ((btnPartofSpeech != null) && (lblPartofSpeech != null))
        {
            btnPartofSpeech.OnClientClick = "javascript:return ShowControl('" + lblPartofSpeech.ClientID + "')";
        }

        try
        {
            string wavFile = string.Empty;
            string alphabet = lblWord.Text.ToLower().Substring(0, 1);
            switch (alphabet)
            {
                case "a":
                case "b":
                    wavFile = "Bee_audio_AB/" + lblWord.Text;
                    break;

                case "c":
                    wavFile = "Bee_audio_C/" + lblWord.Text;
                    break;

                case "d":
                case "e":
                    wavFile = "Bee_audio_DE/" + lblWord.Text;
                    break;

                case "f":
                case "g":
                case "h":
                    wavFile = "Bee_audio_FH/" + lblWord.Text;
                    break;

                case "i":
                case "j":
                case "k":
                case "l":
                    wavFile = "Bee_audio_IL/" + lblWord.Text;
                    break;

                case "m":
                case "n":
                case "o":
                    wavFile = "Bee_audio_MO/" + lblWord.Text;
                    break;

                case "p":
                    wavFile = "Bee_audio_P/" + lblWord.Text;
                    break;

                case "q":
                case "r":
                    wavFile = "Bee_audio_QR/" + lblWord.Text;
                    break;

                case "s":
                    wavFile = "Bee_audio_S/" + lblWord.Text;
                    break;

                case "t":
                case "u":
                case "v":
                case "w":
                case "x":
                case "y":
                case "z":
                    wavFile = "Bee_audio_TZ/" + lblWord.Text;
                    break;
            }
            ViewState["wavpath"] = wavFile + ".wav";
            // Play Sound File.

            Session["audio"] = wavFile + ".wav";
            
            //Response.Write(Session["audio"]);      
            //System.Media.SoundPlayer myPlayer = new System.Media.SoundPlayer();
            //myPlayer.SoundLocation = Server.MapPath(".") + wavFile + ".wav";
            //myPlayer.Play();

          
           
           }
        catch
        {
            //lblerr.Text = Server.MapPath("."); 
        }
    }
 
    protected void btnSelectAnotherWordCategory_Click(object sender, EventArgs e)
    {
        Response.Redirect("Demo_SpellingGame.aspx");
    }

    protected void btnViewWordList_Click(object sender, EventArgs e)
    {
        Session["WordListDataSet"] = dsDemoWords;
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language=JavaScript>");
        strScript.Append("window.open('Demo_SpellingGame_ViewWordList.aspx', \"\",\"height=500,width=400,left=0,top=0,toolbar=no,menubar=no,scrollbars=yes\");");
        strScript.Append("</script>");
       RegisterClientScriptBlock("subscribescript", strScript.ToString());
        
    }

    protected void btnEndSession_Click(object sender, EventArgs e)
    {
        frmViewDemoWords.Visible = false;
        panelControls.Visible = false;
        panelCorrectAnswer.Visible = false;
        panelWrongAnswer.Visible = false;
        panelEndResult.Visible = true;
        lblEndResultAttempt.Text = QuestionCount.ToString();
        lblEndResultCorrect.Text = endResultCorrect.ToString();
        lblEndResultWrong.Text = endResultWrong.ToString();
    }

    }


