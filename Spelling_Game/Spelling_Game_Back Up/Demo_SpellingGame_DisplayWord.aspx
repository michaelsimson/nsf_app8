<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"
    CodeFile="Demo_SpellingGame_DisplayWord.aspx.cs" Inherits="Spelling_Demo_Demo_SpellingGame_DisplayWord"
    Title="Spelling Bee Preparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
        <tr>
            <td>
                <a href="../MainTest.aspx" id="hrefQuitDemo">Quit Demo</a>
                <p>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <b>1. DO NOT</b> use your browser's BACK and FORWARD buttons to navigate. You may
                get inaccurate results.
                <br />
                <b>2.</b> Your results for this session will not be saved if you attempt <b>less than
                    10 words.</b>
                <br />
                <b>3.</b> To <b>End a Game</b> click <b>End Session</b> button, otherwise your results
                for this session will not be saved. This button will appear once you spell a word.
                <br />
                <p>
                </p>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="100%">
        <tr>
            <td width="10%" align="center" valign="middle">
                <img src="images/bee_icon.gif" />
            </td>
            <td>
                <asp:FormView ID="frmViewDemoWords" runat="server" OnItemCommand="frmViewDemoWords_ItemCommand" OnDataBound="frmViewDemoWords_DataBound"
                    AllowPaging="True" Width="80%" >
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <b>Spell the word:</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtWord" />
                                    <asp:RequiredFieldValidator runat="server" ID="RFVword" ControlToValidate="txtWord" ValidationGroup="VG1" ErrorMessage="Enter Word"></asp:RequiredFieldValidator>
                                    <asp:Label runat="server" ID="lblWord" Text='<%# Bind("Word") %>' Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnRepeatWord" runat="server" Text="Repeat Word" CommandName="RepeatWord" CausesValidation="false" />
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmitWord" runat="server" Text="Submit Word" CommandName="SubmitWord" ValidationGroup="VG1" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <br />
                                    <b>Show me...</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnDefinition" runat="server" Text="Definition" />
                                </td>
                                <td>
                                    <asp:Label ID="lblDefinition" runat="server" Text='<%# Bind("Definitions") %>' Style="display: none"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSentence" runat="server" Text="Sentence" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSentence" runat="server" Text='<%# Bind("Sentence") %>' Style="display: none"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnLanguageOrigin" runat="server" Text="Language Origin" />
                                </td>
                                <td>
                                    <asp:Label ID="lblLanguageOrigin" runat="server" Text='<%# Bind("[Language Origin]") %>'
                                        Style="display: none"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnPartofSpeech" runat="server" Text="Part of Speech" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPartofSpeech" runat="server" Text='<%# Bind("[Parts of Speech]") %>'
                                        Style="display: none"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <PagerSettings Visible="False" />
                </asp:FormView>
                <asp:Panel ID="panelWrongAnswer" runat="server" Visible="false" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="100%">
                        <tr>
                            <td>
                            <asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label>
                                <b><i>Sorry!</i></b> You spelt <b>
                                    <asp:Label ID="lblWrongGivenAnswer" runat="server"></asp:Label></b>.
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                The correct spelling is <b>
                                    <asp:Label ID="lblWrongCorrectWord" runat="server"></asp:Label></b>.
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Definition:</b>
                                <asp:Label ID="lblWrongDefinition" runat="server"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Sentence:</b>
                                <asp:Label ID="lblWrongSentence" runat="server"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Part of Speech:</b>
                                <asp:Label ID="lblWrongPartofSpeech" runat="server"></asp:Label>
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You have attempted <b>
                                    <asp:Label ID="lblWrongAttemptCount" runat="server"></asp:Label>
                                    word(s)</b> in this session.
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="panelCorrectAnswer" runat="server" Visible="false" Width="100%">
                    You spelt <b>
                        <asp:Label ID="lblCorrectCorrectWord" runat="server"></asp:Label></b> correctly.
                    Good for you.
                    <br />
                    You have attempted <b>
                        <asp:Label ID="lblCorrectAttemptCount" runat="server"></asp:Label>
                        word(s)</b> in this session.
                    <br />
                    <br />
                </asp:Panel>
                <asp:Panel ID="panelControls" runat="server" Visible="false" Width="100%">
                    <asp:Button ID="btnNextWord" runat="server" Text="Next Word" OnClick="btnNextWord_Click" />
                    <asp:Button ID="btnEndSession" runat="server" Text="End Session" OnClick="btnEndSession_Click" />
                </asp:Panel>
                <asp:Panel ID="panelEndResult" runat="server" Visible="false" Width="100%">
                    You attempted
                    <asp:Label ID="lblEndResultAttempt" runat="server"></asp:Label>
                    words out of which you answered<br />
                    <br />
                    <br />
                    <asp:Label ID="lblEndResultCorrect" runat="server"></asp:Label>
                    <img src="images/correct.gif" />
                    <br />
                    <asp:Label ID="lblEndResultWrong" runat="server"></asp:Label>
                    <img src="images/wrong.gif" />
                    <br />
                    <br />
                    <asp:Button ID="btnViewScore" runat="server" Text="View Score" />&nbsp;&nbsp;
                    <asp:Button ID="btnViewWordList" runat="server" Text="View Word List" OnClick="btnViewWordList_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnSelectAnotherWordCategory" runat="server" Text="Select Another Word Category"
                        OnClick="btnSelectAnotherWordCategory_Click" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
