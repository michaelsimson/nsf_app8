<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GamePerformanceGraph.aspx.cs"
    Inherits="spelling_Bee_GamePerformanceGraph" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Game Performance Graph</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="gvPerformanceGraph" runat="server" Visible="false" AutoGenerateColumns="False"
                CellPadding="4" Width="80%" ForeColor="#333333" BorderColor="#CC9966" BorderStyle="None" OnRowDataBound="gvPerformanceGraph_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="Word Category" DataField="WordCategory" />
                    <asp:TemplateField HeaderText="Percentage (%)">
                        <ItemTemplate>
                            <asp:Label ID="lblPercentage" style="text-align:center" runat="server" Text='<%# Bind("Percentage") %>' ForeColor="White" BackColor="#004080"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <br />
            <asp:Label ID="lblNoRecords" runat="server" Text="No Record Found!" Visible="false"></asp:Label>
        </div>
    </form>
</body>
</html>
