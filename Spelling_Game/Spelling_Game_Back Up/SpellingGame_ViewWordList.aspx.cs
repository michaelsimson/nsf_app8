using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Spelling_Demo_Demo_SpellingGame_ViewWordList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["WordListDataSet"] != null)
        {
            gvEndResult.DataSource = (DataSet)Session["WordListDataSet"];
            gvEndResult.DataBind();
        }
    }
}
