using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Spelling_Demo_Demo_SpellingGame : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["UserID"] = "user1";
        if (Session["UserID"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
        Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + btnStartSpellingBee.ClientID + "').focus();</script>");
    }

    protected void btnStartSpellingBee_Click(object sender, EventArgs e)
    {
        Response.Redirect("SpellingGame_DisplayWord.aspx?category=" + radioBtnLstCategory.SelectedValue.ToString());
    }
}
