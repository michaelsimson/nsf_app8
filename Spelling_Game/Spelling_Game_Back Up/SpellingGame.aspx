<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"
    CodeFile="SpellingGame.aspx.cs" Inherits="Spelling_Demo_Demo_SpellingGame"
    Title="Spelling Bee Preparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
        <tr>
            <td>
                <a href="../MainTest.aspx" id="hrefQuitGame">Quit Game</a>
                <p>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <b>1. DO NOT</b> use your browser's BACK and FORWARD buttons to navigate. You may
                get inaccurate results.
                <br />
                <b>2.</b> Your results for this session will not be saved if you attempt <b>less than
                    10 words.</b>
                <br />
                <b>3.</b> To <b>End a Game</b> click <b>End Session</b> button, otherwise your results
                for this session will not be saved. This button will appear once you spell a word.
                <br />
                <p>
                </p>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
        <tr>
            <td width="10%" align="center" valign="middle">
                <img src="images/bee_icon.gif" />
            </td>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                    <tr>
                        <td>
                            <font class="black-2"><b>Please select a category to start your Spelling Bee preparation.</b></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButtonList ID="radioBtnLstCategory" runat="server" Height="205px" Width="192px"
                                Font-Bold="True">
                                <asp:ListItem Value="11" Selected="True">Beginner / Easy</asp:ListItem>
                                <asp:ListItem Value="12">Beginner / Difficult</asp:ListItem>
                                <asp:ListItem Value="21">Intermediate / Easy</asp:ListItem>
                                <asp:ListItem Value="22">Intermediate / Difficult</asp:ListItem>
                                <asp:ListItem Value="31">Advanced / Easy</asp:ListItem>
                                <asp:ListItem Value="32">Advanced / Difficult</asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnStartSpellingBee" runat="server" Text="Start Spelling Bee" OnClick="btnStartSpellingBee_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Click here for <font family=arial size=2pt><a onclick="javascript:window.open('GamePerformanceGraph.aspx', 'fullscreen','height=500,width=400,left=0,top=0,toolbar=no,menubar=no,scrollbars=yes');" href="#"> Cumulative Graphical View </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
