using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class spelling_Bee_GamePerformanceGraph : System.Web.UI.Page
{
    private static string UserID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserID"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
        else
        {
            UserID = Session["UserID"].ToString();
        }

        DataSet dsPerformace = new DataSet();
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        dsPerformace = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "vusp_GetPerformanceGraph", new SqlParameter("@UserID", UserID));

        if (dsPerformace.Tables[0].Rows.Count > 0)
        {
            gvPerformanceGraph.DataSource = dsPerformace;
            gvPerformanceGraph.DataBind();

            gvPerformanceGraph.Visible = true;
            lblNoRecords.Visible = false;
        }
        else
        {
            gvPerformanceGraph.Visible = false;
            lblNoRecords.Visible = true;
        }
    }

    protected void gvPerformanceGraph_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblPercentage = (Label)e.Row.FindControl("lblPercentage");
        if (lblPercentage != null)
        {
            lblPercentage.Width = Unit.Percentage(Double.Parse(lblPercentage.Text));
            lblPercentage.Text = lblPercentage.Text + " %";
        }
    }
}
