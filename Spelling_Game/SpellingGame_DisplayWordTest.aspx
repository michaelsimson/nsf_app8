<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true"
    CodeFile="SpellingGame_DisplayWordTest.aspx.cs" Inherits="Spelling_Demo_Demo_SpellingGame_DisplayWordTest"
    Title="Spelling Bee Preparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
 
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
        <tr>
            <td>
                <a href="../MainTest.aspx" id="hrefQuitGame">Quit Game</a> &nbsp;
                <a href="SpellingGame.aspx?id=1" id="A1">level Selection</a>
                <p>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <%--<asp:Label ID="lbl1" runat="server" Text = "1. DO NOT " Font-Bold="True" ></asp:Label> use your browser's BACK and FORWARD buttons to navigate. You may
                get inaccurate results.
                <br />
                <asp:Label ID="lbl2a" runat="server" Font-Bold="true" Text="2. "></asp:Label> Your results for this session will not be saved if you attempt <asp:Label ID="lbl2b" runat="server" Text = " less than 10 words." Font-Bold="True" ></asp:Label> <br />
                <asp:Label ID="lbl3a" runat="server" Font-Bold="true" Text="3. "></asp:Label> To <asp:Label ID="lbl3b" runat="server" Font-Bold="true" Text=" End a Game "></asp:Label> click <asp:Label ID="lbl3c" runat="server" Font-Bold="true" Text=" End Session "></asp:Label> button, otherwise your results
                for this session will not be saved. This button will appear once you spell a word.
                <br />--%>
                <div align="center">
                   Type the start with word(s)&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox></br>
                    <asp:Button ID="BtnSearch" runat="server" Text="Search" 
                        onclick="BtnSearch_Click" /><br />
                    <asp:Label ID="lblerr" runat="server" ></asp:Label>
                        </div>
                    </div>
                </p>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="100%">
        <tr>
            <td width="10%" align="center" valign="middle">
                <img src="images/bee_icon.gif" />               
            </td>
            <td>
                <asp:FormView ID="frmViewDemoWords" runat="server" OnItemCommand="frmViewDemoWords_ItemCommand" OnDataBound="frmViewDemoWords_DataBound"
                    AllowPaging="True" Width="80%">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                <EMBED SRC = "<%=Session["audio"] %>" LOOP = "false" AUTOSTART = "true" hidden="true" width="0" align="right" height="0">
                                   <asp:Label ID="lblspellword" Text="Spell the word: " runat="server" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" Visible="false"  ID="txtWord" />
                                     <asp:DropDownList ID="ddlAudioLevel" runat="server">
                                        <asp:ListItem>Low</asp:ListItem>
                                        <asp:ListItem>Good</asp:ListItem>
                                        <asp:ListItem>High</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label runat="server" ID="lblSONO" Text='<%# Bind("SONO") %>' Visible="false"></asp:Label>
                                    <asp:Label runat="server" ID="lblWord" Text='<%# Bind("Word") %>' Visible="true"></asp:Label>
                                    <asp:Label runat="server" ID="lblaudiolevel" Text='<%# Bind("audiolevel") %>' Visible="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align = "center" >
                                    <asp:Button ID="btnRepeatWord" runat="server" Text="Repeat Word" CommandName="RepeatWord" />
                                &nbsp;
                                    <asp:Button  ID="btnSubmitWord" runat="server" Text="Submit Word" CommandName="SubmitWord" />
                                    &nbsp;
                                     <asp:Button ID="btnNextWord" runat="server" Text="Next Word" OnClick="btnNextWord_Click" />
                                     <br />
                                    <asp:Label ID="lblresult" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <%--<tr>
                                <td colspan="2">
                                    <br />
                                    <br />
                                     <asp:Label ID="lblshowme" Text="Show me..." runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnDefinition" runat="server" Text="Definition" />
                                </td>
                                <td>
                                    <asp:Label ID="lblDefinition" runat="server" Text='<%# Bind("Definitions") %>' Style="display: none"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSentence" runat="server" Text="Sentence" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSentence" runat="server" Text='<%# Bind("Sentence") %>' Style="display: none"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnLanguageOrigin" runat="server" Text="Language Origin" />
                                </td>
                                <td>
                                    <asp:Label ID="lblLanguageOrigin" runat="server" Text='<%# Bind("[Language Origin]") %>'
                                        Style="display: none"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnPartofSpeech" runat="server" Text="Part of Speech" />
                                </td>
                                <td>
                                    <asp:Label ID="lblPartofSpeech" runat="server" Text='<%# Bind("[Parts of Speech]") %>'
                                        Style="display: none"></asp:Label>
                                </td>
                            </tr>--%>
                        </table>
                    </ItemTemplate>
                    <PagerSettings Visible="False" />
                </asp:FormView>
                <asp:Panel ID="panelWrongAnswer" runat="server" Visible="false" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="100%">
                        <tr>
                            <td>
                                <asp:Label ID="lblsorry" Text="Sorry!" runat="server" Font-Bold="true" Font-Italic="true"></asp:Label> You spelt 
                                    <asp:Label ID="lblWrongGivenAnswer" runat="server" Font-Bold="true"></asp:Label>.
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                The correct spelling is 
                                    <asp:Label ID="lblWrongCorrectWord" runat="server" Font-Bold="true"></asp:Label>.
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbldef" Text="Definition: " runat="server" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblWrongDefinition" runat="server"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblsen" Text="Sentence: " runat="server" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblWrongSentence" runat="server"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblpos" Text="Part of Speech: " runat="server" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblWrongPartofSpeech" runat="server"></asp:Label>
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You have attempted 
                                    <asp:Label ID="lblWrongAttemptCount" runat="server" Font-Bold="true"></asp:Label>
                                     <asp:Label ID="Label4" Text="word(s)" runat="server" Font-Bold="true"></asp:Label> in this session.
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="panelCorrectAnswer" runat="server" Visible="false" Width="100%">
                    You spelt 
                        <asp:Label ID="lblCorrectCorrectWord" runat="server" Font-Bold="true"></asp:Label> correctly.
                    Good for you.
                    <br />
                    You have attempted 
                        <asp:Label ID="lblCorrectAttemptCount" runat="server" Font-Bold="true"></asp:Label>
                        word(s) in this session.
                    <br />
                    <br />
                </asp:Panel>
                <asp:Panel ID="panelControls" runat="server" Visible="false" Width="100%">
                   
                    <asp:Button ID="btnEndSession" runat="server" Text="End Session" OnClick="btnEndSession_Click" />
                </asp:Panel>
                <asp:Panel ID="panelEndResult" runat="server" Visible="false" Width="100%">
                    You attempted
                    <asp:Label ID="lblEndResultAttempt" runat="server" Font-Bold="true"></asp:Label>
                     <asp:Label ID="Label1" Text=" word(s) " runat="server" Font-Bold="true"></asp:Label> out of which you answered<br />
                    <br />
                    <br />
                    <asp:Label ID="lblEndResultCorrect" runat="server" Font-Bold="true"></asp:Label>
                    <img src="images/correct.gif" />
                    <br />
                    <asp:Label ID="lblEndResultWrong" runat="server" Font-Bold="true"></asp:Label>
                    <img src="images/wrong.gif" />
                    <br />
                    <br />
                    <asp:Button ID="btnViewScore" runat="server" Text="View Score" />&nbsp;&nbsp;
                    <asp:Button ID="btnViewWordList" runat="server" Text="View Word List" OnClick="btnViewWordList_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnSelectAnotherWordCategory" runat="server" Text="Select Another Word Category"
                        OnClick="btnSelectAnotherWordCategory_Click" />
                </asp:Panel>
                <asp:Panel ID="panelWarningLessWords" runat="server" Visible="false" Width="100%">
                    <table border="2" width="100%" cellspacing="0">
                        <tr bgcolor="#80a080">
                            <td>
                               <asp:Label ID="warn" runat="server" Font-Bold="true" ForeColor="red" Text="Warning"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                You have spelt only 
                                    <asp:Label ID="lblWarningLessWordsAttempt" runat="server" Font-Bold="true"></asp:Label>
                                words. You need to spell atleast 10 words for your results from this session
                                to be saved.<br>
                                <br>
                                Click END GAME if you want to <asp:Label ID="lblend" Text="end the game" runat="server" Font-Bold="true"></asp:Label><br>
                                Click CANCEL if you want to continue playing the game
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnWarningEndGame" runat="server" Text="END GAME" OnClick="btnWarningEndGame_Click" />
                                <asp:Button ID="btnWarningCancel" runat="server" Text="CANCEL" OnClick="btnWarningCancel_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
