using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Spelling_Demo_Demo_SpellingGame : System.Web.UI.Page
{
    private int level;
    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    private int subLevel;
    public int SubLevel
    {
        get { return subLevel; }
        set { subLevel = value; }
    }
    private static int ChildNumber;
    private static int category;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["UserID"] = "user1";
        lblerr.Text = "";
        if (Session["childNumber"] == null && Request.QueryString["id"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
        else
            {
                ChildNumber = Convert.ToInt32(Session["childNumber"]);
            }
        Page.RegisterStartupScript("SetFocus", "<script>document.getElementById('" + btnStartSpellingBee.ClientID + "').focus();</script>");
    }

    protected void btnStartSpellingBee_Click(object sender, EventArgs e)
    {
        if (radioBtnLstCategory.SelectedValue.ToString() != null)
        {
            category = Convert.ToInt32(radioBtnLstCategory.SelectedValue.ToString());
            Level = category / 10;
            SubLevel = category % 10;
            string strCategory = string.Empty;
            if (category == 11)
            {
                strCategory = "Beginner / Easy";
            }
            else if (category == 12)
            {
                strCategory = "Beginner / Difficult";
            }
            else if (category == 21)
            {
                strCategory = "Intermediate / Easy";
            }
            else if (category == 22)
            {
                strCategory = "Intermediate / Difficult";
            }
            else if (category == 31)
            {
                strCategory = "Advanced / Easy";
            }
            else if (category == 32)
            {
                strCategory = "Advanced / Difficult";
            }
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

            string strSql = "select count(word) from word_master_new where level_nsf = " + Level + " and [Sub-level_nsf] = " + SubLevel;
            DataSet ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql);
            if (Convert.ToInt32(ds1.Tables[0].Rows[0][0]) == 0)
            {
                lblerr.Text = "There are no words in " + strCategory + " category, please select another category";

            }
            else if (Request.QueryString["id"] != null)
            {
                Response.Redirect("SpellingGame_DisplayWordTest.aspx?category=" + radioBtnLstCategory.SelectedValue.ToString());
            }
            else
            {
                String str = "select count(word) from word_master_new where Audio_flag ='y' and level_nsf = " + Level + " and [Sub-level_nsf] = " + SubLevel + " and definitions is not null and word not in (select word from user_easy_word  where ChildNumber = '" + ChildNumber + "' and word_category = '" + category + "')";
                DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, str);

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) <= 30)
                {
                    String str1 = "delete from user_easy_word where ChildNumber  = '" + ChildNumber + "' and word_category = " + category;
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str1);

                }
                if (ds.Tables[0].Rows.Count == 0)
                {
                    lblerr.Text = "Please select another category as you have attempted all of the words from the " + strCategory + " category.";
                }

                else
                {
                    Response.Redirect("SpellingGame_DisplayWord.aspx?category=" + radioBtnLstCategory.SelectedValue.ToString());
                }

            }
        }
    }
}
