﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContestSettings.aspx.vb" Inherits="ContestSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
     <script type="text/javascript">
         function ConfirmMsgToOverwrite() {
             if (confirm("Do you really want to override the existing records from the server?")) {
                 document.getElementById('<%= btnConfirmOverwrite.ClientID%>').click();
             }
         }
         function ConfirmMsgToReplicate(curYear,proYear) {
              
             if (confirm("Do you really want to replicate the data from "+ curYear +" to "+ proYear+"?")) {
                 document.getElementById('<%= btnConfirmReplicate.ClientID%>').click();
             }
         }
         </script>
    <asp:Button ID="btnConfirmOverwrite" style="display:none" runat="server"  />
    <asp:Button ID="btnConfirmReplicate" style="display:none" runat="server"  />
    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
        <tr>
            <td colspan="2">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>
        </tr> 
        <tr>

            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
               <center><h2>Contest Settings</h2></center> 
            </td>
        </tr>
       
        <tr>
            <td colspan="2">
                 <table width="75%"  style="margin-left:80px;margin-right:auto;font-weight:bold;background-color:#ffffcc;">
                        <tr class="ContentSubTitle">
                           
                            <td>Event</td>
                            <td>
                                <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True"  DataTextField="Name" DataValueField="EventId">
                                </asp:DropDownList>
                            </td> 
                            <td>Contest Year</td>
                            <td>
                                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>Proposed Year</td>
                            <td>
                                <asp:DropDownList ID="ddlProYear" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            
                            </tr>
                        <tr class="ContentSubTitle">
                           
                            <td>&nbsp;</td>
                            <td>
                                &nbsp;</td> 
                            <td>
                                <asp:Button ID="btnAddNew" runat="server" Text="Add New" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td colspan="2">
                                <asp:Button ID="btnReplicate" runat="server" Text="Replicate All" />
                            </td>
                            </tr></table>   <br />
                <div style="text-align:center">                 <asp:Label ID="lblMsg" runat="server"></asp:Label></div>
                            <asp:HiddenField ID="hfContSetID" runat="server" />
             
                <table width="90%" style="margin-left: 40px; margin-right: auto; font-weight: bold; background-color: #ffffcc;" id="tblAddUpd" runat="server" visible="false">
                    <tr class="ContentSubTitle" align="center">
                        <td style="text-align: right">Product Group</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" >
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Product</td>
                        <td  style="text-align: left">
                            <asp:DropDownList ID="ddlProduct" runat="server"  DataTextField="Product" DataValueField="ProductId" Width="110px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Phase</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlPhase" runat="server" Width="65px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Type</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlType" runat="server" Width="95px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">
                            Rounds</td>
                        <td>
                            <asp:DropDownList ID="ddlRounds" runat="server" Width="65px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            Duration</td>
                        <td>
                            <asp:DropDownList ID="ddlDuration" runat="server" Width="65px">
                            </asp:DropDownList>
                        &nbsp;min</td>
                        <td style="text-align: right">
                            Questions
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlQuestion" runat="server" AutoPostBack="True" Width="65px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            Multi-Choice</td>
                        <td>
                            <asp:DropDownList ID="ddlMultiChoice" runat="server" AutoPostBack="True" Width="65px" >
                                <asp:ListItem Value="'Y'">Y</asp:ListItem>
                                <asp:ListItem Value="NULL">N</asp:ListItem>
                                <asp:ListItem Value="'M'">M</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">
                            Projector
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlProjector" runat="server" AutoPostBack="True" Width="65px">
                                <asp:ListItem Value="'Y'">Y</asp:ListItem>
                                <asp:ListItem Value="NULL">N</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            Q time
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlQTime" runat="server" AutoPostBack="True" Width="65px">
                            </asp:DropDownList>
                        &nbsp;sec</td>
                        <td style="text-align: right">
                            Empty Seats
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmptySeats" runat="server" AutoPostBack="True" Width="65px">
                                <asp:ListItem>1</asp:ListItem>
                                 <asp:ListItem Value="Null">0</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            Empty Rows</td>
                        <td>
                            <asp:DropDownList ID="ddlEmptyRows" runat="server" AutoPostBack="True" Width="65px">
                                <asp:ListItem Value="Null">Select</asp:ListItem>
                                  <asp:ListItem>0</asp:ListItem>
                                <asp:ListItem>1</asp:ListItem>
                                 <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">
                            Parents
                        </td>
                        <td>
                         
                            <asp:DropDownList ID="ddlParents" runat="server" AutoPostBack="True" Width="65px">
                                <asp:ListItem Value="'Y'">Y</asp:ListItem>
                                <asp:ListItem Value="Null">N</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            &nbsp;Rec Cap
                        </td>
                        <td>
                         
                            <asp:DropDownList ID="ddlRecCap" runat="server" AutoPostBack="True" Width="65px">
                            </asp:DropDownList>                            
                        </td>
                        <td style="text-align: right">
                            Max Cap</td>
                        <td>
                         
                            <asp:DropDownList ID="ddlMaxCap" runat="server" AutoPostBack="True" Width="65px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            Room Cap</td>
                        <td>
                            <asp:DropDownList ID="ddlRoomCap" runat="server" AutoPostBack="True" Width="65px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="ContentSubTitle">
                        <td style="text-align: right">
                            Published</td>
                        <td>
                         
                            <asp:DropDownList ID="ddlPub" runat="server" AutoPostBack="True" Width="65px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">
                            &nbsp;</td>
                        <td>
                         
                            &nbsp;</td>
                        <td style="text-align: right">
                            &nbsp;</td>
                        <td>
                         
                            &nbsp;</td>
                        <td style="text-align: right">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align: center" colspan="6">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnAddUpdate" runat="server" Text="Add" Style="text-align: center" />
                                &nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                        </td>
                        <td colspan="2" runat="server" id="tdCopyTo" style="visibility:hidden">
                            <asp:Button ID="btnCopyTo" runat="server" Text="Copy To" />
                        &nbsp;&nbsp;
                            <asp:DropDownList ID="ddlCopyToPrd" ToolTip="Select Product" runat="server" Width="110px" Enabled="false"  DataTextField="Product" DataValueField="ProductId">
                            </asp:DropDownList><asp:HiddenField ID="hfRowIndex" runat="server" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" Enabled="false" />
                        </td>
                    </tr>
                    </table>
              <br />

                      <asp:GridView ID="grdContSet" runat="server" AutoGenerateColumns="False" EnableViewState="true" Width="100%" HeaderStyle-BackColor ="#ffffcc">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                                                <div style="display:none">
                                               <asp:Label ID="lblPgId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ProductGroupID")%>'></asp:Label>
                                                <asp:Label ID="lblPId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ProductID")%>'></asp:Label>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ProductID")%>'></asp:Label>
                                                    </div>
                                            </ItemTemplate>                                            
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ContSetID" Visible="True" HeaderText="ContSetID" HeaderStyle-Width="50px"></asp:BoundField> 
                                         <asp:BoundField DataField="ContestYear" Visible="true" HeaderText ="Contest Year"></asp:BoundField>
                                          <asp:BoundField DataField="EventName" Visible="true" HeaderText ="Event"></asp:BoundField>
                                         <asp:BoundField DataField="ProductGroup" Visible="true" HeaderText ="Product Group"></asp:BoundField>
                                         <asp:BoundField DataField="Product" Visible="true" HeaderText ="Product"></asp:BoundField>
                                         <asp:BoundField DataField="Phase" Visible="true" HeaderText ="Phase" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                         <asp:BoundField DataField="Type" Visible="true" HeaderText ="Type"></asp:BoundField>
                                         <asp:BoundField DataField="Rounds" Visible="true" HeaderText ="Rounds"></asp:BoundField>
                                         <asp:BoundField DataField="Duration" Visible="true" HeaderText ="Duration"></asp:BoundField>

                                          <asp:BoundField DataField="Questions" HeaderText ="Questions"></asp:BoundField>
                                        <asp:BoundField DataField="Pub" HeaderText ="Pub"></asp:BoundField>
                                         <asp:BoundField DataField="MultiChoice" HeaderText ="MultiChoice" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                         <asp:BoundField DataField="Projector"  HeaderText ="Projector"  ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                         <asp:BoundField DataField="QTime"  HeaderText ="QTime" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                       
                                         
                                        <asp:BoundField DataField="EmptySeats"  HeaderText ="EmptySeats"></asp:BoundField>
                                         <asp:BoundField DataField="EmptyRows"  HeaderText ="EmptyRows"></asp:BoundField> 
 <asp:BoundField DataField="Parents" HeaderText ="Parents" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                         <asp:BoundField DataField="RecCap"  HeaderText ="RecCap"></asp:BoundField>
                                         <asp:BoundField DataField="MaxCap" HeaderText ="MaxCap"></asp:BoundField>
                                        
                                         <asp:BoundField DataField="RoomCap"  HeaderText ="RoomCap"></asp:BoundField> 

                                    </Columns>
                                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

