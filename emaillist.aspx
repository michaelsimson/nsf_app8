<%@ Page Language="VB" Debug="true" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="emaillist.aspx.vb" ValidateRequest="false" Inherits="emaillist" Title="Untitled Page" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%--<%@ Register TagPrefix="RTE" Namespace="RTE" Assembly="RichTextEditor" %> --%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/jquery.toast.js"></script>
    <link href="css/jquery.toast.css" rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function hide_fromdropdown(var1) {
            document.getElementById(var1).style.display = "none";;
        }
        function hide_fromtext(var1) {
            document.getElementById(var1).style.display = "none";
        }
        function status_change() {
            document.getElementById('<%= lblstatus.ClientID %>').innerHTML = '&nbsp;&nbsp;&nbsp;Processing...';
        }
        function statusMessage() {
            $(".close-jq-toast-single").css("display", "none");
            $.toast({
                // heading: 'Can I add <em>icons</em>?',
                text: '<b>Email has been sent successfully</b>',
                icon: "Success",
                position: 'top-center',
                hideAfter: false,
                stack: 1

            })
        }

        $(function (e) {
            $(".close-jq-toast-single").hide();
            $(".close-jq-toast-single").css("display", "none");
        });
    </script>
   <script type="text/javascript" src="Scripts/jquery-1.4.1.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('<%=txtEmailBody.ClientID%>',
          {
              filebrowserImageUploadUrl: 'Upload.ashx', filebrowserImageBrowseUrl: 'Upload.ashx'
          }); //path to "Upload.ashx"
    });
</script><%-- --%>
    <table style="width: 680px">
        <tr>
            <td style="width: 300px; height: 18px;">
                <asp:HyperLink ID="hybbackvol" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="254px"></asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="Hypbackemail" runat="server" Text="Back to Email" Width="284px"></asp:HyperLink>
            </td>
        </tr>
    </table>
    <table id="tableemail" cellspacing="1" cellpadding="3" width="700" style="margin-left: 10px" border="0" runat="server">
        <tr>
            <td>
                <asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td nowrap>
                <asp:Label ID="lblfrom" runat="server" Text="From:" Width="50px"></asp:Label>
                <asp:TextBox ID="txtfrom" runat="server" BackColor="#FFE0C0"></asp:TextBox>
                <asp:DropDownList ID="drpfrom" runat="server">
                    <asp:ListItem Text="Select From Email" Value="0" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="support@northsouth.org" Value="1"></asp:ListItem>
                    <asp:ListItem Text="nsfmathcounts@northsouth.org" Value="2"></asp:ListItem>
                    <asp:ListItem Value="3">coaching@northsouth.org</asp:ListItem>
                    <asp:ListItem Value="4">workshops@northsouth.org</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="lblfromemailerror" runat="server" ForeColor="red"></asp:Label>
                <%--<asp:RequiredFieldValidator id="reqFrommEmaiID" Text="Select From EmailID" InitialValue="0" ControlToValidate="drpfrom" Runat="server" />--%>
                <%--<asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server" ControlToValidate="txtfrom" 
      ErrorMessage="You must enter an Correct email address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
</asp:RegularExpressionValidator>--%>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <asp:Label ID="lblEmailCount" runat="server" Font-Bold="True" Text="# of Emails to be sent :" ForeColor="#0066CC"></asp:Label>
                &nbsp;<asp:Label ID="lblCnt" runat="server" Font-Bold="true" ForeColor="#0066CC" Width="250px"></asp:Label>
                &nbsp;<asp:Label ID="lblmailSuccessCount" runat="server" ForeColor="green" Font-Size="Large" Font-Bold="true" Width="250px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>CC:&nbsp&nbsp;&nbsp&nbsp;<asp:TextBox ID="ccemaillist" runat="server" TextMode="MultiLine" BackColor="#FFE0C0" Width="382px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblemailsub" runat="server" Text="Please Type the Subject of the Email Below"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 14px">
                <asp:TextBox ID="txtEmailSubject" runat="server" Width="800px" MaxLength="1000" BackColor="#FFE0C0"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr style="height: 25px;">

            <td>
                <asp:Label ID="lblattfile" Text="File to be Attached:" runat="server"></asp:Label>
                <asp:FileUpload ID="AttachmentFile" runat="server" /><span style="align-items: center;"><asp:Label ID="lblFileUploadError" runat="server" Text="" Visible="false"></asp:Label></span><span style="float: right;"><asp:Button ID="insertfilecontentintoeditor" runat="server" Text="Insert File to Editor" /></span></td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="lblemailbody" Text="Please Type the Body of the Email Below" runat="server"></asp:Label>
                <CKEditor:CKEditorControl ID="txtEmailBody" runat="server" Width="800px"></CKEditor:CKEditorControl>
                <%--<RTE:Editor ID="txtEmailBody" ContentCss="example.css" Text="" runat="server" Width="800px" /> --%>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnsendemail" runat="server"
                    Text="Send Email"
                    OnClientClick="status_change() " />
                <asp:Label ID="lblstatus" runat="server" Font-Bold="False" Font-Size="Large"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblEMailError" runat="server"></asp:Label></td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Label ID="lblsentemailnote" runat="server" Text="**NOTE: When giving your own list make sure than they should not be any gap/space after comma (,) or  between emails eg: xxx@xx.com,aaa@aa.com " Visible="false"></asp:Label></td>
        </tr>
        <tr>
            <td style="height: 5px">&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtSendList" runat="server" Enabled="false" Width="800px" Height="58px" TextMode="MultiLine" BorderColor="Black">
            </asp:TextBox></td>
        </tr>
    </table>
</asp:Content>

