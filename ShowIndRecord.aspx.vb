Imports Microsoft.ApplicationBlocks.Data

Partial Class ShowIndRecord
    Inherits System.Web.UI.Page

    Dim strINDSpouseIds As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("LoggedIn") Is Nothing Then
                Response.Redirect("Maintest.aspx")
            End If
            LoadGrid()
        End If
    End Sub
    Private Sub LoadGrid()

        Dim strSql As String

        If Request.QueryString("Last") = "T" And Request.QueryString("First") = "T" Then
            strSql = "SELECT a.*,'' as user_pwd FROM IndSpouse a,IndSpouse b WHERE (a.AutoMemberID = b.AutoMemberID OR a.RelationShip = b.AutoMemberID OR a.AutoMemberID=b.RelationShip) AND b.Email='" & Request.QueryString("Email") & "' and b.Email<> ''"
        Else
            strSql = "Select a.*,b.user_pwd from IndSpouse a  LEFT OUTER JOIN login_master AS b ON a.Email = b.user_email"
            strSql = strSql & " WHERE (a.lastname = '" & Request.QueryString("Last") & "' and a.firstname = '" & Request.QueryString("First") & "') or (a.Email='" & Request.QueryString("Email") & "' and a.Email<> '')"
            strSql = strSql & " or relationship in "
            strSql = strSql & " (select b.automemberid from IndSpouse b where (b.lastname = '" & Request.QueryString("Last") & "' and "
            strSql = strSql & " b.firstname = '" & Request.QueryString("First") & "') or (a.Email='" & Request.QueryString("Email") & "' and a.Email<> ''))"
        End If
        Dim dsIndSpouse As New DataSet
        dsIndSpouse = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        dgIndSpouse.DataSource = dsIndSpouse
        dgIndSpouse.DataBind()


        Try
            If Request.QueryString("Last") = "T" And Request.QueryString("First") = "T" Then
                trT.Visible = True
                LoadDT()
            Else
                ' Display suspected indspouse details
                trDupH.Visible = True
                trDupDetails.Visible = True
                strSql = "Select a.*,b.user_pwd from IndDuplicate a LEFT OUTER JOIN login_master AS b ON a.Email = b.user_email"
                strSql = strSql & " WHERE (a.lastname = '" & Request.QueryString("Last") & "' and a.firstname = '" & Request.QueryString("First") & "') or (a.Email='" & Request.QueryString("Email") & "' and a.Email<> '')"
                strSql = strSql & " or relationship in "
                '  strSql = strSql & " (select b.automemberid from IndDuplicate b where (b.lastname = '" & Request.QueryString("Last") & "' and "
                strSql = strSql & " (select b.IndDupId from IndDuplicate b where (b.lastname = '" & Request.QueryString("Last") & "' and "
                strSql = strSql & " b.firstname = '" & Request.QueryString("First") & "') or (a.Email='" & Request.QueryString("Email") & "' and a.Email<> ''))"
                Dim dsDupIndSpouse As New DataSet
                dsDupIndSpouse = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                dgDupIndSpouse.DataSource = dsDupIndSpouse
                dgDupIndSpouse.DataBind()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadDT()
        Dim strText As String = ""
        If Request.QueryString("Last") = "T" And Request.QueryString("First") = "T" Then
            strText = "SELECT isnull(Newsletter,'') Newsletter, Email, isnull(convert(varchar(10),CreateDate,101) ,'-') CreateDate FROM IndSpouse WHERE Email='" & Request.QueryString("Email") & "' and Email<> ''"
            'Else
            '     strText = " select isnull(Newsletter,'') Newsletter, Email, CreateDate from IndSpouse where ((lastname = '" & Request.QueryString("Last") & "' and "
            '    strText = strText & " firstname = '" & Request.QueryString("First") & "') or (Email='" & Request.QueryString("Email") & "' and Email<> ''))"
        End If
        Dim DS As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strText)
        If DS.Tables(0).Rows.Count > 0 Then
            lblNewsletter.Text = DS.Tables(0).Rows(0)("Newsletter")
            lblEmail.Text = DS.Tables(0).Rows(0)("Email")
            lblCreatedDate.Text = DS.Tables(0).Rows(0)("CreateDate")
        End If
        If strINDSpouseIds.Length > 0 Then
            strText = " Select (select isnull( convert(varchar(10),max(CreateDate),101)  ,'-') from contestant where parentID=" & strINDSpouseIds & ") ConDT,"
            strText = strText & " (select isnull( convert(varchar(10),max(CreateDate),101) ,'-') from CoachReg where PMemberID=" & strINDSpouseIds & ") CoachDT, "
            strText = strText & "(select isnull( convert(varchar(10),max(CreateDate),101) ,'-') from Registration where MemberID=" & strINDSpouseIds & " and EventId=3) OnsiteDT, "
            strText = strText & "(select isnull( convert(varchar(10),max(CreateDate),101) ,'-') from Registration_OnlineWkshop where MemberID=" & strINDSpouseIds & ") OnlineDT, "
            strText = strText & "(select isnull( convert(varchar(10),max(CreateDate),101) ,'-') from Game where MemberID=" & strINDSpouseIds & ") GameDT, "
            strText = strText & "(select isnull( convert(varchar(10),max(CreateDate),101) ,'-') from DonationsInfo where MemberID=" & strINDSpouseIds & ") DonationDT"
            Dim dsDT As New DataSet
            dsDT = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strText)
            If dsDT.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = dsDT.Tables(0).Rows(0)
                lblConDT.Text = dr("ConDT")
                lblCoachDT.Text = dr("CoachDT")
                lblOnSiteDT.Text = dr("OnsiteDT")
                lblOnlineDT.Text = dr("OnlineDT")
                lblGameDT.Text = dr("GameDT")
                lblDonationDT.Text = dr("DonationDT")
            End If
        End If
    End Sub
    Protected Sub dgIndSpouse_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgIndSpouse.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            Dim DonorType As String = CType(e.Item.FindControl("lblDonorType"), Label).Text
            If DonorType.Trim.ToLower = "ind" Then
                Dim iAutomemberid As Integer = Convert.ToInt32(CType(e.Item.FindControl("lblAutoMemberID"), Label).Text)
                If (strINDSpouseIds.Trim().Length = 0) And iAutomemberid <> 0 Then
                    strINDSpouseIds = iAutomemberid.ToString()
                End If
            End If
        End If
    End Sub

    Protected Sub btnUnsubscribe_Click(sender As Object, e As EventArgs) Handles btnUnsubscribe.Click
        Dim strText As String = ""
        If Request.QueryString("Last") = "T" And Request.QueryString("First") = "T" Then
            strText = "SELECT Automemberid FROM IndSpouse WHERE Email='" & Request.QueryString("Email") & "' and Email<> ''"
        Else
            strText = " select automemberid from IndSpouse where ((lastname = '" & Request.QueryString("Last") & "' and "
            strText = strText & " firstname = '" & Request.QueryString("First") & "') or (Email='" & Request.QueryString("Email") & "' and Email<> ''))"
        End If

        Dim iAutoMemberId As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strText)

        If iAutoMemberId > 0 Then
            strText = "update Indspouse set Newsletter=3 where Automemberid=" & iAutoMemberId
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strText) > 0 Then
                lblAlert.ForeColor = Color.Green
                lblAlert.Text = "The complainant was unsubscribed successfully"
                LoadDT()
            End If
        End If

    End Sub
End Class
