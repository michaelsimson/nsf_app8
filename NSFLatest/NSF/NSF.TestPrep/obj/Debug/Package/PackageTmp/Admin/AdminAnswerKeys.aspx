﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AdminAnswerKeys.aspx.cs" Inherits="NSF.TestPrep.Admin.AdminAnswerKeys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td width="8">
                <img height="8" src="../images/spacer.gif" width="8" alt="spacer" />
            </td>
            <td valign="top">
                <table cellpadding="10" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="2">
                            <div style="float: left">
                                <h1>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Answer Keys"></asp:Literal></h1>
                            </div>
                            <div style="float: right">
                                <asp:Button ID="btnCreate" runat="server" Text="Create New Test" OnClick="btnCreate_Click"
                                    CausesValidation="false"></asp:Button>
                                <asp:Label ID="lblSearch" runat="server" Text="Search" AssociatedControlID="txtSearch"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                <asp:ImageButton ID="imgBtnFilter" runat="server" ImageUrl="~/images/filter.png"
                                    ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnFilter_Click" CausesValidation="false"
                                    AlternateText="Filter Grid" ToolTip="Filter Grid" />
                                <asp:ImageButton ID="imgBtnExportToExcel" runat="server" ImageUrl="~/images/excel.png"
                                    ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnExportToExcel_Click"
                                    AlternateText="Export to Excel" ToolTip="Export To Excel" CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <asp:DropDownList ID="ddlEventYear" runat="server" DataSourceID="SqlDataSource1"
                                AppendDataBoundItems="true" DataTextField="EventYear_TestNumber" DataValueField="SATTestSetupRecID"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged">
                                <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NSF_SATConnectionString %>"
                                SelectCommand="SELECT [SATTestSetupRecID], CONVERT(VARCHAR(4), [EventYear]) +  ' - ' +  CONVERT(VARCHAR(50), [TestNumber]) AS [EventYear_TestNumber] FROM [SATTestSetup]">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <asp:GridView ID="gvGridData" runat="server" Width="100%" AutoGenerateColumns="False"
                                EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="AnswerKeyRecID,TestSectionID,QuestionNumber,CorrectAnswer,DifficultyLevel"
                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                OnRowCreated="gvGridData_RowCreated" OnRowDataBound="gvGridData_RowDataBound"
                                OnSorting="gvGridData_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="AnswerKeyRecID" HeaderText="ID" InsertVisible="False"
                                        Visible="false" ReadOnly="True" SortExpression="SATTestSetupRecID" />
                                    <asp:BoundField DataField="TestSectionID" HeaderText="Section" SortExpression="TestSectionID" />
                                    <asp:BoundField DataField="QuestionNumber" HeaderText="Question Number" SortExpression="QuestionNumber" />
                                    <asp:TemplateField HeaderText="Answer Choices" SortExpression="AnswerChoices" Visible="False">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gvtxtAnswerChoices" runat="server" Text='<%#                                        Bind("AnswerChoices") %>'
                                                TextMode="MultiLine"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Correct Answer" SortExpression="CorrectAnswer">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" Text='<%#Bind("CorrectAnswer") %>'
                                                TextMode="MultiLine"></asp:TextBox>
                                            <ajaxToolkit:HtmlEditorExtender ID="heeGvtxtCorrectAnswer" runat="server" TargetControlID="gvtxtCorrectAnswer">
                                            </ajaxToolkit:HtmlEditorExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Difficulty Level" SortExpression="DifficultyLevel">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gvtxtDifficultyLevel" runat="server" Text='<%#Bind("DifficultyLevel") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By" SortExpression="ModifiedBy"
                                        Visible="false" />
                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" SortExpression="ModifyDate"
                                        Visible="false" HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:ButtonField ButtonType="Image" CommandName="modify" ImageUrl="~/images/Modify.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="deactivate" ImageUrl="~/images/Delete.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CausesValidation="false"
                                OnClick="btnSubmit_Click"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="11">
                <img height="11" src="../images/spacer.gif" width="11">
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Panel ID="pnlRequestDetails" runat="server">
        <table class="modalPanel">
            <tr>
                <td>
                    <h2>
                        <asp:Label ID="Message" runat="server" Text=""></asp:Label></h2>
                    <asp:Label ID="EntryLogID" runat="server" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" width="100%" style="background-color: #EFF3FB;
                        border-color: #B5C7DE">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSectionNumber" runat="server" Text="*Section Number: " AssociatedControlID="ddlSectionNumber"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlSectionNumber" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblQuestionNumber" runat="server" Text="*Test Number: " AssociatedControlID="txtQuestionNumber"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtQuestionNumber" runat="server" MaxLength="3" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtQuestionNumberE" runat="server" Minimum="1"
                                    Width="50" Maximum="999" TargetControlID="txtQuestionNumber">
                                </ajaxToolkit:NumericUpDownExtender>
                                <asp:RequiredFieldValidator ID="rfvtxtQuestionNumber" runat="server" ControlToValidate="txtQuestionNumber"
                                    ErrorMessage="Question Number is required" Text="Question Number is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtQuestionNumberE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtQuestionNumber" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblCorrectAnswer" runat="server" Text="*Correct Answer: " AssociatedControlID="txtCorrectAnswer"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCorrectAnswer" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtCorrectAnswer" runat="server" ControlToValidate="txtCorrectAnswer"
                                    ErrorMessage="Correct Answer is required" Text="Correct Answer is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtCorrectAnswerE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtCorrectAnswer" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblDifficultyLevel" runat="server" Text="*Difficulty Level: " AssociatedControlID="txtDifficultyLevel"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDifficultyLevel" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtDifficultyLevel" runat="server" ControlToValidate="txtDifficultyLevel"
                                    ErrorMessage="Difficulty Level is required" Text="Difficulty Level is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtDifficultyLevelE" runat="server"
                                    Enabled="True" TargetControlID="rfvtxtDifficultyLevel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="Add" runat="server" Text="Add" CausesValidation="True" OnClick="Add_Click">
                    </asp:Button><img height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Modify" runat="server" Text="Modify" CausesValidation="True" OnClick="Modify_Click">
                    </asp:Button><img height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="ModifyNew" runat="server" Text="Modify and Add New" CausesValidation="True"
                        OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8"
                            alt="spacer" />
                    <asp:Button ID="Deactivate" runat="server" Text="Deactivate" Visible="false" CausesValidation="False"
                        OnClick="Deactivate_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                            width="8">
                    <asp:Button ID="Activate" runat="server" Text="Activate" Visible="false" CausesValidation="False"
                        OnClick="Activate_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                            width="8">
                    <asp:Button ID="Cancel" runat="server" Text="Cancel" CausesValidation="False"></asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="pnlRequestDetailsE" runat="server" TargetControlID="hiddenTargetControlForRequestDetails"
        PopupControlID="pnlRequestDetails" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
</asp:Content>
