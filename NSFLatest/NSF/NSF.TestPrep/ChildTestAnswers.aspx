﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ChildTestAnswers.aspx.cs" Inherits="NSF.TestPrep.ChildTestAnswers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- COUNTDOWN TIMER -->
    <!-- This goes in the HEAD of the html file -->
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        var pnl1;
        var pnl2;
        var pnl3;
        var pnl4;
        var pnl5;
        var time;
        var ST;
        var TotalSeconds;
        var SD;


        $(document).ready(function () {

            pnl1 = $("#MainContent_Timer1").text() * 60 * 1000;
            pnl2 = $("#MainContent_Timer2").text() * 60 * 1000;
            pnl3 = $("#MainContent_Timer3").text() * 60 * 1000;
            pnl4 = $("#MainContent_Timer4").text() * 60 * 1000;
            pnl5 = $("#MainContent_Timer5").text() * 60 * 1000;
            time = ((pnl1 + pnl2 + pnl3 + pnl4 + pnl5) / 1000) / 60;
            $("#MainContent_Timer1").text((pnl1 / 1000) / 60 + '-Minutes |');
            $("#MainContent_Timer2").text((pnl2 / 1000) / 60 + '-Minutes |');
            $("#MainContent_Timer3").text((pnl3 / 1000) / 60 + '-Minutes |');
            $("#MainContent_Timer4").text((pnl4 / 1000) / 60 + '-Minutes |');
            $("#MainContent_Timer5").text((pnl5 / 1000) / 60 + '-Minutes');
            $(".buttonClass").attr("disabled", "disabled");

            $("#btnstart").click(function () {
                $(".buttonClass").removeAttr("disabled");
                $("#btnstart").attr("disabled", "disabled"); +
                countDown(time, 0);

            });

            $("#AllSections :input").attr("disabled", true);
            $("#MainContent_btnSubmit").click(function () {
                window.clearTimeout(SD);
                document.getElementById('theTime').innerHTML = '';
                $(".buttonClass").attr("disabled", "disabled");
                $("#AllSections :input").attr("disabled", false);

            }); $("#btnsection1").click(function () {
                clearTimer();
                $("#AllSections :input").attr("disabled", true);
                $(this).attr("disabled", "disabled");
                $("#MainContent_pnl1 :input").attr("disabled", false);
                createTimer(pnl1 / 1000);
                setTimeout(Disable_pnl, pnl1);
            }); $("#btnsection2").click(function () {
                clearTimer();
                $("#AllSections :input").attr("disabled", true);
                $(this).attr("disabled", "disabled");
                $("#MainContent_pnl2 :input").attr("disabled", false);
                createTimer(pnl2 / 1000);
                setTimeout(Disable_pnl, pnl2);
            }); $("#btnsection3").click(function () {
                clearTimer();
                $("#AllSections :input").attr("disabled", true);
                $(this).attr("disabled", "disabled");
                $("#MainContent_pnl3 :input").attr("disabled", false);
                createTimer(pnl3 / 1000);
                setTimeout(Disable_pnl, pnl3);
            }); $("#btnsection4").click(function () {
                clearTimer();
                $("#AllSections :input").attr("disabled", true);
                $(this).attr("disabled", "disabled");
                $("#MainContent_pnl4 :input").attr("disabled", false);
                createTimer(pnl4 / 1000);
                setTimeout(Disable_pnl, pnl4);
            }); $("#btnsection5").click(function () {
                clearTimer();
                $("#AllSections :input").attr("disabled", true);
                $(this).attr("disabled", "disabled");
                $("#MainContent_pnl5 :input").attr("disabled", false);
                createTimer(pnl5 / 1000);
                setTimeout(Disable_pnl, pnl5);
            });


        });

        function Disable_pnl() {
            $("#AllSections :input").attr("disabled", true);
        }

        function Disable_Exam() {
            $("#AllSections :input").attr("disabled", true);
        }


        function countDown(min, sec) {
            if (min > 0) {
                if (min == '00' && sec == '00') {
                    sec = "00";

                    document.getElementById('theTime').innerHTML = "00 Min:00 sec";
                    window.clearTimeout(SD);
                }
                sec--;
                if (sec == -01) {
                    sec = 59;
                    min = min - 1;
                }
                else {
                    min = min;
                }

                if (sec <= 9) {
                    sec = "0" + sec;
                }

                time = (min <= 9 ? "0" + min : min) + " min and " + sec + " sec ";

                if (document.getElementById) {
                    document.getElementById('theTime').innerHTML = time;
                }

                if (min > 0 || sec > 0) {
                    SD = window.setTimeout("countDown(" + min + "," + sec + ");", 1000);
                }
                else {
                    $("#AllSections :input").attr("disabled", true);
                    $(".buttonClass").attr("disabled", "disabled");
                    $("#MainContent_btnSubmit").removeAttr("disabled");
                }
            }

        }
        function createTimer(seconds) {
            TotalSeconds = seconds
            ST = setInterval("Tick()", 1000);
        }


        function Tick() {
            TotalSeconds -= 1;
            UpdateTimer();
        }

        function UpdateTimer() {

            var Seconds = TotalSeconds;
            var Minutes = Math.floor(TotalSeconds / 60);
            Seconds = Seconds - (Minutes * 60)
            document.getElementById('sectiontimer').innerHTML = formatDate(Minutes, Seconds);
        }


        function formatDate(minutes, seconds) {
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            else {
                minutes = minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            else {
                seconds = seconds;
            }
            return minutes + ":" + seconds;
        }


        function clearTimer() {
            clearInterval(ST);
            document.getElementById('sectiontimer').innerHTML = '';
        }

    </script>
    <style type="text/css">
        .timeClass
        {
            background-color: #46A71C;
            color: White;
            font-family: arial, verdana, helvetica, sans-serif;
            font-size: 200%;
            font-weight: bold;
            padding: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="top">
                <table class="admin-tan-border" cellpadding="10" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="2">
                            <div style="float: left">
                                <h1>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Take A Test"></asp:Literal></h1>
                            </div>
                             
                            <div style="float: right;">
                                <table>
                                    <tr>
                                        <td width="100%" align="center">
                                            <span id="theTime" style="font-size: xx-large"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlEventYear" runat="server" DataSourceID="SqlDataSource1"
                                            AppendDataBoundItems="true" DataTextField="EventYear_TestNumber" DataValueField="SATTestSetupRecID"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged"
                                            onchange="countDown();" Width="80px">
                                            <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$
            ConnectionStrings:NSF_SATConnectionString %>" SelectCommand="SELECT [SATTestSetupRecID],
            CONVERT(VARCHAR(4), [EventYear]) + ' - ' + CONVERT(VARCHAR(50), [TestNumber]) AS
            [EventYear_TestNumber] FROM [SATTestSetup]"></asp:SqlDataSource>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlmemberID" runat="server" DataTextField="StudentDetails"
                                            DataValueField="MemberID" AppendDataBoundItems="true">
                                            <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:SqlDataSource ID="StudentIDSource" runat="server"></asp:SqlDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <h3 style="font-family: Sans-Serif">
                                Section Timer:<span id="sectiontimer"></span></h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        Section1:
                                    </td>
                                    <td>
                                        <asp:Label ID="Timer1" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        Section2:
                                    </td>
                                    <td>
                                        <asp:Label ID="Timer2" runat="Server"></asp:Label>
                                    </td>
                                    <td>
                                        Section3:
                                    </td>
                                    <td>
                                        <asp:Label ID="Timer3" runat="Server"></asp:Label>
                                    </td>
                                    <td>
                                        Section4:
                                    </td>
                                    <td>
                                        <asp:Label ID="Timer4" runat="Server"></asp:Label>
                                    </td>
                                    <td>
                                        Section5:
                                    </td>
                                    <td>
                                        <asp:Label ID="Timer5" runat="Server"></asp:Label>
                                    </td>
                                    <td>
                                        &nbsp
                                    </td>
                                    <td>
                                        &nbsp
                                    </td>
                                    <td colspan="4" align="center">
                                      <img src="images/spacer.gif" />
                                        <input type="button" id="btnstart" value="Start Exam" />
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="buttonClass" Text="Submit" CausesValidation="false"
                                            OnClick="btnSubmit_Click"></asp:Button>
                                      
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="button" id="btnsection1" value="Section1" class="buttonClass" />
                            <input type="button" id="btnsection2" value="Section2" class="buttonClass" />
                            <input type="button" id="btnsection3" value="Section3" class="buttonClass" />
                            <input type="button" id="btnsection4" value="Section4" class="buttonClass" />
                            <input type="button" id="btnsection5" value="Section5" class="buttonClass" />
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <table id="AllSections">
                                <tr>
                                    <td style="vertical-align: top">
                                        <asp:Panel runat="server" ID="pnl1">
                                            <asp:GridView ID="gvGridData" runat="server" Width="100%" AutoGenerateColumns="False"
                                                EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestID,TestSectionID,QuestionType"
                                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                                OnRowCreated="gvGridData_RowCreated" OnRowDataBound="gvGridData_RowDataBound"
                                                OnSorting="gvGridData_Sorting">
                                                <Columns>
                                                    <%--<asp:BoundField DataField="AnswerKeyRecID" HeaderText="ID" InsertVisible="False"
                                                        Visible="false" ReadOnly="True" SortExpression="SATTestSetupRecID" />--%>
                                                   <%-- <asp:BoundField DataField="QuestionType" HeaderText="QuestionType" SortExpression="QuestionType"
                                                        Visible="False" />--%>
                                                   <%-- <asp:BoundField DataField="TestSectionID" HeaderText="Section" SortExpression="TestSectionID"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="QuestionNumber" HeaderText="Q.No." SortExpression="QuestionNumber" />--%>
                                                    <asp:TemplateField HeaderText="Section 1">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="C" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <%--<SortedAscendingCellStyle
            BackColor="#F1F1F1" /> <SortedAscendingHeaderStyle BackColor="#46A71C" /> <SortedDescendingCellStyle
            BackColor="#CAC9C9" /> <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:Panel ID="pnl2" runat="server">
                                            <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False"
                                                EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestID,TestSectionID,QuestionType"
                                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                                OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound" OnSorting="gvGridData_Sorting">
                                                <Columns>
                                                    <%--<asp:BoundField DataField="AnswerKeyRecID" HeaderText="ID" InsertVisible="False"
                                                        Visible="false" ReadOnly="True" SortExpression="SATTestSetupRecID" />
                                                    <asp:BoundField DataField="QuestionType" HeaderText="QuestionType" SortExpression="QuestionType"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TestSectionID" HeaderText="Section" SortExpression="TestSectionID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="QuestionNumber" HeaderText="Question
            Number" SortExpression="QuestionNumber" Visible="False" />--%>
                                                    <asp:TemplateField HeaderText="Section 2">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="C" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <%--<SortedAscendingCellStyle
            BackColor="#F1F1F1" /> <SortedAscendingHeaderStyle BackColor="#46A71C" /> <SortedDescendingCellStyle
            BackColor="#CAC9C9" /> <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:Panel runat="server" ID="pnl3">
                                            <asp:GridView ID="GridView2" runat="server" Width="100%" AutoGenerateColumns="False"
                                                EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestID,TestSectionID,QuestionType"
                                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                                OnRowCreated="GridView2_RowCreated" OnRowDataBound="GridView2_RowDataBound" OnSorting="gvGridData_Sorting">
                                                <Columns>
                                                    <%--<asp:BoundField DataField="AnswerKeyRecID" HeaderText="ID" InsertVisible="False"
                                                        Visible="false" ReadOnly="True" SortExpression="SATTestSetupRecID" />
                                                    <asp:BoundField DataField="QuestionType" HeaderText="QuestionType" SortExpression="QuestionType"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TestSectionID" HeaderText="Section" SortExpression="TestSectionID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="QuestionNumber" HeaderText="Question Number" SortExpression="QuestionNumber"
                                                        Visible="False" />--%>
                                                    <asp:TemplateField HeaderText="Section 3">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="C" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:Panel runat="server" ID="pnl4">
                                            <asp:GridView ID="GridView3" runat="server" Width="100%" AutoGenerateColumns="False"
                                                EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestID,TestSectionID,QuestionType"
                                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                                OnRowCreated="GridView3_RowCreated" OnRowDataBound="GridView3_RowDataBound" OnSorting="gvGridData_Sorting">
                                                <Columns>
                                                    <%--<asp:BoundField DataField="AnswerKeyRecID" HeaderText="ID" InsertVisible="False"
                                                        Visible="false" ReadOnly="True" SortExpression="SATTestSetupRecID" />
                                                    <asp:BoundField DataField="QuestionType" HeaderText="QuestionType" SortExpression="QuestionType"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TestSectionID" HeaderText="Section" SortExpression="TestSectionID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="QuestionNumber" HeaderText="Question Number" SortExpression="QuestionNumber"
                                                        Visible="False" />--%>
                                                    <asp:TemplateField HeaderText="Section 4">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="C" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                    <td style="vertical-align: top">
                                        <asp:Panel ID="pnl5" runat="server">
                                            <asp:GridView ID="GridView4" runat="server" Width="100%" AutoGenerateColumns="False"
                                                EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestID,TestSectionID,QuestionType"
                                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                                OnRowCreated="GridView4_RowCreated" OnRowDataBound="GridView4_RowDataBound" OnSorting="gvGridData_Sorting">
                                                <Columns>
                                                  <%--  <asp:BoundField DataField="AnswerKeyRecID" HeaderText="ID" InsertVisible="False"
                                                        Visible="false" ReadOnly="True" SortExpression="SATTestSetupRecID" />
                                                    <asp:BoundField DataField="QuestionType" HeaderText="QuestionType" SortExpression="QuestionType"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="TestSectionID" HeaderText="Section" SortExpression="TestSectionID"
                                                        Visible="False" />
                                                    <asp:BoundField DataField="QuestionNumber" HeaderText="Question Number" SortExpression="QuestionNumber"
                                                        Visible="False" />--%>
                                                    <asp:TemplateField HeaderText="Section 5">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="C" Value="B"></asp:ListItem>
                                                                <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Panel ID="pnlRequestDetails" runat="server">
        <table class="modalPanel">
            <tr>
                <td>
                    <h2>
                        <asp:Label ID="Message" runat="server" Text=""></asp:Label></h2>
                    <asp:Label ID="EntryLogID" runat="server" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" width="100%" style="background-color: #EFF3FB;
                        border-color: #B5C7DE">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSectionNumber" runat="server" Text="*Section Number: " AssociatedControlID="ddlSectionNumber"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlSectionNumber" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblQuestionNumber" runat="server" Text="*Test Number: " AssociatedControlID="txtQuestionNumber"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtQuestionNumber" runat="server" MaxLength="3" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtQuestionNumberE" runat="server" Minimum="1"
                                    Width="50" Maximum="999" TargetControlID="txtQuestionNumber">
                                </ajaxToolkit:NumericUpDownExtender>
                                <asp:RequiredFieldValidator ID="rfvtxtQuestionNumber" runat="server" ControlToValidate="txtQuestionNumber"
                                    ErrorMessage="Question Number is required" Text="Question Number is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtQuestionNumberE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtQuestionNumber" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblCorrectAnswer" runat="server" Text="*Correct Answer: " AssociatedControlID="txtCorrectAnswer"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCorrectAnswer" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtCorrectAnswer" runat="server" ControlToValidate="txtCorrectAnswer"
                                    ErrorMessage="Correct Answer is required" Text="Correct Answer is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtCorrectAnswerE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtCorrectAnswer" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblDifficultyLevel" runat="server" Text="*Difficulty Level: " AssociatedControlID="txtDifficultyLevel"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDifficultyLevel" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtDifficultyLevel" runat="server" ControlToValidate="txtDifficultyLevel"
                                    ErrorMessage="Difficulty Level is required" Text="Difficulty Level is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtDifficultyLevelE" runat="server"
                                    Enabled="True" TargetControlID="rfvtxtDifficultyLevel" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="Add" runat="server" CssClass="standard-text" Text="Add" CausesValidation="True"
                        OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Modify" runat="server" CssClass="standard-text" Text="Modify" CausesValidation="True"
                        OnClick="Modify_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="ModifyNew" runat="server" CssClass="standard-text" Text="Modify and Add New"
                        CausesValidation="True" OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                            width="8" alt="spacer" />
                    <asp:Button ID="Deactivate" runat="server" CssClass="standard-text" Text="Deactivate"
                        Visible="false" CausesValidation="False" OnClick="Deactivate_Click"></asp:Button><img
                            height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Activate" runat="server" CssClass="standard-text" Text="Activate"
                        Visible="false" CausesValidation="False" OnClick="Activate_Click"></asp:Button><img
                            height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Cancel" runat="server" CssClass="standard-text" Text="Cancel" CausesValidation="False">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="pnlRequestDetailsE" runat="server" TargetControlID="hiddenTargetControlForRequestDetails"
        PopupControlID="pnlRequestDetails" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <script type="text/javascript">
        //       document.getElementById("MainContent_pnl1").style.display = "Block";
        //        document.getElementById("MainContent_pnl2").style.display = 'none';
        //        document.getElementById("MainContent_pnl3").style.display = 'none';
        //        document.getElementById("MainContent_pnl4").style.display = 'none';
        //        document.getElementById("MainContent_pnl5").style.display = 'none';
    
    </script>
</asp:Content>
