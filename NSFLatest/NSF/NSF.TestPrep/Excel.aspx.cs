using System;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NSF.TestPrep
{
    public partial class Excel : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["tableForExcel"] != null)
                {
                    if (Session["tableForExcel"] is Table)
                    {
                        var table = (Table) Session["tableForExcel"];
                        BuildExcel(table);
                    }
                    else
                    {
                        var table = (GridView) Session["tableForExcel"];

                        if (Session["ReportHeader"] != null)
                        {
                            GenericExport("TimeTracker.xls", table, (Table) Session["ReportHeader"]);
                        }
                        else
                            GenericExport("TimeTracker.xls", table);
                    }
                }
                else
                {
                    Response.Write("Session not available, please rerun the report.");
                }
            }
            catch (ThreadAbortException)
            {
            }
            catch (Exception ee)
            {
                Response.Write("Error: " + ee.Message);
            }
        }

        private void BuildExcel(Table table)
        {
            //Download to Excel, this writes to excel file line by line
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "";
            EnableViewState = false;
            table.GridLines = GridLines.Both;
            form1.Controls.Add(table);

            var tw = new StringWriter();
            var hw = new HtmlTextWriter(tw);

            table.RenderControl(hw);
            Response.AppendHeader("Content-Disposition", "attachment; Filename = TimeTracker.xls");
            Response.Write(tw.ToString());
            Response.End();
            //ApplicationInstance.CompleteRequest();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }

        private void BuildExcel(GridView table)
        {
            //Download to Excel, this writes to excel file line by line
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "";
            EnableViewState = false;
            var tblReportHeader = new Table();
            if (Session["ReportHeader"] != null)
            {
                tblReportHeader = (Table) Session["ReportHeader"];
                form1.Controls.Add(tblReportHeader);
            }
            form1.Controls.Add(table);

            var tw = new StringWriter();
            var hw = new HtmlTextWriter(tw);

            if (Session["ReportHeader"] != null)
            {
                tblReportHeader.RenderControl(hw);
            }
            table.RenderControl(hw);
            Response.AppendHeader("Content-Disposition", "attachment; Filename = TimeTracker.xls");
            Response.Write(tw.ToString());
            Response.End();
            //ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="gv"></param>
        /// <param name="tblReportHeader"></param>
        public void GenericExport(string fileName, GridView gv, Table tblReportHeader)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader(
                "content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/ms-excel";

            using (var sw = new StringWriter())
            {
                using (var htw = new HtmlTextWriter(sw))
                {
                    //  Create a table to contain the grid
                    var table = new Table {GridLines = gv.GridLines};

                    //  include the gridline settings

                    //  add the header row to the table
                    if (gv.HeaderRow != null)
                    {
                        for (int i = 1; i < gv.HeaderRow.Cells.Count; i++)
                        {
                            string headerText = ((LinkButton) (gv.HeaderRow.Cells[i].Controls[0])).Text.ToUpper();
                            if (!headerText.StartsWith("BLANK"))
                                ((LinkButton) (gv.HeaderRow.Cells[i].Controls[0])).Text =
                                    gv.HeaderRow.Cells[i].ToolTip + " | " + headerText;
                        }

                        PrepareControlForExport(gv.HeaderRow);
                        table.Rows.Add(gv.HeaderRow);

                        table.Rows[0].BackColor = gv.HeaderStyle.BackColor;
                        table.Rows[0].ForeColor = gv.HeaderStyle.ForeColor;
                    }
                    int count = 1;
                    //  add each of the data rows to the table
                    foreach (GridViewRow row in gv.Rows)
                    {
                        PrepareControlForExport(row);
                        table.Rows.Add(row);

                        if (count%2 == 0)
                        {
                            table.Rows[count].BackColor = gv.AlternatingRowStyle.BackColor;
                            table.Rows[count].ForeColor = gv.AlternatingRowStyle.ForeColor;
                        }
                        else
                        {
                            table.Rows[count].BackColor = gv.RowStyle.BackColor;
                            table.Rows[count].ForeColor = gv.RowStyle.ForeColor;
                        }
                        count++;
                    }

                    //  add the footer row to the table
                    if (gv.FooterRow != null)
                    {
                        PrepareControlForExport(gv.FooterRow);
                        table.Rows.Add(gv.FooterRow);
                    }

                    if (tblReportHeader != null)
                    {
                        tblReportHeader.RenderControl(htw);
                    }
                    //  render the table into the htmlwriter
                    table.RenderControl(htw);

                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="gv"></param>
        public void GenericExport(string fileName, GridView gv)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader(
                "content-disposition", string.Format("attachment; filename={0}", fileName));
            HttpContext.Current.Response.ContentType = "application/ms-excel";

            using (var sw = new StringWriter())
            {
                using (var htw = new HtmlTextWriter(sw))
                {
                    //  Create a table to contain the grid
                    var table = new Table {GridLines = gv.GridLines};

                    //  include the gridline settings

                    //  add the header row to the table
                    if (gv.HeaderRow != null)
                    {
                        PrepareControlForExport(gv.HeaderRow);
                        table.Rows.Add(gv.HeaderRow);

                        table.Rows[0].BackColor = gv.HeaderStyle.BackColor;
                        table.Rows[0].ForeColor = gv.HeaderStyle.ForeColor;
                    }
                    int count = 1;
                    //  add each of the data rows to the table
                    foreach (GridViewRow row in gv.Rows)
                    {
                        PrepareControlForExport(row);
                        table.Rows.Add(row);

                        if (count%2 == 0)
                        {
                            table.Rows[count].BackColor = gv.AlternatingRowStyle.BackColor;
                            table.Rows[count].ForeColor = gv.AlternatingRowStyle.ForeColor;
                        }
                        else
                        {
                            table.Rows[count].BackColor = gv.RowStyle.BackColor;
                            table.Rows[count].ForeColor = gv.RowStyle.ForeColor;
                        }
                        count++;
                    }

                    //  add the footer row to the table
                    if (gv.FooterRow != null)
                    {
                        PrepareControlForExport(gv.FooterRow);
                        table.Rows.Add(gv.FooterRow);
                    }

                    //  render the table into the htmlwriter
                    table.RenderControl(htw);

                    //  render the htmlwriter into the response
                    HttpContext.Current.Response.Write(sw.ToString());
                    HttpContext.Current.Response.End();
                }
            }
        }

        /// <summary>
        /// Replace any of the contained controls with literals
        /// </summary>
        /// <param name="control"></param>
        protected void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text.ToUpper()));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                else if (current is Label)
                {
                    var lbl = current as Label;
                    if (lbl.Text.Equals("D"))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as Label).ToolTip));
                    }
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }
    }
}