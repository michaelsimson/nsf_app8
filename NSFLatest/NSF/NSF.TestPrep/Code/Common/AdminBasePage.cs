﻿#region Namespaces

using System;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace NSF.TestPrep.Code.Common
{
    public class AdminBasePage : Page
    {
        protected const string C_Ascending = " ASC";
        protected const string C_Descending = " DESC";
        protected const string VS_SortDirection = "sortDirection";
        protected const string VS_SortExpression = "sortExpression";
        protected const string VS_FilterExpression = "filterExpression";
        protected string C_NotAvailable = ConfigurationManager.AppSettings["CONST_NOTAVAILABLE"];
        protected string C_NotRequiredForSD = ConfigurationManager.AppSettings["CONST_NOTREQUIREDFORSD"];

        protected string C_Select = ConfigurationManager.AppSettings["CONST_SELECT"];
        protected string C_SolutionDevelopment = ConfigurationManager.AppSettings["CONST_SOLUTIONDEVLOPMENTID"];

        protected string _connString =
            ConfigurationManager.ConnectionStrings["NSF_SATConnectionString"].ConnectionString;

        protected string _ldapConnString = ConfigurationManager.AppSettings["SMNA_LDAPConnectionString"];
        protected string _ldapPassword = ConfigurationManager.AppSettings["SMNA_LDAPPassword"];
        protected string _ldapUserName = ConfigurationManager.AppSettings["SMNA_LDAPUserName"];

        /// <summary>
        /// Gets or sets the grid filter expression.
        /// </summary>
        /// <value>The grid filter expression.</value>
        protected string GridFilterExpression
        {
            get
            {
                if (ViewState[VS_FilterExpression] == null)
                    ViewState[VS_FilterExpression] = string.Empty;

                return ViewState[VS_FilterExpression].ToString();
            }
            set { ViewState[VS_FilterExpression] = value; }
        }

        /// <summary>
        /// Property to store Sort Expression of the grid
        /// </summary>
        /// <value>The grid view sort expression.</value>
        protected string GridViewSortExpression
        {
            get
            {
                if (ViewState[VS_SortExpression] == null)
                    ViewState[VS_SortExpression] = string.Empty;

                return ViewState[VS_SortExpression].ToString();
            }
            set { ViewState[VS_SortExpression] = value; }
        }

        /// <summary>
        /// Property to store Sort Direction of the grid
        /// </summary>
        /// <value>The grid view sort direction.</value>
        protected string GridViewSortDirection
        {
            get
            {
                if (ViewState[VS_SortDirection] == null)
                    ViewState[VS_SortDirection] = C_Ascending;

                return ViewState[VS_SortDirection].ToString();
            }
            set { ViewState[VS_SortDirection] = value; }
        }

        protected string CurrentUser
        {
            get { return HttpContext.Current.User.Identity.Name; }
        }

        /// <summary>
        /// Escapes the like value.
        /// </summary>
        /// <param name="valueWithoutWildcards">The value without wildcards.</param>
        /// <returns></returns>
        public static string EscapeLikeValue(string valueWithoutWildcards)
        {
            var sb = new StringBuilder();
            foreach (char c in valueWithoutWildcards)
            {
                if (c == '*' || c == '%' || c == '[' || c == ']')
                    sb.Append("[").Append(c).Append("]");
                else if (c == '\'')
                    sb.Append("''");
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Adds and Removes Item functionality for list boxes
        /// </summary>
        /// <param name="FromListBox">From list box.</param>
        /// <param name="ToListBox">To list box.</param>
        protected void AddRemoveListItems(ListBox FromListBox, ListBox ToListBox)
        {
            try
            {
                foreach (ListItem li in FromListBox.Items)
                {
                    if (li.Selected)
                    {
                        ToListBox.Items.Add(li);
                    }
                }

                foreach (ListItem li in ToListBox.Items)
                {
                    if (li.Selected)
                    {
                        FromListBox.Items.Remove(li);
                    }
                }

                ToListBox.ClearSelection();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Sets the image button.
        /// </summary>
        /// <param name="aImgBtn">A img BTN.</param>
        /// <param name="aImageURL">A image URL.</param>
        /// <param name="aAlternateText">A alternate text.</param>
        /// <param name="aToolTip">A tool tip.</param>
        /// <param name="aIsEnabled">if set to <c>true</c> [a is enabled].</param>
        /// <param name="aCommandName">Name of a command.</param>
        protected static void SetImageButton(ImageButton aImgBtn, string aImageURL, string aAlternateText,
                                             string aToolTip, bool aIsEnabled, string aCommandName)
        {
            aImgBtn.ImageUrl = aImageURL;
            aImgBtn.AlternateText = aAlternateText;
            aImgBtn.ToolTip = aToolTip;
            aImgBtn.Enabled = aIsEnabled;
            aImgBtn.CommandName = aCommandName;
        }

        /// <summary>
        /// Gets the date format.
        /// </summary>
        /// <returns></returns>
        protected string GetCultureInfo()
        {
            //string Culture = "en-US";
            //DataSet ds = new DataSet();

            //string SQL = string.Format("SELECT CultureInfo FROM TT_Users WHERE UserID = {0}", UserList.SelectedValue);

            //Culture = (string)SqlHelper.ExecuteScalar(_connString, CommandType.Text, SQL);

            //return Culture.Equals(string.Empty) ? "en-US" : Culture;

            return CultureInfo.CurrentCulture.Name;
        }

        /// <summary>
        /// Gets the date format.
        /// </summary>
        /// <returns></returns>
        protected string GetDateFormat()
        {
            var culture = new CultureInfo(GetCultureInfo());
            return culture.DateTimeFormat.ShortDatePattern;
        }
    }
}