﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSF.TestPrep.Code.Common;
using NSF.TestPrep.Code.DAL;

namespace NSF.TestPrep.Admin
{
    public partial class AdminAnswerKeys : AdminBasePage
    {
        #region Events

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //int UserID = TTSecurity.GetUserID();

            //// Ensure that the visiting user has access to view the current page
            //if (UserID < 0 || TTSecurity.IsInRole(TTUser.UserRoleAdministrator) == false)
            //{
            //    Response.Redirect("~/AccessDenied.htm", true);
            //}

            if (!IsPostBack)
            {
                BindGridData();
                txtSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + imgBtnFilter.ClientID + "')");
            }
        }

        /// <summary>
        /// Handles the Sorting event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (GridViewSortExpression == e.SortExpression && GridViewSortDirection == C_Ascending)
                GridViewSortDirection = C_Descending;
            else if (GridViewSortExpression == e.SortExpression && GridViewSortDirection == C_Descending)
                GridViewSortDirection = C_Ascending;
            else
                GridViewSortDirection = C_Ascending;

            GridViewSortExpression = e.SortExpression;

            BindGridData();
        }

        /// <summary>
        /// Handles the RowCreated event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        var lnk = (LinkButton) tc.Controls[0];
                        {
                            // inizialize a new image
                            var img = new Image
                                          {
                                              ImageUrl =
                                                  string.Format("~/images/arrow_{0}.png",
                                                                (GridViewSortDirection == C_Ascending ? "up" : "down")),
                                              AlternateText = GridViewSortDirection
                                          };
                            // setting the dynamically URL of the image
                            // checking if the header link is the user's choice
                            if (GridViewSortExpression == lnk.CommandArgument)
                            {
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(img);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the RowDataBound event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //ImageButton lGvImgBtnDelete = e.Row.Cells[4].Controls[0] as ImageButton;
                //lGvImgBtnDelete.Width = Unit.Pixel(16);
                //lGvImgBtnDelete.Height = Unit.Pixel(16);

                //if (gvGridData.DataKeys[e.Row.RowIndex].Values["IsActive"].ToString().ToLower().Equals("true"))
                //{
                //    SetImageButton(lGvImgBtnDelete
                //        , "~/images/delete.png"
                //        , "Deactivate Question Type."
                //        , "Deactivate Question Type"
                //        , true, "deactivate");
                //}
                //else
                //{
                //    SetImageButton(lGvImgBtnDelete
                //        , "~/images/active.png"
                //        , "Activate Question Type."
                //        , "Activate Question Type"
                //        , true, "activate");
                //}
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToLower().Equals("modify") || e.CommandName.ToLower().Equals("deactivate") ||
                e.CommandName.ToLower().Equals("activate"))
            {
                int index = Convert.ToInt32(e.CommandArgument);

                Add.Visible = false;
                Modify.Visible = false;
                ModifyNew.Visible = false;
                Cancel.Visible = false;
                Deactivate.Visible = false;
                Activate.Visible = false;
                DataKey dataKey = gvGridData.DataKeys[index];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        EntryLogID.Text = dataKey.Values["AnswerKeyRecID"].ToString();
                        txtQuestionNumber.Text = dataKey.Values["QuestionNumber"].ToString();
                        txtCorrectAnswer.Text = dataKey.Values["CorrectAnswer"].ToString();
                        txtDifficultyLevel.Text = dataKey.Values["DifficultyLevel"].ToString();
                    }
                }

                txtQuestionNumber.Enabled = false;
                txtCorrectAnswer.Enabled = false;
                txtDifficultyLevel.Enabled = false;

                switch (e.CommandName.ToLower())
                {
                    case "modify":
                        Message.Text = "Modify Existing Test";
                        Modify.Visible = true;
                        Cancel.Visible = true;
                        txtQuestionNumber.Enabled = true;
                        txtCorrectAnswer.Enabled = true;
                        txtDifficultyLevel.Enabled = true;

                        break;
                    case "deactivate":
                        Message.Text = "Deactivate Test";
                        Deactivate.Visible = true;
                        Cancel.Visible = true;


                        break;
                    case "activate":
                        Message.Text = "Activate Test";
                        Activate.Visible = true;
                        Cancel.Visible = true;


                        break;
                }
                pnlRequestDetailsE.Show();
            }
        }

        /// <summary>
        /// Handles the Click event of the NewProjectButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Message.Text = "Create New Test";
            Add.Visible = false;
            Modify.Visible = false;
            ModifyNew.Visible = false;
            Cancel.Visible = false;
            Deactivate.Visible = false;
            Activate.Visible = false;
            Add.Visible = true;
            Cancel.Visible = true;
            txtQuestionNumber.Enabled = true;
            txtCorrectAnswer.Enabled = true;
            txtDifficultyLevel.Enabled = true;
            EntryLogID.Text = string.Empty;
            txtQuestionNumber.Text = string.Empty;
            txtCorrectAnswer.Text = string.Empty;
            txtDifficultyLevel.Text = string.Empty;
            pnlRequestDetailsE.Show();
        }


        /// <summary>
        /// Handles the Click event of the AddEntry control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Add_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyInsertUpdate]", txtQuestionNumber.Text,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the Modify control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Modify_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyInsertUpdate]", txtQuestionNumber.Text,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the DeleteEntry control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Deactivate_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyActiveInActive]", EntryLogID.Text, 0,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the Activate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Activate_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyActiveInActive]", EntryLogID.Text, 1,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the imgBtnFilter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void imgBtnFilter_Click(object sender, ImageClickEventArgs e)
        {
            GridFilterExpression = txtSearch.Text;
            BindGridData();
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGridData.PageIndex = e.NewPageIndex;
            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the imgBtnExportToExcel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void imgBtnExportToExcel_Click(object sender, ImageClickEventArgs e)
        {
            DataView dv;
            using (
                DataSet ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,
                                                      string.Format(
                                                          "SELECT * FROM [dbo].[SATAnswerKey] WHERE TestSectionID IN (SELECT [SATTestSectionRecID] FROM [dbo].[SATTestSectionSetup] WHERE TestID = {0})",
                                                          ddlEventYear.SelectedValue)))
            {
                dv = ds.Tables[0].DefaultView;
            }

            dv.Sort = "QuestionNumber" + " " + C_Ascending;

            //if (!GridFilterExpression.Equals(string.Empty))
            //{
            //    StringBuilder sb = new StringBuilder();
            //    foreach (DataColumn dc in dv.Table.Columns)
            //    {
            //        sb.Append(string.Format(" CONVERT({0}, System.String) LIKE '*{1}*' ", dc.ColumnName, EscapeLikeValue(GridFilterExpression)));
            //        sb.Append(" OR ");
            //    }
            //    sb.Append("~~");

            //    dv.RowFilter = sb.ToString().Replace("OR ~~", string.Empty);
            //}

            var gvExportToExcel = new GridView {DataSource = dv};
            gvExportToExcel.DataBind();
            Session["tableForExcel"] = gvExportToExcel;
            Response.Redirect("~/Excel.aspx");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Binds the level1.
        /// </summary>
        private void BindGridData()
        {
            if (!ddlEventYear.SelectedValue.Equals(string.Empty))
            {
                DataView dv;
                using (
                    DataSet ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,
                                                          string.Format(
                                                              "SELECT * FROM [dbo].[SATAnswerKey] WHERE TestSectionID IN (SELECT [SATTestSectionRecID] FROM [dbo].[SATTestSectionSetup] WHERE TestID = {0})",
                                                              ddlEventYear.SelectedValue)))
                {
                    dv = ds.Tables[0].DefaultView;
                }

                if (GridViewSortExpression.Equals(string.Empty))
                {
                    GridViewSortExpression = "TestSectionID";
                    GridViewSortDirection = C_Ascending;
                }

                dv.Sort = GridViewSortExpression + " " + GridViewSortDirection;

                if (!GridFilterExpression.Equals(string.Empty))
                {
                    var sb = new StringBuilder();
                    foreach (DataColumn dc in dv.Table.Columns)
                    {
                        sb.Append(string.Format(" CONVERT({0}, System.String) LIKE '*{1}*' ", dc.ColumnName,
                                                EscapeLikeValue(GridFilterExpression)));
                        sb.Append(" OR ");
                    }
                    sb.Append("~~");

                    dv.RowFilter = sb.ToString().Replace("OR ~~", string.Empty);
                }

                gvGridData.DataSource = dv;
                gvGridData.DataBind();
            }
        }

        /// <summary>
        /// Enables the disable.
        /// </summary>
        /// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
        private void EnableDisable(bool isEnabled)
        {
            txtQuestionNumber.Enabled = isEnabled;
            txtCorrectAnswer.Enabled = isEnabled;
            txtDifficultyLevel.Enabled = isEnabled;
        }

        /// <summary>
        /// Visibles the hidden.
        /// </summary>
        /// <param name="isVisisble">if set to <c>true</c> [is visisble].</param>
        private void VisibleHidden(bool isVisisble)
        {
            Add.Visible = isVisisble;
            Modify.Visible = isVisisble;
            ModifyNew.Visible = isVisisble;
            Deactivate.Visible = isVisisble;
            Activate.Visible = isVisisble;
        }

        #endregion

        protected void ddlEventYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGridData();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gvGridData.Rows)
            {
                string lId = gvr.Cells[0].Text;
                string lAnswerChoices = string.Empty;
                string lCorrectAnswer = string.Empty;
                string lDifficultyLevel = string.Empty;

                var textBox = gvr.Cells[3].FindControl("gvtxtAnswerChoices") as TextBox;
                if (textBox != null)
                {
                    lAnswerChoices = textBox.Text;
                }
                textBox = gvr.Cells[4].FindControl("gvtxtCorrectAnswer") as TextBox;
                if (textBox != null)
                {
                    lCorrectAnswer = textBox.Text;
                }
                textBox = gvr.Cells[5].FindControl("gvtxtDifficultyLevel") as TextBox;
                if (textBox != null)
                {
                    lDifficultyLevel = textBox.Text;
                }
                SqlHelper.ExecuteNonQuery(_connString, CommandType.Text,
                                          string.Format(
                                              "UPDATE [dbo].[SATAnswerKey] SET AnswerChoices = '{0}', CorrectAnswer = '{1}', DifficultyLevel = '{2}', ModifiedBy = '{3}', ModifyDate = GETDATE() WHERE AnswerKeyRecID = {4}",
                                              lAnswerChoices, lCorrectAnswer.ToUpper(), lDifficultyLevel.ToUpper(),
                                              CurrentUser, lId));
            }
        }
    }
}