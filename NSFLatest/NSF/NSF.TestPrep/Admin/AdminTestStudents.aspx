﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="AdminTestStudents.aspx.cs" Inherits="NSF.TestPrep.Admin.AdminTestStudents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td width="8">
                <img height="8" src="../images/spacer.gif" width="8" alt="spacer" />
            </td>
            <td valign="top">
                <table class="admin-tan-border" cellpadding="10" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="2">
                            <div style="float: left">
                                <h1>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Assign Students To Test"></asp:Literal></h1>
                            </div>
                            <div style="float: right">
                                <asp:Button ID="btnCreate" runat="server" CssClass="standard-text" Text="Create New Test"
                                    OnClick="btnCreate_Click" CausesValidation="false"></asp:Button>
                                <asp:Label ID="lblSearch" runat="server" Text="Search" AssociatedControlID="txtSearch"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                <asp:ImageButton ID="imgBtnFilter" runat="server" ImageUrl="~/images/filter.png"
                                    ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnFilter_Click" CausesValidation="false"
                                    AlternateText="Filter Grid" ToolTip="Filter Grid" />
                                <asp:ImageButton ID="imgBtnExportToExcel" runat="server" ImageUrl="~/images/excel.png"
                                    ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnExportToExcel_Click"
                                    AlternateText="Export to Excel" ToolTip="Export To Excel" CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <asp:GridView ID="gvGridData" runat="server" Width="100%" AutoGenerateColumns="False"
                                PageSize="50" EmptyDataText="No records found" AllowPaging="True" AllowSorting="True"
                                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                                CellPadding="3" DataKeyNames="SATTestSetupRecID,TestNumber,TestDescription, EventYear"
                                OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                OnRowCreated="gvGridData_RowCreated" OnRowDataBound="gvGridData_RowDataBound"
                                OnSorting="gvGridData_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="SATTestSetupRecID" HeaderText="ID" InsertVisible="False"
                                        ReadOnly="True" SortExpression="SATTestSetupRecID" />
                                    <asp:BoundField DataField="EventYear" HeaderText="Event Year" SortExpression="EventYear" />
                                    <asp:BoundField DataField="TestNumber" HeaderText="Test Number" SortExpression="TestNumber" />
                                    <asp:BoundField DataField="TestDescription" HeaderText="Test Description" SortExpression="TestDescription" />
                                    <asp:BoundField DataField="NumberOfStudents" HeaderText="Number Of Students" SortExpression="NumberOfStudents" />
                                    <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By" SortExpression="ModifiedBy" />
                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" SortExpression="ModifyDate"
                                        HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:ButtonField ButtonType="Image" CommandName="modify" ImageUrl="~/images/Modify.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="deactivate" ImageUrl="~/images/Delete.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="11">
                <img height="11" src="../images/spacer.gif" width="11">
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Panel ID="pnlRequestDetails" runat="server">
        <table class="modalPanel">
            <tr>
                <td>
                    <h2>
                        <asp:Label ID="Message" runat="server" Text=""></asp:Label></h2>
                    <asp:Label ID="EntryLogID" runat="server" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" width="100%" style="background-color: #EFF3FB;
                        border-color: #B5C7DE">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblEventYear" runat="server" Text="*Event Year: " AssociatedControlID="txtEventYear"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEventYear" runat="server" MaxLength="4" Width="30"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtEventYear" runat="server" ControlToValidate="txtEventYear"
                                    ErrorMessage="Event Year is required" Text="Event Year is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtEventYearE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtEventYear" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblTestNumber" runat="server" Text="*Test Number: " AssociatedControlID="txtTestNumber"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTestNumber" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtTestNumber" runat="server" ControlToValidate="txtTestNumber"
                                    ErrorMessage="Test Number is required" Text="Test Number is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtTestNumberE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtTestNumber" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblTestDescription" runat="server" Text="*Test Description: " AssociatedControlID="txtTestDescription"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtTestDescription" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtTestDescription" runat="server" ControlToValidate="txtTestDescription"
                                    ErrorMessage="Test Description is required" Text="Test Description is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtTestDescriptionE" runat="server"
                                    Enabled="True" TargetControlID="rfvtxtTestDescription" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            All Students
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Assigned Students
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox ID="lbAllStudents" runat="server" Width="200px" SelectionMode="Multiple"
                                                DataSourceID="SqlDataSource1" DataTextField="FullName" DataValueField="ChildNumber">
                                            </asp:ListBox>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NSF_SATConnectionString %>"
                                                SelectCommand="
                                                SELECT ChildNumber, LTRIM(RTRIM(ISNULL(First_Name, ''))) + ' ' +LTRIM(RTRIM(ISNULL(Middle_Initial, ''))) + ' ' + LTRIM(RTRIM(ISNULL(Last_Name, ''))) AS FullName FROM [Child] WHERE [ChildNumber] NOT IN (SELECT MemberID FROM StudentTests WHERE TestID = @TestID) ORDER BY FullName">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="EntryLogID" Name="TestID" PropertyName="Text" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                        <td valign="middle">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnAssignStudents" runat="server" Text="Assign >" OnClick="btnAssignStudents_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnRemoveStudents" runat="server" Text="< Remove" OnClick="btnRemoveStudents_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lbAssignedStudents" runat="server" Width="200px" SelectionMode="Multiple"
                                                DataSourceID="SqlDataSource2" DataTextField="FullName" DataValueField="ChildNumber">
                                            </asp:ListBox>
                                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NSF_SATConnectionString %>"
                                                SelectCommand="SELECT ChildNumber, LTRIM(RTRIM(ISNULL(First_Name, ''))) + ' ' +LTRIM(RTRIM(ISNULL(Middle_Initial, ''))) + ' ' + LTRIM(RTRIM(ISNULL(Last_Name, ''))) AS FullName FROM [Child] WHERE [ChildNumber] IN (SELECT MemberID FROM StudentTests WHERE TestID = @TestID) ORDER BY FullName">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="EntryLogID" Name="TestID" PropertyName="Text" Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="Add" runat="server" CssClass="standard-text" Text="Add" CausesValidation="True"
                        OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Modify" runat="server" CssClass="standard-text" Text="Modify" CausesValidation="True"
                        OnClick="Modify_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="ModifyNew" runat="server" CssClass="standard-text" Text="Modify and Add New"
                        CausesValidation="True" OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                            width="8" alt="spacer" />
                    <asp:Button ID="Deactivate" runat="server" CssClass="standard-text" Text="Deactivate"
                        Visible="false" CausesValidation="False" OnClick="Deactivate_Click"></asp:Button><img
                            height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Activate" runat="server" CssClass="standard-text" Text="Activate"
                        Visible="false" CausesValidation="False" OnClick="Activate_Click"></asp:Button><img
                            height="8" src="../images/spacer.gif" width="8">
                    <asp:Button ID="Cancel" runat="server" CssClass="standard-text" Text="Cancel" CausesValidation="False">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="pnlRequestDetailsE" runat="server" TargetControlID="hiddenTargetControlForRequestDetails"
        PopupControlID="pnlRequestDetails" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
</asp:Content>
