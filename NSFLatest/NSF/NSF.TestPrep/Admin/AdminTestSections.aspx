﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
         CodeBehind="AdminTestSections.aspx.cs" Inherits="NSF.TestPrep.Admin.AdminTestSections" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td width="8">
                <img height="8" src="../images/spacer.gif" width="8" alt="spacer" />
            </td>
            <td valign="top">
                <table class="admin-tan-border" cellpadding="10" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="2">
                            <div style="float: left">
                                <h1>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Test Sections"></asp:Literal></h1>
                            </div>
                            <div style="float: right">
                                <asp:Button ID="btnCreate" runat="server" CssClass="standard-text" Text="Create New Test Section"
                                            OnClick="btnCreate_Click" CausesValidation="false"></asp:Button>
                                <asp:Label ID="lblSearch" runat="server" Text="Search" AssociatedControlID="txtSearch"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                <asp:ImageButton ID="imgBtnFilter" runat="server" ImageUrl="~/images/filter.png"
                                                 ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnFilter_Click" CausesValidation="false"
                                                 AlternateText="Filter Grid" ToolTip="Filter Grid" />
                                <asp:ImageButton ID="imgBtnExportToExcel" runat="server" ImageUrl="~/images/excel.png"
                                                 ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnExportToExcel_Click"
                                                 AlternateText="Export to Excel" ToolTip="Export To Excel" CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <asp:GridView ID="gvGridData" runat="server" Width="100%" AutoGenerateColumns="False" PageSize="50"
                                          EmptyDataText="No records found" AllowPaging="True" AllowSorting="True" BackColor="White"
                                          BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="SATTestSectionRecID,EventYear, TestNumber,SectionNumber, Subject, SectionTimeLimit, SectionVisible,NumberOfChoices, NumberOfQuestions, QuestionType,DropDownBegin,DropDownEnd"
                                          OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                          OnRowCreated="gvGridData_RowCreated" OnRowDataBound="gvGridData_RowDataBound"
                                          OnSorting="gvGridData_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="SATTestSectionRecID" HeaderText="ID" InsertVisible="False"
                                                    ReadOnly="True" SortExpression="SATTestSectionRecID" />
                                    <asp:BoundField DataField="EventYear" HeaderText="Event Year" SortExpression="EventYear" />
                                    <asp:BoundField DataField="TestNumber" HeaderText="Test Number" SortExpression="TestNumber" />
                                    <asp:BoundField DataField="SectionNumber" HeaderText="Section Number" SortExpression="SectionNumber" />
                                    <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject" />
                                    <asp:BoundField DataField="SectionTimeLimit" HeaderText="Time Limit" SortExpression="SectionTimeLimit" />
                                    <asp:BoundField DataField="SectionVisible" HeaderText="Visible" SortExpression="SectionVisible" />
                                    <asp:BoundField DataField="NumberOfQuestions" HeaderText="# Of Questions" SortExpression="NumberOfQuestions" />
                                    <asp:BoundField DataField="QuestionType" HeaderText="Question Type" SortExpression="QuestionType" />
                                    <asp:BoundField DataField="NumberOfChoices" HeaderText="# Of Choices" SortExpression="NumberOfChoices" />
                                    <asp:BoundField DataField="DropDownBegin" HeaderText="Question No. From" SortExpression="DropDownBegin" />
                                    <asp:BoundField DataField="DropDownEnd" HeaderText="Question No. To" SortExpression="DropDownEnd" />
                                    <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By" SortExpression="ModifiedBy" />
                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" SortExpression="ModifyDate"
                                                    HtmlEncode="false" DataFormatString="{0: MM/dd/yyyy}" />
                                    <asp:ButtonField ButtonType="Image" CommandName="modify" ImageUrl="~/images/Modify.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="deactivate" ImageUrl="~/images/Delete.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="11">
                <img height="11" src="../images/spacer.gif" width="11">
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Panel ID="pnlRequestDetails" runat="server">
        <table class="modalPanel">
            <tr>
                <td>
                    <h2>
                        <asp:Label ID="Message" runat="server" Text=""></asp:Label></h2>
                    <asp:Label ID="EntryLogID" runat="server" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" width="100%" style="background-color: #EFF3FB; border-color: #B5C7DE">
                        <tr>
                            <td align="right" nowrap="nowrap">
                                <asp:Label ID="lblEventYear" runat="server" Text="*Event Year - Test Number: " AssociatedControlID="ddlEventYear"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlEventYear" runat="server" DataSourceID="SqlDataSource1"
                                                  DataTextField="EventYear_TestNumber" DataValueField="SATTestSetupRecID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NSF_SATConnectionString %>"
                                                   SelectCommand="SELECT [SATTestSetupRecID], CONVERT(VARCHAR(4), [EventYear]) +  ' - ' +  CONVERT(VARCHAR(50), [TestNumber]) AS [EventYear_TestNumber] FROM [SATTestSetup]">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSectionNumber" runat="server" Text="*Section Number: " AssociatedControlID="txtSectionNumber"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtSectionNumber" runat="server" MaxLength="2" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtSectionNumberE" runat="server" Minimum="1" Width="50"
                                                                   Maximum="99" TargetControlID="txtSectionNumber">
                                </ajaxToolkit:NumericUpDownExtender>
                                <asp:RequiredFieldValidator ID="rfvtxtSectionNumber" runat="server" ControlToValidate="txtSectionNumber"
                                                            ErrorMessage="Section Number is required" Text="Section Number is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtSectionNumberE" runat="server" Enabled="True"
                                                                      TargetControlID="rfvtxtSectionNumber" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSectionVisible" runat="server" Text="*Section Visible: " AssociatedControlID="ddlSectionVisible"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlSectionVisible" runat="server">
                                    <asp:ListItem Text="Yes" Value="1" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSubject" runat="server" Text="*Subject: " AssociatedControlID="ddlTestArea"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlTestArea" runat="server">
                                    <asp:ListItem Text="-- Select --" Value="-1" />
                                    <asp:ListItem Text="Math" Value="Math" />
                                    <asp:ListItem Text="Critical Reading" Value="Critical Reading" />
                                    <asp:ListItem Text="Writing" Value="Writing" />
                                </asp:DropDownList>
                                <%--<asp:TextBox ID="txtSubject" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtSubject" runat="server" ControlToValidate="txtSubject"
                                    ErrorMessage="Subject is required" Text="Subject is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtSubjectE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtSubject" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSectionTimeLimit" runat="server" Text="*Section Time Limit: " AssociatedControlID="txtSectionTimeLimit"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtSectionTimeLimit" runat="server" MaxLength="3" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtSectionTimeLimitE" runat="server" Minimum="1" Width="50"
                                                                   Maximum="999" TargetControlID="txtSectionTimeLimit">
                                </ajaxToolkit:NumericUpDownExtender>
                                <asp:RequiredFieldValidator ID="rfvtxtSectionTimeLimit" runat="server" ControlToValidate="txtSectionTimeLimit"
                                                            ErrorMessage="Section Time Limit is required" Text="Section Time Limit is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtSectionTimeLimitE" runat="server"
                                                                      Enabled="True" TargetControlID="rfvtxtSectionTimeLimit" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblNumberOfQuestions" runat="server" Text="*Number of Questions: "
                                           AssociatedControlID="txtNumberOfQuestions"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtNumberOfQuestions" runat="server" MaxLength="3" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtNumberOfQuestionsE" runat="server" Minimum="1" Width="50"
                                                                   Maximum="999" TargetControlID="txtNumberOfQuestions">
                                </ajaxToolkit:NumericUpDownExtender>
                                <asp:RequiredFieldValidator ID="rfvtxtNumberOfQuestions" runat="server" ControlToValidate="txtNumberOfQuestions"
                                                            ErrorMessage="Number of Questions is required" Text="Number of Questions is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtNumberOfQuestionsE" runat="server"
                                                                      Enabled="True" TargetControlID="rfvtxtNumberOfQuestions" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblQuestionType" runat="server" Text="*Question Type: " AssociatedControlID="ddlQuestionType"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlQuestionType" runat="server">
                                    <asp:ListItem Text="-- Select --" Value="-1" />
                                    <asp:ListItem Text="RadioButton" Value="RadioButton" />
                                    <asp:ListItem Text="Single-Line TextBox" Value="TextBox" />
                                    <asp:ListItem Text="Multi-Line TextBox" Value="TextArea" />
                                </asp:DropDownList>
                                <%-- <asp:TextBox ID="txtQuestionType" runat="server" MaxLength="320" Width="320"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtQuestionType" runat="server" ControlToValidate="txtQuestionType"
                                    ErrorMessage="Question Type is required" Text="Question Type is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtQuestionTypeE" runat="server" Enabled="True"
                                    TargetControlID="rfvtxtQuestionType" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblNumberOfChoices" runat="server" Text="*Number of Choices: " AssociatedControlID="txtNumberOfChoices"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtNumberOfChoices" runat="server" MaxLength="1" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtNumberOfChoicesE" runat="server" Minimum="1" Width="50"
                                                                   Maximum="5" TargetControlID="txtNumberOfChoices">
                                </ajaxToolkit:NumericUpDownExtender>
                                <asp:RequiredFieldValidator ID="rfvtxtNumberOfChoices" runat="server" ControlToValidate="txtNumberOfChoices"
                                                            ErrorMessage="Number of Choices is required" Text="Number of Choices is required"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="rfvtxtNumberOfChoicesE" runat="server"
                                                                      Enabled="True" TargetControlID="rfvtxtNumberOfChoices" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap">
                                <asp:Label ID="lblDropDownBegin" runat="server" Text="*Question No. From: " AssociatedControlID="txtDropDownBegin"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDropDownBegin" runat="server" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtDropDownBeginE" runat="server" Minimum="1" Width="50"
                                                                   Maximum="1000" TargetControlID="txtDropDownBegin">
                                </ajaxToolkit:NumericUpDownExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" nowrap="nowrap">
                                <asp:Label ID="lblDropDownEnd" runat="server" Text="*Question No. To: " AssociatedControlID="txtDropDownEnd"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDropDownEnd" runat="server" Width="50"></asp:TextBox>
                                <ajaxToolkit:NumericUpDownExtender ID="txtDropDownEndE" runat="server" Minimum="1" Width="50"
                                                                   Maximum="1000" TargetControlID="txtDropDownEnd">
                                </ajaxToolkit:NumericUpDownExtender>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="Add" runat="server" CssClass="standard-text" Text="Add" CausesValidation="True"
                                OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8">
                                                                 <asp:Button ID="Modify" runat="server" CssClass="standard-text" Text="Modify" CausesValidation="True"
                                                                             OnClick="Modify_Click"></asp:Button><img height="8" src="../images/spacer.gif" width="8">
                                                                                                                 <asp:Button ID="ModifyNew" runat="server" CssClass="standard-text" Text="Modify and Add New"
                                                                                                                             CausesValidation="True" OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                                                                                                                                                                                           width="8" alt="spacer" />
                                                                                                                 <asp:Button ID="Deactivate" runat="server" CssClass="standard-text" Text="Deactivate"
                                                                                                                             Visible="false" CausesValidation="False" OnClick="Deactivate_Click"></asp:Button><img
                                                                                                                                                                                                                  height="8" src="../images/spacer.gif" width="8">
                                                                                                                                                                                                              <asp:Button ID="Activate" runat="server" CssClass="standard-text" Text="Activate"
                                                                                                                                                                                                                          Visible="false" CausesValidation="False" OnClick="Activate_Click"></asp:Button><img
                                                                                                                                                                                                                                                                                                             height="8" src="../images/spacer.gif" width="8">
                                                                                                                                                                                                                                                                                                         <asp:Button ID="Cancel" runat="server" CssClass="standard-text" Text="Cancel" CausesValidation="False">
                                                                                                                                                                                                                                                                                                         </asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="pnlRequestDetailsE" runat="server" TargetControlID="hiddenTargetControlForRequestDetails"
                                    PopupControlID="pnlRequestDetails" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
</asp:Content>