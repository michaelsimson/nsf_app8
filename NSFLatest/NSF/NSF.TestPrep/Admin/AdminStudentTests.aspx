﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
         CodeBehind="AdminStudentTests.aspx.cs" Inherits="NSF.TestPrep.Admin.AdminStudentTests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td width="8">
                <img height="8" src="../images/spacer.gif" width="8" alt="spacer" />
            </td>
            <td valign="top">
                <table cellpadding="10" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="2">
                            <div style="float: left">
                                <h1>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Assign Tests To Student"></asp:Literal></h1>
                            </div>
                            <div style="float: right">
                                <asp:Button ID="btnCreate" runat="server" Text="Create New Test"
                                            OnClick="btnCreate_Click" CausesValidation="false"></asp:Button>
                                <asp:Label ID="lblSearch" runat="server" Text="Search" AssociatedControlID="txtSearch"></asp:Label>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                <asp:ImageButton ID="imgBtnFilter" runat="server" ImageUrl="~/images/filter.png"
                                                 ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnFilter_Click" CausesValidation="false"
                                                 AlternateText="Filter Grid" ToolTip="Filter Grid" />
                                <asp:ImageButton ID="imgBtnExportToExcel" runat="server" ImageUrl="~/images/excel.png"
                                                 ImageAlign="AbsMiddle" Width="24" Height="24" OnClick="imgBtnExportToExcel_Click"
                                                 AlternateText="Export to Excel" ToolTip="Export To Excel" CausesValidation="false" />
                            </div>
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <asp:GridView ID="gvGridData" runat="server" Width="100%" AutoGenerateColumns="False"
                                          PageSize="50" EmptyDataText="No records found" AllowPaging="True" AllowSorting="True"
                                          BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                                          CellPadding="3" DataKeyNames="ChildNumber, First_Name, Middle_Initial, Last_Name, Grade, SchoolName"
                                          OnPageIndexChanging="gvGridData_PageIndexChanging" OnRowCommand="gvGridData_RowCommand"
                                          OnRowCreated="gvGridData_RowCreated" OnRowDataBound="gvGridData_RowDataBound"
                                          OnSorting="gvGridData_Sorting">
                                <Columns>
                                    <asp:BoundField DataField="ChildNumber" HeaderText="ID" InsertVisible="False" ReadOnly="True"
                                                    SortExpression="ChildNumber" />
                                    <asp:BoundField DataField="First_Name" HeaderText="First Name" SortExpression="First_Name" />
                                    <asp:BoundField DataField="Middle_Initial" HeaderText="Middle Initial" SortExpression="Middle_Initial" />
                                    <asp:BoundField DataField="Last_Name" HeaderText="Last Name" SortExpression="Last_Name" />
                                    <asp:BoundField DataField="Grade" HeaderText="Grade" SortExpression="Grade" />
                                    <asp:BoundField DataField="SchoolName" HeaderText="School Name" SortExpression="SchoolName" />
                                    <asp:BoundField DataField="NumberOfTests" HeaderText="Number Of Tests" SortExpression="NumberOfTests" />
                                    <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" SortExpression="ModifyDate"
                                                    HtmlEncode="false" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:ButtonField ButtonType="Image" CommandName="modify" ImageUrl="~/images/Modify.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                    <asp:ButtonField ButtonType="Image" CommandName="deactivate" ImageUrl="~/images/Delete.png">
                                        <ItemStyle HorizontalAlign="Center" Width="24px"></ItemStyle>
                                    </asp:ButtonField>
                                </Columns>
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <%--<SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#46A71C" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />--%>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="11">
                <img height="11" src="../images/spacer.gif" width="11">
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Panel ID="pnlRequestDetails" runat="server">
        <table class="modalPanel">
            <tr>
                <td>
                    <h2>
                        <asp:Label ID="Message" runat="server" Text=""></asp:Label></h2>
                    <asp:Label ID="EntryLogID" runat="server" Text="" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" cellpadding="3" cellspacing="0" width="100%" style="background-color: #EFF3FB; border-color: #B5C7DE">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblFirstName" runat="server" Text="*First Name: "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblFirstNameVal" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblLastName" runat="server" Text="*Last Name: "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblLastNameVal" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblSchoolName" runat="server" Text="*School Name: "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblSchoolNameVal" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            All Tests [Event Year - Test Number]
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            Assigned Tests
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox ID="lbAllTests" runat="server" Width="200px" DataSourceID="SqlDataSource1"
                                                         SelectionMode="Multiple" DataTextField="EventYear_TestNumber" DataValueField="SATTestSetupRecID">
                                            </asp:ListBox>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NSF_SATConnectionString %>"
                                                               SelectCommand="SELECT [SATTestSetupRecID], CONVERT(VARCHAR(4), [EventYear]) +  ' - ' +  CONVERT(VARCHAR(50), [TestNumber]) AS [EventYear_TestNumber] FROM [SATTestSetup] WHERE [SATTestSetupRecID] NOT IN (SELECT TestID FROM StudentTests WHERE MemberID = @MemberID)">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="EntryLogID" Name="MemberID" PropertyName="Text"
                                                                          Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                        <td valign="middle">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnAssignTests" runat="server" Text="Assign >" OnClick="btnAssignTests_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnRemoveTests" runat="server" Text="< Remove" OnClick="btnRemoveTests_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lbAssignedTests" runat="server" Width="200px" SelectionMode="Multiple"
                                                         DataSourceID="SqlDataSource2" DataTextField="EventYear_TestNumber" DataValueField="SATTestSetupRecID">
                                            </asp:ListBox>
                                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NSF_SATConnectionString %>"
                                                               SelectCommand="SELECT [SATTestSetupRecID], CONVERT(VARCHAR(4), [EventYear]) +  ' - ' +  CONVERT(VARCHAR(50), [TestNumber]) AS [EventYear_TestNumber] FROM [SATTestSetup] WHERE [SATTestSetupRecID] IN (SELECT TestID FROM StudentTests WHERE MemberID = @MemberID)">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="EntryLogID" Name="MemberID" PropertyName="Text"
                                                                          Type="Int32" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="Add" runat="server" Text="Add" CausesValidation="True" OnClick="Add_Click">
                    </asp:Button><img height="8" src="../images/spacer.gif" width="8">
                                 <asp:Button ID="Modify" runat="server" Text="Modify" CausesValidation="True" OnClick="Modify_Click">
                                 </asp:Button><img height="8" src="../images/spacer.gif" width="8">
                                              <asp:Button ID="ModifyNew" runat="server" Text="Modify and Add New"
                                                          CausesValidation="True" OnClick="Add_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                                                                                                                        width="8" alt="spacer" />
                                              <asp:Button ID="Deactivate" runat="server" Text="Deactivate"
                                                          Visible="false" CausesValidation="False" OnClick="Deactivate_Click"></asp:Button><img
                                                                                                                                               height="8" src="../images/spacer.gif" width="8">
                                                                                                                                           <asp:Button ID="Activate" runat="server" Text="Activate" Visible="false" CausesValidation="False"
                                                                                                                                                       OnClick="Activate_Click"></asp:Button><img height="8" src="../images/spacer.gif"
                                                                                                                                                                                                  width="8">
                                                                                                                                                                                             <asp:Button ID="Cancel" runat="server" Text="Cancel" CausesValidation="False"></asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ModalPopupExtender ID="pnlRequestDetailsE" runat="server" TargetControlID="hiddenTargetControlForRequestDetails"
                                    PopupControlID="pnlRequestDetails" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
</asp:Content>