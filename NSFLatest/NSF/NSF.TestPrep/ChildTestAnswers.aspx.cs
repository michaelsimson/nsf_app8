﻿using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using NSF.TestPrep.Code.Common;
using NSF.TestPrep.Code.DAL;
using System.Collections.Generic;

namespace NSF.TestPrep
{
    public partial class ChildTestAnswers : AdminBasePage
    {
        #region Events

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //int UserID = TTSecurity.GetUserID();

            //// Ensure that the visiting user has access to view the current page
            //if (UserID < 0 || TTSecurity.IsInRole(TTUser.UserRoleAdministrator) == false)
            //{
            //    Response.Redirect("~/AccessDenied.htm", true);
            //}

            if (!IsPostBack)
            {
                BindGridData();
                //txtSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + imgBtnFilter.ClientID + "')");
            }
        }


        protected void SetSectionTimers()
        {
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            var ds = new DataSet();

            ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,
                  string.Format("SELECT Distinct SectionNumber ,Sum(SectionTimeLimit) AS SectionTimeLimit FROM [NSF_SAT].[dbo].[SATTestSectionSetup] Where TestID='{0}' Group By SectionNumber,SectionTimeLimit Order By SectionNumber ASC",
                                              ddlEventYear.SelectedValue));
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        dictionary.Add(Int32.Parse(row["SectionNumber"].ToString()), row["SectionTimeLimit"].ToString());
                    }
                }
            }

            Timer1.Text = dictionary[1];
            Timer2.Text = dictionary[2];
            Timer3.Text = dictionary[3];
            Timer4.Text = dictionary[4];
            Timer5.Text = dictionary[5];

        }
        /// <summary>
        /// Handles the Sorting event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewSortEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (GridViewSortExpression == e.SortExpression && GridViewSortDirection == C_Ascending)
                GridViewSortDirection = C_Descending;
            else if (GridViewSortExpression == e.SortExpression && GridViewSortDirection == C_Descending)
                GridViewSortDirection = C_Ascending;
            else
                GridViewSortDirection = C_Ascending;

            GridViewSortExpression = e.SortExpression;

            BindGridData();
        }

        /// <summary>
        /// Handles the RowCreated event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        var lnk = (LinkButton)tc.Controls[0];
                        if (lnk != null)
                        {
                            // inizialize a new image
                            var img = new Image
                                          {
                                              ImageUrl =
                                                  string.Format("~/images/arrow_{0}.png",
                                                                (GridViewSortDirection == C_Ascending ? "up" : "down")),
                                              AlternateText = GridViewSortDirection
                                          };
                            // setting the dynamically URL of the image
                            // checking if the header link is the user's choice
                            if (GridViewSortExpression == lnk.CommandArgument)
                            {
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(img);
                            }
                        }
                    }
                }
            }
        }
        protected void GridView4_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        var lnk = (LinkButton)tc.Controls[0];
                        if (lnk != null)
                        {
                            // inizialize a new image
                            var img = new Image
                            {
                                ImageUrl =
                                    string.Format("~/images/arrow_{0}.png",
                                                  (GridViewSortDirection == C_Ascending ? "up" : "down")),
                                AlternateText = GridViewSortDirection
                            };
                            // setting the dynamically URL of the image
                            // checking if the header link is the user's choice
                            if (GridViewSortExpression == lnk.CommandArgument)
                            {
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(img);
                            }
                        }
                    }
                }
            }
        }
        protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        var lnk = (LinkButton)tc.Controls[0];
                        if (lnk != null)
                        {
                            // inizialize a new image
                            var img = new Image
                            {
                                ImageUrl =
                                    string.Format("~/images/arrow_{0}.png",
                                                  (GridViewSortDirection == C_Ascending ? "up" : "down")),
                                AlternateText = GridViewSortDirection
                            };
                            // setting the dynamically URL of the image
                            // checking if the header link is the user's choice
                            if (GridViewSortExpression == lnk.CommandArgument)
                            {
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(img);
                            }
                        }
                    }
                }
            }
        }
        protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        var lnk = (LinkButton)tc.Controls[0];
                        if (lnk != null)
                        {
                            // inizialize a new image
                            var img = new Image
                            {
                                ImageUrl =
                                    string.Format("~/images/arrow_{0}.png",
                                                  (GridViewSortDirection == C_Ascending ? "up" : "down")),
                                AlternateText = GridViewSortDirection
                            };
                            // setting the dynamically URL of the image
                            // checking if the header link is the user's choice
                            if (GridViewSortExpression == lnk.CommandArgument)
                            {
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(img);
                            }
                        }
                    }
                }
            }
        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell tc in e.Row.Cells)
                {
                    if (tc.HasControls())
                    {
                        // search for the header link
                        var lnk = (LinkButton)tc.Controls[0];
                        if (lnk != null)
                        {
                            // inizialize a new image
                            var img = new Image
                            {
                                ImageUrl =
                                    string.Format("~/images/arrow_{0}.png",
                                                  (GridViewSortDirection == C_Ascending ? "up" : "down")),
                                AlternateText = GridViewSortDirection
                            };
                            // setting the dynamically URL of the image
                            // checking if the header link is the user's choice
                            if (GridViewSortExpression == lnk.CommandArgument)
                            {
                                // adding a space and the image to the header link
                                tc.Controls.Add(new LiteralControl(" "));
                                tc.Controls.Add(img);
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Handles the RowDataBound event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataKey dataKey = gvGridData.DataKeys[e.Row.RowIndex];
                    if (dataKey != null)
                    {
                        if (dataKey.Values != null)
                        {
                            string lquestiontype = dataKey.Values["QuestionType"].ToString();
                            if (lquestiontype.Equals("RadioButton"))
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = false; 
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = true;
                               // var lradiobuttonlist = new RadioButtonList
                               //                            {
                               //                                ID =
                               //                                    string.Format("rbl_{0}_{1}_{2}", e.Row.Cells[0].Text,
                               //                                                  e.Row.Cells[2].Text,
                               //                                                  e.Row.Cells[3].Text),
                               //                                RepeatDirection = RepeatDirection.Horizontal,
                               //                                RepeatLayout = RepeatLayout.Table
                               //                            };
                               // lradiobuttonlist.Items.Add(new ListItem("A", "A"));
                               // lradiobuttonlist.Items.Add(new ListItem("B", "B"));
                               // lradiobuttonlist.Items.Add(new ListItem("C", "C"));
                               // lradiobuttonlist.Items.Add(new ListItem("D", "D"));
                               // lradiobuttonlist.Items.Add(new ListItem("E", "E"));
                               // e.Row.Cells[4].Controls.Clear();                                 
                               //.Add(lradiobuttonlist);
                            }
                            else 
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = true;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }
       
        protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataKey dataKey = GridView4.DataKeys[e.Row.RowIndex];
                    if (dataKey != null)
                    {
                        if (dataKey.Values != null)
                        {
                            string lquestiontype = dataKey.Values["QuestionType"].ToString();
                            if (lquestiontype.Equals("RadioButton"))
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = false;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = true;
                            }
                            else
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = true;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }
        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataKey dataKey = GridView3.DataKeys[e.Row.RowIndex];
                    if (dataKey != null)
                    {
                        if (dataKey.Values != null)
                        {
                            string lquestiontype = dataKey.Values["QuestionType"].ToString();
                            if (lquestiontype.Equals("RadioButton"))
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = false;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = true;
                            }
                            else
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = true;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataKey dataKey = GridView2.DataKeys[e.Row.RowIndex];
                    if (dataKey != null)
                    {
                        if (dataKey.Values != null)
                        {
                            string lquestiontype = dataKey.Values["QuestionType"].ToString();
                            if (lquestiontype.Equals("RadioButton"))
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = false;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = true;
                            }
                            else
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = true;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataKey dataKey = GridView1.DataKeys[e.Row.RowIndex];
                    if (dataKey != null)
                    {
                        if (dataKey.Values != null)
                        {
                            string lquestiontype = dataKey.Values["QuestionType"].ToString();
                            if (lquestiontype.Equals("RadioButton"))
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = false;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = true;
                            }
                            else
                            {
                                e.Row.Cells[0].FindControl("gvtxtCorrectAnswer").Visible = true;
                                e.Row.Cells[0].FindControl("rblCorrectAnswer").Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //throw;
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.ToLower().Equals("modify") || e.CommandName.ToLower().Equals("deactivate") ||
                e.CommandName.ToLower().Equals("activate"))
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvGridData.Rows[index];

                Add.Visible = false;
                Modify.Visible = false;


                ModifyNew.Visible = false;
                Cancel.Visible = false;
                Deactivate.Visible = false;
                Activate.Visible = false;
                DataKey dataKey = gvGridData.DataKeys[index];
                if (dataKey != null)
                {
                    if (dataKey.Values != null)
                    {
                        EntryLogID.Text = dataKey.Values["AnswerKeyRecID"].ToString();
                        txtQuestionNumber.Text = dataKey.Values["QuestionNumber"].ToString();
                        txtCorrectAnswer.Text = dataKey.Values["CorrectAnswer"].ToString();
                        txtDifficultyLevel.Text = dataKey.Values["DifficultyLevel"].ToString();
                    }
                }

                txtQuestionNumber.Enabled = false;
                txtCorrectAnswer.Enabled = false;
                txtDifficultyLevel.Enabled = false;

                switch (e.CommandName.ToLower())
                {
                    case "modify":
                        Message.Text = "Modify Existing Test";
                        Modify.Visible = true;
                        Cancel.Visible = true;
                        txtQuestionNumber.Enabled = true;
                        txtCorrectAnswer.Enabled = true;
                        txtDifficultyLevel.Enabled = true;

                        break;
                    case "deactivate":
                        Message.Text = "Deactivate Test";
                        Deactivate.Visible = true;
                        Cancel.Visible = true;


                        break;
                    case "activate":
                        Message.Text = "Activate Test";
                        Activate.Visible = true;
                        Cancel.Visible = true;


                        break;
                }
                pnlRequestDetailsE.Show();
            }
        }
      

        /// <summary>
        /// Handles the Click event of the NewProjectButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Message.Text = "Create New Test";
            Add.Visible = false;
            Modify.Visible = false;
            ModifyNew.Visible = false;
            Cancel.Visible = false;
            Deactivate.Visible = false;
            Activate.Visible = false;
            Add.Visible = true;
            Cancel.Visible = true;
            txtQuestionNumber.Enabled = true;
            txtCorrectAnswer.Enabled = true;
            txtDifficultyLevel.Enabled = true;
            EntryLogID.Text = string.Empty;
            txtQuestionNumber.Text = string.Empty;
            txtCorrectAnswer.Text = string.Empty;
            txtDifficultyLevel.Text = string.Empty;
            pnlRequestDetailsE.Show();
        }


        /// <summary>
        /// Handles the Click event of the AddEntry control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Add_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyInsertUpdate]", txtQuestionNumber.Text,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the Modify control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Modify_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyInsertUpdate]", txtQuestionNumber.Text,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the DeleteEntry control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Deactivate_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyActiveInActive]", EntryLogID.Text, 0,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the Activate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Activate_Click(object sender, EventArgs e)
        {
            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_SATAnswerKeyActiveInActive]", EntryLogID.Text, 1,
                                      CurrentUser);

            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the imgBtnFilter control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void imgBtnFilter_Click(object sender, ImageClickEventArgs e)
        {
            // GridFilterExpression = txtSearch.Text;
            BindGridData();
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the gvLevel1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void gvGridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGridData.PageIndex = e.NewPageIndex;
            BindGridData();
        }

        /// <summary>
        /// Handles the Click event of the imgBtnExportToExcel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void imgBtnExportToExcel_Click(object sender, ImageClickEventArgs e)
        {
            var ds = new DataSet();

            //ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,
            //                              string.Format(
            //                                  "SELECT * FROM [dbo].[SATAnswerKey] WHERE TestSectionID IN (SELECT [SATTestSectionRecID] FROM [dbo].[SATTestSectionSetup] WHERE TestID = {0})",
            //                                  ddlEventYear.SelectedValue));

            //ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,
            //                              string.Format(
            //                                  "SELECT * FROM [dbo].[SATAnswerKey] WHERE TestSectionID IN (SELECT [SATTestSectionRecID] FROM [dbo].[SATTestSectionSetup] WHERE TestID = {0})",
            //                                  ddlEventYear.SelectedValue));

            DataView dv = ds.Tables[0].DefaultView;

            dv.Sort = "QuestionNumber" + " " + C_Ascending;

            //if (!GridFilterExpression.Equals(string.Empty))
            //{
            //    StringBuilder sb = new StringBuilder();
            //    foreach (DataColumn dc in dv.Table.Columns)
            //    {
            //        sb.Append(string.Format(" CONVERT({0}, System.String) LIKE '*{1}*' ", dc.ColumnName, EscapeLikeValue(GridFilterExpression)));
            //        sb.Append(" OR ");
            //    }
            //    sb.Append("~~");

            //    dv.RowFilter = sb.ToString().Replace("OR ~~", string.Empty);
            //}

            var gvExportToExcel = new GridView { DataSource = dv };
            gvExportToExcel.DataBind();
            Session["tableForExcel"] = gvExportToExcel;
            Response.Redirect("~/Excel.aspx");
        }

        #endregion

        #region Private Methods

        /// <summary>



        /// Binds the level1.
        /// </summary>
        private void BindGridData()
        {
            if (!ddlEventYear.SelectedValue.Equals(string.Empty))
            {
                var ds = new DataSet();
                StringBuilder sb = new StringBuilder("create TABLE #series(id int) ; WITH cte AS ( SELECT 1 AS n UNION ALL SELECT n + 1 FROM cte WHERE n < 100)");
                sb.Append ("insert into #series select * from cte c ; Select A.TestID ,A.QuestionType,A.SectionNumber AS TestSectionID  from #series AS S Cross Join SATTestSectionSetup A");
                sb.Append (" where S.id  < = A.NumberOfQuestions AND TestID={0} drop table #series ;");
                string query=string.Format(sb.ToString(),ddlEventYear.SelectedValue);

                ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,query);

                DataView dv = ds.Tables[0].DefaultView;

                if (GridViewSortExpression.Equals(string.Empty))
                {
                    GridViewSortExpression = "TestSectionID";
                    GridViewSortDirection = C_Ascending;
                }

                dv.Sort = GridViewSortExpression + " " + GridViewSortDirection;

                //if (!GridFilterExpression.Equals(string.Empty))
                //{
                //    var sb = new StringBuilder();
                //    foreach (DataColumn dc in dv.Table.Columns)
                //    {
                //        sb.Append(string.Format(" CONVERT({0}, System.String) LIKE '*{1}*' ", dc.ColumnName,
                //                                EscapeLikeValue(GridFilterExpression)));
                //        sb.Append(" OR ");
                //    }
                //    sb.Append("~~");

                //    dv.RowFilter = sb.ToString().Replace("OR ~~", string.Empty);
                //}

                dv.RowFilter = "TestSectionID = 1";
                gvGridData.DataSource = dv;
                gvGridData.DataBind();

                dv.RowFilter = "TestSectionID = 2";
                GridView1.DataSource = dv;
                GridView1.DataBind();

                dv.RowFilter = "TestSectionID = 3";
                GridView2.DataSource = dv;
                GridView2.DataBind();

                dv.RowFilter = "TestSectionID = 4";
                GridView3.DataSource = dv;
                GridView3.DataBind();

                dv.RowFilter = "TestSectionID = '5'";
                GridView4.DataSource = dv;
                GridView4.DataBind();
            }
        }

        /// <summary>
        /// Enables the disable.
        /// </summary>
        /// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
        private void EnableDisable(bool isEnabled)
        {
            txtQuestionNumber.Enabled = isEnabled;
            txtCorrectAnswer.Enabled = isEnabled;
            txtDifficultyLevel.Enabled = isEnabled;
        }

        /// <summary>
        /// Visibles the hidden.
        /// </summary>
        /// <param name="isVisisble">if set to <c>true</c> [is visisble].</param>
        private void VisibleHidden(bool isVisisble)
        {
            Add.Visible = isVisisble;
            Modify.Visible = isVisisble;
            ModifyNew.Visible = isVisisble;
            Deactivate.Visible = isVisisble;
            Activate.Visible = isVisisble;
        }

        #endregion

        protected void ddlEventYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindddlStudentID();
            BindGridData();
            SetSectionTimers();
        }
        protected void BindddlStudentID()
        {
            List<string> list = new List<string>();
            var ds = new DataSet();

            ds = SqlHelper.ExecuteDataset(_connString, CommandType.Text,
                  string.Format("SELECT A.MemberID AS MemberID,(Convert(Varchar(50),A.MemberID)+' '+ISNULL(B.FIRST_NAME,' ') + ' '+ ISNUll(B.LAST_NAME,'')) As StudentDetails FROM StudentTests AS A Left JOIN ChildInfo AS B ON A.MemberID =B.MEMBERID Where A.TestID ='{0}'",
                                              ddlEventYear.SelectedValue));
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlmemberID.DataSource = ds;
                    ddlmemberID.DataBind();
                 
               }
            }

        }

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{
        //    foreach (GridViewRow gvr in gvGridData.Rows)
        //    {
        //        int lChildNumber = 18726;
        //        int lTestID = Convert.ToInt32(ddlEventYear.SelectedValue);
        //        int lSectionID = Convert.ToInt32(gvr.Cells[2].Text);
        //        int lQuestionNumber = Convert.ToInt32(gvr.Cells[3].Text);
        //        var textBox = gvr.Cells[4].FindControl("gvtxtCorrectAnswer") as TextBox;
        //        if (textBox != null)
        //        {
        //            string lAnswer = textBox.Text;
        //            SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_ChildTestAnswersInsertUpdate]", lChildNumber,
        //                                      lTestID,
        //                                      lSectionID, lQuestionNumber, lAnswer);
        //        }
        //    }
        //}


        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            btnSubmit.Enabled = false;
            int lChildNumber =Convert.ToInt32(ddlmemberID.SelectedValue);
            int lTestID = Convert.ToInt32(ddlEventYear.SelectedValue);
            SubmitSectionResults(gvGridData, lChildNumber, lTestID);
            SubmitSectionResults(GridView1, lChildNumber, lTestID);
            SubmitSectionResults(GridView2, lChildNumber, lTestID);
            SubmitSectionResults(GridView3, lChildNumber, lTestID);
            SubmitSectionResults(GridView4, lChildNumber, lTestID);
           // Response.Redirect("~/Results.aspx");
        }

        protected void SubmitSectionResults(GridView grid, int lChildNumber, int lTestID)
        {
            string correctanswer = string.Empty;
            int lSectionID = Convert.ToInt32(grid.DataKeys[0]["TestSectionID"]);
            int lQuestionNumber = 1;
            foreach (GridViewRow gvr in grid.Rows)
            {
                if (gvr.RowType == DataControlRowType.DataRow)
                {
                    if (gvr.Cells[4].HasControls())
                    {
                        int count = gvr.Cells[4].Controls.Count;
                        foreach (Control control in gvr.Cells[4].Controls)
                        {
                            if (control is RadioButtonList)
                            {
                                RadioButtonList list = (RadioButtonList)control;
                                list.Enabled = true;
                                correctanswer = list.SelectedValue.ToString();
                            }
                            else if (control is TextBox)
                            {
                                TextBox tb = (TextBox)control;
                                correctanswer = tb.Text.ToString();
                            }
                           
                        }
                      
                    }
                }
                // var textBox = gvr.Cells[3].FindControl("gvtxtCorrectAnswer") as TextBox;

                string lAnswer = correctanswer;
                SqlHelper.ExecuteNonQuery(_connString, "[dbo].[usp_ChildTestAnswersInsertUpdate]", lChildNumber,
                                          lTestID,
                                          lSectionID, lQuestionNumber, lAnswer);
                lQuestionNumber += 1;
            }

        }
    }
}