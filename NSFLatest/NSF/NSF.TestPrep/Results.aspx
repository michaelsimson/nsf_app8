﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Results.aspx.cs" Inherits="NSF.TestPrep.Results" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" >

        var time = 50;
        var SD;
        $(document).ready(function () {
            $(':input[name*="rblist"]').attr("disabled", "disabled");
            $("#btnstart").click(function () {
            $(':input[name*="rblist"]').removeAttr("disabled");
            SD = setInterval("Tick()", 1000);
         });$("#btnstop").click(function () {
           $(':input[name*="rblist"]').attr("disabled", "disabled");
           clearInterval(SD);
         });
});

        function Tick() {
            time -= 1;
            updateTime();
            if (time < 0) {
                clearInterval(SD);
                $(':input[name*="rblist"]').attr("disabled", "disabled");
            }

        }
      
       function updateTime() {
           if(time>0)
             {
               if (time<10) {
                    document.getElementById('display').textContent = "0"+time;
                }else{
                 document.getElementById('display').textContent = time;
                }
            }else{
                document.getElementById('display').textContent = 'Timeout'
            }
        }
    
    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%--<h1>Results:</h1>
<input type="button" id="btnstart" value="start" />
<input type="button" id="btnstop" Value="Stop"/>
<label id="display"></label>
<div id="answer">
<asp:RadioButtonList id="rblist" runat="server">
<asp:ListItem Text="A" Value ="A"></asp:ListItem>
<asp:ListItem Text="B" Value ="B"></asp:ListItem>
<asp:ListItem Text="C" Value ="C"></asp:ListItem>
<asp:ListItem Text="D" Value ="D"></asp:ListItem>
</asp:RadioButtonList>--%>
<%--</div>--%>
</asp:Content>

