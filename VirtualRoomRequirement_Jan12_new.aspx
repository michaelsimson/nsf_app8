﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="VirtualRoomRequirement_Jan12_new.aspx.cs" Inherits="VirtualRoomRequirement" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <style> 
.vr_submit_btn
{
	position:relative;
	top:30px;
}
.gv_vr_rep
{
	margin-top:-185px;
	}
	.err_label_vr
	{
		position:relative;
		top:40px;
		left:14px;		
		}
</style>
	<div>
		<table border="0" cellpadding ="3" cellspacing = "0" width ="980">
                <tr><td align="left">
                <asp:hyperlink CssClass="btn_02" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
                 &nbsp;&nbsp;&nbsp;<asp:LinkButton CssClass="btn_02" ID="LBmain" runat="server" onclick="LinkButton1_Click">Back to Main Page</asp:LinkButton>
                  
                    &nbsp;&nbsp;
                    </td></tr>
        </table>
        <asp:Panel ID="Panel1" Visible="false" runat="server">
        <table>
        </table></asp:Panel>
       <asp:Panel runat="server">
        <div align="center"  style="font-size:26px; font-weight:bold ; font-family:Calibri;color: rgb(73, 177, 23);margin-bottom:-25px;">
            Virtual Room Requirement
        
            <br />
           
            <br />
   <br />
            <br /></asp:Panel>
            <table  width=100% >
           <tr>
            <td >
           <div style="padding-left: 175px;width: 175px;">
                <asp:Label ID="Label1" runat="server"  Text="Year" ></asp:Label>
                <asp:DropDownList ID="DDyear" runat="server" >
                </asp:DropDownList>
                </div>
          </td>
           <td >
           <div style="padding-left: 15px;width: 210px;">
                <asp:Label ID="lblpre" runat="server"  Text="Pre-Class Preparation" ></asp:Label>
                <asp:DropDownList ID="DDpre" runat="server" onselectedindexchanged="DDpre_SelectedIndexChanged">
                </asp:DropDownList>
                </div>
          </td>
          <td style="width: 50%;text-align: left;margin-top: -95px;margin-bottom: -95px;">   
          <asp:Button ID="excel" runat="server"  Text="Export To Excel" Visible="false" 
                onclick="excel_Click"/>
                
              
                </td>
                
          <td>
          <div style="padding-right: 175px;width: 230px;">
            
                <asp:Label ID="lblpost" runat="server"  Text="Post-Class Coverage"></asp:Label>
                <asp:DropDownList ID="DDpost" runat="server" 
                    onselectedindexchanged="DDpost_SelectedIndexChanged">
                </asp:DropDownList></div>
                </td></tr>
                <tr> 
                    <td> &nbsp;</td>
                    <td> <asp:Label ID="lblalert" runat="server" ForeColor="Red" 
                        Text="Select All options" CssClass="err_label_vr"></asp:Label></td> 
                     <td> &nbsp;</td>
                </tr>
              </table>
              <br />
               <table><tr> <td align="left" style="width: 450px;" >
          
                    </td>
                    <td>  <asp:Button ID="Btnsubmit" runat="server" Text="Submit" CssClass="vr_submit_btn" 
                onclick="Virtual_Click" style="height: 26px; top: 30px; left: 0px;" /></td>
                      </tr></table>
                <br />
                <br />
                <br />
                <table><tr><td>
            
          
         </td></tr></table>
        
                <br />
                <br />
                <br />
                
                <br />
                
        </div>
        <br />
            <br />
        <div>
        <table></table></div>
        <asp:DataGrid ID="gridview_VR" runat="server"    
                    AutoGenerateColumns="False" Height="14px" CellPadding="2"  
                    BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True" Font-Italic="False" 
                    Font-Overline="False" Font-Strikeout="False" 
            Font-Underline="False" CssClass="gv_vr_rep" >
             
             
         
                <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
			   
			    <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                   
              <asp:TemplateColumn ItemStyle-HorizontalAlign="Left"  HeaderText="S.No">
               <HEADERSTYLE Width="130px" Font-Bold="True" ForeColor="White"></HEADERSTYLE> 
               
                <ItemTemplate><%#Container.DataSetIndex+1 %></ItemTemplate>
               
          

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:TemplateColumn>
                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="SignUpID ">
                    
                        <ItemTemplate>
                                 <asp:Label ID="lblsign" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SignUpID")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >
              <%--          <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Rownum ">
                      <HEADERSTYLE Width="130px" Font-Bold="True" ForeColor="White"></HEADERSTYLE> 
                        <ItemTemplate>
                                 <asp:Label ID="lblsign" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RowNum")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >--%>
                       <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Member ID">
                    
                        <ItemTemplate>
                                 <asp:Label ID="lblmember" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="First Name">
                     
                        <ItemTemplate>
                                 <asp:Label ID="lblfirstname" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FirstName")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Last Name">
                  
                        <ItemTemplate>
                                 <asp:Label ID="lbllastname" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LastName")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >
                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Event Year ">
                    
                        <ItemTemplate>
                                 <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >
                     <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="ProductGroup Code ">
                    
                   
                        <ItemTemplate>
                                 <asp:Label ID="lblpgcode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                    </ItemTemplate> 
                          
                                                                                    

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn > 
                      <asp:TemplateColumn HeaderText="Product Code"> 
                    
                      <ItemTemplate>
                      <asp:Label ID="lblpcode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                       </ItemTemplate>
                      </asp:TemplateColumn>                      
                      <asp:TemplateColumn HeaderText="Level">   
                     
                         <ItemTemplate>
                      <asp:Label ID="lbllevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                       </ItemTemplate>
                      </asp:TemplateColumn>
                                           
                      <asp:TemplateColumn HeaderText="Accepted">
                          
                            <ItemTemplate>
                                 <asp:Label ID="lblaccept" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'></asp:Label>
                                    </ItemTemplate>  
                                           
                       </asp:TemplateColumn >
                               <asp:TemplateColumn HeaderText="D">
                          
                            <ItemTemplate>
                                 <asp:Label ID="lblD" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.D")%>'></asp:Label>
                                    </ItemTemplate>  
                                           
                       </asp:TemplateColumn >
                     
                       <asp:TemplateColumn HeaderText="Day">
                           
                             <ItemTemplate>
                                 <asp:Label ID="lblday" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Day")%>'></asp:Label>
                                    </ItemTemplate>
                           
                                
                       </asp:TemplateColumn> 
                       
                       <asp:TemplateColumn HeaderText="Time">
                            
                              <ItemTemplate>
                                 <asp:Label ID="lbltime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                    </ItemTemplate>
                                  
                                 
                       </asp:TemplateColumn> 
                       <asp:TemplateColumn HeaderText="Duration" Visible="true" >
                           
                                 <ItemTemplate>
                                 <asp:Label ID="lblduration" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Duration")%>'></asp:Label>
                                    </ItemTemplate>
                           
                                                                    
                        </asp:TemplateColumn>
                          <asp:TemplateColumn HeaderText="Begin Time" Visible="true" >
                            
                               <ItemTemplate>
                                 <asp:Label ID="lblbegin" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Begin")%>'></asp:Label>
                                    </ItemTemplate>
                             
                                                                    
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="End Time">
                            
                               <ItemTemplate>
                                 <asp:Label ID="lblEnd" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.End")%>'></asp:Label>
                                    </ItemTemplate>
                            
                                                                      
                         </asp:TemplateColumn > 
                        <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Cycle" >
                             
                               <ItemTemplate>
                                 <asp:Label ID="lblcycle" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Cycle")%>'></asp:Label>
                                    </ItemTemplate>  
                      
<ItemStyle HorizontalAlign="Left"></ItemStyle>
                       </asp:TemplateColumn>  
                       <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Virtual Room" >
                            
                            <ItemTemplate>
                                 <asp:Label ID="lblvir" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.VRoom")%>'></asp:Label>
                                    </ItemTemplate>  

<ItemStyle HorizontalAlign="Left"></ItemStyle>
                     </asp:TemplateColumn>    
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="UserID">
                     
                        <ItemTemplate>
                        <asp:Label ID="Label42" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.UserId")%>'></asp:Label>
                          <%-- <asp:Label ID="Label1" runat="server" Text='<%#"northsouth.account0" +DataBinder.Eval(Container, "DataItem.VRoom")+"@live.com"%>'></asp:Label>
                         --%>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >
                         <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Password">
                     
                        <ItemTemplate>
                        <asp:Label ID="Labe2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Password")%>'></asp:Label>
                                 <%--<asp:Label ID="Lblpwd" runat="server" Text='<%#"nsfaccount0" +DataBinder.Eval(Container, "DataItem.VRoom")%>'></asp:Label>--%>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >    
                        <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Previous" >
                            
                               <ItemTemplate>
                       <%--       <asp:Label ID="Labe2" runat="server" Text='<%#processpwd(Eval("Previous"))%>'></asp:Label>--%>
                                <asp:Label ID="lblprev" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Previous")%>'></asp:Label>
                                <%-- <asp:Label ID="lblrow" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RowNum")%>'></asp:Label>--%>
                                    </ItemTemplate>  
                      
<ItemStyle HorizontalAlign="Left"></ItemStyle>
                       </asp:TemplateColumn>  
                          <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Next" >
                            
                               <ItemTemplate>
                               <%--<asp:Label ID="Labe2" runat="server" Text='<%#processpwd(Eval("next"))%>'></asp:Label>--%>
                               <asp:Label ID="lblnext" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.next")%>'></asp:Label>
                                <%-- <asp:Label ID="lblrow" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RowNum")%>'></asp:Label>--%>
                                    </ItemTemplate>  
                      
<ItemStyle HorizontalAlign="Left"></ItemStyle>
                       </asp:TemplateColumn>                 
                    </Columns> 
           	        <PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
                   
                </asp:DataGrid>
        
	</div>
</asp:Content>

