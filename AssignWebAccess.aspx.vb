﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class AssignWebAccess
    Inherits System.Web.UI.Page

    '*********************  FERDINE SILVA  Oct 22, 2010 ***************************
    '*  All RoleID with 1,2,3,4,5 have permission to access the webpagemgmt	      *
    '*  RoleID 1 has access to all public, Private, etc (Root)              	  *
    '*  RoleID 2 - 5 has the access to chapter Panel selection they come through  *
    '*  RoleID 1-5 should not have any assignmant in VolDocAccess table    	      *
    '*  Assigned volunteer cant be let to delete from Volunteer table.       	  *
    '******************************************************************************


    'Update chapter set WebFolderName = Replace(Replace(Replace(state+'_'+Name,' ',''),'.',''),',','')

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Server.Transfer("login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "90") Then
                If (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "4") Then
                    ' search in Cluster  for role 4,5
                    Session("OnlyCluster") = " AND ChapterID in (select ChapterID from Chapter where ClusterId = (select ClusterId from Chapter where ChapterID =" & Session("selChapterID") & ")) "
                ElseIf (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Then
                    ' search in Zone  for role 2,3
                    Session("OnlyCluster") = " AND ChapterID in (select ChapterID from Chapter where ZoneId = (select ZoneID from Chapter where ChapterID =" & Session("selChapterID") & ")) "
                ElseIf Session("RoleId").ToString() = "1" Then
                    btnassignwebadmin.Visible = True
                Else
                    Session("OnlyCluster") = " "
                End If
                loadvolunteer()
            Else
                Server.Transfer("maintest.aspx")
            End If
            'loadvolunteer()
        End If
    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append(" firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and lastName like '%" + lastName + "%'")
            Else
                strSql.Append(" lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and Email like '%" + email + "%'")
            Else
                strSql.Append(" Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(Session("OnlyCluster"))
            strSql.Append(" order by lastname,firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Search.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        'MsgBox(SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql))
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtParent.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            HlblMemberID.Text = GridMemberDt.DataKeys(index).Value
            pIndSearch.Visible = False
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique"
        End If

    End Sub

    Protected Sub btnassigncoach_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not HlblMemberID.Text = "" Then
            'Dim strCh As String = ""
            'Dim strch1 As String = ""
            'If Session("RoleId").ToString() = "5" Then
            '    strCh = ",ChapterId,ChapterCode"
            '    strch1 = "," & Session("selChapterID") & ",'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select ChapterCode from Chapter where ChapterID=" & Session("selChapterID") & "") & "'"
            'End If
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid in(1,2,3,4,5,90,43) and memberid=" & HlblMemberID.Text & "") < 1 Then
                Dim strsql As String = "Insert into volunteer(MemberID, RoleId, RoleCode, EventYear, CreateDate, CreatedBy,[national]) values ("  '" & strCh & "
                strsql = strsql & HlblMemberID.Text & ",43,'ITWeb',Year(Getdate()),getdate()," & Session("LoginID") & ",'Y')" '" & strch1 & "
                Try
                    SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                    lblerr.Text = "Inserted Successfully"
                    HlblMemberID.Text = ""
                    txtParent.Text = ""
                    loadvolunteer()
                Catch ex As Exception
                    lblerr.Text = strsql
                    lblerr.Text = lblerr.Text & ex.ToString()
                End Try
            Else
                lblerr.Text = " You cannot assign new roles to Roles 1, 2, 3, 4, 5, 43 or 90.  This volunteer already has one of these."
            End If
        Else
            lblerr.Text = "Please select The volunteer to be assinged using Search Button"
        End If
    End Sub
    Protected Sub btnassignwebadmin_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not HlblMemberID.Text = "" Then
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid in(1,2,3,4,5,90,43) and memberid=" & HlblMemberID.Text & "") < 1 Then
                Dim strsql As String = "Insert into volunteer(MemberID, RoleId, RoleCode, EventYear, CreateDate, CreatedBy,[national]) values ("
                strsql = strsql & HlblMemberID.Text & ",90,'WebAdmin',Year(Getdate()),getdate()," & Session("LoginID") & ",'Y')"
                Try
                    SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                    lblerr.Text = "Inserted Successfully"
                    HlblMemberID.Text = ""
                    txtParent.Text = ""
                    loadvolunteer()
                Catch ex As Exception
                    lblerr.Text = strsql
                    lblerr.Text = lblerr.Text & ex.ToString()
                End Try
            Else
                lblerr.Text = " You cannot assign new roles to Roles 1, 2, 3, 4, 5, 43 or 90.  This volunteer already has one of these."
            End If
        Else
            lblerr.Text = "Please select The volunteer to be assinged using Search Button"
        End If
    End Sub

    Private Sub loadvolunteer()
        Dim roleIds As String = "43"
        Dim Chcndtn As String = " "
        'If Session("RoleId").ToString() = "5" Then
        '    Chcndtn = " WHERE V.ChapterID in (select ChapterID from Chapter where ClusterId = (select ClusterId from Chapter where ChapterID =" & Session("selChapterID") & ")) "
        '
        If (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "4") Then
            ' search in Cluster  for role 4,5
            Chcndtn = " WHERE I.ChapterID in (select ChapterID from Chapter where ClusterId = (select ClusterId from Chapter where ChapterID =" & Session("selChapterID") & ")) "
        ElseIf (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Then
            ' search in Zone  for role 2,3
            Chcndtn = " WHERE I.ChapterID in (select ChapterID from Chapter where ZoneId = (select ZoneID from Chapter where ChapterID =" & Session("selChapterID") & ")) "
        ElseIf Session("RoleId").ToString() = "1" Then
            roleIds = "43,90"
        Else
            Session("OnlyCluster") = " "
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct V.volunteerID,V.RoleCode,I.FirstName + ' ' + I.LastName as VName,I.City,I.State,I.Email,I.HPhone,I.CPhone,dbo.ufn_getChapterCode(I.ChapterID) as ChapterCode,CASE WHEN VolDoc.VolDocAccessID IS NULL THEN 'True' Else 'False' END as Status from Volunteer V inner join IndSpouse I ON V.MemberID = I.AutoMemberID and V.RoleId in (" & roleIds & ") Left JOIN VolDocAccess VolDoc ON  VolDoc.MemberID = V.MemberId  " & Chcndtn & "")
        Dim dt As DataTable = ds.Tables(0)
        If ds.Tables(0).Rows.Count > 0 Then
            DGVolunteer.DataSource = dt
            DGVolunteer.DataBind()
        Else
            lblerr.Text = "No volunteer found"
        End If
    End Sub
    'Private Sub DGVolunteer_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGVolunteer.ItemCreated
    '    '    ferdine
    '    If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
    '        Dim btn As LinkButton = Nothing
    '        btn = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
    '        btn.Attributes.Add("onclick", "return confirm('Deleting the volunteer assignment role here will also remove all accesses for " & e.Item.DataItem("Vname") & ". Do you still want to delete the volunteer assignment role?');")
    '        'btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
    '    End If
    'End Sub

    Protected Sub DGVolunteer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGVolunteer.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Deleting the volunteer assignment role here will also remove all accesses for " & e.Item.DataItem("Vname") & ". Do you still want to delete the volunteer assignment role?');")
            'btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Protected Sub DGVolunteer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim volunteerID As Integer = CInt(e.Item.Cells(1).Text)
            Try
                'Delete from Volunteer Where volunteerID=" & volunteerID & "")
                Dim MemberID As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MemberId from Volunteer where VolunteerID =" & volunteerID & "")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DelVolDocAccess (VolDocAccessID, MemberID, FolderListID, [Level], AccessType, IPAddress, Createdate, CreatedBy) SELECT  VolDocAccessID, MemberID, FolderListID, [Level], AccessType, IPAddress, GetDate()," & Session("LoginID") & " FROM VolDocAccess WHERE MemberID = " & MemberID & "; DELETE FROM VolDocAccess WHERE MemberID =" & MemberID & "")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO DeletedVolunteer (MemberID, RoleId, RoleCode, TeamLead, EventYear, EventId, EventCode, ChapterId, ChapterCode,  [National], ZoneId, ZoneCode, Finals, ClusterID, ClusterCode, IndiaChapter, ProductGroupID, ProductGroupCode, ProductID,  ProductCode, IndiaChapterName, AgentFlag, WriteAccess, [Authorization], Yahoogroup, CreateDate, VolunteerID,CreatedBy) SELECT     MemberID, RoleId, RoleCode, TeamLead, EventYear, EventId, EventCode, ChapterId, ChapterCode, [National], ZoneId, ZoneCode, Finals, ClusterID, ClusterCode, IndiaChapter, ProductGroupID, ProductGroupCode, ProductID, ProductCode, IndiaChapterName, AgentFlag, WriteAccess, [Authorization], Yahoogroup, GETDATE(), VolunteerID," & Session("LoginID") & " FROM Volunteer WHERE VolunteerID = " & CStr(volunteerID) & "; DELETE FROM volunteer WHERE VolunteerID =" & volunteerID & "")
                loadvolunteer()
            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        End If
    End Sub

   
End Class
