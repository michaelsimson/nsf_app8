﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Drawing;

public partial class RemoveFromSuppressionList : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    

    #region <<<query section>>>

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
       // Session["LoginID"] = 20586;
        Session["lblAbuse"] = string.Empty;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            loadGridData();  
        }
      

        
    }
    protected void loadGridData()
    {
        try
        {

            //string strquery = "select I.FirstName,I.LastName,I.Email,I.NewsLetter,fc.ComplaintType,convert(varchar(20),fc.Date,101) as Date from Indspouse i inner join FailComplaint fc on i.Email=fc.MailTo where i.AutoMemberid='" + Session["LoginID"] + "' and fc.ComplaintType='Abuse'";
            string strquery = "select I.FirstName,I.LastName,I.Email,I.NewsLetter,fc.complainttype,convert(varchar(20),fc.Date,101) as Date from Indspouse i left join failcomplaint fc on fc.mailto = i.email and fc.complainttype='Abuse' where i.AutoMemberID='" + Session["LoginID"] + "' or i.Relationship='" + Session["LoginID"] + "'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
            gvEmailRemovalList.DataSource = ds;
            gvEmailRemovalList.DataBind();
            gvView.Visible = true;

          
        }
        catch (Exception ex)
        { }
    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //string data = "";
        int i=0;
        foreach (GridViewRow row in gvEmailRemovalList.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkRow = (CheckBox)(row.Cells[5].FindControl("ckallowRemove") as CheckBox);
            
                if (chkRow.Checked)
                {
                    string stemailid = row.Cells[2].Text;
                    //Response.Write("select email" + stemailid);
                    string strupdateqry = "update indspouse set ReqRem='Y',ReqRemDate=getDate(),newsletter=null where Email='" + stemailid + "'";
                    i = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strupdateqry);
                        
                }
              
            }
        }
        if(i>0)
        {
            lblAbuseAlert.Visible=true;
            lblAbuseAlert.ForeColor=Color.Blue;
            lblAbuseAlert.Text="Updated Successfully.  Your request will be submitted to the NSF email server.  You will get an email asking to confirm your request. </br> Please confirm. Otherwise you will not get any emails from NSF until you confirm.";

        }
      
    }
    protected void gvEmailRemovalList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridViewRow gvr = e.Row; 
        Label lblComplaintType = (Label)(gvr.FindControl("ComplaintType") as Label);
        Label lblNewsLetterFlag = (Label)(gvr.FindControl("NewsLetter") as Label);
        CheckBox ckbox = (CheckBox)(gvr.FindControl("ckallowRemove") as CheckBox);
        if (lblComplaintType != null)
        {

            if ((lblComplaintType.Text.ToString()).Equals("abuse"))
            {
                Session["lblAbuse"] = "Y";
                ckbox.Enabled = true;
                btnSubmit.Enabled = true;
            }
            else
            {
                lblComplaintType.Text = "No Abuse";
           
                ckbox.Enabled = false;
                
            }
        }
        if (lblNewsLetterFlag!= null)
        {
            if (lblNewsLetterFlag.Text.Equals("3"))
            {
                lblNewsLetterFlag.Text = "No Email";
            }
        }

        if (Session["lblAbuse"] != null)
        {
            string lblAbuse = Session["lblAbuse"].ToString();
            if (lblAbuse.Equals("Y"))
            {
                lblAbuseAlert.ForeColor = Color.Red;
                lblAbuseAlert.Visible = true;
                lblAbuseAlert.Text = "Warning:  All NSF emails to you are being blocked since you reported Abuse.  You will continue to miss them until you make a request to remove.";
            }

        }

    }
}