﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using NativeExcel;
using Microsoft.ApplicationBlocks.Data;


public partial class ReferralProgram : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatus.Text = "";
        spnStatus.InnerText = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {

        }

    }

    public void populateReferrals()
    {
        try
        {
            if (validateReferral() == 1)
            {
                string cmdText = string.Empty;
                int coachingCount = 0;
                int contestCount = 0;
                int onsiteWCount = 0;
                int onlineWCount = 0;
                int prepClubCount = 0;
                int gameCount = 0;
                int autoMemberID = 0;

                string memberID = string.Empty;
                string rMemberID = string.Empty;
                int refType = 1;

                string strcoachingCount = string.Empty;
                string strcontestCount = string.Empty;
                string stronsiteWCount = string.Empty;
                string stronlineWCount = string.Empty;
                string strprepClubCount = string.Empty;
                string strgameCount = string.Empty;
                string strAutoMemberID = string.Empty;

                int newCount = 0;
                bool isNew = true;

                DataSet ds = null;

                cmdText = "select  case when DonorType='IND' then AutoMemberID else Relationship end as AutoMemberID,FirstName, LastName, Email, Chapter, State, City,AutoMemberID as MemberID from Indspouse where Email='" + txtEmail.Text + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (DDLSelect.SelectedValue == "1")
                        {
                            cmdText = "select count(*) from Indspouse where Email='" + txtEmail.Text + "' and CreateDate>DATEADD(month, -12, Convert(varchar, getdate(), 106))";

                            newCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                            if (newCount > 0)
                            {
                                isNew = true;

                                refType = 1;
                                spnTable2Title.InnerText = "Table 1:" + DDLSelect.SelectedItem.Text;
                                autoMemberID = Convert.ToInt32(ds.Tables[0].Rows[0]["AutoMemberID"].ToString());
                                strAutoMemberID = ds.Tables[0].Rows[0]["MemberID"].ToString();
                                rMemberID = strAutoMemberID.ToString();
                                memberID = Session["LoginID"].ToString();
                                cmdText = "Select Count(*) from Contestant where ParentID=" + autoMemberID + " and PaymentReference is not null";
                                contestCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from CoachReg where PMemberID=" + autoMemberID + " and PaymentReference is not null";
                                coachingCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Registration where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                onsiteWCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Registration_OnlineWkshop where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                onlineWCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Registration_PrepClub where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                prepClubCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Game where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                gameCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                            }
                            else
                            {
                                isNew = false;
                            }

                        }
                        else if (DDLSelect.SelectedValue == "2")
                        {
                            autoMemberID = Convert.ToInt32(Session["CustIndID"].ToString());
                            cmdText = "select count(*) from Indspouse where AutoMemberID=" + autoMemberID + " and CreateDate>DATEADD(month, -12, Convert(varchar, getdate(), 106))";
                            strAutoMemberID = Session["LoginID"].ToString();

                            newCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                            if (newCount > 0)
                            {
                                isNew = true;


                                refType = 2;
                                spnTable2Title.InnerText = "Table 2:" + DDLSelect.SelectedItem.Text;


                                memberID = ds.Tables[0].Rows[0]["MemberID"].ToString();
                                rMemberID = strAutoMemberID.ToString();
                                cmdText = "Select Count(*) from Contestant where ParentID=" + autoMemberID + " and PaymentReference is not null";
                                contestCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from CoachReg where PMemberID=" + autoMemberID + " and PaymentReference is not null";
                                coachingCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Registration where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                onsiteWCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Registration_OnlineWkshop where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                onlineWCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Registration_PrepClub where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                prepClubCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                cmdText = "Select Count(*) from Game where MemberID=" + autoMemberID + " and PaymentReference is not null";
                                gameCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                            }
                            else
                            {
                                isNew = false;
                            }
                        }

                        if ((contestCount > 0 || coachingCount > 0 || onlineWCount > 0 || onsiteWCount > 0 || prepClubCount > 0 || gameCount > 0) && isNew == true)
                        {
                            if (contestCount == 0)
                            {
                                strcontestCount = "null";
                            }
                            else
                            {
                                strcontestCount = contestCount.ToString();
                            }

                            if (coachingCount == 0)
                            {
                                strcoachingCount = "null";
                            }
                            else
                            {
                                strcoachingCount = coachingCount.ToString();
                            }

                            if (onsiteWCount == 0)
                            {
                                stronsiteWCount = "null";
                            }
                            else
                            {
                                stronsiteWCount = onsiteWCount.ToString();
                            }

                            if (onlineWCount == 0)
                            {
                                stronlineWCount = "null";
                            }
                            else
                            {
                                stronlineWCount = onlineWCount.ToString();
                            }
                            if (prepClubCount == 0)
                            {
                                strprepClubCount = "null";
                            }
                            else
                            {
                                strprepClubCount = prepClubCount.ToString();
                            }
                            if (gameCount == 0)
                            {
                                strgameCount = "null";
                            }
                            else
                            {
                                strgameCount = gameCount.ToString();
                            }


                            string year = DateTime.Now.Year.ToString();

                            if (refType == 1)
                            {
                                int isDup = 0;
                                cmdText = "select count(*) from ReferralProg where RefType=" + refType + " and MemberID=" + memberID + " and  RmemberID=" + rMemberID + "";
                                try
                                {
                                    isDup = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                }
                                catch
                                {
                                }
                                if (isDup == 0)
                                {
                                    cmdText = "Insert into ReferralProg(MemberID, RmemberID, RefType, Year, CreateDate, CreatedBy,Contests,Coaching,OnsiteW,OnlineW,PrepClub,Game) values(" + memberID + "," + rMemberID + "," + refType + "," + year + ",GetDate()," + Session["LoginID"].ToString() + "," + strcontestCount + "," + strcoachingCount + "," + stronsiteWCount + "," + stronlineWCount + "," + strprepClubCount + "," + strgameCount + ")";
                                }
                            }
                            else if (refType == 2)
                            {
                                int isDup = 0;
                                cmdText = "select count(*) from ReferralProg where RefType=" + refType + " and MemberID=" + rMemberID + " and  RmemberID=" + memberID + "";
                                try
                                {
                                    isDup = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                                }
                                catch
                                {
                                }
                                if (isDup == 0)
                                {
                                    cmdText = "Insert into ReferralProg(MemberID, RmemberID, RefType, Year, CreateDate, CreatedBy,Contests,Coaching,OnsiteW,OnlineW,PrepClub,Game) values(" + rMemberID + "," + memberID + "," + refType + "," + year + ",GetDate()," + Session["LoginID"].ToString() + "," + strcontestCount + "," + strcoachingCount + "," + stronsiteWCount + "," + stronlineWCount + "," + strprepClubCount + "," + strgameCount + ")";
                                }
                            }

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                            populateReferralsGrid();
                        }
                        else
                        {
                            if (refType == 1)
                            {
                                spnStatus.InnerText = "This parent has not registered and paid for any NSF activity.  Please contact the parent.";
                            }
                            else if (refType == 2)
                            {
                                spnStatus.InnerText = "You have not registered and paid for any NSF activity.  So you cannot select the person at this time.";
                            }
                        }


                    }
                    else
                    {
                        spnStatus.InnerText = "This email address is not in the NSF database.  Please try again.";
                    }
                }
                else
                {
                    spnStatus.Visible = true;
                }
            }
        }
        catch
        {

        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        populateReferrals();
    }

    protected void grdReferrals_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "DeleteR")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                grdReferrals.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string refProgID = ((Label)grdReferrals.Rows[selIndex].FindControl("lblRefProgID") as Label).Text;
                hdnRefProgID.Value = refProgID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmDelete();", true);
            }
        }
        catch
        {

        }
    }

    public int validateReferral()
    {
        int retVal = 1;
        if (txtEmail.Text == "")
        {
            retVal = -1;
            lblStatus.Text = "Please enter email address.";
        }
        return retVal;
    }

    public void populateReferralsGrid()
    {
        try
        {
            DataSet ds = null;
            string cmdText = string.Empty;
            if (DDLSelect.SelectedValue == "1")
            {
                cmdText = "select  AutoMemberID,FirstName, LastName, Email, Chapter, State, City,IP.CreateDate,RP.RefProgID from ReferralProg RP inner join  Indspouse IP on(RP.RmemberID=IP.AutoMemberID) where RP.RefType=1 and RP.memberID=" + Session["LoginID"].ToString() + "";
            }
            else if (DDLSelect.SelectedValue == "2")
            {
                cmdText = "select  AutoMemberID,FirstName, LastName, Email, Chapter, State, City,IP.CreateDate,RP.RefProgID from ReferralProg RP inner join  Indspouse IP on(RP.RMemberID=IP.AutoMemberID) where RP.RefType=2 and RP.MemberID=" + Session["LoginID"].ToString() + "";
            }

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdReferrals.DataSource = ds;
                    grdReferrals.DataBind();
                }
                else
                {
                    grdReferrals.DataSource = ds;
                    grdReferrals.DataBind();
                }
            }

        }
        catch
        {
        }
    }

    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        try
        {
            string cmdText = string.Empty;

            cmdText = "delete from ReferralProg where RefProgID=" + hdnRefProgID.Value + "";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            lblStatus.Text = "Deleted Successfully.";

            populateReferralsGrid();
        }
        catch
        {
        }
    }
}