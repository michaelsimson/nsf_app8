﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SetUpTeams : System.Web.UI.Page
{

    string ConnectionString = "ConnectionString";
    string FirstandLastName;
    bool IsValueCheck = false;
    bool IsEventProd = false;
    bool IsEvent = false;
    string productgroupid1;
    string productid1;
    string productGroup1;
    string product1;
    bool Isdropval = false;
    DataSet dsCheck;
    string StrQryCheck;
    bool Isupdate = false;
    string RoleIdColumnName = "RoleId";
    string LoginID = "LoginID";
    string Panel = "Panel";
    string ColChapterID = "ChapterID";
    string ColTeamLead = "TeamLead";
    string ColTeamID = "TeamID";
    string ColMemberID = "MemberID";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatusMsg.Text = string.Empty;
        lbacces.Text = string.Empty;
        lblVolSignUpText.Text = string.Empty;
        if (Session[LoginID] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Session[Panel] != null)
        {
            if (Session[Panel].ToString().Equals("ChapterPanel"))
            {
                if (Array.IndexOf(new string[] { "1", "2", "3", "4", "5" }, Session[RoleIdColumnName].ToString()) > -1)
                //if ((Session[RoleIdColumnName].ToString() == "1") || (Session[RoleIdColumnName].ToString() == "2") || (Session[RoleIdColumnName].ToString() == "3") || (Session[RoleIdColumnName].ToString() == "4") || (Session[RoleIdColumnName].ToString() == "5"))
                {
                    if (!IsPostBack)
                    {
                        Events();
                        Yearscount();
                        ChapterDrop(ddchapter);

                    }
                }
                else
                {
                    lbacces.Text = "You do not have access to this application";
                    IDContent.Visible = false;
                }
            }
        }
        else if (Array.IndexOf(new string[] { "1", "2", "97", "5" }, Session[RoleIdColumnName].ToString()) > -1)
        {
            if (!IsPostBack)
            {
                Events();
                Yearscount();
                ChapterDrop(ddchapter);
                ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
            }
        }
        else if (Convert.ToInt32(Session[RoleIdColumnName]) > 5)
        {
            DataSet dsRole = new DataSet();
            string cmdText = string.Empty;
            string MemberID = Session[LoginID].ToString();
            string ChapterID = string.Empty;
            string TeamLead = string.Empty;
            string TeamID = string.Empty;


            cmdText = "Select ChapterID, TeamLead from Volunteer where memberID=" + MemberID + " and RoleID=" + Session["RoleID"].ToString() + "";
            dsRole = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            if (dsRole.Tables[0].Rows.Count > 0)
            {
                if (dsRole.Tables[0].Rows[0][ColChapterID] != null && dsRole.Tables[0].Rows[0][ColChapterID].ToString() != string.Empty)
                {
                    ChapterID = dsRole.Tables[0].Rows[0][ColChapterID].ToString();
                }
                if (dsRole.Tables[0].Rows[0][ColTeamLead] != null && dsRole.Tables[0].Rows[0][ColTeamLead].ToString() != string.Empty)
                {
                    TeamLead = dsRole.Tables[0].Rows[0][ColTeamLead].ToString();
                }
            }

            string cmdText1 = "select TeamID from Role where RoleID=" + Session[RoleIdColumnName].ToString() + "";

            DataSet ds1 = new DataSet();
            ds1 = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText1);
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (ds1.Tables[0].Rows[0][ColTeamID] != null && ds1.Tables[0].Rows[0][ColTeamID].ToString() != string.Empty)
                {
                    TeamID = ds1.Tables[0].Rows[0][ColTeamID].ToString();
                }
            }
            if (TeamID == string.Empty || TeamLead == "N")
            {
                lbacces.Text = "Access is not available to your role selected";
                lbacces.ForeColor = Color.Red;
                IDContent.Visible = false;
            }
            else
            {
                if (!IsPostBack)
                {
                    IDContent.Visible = true;
                    //Events();
                    Yearscount();
                    //ChapterDrop(ddchapter);

                    //string MemberID = Session["LoginID"].ToString();
                    //string cmdText = "";
                    DataSet ds = new DataSet();
                    //cmdText = "select * from VolTeamMatrix where TeamID=" + TeamID + "";

                    cmdText = "select distinct E.EventID,E.EventCode from Volunteer V inner join Event E on (V.EventID=E.EventID) where MemberID=" + MemberID + " and RoleID=" + Session["RoleID"].ToString() + " order by E.EventID Asc";


                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    ddEvent.DataSource = ds;
                    ddEvent.DataTextField = "EventCode";
                    ddEvent.DataValueField = "EventID";
                    ddEvent.DataBind();

                    ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 1)
                        {
                            ddEvent.Enabled = true;
                        }
                        else
                        {
                            ddEvent.Enabled = false;
                            ddEvent.SelectedIndex = 1;
                        }
                    }

                    cmdText = "select distinct E.ChapterID,E.ChapterCode from Volunteer V inner join Chapter E on (V.ChapterID=E.ChapterID) where MemberID=" + MemberID + " and RoleID=" + Session["RoleID"].ToString() + " order by E.ChapterID Asc";
                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    ddchapter.DataSource = ds;
                    ddchapter.DataTextField = "ChapterCode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();

                    ddchapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 1)
                        {
                            ddchapter.Enabled = true;
                        }
                        else
                        {
                            ddchapter.Enabled = false;
                            ddchapter.SelectedIndex = 1;
                        }
                    }


                    ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                }
            }
        }

        else
        {
            lbacces.Text = "You do not have access to this application";
            IDContent.Visible = false;
        }

    }

    protected void Events()
    {
        try
        {

            if (Session[Panel] != null)
            {
                if (Session[Panel].ToString().Equals("ChapterPanel"))
                {
                    if (Convert.ToInt32(Session["mailChapterID"].ToString()) > 1)
                    {
                        ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                        ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                        ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                    }
                    if (Convert.ToInt32(Session["mailChapterID"].ToString()) == 1)
                    {
                        ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                        ddEvent.SelectedIndex = 0;
                        ddEvent.Enabled = false;
                    }
                }
            }
            else
            {
                if (Array.IndexOf(new string[] { "1", "2", "3", "4", "5", "97" }, Session[RoleIdColumnName].ToString()) > -1)
                {
                    ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                    ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                    ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                    ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                    ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                    ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                    ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                    ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                }
                else if (Convert.ToInt32(Session[RoleIdColumnName].ToString()) > 5)
                {
                    string MemberID = Session["LoginID"].ToString();
                    string cmdText = "";
                    DataSet ds = new DataSet();

                    cmdText = "select distinct E.EventID,E.EventCode from Volunteer V inner join Event E on (V.EventID=E.EventID) where MemberID=" + MemberID + " and RoleID=" + Session[RoleIdColumnName].ToString() + " order by E.EventID Asc";
                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    ddEvent.DataSource = ds;
                    ddEvent.DataTextField = "EventCode";
                    ddEvent.DataValueField = "EventID";
                    ddEvent.DataBind();

                    ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 1)
                        {
                            ddEvent.Enabled = true;
                        }
                        else
                        {
                            ddEvent.Enabled = false;
                            ddEvent.SelectedIndex = 1;
                        }
                    }
                }
            }
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Team()
    {

        if (ddEvent.SelectedItem.Text != "All")
        {
            string eventText = ddEvent.SelectedItem.Text;
            if (ddEvent.SelectedItem.Text == "Chapter")
            {
                eventText = "ChapterContests";
            }
            StrQryCheck = string.Format("select * from  VolTeamMatrix where {0}='Y'", eventText);
        }
        else
        {
            StrQryCheck = "select * from  VolTeamMatrix";
        }


        DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQryCheck);
        if (dsCheck.Tables[0].Rows.Count > 0)
        {
            Isdropval = true;
            ddTeams.DataSource = dsCheck;
            ddTeams.DataTextField = "TeamName";
            ddTeams.DataValueField = ColTeamID;
            ddTeams.DataBind();
            ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
        }
        else
        {

        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = MaxYear; i <= DateTime.Now.Year + 1; i++)
            {
                if (i != DateTime.Now.Year)
                {
                    list.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            for (int i = MaxYear; i >= DateTime.Now.Year - 2; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));

            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();


            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));

        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {

        if (Convert.ToInt32(Session[RoleIdColumnName].ToString()) == 5)
        {
            string selectedchapterid = Session["mailChapterID"].ToString();
            string ddChapterstr = string.Format("select distinct ChapterID,chaptercode,[state] from chapter where {0}={1}", "ChapterID", selectedchapterid);
            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = ColChapterID;
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
        }
        else if (Convert.ToInt32(Session[RoleIdColumnName].ToString()) == 3)
        {
            string selectedchapterid = Session["mailChapterID"].ToString();
            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ZoneID in (select ZoneID from Chapter where ChapterID=" + selectedchapterid + ")";
            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = ColChapterID;
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
        }//
        else if (Convert.ToInt32(Session[RoleIdColumnName].ToString()) == 4)
        {
            string selectedchapterid = Session["mailChapterID"].ToString();
            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where ClusterId in (select ClusterId from Chapter where ChapterID=" + selectedchapterid + ")";
            DataSet dschapterselected = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapterselected.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapterselected;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = ColChapterID;
                ddChapter.DataBind();
                if (dschapterselected.Tables[0].Rows.Count == 1)
                {
                    ddChapter.SelectedIndex = 0;
                    ddChapter.Enabled = false;
                }
            }
        }
        else if (Convert.ToInt32(Session[RoleIdColumnName].ToString()) == 1 || Convert.ToInt32(Session[RoleIdColumnName].ToString()) == 2)
        {
            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();

                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }
        }
        else if (Convert.ToInt32(Session[RoleIdColumnName].ToString()) > 5)
        {
            string cmdText = "select distinct E.ChapterID,E.ChapterCode from Volunteer V inner join Chapter E on (V.ChapterID=E.ChapterID) where MemberID=" + Session["LoginID"].ToString() + " and RoleID=" + Session["RoleID"].ToString() + " order by E.ChapterID Asc";
            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            ddchapter.DataSource = ds;
            ddchapter.DataTextField = "ChapterCode";
            ddchapter.DataValueField = "ChapterID";
            ddchapter.DataBind();

            ddchapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddchapter.Enabled = true;
                }
                else
                {
                    ddchapter.Enabled = false;
                    ddchapter.SelectedIndex = 1;
                }
            }
        }
        else if (Array.IndexOf(new string[] { "19", "2", "3" }, ddEvent.SelectedValue) > -1)
        {

            string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = ColChapterID;
                ddChapter.DataBind();

                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }

        }

        else
        {
            //string cmdText = "select distinct E.ChapterID,E.ChapterCode from Volunteer V inner join Chapter E on (V.ChapterID=E.ChapterID) where MemberID=" + Session["LoginID"].ToString() + " and RoleID=" + Session["RoleID"].ToString() + " order by E.ChapterID Asc";
            //DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            //ddchapter.DataSource = ds;
            //ddchapter.DataTextField = "ChapterCode";
            //ddchapter.DataValueField = "ChapterID";
            //ddchapter.DataBind();

            //ddchapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            //if (ds.Tables[0] != null)
            //{
            //    if (ds.Tables[0].Rows.Count > 1)
            //    {
            //        ddchapter.Enabled = true;
            //    }
            //    else
            //    {
            //        ddchapter.Enabled = false;
            //        ddchapter.SelectedIndex = 1;
            //    }
            //}
            ddchapter.Items.Clear();
            ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            ddChapter.Enabled = false;
        }
    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedItem.Text != "Select Event")
        {
            Team();
        }
        //ChapterDrop(ddchapter);
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
            ddchapter.Enabled = false;
        }
        else
        {
            ChapterDrop(ddchapter);
        }

    }
    protected void GridDisply()
    {
        try
        {
            string StrQrySearch = " select I.State,I.City,Vs.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.VolsignupId,Vs.TeamId,VT.TeamName,Year,Vs.EventID,PG.Name as ProductGroup,"
             + "  P.Name as Product,Vs.AvailHours,Vs.ProductGroupId,Vs.ProductId,Vs.Eventname,C.Name as ChapterName from "
             + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
             + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null  " + genWhereConditons() + " "
             + " order by memberid , year desc,eventid asc,TeamName";

            DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);

            if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
            {
                btnExcel.Visible = true;
                GvVolsignup.Visible = false;
                GvVolsignup.DataSource = null;
                GvVolsignup.DataBind();
                lblVolSignUpText.Text = "No macth found";
                lblVolSignUpText.ForeColor = Color.Red;
                // btnExcel.Visible = false;
            }
            else
            {
                btnExcel.Visible = true;
                lblVolSignUpText.Text = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string secMemberIDs = string.Empty;
                    string memberIDs = string.Empty;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr[ColMemberID] != null && dr[ColMemberID].ToString() != string.Empty)
                        {
                            if (secMemberIDs != dr[ColMemberID].ToString())
                            {
                                memberIDs += dr[ColMemberID].ToString() + ",";
                            }
                            secMemberIDs = dr[ColMemberID].ToString();
                        }

                    }
                    string iCondtions = string.Empty;
                    if ((ddYear.SelectedItem.Text != "Select Year") && (ddYear.SelectedItem.Text != "All"))
                    {
                        iCondtions += " and  Vs.year=" + ddYear.SelectedValue;
                    }
                    if ((ddEvent.SelectedItem.Text != "Select Event") && (ddEvent.SelectedItem.Text != "All"))
                    {
                        iCondtions += " and Vs.EventID=" + ddEvent.SelectedValue;
                    }

                    if ((ddchapter.SelectedItem.Text != "Select Chapter") && (ddchapter.SelectedItem.Text != "All"))
                    {
                        iCondtions += " and Vs.ChapterId=" + ddchapter.SelectedValue;
                    }

                    if (memberIDs != string.Empty)
                    {
                        memberIDs = memberIDs.Remove(memberIDs.Length - 1);
                        hdnMemberIDs.Value = memberIDs;
                        StrQrySearch = " select I.State,I.City,Vs.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.VolsignupId,Vs.TeamId,VT.TeamName,Year,Vs.EventID,PG.Name as ProductGroup,"
           + "  P.Name as Product,Vs.AvailHours,Vs.ProductGroupId,Vs.ProductId,Vs.Eventname,C.Name as ChapterName from "
           + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
           + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null " + iCondtions + " and Vs.MemberID in (" + memberIDs + ")  order by memberid , year desc,eventid asc,TeamName";


                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
                    }
                    else
                    {
                        hdnMemberIDs.Value = "0";
                    }
                }
                else
                {
                    hdnMemberIDs.Value = "0";
                }
                GvVolsignup.Visible = true;
                GvVolsignup.DataSource = ds;
                GvVolsignup.DataBind();

                btnExcel.Visible = true;

                if (Array.IndexOf(new string[] { "1", "2", "5" }, Session[RoleIdColumnName].ToString()) > -1)
                {
                    for (int i = 0; i < GvVolsignup.Rows.Count; i++)
                    {

                        ((Button)GvVolsignup.Rows[i].FindControl("btnEdit") as Button).Visible = true;
                    }
                }
                else if (Convert.ToInt32(Session[RoleIdColumnName]) > 5)
                {
                    DataSet dsRole = new DataSet();
                    string cmdText = string.Empty;
                    string MemberID = Session[LoginID].ToString();
                    string ChapterID = string.Empty;
                    string TeamLead = string.Empty;
                    string TeamID = string.Empty;
                    string chapterID = ddchapter.SelectedValue;
                    if (chapterID == string.Empty)
                    {

                        string.Format("Select ChapterID, TeamLead from Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7}", "memberID", "EventID", "TeamID", "EventYear", MemberID, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue);
                    }
                    else
                    {
                        //cmdText = "Select ChapterID, TeamLead from Volunteer where memberID=" + MemberID + " and EventID=" + ddEvent.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and ChapterID=" + ddchapter.SelectedValue + "";
                        string.Format("Select ChapterID, TeamLead from Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7}", "memberID", "EventID", "TeamID", "EventYear", MemberID, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue);
                    }
                    dsRole = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    if (dsRole.Tables[0].Rows.Count > 0)
                    {

                        if (dsRole.Tables[0].Rows[0][ColTeamLead] != null && dsRole.Tables[0].Rows[0][ColTeamLead].ToString() != string.Empty)
                        {
                            TeamLead = dsRole.Tables[0].Rows[0][ColTeamLead].ToString();
                        }
                    }
                    if (TeamLead == "Y")
                    {
                        for (int i = 0; i < GvVolsignup.Rows.Count; i++)
                        {

                            ((Button)GvVolsignup.Rows[i].FindControl("btnEdit") as Button).Visible = true;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < GvVolsignup.Rows.Count; i++)
                    {

                        ((Button)GvVolsignup.Rows[i].FindControl("btnEdit") as Button).Enabled = false;
                    }
                }


                if (Array.IndexOf(new string[] { "1", "2", "5" }, Session[RoleIdColumnName].ToString()) > -1)
                {
                    for (int i = 0; i < GvVolsignup.Rows.Count; i++)
                    {

                        ((Button)GvVolsignup.Rows[i].FindControl("btnUpdateTeamLead") as Button).Visible = true;
                        ((DropDownList)GvVolsignup.Rows[i].FindControl("ddlTeamLeadVol") as DropDownList).Enabled = true;

                    }
                }
                else
                {
                    for (int i = 0; i < GvVolsignup.Rows.Count; i++)
                    {

                        ((Button)GvVolsignup.Rows[i].FindControl("btnUpdateTeamLead") as Button).Enabled = false;
                        ((DropDownList)GvVolsignup.Rows[i].FindControl("ddlTeamLeadVol") as DropDownList).Enabled = false;
                    }
                }

                string teamname = ddTeams.SelectedItem.Text;
                for (int i = 0; i < GvVolsignup.Rows.Count; i++)
                {
                    if (((Label)GvVolsignup.Rows[i].FindControl("lblTeamname") as Label).Text == teamname)
                    {
                        if (Convert.ToInt32(Session[RoleIdColumnName]) <= 5)
                        {
                            ((Button)GvVolsignup.Rows[i].FindControl("btnUpdateTeamLead") as Button).Enabled = true;
                            ((DropDownList)GvVolsignup.Rows[i].FindControl("ddlTeamLeadVol") as DropDownList).Enabled = true;
                        }
                        else
                        {
                            ((Button)GvVolsignup.Rows[i].FindControl("btnUpdateTeamLead") as Button).Enabled = false;
                            ((DropDownList)GvVolsignup.Rows[i].FindControl("ddlTeamLeadVol") as DropDownList).Enabled = false;
                        }
                        ((Button)GvVolsignup.Rows[i].FindControl("btnEdit") as Button).Enabled = true;
                    }
                    else
                    {
                        ((Button)GvVolsignup.Rows[i].FindControl("btnUpdateTeamLead") as Button).Enabled = false;
                        ((DropDownList)GvVolsignup.Rows[i].FindControl("ddlTeamLeadVol") as DropDownList).Enabled = false;
                        ((Button)GvVolsignup.Rows[i].FindControl("btnEdit") as Button).Enabled = false;
                    }
                }
            }

        }
        catch (Exception EX)
        {
          //  throw new Exception(EX.Message);
           // Response.Write(EX.ToString());
        }

    }
    protected string genWhereConditons()
    {

        string iCondtions = string.Empty;
        if ((ddYear.SelectedItem.Text != "Select Year") && (ddYear.SelectedItem.Text != "All"))
        {
            iCondtions += " and  Vs.year=" + ddYear.SelectedValue;
        }
        if ((ddEvent.SelectedItem.Text != "Select Event") && (ddEvent.SelectedItem.Text != "All"))
        {
            iCondtions += " and Vs.EventID=" + ddEvent.SelectedValue;
        }

        if ((ddchapter.SelectedItem.Text != "Select Chapter") && (ddchapter.SelectedItem.Text != "All"))
        {
            iCondtions += " and Vs.ChapterId=" + ddchapter.SelectedValue;
        }
        if (ddTeams.Items.Count > 0)
        {
            if ((ddTeams.SelectedItem.Text != "Select Team") && (ddTeams.SelectedItem.Text != "All"))
            {
                iCondtions += " and Vs.TeamId=" + ddTeams.SelectedValue;
            }
        }
        if (Isdropval == true)
        {
            if ((ddTeams.SelectedItem.Text != "Select Team") && (ddTeams.SelectedItem.Text != "All"))
            {
                iCondtions += " and Vs.TeamId=" + ddTeams.SelectedValue;
            }
        }

        return iCondtions;

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        tblAddRole.Visible = false;
        if (rbtIndividualCheck.Checked == true)
        {
            pnlVolunteerSignUp.Visible = true;
            pIndSearch.Visible = false;
            if (validateSearch() == "1")
            {
                if (ddlRole.Items.Count == 1)
                {
                    lblStatusMsg.Text = "Role is not assigned to the Team selected";
                    lblStatusMsg.ForeColor = Color.Red;
                    ddlRole.Enabled = false;
                }
                else if (ddlRole.Items.Count == 2)
                {
                    ddlRole.SelectedIndex = 1;
                    ddlRole.Enabled = false;
                    dvTable1Title.Visible = true;
                    GridDisply();
                    SignupDetails();
                }
                else
                {
                    ddlRole.Enabled = true;
                    if (ddlRole.SelectedValue == "0")
                    {
                        lbacces.Text = "Please select Role";
                        lbacces.ForeColor = Color.Red;
                    }
                    else
                    {
                        dvTable1Title.Visible = true;
                        GridDisply();
                        SignupDetails();

                    }
                }
            }
            else
            {
                validateSearch();
            }
        }
        else if (RadioButton1.Checked == true)
        {
            if (validateSearch() == "1")
            {
                if (ddlRole.Items.Count == 1)
                {
                    lblStatusMsg.Text = "Role is not assigned to the Team selected";
                    lblStatusMsg.ForeColor = Color.Red;
                    ddlRole.Enabled = false;
                }
                else if (ddlRole.Items.Count == 2)
                {
                    ddlRole.SelectedIndex = 1;
                    ddlRole.Enabled = false;
                    dvTable1Title.Visible = true;
                    pnlVolunteerSignUp.Visible = false;
                    GetStates();
                    pIndSearch.Visible = true;
                    ddlDonorType.SelectedValue = "INDSPOUSE";
                    ddlDonorType.Enabled = false;

                    Panel4.Visible = false;
                }
                else
                {
                    ddlRole.Enabled = true;
                    if (ddlRole.SelectedValue == "0")
                    {
                        lbacces.Text = "Please select Role";
                        lbacces.ForeColor = Color.Red;
                    }
                    else
                    {
                        dvTable1Title.Visible = true;
                        pnlVolunteerSignUp.Visible = false;
                        GetStates();
                        pIndSearch.Visible = true;
                        ddlDonorType.SelectedValue = "INDSPOUSE";
                        ddlDonorType.Enabled = false;

                        Panel4.Visible = false;
                    }
                }

            }
            else
            {
                validateSearch();
            }

        }
        else
        {
            lbacces.Text = "Please check any one 'Select from Volunteer Sign Up Pool' or 'Select from NSF database (General)' ";
            lbacces.ForeColor = Color.Red;
        }

    }
    public void fillTeam()
    {

        string cmdText = string.Empty;
        DataSet ds = new DataSet();
        string teamID = ddTeams.SelectedValue;
        cmdText = string.Format("Select RoleID,RoleCode from Role where {0}={1}", "TeamID", teamID);
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlRole.Enabled = true;
            ddlRole.DataSource = ds;
            ddlRole.DataTextField = "RoleCode";
            ddlRole.DataValueField = RoleIdColumnName;
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, new ListItem("Select Role", "0"));
            if (ds.Tables[0].Rows.Count < 2)
            {
                ddlRole.SelectedIndex = 1;
                ddlRole.Enabled = false;
            }
        }
        else
        {

            ddlRole.Enabled = false;
            ddlRole.DataSource = ds;
            ddlRole.DataTextField = "RoleCode";
            ddlRole.DataValueField = RoleIdColumnName;
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, new ListItem("Select Role", "0"));

            ddlRole.SelectedValue = "0";

        }
    }
    protected void ddTeams_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillTeam();
    }
    protected void SignupDetails()
    {
        string StrQrySearchName = " select distinct FirstName+' '+ LastName as name,chapterid  from  IndSpouse where AutoMemberId=" + Session[LoginID] + "";
        DataSet dsNmae = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchName);
        string strName = dsNmae.Tables[0].Rows[0]["name"].ToString();
        ViewState["NameOfUser"] = strName;
        ViewState[ColChapterID] = dsNmae.Tables[0].Rows[0][ColChapterID].ToString();
        string StrQrySearch = " select distinct V.Memberid,V.RoleID,V.RoleCode,I.FirstName,I.LastName,I.Email,I.HPhone,I.CPhone,I.City,I.State from  volunteer V left join"
        + " IndSpouse I on I.AutomemberId=V.memberId inner join VolSignup Vs on Vs.MemberId=I.AutomemberId  where V.RoleId is not null and V.RoleCode is not null and V.MemberId in (" + hdnMemberIDs.Value + ")";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);

        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
        }
        else
        {
            FirstandLastName = ds.Tables[0].Rows[0]["FirstName"].ToString() + " " + ds.Tables[0].Rows[0]["LastName"].ToString();
        }
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {

            GridView1.Visible = false;
            btnExcel.Visible = false;
            dvTable2Title.Visible = true;
            if (btnExcel.Visible != true)
            {
                btnExcel.Visible = false;
            }
        }

        else
        {
            btnExcel.Visible = true;
            dvTable2Title.Visible = true;
            GridView1.Visible = true;
            GridView1.DataSource = ds;
            GridView1.DataBind();
            btnExcel.Visible = true;
        }
    }

    public string validateSearch()
    {
        string retval = "1";
        if (RadioButton1.Checked == false && rbtIndividualCheck.Checked == false)
        {
            retval = "-1";
            lbacces.Text = "Please check any one 'Select from Volunteer Sign Up Pool' or 'Select from NSF database (General)' ";
            lbacces.ForeColor = Color.Red;
        }
        if (rbtIndividualCheck.Checked == true || RadioButton1.Checked == true)
        {
            //SetMessage(ddYear, "Year");
            //SetMessage(ddEvent, "Year");
            if (ddYear.SelectedValue == "0")
            {
                retval = "-1";
                lbacces.Text = "Please select Year";
                lbacces.ForeColor = Color.Red;
            }
            else if (ddEvent.SelectedValue == "0")
            {
                retval = "-1";
                lbacces.Text = "Please select Event";
                lbacces.ForeColor = Color.Red;
            }
            else if (ddchapter.Enabled == true)
            {
                if (ddchapter.SelectedValue == "0")
                {
                    retval = "-1";
                    lbacces.Text = "Please select Chapter";
                    lbacces.ForeColor = Color.Red;
                }
            }
            else if (ddTeams.SelectedValue == "0")
            {
                retval = "-1";
                lbacces.Text = "Please select Team";
                lbacces.ForeColor = Color.Red;
            }

        }
        return retval;
    }
    private string SetMessage(DropDownList ddlList, string Message)
    {
        // if (ddlList.SelectedValue == "0")


        lbacces.Text = string.Format("Please select {0}", Message);
        lbacces.ForeColor = Color.Red;
        return "-1";

        //return "1";
    }
    protected void GvVolsignup_RowCommand(object sender, GridViewCommandEventArgs e)
    {


        try
        {
            GridViewRow row = null;


            if (e.CommandName == "AddRole")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                tblAddRole.Visible = true;
                if (Convert.ToInt32(Session[RoleIdColumnName]) > 5)
                {
                    ddlTeamLead.SelectedValue = "N";
                    ddlTeamLead.Enabled = false;

                }
                else
                {

                    ddlTeamLead.Enabled = true;
                }
                fillProductGroup();
                hdnMemberID.Value = ((Label)GvVolsignup.Rows[selIndex].FindControl("lblmemberId") as Label).Text;
            }
            else if (e.CommandName == "Team Lead")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                hdnMemberID.Value = ((Label)GvVolsignup.Rows[selIndex].FindControl("lblmemberId") as Label).Text;

                string TeamLead = ((DropDownList)GvVolsignup.Rows[selIndex].FindControl("ddlTeamLeadVol") as DropDownList).Text;

                string CmdText = string.Empty;
                DataSet ds = new DataSet();
                int vCount = 0;

                CmdText = string.Format("select count(*) as CountSet from  Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7} and {4}={8}", "memberID", "EventID", "TeamID", "EventYear", "RoleID", hdnMemberID.Value, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue, ddlRole.SelectedValue);

                ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        vCount = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }
                if (vCount > 0)
                {
                    CmdText = "Update Volunteer set TeamLead='" + TeamLead + "' where memberID=" + hdnMemberID.Value + " AND EventID=" + ddEvent.SelectedValue + "and RoleID=" + ddlRole.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + "";

                    SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                    lbacces.Text = "Team Lead updated successfully";
                    lbacces.ForeColor = Color.Blue;
                }
                else
                {
                    lbacces.Text = "No roles assigned for the selected Member. Please assign the roles first";
                    lbacces.ForeColor = Color.Red;
                }

            }
        }
        catch (Exception ex)
        {
        }
    }

    public void AddRole()
    {
        string MemberID = hdnMemberID.Value;

        string RoleID = ddlRole.SelectedValue;
        string RoleCode = ddlRole.SelectedItem.Text;
        string TeamLead = ddlTeamLead.SelectedValue;
        string EventYear = ddYear.SelectedValue;
        string EventCode = string.Empty;
        string cmdText = string.Empty;
        DataSet ds = new DataSet();
        cmdText = "select EventCode from Event where EventID=" + ddEvent.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["EventCode"] != null && ds.Tables[0].Rows[0]["EventCode"].ToString() != string.Empty)
                {
                    EventCode = ds.Tables[0].Rows[0]["EventCode"].ToString();
                }
            }
        }

        string chapterID = string.Empty;
        string chapterCode = string.Empty;
        string national = string.Empty;
        string ZoneID = string.Empty;
        string ZoneCode = string.Empty;
        string Finals = string.Empty;
        string ClusterID = string.Empty;
        string Cluster = string.Empty;
        string IndiaChapterID = string.Empty;
        string IndiaChaptername = string.Empty;
        IndiaChaptername = "null";
        IndiaChapterID = "null";
        string eventID = ddEvent.SelectedValue;
        chapterCode = "Finals, US";
        if (eventID == "1")
        {
            national = "null";
            chapterID = "1";
            chapterCode = "'Finals, US'";

            string cmdChapterText = string.Empty;
            DataSet dsChapter = new DataSet();
            cmdChapterText = "select ZoneId,ZoneCode,ClusterId,ClusterCode from Chapter where ChapterID=" + chapterID + "";

            dsChapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdChapterText);
            if (dsChapter.Tables[0] != null)
            {
                ZoneID = dsChapter.Tables[0].Rows[0]["ZoneId"].ToString();
                ZoneCode = "'" + dsChapter.Tables[0].Rows[0]["ZoneCode"].ToString() + "'";
                ClusterID = dsChapter.Tables[0].Rows[0]["ClusterId"].ToString();
                Cluster = "'" + dsChapter.Tables[0].Rows[0]["ClusterCode"].ToString() + "'";
            }
        }
        else if (Array.IndexOf(new string[] { "2", "3", "19" }, eventID.ToString()) > -1)
        {
            national = "null";
            chapterID = ddchapter.SelectedValue;
            chapterCode = "'" + ddchapter.SelectedItem.Text + "'";
            string cmdChapterText = string.Empty;
            DataSet dsChapter = new DataSet();
            cmdChapterText = "select ZoneId,ZoneCode,ClusterId,ClusterCode from Chapter where ChapterID=" + chapterID + "";

            dsChapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdChapterText);
            if (dsChapter.Tables[0] != null)
            {
                ZoneID = dsChapter.Tables[0].Rows[0]["ZoneId"].ToString();
                ZoneCode = "'" + dsChapter.Tables[0].Rows[0]["ZoneCode"].ToString() + "'";
                ClusterID = dsChapter.Tables[0].Rows[0]["ClusterId"].ToString();
                Cluster = "'" + dsChapter.Tables[0].Rows[0]["ClusterCode"].ToString() + "'";
            }
        }
        else if (Array.IndexOf(new string[] { "20", "13", "21" }, eventID.ToString()) > -1)
        {
            national = "null";
            chapterID = "null";
            chapterCode = "null";
            ZoneID = "null";
            ZoneCode = "null";
            ClusterID = "null";
            Cluster = "null";

        }
        string ProductGroupId = ddlProductGroup.SelectedValue;
        string ProductGroup = "'" + ddlProductGroup.SelectedItem.Text + "'";
        string ProductID = ddlProduct.SelectedValue;
        string Product = "'" + ddlProduct.SelectedItem.Text + "'";

        if (ProductGroupId == "0")
        {
            ProductGroupId = "null";
            ProductGroup = "null";
        }
        if (ProductID == "0" || ProductID == string.Empty)
        {
            ProductID = "null";
            Product = "null";
        }
        if (chapterID != "null")
        {

            cmdText = string.Format("select count(*) as CountSet from  Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7} and {4}={8}", "memberID", "EventID", "TeamID", "EventYear", "RoleID", "ChapterID", hdnMemberID.Value, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue, ddlRole.SelectedValue, chapterID);
        }
        else
        {

            cmdText = string.Format("select count(*) as CountSet from  Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7} and {4}={8}", "memberID", "EventID", "TeamID", "EventYear", "RoleID", hdnMemberID.Value, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue, ddlRole.SelectedValue);
        }

        int count = 0;
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["CountSet"] != null && ds.Tables[0].Rows[0]["CountSet"].ToString() != string.Empty)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
        }
        if (count <= 0)
        {
            cmdText = "Insert into Volunteer (MemberID,RoleID,RoleCode,TeamLead,EventID,EventCode,ChapterID,ChapterCode,[National],ZoneID,ZoneCode,ClusterID,ClusterCode,IndiaChapter,IndiaChapterName,CreateDate,CreatedBy,TeamID,EventYear,ProductGroupID,ProductGroupCode,ProductID,ProductCode) values(" + MemberID + "," + RoleID + ",'" + RoleCode + "','" + TeamLead + "'," + ddEvent.SelectedValue + ",'" + EventCode + "'," + chapterID + "," + chapterCode + "," + national + "," + ZoneID + "," + ZoneCode + "," + ClusterID + "," + Cluster + "," + IndiaChapterID + "," + IndiaChaptername + ",GETDATE()," + Session[LoginID] + "," + ddTeams.SelectedValue + ",'" + ddYear.SelectedValue + "'," + ProductGroupId + "," + ProductGroup + "," + ProductID + "," + Product + ")";

            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            lblStatusMsg.Text = "Role added successfully to selected Volunteer";
            tblAddRole.Visible = false;
            lblStatusMsg.ForeColor = Color.Blue;
            if (RadioButton1.Checked == false)
            {
                GridDisply();
                SignupDetails();
            }
            else
            {
                int memberID = Convert.ToInt32(hdnMemberID.Value);
                FillViewRole(memberID);
                pnlViewRole.Visible = true;
            }
        }
        else
        {
            lblStatusMsg.Text = "Role already exists";
            lblStatusMsg.ForeColor = Color.Red;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (validateAddRole() == "1")
        {
            AddRole();
        }
        else
        {
            validateAddRole();
        }
    }
    public string validateAddRole()
    {
        string retval = "1";
        if (ddYear.SelectedValue == "0")
        {
            retval = "-1";

            setValMessage("Please select Year");
        }
        else if (ddEvent.SelectedValue == "0")
        {
            retval = "-1";

            setValMessage("Please select Event");
        }



        else if (ddlRole.SelectedValue == "0")
        {
            retval = "-1";
            setValMessage("Please select Roles");

        }
        return retval;

    }
    public void setValMessage(string Message)
    {
        lbacces.Text = Message;
        lbacces.ForeColor = Color.Red;
    }
    protected void GvVolsignup_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GvVolsignup.PageIndex = e.NewPageIndex;
        GridDisply();
    }

    public void SearchVolunteer()
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty;
        string email = string.Empty;
        string orgname = string.Empty;
        StringBuilder strSql = new StringBuilder();
        StringBuilder strSqlOrg = new StringBuilder();
        firstName = txtFirstName.Text;
        lastName = txtLastName.Text;
        state = ddlState.SelectedItem.Value.ToString();
        email = txtEmail.Text;
        orgname = txtOrgName.Text;
        //if (state.Length < 1)
        //{
        //    lblIndSearch.Text = "Please Select State";
        //    lblIndSearch.Visible = true;
        //    return;
        //}
        //else
        //{
        //    lblIndSearch.Text = string.Empty;
        //    lblIndSearch.Visible = false;
        //}

        if (orgname.Length > 0)
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'");
        }

        if (firstName.Length > 0)
        {
            strSql.Append("  I.firstName like '%" + firstName + "%'");
        }

        if (lastName.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.lastName like '%" + lastName + "%'");
                //strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
            else
            {
                strSql.Append("  I.lastName like '%" + lastName + "%'");
                // strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
        }

        if (state.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.State like '%" + state + "%'");
            }
            else
            {
                strSql.Append("  I.State like '%" + state + "%'");
            }
        }

        if (email.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
                strSql.Append(" and I.Email like '%" + email + "%'");
            else
                strSql.Append("  I.Email like '%" + email + "%'");
        }

        if (state.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            else
                strSqlOrg.Append(" O.State like '%" + state + "%'");
        }

        if (email.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");

        }
        if (firstName.Length > 0 || orgname.Length > 0 || lastName.Length > 0 || email.Length > 0 || state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorType.SelectedValue == "INDSPOUSE")
                SearchMembers(strSql.ToString());

        }
        else
        {

            lbacces.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";

            return;
        }
    }
    private void SearchMembers(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = string.Empty;
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }
    protected void btnIndClose_onclick(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;

        Panel4.Visible = false;

        //if ((ddlChapters.SelectedIndex != 0) && (ddlYear.SelectedIndex != 0))
        //{
        //    panel3.Visible = true;
        //}
    }
    private void GetStates()
    {
        DataSet dsStates;
        dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
        ddlState.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("Select State", String.Empty));
        ddlState.SelectedIndex = 0;
    }
    protected void GridMemberDt_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = GridMemberDt.Rows[index];
            int IncurMemberID = (int)GridMemberDt.DataKeys[index].Value;
            hdnMemberID.Value = Convert.ToString(IncurMemberID);
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE I.AutoMemberID=" + IncurMemberID + "");
            DataTable dt = ds.Tables[0];
            int Count = dt.Rows.Count;
            DataView dv = new DataView(dt);
            grdFilterVolunteer.DataSource = dt;
            grdFilterVolunteer.DataBind();
            Panel1.Visible = true;
            Panel4.Visible = false;


            if (Count > 0)
            {
                btnExcel.Visible = true;
                Panel1.Visible = true;
                lblIndSearch.Text = string.Empty;
                lblIndSearch.Visible = false;
                if (Session[RoleIdColumnName].ToString() == "1" || Session[RoleIdColumnName].ToString() == "2" || Session[RoleIdColumnName].ToString() == "5")
                {
                    for (int i = 0; i < grdFilterVolunteer.Rows.Count; i++)
                    {

                        ((LinkButton)grdFilterVolunteer.Rows[i].FindControl("lnkAddRole") as LinkButton).Visible = true;
                    }
                }
                else if (Convert.ToInt32(Session[RoleIdColumnName]) > 5)
                {
                    DataSet dsRole = new DataSet();
                    string cmdText = string.Empty;
                    string MemberID = Session[LoginID].ToString();
                    string ChapterID = string.Empty;
                    string TeamLead = string.Empty;
                    string TeamID = string.Empty;
                    string chapterID = ddchapter.SelectedValue;
                    if (chapterID == string.Empty)
                    {
                        string.Format("Select ChapterID, TeamLead from Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7}", "memberID", "EventID", "TeamID", "EventYear", MemberID, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue);


                    }
                    else
                    {
                        string.Format("Select ChapterID, TeamLead from Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7}", "memberID", "EventID", "TeamID", "EventYear", MemberID, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue);


                    }
                    dsRole = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                    if (dsRole.Tables[0].Rows.Count > 0)
                    {

                        if (dsRole.Tables[0].Rows[0][ColTeamLead] != null && dsRole.Tables[0].Rows[0][ColTeamLead].ToString() != string.Empty)
                        {
                            TeamLead = dsRole.Tables[0].Rows[0][ColTeamLead].ToString();
                        }
                    }
                    if (TeamLead == "Y")
                    {
                        for (int i = 0; i < grdFilterVolunteer.Rows.Count; i++)
                        {

                            ((LinkButton)grdFilterVolunteer.Rows[i].FindControl("lnkAddRole") as LinkButton).Visible = true;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < grdFilterVolunteer.Rows.Count; i++)
                    {
                        ((LinkButton)grdFilterVolunteer.Rows[i].FindControl("lnkAddRole") as LinkButton).Enabled = false;

                    }
                }


                if (Session[RoleIdColumnName].ToString() == "1" || Session[RoleIdColumnName].ToString() == "2" || Session[RoleIdColumnName].ToString() == "5")
                {
                    for (int i = 0; i < grdFilterVolunteer.Rows.Count; i++)
                    {

                        ((Button)grdFilterVolunteer.Rows[i].FindControl("btnUpdateTeamLeadVol") as Button).Visible = true;
                        ((DropDownList)grdFilterVolunteer.Rows[i].FindControl("ddlTeamLeadVolunteer") as DropDownList).Enabled = true;

                    }
                }
                else
                {
                    for (int i = 0; i < grdFilterVolunteer.Rows.Count; i++)
                    {

                        ((Button)grdFilterVolunteer.Rows[i].FindControl("btnUpdateTeamLeadVol") as Button).Enabled = false;
                        ((DropDownList)grdFilterVolunteer.Rows[i].FindControl("ddlTeamLeadVolunteer") as DropDownList).Enabled = false;
                    }
                }


            }
            else
            {
                lblIndSearch.Text = "No match found";
                lblIndSearch.Visible = true;
                Panel4.Visible = false;
                btnExcel.Visible = false;
            }
        }




    }
    protected void grdFilterVolunteer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = null;
        if (e.CommandName == "View Role")
        {
            pnlViewRole.Visible = true;

            row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int index = row.RowIndex;
            int IncurMemberID = (int)grdFilterVolunteer.DataKeys[index].Value;
            hdnMemberID.Value = Convert.ToString(IncurMemberID);
            FillViewRole(IncurMemberID);

        }
        else if (e.CommandName == "Add Role")
        {
            row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int index = row.RowIndex;
            int IncurMemberID = (int)grdFilterVolunteer.DataKeys[index].Value;
            hdnMemberID.Value = Convert.ToString(IncurMemberID);
            tblAddRole.Visible = true;
            if (Convert.ToInt32(Session[RoleIdColumnName]) > 5)
            {
                ddlTeamLead.SelectedValue = "N";
                ddlTeamLead.Enabled = false;

            }
            else
            {

                ddlTeamLead.Enabled = true;
            }
            pnlViewRole.Visible = false;
            fillProductGroup();
        }
        else if (e.CommandName == "Update Team Lead")
        {
            row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            int index = row.RowIndex;
            int IncurMemberID = (int)grdFilterVolunteer.DataKeys[index].Value;
            hdnMemberID.Value = Convert.ToString(IncurMemberID); ;
            string TeamLead = ((DropDownList)grdFilterVolunteer.Rows[index].FindControl("ddlTeamLeadVolunteer") as DropDownList).Text;

            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int vCount = 0;


            CmdText = string.Format("select count(*) as CountSet from  Volunteer where {0}={4} and {1}={5} and {2}={6} and {3}={7}", "memberID", "EventID", "TeamID", "EventYear", hdnMemberID.Value, ddEvent.SelectedValue, ddTeams.SelectedValue, ddYear.SelectedValue);
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    vCount = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            if (vCount > 0)
            {
                CmdText = "Update Volunteer set TeamLead='" + TeamLead + "' where memberID=" + hdnMemberID.Value + " AND EventID=" + ddEvent.SelectedValue + "and RoleID=" + ddlRole.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + "";

                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                lbacces.Text = "Team Lead updated successfully";
                lbacces.ForeColor = Color.Blue;
            }
            else
            {
                lbacces.Text = "No roles assigned for the selected Member. Please assign the role first";
                lbacces.ForeColor = Color.Red;
            }

        }
    }
    protected void btnSearch_onClick(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        pnlViewRole.Visible = false;
        SearchVolunteer();
    }
    public void FillViewRole(int IncurMemberID)
    {
        string cmdText = string.Empty;
        cmdText = string.Empty;

        cmdText += "select V.VolunteerId,V.MemberId,IP.FirstName,IP.LastName,V.RoleCode,E.EventCode";
        cmdText += ",C.ChapterCode,Z.ZoneCode,Cl.ClusterCode,V.TeamLead,V.[National],PG.ProductGroupCode";
        cmdText += ",P.ProductCode,V.CreateDate";
        cmdText += " from Volunteer V left join IndSpouse IP on(V.MemberID=IP.AutoMemberID)";
        cmdText += " left Join Chapter Ch On V.ChapterID = Ch.ChapterID ";
        cmdText += " left join ProductGroup PG on V.ProductGroupID=PG.ProductGroupId";
        cmdText += " left Join Product P on V.ProductId=P.ProductId ";
        cmdText += " left join Event E on V.EventID=E.EventID";
        cmdText += " left join Chapter C on V.ChapterID=C.ChapterID";
        cmdText += " left join Cluster Cl on V.ClusterID=Cl.ClusterID";
        cmdText += " left join Zone Z on V.ZoneID=Z.ZoneID";
        cmdText += " where V.MemberId=" + IncurMemberID + "";

        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        grdViewRoles.DataSource = dt;
        grdViewRoles.DataBind();
        Panel1.Visible = true;
        Panel4.Visible = false;
        pnlViewRole.Visible = true;
        lblViewRoleText.Text = string.Empty;
        lblCurrentVolName.Text = grdFilterVolunteer.Rows[0].Cells[5].Text + " " + grdFilterVolunteer.Rows[0].Cells[6].Text;
        if (Count > 0)
        {
            Panel1.Visible = true;
            lblViewRoleText.Text = string.Empty;
            lblIndSearch.Visible = false;
        }
        else
        {
            lblViewRoleText.Text = "No match found";

            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }
    }
    protected void btnCancel_onClick(object sender, EventArgs e)
    {
        tblAddRole.Visible = false;
    }

    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select", "0"));
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
    }

    public void ExportToExcelAll()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        if (rbtIndividualCheck.Checked == true)
        {
            CmdText = " select I.State,I.City,Vs.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.VolsignupId,Vs.TeamId,VT.TeamName,Year,Vs.EventID,PG.Name as ProductGroup,"
         + "  P.Name as Product,Vs.AvailHours,Vs.ProductGroupId,Vs.ProductId,Vs.Eventname,C.Name as ChapterName from "
         + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
         + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null  " + genWhereConditons() + " "
         + " order by memberid , year desc,eventid asc,TeamName";

            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string secMemberIDs = string.Empty;
                string memberIDs = string.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr[ColMemberID] != null && dr[ColMemberID].ToString() != string.Empty)
                    {
                        if (secMemberIDs != dr[ColMemberID].ToString())
                        {
                            memberIDs += dr[ColMemberID].ToString() + ",";
                        }
                        secMemberIDs = dr[ColMemberID].ToString();
                    }

                }
                string iCondtions = string.Empty;
                if ((ddYear.SelectedItem.Text != "Select Year") && (ddYear.SelectedItem.Text != "All"))
                {
                    iCondtions += " and  Vs.year=" + ddYear.SelectedValue;
                }
                if ((ddEvent.SelectedItem.Text != "Select Event") && (ddEvent.SelectedItem.Text != "All"))
                {
                    iCondtions += " and Vs.EventID=" + ddEvent.SelectedValue;
                }

                if ((ddchapter.SelectedItem.Text != "Select Chapter") && (ddchapter.SelectedItem.Text != "All"))
                {
                    iCondtions += " and Vs.ChapterId=" + ddchapter.SelectedValue;
                }

                if (memberIDs != string.Empty)
                {
                    memberIDs = memberIDs.Remove(memberIDs.Length - 1);
                    hdnMemberIDs.Value = memberIDs;
                    CmdText = " select Vs.MemberID,I.FirstName,I.LastName,I.Email,I.hphone,I.Cphone,Vs.VolsignupId,Vs.TeamId,VT.TeamName,Year,PG.Name as ProductGroup,"
          + "  P.Name as Product,Vs.AvailHours,I.State,I.City,Vs.Eventname,C.Name as ChapterName from "
          + "  VolSignUp Vs left join VolTeamMatrix VT on Vs.TeamId=Vt.TeamID left join ProductGroup PG on "
          + "  Pg.ProductGroupId=Vs.ProductGroupId left join  Product P on P.ProductId=Vs.ProductId left join chapter C on C.chapterid=Vs.chapterid  left join IndSpouse I on I.automemberid=Vs.memberID where Eventname is not null " + iCondtions + " and Vs.MemberID in (" + memberIDs + ")  order by memberid , year desc,TeamName;";

                    CmdText += " select distinct V.Memberid,V.RoleID,V.RoleCode,I.FirstName,I.LastName,I.Email,I.HPhone,I.CPhone,I.City,I.State from  volunteer V left join"
        + " IndSpouse I on I.AutomemberId=V.memberId inner join VolSignup Vs on Vs.MemberId=I.AutomemberId  where V.RoleId is not null and V.RoleCode is not null and V.MemberId in (" + hdnMemberIDs.Value + ")";


                    ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
                }

            }


        }
        else
        {
            CmdText = "select I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE I.AutoMemberID=" + hdnMemberID.Value + ";";

            CmdText += "select V.VolunteerId,V.MemberId,IP.FirstName,IP.LastName,V.RoleCode,E.EventCode";
            CmdText += ",C.ChapterCode,Z.ZoneCode,Cl.ClusterCode,V.TeamLead,V.[National],PG.ProductGroupCode";
            CmdText += ",P.ProductCode,V.CreateDate";
            CmdText += " from Volunteer V left join IndSpouse IP on(V.MemberID=IP.AutoMemberID)";
            CmdText += " left Join Chapter Ch On V.ChapterID = Ch.ChapterID ";
            CmdText += " left join ProductGroup PG on V.ProductGroupID=PG.ProductGroupId";
            CmdText += " left Join Product P on V.ProductId=P.ProductId ";
            CmdText += " left join Event E on V.EventID=E.EventID";
            CmdText += " left join Chapter C on V.ChapterID=C.ChapterID";
            CmdText += " left join Cluster Cl on V.ClusterID=Cl.ClusterID";
            CmdText += " left join Zone Z on V.ZoneID=Z.ZoneID";
            CmdText += " where V.MemberId=" + hdnMemberID.Value + "";

        }
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
        DateTime dt = DateTime.Now;
        string month = dt.ToString("MMM");
        string day = dt.ToString("dd");
        string year = dt.ToString("yyyy");
        string monthDay = month + "" + day;

        string filename = "SetUpTeams_" + monthDay + "_" + year + ".xls";
        ds.Tables[0].TableName = "Volunteer Sign Up";
        ds.Tables[1].TableName = "Existing Roles";
        ExcelHelper.ToExcel(ds, filename, Page.Response);

    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        ExportToExcelAll();
    }

    public void UpdateTeamLeadFlage(string TeamLead, string MemberID)
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        CmdText = "Update Volunteer set TeamLead=" + TeamLead + " where memberID=" + MemberID + " AND EventID=" + ddEvent.SelectedValue + "and RoleID=" + ddlRole.SelectedValue + "";

        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, CmdText);
        lbacces.Text = "Team Lead updated successfully";
        lbacces.ForeColor = Color.Blue;
    }
    protected void GridMemberDt_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridMemberDt.PageIndex = e.NewPageIndex;
        SearchVolunteer();
    }

}