Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Text
Imports System.IO
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Partial Class SearchDonationReceipt
    Inherits System.Web.UI.Page
    Dim sSQL As String
    Dim sSQLORG As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("LoggedIn") = True
        Session("LoginID") = 4240
        Session("entryToken") = "v"
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Page.IsPostBack = False Then
            Page.MaintainScrollPositionOnPostBack = True
            If rblIndividual.SelectedValue = "S" Or rblIndividual.SelectedValue = "C" Then
                btnViewAll.Enabled = False
                btnGenerate.Enabled = False
                hlnkDownload.Enabled = False
            End If
            '*** Populate State DropDown
            Dim dsStates As DataSet
            Dim strSqlQuery As String
            strSqlQuery = "SELECT Distinct StateCode,Name FROM StateMaster Order By Name"
            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSqlQuery)
            If dsStates.Tables.Count > 0 Then
                ddlstate.DataSource = dsStates.Tables(0)
                ddlstate.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlstate.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlstate.DataBind()
                ddlstate.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If

            '*** Populate Chapter DropDown
            '*** Populate Chapter Names List
            Dim objChapters As New Chapter
            Dim dsChapters As New DataSet
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

            If dsChapters.Tables.Count > 0 Then
                ddlchapter.DataSource = dsChapters.Tables(0)
                ddlchapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                ddlchapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlchapter.DataBind()
                ddlchapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            End If
            If Len(Session("LoginChapterID")) > 0 Then
                ddlchapter.SelectedValue = Session("LoginChapterID")
                ddlchapter.Enabled = False
            End If
            If Session("RoleID") = "5" Then
                ddlchapter.Items.Clear()
                Dim strSql As String
                strSql = "Select chapterid, chaptercode, state from chapter "
                strSql = strSql & " where clusterid in (Select clusterid from "
                strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
                strSql = strSql & " order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))
                Dim drChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drChapters.Read())
                    ddlchapter.Items.Add(New ListItem(drChapters(1).ToString(), drChapters(0).ToString()))
                End While
                ddlchapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                ddlchapter.Enabled = True
            End If
            Dim thisdate As DateTime
            thisdate = System.DateTime.Now
            Dim dtcurryear As DateTime
            Dim dtPrevYear As DateTime
            dtcurryear = thisdate
            dtPrevYear = System.DateTime.Now.AddYears(-2)
            Dim i As Integer
            For i = dtPrevYear.Year To dtcurryear.Year
                ddlYear.Items.Add(New ListItem(i.ToString(), i.ToString()))
            Next
            ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText((System.DateTime.Now.AddYears(-1).Year).ToString()))
            If Not Request.QueryString("ID") Is Nothing Then
                If Request.QueryString("ID") = 5 Then
                    rblIndividual.SelectedIndex = rblIndividual.Items.IndexOf(rblIndividual.Items.FindByValue(Session("IType")))
                    Dim dataConnection As New SqlConnection(Application("ConnectionString"))
                    Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(Session("sSQL"), dataConnection)
                    Dim dataAdapterInd As SqlDataAdapter
                    Dim dsInd As New DataSet
                    dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
                    Try
                        dataAdapterInd.Fill(dsInd)
                        gvDonationReceipt.DataSource = dsInd.Tables(0)
                        gvDonationReceipt.DataBind()
                    Catch ex As Exception
                        lblMessage.Text = ex.Message
                    End Try
                End If
            End If
        End If
    End Sub
    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        LoadData()
    End Sub
    Protected Sub gvDonationReceipt_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs)

    End Sub
    Protected Sub gvDonationReceipt_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDonationReceipt.PageIndexChanging
        gvDonationReceipt.PageIndex = e.NewPageIndex
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(Session("sSQL"), dataConnection)
        Dim dataAdapterInd As SqlDataAdapter
        Dim dsInd As New DataSet
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvDonationReceipt.DataSource = dsInd.Tables(0)
            gvDonationReceipt.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvDonationReceipt_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDonationReceipt.RowDataBound
        Select Case e.Row.RowType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim hlnkDonationReceipt As HyperLink = CType(e.Row.Cells(2).FindControl("hlnkDonationReceipt"), HyperLink)
                hlnkDonationReceipt.NavigateUrl = "~/ShowDonationReceipt.aspx?MemberID=" & e.Row.Cells(0).Text & "&Year=" & ddlYear.SelectedValue & "&Mail=N&IType=" & rblIndividual.SelectedValue
        End Select
    End Sub

    Protected Sub rblIndividual_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblIndividual.SelectedIndexChanged
        If rblIndividual.SelectedValue = "S" Then
            txtcity.Enabled = True
            txtemail.Enabled = True
            txtfirstname.Enabled = True
            txtlastname.Enabled = True
            txtstreet.Enabled = True
            txtzip.Enabled = True
            ddlstate.Enabled = True
            btnViewAll.Enabled = False
            btnsearch.Enabled = True
            gvDonationReceipt.Visible = True
            'btnToExcel.Enabled = False
            btnGenerate.Enabled = True
        ElseIf rblIndividual.SelectedValue = "C" Then
            txtcity.Enabled = True
            txtemail.Enabled = True
            txtfirstname.Enabled = True
            txtlastname.Enabled = True
            txtstreet.Enabled = True
            txtzip.Enabled = True
            ddlstate.Enabled = True
            btnViewAll.Enabled = False
            btnsearch.Enabled = True
            gvDonationReceipt.Visible = True
            'btnToExcel.Enabled = False
            btnGenerate.Enabled = True
            rblEmail.SelectedIndex = rblEmail.Items.IndexOf(rblEmail.Items.FindByValue("N"))
        Else
            btnToExcel.Enabled = False
            txtcity.Text = ""
            txtemail.Text = ""
            txtfirstname.Text = ""
            txtlastname.Text = ""
            txtstreet.Text = ""
            txtzip.Text = ""
            ddlstate.SelectedIndex = 0
            txtcity.Enabled = False
            txtemail.Enabled = False
            txtfirstname.Enabled = False
            txtlastname.Enabled = False
            txtstreet.Enabled = False
            txtzip.Enabled = False
            ddlstate.Enabled = False
            btnViewAll.Enabled = True
            btnsearch.Enabled = False
            gvDonationReceipt.Visible = False
            btnGenerate.Enabled = True
            btnToExcel.Enabled = True
        End If
    End Sub
    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAll.Click
        Try
            If rblEmail.SelectedValue = "Y" Then
                If ddlchapter.SelectedItem.Text = "Select Chapter" Then
                    Response.Redirect("ShowAllDonationReceipts.aspx?Year=" & ddlYear.SelectedValue)
                Else
                    Response.Redirect("ShowAllDonationReceipts.aspx?Year=" & ddlYear.SelectedValue & "&Chapter=" & ddlchapter.SelectedItem.Text)
                End If
            Else
                LoadData()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToExcel.Click
        Try
            Response.Clear()
            If rblEmail.SelectedValue = "Y" Then
                Response.AddHeader("content-disposition", "attachment;filename=EmailDonationReceipts.xls")
            Else
                Response.AddHeader("content-disposition", "attachment;filename=NoEmailDonationReceipts.xls")
            End If

            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            gvDonationReceipt.AllowPaging = False
            LoadData()
            gvDonationReceipt.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.End()
            gvDonationReceipt.AllowPaging = True
            LoadData()
        Catch ex As Exception

        End Try
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Confirms that an HtmlForm control is rendered for the specified ASP.NET server control at run time.
    End Sub
    Private Sub LoadData()
        lblMessage.Text = String.Empty
        Dim dtCurrDate As DateTime
        dtCurrDate = System.DateTime.Now
        Session("sSQL") = ""
        Session("IType") = rblIndividual.SelectedValue
        If rblIndividual.SelectedValue = "C" Then
            sSQL = "Select O.AutoMemberID, 'OWN' as DONORTYPE, O.ORGANIZATION_NAME as FirstName, O.MIDDLE_INITIAL as MiddleInitial, O.FIRST_NAME+' '+O.LAST_NAME as LastName,"
            sSQL = sSQL & " O.ADDRESS1, O.ADDRESS2, O.STATE, O.ZIP,O.CITY,O.Email, O.PHONE as HPhone, O.LIAISONPERSON as LiasonPerson, '' as ReferredBy, O.ChapterID,C.ChapterCode as Chapter FROM  OrganizationInfo O LEFT JOIN Chapter C ON C.ChapterID = O.ChapterID "
            sSQL = sSQL & " WHERE O.Automemberid in (SELECT DISTINCT MEMBERID FROM donationsinfo "
            sSQL = sSQL & " WHERE DonorType IN ('OWN') "
            If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
            Else
                sSQL = sSQL & " and DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '31-DEC-" & ddlYear.SelectedValue & " 23:59:59'"
            End If
            Dim blnCheck As Boolean = False
            Dim Email As String = txtemail.Text
            Dim FirstName As String = txtfirstname.Text
            Dim LastName As String = txtlastname.Text
            Dim Street As String = txtstreet.Text
            Dim City As String = txtcity.Text
            Dim state As String = ddlstate.Text
            Dim ZipCode As String = txtzip.Text
            'Dim NSFChapter As String = ddlchapter.Text
            Dim NSFChapter As String = ddlchapter.SelectedItem.Text
            If (Len(Trim(FirstName)) > 0) Then
                sSQL = sSQL & " and (O.FIRST_NAME like '%" & FirstName & "%' OR O.ORGANIZATION_NAME like '%" & FirstName & "%')"
                blnCheck = True
            End If

            If (Len(Trim(LastName)) > 0) Then
                sSQL = sSQL & " and O.LAST_NAME like '%" & LastName & "%'"
                blnCheck = True
            End If

            If (Len(Trim(Street)) > 0) Then
                sSQL = sSQL & " and O.ADDRESS1 like '%" & Street & "%'"
                blnCheck = True
            End If

            If (Len(Trim(City)) > 0) Then
                sSQL = sSQL & " and O.CITY like '%" & City & "%'"
                blnCheck = True
            End If

            If (Len(Trim(state)) > 0 And Trim(state) <> "Other") Then
                sSQL = sSQL & " and O.STATE like '%" & state & "%'"
                blnCheck = True
            End If


            If (Len(Trim(ZipCode)) > 0) Then
                sSQL = sSQL & " and O.ZIP like '%" & ZipCode & "%'"
                blnCheck = True
            End If

            If (Len(Trim(Email)) > 0) Then
                sSQL = sSQL & " and O.EMAIL like '%" & Email & "%'"
                blnCheck = True
            End If
            If rblEmail.SelectedValue = "Y" Then
                sSQL = sSQL & " and isnull(O.EMAIL,'') <>''  )"
            Else
                sSQL = sSQL & " and  isnull(O.email,'')='' )"
            End If
            If blnCheck = False Then
                lblMessage.Text = "Please Enter any of the Input"
                lblMessage.Visible = True
                gvDonationReceipt.DataSource = Nothing
                gvDonationReceipt.DataBind()
                Exit Sub
            End If
        Else
            sSQL = "Select "
            sSQL = sSQL & " AutoMemberID,"
            sSQL = sSQL & " IndSpouse.DONORTYPE,FirstName,"
            sSQL = sSQL & " MiddleInitial,LastName,"
            sSQL = sSQL & " Address1,Address2,State,Zip,City,EMail,"
            sSQL = sSQL & " HPhone, LiasonPerson, ReferredBy, Chapter"
            sSQL = sSQL & " from IndSpouse"
            sSQL = sSQL & " WHERE "
            sSQL = sSQL & " IndSpouse.DeletedFlag <> 'Yes' and  "
            sSQL = sSQL & "   DonorType IN ('IND','SPOUSE') AND "
            sSQL = sSQL & " ((IndSpouse.Automemberid in"
            sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
            sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.AutoMemberiD "
            sSQL = sSQL & "   AND DonorType IN ('IND','SPOUSE') "
            If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
            Else
                sSQL = sSQL & " and DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '31-DEC-" & ddlYear.SelectedValue & " 23:59:59'"
            End If
            sSQL = sSQL & " UNION  SELECT DISTINCT MEMBERID from nfg_transactions "
            sSQL = sSQL & "   where ID Is null And fee > 0 and nfg_transactions.MemberiD = IndSpouse.AutoMemberiD "
            If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                sSQL = sSQL & " AND ""Payment Date"" BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
            Else
                sSQL = sSQL & " and ""Payment Date"" BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '31-DEC-" & ddlYear.SelectedValue & " 23:59:59'"
            End If
            sSQL = sSQL & " ))"
            sSQL = sSQL & " OR (IndSpouse.DonorType ='SPOUSE' AND IndSpouse.Relationship in"
            sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
            sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.Relationship "
            sSQL = sSQL & "   AND DonorType IN ('IND','SPOUSE') "
            If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
            Else
                sSQL = sSQL & " and DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '31-DEC-" & ddlYear.SelectedValue & " 23:59:59'"
            End If
            sSQL = sSQL & " UNION  SELECT DISTINCT MEMBERID from nfg_transactions "
            sSQL = sSQL & "   where ID Is null And fee > 0 and nfg_transactions.MemberiD = IndSpouse.Relationship "
            If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                sSQL = sSQL & " AND ""Payment Date"" BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & " 23:59:59'"
            Else
                sSQL = sSQL & " and ""Payment Date"" BETWEEN '01-JAN-" & ddlYear.SelectedValue & " 00:00:00' AND '31-DEC-" & ddlYear.SelectedValue & " 23:59:59'"
            End If
            sSQL = sSQL & " )))"
            Dim blnCheck As Boolean = False
            Dim Email As String = txtemail.Text
            Dim FirstName As String = txtfirstname.Text
            Dim LastName As String = txtlastname.Text
            Dim Street As String = txtstreet.Text
            Dim City As String = txtcity.Text
            Dim state As String = ddlstate.Text
            Dim ZipCode As String = txtzip.Text
            'Dim NSFChapter As String = ddlchapter.Text
            Dim NSFChapter As String = ddlchapter.SelectedItem.Text
            If (Len(Trim(FirstName)) > 0) Then
                sSQL = sSQL & " and FIRSTNAME like '%" & FirstName & "%'"
                blnCheck = True
            End If

            If (Len(Trim(LastName)) > 0) Then
                sSQL = sSQL & " and LASTNAME like '%" & LastName & "%'"
                blnCheck = True
            End If

            If (Len(Trim(Street)) > 0) Then
                sSQL = sSQL & " and ADDRESS1 like '%" & Street & "%'"
                blnCheck = True
            End If

            If (Len(Trim(City)) > 0) Then
                sSQL = sSQL & " and CITY like '%" & City & "%'"
                blnCheck = True
            End If

            If (Len(Trim(state)) > 0 And Trim(state) <> "Other") Then
                sSQL = sSQL & " and STATE like '%" & state & "%'"
                blnCheck = True
            End If


            If (Len(Trim(ZipCode)) > 0) Then
                sSQL = sSQL & " and ZIP like '%" & ZipCode & "%'"
                blnCheck = True
            End If

            If (Len(Trim(Email)) > 0) Then
                sSQL = sSQL & " and EMAIL like '%" & Email & "%'"
                blnCheck = True
            End If
            If rblEmail.SelectedValue = "Y" Then
                sSQL = sSQL & " and isnull(EMAIL,'') <>''  "
            Else
                sSQL = sSQL & " and  isnull(email,'')='' "
            End If
            'If Session("RoleID") = "5" Then
            '    Dim i As Integer
            '    NSFChapter = String.Empty
            '    For i = 1 To ddlchapter.Items.Count - 1
            '        If Len(NSFChapter) > 0 Then
            '            NSFChapter = NSFChapter & ",'" & ddlchapter.Items(i).Text & "'"
            '        Else
            '            NSFChapter = "'" & ddlchapter.Items(i).Text & "'"
            '        End If
            '    Next
            '    sSQL = sSQL & " and CHAPTER IN (" & NSFChapter & ")"
            '    sSQLORG = sSQLORG & " and NSF_CHAPTER IN(" & NSFChapter & ") "
            '    blnCheck = True
            'Else
            '    If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "[Select Chapter]") Then
            '        sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
            '        sSQLORG = sSQLORG & " and NSF_CHAPTER like '%" & NSFChapter & "%'"
            '        blnCheck = True
            '    End If
            'End If
            If Session("RoleID") = "5" Then
                Dim i As Integer
                NSFChapter = ddlchapter.SelectedItem.Text

                If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                    sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                    blnCheck = True
                Else
                    For i = 1 To ddlchapter.Items.Count - 1
                        If Len(NSFChapter) > 0 And NSFChapter <> "Select Chapter" Then
                            NSFChapter = NSFChapter & ",'" & ddlchapter.Items(i).Text & "'"
                        Else
                            NSFChapter = "'" & ddlchapter.Items(i).Text & "'"
                        End If
                    Next
                    sSQL = sSQL & " and CHAPTER IN (" & NSFChapter & ")"
                    blnCheck = True
                End If
            Else
                If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                    sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                    blnCheck = True
                End If
            End If
            If (blnCheck = False) Then
                If Session("RoleID") = "5" Then
                    sSQL = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,"
                    sSQL = sSQL & " LastName,Address1,Address2,City,State,Zip,EMail,HPhone,LiasonPerson,ReferredBy,"
                    sSQL = sSQL & " Chapter from IndSpouse WHERE DeletedFlag <>'Yes' and  "
                    If rblEmail.SelectedValue = "Y" Then
                        sSQL = sSQL & "  isnull(EMAIL,'') <>''  AND "
                    Else
                        sSQL = sSQL & "   isnull(email,'')='' AND "
                    End If
                    'Added DonorType
                    sSQL = sSQL & " DonorType IN ('IND','SPOUSE') AND "
                    sSQL = sSQL & " IndSpouse.Automemberid in"
                    sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
                    sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.AutoMemberiD "
                    sSQL = sSQL & "   AND DonorType IN ('IND','SPOUSE') "
                    If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                        sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
                    Else
                        sSQL = sSQL & "  and DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & "' AND '31-DEC-" & ddlYear.SelectedValue & "'"
                    End If
                    sSQL = sSQL & " )"
                    sSQL = sSQL & " AND Chapter IN(" & NSFChapter & ") ORDER BY LastName,FirstName ASC"
                Else
                    sSQL = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,"
                    sSQL = sSQL & " LastName,Address1,Address2,City,State,Zip,EMail,HPhone,"
                    sSQL = sSQL & " LiasonPerson,ReferredBy,Chapter from "
                    sSQL = sSQL & " IndSpouse WHERE DeletedFlag <>'Yes' AND "
                    If rblEmail.SelectedValue = "Y" Then
                        sSQL = sSQL & "  isnull(EMAIL,'') <> ''  AND "
                    Else
                        sSQL = sSQL & "   isnull(email,'')='' AND "
                    End If
                    'Added DonorType
                    sSQL = sSQL & "  DonorType IN ('IND','SPOUSE') AND "
                    sSQL = sSQL & " IndSpouse.Automemberid in"
                    sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
                    sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.AutoMemberiD "
                    sSQL = sSQL & "   AND DonorType IN ('IND','SPOUSE') "
                    If CInt(ddlYear.SelectedValue) = System.DateTime.Now.Year Then
                        sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
                    Else
                        sSQL = sSQL & "  and DonationDate BETWEEN '01-JAN-" & ddlYear.SelectedValue & "' AND '31-DEC-" & ddlYear.SelectedValue & "'"
                    End If
                    sSQL = sSQL & " )"
                    sSQL = sSQL & " ORDER BY LastName,FirstName ASC"
                End If
            End If
        End If

        Session("sSQL") = sSQL
        'Response.Write(sSQL)


        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
        Dim dataAdapterInd As SqlDataAdapter
        Dim dsInd As New DataSet
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            If dsInd.Tables(0).Rows.Count > 0 Then
                gvDonationReceipt.Visible = True
                gvDonationReceipt.DataSource = dsInd.Tables(0)
                gvDonationReceipt.DataBind()
                'btnToExcel.Enabled = True
                lblMessage.Visible = False
            Else
                gvDonationReceipt.Visible = False
                'btnToExcel.Enabled = False
                lblMessage.Visible = True
                lblMessage.Text = "No donation Receipts Found."
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message & "<br>" & Session("sSQL")
        End Try
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Dim m_OutputFile As String
        Dim strFileName As String
        If rblEmail.SelectedValue = "Y" Then
            m_OutputFile = Server.MapPath("DonorReceipts/Email_DonorReceipts_" & Session("LoginID") & System.DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & System.DateTime.Now.Hour & "_" & System.DateTime.Now.Minute & ".htm")
            strFileName = "DonorReceipts/Email_DonorReceipts_" & Session("LoginID") & System.DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & System.DateTime.Now.Hour & "_" & System.DateTime.Now.Minute & ".htm"
            Dim sw As New StreamWriter(m_OutputFile, False)
            Server.Execute("ShowAllDonationReceipts.aspx?Year=" & ddlYear.SelectedValue & "&Type=Save", sw)
            sw.Flush()
            sw.Close()
            hlnkDownload.Enabled = True
            hlnkDownload.NavigateUrl = strFileName
        Else
            m_OutputFile = Server.MapPath("DonorReceipts/No_Email_DonorReceipts_" & Session("LoginID") & System.DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & System.DateTime.Now.Hour & "_" & System.DateTime.Now.Minute & ".htm")
            strFileName = "DonorReceipts/No_Email_DonorReceipts_" & Session("LoginID") & System.DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & System.DateTime.Now.Hour & "_" & System.DateTime.Now.Minute & ".htm"
            Dim sw As New StreamWriter(m_OutputFile, False)
            Server.Execute("ShowAllDonationReceipts.aspx?Email=N&Year=" & ddlYear.SelectedValue & "&Type=Save", sw)
            sw.Flush()
            sw.Close()
            hlnkDownload.Enabled = True
            hlnkDownload.NavigateUrl = strFileName
        End If
    End Sub
End Class
