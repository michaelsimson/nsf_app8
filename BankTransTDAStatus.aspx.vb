Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class BankTransTDAStatus
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BrokTransID, BankCode, BankName, TransDate, CreatedDate As String
        Dim tableExist As String = ""
        Dim reader As SqlDataReader
        Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0><tr><td align=Center> Table Name </td><td align=Center>Data Exists</td><td align=Center>BrokTransID/ Temp Records</td><td width='175px' align=Center>Bank Name</td><td align=Center>TransDate</td><td align=Center>CreatedDate</td></tr>"

        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Top 1 B.BankCode,(SELECT COUNT(*) FROM BrokTemp) as Records,B.BankName,IsNull(CONVERT(VARCHAR(10), Br.Date, 101),'') as Trans_Date,IsNull(CONVERT(VARCHAR(10),Br.CreatedDate,101) ,'') as Created_Date  From BrokTemp  Br,Bank B Where  Br.AcctNumber =  B.ActNumber order by  Br.Date")
        If reader.Read() Then
            BankCode = reader("BankCode")
            BrokTransID = reader("Records")
            BankName = reader("BankName")
            TransDate = reader("Trans_Date")
            CreatedDate = reader("Created_Date")
            'End While
            TableStr = TableStr & "<tr><td align=Center> BrokTemp -" & BankCode & " </td><td align=Center> Y </td><td align=Center> " & BrokTransID & "</td><td align=Center>" & BankName & " </td><td align=Center> " & TransDate & "</td><td align=Center> " & CreatedDate & "</td></tr>"
            reader.Close()
       End If
        BankCode = ""
        BrokTransID = ""
        BankName = ""
        TransDate = ""
        CreatedDate = ""

        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Top 1 Br.BankCode,Br.BrokTransID,B.BankName,IsNull(CONVERT(VARCHAR(10), Br.TransDate, 101),'') as Trans_Date,IsNull(CONVERT(VARCHAR(10),Br.CreatedDate,101) ,'') as Created_Date  From BrokTrans Br,Bank B Where  Br.BankID = 4 and Br.BankID = B.BankID order by TransDate desc,BrokTransID desc")
        If reader.Read() Then
            BankCode = reader("BankCode")
            BrokTransID = reader("BrokTransID")
            BankName = reader("BankName")
            TransDate = reader("Trans_Date")
            CreatedDate = reader("Created_Date")
            'End While
            TableStr = TableStr & "<tr><td align=Center> BrokTrans -" & BankCode & " </td><td align=Center>  </td><td align=Center> " & BrokTransID & "</td><td align=Center>" & BankName & " </td><td align=Center> " & TransDate & "</td><td align=Center> " & CreatedDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center> BrokTrans - TDA_US_Sch </td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        BankCode = ""
        BrokTransID = ""
        BankName = ""
        TransDate = ""
        CreatedDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Top 1 Br.BankCode,Br.BrokTransID,B.BankName,IsNull(CONVERT(VARCHAR(10), Br.TransDate, 101),'') as Trans_Date,IsNull(CONVERT(VARCHAR(10),Br.CreatedDate,101) ,'') as Created_Date  From BrokTrans Br,Bank B Where   Br.BankID = 5 and Br.BankID =B.BankID order by TransDate desc,BrokTransID desc")
        If reader.Read() Then
            BankCode = reader("BankCode")
            BrokTransID = reader("BrokTransID")
            BankName = reader("BankName")
            TransDate = reader("Trans_Date")
            CreatedDate = reader("Created_Date")
            'End While
            TableStr = TableStr & "<tr><td align=Center> BrokTrans -" & BankCode & " </td><td align=Center>  </td><td align=Center> " & BrokTransID & "</td><td align=Center>" & BankName & " </td><td align=Center> " & TransDate & "</td><td align=Center> " & CreatedDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>BrokTrans - TDA_General</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        BankCode = ""
        BrokTransID = ""
        BankName = ""
        TransDate = ""
        CreatedDate = ""
        ' Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT1temp")
        'If Cnt > 0 Then
        'Cnt = 0
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Top 1 Br.BankCode,Br.BrokTransID,B.BankName,IsNull(CONVERT(VARCHAR(10), Br.TransDate, 101),'') as Trans_Date,IsNull(CONVERT(VARCHAR(10),Br.CreatedDate,101) ,'') as Created_Date  From BrokTrans Br,Bank B Where   Br.BankID = 6 and Br.BankID =B.BankID order by TransDate desc,BrokTransID desc")
        If reader.Read() Then
            BankCode = reader("BankCode")
            BrokTransID = reader("BrokTransID")
            BankName = reader("BankName")
            TransDate = reader("Trans_Date")
            CreatedDate = reader("Created_Date")
            'End While
            TableStr = TableStr & "<tr><td align=Center> BrokTrans -" & BankCode & " </td><td align=Center>  </td><td align=Center> " & BrokTransID & "</td><td align=Center>" & BankName & " </td><td align=Center> " & TransDate & "</td><td align=Center> " & CreatedDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>BrokTrans - TDA_Endow</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        BankCode = ""
        BrokTransID = ""
        BankName = ""
        TransDate = ""
        CreatedDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Top 1 Br.BankCode,Br.BrokTransID,B.BankName,IsNull(CONVERT(VARCHAR(10), Br.TransDate, 101),'') as Trans_Date,IsNull(CONVERT(VARCHAR(10),Br.CreatedDate,101) ,'') as Created_Date  From BrokTrans Br,Bank B Where   Br.BankID = 7 and Br.BankID =B.BankID order by TransDate desc,BrokTransID desc")
        If reader.Read() Then
            BankCode = reader("BankCode")
            BrokTransID = reader("BrokTransID")
            BankName = reader("BankName")
            TransDate = reader("Trans_Date")
            CreatedDate = reader("Created_Date")
            'End While
            TableStr = TableStr & "<tr><td align=Center> BrokTrans -" & BankCode & " </td><td align=Center>  </td><td align=Center> " & BrokTransID & "</td><td align=Center>" & BankName & " </td><td align=Center> " & TransDate & "</td><td align=Center> " & CreatedDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>BrokTrans - TDA_DAF</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        BankCode = ""
        BrokTransID = ""
        BankName = ""
        TransDate = ""
        CreatedDate = ""

        TableStr = TableStr & "</Table><br><br>"
        Literal1.Text = TableStr.ToString
    End Sub

End Class
