Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class WalkathonMarathonFunctions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not IsPostBack Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from WalkMarathon where MemberID =  " & Session("CustIndID")) > 0 Then
                Hyperlink3.NavigateUrl = "Don_athon_custom.aspx?id=" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select  max(WalkMarID) from WalkMarathon where MemberID =  " & Session("CustIndID"))
            Else
                Hyperlink3.Enabled = False
            End If
        End If
    End Sub

    Protected Sub hlinkWalkathonReg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkWalkathonReg.Click
        'walkathonRegis.aspx
        Dim eventid As Integer
        eventid = EventIDCheck.GetEventIDFromDB(5)

        If eventid = 5 Then
            Response.Redirect("walkathonRegis.aspx")
        Else
            lblMessage.Text = "Walk-a-thon is not currently open for online registration."
        End If
    End Sub
End Class
