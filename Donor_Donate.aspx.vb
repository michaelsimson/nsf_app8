Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Namespace VRegistration
    Partial Class Donor_Donate
        Inherits System.Web.UI.Page

        '    Private us As CultureInfo = New CultureInfo("en-US")
        Protected lbBack As System.Web.UI.WebControls.LinkButton
        'Protected RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
        Protected Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents CustomValidator1 As System.Web.UI.WebControls.CustomValidator
        Dim strSqlQuery As String
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'DropDownList1.Select = False

            If Session("LoggedIn") <> "True" Then
                Server.Transfer("login.aspx?entry=" + Session("entryToken").ToString().Substring(0, 1))
            End If
            If Session("entryToken") = "Parent" Then
                lnkback.Text = "Back to Parent Functions Page"
            ElseIf Session("entryToken") = "Donor" Then
                lnkback.Text = "Back to Donor Functions Page"
            ElseIf Session("entryToken") = "Volunteer" Then
                lnkback.Text = "Back to Volunteer Functions Page"
            End If

            'Add back link
            'Dim nsfMaster As NSFMasterPage = Me.Master
            'nsfMaster.addBackMenuItem("termsAndConditions.aspx")
            'nsfMaster.addMenuItem("Back", "reg_contestants.aspx")

            If Not IsPostBack Then
                ' ddlChapterLiaison.SelectedIndex = 0
                LoadDonationPurpose()
                LoadDonationEvent()
                'LoadEducationGame()
                'ddlVolunteer.SelectedIndex = 0

                Dim ds As DataSet = Nothing
                If Session("entryToken") = "Parent" Or Session("entryToken") = "Donor" Then
                    ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByText("Donation"))
                    If (Not Session("CustIndID") Is Nothing) Then

                        Dim conn As New SqlConnection(Application("ConnectionString"))
                        Dim prmArray(6) As SqlParameter

                        prmArray(0) = New SqlParameter
                        prmArray(0).ParameterName = "@ParentID"
                        'old 'prmArray(0).Value = Convert.ToInt32(Session("ParentID").ToString)
                        prmArray(0).Value = Convert.ToInt32(Session("CustIndID").ToString)
                        prmArray(0).Direction = ParameterDirection.Input

                        prmArray(1) = New SqlParameter
                        prmArray(1).ParameterName = "@ContestYear"
                        prmArray(1).Value = Application("ContestYear")
                        prmArray(1).Direction = ParameterDirection.Input

                        Try
                            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetContestCharity", prmArray)
                        Catch se As SqlException
                            lblMessage.Text = se.Message
                            Return
                        End Try
                        If ds Is Nothing Then
                            Return
                        End If
                        If (ds.Tables.Count <= 0) Then
                            Return
                        End If
                        If (ds.Tables(0).Rows.Count <= 0) Then
                            Return
                        End If
                        If (ds.Tables(0).Rows(0)("PaymentDate").ToString <> "") Then
                            Session("Donation") = CType(0, Decimal)
                            Session("DONATIONFOR") = ""
                            'Response.Redirect("reg_Pay.aspx")
                        Else
                            Dim nDonation As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("Donationamount").ToString)
                            txtDonation.Text = nDonation.ToString("0.00")
                            'If (Not (ddlDonationFor.Items.FindByValue(ds.Tables(0).Rows(0)("DonationFor").ToString)) Is Nothing) Then
                            '    ddlDonationFor.SelectedIndex = -1
                            '    ddlDonationFor.Items.FindByValue(ds.Tables(0).Rows(0)("DonationFor").ToString).Selected = True
                            'End If
                            If (ds.Tables(0).Rows(0)("SupportFor1").ToString <> "1") Then
                                'cbSupport1.Checked = True
                            End If
                            'If (ds.Tables(0).Rows(0)("SupportFor2").ToString <> "1") Then
                            '    cbSupport2.Checked = True
                            'End If
                            'If (Not (ddlChapterLiaison.Items.FindByValue(ds.Tables(0).Rows(0)("NSFINDIACHAPTERINFO").ToString)) Is Nothing) Then
                            '    ddlChapterLiaison.SelectedIndex = -1
                            '    ddlChapterLiaison.Items.FindByValue(ds.Tables(0).Rows(0)("NSFINDIACHAPTERINFO").ToString).Selected = True
                            'End If
                            '*** Commented by venkat ambati because it is no longer needed from 2006 Registration
                            'If (Not (ddlVolunteer.Items.FindByValue(ds.Tables(0).Rows(0)("VOLUNTEERINTERESTAREA").ToString)) Is Nothing) Then
                            '    ddlVolunteer.SelectedIndex = -1
                            '    ddlVolunteer.Items.FindByValue(ds.Tables(0).Rows(0)("VOLUNTEERINTERESTAREA").ToString).Selected = True
                            'End If                   
                        End If
                    Else
                        lblMessage.Text = (lblMessage.Text + "<BR>Error: The required session vars CustIndID  not available")
                        Return
                    End If
                Else
                    txtDonation.Text = 0
                    HlnkTellme.Visible = False
                    lblInv.Visible = False
                    'dllEGames.SelectedIndex = dllEGames.Items.IndexOf(dllEGames.Items.FindByText("None"))
                    Dim conn As New SqlConnection(Application("ConnectionString"))
                    Dim strquery As String = "Select * from Contest_Charity where ContestYear = " & Application("ContestYear")
                    Try
                        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strquery)
                    Catch se As SqlException
                        lblMessage.Text = se.Message
                        Return
                    End Try
                    If ds Is Nothing Then
                        Return
                    End If
                    If (ds.Tables.Count <= 0) Then
                        Return
                    End If
                    If (ds.Tables(0).Rows.Count <= 0) Then
                        Return
                    End If
                    If (ds.Tables(0).Rows(0)("PaymentDate").ToString <> "") Then
                        Session("Donation") = CType(0, Decimal)
                        Session("DONATIONFOR") = ""
                    Else
                        Dim nDonation As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("Donationamount").ToString)
                        txtDonation.Text = nDonation.ToString("0.00")
                    End If
                End If
            End If
        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>

        Private Sub InitializeComponent()

        End Sub

        Private Sub lbContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbContinue.Click
            'If (Panel1.Visible _
            '            AndAlso (ddlDonationFor.SelectedIndex = 0)) Then
            If Requiredfieldvalidator4.IsValid = False And Session("entryToken") = "Volunteer" Then
                Exit Sub
            End If
            If ((CInt(Me.txtDonation.Text) > 0) AndAlso (ddlDonationFor.SelectedIndex = 0)) Then
                DonationForLabel.Visible = True
                DonationForLabel.ForeColor = Color.Red
                DonationForLabel.Text = "Please select an option."
            Else
                'get member's chapterid
                Dim chapterId As Integer = 0
                Dim currentDate As Date = Now
                '  Dim conn As New SqlConnection(Application("ConnectionString"))

                Dim EID As Integer = ddlEvent.SelectedItem.Value
                Dim Eve As String = ddlEvent.SelectedItem.ToString
                Dim access As String = "" 'dllEGames.SelectedItem.Value
                Dim anonymous As String = dllAnonymous.SelectedItem.Value.ToString
                Dim Matching As String = ddlmatching.SelectedItem.Value.ToString
                Dim inmemory As String = txtMemoryOf.Text

                If access = "None" Then
                    access = ""
                    'access = Convert.ToInt32(access)
                Else
                    access = access.ToString
                End If

                Session("@EID") = EID
                Session("@EVE") = Eve
                Session("@Anonymous1") = anonymous
                Session("@Access1") = access
                Session("@Matching") = Matching
                Session("@inmemory") = inmemory


                If Session("entryToken") = "Parent" Or Session("entryToken") = "Donor" Then
                    If ((Not Session("CustIndID") Is Nothing)) Then
                        Dim memberId As Integer = CInt(Session("CustIndID"))
                        ViewState("MemberID") = memberId
                        Dim eventId As Integer = 0
                        ViewState("EventID") = eventId

                        If (Not Session("EventID") Is Nothing) Then
                            eventId = CInt(Session("EventID").ToString())
                        End If
                        SP()
                        ''If Session("entryToken").ToString.ToUpper = "DONOR" Then
                        ''    Session("EventID") = 11
                        ''    eventId = CInt(Session("EventID").ToString())
                        ''End If
                        'Try
                        '    Dim ret As Object = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_getIndspouseChapterId", New SqlParameter("@autoMemberId", memberId))
                        '    If (Not ret Is Nothing) Then
                        '        chapterId = CType(ret, Integer)
                        '    End If
                        '    Session("CustIndChapterID") = chapterId
                        'Catch se As SqlException
                        '    '? if there is no chapterid
                        '    lblMessage.Text = se.Message
                        '    lblMessage.Text = (lblMessage.Text + "<BR>Error:There is no Chapter associated with this loginid")
                        '    Return
                        'End Try

                        ''DonationForLabel.Visible = False
                        'Dim strquery As String = "DELETE FROM Contest_Charity WHERE MEMBERID=<MEMBERID> and ContestYear=<CONTESTYEAR>;"
                        'strquery = (strquery + " INSERT INTO Contest_Charity(MEMBERID, ContestYear, DonationAmount, DonationFor, SupportFor1, Support" & _
                        '"For2, NSFIndiaChapterInfo, VolunteerInterestArea,EventId,ChapterId,createdDate)")
                        'strquery = (strquery + " VALUES(<MEMBERID>, <CONTESTYEAR>, <DONATIONAMOUNT>, '<DONATIONFOR>', <SUPPORTFOR1>, <SUPPORTFOR2>, '" & _
                        '"<NSFINDIACHAPTERINFO>', '<VOLUNTEERINTERESTAREA>','<EVENTID>',<CHAPTERID>,<CREATEDDATE>)")

                        'strquery = strquery.Replace("<MEMBERID>", memberId.ToString)
                        'strquery = strquery.Replace("<CONTESTYEAR>", Application("ContestYear").ToString)
                        'strquery = strquery.Replace("<DONATIONAMOUNT>", txtDonation.Text.ToString)
                        'Session("DONATIONFOR") = ""
                        'If (ddlDonationFor.SelectedIndex >= 0) Then
                        '    strquery = strquery.Replace("<DONATIONFOR>", ddlDonationFor.SelectedItem.Value.ToString)
                        '    Session("DONATIONFOR") = ddlDonationFor.SelectedItem.Value.ToString
                        'Else
                        '    strquery = strquery.Replace("<DONATIONFOR>", "")
                        'End If
                        ''If cbSupport1.Checked Then
                        ''    strquery = strquery.Replace("<SUPPORTFOR1>", "1")
                        ''Else
                        'strquery = strquery.Replace("<SUPPORTFOR1>", "0")
                        ''End If
                        ''If cbSupport2.Checked Then
                        ''    strquery = strquery.Replace("<SUPPORTFOR2>", "1")
                        ''Else
                        'strquery = strquery.Replace("<SUPPORTFOR2>", "0")
                        ''End If
                        ''If (ddlChapterLiaison.SelectedIndex >= 0) Then
                        ''strquery = strquery.Replace("<NSFINDIACHAPTERINFO>", ddlChapterLiaison.SelectedItem.Value.ToString)
                        ''Else
                        'strquery = strquery.Replace("<NSFINDIACHAPTERINFO>", "''")
                        ''End If
                        'strquery = strquery.Replace("<VOLUNTEERINTERESTAREA>", "")
                        'strquery = strquery.Replace("<EVENTID>", Session("@EID"))
                        'strquery = strquery.Replace("<CHAPTERID>", chapterId.ToString())
                        'strquery = strquery.Replace("<CREATEDDATE>", "'" + System.DateTime.Today.ToShortDateString + "'")
                        'Try
                        '    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strquery)
                        'Catch se As SqlException
                        '    'lblMessage.Text = strquery
                        '    lblMessage.Text = se.Message
                        '    lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                        '    Return
                        'End Try


                        'Session("Donation") = Convert.ToDecimal(txtDonation.Text.ToString)
                        ''  If Application("EventID") = "1" Then
                        ''Response.Redirect("TermsAndConditions.aspx")
                        ''Else                   
                        'Response.Redirect("Donor_Pay.aspx")
                        ''End If
                    Else
                        lblMessage.Text = (lblMessage.Text + "<BR>Error: The required session var CustIndID is not available")
                        Return
                    End If
                ElseIf ((Not Session("CustIndID") Is Nothing)) Then
                    Dim memberId As Integer = CInt(Session("CustIndID"))
                    ViewState("MemberID") = memberId
                    Dim eventId As Integer = 0
                    ViewState("EventID") = eventId
                    If (Not Session("EventID") Is Nothing) Then
                        eventId = CInt(Session("EventID").ToString())
                    End If
                    SP()
                Else
                    Dim memberId As Integer = CInt(Session("LoginID"))
                    ViewState("MemberID") = memberId
                    Dim eventId As Integer = 0

                    If (Not Session("EventID") Is Nothing) Then
                        eventId = CInt(Session("EventID").ToString())
                    End If
                    SP()
                End If
            End If
        End Sub

        Private Sub txtDonation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDonation.TextChanged
            lblMessage.ForeColor = Color.Blue
            lblMessage.Text = ""
            txtDonation.Text = txtDonation.Text.Trim
            Dim fDonation As Double = 0
            Try
                fDonation = Convert.ToDouble(txtDonation.Text)
                rfvDonationFor.Enabled = (fDonation > 0)
                Panel1.Visible = (fDonation > 0)
            Catch exception As System.Exception
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "Invalid number, please enter 0 or an amount of your choice"
            End Try
        End Sub

        Private Sub LoadDonationPurpose()
            strSqlQuery = "SELECT PurposeCode ,PurposeDesc FROM DonationPurpose   "
            Dim drPurpose As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drPurpose = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
            While drPurpose.Read()
                ddlDonationFor.Items.Add(New ListItem(drPurpose(1).ToString(), drPurpose(0).ToString()))
            End While
            ddlDonationFor.Items.Insert(0, New ListItem("Select Purpose", ""))
            ddlDonationFor.SelectedIndex = 4
        End Sub

        Private Sub LoadDonationEvent()
            'strSqlQuery = "SELECT EventCode,Name FROM Event "
            Dim drEvents1 As DataSet
            'Dim drEvents As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))

            If (Session("EventID") > 0 And Session("EventID") <> 11) Then
                strSqlQuery = "Select EventID,EventCode, Name from event where eventid = " & Session("EventID")
                drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)
                Dim Count1 As Integer = 0
                If (drEvents1.Tables(0).Rows.Count > 0) Then
                    Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

                    Dim i As Integer
                    For i = 0 To Count1 - 1
                        Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                        Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                        ddlEvent.DataTextField = "Name"
                        ddlEvent.DataValueField = "EventID"
                        Dim li As ListItem = New ListItem(Name)
                        li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                        li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                        ddlEvent.Items.Add(li)
                    Next i

                    ddlEvent.SelectedIndex = Session("EventID")
                    ddlEvent.Enabled = False
                End If

            Else
                'Dim ass As Integer = Session("EventID")
                strSqlQuery = "SELECT EventID,EventCode,Name FROM Event where EventID in ( 5 ,6, 9, 11, 12,10) " '4,
                'Dim drEvents As SqlDataReader
                drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)
                Dim Count1 As Integer = 0
                If (drEvents1.Tables(0).Rows.Count > 0) Then
                    Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

                    Dim i As Integer
                    For i = 0 To Count1 - 1
                        Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                        Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                        ddlEvent.DataTextField = "Name"
                        ddlEvent.DataValueField = "EventID"
                        Dim li As ListItem = New ListItem(Name)
                        li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                        li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                        ddlEvent.Items.Add(li)
                    Next i
                    ddlEvent.Items.Insert(0, New ListItem("Select Purpose", "-1"))
                    ddlEvent.SelectedIndex = 0

                End If
                'Else
                '    strSqlQuery = "SELECT EventID,EventCode,Name FROM Event "
                '    'Dim drEvents As SqlDataReader
                '    drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)
                '    Dim Count1 As Integer = 0
                '    If (drEvents1.Tables(0).Rows.Count > 0) Then
                '        Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

                '        Dim i As Integer
                '        For i = 0 To Count1 - 1
                '            Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                '            Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                '            ddlEvent.DataTextField = "Name"
                '            ddlEvent.DataValueField = "EventID"
                '            Dim li As ListItem = New ListItem(Name)
                '            li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                '            li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                '            ddlEvent.Items.Add(li)
                '        Next i

                '    End If

            End If

        End Sub

        Private Sub LoadEducationGame()
            strSqlQuery = "select ProductID, Name from Product where EventID =4 and Status = 'O'"
            Dim drGames As DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim Count1 As Integer = 0


            drGames = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)

            If (drGames.Tables(0).Rows.Count > 0) Then
                Count1 = Convert.ToInt32(drGames.Tables(0).Rows.Count.ToString())

                Dim i As Integer
                For i = 0 To Count1 - 1
                    Dim Name As String = drGames.Tables(0).Rows(i)(1).ToString()
                    Dim Product As String = drGames.Tables(0).Rows(i)(0).ToString()
                    dllEGames.DataValueField = "Product"
                    dllEGames.DataTextField = "Name"
                    Dim li As ListItem = New ListItem(Name)
                    li.Text = drGames.Tables(0).Rows(i)(1).ToString()
                    li.Value = drGames.Tables(0).Rows(i)(0).ToString()

                    dllEGames.Items.Add(li)
                Next i

                dllEGames.Items.Insert(0, New ListItem("Select Game", ""))
                dllEGames.Items.Insert(1, "None")
                dllEGames.SelectedIndex = 0

            End If

        End Sub

        Protected Sub lnkback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkback.Click
            'lnkback.CausesValidation = False
            If Session("entryToken") = "Parent" Then
                Response.Redirect("UserFunctions.aspx")
            ElseIf Session("entryToken") = "Donor" Then
                Response.Redirect("DonorFunctions.aspx")
            ElseIf Session("entryToken") = "Volunteer" Then
                Response.Redirect("VolunteerFunctions.aspx")
            End If
        End Sub

        Protected Sub SP()
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim chapterId As Integer = 0
            Try
                Dim ret As Object = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_getIndspouseChapterId", New SqlParameter("@autoMemberId", ViewState("MemberID")))
                If (Not ret Is Nothing) Then
                    chapterId = CType(ret, Integer)
                End If
                Session("CustIndChapterID") = chapterId
            Catch se As SqlException
                lblMessage.Text = se.Message
                lblMessage.Text = (lblMessage.Text + "<BR>Error:There is no Chapter associated with this loginid")
                Return
            End Try
            Dim strquery As String = "DELETE FROM Contest_Charity WHERE MEMBERID=<MEMBERID> and ContestYear=" & DateTime.Now.Year & ";"
            strquery = (strquery + " INSERT INTO Contest_Charity(MEMBERID, ContestYear, DonationAmount, DonationFor, SupportFor1, Support" & _
            "For2, NSFIndiaChapterInfo, VolunteerInterestArea,EventId,ChapterId,createdDate)")
            strquery = (strquery + " VALUES(<MEMBERID>, <CONTESTYEAR>, <DONATIONAMOUNT>, '<DONATIONFOR>', <SUPPORTFOR1>, <SUPPORTFOR2>, '" & _
            "<NSFINDIACHAPTERINFO>', '<VOLUNTEERINTERESTAREA>','<EVENTID>',<CHAPTERID>,<CREATEDDATE>)")

            strquery = strquery.Replace("<MEMBERID>", ViewState("MemberID"))
            strquery = strquery.Replace("<CONTESTYEAR>", Application("ContestYear").ToString)
            strquery = strquery.Replace("<DONATIONAMOUNT>", txtDonation.Text.ToString)
            Session("DONATIONFOR") = ""
            If (ddlDonationFor.SelectedIndex >= 0) Then
                strquery = strquery.Replace("<DONATIONFOR>", ddlDonationFor.SelectedItem.Value.ToString)
                Session("DONATIONFOR") = ddlDonationFor.SelectedItem.Value.ToString
            Else
                strquery = strquery.Replace("<DONATIONFOR>", "")
            End If
            strquery = strquery.Replace("<SUPPORTFOR1>", "0")
            strquery = strquery.Replace("<SUPPORTFOR2>", "0")
            strquery = strquery.Replace("<NSFINDIACHAPTERINFO>", "''")
            strquery = strquery.Replace("<VOLUNTEERINTERESTAREA>", "")
            strquery = strquery.Replace("<EVENTID>", Session("@EID"))
            strquery = strquery.Replace("<CHAPTERID>", chapterId.ToString())
            strquery = strquery.Replace("<CREATEDDATE>", "GetDate()")
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strquery)
            Catch se As SqlException
                lblMessage.Text = se.Message
                lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                Return
            End Try


            Session("Donation") = Convert.ToDecimal(txtDonation.Text.ToString)
            If Session("Eventid") = 4 Then
                'If dllEGames.SelectedIndex = 1 Then
                '    lblerror.Text = "None is not an acceptable choice."
                'Else
                '    Response.Redirect("Donor_Pay.aspx")
                'End If
            Else
                Session("DoPay") = "DonorDonate"
                Response.Redirect("Donor_Pay.aspx")
            End If
        End Sub
    End Class
End Namespace