﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class ContestTeam
    Inherits System.Web.UI.Page

    Dim dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        'If (Session("RoleID") = 1 Or Session("RoleID") = 2) Then
        ''To Add Values to DropDownList

        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            ' GetProductGroup(ddlProductGroup)
            ' GetDate()
            'LoadDisplayTime()
        End If
        ' End If
    End Sub
    Private Sub GetContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        'ddlPurpose.DataTextField = "Purpose"
        'ddlPurpose.DataValueField = "Purpose"
        ddlPurpose.DataBind()
        ' ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByText("Contests"))
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 0
    End Sub

    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        'ddlChapter.DataTextField = "ChapterCode"
        'ddlChapter.DataValueField = "ChapterId"
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup(ByVal ddlObject As DropDownList)
        Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlObject.DataSource = ds
        'ddlProductGroup.DataTextField = "ProductGroup"
        'ddlProductGroup.DataValueField = "ProductGroupId"
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, "Select ProductGroup")
        ddlObject.SelectedIndex = 0
    End Sub
    Private Sub GetDate()
        Dim StrDateSQL As String = ""
        StrDateSQL = "Select Distinct  convert(VarChar(10), Date, 101) as Date  From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDateSQL)
        ddlDate.DataSource = ds
        'ddlDate.DataTextField = "Date"
        'ddlDate.DataValueField = "Date"
        ddlDate.DataBind()
        ddlDate.Items.Insert(0, "Select ContestDate")
        ddlDate.SelectedIndex = 0
    End Sub
    Public Sub LoadDisplayTime()
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "6:00:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "6:00:00 AM " To "8:00:00 PM "
        While StartTime <= "8:00:00 PM "

            dr = dt.NewRow()
            dr("ddlText") = StartTime.ToString("hh:mm tt")
            dr("ddlValue") = StartTime.ToString("hh:mm tt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddMinutes(15)
        End While

        ddlStartTime.DataSource = dt
        ddlStartTime.DataTextField = "ddlText"
        ddlStartTime.DataValueField = "ddlValue"
        ddlStartTime.DataBind()

        ddlStartTime.Items.Insert(0, "Select Start time")
        ddlStartTime.SelectedIndex = 0

        ddlEndTime.DataSource = dt
        ddlEndTime.DataTextField = "ddlText"
        ddlEndTime.DataValueField = "ddlValue"
        ddlEndTime.DataBind()
        ddlEndTime.Items.Insert(0, "Select End time")
        ddlEndTime.SelectedIndex = 0

    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            'GetProductGroup(ddlProductGroup)
            'GetDate()
            ddlPurpose.SelectedIndex = 0
            ClearDropDowns()
            LoadRolesVol()
            'LoadBldgID()
        Else
            GetChapter(ddlChapter)
        End If
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        If ddlChapter.SelectedValue = 1 Then
            ddlEvent.SelectedValue = 1
        End If
        'GetProductGroup(ddlProductGroup)
        'GetDate()
        ddlPurpose.SelectedIndex = 0
        ClearDropDowns()
        LoadRolesVol()
        'LoadBldgID()
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        ClearDropDowns()
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event/Chapter"
            Exit Sub
        End If

        LoadRoomSchData()
    End Sub
    Private Sub LoadRoomSchData()

        Dim SQLRoomsch As String = "Select * from Roomschedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterId=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomsch)
        Try

            GetDate()
            GetProductGroup(ddlProductGroup) '
            LoadBldgID()
        
            lblAddUPdate.Text = ""

        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged

        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        LoadProduct(ddlProductGroup, ddlProduct)
        LoadPhase()

    End Sub
    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ddlProductObj As DropDownList)

        Dim StrSQL As String = "Select Distinct ProductCode,ProductId from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlObject.SelectedValue & "" 'and BldgId='" & ddlBldgID.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlProductObj.DataSource = ds
        'ddlProductObj.DataTextField = "ProductCode"
        'ddlProductObj.DataValueField = "ProductId"
        ddlProductObj.DataBind()

    End Sub

    Private Sub LoadPhase()

        Dim StrSQL As String = "Select Distinct Phase from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlPhase.DataSource = ds
        'ddlPhase.DataTextField = "Phase"
        'ddlPhase.DataValueField = "Phase"
        ddlPhase.DataBind()

        ddlPhase.Items.Insert(0, New ListItem("Select Phase ", "0"))
        ddlPhase.SelectedIndex = 0
        LoadDGRoomSchedule()

    End Sub

    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        TrCopy.Visible = False
        ddlRoomNo.Enabled = True
        ddlRoomNo.Items.Clear()
        LoadRoomNumber(ddlBldgID.SelectedValue)
        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
    End Sub
    Private Sub LoadRoomNumber(ByVal ddlBldgIdVal As String)

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo,RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlSeqNo.DataSource = ds
        ddlSeqNo.DataBind()

        ddlSeqNo.Items.Insert(0, "Select SeqNo")
        ddlSeqNo.SelectedIndex = 0

        ddlRoomNo.DataSource = ds
        'ddlRoomNo.DataTextField = "RoomNumber"
        'ddlRoomNo.DataValueField = "RoomNumber"
        ddlRoomNo.DataBind()

        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
        ddlRoomNo.SelectedIndex = 0
        'GetProductGroup()
        'GetDate()
    End Sub

    Protected Sub ddlSeqNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSeqNo.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        'ddlSeqNo.SelectedIndex = ddlRoomNo.SelectedIndex
        Dim StrSeqNoSQl As String = "Select Distinct RoomNumber,StartTime,EndTime from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and SeqNo='" & ddlSeqNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSeqNoSQl)

        ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber")))

        'ddlStartTime.DataSource = ds
        'ddlStartTime.DataBind()

        'ddlEndTime.DataSource = ds
        'ddlEndTime.DataBind()

        ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
        ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))

        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        'ddlSeqNo.SelectedIndex = ddlRoomNo.SelectedIndex
        Dim StrRoomNoSQl As String = "Select Distinct SeqNo,StartTime,EndTime from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo")))

        'ddlStartTime.DataSource = ds
        'ddlStartTime.DataBind()

        'ddlEndTime.DataSource = ds
        'ddlEndTime.DataBind()


        ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
        ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))

        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub LoadBldgID()
        ddlRoomNo.Enabled = True

        Dim StrBldgSQlEval As String = ""
        Dim StrBldgSQl As String = ""

        StrBldgSQl = "Select Distinct BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)

        ' ddlRoomNo.Items.Clear()
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)
        ddlBldgID.DataSource = ds
        ddlBldgID.DataTextField = "BldgID"
        ddlBldgID.DataValueField = "BldgID"
        ddlBldgID.DataBind()

        ddlBldgID.Items.Insert(0, "Select BldgID")
        ddlBldgID.SelectedIndex = 0
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Or ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please do all the Selections"
            Exit Sub
        End If
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub LoadRoleData()
        Dim SQLContestTeam As String = ""
        Dim ds As DataSet
        Try

            SQLContestTeam = "Select * From ContestTeamSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLContestTeam)
    
            If ds.Tables(0).Rows.Count > 0 Then
                ' ddlStartTime.SelectedValue = ds.Tables(0).Rows(0)("StartTime")
                ' ddlEndTime.SelectedValue = ds.Tables(0).Rows(0)("EndTime")
                ddlRoomMc.SelectedIndex = ddlRoomMc.Items.IndexOf(ddlRoomMc.Items.FindByValue(ds.Tables(0).Rows(0)("RMcMemberID").ToString))
                ddlPronouncer.SelectedIndex = ddlPronouncer.Items.IndexOf(ddlPronouncer.Items.FindByValue(ds.Tables(0).Rows(0)("PronMemberID")))
                ddlChiefJudge.SelectedIndex = ddlChiefJudge.Items.IndexOf(ddlChiefJudge.Items.FindByValue(ds.Tables(0).Rows(0)("CJMemberID")))
                ddlAssociateJudge.SelectedIndex = ddlAssociateJudge.Items.IndexOf(ddlAssociateJudge.Items.FindByValue(ds.Tables(0).Rows(0)("AJMemberID")))
                ddlLaptopJudge.SelectedIndex = ddlLaptopJudge.Items.IndexOf(ddlLaptopJudge.Items.FindByValue(ds.Tables(0).Rows(0)("LTJudMemberID")))
                ddlDictHandler.SelectedIndex = ddlDictHandler.Items.IndexOf(ddlDictHandler.Items.FindByValue(ds.Tables(0).Rows(0)("DictHMemberID")))
                ddlGrading.SelectedIndex = ddlGrading.Items.IndexOf(ddlGrading.Items.FindByValue(ds.Tables(0).Rows(0)("GradMemberID")))
                ddlProctor.SelectedIndex = ddlProctor.Items.IndexOf(ddlProctor.Items.FindByValue(ds.Tables(0).Rows(0)("ProcMemberID")))
            Else
                SQLContestTeam = "Select * From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "" ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLContestTeam)
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlStartTime.DataSource = ds '.SelectedValue = ds.Tables(0).Rows(0)("StartTime")
                    ddlStartTime.DataBind()
                    ddlEndTime.DataSource = ds '.SelectedValue = ds.Tables(0).Rows(0)("EndTime")
                    ddlEndTime.DataBind()
                Else
                    ddlStartTime.SelectedIndex = 0
                    ddlEndTime.SelectedIndex = 0
                    ClearRoles()
                End If
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub LoadDGRoomSchedule()
        DGRoomSchedule.Visible = True
        Dim dgItem As DataGridItem
        Dim lblCapcty As Label
        Dim lbtnRemoveCont As LinkButton
        Dim lbtnEditCont As LinkButton
        Dim lbl_RoomMc As Label
        Dim lbl_Pronouncer As Label
        Dim lbl_AssoJudge As Label
        Dim lbl_ChiefJudge As Label
        Dim lbl_LTJudge As Label
        Dim lbl_DictH As Label
        Dim lbl_Grading As Label
        Dim lbl_Proctor As Label
        Dim lbl_ContTeamID As Label
        Dim i As Integer = 0
        Dim StrRoomSch As String = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomSch)

        DGRoomSchedule.DataSource = ds
        DGRoomSchedule.DataBind()
        Dim ds1, ds2 As DataSet

        Try
            For Each dgItem In DGRoomSchedule.Items
                lblCapcty = dgItem.FindControl("lblCapacity")
                ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Capacity from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'")
                lblCapcty.Text = ds1.Tables(0).Rows(0)("Capacity")

                lbtnRemoveCont = dgItem.FindControl("lbtnRemove")
                lbtnEditCont = dgItem.FindControl("lbtnEdit")

                lbl_RoomMc = dgItem.FindControl("lblRoomMc")
                lbl_Pronouncer = dgItem.FindControl("lblPronouncer")
                lbl_AssoJudge = dgItem.FindControl("lblAssociateJudge")
                lbl_ChiefJudge = dgItem.FindControl("lblChiefJudge")
                lbl_LTJudge = dgItem.FindControl("lblLaptopJudge")
                lbl_DictH = dgItem.FindControl("lblDictHandler")
                lbl_Grading = dgItem.FindControl("lblGrading")
                lbl_Proctor = dgItem.FindControl("lblProctor")
                lbl_ContTeamID = dgItem.FindControl("lblContTeamID")

                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") > 0 Then

                    ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ContTeamID,IsNull(RoomMc,'') as RoomMc,IsNull(Pronouncer,'') as Pronouncer,IsNull(AssoJudge,'') as AssoJudge,IsNull(ChiefJudge,'') as ChiefJudge,IsNull(LTJudge,'') as LTJudge,IsNull(DictH,'')as DictH,IsNull(Grading,'') as Grading,IsNull(Proctor,'') as Proctor From ContestTeamSchedule  Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'") ' " & IIf(ddlBldgID.SelectedIndex = 0, "", "  ''''' ") & IIf(ddlRoomNo.SelectedIndex <= 0, "", "
                    If ds2.Tables(0).Rows.Count > 0 Then
                        ' For j As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                        lbl_RoomMc.Text = ds2.Tables(0).Rows(0)("RoomMc")
                        lbl_Pronouncer.Text = ds2.Tables(0).Rows(0)("Pronouncer")
                        lbl_AssoJudge.Text = ds2.Tables(0).Rows(0)("AssoJudge")
                        lbl_ChiefJudge.Text = ds2.Tables(0).Rows(0)("ChiefJudge")
                        lbl_LTJudge.Text = ds2.Tables(0).Rows(0)("LTJudge")
                        lbl_DictH.Text = ds2.Tables(0).Rows(0)("DictH")
                        lbl_Grading.Text = ds2.Tables(0).Rows(0)("Grading")
                        lbl_Proctor.Text = ds2.Tables(0).Rows(0)("Proctor")
                        lbl_ContTeamID.Text = ds2.Tables(0).Rows(0)("ContTeamID")
                        ' Next
                    Else
                        lbl_RoomMc.Text = ""
                        lbl_Pronouncer.Text = ""
                        lbl_AssoJudge.Text = ""
                        lbl_ChiefJudge.Text = ""
                        lbl_LTJudge.Text = ""
                        lbl_DictH.Text = ""
                        lbl_Grading.Text = ""
                        lbl_Proctor.Text = ""
                        lbl_ContTeamID.Text = ""
                    End If
                Else
                    lbl_RoomMc.Text = ""
                    lbl_Pronouncer.Text = ""
                    lbl_AssoJudge.Text = ""
                    lbl_ChiefJudge.Text = ""
                    lbl_LTJudge.Text = ""
                    lbl_DictH.Text = ""
                    lbl_Grading.Text = ""
                    lbl_Proctor.Text = ""
                    lbl_ContTeamID.Text = ""
                End If

                If (lbl_ContTeamID.Text <> "") Then
                    lbtnRemoveCont.Enabled = True
                    lbtnEditCont.Enabled = True
                Else
                    lbtnRemoveCont.Enabled = False
                    lbtnEditCont.Enabled = False
                End If
                i = i + 1
            Next
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub LoadRolesVol()

        LoadRoleNames(ddlRoomMc, "RoleID = 86")
        LoadRoleNames(ddlPronouncer, "(RoleID = 15 Or RoleID = 16)")
        LoadRoleNames(ddlChiefJudge, "(RoleID = 15 Or RoleID = 16)")
        LoadRoleNames(ddlAssociateJudge, "(RoleID = 17 Or RoleID = 18)")
        LoadRoleNames(ddlLaptopJudge, "(RoleID = 17 Or RoleID = 18)")
        LoadRoleNames(ddlDictHandler, "RoleID = 19")
        LoadRoleNames(ddlGrading, "RoleID = 21")
        LoadRoleNames(ddlProctor, "(RoleID = 15 Or RoleID = 16 Or RoleID = 20)")

    End Sub
    Private Sub LoadRoleNames(ByVal ddlObject As DropDownList, ByVal Role As String)
        Dim SQLRoleStr As String = ""
        If ddlEvent.SelectedValue = 1 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +''+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Finals='Y' and " & Role & " )"
        ElseIf ddlEvent.SelectedValue = 2 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +''+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID > 1 and ChapterID=" & ddlChapter.SelectedValue & " and " & Role & " )"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleStr)
        ddlObject.DataSource = ds
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, New ListItem("Select", 0))
    End Sub
    Private Sub ClearRoles()
        ddlRoomMc.Items.Clear()
        ddlPronouncer.Items.Clear()
        ddlChiefJudge.Items.Clear()
        ddlAssociateJudge.Items.Clear()
        ddlLaptopJudge.Items.Clear()
        ddlDictHandler.Items.Clear()
        ddlGrading.Items.Clear()
        ddlProctor.Items.Clear()
        'LoadRolesVol()
        ' LoadRoleData()
    End Sub
    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click

        Dim SQLContTeamInsert As String = ""
        Dim SQLContTeamUpdate As String = ""
        Dim SQLContTeamEval As String = ""
        Dim SQLContTeamExec As String = ""

        lblAddUPdate.Text = ""

        If ddlEvent.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Purpose"
            Exit Sub
        ElseIf ddlDate.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Date"
            Exit Sub
        ElseIf ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select ProductGroup"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Phase"
            Exit Sub
            'ElseIf ddlStartTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select StartTime"
            '    Exit Sub
            'ElseIf ddlEndTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select EndTime"
            '    Exit Sub
        ElseIf ddlBldgID.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select BldgID"
            Exit Sub
        ElseIf ddlRoomNo.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select RoomNo"
            Exit Sub
        End If
        Try
            SQLContTeamInsert = "Insert into ContestTeamSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,StartTime,EndTime,"
            SQLContTeamInsert = SQLContTeamInsert & "RMcMemberID,RoomMc,PronMemberID,Pronouncer,CJMemberID,ChiefJudge,AJMemberID,AssoJudge,LTJudMemberID,LTJudge,DictHMemberID,DictH,GradMemberID,Grading,ProcMemberID,Proctor,CreatedBy,CreatedDate)"
            SQLContTeamInsert = SQLContTeamInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ddlBldgID.SelectedItem.Text & "'," & ddlSeqNo.SelectedValue & ",'" & ddlRoomNo.SelectedItem.Text & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ",'" & ddlStartTime.SelectedItem.Text & "','" & ddlEndTime.SelectedItem.Text & "',"
            SQLContTeamInsert = SQLContTeamInsert & IIf(ddlRoomMc.SelectedValue = 0, 0, ddlRoomMc.SelectedValue) & ",'" & IIf(ddlRoomMc.SelectedValue = 0, "", ddlRoomMc.SelectedItem.Text) & "'," & IIf(ddlPronouncer.SelectedValue = 0, 0, ddlPronouncer.SelectedValue) & ",'" & IIf(ddlPronouncer.SelectedValue = 0, "", ddlPronouncer.SelectedItem.Text) & "'," & IIf(ddlChiefJudge.SelectedValue = 0, 0, ddlChiefJudge.SelectedValue) & ",'" & IIf(ddlChiefJudge.SelectedValue = 0, "", ddlChiefJudge.SelectedItem.Text) & "'," & IIf(ddlAssociateJudge.SelectedValue = 0, 0, ddlAssociateJudge.SelectedValue) & ",'"
            SQLContTeamInsert = SQLContTeamInsert & IIf(ddlAssociateJudge.SelectedValue = 0, "", ddlAssociateJudge.SelectedItem.Text) & "'," & IIf(ddlLaptopJudge.SelectedValue = 0, 0, ddlLaptopJudge.SelectedValue) & ",'" & IIf(ddlLaptopJudge.SelectedValue = 0, "", ddlLaptopJudge.SelectedItem.Text) & "'," & IIf(ddlDictHandler.SelectedValue = 0, 0, ddlDictHandler.SelectedValue) & ",'" & IIf(ddlDictHandler.SelectedValue = 0, "", ddlDictHandler.SelectedItem.Text) & "'," & IIf(ddlGrading.SelectedValue = 0, 0, ddlGrading.SelectedValue) & ",'" & IIf(ddlGrading.SelectedValue = 0, "", ddlGrading.SelectedItem.Text) & "'," & IIf(ddlProctor.SelectedValue = 0, 0, ddlProctor.SelectedValue) & ",'" & IIf(ddlProctor.SelectedValue = 0, "", ddlProctor.SelectedItem.Text) & "'," & Session("LoginID") & ",GetDate() )"

            SQLContTeamUpdate = "Update ContestTeamSchedule Set Date='" & ddlDate.SelectedItem.Text & "' ,StartTime='" & ddlStartTime.SelectedItem.Text & "',EndTime='" & ddlEndTime.SelectedItem.Text & "',RMcMemberID = " & IIf(ddlRoomMc.SelectedValue = 0, 0, ddlRoomMc.SelectedValue) & ",RoomMc= '" & IIf(ddlRoomMc.SelectedValue = 0, "", ddlRoomMc.SelectedItem.Text) & "',PronMemberID=" & IIf(ddlPronouncer.SelectedValue = 0, 0, ddlPronouncer.SelectedValue) & ",Pronouncer='" & IIf(ddlPronouncer.SelectedValue = 0, "", ddlPronouncer.SelectedItem.Text) & "',CJMemberID=" & IIf(ddlChiefJudge.SelectedValue = 0, 0, ddlChiefJudge.SelectedValue) & ",ChiefJudge='" & IIf(ddlChiefJudge.SelectedValue = 0, "", ddlChiefJudge.SelectedItem.Text) & "',AJMemberID=" & IIf(ddlAssociateJudge.SelectedValue = 0, 0, ddlAssociateJudge.SelectedValue) & ",AssoJudge='" & IIf(ddlAssociateJudge.SelectedValue = 0, "", ddlAssociateJudge.SelectedItem.Text) & "',"
            SQLContTeamUpdate = SQLContTeamUpdate & " LTJudMemberID=" & IIf(ddlLaptopJudge.SelectedValue = 0, 0, ddlLaptopJudge.SelectedValue) & ",LTJudge='" & IIf(ddlLaptopJudge.SelectedValue = 0, "", ddlLaptopJudge.SelectedItem.Text) & "',DictHMemberID=" & IIf(ddlDictHandler.SelectedValue = 0, 0, ddlDictHandler.SelectedValue) & ",DictH='" & IIf(ddlDictHandler.SelectedValue = 0, "", ddlDictHandler.SelectedItem.Text) & "',GradMemberID=" & IIf(ddlGrading.SelectedValue = 0, 0, ddlGrading.SelectedValue) & ",Grading='" & IIf(ddlGrading.SelectedValue = 0, "", ddlGrading.SelectedItem.Text) & "',ProcMemberID=" & IIf(ddlProctor.SelectedValue = 0, 0, ddlProctor.SelectedValue) & ",Proctor='" & IIf(ddlProctor.SelectedValue = 0, "", ddlProctor.SelectedItem.Text) & "',ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
            SQLContTeamUpdate = SQLContTeamUpdate & "Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            SQLContTeamEval = "Select Count(*) From ContestTeamSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLContTeamEval) = 0 Then
                SQLContTeamExec = SQLContTeamInsert
                lblAddUPdate.Text = "Added Successfully"
            Else
                SQLContTeamExec = SQLContTeamUpdate
                lblAddUPdate.Text = "Updated Successfully"
            End If

            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamExec)

            LoadDGRoomSchedule()
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click

        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlChapter.SelectedIndex = 0
        ddlPurpose.SelectedIndex = 0
        ClearDropDowns()
        ClearRoles()

    End Sub
    Private Sub ClearDropDowns()
        DGRoomSchedule.Visible = False

        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        ddlPhase.Items.Clear()
        ddlDate.Items.Clear()
        ddlBldgID.Items.Clear()
        ddlRoomNo.Items.Clear()
        ddlStartTime.Items.Clear()
        ddlEndTime.Items.Clear()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        ClearDropDowns()
        ClearRoles()
       
    End Sub

    Protected Sub DGRoomSchedule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lblContestTeamId As Label
        lblContestTeamId = e.Item.FindControl("lblContTeamID")
        Dim ContTeamID As Integer = lblContestTeamId.Text ' CInt(e.Item.Cells(25).Text)

        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from ContestTeamSchedule Where ContTeamID=" & ContTeamID & "")
                LoadDGRoomSchedule()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            'lblRoomSchID.Text = ContTeamID.ToString()
            LoadForUpdate(ContTeamID)
        End If
    End Sub
    Private Sub LoadForUpdate(ByVal ContTeamID As Integer)

        btnAddUpdate.Text = "Update"
        lblAddUPdate.Text = ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from ContestTeamSchedule Where ContTeamID=" & ContTeamID & "")
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo")  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim BldgID As String = ""
        Try
            If ds.Tables(0).Rows.Count > 0 Then

                'ddlStartTime.DataSource = ds '.SelectedItem.Text = ds.Tables(0).Rows(0)("StartTime").ToString
                'ddlStartTime.DataBind()

                'ddlEndTime.DataSource = ds    '   SelectedItem.Text = ds.Tables(0).Rows(0)("EndTime").ToString
                'ddlEndTime.DataBind()

                'ddlStartTime.SelectedValue = ds.Tables(0).Rows(0)("StartTime")
                'ddlEndTime.SelectedValue = ds.Tables(0).Rows(0)("EndTime")

                ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
                ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))

                ddlBldgID.SelectedIndex = ddlBldgID.Items.IndexOf(ddlBldgID.Items.FindByValue(ds.Tables(0).Rows(0)("BldgID")))
                ddlRoomNo.Items.Clear()
                LoadRoomNumber(ds.Tables(0).Rows(0)("BldgID").ToString.Trim)
                ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo")))
                ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber").ToString))

                ddlRoomMc.SelectedIndex = ddlRoomMc.Items.IndexOf(ddlRoomMc.Items.FindByValue(ds.Tables(0).Rows(0)("RMcMemberID").ToString))
                ddlPronouncer.SelectedIndex = ddlPronouncer.Items.IndexOf(ddlPronouncer.Items.FindByValue(ds.Tables(0).Rows(0)("PronMemberID")))
                ddlChiefJudge.SelectedIndex = ddlChiefJudge.Items.IndexOf(ddlChiefJudge.Items.FindByValue(ds.Tables(0).Rows(0)("CJMemberID")))
                ddlAssociateJudge.SelectedIndex = ddlAssociateJudge.Items.IndexOf(ddlAssociateJudge.Items.FindByValue(ds.Tables(0).Rows(0)("AJMemberID")))
                ddlLaptopJudge.SelectedIndex = ddlLaptopJudge.Items.IndexOf(ddlLaptopJudge.Items.FindByValue(ds.Tables(0).Rows(0)("LTJudMemberID")))
                ddlDictHandler.SelectedIndex = ddlDictHandler.Items.IndexOf(ddlDictHandler.Items.FindByValue(ds.Tables(0).Rows(0)("DictHMemberID")))
                ddlGrading.SelectedIndex = ddlGrading.Items.IndexOf(ddlGrading.Items.FindByValue(ds.Tables(0).Rows(0)("GradMemberID")))
                ddlProctor.SelectedIndex = ddlProctor.Items.IndexOf(ddlProctor.Items.FindByValue(ds.Tables(0).Rows(0)("ProcMemberID")))

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopy.Click
        TrCopy.Visible = True
        GetProductGroup(ddlToPG)
    End Sub

    Protected Sub ddlToPG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToPG.SelectedIndexChanged
        LoadProduct(ddlToPG, ddlToProduct)
        Dim StrRoomNoSQl As String = "Select Distinct StartTime,EndTime from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "" ' and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim dsTime As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlStartCpTime.DataSource = dsTime
        ddlStartCpTime.DataBind()
        ddlEndCpTime.DataSource = dsTime
        ddlEndCpTime.DataBind()
    End Sub
    Protected Sub BtnCopySchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopySchedule.Click

        Dim SQLContTeamCpInsert As String = ""
        Dim SQLContTeamCpUpdate As String = ""
        Dim SQLContTeamCpEval As String = ""
        Dim SQLContTeamCpExec As String = ""
        Dim StrFromContest As String = ""
        Dim i, count As Integer
        lblAddUPdate.Text = ""

        lblAddUPdate.Text = ""
        If ddlToPG.SelectedIndex = 0 Then
            lblErr.Text = "Please Select Copy Contest"
            Exit Sub
        End If

        StrFromContest = "Select * From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrFromContest)

        Try
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1

                    SQLContTeamCpInsert = SQLContTeamCpInsert & "Insert into ContestTeamSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,StartTime,EndTime,"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "RMcMemberID,RoomMc,PronMemberID,Pronouncer,CJMemberID,ChiefJudge,AJMemberID,AssoJudge,LTJudMemberID,LTJudge,DictHMemberID,DictH,GradMemberID,Grading,ProcMemberID,Proctor,CreatedBy,CreatedDate)"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ds.Tables(0).Rows(i)("BldgID") & "','" & ds.Tables(0).Rows(i)("SeqNo") & "','" & ds.Tables(0).Rows(i)("RoomNumber") & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlToPG.SelectedValue & ",'" & ddlToPG.SelectedItem.Text & "'," & ddlToProduct.SelectedValue & ",'" & ddlToProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ",'" & ddlStartCpTime.SelectedValue & "','" & ddlEndTime.SelectedValue & "',"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("RMcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RMcMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("RoomMc") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RoomMc")) & "'," & IIf(ds.Tables(0).Rows(i)("PronMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("PronMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("Pronouncer") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Pronouncer")) & "'," & IIf(ds.Tables(0).Rows(i)("CJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("CJMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("ChiefJudge") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ChiefJudge")) & "'," & IIf(ds.Tables(0).Rows(i)("AJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AJMemberID")) & ",'"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("AssoJudge") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AssoJudge")) & "'," & IIf(ds.Tables(0).Rows(i)("LTJudMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("LTJudge") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudge")) & "'," & IIf(ds.Tables(0).Rows(i)("DictHMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictHMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("DictH") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictH")) & "'," & IIf(ds.Tables(0).Rows(i)("GradMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("Grading") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Grading")) & "'," & IIf(ds.Tables(0).Rows(i)("ProcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ProcMemberID")) & ",'" & IIf(ds.Tables(0).Rows(i)("Proctor") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Proctor")) & "'," & Session("LoginID") & ",GetDate() )"

                    SQLContTeamCpUpdate = SQLContTeamCpUpdate & "Update ContestTeamSchedule Set Date='" & ddlDate.SelectedItem.Text & "', BldgID='" & ds.Tables(0).Rows(i)("BldgID") & "',SeqNo=" & ds.Tables(0).Rows(i)("SeqNo") & ", RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "',StartTime='" & ddlStartCpTime.SelectedValue & "',EndTime='" & ddlEndCpTime.SelectedValue & "',RMcMemberID = " & IIf(ds.Tables(0).Rows(i)("RMcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RMcMemberID")) & ",RoomMc= '" & IIf(ds.Tables(0).Rows(i)("RoomMc") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RoomMc")) & "',PronMemberID=" & IIf(ds.Tables(0).Rows(i)("PronMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("PronMemberID")) & ",Pronouncer='" & IIf(ds.Tables(0).Rows(i)("Pronouncer") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Pronouncer")) & "',CJMemberID=" & IIf(ds.Tables(0).Rows(i)("CJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("CJMemberID")) & ",ChiefJudge='" & IIf(ds.Tables(0).Rows(i)("ChiefJudge") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ChiefJudge")) & "',AJMemberID=" & IIf(ds.Tables(0).Rows(i)("AJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AJMemberID")) & ",AssoJudge='" & IIf(ds.Tables(0).Rows(i)("AssoJudge") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AssoJudge")) & "',"
                    SQLContTeamCpUpdate = SQLContTeamCpUpdate & " LTJudMemberID=" & IIf(ds.Tables(0).Rows(i)("LTJudMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudMemberID")) & ",LTJudge='" & IIf(ds.Tables(0).Rows(i)("LTJudge") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudge")) & "',DictHMemberID=" & IIf(ds.Tables(0).Rows(i)("DictHMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictHMemberID")) & ",DictH='" & IIf(ds.Tables(0).Rows(i)("DictH") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictH")) & "',GradMemberID=" & IIf(ds.Tables(0).Rows(i)("GradMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID")) & ",Grading='" & IIf(ds.Tables(0).Rows(i)("Grading") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Grading")) & "',ProcMemberID=" & IIf(ds.Tables(0).Rows(i)("ProcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ProcMemberID")) & ",Proctor='" & IIf(ds.Tables(0).Rows(i)("Proctor") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("Proctor")) & "',ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
                    SQLContTeamCpUpdate = SQLContTeamCpUpdate & "Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

                Next
                SQLContTeamCpEval = "Select Count(*) From ContestTeamSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""      'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") 'Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'

                If i <= count Then 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLContTeamCpEval) = 0 Then
                        SQLContTeamCpExec = SQLContTeamCpInsert
                        lblAddUPdate.Text = "Copied Successfully(Added)"
                    Else
                        SQLContTeamCpExec = SQLContTeamCpUpdate
                        lblAddUPdate.Text = "Copied Successfully(Updated)"
                    End If
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpExec)

                ElseIf i > count Then
                    lblErr.Text = "RoomSchedule data  has fewer records for the Contest"
                Else
                    lblErr.Text = "RoomSchedule data is not yet added for the Contest"
                End If
            End If
            LoadDGRoomSchedule()

        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
End Class

