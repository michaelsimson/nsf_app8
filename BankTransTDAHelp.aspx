<%@ Page AutoEventWireup="false" Title="BankTransTDAhelp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Filename and file type conventions</title>
		<LINK href="Styles.css" rel="stylesheet">
	</HEAD>
	<body aleftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
		<form id="Form1" method="post" runat="server">
		<p class="SmallFont">  1) The files from the brokerage house have a file type  .dat.  They need to be opened into Excel and then re-saved into .csv format (comma delimited).</p>
		<p class="SmallFont">  2) During the upload to the server, the system checks for standardized filename.  The highlighted portion below should be followed in order for the system to accept your file for uploading. </p>
		<p class="SmallFont">  TDA_US_Sch_Trans_May1_2011_Apr30_2012.csv</p>
        <p class="SmallFont">  TDA_US_Sch_Trans_May1_2011_Apr30_2012.csv</p>
        <p class="SmallFont">  TDA_General_Trans_May1_2011_Apr30_2012.csv</p>
        <p class="SmallFont">  TDA_Endow_Trans_May1_2011_Apr30_2012.csv</p>
        <p class="SmallFont">  TDA_DAF_Trans_May1_2011_Apr30_2012.csv</p>
		</form>
	</body>
</HTML>

 

 
 
 