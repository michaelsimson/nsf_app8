Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class Reports_CCReconcileStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename like 'PP%' and BeginRecID>0  order by CCpostinglogID Desc
        'select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename like 'MS%' and BeginRecID>0  order by CCpostinglogID Desc
        'select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename like 'EM%' and BeginRecID>0  order by CCpostinglogID Desc
        'select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename is NUll and BeginRecID>0  order by CCpostinglogID Desc
        'select count(*) from PPtemp
        'Select count(*) from MStemp
        'Select count(*) from EMtemp2
        'select MIN(TDate) as BeginDate, MAX(TDate) as EndDate from PPtemp where Tdate is Not NULL
        'select MIN(TDate) as BeginDate, MAX(TDate) as EndDate from MStemp where Tdate is Not NULL
        'select  MIN(TransDate) as BeginDate, MAX(TransDate) as EndDate from EMTemp2 where TransDate is Not NULL
        Dim Begindate, EndDate, beginRecID, EndRecid As String
        Dim Cnt As Integer
        Dim tableExist As String = ""
        Dim reader As SqlDataReader
        Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0><tr><td align=Center>Table Name</td><td align=Center>Table Exists</td><td align=Center>Beg Rec#</td><td align=Center>Beg Date</td><td align=Center>End Rec#</td><td align=Center>End Date</td></tr>"
        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from PPtemp")
        If Cnt > 0 Then
            Cnt = 0
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select CONVERT(VARCHAR(10),MIN(TDate),101) as BeginDate, CONVERT(VARCHAR(10),MAX(TDate),101) as EndDate from PPtemp where Tdate is Not NULL")
            While reader.Read()
                Begindate = reader("BeginDate")
                EndDate = reader("EndDate")
            End While
            TableStr = TableStr & "<tr><td align=Center>PPTemp</td><td align=Center> Y </td><td align=Center>  </td><td align=Center>" & Begindate & " </td><td align=Center> </td><td align=Center> " & EndDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>PPTemp</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename like 'PP%' and BeginRecID>0  order by CCpostinglogID Desc")
        While reader.Read()
            beginRecID = reader("BeginRecID")
            Begindate = reader("BeginDate")
            EndRecid = reader("EndRecID")
            EndDate = reader("EndDate")
        End While
        TableStr = TableStr & "<tr><td align=Center>PPCharge</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        reader.Close()
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from MStemp")
        If Cnt > 0 Then
            Cnt = 0
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select CONVERT(VARCHAR(10),MIN(TDate),101) as BeginDate, CONVERT(VARCHAR(10),MAX(TDate),101) as EndDate from MStemp where Tdate is Not NULL")
            While reader.Read()
                Begindate = reader("BeginDate")
                EndDate = reader("EndDate")
            End While
            TableStr = TableStr & "<tr><td align=Center>MSTemp</td><td align=Center> Y </td><td align=Center>  </td><td align=Center>" & Begindate & " </td><td align=Center> </td><td align=Center> " & EndDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>MSTemp</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename like 'MS%' and BeginRecID>0  order by CCpostinglogID Desc")
        While reader.Read()
            beginRecID = reader("BeginRecID")
            Begindate = reader("BeginDate")
            EndRecid = reader("EndRecID")
            EndDate = reader("EndDate")
        End While
        TableStr = TableStr & "<tr><td align=Center>MSCharge</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        reader.Close()
        'beginRecID = ""
        'Begindate = ""
        'EndRecid = ""
        'EndDate = ""
        'Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from EMtemp2")
        'If Cnt > 0 Then
        '    Cnt = 0
        '    reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select  CONVERT(VARCHAR(10),MIN(TransDate),101) as BeginDate, CONVERT(VARCHAR(10),MAX(TransDate),101) as EndDate from EMTemp2 where TransDate is Not NULL")
        '    While reader.Read()
        '        Begindate = reader("BeginDate")
        '        EndDate = reader("EndDate")
        '    End While
        '    TableStr = TableStr & "<tr><td align=Center>EMTemp2</td><td align=Center> Y </td><td align=Center>  </td><td align=Center>" & Begindate & " </td><td align=Center> </td><td align=Center> " & EndDate & "</td></tr>"
        '    reader.Close()
        'Else
        '    TableStr = TableStr & "<tr><td align=Center>EMTemp2</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        'End If
        'beginRecID = ""
        'Begindate = ""
        'EndRecid = ""
        'EndDate = ""
        'reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename like 'EM%' and BeginRecID>0  order by CCpostinglogID Desc")
        'While reader.Read()
        '    beginRecID = reader("BeginRecID")
        '    Begindate = reader("BeginDate")
        '    EndRecid = reader("EndRecID")
        '    EndDate = reader("EndDate")
        'End While
        'TableStr = TableStr & "<tr><td align=Center>EMCharge</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        'reader.Close()
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BeginDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndDate,101),'') as EndDate from CCpostinglog where inputfilename is NUll and BeginRecID>0  order by CCpostinglogID Desc")
        While reader.Read()
            beginRecID = reader("BeginRecID")
            Begindate = reader("BeginDate")
            EndRecid = reader("EndRecID")
            EndDate = reader("EndDate")
        End While
        TableStr = TableStr & "<tr><td align=Center>ChargeRec</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        reader.Close()
        TableStr = TableStr & "</Table><br><br>"
        Literal1.Text = TableStr.ToString
    End Sub
End Class
