Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions

Partial Class SendEmail
    Inherits System.Web.UI.Page
    Dim cnTemp As SqlConnection
    Dim intCtr As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Debug()
        'Session("LoggedIn") = "True"
        'Session("LoginRole") = "ChapterC"
        'Session("RoleId") = 5
        'Session("LoginChapterID") = 48

        cnTemp = New SqlConnection(Application("ConnectionString"))
        'lbldebug.Text = Session("LoginRole") & Session("LoginRoleCategoryID")
        'lbldebug.Visible = True

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            Exit Sub
        End If

        LoadlstAddress("PP")

    End Sub

    Private Sub LoadlstAddress(ByVal strType As String)
        Dim strSQL As String, dsData As New DataSet

        Try
            strSQL = ""
            Select Case Session("RoleId")
                Case 5  'ChapterCoordinator
                    Select Case strType
                        Case "PP"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                     " Where ChapterID = " & Session("LoginChapterID") & ") or (Relationship in (Select Distinct ParentID " & _
                                     " From Contestant Where ChapterID = " & Session("LoginChapterID") & ") and PrimaryContact = 'Y' ))" & _
                                     " Order By AutoMemberID "
                        Case "PB"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                     " Where ChapterID = " & Session("LoginChapterID") & ") or (Relationship in (Select Distinct ParentID " & _
                                     " From Contestant Where ChapterID = " & Session("LoginChapterID") & ")))" & _
                                     " Order By AutoMemberID "
                        Case "VR"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                     " Where RoleID IN (5) and ChapterID = " & Session("LoginChapterID") & ") or (Relationship in (Select Distinct ParentID " & _
                                     " From Contestant Where ChapterID = " & Session("LoginChapterID") & ")))" & _
                                     " Order By AutoMemberID "
                        Case "VUR"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " and (VolunteerRole1 in ( 34 ) or VolunteerRole2 in ( 34 ) or VolunteerRole3 in ( 34 )) " & _
                                     " Order By AutoMemberID "
                    End Select

                Case 4 'ClusterC
                    If Session("LoginClusterID") <> 0 Then
                        Select Case strType
                            Case "PP"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                        " From IndSpouse where ChapterID IN (" & _
                                        " Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & _
                                        " ) and email<>'' " & _
                                        " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                        " Where ChapterID IN " & _
                                        " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                        " ) or (Relationship in (Select Distinct ParentID " & _
                                        " From Contestant Where ChapterID IN " & _
                                        " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                        " ) and PrimaryContact = 'Y' ))" & _
                                        " Order By AutoMemberID "
                            Case "PB"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " Where ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                         " )))" & _
                                         " Order By AutoMemberID "
                            Case "VR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                         " Where RoleID IN (5) and ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                         ") or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                         ")))" & _
                                         " Order By AutoMemberID "
                            Case "VUR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID") & ") " & _
                                         " ) and email<>'' " & _
                                         " and (VolunteerRole1 in ( 34 ) or VolunteerRole2 in ( 34 ) or VolunteerRole3 in ( 34 )) " & _
                                         " Order By AutoMemberID "
                        End Select
                    End If
                    If Session("LoginZoneID") <> 0 Then
                        Select Case strType
                            Case "PP"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) and PrimaryContact = 'Y' ))" & _
                                         " Order By AutoMemberID "
                            Case "PB"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " )))" & _
                                         " Order By AutoMemberID "
                            Case "VR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                         " Where RoleID IN (5) and ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         ") or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         ")))" & _
                                         " Order By AutoMemberID "
                            Case "VUR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) and email<>'' " & _
                                         " and (VolunteerRole1 in ( 34 ) or VolunteerRole2 in ( 34 ) or VolunteerRole3 in ( 34 )) " & _
                                         " Order By AutoMemberID "
                        End Select

                    End If

                Case 3, 2, 1 'ZonalC , NationalC, Admin
                    If Session("LoginZoneID") <> 0 Then
                        Select Case strType
                            Case "PP"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) and PrimaryContact = 'Y' ))" & _
                                         " Order By AutoMemberID "
                            Case "PB"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID = " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " )))" & _
                                         " Order By AutoMemberID "
                            Case "VR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) and email<>'' " & _
                                         " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                         " Where RoleID IN (5) and ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         ") or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID IN " & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         ")))" & _
                                         " Order By AutoMemberID "
                            Case "VUR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & _
                                         " ( Select ChapterID from Chapter Where ZoneID = " & Session("LoginZoneID") & ") " & _
                                         " ) and email<>'' " & _
                                         " and (VolunteerRole1 in ( 34 ) or VolunteerRole2 in ( 34 ) or VolunteerRole3 in ( 34 )) " & _
                                         " Order By AutoMemberID "
                        End Select
                    End If

            End Select

            lblDebug.Text = strSQL
            lblDebug.Visible = True
            If strSQL <> "" Then
                dsData = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, strSQL)
            Else
                Exit Sub
            End If
        Catch ex As Exception

        End Try

        Dim intctr As Integer
        intctr = 0
        If Not dsData Is Nothing Then
            lstAddress.DataSource = dsData
            lstAddress.DataBind()
            lstAddress.Visible = True
            For intctr = 0 To lstAddress.Items.Count - 1
                lstAddress.Items(intctr).Selected = True
            Next

        Else
            lstAddress.Visible = False
        End If
    End Sub

    Protected Sub btnIns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIns.Click
        txtTo.Text = ""
        For intCtr = 0 To lstAddress.Items.Count - 1
            If lstAddress.Items(intCtr).Selected = True Then
                txtTo.Text = txtTo.Text & lstAddress.Items(intCtr).Text & ", "
            End If
        Next
        If txtTo.Text <> "" Then
            txtTo.Text = Mid(txtTo.Text, 1, (Len(txtTo.Text) - 2))
        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sMailFrom As String) As Boolean
        'Dim client As New SmtpClient()
        'Dim email As New MailMessage


        ''!!! UPDATE THIS VALUE TO YOUR EMAIL ADDRESS
        'Const ToAddress As String = "chitturi9@gmail.com"
        'Const FromAddress As String = "contests@northsouth.org"  '"sudhisandhya@yahoo.com"

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage(SMailFrom, sMailTo)

        '(2) Assign the MailMessage's properties
        mm.Subject = sSubject
        mm.Body = sBody
        mm.IsBodyHtml = True
        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure

        'Attach the file
        'Make sure a file has been uploaded
        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then
            '  Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName))
        End If

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient

        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Host = "server420.mysite4now.net"   REM "espresso.extremezone.com"

        Try
            smtp.Send(mm)
        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function


    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim sbEmailSuccess As New StringBuilder
        Dim sbEmailFailure As New StringBuilder

        If txtFrom.Text = "" Then Exit Sub
        If txtTo.Text = "" Then Exit Sub
        If txtSubject.Text = "" Then Exit Sub
        If txtEmailBodtText.Text = "" Then Exit Sub

        strEmailAddressList = Split(txtTo.Text, ",")

        For Each item In strEmailAddressList
            If SendEmail(Replace(txtSubject.Text, "'", "''"), Replace(txtEmailBodtText.Text, "'", "''"), item.ToString, txtFrom.text) = True Then
                sbEmailSuccess.Append("Email Send To:" & item.ToString & "<BR>")
            Else
                sbEmailFailure.Append("Email Send To:" & item.ToString & "<BR>")
            End If
        Next
        lblEmailError.Text = sbEmailFailure.ToString


    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        LoadlstAddress(ddlTo.SelectedValue)
    End Sub
End Class

