﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
using System.Collections.Generic;
 
public partial class NSFMajorLeaguePdf: System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}

        if (!IsPostBack)
        {
           
            load();
        }
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        load();

    }
    public void load()
    {
       // string YearRange = FromYear.SelectedValue + "-" + ToYear.SelectedValue;
        string ProductGroupQry = "select distinct contest from MLWinners order by contest desc";
        DataSet dsProductGroup = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ProductGroupQry);
        if (dsProductGroup.Tables[0].Rows.Count > 0)
        {
            DataLists.DataSource = dsProductGroup.Tables[0];
            DataLists.DataBind();
        }
    }

    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label lblyear = (Label)e.Item.FindControl("lblyear");
        Label lblContest = (Label)e.Item.FindControl("lblContest");
        Repeater rp = (Repeater)e.Item.FindControl("rp_Postings");
        string DDListItem = "select ML.contestyear, ML.contest,ML.place,C.FIRST_NAME,C.LAST_NAME from MLWinners ML left join Child c on C.ChildNumber=Ml.ChildNumber where ContestYear=" + lblyear.Text + " and Contest='" + lblContest.Text + "' order by ML.place asc";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDListItem);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            rp.DataSource = dsstate.Tables[0];
            rp.DataBind();
        }

    }
 
    protected void btnsearchChild0_Click(object sender, EventArgs e)
    {
        try
        {
            load();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Div1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
            
            //string attachment = "attachment; filename=TravelGuide.pdf";
            //Response.ClearContent();
            //Response.AddHeader("content-disposition", attachment);
            //Response.ContentType = "application/pdf";
            //// Rendering DataList into HTMLTextWriter
            //StringWriter swHtml = new StringWriter();
            //HtmlTextWriter hTextWriter = new HtmlTextWriter(swHtml);
            //DataLists.RenderControl(hTextWriter);

            //try
            //{
            //    using (Document doc = new Document(PageSize.A4))
            //    {
            //        using (PdfWriter w = PdfWriter.GetInstance(doc, Response.OutputStream))
            //        {
            //            doc.Open();
            //            // Getting City Name From Session
            //            //string cityName = string.Empty;
            //            //if (!string.IsNullOrEmpty(SessionUtil.GetCurrentCityNameFromSession()))
            //            //    cityName = " to " + SessionUtil.GetCurrentCityNameFromSession();
            //            ////add guide header
            //            //Chunk c = new Chunk("Your Guide" + cityName + "\n", FontFactory.GetFont("Verdana", 15));
            //            //Paragraph p = new Paragraph();
            //            //p.Alignment = Element.HEADER;
            //            //p.Add(c);
            //            //doc.Add(p);
            //            //add columns
            //            MultiColumnText columns = new MultiColumnText();
            //            columns.AddRegularColumns(36f, doc.PageSize.Width - 36f, 25f, 2);
            //            // Set margin to document
            //            doc.SetMargins(0f, 8f, 8f, 10f);
            //            // Apply CSS to DataList for style
            //            StyleSheet style = new StyleSheet();
            //           // style.LoadTagStyle(HtmlTags.IMG, HtmlTags.WIDTH, "220px");
            //            //style.LoadTagStyle(HtmlTags.IMG, HtmlTags.HEIGHT, "80px");
            //           // style.LoadStyle("address", "style", "font-size: 8px; text-align: justify; font-family: Arial, Helvetica, sans-serif;");
            //           // style.LoadStyle("largeName", "style", "font-size: 10px; text-align: justify; font-family: Arial, Helvetica, sans-serif;");
            //           // style.LoadStyle("description", "style", "font-size: 8px; text-align: justify; font-family: Arial, Helvetica, sans-serif;");
            //            // Reading value Of DataList to write on the PDF Document
            //            using (StringReader sr = new StringReader(swHtml.ToString()))
            //            {
            //                List<IElement> list = HTMLWorker.ParseToList(sr, style);
            //                foreach (IElement elm in list)
            //                {
            //                    columns.AddElement(elm);
            //                    doc.Add(columns);
            //                }
            //            }
            //            doc.Close();
            //            Response.Write(doc);
            //            Response.End();
            //        }
            //    }
            //}
            //catch
            //{
            //    //throw new ApplicationException("Oops! Error occurred while downloading the Travel Guide! "+ ex.ToString());
            //}
            //finally
            //{
            //    swHtml = null;
            //    hTextWriter = null;
            //}









            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "attachment;filename=MajorLeague.pdf");
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);
            //DataLists.DataBind();
            //DataLists.RenderControl(hw);
            //StringReader sr = new StringReader(sw.ToString());
            //Document pdfDoc = new Document(PageSize.A4, 80, 50, 30, 65);
            //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();
            //htmlparser.Parse(sr);
            //pdfDoc.Close();
            //Response.Write(pdfDoc);
            //Response.End();
        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
    }
    protected void DataLists_ItemDataBound(object sender, DataListItemEventArgs e)
    {
       
        HiddenField hdnYear = (HiddenField)e.Item.FindControl("hdn");
        DataList DataList1 = (DataList)e.Item.FindControl("DataList1");
        string DDLstate = "select distinct contestyear,contest from MLWinners where contest='" + hdnYear.Value + "' order by contestyear desc";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLstate);
        if (dsstate.Tables[0].Rows.Count > 0)
        {
            DataList1.DataSource = dsstate.Tables[0];
            DataList1.DataBind();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        load();
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        load();
    }
}