﻿
Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ShowVoucher
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim dblRegFee As Decimal
    Dim BusType As String
    Dim IRSCat As String
    Dim ChapterCode As String
    Dim Amount As String
    Dim DonationType As String
    Dim DonationID As Integer
    Dim DonorType As String
    Dim purpose As String
    Dim EventName As String
    Dim checkNo As String
    Dim depositSlip As String
    Dim depDate As String
    Dim donDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If Not IsPostBack Then
                If Request.QueryString("DType").ToUpper() = "D" Then
                    GetDonationReceipt(Request.QueryString("DSlip"), Request.QueryString("DDate"))
                    If Not Session("DCatID") = 2 Then
                        lblHeading.Text = "Bank Deposits"
                    Else
                        lblHeading.Text = "Other Deposits"
                    End If
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "EX" Then
                    GetExpenseReceipt(Request.QueryString("DSlip"), Request.QueryString("DDate"))
                    lblHeading.Text = "Expense Voucher"
                    Requestorlbl.Text = "Payee"
                ElseIf Request.QueryString("DType").ToUpper() = "CC" And Not Session("DCatID") = 4 Then
                    GetCCRevenues(Request.QueryString("SDt"), Request.QueryString("EDt"))
                    lblHeading.Text = "Credit Card Revenues"
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "CC" And Session("DCatID") = 4 Then
                    GetCCRevenues(Request.QueryString("SDt"), Request.QueryString("EDt"))
                    lblHeading.Text = "Investment Income"
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "ACT_EFT" And Session("DCatID") = 6 Then
                    GetADT_EFTDonations(Request.QueryString("TDate"), Request.QueryString("BankID"))
                    lblHeading.Text = "ACT_EFT Donations"
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "CCFEES" And Session("DCatID") = 7 Then
                    GetBnk_CCFees(Request.QueryString("TDate"), Request.QueryString("BankID"), Request.QueryString("VendCust"))
                    lblHeading.Text = "Bank Service Charges/Credit Card Fees"
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "TRANFERS" And Session("DCatID") = 8 Then
                    GetTransfers(Request.QueryString("TDate"), Request.QueryString("BankID"), Request.QueryString("Ticker"), Request.QueryString("Quantity"), Request.QueryString("Amount"), Request.QueryString("StartDate"), Request.QueryString("EndDate"))
                    lblHeading.Text = "Transfer Vouchers "
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "BUY_SELL" And Session("DCatID") = 9 Then
                    GetBuy_Sell(Request.QueryString("TDate"), Request.QueryString("BankID"), Request.QueryString("AssetClass"), Request.QueryString("Quantity"), Request.QueryString("Amount"), Request.QueryString("StartDate"), Request.QueryString("EndDate"))
                    lblHeading.Text = "Buy Transactions"
                    Requestorlbl.Text = "Requestor"
                ElseIf Request.QueryString("DType").ToUpper() = "BUY_SELL" And Session("DCatID") = 10 Then
                    GetBuy_Sell(Request.QueryString("TDate"), Request.QueryString("BankID"), Request.QueryString("AssetClass"), Request.QueryString("Quantity"), Request.QueryString("Amount"), Request.QueryString("StartDate"), Request.QueryString("EndDate"))
                    lblHeading.Text = "Sell Transactions"
                    Requestorlbl.Text = "Requestor"
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub GetBuy_Sell(ByVal Transdate As String, ByVal BankID As Integer, ByVal AssetClass As String, ByVal Quantity As Double, ByVal Amount As Double, ByVal StartDate As String, ByVal EndDate As String)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        Dim Frmdate As Date
        Dim Todate As DateTime
        Dim StrSQL As String

        Frmdate = Convert.ToDateTime(Transdate)
        Todate = Convert.ToDateTime(Transdate)
        Dim NetAmount As Double = 0.0
        'If Amount = 0 Then
        '    NetAmount = Quantity * GetAvgPrice(BankID, AssetClass, Quantity, Transdate, StartDate, EndDate)
        'Else
        '    NetAmount = Amount
        'End If

        If Session("DCatID") = "9" Then
            StrSQL = " SELECT '' as DonorType,'' as DonationType,B.BankCode,'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,B.TransDate as Date,'IGN' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class,-SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,'" & Frmdate & "' as StartDate,'" & Todate & "' as EndDate,GETDATE()," & Session("LoginID") & ",AccType,B.TransDate as DepositDate"
            StrSQL = StrSQL & " ,1 as H FROM BrokTrans B "
            StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Investment-General Unrestricted'"
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
            StrSQL = StrSQL & " Where B.Transcat='Buy' and B.TransDate Between '" & Frmdate & "' and '" & Todate & " 23:59' and B.AssetClass='" & AssetClass & "'"
            StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName"
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " SELECT '' as DonorType,'' as DonationType,B.BankCode,'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,B.TransDate as Date,'IGN' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class,SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,'" & Frmdate & "' as StartDate,'" & Todate & "' as EndDate,GETDATE()," & Session("LoginID") & ",AccType,B.TransDate as DepositDate"
            StrSQL = StrSQL & " ,0 as H FROM BrokTrans B "
            StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') and N.Description ='MMKT - General Unrestricted'"
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
            StrSQL = StrSQL & " Where B.Transcat='Buy' and B.TransDate Between '" & Frmdate & "' and '" & Todate & " 23:59' and B.AssetClass='" & AssetClass & "'"
            StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName"
            'Response.Write(StrSQL)
        ElseIf Session("DCatID") = "10" Then
            StrSQL = " SELECT '' as DonorType,'' as DonationType,B.BankCode,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,B.TransDate as Date,'IGN' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class,SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,'" & Frmdate & "' as StartDate,'" & Todate & "' as EndDate,GETDATE()," & Session("LoginID") & ",AccType,B.TransDate as DepositDate"
            StrSQL = StrSQL & " ,1 as H FROM BrokTrans B "
            StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Investment-General Unrestricted'"
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
            StrSQL = StrSQL & " Where B.Transcat='Sell' and B.TransDate Between '" & Frmdate & "' and '" & Todate & " 23:59' and B.AssetClass='" & AssetClass & "'"
            StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName"
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " SELECT '' as DonorType,'' as DonationType,B.BankCode,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,B.TransDate as Date,'IGN' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class,-SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,'" & Frmdate & "' as StartDate,'" & Todate & "' as EndDate,GETDATE()," & Session("LoginID") & ",AccType,B.TransDate as DepositDate"
            StrSQL = StrSQL & " ,0 as H FROM BrokTrans B "
            StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') and N.Description ='MMKT - General Unrestricted'"
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
            StrSQL = StrSQL & " Where B.Transcat='Sell' and B.TransDate Between '" & Frmdate & "' and '" & Todate & " 23:59' and B.AssetClass='" & AssetClass & "'"
            StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName"
        End If
        Try
            dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
            If dsDonation.Tables(0).Rows.Count > 0 Then
                dgVoucher.Visible = True
                Dim row As DataRow = dsDonation.Tables(0).NewRow
                row("Amount") = 0
                row("Description") = "Total"
                Dim cnt As Integer
                For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                    If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                        dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                        Exit For
                    End If
                Next
                If Session("DCatID") = 9 Then dgVoucher.Columns(0).Visible = False
                dgVoucher.DataSource = dsDonation.Tables(0)
                dgVoucher.DataBind()
                lblBankAccount.Text = "Bank Account: " & dsDonation.Tables(0).Rows(0)("BanKCode")
                lblDepositNo.Text = "VoucherNo: " & dsDonation.Tables(0).Rows(0)("VoucherNo")
            Else
                dgVoucher.DataSource = Nothing
                dgVoucher.DataBind()
                dgVoucher.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try

    End Sub

    Private Sub GetTransfers(ByVal Transdate As String, ByVal BankID As Integer, ByVal Ticker As String, ByVal Quantity As Double, ByVal Amount As Double, ByVal StartDate As String, ByVal EndDate As String)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        Dim Frmdate As Date
        Dim Todate As DateTime
        Dim StrSQL As String

        Frmdate = Convert.ToDateTime(Transdate)
        Todate = Convert.ToDateTime(Transdate + " 23:59 ")
        Dim NetAmount As Double = 0.0
           If Amount = 0 Then
            NetAmount = Quantity * GetAvgPrice(BankID, Ticker, Quantity, Transdate, StartDate, EndDate)
        Else
            NetAmount = Amount
        End If

        If Session("DCatID") = "8" Then
            StrSQL = " SELECT  N.AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), B.Transdate , 101) AS DepositDate,N.Description,Ch.WebFolderName as Class,"
            StrSQL = StrSQL & " (" & NetAmount & ") as Amount ,'IGT' as TransType,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+ Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as VoucherNo ,N.RestrictionType as Donationtype,B.BankID,1 as H"
            StrSQL = StrSQL & " ,B.BankCode FROM BrokTrans B "
            StrSQL = StrSQL & " Inner join BrokTrans b1 On B1.TranId=B.TranID and B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat "
            StrSQL = StrSQL & " Inner JOin NSFAccounts N On N.BankID=B.BankId and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
            StrSQL = StrSQL & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.TransDate Between '" & Frmdate & "' and '" & Todate & "' and B.BankID=" & BankID & ""
            StrSQL = StrSQL & " Group by N.AccNo,B.TransDate,N.Description,B1.BanKID,B.BankID,B.AssetClass,N.RestrictionType,B.BankCode,Ch.WebFolderName"
            StrSQL = StrSQL & "  UNION ALL"
            StrSQL = StrSQL & " SELECT  N.AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), B.Transdate , 101) AS DepositDate,N.Description,Ch.WebFolderName as Class,"
            StrSQL = StrSQL & " -( " & NetAmount & ") as Amount ,'IGT' as TransType,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+ Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as VoucherNo,N.RestrictionType as Donationtype,B.BankID,0 as H"
            StrSQL = StrSQL & " ,B.BankCode FROM BrokTrans B  "
            StrSQL = StrSQL & " Inner join BrokTrans b1 On B1.TranId=B.TranID and B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat "
            StrSQL = StrSQL & " Inner JOin NSFAccounts N On N.BankID=B1.BankId  and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
            StrSQL = StrSQL & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.TransDate Between '" & Frmdate & "' and '" & Todate & "' and B.BankID=" & BankID & ""
            StrSQL = StrSQL & " Group by N.AccNo,B.TransDate,N.Description,B1.BanKID,B.BankID,B.AssetClass,N.RestrictionType,B.BankCode,Ch.WebFolderName"
            Try
                dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
                If dsDonation.Tables(0).Rows.Count > 0 Then
                    dgVoucher.Visible = True
                    Dim row As DataRow = dsDonation.Tables(0).NewRow
                    row("Amount") = 0
                    row("Description") = "Total"
                    Dim cnt As Integer
                    For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                        If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                            dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                            Exit For
                        End If
                    Next
                    If Session("DCatID") = 8 Then dgVoucher.Columns(0).Visible = False
                    dgVoucher.DataSource = dsDonation.Tables(0)
                    dgVoucher.DataBind()
                    lblBankAccount.Text = "Bank Account: " & dsDonation.Tables(0).Rows(0)("BanKCode")
                    lblDepositNo.Text = "VoucherNo: " & dsDonation.Tables(0).Rows(0)("VoucherNo")
                Else
                    dgVoucher.DataSource = Nothing
                    dgVoucher.DataBind()
                    dgVoucher.Visible = False
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString)
            End Try
        End If
    End Sub

    Private Sub GetBnk_CCFees(ByVal Transdate As String, ByVal BankID As Integer, ByVal VendCust As String)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))

        Dim Frmdate As Date
        Dim Todate As DateTime
        Dim StrSQL As String

        Frmdate = Convert.ToDateTime(Transdate)
        Todate = Convert.ToDateTime(Transdate + " 23:59 ")

        If Session("DCatID") = "7" Then
            StrSQL = " SELECT  AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & Transdate & "', 101) AS DepositDate,Description,WebFolderName as Class,H,SUM(Amount) as Amount ,'VPE' as TransType,RIGHT('00'+ CONVERT(VARCHAR,BankID),2) As BankID "
            StrSQL = StrSQL & " FROM ("
            StrSQL = StrSQL & "SELECT B.BankID,B.TransDate,Ch.WebFolderName,-SUM(Amount) as Amount,N.AccType,N.Description, N.AccNo,B.TransType,1 as H FROM BankTrans B INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode INNER JOIN NSFAccounts N ON E.Account = N.AccNo Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID = " & BankID & " and TransDate Between '" & Frmdate & "' and '" & Todate & "'  AND B.VendCust='" & VendCust & "'"
            StrSQL = StrSQL & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,N.AccType,N.Description,Ch.WebFolderName "
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " SELECT B.BankID,B.TransDate,Ch.WebFolderName,-SUM(Amount) as Amount,N.AccType,N.Description,N.AccNo,B.TransType,1 as H FROM BankTrans B INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode INNER JOIN NSFAccounts N ON E.Account =N.AccNo Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID = " & BankID & " and TransDate Between '" & Frmdate & "' and '" & Todate & "' and B.VendCust='" & VendCust & "'"
            StrSQL = StrSQL & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,N.AccType,N.Description,Ch.WebFolderName "
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " SELECT B.BankID,B.TransDate,Ch.WebFolderName,SUM(Amount) as Amount,N.AccType,N.Description,N.AccNo,B.TransType,0 as H FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID = " & BankID & " and TransDate Between '" & Frmdate & "' and '" & Todate & "' and B.VendCust='" & VendCust & "'"
            StrSQL = StrSQL & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,N.AccType,N.Description,Ch.WebFolderName "
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " SELECT B.BankID,B.TransDate,Ch.WebFolderName,SUM(Amount) as Amount,N.AccType,N.Description,N.AccNo,B.TransType,0 as H FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID = " & BankID & " and TransDate Between '" & Frmdate & "' and '" & Todate & "' AND B.VendCust='" & VendCust & "'"
            StrSQL = StrSQL & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,N.AccType,N.Description,Ch.WebFolderName"
            StrSQL = StrSQL & " ) T Group by BankID,AccNo,AccType,TransDate,Description,H,WebFolderName Order by H desc"

            Try
                dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
                If dsDonation.Tables(0).Rows.Count > 0 Then
                    dgVoucher.Visible = True
                    Dim row As DataRow = dsDonation.Tables(0).NewRow
                    row("Amount") = 0
                    row("Description") = "Total"
                    Dim cnt As Integer
                    For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                        If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                            dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                            Exit For
                        End If
                    Next
                    If Session("DCatID") = 7 Then dgVoucher.Columns(0).Visible = False
                    dgVoucher.DataSource = dsDonation.Tables(0)
                    dgVoucher.DataBind()
                Else
                    dgVoucher.DataSource = Nothing
                    dgVoucher.DataBind()
                    dgVoucher.Visible = False
                End If
            Catch ex As Exception
                ' Response.Write(ex.ToString)
            End Try
        End If

    End Sub

    Private Sub GetADT_EFTDonations(ByVal Transdate As String, ByVal BankID As Integer)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrSQL As String
        If Session("DCatID") = "6" Then

            StrSQL = " SELECT  AccNo,'' as DonorType,TransDate as DepositDate,Description,WebFolderName as Class,SUM(Amount) as Amount,VoucherNo,RestrictionType as Donationtype,BankID, 1 as H FROM "
            StrSQL = StrSQL & " (SELECT  B.BankID,B.BankTransID,N.AccNo, SUM(B.Amount) as Amount,N.RestrictionType,B.TransCat,'DGE' as TransType,B.TransDate,"
            StrSQL = StrSQL & " 'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,"
            StrSQL = StrSQL & " B.VendCust, N.Description,N.DonorType,Ch.WebFolderName FROM BankTrans B "
            StrSQL = StrSQL & " LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat=C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankId =1"
            StrSQL = StrSQL & " LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat=H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason Is Null and H.BK_Reason is Null)) and B.BankId =2"
            StrSQL = StrSQL & " Inner Join  NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
            StrSQL = StrSQL & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID"
            StrSQL = StrSQL & " Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' "
            StrSQL = StrSQL & " and B.BankID =" & BankID & " and Convert(Date,TransDate)  = CONVERT (Date,'" & Transdate & "')"
            StrSQL = StrSQL & " Group By N.AccNo ,B.BankID,N.Description,N.DonorType ,B.TransCat,B.TransDate ,B.Vendcust,B.BankTransID,N.RestrictionType,Ch.WebFolderName"
            StrSQL = StrSQL & " ) T1 Group by AccNo,Description,VoucherNo,Restrictiontype,BankID,TransDate,WebFolderName UNION ALL "
            StrSQL = StrSQL & "SELECT  AccNo,'' as DonorType,TransDate as DepositDate,Description,WebFolderName as Class,SUM(Amount) as Amount,VoucherNo,RestrictionType as Donationtype,BankID , 0 as H FROM "
            StrSQL = StrSQL & " (SELECT B.BankId,B.BankTransID,N.AccNo,-SUM(B.Amount) as Amount,N.RestrictionType,B.Transcat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,"
            StrSQL = StrSQL & " B.VendCust, N.Description ,N.DonorType,Ch.WebFolderName FROM BankTrans B INNER JOIN NSFAccounts N ON  "
            StrSQL = StrSQL & " B.BankID = N.BankID Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where N.RestrictionType = 'Unrestricted' and B.TransCat='Donation' "
            StrSQL = StrSQL & " and B.BankID =" & BankID & " and Convert(Date,TransDate)  = CONVERT (Date,'" & Transdate & "') Group by B.BankId,N.AccNo,B.Transcat,B.VendCust, N.Description ,N.DonorType,B.TransDate,B.BankTransID,N.RestrictionType,Ch.WebFolderName"
            StrSQL = StrSQL & " ) T Group by AccNo,Description,VoucherNo,Restrictiontype,BankID,TransDate,WebFolderName"
            'Response.Write(StrSQL)
            Try
                dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
                If dsDonation.Tables(0).Rows.Count > 0 Then
                    dgVoucher.Visible = True
                    Dim row As DataRow = dsDonation.Tables(0).NewRow
                    row("Amount") = 0
                    row("Description") = "Total"
                    Dim cnt As Integer
                    For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                        If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                            dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                            Exit For
                        End If
                    Next
                    dgVoucher.DataSource = dsDonation.Tables(0)
                    dgVoucher.DataBind()
                Else
                    dgVoucher.DataSource = Nothing
                    dgVoucher.DataBind()
                    dgVoucher.Visible = False
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString)
            End Try
        End If
    End Sub
    Private Sub GetCCRevenues(ByVal frmdate As String, ByVal todate As String)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        'dgVoucher
        Dim StrSQL As String
        todate = todate + " 23:59 "
        'AccNo,DonorType,DonationType,Description,Class,Amount
        If Session("DCatID") = 4 Then
            StrSQL = " SELECT 1 as H, Acc.AccNo as AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate, Acc.Description,Ch.WebFolderName as Class,(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) AS Amount,B.Ticker + '' + CONVERT(VARCHAR(25),b.TransDate,101) As I, MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID FROM BrokTrans B"
            StrSQL = StrSQL & " Inner Join NSFAccounts Acc On ((Acc.InvIncType ='Dividend' AND B.TransCat = 'OrdDiv') OR (Acc.InvIncType ='DivCG' AND  B.TransCat IN ('STCGDiv','LTCGDiv')) OR (Acc.InvIncType ='Interest' AND B.TransCat = 'Div')) AND ((Acc.RestrictionType ='Unrestricted' AND B.BankID IN (4,5,6)) OR (Acc.RestrictionType ='Temp Restricted' AND B.BankID =7))" '
            'StrSQL = StrSQL & "  Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType is NULL) OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK')"
            StrSQL = StrSQL & " INNER JOIN Chapter Ch ON Acc.ChapterID=Ch.ChapterID"
            StrSQL = StrSQL & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' "
            StrSQL = StrSQL & " AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Request.QueryString("BID") & ")"
            StrSQL = StrSQL & " Union All "
            StrSQL = StrSQL & " SELECT 0 as H, Acc.AccNo as AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate, Acc.Description,Ch.WebFolderName as Class,-SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) AS Amount,Case WHEN B.BankID = 4 Then 'ISCH' ELSE Case WHEN B.BankID = 5 Then 'IGEN' ELSE Case WHEN B.BankID = 6 Then 'IEND' ELSE 'IDAF' End End End + Replace(CONVERT(VARCHAR(10), '" & frmdate & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & todate & "', 101),'/','') as I, 0 as Mnth,0 as Yr, RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID FROM   BrokTrans B  Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK' AND Acc.InvIncType='MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted') OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK') "
            StrSQL = StrSQL & " INNER JOIN Chapter Ch ON Acc.ChapterID=Ch.ChapterID"
            StrSQL = StrSQL & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Request.QueryString("BID") & ") AND Acc.IntDiv='Y' Group By  B.BankID, Acc.AccNo,Acc.Description,Ch.WebFolderName  "
            'StrSQL = StrSQL & " Order By 1 Desc, B.BankID,YEAR(TransDate),MONTH(B.TransDate), Acc.AccNo "
            StrSQL = StrSQL & "  UNION ALL "
            StrSQL = StrSQL & " SELECT 0 as H, Acc.AccNo as AccNo,'' as DonorType,'' as DonationType, CONVERT(VARCHAR(10), B.TransDate ,101) as DepositDate, Acc.Description,Ch.WebFolderName as Class,-SUM(B.Amount) as Amount,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End + CONVERT(VARCHAR(25),b.TransDate,101) As I, MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) FROM BankTrans B Inner Join NSFAccounts Acc On Acc.BankID=B.BankID AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Cash' AND Acc.BankID in (1,2,3) INNER JOIN Chapter Ch ON Acc.ChapterID=Ch.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit' AND B.TransDate Between '" & frmdate & "' AND '" & todate & "'  AND B.BankID in (" & Request.QueryString("BID") & ")  AND Acc.IntDiv='Y' Group By CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo ,B.TransDate,Acc.Description,Ch.WebFolderName"
            StrSQL = StrSQL & "  UNION ALL "
            StrSQL = StrSQL & " SELECT 1 as H, Acc.AccNo as AccNo,'' as DonorType,'' as DonationType, CONVERT(VARCHAR(10), B.TransDate ,101) as DepositDate,Acc.Description,Ch.WebFolderName as Class,SUM(B.Amount) as Amount,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End + CONVERT(VARCHAR(25),b.TransDate,101) As I, MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) FROM BankTrans B Inner Join NSFAccounts Acc On (Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Interest') AND B.BankID in (1,2,3) INNER JOIN Chapter Ch ON Acc.ChapterID=Ch.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit'  AND B.TransDate Between '" & frmdate & "' AND '" & todate & "'  AND B.BankID in (" & Request.QueryString("BID") & ") Group By CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo ,B.TransDate,Acc.Description,Ch.WebFolderName Order by H desc"

        Else
            StrSQL = " SELECT  AccNo,DonorType,DonationType,DepositDate,Description,Class,H,SUM(Amount) as Amount FROM("
            StrSQL = StrSQL & " select  T.AccNo as AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate,N.Description,T.WebFolderName as Class,ROUND(Sum(T.Amount),2) as  Amount, 1 as H  from ("
            'Contestant
            StrSQL = StrSQL & " SELECT SUM(con.Fee)*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(Con.Fee)*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            ' ''DuplicateContestantReg
            StrSQL = StrSQL & " SELECT SUM(con.Fee)*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(Con.Fee)*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            'Refunds EventID=2 (for minus values)
            'StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            'StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment < 0 and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            'StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            'StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.TotalPayment < 0 and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            'Deleted PaymentReference EventID=3
            'StrSQL = StrSQL & " SELECT  SUM(R.Na_Amount)*2/3 as Amount, 41105  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber Inner Join Chapter C On R.ChapterID = C.ChapterID  where R.pa_reason<>'Refund Donation' and NFG.Fee is not null"
            'StrSQL = StrSQL & " and NFG.TotalPayment > 0 and NFG.asp_session_id in (SELECT Distinct NFG.asp_session_id  FROM NFG_Transactions NFG Left Join Registration R  On NFG.asp_session_id = R.PaymentReference   where  NFG.Fee is not null  and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0   Group by NFG.asp_session_id,NFG.Fee having SUM(ISNull(R.Fee,0))< NFG.Fee)   Group by C.WebFolderName, C.State,C.Name Union All"
            'StrSQL = StrSQL & " SELECT  SUM(R.Na_Amount)*1/3 as Amount, 51100  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber Inner Join Chapter C On R.ChapterID = C.ChapterID  where R.pa_reason<>'Refund Donation' and NFG.Fee is not null"
            'StrSQL = StrSQL & " and NFG.TotalPayment > 0 and NFG.asp_session_id in (SELECT Distinct NFG.asp_session_id  FROM NFG_Transactions NFG Left Join Registration R  On NFG.asp_session_id = R.PaymentReference   where  NFG.Fee is not null  and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0   Group by NFG.asp_session_id,NFG.Fee having SUM(ISNull(R.Fee,0))< NFG.Fee)   Group by C.WebFolderName, C.State,C.Name Union All"

            'Deleted PaymentReference EventID=2
            'StrSQL = StrSQL & " SELECT SUM(R.Na_Amount)*2/3 as Amount,41105  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name  FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber  Left Join (select SUM(fee) as fee,PaymentReference from Contestant where PaymentReference in (select asp_session_id from NFG_Transactions where TotalPayment > 0 and MS_TransDate Between '" & frmdate & "' AND '" & todate & "') Group By PaymentReference) Reg  On NFG.asp_session_id = Reg.PaymentReference Inner Join Chapter C On R.ChapterID = C.ChapterID    where  NFG.Fee is not null  "
            'StrSQL = StrSQL & " and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0 and R.pa_reason<>'Refund Donation' AND R.Na_Status = 'Approved'  Group by C.WebFolderName, C.State,C.Name,NFG.Fee having SUM(ISNull(Reg.Fee,0))< NFG.Fee  and SUM(R.Na_Amount)>0  Union All"
            'StrSQL = StrSQL & " SELECT SUM(R.Na_Amount)*1/3 as Amount,51100  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name  FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber  Left Join (select SUM(fee) as fee,PaymentReference from Contestant where PaymentReference in (select asp_session_id from NFG_Transactions where TotalPayment > 0 and MS_TransDate Between '" & frmdate & "' AND '" & todate & "') Group By PaymentReference) Reg  On NFG.asp_session_id = Reg.PaymentReference Inner Join Chapter C On R.ChapterID = C.ChapterID    where  NFG.Fee is not null  "
            'StrSQL = StrSQL & " and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0 and R.pa_reason<>'Refund Donation' AND R.Na_Status = 'Approved'  Group by C.WebFolderName, C.State,C.Name,NFG.Fee having SUM(ISNull(Reg.Fee,0))< NFG.Fee  and SUM(R.Na_Amount)>0  Union All"

            'Deleted PaymentReference EventID=3
            'StrSQL = StrSQL & " SELECT SUM(R.Na_Amount)*2/3 as Amount,41105  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name  FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber  Left Join (select SUM(fee) as fee,PaymentReference from Registration  where PaymentReference in (select asp_session_id from NFG_Transactions where TotalPayment > 0 and MS_TransDate Between '" & frmdate & "' AND '" & todate & "') Group By PaymentReference) Reg  On NFG.asp_session_id = Reg.PaymentReference Inner Join Chapter C On R.ChapterID = C.ChapterID    where  NFG.Fee is not null  "
            'StrSQL = StrSQL & " and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0 and R.pa_reason<>'Refund Donation' AND R.Na_Status = 'Approved' Group by C.WebFolderName, C.State,C.Name,NFG.Fee having SUM(ISNull(Reg.Fee,0))< NFG.Fee  and SUM(R.Na_Amount)>0  Union All"
            'StrSQL = StrSQL & " SELECT SUM(R.Na_Amount)*1/3 as Amount,51200  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name  FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber  Left Join (select SUM(fee) as fee,PaymentReference from Registration  where PaymentReference in (select asp_session_id from NFG_Transactions where TotalPayment > 0 and MS_TransDate Between '" & frmdate & "' AND '" & todate & "') Group By PaymentReference) Reg  On NFG.asp_session_id = Reg.PaymentReference Inner Join Chapter C On R.ChapterID = C.ChapterID    where  NFG.Fee is not null  "
            'StrSQL = StrSQL & " and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0 and R.pa_reason<>'Refund Donation' AND R.Na_Status = 'Approved'  Group by C.WebFolderName, C.State,C.Name,NFG.Fee having SUM(ISNull(Reg.Fee,0))< NFG.Fee  and SUM(R.Na_Amount)>0  Union All"

            ''Deleted PaymentReference EventID=2
            'StrSQL = StrSQL & " SELECT  SUM(R.Na_Amount)*2/3 as Amount, 41105  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber Inner Join Chapter C On R.ChapterID = C.ChapterID  where R.pa_reason<>'Refund Donation' and NFG.Fee is not null"
            'StrSQL = StrSQL & " and NFG.TotalPayment > 0 and NFG.asp_session_id in (SELECT Distinct NFG.asp_session_id  FROM NFG_Transactions NFG Left Join Registration R  On NFG.asp_session_id = R.PaymentReference   where  NFG.Fee is not null  and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0   Group by NFG.asp_session_id,NFG.Fee having SUM(ISNull(R.Fee,0))< NFG.Fee)    Group by C.WebFolderName, C.State,C.Name Union All"
            'StrSQL = StrSQL & " SELECT  SUM(R.Na_Amount)*1/3 as Amount, 51200  as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name FROM Refund R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.Na_OrderNumber Inner Join Chapter C On R.ChapterID = C.ChapterID  where R.pa_reason<>'Refund Donation' and NFG.Fee is not null"
            'StrSQL = StrSQL & " and NFG.TotalPayment > 0 and NFG.asp_session_id in (SELECT Distinct NFG.asp_session_id  FROM NFG_Transactions NFG Left Join Registration R  On NFG.asp_session_id = R.PaymentReference   where  NFG.Fee is not null  and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' and NFG.TotalPayment > 0   Group by NFG.asp_session_id,NFG.Fee having SUM(ISNull(R.Fee,0))< NFG.Fee)   Group by C.WebFolderName, C.State,C.Name Union All"

            'Refunds EventID=3 (for minus values)
            'StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            'StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.TotalPayment < 0 and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            'StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            'StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.TotalPayment < 0 and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            'Coaching
            StrSQL = StrSQL & " SELECT SUM(R.Fee)*2/3 as Amount, 41105 as AccNo,10 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM CoachReg R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = (CASE WHEN R.PaymentReference IS NULL THEN R.OrderNo ELSE R.PaymentReference END) Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=13 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(R.Fee)*1/3 as Amount, 51150 as AccNo,11 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM CoachReg R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = (CASE WHEN R.PaymentReference IS NULL THEN R.OrderNo ELSE R.PaymentReference END) Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=13 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            ''Refunds EventID=13 (for minus values)
            'StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*2/3 as Amount, 41105 as AccNo,10 as OrderNo,  C.WebFolderName, C.State,C.Name"
            'StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.TotalPayment < 0 and NFG.EventId=13 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            'StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51150 as AccNo,11 as OrderNo,  C.WebFolderName, C.State,C.Name"
            'StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.TotalPayment < 0 and NFG.EventId=13 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            'Registration
            StrSQL = StrSQL & " SELECT SUM(R.Fee)*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(R.Fee)*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            'Finals and Donations
            StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*2/3 as Amount, 41105 as AccNo,5 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name " ' C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Transactions NFG  where NFG.EventId=1 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0)  Union All"
            StrSQL = StrSQL & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51100 as AccNo,6 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"  ' C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Transactions NFG  where NFG.MatchedStatus='Y' and NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0)  Union All"

            ''NFG_Supp--Contestant
            StrSQL = StrSQL & " SELECT SUM(ISNULL(con.Amount_pars,0))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(ISNULL(Con.Amount_pars,0))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            'NFG_Supp----Registration
            StrSQL = StrSQL & " SELECT SUM(ISNULL(R.Amount_pars,0))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(ISNULL(R.Amount_pars,0))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null  and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            ''Finals
            StrSQL = StrSQL & " SELECT SUM(ISNULL(Con.Amount_pars,0))*2/3 as Amount, 41105 as AccNo,5 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"  ' C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference where NFG.Fee is not null and NFG.EventId=1 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0)  Union All"
            StrSQL = StrSQL & " SELECT SUM(ISNULL(Con.Amount_pars,0))*1/3 as Amount, 51100 as AccNo,6 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name " ' C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference where NFG.Fee is not null  and NFG.EventId=1 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0)  Union All"

            ''NFG_Supp---Coaching
            StrSQL = StrSQL & " SELECT SUM(ISNULL(R.Amount_pars,0))*2/3 as Amount, 41105 as AccNo,10 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.EventId=13 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQL = StrSQL & " SELECT SUM(ISNULL(R.Amount_pars,0))*1/3 as Amount, 51150 as AccNo,11 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.EventId=13 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            'StrSQL = StrSQL & " SELECT SUM(NF.Amount_pars) as Amount,85300 as AccNo,12 as OrderNo,C.WebFolderName, C.State,C.Name FROM NFG_Supp NF Inner Join  NFG_Transactions NFG On NFG.asp_session_id = NF.PaymentReference and (NFG.MatchedStatus = NF.MatchedStatus Or (NF.MatchedStatus is null and NFG.MatchedStatus is null)) Inner Join Chapter C ON NFG.ChapterID = C.ChapterID  where NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name UNION ALL"

            'Meals
            StrSQL = StrSQL & " SELECT SUM(NFG.MealsAmount)as Amount, 51100 as AccNo,7 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name " ' C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Transactions NFG   where (NFG.MealsAmount>0 or NFG.MealsAmount<0) and NFG.MealsAmount is not Null and NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.MealsAmount)>0  or SUM(NFG.MealsAmount)<0) Union All"
            StrSQL = StrSQL & " SELECT SUM(NFG.[Contribution Amount])as Amount, 41101 as AccNo,8 as OrderNo,  ISNULL(C.WebFolderName,'US_HomeOffice') as WebFolderName, ISNULL(C.State,'IL') as State,ISNULL(C.Name,'Home Office') as Name "
            StrSQL = StrSQL & " FROM NFG_Transactions NFG Left Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[Contribution Amount] > 0 or NFG.[Contribution Amount] < 0) and NFG.EventID not in (10) and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name Union All"
            StrSQL = StrSQL & " SELECT SUM(NFG.[TotalPayment])as Amount, 51400 as AccNo,9 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQL = StrSQL & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID  where  (NFG.[TotalPayment] > 0 or NFG.[TotalPayment] < 0) and NFG.EventID = 10 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name) T"
            StrSQL = StrSQL & " inner Join  NSFAccounts N ON T.AccNo = N.AccNo "
            StrSQL = StrSQL & " Group by T.AccNo,N.Description,T.WebFolderName,T.State,T.Name"
            StrSQL = StrSQL & " UNION ALL "

            ''Modified on ''05-11-2012
            StrSQL = StrSQL & " select N.AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate,N.Description,'US_HomeOffice' as Class,-Sum(NFG.TotalPayment)as  Amount,0 as H  From NFG_Transactions NFG Inner Join NSFAccounts N ON N.BankID=1 and N.RestrictionType='Unrestricted' where NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group By N.AccNo,N.Description  having SUM(NFG.[TotalPayment]) > 0 or SUM(NFG.[TotalPayment]) < 0" 'Order By H Desc

            ''Added To Merge  CCRevenueAdjustments ''05-11-2012
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " select  Temp.AccNo as AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate,Temp.Description,Temp.F as Class,ROUND(Sum(Temp.Amount),2) as  Amount, H  from "
            StrSQL = StrSQL & "(Select 'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*2/3 as Amount,41105 as AccNo,'Donation Fees – Unrestricted' as Description,'US_HomeOffice' as F,1 as H,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & frmdate & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & todate & "', 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
            StrSQL = StrSQL & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQL = StrSQL & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
            StrSQL = StrSQL & ")T1 Group by TransDate  UNION ALL "
            StrSQL = StrSQL & "Select 'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51100 as AccNo,'Program Service Fees - Contests' as Description, 'US_HomeOffice' as F,1 as H,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & frmdate & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & todate & "', 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
            StrSQL = StrSQL & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQL = StrSQL & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))"
            StrSQL = StrSQL & ") T2 Group by TransDate UNION ALL "
            StrSQL = StrSQL & "Select 'CGN' as TransType,-(SUM(CreditCC)-SUM(Registrations)) as Amount,10101 as AccNo,'Chase Bank - Unrestricted ' as Description,'US_HomeOffice' as F,0 as H,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & frmdate & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & todate & "', 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from "
            StrSQL = StrSQL & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQL = StrSQL & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
            StrSQL = StrSQL & ") T3 Group by TransDate "
            StrSQL = StrSQL & " )Temp Group by Temp.AccNo,F,Description,H"
            StrSQL = StrSQL & " ) VC Group by AccNo,Class,Description,H,DonorType,DonationType,DepositDate Order by H desc"
            'StrSQL = StrSQL & " UNION ALL select N.AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate,N.Description,Ch.WebFolderName as Class,-Sum(NFG.TotalPayment)as  Amount,0 as H  From NFG_Transactions NFG Inner Join NSFAccounts N ON N.BankID=1 and N.RestrictionType='Unrestricted' Inner Join Chapter Ch On N.ChapterID=Ch.ChapterID where NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group By N.AccNo,N.Description,Ch.WebFolderName having SUM(NFG.[TotalPayment]) > 0 or SUM(NFG.[TotalPayment]) < 0 Order By H Desc"

        End If
        'Response.Write(StrSQL)
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
        If dsDonation.Tables(0).Rows.Count > 0 Then
            dgVoucher.Visible = True
            Dim row As DataRow = dsDonation.Tables(0).NewRow
            row("Amount") = 0
            row("Description") = "Total"
            If Session("DCatID") = 4 Then row("I") = ""
            Dim cnt As Integer
            For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                    dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                    Exit For
                End If
            Next
            If Session("DCatID") = 4 Then dgVoucher.Columns(4).Visible = True
            dgVoucher.DataSource = dsDonation.Tables(0)
            dgVoucher.DataBind()
            'If ((dsDonation.Tables(0).Rows(0)("DonorType").ToString().Trim() = "IND") Or (dsDonation.Tables(0).Rows(0)("DonorType").ToString().Trim() = "SPOUSE")) Then
            '    lblRequester.Text = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select Firstname + ' '+ lastname from IndSpouse where automemberid=" & dsDonation.Tables(0).Rows(0)("ReimbMemberID").ToString() & "")
            'ElseIf (dsDonation.Tables(0).Rows(0)("DonorType").ToString().Trim() = "OWN") Then
            '    lblRequester.Text = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select Organization_Name from  organizationInfo where automemberid=" & dsDonation.Tables(0).Rows(0)("ReimbMemberID").ToString() & "")
            'End If
        Else
            dgVoucher.DataSource = Nothing
            dgVoucher.DataBind()
            dgVoucher.Visible = False
        End If
    End Sub

    Private Sub GetExpenseReceipt(ByVal Depositslip As String, ByVal DDate As String)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        'dgVoucher
        strSql = " SELECT 1 as H, EJ.ReimbMemberID,EJ.CheckNumber,EJ.DonorType,Null as DonationType,CONVERT(VARCHAR(10), EJ.DatePaid, 101) AS DepositDate, CASE WHEN EJ.TransType = 'Transfers'  THEN BS.Accno ELSE  EC.Account END as AccNo, CASE WHEN EJ.TransType = 'Transfers' THEN BS.Description ELSE  EC.AccountName END As Description, " ' CASE WHEN EJ.TransType = 'Transfers' THEN BS.Description ELSE CASE WHEN EJ.ChapterID=1 and EJ.EventID is null and EJ.TransType = 'GenAdmin' THEN 'US_HomeAdmin' ELSE  EC.AccountName END END
        strSql = strSql & " CASE WHEN EJ.ChapterID=1 and EJ.EventID is null and EJ.TransType = 'GenAdmin' THEN 'US_HomeAdmin' ELSE  CASE WHEN EJ.ChapterID=1 and EJ.EventID = 1 and EJ.Transtype='FinalsExp' THEN  'US_Finals' ELSE Ch.webfoldername END END  as Class,CASE WHEN EJ.ChapterID=1 and EventID is Null and  EJ.TransType  <>'GenAdmin' and EJ.TransType <> 'Grants'  and EJ.TransType <> 'Transfers' THEN 'False' Else 'True' END AS ErrFlag,Ch.ChapterCode,Ch.ChapterID,SUM(EJ.ExpenseAmount) as Amount,ATT.Name as TransType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,EJ.BankID),2) As BankID "
        strSql = strSql & " from ExpJournal EJ Left Join Bank B ON B.BankID=EJ.BankID Left Outer Join ExpenseCategory EC ON  EJ.ExpCatID=EC.ExpCatID  "
        strSql = strSql & " Left Outer Join NSFAccounts BS ON  BS.BankID=EJ.ToBankID and BS.RestrictionType=EJ.RestTypeFrom and EJ.TransType = 'Transfers' Inner Join AcctgTransType ATT ON ATT.Code = EJ.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4,5) Inner Join Chapter Ch ON Ch.ChapterID = CASE WHEN  EJ.ToChapterID IS NULL THEN EJ.ChapterID ELSE EJ.ToChapterID END "
        strSql = strSql & " WHERE DateDiff(d,EJ.DatePaid,'" & DDate & "')=0 AND EJ.CheckNumber='" & Depositslip & "'"
        strSql = strSql & " Group by EJ.CheckNumber,EJ.DonorType,EJ.ReimbMemberID,EJ.DatePaid,EC.Account,EC.AccountName,Ch.webfoldername,Ch.ChapterCode,Ch.ChapterID,ATT.Name,B.BankCode,EJ.TransType,BS.Description,BS.Accno,EJ.BankID,EJ.EventID,EJ.ChapterID "
        strSql = strSql & " UNION ALL"
        strSql = strSql & " select 0 as H,0 as ReimbMemberID,EJ.CheckNumber,'' as DonorType,Null as DonationType,CONVERT(VARCHAR(10), EJ.DatePaid, 101) AS DepositDate,N.AccNo,N.Description,Ch.WebFolderName as Class,'True' as ErrFlag,'' as ChapterCode,1 as ChapterID,-SUM(EJ.ExpenseAmount) as Amount,'' as TransType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,EJ.BankID),2) As BankID  From ExpJournal EJ Left Join Bank B ON B.BankID=EJ.BankID Inner Join NSFAccounts N ON EJ.BankID=N.BankID and EJ.RestTypeFrom=N.RestrictionType and N.[Level]='L' and N.InvIncType IN ('MMK','CASH') Left Join OrganizationInfo O ON EJ.DonorType = 'OWN' and O.AutoMemberid =EJ.ReimbMemberid Left Join IndSpouse I ON EJ.DonorType <> 'OWN' AND EJ.ReimbMemberid = I.AutoMemberID Inner Join Chapter Ch On N.ChapterID=Ch.ChapterID WHERE EJ.CheckNumber = '" & Depositslip & "' AND DateDiff(d,EJ.DatePaid,'" & DDate & "')=0 Group by EJ.Datepaid,EJ.CheckNumber,N.AccNo,EJ.BankID,I.FirstName,I.LastName,O.ORGANIZATION_NAME,EJ.DonorType,N.Description,B.BankCode,Ch.WebFolderName Order By H desc,10"

        'Response.Write(strSql)
        'StrSQL = " Select T.DonorType,T.AccNo, T.Description,T.DepositDate, T.Class,T.BusinessType,T.DonationType,T.ChapterID,SUM(T.AMOUNT) as Amount   from"
        'StrSQL = StrSQL & " (SELECT D.DepositSlip,CONVERT(VARCHAR(10), D.DepositDate, 101) AS DepositDate ,CASE WHEN D.DonorType = 'SPOUSE' THEN 'IND' ELSE D.DonorType END as DonorType,D.MEMBERID, Acc.AccNo, "
        'StrSQL = StrSQL & " Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,D.AMOUNT ,Acc.DonationType,Acc.BusinessType FROM DonationsInfo D "
        'StrSQL = StrSQL & " Inner Join NSFAccounts Acc ON Acc.DonorType='IND/SPOUSE' AND D.DonationType=Acc.DonationType Inner JOIn Chapter Ch ON D.ChapterID=Ch.ChapterID WHERE D.DonorType <>'OWN' "
        ''StrSQL = StrSQL & " AND D.DepositSlip=1231212 AND D.DepositDate='01/24/2011' "
        'StrSQL = StrSQL & " AND D.DepositSlip=" & Depositslip & " AND D.DepositDate='" & DDate & "'"
        'StrSQL = StrSQL & " UNION ALL SELECT D.DepositSlip,CONVERT(VARCHAR(10), D.DepositDate, 101) AS DepositDate,D.DonorType,D.MEMBERID, Acc.AccNo, Acc.Description, "
        'StrSQL = StrSQL & " Ch.webfoldername as Class,Ch.ChapterID,D.AMOUNT ,Acc.DonationType,Acc.BusinessType FROM DonationsInfo D "
        'StrSQL = StrSQL & " Inner Join NSFAccounts Acc ON D.DonorType = Acc.DonorType AND D.DonationType=Acc.DonationType "
        'StrSQL = StrSQL & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID Inner Join OrganizationInfo O ON D.MemberID = O.Automemberid AND D.DonorType = 'OWN' AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)','Non-proft,PAC', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END WHERE "
        ''StrSQL = StrSQL & " D.DepositSlip=1231212 AND D.DepositDate='01/24/2011'"
        'StrSQL = StrSQL & " D.DepositSlip=" & Depositslip & " AND D.DepositDate='" & DDate & "'"
        'StrSQL = StrSQL & " ) T Group By T.DonorType,T.BusinessType,T.DonationType,T.ChapterID,T.AccNo, T.Description, T.Class,T.DepositDate Order By T.BusinessType,T.DonationType,T.ChapterID"
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        If dsDonation.Tables(0).Rows.Count > 0 Then
            dgVoucher.Visible = True
            Dim row As DataRow = dsDonation.Tables(0).NewRow
            row("Amount") = 0
            row("Description") = "Total"
            Dim cnt As Integer
            For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                    dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                    Exit For
                End If
            Next
            dgVoucher.DataSource = dsDonation.Tables(0)
            dgVoucher.DataBind()
            Dim i As Integer = 0 '
            For i = 0 To dsDonation.Tables(0).Rows.Count - 1
                If dsDonation.Tables(0).Rows(i)("ErrFlag").ToString().Trim = "False" Then
                    lblError.Text = "EventID is Null : " & dsDonation.Tables(0).Rows(i)("AccNo").ToString() & " - " & dsDonation.Tables(0).Rows(i)("Description").ToString() & " : $" & dsDonation.Tables(0).Rows(i)("Amount").ToString().Replace(".0000", ".00") & "<br>"
                End If
            Next
            lblBankAccount.Text = "Bank Account: Prog. Services/" & dsDonation.Tables(0).Rows(0)("BanKName")
            If ((dsDonation.Tables(0).Rows(0)("DonorType").ToString().Trim() = "IND") Or (dsDonation.Tables(0).Rows(0)("DonorType").ToString().Trim() = "SPOUSE")) Then
                lblRequester.Text = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select Firstname + ' '+ lastname from IndSpouse where automemberid=" & dsDonation.Tables(0).Rows(0)("ReimbMemberID").ToString() & "")
            ElseIf (dsDonation.Tables(0).Rows(0)("DonorType").ToString().Trim() = "OWN") Then
                lblRequester.Text = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select Organization_Name from  organizationInfo where automemberid=" & dsDonation.Tables(0).Rows(0)("ReimbMemberID").ToString() & "")
            End If
        Else
            dgVoucher.DataSource = Nothing
            dgVoucher.DataBind()
            dgVoucher.Visible = False
        End If
    End Sub

    Private Sub GetDonationReceipt(ByVal Depositslip As Integer, ByVal DDate As String)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        'dgVoucher
        '** Account Number eventwise
        ' Finals 1/3 51100 2/3 41105
        'Regionals 1/3 51100 2/3 41105
        'WorkShop 1/3 51200 2/3  41105
        'Coaching 1/3 51150 2/3 41105...

        '51150 for Coaching... Program Service Fees - Coaching
        Dim StrSQL As String
        'StrSQL = " SELECT D.DepositSlip,CONVERT(VARCHAR(10), D.DepositDate, 101)  AS DepositDate ,D.DonorType,D.MEMBERID,D.DonationType, Acc.AccNo, Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,D.AMOUNT ,D.DonationType,Acc.RestrictionType,Acc.BusinessType"
        'StrSQL = StrSQL & " FROM DonationsInfo D Inner Join  NSFAccounts Acc ON   Acc.DonorType='IND/SPOUSE'  AND D.DonationType=Acc.DonationType"
        'StrSQL = StrSQL & " Inner JOIn Chapter Ch ON D.ChapterID=Ch.ChapterID WHERE D.DonorType <>'OWN' AND D.DepositSlip=" & Depositslip & " AND D.DepositDate='" & DDate & "'"
        'StrSQL = StrSQL & " UNION ALL"
        'StrSQL = StrSQL & " SELECT D.DepositSlip,CONVERT(VARCHAR(10), D.DepositDate, 101)  AS DepositDate,D.DonorType,D.MEMBERID,D.DonationType, Acc.AccNo, Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,D.AMOUNT ,D.DonationType,Acc.DonationType,Acc.BusinessType"
        'StrSQL = StrSQL & " FROM DonationsInfo D Inner Join  NSFAccounts Acc ON D.DonorType = Acc.DonorType  AND D.DonationType=Acc.DonationType"
        'StrSQL = StrSQL & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID"
        'StrSQL = StrSQL & " Inner Join  OrganizationInfo O ON D.MemberID = O.Automemberid AND D.DonorType = 'OWN' AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END"
        'StrSQL = StrSQL & " WHERE D.DepositSlip=" & Depositslip & " AND D.DepositDate='" & DDate & "'"
        'StrSQL = StrSQL & " Order By D.DonorType,Acc.BusinessType,D.DonationType,Ch.ChapterID"
        'StrSQL = StrSQL & " Group By D.DepositSlip,D.DepositDate ,D.DonorType,D.MEMBERID,D.DonationType, Acc.AccNo, Acc.Description, Ch.webfoldername,Ch.ChapterID,D.AMOUNT ,D.DonationType,Acc.DonationType,Acc.BusinessType"
        If Session("DCatID") = 2 Then
            'StrSQL = " SELECT OD.DepositSlipNo,CONVERT(VARCHAR(10), OD.DepositDate, 101) AS DepositDate ,CASE WHEN OD.DonorType = 'SPOUSE' THEN 'IND' ELSE OD.DonorType END as DonorType,OD.MEMBERID, Acc.AccNo, Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,SUM(OD.AMOUNT) as Amount ,Acc.RestrictionType as DonationType,Acc.BusinessType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) as BankID FROM OtherDeposits OD Left Join Bank B ON B.BankID=OD.BankID Inner Join NSFAccounts Acc ON Acc.DonorType = CASE WHEN OD.DonorType = 'SPOUSE' OR OD.DonorType = 'IND' OR OD.DonorType IS Null  THEN 'IND/SPOUSE' ELSE OD.DonorType END  AND OD.RestrictionType=Acc.RestrictionType Inner JOIn Chapter Ch ON OD.ChapterID=Ch.ChapterID WHERE  OD.DepositSlipNo=" & Depositslip & " AND OD.DepositDate='" & DDate & "' Group BY OD.DepositSlipNo,OD.DepositDate,OD.DonorType,OD.MEMBERID, Acc.AccNo, Acc.Description, Ch.webfoldername,Ch.ChapterID,Acc.RestrictionType,Acc.BusinessType,B.BankCode,OD.BankID "
            StrSQL = " select  T.DepositDate, " & Depositslip & " as DepositSlipNo,'' as DonorType, T.AccNo,N.Description,T.WebFolderName as Class,ROUND(Sum(T.Amount),2) as  Amount,1 as H, T.VoucherNo,N.RestrictionType as DonationType,N.BusinessType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,T.BankID),2) as BankID,T.RevenueType  from ("
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID, SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51200  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "
            'coaching added on october 05 2012
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "

            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51400  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID UNION ALL "
            StrSQL = StrSQL & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,CASE WHEN OD.EventID=1 or OD.EventID=2 THEN 51100 ELSE CASE WHEN OD.EventID=3 THEN 51200 ELSE 51300 END END  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sponsorship' AND OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID)"
            StrSQL = StrSQL & " T Left Join Bank B ON B.BankID=T.BankID inner Join  NSFAccounts N ON T.AccNo = N.AccNo Group by T.RevenueType,T.DepositDate,T.VoucherNo,T.AccNo,N.Description,T.WebFolderName,T.State,T.BankID,T.Name,N.RestrictionType,N.BusinessType,B.BankCode "
            StrSQL = StrSQL & " UNION ALL"
            StrSQL = StrSQL & " select CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate," & Depositslip & " as DepositSlipNo,'' as DonorType,N.AccNo,N.Description,Ch.WebFolderName as Class,-SUM(OD.AMOUNT) as  Amount,0 as H, 'OTH'+ RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,N.RestrictionType as DonationType,N.BusinessType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) as BankID,OD.RevenueType  From OtherDeposits OD Inner Join NSFAccounts N ON OD.BankID=N.BankID and OD.RestrictionType=N.RestrictionType and  N.[Level]='L' AND (N.InvIncType IS NULL OR N.InvIncType<>'Inv') Left Join Bank B ON B.BankID=OD.BankID  Inner Join Chapter Ch ON N.ChapterID=Ch.ChapterID  WHERE OD.DepositSlipNo = " & Depositslip & " AND OD.DepositDate='" & DDate & "'" & " Group by OD.DepositDate,OD.DepositSlipNo,N.AccNo,OD.BankID,N.Description,OD.RevenueType,N.RestrictionType,N.BusinessType,B.BankCode,Ch.WebFolderName Order By H Desc"

        Else
            StrSQL = " Select T.DonorType,T.AccNo, T.Description,T.DepositDate, T.Class,T.BusinessType,T.RestrictionType as DonationType,T.ChapterID,SUM(T.AMOUNT) as Amount, 1 as H,T.BankName,T.BankID   from"
            StrSQL = StrSQL & " (SELECT D.DepositSlip,CONVERT(VARCHAR(10), D.DepositDate, 101) AS DepositDate ,CASE WHEN D.DonorType = 'SPOUSE' THEN 'IND' ELSE D.DonorType END as DonorType,D.MEMBERID, Acc.AccNo, "
            StrSQL = StrSQL & " Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,D.AMOUNT ,Acc.RestrictionType,Acc.BusinessType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) as BankID FROM DonationsInfo D Left Join Bank B ON B.BankID=D.BankID"
            StrSQL = StrSQL & " Inner Join NSFAccounts Acc ON Acc.DonorType='IND/SPOUSE' AND D.DonationType=Acc.RestrictionType Inner JOIn Chapter Ch ON D.ChapterID=Ch.ChapterID WHERE D.DonorType <>'OWN' "
            'StrSQL = StrSQL & " AND D.DepositSlip=1231212 AND D.DepositDate='01/24/2011' "
            StrSQL = StrSQL & " AND D.DepositSlip=" & Depositslip & " AND D.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " UNION ALL SELECT D.DepositSlip,CONVERT(VARCHAR(10), D.DepositDate, 101) AS DepositDate,D.DonorType,D.MEMBERID, Acc.AccNo, Acc.Description, "
            StrSQL = StrSQL & " Ch.webfoldername as Class,Ch.ChapterID,D.AMOUNT ,Acc.RestrictionType,Acc.BusinessType,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) as BankID FROM DonationsInfo D Left Join Bank B ON B.BankID=D.BankID"
            StrSQL = StrSQL & " Inner Join NSFAccounts Acc ON D.DonorType = Acc.DonorType AND D.DonationType=Acc.RestrictionType "
            StrSQL = StrSQL & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID Inner Join OrganizationInfo O ON D.MemberID = O.Automemberid AND D.DonorType = 'OWN' AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)','Non-proft,PAC', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END WHERE "
            'StrSQL = StrSQL & " D.DepositSlip=1231212 AND D.DepositDate='01/24/2011'"
            StrSQL = StrSQL & " D.DepositSlip=" & Depositslip & " AND D.DepositDate='" & DDate & "'"
            StrSQL = StrSQL & " ) T Group By T.DonorType,T.BusinessType,T.RestrictionType,T.ChapterID,T.AccNo, T.Description, T.Class,T.DepositDate,T.BankName,T.BankID "
            StrSQL = StrSQL & " UNION ALL"
            StrSQL = StrSQL & " select '' as DonorType,N.AccNo,N.Description,CONVERT(VARCHAR(10), D.DepositDate, 101) as DepositDate,Ch.WebFolderName as Class,N.BusinessType,N.RestrictionType as DonationType,1 as ChapterID,-SUM(D.AMOUNT) as  Amount,0 as H,ISNULL(B.BankCode,'') as BankName,RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) as BankID From DonationsInfo D Inner Join NSFAccounts N ON D.BankID=N.BankID and D.DonationType=N.RestrictionType and  N.[Level]='L' AND (N.InvIncType IS NULL OR N.InvIncType<>'Inv') Left Join Bank B ON B.BankID=D.BankID Inner Join Chapter Ch ON N.ChapterID = Ch.ChapterID WHERE D.DepositSlip = " & Depositslip & " AND D.DepositDate='" & DDate & "'" & " Group by D.DepositDate,D.DepositSlip,N.AccNo,D.BankID,N.Description,N.RestrictionType,N.BusinessType,B.BankCode,Ch.WebfolderName Order By H Desc,6,7,8"

        End If

        'Response.Write(StrSQL)
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
        If dsDonation.Tables(0).Rows.Count > 0 Then
            dgVoucher.Visible = True
            Dim row As DataRow = dsDonation.Tables(0).NewRow
            row("Amount") = 0
            row("Description") = "Total"
            Dim cnt As Integer
            For cnt = 0 To dsDonation.Tables(0).Rows.Count - 1
                If dsDonation.Tables(0).Rows(cnt)("H") = 0 Then
                    dsDonation.Tables(0).Rows.InsertAt(row, cnt)
                    Exit For
                End If
            Next
            dgVoucher.DataSource = dsDonation.Tables(0)
            dgVoucher.DataBind()
            lblBankAccount.Text = "Bank Account: " & dsDonation.Tables(0).Rows(0)("BanKName")
            If Session("DCatID") = 2 Then
                lblPurpose.Text = IIf(IsDBNull(dsDonation.Tables(0).Rows(0)("RevenueType")), "", dsDonation.Tables(0).Rows(0)("RevenueType"))
                lblChaseTransNo.Text = dsDonation.Tables(0).Rows(0)("BanKName") & " Trans#: " & Request.QueryString("DSlip")
            Else
                lblRequester.Text = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select Firstname + ' '+ lastname from IndSpouse where automemberid=" & Session("LoginID") & "")
            End If
        Else
            'Response.Write(StrSQL)
            dgVoucher.DataSource = Nothing
            dgVoucher.DataBind()
            dgVoucher.Visible = False
        End If
    End Sub

    Protected Sub dgVoucher_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVoucher.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'Response.Write("<br />")
            Dim donationAmount As Decimal = DataBinder.Eval(e.Item.DataItem, "Amount")
            'Response.Write("***LIst***" & donationAmount)
            If DataBinder.Eval(e.Item.DataItem, "Description") = "Total" Then
                CType(e.Item.FindControl("lblAmount"), Label).Text = FormatNumber(IIf(dblRegFee = 0.0, 0, dblRegFee), 2)
            Else
                dblRegFee += donationAmount
            End If
            'Response.Write(dblRegFee.ToString() & "<br>")
            'MsgBox(DataBinder.Eval(e.Item.DataItem, "DepositDate"))
            lblDDate.Text = DataBinder.Eval(e.Item.DataItem, "DepositDate", "{0:d}")

            'Voucher Number:  For chase checks = "DGN" + Date  ("05/06/2010") + Deposit Slip #  Ex: DGN05062010129
            If Request.QueryString("DType").ToUpper() = "D" Then
                If Session("DCatID") = 2 Then
                    lblDepositNo.Text = "Deposit No: OTH"
                Else
                    lblDepositNo.Text = "Deposit No: DGN"
                    lblChaseTransNo.Text = "Chase Trans#: " & Request.QueryString("DSlip")
                    lblPurpose.Text = "Donation Checks"
                End If
                lblDepositNo.Text = lblDepositNo.Text & DataBinder.Eval(e.Item.DataItem, "BankID").ToString() & DataBinder.Eval(e.Item.DataItem, "DepositDate").ToString().Replace("/", "") & Request.QueryString("DSlip")
            ElseIf Request.QueryString("DType").ToUpper() = "EX" Then
                lblDepositNo.Text = "Voucher No: VPS" & DataBinder.Eval(e.Item.DataItem, "BankID").ToString() & DataBinder.Eval(e.Item.DataItem, "DepositDate").ToString().Replace("/", "") & Request.QueryString("DSlip")
                lblChaseTransNo.Text = "Check No: " & Request.QueryString("DSlip")
                dgVoucher.Columns(0).Visible = False
                lblPurpose.Text = DataBinder.Eval(e.Item.DataItem, "TransType").ToString() ' DataBinder.Eval(e.Item.DataItem, "ChapterCode") & " - " &
            ElseIf Request.QueryString("DType").ToUpper() = "CC" And Not Session("DCatID") = 4 Then
                lblDepositNo.Text = "Voucher No: CGN01" & Request.QueryString("SDt").ToString().Replace("/", "") & "_" & Request.QueryString("EDt").ToString().Replace("/", "")
                lblBankAccount.Text = "Bank Account: Chase"
                'lblChaseTransNo.Text = "Check No: " & Request.QueryString("DSlip")
                dgVoucher.Columns(0).Visible = False
                ' lblPurpose.Text = DataBinder.Eval(e.Item.DataItem, "ChapterCode") & " - " & DataBinder.Eval(e.Item.DataItem, "TransType").ToString()
            ElseIf Request.QueryString("DType").ToUpper() = "CC" And Session("DCatID") = 4 Then
                CType(e.Item.FindControl("lblDocNum"), Label).Text = DataBinder.Eval(e.Item.DataItem, "I")
                lblDepositNo.Text = "Cash Receipt No: IGN" & DataBinder.Eval(e.Item.DataItem, "BankID").ToString() & Request.QueryString("SDt").ToString().Replace("/", "") & "_" & Request.QueryString("EDt").ToString().Replace("/", "")
                lblBankAccount.Text = ""
                Try
                    lblBankAccount.Text = " Bank Account: " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT BankName FROM Bank WHERE BanKID=" & Request.QueryString("BID")) 'No specific bank Accounts
                Catch ex As Exception

                End Try
                'lblChaseTransNo.Text = "Check No: " & Request.QueryString("DSlip")
                dgVoucher.Columns(0).Visible = False
                lblPurpose.Text = "Investment Income" 'DataBinder.Eval(e.Item.DataItem, "ChapterCode") & " - " & DataBinder.Eval(e.Item.DataItem, "TransType").ToString()
            ElseIf Request.QueryString("DType").ToUpper() = "ACT_EFT" And Session("DCatID") = 6 Then
                lblDepositNo.Text = "Voucher No: DGE0" & Request.QueryString("BankID").ToString() & Request.QueryString("TDate").ToString().Replace("/", "") & Request.QueryString("BankTransID").ToString()
                dgVoucher.Columns(0).Visible = False
                lblBankAccount.Text = " Bank Account: " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT BankName FROM Bank WHERE BanKID=" & Request.QueryString("BankID"))
            ElseIf Request.QueryString("DType").ToUpper() = "CCFEES" And Session("DCatID") = 7 Then
                lblDepositNo.Text = "Voucher No: " & Request.QueryString("VoucherNo").ToString() '& Request.QueryString("TDate").ToString().Replace("/", "") & Request.QueryString("BankTransID").ToString()
                dgVoucher.Columns(0).Visible = False
                lblDDate.Text = DataBinder.Eval(e.Item.DataItem, "DepositDate", "{0:d}")
                lblBankAccount.Text = " Bank Account: " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT BankName FROM Bank WHERE BanKID=" & Request.QueryString("BankID"))
            End If
        End If
        lblTotal.Text = FormatCurrency(IIf(dblRegFee = 0.0, 0, dblRegFee), 2)
    End Sub

    Protected Sub dgVoucher_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVoucher.ItemCommand
        Dim StrSQl As String
        Dim index As Integer = Integer.Parse(dgVoucher.SelectedIndex)
        If e.CommandName = "EditRow" Then
            If Session("DCatID") = 2 Then
                StrSQl = " SELECT OD.OtherDepositID as DonationID, OD.DepositSlipNo as DepositSlip,CASE WHEN OD.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as DName,CONVERT(VARCHAR(10), OD.DepositDate, 101) AS DepositDate,CONVERT(VARCHAR(10), OD.DepositDate, 101) AS DonationDate,OD.RevenueType as BusType,CASE WHEN OD.RevenueType='Sales' then SC.SalesCatDesc ELSE S.SponsorDesc END as IRScat ,CASE WHEN OD.DonorType = 'SPOUSE' THEN 'IND' ELSE OD.DonorType END as DonorType,OD.MEMBERID, Acc.AccNo, Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,Ch.ChapterCode,OD.AMOUNT,Acc.RestrictionType as DonationType,Acc.BusinessType,ISNULL(B.BankCode,'') as BankName"
                StrSQl = StrSQl & "  FROM OtherDeposits OD Left Join Bank B ON B.BankID=OD.BankID Inner Join NSFAccounts Acc ON Acc.DonorType = CASE WHEN OD.DonorType = 'SPOUSE' OR OD.DonorType = 'IND' OR OD.DonorType IS Null  THEN 'IND/SPOUSE' ELSE OD.DonorType END  AND OD.RestrictionType=Acc.RestrictionType Inner JOIn Chapter Ch ON OD.ChapterID=Ch.ChapterID"
                StrSQl = StrSQl & " Left Join OrganizationInfo O ON OD.DonorType = 'OWN' and O.AutoMemberid =OD.Memberid"
                StrSQl = StrSQl & " Left Join IndSpouse I ON OD.DonorType <> 'OWN' AND OD.MEMBERID = I.AutoMemberID "
                StrSQl = StrSQl & " Left Join  SalesCat SC ON  SC.salescatID=OD.SalesID AND OD.RevenueType='Sales' "
                StrSQl = StrSQl & " Left Join SponsorCat S ON S.SponsorID= OD.SponsorID AND OD.RevenueType='Sponsor' "
                StrSQl = StrSQl & " WHERE  OD.DepositSlipNo=" & Request.QueryString("DSlip") & ""
                StrSQl = StrSQl & "  AND OD.DepositDate='" & Request.QueryString("DDate") & "' AND Ch.webfoldername='" & CType(e.Item.FindControl("lblClass"), Label).Text.Trim() & "'"
                grdEditVoucher.Columns(0).Visible = False
                grdEditVoucher.Columns(4).Visible = False
                grdEditVoucher.Columns(7).HeaderText = "RevenueType"
                grdEditVoucher.Columns(8).HeaderText = "Sales/SponsorCat"
            Else
                StrSQl = " Select  D.DonationID, D.DepositSlip, CASE WHEN D.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as DName,CONVERT(VARCHAR(10), D.DonationDate, 101) as DonationDate, CONVERT(VARCHAR(10), D.DepositDate, 101)   as DepositDate, D.DonorType, O.BusType,O.IRScat , D.DonationType , D.ChapterID, C.WebFolderName as ChapterCode,D.Amount,D.Purpose,D.TRANSACTION_NUMBER,E.Name as EventName,D.MemberID"
                StrSQl = StrSQl & " from DonationsInfo  D Inner Join Chapter C ON C.chapterid = D.ChapterID Left Join Event E ON E.EventID=D.EventID"
                If CType(e.Item.FindControl("lblDonorType"), Label).Text.Trim() = "OWN" Then
                    'OWN
                    StrSQl = StrSQl & " Inner Join NSFAccounts Acc ON D.DonorType = Acc.DonorType AND D.DonationType=Acc.RestrictionType AND Acc.AccNo=" & CType(e.Item.FindControl("lblAccount"), Label).Text.Trim()
                Else
                    'IN
                    StrSQl = StrSQl & " Inner Join NSFAccounts Acc ON Acc.DonorType='IND/SPOUSE' AND D.DonationType=Acc.RestrictionType AND Acc.AccNo=" & CType(e.Item.FindControl("lblAccount"), Label).Text.Trim()
                End If
                StrSQl = StrSQl & " Left Join OrganizationInfo O ON D.DonorType = 'OWN' and O.AutoMemberid =D.Memberid " '"AND  " & IIf(CType(e.Item.FindControl("lblDescription"), Label).Text.Trim().Contains("Non-Profit"), "O.IRScat IN ('Non-proft,501(c)(3)','Non-proft,PAC', 'Non-proft,Other')", "(O.IRScat IN ('profit') OR O.IRScat is NULL)")
                StrSQl = StrSQl & " Left Join IndSpouse I ON D.DonorType <> 'OWN' AND D.MEMBERID = I.AutoMemberID "
                StrSQl = StrSQl & " where (I.AutoMemberID IS NOT NULL or O.AutoMemberID IS NOT NULL ) and    D.DepositSlip = " & Request.QueryString("DSlip") & " AND D.DepositDate='" & Request.QueryString("DDate") & "' AND C.webfoldername='" & CType(e.Item.FindControl("lblClass"), Label).Text.Trim() & "' AND D.DonorType IN (" & IIf(CType(e.Item.FindControl("lblDonorType"), Label).Text.Trim() = "OWN", "'OWN'", "'IND','SPOUSE'") & ") AND D.DonationType='" & CType(e.Item.FindControl("lblDonationType"), Label).Text.Trim() & "'"
                If CType(e.Item.FindControl("lblDonorType"), Label).Text.Trim() = "OWN" Then StrSQl = StrSQl & " AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)','Non-proft,PAC', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END "
                StrSQl = StrSQl & " order by  D.DonorType,  D.DonationType,  O.BusType,  D.ChapterID, D.DepositDate Desc"
            End If
            Session("StrTrans") = StrSQl
            'Response.Write(StrSQl)
            LoadErrorData()
        End If
    End Sub

    Public Sub LoadErrorData()
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Try
            dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrTrans"))
            If dsTransDonation.Tables(0).Rows.Count > 0 Then
                'trEdit.Visible = True
                btnClose.Visible = True
                TrDetailView.Visible = True
                lblErr.Text = ""
                grdEditVoucher.DataSource = dsTransDonation.Tables(0)
                grdEditVoucher.DataBind()

            Else
                grdEditVoucher.DataSource = Nothing
                grdEditVoucher.DataBind()
                lblErr.Text = "Sorry No detailed view to show"
                ' Tredit.Visible = False
                TrDetailView.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(strSql)
        End Try
    End Sub

    Protected Sub grdEditVoucher_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdEditVoucher.CancelCommand
        grdEditVoucher.EditItemIndex = -1
        LoadErrorData()
    End Sub

    Protected Sub grdEditVoucher_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)
        DonationID = CInt(e.Item.Cells(1).Text)
        DonorType = CType(e.Item.Cells(5).FindControl("lblDonorType"), Label).Text
        BusType = CType(e.Item.Cells(6).FindControl("lblBusType"), Label).Text
        IRSCat = CType(e.Item.Cells(7).FindControl("lblIRSCat"), Label).Text
        DonationType = CType(e.Item.Cells(8).FindControl("lblDonationType"), Label).Text
        ChapterCode = CType(e.Item.Cells(10).FindControl("lblChapterCode"), Label).Text
        Amount = CType(e.Item.Cells(11).FindControl("lblAmount"), Label).Text
        purpose = CType(e.Item.Cells(12).FindControl("lblpurpose"), Label).Text
        EventName = CType(e.Item.Cells(9).FindControl("lblEvent"), Label).Text
        'checkNo = CType(e.Item.Cells(14).FindControl("lblcheckNumber"), Label).Text
        'depositSlip = CType(e.Item.Cells(3).FindControl("lbldepositSlip"), Label).Text
        'depDate = CType(e.Item.Cells(4).FindControl("lblDepositdate"), Label).Text
        'donDate = CType(e.Item.Cells(13).FindControl("lblDonationdate"), Label).Text
        Session("DonationID") = DonationID
        Session("DonorType") = DonorType
        Session("BusType") = BusType
        Session("IRSCat") = IRSCat
        Session("DonationType") = DonationType
        Session("ChapterCode") = ChapterCode

        Session("Purpose") = purpose
        Session("EventName") = EventName
        LoadErrorData()

    End Sub

    Protected Sub grdEditVoucher_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim row As Integer = CInt(e.Item.ItemIndex)
        Dim ddlTemp As DropDownList

        Dim chapterId As Integer
        Dim EventId As Integer

        ddlTemp = e.Item.FindControl("ddlDonorType")
        DonorType = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlBusType")
        BusType = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlIRSCat")
        IRSCat = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlDonationType")
        DonationType = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlChapterCode")
        ChapterCode = ddlTemp.SelectedItem.Text
        chapterId = ddlTemp.SelectedValue

        ddlTemp = e.Item.FindControl("ddlEvent")
        EventName = ddlTemp.SelectedItem.Text
        EventId = ddlTemp.SelectedValue

        ddlTemp = e.Item.FindControl("ddlpurpose")
        purpose = ddlTemp.SelectedItem.Text

        Amount = CType(e.Item.FindControl("txtAmount"), TextBox).Text
        'checkNo = CType(e.Item.FindControl("txtcheckNumber"), TextBox).Text
        depositSlip = CType(e.Item.FindControl("txtDepositSlip"), TextBox).Text
        depDate = CType(e.Item.FindControl("txtDepositdate"), TextBox).Text
        'donDate = CType(e.Item.FindControl("txtDonationdate"), TextBox).Text
        If DonorType.Trim = "OWN" Then
            'Change in DonationsInfo & Organization Table (BusType,IRSCat)
            If Session("BusType") <> BusType.Trim Or Session("IRSCat") <> IRSCat.Trim Then
                'Organization Table (BusType,IRSCat)
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update OrganizationInfo set BusType='" & BusType & "',IRSCat='" & IRSCat & "' where AutoMemberID= " & CType(e.Item.FindControl("lblMemberID"), Label).Text)
            End If
        End If
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update DonationsInfo SET  DonorType='" & DonorType & "', AMOUNT=" & Amount & ",ChapterId=" & chapterId & ", DonationType='" & DonationType & "',EVENT='" & EventName & "',EventId=" & EventId & ",PURPOSE='" & purpose & "',DepositDate='" & depDate & "',DepositSlip=" & depositSlip & ",ModifyDate=GetDate(),ModifiedBy=" & Session("LoginID") & " where DonationID = " & Session("DonationID")) '"',TRANSACTION_NUMBER='" & checkNo & "',DonationDate='" & donDate &
        lblErr.Text = "Updated Successfully"
        '**If validation passess
        grdEditVoucher.EditItemIndex = -1
        '**If Validation fails
        '**grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)

        LoadErrorData()
        GetDonationReceipt(Request.QueryString("DSlip"), Request.QueryString("DDate"))
    End Sub

    Public Sub SetDropDown_BusType(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("BusType")))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetDropDown_DonorType(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("DonorType")))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetDropDown_Chapter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ChapterID,WebFolderName from Chapter order by State,Chaptercode")
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("ChapterCode")))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetDropDown_Purpose(ByVal sender As Object, ByVal e As System.EventArgs)
        strSql = "SELECT PurposeCode ,PurposeDesc FROM DonationPurpose   "
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("Purpose")))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetDropDown_Event(ByVal sender As Object, ByVal e As System.EventArgs)
        strSql = "Select EventID,EventCode, Name from event"
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("EventName")))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetDropDown_donationtype(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("DonationType")))
        Catch ex As Exception

        End Try
    End Sub

    Public Sub SetDropDown_IRSCat(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("IRSCat")))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TrDetailView.Visible = False
        btnClose.Visible = False
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvCreditCardchapter As Control)
    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & lblDepositNo.Text.Replace("Deposit No:", "").Replace("Voucher No:", "").Replace("Cash Receipt No:", "").Trim & ".xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        dgVoucher.RenderControl(htmlWrite)
        Response.Write("<table><tr><td align='center' colspan='4'><font face='Arial' size='4'>North South Foundation<br />" & lblHeading.Text & "</font></td></tr><tr><td align='right' colspan='4'>" & lblDepositNo.Text & "</td></tr><tr><td align='center' colspan='4'>" & lblBankAccount.Text & "</td></tr><tr><td align='right' colspan='4'>")
        Response.Write(stringWrite.ToString())
        Response.Write("</td></tr>") '<tr><td align='right' colspan='4'>Total : " & lblTotal.Text & "</td></tr>")
        Response.Write("</table>")
        Response.End()
    End Sub
    Function GetAvgPrice(ByVal BankID As Integer, ByVal CurrTicker As String, ByVal Quantity As Double, ByVal TransDate As String, ByVal sFY_BegDate As String, ByVal sFY_EndDate As String) As Double
        Dim sSql As String
      
        If BankID = 0 Then
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=(select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B  WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID in (select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
        Else
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=" & BankID & " and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B  WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID =" & BankID & " AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "

        End If
        Dim rsBankTran As SqlDataReader = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Shares", Type.GetType("System.Decimal"))
        dt.Columns.Add("Price", Type.GetType("System.Decimal"))
        dt.Columns.Add("Amount", Type.GetType("System.Decimal"))
        dt.Columns.Add("OutstandingShares", Type.GetType("System.Decimal"))
        dt.Columns.Add("CostBasis", Type.GetType("System.Decimal"))
        dt.Columns.Add("AvgPrice", Type.GetType("System.Decimal"))
        Dim currentIndex As Integer = -1
        While rsBankTran.Read
            If rsBankTran("TransCat").ToString().ToLower.Trim = "begbal" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount")
                dr("OutstandingShares") = rsBankTran("Quantity")
                dr("CostBasis") = rsBankTran("NetAmount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf currentIndex = -1 Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = 0.0
                dr("Price") = 0.0
                dr("Amount") = 0.0
                dr("OutstandingShares") = 0.0
                dr("CostBasis") = 0.0
                dr("AvgPrice") = 0.0
                dt.Rows.Add(dr)
            End If
            If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("Quantity"), rsBankTran("Quantity") * -1)
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)

            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity") * -1
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            End If
        End While
        If currentIndex < 0 Then
            Return 0.0
        Else
            Return Math.Round(dt.Rows(currentIndex)("AvgPrice"), 2)
        End If
    End Function
End Class
