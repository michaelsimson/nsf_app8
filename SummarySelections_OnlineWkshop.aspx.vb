Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Partial Class SummarySelections_OnlineWkshop
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("LoginEmail") = "sreechitra@gmail.com"
        'Session("CustIndID") = 63219
        'Session("EventID") = 20

        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
        End If

        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"
        hdnMemberId.Value = Session("CustIndID")
        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") &
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                Session("ChapterID") = dsIndSpouse.Tables(0).Rows(0).Item("ChapterID")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                lblAddress2.Text = drIndSpouse.Item("Address2")
                lblStateZip.Text = drIndSpouse.Item("City") & ", " & drIndSpouse.Item("state") & " " & drIndSpouse.Item("zip")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")

            End If
        End If




        'Dim objChild As New Child
        Dim dsChild As New DataSet

        'objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & intIndID & "'")

        'If dsChild.Tables.Count > 0 Then
        '    dgChildList.DataSource = dsChild.Tables(0)
        '    dgChildList.DataBind()
        '    Session("ChildCount") = dsChild.Tables(0).Rows.Count
        '    ViewState("ChildInfo") = dsChild
        'End If

        Dim dsInvitees As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        If conn.State = ConnectionState.Closed Then conn.Open()
        hdnMemberId.Value = Session("CustIndID")
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_GetPendingTran_Registration_OnlineWkShop"
        cmd.Connection = conn
        'cmd.Parameters.Add(New SqlParameter("@ChildNumber", e.Item.DataItem("ChildNumber")))
        'cmd.Parameters.Add(New SqlParameter("@ChapterID", 48))
        'cmd.Parameters.Add(New SqlParameter("@EventYear", Application("ContestYear")))
        cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
        cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
        da.SelectCommand = cmd
        da.Fill(dsInvitees)
        dgChildList.DataSource = dsInvitees.Tables(0)
        dgChildList.DataBind()
        lblCommuEmail.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSpouse where AutoMemberID=" & Session("CustIndID") & "")
        CalculateTotal()
    End Sub

    Private Sub CalculateTotal()

        Dim dgItem As DataGridItem
        Dim strPaymentNotes As String

        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim Lbl3 As Label
        Dim Lbl4 As Label
        strPaymentNotes = ""
        Dim totalFee, RegFee, LateFee As Double
        totalFee = 0.0
        RegFee = 0.0
        LateFee = 0.0

        For Each dgItem In dgChildList.Items
            Lbl1 = dgItem.FindControl("lblFee")
            totalFee = totalFee + Double.Parse(Lbl1.Text)
            RegFee = RegFee + Double.Parse(Lbl1.Text)

            Lbl2 = dgItem.FindControl("lblProductId")
            strPaymentNotes = strPaymentNotes + Lbl2.Text
            Lbl3 = dgItem.FindControl("lblChildId")
            strPaymentNotes = strPaymentNotes + "(" + Lbl3.Text + ")"

            Lbl4 = dgItem.FindControl("lblLateFee")
            totalFee = totalFee + Double.Parse(Lbl4.Text)
            LateFee = LateFee + Double.Parse(Lbl4.Text)

            'strPaymentNotes = strPaymentNotes + "(" + Lbl4.Text + ")"
            strPaymentNotes = strPaymentNotes + "(" + Lbl1.Text + ")"
        Next
        lblTotalAmt.Text = totalFee.ToString()
        Session("RegFee") = RegFee
        Session("LATEFEE") = LateFee

        Session("ContestsSelected") = strPaymentNotes
    End Sub
    Protected Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildList.ItemDataBound

        'Select Case e.Item.ItemType
        '    Case ListItemType.Item, ListItemType.AlternatingItem
        '        'Dim dsContest As DataSet

        '        Dim conn As New SqlConnection(Application("ConnectionString"))
        '        Dim cmd As New SqlCommand
        '        Dim dsInvitees As New DataSet
        '        Dim da As New SqlDataAdapter

        '        cmd.Connection = conn
        '        conn.Open()
        '        cmd.CommandType = CommandType.StoredProcedure
        '        cmd.CommandText = "usp_GetPendingTran_Registration "
        '        'cmd.Parameters.Add(New SqlParameter("@ChildNumber", e.Item.DataItem("ChildNumber")))
        '        'cmd.Parameters.Add(New SqlParameter("@ChapterID", 48))
        '        'cmd.Parameters.Add(New SqlParameter("@EventYear", Application("ContestYear")))
        '        cmd.Parameters.Add(New SqlParameter("@EventID", Session("EventID")))
        '        cmd.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))

        '        da.SelectCommand = cmd
        '        da.Fill(dsInvitees)

        '        If dsInvitees.Tables(0).Rows.Count <= 0 Then
        '            lblContestInfo.Text = "There are no contests yet offered in your center.  Please consult your chapter coordinator." & _
        '                                  "If you proceed and register at another center, it is your responsibility to follow through and participate " & _
        '                                  " at that center on the date(s) at the venue available at the time.  " & _
        '                                  "No refund will be given under any circumstances."
        '            lblContestInfo.Visible = True
        '        Else
        '            lblContestInfo.Visible = False
        '        End If

        '        Dim sbContest As New StringBuilder, strContestDesc As String
        '        strContestDesc = ""
        '        sbContest.Append("<ul>")
        '        For Each dr As DataRow In dsInvitees.Tables(0).Rows
        '            If strContestDesc <> dr.Item("Description") Then
        '                sbContest.Append("<li>" & dr.Item("Description") & "</li>")
        '                strContestDesc = dr.Item("Description")
        '            End If
        '            If InStr(dr.Item("EventDate"), "Announced") > 0 Then
        '                lblContestDateInfo.Text = "Date(s)/Venue is not yet available for your center.  Please consult your chapter coordinator." & _
        '                                     "If you proceed and register at another center, it is your responsibility to follow through and participate " & _
        '                                     " at the selected center on the date(s) at the venue available at the time.  " & _
        '                                     "No refund will be given under any circumstances."
        '                lblContestDateInfo.Visible = True
        '            Else
        '                lblContestDateInfo.Visible = False
        '            End If
        '        Next
        '        sbContest.Append("</ul>")

        '        CType(e.Item.FindControl("lblEligibleContests"), Label).Text = sbContest.ToString

        '        Dim sbCity As New StringBuilder, strCity As String
        '        strCity = ""
        '        sbCity.Append("<ul>")
        '        For Each dr As DataRow In dsInvitees.Tables(0).Rows
        '            If InStr(strCity, dr.Item("Venue")) <= 0 Then
        '                sbCity.Append("<li>" & dr.Item("Venue") & "</li>")
        '                strCity = strCity & ", " & dr.Item("Venue")
        '            End If
        '        Next
        '        sbCity.Append("</ul>")

        '        CType(e.Item.FindControl("lblLocation"), Label).Text = sbCity.ToString

        'End Select
    End Sub


    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        lblContestInfo.Text = ""
        lblEmailInfo.Text = ""
        If ChkEmail.Checked = False Then
            lblContestInfo.Text = "Please check that you will use this email for all communications with NSF."
            Exit Sub
        ElseIf ChkDetails.Checked = False Then
            'lblContestInfo.Text = "Please check that Online Workshop Date and timing cannot be changed later and that seat is not guaranteed until paid."
            lblContestInfo.Text = "Please check the second box on change of subject, date or child."
            Exit Sub
        ElseIf ChkCommEmail.Checked = False Then
            'lblContestInfo.Text = "Please check the mailing address above is correct to receive any workshop books."
            lblContestInfo.Text = "Please check the last box on Workshop books."
            Exit Sub
        Else
            lblContestInfo.Text = ""
            Page.Response.Redirect("TermsAndConditions.aspx")
        End If
    End Sub
    Protected Sub hlinkEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkEmail.Click
        lblEmailInfo.Text = "Please Refresh the Page after Email Change and Proceed"
        Response.Redirect("ChangeEmail.aspx?ID=2")
        'Response.Write("<script language='javascript'>window.open('ChangeEmail.aspx','_blank','left=150,top=0,width=800,height=500,toolbar=0,location=0,scrollbars=1');</script> ")
    End Sub
End Class
