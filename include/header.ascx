<%@ Control Language="vb" AutoEventWireup="false" Inherits="VRegistration.header" CodeFile="header.ascx.vb" %>
<HTML>
	<HEAD>
		<title>North South Foundation: Helping those in need to help themselves </title>
		<LINK href="cssMain.css" type="text/css" rel="stylesheet">
		<script>
			function resizeDoc(obj, frame)			
			{
				var docHeight = frame.document.body.scrollHeight;
				obj.style.height = docHeight + 10 + 'px';
			}
		</script>
	</HEAD>
	<body TOPMARGIN="0" leftmargin="0" rightmargin="0">
		<TABLE cellSpacing="2" cellPadding="0" width="770" border="0">
			<tr>
				<td>
					<TABLE height="57" cellSpacing="0" cellPadding="0" align="left" bgColor="white" border="0">
						<TR>
							<TD align="left"><IMG height="57" src="../Images/spacer_tile.gif" width="83"><img src="../Images/banner_left.gif" valign="top" width="73" height="57"><img src="../Images/banner_left2.gif" width="98" height="57"><img src="../Images/banner_center.gif" width="100" height="57"><img src="../Images/banner_right.gif" width="99" height="57"><img src="../Images/banner_right2.gif" width="87" height="57"><img src="../Images/banner_right-corner.gif" width="65" height="57"><FONT face="Verdana"><IMG style="WIDTH: 81px; HEIGHT: 57px" height="57" src="../Images/spacer_tile.gif" width="81"></FONT></TD>
							<TD align="left"><a href="http://www.northsouth.org/"><font color="#000000"><img src="../Images/home.gif" border="0" width="36" height="57"></font></a><a href="http://www.northsouth.org/new_site/home.asp"></a></TD>
						</TR>
						<tr>
							<td align="center">
								<table border="0" width="100%">
									<tr>
										<td align="center" valign="middle"><IMG height="37" src="../Images/beepicture.jpg"></td>
										<td align="center" class="largewordingbold" style="FONT-SIZE: 16pt" valign="middle">Bee 
											Corner
										</td>
										<td class="largewordingbold" style="FONT-SIZE: 8pt" valign="bottom">Register for 
											Contests, Download Practice Words, Check Contest Scores and more...
										</td>
									</tr>
									<tr>
										<td>
										</td>
										<td colspan="2" class="mediumwording" style="FONT-SIZE: 8pt" valign="bottom"><br>
											Please make sure that you are using the Microsoft Internet Explorer. We did not 
											test with other browsers.</td>
									</tr>
								</table>
					</TABLE>
				</td>
			</tr>
			<tr>
				<td>
