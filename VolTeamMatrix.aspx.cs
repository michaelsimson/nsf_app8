﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;

public partial class VolTeamMatrix : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if(!IsPostBack)
        {
        GridDisplay();
            }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (TxtTeam.Text != string.Empty)
        {
            int Count = 0;
            string countQuery = "select COUNT(*) from  VolTeamMatrix where TeamName='" + TxtTeam.Text.Trim() + "'";
           
            Count = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, countQuery).ToString());
            if (Count ==0)
            {
                lbdup.Visible = false;
                lbdup.Text = "";
                string StrQryInsert = "insert into VolTeamMatrix (TeamName) values ('" + TxtTeam.Text.Trim() + "')";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryInsert);
                lbdup.Visible = true;
                lbdup.Text = "Team Added Successfully ";
                GridDisplay();

               
            }
            else
            {
                lbdup.Visible = true;
                lbdup.Text = "Team Name already exixts";
            }
        }
        else
        {
            lbdup.Visible = true;
            lbdup.Text = "Please Enter Team Name";
        }
    }
    protected void update(object sender, EventArgs e)
    {
    }
    protected void GridDisplay()
    {
        string StrQrySearch = "select TeamId,TeamName,Finals ,ChapterContests , Workshop ,Coaching ,Prepclub , OnlineWorkshop , [National] from VolTeamMatrix";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
            lbErr.Visible = true;
            lbErr.Text = "No Records found ";
        }
        else
        {
            lbErr.Visible = false;
            GVTeamMatrix.DataSource = ds;
            GVTeamMatrix.DataBind();
        }
          
    }
    protected void btnUp_Click(object sender, EventArgs e)
    {
        string StrUpdate = string.Empty;
        foreach (DataGridItem row in GVTeamMatrix.Items)
        {
            string stTeamId = row.Cells[0].Text;
           StrUpdate = StrUpdate + " Update VolTeamMatrix Set Finals="
               + (((DropDownList)row.FindControl("ddlAdd1")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd1")).SelectedItem.Value + "'") + ",ChapterContests="
               + (((DropDownList)row.FindControl("ddlAdd2")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd2")).SelectedItem.Value + "'") + ",Workshop="
                + (((DropDownList)row.FindControl("ddlAdd3")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd3")).SelectedItem.Value + "'") + ",Coaching="
               + (((DropDownList)row.FindControl("ddlAdd4")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd4")).SelectedItem.Value + "'") + ",Prepclub="
               + (((DropDownList)row.FindControl("ddlAdd5")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd5")).SelectedItem.Value + "'") + ",OnlineWorkshop="
                + (((DropDownList)row.FindControl("ddlAdd6")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd6")).SelectedItem.Value + "'") + ",[National]="
                + (((DropDownList)row.FindControl("ddlAdd7")).SelectedItem.Text.Trim() == "Sel" ? "NULL" : "'" + ((DropDownList)row.FindControl("ddlAdd7")).SelectedItem.Value + "'") + " where TeamId=" + stTeamId + "";
        }
        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrUpdate);
        lbdup.Visible = true;
        lbdup.Text = "Team Updated Successfully ";
    }
    protected void BTnClick_Click(object sender, EventArgs e)
    {
        EventTeam();
    }
    protected void EventTeam()
    {
        string StrQrySearch = "select teameventid,teameventname,case when Activestate=1 then 'Active' else 'Inactive' end from   teamEvent ";
        DataSet ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearch);
        if (null != ds && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count <= 0)
        {
            lbErr.Visible = true;
            lbErr.Text = "No Records found ";
        }
        else
        {
            tblButton.Visible = true;
            tblAddDel.Visible = true;
            lblist.Visible = true;
            GvTeam.Visible = true;
            lbErr.Visible = false;
            GvTeam.DataSource = ds;
            GvTeam.DataBind();
        }

    }
    protected void btnAddcol_Click(object sender, EventArgs e)
    {
        if ((TxtTeamName.Text != string.Empty) && ((TxtId.Text != string.Empty)))
        {
            int Count = 0;
            string countQuery = "select COUNT(*) from  teamEvent where teameventname='" + TxtTeamName.Text.Trim() + "'  and  teamEventId=" + TxtId.Text.Trim() + "";

            Count = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, countQuery).ToString());
            if (Count == 0)
            {
                lbdup.Visible = false;
                lbdup.Text = "";
                string StrQryInsert = "insert into teamEvent (teameventname,teamEventId,Activestate) values ('" + TxtTeamName.Text.Trim() + "','" + TxtId.Text.Trim() + "',1)";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryInsert);
                lbdup.Visible = true;
                lbdup.Text = "Team Added Successfully ";
                EventTeam();
                string StrQryInsertcol = "ALTER TABLE VolTeamMatrix ADD  " + TxtTeamName.Text.Trim() + "  nvarchar(50)";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryInsertcol);
            }
            else
            {
                lbdup.Visible = true;
                lbdup.Text = "Team Name already exixts";
            }
        }
        else
        {
            lbdup.Visible = true;
            lbdup.Text = "Please Enter Team Name";
        }
    }
    public class DynamicTemplateField : ITemplate
    {

        public void InstantiateIn(Control container)
        {
            //define the control to be added , i take text box as your need
            TextBox txt1 = new TextBox();
            txt1.ID = "txt1";
            container.Controls.Add(txt1);
        }
    }

    //Method to bind the Grid View
    public void BindData()
{
    TemplateField temp1  = new TemplateField();  //Create instance of Template field
    temp1.HeaderText = "New Dynamic Temp Field"; //Give the header text

    temp1.ItemTemplate = new DynamicTemplateField(); //Set the properties **ItemTemplate** as the instance of DynamicTemplateField class.


    gv.Columns.Add(temp1); //add the instance if template field in columns of grid view

    //Bind the grid  view
    gv.DataSource = [your data source];
    gv.DataBind();

 }
}