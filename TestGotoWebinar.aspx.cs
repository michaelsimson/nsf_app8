﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
//using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public partial class TestGotoWebinar : System.Web.UI.Page
{
    public static string AccessToken = "ZR8zO6GhdSoGiKIXXgd5Ic3z4xQA";
    public static string Organizerkey = "5516732880916201477";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Server.Transfer("maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {

                hdnLoginId.Value = Session["LoginID"].ToString();
                hdnAccessToken.Value = AccessToken;
                hdnOrganizerKey.Value = Organizerkey;
            }
        }
    }
    public class Webinar
    {
        public string WebinarID { get; set; }
        public string AutoMemberId { get; set; }
        public string EventId { get; set; }
        public string EventYear { get; set; }
        public string ProductGroupId { get; set; }
        public string ProductId { get; set; }
        public string ProductGroupCode { get; set; }
        public string ProductCode { get; set; }
        public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public string TimeZone { get; set; }
        public string SessionType { get; set; }
        public string WebinarKey { get; set; }
        public string HostURL { get; set; }
        public string IsPwdProtected { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifyDate { get; set; }
        public string ModifiedBy { get; set; }
        public string WebinarLink { get; set; }
        public string JoinURL { get; set; }
        public string RegistrantKey { get; set; }
        public string OnlineWsCalID { get; set; }
        public string RegId { get; set; }
        public double Duration { get; set; }
        public string UTCStartTime { get; set; }
        public string UTCEndTime { get; set; }
        public string Mode { get; set; }
        public string IsValidation { get; set; }
        public string ProductGroupName { get; set; }
        public string EventCode { get; set; }
        public string OnlineWsCalId { get; set; }
        public string Source { get; set; }

    }

    public class Attendees
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string ProductName { get; set; }
        public string ParentName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Source { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string OnlineWsCalID { get; set; }
        public string RegId { get; set; }
        public string Name { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string Chapter { get; set; }
        public string DonorType { get; set; }
        public string Login { get; set; }
        public string AutoMemberId { get; set; }
        public string Webinarkey { get; set; }
        public string RegistrantKey { get; set; }
        public string JoinURL { get; set; }
        public string LoginId { get; set; }
        public string SpouseEmail { get; set; }
        public string SecondaryEmail { get; set; }
    }

    public class Event
    {
        public string EventId { get; set; }
        public string EventName { get; set; }
        public string EventCode { get; set; }
    }

    [WebMethod]
    public static int PostNewWebinar(Webinar ObjWebinar)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            if (ObjWebinar.Mode == "1")
            {
                ObjWebinar.ProductGroupId = (ObjWebinar.ProductGroupId == "0" ? "null" : ObjWebinar.ProductGroupId);
                ObjWebinar.ProductId = (ObjWebinar.ProductId == "0" ? "null" : ObjWebinar.ProductId);
                ObjWebinar.EventId = (ObjWebinar.EventId == "0" ? "null" : ObjWebinar.EventId);

                cmdText = "insert into webinarsessions( [EventYear], EventId ,[ProductGroupId],[ProductId],[TeacherId],[Title] ,[StartDate] ,[StartTime] ,[TimeZone] ,[SessionType],[WebinarKey], Duration )values(" + ObjWebinar.EventYear + "," + ObjWebinar.EventId + "," + ObjWebinar.ProductGroupId + "," + ObjWebinar.ProductId + "," + ObjWebinar.TeacherId + ",'" + ObjWebinar.Title + "','" + ObjWebinar.StartDate + "','" + ObjWebinar.StartTime + "','America/newyork','" + ObjWebinar.SessionType + "','" + ObjWebinar.WebinarKey + "', " + ObjWebinar.Duration + "); ";

                SqlCommand cmd = new SqlCommand(cmdText, cn);

                cmd.ExecuteNonQuery();

                string Date = Convert.ToDateTime(ObjWebinar.StartDate).ToString("yyyy-dd-MM");

                cmdText = "select top 1 OnlineWsCalId from OnlineWsCal where EventYear=" + ObjWebinar.EventYear + " and TeacherId=" + ObjWebinar.TeacherId + " and ProductId=" + ObjWebinar.ProductId + " order by CreatedDate desc";
                string OnlineWsCalId = string.Empty;
                try
                {
                    OnlineWsCalId = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, cmdText).ToString();
                }
                catch
                {

                }
                if (ObjWebinar.EventId == "20")
                {
                    // cmdText = " update onlineWsCal set WebinarKey='" + ObjWebinar.WebinarKey + "' where EventYear=" + ObjWebinar.EventYear + " and ProductGroupid=" + ObjWebinar.ProductGroupId + " and Productid=" + ObjWebinar.ProductId + " and TeacherId=" + ObjWebinar.TeacherId + " and Date ='" + ObjWebinar.StartDate + "'";

                    cmdText = " update onlineWsCal set WebinarKey='" + ObjWebinar.WebinarKey + "' where OnlineWsCalId=" + OnlineWsCalId + "";

                    cmd = new SqlCommand(cmdText, cn);

                    cmd.ExecuteNonQuery();
                }
            }
            else if (ObjWebinar.Mode == "2")
            {
                ObjWebinar.ProductGroupId = (ObjWebinar.ProductGroupId == "0" ? "null" : ObjWebinar.ProductGroupId);
                ObjWebinar.ProductId = (ObjWebinar.ProductId == "0" ? "null" : ObjWebinar.ProductId);
                ObjWebinar.EventId = (ObjWebinar.EventId == "0" ? "null" : ObjWebinar.EventId);
                cmdText += " update WebinarSessions set StartDate='" + ObjWebinar.StartDate + "', StartTime='" + ObjWebinar.StartTime + "', Duration=" + ObjWebinar.Duration + ", TeacherId=" + ObjWebinar.TeacherId + ", ProductGroupId=" + ObjWebinar.ProductGroupId + ", Productid=" + ObjWebinar.ProductId + ", Eventid=" + ObjWebinar.EventId + ", title='" + ObjWebinar.Title + "'  where Webinarkey=" + ObjWebinar.WebinarKey + " ";
            }


            retVal = 1;

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
            retVal = -1;
        }
        return retVal;
    }



    [WebMethod]
    public static List<Webinar> ListWebinarSessions(Webinar objWeniar)
    {
        int retVal = -1;
        List<Webinar> ListWebinar = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "  select Ws.teacherid, ws.Eventyear, Ip.Firstname+' '+IP.Lastname as teacherName, Ws.Title, PG.Productgroupid, PG.ProductgroupCode, P.ProductId, P.ProductCode, ws.StartDate, ws.StartTime, ws.SessionType, ws.WebinarId, ws.WebinarKey, e.name as EventName, ws.Duration from Webinarsessions WS inner join Indspouse IP on (ws.TeacherId=IP.AutomemberId) left join Productgroup PG on (ws.ProductgroupId=PG.productGroupid) left join product P on (ws.productId=P.Productid) left join event E on (E.EventId=ws.EventId) where ws.EventYear=" + objWeniar.EventYear + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar webinar = new Webinar();
                        webinar.EventYear = dr["Eventyear"].ToString();
                        webinar.WebinarID = dr["WebinarID"].ToString();
                        webinar.ProductGroupId = dr["ProductGroupId"].ToString();
                        webinar.ProductId = dr["ProductId"].ToString();
                        webinar.ProductGroupCode = dr["ProductGroupCode"].ToString();
                        webinar.ProductCode = dr["ProductCode"].ToString();
                        webinar.TeacherId = dr["TeacherId"].ToString();
                        webinar.TeacherName = dr["TeacherName"].ToString();
                        webinar.Title = dr["Title"].ToString();

                        webinar.StartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        // webinar.EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        webinar.StartTime = Convert.ToDateTime(dr["StartTime"].ToString()).ToString("HH:mm:ss");
                        //  webinar.EndTime = Convert.ToDateTime(dr["EndTime"].ToString()).ToString("HH:mm:tt");
                        webinar.SessionType = dr["SessionType"].ToString();
                        webinar.WebinarKey = dr["WebinarKey"].ToString();
                        webinar.EventId = dr["EventName"].ToString();
                        webinar.Duration = Convert.ToDouble(dr["Duration"].ToString());

                        ListWebinar.Add(webinar);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListWebinar;

    }

    [WebMethod]
    public static List<Attendees> ListAttendees(Webinar objWeniar)
    {
        int retVal = -1;
        List<Attendees> ListAttendees = new List<Attendees>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            if (objWeniar.Source == "OW")
            {
                cmdText = "  select C.First_name, C.Last_Name, case when C.Email is null then IP.Email when C.Email ='' then IP.Email else C.Email end as Email,IP.State, IP.City, IP.Country, IP.Zip, Ip.Address1, case when IP.CPhone is null then IP.HPhone end as Phone, RO.OnlineWsCalId, RO.RegId, OWS.WebinarKey, RO.JoinURL, RO.RegistrantKey, IP.DonorType, IP1.Email as SpouseEmail, IP.SecondaryEmail, Ip.FirstName +' '+ IP.LastName as ParentName, P.Name as ProductName, OWS.Date, OWS.Time  from Registration_OnlineWkShop RO inner join Child C on (Ro.ChildNumber=C.ChildNumber) inner join Indspouse IP on (IP.AutoMemberId=C.MemberId)  left join Indspouse IP1 on (IP1.RelationShip=IP.AutomemberId) inner join OnlineWsCal OWS on (OWS.ProductGroupId=RO.ProductGroupId and OWS.ProductId=RO.ProductId and OWS.OnlineWsCalId=RO.OnlineWsCalId) inner join Product P on (OWS.ProductId=P.ProductId) where RO.Eventyear=" + objWeniar.EventYear + " and RO.Approved='Y' and RO.MemberId=" + objWeniar.AutoMemberId + " and RO.OnlineWsCalId=" + objWeniar.OnlineWsCalID + "";

            }
            else
            {
                string text = "select distinct OnlineWsCalId from onlinewscal OW inner join Webinarsessions WS on (OW.Date=WS.StartDAte) where WS.Webinarkey='" + objWeniar.WebinarKey + "'";
                string OnlineWsCalId = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, text).ToString();

                cmdText = "  select C.First_name, C.Last_Name, case when C.Email is null then IP.Email when C.Email ='' then IP.Email else C.Email end as Email,IP.State, IP.City, IP.Country, IP.Zip, Ip.Address1, case when IP.CPhone is null then IP.HPhone end as Phone, RO.OnlineWsCalId, RO.RegId, OWS.WebinarKey, RO.JoinURL, RO.RegistrantKey, IP.DonorType, IP1.Email as SpouseEmail, IP.SecondaryEmail, Ip.FirstName +' '+ IP.LastName as ParentName, P.Name as ProductName, OWS.Date, OWS.Time  from Registration_OnlineWkShop RO inner join Child C on (Ro.ChildNumber=C.ChildNumber) inner join Indspouse IP on (IP.AutoMemberId=C.MemberId)  left join Indspouse IP1 on (IP1.RelationShip=IP.AutomemberId) inner join OnlineWsCal OWS on (OWS.ProductGroupId=RO.ProductGroupId and OWS.ProductId=RO.ProductId and OWS.OnlineWsCalId=RO.OnlineWsCalId) inner join Product P on (OWS.ProductId=P.ProductId) where RO.Eventyear=" + objWeniar.EventYear + " and RO.Approved='Y' and OWS.TeacherId=" + objWeniar.TeacherId + " and OWS.OnlineWsCalid=" + OnlineWsCalId + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Attendees attendees = new Attendees();
                        attendees.FirstName = dr["First_Name"].ToString();
                        attendees.LastName = dr["Last_Name"].ToString();
                        attendees.Email = dr["Email"].ToString();
                        attendees.Address = dr["Address1"].ToString();
                        attendees.Source = "NSF";
                        attendees.City = dr["City"].ToString();
                        attendees.State = dr["State"].ToString();
                        attendees.Country = dr["Country"].ToString();
                        attendees.Zip = dr["Zip"].ToString();
                        attendees.Phone = dr["Phone"].ToString();
                        attendees.OnlineWsCalID = dr["OnlineWsCalID"].ToString();
                        attendees.RegId = dr["RegId"].ToString();
                        attendees.Webinarkey = dr["WebinarKey"].ToString();
                        attendees.JoinURL = dr["JoinURL"].ToString();
                        attendees.RegistrantKey = dr["RegistrantKey"].ToString();
                        attendees.SpouseEmail = dr["SpouseEmail"].ToString();
                        attendees.SecondaryEmail = dr["SecondaryEmail"].ToString();
                        attendees.ParentName = dr["ParentName"].ToString();
                        attendees.ProductName = dr["ProductName"].ToString();
                        attendees.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        attendees.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss");

                        ListAttendees.Add(attendees);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListAttendees;

    }

    [WebMethod]
    public static List<Attendees> ListAttendeesFromWS(string EventYear, string AutoMemberId, string OnlineWsCalID)
    {
        int retVal = -1;
        List<Attendees> ListAttendees = new List<Attendees>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "  select C.First_name, C.Last_Name, case when C.Email is null then IP.Email when C.Email ='' then IP.Email else C.Email end as Email,IP.State, IP.City, IP.Country, IP.Zip, Ip.Address1, case when IP.CPhone is null then IP.HPhone end as Phone, RO.OnlineWsCalId, RO.RegId, OWS.WebinarKey, RO.JoinURL, RO.RegistrantKey, IP.DonorType, IP1.Email as SpouseEmail, IP.SecondaryEmail, Ip.FirstName +' '+ IP.LastName as ParentName, P.Name as ProductName, OWS.Date, OWS.Time  from Registration_OnlineWkShop RO inner join Child C on (Ro.ChildNumber=C.ChildNumber) inner join Indspouse IP on (IP.AutoMemberId=C.MemberId)  left join Indspouse IP1 on (IP1.RelationShip=IP.AutomemberId) inner join OnlineWsCal OWS on (OWS.ProductGroupId=RO.ProductGroupId and OWS.ProductId=RO.ProductId and OWS.OnlineWsCalId=RO.OnlineWsCalId) inner join Product P on (OWS.ProductId=P.ProductId) where RO.Eventyear=" + EventYear + " and RO.Approved='Y' and RO.MemberId=" + AutoMemberId + " and RO.OnlineWsCalId in(" + OnlineWsCalID + ")";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Attendees attendees = new Attendees();
                        attendees.FirstName = dr["First_Name"].ToString();
                        attendees.LastName = dr["Last_Name"].ToString();
                        attendees.Email = dr["Email"].ToString();
                        attendees.Address = dr["Address1"].ToString();
                        attendees.Source = "NSF";
                        attendees.City = dr["City"].ToString();
                        attendees.State = dr["State"].ToString();
                        attendees.Country = dr["Country"].ToString();
                        attendees.Zip = dr["Zip"].ToString();
                        attendees.Phone = dr["Phone"].ToString();
                        attendees.OnlineWsCalID = dr["OnlineWsCalID"].ToString();
                        attendees.RegId = dr["RegId"].ToString();
                        attendees.Webinarkey = dr["WebinarKey"].ToString();
                        attendees.JoinURL = dr["JoinURL"].ToString();
                        attendees.RegistrantKey = dr["RegistrantKey"].ToString();
                        attendees.SpouseEmail = dr["SpouseEmail"].ToString();
                        attendees.SecondaryEmail = dr["SecondaryEmail"].ToString();
                        attendees.ParentName = dr["ParentName"].ToString();
                        attendees.ProductName = dr["ProductName"].ToString();
                        attendees.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        attendees.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss");

                        ListAttendees.Add(attendees);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListAttendees;

    }

    [WebMethod]
    public static List<Attendees> ListAttendeesFromChangeDate(string RegID)
    {
        int retVal = -1;
        List<Attendees> ListAttendees = new List<Attendees>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;


            cmdText = "  select C.First_name, C.Last_Name, case when C.Email is null then IP.Email when C.Email ='' then IP.Email else C.Email end as Email,IP.State, IP.City, IP.Country, IP.Zip, Ip.Address1, case when IP.CPhone is null then IP.HPhone end as Phone, RO.OnlineWsCalId, RO.RegId, OWS.WebinarKey, RO.JoinURL, RO.RegistrantKey, IP.DonorType, IP1.Email as SpouseEmail, IP.SecondaryEmail, Ip.FirstName +' '+ IP.LastName as ParentName, P.Name as ProductName, OWS.Date, OWS.Time  from Registration_OnlineWkShop RO inner join Child C on (Ro.ChildNumber=C.ChildNumber) inner join Indspouse IP on (IP.AutoMemberId=C.MemberId)  left join Indspouse IP1 on (IP1.RelationShip=IP.AutomemberId) inner join OnlineWsCal OWS on (OWS.ProductGroupId=RO.ProductGroupId and OWS.ProductId=RO.ProductId and OWS.OnlineWsCalId=RO.OnlineWsCalId) inner join Product P on (OWS.ProductId=P.ProductId) where RO.RegId=" + RegID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Attendees attendees = new Attendees();
                        attendees.FirstName = dr["First_Name"].ToString();
                        attendees.LastName = dr["Last_Name"].ToString();
                        attendees.Email = dr["Email"].ToString();
                        attendees.Address = dr["Address1"].ToString();
                        attendees.Source = "NSF";
                        attendees.City = dr["City"].ToString();
                        attendees.State = dr["State"].ToString();
                        attendees.Country = dr["Country"].ToString();
                        attendees.Zip = dr["Zip"].ToString();
                        attendees.Phone = dr["Phone"].ToString();
                        attendees.OnlineWsCalID = dr["OnlineWsCalID"].ToString();
                        attendees.RegId = dr["RegId"].ToString();
                        attendees.Webinarkey = dr["WebinarKey"].ToString();
                        attendees.JoinURL = dr["JoinURL"].ToString();
                        attendees.RegistrantKey = dr["RegistrantKey"].ToString();
                        attendees.SpouseEmail = dr["SpouseEmail"].ToString();
                        attendees.SecondaryEmail = dr["SecondaryEmail"].ToString();
                        attendees.ParentName = dr["ParentName"].ToString();
                        attendees.ProductName = dr["ProductName"].ToString();
                        attendees.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        attendees.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss");

                        ListAttendees.Add(attendees);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListAttendees;

    }


    [WebMethod]
    public static int UpdateJoinURLToAttendees(Webinar ObjWebinar)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Update Registration_OnlineWkShop set JoinURL='" + ObjWebinar.JoinURL + "', RegistrantKey='" + ObjWebinar.RegistrantKey + "' where RegId=" + ObjWebinar.RegId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);

            cmd.ExecuteNonQuery();
            retVal = 1;

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
            retVal = -1;
        }
        return retVal;
    }

    [WebMethod]
    public static List<Webinar> ListProductGroup(Webinar objWeniar)
    {
        int retVal = -1;
        List<Webinar> ListPrdGroup = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            if (objWeniar.EventId == "20")
            {

                cmdText = " select distinct P.productgroupcode, P.productgroupid from onlinewscal os inner join Productgroup P on (os.productGroupId=P.ProductGroupId) where os.eventyear =" + objWeniar.EventYear + " and P.EventId=" + objWeniar.EventId + "";
            }
            else
            {
                cmdText = " select distinct productgroupcode, productgroupid from Productgroup  where EventId=" + objWeniar.EventId + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjPrdgroup = new Webinar();
                        ObjPrdgroup.ProductGroupId = dr["productGroupId"].ToString();
                        ObjPrdgroup.ProductGroupCode = dr["ProductGroupCode"].ToString();


                        ListPrdGroup.Add(ObjPrdgroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListPrdGroup;

    }

    [WebMethod]
    public static List<Webinar> ListProduct(Webinar objWeniar)
    {
        int retVal = -1;
        List<Webinar> ListProduct = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select distinct P.ProductCode, P.ProductId from  product P  where  P.ProductgroupId = " + objWeniar.ProductGroupId + " and P.Status='O'";

            // and P.ProductgroupId = " + objWeniar.ProductGroupId + "


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjPrd = new Webinar();
                        ObjPrd.ProductId = dr["ProductId"].ToString();
                        ObjPrd.ProductCode = dr["ProductCode"].ToString();

                        ListProduct.Add(ObjPrd);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListProduct;

    }

    [WebMethod]
    public static List<Webinar> ListTeacher(Webinar objWeniar)
    {
        int retVal = -1;
        List<Webinar> ListTeacher = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            if (objWeniar.ProductId != "0" && objWeniar.ProductGroupId != "0" && objWeniar.ProductGroupId != null && objWeniar.EventId == "20")
            {

                cmdText = " select distinct Os.TeacherId, Ip.FirstName+' '+Ip.LastName as Name from onlinewscal Os inner join Indspouse IP on (Os.teacherId=IP.AutomemberId) where Os.eventyear =" + objWeniar.EventYear + " and os.productId = " + objWeniar.ProductId + "";
            }
            else if (objWeniar.EventId != "0" && objWeniar.ProductId == "0" && (objWeniar.ProductGroupId == "0" || objWeniar.ProductGroupId == null))
            {
                cmdText = " select distinct V.memberid as TeacherId, IP.FirstName+' '+IP.LastName as Name from Volunteer V inner join Indspouse IP on (IP.Automemberid=V.memberid) where V.EventId=" + objWeniar.EventId + "";
            }
            else if (objWeniar.EventId != "0" && objWeniar.ProductGroupId != "0" && objWeniar.ProductGroupId != null && (objWeniar.ProductId == "0" || objWeniar.ProductId == null))
            {
                cmdText = " select distinct V.memberid as TeacherId, IP.FirstName+' '+IP.LastName as Name from Volunteer V inner join Indspouse IP on (IP.Automemberid=V.memberid) where V.EventId=" + objWeniar.EventId + " and V.ProductGroupid=" + objWeniar.ProductGroupId + "";
            }
            else if (objWeniar.EventId != "0" && objWeniar.ProductGroupId != "0" && objWeniar.ProductGroupId != null && objWeniar.ProductId != "0" && objWeniar.ProductId != null)
            {
                cmdText = " select distinct V.memberid as TeacherId, IP.FirstName+' '+IP.LastName as Name from Volunteer V inner join Indspouse IP on (IP.Automemberid=V.memberid) where V.EventId=" + objWeniar.EventId + " and V.ProductGroupid=" + objWeniar.ProductGroupId + " and V.ProductId=" + objWeniar.ProductId + "";
            }

            //  and os.productId = " + objWeniar.ProductId + "


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjTeacher = new Webinar();
                        ObjTeacher.TeacherId = dr["TeacherId"].ToString();
                        ObjTeacher.TeacherName = dr["Name"].ToString();

                        ListTeacher.Add(ObjTeacher);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListTeacher;

    }
    [WebMethod]
    public static List<Attendees> ListNSFMembers(Attendees ObjAttendees)
    {
        List<Attendees> ListOrganizer = new List<Attendees>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string firstName = ObjAttendees.FirstName;
            string lastName = ObjAttendees.LastName;
            string email = ObjAttendees.Email;

            string strSql = "";


            int length = 0;
            if (firstName.Length > 0)
            {
                firstName = firstName.Replace(" ", "");
                if (firstName.Contains("_"))
                {
                    firstName = "'%" + firstName.Replace("_", "/_") + "%' ESCAPE '/' ";
                }
                else
                    firstName = "'%" + firstName + "%'";

                strSql += (" (ltrim(rtrim(I.firstName)) like " + firstName + " or  ltrim(rtrim(I1.firstName)) like " + firstName + " )");
            }
            if (lastName.Length > 0)
            {
                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                lastName = lastName.Replace(" ", "");
                if (lastName.Contains("_"))
                {
                    lastName = "'%" + lastName.Replace("_", "/_") + "%' ESCAPE '/' ";
                }
                else
                {
                    lastName = "'%" + lastName + "%'";

                }

                if (length > 0)
                {
                    strSql += (" and (ltrim(rtrim(I.lastName)) like " + lastName + " or ltrim(rtrim(I1.lastName)) like " + lastName + " )");
                }
                else
                {
                    strSql += ("  (ltrim(rtrim(I.lastName)) like " + lastName + " or ltrim(rtrim(I1.lastName)) like " + lastName + " )");
                }
            }

            if (email.Length > 0)
            {
                if (email.Contains("_"))
                {
                    email = "'%" + email.Replace("_", "/_") + "%' ESCAPE '/' ";
                }
                else
                    email = "'%" + email + "%'";

                if (!string.IsNullOrEmpty(strSql))
                {
                    length = strSql.ToString().Length;
                }
                if (length > 0)
                {
                    strSql += ("and (I.Email like " + email + " or I1.Email like " + email + " ) ");
                }
                else
                {
                    strSql += (" (I.Email like " + email + " or I1.Email like " + email + ")");
                }
            }


            if (strSql.Length > 0)
            {
                strSql = " Where " + strSql;
            }

            if ((firstName.Length > 0) || (lastName.Length > 0) || (email.Length > 0))
            {
                string StrQrySearch = "select distinct automemberid,Employer,firstname,lastname, firstName+' '+lastname as name,donortype,email,case when Email<>'' then 'Yes' else 'NO' end as [Exists],hphone,chapter,Cphone,"
    + " address1,city,state,zip,chapterCode ,referredby,liasonperson from ( select distinct I.automemberid,I.Employer,I.firstname,I.lastname,I1.firstname+' '+I1.lastName as name,I.donortype,I.email,case when I.Email<>'' then 'Yes' else 'NO' end as [Exists],I.hphone,I.chapter,I.Cphone,"
    + " I.address1,I.city,I.state,I.zip,C.chapterCode ,I.referredby,I.liasonperson from indspouse I LEFT JOIN Chapter C ON C.ChapterID = I.ChapterID left JOIN IndSpouse I1 ON I.automemberid=I1.Relationship and I.AutomemberID is not null  " + strSql + " "
    + " UNION ALL select distinct I1.automemberid,I1.Employer,I1.firstname,I1.lastname, I1.firstname+' '+I1.lastName as name,I1.donortype,I1.email,case when I1.Email<>'' then 'Yes' else 'NO' end as [Exists],I1.hphone,I1.chapter,I1.Cphone,"
    + " I1.address1,I1.city,I1.state,I1.zip,C.chapterCode ,I1.referredby,I1.liasonperson from indspouse I1 LEFT JOIN Chapter C ON C.ChapterID = I1.ChapterID left JOIN IndSpouse I ON I.automemberid=I1.Relationship and I1.AutomemberID is not null " + strSql + ""
    + " ) as tb order by name";

                SqlCommand cmd = new SqlCommand(StrQrySearch, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (null != ds)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {


                            Attendees objAtt = new Attendees();
                            objAtt.Name = dr["Name"].ToString();
                            objAtt.Email = dr["Email"].ToString();
                            objAtt.Login = dr["Exists"].ToString();
                            objAtt.DonorType = dr["DonorType"].ToString();
                            objAtt.HPhone = dr["HPhone"].ToString();
                            objAtt.CPhone = dr["CPhone"].ToString();
                            objAtt.Address = dr["Address1"].ToString();
                            objAtt.City = dr["City"].ToString();
                            objAtt.State = dr["State"].ToString();
                            objAtt.Chapter = dr["chapter"].ToString();
                            objAtt.AutoMemberId = dr["automemberid"].ToString();
                            ListOrganizer.Add(objAtt);
                        }
                    }
                }


            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
            // Response.Write(ex.ToString());
        }
        return ListOrganizer;

    }

    [WebMethod]
    public static List<Attendees> ListOrganizer(Attendees ObjAttendees)
    {
        List<Attendees> ListOrganizer = new List<Attendees>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            //string Cmdtext = "update WebinarSessions set CoOrganizerid='" + ObjAttendees.AutoMemberId.TrimEnd(',') + "' where WebinarKey=" + ObjAttendees.Webinarkey + "";

            //SqlCommand cmd = new SqlCommand(Cmdtext, cn);

            //cmd.ExecuteNonQuery();

            string StrQrySearch = "select distinct automemberid,firstName+' '+lastname as name,email from indspouse where AutoMemberid in(" + ObjAttendees.AutoMemberId.TrimEnd(',') + ")";

            SqlCommand cmd = new SqlCommand(StrQrySearch, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Attendees objAtt = new Attendees();
                        objAtt.Name = dr["Name"].ToString();
                        objAtt.Email = dr["Email"].ToString();
                        objAtt.Webinarkey = ObjAttendees.Webinarkey;
                        objAtt.AutoMemberId = dr["automemberid"].ToString();

                        ListOrganizer.Add(objAtt);
                    }
                }
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
            // Response.Write(ex.ToString());
        }
        return ListOrganizer;

    }

    [WebMethod]
    public static int InsertCoOrganizers(Attendees ObjAttendee)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = " insert into Webinar_Organizers (memberId, WebinarKey, JoinURl, RegistrantKey, CreatedDate,CreatedBy) values (" + ObjAttendee.AutoMemberId + ", '" + ObjAttendee.Webinarkey + "', '" + ObjAttendee.JoinURL + "', '" + ObjAttendee.RegistrantKey + "', Getdate()," + ObjAttendee.LoginId + ")";

            SqlCommand cmd = new SqlCommand(cmdText, cn);

            cmd.ExecuteNonQuery();

            cmdText = " update onlineWsCal set WebinarLink='" + ObjAttendee.JoinURL + "' where WebinarKey='" + ObjAttendee.Webinarkey + "'";
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();

            retVal = 1;

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
            retVal = -1;
        }
        return retVal;
    }

    [WebMethod]
    public static List<Attendees> ListOrganizersFromNSF(Attendees ObjAttendees)
    {
        List<Attendees> ListOrganizer = new List<Attendees>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            //string Cmdtext = "update WebinarSessions set CoOrganizerid='" + ObjAttendees.AutoMemberId.TrimEnd(',') + "' where WebinarKey=" + ObjAttendees.Webinarkey + "";

            //SqlCommand cmd = new SqlCommand(Cmdtext, cn);

            //cmd.ExecuteNonQuery();

            string StrQrySearch = "select distinct automemberid,firstName+' '+lastname as name,email, OZ.JoinURl from indspouse IP inner join Webinar_Organizers OZ on (OZ.MemberId=IP.AutoMemberId) where OZ.WebinarKey='" + ObjAttendees.Webinarkey + "'";

            SqlCommand cmd = new SqlCommand(StrQrySearch, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Attendees objAtt = new Attendees();
                        objAtt.Name = dr["Name"].ToString();
                        objAtt.Email = dr["Email"].ToString() + "test";
                        objAtt.JoinURL = dr["JoinURL"].ToString();
                        objAtt.AutoMemberId = dr["automemberid"].ToString();

                        ListOrganizer.Add(objAtt);
                    }
                }
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
            // Response.Write(ex.ToString());
        }
        return ListOrganizer;

    }

    [WebMethod]
    public static List<Webinar> GetWebinarDetails(string OnlineWsCalID)
    {
        int retVal = -1;
        List<Webinar> ListWebinar = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select * from OnlineWsCal where OnlineWsCalId=" + OnlineWsCalID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjWebinar = new Webinar();
                        ObjWebinar.ProductGroupId = dr["ProductgroupId"].ToString();
                        ObjWebinar.ProductId = dr["ProductId"].ToString();
                        ObjWebinar.TeacherId = dr["TeacherId"].ToString();
                        ObjWebinar.EventYear = dr["EventYear"].ToString();
                        ObjWebinar.StartDate = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd");
                        ObjWebinar.StartTime = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss");
                        ObjWebinar.Duration = Convert.ToDouble(dr["Duration"].ToString());
                        //  ObjWebinar.EndDate = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd");

                        //  string EndTime = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        //   ObjWebinar.EndTime = Convert.ToDateTime(EndTime).AddHours(ObjWebinar.Duration).ToString("HH:mm:ss");

                        //     ObjWebinar.UTCStartTime = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss") + "Z";
                        //    ObjWebinar.UTCEndTime = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd") + "T" + ObjWebinar.EndTime + "Z";

                        ListWebinar.Add(ObjWebinar);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListWebinar;

    }

    [WebMethod]
    public static int CheckDuplicateWebinar(Webinar ObjWebianr)
    {
        int retVal = -1;

        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select count(*) as CountSet from WebinarSessions where  EventYear=" + ObjWebianr.EventYear + "  and teacherId=" + ObjWebianr.TeacherId + " and StartDate='" + ObjWebianr.StartDate + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return retVal;
    }

    [WebMethod]
    public static int CheckDuplicateOrganizer(Webinar ObjWebianr)
    {
        int retVal = -1;

        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select count(*) as CountSet from Webinar_Organizers where memberId=" + ObjWebianr.TeacherId + " and WebinarKey='" + ObjWebianr.WebinarKey + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return retVal;
    }

    [WebMethod]
    public static int DeleteOrganizer(Webinar ObjWebianr)
    {
        int retVal = -1;

        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " Delete from Webinar_Organizers where memberId=" + ObjWebianr.TeacherId + " and WebinarKey='" + ObjWebianr.WebinarKey + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return retVal;
    }

    [WebMethod]
    public static List<Webinar> GetWebinarDetailsFromNSF(string Webinarkey)
    {
        int retVal = -1;
        List<Webinar> ListWebinar = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select ws.*, IP.FirstName+' '+IP.LastName as Name from WebinarSessions Ws inner join Indspouse IP on (ws.Teacherid=IP.AutoMemberId) where webinarkey='" + Webinarkey + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjWebinar = new Webinar();
                        ObjWebinar.ProductGroupId = dr["ProductgroupId"].ToString();
                        ObjWebinar.ProductId = dr["ProductId"].ToString();
                        ObjWebinar.TeacherId = dr["TeacherId"].ToString();
                        ObjWebinar.TeacherName = dr["Name"].ToString();
                        ObjWebinar.EventYear = dr["EventYear"].ToString();
                        ObjWebinar.StartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM-dd-yyyy");
                        ObjWebinar.StartTime = Convert.ToDateTime(dr["StartTime"].ToString()).ToString("HH:mm:ss");
                        ObjWebinar.Duration = Convert.ToDouble(dr["Duration"].ToString());
                        //   ObjWebinar.EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("yyyy-MM-dd");

                        //     ObjWebinar.EndTime = Convert.ToDateTime(dr["EndTime"].ToString()).ToString("HH:mm:ss");
                        ObjWebinar.WebinarKey = dr["WebinarKey"].ToString();
                        ObjWebinar.EventId = dr["EventId"].ToString();
                        ObjWebinar.Title = dr["title"].ToString();

                        ListWebinar.Add(ObjWebinar);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListWebinar;

    }

    [WebMethod]
    public static int DeleteWebinar(Webinar ObjWebianr)
    {
        int retVal = -1;

        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " Delete from WebinarSessions where   WebinarKey='" + ObjWebianr.WebinarKey + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return retVal;
    }
    [WebMethod]
    public static List<Event> ListEvents()
    {
        int retVal = -1;
        List<Event> ObjListEvent = new List<Event>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select EventId , EventCode, name from Event where Status='O'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Event ObjEvent = new Event();
                        ObjEvent.EventId = dr["EventId"].ToString();
                        ObjEvent.EventName = dr["Name"].ToString();
                        ObjEvent.EventCode = dr["EventCode"].ToString();
                        ObjListEvent.Add(ObjEvent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ObjListEvent;
    }

    [WebMethod]
    public static List<Webinar> GetWebinarStartDateAndEndDate(Webinar ObjWebinarIn)
    {
        int retVal = -1;
        List<Webinar> ListWebinar = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select * from OnlineWsCal where productId=" + ObjWebinarIn.ProductId + "  and EventYear=" + ObjWebinarIn.EventYear + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjWebinar = new Webinar();
                        ObjWebinar.ProductGroupId = dr["ProductgroupId"].ToString();
                        ObjWebinar.ProductId = dr["ProductId"].ToString();
                        ObjWebinar.TeacherId = dr["TeacherId"].ToString();
                        ObjWebinar.EventYear = dr["EventYear"].ToString();
                        ObjWebinar.StartDate = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM-dd-yyyy");
                        ObjWebinar.StartTime = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss");
                        ObjWebinar.Duration = Convert.ToDouble(dr["Duration"].ToString());
                        //  ObjWebinar.EndDate = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd");

                        //   string EndTime = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-mm-dd") + " " + Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss");
                        //  ObjWebinar.EndTime = Convert.ToDateTime(EndTime).AddHours(ObjWebinar.Duration).ToString("HH:mm:ss");

                        //  ObjWebinar.UTCStartTime = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm:ss") + "Z";
                        //   ObjWebinar.UTCEndTime = Convert.ToDateTime(dr["Date"].ToString()).ToString("yyyy-MM-dd") + "T" + ObjWebinar.EndTime + "Z";

                        ListWebinar.Add(ObjWebinar);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListWebinar;
    }

    [WebMethod]
    public static string GetWebinarTitle(Webinar ObjWebinarIn)
    {

        string WebinarTitle = string.Empty;
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select E.EventId, E.Name as EventName, E.EventCode, P.ProductCode, P.Name, PG.ProductgroupCode,PG.Name as PGName from Product P inner join Event E on (P.EventId=E.EventId) inner join ProductGroup PG on (PG.ProductGroupid=P.ProductGroupId)  where p.ProductGroupid=" + ObjWebinarIn.ProductGroupId + "  ";
            if (ObjWebinarIn.ProductId != "0" && ObjWebinarIn.ProductId != null)
            {
                cmdText += " and p.ProductId=" + ObjWebinarIn.ProductId + "";
            }


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ObjWebinarIn.ProductId != "0" && ObjWebinarIn.ProductId != null)
                    {
                        WebinarTitle = ObjWebinarIn.EventYear + " " + ds.Tables[0].Rows[0]["EventName"].ToString() + "-" + ds.Tables[0].Rows[0]["ProductCode"].ToString() + " " + ds.Tables[0].Rows[0]["Name"].ToString();
                    }
                    else
                    {
                        WebinarTitle = ObjWebinarIn.EventYear + " " + ds.Tables[0].Rows[0]["EventName"].ToString() + "-" + ds.Tables[0].Rows[0]["ProductGroupCode"].ToString() + " " + ds.Tables[0].Rows[0]["PGName"].ToString();
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return WebinarTitle;
    }


    [WebMethod]
    public static List<Webinar> ValidateWebinarDateTimeConflicts(Webinar ObjWebinar)
    {
        List<Webinar> ObjListWebinar = new List<Webinar>();
        string StartTime = ObjWebinar.StartTime;
        string StartDateAndTime = ObjWebinar.StartDate + " " + ObjWebinar.StartTime;
        StartDateAndTime = Convert.ToDateTime(StartDateAndTime).AddMinutes(-30).ToString();
        DateTime dtStartTime = Convert.ToDateTime(StartDateAndTime);
        Double Duration = Convert.ToDouble(ObjWebinar.Duration) + 0.5;
        string EndTime = Convert.ToDateTime(StartDateAndTime).AddHours(Duration).ToString();
        DateTime dtEndTime = Convert.ToDateTime(EndTime);
        bool IsValidation = true;
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select StartDate, StartTime, Duration from WebinarSessions where EventYear=" + ObjWebinar.EventYear + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string Date = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        if (Date == ObjWebinar.StartDate.Trim())
                        {
                            string StrStartTime = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy") + " " + Convert.ToDateTime(dr["StartTime"].ToString()).ToString("HH:mm:ss");
                            DateTime dtWrStartTime = Convert.ToDateTime(StrStartTime);
                            string StrEndTime = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy") + " " + Convert.ToDateTime(dr["StartTime"].ToString()).ToString("HH:mm:ss");
                            StrEndTime = Convert.ToDateTime(StrEndTime).AddHours(Convert.ToDouble(dr["Duration"].ToString())).ToString();
                            DateTime dtWrEndTime = Convert.ToDateTime(StrEndTime);


                            if ((dtStartTime >= dtWrStartTime || dtStartTime <= dtWrStartTime) && (dtStartTime <= dtWrEndTime && dtEndTime >= dtWrStartTime) && (dtEndTime <= dtWrEndTime || dtEndTime >= dtWrEndTime))
                            {


                                IsValidation = false;

                                Webinar ObjWeb = new Webinar();
                                ObjWeb.Duration = Convert.ToDouble(ds.Tables[0].Rows[0]["Duration"].ToString());
                                ObjWeb.StartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartTime"].ToString()).ToString("HH:mm:ss");
                                StrEndTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(ds.Tables[0].Rows[0]["StartTime"].ToString()).ToString("HH:mm:ss");
                                ObjWeb.EndTime = Convert.ToDateTime(StrEndTime).AddHours(ObjWeb.Duration).ToString("HH:mm:ss");
                                ObjWeb.IsValidation = "false";
                                ObjListWebinar.Add(ObjWeb);
                            }
                            else
                            {
                            }
                        }
                    }



                    if (IsValidation == true)
                    {
                        //Webinar ObjWeb = new Webinar();
                        //ObjWeb.IsValidation = "true";
                        //ObjWeb.StartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartTime"].ToString()).ToString("HH:mm:ss");
                        //ObjWeb.Duration = Convert.ToDouble(ds.Tables[0].Rows[0]["Duration"].ToString());
                        //ObjWeb.EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("yyyy-MM-dd");

                        //string StrEndTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(ds.Tables[0].Rows[0]["StartTime"].ToString()).ToString("HH:mm:ss");
                        //ObjWeb.EndTime = Convert.ToDateTime(StrEndTime).AddHours(ObjWeb.Duration).ToString("HH:mm:ss");

                        //ObjWeb.UTCStartTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("yyyy-MM-dd") + "T" + Convert.ToDateTime(ds.Tables[0].Rows[0]["StartTime"].ToString()).ToString("HH:mm:ss") + "Z";
                        //ObjWeb.UTCEndTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("yyyy-MM-dd") + "T" + ObjWeb.EndTime + "Z";
                        //ObjListWebinar.Add(ObjWeb);

                        Webinar ObjWeb = new Webinar();
                        ObjWeb.IsValidation = "true";
                        ObjWeb.StartDate = Convert.ToDateTime(ObjWebinar.StartDate).ToString("yyyy-MM-dd");
                        ObjWeb.StartTime = Convert.ToDateTime(ObjWebinar.StartTime).ToString("HH:mm:ss");
                        ObjWeb.Duration = Convert.ToDouble(ObjWebinar.Duration);
                        ObjWeb.EndDate = Convert.ToDateTime(ObjWeb.StartDate).ToString("yyyy-MM-dd");

                        ObjWeb.EndTime = Convert.ToDateTime(ObjWebinar.StartTime).AddHours(ObjWeb.Duration).ToString("HH:mm:ss");

                        string strWebStarTDateAndTime = ObjWeb.StartDate + " " + ObjWeb.StartTime;
                        DateTime dtWebStartTime = Convert.ToDateTime(strWebStarTDateAndTime).AddHours(4);
                        string strWebStartDate = dtWebStartTime.ToString("yyyy-MM-dd");
                        string strWebStartTime = dtWebStartTime.ToString("HH:mm:ss");

                        string strWebEndDateAndTime = ObjWeb.EndDate + " " + ObjWeb.EndTime;
                        DateTime dtWebEndTime = Convert.ToDateTime(strWebEndDateAndTime).AddHours(4);
                        string strWebEndDate = dtWebEndTime.ToString("yyyy-MM-dd");
                        string strWebEndTime = dtWebEndTime.ToString("HH:mm:ss");

                        ObjWeb.UTCStartTime = strWebStartDate + "T" + strWebStartTime + "Z";
                        ObjWeb.UTCEndTime = strWebEndDate + "T" + strWebEndTime + "Z";
                        ObjListWebinar.Add(ObjWeb);
                    }
                    else
                    {
                        // Webinar ObjWeb = new Webinar();

                        //  ObjListWebinar.Add(ObjWeb);
                    }

                }
                else
                {
                    Webinar ObjWeb = new Webinar();
                    ObjWeb.IsValidation = "true";
                    ObjWeb.StartDate = Convert.ToDateTime(ObjWebinar.StartDate).ToString("yyyy-MM-dd");
                    ObjWeb.StartTime = Convert.ToDateTime(ObjWebinar.StartTime).ToString("HH:mm:ss");
                    ObjWeb.Duration = Convert.ToDouble(ObjWebinar.Duration);
                    ObjWeb.EndDate = Convert.ToDateTime(ObjWebinar.StartDate).ToString("yyyy-MM-dd");

                    ObjWeb.EndTime = Convert.ToDateTime(EndTime).AddHours(ObjWeb.Duration).ToString("HH:mm:ss");

                    string strWebStarTDateAndTime = ObjWeb.StartDate + " " + ObjWeb.StartTime;
                    DateTime dtWebStartTime = Convert.ToDateTime(strWebStarTDateAndTime).AddHours(4);
                    string strWebStartDate = dtWebStartTime.ToString("yyyy-MM-dd");
                    string strWebStartTime = dtWebStartTime.ToString("HH:mm:ss");

                    string strWebEndDateAndTime = ObjWeb.EndDate + " " + ObjWeb.EndTime;
                    DateTime dtWebEndTime = Convert.ToDateTime(strWebEndDateAndTime).AddHours(4);
                    string strWebEndDate = dtWebEndTime.ToString("yyyy-MM-dd");
                    string strWebEndTime = dtWebEndTime.ToString("HH:mm:ss");


                    ObjWeb.UTCStartTime = strWebStartDate + "T" + strWebStartTime + "Z";
                    ObjWeb.UTCEndTime = strWebEndDate + "T" + strWebEndTime + "Z";
                    ObjListWebinar.Add(ObjWeb);
                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ObjListWebinar;
    }


    [WebMethod]
    public static List<Webinar> GetTeacherDetails(Webinar ObjWebinarIn)
    {
        int retVal = -1;
        List<Webinar> ListTeacher = new List<Webinar>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select FirstNAme +' '+LastName as Name, AutoMemberId from indspouse where AutoMemberId=" + ObjWebinarIn.TeacherId + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {


                        Webinar ObjWebinar = new Webinar();
                        ObjWebinar.TeacherName = dr["Name"].ToString();
                        ObjWebinar.TeacherId = dr["AutoMemberId"].ToString();


                        ListTeacher.Add(ObjWebinar);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return ListTeacher;
    }


    [WebMethod]
    public static int SendEmailToAdmin(string ParentName, string ParentEmail, string ParentPhone, string ProductName, string Date, string Time, string ChildName, string IssueName)
    {

        int retVal = -1;
        string tblHtml = "";
        try
        {
            tblHtml += "<table>";
            tblHtml += "<tr>";
            tblHtml += "<td>";
            tblHtml += "<span>Hi, </span> <br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";

            tblHtml += "<tr>";
            tblHtml += "<td>";

            tblHtml += "</td>";
            tblHtml += "</tr>";
            tblHtml += "<tr>";
            tblHtml += "<td>";


            tblHtml += "<span>There is a problem with online workshop registration.</span> <br />";
            tblHtml += "<span><b>Description:</b></span> <br />";
            tblHtml += "<span><b>The user is already registered with same email.</b></span> <br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";



            tblHtml += "<tr>";
            tblHtml += "<td> <br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";

            tblHtml += "<tr>";
            tblHtml += "<td>";
            tblHtml += "<b>Workshop Details</b><br />";
            tblHtml += "<b>Name : </b> " + ProductName + "<br />";
            tblHtml += "<b>Date : </b> " + Date + "<br />";
            tblHtml += "<b>Time : </b> " + Time + "<br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";

            tblHtml += "<tr>";
            tblHtml += "<td>  <br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";

            tblHtml += "<tr>";
            tblHtml += "<td>";
            tblHtml += "<b>Parent Details</b><br />";
            tblHtml += "<b>Name : </b> " + ParentName + "<br />";
            tblHtml += "<b>Email: </b> " + ParentEmail + "<br />";
            tblHtml += "<b>Phone: </b> " + ParentPhone + "<br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";

            tblHtml += "<tr>";
            tblHtml += "<td>  <br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";


            tblHtml += "<tr>";
            tblHtml += "<td>";
            tblHtml += "<b>Registration Details</b><br />";
            tblHtml += "<b>Child Name : </b> " + ChildName + "<br />";
            tblHtml += "</td>";
            tblHtml += "</tr>";


            tblHtml += "<tr>";
            tblHtml += "<td><br/>";
            tblHtml += "</td>";
            tblHtml += "</tr>";

            tblHtml += "</table>";
            //  volunteerEmail = "michael.simson@capestart.com;arul.raj@capestart.com,emim.lumina@capestart.com;";
            string fromEMail = "nsfcontests@northsouth.org";

            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(fromEMail);
                mail.Body = tblHtml;
                mail.Subject = "Problem with online workshop registration";

                // mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

                String[] strEmailAddressList = null;
                String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
                Match EmailAddressMatch = default(Match);
                string Email = "nsfprogramleads@gmail.com";
                //  Email = "michael.simson@capestart.com";
                mail.Bcc.Add(new MailAddress(Email.ToString().Trim()));

                mail.Bcc.Add(new MailAddress("michael.simson@capestart.com"));

                SmtpClient client = new SmtpClient();
                // client.Host = host;
                mail.IsBodyHtml = true;
                try
                {
                    client.Send(mail);
                    retVal = 1;
                    //lblError.Text = ex.ToString();
                }
                catch (Exception ex)
                {
                }
            }
            catch
            {
            }
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Set up Webinar");
        }
        return retVal;

    }


    [WebMethod]
    public static void ExceptionLog(string Msg)
    {

        CoachingExceptionLog.createWebinarExceptionLog("", "Exception", Msg, "Webinar registration");

    }

}