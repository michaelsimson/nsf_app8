Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports RKLib.ExportData
Imports Microsoft.ApplicationBlocks.Data

Partial Class ContactListUnassigned
    Inherits System.Web.UI.Page

    Protected isProductGroupSelectionChanged As Boolean
    Dim isDebug As Boolean = False
    Dim conn As SqlConnection

    Dim volRoleDataReader As SqlDataReader
    Dim isInEditMode As Boolean = False
    Dim editIndex As Integer = -1
    Dim loginId As Integer = 0



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'todo: uncomment before uploading
        If (Session("LoggedIn") Is Nothing) Or (Session("LoggedIn") <> "True") Then
            If (Not Session("entryToken") Is Nothing) Then
                Response.Redirect("Login.aspx?entry=" + Session("entryToken").ToString.Substring(1, 1))
            Else
                'showTimeOutMsg()
            End If
        End If

        Dim loginChapterId As Integer = 0
        Dim loginZoneId As Integer = 0
        Dim loginClusterId As Integer = 0
        Dim loginVolunteerId As Integer = 0
        Dim loginRoleId As Integer = 0
        Dim loginName As String = ""

        conn = New SqlConnection(Application("ConnectionString"))
        'If (Session("LoginID") = Nothing) Then
        '    showTimeOutMsg()
        '    Return
        'End If
        'If (Session("RoleId") = Nothing) Then
        '    showTimeOutMsg()
        '    Return
        'End If

        If (Not Page.IsPostBack) Then

            Dim nsfMaster As VRegistration.NSFMasterPage = Me.Master
            nsfMaster.addBackMenuItem("volunteerfunctions.aspx")

            loginId = CType(Session("LoginId"), Decimal)
            loginRoleId = CType(Session("RoleId"), Decimal)

            Dim sb As StringBuilder = New StringBuilder

            sb.Append("select firstname,lastname,v.volunteerid ,isnull(v.chapterId,0) as chapterid ,isnull(v.clusterid,0) as clusterid ,isnull(v.zoneid,0) as zoneid ")
            sb.Append(" from volunteer v ,indspouse i where v.memberid = i.automemberid and v.memberid= ")
            sb.Append(CType(loginId, String))
            sb.Append(" and roleid =")
            sb.Append(CType(loginRoleId, String))


            Dim dr As SqlDataReader
            Try
                dr = SqlHelper.ExecuteReader(Application("connectionString"), CommandType.Text, sb.ToString)
                ' Iterate through DataReader, should only be one 
                While (dr.Read())
                    If Not dr.IsDBNull(0) Then
                        loginName = CStr(dr("firstName")) + " " + CStr(dr("lastname"))
                        loginVolunteerId = CInt(dr("volunteerid"))
                        loginChapterId = CInt(dr("chapterid"))
                        loginZoneId = CInt(dr("ZoneId"))
                        loginClusterId = CInt(dr("ClusterId"))
                    End If
                End While
                If Not dr Is Nothing Then dr.Close()
            Finally
                dr = Nothing
            End Try


            If ((loginRoleId = Nothing) Or (loginRoleId = 0)) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = "There is no role assigned to this login id."
                Return
            End If
            If (loginRoleId < 1 Or loginRoleId > 5) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = "There is a mismatch in the assignment of roles.  Please contact the National Coordinator"
                Return
            End If

            Dim errMsg As String = Nothing
            If (loginRoleId = 3 And loginZoneId = 0) Then
                errMsg = "The login Zonal Coordinator's zone information missing"
            ElseIf (loginRoleId = 4 And loginClusterId = 0) Then
                errMsg = "The login Cluster coordinator's cluster information missing"
            ElseIf (loginRoleId = 5 And loginChapterId = 0) Then
                errMsg = "The login Chapter Coordinator's chapter information missing"
            End If
            If (Not errMsg Is Nothing) Then
                tblAssignRoles.Visible = False
                tblMessage.Visible = True
                lblError.Visible = True
                lblError.Text = errMsg
                Return
            End If



            Session("loginChapterId") = loginChapterId
            Session("loginRoleId") = loginRoleId
            Session("loginZoneId") = loginZoneId
            Session("loginClusterId") = loginClusterId
            Session("loginVolunteerId") = loginVolunteerId
            Session("loginName") = loginName

            DdlCluster.DataSource = ClusterDS
            DdlCluster.DataTextField = "ClusterCode"
            DdlCluster.DataValueField = "ClusterID"
            DdlCluster.DataBind()

            ddlChapter.DataSource = ChapersDSet
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()

            DdlZonalCoordinator.DataSource = ZoneDS
            DdlZonalCoordinator.DataTextField = "ZoneCode"
            DdlZonalCoordinator.DataValueField = "ZoneID"
            DdlZonalCoordinator.DataBind()

            loadSearchControls(loginVolunteerId, loginRoleId, loginZoneId, loginClusterId, loginChapterId)
        End If
        Page.MaintainScrollPositionOnPostBack = True
        clearMessages()
        If (isSessionValid()) Then
            showTimeOutMsg()
            Return
        End If
    End Sub
    Private Sub showTimeOutMsg()
        'lblSessionTimeout.Visible = True
        'lblSessionTimeout.Text = "<blink>Session has expired. Please try logging in again.</blink>"
        Response.Redirect("timeoutError.aspx")
    End Sub
    Private Function isSessionValid() As Boolean
        If (Session("LoginID") Is Nothing Or Session("loginRoleId") Is Nothing Or Session("loginVolunteerId") Is Nothing) Then
            Return True
        Else : Return False
        End If
    End Function
    Private Function getLoginTypeId(ByVal type As String) As Integer
        Dim id As Integer = 0
        If (type = "Zone") Then
            If (Not Session("loginZoneId") Is Nothing) Then
                id = CInt(Session("loginZoneId").ToString)
            End If
        ElseIf (type = "Chapter") Then
            If (Not Session("loginChapterId") Is Nothing) Then
                id = CInt(Session("loginChapterId").ToString)
            End If
        ElseIf (type = "Cluster") Then
            If (Not Session("loginClusterId") Is Nothing) Then
                id = CInt(Session("loginClusterId").ToString)
            End If
        End If
        Return id
    End Function
    Public Sub ddlCategory_selectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Dim ddl As DropDownList = sender
        Dim selectText As String = ddl.SelectedValue
        Dim loginRoleId As Integer = 0

        clearAllPanels()
        Session("volTaskSearchCriteria") = ""
        Session("volTaskSearchSql") = ""
        hideSearchCriteria()
        loadTaskDescription(selectText)
        If (selectText = "3") Then
            If ((Not Session("selChapterID") Is Nothing) And Session("selChapterID") <> "" And Session("selChapterID") <> "0" And Session("selChapterID") <> "Select Chapter") Then
                DdlZonalCoordinator.Enabled = False
                DdlCluster.Enabled = False
                ddlChapter.Enabled = False
            ElseIf ((Not Session("selClusterID") Is Nothing) And Session("selClusterID") <> "0") Then
                DdlCluster.Enabled = False
            ElseIf ((Not Session("selZoneID") Is Nothing) And Session("selZoneID") <> "0") Then
                DdlZonalCoordinator.Enabled = False
            Else
                DdlZonalCoordinator.Enabled = True
                DdlCluster.Enabled = True
                ddlChapter.Enabled = True
            End If
        Else
            DdlZonalCoordinator.Enabled = False
            DdlCluster.Enabled = False
            ddlChapter.Enabled = False
        End If

    End Sub

    Protected Function getLoginRoleId() As Integer
        Dim loginRoleId As Integer = 0
        Try
            If (Not Session("loginRoleId") Is Nothing) Then
                loginRoleId = CInt(Session("loginRoleId").ToString())
            End If
        Catch

        End Try
        Return loginRoleId
    End Function
    Private Sub LoadSelectSessions()
        'Response.Write("selChapterID=" & Session("selChapterID"))
        'Response.Write("selClusterID=" & Session("selClusterID"))
        'Response.Write("selZoneID=" & Session("selZoneID"))

        If ((Not Session("selChapterID") Is Nothing) And Session("selChapterID") <> "" And Session("selChapterID") <> "0" And Session("selChapterID") <> "Select Chapter") Then
            ddlChapter.SelectedIndex = -1
            If (Not ddlChapter.Items.FindByValue(Session("selChapterID")) Is Nothing) Then
                ddlChapter.Items.FindByValue(Session("selChapterID")).Selected = True
                ddlChapter.Enabled = False
            End If
        ElseIf ((Not Session("selClusterID") Is Nothing) And Session("selClusterID") <> "0") Then
            DdlCluster.SelectedIndex = -1
            If (Not DdlCluster.Items.FindByValue(Session("selClusterID")) Is Nothing) Then
                DdlCluster.Items.FindByValue(Session("selClusterID")).Selected = True
                DdlCluster.Enabled = False
            End If
        ElseIf ((Not Session("selZoneID") Is Nothing) And Session("selZoneID") <> "0") Then
            DdlZonalCoordinator.SelectedIndex = -1
            If (Not DdlZonalCoordinator.Items.FindByValue(Session("selZoneID")) Is Nothing) Then
                DdlZonalCoordinator.Items.FindByValue(Session("selZoneID")).Selected = True
                DdlZonalCoordinator.Enabled = False
            End If
        End If
    End Sub
    Public Sub loadSearchControls(ByVal volId As Integer, ByVal roleId As Integer, ByVal zoneId As Integer, ByVal clusterId As Integer, ByVal chapterId As Integer)
        'Response.Write("zoneid=" & zoneId)
        'Response.Write("clusterid=" & clusterId)
        'Response.Write("chapterid=" & chapterId)
        'Response.End()
        If (Not roleId = Nothing) Then
            lblLoginName.Text = Session("loginName")
            Select Case roleId
                Case Is < 3 'admin, NationalC
                    If (roleId = 1) Then
                        lblLoginRole.Text = " You are logged in as 'Admin'"
                    ElseIf (roleId = 2) Then
                        lblLoginRole.Text = "You are logged in as 'National Coordinator'."
                    End If
                    LoadSelectSessions()
                    DdlZonalCoordinator.Enabled = False
                    DdlCluster.Enabled = False
                    ddlChapter.Enabled = False
                Case 3 'ZonalC
                    lblLoginRole.Text = "You are logged in as 'Zonal Coordinator'"
                    SetCategory()

                    If (zoneId <> 0) Then
                        DdlZonalCoordinator.SelectedIndex = -1
                        DdlZonalCoordinator.Items.FindByValue(zoneId).Selected = True
                        LoadZonalChapters()
                        DdlZonalCoordinator.Enabled = False
                        DdlCluster.Enabled = False
                        ddlChapter.Enabled = True
                    End If

                    LoadSelectSessions()

                Case 4 'ClusterC
                    lblLoginRole.Text = "You are logged in as 'Cluster Coordinator'"
                    SetCategory()

                    If (clusterId <> 0) Then
                        DdlZonalCoordinator.Enabled = False
                        DdlCluster.SelectedIndex = -1
                        DdlCluster.Items.FindByValue(clusterId).Selected = True
                        LoadClusterChapters()
                        DdlCluster.Enabled = False
                        ddlChapter.Enabled = True
                    End If
                    LoadSelectSessions()
                Case 5 'ChapterC
                    lblLoginRole.Text = "You are logged in as 'Chapter Coordinator'"
                    SetCategory()
                    If (chapterId <> 0) Then
                        DdlZonalCoordinator.Enabled = False
                        DdlCluster.Enabled = False
                        ddlChapter.SelectedIndex = -1
                        ddlChapter.Items.FindByValue(chapterId).Selected = True
                        ddlChapter.Enabled = False
                    End If
            End Select
            loadTaskDescription(ddlCategory.SelectedValue)
        End If
    End Sub
    Private Sub SetCategory()
        ddlCategory.SelectedIndex = -1
        ddlCategory.Items.FindByValue(3).Selected = True 'set to chapter
        ddlCategory.Enabled = False
        loadTaskDescription(3)
    End Sub
    Public Sub loadTaskDescription(ByVal CategoryId)

        Dim strSql As String = "SELECT DISTINCT TaskDescription FROM VolunteerTasks where LevelCode='" + Trim(Convert.ToString(CategoryId)) + "'"


        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        Catch se As SqlException
            displayErrorMessage(strSql)
            Return
        End Try
        If (ds.Tables.Count > 0) Then
            ddlTaskDesc.DataSource = ds.Tables(0)
            ddlTaskDesc.DataBind()
            Dim lstItem As ListItem
            lstItem = New ListItem()
            lstItem.Text = "Select a Task Description"
            lstItem.Value = 0
            ddlTaskDesc.Items.Insert(0, lstItem)
            ddlTaskDesc.SelectedIndex = -1
            ddlTaskDesc.Items.FindByText("Select a Task Description").Selected = True
            ddlTaskDesc.Items.FindByText("Select a Task Description").Value = 0
        Else
            ddlTaskDesc.Items.Clear()
            ddlTaskDesc.Items.Add("Select a Task Description")
            ddlTaskDesc.SelectedIndex = -1
            ddlTaskDesc.Items.FindByText("Select a Task Description").Selected = True
            ddlTaskDesc.Items.FindByText("Select a Task Description").Value = 0
        End If
    End Sub

    Private Sub displayErrorMessage(ByVal msg As String)
        lblError.Visible = True
        'lblMsg.Visible = False
        lblMsg.Text = ""
        lblError.Text = msg
        tblMessage.Visible = True
        TblSearchString.Visible = True
        'txtSearchCriteria.Text = ""
    End Sub
    Private Sub clearMessages()
        lblError.Visible = False
        lblMsg.Visible = False
        lblMsg.Text = ""
        lblError.Text = ""
        lblSessionTimeout.Text = ""
        lblSessionTimeout.Visible = False
        lblUpdateError.Text = ""
    End Sub
    Private Sub clearAllPanels()
        tblVolRoleResults.Visible = False
        grdVolResults.Visible = False
    End Sub
    Private Function getSessionSelectionID(ByVal type As String) As Integer
        Dim selChapterId As Integer = 0  'sel*ID are values selected at VolunteerFunctions.aspx page
        Dim selClusterId As Integer = 0
        Dim selZoneId As Integer = 0
        Dim retId As Integer = 0

        If (Not Session("selZoneID") Is Nothing) Then
            selZoneId = CInt(Session("selZoneID"))
        End If
        If (Not Session("selClusterID") Is Nothing) Then
            selClusterId = CInt(Session("selClusterID"))
        End If
        If (Not Session("selChapterID") Is Nothing) Then
            selChapterId = CInt(Session("selChapterID"))
        End If
        Select Case type.ToLower()
            Case "zone"
                retId = selZoneId
            Case "cluster"
                retId = selClusterId
            Case "chapter"
                retId = selChapterId
        End Select
        Return retId
    End Function

    Protected Sub Find_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Find.Click

        Dim sb As StringBuilder = New StringBuilder()
        Dim sbSch As StringBuilder = New StringBuilder()
        Dim txtTaskDesc As String = "0" '= ddlRoleSch.SelectedValue
        Dim loginRoleId As Integer = getLoginRoleId()
        Dim bError As Boolean = False
        Dim strErrMsg As String = ""
        Dim strTaskDescText As String = ""

        Dim strCategory As String = ddlCategory.Items(ddlCategory.SelectedIndex).Text


        grdVolResults.Visible = False
        grdVolResults.Controls.Clear()
        Session("volRoleSearchCriteria") = ""
        Session("volRoleSearchSql") = ""
        hideSearchCriteria()

        Dim intIndex
        For intIndex = 0 To ddlTaskDesc.Items.Count - 1
            If ddlTaskDesc.Items(intIndex).Selected Then
                txtTaskDesc = txtTaskDesc + ", '" + ddlTaskDesc.Items(intIndex).Value.ToString() + "'"
                strTaskDescText = strTaskDescText + "|" + ddlTaskDesc.Items(intIndex).Text
            End If
        Next

        txtTaskDesc = CStr(txtTaskDesc)
        Dim intLength As String = txtTaskDesc.Length
        If intLength >= 2 Then txtTaskDesc = txtTaskDesc.Substring(2, intLength - 2)

        If ddlCategory.SelectedIndex = 0 Then
            bError = True
            strErrMsg = "Please select a Category"
        ElseIf txtTaskDesc.Length = 0 Then
            bError = True
            strErrMsg = "Please select a Task Description"
        Else
            sbSch.Append("<font color=green>Category = </font>" + strCategory + "<br>")
            sbSch.Append("<font color=green> Task Description = </font>" + strTaskDescText + "<br>")

            sb.Remove(0, sb.Length)
            sb.Append("SELECT VolunteerTaskID FROM VolunteerTasks where LevelCode = " + ddlCategory.Items(ddlCategory.SelectedIndex).Value + " and TaskDescription in (" + txtTaskDesc + ")")

        End If


        Dim whereStr As String = ""

        If (sb.Length() > 0) Then
            whereStr = sb.ToString()
        End If

        Session("volTaskSearchSql") = whereStr
        Session("volTaskSearchCriteria") = sbSch.ToString
        Session("searchTaskDesc") = strTaskDescText

        clearGrid()
        loadGrid(True)

        If (bError = True) Then
            displayErrorMessage(strErrMsg)
        End If
    End Sub
    Public Sub displaySearchCriteria()
        Dim str As String = Session("volTaskSearchCriteria").ToString
        'Dim str As String = Session("volRoleSearchSql").ToString ''for debugging
        TblSearchString.Visible = True
        txtSearchCriteria.Visible = True
        txtSearchCriteria.Text = str

    End Sub
    Public Sub hideSearchCriteria()
        TblSearchString.Visible = False
        txtSearchCriteria.Visible = False
        txtSearchCriteria.Text = False
    End Sub
    Public Sub clearGrid()
        btnExport1.Enabled = False
        btnEmailExport.Enabled = False
        grdVolResults.EditItemIndex = -1
        grdVolResults.DataSource = Nothing
        grdVolResults.DataBind()
    End Sub

    Public Sub loadGrid(ByVal blnReload As Boolean)
        btnExport1.Enabled = True
        btnEmailExport.Enabled = True
        If (isDebug = True) Then
            Response.Write("<br>loadGrid")
        End If
        Dim ds As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        cmd.CommandText = " " + Session("volTaskSearchSql").ToString

        Dim strChapterCondition = ""

        If (ddlChapter.SelectedValue <> "" And ddlChapter.SelectedValue <> "Select Chapter") Then
            strChapterCondition = ddlChapter.SelectedValue
        ElseIf (DdlZonalCoordinator.SelectedValue <> "0") Then
            strChapterCondition = "select ChapterID from Chapter where ZoneId=" + DdlZonalCoordinator.SelectedValue
        ElseIf (DdlCluster.SelectedValue <> "0") Then
            strChapterCondition = "select ChapterID from Chapter where ClusterId=" + DdlCluster.SelectedValue
        End If

       

        If (blnReload = True) Then
            'Response.Write(cmd.CommandText)
            Session("volDataSet") = Nothing
            Try

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "dbo.usp_SelectUnAssignedContactList", New SqlParameter("@ChaterCondition", strChapterCondition), New SqlParameter("@wherecondition", Session("volTaskSearchSql").ToString()))
                Session("volDataSet") = ds
                grdVolResults.CurrentPageIndex = 0
            Catch se As SqlException
                displayErrorMessage(cmd.CommandText + " " + se.ToString())
                'displayErrorMessage("There is an error while trying to execute the query.")
                Return
            End Try
        Else
            If (Not Session("volDataSet") Is Nothing) Then
                ds = CType(Session("volDataSet"), DataSet)
            End If
        End If

        If (ds Is Nothing Or ds.Tables.Count < 1) Then
            displayErrorMessage("No records matching your criteria ")
            grdVolResults.Visible = False
            tblVolRoleResults.Visible = False
            lblGrdVolResultsHdg.Text = ""
        ElseIf (ds.Tables(0).Rows.Count < 1) Then
            displayErrorMessage("No records matching your criteria ")
            grdVolResults.Visible = False
            tblVolRoleResults.Visible = False
            lblGrdVolResultsHdg.Text = ""
        Else
            Dim i As Integer = ds.Tables(0).Rows.Count
            grdVolResults.Visible = True
            grdVolResults.DataSource = ds.Tables(0)
            grdVolResults.DataBind()
            ' lblError.Visible = False
            'tblMessage.Visible = True
            tblVolRoleResults.Visible = True
            lblGrdVolResultsHdg.Text = "2. List of Contacts under<u>'" + Session("searchTaskDesc") + "'</u>"
        End If
        'hLinkAddNewVolunteer.Visible = True
        displaySearchCriteria()

    End Sub
    Protected Sub grdVolResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdVolResults.PageIndexChanged
        grdVolResults.CurrentPageIndex = e.NewPageIndex
        grdVolResults.EditItemIndex = -1
        loadGrid(False)
    End Sub
    Sub PagerButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim arg As String = sender.CommandArgument
        Select Case arg
            Case "Next"
                If (grdVolResults.CurrentPageIndex < (grdVolResults.PageCount - 1)) Then
                    grdVolResults.CurrentPageIndex += 1
                End If
            Case "Prev"
                If (grdVolResults.CurrentPageIndex > 0) Then
                    grdVolResults.CurrentPageIndex -= 1
                End If
            Case "Last"
                grdVolResults.CurrentPageIndex = (grdVolResults.PageCount - 1)
            Case Else
                grdVolResults.CurrentPageIndex = Convert.ToInt32(arg)
        End Select
        loadGrid(False)
    End Sub

    Protected Sub btnExport1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport1.Click
        ' Export all the details
        Try
            ' Get the datatable to export			

            Dim dtEmployee As DataTable = (CType(Session("volDataSet"), DataSet)).Tables(0).Copy()

            ' Specify the column list and headers to export
            Dim iColumns() As Integer = New Integer() {8, 9, 2, 7, 6, 4, 10, 11}
            Dim sHeaders() As String = New String() {"FistName", "LastName", "Email", "CPhone", "HPhone", "WPhone", "City", "State"}
            ' Export all the details to CSV
            Dim objExport As New RKLib.ExportData.Export("Web")
            objExport.ExportDetails(dtEmployee, iColumns, sHeaders, Export.ExportFormat.CSV, "nsfVolunteersUnassigned.csv")
        Catch Ex As Exception
            lblError.Text = Ex.Message
        End Try

    End Sub

    Protected Sub btnEmailExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailExport.Click
        Try

            Dim appType As String = "web"
            Dim response_ As System.Web.HttpResponse
            response_ = System.Web.HttpContext.Current.Response()
            'Appending Headers
            response_.Clear()
            response_.Buffer = True

            response_.ContentType = "text"
            response_.AppendHeader("content-disposition", "attachment; filename=ContestEmail.txt")

            Dim Stream As MemoryStream = New MemoryStream()
            Dim writer As System.Xml.XmlTextWriter = New System.Xml.XmlTextWriter(Stream, Encoding.UTF8)

            writer.Flush()
            Stream.Seek(0, SeekOrigin.Begin)
            ' Get the datatable to export			
            Dim dtEmployee As DataTable = (CType(Session("volDataSet"), DataSet)).Tables(0).Copy()

            Dim sw As System.IO.StringWriter = New System.IO.StringWriter()
            ' Specify the column list to export
            Dim iColumns() As Integer = New Integer() {12}
            Dim i As Integer
            Dim strEmail As String
            For i = 0 To dtEmployee.Rows.Count
                Try
                    strEmail = dtEmployee.Rows(i)(2)
                    If strEmail = "" Or strEmail = Nothing Then
                        strEmail = ""
                    End If
                Catch
                    strEmail = ""
                End Try
                If strEmail <> "" Then
                    sw.Write(strEmail)
                    sw.Write(",")
                End If
            Next

            'Writeout the Content				
            response_.Write(sw.ToString())
            sw.Close()
            writer.Close()
            Stream.Close()
            response_.End()


            ' Export the details of specified columns to Excel
            ' Dim objExport As New RKLib.ExportData.Export("Web")
            'objExport.ExportDetails(dtEmployee, iColumns, Export.ExportFormat.Excel, "EmployeesInfo2.xls")
        Catch Ex As Exception
            lblError.Text = Ex.Message
        End Try
    End Sub
    Private Sub LoadClusterChapters()
        If (DdlCluster.SelectedIndex <> 0) Then
            ddlChapter.Items.Clear()
            ddlChapter.Items.Insert(0, New ListItem("[Select Chapter]", String.Empty))
            ddlChapter.DataSource = ChaWithinClustersDS
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()
            DdlZonalCoordinator.SelectedIndex = -1
            DdlZonalCoordinator.Items.FindByText("[Select Zone]").Selected = True
        Else
            ddlChapter.Items.Clear()
            ddlChapter.Items.Insert(0, New ListItem("[Select Chapter]", String.Empty))
            ddlChapter.DataSource = ChapInZonesDS
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()
            DdlCluster.SelectedIndex = -1
            DdlCluster.Items.FindByText("[Select Cluster]").Selected = True
        End If
    End Sub
    Protected Sub Cluster_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DdlCluster.SelectedIndexChanged
        LoadClusterChapters()
    End Sub
    Private Sub LoadZonalChapters()
        If (DdlZonalCoordinator.SelectedIndex <> 0) Then
            ddlChapter.Items.Clear()
            ddlChapter.Items.Insert(0, New ListItem("[Select Chapter]", String.Empty))
            ddlChapter.DataSource = ChapInZonesDS
            ddlChapter.DataTextField = "ChapterCode"
            ddlChapter.DataValueField = "ChapterID"
            ddlChapter.DataBind()
            DdlCluster.SelectedIndex = -1
            DdlCluster.Items.FindByText("[Select Cluster]").Selected = True
        End If
    End Sub
    Protected Sub DdlZonalCoordinator_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles DdlZonalCoordinator.SelectedIndexChanged
        LoadZonalChapters()
    End Sub
End Class


