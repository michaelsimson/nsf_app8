Imports System
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports NorthSouth.BAL
Imports System.Web.Mail

Namespace VRegistration

    Partial Class ChangeWrkshpCenter
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If Page.IsPostBack = False Then
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    txtUserId.Text = Trim(Session("LoginEmail"))
                    txtUserId.Enabled = False
                    hlnkMainMenu.NavigateUrl = "../UserFunctions.aspx"
                End If
            End If
        End Sub

        Private Sub btnFindContest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindContest.Click
            Try
                Dim connContest As New SqlConnection(Application("ConnectionString"))
                Dim conn1 As New SqlConnection(Application("ConnectionString"))

                Dim prm(2) As SqlParameter
                prm(0) = New SqlParameter
                prm(0).ParameterName = "@EmailID"
                prm(0).Value = Server.HtmlEncode(txtUserId.Text)
                prm(0).Direction = ParameterDirection.Input

                prm(1) = New SqlParameter
                prm(1).ParameterName = "@AutoMemberID"
                prm(1).Direction = ParameterDirection.Output
                prm(1).SqlDbType = SqlDbType.Int

                SqlHelper.ExecuteScalar(conn1, CommandType.StoredProcedure, "usp_GetParentID", prm)
                'Dim intParentID As Int32 = CType(prm(1).Value, Int32)
                ViewState("ParentID") = prm(1).Value


                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim cmd As New SqlCommand
                Dim IndID As Integer
                Dim SpouseID As Integer
                Dim str As String

                str = "MemberId='" & ViewState("ParentID") & "'"

                Dim objChild As New Child
                Dim dsChild As New DataSet

                objChild.SearchChildWhere(Application("ConnectionString"), dsChild, str)
                ViewState("ChildInfo") = dsChild
                cmd = Nothing
                conn = Nothing

                Dim conn2 As New SqlConnection(Application("ConnectionString"))
                Dim cmd1 As New SqlCommand

                Dim dsContest As New DataSet
                Dim tblConest() As String = {"Contest"}

                Dim prmArray(2) As SqlParameter
                prmArray(0) = New SqlParameter
                prmArray(0).ParameterName = "@MemberID"
                prmArray(0).SqlDbType = SqlDbType.Int
                prmArray(0).Value = ViewState("ParentID")

                prmArray(0).Direction = ParameterDirection.Input
                prmArray(1) = New SqlParameter
                prmArray(1).ParameterName = "@ContestYear"
                Dim conn3 As SqlConnection = New SqlConnection(Application("ConnectionString").ToString())
                Dim eventyear As Integer = SqlHelper.ExecuteScalar(conn3, CommandType.Text, "SELECT EventYear FROM Event WHERE (EventId = 3) AND (Status = 'O')")
                ' MsgBox(eventyear & " " & ViewState("ParentID"))
                prmArray(1).Value = eventyear
                prmArray(1).Direction = ParameterDirection.Input
                ' SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContestInCluster", dsContest, tblConest, prmArray)
                SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetWrkshpInCluster", dsContest, tblConest, prmArray)
                ViewState("Contest") = dsContest
                Call DisplayPaidContests()
            Catch ex As Exception
                ' MsgBox(ex.ToString())

            End Try
        End Sub

        Private Sub DisplayPaidContests()
            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim prmArray(2) As SqlParameter
            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}
            Dim dvSelectedContes As DataView
            Dim strToday As Date = Date.Now

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@MemberID"
            prmArray(0).SqlDbType = SqlDbType.Int
            prmArray(0).Value = ViewState("ParentID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
            prmArray(1).Value = SqlHelper.ExecuteScalar(connContest, CommandType.Text, "SELECT EventYear FROM Event WHERE (EventId = 3) AND (Status = 'O')")
            prmArray(1).Direction = ParameterDirection.Input
            connContest = New SqlConnection(Application("ConnectionString"))

            'SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)
            SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetChangeCenterWorkshop", dsContestant, tblConestant, prmArray)

            If Not ViewState("SelectedContests") Is Nothing Then
                CType(ViewState("SelectedContests"), ArrayList).Clear()
            End If

            If dsContestant.Tables(0).Rows.Count > 0 Then
                ' lblError.Text = dsContestant.Tables(0).Rows.Count.ToString()
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                dgSelectedContests.DataBind()
                dgSelectedContests.Visible = True
                lblError.Visible = False
            Else
                lblError.Visible = True
                lblError.Text = "No workshop registration was found"
                dgSelectedContests.Visible = False
            End If

            connContest = Nothing

        End Sub

        Private Sub dgSelectedContests_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.EditCommand
            Dim i As Integer
            Dim intIndexKey As Integer
            Dim strChapterID As String
            Dim strWhere As String

            intIndexKey = dgSelectedContests.DataKeys(e.Item.ItemIndex)

            DisplayPaidContests()

            For i = 0 To dgSelectedContests.DataKeys.Count - 1
                If (dgSelectedContests.DataKeys(i) = intIndexKey) Then
                    dgSelectedContests.EditItemIndex = i
                    Exit For
                End If
            Next

            Page.DataBind()

        End Sub

        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound

            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info <BR> Amount:US$ " & e.Item.DataItem("Fee") & "<BR>" & _
             "Date Paid :" & e.Item.DataItem("PaymentDate") & "<BR>" & _
             "Reference :" & e.Item.DataItem("PaymentReference") & "<BR>"
                Case ListItemType.EditItem
                    Dim objChapters As New NorthSouth.BAL.Chapter
                    Dim dsChapters As New DataSet
                    Dim StrSql1 As String
                    If Session("entryToken") = "Parent" Then
                        StrSql1 = "Select * from chapter where Status='A' AND zoneid=(Select Zoneid from chapter where chapterid=" & Session("ChapterID") & ") order by State, ChapterCode"
                    Else
                        If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 29 Then
                            StrSql1 = "Select * from chapter where Status='A' order by State, ChapterCode"
                        Else
                            StrSql1 = "Select * from chapter where Status='A' AND zoneid=(Select Zoneid from chapter where chapterid=" & CType(e.Item.FindControl("lblChapterID"), Label).Text & ") order by State, ChapterCode"
                        End If
                    End If
                    dsChapters = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql1)

                    If dsChapters.Tables.Count > 0 Then
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataSource = dsChapters.Tables(0)
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataBind()
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                    End If
            End Select

        End Sub

        Private Sub dgSelectedContests_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.CancelCommand

            dgSelectedContests.EditItemIndex = -1
            Page.DataBind()
            DisplayPaidContests()
        End Sub
        Private Sub dgSelectedContests_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.UpdateCommand
            Dim iCount As Integer
            Dim strSql As String
            Dim flag As Integer = 0
            Dim bCurrentContest As Boolean
            Dim bNewContest As Boolean
            Dim bBadgeNumbers As Boolean
            Dim iDaysDiff As Integer
            If Session("RoleID") = "2" Or Session("RoleID") = "1" Then
                iDaysDiff = 0
            Else
                iDaysDiff = 0
            End If
            Dim strContest As String
            strContest = CType(e.Item.FindControl("lblContestDate"), Label).Text.ToUpper()
            If strContest = "TO BE ANNOUNCED" Then
                lblMessage.Visible = True
                lblMessage.Text = "Center you selected does not offer this workshop."
                dgSelectedContests.EditItemIndex = -1
                DisplayPaidContests()
            Else
                If CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue <> String.Empty And CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue <> CType(e.Item.FindControl("lblChapterID"), Label).Text Then
                    strSql = " SELECT  ECalendarId,EventDATE,RegDeadLine FROM EventCalendar WHERE EventYEAR=" & CType(e.Item.FindControl("lblContestYear"), Label).Text
                    strSql = strSql & " AND CHAPTERID=" & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & " AND PRODUCTID=" & CType(e.Item.FindControl("lblProductID"), Label).Text
                    strSql = strSql & " AND EventID=3"
                    'Response.Write(strSql)
                    Dim bIsUpdate As Boolean
                    Dim drContest As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                    Dim strContestID As String
                    Dim strContestDate As String
                    Dim strRegistrationDeadlineDate As String
                    If (drContest.Read()) Then
                        If Not drContest("RegDeadLine") Is System.DBNull.Value Then
                            strContestID = drContest("ECalendarId")
                            strRegistrationDeadlineDate = drContest("RegDeadLine")
                            'Response.Write(IsDate(strRegistrationDeadlineDate).ToString())
                            If IsDate(strRegistrationDeadlineDate) = True Then
                                'Response.Write(strRegistrationDeadlineDate + " " + CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text)
                                If (CDate(Date.Now) < DateAdd(DateInterval.Day, -1, CDate(strRegistrationDeadlineDate))) And (CDate(Date.Now) < DateAdd(DateInterval.Day, -1, CDate(CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text))) Then
                                    If IsDate(drContest("EventDate")) = True Then
                                        strContestDate = drContest("EventDate")

                                        If DateAdd(DateInterval.Day, iDaysDiff, CDate(Date.Now)) < CDate(CType(e.Item.FindControl("lblContestDate"), Label).Text) Then
                                            bCurrentContest = True
                                        Else
                                            bCurrentContest = False
                                        End If
                                        If DateAdd(DateInterval.Day, iDaysDiff, CDate(Date.Now)) < CDate(strContestDate) Then
                                            bNewContest = True
                                        Else
                                            bNewContest = False
                                        End If
                                    Else
                                        lblMessage.Visible = True
                                        lblMessage.Text = "This Workshop is not offered at the new center."
                                        bIsUpdate = False
                                    End If
                                Else
                                    lblMessage.Visible = True
                                    lblMessage.Text = "Registration Deadline passed in either of the Event."
                                    bIsUpdate = False
                                End If
                            End If
                        Else
                            flag = 1
                            lblMessage.Text = "Contest Date was not set. So you cannot switch at this time."
                            lblMessage.Visible = True
                        End If
                    Else
                        lblMessage.Visible = True
                        lblMessage.Text = "This WorkShop is not offered at the new center."
                        bIsUpdate = False
                    End If
                        Dim intIndexKey As Integer
                        'Commente dby ferdine
                        'If bCurrentContest = True And bNewContest = True And bBadgeNumbers = True Then
                        If bCurrentContest = True And bNewContest = True Then

                            bIsUpdate = True
                        Else

                            If IsDate(strContestDate) = True Then
                                bIsUpdate = False
                                lblMessage.Visible = True
                                lblMessage.Text = "It is too late to change the center."
                            Else
                                bIsUpdate = False
                            If (flag <> 1) Then
                                lblMessage.Visible = True
                                lblMessage.Text = "Center you selected does not offer this workshop."
                            End If
                            End If
                        End If
                        intIndexKey = dgSelectedContests.DataKeys(e.Item.ItemIndex)
                        ' MsgBox(intIndexKey)
                        If bIsUpdate = True Then
                            Dim prmArray(3) As SqlParameter

                            prmArray(0) = New SqlParameter
                            prmArray(0).ParameterName = "@RegID"
                            prmArray(0).SqlDbType = SqlDbType.Int
                            prmArray(0).Value = intIndexKey
                            prmArray(0).Direction = ParameterDirection.Input

                            prmArray(1) = New SqlParameter
                            prmArray(1).ParameterName = "@ChapterID"
                            prmArray(1).Value = CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue
                            prmArray(1).Direction = ParameterDirection.Input

                            prmArray(2) = New SqlParameter
                            prmArray(2).ParameterName = "@ECalendarId"
                            prmArray(2).Value = strContestID
                            prmArray(2).Direction = ParameterDirection.Input

                            Dim connContest As New SqlConnection(Application("ConnectionString"))
                            Dim j As Integer = 0
                            j = SqlHelper.ExecuteNonQuery(connContest, CommandType.StoredProcedure, "usp_ChangeWrkShpCenter", prmArray)
                            'Ferdine
                            If j > 0 Then
                                Dim strs As String = "Insert INTO ContestChange(parentId,ContestYear,F_ChapterID,F_ChildNumber,F_ContestID,F_ProductGroupID,F_ProductGroupCode,F_ProductID,F_ProductCode,T_ChapterID,T_ChildNumber,T_ContestID,T_ProductGroupID,T_ProductGroupCode,T_ProductID,T_ProductCode,Contestant_ID,EventID,EventCode,CreatedDate, CreatedBy) values("
                                strs = strs & CType(e.Item.FindControl("lblMemberID"), Label).Text & "," & CType(e.Item.FindControl("lblContestYear"), Label).Text & "," & CType(e.Item.FindControl("lblChapterID"), Label).Text & "," & CType(e.Item.FindControl("lblChildID"), Label).Text & "," & CType(e.Item.FindControl("lblECalendarId"), Label).Text & "," & CType(e.Item.FindControl("lblProductGroupId"), Label).Text & ",'" & CType(e.Item.FindControl("lblProductGroupCode"), Label).Text & "'," & CType(e.Item.FindControl("lblProductID"), Label).Text & ",'" & CType(e.Item.FindControl("lblProductCode"), Label).Text
                                strs = strs & "'," & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & "," & CType(e.Item.FindControl("lblChildID"), Label).Text & "," & strContestID & "," & CType(e.Item.FindControl("lblProductGroupId"), Label).Text & ",'" & CType(e.Item.FindControl("lblProductGroupCode"), Label).Text & "'," & CType(e.Item.FindControl("lblProductID"), Label).Text & ",'" & CType(e.Item.FindControl("lblProductCode"), Label).Text & "'," & intIndexKey & ",3,'WkShop','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "'," & Session("LoginID") & ")"
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strs)
                                lblMessage.Text = "Record Updated Successfully"
                                lblMessage.Visible = True
                            End If
                        End If
                        dgSelectedContests.EditItemIndex = -1
                        ViewState("SelectedContests") = Nothing

                        DisplayPaidContests()
                    Else
                    lblMessage.Text = "Please Select Valid Chapter"
                        lblMessage.Visible = True
                    End If
                

            End If
        End Sub

    End Class

End Namespace


