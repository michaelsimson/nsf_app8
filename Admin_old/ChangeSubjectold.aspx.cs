﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class Admin_ChangeSubject : System.Web.UI.Page
{
    DataSet DsDetails;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnFindContest_Click(object sender, EventArgs e)
    {
        string commandString = "select C.First_Name+''+C.Last_Name as Name,O.Organization_Name,Rp.productcode,Rp.Fee,Rp.PaymentDate from " +

            " Registration_PrepClub Rp left join OrganizationInfo O on Rp.VenueID=o.automemberid left join Child c on Rp.childnumber=c.childnumber";



        DsDetails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandString);

        dgSelectedChild.DataSource = DsDetails;
        dgSelectedChild.DataBind();
    }
}