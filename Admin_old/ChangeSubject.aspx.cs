﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
/// <summary>
/// Modified by Suriya
/// Modified Date: Jun 17 2014.

/// /// Modified by Ahila
/// Modified Date: Jun 18 2014.
/// </summary>
public partial class Admin_ChangeSubject : System.Web.UI.Page
{
    #region Variable Declaration

    DataSet DsDetails;
    DropDownList ddlTemp = null;
    DataSet dsDropdown;

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (null ==Session["LoggedIn"] || Session["LoggedIn"].ToString().ToLower() != "true")
            {
                Response.Redirect("..\\maintest.aspx");
            }
            if (Convert.ToString(Session["entryToken"]) == "Parent")
            {
                txtUserId.Text = Convert.ToString(Session["UserID"]);
                txtUserId.Enabled = false;
            }
        }
        catch
        {
            SessionExp();
        }
    }
    private void SessionExp()
    {
        if (Session["LoggedIn"] == null)
        {
            Response.Redirect("..\\maintest.aspx");
        }
    }

    protected void btnFindContest_Click(object sender, EventArgs e)
    {
        if (txtUserId.Text != "")
        {
            Data();
        }
    }   

    protected void dgSelectedChild_RowEditing(object sender, GridViewEditEventArgs e)
    {
        dgSelectedChild.EditIndex = e.NewEditIndex;
        Data();
    }

    protected void lnkDropdown_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdchild");
            HiddenField Grade = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("Hdgrade");
            HiddenField product = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdProductcode");
            HiddenField Fee = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdFee");
            ViewState["ChildNumber"] = Childnumber.Value;
            ViewState["Grade"] = Grade.Value;
            ViewState["Product"] = product.Value;
            ViewState["Fee"] = Fee.Value;
        }
        catch
        {
            Response.Write("Admin Error");
        }
    }

    protected void ddProductGroup(object sender, System.EventArgs e)
    {
        try
        {
            ddlTemp = (DropDownList)sender;
            string commandStringDropdown = "select distinct(E.productcode),pc.RegDeadline,pc.StartTime,CAST(pc.venueID AS VARCHAR(10)) + ',' + E.productcode as CodeVSvenue ," +
            "pc.EndTime,pc.venueID,E.productcode+','+O.ORGANIZATION_NAME as ProductVenue" +
            " from EventFees E  left join   PrepClubCal Pc on  E.EventID=pc.EventID and E.ProductID=pc.ProductId left join OrganizationInfo O on O.AutoMemberID=Pc.VenueID  " +
            " where  " + ViewState["Grade"] + " between E.GradeFrom and E.GradeTo and E.ProductCode not in  " +
            " (select productcode from Registration_PrepClub where ChildNumber=" + ViewState["ChildNumber"] + " ) and e.RegFee <=" + ViewState["Fee"] + "" +
            "and (pc.RegDeadline>GETDATE() or ( pc.RegDeadline=GETDATE() and cast(GETDATE() as time) between pc.StartTime and pc.EndTime)) and VenueId is not null;";
            dsDropdown = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandStringDropdown);
            ddlTemp.DataSource = dsDropdown;
            DataTable dt = dsDropdown.Tables[0];
            ddlTemp.DataTextField = "ProductVenue";
            ddlTemp.DataValueField = "CodeVSvenue";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "[Select Subject]");
            ddlTemp.SelectedIndex = 0;
        }
        catch
        {
            Response.Write("Admin Error");
        }
    }

    protected void dgSelectedChild_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        dgSelectedChild.EditIndex = -1;
        Data();
    }
   
    protected void dgSelectedChild_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            DropDownList ddfromchaptercod = (DropDownList)dgSelectedChild.Rows[e.RowIndex].FindControl("DDNewSubject");
            string str = string.Empty;

            if (ddfromchaptercod.SelectedItem.Text != "[Select Subject]")
            {
                str = ddfromchaptercod.SelectedValue;
                string[] splitProduct = str.Split(',');
                string productcodeName = splitProduct[1];
                string ProductVenue = splitProduct[0];
                int ObjValiddate = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text,
                    " select  prepclubcalid from PrepClubCal where (DATEADD(dd, 0, DATEDIFF(dd, 0, regdeadline))>cast(GETDATE() as date) and  productcode='" 
                    + productcodeName + "' and venueid=" + ProductVenue + ") or ((DATEADD(dd, 0, DATEDIFF(dd, 0, regdeadline))=cast(GETDATE() as date)) and (starttime>cast(GETDATE() as time))   and  productcode='"  
                    + productcodeName + "' and venueid=" + ProductVenue + ")"));
                if (ObjValiddate != 0)
                {
                        string qryUpdate = "update Registration_PrepClub set venueid=" + ProductVenue + " ,ProductCode='"
                            + productcodeName + "' where ProductCode='" + ViewState["Product"] + "' and ChildNumber="
                            + ViewState["ChildNumber"] + "";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);
                        Response.Write("<script>alert('Updated successfully')</script>");
                }
                else
                {
                    Response.Write("<script>alert('Time Deadline Crossed ')</script>");
                }
                dgSelectedChild.EditIndex = -1;
                Data();
            }
            dgSelectedChild.EditIndex = -1;
            Data();
        }
        catch (Exception ex)
        {
            Response.Write("Admin Error");
        }
    }
    #endregion

    #region User Methods
    protected void Data()
    {
        try
        {
            string commandString = "select C.First_Name+' '+C.Last_Name as Name,I.FirstName+''+I.LastName as ParentName,Rp.Grade," +
            "Rp.childnumber,O.Organization_Name,Rp.productcode,Rp.Fee,Rp.PaymentDate from " +
                " Registration_PrepClub Rp left join OrganizationInfo O on Rp.VenueID=o.automemberid left join " +
            "Child c on Rp.childnumber=c.childnumber left join IndSpouse I on I.AutoMemberID=Rp.MemberId  where I.Email='" + txtUserId.Text + "'";
            DsDetails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandString);
            dgSelectedChild.DataSource = DsDetails;
            dgSelectedChild.DataBind();

            if (null != DsDetails && DsDetails.Tables.Count > 0 && DsDetails.Tables[0].Rows.Count <= 0)
            {
                lblError.Visible = true;
            }
            else
            {
                lblError.Visible = false;
            }
        }
        catch
        {
            Response.Write("Admin Error");
        }
    }
    #endregion
}