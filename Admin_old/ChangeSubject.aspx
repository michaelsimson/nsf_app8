﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="ChangeSubject.aspx.cs" Inherits="Admin_ChangeSubject" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">


    <table id="tblLogin" width="50%" runat="server">
				<tr>
					<td></td><td></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<h1>Change Subject</h1>
					</td>
					
				</tr>
				 <tr>
			        <td colspan="4">
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
			          </td>
			    </tr>
			    <tr>
			        <td>&nbsp;</td>
			    </tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right">Parent Email ID</td>
					<td><asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="300" MaxLength="50"></asp:textbox><br /><br />
                        <asp:button id="btnFindContest" runat="server" CssClass="FormButtonCenter" 
                            Text="Find Subject" OnClick="btnFindContest_Click" ></asp:button><br>
						&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
							Display="Dynamic"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
							ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>

                           
				</tr></table>

 <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Email is not available" Visible="false"></asp:Label>
    <div>



    <asp:GridView ID="dgSelectedChild" runat="server" 
            Style="width: 90%; margin: 0 auto;" AutoGenerateColumns="false" 
            OnRowEditing="dgSelectedChild_RowEditing" 
           
           
            OnRowCancelingEdit="dgSelectedChild_RowCancelingEdit" 
            OnRowUpdating="dgSelectedChild_RowUpdating" 
            
               DataKeyNames="childnumber"    >
             <Columns> 
             
             <asp:TemplateField>
 
      <ItemTemplate>
      <asp:Button ID="btnEdit" Text="Change Subject" runat="server"   CommandName="Edit" OnClick="lnkDropdown_Click" />

      </ItemTemplate>
 
      <EditItemTemplate>
       <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" /> 

<asp:Button  ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
   
      </EditItemTemplate>

 
      </asp:TemplateField>



            <asp:TemplateField HeaderText="New Subject">
     <ItemTemplate>
      
            
     </ItemTemplate>
     <EditItemTemplate>
         <asp:DropDownList ID="DDNewSubject" runat="server" Width="150px"  AutoPostBack="false" OnPreRender="ddProductGroup" >
         
         </asp:DropDownList>
             <asp:TextBox Visible="false"  runat="server" ID="Txtproductcode" Text='<%#Eval("productcode") %>' />
              </EditItemTemplate>
     
</asp:TemplateField>


            <asp:TemplateField HeaderText="Child Name">
     <ItemTemplate>
            <asp:Label runat="server" ID="lbname"   Text='<%#Eval("Name") %>' />
             <asp:HiddenField ID="hdchild" Value='<%# Bind("childnumber")%>'  runat="server" />  
        
     </ItemTemplate>
    
</asp:TemplateField>


 <asp:TemplateField HeaderText="Current Venue">
     <ItemTemplate>
           
              <asp:Label runat="server" ID="lbOrg"   Text='<%#Eval("Organization_Name") %>' />
     </ItemTemplate>
    
</asp:TemplateField>

            
      
      <asp:TemplateField HeaderText="Current Subject">
     <ItemTemplate>
            <asp:Label runat="server" ID="lblyear"   Text='<%#Eval("productcode") %>' />
             <asp:HiddenField ID="hdProductcode" Value='<%# Bind("productcode")%>'  runat="server" /> 
              <asp:HiddenField ID="Hdgrade" Value='<%# Bind("Grade")%>'  runat="server" /> 
     </ItemTemplate>
    
</asp:TemplateField>

 <asp:TemplateField HeaderText="ParentName">
     <ItemTemplate>
           
              <asp:Label runat="server" ID="lbParent"   Text='<%#Eval("ParentName") %>' />
     </ItemTemplate>
    
</asp:TemplateField>

<asp:TemplateField HeaderText="Contact Info">
     <ItemTemplate>
           <asp:Label id="Label1" runat="server" CssClass="SmallFont" Text='<%#"PaymentInfo:" %>'>	
                                                    </asp:Label><br />
                                                           <asp:HiddenField ID="hdFee" Value='<%# Bind("Fee")%>'  runat="server" /> 
													<asp:Label id="lblFee" runat="server" CssClass="SmallFont" Text='<%#"Amount:"+ DataBinder.Eval(Container.DataItem, "Fee") %>'>	
                                                    </asp:Label><br />
                                                    <asp:Label id="lbtdate" runat="server" CssClass="SmallFont" Text='<%#"PaymentDate:"+ DataBinder.Eval(Container.DataItem, "PaymentDate") %>'>																									
													</asp:Label>
              
     </ItemTemplate>
    
</asp:TemplateField>



            


   


 


  
 
        </Columns>
            </asp:GridView>





 
    </div>
</asp:Content>

