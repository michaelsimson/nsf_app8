﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Reflection
Imports NativeExcel
Partial Class Admin_SpellingBeeCriteria
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("../maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            loadyear()
            LoadCount()
        End If
        lblcalc()
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year + 2
            ddlYear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlyear.Items(0).Selected = True
    End Sub

    Private Sub lblcalc()
        Try
            lbltotalSum.Text = Val(lbl11.Text) + Val(lbl12.Text) + Val(lbl21.Text) + Val(lbl22.Text) + Val(lbl31.Text) + Val(lbl32.Text)
            lblrsum.Text = Val(txt11r.Text) + Val(txt12r.Text) + Val(txt21r.Text) + Val(txt22r.Text) + Val(txt31r.Text) + Val(txt32r.Text)
            lblnsum.Text = Val(txt11n.Text) + Val(txt12n.Text) + Val(txt21n.Text) + Val(txt22n.Text) + Val(txt31n.Text) + Val(txt32n.Text)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadCount()
        ' display the number of words
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from SBPubCriteria where ContestYear=" & ddlYear.SelectedValue & "") > 0 Then
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 1 and SubLevel=1")
            'lbl11.Text = ds.Tables(0).Rows(0)(0)
            txt11r.Text = ds.Tables(0).Rows(0)(1)
            txt11n.Text = ds.Tables(0).Rows(0)(2)
            ddl11r.SelectedIndex = ddl11r.Items.IndexOf(ddl11r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl11n.SelectedIndex = ddl11n.Items.IndexOf(ddl11n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 1 and SubLevel=2")
            'lbl12.Text = ds.Tables(0).Rows(0)(0)
            txt12r.Text = ds.Tables(0).Rows(0)(1)
            txt12n.Text = ds.Tables(0).Rows(0)(2)
            ddl12r.SelectedIndex = ddl12r.Items.IndexOf(ddl12r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl12n.SelectedIndex = ddl12n.Items.IndexOf(ddl12n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 2 and SubLevel=1")
            ' lbl21.Text = ds.Tables(0).Rows(0)(0)
            txt21r.Text = ds.Tables(0).Rows(0)(1)
            txt21n.Text = ds.Tables(0).Rows(0)(2)
            ddl21r.SelectedIndex = ddl21r.Items.IndexOf(ddl21r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl21n.SelectedIndex = ddl21n.Items.IndexOf(ddl21n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 2 and SubLevel=2")
            ' lbl22.Text = ds.Tables(0).Rows(0)(0)
            txt22r.Text = ds.Tables(0).Rows(0)(1)
            txt22n.Text = ds.Tables(0).Rows(0)(2)
            ddl22r.SelectedIndex = ddl22r.Items.IndexOf(ddl22r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl22n.SelectedIndex = ddl22n.Items.IndexOf(ddl22n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 3 and SubLevel=1")
            'lbl31.Text = ds.Tables(0).Rows(0)(0)
            txt31r.Text = ds.Tables(0).Rows(0)(1)
            txt31n.Text = ds.Tables(0).Rows(0)(2)
            ddl31r.SelectedIndex = ddl31r.Items.IndexOf(ddl31r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl31n.SelectedIndex = ddl31n.Items.IndexOf(ddl31n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 3 and SubLevel=2")
            ' lbl32.Text = ds.Tables(0).Rows(0)(0)
            txt32r.Text = ds.Tables(0).Rows(0)(1)
            txt32n.Text = ds.Tables(0).Rows(0)(2)
            ddl32r.SelectedIndex = ddl32r.Items.IndexOf(ddl32r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl32n.SelectedIndex = ddl32n.Items.IndexOf(ddl32n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))
            btnExport.Visible = True
        Else
            btnGenerate.Enabled = False
        End If
        lbl11.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=1 and [sub-Level_NSF] =1")
        lbl12.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=1 and [sub-Level_NSF] =2")

        lbl21.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=2 and [sub-Level_NSF] =1")
        lbl22.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=2 and [sub-Level_NSF] =2")

        lbl31.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=3 and [sub-Level_NSF] =1")
        lbl32.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=3 and [sub-Level_NSF] =2")


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Sum of JSB and SSB in reg and Nat must be 1000
        'Sum of Reg & Nat must be less than total
        Dim JSBsumR, SSBSumR, JSBSumN, SSBSumN As Integer
        JSBsumR = 0
        SSBSumR = 0
        JSBSumN = 0
        SSBSumN = 0
        Try

            If ddl11r.SelectedValue = "JSB" Then
                JSBsumR = Val(txt11r.Text)
            Else
                SSBSumR = Val(txt11r.Text)
            End If
            If ddl12r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt12r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt12r.Text)
            End If
            If ddl21r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt21r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt21r.Text)
            End If
            If ddl22r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt22r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt22r.Text)
            End If
            If ddl31r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt31r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt31r.Text)
            End If
            If ddl32r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt32r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt32r.Text)
            End If


            'Nat
            If ddl11n.SelectedValue = "JSB" Then
                JSBSumN = Val(txt11n.Text)
            Else
                SSBSumN = Val(txt11n.Text)
            End If
            If ddl12n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt12n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt12n.Text)
            End If
            If ddl21n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt21n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt21n.Text)
            End If
            If ddl22n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt22n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt22n.Text)
            End If
            If ddl31n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt31n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt31n.Text)
            End If
            If ddl32n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt32n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt32n.Text)
            End If
            If JSBSumN < 1000 Then
                lblerr.Text = " JSB National is less than 1000"
                Exit Sub
            End If
            If JSBsumR < 1000 Then
                lblerr.Text = " JSB Regional is less than 1000"
                Exit Sub
            End If
            If SSBSumN < 1000 Then
                lblerr.Text = " SSB National is less than 1000"
                Exit Sub
            End If
            If SSBSumR < 1000 Then
                lblerr.Text = " SSB Regional is less than 1000"
                Exit Sub
            End If

            If Convert.ToInt32(lbl11.Text) < (Val(txt11r.Text) + Val(txt11n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 1 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl12.Text) < (Val(txt12r.Text) + Val(txt12n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 1 Sub Level 2  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl21.Text) < (Val(txt21r.Text) + Val(txt21n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 2 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl22.Text) < (Val(txt22r.Text) + Val(txt22n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 2 Sub Level 2  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl31.Text) < (Val(txt31r.Text) + Val(txt31n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 3 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl32.Text) < (Val(txt32r.Text) + Val(txt32n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 3 Sub Level 2  "
                Exit Sub
            End If

        Catch ex As Exception
            lblerr.Text = "Please enter all Values & Enter Numeric values only"
            Exit Sub
        End Try
        lblerr.Text = ""
        Dim strSQl As String
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from SBPubCriteria where ContestYear=" & ddlYear.SelectedValue & "") < 1 Then
            strSQl = "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (1,1," & lbl11.Text & ",'" & ddl11r.SelectedValue & "'," & Val(txt11r.Text) & ",'" & ddl11n.SelectedValue & "'," & Val(txt11n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (1,2," & lbl12.Text & ",'" & ddl12r.SelectedValue & "'," & Val(txt12r.Text) & ",'" & ddl12n.SelectedValue & "'," & Val(txt12n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (2,1," & lbl21.Text & ",'" & ddl21r.SelectedValue & "'," & Val(txt21r.Text) & ",'" & ddl21n.SelectedValue & "'," & Val(txt21n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (2,2," & lbl22.Text & ",'" & ddl22r.SelectedValue & "'," & Val(txt22r.Text) & ",'" & ddl22n.SelectedValue & "'," & Val(txt22n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (3,1," & lbl31.Text & ",'" & ddl31r.SelectedValue & "'," & Val(txt31r.Text) & ",'" & ddl31n.SelectedValue & "'," & Val(txt31n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (3,2," & lbl32.Text & ",'" & ddl32r.SelectedValue & "'," & Val(txt32r.Text) & ",'" & ddl32n.SelectedValue & "'," & Val(txt32n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Values inserted successfully"
        Else
            strSQl = "UPDATE SBPubCriteria SET Total=" & Val(lbl11.Text) & ", Regional=" & Val(txt11r.Text) & ", RegContest='" & ddl11r.SelectedValue & "', [National]=" & Val(txt11n.Text) & ", NatContest='" & ddl11n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 1 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl12.Text) & ", Regional=" & Val(txt12r.Text) & ", RegContest='" & ddl12r.SelectedValue & "', [National]=" & Val(txt12n.Text) & ", NatContest='" & ddl12n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 1 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl21.Text) & ", Regional=" & Val(txt21r.Text) & ", RegContest='" & ddl21r.SelectedValue & "', [National]=" & Val(txt21n.Text) & ", NatContest='" & ddl21n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 2 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl22.Text) & ", Regional=" & Val(txt22r.Text) & ", RegContest='" & ddl22r.SelectedValue & "', [National]=" & Val(txt22n.Text) & ", NatContest='" & ddl22n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 2 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl31.Text) & ", Regional=" & Val(txt31r.Text) & ", RegContest='" & ddl31r.SelectedValue & "', [National]=" & Val(txt31n.Text) & ", NatContest='" & ddl31n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 3 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl32.Text) & ", Regional=" & Val(txt32r.Text) & ", RegContest='" & ddl32r.SelectedValue & "', [National]=" & Val(txt32n.Text) & ", NatContest='" & ddl32n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 3 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Values updated successfully"
        End If
        btnGenerate.Enabled = True
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, "usp_createTempTable")
        Dim dstemp, dsword As DataSet
        dstemp = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select SBPubTempID from  sbpubtemp  order by Randvalue")
        dsword = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select word from Word_Master_New")
        Dim i As Integer
        For i = 0 To dsword.Tables(0).Rows.Count - 1
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE Word_Master_New set [Reg_Rand#] = " & dstemp.Tables(0).Rows(i)(0) & ",Regional_Flag=Null , nat_flag=Null where word='" & dsword.Tables(0).Rows(i)(0) & "'")
        Next
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "drop table SBPubTemp")
        dsword = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select [Regional],[National], [level],sublevel from SBPubCriteria where ContestYeaR=" & ddlYear.SelectedValue & "")
        Dim sqlstr As String
        For i = 0 To dsword.Tables(0).Rows.Count - 1
            sqlstr = "Update Word_Master_New set  Regional_Flag='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("Regional") & " word from Word_Master_New where Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional_Flag  is null and nat_flag is Null order by Reg_Rand#) AND  Regional_Flag  is null and nat_flag is Null and Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & "; "
            sqlstr = sqlstr & "Update Word_Master_New set  nat_flag='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("National") & " word from Word_Master_New where Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional_Flag  is null and nat_flag is Null order by Reg_Rand#) AND  Regional_Flag  is null and nat_flag is Null and Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & " "
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
            ' Response.Write(sqlstr & "<br><br>")
        Next
        lblerr.Text = "Generated Successfully"
        btnExport.Visible = True
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Create workbook
        Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim i As Integer
        Dim j As Integer = 0
        Dim ds As DataSet
        Dim sheet As IWorksheet = book.Worksheets.Add()
        sheet.Cells("A2").Value = "JSB_REG_WORDS"
        sheet.Cells("B2").Value = "Level"
        sheet.Cells("C2").Value = "SubLevel"
        sheet.Name = "JSB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='JSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
       
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet1 As IWorksheet = book.Worksheets.Add()
        sheet1.Cells("A2").Value = "JSB_NAT_WORDS"
        sheet1.Cells("B2").Value = "Level"
        sheet1.Cells("C2").Value = "SubLevel"
        sheet1.Name = "JSB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.NatContest='JSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet1.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet1.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet1.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet2 As IWorksheet = book.Worksheets.Add()
        sheet2.Cells("A2").Value = "SSB_REG_WORDS"
        sheet2.Cells("B2").Value = "Level"
        sheet2.Cells("C2").Value = "SubLevel"
        sheet2.Name = "SSB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='SSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet2.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet2.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet2.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet3 As IWorksheet = book.Worksheets.Add()
        sheet3.Cells("A2").Value = "SSB_NAT_WORDS"
        sheet3.Cells("B2").Value = "Level"
        sheet3.Cells("C2").Value = "SubLevel"
        sheet3.Name = "SSB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.NatContest='SSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet3.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet3.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet3.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='JSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.RegContest='JSB' order by w.[Reg_Rand#]

        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='SSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.RegContest='SSB' order by w.[Reg_Rand#]

        'Stream workbook  
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=SBPubWords_" & ddlYear.SelectedValue & ".xls")
        book.SaveAs(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadCount()
        lblcalc()
    End Sub
End Class
