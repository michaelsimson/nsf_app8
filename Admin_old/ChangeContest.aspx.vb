Imports System
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports NorthSouth.BAL
Imports System.Web.Mail


Namespace VRegistration

    Partial Class ChangeContest
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If LCase(Session("LoggedIn")) <> "true" Then
                Response.Redirect("..\maintest.aspx")
            End If
            If Page.IsPostBack = False Then
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    txtUserId.Text = Trim(Session("LoginEmail"))
                    txtUserId.Enabled = False
                    hlnkMainMenu.NavigateUrl = "../UserFunctions.aspx"
                End If
            End If
        End Sub

        Private Sub btnFindContest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindContest.Click
            Dim connContest As New SqlConnection(Application("ConnectionString"))
            Dim conn1 As New SqlConnection(Application("ConnectionString"))


            Dim prm(2) As SqlParameter
            prm(0) = New SqlParameter
            prm(0).ParameterName = "@EmailID"
            prm(0).Value = Server.HtmlEncode(txtUserId.Text)
            prm(0).Direction = ParameterDirection.Input

            prm(1) = New SqlParameter
            prm(1).ParameterName = "@AutoMemberID"
            prm(1).Direction = ParameterDirection.Output
            prm(1).SqlDbType = SqlDbType.Int

            SqlHelper.ExecuteScalar(conn1, CommandType.StoredProcedure, "usp_GetParentID", prm)
            'Dim intParentID As Int32 = CType(prm(1).Value, Int32)
            ViewState("ParentID") = prm(1).Value

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            Dim IndID As Integer
            Dim SpouseID As Integer
            Dim str As String

            str = "MemberId='" & ViewState("ParentID") & "'"

            Dim objChild As New Child
            Dim dsChild As New DataSet

            objChild.SearchChildWhere(Application("ConnectionString"), dsChild, str)
            ViewState("ChildInfo") = dsChild
            cmd = Nothing
            conn = Nothing

            Dim conn2 As New SqlConnection(Application("ConnectionString"))
            Dim cmd1 As New SqlCommand

            Dim dsContest As New DataSet
            Dim tblConest() As String = {"Contest"}

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ParentID"
            prmArray(0).SqlDbType = SqlDbType.Int
            prmArray(0).Value = ViewState("ParentID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
            prmArray(1).Value = Now.Year
            prmArray(1).Direction = ParameterDirection.Input

            SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContestInCluster", dsContest, tblConest, prmArray)

            ViewState("Contest") = dsContest


            Call DisplayPaidContests()
        End Sub

        Private Sub DisplayPaidContests()
            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim prmArray(2) As SqlParameter
            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}
            Dim dvSelectedContes As DataView
            Dim strToday As Date = Date.Now

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ParentID"
            prmArray(0).SqlDbType = SqlDbType.Int
            prmArray(0).Value = ViewState("ParentID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
            prmArray(1).Value = Now.Year
            prmArray(1).Direction = ParameterDirection.Input
            connContest = New SqlConnection(Application("ConnectionString"))

            'SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)
            SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetChangeCenterContest", dsContestant, tblConestant, prmArray)

            If Not ViewState("SelectedContests") Is Nothing Then
                CType(ViewState("SelectedContests"), ArrayList).Clear()
            End If

            If dsContestant.Tables(0).Rows.Count > 0 Then
                dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
                dgSelectedContests.DataBind()
                dgSelectedContests.Visible = True
                lblError.Visible = False
            Else
                lblError.Visible = True
                lblError.Text = "No Contest registration was found"
                dgSelectedContests.Visible = False
            End If

            connContest = Nothing

        End Sub

        Private Sub dgSelectedContests_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.EditCommand
            Dim i As Integer
            Dim intIndexKey As Integer
            Dim strChapterID As String
            Dim strWhere As String

            intIndexKey = dgSelectedContests.DataKeys(e.Item.ItemIndex)

            DisplayPaidContests()

            For i = 0 To dgSelectedContests.DataKeys.Count - 1
                If (dgSelectedContests.DataKeys(i) = intIndexKey) Then
                    dgSelectedContests.EditItemIndex = i
                    Exit For
                End If
            Next

            Page.DataBind()

        End Sub

        Private Sub dgSelectedContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound

            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    CType(e.Item.FindControl("lblPaymentInfo"), Label).Text = "Payment Info <BR> Amount:US$ " & e.Item.DataItem("Fee") & "<BR>" & _
             "Date Paid :" & e.Item.DataItem("PaymentDate") & "<BR>" & _
             "Reference :" & e.Item.DataItem("PaymentReference") & "<BR>"
                Case ListItemType.EditItem
                    Dim objChapters As New NorthSouth.BAL.Chapter
                    Dim dsChapters As New DataSet
                    Dim StrSql1 As String

                    Dim Product_Code As String = ""
                    Dim ds_Product As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Contestant where childnumber =" & CType(e.Item.FindControl("lblChildID"), Label).Text & " AND ContestYear=" & CType(e.Item.FindControl("lblContestYear"), Label).Text & " AND ContestID is Not null and PaymentReference is null")
                    If ds_Product.Tables(0).Rows.Count > 0 Then
                        For i As Integer = 0 To ds_Product.Tables(0).Rows.Count - 1
                            Product_Code = Product_Code & ds_Product.Tables(0).Rows(i)("ProductCode") + ","
                        Next
                    End If
                    Product_Code = Product_Code.TrimEnd(",")
                    If Product_Code <> "" Then
                        lblError.Visible = True
                        lblError.Text = "Please Check if the Payment has beed made for the Contest " & Product_Code
                        'Exit Sub
                    Else
                        lblError.Text = ""
                    End If

                    StrSql1 = "SELECT CC.ContestDesc, C.ContestID FROM ContestCategory CC INNER JOIN Contest C ON CC.ContestCategoryID = C.ContestCategoryID Inner JOIN Child Ch ON Ch.childnumber = " & CType(e.Item.FindControl("lblChildID"), Label).Text & " WHERE ch.GRADE BETWEEN CC.GradeFrom AND CC.GradeTo AND C.NSFChapterID = " & CType(e.Item.FindControl("lblChapterID"), Label).Text & IIf(Session("EntryToken").ToString.ToUpper() = "PARENT", " AND DATEADD(day,1,C.RegistrationDeadline) >= GETDATE()", "") & " AND C.Contest_Year =" & CType(e.Item.FindControl("lblContestYear"), Label).Text & " AND  C.ProductCode NOT IN (Select ProductCode from Contestant where childnumber =" & CType(e.Item.FindControl("lblChildID"), Label).Text & " AND ContestYear=" & CType(e.Item.FindControl("lblContestYear"), Label).Text & " AND ContestID is Not null)"
                    ' StrSql1 = "SELECT CC.ContestDesc, C.ContestID FROM ContestCategory CC INNER JOIN Contest C ON CC.ContestCategoryID = C.ContestCategoryID WHERE C.NSFChapterID = " & CType(e.Item.FindControl("lblChapterID"), Label).Text & " AND C.RegistrationDeadline >= GETDATE() AND C.Contest_Year = 2010"
                    'objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)
                    dsChapters = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql1)

                    If dsChapters.Tables(0).Rows.Count > 0 Then
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataSource = dsChapters.Tables(0)
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataTextField = dsChapters.Tables(0).Columns("ContestDesc").ToString
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataValueField = dsChapters.Tables(0).Columns("ContestID").ToString
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).DataBind()
                        CType(e.Item.FindControl("ddlNewCenters"), DropDownList).Items.Insert(0, New ListItem("Select Contest", String.Empty))
                    Else
                        Dim tempdrop As DropDownList
                        tempdrop = CType(e.Item.FindControl("ddlNewCenters"), DropDownList)
                        tempdrop.Items.Insert(0, New ListItem("No Contest to Display", String.Empty))
                        tempdrop.Attributes.Add("onclick", "alert('Cannot be changed, since the deadline has passed');")
                    End If
                    'CType(e.Item.FindControl("ddlContestantEdit"), DropDownList).DataSource = ViewState("ChildInfo").Tables(0)
                    'CType(e.Item.FindControl("ddlContestantEdit"), DropDownList).DataValueField = ViewState("ChildInfo").Tables(0).Columns.Item("ChildNumber").ToString
                    'CType(e.Item.FindControl("ddlContestantEdit"), DropDownList).DataTextField = ViewState("ChildInfo").Tables(0).Columns.Item(3).ToString
                    'CType(e.Item.FindControl("ddlContestantEdit"), DropDownList).DataBind()

                    'CType(e.Item.FindControl("ddlContestEdit"), DropDownList).DataSource = ViewState("Contest").Tables(0)
                    'CType(e.Item.FindControl("ddlContestEdit"), DropDownList).DataValueField = ViewState("Contest").Tables(0).Columns.Item("ContestID").ToString
                    'CType(e.Item.FindControl("ddlContestEdit"), DropDownList).DataTextField = ViewState("Contest").Tables(0).Columns.Item("ContestDescription").ToString
                    'CType(e.Item.FindControl("ddlContestEdit"), DropDownList).DataBind()

                    'CType(e.Item.FindControl("ddlContestantEdit"), DropDownList).Items.FindByValue(e.Item.DataItem("ChildNumber")).Selected = True
                    'CType(e.Item.FindControl("ddlContestEdit"), DropDownList).Items.FindByValue(e.Item.DataItem("ContestCode")).Selected = True
            End Select
        End Sub

        Private Sub dgSelectedContests_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.CancelCommand
            dgSelectedContests.EditItemIndex = -1
            Page.DataBind()
            DisplayPaidContests()
        End Sub
        Private Sub dgSelectedContests_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.UpdateCommand
            '################### FERDINE SILVA ########################
            If Convert.ToDateTime(Now).ToString("MM/dd/yyyy") <= CDate(CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text) Then
                If CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue <> String.Empty And CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue <> CType(e.Item.FindControl("lblcontestID"), Label).Text Then
                    Dim flag As Integer = 0
                    Dim strSql As String
                    strSql = "SELECT COUNT(cs.ContestID) FROM Contest AS cs INNER JOIN ContestCategory AS CC ON cs.ContestCategoryID = CC.ContestCategoryID CROSS JOIN Child AS chld WHERE  (chld.GRADE BETWEEN CC.GradeFrom AND CC.GradeTo) AND cs.ContestID =" & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & " AND chld.ChildNumber = " & CType(e.Item.FindControl("lblChildID"), Label).Text
                    flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
                    If flag > 0 Then
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(contestant_ID) from contestant where BadgeNumber is not NUll and ContestID in (" & CType(e.Item.FindControl("lblcontestID"), Label).Text & "," & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & ")") = 0 Then
                            updateitem(e)
                        Else
                            lblMessage.Visible = True
                            lblMessage.Text = "Badge Numbers were issued you can't Change."
                        End If
                        '********* cutted from here ***************
                    Else
                        lblMessage.Visible = True
                        lblMessage.Text = "Child's Grade is not eligible to Participate in this Contest."
                    End If
                Else
                    lblMessage.Visible = True
                    lblMessage.Text = "Please Select Valid Contest"

                End If
            Else
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Sorry! Registration Deadline was passed."
                ElseIf Session("roleid") = 1 Then
                    'If RoleID is 1, if the deadline has passed (or the if the test fails)

                    If CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue <> String.Empty And CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue <> CType(e.Item.FindControl("lblcontestID"), Label).Text Then
                        Dim flag As Integer = 0
                        Dim strSql As String
                        strSql = "SELECT COUNT(cs.ContestID) FROM Contest AS cs INNER JOIN ContestCategory AS CC ON cs.ContestCategoryID = CC.ContestCategoryID CROSS JOIN Child AS chld WHERE  (chld.GRADE BETWEEN CC.GradeFrom AND CC.GradeTo) AND cs.ContestID =" & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & " AND chld.ChildNumber = " & CType(e.Item.FindControl("lblChildID"), Label).Text
                        flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
                        If flag > 0 Then
                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(contestant_ID) from contestant where BadgeNumber is not NUll and ContestID in (" & CType(e.Item.FindControl("lblcontestID"), Label).Text & "," & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & ")") = 0 Then
                                Dim f As System.Web.UI.WebControls.DataGridCommandEventArgs = e
                                Cache("CommandEventArgs") = f
                                tradmin.Visible = True
                            Else
                                lblMessage.Visible = True
                                lblMessage.Text = "Badge Numbers were issued you can't Change."
                            End If
                            '********* cutted from here ***************
                        Else
                            lblMessage.Visible = True
                            lblMessage.Text = "Child's Grade is not eligible to Participate in this Contest."
                        End If
                    Else
                        lblMessage.Visible = True
                        lblMessage.Text = "Please Select Valid Contest"

                    End If

                Else
                    lblMessage.Visible = True
                    lblMessage.Text = "Sorry! Registration Deadline was passed."
                End If
            End If
            If tradmin.Visible = False Then
                dgSelectedContests.EditItemIndex = -1
                ViewState("SelectedContests") = Nothing
                DisplayPaidContests()
            End If
        End Sub
        Protected Sub btnAdminYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdminYes.Click
            tradmin.Visible = False
            updateitem(Cache("CommandEventArgs"))
            Cache.Remove("CommandEventArgs")
        End Sub

        Protected Sub btnAdminNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdminNo.Click
            tradmin.Visible = False
            dgSelectedContests.EditItemIndex = -1
            DisplayPaidContests()
        End Sub
        Private Sub updateitem(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Dim strSql As String
            strSql = "SELECT COUNT(cc.RegionalFee) FROM ContestCategory AS cc INNER JOIN Contest AS c ON cc.ContestCategoryID = c.ContestCategoryID WHERE c.ContestID = " & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & " AND cc.RegionalFee <= (SELECT     cc.RegionalFee  FROM ContestCategory AS cc INNER JOIN Contest AS c ON cc.ContestCategoryID = c.ContestCategoryID WHERE c.ContestID = " & CType(e.Item.FindControl("lblcontestID"), Label).Text & ")"
            Dim flag1 As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            If flag1 > 0 Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(contestant_ID) from contestant where BadgeNumber is not NUll and ContestID in (" & CType(e.Item.FindControl("lblcontestID"), Label).Text & "," & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & ")") > 0 Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Badge Numbers were issued you can't Change."
                    Exit Sub
                End If
                Dim ContestCategoryID, ProductGroupId, ProductId As Integer
                Dim ProductGroupCode, ProductCode As String
                Dim intIndexKey As Integer
                intIndexKey = dgSelectedContests.DataKeys(e.Item.ItemIndex)
                'Retriving values from table
                strSql = "SELECT C.ContestCategoryID, CC.ProductGroupId, CC.ProductGroupCode, C.ProductId, C.ProductCode FROM ContestCategory AS CC INNER JOIN Contest AS C ON CC.ContestCategoryID = C.ContestCategoryID WHERE C.ContestID = " & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
                While readr.Read()
                    ContestCategoryID = readr("ContestCategoryID")
                    ProductGroupId = readr("ProductGroupId")
                    ProductGroupCode = readr("ProductGroupCode")
                    ProductId = readr("ProductId")
                    ProductCode = readr("ProductCode")
                End While
                Dim j As Integer = 0
                strSql = "update contestant set ContestCode = " & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & ", ContestCategoryID =" & ContestCategoryID & ", ContestID=" & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & ",ProductGroupId = " & ProductGroupId & " , ProductGroupCode='" & ProductGroupCode & "', ProductId=" & ProductId & ", ProductCode ='" & ProductCode & "', ModifiedDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', ModifiedBy=" & Session("LoginID") & " where contestant_id=" & intIndexKey
                j = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                If j > 0 Then
                    Dim strs As String = "Insert INTO ContestChange(parentId,ContestYear,F_ChapterID,F_ChildNumber,F_ContestID,F_ProductGroupID,F_ProductGroupCode,F_ProductID,F_ProductCode,T_ChapterID,T_ChildNumber,T_ContestID,T_ProductGroupID,T_ProductGroupCode,T_ProductID,T_ProductCode,Contestant_ID,EventID,EventCode,CreatedDate, CreatedBy) values("
                    strs = strs & CType(e.Item.FindControl("lblMemberID"), Label).Text & "," & CType(e.Item.FindControl("lblContestYear"), Label).Text & "," & CType(e.Item.FindControl("lblChapterID"), Label).Text & "," & CType(e.Item.FindControl("lblChildID"), Label).Text & "," & CType(e.Item.FindControl("lblcontestID"), Label).Text & "," & CType(e.Item.FindControl("lblProductGroupId"), Label).Text & ",'" & CType(e.Item.FindControl("lblProductGroupCode"), Label).Text & "'," & CType(e.Item.FindControl("lblProductID"), Label).Text & ",'" & CType(e.Item.FindControl("lblProductCode"), Label).Text
                    strs = strs & "'," & CType(e.Item.FindControl("lblChapterID"), Label).Text & "," & CType(e.Item.FindControl("lblChildID"), Label).Text & "," & CType(e.Item.FindControl("ddlNewCenters"), DropDownList).SelectedValue & "," & ProductGroupId & ",'" & ProductGroupCode & "'," & ProductId & ",'" & ProductCode & "'," & intIndexKey & "," & CType(e.Item.FindControl("lblEventID"), Label).Text & ",'" & CType(e.Item.FindControl("lblEventCode"), Label).Text & "','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "'," & Session("LoginID") & ")"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strs)
                    lblMessage.Text = "Record Updated Successfully"
                    lblMessage.Visible = True
                End If
            Else
                lblMessage.Visible = True
                lblMessage.Text = "Fees for the selected contest is more than the old."
            End If
            dgSelectedContests.EditItemIndex = -1
            ViewState("SelectedContests") = Nothing
            DisplayPaidContests()
        End Sub
    End Class

End Namespace

