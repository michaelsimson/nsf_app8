﻿
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Reflection
Imports NativeExcel
Partial Class Admin_VocabBeeCriteria
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("../maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            loadyear()
            LoadCount()
        End If
        lblcalc()
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year + 2
            ddlYear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlYear.Items(0).Selected = True
    End Sub

    Private Sub lblcalc()
        Try
            lbltotalSum.Text = Val(lbl11.Text) + Val(lbl12.Text) + Val(lbl21.Text) + Val(lbl22.Text) + Val(lbl31.Text) + Val(lbl32.Text)
            lblrsum.Text = Val(txt11r.Text) + Val(txt12r.Text) + Val(txt21r.Text) + Val(txt22r.Text) + Val(txt31r.Text) + Val(txt32r.Text)
            lblrrsum.Text = Val(txt11rr.Text) + Val(txt12rr.Text) + Val(txt21rr.Text) + Val(txt22rr.Text) + Val(txt31rr.Text) + Val(txt32rr.Text)

            lblnsum.Text = Val(txt11n.Text) + Val(txt12n.Text) + Val(txt21n.Text) + Val(txt22n.Text) + Val(txt31n.Text) + Val(txt32n.Text)
            lblnrsum.Text = Val(txt11nr.Text) + Val(txt12nr.Text) + Val(txt21nr.Text) + Val(txt22nr.Text) + Val(txt31nr.Text) + Val(txt32nr.Text)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadCount()
        ' display the number of words
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from VBPubCriteria where ContestYear=" & ddlYear.SelectedValue & "") > 0 Then
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,Reg_Reserved,[National],Nat_Reserved,RegContest, NatContest,RegResvContest,NatResvContest from VBPubCriteria where [level] = 1 and SubLevel=1")
            'lbl11.Text = ds.Tables(0).Rows(0)(0)
            txt11r.Text = ds.Tables(0).Rows(0)(1)
            txt11rr.Text = ds.Tables(0).Rows(0)(2)
            txt11n.Text = ds.Tables(0).Rows(0)(3)
            txt11nr.Text = ds.Tables(0).Rows(0)(4)
            ddl11r.SelectedIndex = ddl11r.Items.IndexOf(ddl11r.Items.FindByText(ds.Tables(0).Rows(0)(5).ToString().Trim()))
            ddl11n.SelectedIndex = ddl11n.Items.IndexOf(ddl11n.Items.FindByText(ds.Tables(0).Rows(0)(6).ToString().Trim()))
            ddl11rr.SelectedIndex = ddl11rr.Items.IndexOf(ddl11rr.Items.FindByText(ds.Tables(0).Rows(0)(7).ToString().Trim()))
            ddl11nr.SelectedIndex = ddl11nr.Items.IndexOf(ddl11nr.Items.FindByText(ds.Tables(0).Rows(0)(8).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,Reg_Reserved,[National],Nat_Reserved,RegContest, NatContest,RegResvContest,NatResvContest from VBPubCriteria where [level] = 1 and SubLevel=2")
            'lbl12.Text = ds.Tables(0).Rows(0)(0)
            txt12r.Text = ds.Tables(0).Rows(0)(1)
            txt12rr.Text = ds.Tables(0).Rows(0)(2)
            txt12n.Text = ds.Tables(0).Rows(0)(3)
            txt12nr.Text = ds.Tables(0).Rows(0)(4)
            ddl12r.SelectedIndex = ddl12r.Items.IndexOf(ddl12r.Items.FindByText(ds.Tables(0).Rows(0)(5).ToString().Trim()))
            ddl12n.SelectedIndex = ddl12n.Items.IndexOf(ddl12n.Items.FindByText(ds.Tables(0).Rows(0)(6).ToString().Trim()))
            ddl12rr.SelectedIndex = ddl12rr.Items.IndexOf(ddl12rr.Items.FindByText(ds.Tables(0).Rows(0)(7).ToString().Trim()))
            ddl12nr.SelectedIndex = ddl12nr.Items.IndexOf(ddl12nr.Items.FindByText(ds.Tables(0).Rows(0)(8).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,Reg_Reserved,[National],Nat_Reserved,RegContest, NatContest,RegResvContest,NatResvContest from VBPubCriteria where [level] = 2 and SubLevel=1")
            'lbl21.Text = ds.Tables(0).Rows(0)(0)
            txt21r.Text = ds.Tables(0).Rows(0)(1)
            txt21rr.Text = ds.Tables(0).Rows(0)(2)
            txt21n.Text = ds.Tables(0).Rows(0)(3)
            txt21nr.Text = ds.Tables(0).Rows(0)(4)
            ddl21r.SelectedIndex = ddl21r.Items.IndexOf(ddl21r.Items.FindByText(ds.Tables(0).Rows(0)(5).ToString().Trim()))
            ddl21n.SelectedIndex = ddl21n.Items.IndexOf(ddl21n.Items.FindByText(ds.Tables(0).Rows(0)(6).ToString().Trim()))
            ddl21rr.SelectedIndex = ddl21rr.Items.IndexOf(ddl21rr.Items.FindByText(ds.Tables(0).Rows(0)(7).ToString().Trim()))
            ddl21nr.SelectedIndex = ddl21nr.Items.IndexOf(ddl21nr.Items.FindByText(ds.Tables(0).Rows(0)(8).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,Reg_Reserved,[National],Nat_Reserved,RegContest, NatContest,RegResvContest,NatResvContest from VBPubCriteria where [level] = 2 and SubLevel=2")
            'lbl22.Text = ds.Tables(0).Rows(0)(0)
            txt22r.Text = ds.Tables(0).Rows(0)(1)
            txt22rr.Text = ds.Tables(0).Rows(0)(2)
            txt22n.Text = ds.Tables(0).Rows(0)(3)
            txt22nr.Text = ds.Tables(0).Rows(0)(4)
            ddl22r.SelectedIndex = ddl22r.Items.IndexOf(ddl22r.Items.FindByText(ds.Tables(0).Rows(0)(5).ToString().Trim()))
            ddl22n.SelectedIndex = ddl22n.Items.IndexOf(ddl22n.Items.FindByText(ds.Tables(0).Rows(0)(6).ToString().Trim()))
            ddl22rr.SelectedIndex = ddl22rr.Items.IndexOf(ddl22rr.Items.FindByText(ds.Tables(0).Rows(0)(7).ToString().Trim()))
            ddl22nr.SelectedIndex = ddl22nr.Items.IndexOf(ddl22nr.Items.FindByText(ds.Tables(0).Rows(0)(8).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,Reg_Reserved,[National],Nat_Reserved,RegContest, NatContest,RegResvContest,NatResvContest from VBPubCriteria where [level] = 3 and SubLevel=1")
            'lbl31.Text = ds.Tables(0).Rows(0)(0)
            txt31r.Text = ds.Tables(0).Rows(0)(1)
            txt31rr.Text = ds.Tables(0).Rows(0)(2)
            txt31n.Text = ds.Tables(0).Rows(0)(3)
            txt31nr.Text = ds.Tables(0).Rows(0)(4)
            ddl31r.SelectedIndex = ddl31r.Items.IndexOf(ddl31r.Items.FindByText(ds.Tables(0).Rows(0)(5).ToString().Trim()))
            ddl31n.SelectedIndex = ddl31n.Items.IndexOf(ddl31n.Items.FindByText(ds.Tables(0).Rows(0)(6).ToString().Trim()))
            ddl31rr.SelectedIndex = ddl31rr.Items.IndexOf(ddl31rr.Items.FindByText(ds.Tables(0).Rows(0)(7).ToString().Trim()))
            ddl31nr.SelectedIndex = ddl31nr.Items.IndexOf(ddl31nr.Items.FindByText(ds.Tables(0).Rows(0)(8).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,Reg_Reserved,[National],Nat_Reserved,RegContest, NatContest,RegResvContest,NatResvContest from VBPubCriteria where [level] = 3 and SubLevel=2")
            ' lbl32.Text = ds.Tables(0).Rows(0)(0)
            txt32r.Text = ds.Tables(0).Rows(0)(1)
            txt32rr.Text = ds.Tables(0).Rows(0)(2)
            txt32n.Text = ds.Tables(0).Rows(0)(3)
            txt32nr.Text = ds.Tables(0).Rows(0)(4)
            ddl32r.SelectedIndex = ddl32r.Items.IndexOf(ddl32r.Items.FindByText(ds.Tables(0).Rows(0)(5).ToString().Trim()))
            ddl32n.SelectedIndex = ddl32n.Items.IndexOf(ddl32n.Items.FindByText(ds.Tables(0).Rows(0)(6).ToString().Trim()))
            ddl32rr.SelectedIndex = ddl32rr.Items.IndexOf(ddl32rr.Items.FindByText(ds.Tables(0).Rows(0)(7).ToString().Trim()))
            ddl32nr.SelectedIndex = ddl32nr.Items.IndexOf(ddl32nr.Items.FindByText(ds.Tables(0).Rows(0)(8).ToString().Trim()))
            btnExport.Visible = True
        Else
            btnGenerate.Enabled = False
        End If
        lbl11.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from VocabWords_New  where [Level]=1 and [Sub-Level] =1")
        lbl12.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from VocabWords_New  where [Level]=1 and [Sub-Level] =2")

        lbl21.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from VocabWords_New  where [Level]=2 and [Sub-Level] =1")
        lbl22.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from VocabWords_New  where [Level]=2 and [Sub-Level] =2")

        lbl31.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from VocabWords_New  where [Level]=3 and [Sub-Level] =1")
        lbl32.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from VocabWords_New  where [Level]=3 and [Sub-Level] =2")


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Sum of JSB and SSB in reg and Nat must be 1000
        'Sum of Reg & Nat must be less than total
        Dim JVBsumR, IVBSumR, JVBSumN, IVBSumN As Integer
        JVBsumR = 0
        IVBSumR = 0
        JVBSumN = 0
        IVBSumN = 0
        Try

            If ddl11r.SelectedValue = "JVB" Then
                JVBsumR = Val(txt11r.Text)
            Else
                IVBSumR = Val(txt11r.Text)
            End If
            If ddl12r.SelectedValue = "JVB" Then
                JVBsumR = JVBsumR + Val(txt12r.Text)
            Else
                IVBSumR = IVBSumR + Val(txt12r.Text)
            End If
            If ddl21r.SelectedValue = "JVB" Then
                JVBsumR = JVBsumR + Val(txt21r.Text)
            Else
                IVBSumR = IVBSumR + Val(txt21r.Text)
            End If
            If ddl22r.SelectedValue = "JVB" Then
                JVBsumR = JVBsumR + Val(txt22r.Text)
            Else
                IVBSumR = IVBSumR + Val(txt22r.Text)
            End If
            If ddl31r.SelectedValue = "JVB" Then
                JVBsumR = JVBsumR + Val(txt31r.Text)
            Else
                IVBSumR = IVBSumR + Val(txt31r.Text)
            End If
            If ddl32r.SelectedValue = "JVB" Then
                JVBsumR = JVBsumR + Val(txt32r.Text)
            Else
                IVBSumR = IVBSumR + Val(txt32r.Text)
            End If


            'Nat
            If ddl11n.SelectedValue = "JVB" Then
                JVBSumN = Val(txt11n.Text)
            Else
                IVBSumN = Val(txt11n.Text)
            End If
            If ddl12n.SelectedValue = "JVB" Then
                JVBSumN = JVBSumN + Val(txt12n.Text)
            Else
                IVBSumN = IVBSumN + Val(txt12n.Text)
            End If
            If ddl21n.SelectedValue = "JVB" Then
                JVBSumN = JVBSumN + Val(txt21n.Text)
            Else
                IVBSumN = IVBSumN + Val(txt21n.Text)
            End If
            If ddl22n.SelectedValue = "JVB" Then
                JVBSumN = JVBSumN + Val(txt22n.Text)
            Else
                IVBSumN = IVBSumN + Val(txt22n.Text)
            End If
            If ddl31n.SelectedValue = "JVB" Then
                JVBSumN = JVBSumN + Val(txt31n.Text)
            Else
                IVBSumN = IVBSumN + Val(txt31n.Text)
            End If
            If ddl32n.SelectedValue = "JVB" Then
                JVBSumN = JVBSumN + Val(txt32n.Text)
            Else
                IVBSumN = IVBSumN + Val(txt32n.Text)
            End If
            If JVBSumN < 1000 Then
                lblerr.Text = " JVB National is less than 1000"
                Exit Sub
            End If
            If JVBsumR < 1000 Then
                lblerr.Text = " JVB Regional is less than 1000"
                Exit Sub
            End If
            If IVBSumN < 1000 Then
                lblerr.Text = " IVB National is less than 1000"
                Exit Sub
            End If
            If IVBSumR < 1000 Then
                lblerr.Text = " IVB Regional is less than 1000"
                Exit Sub
            End If

            If Convert.ToInt32(lbl11.Text) < (Val(txt11r.Text) + Val(txt11n.Text) + Val(txt11rr.Text) + Val(txt11nr.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 1 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl12.Text) < (Val(txt12r.Text) + Val(txt12n.Text) + Val(txt12rr.Text) + Val(txt12nr.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 1 Sub Level 2  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl21.Text) < (Val(txt21r.Text) + Val(txt21n.Text) + Val(txt21rr.Text) + Val(txt21nr.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 2 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl22.Text) < (Val(txt22r.Text) + Val(txt22n.Text) + Val(txt22rr.Text) + Val(txt22nr.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 2 Sub Level 2  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl31.Text) < (Val(txt31r.Text) + Val(txt31n.Text) + Val(txt31rr.Text) + Val(txt31nr.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 3 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl32.Text) < (Val(txt32r.Text) + Val(txt32n.Text) + Val(txt32rr.Text) + Val(txt32nr.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 3 Sub Level 2  "
                Exit Sub
            End If

        Catch ex As Exception
            lblerr.Text = "Please enter all Values & Enter Numeric values only"
            Exit Sub
        End Try
        lblerr.Text = ""
        Dim strSQl As String
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from VBPubCriteria where ContestYear=" & ddlYear.SelectedValue & "") < 1 Then
            strSQl = "INSERT INTO VBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National],RegResvContest,Reg_Reserved,  NatResvContest, Nat_Reserved, ContestYear, CreateDate, CreatedBy) VALUES (1,1," & lbl11.Text & ",'" & ddl11r.SelectedValue & "'," & Val(txt11r.Text) & ",'" & ddl11n.SelectedValue & "'," & Val(txt11n.Text) & ",'" & ddl11rr.SelectedValue & "'," & Val(txt11rr.Text) & ",'" & ddl11nr.SelectedValue & "'," & Val(txt11nr.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO VBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National],RegResvContest,Reg_Reserved,  NatResvContest, Nat_Reserved, ContestYear, CreateDate, CreatedBy) VALUES (1,2," & lbl12.Text & ",'" & ddl12r.SelectedValue & "'," & Val(txt12r.Text) & ",'" & ddl12n.SelectedValue & "'," & Val(txt12n.Text) & ",'" & ddl12rr.SelectedValue & "'," & Val(txt12rr.Text) & ",'" & ddl12nr.SelectedValue & "'," & Val(txt12nr.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO VBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National],RegResvContest,Reg_Reserved,  NatResvContest, Nat_Reserved, ContestYear, CreateDate, CreatedBy) VALUES (2,1," & lbl21.Text & ",'" & ddl21r.SelectedValue & "'," & Val(txt21r.Text) & ",'" & ddl21n.SelectedValue & "'," & Val(txt21n.Text) & ",'" & ddl21rr.SelectedValue & "'," & Val(txt21rr.Text) & ",'" & ddl21nr.SelectedValue & "'," & Val(txt21nr.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO VBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National],RegResvContest,Reg_Reserved,  NatResvContest, Nat_Reserved, ContestYear, CreateDate, CreatedBy) VALUES (2,2," & lbl22.Text & ",'" & ddl22r.SelectedValue & "'," & Val(txt22r.Text) & ",'" & ddl22n.SelectedValue & "'," & Val(txt22n.Text) & ",'" & ddl22rr.SelectedValue & "'," & Val(txt22rr.Text) & ",'" & ddl22nr.SelectedValue & "'," & Val(txt22nr.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO VBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National],RegResvContest,Reg_Reserved,  NatResvContest, Nat_Reserved, ContestYear, CreateDate, CreatedBy) VALUES (3,1," & lbl31.Text & ",'" & ddl31r.SelectedValue & "'," & Val(txt31r.Text) & ",'" & ddl31n.SelectedValue & "'," & Val(txt31n.Text) & ",'" & ddl31rr.SelectedValue & "'," & Val(txt31rr.Text) & ",'" & ddl31nr.SelectedValue & "'," & Val(txt31nr.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO VBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National],RegResvContest,Reg_Reserved,  NatResvContest, Nat_Reserved, ContestYear, CreateDate, CreatedBy) VALUES (3,2," & lbl32.Text & ",'" & ddl32r.SelectedValue & "'," & Val(txt32r.Text) & ",'" & ddl32n.SelectedValue & "'," & Val(txt32n.Text) & ",'" & ddl32rr.SelectedValue & "'," & Val(txt32rr.Text) & ",'" & ddl32nr.SelectedValue & "'," & Val(txt32nr.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Values inserted successfully"
        Else
            strSQl = "UPDATE VBPubCriteria SET Total=" & Val(lbl11.Text) & ", Regional=" & Val(txt11r.Text) & ", RegContest='" & ddl11r.SelectedValue & "', [National]=" & Val(txt11n.Text) & ", NatContest='" & ddl11n.SelectedValue & "',Reg_Reserved=" & Val(txt11rr.Text) & ", RegResvContest='" & ddl11rr.SelectedValue & "', Nat_Reserved=" & Val(txt11nr.Text) & ", NatResvContest='" & ddl11nr.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 1 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE VBPubCriteria SET Total=" & Val(lbl12.Text) & ", Regional=" & Val(txt12r.Text) & ", RegContest='" & ddl12r.SelectedValue & "', [National]=" & Val(txt12n.Text) & ", NatContest='" & ddl12n.SelectedValue & "',Reg_Reserved=" & Val(txt12rr.Text) & ", RegResvContest='" & ddl12rr.SelectedValue & "', Nat_Reserved=" & Val(txt12nr.Text) & ", NatResvContest='" & ddl12nr.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 1 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE VBPubCriteria SET Total=" & Val(lbl21.Text) & ", Regional=" & Val(txt21r.Text) & ", RegContest='" & ddl21r.SelectedValue & "', [National]=" & Val(txt21n.Text) & ", NatContest='" & ddl21n.SelectedValue & "',Reg_Reserved=" & Val(txt21rr.Text) & ", RegResvContest='" & ddl21rr.SelectedValue & "', Nat_Reserved=" & Val(txt21nr.Text) & ", NatResvContest='" & ddl21nr.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 2 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE VBPubCriteria SET Total=" & Val(lbl22.Text) & ", Regional=" & Val(txt22r.Text) & ", RegContest='" & ddl22r.SelectedValue & "', [National]=" & Val(txt22n.Text) & ", NatContest='" & ddl22n.SelectedValue & "',Reg_Reserved=" & Val(txt22rr.Text) & ", RegResvContest='" & ddl22rr.SelectedValue & "', Nat_Reserved=" & Val(txt22nr.Text) & ", NatResvContest='" & ddl22nr.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 2 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE VBPubCriteria SET Total=" & Val(lbl31.Text) & ", Regional=" & Val(txt31r.Text) & ", RegContest='" & ddl31r.SelectedValue & "', [National]=" & Val(txt31n.Text) & ", NatContest='" & ddl31n.SelectedValue & "',Reg_Reserved=" & Val(txt31rr.Text) & ", RegResvContest='" & ddl31rr.SelectedValue & "', Nat_Reserved=" & Val(txt31nr.Text) & ", NatResvContest='" & ddl31nr.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 3 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE VBPubCriteria SET Total=" & Val(lbl32.Text) & ", Regional=" & Val(txt32r.Text) & ", RegContest='" & ddl32r.SelectedValue & "', [National]=" & Val(txt32n.Text) & ", NatContest='" & ddl32n.SelectedValue & "',Reg_Reserved=" & Val(txt32rr.Text) & ", RegResvContest='" & ddl32rr.SelectedValue & "', Nat_Reserved=" & Val(txt32nr.Text) & ", NatResvContest='" & ddl32nr.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 3 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Values updated successfully"
        End If
        btnGenerate.Enabled = True
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, "usp_createVBTempTable")
        Dim dstemp, dsword As DataSet
        dstemp = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select VBPubTempID from  VBPubTemp  order by Randvalue")
        dsword = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select word from VocabWords_New ")
        Dim i As Integer
        For i = 0 To dsword.Tables(0).Rows.Count - 1
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE VocabWords_New  set Random = " & dstemp.Tables(0).Rows(i)(0) & ",Regional=Null , [national]=Null,Reg_Reserved=Null,National_Reserved=Null where word='" & dsword.Tables(0).Rows(i)(0).ToString.Replace("'", "''") & "'")
            Catch ex As Exception
                Response.Write(ex.ToString() & "<br>")
                Response.Write("UPDATE VocabWords_New  set Random = " & dstemp.Tables(0).Rows(i)(0) & ",Regional=Null , [national]=Null,Reg_Reserved=Null,National_Reserved=Null where word='" & dsword.Tables(0).Rows(i)(0) & "'")
                Exit Sub
            End Try
        Next
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "drop table VBPubTemp")
        dsword = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select [Regional],[National],Reg_Reserved, Nat_Reserved, [level],sublevel from VBPubCriteria where ContestYeaR=" & ddlYear.SelectedValue & "")
        Dim sqlstr As String
        For i = 0 To dsword.Tables(0).Rows.Count - 1
            sqlstr = ""
            sqlstr = sqlstr & "Update VocabWords_New  set   Regional='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("Regional") & " word from VocabWords_New  where [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null order by Random) AND  Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null  and [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & "; "
            sqlstr = sqlstr & "Update VocabWords_New  set [national]='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("National") & " word from VocabWords_New  where [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null order by Random) AND  Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null and [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & ";"
            sqlstr = sqlstr & "Update VocabWords_New  set   Reg_Reserved='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("Reg_Reserved") & " word from VocabWords_New  where [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null order by Random) AND  Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null  and [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & "; "
            sqlstr = sqlstr & "Update VocabWords_New  set National_Reserved='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("Nat_Reserved") & " word from VocabWords_New  where [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null order by Random) AND  Regional  is null and [national] is Null and Reg_Reserved is Null and National_Reserved is Null and [Level]= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level] =" & dsword.Tables(0).Rows(i)("sublevel") & ";"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
            ' Response.Write(sqlstr & "<br><br>")
        Next
        lblerr.Text = "Generated Successfully"
        btnExport.Visible = True
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Create workbook
        Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim i As Integer
        Dim j As Integer = 0
        Dim ds As DataSet
        Dim sheet As IWorksheet = book.Worksheets.Add()
        sheet.Cells("A2").Value = "JVB_REG_WORDS"
        sheet.Cells("B2").Value = "Level"
        sheet.Cells("C2").Value = "SubLevel"
        sheet.Name = "JVB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from VocabWords_New  w Inner Join VBPubCriteria S ON w.[Level]= S.Level AND w.[Sub-Level] = S.SubLevel where   w.Regional='Y' and S.RegContest='JVB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.Random")

        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet1 As IWorksheet = book.Worksheets.Add()
        sheet1.Cells("A2").Value = "JVB_NAT_WORDS"
        sheet1.Cells("B2").Value = "Level"
        sheet1.Cells("C2").Value = "SubLevel"
        sheet1.Name = "JVB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from VocabWords_New  w Inner Join VBPubCriteria S ON w.[Level]= S.Level AND w.[Sub-Level] = S.SubLevel where   w.[national]='Y' and S.NatContest='JVB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.Random")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet1.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet1.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet1.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet2 As IWorksheet = book.Worksheets.Add()
        sheet2.Cells("A2").Value = "IVB_REG_WORDS"
        sheet2.Cells("B2").Value = "Level"
        sheet2.Cells("C2").Value = "SubLevel"
        sheet2.Name = "IVB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from VocabWords_New  w Inner Join VBPubCriteria S ON w.[Level]= S.Level AND w.[Sub-Level] = S.SubLevel where   w.Regional='Y' and S.RegContest='IVB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.Random")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet2.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet2.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet2.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet3 As IWorksheet = book.Worksheets.Add()
        sheet3.Cells("A2").Value = "IVB_NAT_WORDS"
        sheet3.Cells("B2").Value = "Level"
        sheet3.Cells("C2").Value = "SubLevel"
        sheet3.Name = "IVB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from VocabWords_New  w Inner Join VBPubCriteria S ON w.[Level]= S.Level AND w.[Sub-Level] = S.SubLevel where   w.[national]='Y' and S.NatContest='IVB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.Random")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet3.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet3.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet3.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next

        'Stream workbook  
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=VBPubWords_" & ddlYear.SelectedValue & ".xls")
        book.SaveAs(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadCount()
        lblcalc()
    End Sub
End Class
