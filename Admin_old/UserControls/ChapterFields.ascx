<%@ Control Language="C#" ClassName="ChapterFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">ChapterCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChapterCode" Text='<%# Bind("ChapterCode") %>' MaxLength="35"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataChapterCode" runat="server" Display="Dynamic" ControlToValidate="dataChapterCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
					<asp:CustomValidator ID="ChapterCodeValidator" runat="server"
                          ControlToValidate="dataChapterCode"
                          Display="Static"
                          Font-Size="8pt" foreColor ="blue"
                          ErrorMessage = "Duplicate Chapter Code">
                     </asp:CustomValidator>					
				</td>
			</tr>				
			<tr>
				<td class="literal">Name:</td>
				<td>
					<asp:TextBox runat="server" ID="dataName" Text='<%# Bind("Name") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">State:</td>
				<td>
					<asp:TextBox runat="server" ID="dataState" Text='<%# Bind("State") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">City:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCity" Text='<%# Bind("City") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Status:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataStatus" DataSourceID="StatusStatusDataSource" DataTextField="DataText" DataValueField="DataValue" SelectedValue='<%# Bind("Status") %>'></asp:DropDownList>
					<data:StatusDataSource ID="StatusStatusDataSource" runat="server"
 						SelectMethod="GetByTableID" 
 					>
 					
 					<Parameters>
 
 					<data:DataParameter Name="TableID" Type="int32" DefaultValue='5' DataSourceID="LookUpCodesDataSource1" />
 					</Parameters>
					</data:StatusDataSource>
					<data:LookUpCodesDataSource ID="LookUpCodesDataSource1" runat="server"
 						SelectMethod="GetAll" 
 					>
					</data:LookUpCodesDataSource>
				</td>
			
			</tr>				
			<tr>
				<td class="literal">ZoneCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataZoneCode" AutoPostBack="true" DataSourceID="ZoneCodeZoneDataSource" DataTextField="ZoneCode" DataValueField="ZoneCode" SelectedValue='<%# Bind("ZoneCode") %>'></asp:DropDownList>
					<data:ZoneDataSource ID="ZoneCodeZoneDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ZoneDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ZoneId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZoneId"  Text='<%# Bind("ZoneId") %>'></asp:TextBox>
				</td>
			</tr>				
			
			<tr>
				<td class="literal">ClusterCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataClusterCode" AutoPostBack="true" DataSourceID="ClusterCodeClusterDataSource" DataTextField="ClusterCode" DataValueField="ClusterCode" SelectedValue='<%# Bind("ClusterCode") %>'></asp:DropDownList>
					<data:ClusterDataSource ID="ClusterCodeClusterDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ClusterDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ClusterId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataClusterId"  Text='<%# Bind("ClusterId") %>'></asp:TextBox>
				</td>
			</tr>				

			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


