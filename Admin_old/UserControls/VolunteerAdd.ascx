<%@ Control Language="C#" ClassName="VolunteerAdd" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">MemberId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMemberId" Text='<%# Request.QueryString["Id"] %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataMemberId" runat="server" Display="Dynamic" ControlToValidate="dataMemberId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataMemberId" runat="server" Display="Dynamic" ControlToValidate="dataMemberId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">RoleId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataRoleId" DataSourceID="RoleIdRoleDataSource" DataTextField="RoleCode" DataValueField="RoleId" SelectedValue='<%# Bind("RoleId") %>'></asp:DropDownList>
					<data:RoleDataSource ID="RoleIdRoleDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:RoleDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">RoleCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataRoleCode" DataSourceID="RoleCodeRoleDataSource" DataTextField="RoleCode" DataValueField="RoleCode" SelectedValue='<%# Bind("RoleCode") %>'></asp:DropDownList>
					<data:RoleDataSource ID="RoleCodeRoleDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:RoleDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">TeamLead:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataTeamLead" DataTextField="TeamLead" DataValueField='<%# Bind("TeamLead") %>'   AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventYear:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventYear" Text='<%# Bind("EventYear") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataEventYear" runat="server" Display="Dynamic" ControlToValidate="dataEventYear" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventId" Text='<%# Bind("EventId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventCode" Text='<%# Bind("EventCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventCode" runat="server" Display="Dynamic" ControlToValidate="dataEventCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChapterId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChapterId" Text='<%# Bind("ChapterId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataChapterId" runat="server" Display="Dynamic" ControlToValidate="dataChapterId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataChapterId" runat="server" Display="Dynamic" ControlToValidate="dataChapterId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChapterCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChapterCode" Text='<%# Bind("ChapterCode") %>' MaxLength="35"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataChapterCode" runat="server" Display="Dynamic" ControlToValidate="dataChapterCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">National:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataNational" DataValueField='<%# Bind("National") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">ZoneId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZoneId" Text='<%# Bind("ZoneId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataZoneId" runat="server" Display="Dynamic" ControlToValidate="dataZoneId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataZoneId" runat="server" Display="Dynamic" ControlToValidate="dataZoneId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ZoneCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZoneCode" Text='<%# Bind("ZoneCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataZoneCode" runat="server" Display="Dynamic" ControlToValidate="dataZoneCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ClusterId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataClusterId" Text='<%# Bind("ClusterId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataClusterId" runat="server" Display="Dynamic" ControlToValidate="dataClusterId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataClusterId" runat="server" Display="Dynamic" ControlToValidate="dataClusterId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ClusterCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataClusterCode" Text='<%# Bind("ClusterCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataClusterCode" runat="server" Display="Dynamic" ControlToValidate="dataClusterCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Finals:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataFinals" DataValueField='<%# Bind("Finals") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">IndiaChapter:</td>
				<td>
					<asp:TextBox runat="server" ID="dataIndiaChapter" Text='<%# Bind("IndiaChapter") %>' MaxLength="35"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupID" Text='<%# Bind("ProductGroupID") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupID" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupID" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductGroupID" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupCode" Text='<%# Bind("ProductGroupCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupCode" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductId" Text='<%# Bind("ProductId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">AgentFlag:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataAgentFlag" DataValueField='<%# Bind("AgentFlag") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">WriteAccess:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataWriteAccess" DataValueField='<%# Bind("WriteAccess") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">Authorization:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataAuthorization" DataValueField='<%# Bind("Authorization") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">Yahoogroup:</td>
				<td>
					<asp:TextBox runat="server" ID="dataYahoogroup" Text='<%# Bind("Yahoogroup") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">IndiaChapterName:</td>
				<td>
					<asp:TextBox runat="server" ID="dataIndiaChapterName" Text='<%# Bind("IndiaChapterName") %>' MaxLength="35"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
	</asp:FormView>
