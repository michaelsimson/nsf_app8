<%@ Control Language="C#" ClassName="OrganizationInfoFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">MEMBERID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMEMBERID" Text='<%# Bind("MEMBERID") %>' MaxLength="25"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ORGANIZATIONNAME:</td>
				<td>
					<asp:TextBox runat="server" ID="dataORGANIZATIONNAME" Text='<%# Bind("ORGANIZATIONNAME") %>' MaxLength="75"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">TITLE:</td>
				<td>
					<asp:TextBox runat="server" ID="dataTITLE" Text='<%# Bind("TITLE") %>' MaxLength="6"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FIRSTNAME:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFIRSTNAME" Text='<%# Bind("FIRSTNAME") %>' MaxLength="35"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">MIDDLEINITIAL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMIDDLEINITIAL" Text='<%# Bind("MIDDLEINITIAL") %>' MaxLength="25"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">LASTNAME:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLASTNAME" Text='<%# Bind("LASTNAME") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ADDRESS1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataADDRESS1" Text='<%# Bind("ADDRESS1") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ADDRESS2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataADDRESS2" Text='<%# Bind("ADDRESS2") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CITY:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCITY" Text='<%# Bind("CITY") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">STATE:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSTATE" Text='<%# Bind("STATE") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ZIP:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZIP" Text='<%# Bind("ZIP") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">COUNTRY:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCOUNTRY" Text='<%# Bind("COUNTRY") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PHONE:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPHONE" Text='<%# Bind("PHONE") %>' MaxLength="15"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FAX:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFAX" Text='<%# Bind("FAX") %>' MaxLength="15"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EMAIL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEMAIL" Text='<%# Bind("EMAIL") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SecondaryEMAIL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSecondaryEMAIL" Text='<%# Bind("SecondaryEMAIL") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">MATCHINGGIFT:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMATCHINGGIFT" Text='<%# Bind("MATCHINGGIFT") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SENDEMAIL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSENDEMAIL" Text='<%# Bind("SENDEMAIL") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">REFERREDBY:</td>
				<td>
					<asp:TextBox runat="server" ID="dataREFERREDBY" Text='<%# Bind("REFERREDBY") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">NSFCHAPTER:</td>
				<td>
					<asp:TextBox runat="server" ID="dataNSFCHAPTER" Text='<%# Bind("NSFCHAPTER") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SENDNEWSLETTER:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSENDNEWSLETTER" Text='<%# Bind("SENDNEWSLETTER") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">MAILINGLABEL:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMAILINGLABEL" Text='<%# Bind("MAILINGLABEL") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SENDRECEIPT:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSENDRECEIPT" Text='<%# Bind("SENDRECEIPT") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">LIAISONPERSON:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLIAISONPERSON" Text='<%# Bind("LIAISONPERSON") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">MemberSince:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMemberSince" Text='<%# Bind("MemberSince", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataMemberSince" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">DeleteReason:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeleteReason" Text='<%# Bind("DeleteReason") %>' MaxLength="200"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">DeletedFlag:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeletedFlag" Text='<%# Bind("DeletedFlag") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">IRScat:</td>
				<td>
					<asp:TextBox runat="server" ID="dataIRScat" Text='<%# Bind("IRScat") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">BusType:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBusType" Text='<%# Bind("BusType") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Venue:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVenue" Text='<%# Bind("Venue") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Sponsor:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSponsor" Text='<%# Bind("Sponsor") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Website:</td>
				<td>
					<asp:TextBox runat="server" ID="dataWebsite" Text='<%# Bind("Website") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


