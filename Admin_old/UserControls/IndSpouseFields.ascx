<%@ Control Language="C#" ClassName="IndSpouseFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">AutoMemberID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataAutoMemberID" Text='<%# Bind("AutoMemberID") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataAutoMemberID" runat="server" Display="Dynamic" ControlToValidate="dataAutoMemberID" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataAutoMemberID" runat="server" Display="Dynamic" ControlToValidate="dataAutoMemberID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Relationship:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRelationship" Text='<%# Bind("Relationship") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataRelationship" runat="server" Display="Dynamic" ControlToValidate="dataRelationship" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">DeleteReason:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeleteReason" Text='<%# Bind("DeleteReason") %>' MaxLength="255"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">DeletedFlag:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeletedFlag" Text='<%# Bind("DeletedFlag") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">MemberSince:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMemberSince" Text='<%# Bind("MemberSince", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataMemberSince" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">MailingLabel:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMailingLabel" Text='<%# Bind("MailingLabel") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SendReceipt:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSendReceipt" Text='<%# Bind("SendReceipt") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">LiasonPerson:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLiasonPerson" Text='<%# Bind("LiasonPerson") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ReferredBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataReferredBy" Text='<%# Bind("ReferredBy") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Chapter:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChapter" Text='<%# Bind("Chapter") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">NewsLetter:</td>
				<td>
					<asp:TextBox runat="server" ID="dataNewsLetter" Text='<%# Bind("NewsLetter") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Group2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataGroup2" Text='<%# Bind("Group2") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Group3:</td>
				<td>
					<asp:TextBox runat="server" ID="dataGroup3" Text='<%# Bind("Group3") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PotentialDonor:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPotentialDonor" Text='<%# Bind("PotentialDonor") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">VolunteerFlag:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVolunteerFlag" Text='<%# Bind("VolunteerFlag") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">VolunteerRole1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVolunteerRole1" Text='<%# Bind("VolunteerRole1") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataVolunteerRole1" runat="server" Display="Dynamic" ControlToValidate="dataVolunteerRole1" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Group1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataGroup1" Text='<%# Bind("Group1") %>' MaxLength="60"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Career:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCareer" Text='<%# Bind("Career") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Employer:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEmployer" Text='<%# Bind("Employer") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CountryOfOrigin:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCountryOfOrigin" Text='<%# Bind("CountryOfOrigin") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Email:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEmail" Text='<%# Bind("Email") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">SecondaryEmail:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSecondaryEmail" Text='<%# Bind("SecondaryEmail") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Education:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEducation" Text='<%# Bind("Education") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Fax:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFax" Text='<%# Bind("Fax") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">WPhone:</td>
				<td>
					<asp:TextBox runat="server" ID="dataWPhone" Text='<%# Bind("WPhone") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">WFax:</td>
				<td>
					<asp:TextBox runat="server" ID="dataWFax" Text='<%# Bind("WFax") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Gender:</td>
				<td>
					<asp:TextBox runat="server" ID="dataGender" Text='<%# Bind("Gender") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">HPhone:</td>
				<td>
					<asp:TextBox runat="server" ID="dataHPhone" Text='<%# Bind("HPhone") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CPhone:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCPhone" Text='<%# Bind("CPhone") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">State:</td>
				<td>
					<asp:TextBox runat="server" ID="dataState" Text='<%# Bind("State") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Zip:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZip" Text='<%# Bind("Zip") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Country:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCountry" Text='<%# Bind("Country") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Address1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataAddress1" Text='<%# Bind("Address1") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Address2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataAddress2" Text='<%# Bind("Address2") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">City:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCity" Text='<%# Bind("City") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FirstName:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFirstName" Text='<%# Bind("FirstName") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">MiddleInitial:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMiddleInitial" Text='<%# Bind("MiddleInitial") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">LastName:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLastName" Text='<%# Bind("LastName") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">MemberID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMemberID" Text='<%# Bind("MemberID") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">DonorType:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDonorType" Text='<%# Bind("DonorType") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Title:</td>
				<td>
					<asp:TextBox runat="server" ID="dataTitle" Text='<%# Bind("Title") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">StateOfOrigin:</td>
				<td>
					<asp:TextBox runat="server" ID="dataStateOfOrigin" Text='<%# Bind("StateOfOrigin") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PrimaryEmailOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPrimaryEmailOld" Text='<%# Bind("PrimaryEmailOld") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">HomePhoneOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataHomePhoneOld" Text='<%# Bind("HomePhoneOld") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CellPhoneOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCellPhoneOld" Text='<%# Bind("CellPhoneOld") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Address1Old:</td>
				<td>
					<asp:TextBox runat="server" ID="dataAddress1Old" Text='<%# Bind("Address1Old") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Address2Old:</td>
				<td>
					<asp:TextBox runat="server" ID="dataAddress2Old" Text='<%# Bind("Address2Old") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CityOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCityOld" Text='<%# Bind("CityOld") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">StateOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataStateOld" Text='<%# Bind("StateOld") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ZipCodeOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataZipCodeOld" Text='<%# Bind("ZipCodeOld") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PrimaryIndSpouseID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPrimaryIndSpouseID" Text='<%# Bind("PrimaryIndSpouseID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataPrimaryIndSpouseID" runat="server" Display="Dynamic" ControlToValidate="dataPrimaryIndSpouseID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">PrimaryContact:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPrimaryContact" Text='<%# Bind("PrimaryContact") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">VolunteerRole2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVolunteerRole2" Text='<%# Bind("VolunteerRole2") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataVolunteerRole2" runat="server" Display="Dynamic" ControlToValidate="dataVolunteerRole2" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">VolunteerRole3:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVolunteerRole3" Text='<%# Bind("VolunteerRole3") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataVolunteerRole3" runat="server" Display="Dynamic" ControlToValidate="dataVolunteerRole3" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChapterID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChapterID" Text='<%# Bind("ChapterID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataChapterID" runat="server" Display="Dynamic" ControlToValidate="dataChapterID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">MaritalStatus:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMaritalStatus" Text='<%# Bind("MaritalStatus") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Sponsor:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSponsor" Text='<%# Bind("Sponsor") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Liaison:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLiaison" Text='<%# Bind("Liaison") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


