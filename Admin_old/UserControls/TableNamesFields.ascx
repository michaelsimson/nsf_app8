<%@ Control Language="C#" ClassName="TableNamesFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">TableId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataTableId" Text='<%# Bind("TableId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataTableId" runat="server" Display="Dynamic" ControlToValidate="dataTableId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataTableId" runat="server" Display="Dynamic" ControlToValidate="dataTableId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">TableName:</td>
				<td>
					<asp:TextBox runat="server" ID="dataTableName" Text='<%# Bind("TableName") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


