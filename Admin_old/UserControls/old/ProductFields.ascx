<%@ Control Language="C#" ClassName="ProductFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataProductGroupId" DataSourceID="ProductGroupIdProductGroupDataSource" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" SelectedValue='<%# Bind("ProductGroupId") %>'></asp:DropDownList>
					<data:ProductGroupDataSource ID="ProductGroupIdProductGroupDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ProductGroupDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataProductGroupCode" DataSourceID="ProductGroupCodeProductGroupDataSource" DataTextField="ProductGroupCode" DataValueField="ProductGroupCode" SelectedValue='<%# Bind("ProductGroupCode") %>'></asp:DropDownList>
					<data:ProductGroupDataSource ID="ProductGroupCodeProductGroupDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ProductGroupDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataEventId" DataSourceID="EventIdEventDataSource" DataTextField="EventCode" DataValueField="EventId" SelectedValue='<%# Bind("EventId") %>'></asp:DropDownList>
					<data:EventDataSource ID="EventIdEventDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:EventDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataEventCode" DataSourceID="EventCodeEventDataSource" DataTextField="EventCode" DataValueField="EventCode" SelectedValue='<%# Bind("EventCode") %>'></asp:DropDownList>
					<data:EventDataSource ID="EventCodeEventDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:EventDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">Name:</td>
				<td>
					<asp:TextBox runat="server" ID="dataName" Text='<%# Bind("Name") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Status:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataStatus" DataSourceID="StatusStatusDataSource" DataTextField="DataValue" DataValueField="DataValue" SelectedValue='<%# Bind("Status") %>'></asp:DropDownList>
					<data:StatusDataSource ID="StatusStatusDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:StatusDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


