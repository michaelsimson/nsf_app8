<%@ Control Language="C#" ClassName="StatusFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">TableID:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataTableID" DataSourceID="TableIDLookUpCodesDataSource" DataTextField="LookUpCode" DataValueField="LookUpCodeId" SelectedValue='<%# Bind("TableID") %>'></asp:DropDownList>
					<data:LookUpCodesDataSource ID="TableIDLookUpCodesDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:LookUpCodesDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">DataValue:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDataValue" Text='<%# Bind("DataValue") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">DataText:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDataText" Text='<%# Bind("DataText") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


