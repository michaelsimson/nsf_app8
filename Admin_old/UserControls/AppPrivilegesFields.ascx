<%@ Control Language="C#" ClassName="AppPrivilegesFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">VolunteerId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVolunteerId" Text='<%# Bind("VolunteerId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataVolunteerId" runat="server" Display="Dynamic" ControlToValidate="dataVolunteerId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataVolunteerId" runat="server" Display="Dynamic" ControlToValidate="dataVolunteerId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">App1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp1" Text='<%# Bind("App1") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp2" Text='<%# Bind("App2") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App3:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp3" Text='<%# Bind("App3") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App4:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp4" Text='<%# Bind("App4") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App5:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp5" Text='<%# Bind("App5") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App6:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp6" Text='<%# Bind("App6") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App7:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp7" Text='<%# Bind("App7") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App8:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp8" Text='<%# Bind("App8") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App9:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp9" Text='<%# Bind("App9") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">App10:</td>
				<td>
					<asp:TextBox runat="server" ID="dataApp10" Text='<%# Bind("App10") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


