<%@ Control Language="C#" ClassName="LookUpCodesFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">LookUpCodeId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLookUpCodeId" Text='<%# Bind("LookUpCodeId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataLookUpCodeId" runat="server" Display="Dynamic" ControlToValidate="dataLookUpCodeId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataLookUpCodeId" runat="server" Display="Dynamic" ControlToValidate="dataLookUpCodeId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">LookUpCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLookUpCode" Text='<%# Bind("LookUpCode") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


