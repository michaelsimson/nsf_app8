<%@ Control Language="C#" ClassName="EventCalendarFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataEventId" DataSourceID="EventIdEventDataSource" DataTextField="EventCode" DataValueField="EventId" SelectedValue='<%# Bind("EventId") %>'></asp:DropDownList>
					<data:EventDataSource ID="EventIdEventDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:EventDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataEventCode" DataSourceID="EventCodeEventDataSource" DataTextField="EventCode" DataValueField="EventCode" SelectedValue='<%# Bind("EventCode") %>'></asp:DropDownList>
					<data:EventDataSource ID="EventCodeEventDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:EventDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupId" Text='<%# Bind("ProductGroupId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupCode" Text='<%# Bind("ProductGroupCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupCode" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChapterId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataChapterId" DataSourceID="ChapterIdChapterDataSource" DataTextField="ChapterCode" DataValueField="ChapterID" SelectedValue='<%# Bind("ChapterId") %>'></asp:DropDownList>
					<data:ChapterDataSource ID="ChapterIdChapterDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ChapterDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChapterCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataChapterCode" DataSourceID="ChapterCodeChapterDataSource" DataTextField="ChapterCode" DataValueField="ChapterCode" SelectedValue='<%# Bind("ChapterCode") %>'></asp:DropDownList>
					<data:ChapterDataSource ID="ChapterCodeChapterDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ChapterDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductId" Text='<%# Bind("ProductId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventYear:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventYear" Text='<%# Bind("EventYear") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataEventYear" runat="server" Display="Dynamic" ControlToValidate="dataEventYear" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventDay1" Text='<%# Bind("EventDay1", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataEventDay1" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">EventDay1Desc:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventDay1Desc" Text='<%# Bind("EventDay1Desc") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventDay1Fee:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventDay1Fee" Text='<%# Bind("EventDay1Fee") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataEventDay1Fee" runat="server" Display="Dynamic" ControlToValidate="dataEventDay1Fee" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">VenueId1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVenueId1" Text='<%# Bind("VenueId1") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataVenueId1" runat="server" Display="Dynamic" ControlToValidate="dataVenueId1" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataVenueId1" runat="server" Display="Dynamic" ControlToValidate="dataVenueId1" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CheckinTimeDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCheckinTimeDay1" Text='<%# Bind("CheckinTimeDay1") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">StartTimeDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataStartTimeDay1" Text='<%# Bind("StartTimeDay1") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EndTimeDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEndTimeDay1" Text='<%# Bind("EndTimeDay1") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">BuildingDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBuildingDay1" Text='<%# Bind("BuildingDay1") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">RoomDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRoomDay1" Text='<%# Bind("RoomDay1") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">RegDeadlineDay1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRegDeadlineDay1" Text='<%# Bind("RegDeadlineDay1", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataRegDeadlineDay1" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">LateFee1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLateFee1" Text='<%# Bind("LateFee1") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataLateFee1" runat="server" Display="Dynamic" ControlToValidate="dataLateFee1" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">DeadlineLateFee1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeadlineLateFee1" Text='<%# Bind("DeadlineLateFee1", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataDeadlineLateFee1" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">EventDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventDay2" Text='<%# Bind("EventDay2", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataEventDay2" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">EventDay2Desc:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventDay2Desc" Text='<%# Bind("EventDay2Desc") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventDay2Fee:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventDay2Fee" Text='<%# Bind("EventDay2Fee") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataEventDay2Fee" runat="server" Display="Dynamic" ControlToValidate="dataEventDay2Fee" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">VenueId2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVenueId2" Text='<%# Bind("VenueId2") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataVenueId2" runat="server" Display="Dynamic" ControlToValidate="dataVenueId2" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataVenueId2" runat="server" Display="Dynamic" ControlToValidate="dataVenueId2" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CheckinTimeDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCheckinTimeDay2" Text='<%# Bind("CheckinTimeDay2") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">StartTimeDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataStartTimeDay2" Text='<%# Bind("StartTimeDay2") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EndTimeDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEndTimeDay2" Text='<%# Bind("EndTimeDay2") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">BuildingDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBuildingDay2" Text='<%# Bind("BuildingDay2") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">RoomDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRoomDay2" Text='<%# Bind("RoomDay2") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">RegDeadlineDay2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRegDeadlineDay2" Text='<%# Bind("RegDeadlineDay2", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataRegDeadlineDay2" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">LateFee2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLateFee2" Text='<%# Bind("LateFee2") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataLateFee2" runat="server" Display="Dynamic" ControlToValidate="dataLateFee2" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">DeadlineLateFee2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDeadlineLateFee2" Text='<%# Bind("DeadlineLateFee2", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataDeadlineLateFee2" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


