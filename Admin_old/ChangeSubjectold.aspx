﻿<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="ChangeSubjectold.aspx.cs" Inherits="Admin_ChangeSubject" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    

    <table id="tblLogin" width="50%" runat="server">
				<tr>
					<td></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<h1>Change Subject</h1>
					</td>
					
				</tr>
				 <tr>
			        <td colspan="4">
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
			          </td>
			    </tr>
			    <tr>
			        <td>&nbsp;</td>
			    </tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right">Parent Email ID</td>
					<td><asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="300" MaxLength="50"></asp:textbox>
                        <asp:button id="btnFindContest" runat="server" CssClass="FormButtonCenter" 
                            Text="Find Subject" OnClick="btnFindContest_Click"></asp:button><br>
						&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
							Display="Dynamic"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
							ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
				</tr></table>


    <div>
 <asp:datagrid id="dgSelectedChild" runat="server" CssClass="GridStyle" Width="100%" AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" >
										<FooterStyle CssClass="GridFooter"></FooterStyle>
										<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
										
										<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
										<ItemStyle  HorizontalAlign="Left" Wrap="true"></ItemStyle>
										<HeaderStyle Wrap="true"  Width="100%"></HeaderStyle>
										<Columns>
											<asp:EditCommandColumn UpdateText="Update /" CancelText="Cancel"
												EditText="Change Subject">
                                                <HeaderStyle CssClass="GridHeader" />
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn HeaderText="New Subject" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
                                                <EditItemTemplate>
                                                    <asp:DropDownList runat="server" ID="ddlNewCenters" CssClass="SmallFont"></asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn> 
											<asp:TemplateColumn HeaderText="Child Name" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
												<ItemTemplate>
													<asp:Label id="lblChildName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'>
													</asp:Label>
													<%--<asp:Label runat="server" Visible="false" ID="lblChildID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ChildNumber") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblContestYear" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ContestYear") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblContestDate" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ContestDate") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblChapterID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ChapterID") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblRegistrationDeadline" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"RegistrationDeadline") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblcontestID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ContestID") %>'></asp:Label>
											     	<asp:Label runat="server" Visible="false" ID="lblProductGroupId" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductGroupId") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductGroupCode" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductCode" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblMemberID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ParentID") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblEventID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                                    <asp:Label runat="server" Visible="false" ID="lblEventCode" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"EventCode") %>'></asp:Label>--%>
												</ItemTemplate>												
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Current Venue" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
											    <ItemTemplate>
											        <asp:Label ID="lblCurrenctCenter" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Organization_Name") %>'></asp:Label>
											    </ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Current Subject" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
												<ItemTemplate>
													<asp:Label id="lblContestRegistered" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "productcode") %>'>													
													
													</asp:Label>
												</ItemTemplate>
												
											</asp:TemplateColumn>
										
										</Columns>
										<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>
    </div>
    </form>
</body>
</html>
