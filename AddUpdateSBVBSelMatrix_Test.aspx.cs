﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;

public partial class AddUpdateSBVBSelMatrix : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            //Session["LoginID"] = 4240;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            //if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            //{
            //    Response.Redirect("~/login.aspx?entry=p");
            //}
            if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97)
            {
                Response.Redirect("~/VolunteerFunctions.aspx");
            }
     
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= (DateTime.Now.Year-3); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
          
           
        }


       
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }
   



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            lblErr.Visible = true;
        }
        else if (ddlProductGroup.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product Group";
            lblErr.Visible = true;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            lblErr.Visible = true;
        }
        else if (ddlPhase.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Phase";
            lblErr.Visible = true;
        }
        else
        {
            displayGridView();
            
        }

       
    }
    protected void displayGridView()
    {

        if (ddlPhase.SelectedIndex == 1)
        {

            string checkexistsqry = "select SBVBSelMatrixID,RoundType,RoundCount,PubUnpub,Level,SubLevel,WordCount1 as Words1,WordCount2 as Words2,WordCount3 as Words3,WordCount4 as Words4 from SBVBSelMatrix where Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode=( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") and Phase=" + ddlPhase.SelectedValue + "";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, checkexistsqry);
            DataTable dt;
            if (ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("SBVBSelMatrixID");
                dt.Columns.Add("RoundType");
                dt.Columns.Add("RoundCount");
                dt.Columns.Add("PubUnpub");
                dt.Columns.Add("Level");
                dt.Columns.Add("SubLevel");
                dt.Columns.Add("Words1");
                dt.Columns.Add("Words2");
                dt.Columns.Add("Words3");
                dt.Columns.Add("Words4");
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);
                if (!((ddlEvent.SelectedIndex == 1) && (ddlProduct.SelectedIndex == 2)))
                {
                    dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                }

                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Tie Breaker";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);

            }
            gvVocabSelMatrix.DataSource = dt;
            gvVocabSelMatrix.DataBind();
            gvVocabSelMatrix.Rows[0].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            gvVocabSelMatrix.Rows[1].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            gvVocabSelMatrix.Rows[2].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            if (!((ddlEvent.SelectedIndex == 1) && (ddlProduct.SelectedIndex == 2)))
            {
                gvVocabSelMatrix.Rows[3].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            }
            gvVocabSelMatrix.Visible = true;
            btnUpdate.Visible = true;
            lblErr.Visible = false;
            gvVocabSelMatrix.Columns[6].Visible = false;
            gvVocabSelMatrix.Columns[7].Visible = false;
            gvVocabSelMatrix.Columns[8].Visible = false;

        }
        else if (ddlPhase.SelectedIndex == 2 || ddlPhase.SelectedIndex==3)
        {

            string checkexistsqry;
            checkexistsqry= "select SBVBSelMatrixID,RoundType,RoundCount,RoundFrom,RoundTo,PubUnpub,Level,SubLevel,WordCount1  as Words1,WordCount2 as Words2,WordCount3 as Words3,WordCount4 as Words4 from SBVBSelMatrix where Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode=( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") and Phase=" + ddlPhase.SelectedValue + "";
            //if (ddlEvent.SelectedIndex == 1 && ddlPhase.SelectedIndex == 3)
            //{
            //    checkexistsqry = "select SBVBSelMatrixID,RoundType,RoundFrom,RoundTo,PubUnpub,Level,SubLevel,WordCount1  as Words1,WordCount2 as Words2,WordCount3 as Words3,WordCount4 as Words4 from SBVBSelMatrix where Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode=( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") and Phase=" + ddlPhase.SelectedValue + "";
            //}
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, checkexistsqry);
            DataTable dt;
            if (ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = new DataTable();
                DataRow dr;
                //if (ddlEvent.SelectedIndex == 1 && ddlPhase.SelectedIndex == 3)
                //{
                //  dt.Rows[0].Cells
                //}
                dt.Columns.Add("SBVBSelMatrixID");
                dt.Columns.Add("RoundType");
                dt.Columns.Add("RoundCount");
                dt.Columns.Add("PubUnpub");
                dt.Columns.Add("Level");
                dt.Columns.Add("SubLevel");
                dt.Columns.Add("Words1");
                dt.Columns.Add("Words2");
                dt.Columns.Add("Words3");
                dt.Columns.Add("Words4");
                dt.Columns.Add("RoundFrom");
                dt.Columns.Add("RoundTo");
               
                dr = dt.NewRow();
               
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);

                if (ddlEvent.SelectedIndex == 1 && ddlPhase.SelectedIndex==2)
                {
                    dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                   
                }
                
               
            }

            gvVocabSelMatrix.DataSource = dt;
            gvVocabSelMatrix.DataBind();
            gvVocabSelMatrix.Rows[0].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            gvVocabSelMatrix.Rows[1].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");

            gvVocabSelMatrix.Visible = true;
            btnUpdate.Visible = true;
            lblErr.Visible = false;
            if (ddlEvent.SelectedIndex == 1)
            {
                gvVocabSelMatrix.Columns[6].Visible = false;
                gvVocabSelMatrix.Columns[7].Visible = false;
                gvVocabSelMatrix.Columns[8].Visible = false;
            }
            else
            {
                gvVocabSelMatrix.Columns[6].Visible = true;
                gvVocabSelMatrix.Columns[7].Visible = true;
                gvVocabSelMatrix.Columns[8].Visible = true;
            }

        }
   
    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {


        fillProduct();
       
    }
    protected void fillProduct() { 
    
   
        //ddlProductGroup
        string ddlproductqry;


        ddlproductqry = "select Name,ProductId,ProductCode from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Status='O'";
            
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
            
            ddlProduct.DataSource = dsstate;
            ddlProductGroup.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "Name";
            ddlProduct.DataBind();


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
        
    
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //DataTable dt=
        //BindingSource bs = (BindingSource)gvVocabSelMatrix.DataSource;
        //DataTable ds =(DataTable) gvVocabSelMatrix.DataSource;
        int k;
        string roundtype;
        string strRC;
        string PubUnPub;
        string Level;
        string SubLevel;
        string wc1;
        string wc2;
        string wc3;
        string wc4;
        string updateqry="";
        string insertqry="";
        string strSBVBSelMatrixID;
        int sumwc=0;
        int sumrc = 0;
        string checkexistsqry = "select * from SBVBSelMatrix where Year="+ddlYear.SelectedValue+" and Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode=( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") and Phase=" + ddlPhase.SelectedValue + "";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, checkexistsqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ddlPhase.SelectedIndex == 1)
            {
                int ic = 3;
                if (!((ddlEvent.SelectedIndex == 1) && (ddlProduct.SelectedIndex == 2)))
                {
                    ic = 4;
                }
                for (int i = 0; i < ic; i++)
                {
                    HiddenField hfSBVBSelMatrixID = (HiddenField)gvVocabSelMatrix.Rows[i].FindControl("SBVBSelMatrixID");
                    strSBVBSelMatrixID = hfSBVBSelMatrixID.Value;
                    //roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    TextBox tbRoundCount = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundCount");
                    strRC = tbRoundCount.Text;
                    if (strRC.Equals(""))
                        strRC = "null";
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    wc1 = tbwc1.Text;
                    if (wc1.Equals(""))
                        wc1 = "0";
                    sumwc = sumwc + Convert.ToInt32(wc1);
                    
                    updateqry = updateqry + " update SBVBSelMatrix set RoundCount=" + strRC + ",PubUnpub='" + PubUnPub + "',Level=" + Level + ",SubLevel=" + SubLevel + ",WordCount1=" + wc1 + ",ModifiedDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where SBVBSelMatrixID=" + strSBVBSelMatrixID + "";// and Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode=( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") and Phase=" + ddlPhase.SelectedValue + "";

                }
                //updating into SQL Table
                string strfindWCcontest = "select Questions from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int WCContestSQL = 0;
                DataSet dsWCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindWCcontest);
                if (dsWCContest.Tables[0].Rows.Count > 0)
                {
                    WCContestSQL = Convert.ToInt32(dsWCContest.Tables[0].Rows[0]["Questions"].ToString());
                    if (WCContestSQL == sumwc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Updated Successfully!";
                            lblErr.Visible = true;
                            
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Word1 Column is should be equal to "+WCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "#of Questions is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }
               
            }
            else if (ddlPhase.SelectedIndex == 2 || ddlPhase.SelectedIndex==3)
            {
                int nc = 2;
                if (ddlEvent.SelectedIndex == 1 && (ddlProduct.SelectedIndex==1 || ddlProduct.SelectedIndex==2))
                {
                    nc = 3;
                }

                for (int i = 0; i < nc; i++)
                {
                    HiddenField hfSBVBSelMatrixID = (HiddenField)gvVocabSelMatrix.Rows[i].FindControl("SBVBSelMatrixID");
                    strSBVBSelMatrixID = hfSBVBSelMatrixID.Value;
                    //roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    TextBox tbRoundCount = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundCount");
                    strRC = tbRoundCount.Text;
                    
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    //TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    //wc1 = tbwc1.Text;
                    //if (wc1.Equals(""))
                    if (strRC.Equals(""))
                    {
                        wc1 ="null";
                        wc2 = "null";
                        wc3 = "null";
                        wc4 = "null";
                    }
                    else
                    {
                        int cs = 0;
                        if (ddlEvent.SelectedIndex == 1)
                        {
                            cs = 20;
                            //(class size + 2) * (1 + (1/3))
                            wc1 = (Math.Ceiling(((cs + 2) * (Convert.ToInt32(strRC) + (1 / 3))) * 1.3)).ToString();
                        }
                        else
                        {
                            cs = 5;
                            wc1 = ((cs + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                        }
                        
                        //TextBox tbwc2 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words2");
                        //wc2 = tbwc2.Text;
                        //if (wc2.Equals(""))
                        wc2 = ((15 + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                        //TextBox tbwc3 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words3");
                        //wc3 = tbwc3.Text;
                        //if (wc3.Equals(""))
                        wc3 = ((25 + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                        //TextBox tbwc4 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words4");
                        //wc4 = tbwc4.Text;
                        //if (wc4.Equals(""))
                        wc4 = ((35 + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                    }
                    if (strRC.Equals(""))
                        strRC = "0";
                    sumrc = sumrc + Convert.ToInt32(strRC);

                    updateqry = updateqry + " update SBVBSelMatrix set RoundCount=" + strRC + ",PubUnpub='" + PubUnPub + "',Level=" + Level + ",SubLevel=" + SubLevel + ",WordCount1=" + wc1 + ",WordCount2=" + wc2 + ",WordCount3=" + wc3 + ",WordCount4=" + wc4 + ",ModifiedDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where SBVBSelMatrixID=" + strSBVBSelMatrixID + "";// and Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode=( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + ") and Phase=" + ddlPhase.SelectedValue + "";

                }
                //updating into SQL Table
                string strfindRCcontest = "select Rounds from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int RCContestSQL = 0;
                DataSet dsRCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindRCcontest);
                if (dsRCContest.Tables[0].Rows.Count > 0)
                {
                    RCContestSQL = Convert.ToInt32(dsRCContest.Tables[0].Rows[0]["Rounds"].ToString());
                    if (RCContestSQL == sumrc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Updated Successfully!";
                            lblErr.Visible = true;
                            
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Round Count Column is should be equal to "+RCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "#of rounds is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }
            }
            //Response.Write("sum of word count1: " + sumwc+"  roundcount"+sumrc);

            


           
        }
        else
        {
            if (ddlPhase.SelectedIndex == 1)
            {
                int ic = 3;
                if (!((ddlEvent.SelectedIndex == 1) && (ddlProduct.SelectedIndex == 2)))
                {
                    ic = 4;
                }
                for (int i = 0; i < ic; i++)
                {
                    //BoundField tbRoundCount = (BoundField)gvVocabSelMatrix.Rows[i].FindControl("RoundType");
                    //strRC = tbRoundCount.Text;
                    // row.Cells(1).Text.Trim()

                    roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    TextBox tbRoundCount = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundCount");
                    strRC = tbRoundCount.Text;
                    if (strRC.Equals(""))
                        strRC = "null";
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    wc1 = tbwc1.Text;
                    if (wc1.Equals(""))
                        wc1 = "0";
                    sumwc = sumwc + Convert.ToInt32(wc1);

                    insertqry = insertqry + " insert into SBVBSelMatrix(Year,EventID,Event,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnpub,Phase,RoundType,RoundCount,[Level],SubLevel,WordCount1,CreatedDate,CreatedBy) values(" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + ",'" + ddlEvent.SelectedItem.Text + "',( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and EventID=" + ddlEvent.SelectedValue + "),'" + ddlProductGroup.SelectedValue + "',( select ProductID from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + "),( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + "),'" + PubUnPub + "'," + ddlPhase.SelectedValue + ",'" + roundtype + "'," + strRC + "," + Level + "," + SubLevel + "," + wc1 + ",getDate()," + Session["LoginID"] + ")";

                }
                //inserting into SQL Table
                string strfindWCcontest = "select Questions from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int WCContestSQL = 0;
                DataSet dsWCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindWCcontest);
                if (dsWCContest.Tables[0].Rows.Count > 0)
                {
                    WCContestSQL = Convert.ToInt32(dsWCContest.Tables[0].Rows[0]["Questions"].ToString());
                    if (WCContestSQL == sumwc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, insertqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Added Successfully!";
                            lblErr.Visible = true;
                            
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Word1 Column is should be equal to "+WCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "#of Questions is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }
            }
            else if (ddlPhase.SelectedIndex == 2 || ddlPhase.SelectedIndex==3)
            {
                int nc = 2;
                if (ddlEvent.SelectedIndex == 1 && (ddlProduct.SelectedIndex == 1 || ddlProduct.SelectedIndex == 2))
                {
                    nc = 3;
                }

                for (int i = 0; i < nc; i++)
                {
                    //BoundField tbRoundCount = (BoundField)gvVocabSelMatrix.Rows[i].FindControl("RoundType");
                    //strRC = tbRoundCount.Text;
                    // row.Cells(1).Text.Trim()

                    roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    TextBox tbRoundCount = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundCount");
                    strRC = tbRoundCount.Text;
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    //TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    //wc1 = tbwc1.Text;
                    //if (wc1.Equals(""))
                    if (strRC.Equals(""))
                    {
                        wc1 = "null";
                        wc2 = "null";
                        wc3 = "null";
                        wc4 = "null";
                    }
                    else
                    {
                        int cs=0;
                        if (ddlEvent.SelectedIndex == 1)
                        {
                            cs = 20;
                            //(class size + 2) * (1 + (1/3))
                            wc1 = (Math.Ceiling(((cs + 2) * (Convert.ToInt32(strRC) + (1 / 3))) * 1.3)).ToString();
                        }
                        else
                        {
                            cs = 5;
                            wc1 = ((cs + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                        }
                     
                        //TextBox tbwc2 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words2");
                        //wc2 = tbwc2.Text;
                        //if (wc2.Equals(""))
                        wc2 = ((15 + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                        //TextBox tbwc3 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words3");
                        //wc3 = tbwc3.Text;
                        //if (wc3.Equals(""))
                        wc3 = ((25 + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                        //TextBox tbwc4 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words4");
                        //wc4 = tbwc4.Text;
                        //if (wc4.Equals(""))
                        wc4 = ((35 + 2) * (Convert.ToInt32(strRC) + 1)).ToString();
                    }
                    if (strRC.Equals(""))
                        strRC = "0";
                    sumrc = sumrc + Convert.ToInt32(strRC);
                    insertqry = insertqry + " insert into SBVBSelMatrix(Year,EventID,Event,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnpub,Phase,RoundType,RoundCount,[Level],SubLevel,WordCount1,WordCount2,WordCount3,WordCount4,CreatedDate,CreatedBy) values(" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + ",'" + ddlEvent.SelectedItem.Text + "',( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and EventID=" + ddlEvent.SelectedValue + "),'" + ddlProductGroup.SelectedValue + "',( select ProductID from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + "),( select ProductCode from product where Name='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + "),'" + PubUnPub + "'," + ddlPhase.SelectedValue + ",'" + roundtype + "'," + strRC + "," + Level + "," + SubLevel + "," + wc1 + "," + wc2 + "," + wc3 + "," + wc4 + ",getDate()," + Session["LoginID"] + ")";

                }
                //inserting into SQL Table
                string strfindRCcontest = "select Rounds from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int RCContestSQL = 0;
                DataSet dsRCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindRCcontest);
                if (dsRCContest.Tables[0].Rows.Count > 0)
                {
                    RCContestSQL = Convert.ToInt32(dsRCContest.Tables[0].Rows[0]["Rounds"].ToString());
                    if (RCContestSQL == sumrc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, insertqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Added Successfully!";
                            lblErr.Visible = true;
                            
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Round Count Column is should be equal to "+RCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "“#of rounds is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }
            }
         
           
        }
        
        

    }
    protected void gvVocabSelMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ddlPhase.SelectedIndex == 1)
            {
              
                    TextBox tbRoundCount =(e.Row.FindControl("RoundCount") as TextBox);
                    tbRoundCount.Enabled = false;
               
            }
            else if (ddlPhase.SelectedIndex == 2)
            {

                TextBox tbWC1 = (e.Row.FindControl("Words1") as TextBox);
                tbWC1.Enabled = false;
                TextBox tbWC2 = (e.Row.FindControl("Words2") as TextBox);
                tbWC2.Enabled = false;
                TextBox tbWC3 = (e.Row.FindControl("Words3") as TextBox);
                tbWC3.Enabled = false;
                TextBox tbWC4 = (e.Row.FindControl("Words4") as TextBox);
                tbWC4.Enabled = false;
               

            }
            else if (ddlPhase.SelectedIndex == 3)
            {

                TextBox tbWC1 = (e.Row.FindControl("Words1") as TextBox);
                tbWC1.Enabled = false;
                TextBox tbWC2 = (e.Row.FindControl("Words2") as TextBox);
                tbWC2.Enabled = false;
                TextBox tbWC3 = (e.Row.FindControl("Words3") as TextBox);
                tbWC3.Enabled = false;
                TextBox tbWC4 = (e.Row.FindControl("Words4") as TextBox);
                tbWC4.Enabled = false;
               

            }
            
        }
    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlPhase.Items.Clear();
        if (ddlEvent.SelectedValue.Equals("1"))
        {
            ddlPhase.Items.Add(new ListItem("Select Phase"));
            ddlPhase.Items.Add(new ListItem("Phase I","1"));
            ddlPhase.Items.Add(new ListItem("Phase II", "2"));
            ddlPhase.Items.Add(new ListItem("Phase III", "3"));
          
        }
        else if(ddlEvent.SelectedValue.Equals("2"))
        {
            ddlPhase.Items.Add(new ListItem("Select Phase"));
            ddlPhase.Items.Add(new ListItem("Phase I", "1"));
            ddlPhase.Items.Add(new ListItem("Phase II", "2"));
            
        }
    }
    protected void gvVocabSelMatrix_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (ddlEvent.SelectedIndex == 1 && ddlPhase.SelectedIndex == 3)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].ColumnSpan = 2;
                //now make up for the colspan from cell2
               // e.Row.Cells.RemoveAt(4);
            }
        }
    }
}