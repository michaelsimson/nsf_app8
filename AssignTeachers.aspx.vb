﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class AssignTeachers
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If

        If Not Page.IsPostBack Then
            If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
                ddlRole.Enabled = True
                'TrPrdGrp.Visible = True
                'TrPrd.Visible = True
            ElseIf Session("RoleId").ToString() = "97" Or Session("RoleId").ToString() = "96" Then
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByValue("98"))
                ddlRole.Enabled = False
                'TrPrdGrp.Visible = False
                'TrPrd.Visible = False
                btnassignteacher.Text = "Assign teacher"
            Else
                Server.Transfer("maintest.aspx")
            End If
            loadvolunteer()
            'LoadProductGroup()
        End If
    End Sub
    Private Sub loadvolunteer()
        lblerr.Text = String.Empty
        'Response.Write("select  distinct V.volunteerID,(Select top 1 Name from Role where RoleID=V.RoleID ) as Role,I.FirstName + ' ' + I.LastName as VName,I.City,I.State,I.Email,P.Name as ProductName,CASE WHEN cc.SignUpID IS NULL THEN 'True' Else 'False' END as Status from Volunteer V inner join IndSpouse I ON V.MemberID = I.AutoMemberID and V.RoleId=" & ddlRole.SelectedValue & " Left Join Product P On P.ProductID=V.ProductID Left JOIN CalSignUp CC ON  CC.MemberID = V.MemberId and GETDATE()< CC.Enddate and CC.EventYear >= Year(GETDATE())")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select  distinct V.volunteerID,(Select top 1 Name from Role where RoleID=V.RoleID ) as Role,I.FirstName + ' ' + I.LastName as VName,I.City,I.State,I.Email,P.Name as ProductName,CASE WHEN cc.SignUpID IS NULL THEN 'True' Else 'False' END as Status from Volunteer V inner join IndSpouse I ON V.MemberID = I.AutoMemberID and V.RoleId=" & ddlRole.SelectedValue & " Left Join Product P On P.ProductID=V.ProductID Left JOIN CalSignUp CC ON  CC.MemberID = V.MemberId and GETDATE()< CC.Enddate and CC.EventYear >= Year(GETDATE())")
        Dim dt As DataTable = ds.Tables(0)
        If ds.Tables(0).Rows.Count > 0 Then
            DGVolunteer.DataSource = dt
            If ddlRole.SelectedValue = "98" Then
                DGVolunteer.Columns(7).Visible = False
                btnassignteacher.Text = "Assign teacher"
            Else
                DGVolunteer.Columns(7).Visible = True
                btnassignteacher.Text = "Assign Online Workshop Coordinator"
            End If
            DGVolunteer.DataBind()

        Else
            DGVolunteer.DataSource = Nothing
            DGVolunteer.DataBind()
            lblerr.Text = "No " & ddlRole.SelectedItem.Text & " found"
        End If

    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and Email like '%" + email + "%'")
            Else
                strSql.Append("  Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by lastname,firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Search.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        'MsgBox(SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql))
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtParent.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            HlblParentID.Text = GridMemberDt.DataKeys(index).Value
            pIndSearch.Visible = False
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
        End If
    End Sub

    Protected Sub btnassignteacher_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnassignteacher.Click
        If Not HlblParentID.Text = "" Then
            If ddlRole.SelectedValue = "98" Then
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=98 and memberid=" & HlblParentID.Text & "") < 1 Then
                    Dim strsql As String = "Insert into volunteer(MemberID, RoleId, RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national]) values ("
                    strsql = strsql & HlblParentID.Text & ",98,'OWkshopTeach',Year(Getdate()),20,'OWkshop',getdate()," & Session("LoginID") & ",'Y')"
                    Try
                        SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                        lblerr.Text = "Inserted Successfully"
                        HlblParentID.Text = ""
                        txtParent.Text = ""
                        loadvolunteer()
                    Catch ex As Exception
                        lblerr.Text = strsql
                        lblerr.Text = lblerr.Text & ex.ToString()
                    End Try
                Else
                    lblerr.Text = " Role already exist for this volunteer"
                End If
            Else
                ''teacher Admin
                'If ddlPrdGroup.Items(0).Selected = True And ddlPrdGroup.SelectedItem.Text = "Select Product Group" Then
                '    lblerr.Text = " Please select Product Group"
                '    Exit Sub
                'ElseIf ddlPrd.Items(0).Selected = True And ddlPrd.SelectedItem.Text = "Select Product" Then
                '    lblerr.Text = " Please select Product"
                '    Exit Sub
                'End If
                'Dim PrdgrpCode As String()
                'PrdgrpCode = ddlPrdGroup.SelectedValue.Split("-")
                'Dim PrdCode As String()
                'PrdCode = ddlPrd.SelectedValue.Split("-")

                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=97  and memberid=" & HlblParentID.Text) < 1 Then 'and ProductID=" & PrdCode(0).ToString()) < 1 Then
                    Dim strsql As String = "Insert into volunteer(MemberID, RoleId,RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national]) values (" 'ProductID,ProductCode,ProductGroupID,ProductGroupCode,
                    strsql = strsql & HlblParentID.Text & ",97,'OWkshopC',Year(Getdate()),20,'OWkshop',getdate()," & Session("LoginID") & ",'Y')" ' " & PrdCode(0).ToString() & ",'" & PrdCode(1).ToString() & "'," & PrdgrpCode(0).ToString() & ",'" & PrdgrpCode(1).ToString() & "',
                    Try
                        SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                        lblerr.Text = "Inserted Successfully"
                        HlblParentID.Text = ""
                        txtParent.Text = ""
                        loadvolunteer()
                    Catch ex As Exception
                        lblerr.Text = strsql
                        lblerr.Text = lblerr.Text & ex.ToString()
                    End Try
                Else
                    lblerr.Text = "Online Workshop Coordinator is already assigned for the Volunteeer" ' & ddlPrd.SelectedItem.Text
                End If

            End If
        Else
            lblerr.Text = "Please select The volunteer to be assinged using Search Button"
        End If
    End Sub

    Protected Sub DGVolunteer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim volunteerID As Integer = CInt(e.Item.Cells(1).Text)
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from Volunteer Where volunteerID=" & volunteerID & "")
                loadvolunteer()
            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    'Protected Sub ddlPrdGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    LoadProductID()
    'End Sub

    'Private Sub LoadProductGroup()
    '    Dim strSql As String = "SELECT  Convert(varchar,ProductGroupID) + '-' + ProductGroupCode as code,Name from ProductGroup where EventId=13  order by ProductGroupID"
    '    Dim drproductgroup As SqlDataReader
    '    Dim conn As New SqlConnection(Application("ConnectionString"))
    '    drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
    '    ddlPrdGroup.DataSource = drproductgroup
    '    ddlPrdGroup.DataBind()
    '    If ddlPrdGroup.Items.Count < 1 Then
    '        lblerr.Text = "No Product is opened. Please Contact admin and Get Product Opened in EventFees table"
    '    ElseIf ddlPrdGroup.Items.Count > 1 Then
    '        ddlPrdGroup.Items.Insert(0, "Select Product Group")
    '        ddlPrdGroup.Items(0).Selected = True
    '        ddlPrdGroup.Enabled = True
    '    Else
    '        ddlPrdGroup.Enabled = False
    '        LoadProductID()
    '    End If
    'End Sub
    'Private Sub LoadProductID()
    '    ' will load depending on Selected item in Productgroup
    '    Dim conn As New SqlConnection(Application("ConnectionString"))
    '    If ddlPrdGroup.Items(0).Selected = True And ddlPrdGroup.SelectedItem.Text = "Select Product Group" Then
    '        ddlPrd.Items.Clear()
    '        ddlPrd.Enabled = False
    '    Else
    '        Dim strSql As String
    '        Dim PrdgrpCpde As String()
    '        PrdgrpCpde = ddlPrdGroup.SelectedValue.Split("-")
    '        Try
    '            strSql = "Select Convert(varchar,ProductID) + '-' + ProductCode as code, Name from Product Where EventId=13 AND ProductGroupID =" & PrdgrpCpde(0).ToString() & " order by ProductID"
    '            Dim drproductid As SqlDataReader
    '            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
    '            ddlPrd.DataSource = drproductid
    '            ddlPrd.DataBind()
    '            If ddlPrd.Items.Count > 1 Then
    '                ddlPrd.Items.Insert(0, "Select Product")
    '                ddlPrd.Items(0).Selected = True
    '                ddlPrd.Enabled = True
    '            ElseIf ddlPrd.Items.Count < 1 Then
    '                ddlPrd.Enabled = False
    '            Else
    '                ddlPrd.Enabled = False
    '                ''
    '            End If
    '        Catch ex As Exception
    '            lblerr.Text = strSql & ex.ToString
    '        End Try
    '    End If
    'End Sub

    Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlRole.SelectedValue = "98" Then
            'TrPrdGrp.Visible = False
            'TrPrd.Visible = False
            btnassignteacher.Text = "Assign teacher"
        Else
            btnassignteacher.Text = "Assign teacher Admin"
            'TrPrdGrp.Visible = True
            'TrPrd.Visible = True
            'LoadProductGroup()
            'LoadProductID()
        End If
        loadvolunteer()

    End Sub


End Class
