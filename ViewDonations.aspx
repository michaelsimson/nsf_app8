<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewDonations.aspx.vb" Inherits="ViewDonations" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">        
                     
           <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
				<tr bgcolor="#FFFFFF">
					<td  class="Heading">Donations Information</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td class="ContentHeading" align="right"><asp:hyperlink id="lnkAdd" runat="server" Font-Bold="True" NavigateUrl="~/AddDonation.aspx" Text="Add Donation"></asp:hyperlink>&nbsp;&nbsp;&nbsp;
					    <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/DonorFunctions.aspx" >Back to Main Page</asp:hyperlink>&nbsp;&nbsp;&nbsp;
					    <asp:hyperlink id="hlnkSearch" runat="server" NavigateUrl="~/dbsearchresults.aspx" >Back to Search Results</asp:hyperlink>
					</td>
				</tr>
				<asp:Panel runat="server" ID="pnlParams" Visible="false" >
				<tr bgcolor="#FFFFFF">	
				    <td>
				        <table>
				            <tr>
				                <td align="center">
				                    Start Date        
				                 </td>
				                <td>
				                  <asp:TextBox runat="server" ID="txtFromDate"></asp:TextBox>
				                  <asp:RequiredFieldValidator runat="server" ID="reqtxtFromDt" ControlToValidate="txtFromDate" ErrorMessage="Enter Valid From Date in [MM/DD/YYYY] Format." Font-Size="Medium"></asp:RequiredFieldValidator><br />
				                  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFromDate" ErrorMessage="Enter Valid From Date in MM/DD/YYYY Format"  Font-Size="Medium" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            	</td>
				          </tr>    
				           <tr bgcolor="#FFFFFF">				    
				                <td align="center">
				                    End Date        
				                </td>
				                <td>
				                    <asp:TextBox runat="server" ID="txtToDate"></asp:TextBox>
				                    <asp:RequiredFieldValidator runat="server" ID="reqtxtToDt" ControlToValidate="txtToDate" ErrorMessage="Enter Valid To Date in [MM/DD/YYYY] Format." Font-Size="Medium"></asp:RequiredFieldValidator><br />
				                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtToDate" ErrorMessage="Enter Valid To Date in MM/DD/YYYY Format" Font-Size="Medium" ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"></asp:RegularExpressionValidator><br />
				                    <asp:CompareValidator runat="server" ID="cvRegDate" ControlToValidate="txtFromDate" ControlToCompare="txtToDate" Operator="lessThanEqual" Type="date" ErrorMessage="Start Date should be less than the End Date"></asp:CompareValidator>
				                    <asp:CustomValidator ID="CustomValidator1"  runat="server"  ControlToValidate="txtFromDate" EnableClientScript="true" ClientValidationFunction="" ></asp:CustomValidator>
				                </td>
				            </tr>
				            <tr>				    				    
				                <td  align="left">Donor Type</td>  <td  align="left">
                                    <asp:DropDownList ID="ddlDonorType" runat="server">
                                     <asp:ListItem Text="Individual" Value="IND" Selected="True"></asp:ListItem>
                                     <asp:ListItem Text="Organization" Value="OWN"></asp:ListItem>
                                    <%-- <asp:ListItem Text="Both" Value="Both"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </td> </tr> 
				            <tr>				    				    
				                <td colspan="2" align="center">
				                    <asp:Button runat="server" ID="btnSubmit" CssClass="FormButton" Text="Submit"/>
				                </td>
				            </tr>
				        </table>
				    </td>
				</tr>				
				</asp:Panel>
				<asp:Panel runat="server" ID="pnlReport">
				<tr bgcolor="#FFFFFF">
					<td colspan="2">
					    <asp:datagrid id="dgDonationList" runat="server" Width="100%"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="automemberid" AllowPaging="True" AllowSorting="True">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
							<ItemStyle Wrap="False" ></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
							<asp:TemplateColumn HeaderText="Update">
									<ItemTemplate>
										<asp:LinkButton  id="hlUpdateDonation" runat="server" Text='Update' Font-Bold="true"></asp:LinkButton>&nbsp;
										<asp:LinkButton  id="lbReceipt" runat="server" Text="Receipt"  Font-Bold="true"></asp:LinkButton>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Member ID" >
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.automemberid") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderText="Amount" SortExpression="amount DESC" >
									<ItemTemplate>
									    <div style="text-align:right">
									        <%# GetAmount(DataBinder.Eval(Container, "DataItem.amount")) %>
									   </div> 
									</ItemTemplate>                                    
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
								    <ItemTemplate>
								        <asp:Label runat="server" ID="lblDonationNumber" Text='<%# DataBinder.Eval(Container,"DataItem.DonationNumber") %>'></asp:Label>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn Visible="False">
								    <ItemTemplate>
								        <asp:Label runat="server" ID="lblDonationID" Text='<%# DataBinder.Eval(Container,"DataItem.DonationID") %>'></asp:Label>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Name" SortExpression="Name">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								
								<asp:TemplateColumn HeaderText="Transaction Number" >
									<ItemTemplate>
										<asp:Label id="lblTrnsNumber" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.transaction_number") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Donation Date" SortExpression="Convert(Varchar,donationdate,111) DESC" >
									<ItemTemplate>
										<asp:Label id="lblDonationDt" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.donationdate") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Deposit Date">
									<ItemTemplate>
										<asp:Label id="lbldepositdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DepositDate","{0:MM/dd/yyyy}") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Deposit Slip" >
									<ItemTemplate>
										<asp:Label id="lbldepostslip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DepositSlip") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Donation Type" >
									<ItemTemplate>
										<asp:Label id="lbldonationtype" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DonationType") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Event" >
									<ItemTemplate>
										<asp:Label id="lblEvent" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Event") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Chapter" >
									<ItemTemplate>
										<asp:Label id="lblchapterid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Method" >
									<ItemTemplate>
										<asp:Label id="lblMethod" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.method")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Purpose" >
									<ItemTemplate>
										<asp:Label id="lblPurpose" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.purpose")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Project" >
									<ItemTemplate>
										<asp:Label id="lblProject" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Project")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>


                                <asp:TemplateColumn HeaderText="Suppcode" >
									<ItemTemplate>
										<asp:Label id="lblSuncode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.suppcode")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>

								<asp:TemplateColumn HeaderText="Event Year" >
									<ItemTemplate>
										<asp:Label id="lblEventYear" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventYear")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>
						
					</td>
				</tr>
				</asp:Panel>
				<tr bgcolor="#FFFFFF">
					<td class="ItemCenter" align="center"><asp:label id="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red" Font-Size="Medium"></asp:label></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  class="Heading" height="15"></td>
				</tr>
 				<tr bgcolor="#FFFFFF">
					<td >
					    <asp:Label ID="lblDebug" runat="server" Visible="false"></asp:Label></td>
				</tr> 
	            </table>
</asp:Content>



 
 
 