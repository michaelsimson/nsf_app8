﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CreditCardFeesCalculation.aspx.vb" Inherits="CreditCardFeesCalculation"%>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
      <asp:HyperLink ID="HyperLink1" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink> &nbsp;&nbsp;<br /><br />

<div id="divaddupdateBankBalances" runat="server" visible = "true"  align="center">
<table cellpadding="2" width="400px"  style ="background-color:#3EAA09"  cellspacing = "2" border ="0" align="center" >
<tr><td colspan="2" align="center" class="title03" > 
  Credit Card Fees Calculation
    </td> 
</tr>
<tr><td colspan="2" align="center" class="title03" > 
  <table cellpadding="2" width="400px"  style ="background-color:#FFFFFF"  cellspacing = "2" border ="0" align="center" >
  
<tr><td width="150px" align="right"> Credit Card </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlCC" Width="150px" runat="server">
    </asp:DropDownList>
</td>
</tr>
<tr><td  align="right"> Year </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlYear"  Width="150px" runat="server"></asp:DropDownList>
</td>
</tr>

<tr>
<td colspan ="2" align="center">
   
    <asp:Button ID="BtnSubmit" OnClick ="BtnSubmit_Click" runat="server" Text="Submit" /> 
   <br />
    <asp:Label ID="lblErr" runat="server"   ForeColor ="Red" ></asp:Label>
</td>
</tr>
<tr>
<td colspan ="2" align="center">
    <asp:GridView ID="gvbankbalances" runat="server" AutoGenerateColumns="False" >
      <Columns>    
     <asp:BoundField DataField ="FiscalYear" HeaderText="FiscalYear" />
     <asp:BoundField DataField ="BankCode" HeaderText="Bank" />
      <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField ="BegBalance" HeaderText="Beginning_Balance" DataFormatString="{0:c}" />
    </Columns> 
    </asp:GridView>
</td>
</tr>

</table> </td> </tr> </table> 
</div>
</asp:Content>


