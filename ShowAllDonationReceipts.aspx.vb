Imports NorthSouth.BAL
Imports System.Net
Imports System.IO
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser

Partial Class ShowAllDonationReceipts
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim sSQL As String
    Dim dblRegFee As Double
    Dim dblDonationAmt As Double
    Protected WithEvents dgFess As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgDonation As System.Web.UI.WebControls.DataGrid
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("LoginID"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("entryToken"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Session("entryToken") = "Parent" Then
                hlinkParentRegistration.NavigateUrl = "UserFunctions.aspx"
            ElseIf Session("entryToken") = "Donor" Then
                hlinkParentRegistration.NavigateUrl = "DonorFunctions.aspx"
            ElseIf Session("entryToken") = "Volunteer" Then
                hlinkParentRegistration.NavigateUrl = "VolunteerFunctions.aspx"
                hlnkSearch.Visible = True
                hlnkSearch.NavigateUrl = "SearchDonationReceipt.aspx"
            End If
            If Request.QueryString("Type") = "Save" Then
                hlnkSearch.Visible = False
                hlinkParentRegistration.Visible = False
                btnPrint.Visible = False
            End If
            If Request.QueryString("Type") = "Multiple" Then
                hlnkSearch.Visible = False
                hlinkParentRegistration.Visible = False
                btnPrint.Visible = False
            End If
            GetAllDonationReceipts()
        End If
    End Sub
    Protected Function GetTaxablePercentage(ByVal EventID As Integer) As Decimal
        Dim iTaxablePercentage As Decimal
        If EventID = 1 Or EventID = 2 Then
            strSql = " SELECT taxdedregional, "
            strSql = strSql & " taxdednational from contestcategory "
            strSql = strSql & " where contestyear=" & Request.QueryString("Year") & " and contestcode = 'JSB'"
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iTaxablePercentage = FormatNumber(CDbl("0" & drContestCategory("taxdednational")), 2)
            End If
            drContestCategory.Close()
        ElseIf EventID > 2 Then
            strSql = " select top 1 TaxDedRegional  from eventFees where EventYear =" & Request.QueryString("Year") & " and eventID=" & EventID & " and (ProductGroupCode='SB' OR ProductGroupCode='MB') "
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iTaxablePercentage = FormatNumber(CDbl("0" & drContestCategory("TaxDedRegional")), 2)
            End If
            drContestCategory.Close()
        End If
        Return iTaxablePercentage
    End Function
    Protected Function GetDonationAmount(ByVal EventID As Integer, ByVal RegFee As Double) As Decimal
        Dim iDonationAmount As Decimal
        If EventID = 1 Or EventID = 2 Then
            strSql = " SELECT taxdedregional, "
            strSql = strSql & " taxdednational from contestcategory "
            strSql = strSql & " where contestyear=" & Request.QueryString("Year") & " and contestcode = 'JSB'"
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iDonationAmount = FormatNumber((drContestCategory("taxdednational") * RegFee) / 100, 2)
            End If
            drContestCategory.Close()
        ElseIf EventID > 2 Then
            strSql = " select top 1 TaxDedRegional  from eventFees where EventYear =" & Request.QueryString("Year") & " and eventID=" & EventID & " and (ProductGroupCode='SB' OR ProductGroupCode='MB') "
            Dim drContestCategory As SqlDataReader
            drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drContestCategory.Read() Then
                iDonationAmount = FormatNumber((drContestCategory("TaxDedRegional") * RegFee) / 100, 2)
            End If
            drContestCategory.Close()
        End If
        Return iDonationAmount
    End Function
    Private Sub GetAllDonationReceipts()
        Try

            Dim dtCurrDate As DateTime
            dtCurrDate = System.DateTime.Now
            Dim objChapters As New Chapter
            Dim dsChapters As New DataSet
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

            If dsChapters.Tables.Count > 0 Then
                ddlChapter.DataSource = dsChapters.Tables(0)
                ddlChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                ddlChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlChapter.DataBind()
                ddlChapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            End If
            If Len(Session("LoginChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("LoginChapterID")
                ddlChapter.Enabled = False
            End If
            If Session("RoleID") = "5" Then
                ddlChapter.Items.Clear()
                Dim strSql As String
                strSql = "Select chapterid, chaptercode, state from chapter "
                strSql = strSql & " where clusterid in (Select clusterid from "
                strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
                strSql = strSql & " order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))
                Dim drChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drChapters.Read())
                    ddlChapter.Items.Add(New ListItem(drChapters(1).ToString(), drChapters(0).ToString()))
                End While
                ddlChapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                ddlChapter.Enabled = True
            End If

            Session("sSQL") = ""
            sSQL = "Select ROW_NUMBER() Over (ORDER BY LastName,FirstName) as RowNumber,"
            sSQL = sSQL & " AutoMemberID,"
            sSQL = sSQL & " IndSpouse.DONORTYPE,FirstName,"
            sSQL = sSQL & " MiddleInitial,LastName,"
            sSQL = sSQL & " Address1,Address2,State,Zip,City,EMail,"
            sSQL = sSQL & " HPhone, LiasonPerson, ReferredBy, Chapter"
            sSQL = sSQL & " from IndSpouse"
            sSQL = sSQL & " WHERE "
            sSQL = sSQL & " IndSpouse.DeletedFlag <> 'Yes' and  "
            If Request.QueryString("Email") = "N" Then
                sSQL = sSQL & "  ( isnull(email,'')='' OR ValidEmailFlag is not null) AND "
            Else
                sSQL = sSQL & " ( isnull(EMAIL,'') <>'' AND ValidEmailFlag is null) AND "
            End If
            strSql = strSql & "  DonorType IN ('IND','SPOUSE') AND "
            sSQL = sSQL & " IndSpouse.Automemberid in"
            sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
            sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.AutoMemberiD "
            sSQL = sSQL & " AND DONORTYPE IN('IND','SPOUSE') "
            If CInt(Request.QueryString("Year")) = System.DateTime.Now.Year Then
                sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
            Else
                sSQL = sSQL & "  and DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '31-DEC-" & Request.QueryString("Year") & "'"
            End If
            sSQL = sSQL & " )"
            Dim blnCheck As Boolean = False
            Dim NSFChapter As String
            If Not Request.QueryString("Chapter") Is Nothing Then
                NSFChapter = Request.QueryString("Chapter")
            End If
            If Session("RoleID") = "5" Then
                Dim i As Integer
                If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                    sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                    blnCheck = True
                Else
                    For i = 1 To ddlChapter.Items.Count - 1
                        If Len(NSFChapter) > 0 And NSFChapter <> "Select Chapter" Then
                            NSFChapter = NSFChapter & ",'" & ddlChapter.Items(i).Text & "'"
                        Else
                            NSFChapter = "'" & ddlChapter.Items(i).Text & "'"
                        End If
                    Next
                    sSQL = sSQL & " and CHAPTER IN (" & NSFChapter & ")"
                    blnCheck = True
                End If
            Else
                If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                    sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                    blnCheck = True
                End If
            End If
            If (blnCheck = False) Then
                If Session("RoleID") = "5" Then
                    sSQL = "Select ROW_NUMBER() Over (ORDER BY LastName,FirstName) as RowNumber,MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,"
                    sSQL = sSQL & " LastName,Address1,Address2,City,State,Zip,EMail,HPhone,LiasonPerson,ReferredBy,"
                    sSQL = sSQL & " Chapter from IndSpouse WHERE DeletedFlag <>'Yes' and  "
                    If Request.QueryString("Email") = "N" Then
                        sSQL = sSQL & "   isnull(email,'')='' AND "
                    Else
                        sSQL = sSQL & "  isnull(EMAIL,'') <>'' AND "
                    End If
                    strSql = strSql & "  DonorType IN ('IND','SPOUSE') AND "
                    sSQL = sSQL & " IndSpouse.Automemberid in"
                    sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
                    sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.AutoMemberiD "
                    sSQL = sSQL & " AND DONORTYPE IN('IND','SPOUSE') "
                    If CInt(Request.QueryString("Year")) = System.DateTime.Now.Year Then
                        sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
                    Else
                        sSQL = sSQL & "  and DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '31-DEC-" & Request.QueryString("Year") & "'"
                    End If
                    sSQL = sSQL & " )"
                    sSQL = sSQL & " AND Chapter IN(" & NSFChapter & ") ORDER BY LastName,FirstName ASC"
                Else
                    sSQL = "Select ROW_NUMBER() Over (ORDER BY LastName,FirstName) as RowNumber,MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,"
                    sSQL = sSQL & " LastName,Address1,Address2,City,State,Zip,EMail,HPhone,"
                    sSQL = sSQL & " LiasonPerson,ReferredBy,Chapter from "
                    sSQL = sSQL & " IndSpouse WHERE DeletedFlag <>'Yes' AND "
                    If Request.QueryString("Email") = "N" Then
                        sSQL = sSQL & "   isnull(email,'')='' AND "
                    Else

                        sSQL = sSQL & "  isnull(EMAIL,'') <>'' AND "
                    End If
                    strSql = strSql & "  DonorType IN ('IND','SPOUSE') AND "
                    sSQL = sSQL & " IndSpouse.Automemberid in"
                    sSQL = sSQL & " (SELECT DISTINCT MEMBERID FROM donationsinfo "
                    sSQL = sSQL & " WHERE Donationsinfo.MemberiD = IndSpouse.AutoMemberiD "
                    sSQL = sSQL & " AND DONORTYPE IN('IND','SPOUSE') "
                    If CInt(Request.QueryString("Year")) = System.DateTime.Now.Year Then
                        sSQL = sSQL & " AND DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
                    Else
                        sSQL = sSQL & "  and DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '31-DEC-" & Request.QueryString("Year") & "'"
                    End If
                    sSQL = sSQL & " )"
                    sSQL = sSQL & " ORDER BY LastName,FirstName ASC"
                End If
            End If
            Session("sSQL") = sSQL
            'Response.Write(sSQL)
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim dsDonation As New DataSet
            Dim tableName As String() = New String(0) {}
            tableName(0) = "Donation"
            SqlHelper.FillDataset(conn, CommandType.Text, sSQL, dsDonation, tableName)
            Dim drDonation As SqlDataReader
            drDonation = SqlHelper.ExecuteReader(conn, CommandType.Text, sSQL)
            Dim j As Integer
            Dim m_OutputFile As String
            Dim bIsEmailExist As Boolean
            Dim dsEmail As New DataSet
            If dsDonation.Tables(0).Rows.Count > 0 Then
                pnlData.Visible = True
                pnlMessage.Visible = False
                rptReceipt.DataSource = dsDonation
                rptReceipt.DataBind()
                If Request.QueryString("Type") <> "Save" Then
                    If drDonation.Read() Then
                        While drDonation.Read()
                            Try
                                bIsEmailExist = False
                                'If drDonation("RowNumber") > 396 Then ''Added on 06-02-2014 to skip first n records to Send email
                                'strSql = "SELECT * FROM INDSPOUSE WHERE AUTOMEMBERID=" & drDonation("AutoMemberID")
                                'dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                                Dim Valid_Email As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Email FROM INDSPOUSE WHERE AUTOMEMBERID=" & drDonation("AutoMemberID"))

                                'If dsDonation.Tables(0).Rows.Count > 0 Then
                                If Valid_Email <> "" Then
                                    If Len(Valid_Email) > 0 Then
                                        'If Len(Trim(dsDonation.Tables(0).Rows(0)("Email"))) > 0 Then
                                        m_OutputFile = Server.MapPath("DonorReceipts/Email_DonationReceipt_" & drDonation("AutoMemberID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm")
                                        bIsEmailExist = True
                                    Else
                                        m_OutputFile = Server.MapPath("DonorReceipts/NoEmail_DonationReceipt_" & drDonation("AutoMemberID") & "_" & DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & DateTime.Now.Hour & "_" & DateTime.Now.Minute & ".htm")
                                        bIsEmailExist = False
                                        ' End If
                                    End If
                                End If
                                'End If

                                Dim sw As New StreamWriter(m_OutputFile, False)
                                If Not drDonation("AutoMemberID") Is Nothing Then
                                    Server.Execute("ShowDonationReceipt.aspx?MemberID=" & drDonation("AutoMemberID") & "&Year=" & Request.QueryString("Year") & "&Type=Multiple", sw)
                                Else
                                    Server.Execute("ShowDonationReceipt.aspx?Year=" & Request.QueryString("Year") & "&Type=Multiple", sw)
                                End If
                                sw.Flush()
                                sw.Close()
                                Dim strBody As String
                                strBody = "Dear donor," & vbCrLf
                                strBody = strBody & vbCrLf
                                strBody = strBody & "            Attached you will find a receipt for your donation(s).  "
                                strBody = strBody & " Please keep this for your tax records.  "
                                strBody = strBody & " If you have a Login ID and password, "
                                strBody = strBody & " you can also get a receipt by going to the NSF website.  "
                                strBody = strBody & " We are grateful for your contribution." & vbCrLf
                                strBody = strBody & vbCrLf
                                strBody = strBody & "Thank you. " & vbCrLf
                                strBody = strBody & vbCrLf
                                strBody = strBody & " With regards," & vbCrLf
                                strBody = strBody & vbCrLf
                                strBody = strBody & " NSF Fundraising Team" & vbCrLf
                                Dim sAttachment As String
                                sAttachment = m_OutputFile
                                If bIsEmailExist = True Then
                                    'Response.Write(Valid_Email & "<br />") 'dsDonation.Tables(0).Rows(0)("Email"))
                                    ' SendEmail("NSF Donation Receipt", strBody, Trim(dsDonation.Tables(0).Rows(0)("Email")), sAttachment)
                                    SendEmail("NSF Donation Receipt", strBody, Trim(Valid_Email), sAttachment)
                                End If
                                j = j + 1
                                'End If
                            Catch ex As Exception
                                '  Response.Write(ex.ToString())
                                lblMessage.Text = ex.ToString()
                            End Try
                        End While
                        drDonation.Close()
                    End If
                Else
                    Dim strFileName As String = Session("OutputFile")
                    '"DonorReceipts/No_Email_DonorReceipts_" & Session("LoginID") & System.DateTime.Now.ToString("MMMM-dd-yyyy") & "_" & System.DateTime.Now.Hour & "_" & System.DateTime.Now.Minute & ".pdf"
                    ' Dim ms As New MemoryStream
                    Dim pgSize As New iTextSharp.text.RectangleReadOnly(612, 792)
                    Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20.0F, 10.0F, 30.0F, 0.0F)
                    Dim htmlparser As New HTMLWorker(pdfDoc)
                    Dim output As New FileStream(Server.MapPath(strFileName), FileMode.Create)
                    PdfWriter.GetInstance(pdfDoc, output)
                    pdfDoc.Open()
                    Dim i As Integer
                    For i = 0 To rptReceipt.Items.Count - 1 Step i + 1
                        Dim sw As New StringWriter()
                        Dim hw As New HtmlTextWriter(sw)
                        pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4)
                        pdfDoc.NewPage()
                        rptReceipt.Items(i).RenderControl(hw)
                        Dim repeaterTable As String = sw.ToString()
                        Dim sread As StringReader = New StringReader(repeaterTable)
                        htmlparser.Parse(sread)
                    Next
                    ' output.Flush()
                    'output.Close()
                    pdfDoc.Close()


                    'Response.Clear()
                    'Response.ContentType = "application/octet-stream"
                    'Response.AddHeader("content-disposition", "attachment;filename= " & strFileName)
                    'Response.Buffer = True
                    'Response.Clear()
                    'Dim bytes() As Byte = ms.ToArray()
                    'Response.OutputStream.Write(bytes, 0, bytes.Length)
                    'Response.OutputStream.Flush()
                End If
            Else
                pnlData.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No Donations Found."
            End If
        Catch ex As Exception
            lblMessage.Text = ex.ToString()
        End Try
    End Sub
    Protected Function GetRegistrationDataSource(ByVal MemberID As Double) As DataSet
        'Registration Fee
        dblRegFee = 0
        Dim conn As New SqlConnection(Application("ConnectionString"))

        Dim dtCurrDate As DateTime
        dtCurrDate = DateTime.Now

        strSql = " Select chapterid,"
        strSql = strSql & " memberid, eventid,""Fee"" as RegFee, Convert(Varchar,""Payment Date"",101) as DonationDate"
        strSql = strSql & "  from nfg_transactions "
        strSql = strSql & "  where ID Is null And fee > 0"
        strSql = strSql & " and MemberID = " & MemberID
        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then
            strSql = strSql & " AND ""Payment Date"" BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
        Else
            strSql = strSql & "  and ""Payment Date"" BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '31-DEC-" & Request.QueryString("Year") & "'"
        End If
        strSql = strSql & "  ORDER BY ""Payment Date"""
        Dim dsRegistrationFee As New DataSet
        Dim tblTrans As String() = New String(0) {}
        tblTrans(0) = "NFGTRANSACTION"
        SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsRegistrationFee, tblTrans)
        Return dsRegistrationFee
    End Function
    Protected Function GetDonorName(ByVal FirstName As String, ByVal LastName As String) As String
        Return FirstName & " " & LastName
    End Function

    Protected Function GetDonationDataSource(ByVal MemberiD As Double) As DataSet
        'Donations
        dblDonationAmt = 0
        Dim dtCurrDate As DateTime
        dtCurrDate = DateTime.Now

        strSql = " SELECT A.MemberID, "
        strSql = strSql & " IsNull(amount,0) as amount, transaction_number,DonationID,ChapterID,DonorType, Convert(Varchar,donationdate,101) donationdate, method, Event,DonationNumber "
        strSql = strSql & " FROM DonationsInfo a "
        strSql = strSql & " where A.MemberID=" & MemberiD

        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then
            strSql = strSql & " AND DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '" & dtCurrDate.Day & "-" & dtCurrDate.ToString("MMM") & "-" & dtCurrDate.Year & "'"
        Else
            strSql = strSql & " AND DonationDate BETWEEN '01-JAN-" & Request.QueryString("Year") & "' AND '31-DEC-" & Request.QueryString("Year") & "'"
        End If
        'Added DonorType 
        strSql = strSql & " AND DonorType IN ('IND','SPOUSE') ORDER BY DonationDate"
        Dim tableName As String() = New String(0) {}
        tableName(0) = "Donation"
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim dsDonation As New DataSet
        SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsDonation, tableName)
        Return dsDonation
    End Function

    Protected Sub rptReceipt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptReceipt.ItemDataBound
        Try
            Dim dgFees As DataGrid = CType(e.Item.FindControl("dgFess"), DataGrid)
            If dgFees.Items.Count <= 0 Then
                Dim lblRegFee As Label
                lblRegFee = CType(e.Item.FindControl("lblTaxDeductibleAmt"), Label)
                If Not lblRegFee Is Nothing Then
                    lblRegFee.Visible = False
                    Dim lblTaxableDonation As Label
                    lblTaxableDonation = CType(e.Item.FindControl("lblTaxDonation"), Label)
                    lblTaxableDonation.Text = "1) Tax-deductible Amount from Donation(s)"
                End If
                dgFees.Visible = False
            End If
            Dim txtRegistrationFee As New HiddenField
            ' txtRegistrationFee = CType(e.Item.FindControl("txtRegFee"), HiddenField)
            If Not txtRegistrationFee Is Nothing Then
                txtRegistrationFee.Value = FormatNumber(dblRegFee, 2)
            End If
            Dim txtDonationAmt As New HiddenField
            'txtDonationAmt = CType(e.Item.FindControl("txtDonation"), HiddenField)
            If Not txtDonationAmt Is Nothing Then
                txtDonationAmt.Value = FormatNumber(dblDonationAmt, 2)
            End If

            Dim dblGrandTotal As Double
            dblGrandTotal = FormatNumber(CDbl(txtRegistrationFee.Value) + CDbl(txtDonationAmt.Value), 2)

            'dblGrandTotal = FormatNumber(CDbl("0" & txtRegistrationFee.Value) + CDbl("0" & txtDonationAmt.Value), 2)
            Dim lblToTal As Label
            lblToTal = CType(e.Item.FindControl("lblGrandTotal"), Label)
            If Not lblToTal Is Nothing Then
                lblToTal.Text = FormatNumber(dblGrandTotal, 2)
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub dgFess_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFess.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim donationAmount As Double = GetDonationAmount(DataBinder.Eval(e.Item.DataItem, "EventID"), DataBinder.Eval(e.Item.DataItem, "RegFee"))
            dblRegFee += donationAmount
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            e.Item.Cells(3).Font.Bold = True
            e.Item.Cells(3).Font.Size = 12
            e.Item.Cells(3).Text = "Sub Total: " & FormatNumber(dblRegFee, 2)
        End If
    End Sub
    Protected Sub dgDonation_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDonation.ItemDataBound
        e.Item.Cells(1).Attributes.Add("align", "right")
        e.Item.Cells(2).Attributes.Add("align", "right")
        e.Item.Cells(3).Attributes.Add("align", "right")
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim donationAmount As Double = DataBinder.Eval(e.Item.DataItem, "Amount")
            dblDonationAmt += donationAmount
        ElseIf e.Item.ItemType = ListItemType.Footer Then
            e.Item.Cells(3).Font.Bold = True
            e.Item.Cells(3).Font.Size = 12
            e.Item.Cells(3).Text = "Sub Total: " & FormatNumber(dblDonationAmt, 2)
        End If
    End Sub
    Protected Function GetFromDate() As String
        Return "01/01/" & Request.QueryString("Year")
    End Function
    Protected Function GetToDate() As String
        Dim strToDate As String
        Dim dtCurrDate As DateTime
        dtCurrDate = System.DateTime.Now
        If CInt(Request.QueryString("Year")) = DateTime.Now.Year Then
            strToDate = dtCurrDate.ToString("MM") & "/" & dtCurrDate.Day & "/" & dtCurrDate.Year
        Else
            strToDate = "12/31/" & Request.QueryString("Year")
        End If
        Return strToDate
    End Function
    Protected Function GetDate() As String
        Return DateTime.Now.ToString("MMMM dd, yyyy")
    End Function
    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sAttachment As String)
        Dim email As New MailMessage
        email.From = New MailAddress("nsffundraising@gmail.com")
        'sMailTo = "chitturi9@gmail.com"
        email.To.Add(sMailTo)
        email.Subject = sSubject
        Dim attach As New Attachment(sAttachment)
        email.Attachments.Add(attach)
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody

        Dim client As New SmtpClient()
        Dim ok As Boolean = True

        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

        'client.Host = host
        'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
        Try
            client.Send(email)
        Catch e As Exception
            ok = False
        Finally
            email.Dispose()
        End Try
    End Sub
End Class
