<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowParticipantCertificatePDF.aspx.vb" Inherits="ShowParticipantCertificatePDF" %>
<%@ Reference Page="~/GenerateParticipantCertificates.aspx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>  
        <style type="text/css" media="screen" >
        <!-- 
        p.MsoNormal, li.MsoNormal, div.MsoNormal
	    {
	        margin:0in;
	        margin-bottom:.0001pt;
	        text-autospace:none;
	        font-size:10.0pt;
	        font-family:"Times New Roman","serif";
	    }  
        @page Section1
	        {
	            size:11.0in 8.5in;
	            margin:.5in 1.0in .5in 1.0in;
	        }
        div.Section1
	        {
	            page:Section1;
	        }
        -->
        </style>
        <script language="javascript" type="text/javascript">
        function printdoc()
        {
            document.getElementById('btnPrint').style.visibility="hidden";
            document.getElementById('hlnkMainMenu').style.visibility="hidden";            
            window.print();
            document.getElementById('btnPrint').style.visibility="visible";
            document.getElementById('hlnkMainMenu').style.visibility="visible";            
            return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="Section1">
        <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />
            <asp:Repeater runat="server" ID="rptCertificate" OnItemDataBound="rptCertificate_OnItemDataBound">
                <ItemTemplate>
             <div class="Section1" style="page-break-before:always">     
               <table cellspacing="0" cellpadding="0" width="97%"  align="center" border="0" >                
                    <tr>
                  <td colspan="8">
                  <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0">
                  <tr>
                  <td rowspan="3" align="left" width="20%">
                  
                  <asp:Image runat="server" ID="imgThinkingMan" ImageUrl="http://www.northsouth.org/app8/Images/nsf.jpg"/>                            
                  </td>
                  
                  <td  rowspan="5" align="left" width="80%">
                  <asp:Image runat="server" ID="imgHeader"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg1A.jpg"/><br />
                     <% If Session("SelChapterID") = 1 Then%>
                            <asp:Image runat="server" ID="imgTitleNational"  ImageUrl="http://www.northsouth.org/app8/Images/image007_National.gif"/>
                        <%else %>
                            <asp:Image runat="server" ID="imgTitle"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg2A.jpg"/>
                            <%end if %>
                  </td>
                  </tr>

               
                  </table>
                  </td>
                  </tr>
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                           <br />
                        </td>
                    </tr>
  
                            
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle" ForeColor="#761e28"  Font-Names="Script MT Bold" Text="Certificate of Participation" Font-Bold="true" Font-Size="36px"></asp:Label>
                        </td>
                    </tr>                   
                         <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                           <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle2" ForeColor="#761e28"  Text="awarded to" Font-Names="Script MT Bold" Font-Bold="true" Font-Size="26px" ></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8"><br /></td>
                    </tr>  
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblName"  ForeColor="Blue" Font-Names="Times-Roman" Font-Bold="true" Font-Size="22px" Text='<%# DataBinder.Eval(Container,"DataItem.First_Name").ToString() & " " & DataBinder.Eval(Container,"DataItem.Last_Name").ToString() %>'></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8"><br /></td>
                    </tr>  
                    
                    <tr>
                        <td colspan="8" width="100%" align="left" style="text-align:justify;">
                            <b>
                           <asp:Label runat="server" ID="lblcomm"  Font-Bold="true" >for successfully participating in the 
                             <%-- <% If Session("SelChapterID") = 1 Then%>
                               <asp:Label runat="server" ID="Label2" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ExamName") %>'/>
                              <%Else%>--%>
                            <%--  <%End If%>--%>
                           
                            <asp:Label runat="server" ID="lblContestYear"  Font-Bold="true" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.contestyear")) %>'></asp:Label>
                            
                            <% If Session("SelChapterID") = 1 Then%>
                            National Championship Finals held <asp:Label runat="server" ID="Label3"  Font-Bold="true" Text='<%# GetContestDates(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ContestDate")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ProductCnt"))) %>'></asp:Label> 
                            <%Else%>
                            Regional  
                            <%End If%>
                            <asp:Label runat="server" ForeColor="Blue" ID="lblProduct" Font-Bold="true" Text='<%# GetProductName(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber"))) %>'/>
                            contests held at the 
                            <asp:Label runat="server" ForeColor="Blue" ID="lblLocation" Font-Bold="true" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChapterCode")) %>'></asp:Label>
                             <% If Not Session("SelChapterID") = 1 Then%>
                             Chapter.
                            <%End If%>
                           
                              </asp:Label></b></td></tr><tr>
                        <td colspan="8"><br /></td>
                    </tr> 
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                            
                             <asp:Label runat="server" ID="lblNSF"  Font-Italic="true"> North South Foundation (NSF) is a non-profit organization involved in implementing educational programs for children in North America and India. The Foundation believes that this world can be a better place to live if the children of today are better prepared to be good citizens of tomorrow. Toward this end, the Foundation encourages children to endeavor to become the best they can be, by giving their best. Further, while it is self-evident that all humans are created equal, it is education that is paramount to actually realizing the rights of equality including life, liberty and the pursuit of happiness as the Founding Fathers of this Nation envisaged more than two hundred years ago.
                             </asp:Label></td></tr><tr>
                        <td colspan="8"><br /></td>
                    </tr>
                  
                    <tr>
                        <td colspan="8" >
                           <table cellspacing="2" cellpadding="2" width="98%" border="0" >                
                                <tr >
                                <td  width="40%">
                                     
                                </td>
                                
                                    <td rowspan="4" width="20%" align="center" valign="middle">
                        <br />
                                <asp:Image runat="server" ID="Image1" ImageUrl="http://www.northsouth.org/app8/Images/CertImg3A.jpg" />
                   

                                    </td> 
                                
                                     <td  width="40%">
                                     
                                     </td>
                                </tr>
                                     <tr >
                                    <td align="left" >
                              
                                    
                  <% If Session("SelChapterID") = 1 Then%>
                                     <img name="leftsign1"  runat="server" id="leftsign1" src='<%# ShowImage(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureImage")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.SpacerURL")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureImagePath"))) %>' alt="LeftSign" width="150" height="60" />
                                     <%Else%>
                                         <img name="leftsign1"  runat="server" id="Img1" src='<%# GetLeftShowImage(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureImage")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.SpacerURL")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureImagePath")),Session("LeftSignatureImage")) %>' alt="LeftSign" width="150" height="60" />

<%--                                         <img name="leftsign" src="<%=Session("LeftSignatureImage") %>" alt="LeftSign" width="150" height="60" />
--%>                                     <%end if %><br />
                                     <asp:Image runat="server" ID="Image2"  ImageUrl="http://www.northsouth.org/app8/Images/Signline.jpg" Width="90%" />
                                    </td> 
                                
                             
                                
                                    <td align="left" >
                                       <% If Session("SelChapterID") = 1 Then%>
                 <img name="rightsign" id="rightsing1" runat="server" src='<%# ShowImage(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureImage")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.SpacerURL")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureImagePath"))) %>' alt="LeftSign" width="150" height="60"/>
                                     <%Else%>
                                      <img name="_rightsign" id="rightsing2" src='<%# GetLeftShowImage(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureImage")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.SpacerURL")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureImagePath")),Session("RightSignatureImage")) %>' alt="LeftSign" width="150" height="60"/>
                                     <%end if %><br />

                                      <asp:Image runat="server" ID="Image3"  ImageUrl="http://www.northsouth.org/app8/Images/Signline.jpg" Width="90%" />
                                    </td> 
                                </tr>
                     
                          <%-- <tr>
                                    <td ><br /></td>
                                    <td ><br /></td>
                                </tr>--%>
                                <tr>
                                    <td align="left"  valign="top"> 
                                       <% If Session("SelChapterID") <> 1 Then%>
                                    <asp:Label runat="server" ID="Label11"  Font-Size="14" Font-Bold="true"  Text='<%# GetContestNo(ProcessMyDataItem(DataBinder.Eval(Container, "DataItem.ChildNumber")), ProcessMyDataItem(DataBinder.Eval(Container, "DataItem.LeftTitle")), Session("LeftTitle"))%>'></asp:Label>&nbsp; <asp:Label runat="server" ID="Label12"  Font-Size="14" Font-Bold="true" Text='<%# GetContestNo(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureName")),Session("LeftSignatureName")) %>'></asp:Label><%--
                                    <asp:Label runat="server" ID="lblLeftTitle" Font-Size="12" Font-Bold="true"  Text='<%# Session("LeftTitle") %>'></asp:Label>&nbsp;
                                    <asp:Label runat="server" ID="lblLeftSignature" Font-Size="12" Font-Bold="true" Text='<%# Session("LeftSignatureName") %>'></asp:Label>                                        
                           --%><%else %><asp:Label runat="server" ID="Label4"  Font-Size="14" Font-Bold="true"  Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftTitle")) %>'></asp:Label>&nbsp; <asp:Label runat="server" ID="Label5"  Font-Size="14" Font-Bold="true" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureName")) %>'></asp:Label><%End If%></td><td align="left" valign="top" >

                                      <% If Session("SelChapterID") <> 1 Then%>
                                        <asp:Label runat="server" ID="lblRightTitle" Font-Size="14"  Font-Bold="true" Text='<%# GetContestNo(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightTitle")),Session("RightTitle")) %>'></asp:Label>&nbsp; <asp:Label runat="server" ID="lblRightSignature"  Font-Size="14"  Font-Bold="true" Text='<%# GetContestNo(DataBinder.Eval(Container,"DataItem.ChildNumber"),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureName")),Session("RightSignatureName")) %>'></asp:Label><%else %><asp:Label runat="server" ID="lblRightTitle1" Font-Size="14"  Font-Bold="true" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightTitle")) %>'></asp:Label>&nbsp; <asp:Label runat="server" ID="lblRightTitle2"  Font-Size="14"  Font-Bold="true" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureName")) %>'></asp:Label><%End If%></td></tr><tr>
                                    <td align="left"  valign="top" >
                                           <% If Session("SelChapterID") <> 1 Then%>                                  
                                    <asp:Label runat="server" ID="lblSigTitle" Text='<%# GetContestNo(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureTitle")),Session("LeftSignatureTitle")) %>'></asp:Label><%else %><asp:Label runat="server" ID="lblSigTitle1" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.LeftSignatureTitle")) %>'></asp:Label><%End If%></td><td align="left" valign="top" >
               <% If Session("SelChapterID") <> 1 Then%>
                                    <asp:Label runat="server" ID="lblRightSigTitle"  Text='<%# GetContestNo(ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.ChildNumber")),ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureTitle")),Session("RightSignatureTitle")) %>'></asp:Label><%else %><asp:Label runat="server" ID="lblRightSigTitle1" Text='<%# ProcessMyDataItem(DataBinder.Eval(Container,"DataItem.RightSignatureTitle")) %>'></asp:Label><%End If%></td></tr></table></td></tr></table></div></ItemTemplate></asp:Repeater><asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" class="tblMain" cellpadding="0" width="100%"  align="left" border="0" >
                <tr >
                    <td class="Heading"><asp:Label runat="server" ID="lblMessage" ></asp:Label></td></tr></table></asp:Panel></div></form></body></html>