Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Net.Mail
Imports System.IO
Imports System.Text.RegularExpressions
Imports Microsoft.ApplicationBlocks.Data


Namespace VRegistration
    Partial Class MakeEmailIdsUnique
        Inherits System.Web.UI.Page
 
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=v")
            End If
            If Session("entryToken").ToString() = "Parent" Then
                hlinkParentRegistration.Visible = True
            ElseIf Session("entryToken").ToString() = "Volunteer" Then
                hlinkVolunteerRegistration.Visible = True
            ElseIf Session("entryToken").ToString() = "Donor" Then
                hlinkDonorFunctions.Visible = True
            End If

            If Not Page.IsPostBack Then
                PnlNew.Visible = True
                lblError.Text = ""
                'lblSpErr.Text = ""
                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim cnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "Select count(*) from IndSpouse where email = '" & Session("userid") & "'")
                If cnt = 0 Then
                    'PnlNo.Visible = True
                    lblError.Text = "Thank you for your offer to volunteer. The email address you used to login needs to be linked to your profile, if it exists.  By profile, we mean your name, address, contact information, etc.  Only you know if it exists.  If you are married, your spouse may have entered your  profile, but we don�t know that at this point.  If you know for certain that your profile  does not exist in our system,Please contact nsfcontests@northsouth.org with this error message. Thank you for your kind assistance."
                ElseIf cnt > 2 Then
                    trContinue.Visible = True
                    btncontinue.Visible = True
                ElseIf cnt = 1 Then
                    DisplayIndSpouse()
                    TrSwapEmails.Visible = True
                    If LblIndSpEmail.Text <> "" Then
                        lblError.Text = "The emails are already unique or one of them does not exist. So you cannot use this application "
                        Exit Sub
                    Else
                        tblSwapEmails.Visible = False
                        ChangeEmail(False)
                    End If
                Else
                    ChangeEmail(True)
                    'Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & Session("UserID") & "' " & IIf(LblIndSpEmail.Text <> "", " AND b.Email = '" & Session("UserID") & "'", "") & " AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
                    'Dim i As Integer = 0
                    'While readr.Read
                    '    i = 1
                    '    Session("IndId") = readr("INDID")
                    '    Session("SpouseID") = readr("SpouseID")
                    'End While
                    'readr.Close()
                    'If i > 0 Then
                    '    PnlNew.Visible = True
                    '    Dim indRw As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS Name, b.user_pwd FROM IndSpouse AS a CROSS JOIN login_master AS b WHERE b.user_email = '" & Session("UserID") & "' AND a.AutoMemberID = " & Session("IndId") & "")
                    '    While indRw.Read()
                    '        lblIndName.Text = indRw("Name")
                    '        lblIndEmail.Text = Session("UserID")
                    '        'lblIndPwd.Text = indRw("user_pwd")
                    '    End While
                    '    indRw.Close()
                    '    Dim indSpRw As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS Name, a.email,b.user_pwd FROM IndSpouse AS a CROSS JOIN login_master AS b WHERE " & IIf(LblIndSpEmail.Text <> "", "b.user_email = '" & Session("UserID") & "' AND", "") & "  a.AutoMemberID = " & Session("SpouseID") & "")
                    '    While indSpRw.Read()
                    '        lblIndSpName.Text = indSpRw("Name")
                    '        LblIndSpEmail.Text = indSpRw("email") 'Session("UserID")
                    '        ' lblIndSpPwd.Text = indSpRw("user_pwd")
                    '    End While
                    '    indSpRw.Close()
                    'End If
                End If
            End If
        End Sub
        Private Sub ChangeEmail(ByVal IndFlag As Boolean)
            Try

          
                ddlChange.Enabled = True
                ddlChange1.Enabled = True
                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & Session("UserID") & "' " & IIf(IndFlag = True, " AND b.Email = '" & Session("UserID") & "'", "") & " AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
                Dim i As Integer = 0
                While readr.Read
                    i = 1
                    Session("IndId") = readr("INDID")
                    Session("SpouseID") = readr("SpouseID")
                End While
                readr.Close()
                If i > 0 Then
                    PnlNew.Visible = True
                    Dim indRw As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS Name, b.user_pwd FROM IndSpouse AS a CROSS JOIN login_master AS b WHERE b.user_email = '" & Session("UserID") & "' AND a.AutoMemberID = " & Session("IndId") & "")
                    While indRw.Read()
                        lblIndName.Text = indRw("Name")
                        lblIndEmail.Text = Session("UserID")
                        'lblIndPwd.Text = indRw("user_pwd")
                    End While
                    indRw.Close()
                    Dim indSpRw As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS Name, a.email,b.user_pwd FROM IndSpouse AS a CROSS JOIN login_master AS b WHERE " & IIf(IndFlag = True, " b.user_email = '" & Session("UserID") & "' AND", "") & "  a.AutoMemberID = " & Session("SpouseID") & "")
                    While indSpRw.Read()
                        lblIndSpName.Text = indSpRw("Name")
                        LblIndSpEmail.Text = IIf(indSpRw("email") Is DBNull.Value, "", indSpRw("email")) 'Session("UserID")
                        ' lblIndSpPwd.Text = indSpRw("user_pwd")
                    End While
                    indSpRw.Close()
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End Sub
        Private Sub DisplayIndSpouse()
            Dim Sp_flag As Boolean = False
            Dim SpRec_flag As Boolean = False
            'TrPassword.Visible = False
            ddlChange.Enabled = False
            ddlChange1.Enabled = False
            Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT a.FirstName + ' ' + a.LastName AS IndName,b.FirstName + ' ' + b.LastName AS SpouseName,a.AutoMemberID AS INDID, b.AutoMemberID AS SpouseID,a.Email as IndEmail,b.Email as SpouseEmail FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON b.Relationship = a.AutoMemberID WHERE a.Email = '" & Session("UserID") & "' AND b.Email <> '" & Session("UserID") & "' AND a.Relationship = 0 AND a.DonorType = 'IND' AND b.DonorType = 'SPOUSE'")
            Dim i As Integer = 0

            While readr.Read
                i = 1
                Session("IndId") = readr("INDID")
                Session("SpouseID") = readr("SpouseID")
                lblIndName.Text = readr("IndName")
                lblIndEmail.Text = readr("IndEmail")
                lblIndSpName.Text = readr("SpouseName")
                LblIndSpEmail.Text = readr("SpouseEmail")
            End While

            If Not readr.HasRows Then
                Sp_flag = True
            End If
            readr.Close()

            If Sp_flag = True Then
                Dim readr_Sp As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT b.FirstName + ' ' + b.LastName AS IndName,a.FirstName + ' ' + a.LastName AS SpouseName,b.AutoMemberID AS INDID, a.AutoMemberID AS SpouseID,b.Email as IndEmail,a.Email as SpouseEmail FROM IndSpouse AS a INNER JOIN IndSpouse AS b ON a.Relationship = b.AutoMemberID WHERE a.Email = '" & Session("UserID") & "' AND b.Email <> '" & Session("UserID") & "' AND b.Relationship = 0 AND b.DonorType = 'IND' AND a.DonorType = 'SPOUSE'")
                While readr_Sp.Read
                    Session("IndId") = readr_Sp("INDID")
                    Session("SpouseID") = readr_Sp("SpouseID")
                    lblIndName.Text = readr_Sp("IndName")
                    lblIndEmail.Text = readr_Sp("IndEmail")
                    lblIndSpName.Text = readr_Sp("SpouseName")
                    LblIndSpEmail.Text = readr_Sp("SpouseEmail")
                End While
                If Not readr_Sp.HasRows Then
                    SpRec_flag = True
                End If
                readr_Sp.Close()
            End If

            If Sp_flag = True And SpRec_flag = True Then
                PnlNew.Visible = False
                'lblSpErr.Visible = True
                lblError.Text = "The Login Email does not have a spouse record with Valid email.<br /> Would you like to create a login for the Spouse?  ."
                btnSubmit.Enabled = False
                TrErrSp.Visible = True
                Exit Sub
            End If
        End Sub

        Protected Sub btncontinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncontinue.Click
            Response.Redirect("http://www.northsouth.org")
        End Sub

        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim NewEmail, NewPwd, NewName, chapter As String
            Dim NewId, chapterID As Integer
            Dim Update_flag As Boolean = False
            lblError.Text = ""
            If (txtIndSpEmail.Text.Length > 0 And txtIndSpPwd.Text.Length > 0) Or (txtIndEmail.Text.Length > 0 And txtIndPwd.Text.Length > 0) Then
                If Not Page.IsValid Then
                    lblError.Text = "Please Enter Valid Email and Password"
                    Exit Sub
                End If
                 
                If txtIndEmail.Text.Length < 2 And txtIndSpPwd.Text.Length > 0 And txtIndSpEmail.Text = txtIndSpCEmail.Text And txtIndSpPwd.Text = txtIndCSpPwd.Text And txtIndSpEmail.Text <> lblIndEmail.Text Then
                    NewEmail = txtIndSpEmail.Text
                    NewPwd = txtIndSpPwd.Text
                    NewId = Session("SpouseID")
                    NewName = lblIndSpName.Text
                ElseIf txtIndEmail.Text.Length > 0 And txtIndSpPwd.Text.Length < 2 And txtIndEmail.Text = txtIndCEmail.Text And txtIndPwd.Text = txtIndCPwd.Text Then
                    NewEmail = txtIndEmail.Text
                    NewPwd = txtIndPwd.Text
                    NewId = Session("IndId")
                    NewName = lblIndName.Text
                ElseIf (txtIndSpEmail.Text.Length > 0 And txtIndSpEmail.Text = lblIndEmail.Text) Or (txtIndEmail.Text.Length > 0 And txtIndEmail.Text = LblIndSpEmail.Text) Then
                    lblError.Text = "The Email already exists for the Spouse.Please Enter Unique Email."
                    Exit Sub
                Else
                    lblError.Text = "Password Mismatch"
                    Exit Sub
                End If
                Dim Strsql1 As String = "Select count(*) from Child where email = '" & NewEmail & "'"
                Dim Chcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql1)
                Dim Strsql As String = "Select count(*) from Indspouse where email = '" & NewEmail & "' AND automemberID NOT IN (" & Session("IndId") & "," & Session("SpouseID") & ")"
                '  Dim Dupcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql)
                Dim Dupcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql)
                If Dupcnt > 0 Or Chcnt > 0 Then
                    lblError.Text = "Your email exists elsewhere in the NSF database. Please contact <a href='mailto:nsfcontests@northsouth.org'><b>nsfcontests@northsouth.org</b></a> with this error message."
                    DisableOptions()
                    Exit Sub
                Else
                    Dim strsq As String = "UPDATE IndSpouse SET Email='" & NewEmail & "' WHERE automemberID =" & NewId
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsq)
                    Strsql = "Select count(*) from Login_Master where User_email = '" & NewEmail & "'"
                    Dim DupLcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, Strsql)
                    If DupLcnt = 1 Then
                        Strsql = "Update Login_Master set user_pwd ='" & NewPwd & "', ModifyDate ='" & DateTime.Now & "', ModifiedBy=" & Session("IndId") & " WHERE User_Email='" & NewEmail & "'"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsq)
                        Update_flag = True
                    ElseIf DupLcnt = 0 Then
                        Dim readr As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT Chapter, ChapterID FROM IndSpouse WHERE Email='" & NewEmail & "'")
                        While readr.Read()
                            chapter = readr("Chapter")
                            chapterID = readr("ChapterID")
                        End While
                        readr.Close()

                        Strsql = "Insert Into Login_Master(user_email, user_pwd, date_added,  user_role, user_name, chapter, active, ChapterID, CreateDate, CreatedBy) Values('"
                        Strsql = Strsql & NewEmail & "','" & NewPwd & "','" & DateTime.Now & "','Volunteer','" & NewName & "','" & chapter & "','Yes'," & chapterID & ",'" & DateTime.Now & "'," & Session("IndId") & ")"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, Strsql)
                        Update_flag = True

                    Else
                        lblError.Text = "E-Mail Has Changed in Profile but You have error in Login kindly mail to nsfcontests@northsouth.org"
                    End If
                    If Update_flag = True Then
                        lblSuccess.Text = "Change to Unique Email was done Succesfully"
                        DisableOptions()
                        DisplayIndSpouse()
                        TrSwapEmails.Visible = True
                    Else
                        lblError.Text = "E-Mail Has Changed in Profile but You have error in Login kindly mail to nsfcontests@northsouth.org"
                    End If
                    'Response.Redirect("login.aspx")
                End If
            Else
                lblError.Text = "Please Enter Valid E-Mail and Password"
            End If
        End Sub
        Private Sub DisableOptions()
            ddlChange.SelectedValue = 0
            ddlChange1.SelectedValue = 0

            txtIndEmail.Enabled = False
            txtIndPwd.Enabled = False
            txtIndCEmail.Enabled = False
            txtIndCPwd.Enabled = False

            txtIndSpEmail.Enabled = False
            txtIndSpPwd.Enabled = False
            txtIndSpCEmail.Enabled = False
            txtIndCSpPwd.Enabled = False

            txtIndEmail.Text = ""
            txtIndPwd.Text = ""
            txtIndCEmail.Text = ""
            txtIndCPwd.Text = ""

            txtIndSpEmail.Text = ""
            txtIndSpPwd.Text = ""
            txtIndSpCEmail.Text = ""
            txtIndCSpPwd.Text = ""
        End Sub
        Protected Sub ddlChange_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChange.SelectedIndexChanged
            lblError.Text = ""
            If ddlChange.SelectedValue = "1" Then
                If ddlChange1.SelectedValue = "1" Then
                    lblError.Text = "Change can be requested only on one side."
                    ddlChange.SelectedValue = "0"
                    Exit Sub
                Else
                    txtIndEmail.Enabled = True
                    txtIndPwd.Enabled = True
                    txtIndCEmail.Enabled = True
                    txtIndCPwd.Enabled = True
                End If
            Else
                txtIndEmail.Enabled = False
                txtIndPwd.Enabled = False
                txtIndCEmail.Enabled = False
                txtIndCPwd.Enabled = False

                txtIndEmail.Text = ""
                txtIndPwd.Text = ""
                txtIndCEmail.Text = ""
                txtIndCPwd.Text = ""
            End If
        End Sub

        Protected Sub ddlChange1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChange1.SelectedIndexChanged
            lblError.Text = ""
            If ddlChange1.SelectedValue = "1" Then
                If ddlChange.SelectedValue = "1" Then
                    lblError.Text = "Change can be requested only on one side."
                    ddlChange1.SelectedValue = "0"
                    Exit Sub
                Else
                    txtIndSpEmail.Enabled = True
                    txtIndSpPwd.Enabled = True
                    txtIndSpCEmail.Enabled = True
                    txtIndCSpPwd.Enabled = True
                End If
            Else
                txtIndSpEmail.Enabled = False
                txtIndSpPwd.Enabled = False
                txtIndSpCEmail.Enabled = False
                txtIndCSpPwd.Enabled = False

                txtIndSpEmail.Text = ""
                txtIndSpPwd.Text = ""
                txtIndSpCEmail.Text = ""
                txtIndCSpPwd.Text = ""
            End If
        End Sub

        Protected Sub radYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYes.CheckedChanged
            Dim SqlIND_Update As String = ""
            Dim SqlSpouse_Update As String = ""
            lblError.Text = ""
            lblSuccess.Text = ""
            SqlIND_Update = "UPDATE IndSpouse SET Email='" & LblIndSpEmail.Text & "' WHERE Email ='" & lblIndEmail.Text & "' and AutomemberID =" & Session("IndID")
            SqlSpouse_Update = "UPDATE IndSpouse SET Email='" & lblIndEmail.Text & "' WHERE Email ='" & LblIndSpEmail.Text & "' and  AutomemberID =" & Session("SpouseID")

            Dim SQLExec As String = SqlIND_Update & ";" & SqlSpouse_Update
            Try
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLExec) > 0 Then
                    lblSwapMsg.Text = "Emails Swapped Successfully"
                    TrSwapEmails.Visible = False
                    DisplayIndSpouse()
                Else
                    lblSwapMsg.Text = "" 'Email Swapping was not done.Please mail to <a href='mailto:nsfcontests@northsouth.org'><b>nsfcontests@northsouth.org</b></a> for Issues. "
                    TrSwapEmails.Visible = False
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try

        End Sub

        Protected Sub radNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNo.CheckedChanged
            lblError.Text = ""
            lblSuccess.Text = ""
            lblSwapMsg.Text = "No Email Swap was done"
            TrSwapEmails.Visible = False
        End Sub

        Protected Sub btn_YesSp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_YesSp.Click
            ddlChange1.SelectedValue = "1"

            txtIndSpEmail.Enabled = True
            txtIndSpPwd.Enabled = True
            txtIndSpCEmail.Enabled = True
            txtIndCSpPwd.Enabled = True

            TrErrSp.Visible = False
            btnSubmit.Enabled = True
            lblError.Text = ""
        End Sub

        Protected Sub btn_NoSp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_NoSp.Click
            TrErrSp.Visible = False
            btnSubmit.Enabled = False
            lblError.Text = ""
        End Sub
    End Class

End Namespace