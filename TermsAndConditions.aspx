<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master"  AutoEventWireup="false" Inherits="VRegistration.TermsAndConditions" CodeFile="TermsAndConditions.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">	
			<TABLE id="Table1" runat="server" width=100% border="0">
				<TBODY>
					<TR>
						<TD style="WIDTH: 36px"></TD>
						<TD class="Heading"><b>General Terms and Conditions</b></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 36px"></TD>
						<TD>
							<P>1. The North South Foundation reserves the right to verify the date of birth and 
								grade of the contestant, and revoke the rank(s) and prize(s) in case of any 
								discrepancy with the contest rules.
							</P>
							
							<P>2. Judges have the right to change rules on site without notice if the situation 
								warrants. All decisions by the judges are final. Neither the question papers nor the student answer sheets will be made available for any purpose.
							</P>
							<P>3. The registration fees and meal charges are <i><b>non-refundable</b></i> under 
								any circumstances. If you withdraw from a contest after registering, your 
								payment may be considered as a 100 percent tax-deductible contribution to the Foundation.
							</P>
							<P>4. In case of a duplicate charge for the same child and for the same contest(s), the Foundation will   
								refund charges. In order to get a refund, it is the responsibility of the parent to make the request for a refund and provide sufficient evidence to the Foundation to verify and process the request.							
                            </P>								
                            <P>
                                5. A child is allowed to register only once and in only one location for a given contest during the contest season.  
                                In other words, a child is not allowed to have double registrations for a single contest or contest category.  In case of this violation, 
                                the Foundation reserves the right to disqualify the child in any or all current and future contests.  No money will be 
                                refunded as a result of the disqualification. 
                            </p>
                            <p>
                                6. The Foundation is all run by volunteers.  These contests are conducted as a community event on reasonable efforts basis to improve learning and 
                                 encourage academic excellence among the NRI children.  In this regard, the Foundation reserves the right to use periodic
                                 newsletters and emails to keep you abreast of its activities, both in the US and India, and seek your support.                              
                                
                                </p>
                                <p> 
                                7. <strong> No videography or the use of cell cameras</strong> is permitted during the contests. The Foundation takes photos and videos at events.  The Foundation reserves the right to use them in promoting and conducting its activities.  
                                
                                </p>
                            <p>
                                8. I hereby release and discharge the Foundation and the organizations providing the venue and facilities from any cause of action of any nature whatsoever arising from the participation of my child(ren) and other members of my family and friends in any and all activities associated with the Workshops and Bee competitions.  I am fully responsible for their actions as well as to comply with all the rules and regulations of the venue and North South Foundation.
                            </p>
                            <p>
                                I have read and understood the terms and conditions described above. Further, 
						from the North South Foundation website, I have read and understood the a) 
						General Rules, b) Specific Rules applicable to my contest(s), c) General FAQs 
						(frequently asked questions), and d) Specific FAQs applicable to my contest(s). 
						I accept and will comply with all the terms and conditions implied therein, all 
								inclusive.
                            </p>
						</TD>
					</TR>
					<tr>
						<td class="Item" style="FONT-WEIGHT: bold; COLOR: #cc3300; FONT-VARIANT: normal; width: 36px;">*</td>
						<td class="Item"><p style="FONT-WEIGHT: bold; COLOR: #cc3300; FONT-VARIANT: normal">
								If you choose to decline, the Registrations for the contest(s) will not be Complete. In that case, you may come back again at a later date to complete the registration.</p>
						</td>
					</tr>					
				</TBODY>
			</TABLE>
			<TABLE id="tblFundraising" runat="server" visible="false"  width=100% border="0">
				<TBODY>
					<TR>
						<TD style="WIDTH: 36px"></TD>
						<TD class="Heading"><b>General Terms and Conditions</b></TD>
					</TR>
					<TR>
						<TD style="WIDTH: 36px"></TD>
						<TD>
							<P>1. The ticket fees and sponsorship moneys are non-refundable under any circumstances. 
							</P>
							
							<P>2. North South Foundation (NSF) is all run by volunteers. The Foundation encourages academic excellence among the poor in India by providing college scholarships and among the NRI children in the US by conducting contests in spelling, vocabulary, math, science, geography, essay writing, public speaking and brain bee.  NSF sends periodic newsletters and emails to keep you abreast of its activities, both in the US and India, and seek your support. </P>
							<P>3. The use of any cameras, including but not limited to video, mobile and cell, is not permitted during the events. The Foundation takes photos and videos at events. The Foundation reserves the right to use them in promoting and conducting its activities.</P>
							<P>4. Patron Consent and Media Release for participation: I hereby release and discharge, and hold harmless, NSF, its officers, sponsors, agents, volunteers, persons, or corporations acting on its behalf, including the organizations providing the events, venues and facilities, their employees and agents, and the heirs, executors, administrators, successors of all these parties, from any liability or damages, including any claim for injuries incurred by me, or accompanying family or friends, as a result of our participation in any of the NSF activities such as Workshops, Contests, Coaching, Games, Walk-a-thons and Fundraising. 
                            </P><p>
                                I have read and understood the terms and conditions described above. Further, 
						from the North South Foundation website, I have read and understood the a) 
						General Rules, b) Specific Rules applicable to my contest(s), c) General FAQs 
						(frequently asked questions), and d) Specific FAQs applicable to my contest(s). 
						I accept and will comply with all the terms and conditions implied therein, all 
								inclusive.
                            </p>
						</TD>
					</TR>
					<tr>
						<td class="Item" style="FONT-WEIGHT: bold; COLOR: #cc3300; FONT-VARIANT: normal; width: 36px;">*</td>
						<td class="Item"><p style="FONT-WEIGHT: bold; COLOR: #cc3300; FONT-VARIANT: normal">
								- If you choose to decline, your registration for this event will not be complete. In that case, you may come back again at a later date to complete your registration.</p>
						</td>
					</tr>					
				</TBODY>
			</TABLE>
			<div align="center">
			<p><asp:Button id="btnDeclineLinkbutton" runat="server"  Text="Decline"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button id="btnContinue" runat="server" Text="Accept"></asp:Button></p>
			</div>
			
</asp:Content>

 

 
 
 