Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.StringSplitOptions
Imports System.String


Partial Class NFG_Testing
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim SQLStr As String = "SELECT PaymentNotes,asp_session_id from NFG_Transactions Where MatchedStatus is null and MS_Transdate between '05/01/2011' and '04/30/2012' order by ChapterId"
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
        Dim PaymentMatch As String
        Dim pattern As String = ""
        Dim CheckFlag As Boolean = False
        Dim ds_Product As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "SELECT Distinct ProductCode From Product")
        For j As Integer = 0 To ds_Product.Tables(0).Rows.Count - 1
            pattern = pattern & ds_Product.Tables(0).Rows(j)("ProductCode") & "|"
        Next

        pattern = pattern.TrimEnd("|")

        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            PaymentMatch = ds.Tables(0).Rows(i)("PaymentNotes")
            'pattern = "JSB|JVB|MB1|ISC|IVB|SSB|PS3|EW3|PS1|SSC|EW2|SVB|MB2|JGB|EW1|BB|MB4|JSC|MB3|SGB"
            Dim substrings() As String = Regex.Split(PaymentMatch, pattern)
            For Each match As String In substrings
                CheckPaymentNotes(match, ds.Tables(0).Rows(i)("asp_session_id"))
                '    CheckFlag = True
            Next
        Next

    End Sub
    Function CheckPaymentNotes(ByVal PaymentMatch As String, ByVal asp_session_id As String) As Boolean
        'Get the amount in it and Check
        Dim match As Match
        Dim capture As Capture
        Dim Amt As Decimal = 0
        Dim Totamt As Decimal = 0
        Dim Mealamt As Decimal = 0
        Dim Donamt As Decimal = 0
        Dim ChapterId As Integer
        Dim MatchFlag As Boolean = False
        'Dim pattern As String = ("[(][0-9]+[)]")
        Dim pattern As String = ("[(][0-9]{1,3}[)]")
        ' LateFee:0 Donation:0 
        Dim SQL_NFG As String = "Insert into TempNFG_Trans (asp_session_id,ChapterID,Amount,MealsAmount,Donation,CreateDate) Values ('" & asp_session_id & "'"
        Dim Matches As MatchCollection = Regex.Matches(PaymentMatch, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                ChapterId = Convert.ToDecimal(capture.Value.Replace("(", "").Replace(")", ""))
            Next
            MatchFlag = True
        Next
        SQL_NFG = SQL_NFG & "," & IIf(MatchFlag = True, ChapterId, "NULL")
        MatchFlag = False
        'individual Registrations
        pattern = ("[(][-]*[0-9]+[.][0-9]+[)]")
        ' LateFee:0 Donation:0 
        Matches = Regex.Matches(PaymentMatch, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Amt = Convert.ToDecimal(capture.Value.Replace("(", "").Replace(")", ""))
            Next
            MatchFlag = True
        Next
        SQL_NFG = SQL_NFG & "," & IIf(MatchFlag = True, Amt, "NULL")
        MatchFlag = False
        ''Mealsamount:0
        pattern = "[M][e][a][l][s][a][m][o][u][n][t][:][-]*[0-9]+"
        Matches = Regex.Matches(PaymentMatch, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Mealamt = Convert.ToDecimal(capture.Value.Replace("Mealsamount:", ""))
            Next
            MatchFlag = True
            'Response.Write("***MeAm*" & Mealamt)
        Next
        SQL_NFG = SQL_NFG & "," & IIf(MatchFlag = True, Mealamt, "NULL")
        MatchFlag = False
        'Donation:0 
        pattern = "[D][o][n][a][t][i][o][n][:][-]*[0-9]+"
        Matches = Regex.Matches(PaymentMatch, pattern)
        For Each match In Matches
            For Each capture In match.Captures
                Donamt = Convert.ToDecimal(capture.Value.Replace("Donation:", ""))
            Next
            MatchFlag = True
        Next
        SQL_NFG = SQL_NFG & "," & IIf(MatchFlag = True, Donamt, "NULL") & ",GETDATE())"
        MatchFlag = False
        Response.Write(SQL_NFG)
        'Total:
        'pattern = "[T][o][t][a][l][:][-]*[0-9]+"
        'Matches = Regex.Matches(PaymentMatch, pattern)
        'For Each match In Matches
        '    For Each capture In match.Captures
        '        Totamt = Convert.ToDecimal(capture.Value.Replace("Total:", ""))
        '    Next
        '    Response.Write("***Totam**" & Totamt)
        'Next

        'If Val(PaymentMatch) <> (-1 * Totamt) Then
        '    lblErr.Text = "The refunded amount is not equal to the total in payment Notes."
        '    Return False
        'ElseIf (Amt + Mealamt + Donamt) <> Totamt Then
        '    lblErr.Text = "Components do not add to the total."
        '    Return False
        'End If
        Dim conn As New SqlConnection(Application("ConnectionString"))
        SqlHelper.ExecuteScalar(conn, CommandType.Text, SQL_NFG)
        Return True
    End Function
End Class
