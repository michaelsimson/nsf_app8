﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SplashPageContents.aspx.cs" Inherits="SplashPageContents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-1.9.1.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <%--<script src="js/jquery.table2excel.js"></script>--%>
    <%--  <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>--%>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <link href="css/ezmodal.css" rel="stylesheet" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script src="js/ezmodal.js"></script>
    <script type="text/javascript">


        $(function (e) {
            getContentDetails();
            $(".btnPopUP").trigger("click");

        });
        $(document).on("click", ".continue", function (e) {
            location.href = "volunteerfunctions.aspx";
        });
        function getContentDetails() {

            var jsonData = { ContentID: 1 };
            $.ajax({
                type: "POST",
                url: "SplashPageContents.aspx/GetSplashPageContents",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(jsonData),
                async: true,
                cache: false,
                success: function (data) {
                    var strContent = "";
                    var contentHtml = "";
                    var i = 0;
                    $.each(data.d, function (index, value) {
                        strContent = value.ContentText;
                        contentHtml += "<div>";
                        contentHtml += value.ContentText;
                        contentHtml += "</div>";
                        contentHtml += "<div style='clear: both; margin-bottom: 10px;'></div>";
                    });

                    $(".dvContents").html(contentHtml);
                    $(".dvContents").find('table').css("width", "1000px;");
                    //alert(strContent);
                    if (strContent.length = 0 || strContent == "") {

                    }
                }
            })
        }
    </script>
    <style type="text/css">
        .button-success {
            padding: 10px 15px 11px;
            font-size: 18px !important;
            background-color: blue;
            font-weight: bold;
            text-shadow: 1px 1px #57D6C7;
            color: #ffffff;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border: 1px solid #57D6C7;
            cursor: pointer;
            box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
            /*color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
            background: rgb(28, 184, 65); /* this is a green */ */;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>

        <div id="demo" class="ezmodal" style="background: #FFFFCC;">

            <div class="ezmodal-container">
                <div class="ezmodal-header" style="background-color: #efefef;">


                    <span class="spnMemberTitle">Splash Page Details</span>

                    <asp:Button ID="Button1" runat="server" CssClass="button-success" Text="Continue" Style="float: right; float: right; padding: 0px; padding: 5px; position: relative; top: -8px;"
                        OnClick="btnContinue_Click" />

                </div>

                <div class="ezmodal-content" style="height: 500px; width: 800px; overflow-y: scroll; background-color: #fff;">

                    <div class="dvContents" style="padding: 15px;"></div>

                </div>



                <div class="ezmodal-footer">

                    <%-- <input type="button" class="continue" value="Continue" />--%>
                    <asp:Button ID="btnContinue" runat="server" CssClass="button-success" Text="Continue" OnClick="btnContinue_Click" />

                </div>

            </div>

        </div>

        <%--  <div style="background-color: #C3C3C3; width: auto; min-height: 630px;">
            <div style="margin: auto; width: 800px; background-color: white; min-height: 500px; position: relative; top: 50px;">

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center" style="font-weight: bold; font-size: 21px;">Splash Page Details</div>
                <div style="clear: both;"></div>
                <div style="float: right; margin-right: 10px;">
                   
                    <asp:Button ID="btnContinueTop" runat="server" Text="Continue" OnClick="btnContinue_Click" />

                </div>
                <div style="clear: both; margin-bottom: 20px;"></div>
                <div class="dvContents" style="margin-left: 10px; min-height: 400px;"></div>
                <div style="float: right; margin-right: 10px;">
                   
                    <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick="btnContinue_Click" />

                </div>
            </div>
        </div>--%>
    </form>
</body>
</html>
