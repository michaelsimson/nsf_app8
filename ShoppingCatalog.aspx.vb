﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class ShoppingCatalog
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Not IsPostBack Then
            LoadChapter(ddlChapter)
            Dim Year As Integer = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(Year - 1))
            ddlEventYear.Items.Insert(1, DateTime.Now.Year.ToString())
            ddlEventYear.Items(1).Selected = True
            If Request.QueryString("id") = 1 Then
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlinkChapterFunctions.Text = "Back to Parent Functions"
                    hlinkChapterFunctions.NavigateUrl = "UserFunctions.aspx"
                    lblCustIndID.Text = Session("CustIndID")
                    ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(Session("CustIndChapterID")))
                    ddlChapter.Enabled = False
                    deleteOldRec(Session("CustIndID"))
                    loadgrid()
                    loadPastPurchases()
                    Session("SEmail") = Session("LoginEmail")
                Else
                    Response.Redirect("maintest.aspx")
                End If
            ElseIf Request.QueryString("id") = 2 Then
                If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    clearsession()
                    pIndSearch.Visible = True
                    trH1.Visible = False
                    trH2.Visible = False
                    trH3.Visible = False
                    Session("EventID") = 10
                Else
                    Response.Redirect("maintest.aspx")
                End If
            Else
                Response.Redirect("maintest.aspx")
            End If
        End If
    End Sub

    Private Sub deleteOldRec(ByVal MemberID As Integer)
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from Saletran WHERE PaymentReference is Null and ((DATEDIFF(d,createdate,getdate())>90 AND ModifyDate is Null)  OR (DATEDIFF(d,ModifyDate,getdate())>90 AND ModifyDate is Not Null)) AND MEMBERID=" & MemberID & "")
    End Sub

    Private Sub LoadChapter(ByVal ddlst As DropDownList)
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, "select chapterID,chaptercode from chapter where Status='A' order by State,Chaptercode")
        While drStates.Read()
            ddlst.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlst.Items.Insert(0, New ListItem("Select NSF Chapter", "0"))
        ddlst.SelectedIndex = 0
    End Sub

    Private Sub loadgrid()
        Dim strSQL As String = "SELECT C.CatID, C.Category, C.ShortName, C.Description, C.UnitPrice, CASE WHEN S.SaleTranID IS NUll Then 0 Else S.SaleTranID End as Status,CASE WHEN S.Quantity IS NUll Then 0 Else S.Quantity End as Quantity,CASE WHEN S.Amount IS NUll Then 0.00 Else S.Amount End as Amount  FROM Catalog C Left JOIN SaleTran S ON C.CatID = S.CatID AND S.Memberid=" & lblCustIndID.Text & " AND S.PaymentReference is Null"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        dgCatalog.DataSource = dt
        dgCatalog.DataBind()
        If (Count < 1) Then
            lblerr.Text = "No Products are for sale now."
        Else
            lblerr.Text = ""
            lblTotal.Text = "Total  :  $" & CalcAmount()
        End If
    End Sub

    Private Sub loadPastPurchases()
        Dim strSQL As String = "SELECT C.CatID, C.Category, C.ShortName, C.Description, C.UnitPrice, S.SaleTranID,S.Quantity ,S.Amount,S.EventYear,Ch.ChapterCode,E.Name as EventName,S.PaymentDate,S.PaymentReference FROM Catalog C INNER JOIN SaleTran S ON C.CatID = S.CatID AND S.Memberid=" & lblCustIndID.Text & " INNER JOIN Chapter Ch ON S.ChapterID=Ch.ChapterID INNER JOIN Event E ON S.SEventID=E.EventID WHERE S.PaymentReference is Not Null and DateDiff(mm,S.PaymentDate,GetDate())<13 ORDER BY S.PaymentDate Desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        dgPaidProducts.DataSource = dt
        dgPaidProducts.DataBind()
        If (Count < 1) Then
            dgPaidProducts.Visible = False
            lblerror.Text = "No Purchase is made yet."
        Else
            dgPaidProducts.Visible = True
            lblerror.Text = ""
        End If
    End Sub

    Protected Sub dgCatalog_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim Chk1 As CheckBox = CType(e.Item.FindControl("chkSelect"), CheckBox)
                'lblDay1
                Dim lblStatus As Label, txtQuantity As TextBox

                lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                txtQuantity = CType(e.Item.FindControl("txtQuantity"), TextBox)
                If lblStatus.Text = "0" Then
                    Chk1.Checked = False
                    txtQuantity.Enabled = False
                Else
                    Chk1.Checked = True
                    txtQuantity.Enabled = True
                End If
        End Select
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim dgitem As DataGridItem = CType(chk.Parent.Parent, DataGridItem)
        Dim txtQuantity As TextBox = CType(dgitem.FindControl("txtQuantity"), TextBox)
        Dim lblAmount As Label = CType(dgitem.FindControl("lblAmount"), Label)
        Dim lblUnitPrice As Label = CType(dgitem.FindControl("lblUnitPrice"), Label)
        If chk.Checked = True Then
            txtQuantity.Text = 1
            lblAmount.Text = lblUnitPrice.Text
            txtQuantity.Enabled = True
            lblTotal.Text = "Total  :  $" & CalcAmount()
        Else
            txtQuantity.Text = 0
            txtQuantity.Enabled = False
            lblAmount.Text = "$0.00"
            lblTotal.Text = "Total  :  $" & CalcAmount()
        End If
    End Sub

    Protected Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Dim chk As CheckBox = CType(sender, CheckBox)
        Dim txtQuantity As TextBox = CType(sender, TextBox)
        If IsNumeric(txtQuantity.Text) = False Then
            lblerr.Text = "Please enter only numeric data in Quantity field."
        ElseIf Decimal.Parse(txtQuantity.Text) < 0 Then
            lblerr.Text = "Please enter non negative numeric data in Quantity field."
        Else
            lblerr.Text = ""
            Dim dgitem As DataGridItem = CType(txtQuantity.Parent.Parent, DataGridItem)
            'Dim txtQuantity As TextBox = CType(dgitem.FindControl("txtQuantity"), TextBox)
            Dim chk As CheckBox = CType(dgitem.FindControl("chkSelect"), CheckBox)
            Dim lblUnitPrice As Label = CType(dgitem.FindControl("lblUnitPrice"), Label)
            Dim lblAmount As Label = CType(dgitem.FindControl("lblAmount"), Label)
            If chk.Checked = True Then
                If txtQuantity.Text.Length > 0 Then
                    If Not CInt(txtQuantity.Text) = 0 Then
                        lblAmount.Text = "$" & (CInt(txtQuantity.Text) * Decimal.Parse(lblUnitPrice.Text.Replace("$", "")))
                        'MsgBox(Val(lblUnitPrice.Text.Replace("$", "")))
                    Else
                        lblAmount.Text = "$0.00"
                        chk.Checked = False
                    End If
                    lblTotal.Text = "Total  :  $" & CalcAmount()
                End If
            Else
                txtQuantity.Text = 0
                txtQuantity.Enabled = False
                lblAmount.Text = "$0.00"
                lblTotal.Text = "Total  :  $" & CalcAmount()
            End If
        End If
    End Sub

    Function CalcAmount() As Decimal
        Dim totamount As Decimal = 0.0
        Dim item As DataGridItem
        For Each item In dgCatalog.Items
            Dim lblamount As Label = CType(item.FindControl("lblAmount"), Label)
            totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
        Next
        Return totamount
    End Function

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub

    Private Sub clear()
        If Not Session("EntryToken").ToString.ToUpper() = "PARENT" Then
            ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue("0"))
        End If
        ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue("0"))
        lblTotal.Text = ""
        lblerr.Text = ""
        loadgrid()
    End Sub

    Function checkquantity() As Boolean
        Dim flag As Boolean = False
        Dim item As DataGridItem
        Dim chk As CheckBox, txtQuantity As TextBox
        For Each item In dgCatalog.Items
            chk = CType(item.FindControl("chkSelect"), CheckBox)
            txtQuantity = CType(item.FindControl("txtQuantity"), TextBox)
            If chk.Checked = True Then
                If IsNumeric(txtQuantity.Text) = False Then
                    Return True
                    Exit Function
                ElseIf Decimal.Parse(txtQuantity.Text) < 0 Then
                    Return True
                    Exit Function
                End If
            End If
        Next
        Return flag
    End Function

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        Dim flag As Boolean = False
        Dim totamount As Decimal
        If checkquantity() Then
            lblerr.Text = "Please enter non negative numeric data in Quantity field."
            Exit Sub
        ElseIf ddlChapter.SelectedValue = "0" Then
            lblerr.Text = "Please select Chapter"
            Exit Sub
        ElseIf ddlEvent.SelectedValue = "0" Then
            lblerr.Text = "Please select Event"
            Exit Sub
        Else
            lblerr.Text = ""
            totamount = 0.0
            Dim item As DataGridItem
            Dim txtQuantity As TextBox, lblUnitPrice As Label, lblCatID As Label, lblCategory As Label, lblShortName As Label, lblamount As Label ', lblDescription As Label
            Dim chk As CheckBox, StrSQL As String, lblStatus As Label
            For Each item In dgCatalog.Items
                chk = CType(item.FindControl("chkSelect"), CheckBox)
                lblStatus = CType(item.FindControl("lblStatus"), Label)
                If chk.Checked = True Then
                    flag = True
                    txtQuantity = CType(item.FindControl("txtQuantity"), TextBox)
                    lblUnitPrice = CType(item.FindControl("lblUnitPrice"), Label)
                    lblCatID = CType(item.FindControl("lblCatID"), Label)
                    lblCategory = CType(item.FindControl("lblCategory"), Label)
                    lblShortName = CType(item.FindControl("lblShortName"), Label)
                    'lblDescription = CType(item.FindControl("lblDescription"), Label)
                    lblamount = CType(item.FindControl("lblAmount"), Label)
                    totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
                    If lblStatus.Text = "0" Then
                        StrSQL = "INSERT INTO  SaleTran(ChapterID, MemberId, CatId, Category, ShortName, Quantity, UnitPrice, Amount, EventID,SEventID,EventYear, CreateDate, CreatedBy) VALUES ("
                        StrSQL = StrSQL & ddlChapter.SelectedValue & "," & lblCustIndID.Text & "," & lblCatID.Text & ",'" & lblCategory.Text & "','" & lblShortName.Text & "'," & txtQuantity.Text & "," & lblUnitPrice.Text & "," & lblamount.Text & "," & Session("EventID") & "," & ddlEvent.SelectedValue & "," & ddlEventYear.SelectedValue & ",Getdate()," & Session("LoginID") & ")"
                        lblerr.Text = "Inserted Successfully"
                    Else
                        StrSQL = "UPDATE SaleTran SET  Quantity=" & txtQuantity.Text & ", ChapterID=" & ddlChapter.SelectedValue & ", Amount=" & lblamount.Text & ", SEventID=" & ddlEvent.SelectedValue & ", EventYear=" & ddlEventYear.SelectedValue & ", ModifyDate=Getdate(), Modifiedby=" & Session("LoginID") & "WHERE  SaleTranID=" & lblStatus.Text
                    End If
                    Try
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL)
                    Catch ex As Exception
                        lblerr.Text = StrSQL
                        lblerr.Text = lblerr.Text & "<br>" & ex.ToString()
                    End Try
                ElseIf Not lblStatus.Text = "0" Then
                    StrSQL = "DELETE FROM SaleTran WHERE  SaleTranID=" & lblStatus.Text
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL)
                End If
            Next
        End If
        If flag = True Then
            If Session("EventYear") Is Nothing Then Session("EventYear") = ddlEventYear.SelectedValue
            Session("SEventID") = ddlEvent.SelectedValue
            Session("SChapterID") = ddlChapter.SelectedValue
            Response.Redirect("ShoppingSummary.aspx")
        End If
    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        lblCustIndID.Text = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and Email like '%" + email + "%'")
            Else
                strSql.Append("  Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by lastname,firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No member match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        clearsession()
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Automemberid,Relationship,DonorType,Email,chapterID,FirstName+' '+LastName as Name from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & "")
        While reader.Read()
            If reader("DonorType").ToUpper() = "IND" Then
                lblCustIndID.Text = reader("Automemberid")
                Session("SEmail") = reader("Email")
                Session("CustIndID") = reader("Automemberid")
                Session("SChapterID") = reader("ChapterID")
                ltl1.Text = "Name : " & reader("Name")
            ElseIf reader("DonorType").ToUpper() = "SPOUSE" Then
                lblCustIndID.Text = reader("Relationship")
                Session("CustIndID") = reader("Relationship")
                ltl1.Text = "Name : " & reader("Name")
                setSpouseValues(Session("CustIndID"))
            End If
        End While
        reader.Close()
        If Not lblCustIndID.Text = String.Empty Then
            ' Panel4.Visible = False
            pIndSearch.Visible = False
            ' lblIndSearch.Visible = False
            trH1.Visible = True
            trH2.Visible = True
            trH3.Visible = True
            btnSrchParent.Visible = True
            ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(Session("SChapterID")))
            ddlChapter.Enabled = True
            deleteOldRec(Session("CustIndID"))
            dgCatalog.Visible = True
            loadgrid()
            loadPastPurchases()
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "No data found"
        End If
    End Sub

    Private Sub setSpouseValues(ByVal Memberid As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Email,chapterID from Indspouse where automemberid=" & Memberid & "")
        If ds.Tables(0).Rows.Count > 0 Then
            Session("SEmail") = ds.Tables(0).Rows(0)("Email")
            Session("SChapterID") = ds.Tables(0).Rows(0)("ChapterID")
        End If
    End Sub

    Protected Sub btnSrchParent_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = True
        ltl1.Text = String.Empty
        trH1.Visible = False
        trH2.Visible = False
        trH3.Visible = False
        dgCatalog.Visible = False
        lblTotal.Text = ""
    End Sub
    Private Sub clearsession()
        Session.Remove("CustIndID")
        Session.Remove("Donation")
        Session.Remove("outXml")
        Session.Remove("PaymentReference")
        Session.Remove("R_Approved")
        Session.Remove("SaleAmt")
        Session.Remove("SaleItems")
        Session.Remove("SChapterID")
        Session.Remove("SEmail")
        Session.Remove("SEventID")
    End Sub
End Class
