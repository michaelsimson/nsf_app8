﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Drawing;

public partial class SendEmailLog : System.Web.UI.Page
{
    string EmailQry = "* from SentEmailLog";

    protected void Page_Load(object sender, EventArgs e)
    {

        //Session["RoleId"] = 1;
        //Session["LoggedIn"] = true;
        //Session["LoginID"] = 4240;
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
        {
            Response.Redirect("~/login.aspx?entry=p");
        }
        if (Session["RoleId"] != null)
        {
            if (this.IsPostBack == false)
            {
                PopulateChapter(ddlChapter, string.Empty);
                populateCluster(ddlCluster, string.Empty);

                PopulateZone(ddlZone);
                ddlZone.Enabled = false;
                ddlCluster.Enabled = false;
                ddlChapter.Enabled = false;
                if (Session["SmLogIDZone"] == null) { Session["SmLogIDZone"] = "-1"; }
                if (Session["SmLogIDCluster"] == null) { Session["SmLogIDCluster"] = "-1"; }
                if (Session["SmLogIDChapter"] == null) { Session["SmLogIDChapter"] = "-1"; }
                if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2")
                {
                    PopulateGridView();// EmailQry = "select  top(50) * from SentEmailLog";
                    ddlZone.Enabled = true;
                    ddlChapter.Enabled = true;
                    ddlCluster.Enabled = true;
                }
                else if (Session["RoleId"].ToString() == "3")
                {
                    ddlSource.SelectedIndex = ddlSource.Items.IndexOf(ddlSource.Items.FindByText("Email_CC"));
                    string StrZoneQry = "select z.Name from Zone z inner join Volunteer v on z.ZoneId=v.ZoneId where v.RoleId=" + Session["RoleId"] + " and v.Memberid=" + Session["LoginId"] + "";
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrZoneQry);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string ZoneName = ds.Tables[0].Rows[0]["Name"].ToString();
                        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(ZoneName));
                    }

                    //ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(Session["SmLogIDZone"].ToString()));
                    //ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(Session["SmLogIDChapter"].ToString()));
                    //ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue(Session["SmLogIDCluster"].ToString()));
                    ddlSource.Enabled = false;
                    ddlZone.Enabled = false;
                    ddlCluster.Enabled = false;
                    ddlChapter.Enabled = false;
                    //if (ddlChapter.SelectedItem.Value != "-1")
                    //{
                    //    ddlChapter.Enabled = false;
                    //    ddlCluster.Enabled = false;
                    //}
                    //else
                    //{
                    //    if (ddlZone.SelectedItem.Text != "[Select Zone]")
                    //    {
                    //        populateCluster(ddlCluster, " Where ZoneID=" + ddlZone.SelectedItem.Value.ToString());
                    //        PopulateChapter(ddlChapter, " Where ZoneID=" + ddlZone.SelectedItem.Value.ToString());
                    //    }
                    //    ddlChapter.Enabled = true;
                    //    ddlCluster.Enabled = true;
                    //}
                    PopulateGridView();
                }
                else if (Session["RoleId"].ToString() == "5")
                {


                    ddlSource.SelectedIndex = ddlSource.Items.IndexOf(ddlSource.Items.FindByText("Email_CC"));
                    string StrZoneQry = "select z.Name from Zone z inner join Volunteer v on z.ZoneId=v.ZoneId where v.RoleId=" + Session["RoleId"] + " and v.Memberid=" + Session["LoginId"] + "";
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrZoneQry);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string ZoneName = ds.Tables[0].Rows[0]["Name"].ToString();
                        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(ZoneName));
                    }
                    string StrClusterQry = "select c.Name from Cluster c inner join Volunteer v on c.ClusterId=v.ClusterId where v.RoleId=" + Session["RoleId"] + " and v.Memberid=" + Session["LoginId"] + "";
                    DataSet dsCluster = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrClusterQry);
                    if (dsCluster.Tables[0].Rows.Count > 0)
                    {
                        string ClusterName = dsCluster.Tables[0].Rows[0]["Name"].ToString();
                        ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue(ClusterName));
                    }
                    string StrChapterQry = "	select c.Name from Chapter c inner join Volunteer v on c.Chapterid=v.Chapterid where v.RoleId=" + Session["RoleId"] + " and v.Memberid=" + Session["LoginId"] + "";
                    DataSet dsChapter = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrChapterQry);
                    if (dsChapter.Tables[0].Rows.Count > 0)
                    {
                        string ChapterName = dsChapter.Tables[0].Rows[0]["Name"].ToString();
                        ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(ChapterName));
                    }

                    ddlSource.Enabled = false;
                    ddlZone.Enabled = false;
                    ddlChapter.Enabled = false;
                    ddlCluster.Enabled = false;

                    //ddlSource.SelectedIndex = ddlSource.Items.IndexOf(ddlSource.Items.FindByText("Email_CC"));
                    //ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(Session["SmLogIDChapter"].ToString()));
                    //ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(Session["SmLogIDZone"].ToString()));
                    //ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue(Session["SmLogIDCluster"].ToString()));

                    //ddlChapter.Enabled = false;
                    ddlSource.Enabled = false;
                    PopulateGridView();
                }
                else if (Session["RoleId"].ToString() == "4")
                {


                    ddlSource.SelectedIndex = ddlSource.Items.IndexOf(ddlSource.Items.FindByText("Email_CC"));
                    string StrZoneQry = "select z.Name from Zone z inner join Volunteer v on z.ZoneId=v.ZoneId where v.RoleId=" + Session["RoleId"] + " and v.Memberid=" + Session["LoginId"] + "";
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrZoneQry);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string ZoneName = ds.Tables[0].Rows[0]["Name"].ToString();
                        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(ZoneName));
                    }
                    string StrClusterQry = "select c.Name from Cluster c inner join Volunteer v on c.ClusterId=v.ClusterId where v.RoleId=" + Session["RoleId"] + " and v.Memberid=" + Session["LoginId"] + "";
                    DataSet dsCluster = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrClusterQry);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string ClusterName = dsCluster.Tables[0].Rows[0]["Name"].ToString();
                        ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue(ClusterName));
                    }
                    ddlSource.Enabled = false;
                    ddlZone.Enabled = false;
                    ddlCluster.Enabled = false;
                    ddlChapter.Enabled = false;
                    //if (ddlChapter.SelectedItem .Value !="-1") 
                    //{
                    //ddlChapter.Enabled = false ;
                    //}
                    //else
                    //{
                    //    PopulateChapter(ddlChapter, " Where ClusterID=" + ddlCluster.SelectedItem.Value.ToString());
                    //    ddlChapter.Enabled = true;
                    //}

                    PopulateGridView();
                }
                else if (Session["RoleId"].ToString() == "88" | Session["RoleId"].ToString() == "89" | Session["RoleID"].ToString() == "96")
                {
                    ddlSource.SelectedIndex = ddlSource.Items.IndexOf(ddlSource.Items.FindByText("EmailCoach"));
                    ddlSource.Enabled = false;
                    ddlZone.Enabled = false;
                    ddlCluster.Enabled = false;
                    ddlChapter.Enabled = false;
                    PopulateGridView();

                }
                else if (Session["RoleId"].ToString() == "97" | Session["RoleId"].ToString() == "98")
                {
                    ddlSource.SelectedIndex = ddlSource.Items.IndexOf(ddlSource.Items.FindByText("Email_OnlineWS"));
                    ddlSource.Enabled = false;
                    ddlZone.Enabled = false;
                    ddlCluster.Enabled = false;
                    ddlChapter.Enabled = false;
                    PopulateGridView();

                }
                else
                {

                    lblError.ForeColor = Color.Red;
                    lblError.Visible = true;
                    lblError.Text = "You do not have access to this function.";
                    Pnldefault.Visible = false;
                    GridEmail.Visible = false;
                }

                LoadFromEmail();
                LoadLoginEmail();
            }


        }
    }


    protected void PopulateGridView()
    {
        string sqlStr = string.Empty;
        DataSet ds = null;
        Session["dtmailExport"] = null;
        sqlStr = "select SE.[SentEmailLogID],SE.[Source],SE.[EventID],CH.[ChapterCode],SE.[LoginEmail],SE.[FromEmail],SE.[EmailSubject],SE.[RoleID],SE.[MemberID],SE.[StartTime],SE.[TotalEmails_Count],SE.[SentEmails_Count],SE.[Endtime] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID";
        string WhCont = string.Empty;
        if (int.Parse(ddlSource.SelectedItem.Value) > 1)
        {
            //if (ddlSource.SelectedItem.Text == "Email_CC")
            //{
            WhCont = " Where SE.Source ='" + ddlSource.SelectedItem.Text + "' ";
            if (ddlZone.SelectedItem.Text != "[Select Zone]")
            {
                WhCont = WhCont + " And CH.ZoneID=" + ddlZone.SelectedItem.Value;
            }
            if (ddlCluster.SelectedItem.Text != "[Select Cluster]")
            {
                WhCont = WhCont + " And ch.ClusterId=" + ddlCluster.SelectedItem.Value;
            }
            if (ddlChapter.SelectedItem.Text != "[Select Chapter]")
            {
                WhCont = WhCont + " And SE.ChapterID=" + ddlChapter.SelectedItem.Value;
            }
            if (ddlFromEmail.SelectedValue != "-1" && ddlFromEmail.SelectedValue != "")
            {
                WhCont = WhCont + " And SE.FromEmail='" + ddlFromEmail.SelectedItem.Value + "'";
            }
            if (ddlLoginEmail.SelectedValue != "-1" && ddlLoginEmail.SelectedValue != "")
            {
                WhCont = WhCont + " And SE.LoginEmail='" + ddlLoginEmail.SelectedItem.Value + "'";
            }

            if (txtFrom.Text != "" && txtTo.Text != "")
            {
                string ToDate = Convert.ToDateTime(txtTo.Text).AddDays(1).ToString("MM-dd-yyyy");

                WhCont = WhCont + " And SE.StartTime between '" + txtFrom.Text.Trim().Replace('/', '-') + "' and '" + ToDate + "'";
            }

            sqlStr = sqlStr + WhCont + "  order by  SentEmailLogID desc";
            //}
            //else
            //{
            //    sqlStr = "select SE.[SentEmailLogID],SE.[Source],SE.[EventID],CH.[ChapterCode],SE.[LoginEmail],SE.[FromEmail],SE.[EmailSubject],SE.[RoleID],SE.[MemberID],SE.[StartTime],SE.[TotalEmails_Count],SE.[SentEmails_Count],SE.[Endtime] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID ";
            //    WhCont = " Where SE.[Source] ='" + ddlSource.SelectedItem.Text +"' ";
            //    sqlStr = sqlStr + WhCont + " order by  SE.SentEmailLogID desc";
            //}
        }
        else
        {
            sqlStr = " select SE.[SentEmailLogID],SE.[Source],SE.[EventID],CH.[ChapterCode],SE.[LoginEmail],SE.[FromEmail],SE.[EmailSubject],SE.[RoleID],SE.[MemberID],SE.[StartTime],SE.[TotalEmails_Count],SE.[SentEmails_Count],SE.[Endtime] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID  ";

            sqlStr += "  order by  SE.SentEmailLogID desc";
        }

        if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "96")
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStr);
        }
        else
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Genwherecond());
        }
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count >= 1)
        {
            GridEmail.DataSource = ds;
            GridEmail.DataBind();
            lblnorecord.Visible = false;
            btn_export.Enabled = true;
            Session["dtmailExport"] = dt;







        }
        else
        {
            GridEmail.DataSource = null;
            GridEmail.DataBind();
            lblnorecord.Visible = true;
            btn_export.Enabled = false;
        }
    }
    public void LoadFromEmail()
    {
        string sqlStr = string.Empty;
        DataSet ds = null;

        sqlStr = "select distinct SE.[FromEmail] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID";
        string WhCont = string.Empty;
        if (int.Parse(ddlSource.SelectedItem.Value) > 1)
        {
            //if (ddlSource.SelectedItem.Text == "Email_CC")
            //{
            WhCont = " Where SE.Source ='" + ddlSource.SelectedItem.Text + "' ";
            if (ddlZone.SelectedItem.Text != "[Select Zone]")
            {
                WhCont = WhCont + " And CH.ZoneID=" + ddlZone.SelectedItem.Value;
            }
            if (ddlCluster.SelectedItem.Text != "[Select Cluster]")
            {
                WhCont = WhCont + " And ch.ClusterId=" + ddlCluster.SelectedItem.Value;
            }
            if (ddlChapter.SelectedItem.Text != "[Select Chapter]")
            {
                WhCont = WhCont + " And SE.ChapterID=" + ddlChapter.SelectedItem.Value;
            }
            sqlStr = sqlStr + WhCont + "  order by  FromEmail desc";

        }
        else
        {
            sqlStr = " select distinct SE.[FromEmail] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID   order by  SE.FromEmail desc";
        }

        if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "96")
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStr);
        }
        else
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Genwherecond());
        }

        ddlFromEmail.DataValueField = "FromEmail";
        ddlFromEmail.DataTextField = "FromEmail";
        ddlFromEmail.DataSource = ds;
        ddlFromEmail.DataBind();
        ddlFromEmail.Items.Insert(0, new ListItem("[Select ]", "-1"));
        if (ds.Tables[0].Rows.Count == 1)
        {
            ddlFromEmail.SelectedValue = ds.Tables[0].Rows[0]["FromEmail"].ToString();
            ddlFromEmail.Enabled = false;
        }
        else
        {
            ddlFromEmail.Enabled = true;
        }

    }

    public void LoadLoginEmail()
    {
        string sqlStr = string.Empty;
        DataSet ds = null;

        sqlStr = "select distinct SE.[LoginEmail] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID";
        string WhCont = string.Empty;
        if (int.Parse(ddlSource.SelectedItem.Value) > 1)
        {
            //if (ddlSource.SelectedItem.Text == "Email_CC")
            //{
            WhCont = " Where SE.Source ='" + ddlSource.SelectedItem.Text + "' ";
            if (ddlZone.SelectedItem.Text != "[Select Zone]")
            {
                WhCont = WhCont + " And CH.ZoneID=" + ddlZone.SelectedItem.Value;
            }
            if (ddlCluster.SelectedItem.Text != "[Select Cluster]")
            {
                WhCont = WhCont + " And ch.ClusterId=" + ddlCluster.SelectedItem.Value;
            }
            if (ddlChapter.SelectedItem.Text != "[Select Chapter]")
            {
                WhCont = WhCont + " And SE.ChapterID=" + ddlChapter.SelectedItem.Value;
            }
            sqlStr = sqlStr + WhCont + "  order by  LoginEmail desc";

        }
        else
        {
            sqlStr = " select distinct SE.[LoginEmail] from SentEmailLog SE  inner join chapter CH on ch.ChapterID  = se.ChapterID   order by  SE.LoginEmail desc";
        }

        if (Session["RoleId"].ToString() == "1" || Session["RoleId"].ToString() == "2" || Session["RoleId"].ToString() == "96")
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStr);
        }
        else
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Genwherecond());
        }

        ddlLoginEmail.DataValueField = "LoginEmail";
        ddlLoginEmail.DataTextField = "LoginEmail";
        ddlLoginEmail.DataSource = ds;
        ddlLoginEmail.DataBind();
        ddlLoginEmail.Items.Insert(0, new ListItem("[Select ]", "-1"));
        if (ds.Tables[0].Rows.Count == 1)
        {
            ddlLoginEmail.SelectedValue = ds.Tables[0].Rows[0]["LoginEmail"].ToString();
            ddlLoginEmail.Enabled = false;
        }
        else
        {
            ddlLoginEmail.Enabled = true;
        }
    }

    protected void populateCluster(DropDownList ddlObject, string ipt)
    {
        try
        {
            ddlObject.Enabled = true;
            ddlObject.Items.Clear();
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select ClusterID,ClusterCode  from Cluster " + ipt + " order by ClusterCode");
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ClusterCode";
                ddlObject.DataValueField = "ClusterID";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();
                if (dsChapter.Tables[0].Rows.Count == 1) { ddlObject.Enabled = false; } else { ddlObject.Items.Insert(0, new ListItem("[Select Cluster]", "-1")); }
                ddlObject.SelectedIndex = 0;
            }
            else
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Cluster]", "-1"));
            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void PopulateZone(DropDownList ddlObject)
    {
        try
        {
            ddlObject.Items.Clear();
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select ZoneId,ZoneCode from Zone  order by ZoneCode");
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ZoneCode";
                ddlObject.DataValueField = "ZoneId";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select Zone]", "-1"));
                ddlObject.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void PopulateChapter(DropDownList ddlObject, string ipt)
    {
        try
        {
            ddlObject.Enabled = true;
            ddlObject.Items.Clear();
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID,ChapterCode from Chapter" + ipt + " order by state,ChapterCode");
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ChapterCode";
                ddlObject.DataValueField = "ChapterID";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();
                if (dsChapter.Tables[0].Rows.Count == 1) { ddlObject.Enabled = false; } else { ddlObject.Items.Insert(0, new ListItem("[Select Chapter]", "-1")); }
                ddlObject.SelectedIndex = 0;
            }
            else
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Chapter]", "-1"));
            }
        }
        catch (Exception ex) { }
    }
    protected string Genwherecond()
    {
        string whCont = string.Empty;
        string iCondtions = string.Empty;
        if (int.Parse(ddlSource.SelectedItem.Value) > 1)
        {
            if (ddlSource.SelectedItem.Text == "Email_CC")
            {
                whCont = " Where SE.Source ='Email_CC' ";
                if (ddlZone.SelectedItem.Text != "[Select Zone]")
                {
                    whCont = whCont + " And CH.ZoneID=" + ddlZone.SelectedItem.Value;
                }
                if (ddlCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    whCont = whCont + " And ch.ClusterId=" + ddlCluster.SelectedItem.Value;
                }
                if (ddlChapter.SelectedItem.Text != "[Select Chapter]")
                {
                    whCont = whCont + " And ch.ChapterID=" + ddlChapter.SelectedItem.Value;
                }
            }
            else
            {
                whCont = " Where Source='" + ddlSource.SelectedItem.Text + "' ";

               
            }
            if (ddlSource.SelectedItem.Text == "EmailCoach")
            {
                iCondtions = "select SE.[SentEmailLogID],SE.[Source],SE.[EventID],CH.[ChapterCode],SE.[LoginEmail],SE.[FromEmail],SE.[EmailSubject],SE.[RoleID],SE.[MemberID],SE.[StartTime],SE.[TotalEmails_Count],SE.[SentEmails_Count],SE.[Endtime] from SentEmailLog SE inner join chapter CH on ch.ChapterID  = SE.ChapterID  where SE.Source='" + ddlSource.SelectedItem.Text + "' and SE.MemberID=" + Session["LoginId"].ToString() + " ";

                if (txtFrom.Text != "" && txtTo.Text != "")
                {
                    string ToDate = Convert.ToDateTime(txtTo.Text).AddDays(1).ToString("MM-dd-yyyy");

                    iCondtions = iCondtions + " And SE.StartTime between '" + txtFrom.Text.Trim().Replace('/', '-') + "' and '" + ToDate + "'";
                }

                iCondtions += " order by  SE.SentEmailLogID desc";
            }
            else
            {
                iCondtions = "select SE.[SentEmailLogID],SE.[Source],SE.[EventID],CH.[ChapterCode],SE.[LoginEmail],SE.[FromEmail],SE.[EmailSubject],SE.[RoleID],SE.[MemberID],SE.[StartTime],SE.[TotalEmails_Count],SE.[SentEmails_Count],SE.[Endtime] from SentEmailLog SE inner join chapter CH on ch.ChapterID  = SE.ChapterID " + whCont + "";

                if (txtFrom.Text != "" && txtTo.Text != "")
                {
                    string ToDate = Convert.ToDateTime(txtTo.Text).AddDays(1).ToString("MM-dd-yyyy");

                    iCondtions = iCondtions + " And SE.StartTime between '" + txtFrom.Text.Trim().Replace('/', '-') + "' and '" + ToDate + "'";
                }

                iCondtions += " order by  SE.SentEmailLogID desc";
            }

        }
        return iCondtions;
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            PopulateGridView();
        }
        catch (Exception ex) { }
    }
    protected void ddlSource_SelectedIndexChanged(object sender, EventArgs e)
    {

        PopulateZone(ddlZone);
        //if (ddlSource.SelectedItem.Text == "Email_CC")
        //{
        //    ddlZone.Enabled = true;
        //    ddlCluster.Enabled = true ;
        //    ddlChapter.Enabled = true;
        //    PopulateZone(ddlZone);
        //    populateCluster(ddlCluster, string.Empty);
        //    PopulateChapter(ddlChapter, string.Empty);
        //}
        //else
        //{
        //    ddlZone.Items.Clear();
        //    ddlCluster.Items.Clear();
        //    ddlChapter.Items.Clear();
        //    ddlZone.Items.Insert(0, new ListItem("[Select Zone]", "-1"));
        //    ddlCluster.Items.Insert(0, new ListItem("[Select Cluster]", "-1"));
        //    ddlChapter.Items.Insert(0, new ListItem("[Select Chapter]", "-1"));
        //    ddlZone.Enabled = false;
        //    ddlCluster.Enabled = false;
        //    ddlChapter.Enabled = false;
        //}
    }
    protected void ddlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlZone.SelectedItem.Text != "[Select Zone]")
        {
            populateCluster(ddlCluster, " Where ZoneID=" + ddlZone.SelectedItem.Value.ToString());
            PopulateChapter(ddlChapter, " Where ZoneID=" + ddlZone.SelectedItem.Value.ToString());
        }
        else
        {
            populateCluster(ddlCluster, string.Empty);
            PopulateChapter(ddlChapter, string.Empty);
        }
    }
    protected void ddlCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        string iString = string.Empty;
        if (ddlCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddlZone.SelectedItem.Text != "[Select Zone]")
            {
                iString = " Where ZoneID=" + ddlZone.SelectedItem.Value.ToString() + " and ClusterID=" + ddlCluster.SelectedItem.Value.ToString();
                PopulateChapter(ddlChapter, iString);
            }
            else
            {
                iString = " Where ClusterID=" + ddlCluster.SelectedItem.Value.ToString();
                PopulateChapter(ddlChapter, iString);
            }
        }
        else
        {
            if (ddlZone.SelectedItem.Text != "[Select Zone]")
            {
                iString = " Where ZoneID=" + ddlZone.SelectedItem.Value.ToString();
                PopulateChapter(ddlChapter, iString);
            }
            else
            {
                PopulateChapter(ddlChapter, string.Empty);
            }
        }
    }
    protected void btn_export_Click(object sender, EventArgs e)
    {
        ExportTableData((DataTable)Session["dtmailExport"]);
    }
    public void ExportTableData(DataTable dtdata)
    {
        string attach = string.Empty;
        attach = "attachment;filename=SendEmailLog.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";// "application/vnd.openxmlformats";// "application/vnd.xls";//ms-excel";
        if (dtdata != null)
        {
            foreach (DataColumn dc in dtdata.Columns)
            {
                Response.Write(dc.ColumnName + "\t");
                //sep = ";";
            }
            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {
                    Response.Write(dr[i].ToString() + "\t");
                }
                Response.Write("\n");
            }
            Response.Flush();
            Response.End();
        }
    }
    protected void GridEmail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridEmail.PageIndex = e.NewPageIndex;
            GridEmail.DataSource = (DataTable)Session["dtmailExport"];
            GridEmail.DataBind();
        }
        catch (Exception ex) { }
    }
}