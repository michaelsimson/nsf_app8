﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HumStudyMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // if (!IsPostBack)
       // {
           if (Session["entryToken"] != null)
            {
               switch (Session["entryToken"].ToString().ToLower())
                {
                    case "volunteer":
                        hfEntryToken.Value = Session["entryToken"].ToString().ToLower();
                        hfLoginID.Value = Session["MemberId"].ToString();
                        //hdnEntryToken.Value = Session["entryToken"].ToString().ToLower();
                        break;
                      
                    case "school":
                        hfEntryToken.Value = Session["entryToken"].ToString().ToLower();
                        hfLoginID.Value = Session["SchoolId"].ToString();
                        break;

                    case "teacher":
                        //    EnableMenu(false, false, true, false);
                        hfEntryToken.Value = Session["entryToken"].ToString().ToLower();
                        hfLoginID.Value = Session["TeacherId"].ToString();
                        break;

                    case "student":
                        //  EnableMenu(false, false, false, true);
                        hfLoginID.Value = Session["StudentID"].ToString();
                        hfEntryToken.Value = Session["entryToken"].ToString().ToLower();
                        break;
                }
            }
        //}
    }
     
}
