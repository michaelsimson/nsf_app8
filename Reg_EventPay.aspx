﻿<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.Reg_EventPay" Trace="false" CodeFile="Reg_EventPay.aspx.vb" CodeFileBaseClass="VRegistration.LinkPointAPI_cs.LinkPointTxn_Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="js/braintree-data.js"></script>
    <table width="75%" style="margin: 0 auto;">
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td class="ContentSubTitle" valign="top" nowrap align="center" colspan="2">
                            <h1 align="center"><font face="Arial" color="#0000ff" size="3"><b>Your Payment</b></font>&nbsp;<br>
                            </h1>
                        </td>
                    </tr>

                    <tr>
                        <td class="largewordingbold" style="text-align: left">This is a Secure Page. Security by 
								GeoTrust<br />
                            <img height="50" src="images/gt-logo.gif" onclick="window.open('https://smarticon.geotrust.com/smarticonprofile?Referer=http://www.northsouth.org')" style="cursor: pointer"><a href="" target="new"></a></td>
                        <td valign="middle">
                            <table id="Table3" cellspacing="1" cellpadding="1" width="300" border="0">
                                <tr>
                                    <td class="largewordingbold" align="left" style="font-size: 12pt;">
                                        <asp:Label ID="lblRgfee" Text="Registration Fee :" runat="server" />
                                    </td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblRegFee" runat="server" Width="46px"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trDonation">
                                    <td class="largewordingbold" align="Left" style="font-size: 12pt;">Donation :</td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblDonation" runat="server" Width="46px"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trFundR" visible="false">
                                    <td class="largewordingbold" align="left" style="font-size: 12pt;">
                                        <asp:Label ID="Label1" Text="Tickets and Sponsorships :" runat="server" /></td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblFundRAmt" runat="server" Width="46px"></asp:Label></td>
                                </tr>
                                <tr runat="server" id="trsale" visible="false">
                                    <td class="largewordingbold" align="left" style="font-size: 12pt;">
                                        <asp:Label ID="lblSale" Text="Purchase Amount :" runat="server" />
                                    </td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblSaleAmount" runat="server" Width="46px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <%--<TD class="largewordingbold">Meals :</TD>--%>
                                    <td class="largewordingbold" align="left" style="font-size: 12pt;">
                                        <asp:Label ID="lblMeals" Text="Meals :" runat="server" />
                                    </td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblMealsAmount" runat="server" Width="46px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <%--<TD class="largewordingbold">Late Fee :</TD>--%>
                                    <td class="largewordingbold" align="Left" style="font-size: 12pt;">
                                        <asp:Label ID="lblLatefeetext" Text="Late Fee :" runat="server" />
                                    </td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblLateFee" runat="server" Width="46px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="Left" style="font-size: 12pt;">
                                        <asp:Label ID="lblDiscountText" Text="Discount :" runat="server" />
                                    </td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblDiscount" runat="server" Width="46px"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="left" style="font-size: 12pt;"><span style="color: #0033FF; font-weight: bold">Total Due</span> :</td>
                                    <td class="largewordingbold" align="right" style="font-size: 12pt;">
                                        <asp:Label ID="lblTotalAmount" runat="server" Style="color: #0033FF; font-weight: bold" Width="46px"></asp:Label></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="largewordingbold" colspan="2">
                            <table id="tblError" runat="server" visible="false" border="1" width="100%">
                                <tr>
                                    <td style="height: 26px">
                                        <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></asp:Label>
                                        <asp:Label ID="lblIP" runat="server" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="1">
                                <tr>
                                    <td class="largewordingbold" width="100%" colspan="4"><b>Billing Address</b></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px"><b style="color: #6D6363;">Name</b></td>
                                    <td class="largewordingbold" align="left" width="75%">
                                        <asp:Label ID="nameLabel" runat="server" Width="46px"></asp:Label></td>
                                    <td class="largewordingbold" align="right" width="75%"
                                        style="width: 0%; font-weight: 700; color: #6D6363;">Email</td>
                                    <td class="largewordingbold" align="left" width="75%" style="width: 25%">
                                        <asp:Label ID="lblmail" runat="server"></asp:Label></td>
                                </tr>

                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px"><b>Street Address</b></td>
                                    <td class="largewordingbold" align="left" width="75%" colspan="3">&nbsp;<asp:TextBox ID="txtAddress1" runat="server" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="txtAddress1" ErrorMessage="Street Address Required"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px"><b>Apt#</b></td>
                                    <td class="largewordingbold" align="left" width="75%" colspan="3">&nbsp;<asp:TextBox ID="txtAddress2" runat="server" MaxLength="50"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" valign="top" align="right" style="width: 162px"><b>City</b></td>
                                    <td class="largewordingbold" valign="top" align="left" width="25%">&nbsp;<asp:TextBox ID="txtCity" runat="server" MaxLength="50"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" ControlToValidate="txtCity" ErrorMessage="City Required"></asp:RequiredFieldValidator></td>
                                    <td class="largewordingbold" valign="top" align="right" width="25%"><b>State/Province</b></td>
                                    <td class="largewordingbold" valign="top" align="left" width="25%">&nbsp;<asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList><asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" ControlToValidate="ddlState" ErrorMessage="Please select a State"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" valign="top" align="right" style="width: 162px"><b>Country</b></td>
                                    <td class="largewordingbold" valign="top" align="left" width="25%">&nbsp;
										<asp:DropDownList ID="ddlCountry" runat="server"></asp:DropDownList><asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="ddlCountry" ErrorMessage="Please select a Country "></asp:RequiredFieldValidator></td>
                                    <td class="largewordingbold" valign="top" align="right" width="25%"><b>Zip/Postal Code</b></td>
                                    <td class="largewordingbold" valign="top" align="left" width="25%">&nbsp;<asp:TextBox ID="txtZip" runat="server" Width="84px" MaxLength="20"></asp:TextBox><asp:RequiredFieldValidator ID="rfv_zip" runat="server" ControlToValidate="txtZip" ErrorMessage="Zip/Postal Code required"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="rev_zip" runat="server" ControlToValidate="txtZip" ErrorMessage="Invalid Zip/Postal code"
                                        ValidationExpression="^\d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z]\d[A-Z]\d$"></asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" width="100%" colspan="4">Please ensure credit card 
										information entered is accurate. Credit Card holder name,
                                        Card number and Card expiration date should be correct. If payment fails for some reason, you will need 
										to start all over again.</td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px" nowrap="noWrap"><b>Credit Card Information</b></td>
                                    <td class="largewordingbold" align="left" width="75%" colspan="3">
                                        <img height="27" alt="We Accept American Express, Discover,Mastercard and Visa cards"
                                            src="images/validCreditcards.gif" width="125"></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px"><b>Card Holder Name</b></td>
                                    <td class="largewordingbold" align="left" width="75%" colspan="3">&nbsp;<asp:TextBox ID="txtCardHolderName" runat="server" MaxLength="50"></asp:TextBox>(as
                                        it exactly appears on your card)<br />
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" runat="server" ControlToValidate="txtCardHolderName"
                                            ErrorMessage="Card Holder &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;Name Required"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px"><b>Card Number</b></td>
                                    <td class="largewordingbold" align="left" width="75%">&nbsp;<asp:TextBox ID="txtCardNumber" runat="server" MaxLength="20"></asp:TextBox><asp:RequiredFieldValidator ID="Requiredfieldvalidator8" runat="server" ControlToValidate="txtCardNumber" ErrorMessage="CardNumber &#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;&#9;Required"></asp:RequiredFieldValidator><asp:Label ID="lblCardError" runat="server" ForeColor="Red"></asp:Label><asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" ControlToValidate="txtCardNumber"
                                        ErrorMessage="Invalid card number - please enter only numbers - no spaces or dashes" ValidationExpression="\d{15,16}"></asp:RegularExpressionValidator></td>

                                    <td class="largewordingbold" align="right" style="height: 44px; width: 23%; text-align: right;">
                                        <asp:Label ID="lblcvv" runat="server" Text="CVV" Font-Bold="True" Height="21px" Width="42px"></asp:Label></td>
                                    <td class="largewordingbold" align="right" style="height: 44px; width: 50%; text-align: left;">
                                        <asp:TextBox ID="txtcvv" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                        <asp:Label ID="lblErrCvv" runat="server" Font-Bold="True" Font-Size="X-Small" ForeColor="Red"></asp:Label>&nbsp;
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidatorcvv" runat="server" ControlToValidate="txtcvv" ErrorMessage="Credit Card Verification Number Required"></asp:RequiredFieldValidator>
                                        <img alt="For Visa, MasterCard, and Discover cards, the card code is the last 3 digit number located on the back of your card on or above your signature line. For an American Express card, it is the 4 digits on the FRONT above the end of your card number."
                                            src="Images/CVV2.jpg" onclick="javascript:return displayalert1" height="39px" style="width: 102px" />
                                        <asp:HyperLink ID="Hypcvv" NavigateUrl="~/cvv.aspx" runat="server" Width="174px" onclick="window.open (this.href, 'popupwindow','width=400,height=300,scrollbars,resizable');return false;">Where do I find CVV Code?</asp:HyperLink></td>

                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="right" style="width: 162px"><b>Card Expiration</b></td>
                                    <td class="largewordingbold" width="75%" colspan="3"><b>Month</b>&nbsp;
										<asp:DropDownList ID="ddlMonth" runat="server" Width="42px">
                                            <asp:ListItem Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList><b>Year</b>&nbsp;
										<asp:DropDownList ID="ddlYear" runat="server" Width="66px"></asp:DropDownList><asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" ControlToValidate="ddlMonth" ErrorMessage="Please select a Month"></asp:RequiredFieldValidator><asp:RequiredFieldValidator ID="Requiredfieldvalidator10" runat="server" ControlToValidate="ddlYear" ErrorMessage="Please select a Year"></asp:RequiredFieldValidator></td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="left" width="100%" colspan="4"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="height: 177px">
                            <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
                                <tr>
                                    <td class="largewordingbold" colspan="2" nowrap="noWrap">*** Please read the following notes before you 
										proceed with payment:</td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="left">
                                        <font color="#ff0000">1. After clicking on "Submit for Payment," please be patient and wait for the payment confirmation screen. This can take 10 seconds or up to 3 minutes depending on Internet traffic and the speed of your connection. </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="left">
                                        <font class="ErrorFont">2. Please do not use "Back Button" or "Refresh button" on your Browser to avoid any duplicate charges to your Credit Card. </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="left"><font color="#ff0000">3. We had instances of people pressing "Submit for Payment" multiple times due to impatience, which resulted in multiple charges. </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="largewordingbold" align="left"><font color="#ff0000">4. If you do not want to pay at this time
												<asp:LinkButton ID="lbRegistration" runat="server" CausesValidation="False">Click Here </asp:LinkButton>to 
												go back to Registration Page. </font>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" colspan="2" align="center">
                            <table id="Table4" cellspacing="1" cellpadding="1" width="100%" border="0">
                                <tr>
                                    <td class="largewordingbold" align="left" style="height: 34px">Clicking Submit for Payment will enable 
										credit card payment. Your registration will
                                        be completed, after your credit card payment is processed and a confirmation is
                                        received.</td>
                                </tr>
                                <tr>
                                    <td align="center" style="height: 55px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                                        <asp:LinkButton ID="lbContinue" runat="server" Font-Bold="True" Font-Size="Medium">Submit for Payment</asp:LinkButton>&nbsp;&nbsp;
                                        (Once paid, your payment is non-refundable.)

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
    </table>
    <asp:Label ID="lbltest" runat="server"></asp:Label>
</asp:Content>
