﻿Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ManageVoucher
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim dblRegFee As Double
    Dim BusType As String
    Dim IRSCat As String
    Dim ChapterCode As String
    Dim Amount As String
    Dim DonationType As String
    Dim DonationID As Integer
    Dim DonorType As String
    Dim setFlag As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Session("LoggedIn") = "true"
            Session("LoginId") = "4240"
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If Not IsPostBack Then
                TrDetailView.Visible = False
                Try
                    If Request.QueryString("id") = 1 Then
                        TxtFrom.Text = Session("Frmdate")
                        txtTo.Text = Session("ToDate")
                        ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(Session("DCatID")))
                        If ddlCategory.SelectedValue = 3 Then
                            BtnDetRawData.Visible = True
                            BtnExportList.Visible = False
                        ElseIf ddlCategory.SelectedValue = 4 Then
                            BtnDetRawData.Visible = False
                            BtnExportList.Visible = False
                            'ElseIf ddlCategory.SelectedValue = 1 Then
                            '    BtnDetRawData.Visible = True
                            '    BtnExportList.Visible = True
                        Else
                            BtnDetRawData.Visible = False
                            BtnExportList.Visible = True
                        End If
                        GetDonationReceipt(Session("ManageVouchersSQl"))
                        If Session("DCatID") = 4 Then
                            loadbank()
                            trbanks.Visible = True
                        End If
                    End If
                Catch ex As Exception
                End Try
                '' Show GeneralLedger table Data
                'Response.Write("<script language='javascript'>")
                'Response.Write(" window.open('ShowGeneralLedger.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');")
                'Response.Write("</script>")
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    'lstBank
    Private Sub loadbank()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank WHERE BankID in (1,2,3,4,5,6,7)")
        lstBank.DataSource = ds
        lstBank.DataTextField = "BankCode"
        lstBank.DataValueField = "BankID"
        lstBank.DataBind()
        lstBank.Items.Insert(0, New ListItem("All", 0))
        lstBank.SelectedIndex = 0

    End Sub

    Private Sub GetDonationReceipt(ByVal StrSQL As String)
        'gvDonation
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Session("StrSQL") = StrSQL
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)


        If dsDonation.Tables(0).Rows.Count > 0 Then
            If ddlCategory.SelectedValue = 3 Then
                BtnDetRawData.Visible = True
                BtnExportList.Visible = False
            ElseIf ddlCategory.SelectedValue = 4 Then
                BtnDetRawData.Visible = False
                BtnExportList.Visible = False
                'ElseIf ddlCategory.SelectedValue = 1 Then
                '    BtnDetRawData.Visible = True
                '    BtnExportList.Visible = True
            Else
                BtnDetRawData.Visible = False
                BtnExportList.Visible = True
            End If
        End If


        If ddlCategory.SelectedValue = 1 Or ddlCategory.SelectedValue = 2 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                gvDonation.Visible = True
                GVCCRevenues.Visible = False
                GVTDAIncome.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                gvExp.Visible = False
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                setFlag = False
                gvDonation.DataSource = dsDonation.Tables(0)
                gvDonation.DataBind()
                If ddlCategory.SelectedValue = 2 Then
                    gvDonation.Columns(11).Visible = False
                Else
                    gvDonation.Columns(11).Visible = True
                End If
                gvDonation.DataBind()
                If setFlag = True Then
                    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                Else
                    btnGenAll.Attributes.Remove("onclick")
                End If
            Else
                gvDonation.DataSource = Nothing
                gvDonation.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                gvExp.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = 5 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                gvDonation.Visible = False
                GVCCRevenues.Visible = False
                GVTDAIncome.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                gvExp.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True

                'setFlag = False
                gvExp.DataSource = dsDonation.Tables(0)
                gvExp.DataBind()
                If gvExp.Rows.Count > 0 Then
                    For i As Integer = 0 To gvExp.Rows.Count
                        gvExp.Columns(5).ItemStyle.ForeColor = Color.Black
                        gvExp.Columns(1).Visible = True
                        gvExp.Columns(2).Visible = True
                    Next
                End If

                'If setFlag = True Then
                '    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                'Else
                '    btnGenAll.Attributes.Remove("onclick")
                'End If
            Else
                Dim StrSQLExp As String = "Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber,'Not Available' as DateCashed, E.DatePaid,"
                StrSQLExp = StrSQLExp + "'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,"
                StrSQLExp = StrSQLExp + " E.BankID From ExpJournal E Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7)  WHERE E.CheckNumber is Not null "
                StrSQLExp = StrSQLExp + " AND (E.Account is Not null or E.TransType = 'Transfers') AND E.DatePaid Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' "
                StrSQLExp = StrSQLExp + " Group BY E.DatePaid, E.CheckNumber,E.BankID ORDER BY E.CheckNumber,E.DatePaid "

                Dim dsExp1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQLExp)
                If dsExp1.Tables(0).Rows.Count > 0 Then
                    TrDonation.Visible = True
                    gvDonation.Visible = False
                    GVCCRevenues.Visible = False
                    GVTDAIncome.Visible = False
                    GVACT_EFTDon.Visible = False
                    GVBnk_CCFees.Visible = False
                    GVTransfers.Visible = False
                    GVBuyTrans.Visible = False
                    gvExp.Visible = True
                    btnGenAll.Visible = True
                    btnAddUpdateGL.Visible = True

                    gvExp.DataSource = dsExp1.Tables(0)
                    gvExp.DataBind()
                    For i As Integer = 0 To gvExp.Rows.Count
                        gvExp.Columns(5).ItemStyle.ForeColor = Color.Red
                        gvExp.Columns(1).Visible = False
                        gvExp.Columns(2).Visible = False
                    Next
                    btnGenAll.Visible = False
                    btnAddUpdateGL.Visible = False
                Else
                    gvExp.DataSource = Nothing
                    gvExp.DataBind()
                    lblErr.Text = "Sorry No data found"
                    gvExp.Visible = False
                    gvDonation.Visible = False
                    GVCCRevenues.Visible = False
                    GVACT_EFTDon.Visible = False
                    GVBnk_CCFees.Visible = False
                End If
            End If
        ElseIf ddlCategory.SelectedValue = 3 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVCCRevenues.Visible = True
                ' btnGenAll.Visible = True
                'setFlag = False
                GVCCRevenues.DataSource = dsDonation.Tables(0)
                GVCCRevenues.DataBind()
                'If setFlag = True Then
                '    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                'Else
                '    btnGenAll.Attributes.Remove("onclick")
                'End If
            Else
                GVCCRevenues.DataSource = Nothing
                GVCCRevenues.DataBind()
                lblErr.Text = "Sorry No data found"
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                gvExp.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = 4 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                gvDonation.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVTDAIncome.Visible = True
                ' btnGenAll.Visible = True
                'setFlag = False
                'GVTDAIncome.Columns(6).Visible = True
                GVTDAIncome.DataSource = dsDonation.Tables(0)
                GVTDAIncome.DataBind()
                'GVTDAIncome.Columns(6).Visible = False
                'If setFlag = True Then
                '    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                'Else
                '    btnGenAll.Attributes.Remove("onclick")
                'End If
            Else
                GVTDAIncome.DataSource = Nothing
                GVTDAIncome.DataBind()
                lblErr.Text = "Sorry No data found"
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                gvExp.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "6" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVACT_EFTDon.Visible = True
                GVACT_EFTDon.DataSource = dsDonation.Tables(0)
                GVACT_EFTDon.DataBind()
            Else
                GVACT_EFTDon.DataSource = Nothing
                GVACT_EFTDon.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "7" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVBnk_CCFees.Visible = True
                'GVBnk_CCFees.Columns(2).Visible = True
                'GVBnk_CCFees.Columns(9).Visible = True
                GVBnk_CCFees.DataSource = dsDonation.Tables(0)
                GVBnk_CCFees.DataBind()
                'GVBnk_CCFees.Columns(2).Visible = False
                'GVBnk_CCFees.Columns(8).Visible = False
                ' GVBnk_CCFees.Columns(9).Visible = False
            Else
                GVBnk_CCFees.DataSource = Nothing
                GVBnk_CCFees.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "8" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVBuyTrans.Visible = False
                GVTransfers.Visible = True
                GVTransfers.Columns(14).Visible = True
                GVTransfers.Columns(2).Visible = True
                GVTransfers.Columns(13).Visible = True
                GVTransfers.Columns(6).Visible = True
                GVTransfers.Columns(8).Visible = True
                GVTransfers.Columns(9).Visible = True

                GVTransfers.DataSource = dsDonation.Tables(0)
                GVTransfers.DataBind()
                Dim AvgValue As Double

                For i As Integer = 0 To GVTransfers.Rows.Count - 1
                    If GVTransfers.Rows(i).Cells(10).Text = 0 Then
                        AvgValue = (Convert.ToDouble(GVTransfers.Rows(i).Cells(14).Text.ToString()) * GetAvgPrice(GVTransfers.Rows(i).Cells(2).Text, GVTransfers.Rows(i).Cells(13).Text, GVTransfers.Rows(i).Cells(14).Text, GVTransfers.Rows(i).Cells(6).Text, GVTransfers.Rows(i).Cells(8).Text, GVTransfers.Rows(i).Cells(9).Text))
                        GVTransfers.Rows(i).Cells(10).Text = String.Format("{0:c}", AvgValue)
                        'Format$(GVTransfers.Rows(i).Cells(10).Text, "Currency")
                    End If
                Next
            Else
                GVTransfers.DataSource = Nothing
                GVTransfers.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "9" Or ddlCategory.SelectedValue = "10" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                btnGenAll.Visible = True
                btnAddUpdateGL.Visible = True
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = True
                GVBuyTrans.DataSource = dsDonation.Tables(0)
                'GVBuyTrans.DataBind()
                If ddlCategory.SelectedValue = "10" Then
                    GVBuyTrans.Columns(13).Visible = True
                    GVBuyTrans.Columns(14).Visible = True
                Else
                    GVBuyTrans.Columns(13).Visible = False
                    GVBuyTrans.Columns(14).Visible = False
                End If
                GVBuyTrans.DataBind()
            Else
                GVBuyTrans.DataSource = Nothing
                GVBuyTrans.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
            End If
        End If
    End Sub

    Function GetAvgPrice(ByVal BankID As Integer, ByVal CurrTicker As String, ByVal Quantity As Double, ByVal TransDate As String, ByVal sFY_BegDate As String, ByVal sFY_EndDate As String) As Double
        Dim sSql As String
    
        If BankID = 0 Then
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=(select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID in (select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
        Else
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=" & BankID & " and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID =" & BankID & " AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
        End If

        Dim rsBankTran As SqlDataReader = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Shares", Type.GetType("System.Decimal"))
        dt.Columns.Add("Price", Type.GetType("System.Decimal"))
        dt.Columns.Add("Amount", Type.GetType("System.Decimal"))
        dt.Columns.Add("OutstandingShares", Type.GetType("System.Decimal"))
        dt.Columns.Add("CostBasis", Type.GetType("System.Decimal"))
        dt.Columns.Add("AvgPrice", Type.GetType("System.Decimal"))
        Dim currentIndex As Integer = -1
        While rsBankTran.Read
            If rsBankTran("TransCat").ToString().ToLower.Trim = "begbal" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount")
                dr("OutstandingShares") = rsBankTran("Quantity")
                dr("CostBasis") = rsBankTran("NetAmount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf currentIndex = -1 Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = 0.0
                dr("Price") = 0.0
                dr("Amount") = 0.0
                dr("OutstandingShares") = 0.0
                dr("CostBasis") = 0.0
                dr("AvgPrice") = 0.0
                dt.Rows.Add(dr)
            End If
            If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("Quantity"), rsBankTran("Quantity") * -1)
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)

            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity") * -1
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            End If
        End While
        If currentIndex < 0 Then
            Return 0.0
        Else
            Return Math.Round(dt.Rows(currentIndex)("AvgPrice"), 2)
        End If
    End Function
    Protected Sub gvDonation_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDonation.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(9).Enabled = e.Row.DataItem("DSlipNoStatus")
            ''** e.Row.Cells(7).Enabled = e.Row.DataItem("ReconcileStatus")
            If Not e.Row.DataItem("Reconcile").ToString().Trim = "Matched" Then
                setFlag = True
                e.Row.Cells(2).Attributes.Add("onclick", "return confirm('Not reconciled.  Do you still want a QB file?');")
            End If
        End If
    End Sub
    
    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TrDetailView.Visible = False
        btnClose.Visible = False
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter to date in mm/dd/yyyy format"
            Exit Sub
        ElseIf ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            lblErr.Text = ""
            Dim StrSQL As String
            If ddlCategory.SelectedValue = "1" Then
                StrSQL = " Select Distinct Top 100 sum(D.Amount) as Amount, D.DepositSlip, D.DepositDate,'DGN'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlip) as VoucherNo"
                StrSQL = StrSQL & " , CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile,CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0) AND (COUNT(BrS.DepCheckNo)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted ,"
                StrSQL = StrSQL & " CASE WHEN (COUNT(BS.DepSlipNumber)>0 OR COUNT(BrS.DepCheckNo) > 0) then CASE WHEN D.BankID in (4,5,6,7) THEN Convert(Varchar,BrS.DepCheckNo) ELSE Convert(Varchar,BS.DepSlipNumber) END ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END END "
                StrSQL = StrSQL & " as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null AND BrS.DepCheckNo is NULL) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'False' ELSE 'True' END as ReconcileStatus,CASE WHEN D.BankID in (4,5,6,7) THEN BrS.DepCheckNo  ElSE BS.DepSlipNumber END as DepSlipNumber "
                'StrSQL = StrSQL & " ,CASE WHEN sum(D.Amount) = BS.Amount THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END  END as Reconcile, CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted ,"
                'StrSQL = StrSQL & " CASE WHEN COUNT(BS.DepSlipNumber) > 0 then Convert(Varchar,BS.DepSlipNumber) ELSE  CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END"
                'StrSQL = StrSQL & " END as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = BS.Amount THEN 'False' ELSE 'True' END as ReconcileStatus,BS.DepSlipNumber"
                StrSQL = StrSQL & ",D.BankID from DonationsInfo  D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON  B.BankID=D.BankID AND B.TransCat='Deposit'  AND D.DepositDate = B.TransDate "
                StrSQL = StrSQL & " Left Outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND  BS.BankID=D.BankID AND BS.TransCat='Deposit' AND D.DepositSlip = BS.DepSlipNumber"
                StrSQL = StrSQL & " Left Outer Join BrokTrans BrS ON D.DepositDate=BrS.TransDate AND BrS.BankID=D.BankID AND BrS.TransCat='Donation' AND D.DepositSlip = BrS.DepCheckNo AND D.BankID in (4,5,6,7)"
                StrSQL = StrSQL & " where D.METHOD  IN ('Check','CASH') AND D.DepositDate  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' Group BY D.DepositSlip, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount,BrS.NetAmount,Brs.DepCheckNo order by  D.DepositDate,D.DepositSlip"

                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 1
                GetDonationReceipt(StrSQL)
            ElseIf ddlCategory.SelectedValue = "2" Then
                StrSQL = "Select  Top 100 sum(D.Amount) as Amount, RTRIM(D.DepositSlipNo) as DepositSlip, D.DepositDate,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlipNo) as VoucherNo ,"
                StrSQL = StrSQL & " CASE WHEN sum(D.Amount) = BS.Amount THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile, CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted , "
                StrSQL = StrSQL & " CASE WHEN COUNT(BS.DepSlipNumber) > 0 then Convert(Varchar,BS.DepSlipNumber) ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END "
                StrSQL = StrSQL & " END as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = BS.Amount THEN 'False' ELSE 'True' END as ReconcileStatus,BS.DepSlipNumber,D.BankID from OtherDeposits D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON B.BankID=D.BankID AND B.TransCat='Deposit' AND D.DepositDate = B.TransDate"
                StrSQL = StrSQL & "  Left outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND BS.BankID=D.BankID AND BS.TransCat IN ('Deposit','DepositError') AND D.DepositSlipNo = BS.DepSlipNumber where  D.DepositDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "  23:59' Group BY D.DepositSlipNo, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount order by D.DepositDate,D.DepositSlipNo "
                'Response.Write(StrSQL)
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 2
                GetDonationReceipt(StrSQL)
            ElseIf ddlCategory.SelectedValue = "3" Then
                '3 - CC Revenues -
                'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
                ' having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0
                StrSQL = "SELECT SUM(Amount) as Amount,'CGN01'+Replace(CONVERT(VARCHAR(10), StartDate, 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), EndDate, 101),'/','') as VoucherNo ,StartDate,EndDate from("
                StrSQL = StrSQL & " Select Sum(NFG.TotalPayment) as Amount "  ' ,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " ,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate "
                'StrSQL = StrSQL & "  ,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(NFG.MS_TransDate)-1),NFG.MS_TransDate),101) AS StartDate ,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(DATEADD(mm,1,NFG.MS_TransDate))),DATEADD(mm,1,NFG.MS_TransDate)),101) AS EndDate "
                StrSQL = StrSQL & " from NFG_Transactions NFG where NFG.MS_TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 ' Group by NFG.MS_TransDate"
                'Commented on 21-11-2013 
                'StrSQL = StrSQL & " having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0 or SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) < 0 "

                ''Added to Merge CCRevenueAdjustments
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " Select (SUM(CreditCC)- SUM(Registrations)) as Amount " ','CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate from "
                StrSQL = StrSQL & ",'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate "
                'StrSQL = StrSQL & ", CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(TransDate)-1),TransDate),101) AS StartDate,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(DATEADD(mm,1,TransDate))),DATEADD(mm,1,TransDate)),101) AS EndDate "
                StrSQL = StrSQL & " From (select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) "
                StrSQL = StrSQL & " Union All "
                StrSQL = StrSQL & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
                StrSQL = StrSQL & ")T3 Group by TransDate "
                StrSQL = StrSQL & ")T4 group by StartDate,EndDate Order By StartDate"  'VoucherNo,
                'Response.Write(StrSQL)

                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 3
                GetDonationReceipt(StrSQL)

            ElseIf ddlCategory.SelectedValue = "4" Then
                '4 - TDA Accounts -
                Dim i As Integer
                Dim BankIDs As String = "0"
                If lstBank.Items(i).Selected = True Then
                    BankIDs = "1,2,3,4,5,6,7"
                Else
                    For i = 0 To lstBank.Items.Count - 1
                        If lstBank.Items(i).Selected Then
                            BankIDs = BankIDs & "," & lstBank.Items(i).Value
                        End If
                    Next
                End If
                StrSQL = " Select SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,B.BankID,B.BankCode,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " FROM   BrokTrans B Inner Join Bank BB On B.BankID=BB.BankID WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 '"
                StrSQL = StrSQL & " AND B.BankID in (" & BankIDs & ") Group By   B.BankID,B.BankCode " ' Order By B.BankID CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) " 
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " Select SUM(B.Amount) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,B.BankID,BB.BankCode ,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " FROM  BankTrans B Inner Join Bank BB On B.BankID=BB.BankID "
                StrSQL = StrSQL & " WHERE B.TransType='Credit' and B.TransCat='Interest' AND  B.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 '"
                StrSQL = StrSQL & " AND B.BankID in (" & BankIDs & ") Group By   B.BankID,BB.BankCode Order by B.BankID"

                'Response.Write(StrSQL)
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("BankIDs") = BankIDs
                Session("DCatID") = 4
                GetDonationReceipt(StrSQL)
            ElseIf ddlCategory.SelectedValue = "5" Then
                '5 - Vouchers - Expense checks
                StrSQL = " Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber," 'Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End as DateCashed"
                StrSQL = StrSQL & " Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End "
                StrSQL = StrSQL & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else 'Not Available' End"
                StrSQL = StrSQL & " Else 'Not Available' End End as DateCashed"
                StrSQL = StrSQL & ", E.DatePaid,'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,E.BankID " 'Case When Ba.Transdate is not null Then Ba.Transdate Else '' End as DateCashed,
                StrSQL = StrSQL & " From ExpJournal E Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7)"
                StrSQL = StrSQL & " LEFT Join BankTrans Ba on CONVERT(Varchar,Ba.CheckNumber)=E.CheckNumber and Ba.BankID=E.BanKID" ' AND Ba.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
                StrSQL = StrSQL & " LEFT Join BrokTrans Br on (CONVERT(Varchar,Br.ExpCheckNo)=E.CheckNumber OR CONVERT(Varchar,Br.DepCheckNo)=E.CheckNumber) and Br.BankID=E.BanKID " 'AND Br.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
                StrSQL = StrSQL & " WHERE E.CheckNumber is Not null AND (ba.BankTransID IS NOT NULL OR Br.BrokTransID IS NOT Null Or E.TransactionID is not null) AND  (E.Account is Not null or E.TransType = 'Transfers')" ' AND E.DatePaid  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Modified on 29-04-2013

                StrSQL = StrSQL & " And  Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else E.DatePaid End "
                StrSQL = StrSQL & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else E.DatePaid End"
                StrSQL = StrSQL & " Else E.DatePaid End End "
                StrSQL = StrSQL & " Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'"
                StrSQL = StrSQL & " Group BY E.DatePaid, E.CheckNumber,E.BankID,Ba.Transdate, Br.TransDate ORDER BY E.CheckNumber,E.DatePaid"

                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 5
                GetDonationReceipt(StrSQL)

            ElseIf ddlCategory.SelectedValue = "6" Then
                'ACT_EFT Donations
                StrSQL = "SELECT  B.BankID,B.BankTransID, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,"
                StrSQL = StrSQL & "B.VendCust, N.Description ,N.DonorType,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate,B.MemberID FROM BankTrans B "
                StrSQL = StrSQL & "LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat = C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID=1"
                StrSQL = StrSQL & "LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat = H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID=2"
                StrSQL = StrSQL & "Inner Join NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
                StrSQL = StrSQL & "Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "'"
                StrSQL = StrSQL & "Group By B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate,B.Vendcust,B.BankTransID,B.MemberID Order by B.TransDate"

                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 6
                GetDonationReceipt(StrSQL)

                'lblErr.Text = "This Category is under Development"
            ElseIf ddlCategory.SelectedValue = "7" Then
                'Vouchers Bank Service Charges
                StrSQL = "SELECT B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.VendCust,'') as VoucherNo," ''_'+RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
                StrSQL = StrSQL & " E.AccountName,B.TransDate,'VPE' as TransType,E.ExpCatCode,  IsNull(B.VendCust,'') as VendCust,B.Reason,B.AddInfo"
                StrSQL = StrSQL & " FROM BankTrans  B "
                StrSQL = StrSQL & " INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo "
                StrSQL = StrSQL & " Where TransType= 'DEBIT' And TransCat = 'CreditCard' and TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group By B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " SELECT B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ + IsNull(B.VendCust,'') as VoucherNo," '_'+'RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
                StrSQL = StrSQL & " E.AccountName,B.TransDate,'VPE' as TransType,E.ExpCatCode,IsNull(B.VendCust,'') as  VendCust,B.Reason,B.AddInfo"
                StrSQL = StrSQL & " FROM BankTrans B "
                StrSQL = StrSQL & " INNER JOIN ExpenseCategory  E ON B.TransCat= E.ExpCatCode "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo "
                StrSQL = StrSQL & " where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group By B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 7
                GetDonationReceipt(StrSQL)
                'Response.Write(StrSQL)
            ElseIf ddlCategory.SelectedValue = "8" Then
                ''TransferOut
                StrSQL = "SELECT B.BankID,B.BrokTransID,B.BankCode,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+"
                StrSQL = StrSQL & " Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as VoucherNo,B.TransDate,B.TransCat,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate,'IGT' as TransType,B.AssetClass,B.Ticker,B.Quantity,B.Price,"
                StrSQL = StrSQL & " B.NetAmount as Amount,B.RestType,B1.RestType as RestTypeTo,B1.BankID as ToBankID,N.Description FROM BrokTrans B "
                StrSQL = StrSQL & " Inner join BrokTrans B1 On B.TransType = B1.TransType and B.TransDate= B1.TransDAte and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and 
                StrSQL = StrSQL & " Inner join NSFAccounts N On N.BankID = B.BankId " ' and B.RestType=N.RestrictionType "
                StrSQL = StrSQL & " and (B.RestType=N.RestrictionType Or N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end  ))"
                StrSQL = StrSQL & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
                StrSQL = StrSQL & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Transdate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Order by B.TransDate"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 8
                'Response.Write(StrSQL) '
                GetDonationReceipt(StrSQL)

                'lblErr.Text = "This Category is under Development"
            ElseIf ddlCategory.SelectedValue = "9" Then
                ''Buy Transactions
                StrSQL = "SELECT B.BankID,B.BankCode,N.AccNo,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount"
                StrSQL = StrSQL & " ,N.Description, 'IGB' as TransType,B.AssetClass,"
                StrSQL = StrSQL & " 'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,N.Description,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate,B.Disposed_Basis,B.Capital_Gains FROM BrokTrans B "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
                StrSQL = StrSQL & " Where B.Transcat='Buy' and B.TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass,B.Disposed_Basis,B.Capital_Gains Order by B.TransDate"
                'Response.Write(StrSQL)
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 9
                GetDonationReceipt(StrSQL)
                'lblErr.Text = "This Category is under Development"
            ElseIf ddlCategory.SelectedValue = "10" Then
                ''Sell Transactions
                StrSQL = "SELECT Distinct B.BankID,B.BankCode,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount,N.AccNo" '
                StrSQL = StrSQL & " , 'IGS' as TransType,B.AssetClass,N.Description,B.Ticker," '
                StrSQL = StrSQL & " 'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate,B.Disposed_Basis,B.Capital_Gains FROM BrokTrans B "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
                StrSQL = StrSQL & " Where B.Transcat='Sell' and B.TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " and N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end  )"
                StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass,B.Ticker,B.Disposed_Basis,B.Capital_Gains Order by B.TransDate" '
                'Response.Write(StrSQL)
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 10
                GetDonationReceipt(StrSQL)
                'lblErr.Text = "This Category is under Development"
            End If

        End If
    End Sub
    Protected Sub gvDonation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDonation.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = gvDonation.Rows(index)

        'Export
        'Dim gvtemp As New GridView
        Dim StrSQl As String
        If e.CommandName = "ViewTrans" Then
            If Session("DCatID") = 2 Then
                StrSQl = " SELECT OD.OtherDepositID as DonationID, OD.DepositSlipNo as DepositSlip,CASE WHEN OD.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as DName,CONVERT(VARCHAR(10), OD.DepositDate, 101) AS DepositDate,CONVERT(VARCHAR(10), OD.DepositDate, 101) AS DonationDate,OD.RevenueType as BusType,CASE WHEN OD.RevenueType='Sales' then SC.SalesCatDesc ELSE S.SponsorDesc   END as IRScat ,CASE WHEN OD.DonorType = 'SPOUSE' THEN 'IND' ELSE OD.DonorType END as DonorType,OD.MEMBERID, Acc.AccNo, Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,Ch.ChapterCode,OD.AMOUNT,Acc.RestrictionType as DonationType,Acc.BusinessType,ISNULL(B.BankCode,'') as BankName"
                StrSQl = StrSQl & " FROM OtherDeposits OD Left Join Bank B ON B.BankID=OD.BankID Inner Join NSFAccounts Acc ON Acc.DonorType = CASE WHEN OD.DonorType = 'SPOUSE' OR OD.DonorType = 'IND' OR OD.DonorType IS Null  THEN 'IND/SPOUSE' ELSE OD.DonorType END  AND OD.RestrictionType=Acc.RestrictionType Inner JOIn Chapter Ch ON OD.ChapterID=Ch.ChapterID"
                StrSQl = StrSQl & " Left Join OrganizationInfo O ON OD.DonorType = 'OWN' and O.AutoMemberid =OD.Memberid"
                StrSQl = StrSQl & " Left Join IndSpouse I ON OD.DonorType <> 'OWN' AND OD.MEMBERID = I.AutoMemberID "
                StrSQl = StrSQl & " Left Join  SalesCat SC ON  SC.salescatID=OD.SalesID AND OD.RevenueType='Sales' "
                StrSQl = StrSQl & " Left Join SponsorCat S ON S.SponsorID= OD.SponsorID AND OD.RevenueType='Sponsor' "
                StrSQl = StrSQl & " WHERE  OD.DepositSlipNo=" & gvDonation.DataKeys(index).Value & ""
                StrSQl = StrSQl & "  AND OD.DepositDate='" & row.Cells(5).Text & "'"
                grdEditVoucher.Columns(0).Visible = False
                grdEditVoucher.Columns(4).Visible = False
                grdEditVoucher.Columns(7).HeaderText = "RevenueType"
                grdEditVoucher.Columns(8).HeaderText = "Sales/SponsorCat"
            Else
                StrSQl = " Select  D.DonationID, D.DepositSlip, CASE WHEN D.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as DName,CONVERT(VARCHAR(10), D.DonationDate, 101) as DonationDate, CONVERT(VARCHAR(10), D.DepositDate, 101)   as DepositDate, D.DonorType, O.BusType,O.IRScat , D.DonationType , D.ChapterID, C.WebFolderName as ChapterCode,D.Amount,D.MemberID"
                StrSQl = StrSQl & " from DonationsInfo  D Inner Join Chapter C ON C.chapterid = D.ChapterID"
                StrSQl = StrSQl & " Left Join OrganizationInfo O ON D.DonorType = 'OWN' and O.AutoMemberid =D.Memberid"
                StrSQl = StrSQl & " Left Join IndSpouse I ON D.DonorType <> 'OWN' AND D.MEMBERID = I.AutoMemberID "
                StrSQl = StrSQl & " where   D.DepositSlip = " & gvDonation.DataKeys(index).Value & " AND D.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " order by  D.DonationID,D.DonorType,  D.DonationType,  O.BusType,  D.ChapterID"
                grdEditVoucher.Columns(0).Visible = True
                grdEditVoucher.Columns(4).Visible = True
                grdEditVoucher.Columns(7).HeaderText = "BusType"
                grdEditVoucher.Columns(8).HeaderText = "IRScat"
            End If
            Session("StrTrans") = StrSQl
            LoadErrorData()
        ElseIf e.CommandName = "GenerateQB" Then
            Dim Filename As String
            If Session("DCatID") = 2 Then
                'StrSQl = " SELECT OD.DepositSlipNo,CONVERT(VARCHAR(10), OD.DepositDate, 101) AS DepositDate, CASE WHEN OD.DonorType = 'SPOUSE' THEN 'IND' ELSE OD.DonorType END as DonorType,OD.MEMBERID, Acc.AccNo, Acc.Description, Ch.webfoldername as Class,Ch.ChapterID,SUM(OD.AMOUNT) as Amount ,Acc.RestrictionType as DonationType,Acc.BusinessType,ISNULL(B.BankCode,'') as BankName FROM OtherDeposits OD Left Join Bank B ON B.BankID=OD.BankID Inner Join NSFAccounts Acc ON Acc.DonorType = CASE WHEN OD.DonorType = 'SPOUSE' OR OD.DonorType = 'IND' OR OD.DonorType IS Null  THEN 'IND/SPOUSE' ELSE OD.DonorType END  AND OD.RestrictionType=Acc.RestrictionType Inner JOIn Chapter Ch ON OD.ChapterID=Ch.ChapterID WHERE  OD.DepositSlipNo=" & Depositslip & " AND OD.DepositDate='" & DDate & "' Group BY OD.DepositSlipNo,OD.DepositDate,OD.DonorType,OD.MEMBERID, Acc.AccNo, Acc.Description, Ch.webfoldername,Ch.ChapterID,Acc.RestrictionType,Acc.BusinessType,B.BankCode "
                StrSQl = "select A,B,C,D,E,F,Sum(G),H,I,J from (select CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), OD.DepositDate, 101)   as D ,N.AccNo as E,Ch.WebFolderName as F,SUM(OD.AMOUNT) as G,'' as H, 'OTH'+ RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as I,0 as J  From OtherDeposits OD Inner Join NSFAccounts N ON OD.BankID=N.BankID and OD.RestrictionType=N.RestrictionType and  N.[Level]='L' INNER JOIN Chapter Ch ON N.ChapterID=Ch.ChapterID WHERE OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'" & " Group by OD.DepositDate,OD.DepositSlipNo,N.AccNo,OD.BankID,Ch.WebFolderName"
                StrSQl = StrSQl & " UNION ALL"
                'Fees 1/3 and 2/3
                StrSQl = StrSQl & " select  'SPL' as A,'' as B,'GENERAL JOURNAL' as C,T.DepositDate as D, T.AccNo as E,T.WebFolderName as F,-ROUND(Sum(T.Amount),2) as  G,'' as H, T.VoucherNo  as I, 1 as J  from ("
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51200  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "
                'Coaching
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "

                'PrepClub Coaching
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =19 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =19 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "

                '*************** Added on 09-08-2014 as Mr.Ramdev wanted to use a separate account 51450 for food sales*****************'
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51450  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.SalesID = 1 AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID UNION ALL "
                '********************************************************************************************************************************************'
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51400  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.SalesID <> 1 AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID UNION ALL "

                '/***************Added on Dec 10 2014 to inlcude Refund in Other Deposits*********************************'
                StrSQl = StrSQl & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,Ex.Account  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits  OD inner join ExpenseCategory EX on Ex.ExpCatID =OD.RefundId Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Refund'  AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,Ex.Account UNION ALL "
                '/**************************************************************************************************************************'

                StrSQl = StrSQl & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,CASE WHEN OD.EventID=1 or OD.EventID=2 THEN 51100 ELSE CASE WHEN OD.EventID=3 THEN 51200 ELSE 51300 END END  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sponsorship' AND OD.DepositSlipNo = " & gvDonation.DataKeys(index).Value & " AND OD.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID)"
                StrSQl = StrSQl & " T inner Join  NSFAccounts N ON T.AccNo = N.AccNo Group by T.DepositDate,T.VoucherNo,T.AccNo,N.Description,T.WebFolderName,T.State,T.Name "
                StrSQl = StrSQl & " UNION ALL"
                StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                StrSQl = StrSQl & " ) TMP Group by A,B,C,D,E,F,H,I,J Order By J,E"
            Else
                StrSQl = "select A,B,C,D,E,F,Sum(G),H,I,J from (select CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), D.DepositDate, 101)   as D ,Case when D.DonationType ='Temp Restricted' and D.Project ='DAF' Then 10108 Else N.AccNo End as E,Ch.WebFolderName as F,SUM(D.AMOUNT) as G,'' as H, 'DGN'+ RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) + REPLACE(CONVERT(VARCHAR(10), DepositDate, 101),'/','') + Convert(varchar,D.DepositSlip) as I,0 as J  From DonationsInfo D Inner Join NSFAccounts N ON D.BankID=N.BankID and D.DonationType=N.RestrictionType AND (N.InvIncType IS NULL OR N.InvIncType<>'Inv') INNER JOIN Chapter ch ON N.ChapterID = Ch.ChapterID WHERE D.DepositSlip = " & gvDonation.DataKeys(index).Value & " AND D.DepositDate='" & row.Cells(5).Text & "'" & ""
                'StrSQl = StrSQl & " and ((D.DonationType ='Temp Restricted' and D.Project ='DAF') Or N.accNo  <>10108)" ' and N.accNo  =10108 ((D.Project is null Or D.Project <>'DAF') and N.accNo  <>10108))"
                StrSQl = StrSQl & " and ((D.DonationType ='Temp Restricted' and D.Project ='DAF'  and D.BankID<>1) Or (N.accNo  <>10108 and D.BankID=1  ))"
                StrSQl = StrSQl & "  Group by D.DepositDate,D.DepositSlip,N.AccNo,D.BankID,Ch.WebFolderName,D.DonationType,D.Project"
                StrSQl = StrSQl & " UNION ALL"
                StrSQl = StrSQl & " select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), D.DepositDate, 101)  as D ,Acc.AccNo as E,Ch.Webfoldername as F,-D.AMOUNT as G,'' as H, 'DGN'+ RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) + REPLACE(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','') + Convert(varchar,D.DepositSlip) as I, 1 as J"
                StrSQl = StrSQl & " FROM DonationsInfo D Inner Join  NSFAccounts Acc ON    D.DonationType=Acc.RestrictionType AND Acc.AccType='I' AND Acc.DonorType='IND/SPOUSE' AND (Acc.InvIncType IS NULL OR Acc.InvIncType<>'Inv')"
                StrSQl = StrSQl & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID "
                StrSQl = StrSQl & " WHERE D.DonorType <>'OWN' and  D.DepositSlip = " & gvDonation.DataKeys(index).Value & " AND D.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " UNION ALL"
                StrSQl = StrSQl & " select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), D.DepositDate, 101)   as D ,Acc.AccNo as E,Ch.Webfoldername as F,-D.AMOUNT as G,'' as H, 'DGN'+ RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) + REPLACE(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','') + Convert(varchar,D.DepositSlip) as I, 1 as J"
                StrSQl = StrSQl & " FROM DonationsInfo D Inner Join  NSFAccounts Acc ON D.DonorType = Acc.DonorType  AND D.DonationType=Acc.RestrictionType AND Acc.AccType='I'"
                StrSQl = StrSQl & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID"
                StrSQl = StrSQl & " Inner Join  OrganizationInfo O ON D.MemberID = O.Automemberid AND D.DonorType = 'OWN' AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END"
                StrSQl = StrSQl & " WHERE D.DepositSlip = " & gvDonation.DataKeys(index).Value & " AND D.DepositDate='" & row.Cells(5).Text & "'"
                StrSQl = StrSQl & " UNION ALL"
                StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                StrSQl = StrSQl & " ) TMP Group by A,B,C,D,E,F,H,I,J Order By J,E"
            End If

            'StrSQl = StrSQl & " Order By  J,E"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("J")

                Dim sb As StringBuilder
                Dim i, j As Integer
                '  Dim Filename As String = Server.MapPath("ScoreSheets\") & "QB" & Now.Month & Now.Day & Now.Year & ".iif"
                'Filename = Filename & ds.Tables(0).Rows(0)("D").ToString().Replace("/", "") & gvDonation.DataKeys(index).Value.ToString().Trim & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                Filename = ds.Tables(0).Rows(0)("I").ToString().ToString().Trim & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                ' fp = File.CreateText(Filename)
                'fp.WriteLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!ENDTRNS" & vbTab)
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        Else
            ' Details
        End If

    End Sub
    Protected Sub GVBuyTrans_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVBuyTrans.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVBuyTrans.Rows(index)

        Dim frmdate As Date = Convert.ToDateTime(row.Cells(5).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(5).Text + " 23:59")
        Dim BankID As Integer = row.Cells(2).Text
        Dim VoucherNo As String = row.Cells(4).Text
        Dim AssetClass As String = row.Cells(11).Text

        Dim StrSQl As String = ""
        If e.CommandName = "GenerateQB" Then
            If Session("DCatID") = 9 Then
                StrSQl = "SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
                StrSQl = StrSQl & " Ch.WebFolderName as F,-Sum(B.NetAmount) as G,'' as H"
                StrSQl = StrSQl & " ,'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQl = StrSQl & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,1 as J FROM BrokTrans B "
                StrSQl = StrSQl & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))  and N.Description ='Investment-General Unrestricted'"
                StrSQl = StrSQl & " INNER JOIN Chapter Ch ON Ch.ChapterId = N.ChapterID"
                StrSQl = StrSQl & " Where B.Transcat='Buy' and B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.AssetClass='" & AssetClass & "' "
                StrSQl = StrSQl & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
                StrSQl = StrSQl & " UNION ALL"
                StrSQl = StrSQl & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
                StrSQl = StrSQl & " Ch.WebFolderName as F,Sum(B.NetAmount) as G,'' as H"
                StrSQl = StrSQl & " ,'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQl = StrSQl & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,0 as J  FROM BrokTrans B "
                StrSQl = StrSQl & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK')  and N.Description ='MMKT - General Unrestricted'"
                StrSQl = StrSQl & " INNER JOIN Chapter Ch ON Ch.ChapterId = N.ChapterID"
                StrSQl = StrSQl & " Where B.Transcat='Buy' and B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.AssetClass='" & AssetClass & "' "
                StrSQl = StrSQl & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
                StrSQl = StrSQl & " UNION ALL "
                StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                StrSQl = StrSQl & " Order by J"
            Else
                'StrSQl = "SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
                'StrSQl = StrSQl & " Ch.WebFolderName as F,-Sum(B.NetAmount) as G,'' as H"
                'StrSQl = StrSQl & " ,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                'StrSQl = StrSQl & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,1 as J FROM BrokTrans B "
                'StrSQl = StrSQl & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Investment-DAF Temp. Restricted' " 'and N.Description ='Investment-General Unrestricted'"
                'StrSQl = StrSQl & " INNER JOIN Chapter Ch ON Ch.ChapterId = N.ChapterID"
                'StrSQl = StrSQl & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.AssetClass='" & AssetClass & "' "
                'StrSQl = StrSQl & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
                'StrSQl = StrSQl & " UNION ALL"
                'StrSQl = StrSQl & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
                'StrSQl = StrSQl & " Ch.WebFolderName as F,Sum(B.NetAmount) as G,'' as H"
                'StrSQl = StrSQl & " ,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                'StrSQl = StrSQl & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,0 as J  FROM BrokTrans B "
                'StrSQl = StrSQl & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') and N.Description ='MMKT - DAF Temp. Restricted'" 'and N.Description ='MMKT - General Unrestricted'"
                'StrSQl = StrSQl & " INNER JOIN Chapter Ch ON Ch.ChapterId = N.ChapterID"
                'StrSQl = StrSQl & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.AssetClass='" & AssetClass & "' "
                'StrSQl = StrSQl & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
                'StrSQl = StrSQl & " UNION ALL "
                'StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                'StrSQl = StrSQl & " Order by J"

                '/************************************************************************/
                'Updated on Oct 16 2014 assumption of the following for all Sell  and Buy Transactions: 

                '                BankID(RestrictionType)
                '----------      ----------------------
                '4:              Temp(Restricted)
                '5:              Unrestricted()
                '6:              Perm(Restricted)
                '7:              Temp(Restricted)

                StrSQl = StrSQl & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-SUM(Distinct B.Quantity*C.AvgPrice) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,0 as J FROM BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & "  and N.RestrictionType in (Case when B.BankID =  4 then 'Unrestricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end) and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
                StrSQl = StrSQl & " and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares " 'And C.CostBasis = B.CostBasis
                StrSQl = StrSQl & " and B.AssetClass='" & AssetClass & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and N.Description ='Investment-DAF Temp. Restricted'
                StrSQl = StrSQl & " UNION ALL "
                StrSQl = StrSQl & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,SUM(B.NetAmount) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,0 as J FROM BrokTrans B INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and  N.InvIncType in ('MMK') and B.AssetClass not in ('Cash','MMK') Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & " and N.RestrictionType in (Case when B.BankID =  4 then 'Unrestricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end) and B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.AssetClass='" & AssetClass & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and N.Description ='MMKT - DAF Temp. Restricted' ' 'N.InvIncType in ('MMK','Inv') '  and((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and B.AssetClass not in ('Cash','MMK')
                StrSQl = StrSQl & " UNION ALL "
                StrSQl = StrSQl & " Select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,SUM(Distinct B.Quantity*C.AvgPrice) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,1 as J from BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null)and B.AssetClass not in ('Cash','MMK') and N.Description = Case when B.BankID <> 7 then 'Securities Sales - Cost'  Else 'Securities sales - Cost - Temp Rest' End Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & " and B.TransDate Between '" & frmdate & "' and '" & todate & "' " ''Securities Sales - Cost'
                StrSQl = StrSQl & " and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares " 'And C.CostBasis = B.CostBasis
                StrSQl = StrSQl & " and B.AssetClass='" & AssetClass & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName "
                StrSQl = StrSQl & " UNION ALL"
                StrSQl = StrSQl & " Select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-SUM(Distinct B.NetAmount) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10),B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I  ,1 as J from BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null)and B.AssetClass not in ('Cash','MMK') and N.Description = Case when B.BankID <> 7 then 'Securities Sales - Gross'  Else 'Securities Sales - Gross - Temp Rest' End Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & " and B.TransDate Between '" & frmdate & "' and '" & todate & "' " ''Securities sales - Gross'
                StrSQl = StrSQl & " and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares " 'And C.CostBasis = B.CostBasis
                StrSQl = StrSQl & " and B.AssetClass='" & AssetClass & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName "
                StrSQl = StrSQl & " UNION ALL "
                StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J "
                StrSQl = StrSQl & " Order by J,G"
            End If
        End If

        'Response.Write(StrSQl)
        'Exit Sub

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)

        If ds.Tables(0).Rows.Count > 0 Then
            lblErr.Text = ""
            ds.Tables(0).Columns.Remove("J")
            Dim sb As StringBuilder
            Dim i, j As Integer
            Dim Filename As String = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "").Trim & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            sb = New StringBuilder("")
            sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!ENDTRNS" & vbTab)

            For i = 0 To ds.Tables(0).Rows.Count - 1
                If i > 0 Then
                    ' fp.WriteLine()
                    sb.AppendLine()
                End If
                For j = 0 To ds.Tables(0).Columns.Count - 1
                    If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                        ' fp.Write(ds.Tables(0).Rows(i)(j))
                        sb.Append(ds.Tables(0).Rows(i)(j))
                        Exit For
                    End If
                    If j = ds.Tables(0).Columns.Count - 1 Then
                        'fp.Write(ds.Tables(0).Rows(i)(j))
                        sb.Append(ds.Tables(0).Rows(i)(j))
                    Else
                        'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                        sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                    End If
                Next
            Next
            sb.AppendLine()
            sb.AppendLine("ENDTRNS" & vbTab)
            ' fp.Close()
            'Download file
            downfile(Filename, sb)
        Else
            lblErr.Text = "Sorry No data found"
        End If
    End Sub
    Protected Sub GVTransfers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVTransfers.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVTransfers.Rows(index)

        Dim frmdate As Date = Convert.ToDateTime(row.Cells(6).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(6).Text + " 23:59")
        Dim BankID As Integer = row.Cells(2).Text
        Dim VoucherNo As String = row.Cells(5).Text
        Dim Amount As Double = (row.Cells(10).Text.ToString())

        Dim StrSQl As String

        If e.CommandName = "GenerateQB" Then

            StrSQl = " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
            StrSQl = StrSQl & " Ch.WebFolderName as F," & Decimal.Parse(Amount) & " as G,'' as H,'" & VoucherNo & "' as I,1 as J,B.BankID as K ,B.Quantity as  L,B.Ticker as M"
            StrSQl = StrSQl & " FROM BrokTrans B "
            StrSQl = StrSQl & " Inner join BrokTrans b1 On  B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and
            StrSQl = StrSQl & " Inner join NSFAccounts N On N.BankID=B.BankId " 'and B.RestType=N.RestrictionType 
            StrSQl = StrSQl & " and (N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end))"
            StrSQl = StrSQl & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            StrSQl = StrSQl & " Inner join Chapter Ch ON N.ChapterID = Ch.ChapterID "
            StrSQl = StrSQl & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut'  and B.Medium='IntTransfer' and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
            StrSQl = StrSQl & " UNION ALL "
            StrSQl = StrSQl & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
            StrSQl = StrSQl & " Ch.WebFolderName as F," & -Decimal.Parse(Amount) & " as G,'' as H,'" & VoucherNo & "' as I,0 as J,B.BankID as K ,-(B.Quantity) as L ,B.Ticker as M"
            StrSQl = StrSQl & " FROM BrokTrans B "
            StrSQl = StrSQl & " Inner join BrokTrans b1 On  B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and
            StrSQl = StrSQl & " Inner join NSFAccounts N On N.BankID=B1.BankId " 'and B1.RestType=N.RestrictionType"
            StrSQl = StrSQl & " and (N.RestrictionType in (Case when B1.BankID =4 and B.BankID <>5 then B.ToRestType Else Case when B.BankID = 4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'UnRestricted' End end end end end))"
            StrSQl = StrSQl & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            StrSQl = StrSQl & " Inner join Chapter Ch ON N.ChapterID = Ch.ChapterID "
            StrSQl = StrSQl & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut'  and B.Medium='IntTransfer' and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
            StrSQl = StrSQl & " UNION ALL "
            StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,0 as G,'' as H, '' as I, 2 as J,'' as K,'' as L,'' as M"
            StrSQl = StrSQl & " Order by J"

            'Response.Write(StrSQl)
            'Exit Sub

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)

            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("J")
                ds.Tables(0).Columns.Remove("K")
                ds.Tables(0).Columns.Remove("L")
                ds.Tables(0).Columns.Remove("M")


                Dim sb As StringBuilder
                Dim i, j As Integer
                Dim Filename As String = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        End If
    End Sub
    Protected Sub GVBnk_CCFees_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVBnk_CCFees.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVBnk_CCFees.Rows(index)

        Dim frmdate As Date = Convert.ToDateTime(row.Cells(5).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(5).Text + " 23:59")
        Dim BankID As Integer = row.Cells(2).Text
        Dim VoucherNo As String = row.Cells(4).Text
        Dim VendCust As String = row.Cells(9).Text
        Dim ExpCatCode As String = row.Cells(8).Text
        'Export
        'Dim gvtemp As New GridView
        Dim StrSQl As String

        If e.CommandName = "GenerateQB" Then
            StrSQl = "Select A,B,C,D,E,F,Sum(G),H,I,J from "
            StrSQl = StrSQl & "(Select A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101)  as D,"
            StrSQl = StrSQl & " Temp1.AccNo as E,Temp1.WebFolderName AS F,-ROUND(Sum(Temp1.Amount),2) as  G,'' as H, VoucherNo as I,J  from "
            StrSQl = StrSQl & "(SELECT  'SPL' as A,B.BankID,B.TransDate,SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 1 as J,Ch.WebFolderName "
            ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
            StrSQl = StrSQl & "FROM BankTrans  B "
            StrSQl = StrSQl & "INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode "
            StrSQl = StrSQl & "INNER JOIN NSFAccounts N ON E.Account = N.AccNo  "
            StrSQl = StrSQl & "INNER JOIN Chapter Ch ON N.ChapterID = Ch.ChapterID "
            StrSQl = StrSQl & "Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID = " & BankID & " and TransDate Between '" & frmdate & "' and '" & todate & "' "
            'AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "') "
            StrSQl = StrSQl & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
            'StrSQl = StrSQl & "Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,Ch.WebFolderName "
            StrSQl = StrSQl & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

            StrSQl = StrSQl & "UNION ALL "
            StrSQl = StrSQl & "SELECT  'SPL' as A,B.BankID,B.TransDate,SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 1 as J,Ch.WebFolderName "
            ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
            StrSQl = StrSQl & "FROM BankTrans B "
            StrSQl = StrSQl & "INNER JOIN ExpenseCategory  E ON B.TransCat= E.ExpCatCode "
            StrSQl = StrSQl & "INNER JOIN NSFAccounts N ON E.Account =N.AccNo "
            StrSQl = StrSQl & "INNER JOIN Chapter Ch ON N.ChapterID = Ch.ChapterID "
            StrSQl = StrSQl & "where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID =  " & BankID & " and TransDate Between '" & frmdate & "' and '" & todate & "'"
            ' " AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "') "
            StrSQl = StrSQl & "" & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
            'StrSQl = StrSQl & "Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate ,Ch.WebFolderName "
            StrSQl = StrSQl & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

            StrSQl = StrSQl & "UNION ALL "
            StrSQl = StrSQl & "SELECT  'TRNS' as A,B.BankID,B.TransDate,-SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 0 as J,Ch.WebFolderName "
            ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
            StrSQl = StrSQl & "FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' "
            StrSQl = StrSQl & "INNER JOIN Chapter Ch ON N.ChapterID = Ch.ChapterID "
            StrSQl = StrSQl & "Where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID =  " & BankID & " and TransDate Between '" & frmdate & "' and '" & todate & "'"
            ' AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "') "
            StrSQl = StrSQl & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
            'StrSQl = StrSQl & "Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,Ch.WebFolderName "
            StrSQl = StrSQl & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

            StrSQl = StrSQl & "UNION ALL "
            StrSQl = StrSQl & "SELECT  'TRNS' as A,B.BankID,B.TransDate,-SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 0 as J ,Ch.WebFolderName "
            ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
            StrSQl = StrSQl & "FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' "
            StrSQl = StrSQl & "INNER JOIN Chapter Ch ON N.ChapterID = Ch.ChapterID "
            StrSQl = StrSQl & "Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID =  " & BankID & " and TransDate Between '" & frmdate & "' and '" & todate & "'"
            'AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "') "
            StrSQl = StrSQl & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
            'StrSQl = StrSQl & "Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,Ch.WebFolderName "
            StrSQl = StrSQl & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

            StrSQl = StrSQl & ")Temp1 Group by Temp1.AccNo,Temp1.BankID ,Temp1.VoucherNo,A,J ,Temp1.WebFolderName "
            StrSQl = StrSQl & " UNION ALL "
            StrSQl = StrSQl & " Select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,0 as G,'' as H, '' as I, 2 as J"
            StrSQl = StrSQl & " )T1 Group by E,F,I,A,B,C,D,G,H,J Order By J"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("J")

                Dim sb As StringBuilder
                Dim i, j As Integer
                '  Dim Filename As String = Server.MapPath("ScoreSheets\") & "QB" & Now.Month & Now.Day & Now.Year & ".iif"
                Dim Filename As String = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                ' fp = File.CreateText(Filename)
                'fp.WriteLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!ENDTRNS" & vbTab)
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        Else
            ' Details
        End If
    End Sub

    Protected Sub gvExp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExp.PageIndexChanging
        Dim dsExp As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsExp = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("ManageVouchersSQl"))
        gvExp.DataSource = dsExp
        gvExp.PageIndex = e.NewPageIndex
        gvExp.DataBind()
    End Sub

    Protected Sub gvExp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvExp.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = gvExp.Rows(index)

        'Export
        'Dim gvtemp As New GridView

        'StrSQl = " SELECT EJ.ReimbMemberID,EJ.CheckNumber,EJ.DonorType,Null as DonationType,CONVERT(VARCHAR(10), EJ.DatePaid, 101) AS DepositDate, CASE WHEN EJ.TransType = 'Transfers' THEN BS.Accno ELSE  EC.Account END as AccNo, CASE WHEN EJ.TransType = 'Transfers' THEN BS.Description ELSE EC.AccountName END As Description, "
        'StrSQl = StrSQl & " Ch.webfoldername as Class,Ch.ChapterCode,Ch.ChapterID,SUM(EJ.ExpenseAmount) as Amount,ATT.Name as TransType,ISNULL(B.BankCode,'') as BankName "
        'StrSQl = StrSQl & " from ExpJournal EJ Left Join Bank B ON B.BankID=EJ.BankID Left Outer Join ExpenseCategory EC ON  EJ.ExpCatID=EC.ExpCatID  "
        'StrSQl = StrSQl & " Left Outer Join NSFAccounts BS ON  BS.BankID=EJ.BankID and BS.RestrictionType=EJ.RestTypeFrom and EJ.TransType = 'Transfers' Inner Join AcctgTransType ATT ON ATT.Code = EJ.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4,5) Inner Join Chapter Ch ON Ch.ChapterID = CASE WHEN  EJ.ToChapterID IS NULL THEN EJ.ChapterID ELSE EJ.ToChapterID END "
        'StrSQl = StrSQl & " WHERE DateDiff(d,EJ.DatePaid,'" & DDate & "')=0 AND EJ.CheckNumber='" & Depositslip & "'"
        'StrSQl = StrSQl & " Group by EJ.CheckNumber,EJ.DonorType,EJ.ReimbMemberID,EJ.DatePaid,EC.Account,EC.AccountName,Ch.webfoldername,Ch.ChapterCode,Ch.ChapterID,ATT.Name,B.BankCode,EJ.TransType,BS.Description,BS.Accno Order By Ch.webfoldername"

        Dim StrSQl As String
        If e.CommandName = "GenerateQB" Then
            StrSQl = "select A,B,C,D,E,F,Sum(G),H,I,J,K from (select CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), E.Datepaid, 101) as D ,N.AccNo as E,Ch.WebFolderName as F,-SUM(E.ExpenseAmount) as G,'' as H, 'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2) + REPLACE(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','') + Convert(varchar,E.CheckNumber) as I,CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as J,0 as K  From ExpJournal E Inner Join NSFAccounts N ON E.BankID=N.BankID and E.RestTypeFrom=N.RestrictionType and N.[Level]='L' and N.InvIncType IN ('MMK','CASH') Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid = E.ReimbMemberid Left Join IndSpouse I ON E.DonorType <> 'OWN' AND E.ReimbMemberid = I.AutoMemberID  Inner Join Chapter Ch ON N.ChapterID = Ch.ChapterID WHERE E.CheckNumber = '" & gvExp.DataKeys(index).Value & "' AND E.Datepaid='" & row.Cells(4).Text & "'" & " Group by E.Datepaid,E.CheckNumber,N.AccNo,E.BankID,I.FirstName,I.LastName,O.ORGANIZATION_NAME,E.DonorType,Ch.WebFolderName"
            StrSQl = StrSQl & " UNION ALL"
            StrSQl = StrSQl & " select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), EJ.DatePaid, 101)  as D,CASE WHEN EJ.TransType = 'Transfers' THEN BS.Accno ELSE  EC.Account END as E,CASE WHEN EJ.ChapterID=1 and EJ.EventID is null and EJ.TransType = 'GenAdmin' THEN 'US_HomeAdmin' ELSE  CASE WHEN EJ.ChapterID=1 and EJ.EventID = 1 and EJ.Transtype='FinalsExp' THEN  'US_Finals' ELSE Ch.webfoldername END END as F,EJ.ExpenseAmount as G,'' as H, 'VPS'+RIGHT('00'+ CONVERT(VARCHAR,EJ.BankID),2) + REPLACE(CONVERT(VARCHAR(10), EJ.DatePaid, 101),'/','') + Convert(varchar,EJ.CheckNumber) as I,CASE WHEN EJ.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as J ,1 as K "
            StrSQl = StrSQl & " FROM  ExpJournal EJ Left Join Bank B ON B.BankID=EJ.BankID Left Outer Join ExpenseCategory EC ON  EJ.ExpCatID=EC.ExpCatID  "
            StrSQl = StrSQl & " Left Outer Join NSFAccounts BS ON  BS.BankID=EJ.ToBankID and BS.RestrictionType=EJ.RestTypeFrom and EJ.TransType = 'Transfers' AND (BS.InvIncType IS NULL OR BS.InvIncType<>'Inv') Inner Join AcctgTransType ATT ON ATT.Code = EJ.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7) Inner Join Chapter Ch ON Ch.ChapterID = CASE WHEN  EJ.ToChapterID IS NULL THEN EJ.ChapterID ELSE EJ.ToChapterID END "
            StrSQl = StrSQl & " Left Join OrganizationInfo O ON EJ.DonorType = 'OWN' and O.AutoMemberid =EJ.ReimbMemberid Left Join IndSpouse I ON EJ.DonorType <> 'OWN' AND EJ.ReimbMemberid = I.AutoMemberID "
            StrSQl = StrSQl & " WHERE  EJ.CheckNumber = '" & gvExp.DataKeys(index).Value & "' AND EJ.DatePaid='" & row.Cells(4).Text & "'"
            StrSQl = StrSQl & " UNION ALL"
            StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I,'' as J,2 as K"
            StrSQl = StrSQl & " ) TMP Group by A,B,C,D,E,F,H,I,J,K Order By K,E"
          
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("K")

                Dim sb As StringBuilder
                Dim i, j As Integer
                '  Dim Filename As String = Server.MapPath("ScoreSheets\") & "QB" & Now.Month & Now.Day & Now.Year & ".iif"
                Dim Filename As String = ds.Tables(0).Rows(0)("I").ToString() & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                ' fp = File.CreateText(Filename)
                'fp.WriteLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!ENDTRNS" & vbTab)
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        Else
            ' Details
        End If
    End Sub

    Protected Sub GVCCRevenues_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVCCRevenues.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVCCRevenues.Rows(index)
        'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
        ' having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0

        'Export
        'Dim gvtemp As New GridView
        Dim frmdate As Date = Convert.ToDateTime(row.Cells(3).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(4).Text + " 23:59")

        Dim StrSQl As String
        ',Convert(DateTime,'" & todate & "'),
        If e.CommandName = "GenerateQB" Then
            StrSQl = "select A,B,C,D,E,F,Sum(G),H,I,J from (select 'TRNS' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D ,N.AccNo as E,Ch.WebFolderName as F,Sum(NFG.TotalPayment) as G,'' as H, 'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as I,0 as J  From NFG_Transactions NFG Inner Join NSFAccounts N ON N.BankID=1 and N.RestrictionType='Unrestricted' Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID where NFG.MS_TransDate Between '" & row.Cells(3).Text & "' AND '" & row.Cells(4).Text & " 23:59' Group By N.AccNo,Ch.WebFolderName having SUM(NFG.[TotalPayment]) > 0 or SUM(NFG.[TotalPayment]) < 0"
            StrSQl = StrSQl & " UNION ALL "
            StrSQl = StrSQl & " select  'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D, T.AccNo as E,T.WebFolderName as F,-ROUND(Sum(T.Amount),2) as  G,'' as H, 'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as I, 1 as J  from ("
            ' ''Contestant
            StrSQl = StrSQl & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

            '' ''DuplicateContestantReg
            StrSQl = StrSQl & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

            'Registration
            StrSQl = StrSQl & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

            'Finals and NFG.MatchedStatus='O'  NFG.MatchedStatus='O' and 
            StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=1 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
            StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,6 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=1 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

            '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=13 and Amount >0 are summed. ChapterID should be 112 for Coaching
            'Coaching - similar to Finals  and NFG.MatchedStatus='O' NFG.MatchedStatus='O' and 
            StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Coaching' as WebFolderName, 'US' as State,'Coaching' as Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=13 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
            StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Coaching' as WebFolderName, 'US' as State,'Coaching' as Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=13 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

            '***************Added on 07/22/2014 as per new req. If EventID=19 [PrepClub], NFG_Transactions table is used and the records with EventID=19 and Amount >0 are summed by ChapterID.
            'Prepclub
            StrSQl = StrSQl & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM Registration_PrepClub R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=19 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM Registration_PrepClub R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=19 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

            '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=4 and Amount >0 are summed. ChapterID should be 117 for Game
            'Game - similar to Finals and NFG.MatchedStatus='O' NFG.MatchedStatus='O' and 
            StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=4 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
            StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=4 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

            '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=3 and Amount >0 are summed. ChapterID should be ?? for Workshop
            'Workshop - similar to Finals 
            ''To be Done for Workshop
            'StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
            'StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=20 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
            'StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
            'StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.MatchedStatus='O' and NFG.EventId=20 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
            '*********************************************'
            '*********************************************'
            '*********************************************'

            ''NFG_Supp

            ''NFG_Supp--Contestant
            StrSQl = StrSQl & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            'NFG_Supp----Registration
            StrSQl = StrSQl & " SELECT (SUM(IsNull(R.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(IsNull(R.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null  and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

            '*************************************************************************************************************************************************************************************************************'
            'NFG_Supp----PrepClub_Registration Added on 08-19-2014 for PrepCLub Refund Records
            StrSQl = StrSQl & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=19 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
            StrSQl = StrSQl & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=19 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

            '/**************************Replacing  51100 with 51450 on 07-10-2013*****************************************************************/
            StrSQl = StrSQl & " SELECT SUM(NFG.MealsAmount) as Amount,51450 as AccNo,7 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"  ' C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG where (NFG.MealsAmount>0 or NFG.MealsAmount<0) and NFG.MealsAmount is not Null and NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.MealsAmount)>0  or SUM(NFG.MealsAmount)<0) Union All"  '  Inner Join Chapter C On NFG.ChapterID = C.ChapterID  Group by C.WebFolderName, C.State,C.Name

            'Donations
            StrSQl = StrSQl & " SELECT SUM(NFG.[Contribution Amount])as Amount, 41101 as AccNo,8 as OrderNo,  ISNULL(C.WebFolderName,'US_HomeOffice') as WebFolderName, ISNULL(C.State,'IL') as State,ISNULL(C.Name,'Home Office') as Name "
            StrSQl = StrSQl & " FROM NFG_Transactions NFG Left Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[Contribution Amount] > 0 or NFG.[Contribution Amount] < 0) and NFG.EventID not in (10) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name Union All"
            StrSQl = StrSQl & " SELECT SUM(NFG.[TotalPayment])as Amount, 51400 as AccNo,9 as OrderNo,  C.WebFolderName, C.State,C.Name"
            StrSQl = StrSQl & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[TotalPayment] > 0 or NFG.[TotalPayment] < 0) and NFG.EventID=10 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name) T"
            StrSQl = StrSQl & " inner Join  NSFAccounts N ON T.AccNo = N.AccNo "
            StrSQl = StrSQl & " Group by T.AccNo,N.Description,T.WebFolderName,T.State,T.Name"

            ''Added To merge CC Revenues and CC revenue Adjustments 05-11-2012
            ''CC Revenue Adjustments are to make the NFG_transactions equal to BankTrans numbers for the same period
            StrSQl = StrSQl & " UNION ALL "
            'StrSQl = StrSQl & " Select A,B,C,D,E,F,Sum(G),H,I,J from ("
            StrSQl = StrSQl & "select A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D, Temp.AccNo as E, F,-ROUND(Sum(Temp.Amount),2) as  G,'' as H, VoucherNo as I, J  from "
            StrSQl = StrSQl & "(Select  'SPL' as A,'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*2/3 as Amount,41105 as AccNo,'US_HomeOffice' as F,1 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
            StrSQl = StrSQl & "(select SUM(Amount) as CreditCC,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQl = StrSQl & "select 0 as CreditCC, SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
            StrSQl = StrSQl & ")T1 Group by TransDate  UNION ALL "
            '***********Added on 25-02-2014 to show transactions are not in Contests['Program Service Fees - Contests'] for sep,oct,nov ***********'
            StrSQl = StrSQl & "Select 'SPL' as A,'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51150 as AccNo,'US_HomeOffice' as F,1 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
            StrSQl = StrSQl & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQl = StrSQl & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
            StrSQl = StrSQl & ") T21 where Month(T21.TransDate) in (9,10,11) Group by TransDate UNION ALL "
            '****************Updated on on 25-02-2014 **********************************************************************************************'
            StrSQl = StrSQl & "Select 'SPL' as A,'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51100 as AccNo,'US_HomeOffice' as F,1 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
            StrSQl = StrSQl & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQl = StrSQl & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
            StrSQl = StrSQl & ") T22 where Month(T22.TransDate) not in (9,10,11) Group by TransDate UNION ALL "
            '****************************************************************************************************************************************'
            StrSQl = StrSQl & "Select  'TRNS' as A,'CGN' as TransType,-(SUM(CreditCC)-SUM(Registrations)) as Amount,10101 as AccNo,'US_HomeOffice' as F,0 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from "
            StrSQl = StrSQl & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
            StrSQl = StrSQl & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
            StrSQl = StrSQl & ") T3 Group by TransDate "
            StrSQl = StrSQl & " )Temp Group by Temp.AccNo,F,VoucherNo,A,J "

            StrSQl = StrSQl & " UNION ALL"
            StrSQl = StrSQl & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
            StrSQl = StrSQl & " ) TMP Group by E,F,I,C,D,H,J,A,B Order By J,E"

            'Response.Write(StrSQl)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                Dim Filename As String
                ds.Tables(0).Columns.Remove("J")
                Filename = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "") & ".iif" '"VPS" & ds.Tables(0).Rows(0)("D").ToString().Replace("/", "") & gvExp.DataKeys(index).Value.ToString() & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                Dim sb As StringBuilder
                Dim i, j As Integer
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        Else
            ' Details
        End If
    End Sub
    Protected Sub GVACT_EFTDon_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVACT_EFTDon.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVACT_EFTDon.Rows(index)
        Dim frmdate As Date = Convert.ToDateTime(row.Cells(7).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(8).Text)
        Dim BankID As Integer = row.Cells(3).Text
        Dim BankTransID As Integer = row.Cells(15).Text

        Dim StrSQl As String
        ',Convert(DateTime,'" & todate & "'),
        If e.CommandName = "ViewTrans" Then
            lblErr.Text = "This module is under Development"
        ElseIf e.CommandName = "GenerateQB" Then
            StrSQl = " Select A,B,C,D,E,F,Sum(G),H,I,J from "
            StrSQl = StrSQl & " (Select A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & row.Cells(5).Text & " 11:59:00 PM'), 101)  as D,"
            StrSQl = StrSQl & "  Temp1.AccNo as E,Temp1.WebFolderName AS F,-ROUND(Sum(Temp1.Amount),2) as  G,'' as H, VoucherNo as I, 1 as J  from "
            StrSQl = StrSQl & " ((SELECT  'SPL' as A,B.BankID,B.BankTransID,N.AccNo, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,B.VendCust, N.Description ,N.DonorType,Ch.WebFolderName FROM BankTrans B "
            StrSQl = StrSQl & " LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat=C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID = 1 "
            StrSQl = StrSQl & " LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat=H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID = 2 "
            StrSQl = StrSQl & " Inner Join  NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
            StrSQl = StrSQl & " Inner Join Chapter Ch ON N.chapterID = Ch.ChapterID "
            StrSQl = StrSQl & " Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between '" & frmdate & "' and '" & todate & "' and B.BankID =" & BankID & " and Convert(Date,TransDate)  = CONVERT (Date,'" & row.Cells(5).Text & "') and B.BankTransID in (" & BankTransID & ")"
            StrSQl = StrSQl & " Group By N.AccNo ,B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate ,B.Vendcust,B.BankTransID,Ch.WebFolderName"
            StrSQl = StrSQl & " ) UNION ALL "
            StrSQl = StrSQl & " (SELECT 'TRNS' as A,B.BankId,B.BankTransID,N.AccNo,-SUM(B.Amount) as Amount,B.Transcat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,B.VendCust, N.Description ,N.DonorType,Ch.WebFolderName FROM BankTrans B  "
            StrSQl = StrSQl & " INNER JOIN NSFAccounts N ON   B.BankID = N.BankID"
            StrSQl = StrSQl & " Inner Join Chapter Ch ON N.chapterID = Ch.ChapterID "
            StrSQl = StrSQl & " Where N.RestrictionType = 'Unrestricted' and B.TransCat='Donation' and B.TransDate Between '" & frmdate & "' AND '" & todate & "'"
            StrSQl = StrSQl & "  and B.BankID =" & BankID & " and Convert(Date,TransDate)  = CONVERT (Date,'" & row.Cells(5).Text & "') and B.BankTransID in (" & BankTransID & ") Group by B.BankId,N.AccNo,B.Transcat,B.VendCust, N.Description ,N.DonorType,B.TransDate,B.BankTransID,Ch.WebFolderName"
            StrSQl = StrSQl & " ))Temp1 Group by Temp1.AccNo,Temp1.BankID ,Temp1.VoucherNo,Temp1.BankTransID,Temp1.A,Temp1.WebFolderName"
            StrSQl = StrSQl & " )T1 Group by E,F,I,A,B,C,D,G,H,J"
            StrSQl = StrSQl & "  UNION ALL "
            StrSQl = StrSQl & " Select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,0 as G,'' as H, '' as I, 2 as J"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                Dim Filename As String
                ds.Tables(0).Columns.Remove("J")
                Filename = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "") & ".iif" '"VPS" & ds.Tables(0).Rows(0)("D").ToString().Replace("/", "") & gvExp.DataKeys(index).Value.ToString() & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                Dim sb As StringBuilder
                Dim i, j As Integer
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        Else
            ' Details
        End If
    End Sub

    Protected Sub GVTDAIncome_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVTDAIncome.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVTDAIncome.Rows(index)
        'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
        'having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0

        'Export
        'Dim gvtemp As New GridView
        Dim frmdate As Date = Convert.ToDateTime(row.Cells(3).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(4).Text + " 23:59")
        Dim BankID As String = row.Cells(6).Text
        Dim StrSQl As String
        ',Convert(DateTime,'" & todate & "'),
        If e.CommandName = "GenerateQB" Then
            StrSQl = " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo As E,Ch.WebFolderName as  F, Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End   as G,Case WHEN B.BankID = 4 Then 'ISCH' ELSE Case WHEN B.BankID = 5 Then 'IGEN' ELSE Case WHEN B.BankID = 6 Then 'IEND' ELSE 'IDAF' End End End + Replace(CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),'/','') +'_'+ Replace(CONVERT(VARCHAR(25),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),101),'/','') As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,0 as Ord,B.BankID FROM BrokTrans B"
            'StrSQl = StrSQl & " Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted') OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7))) OR (Acc.AccNo = 15101 AND  B.AssetClass <> 'MMK') "
            StrSQl = StrSQl & " Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='MMK') OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK')"
            StrSQl = StrSQl & " Inner Join Chapter Ch ON Acc.ChapterID = Ch.ChapterID"
            StrSQl = StrSQl & " WHERE Acc.IntDiv='Y' AND ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & BankID & ") Group By  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo,Ch.WebFolderName, B.NetAmount , B.Quantity union All"
            StrSQl = StrSQl & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo as E,Ch.WebFolderName as  F,-Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End AS G,B.Ticker + '' + CONVERT(VARCHAR(25),b.TransDate,101)  As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,1 as Ord,B.BankID  FROM BrokTrans B"
            StrSQl = StrSQl & " Inner Join NSFAccounts Acc On ((Acc.InvIncType ='Dividend' AND B.TransCat = 'OrdDiv') OR (Acc.InvIncType ='DivCG' AND B.TransCat IN ('STCGDiv','LTCGDiv')) OR (Acc.InvIncType ='Interest' AND B.TransCat = 'Div')) AND ((Acc.RestrictionType ='Unrestricted' AND B.BankID IN (4,5,6)) OR (Acc.RestrictionType ='Temp Restricted' AND B.BankID =7))"
            'StrSQl = StrSQl & " Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType is NULL) OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK')"
            StrSQl = StrSQl & " Inner Join Chapter Ch ON Acc.ChapterID = Ch.ChapterID"
            StrSQl = StrSQl & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & BankID & ") " ' Order By B.BankID,YEAR(TransDate),MONTH(B.TransDate), 13,Acc.AccNo"
            'StrSQl = StrSQl & " Group By  B.TransDate,B.Ticker,B.BankID,Acc.AccNo,Ch.WebfolderName"
            '' Added to include BankIds(1,2,3)
            StrSQl = StrSQl & " UNION ALL "
            StrSQl = StrSQl & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo As E,Ch.WebFolderName as F,SUM(B.Amount) as G,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End + Replace(CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),'/','') +'_'+ Replace(CONVERT(VARCHAR(25),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),101),'/','') As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,0 as Ord,B.BankID FROM BankTrans B Inner Join NSFAccounts Acc On Acc.BankID=B.BankID AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Cash' AND Acc.BankID in (1,2,3) Inner Join Chapter Ch ON Acc.ChapterID = Ch.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit' AND B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.BankID in (" & BankID & ") And Acc.IntDiv='Y' Group By CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo,Ch.WebFolderName "
            StrSQl = StrSQl & " UNION ALL"
            StrSQl = StrSQl & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo As E,Ch.WebFolderName as F,-SUM(B.Amount) as G,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End + Replace(CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),'/','') +'_'+ Replace(CONVERT(VARCHAR(25),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),101),'/','') As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,0 as Ord,B.BankID FROM BankTrans B Inner Join NSFAccounts Acc On (Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Interest') AND B.BankID in (1,2,3) Inner Join Chapter Ch ON Acc.ChapterID = Ch.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit'  AND B.TransDate Between '" & frmdate & "' AND '" & todate & "'  and B.BankID in (" & BankID & ") Group By CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo,Ch.WebFolderName Order by D,B.BankId,Acc.Accno "

            'Response.Write(StrSQl)
            'Exit Sub
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim cnt As Integer = 0
                Dim CurrBanKID As Integer = 0
                Dim CurrMon As Integer = 0
                Dim row1 As DataRow
                Dim destinationrowcount As Integer = ds.Tables(0).Rows.Count - 1
                Do While cnt <= destinationrowcount
                    If CurrBanKID = 0 Then
                        CurrBanKID = ds.Tables(0).Rows(cnt)("BankID")
                        CurrMon = ds.Tables(0).Rows(cnt)("Mnth")
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    ElseIf CurrBanKID <> ds.Tables(0).Rows(cnt)("BankID") Or CurrMon <> ds.Tables(0).Rows(cnt)("Mnth") Then
                        row1 = ds.Tables(0).NewRow
                        CurrBanKID = ds.Tables(0).Rows(cnt)("BankID")
                        CurrMon = ds.Tables(0).Rows(cnt)("Mnth")
                        row1("A") = "ENDTRNS"
                        ds.Tables(0).Rows.InsertAt(row1, cnt)
                        destinationrowcount = destinationrowcount + 1
                        cnt = cnt + 1
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    End If
                    cnt = cnt + 1
                Loop
                row1 = ds.Tables(0).NewRow
                row1("A") = "ENDTRNS"
                ds.Tables(0).Rows.InsertAt(row1, ds.Tables(0).Rows.Count)
            End If
            ' ''Ferdine ** testing ##
            'Exit Sub
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                Dim Filename As String
                ds.Tables(0).Columns.Remove("Ord")
                ds.Tables(0).Columns.Remove("Mnth")
                ds.Tables(0).Columns.Remove("BankID")
                ds.Tables(0).Columns.Remove("Yr")
                Filename = row.Cells(2).Text & ".iif"
                Dim sb As StringBuilder
                Dim i, j As Integer
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                ' fp.Close()
                'Download file
                downfile(Filename, sb)
            Else
                lblErr.Text = "Sorry No data found"
            End If
        Else
            ' Details
        End If
    End Sub


    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim TempGrid As New GridView
        Dim Filename As String = GetData(TempGrid)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & Filename & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        TempGrid.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Function HasRecords(frmDate As String, toDate As String) As Boolean
        If ddlCategory.SelectedValue = "1" Then
            strSql = " Select Distinct Top 100 sum(D.Amount) as Amount, D.DepositSlip, D.DepositDate,'DGN'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlip) as VoucherNo"
            strSql = strSql & " , CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile,CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0) AND (COUNT(BrS.DepCheckNo)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted ,"
            strSql = strSql & " CASE WHEN (COUNT(BS.DepSlipNumber)>0 OR COUNT(BrS.DepCheckNo) > 0) then CASE WHEN D.BankID in (4,5,6,7) THEN Convert(Varchar,BrS.DepCheckNo) ELSE Convert(Varchar,BS.DepSlipNumber) END ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END END "
            strSql = strSql & " as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null AND BrS.DepCheckNo is NULL) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'False' ELSE 'True' END as ReconcileStatus,CASE WHEN D.BankID in (4,5,6,7) THEN BrS.DepCheckNo  ElSE BS.DepSlipNumber END as DepSlipNumber "
            strSql = strSql & ",D.BankID from DonationsInfo  D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON  B.BankID=D.BankID AND B.TransCat='Deposit'  AND D.DepositDate = B.TransDate "
            strSql = strSql & " Left Outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND  BS.BankID=D.BankID AND BS.TransCat='Deposit' AND D.DepositSlip = BS.DepSlipNumber"
            strSql = strSql & " Left Outer Join BrokTrans BrS ON D.DepositDate=BrS.TransDate AND BrS.BankID=D.BankID AND BrS.TransCat='Donation' AND D.DepositSlip = BrS.DepCheckNo AND D.BankID in (4,5,6,7)"
            strSql = strSql & " where D.METHOD  IN ('Check','CASH') AND D.DepositDate  Between '" & frmDate & "' AND '" & toDate & " 23:59' Group BY D.DepositSlip, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount,BrS.NetAmount,Brs.DepCheckNo order by  D.DepositDate,D.DepositSlip"
        ElseIf ddlCategory.SelectedValue = "2" Then
            strSql = "Select  Top 100 sum(D.Amount) as Amount, RTRIM(D.DepositSlipNo) as DepositSlip, D.DepositDate,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlipNo) as VoucherNo ,"
            strSql = strSql & " CASE WHEN sum(D.Amount) = BS.Amount THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile, CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted , "
            strSql = strSql & " CASE WHEN COUNT(BS.DepSlipNumber) > 0 then Convert(Varchar,BS.DepSlipNumber) ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END "
            strSql = strSql & " END as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = BS.Amount THEN 'False' ELSE 'True' END as ReconcileStatus,BS.DepSlipNumber,D.BankID from OtherDeposits D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON B.BankID=D.BankID AND B.TransCat='Deposit' AND D.DepositDate = B.TransDate"
            strSql = strSql & "  Left outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND BS.BankID=D.BankID AND BS.TransCat IN ('Deposit','DepositError') AND D.DepositSlipNo = BS.DepSlipNumber where  D.DepositDate Between '" & frmDate & "' AND '" & toDate & "  23:59' Group BY D.DepositSlipNo, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount order by D.DepositDate,D.DepositSlipNo "
        ElseIf ddlCategory.SelectedValue = "3" Then
            strSql = "SELECT SUM(Amount) as Amount,'CGN01'+Replace(CONVERT(VARCHAR(10), StartDate, 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), EndDate, 101),'/','') as VoucherNo ,StartDate,EndDate from("
            strSql = strSql & " Select Sum(NFG.TotalPayment) as Amount "  ' ,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
            strSql = strSql & " ,'" & frmDate & "' as StartDate,'" & toDate & "' as EndDate "
            strSql = strSql & " from NFG_Transactions NFG where NFG.MS_TransDate Between '" & frmDate & "' AND '" & toDate & " 23:59 ' Group by NFG.MS_TransDate"
            strSql = strSql & " UNION ALL "
            strSql = strSql & " Select (SUM(CreditCC)- SUM(Registrations)) as Amount " ','CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate from "
            strSql = strSql & ",'" & frmDate & "' as StartDate,'" & toDate & "' as EndDate "
            strSql = strSql & " From (select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmDate & "' AND '" & toDate & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) "
            strSql = strSql & " Union All "
            strSql = strSql & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmDate & "' AND '" & toDate & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
            strSql = strSql & ")T3 Group by TransDate "
            strSql = strSql & ")T4 group by StartDate,EndDate Order By StartDate"  'VoucherNo,
        ElseIf ddlCategory.SelectedValue = "4" Then
            '4 - TDA Accounts -
            Dim i As Integer
            Dim BankIDs As String = "0"
            If lstBank.Items(i).Selected = True Then
                BankIDs = "1,2,3,4,5,6,7"
            Else
                For i = 0 To lstBank.Items.Count - 1
                    If lstBank.Items(i).Selected Then
                        BankIDs = BankIDs & "," & lstBank.Items(i).Value
                    End If
                Next
            End If
            strSql = " Select SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & frmDate & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & toDate & "', 101),'/','') as VoucherNo,B.BankID,B.BankCode,'" & frmDate & "' as StartDate , '" & toDate & "' as EndDate "
            strSql = strSql & " FROM   BrokTrans B Inner Join Bank BB On B.BankID=BB.BankID WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmDate & "' AND '" & toDate & " 23:59 '"
            strSql = strSql & " AND B.BankID in (" & BankIDs & ") Group By   B.BankID,B.BankCode " ' Order By B.BankID CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) " 
            strSql = strSql & " UNION ALL "
            strSql = strSql & " Select SUM(B.Amount) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & frmDate & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & toDate & "', 101),'/','') as VoucherNo,B.BankID,BB.BankCode ,'" & frmDate & "' as StartDate , '" & toDate & "' as EndDate "
            strSql = strSql & " FROM  BankTrans B Inner Join Bank BB On B.BankID=BB.BankID "
            strSql = strSql & " WHERE B.TransType='Credit' and B.TransCat='Interest' AND  B.TransDate Between '" & frmDate & "' AND '" & toDate & " 23:59 '"
            strSql = strSql & " AND B.BankID in (" & BankIDs & ") Group By   B.BankID,BB.BankCode Order by B.BankID"

        ElseIf ddlCategory.SelectedValue = "5" Then
            '5 - Vouchers - Expense checks
            strSql = " Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber," 'Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End as DateCashed"
            strSql = strSql & " Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End "
            strSql = strSql & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else 'Not Available' End"
            strSql = strSql & " Else 'Not Available' End End as DateCashed"
            strSql = strSql & ", E.DatePaid,'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,E.BankID " 'Case When Ba.Transdate is not null Then Ba.Transdate Else '' End as DateCashed,
            strSql = strSql & " From ExpJournal E Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7)"
            strSql = strSql & " LEFT Join BankTrans Ba on CONVERT(Varchar,Ba.CheckNumber)=E.CheckNumber and Ba.BankID=E.BanKID" ' AND Ba.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
            strSql = strSql & " LEFT Join BrokTrans Br on (CONVERT(Varchar,Br.ExpCheckNo)=E.CheckNumber OR CONVERT(Varchar,Br.DepCheckNo)=E.CheckNumber) and Br.BankID=E.BanKID " 'AND Br.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
            strSql = strSql & " WHERE E.CheckNumber is Not null AND (ba.BankTransID IS NOT NULL OR Br.BrokTransID IS NOT Null Or E.TransactionID is not null) AND  (E.Account is Not null or E.TransType = 'Transfers')" ' AND E.DatePaid  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Modified on 29-04-2013
            strSql = strSql & " And  Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else E.DatePaid End "
            strSql = strSql & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else E.DatePaid End"
            strSql = strSql & " Else E.DatePaid End End "
            strSql = strSql & " Between '" & frmDate & "' AND '" & toDate & " 23:59'" 'Modified on 29-04-2013
            strSql = strSql & " Group BY E.DatePaid, E.CheckNumber,E.BankID,Ba.Transdate, Br.TransDate ORDER BY E.CheckNumber,E.DatePaid"
        ElseIf ddlCategory.SelectedValue = "6" Then
            'ACT_EFT Donations
            strSql = "SELECT  B.BankID,B.BankTransID, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,"
            strSql = strSql & "B.VendCust, N.Description ,N.DonorType,'" & frmDate & "' as StartDate , '" & toDate & "' as EndDate,B.MemberID FROM BankTrans B "
            strSql = strSql & "LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat = C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID=1"
            strSql = strSql & "LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat = H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID=2"
            strSql = strSql & "Inner Join NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
            strSql = strSql & "Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between '" & frmDate & "' and '" & toDate & "'"
            strSql = strSql & "Group By B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate,B.Vendcust,B.BankTransID,B.MemberID Order by B.TransDate"
        ElseIf ddlCategory.SelectedValue = "7" Then
            'Vouchers Bank Service Charges
            strSql = "SELECT B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.VendCust,'') as VoucherNo," ''_'+RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
            strSql = strSql & " E.AccountName,B.TransDate,'VPE' as TransType,E.ExpCatCode,  IsNull(B.VendCust,'') as VendCust,B.Reason,B.AddInfo"
            strSql = strSql & " FROM BankTrans  B "
            strSql = strSql & " INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode "
            strSql = strSql & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo "
            strSql = strSql & " Where TransType= 'DEBIT' And TransCat = 'CreditCard' and TransDate Between '" & frmDate & "' and '" & toDate & "' "
            strSql = strSql & " Group By B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
            strSql = strSql & " UNION ALL "
            strSql = strSql & " SELECT B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ + IsNull(B.VendCust,'') as VoucherNo," '_'+'RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
            strSql = strSql & " E.AccountName,B.TransDate,'VPE' as TransType,E.ExpCatCode,IsNull(B.VendCust,'') as  VendCust,B.Reason,B.AddInfo"
            strSql = strSql & " FROM BankTrans B "
            strSql = strSql & " INNER JOIN ExpenseCategory  E ON B.TransCat= E.ExpCatCode "
            strSql = strSql & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo "
            strSql = strSql & " where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and TransDate Between '" & frmDate & "' and '" & toDate & "' "
            strSql = strSql & " Group By B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
        ElseIf ddlCategory.SelectedValue = "8" Then
            ''TransferOut
            strSql = "SELECT B.BankID,B.BrokTransID,B.BankCode,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+"
            strSql = strSql & " Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as VoucherNo,B.TransDate,B.TransCat,'" & frmDate & "' as StartDate,'" & toDate & "' as EndDate,'IGT' as TransType,B.AssetClass,B.Ticker,B.Quantity,B.Price,"
            strSql = strSql & " B.NetAmount as Amount,B.RestType,B1.RestType as RestTypeTo,B1.BankID as ToBankID,N.Description FROM BrokTrans B "
            strSql = strSql & " Inner join BrokTrans B1 On B.TransType = B1.TransType and B.TransDate= B1.TransDAte and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and 
            strSql = strSql & " Inner join NSFAccounts N On N.BankID = B.BankId " ' and B.RestType=N.RestrictionType "
            strSql = strSql & " and (B.RestType=N.RestrictionType Or N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end  ))"
            strSql = strSql & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            strSql = strSql & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Transdate Between '" & frmDate & "' and '" & toDate & "' Order by B.TransDate"
        ElseIf ddlCategory.SelectedValue = "9" Then
            ''Buy Transactions
            strSql = "SELECT B.BankID,B.BankCode,N.AccNo,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount"
            strSql = strSql & " ,N.Description, 'IGB' as TransType,B.AssetClass,"
            strSql = strSql & " 'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            strSql = strSql & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,N.Description,'" & frmDate & "' as StartDate,'" & toDate & "' as EndDate,B.Disposed_Basis,B.Capital_Gains FROM BrokTrans B "
            strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
            strSql = strSql & " Where B.Transcat='Buy' and B.TransDate Between '" & frmDate & "' and '" & toDate & "' "
            strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass,B.Disposed_Basis,B.Capital_Gains Order by B.TransDate"
        ElseIf ddlCategory.SelectedValue = "10" Then
            ''Sell Transactions
            strSql = "SELECT Distinct B.BankID,B.BankCode,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount,N.AccNo" '
            strSql = strSql & " , 'IGS' as TransType,B.AssetClass,N.Description,B.Ticker," '
            strSql = strSql & " 'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            strSql = strSql & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,'" & frmDate & "' as StartDate,'" & toDate & "' as EndDate,B.Disposed_Basis,B.Capital_Gains FROM BrokTrans B "
            strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
            strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmDate & "' and '" & toDate & "' "
            strSql = strSql & " and N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end  )"
            strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass,B.Ticker,B.Disposed_Basis,B.Capital_Gains Order by B.TransDate" '
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        End If
        Return False
    End Function
    Function HasDuplicatesGL(frmDate As String, toDate As String) As Boolean
        Dim iCnt As Integer = 0
        Dim cmdText As String = ""
        If ddlCategory.SelectedValue = "1" Then 'Deposits
            cmdText = "SELECT count(*) from GeneralLedgerTemp Where TransType='DGN' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "2" Then 'Other Deposits (OTH)
            cmdText = "SELECT count(*) from GeneralLedgerTemp Where TransType='OTH' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "3" Then
            'CREDIT CARDS 'CGN
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='CGN' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "4" Then
            'IGN-Cash Receipts 
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='IGN' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "5" Then
            'Expense Vouchers
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='VPS' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "6" Then
            ''ACT_EFT Donations
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='DGE' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "7" Then
            'Bank Service Charges
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='VPE' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "8" Then
            ''TransferOut
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='IGT' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "9" Then
            ''Buy Transactions
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='IGB' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        ElseIf ddlCategory.SelectedValue = "10" Then
            ''SELL Transactions
            cmdText = "SELECT count(*) From GeneralLedgerTemp Where TransType='IGS' and Date between '" & frmDate & "' and  '" & toDate & " 23:00'"
        End If
        iCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
        If iCnt > 0 Then
            Return True
        End If

        Return False
    End Function

    Private Sub DisplayRecords()
        lblErr.Text = ""
        Dim StrSQL As String
        Dim BeginDate As DateTime = Convert.ToDateTime(TxtFrom.Text)
        Dim EndDate As DateTime = Convert.ToDateTime(txtTo.Text)
        Dim startWith As String, endWith As String
        Dim iDays As Integer
        Dim MissingDateMsg As String = ""
        StrSQL = ""
        If ddlCategory.SelectedValue = "1" Then
            StrSQL = " Select Distinct Top 100 sum(D.Amount) as Amount, D.DepositSlip, D.DepositDate,'DGN'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlip) as VoucherNo"
            StrSQL = StrSQL & " , CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile,CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0) AND (COUNT(BrS.DepCheckNo)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted ,"
            StrSQL = StrSQL & " CASE WHEN (COUNT(BS.DepSlipNumber)>0 OR COUNT(BrS.DepCheckNo) > 0) then CASE WHEN D.BankID in (4,5,6,7) THEN Convert(Varchar,BrS.DepCheckNo) ELSE Convert(Varchar,BS.DepSlipNumber) END ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END END "
            StrSQL = StrSQL & " as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null AND BrS.DepCheckNo is NULL) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'False' ELSE 'True' END as ReconcileStatus,CASE WHEN D.BankID in (4,5,6,7) THEN BrS.DepCheckNo  ElSE BS.DepSlipNumber END as DepSlipNumber "
            StrSQL = StrSQL & ",D.BankID from DonationsInfo  D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON  B.BankID=D.BankID AND B.TransCat='Deposit'  AND D.DepositDate = B.TransDate "
            StrSQL = StrSQL & " Left Outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND  BS.BankID=D.BankID AND BS.TransCat='Deposit' AND D.DepositSlip = BS.DepSlipNumber"
            StrSQL = StrSQL & " Left Outer Join BrokTrans BrS ON D.DepositDate=BrS.TransDate AND BrS.BankID=D.BankID AND BrS.TransCat='Donation' AND D.DepositSlip = BrS.DepCheckNo AND D.BankID in (4,5,6,7)"
            StrSQL = StrSQL & " where D.METHOD  IN ('Check','CASH') AND D.DepositDate  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' Group BY D.DepositSlip, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount,BrS.NetAmount,Brs.DepCheckNo order by  D.DepositDate,D.DepositSlip"

            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 1
            GetDonationReceipt(StrSQL)
        ElseIf ddlCategory.SelectedValue = "2" Then
            StrSQL = "Select  Top 100 sum(D.Amount) as Amount, RTRIM(D.DepositSlipNo) as DepositSlip, D.DepositDate,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlipNo) as VoucherNo ,"
            StrSQL = StrSQL & " CASE WHEN sum(D.Amount) = BS.Amount THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile, CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted , "
            StrSQL = StrSQL & " CASE WHEN COUNT(BS.DepSlipNumber) > 0 then Convert(Varchar,BS.DepSlipNumber) ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END "
            StrSQL = StrSQL & " END as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = BS.Amount THEN 'False' ELSE 'True' END as ReconcileStatus,BS.DepSlipNumber,D.BankID from OtherDeposits D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON B.BankID=D.BankID AND B.TransCat='Deposit' AND D.DepositDate = B.TransDate"
            StrSQL = StrSQL & "  Left outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND BS.BankID=D.BankID AND BS.TransCat IN ('Deposit','DepositError') AND D.DepositSlipNo = BS.DepSlipNumber where  D.DepositDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "  23:59' Group BY D.DepositSlipNo, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount order by D.DepositDate,D.DepositSlipNo "
            'Response.Write(StrSQL)
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 2
            GetDonationReceipt(StrSQL)
        ElseIf ddlCategory.SelectedValue = "3" Then
            '3 - CC Revenues -
            'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
            ' having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0
            MissingDateMsg = ""
            StrSQL = ""
            While BeginDate <= EndDate
                iDays = Date.DaysInMonth(BeginDate.Year, BeginDate.Month)
                startWith = BeginDate.ToShortDateString
                endWith = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month) - 1)
                If HasRecords(startWith, endWith) = False Then
                    If Len(MissingDateMsg) > 0 Then
                        MissingDateMsg = MissingDateMsg + ","
                    End If
                    MissingDateMsg = MissingDateMsg + startWith
                End If
                StrSQL = StrSQL & " (SELECT SUM(Amount) as Amount,'CGN01'+Replace(CONVERT(VARCHAR(10), StartDate, 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), EndDate, 101),'/','') as VoucherNo ,StartDate,EndDate from("
                StrSQL = StrSQL & " Select Sum(NFG.TotalPayment) as Amount "  ' ,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " ,'" & startWith & "' as StartDate,'" & endWith & "' as EndDate "
                StrSQL = StrSQL & " from NFG_Transactions NFG where NFG.MS_TransDate Between '" & startWith & "' AND '" & endWith & " 23:59 ' Group by NFG.MS_TransDate"

                ''Added to Merge CCRevenueAdjustments
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " Select (SUM(CreditCC)- SUM(Registrations)) as Amount " ','CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate from "
                StrSQL = StrSQL & ",'" & startWith & "' as StartDate,'" & endWith & "' as EndDate "
                'StrSQL = StrSQL & ", CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(TransDate)-1),TransDate),101) AS StartDate,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(DATEADD(mm,1,TransDate))),DATEADD(mm,1,TransDate)),101) AS EndDate "
                StrSQL = StrSQL & " From (select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & startWith & "' AND '" & endWith & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) "
                StrSQL = StrSQL & " Union All "
                StrSQL = StrSQL & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & startWith & "' AND '" & endWith & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
                StrSQL = StrSQL & ")T3 Group by TransDate "
                StrSQL = StrSQL & ")T4 group by StartDate,EndDate) "  'VoucherNo,
                BeginDate = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month))
                If BeginDate < EndDate Then
                    StrSQL = StrSQL & " UNION "
                End If
            End While

            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 3
            GetDonationReceipt(StrSQL)

        ElseIf ddlCategory.SelectedValue = "4" Then
            '4 - TDA Accounts -
            Dim i As Integer
            Dim BankIDs As String = "0"
            If lstBank.Items(i).Selected = True Then
                BankIDs = "1,2,3,4,5,6,7"
            Else
                For i = 0 To lstBank.Items.Count - 1
                    If lstBank.Items(i).Selected Then
                        BankIDs = BankIDs & "," & lstBank.Items(i).Value
                    End If
                Next
            End If

            StrSQL = ""
            While BeginDate <= EndDate
                iDays = Date.DaysInMonth(BeginDate.Year, BeginDate.Month)
                startWith = BeginDate.ToShortDateString
                endWith = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month) - 1)
                If HasRecords(startWith, endWith) = False Then
                    If Len(MissingDateMsg) > 0 Then
                        MissingDateMsg = MissingDateMsg + ","
                    End If
                    MissingDateMsg = MissingDateMsg + startWith
                End If
                StrSQL = StrSQL & " (Select SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & startWith & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & endWith & "', 101),'/','') as VoucherNo,B.BankID,B.BankCode,'" & startWith & "' as StartDate , '" & endWith & "' as EndDate "
                StrSQL = StrSQL & " FROM   BrokTrans B Inner Join Bank BB On B.BankID=BB.BankID WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & startWith & "' AND '" & endWith & " 23:59 '"
                StrSQL = StrSQL & " AND B.BankID in (" & BankIDs & ") Group By   B.BankID,B.BankCode " ' Order By B.BankID CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) " 
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " Select SUM(B.Amount) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & startWith & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & endWith & "', 101),'/','') as VoucherNo,B.BankID,BB.BankCode ,'" & startWith & "' as StartDate , '" & endWith & "' as EndDate "
                StrSQL = StrSQL & " FROM  BankTrans B Inner Join Bank BB On B.BankID=BB.BankID "
                StrSQL = StrSQL & " WHERE B.TransType='Credit' and B.TransCat='Interest' AND  B.TransDate Between '" & startWith & "' AND '" & endWith & " 23:59 '"
                StrSQL = StrSQL & " AND B.BankID in (" & BankIDs & ") Group By B.BankID,BB.BankCode ) "
                BeginDate = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month))
                If BeginDate < EndDate Then
                    StrSQL = StrSQL & " UNION "
                Else
                    StrSQL = StrSQL & " Order by StartDate"
                End If
            End While
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("BankIDs") = BankIDs
            Session("DCatID") = 4
            GetDonationReceipt(StrSQL)
        ElseIf ddlCategory.SelectedValue = "5" Then
            '5 - Vouchers - Expense checks
            StrSQL = " Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber," 'Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End as DateCashed"
            StrSQL = StrSQL & " Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End "
            StrSQL = StrSQL & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else 'Not Available' End"
            StrSQL = StrSQL & " Else 'Not Available' End End as DateCashed"
            StrSQL = StrSQL & ", E.DatePaid,'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,E.BankID " 'Case When Ba.Transdate is not null Then Ba.Transdate Else '' End as DateCashed,
            StrSQL = StrSQL & " From ExpJournal E Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7)"
            StrSQL = StrSQL & " LEFT Join BankTrans Ba on CONVERT(Varchar,Ba.CheckNumber)=E.CheckNumber and Ba.BankID=E.BanKID" ' AND Ba.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
            StrSQL = StrSQL & " LEFT Join BrokTrans Br on (CONVERT(Varchar,Br.ExpCheckNo)=E.CheckNumber OR CONVERT(Varchar,Br.DepCheckNo)=E.CheckNumber) and Br.BankID=E.BanKID " 'AND Br.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
            StrSQL = StrSQL & " WHERE E.CheckNumber is Not null AND (ba.BankTransID IS NOT NULL OR Br.BrokTransID IS NOT Null Or E.TransactionID is not null) AND  (E.Account is Not null or E.TransType = 'Transfers')" ' AND E.DatePaid  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Modified on 29-04-2013

            StrSQL = StrSQL & " And  Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else E.DatePaid End "
            StrSQL = StrSQL & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else E.DatePaid End"
            StrSQL = StrSQL & " Else E.DatePaid End End "
            ' StrSQL = StrSQL & " AND E.DatePaid "
            StrSQL = StrSQL & " Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Modified on 29-04-2013
            StrSQL = StrSQL & " Group BY E.DatePaid, E.CheckNumber,E.BankID,Ba.Transdate, Br.TransDate ORDER BY E.CheckNumber,E.DatePaid"

            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 5
            GetDonationReceipt(StrSQL)

        ElseIf ddlCategory.SelectedValue = "6" Then
            'ACT_EFT Donations
            StrSQL = ""
            While BeginDate <= EndDate
                iDays = Date.DaysInMonth(BeginDate.Year, BeginDate.Month)
                startWith = BeginDate.ToShortDateString
                endWith = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month) - 1)
                If HasRecords(startWith, endWith) = False Then
                    If Len(MissingDateMsg) > 0 Then
                        MissingDateMsg = MissingDateMsg + ","
                    End If
                    MissingDateMsg = MissingDateMsg + startWith
                End If

                StrSQL = StrSQL & " (SELECT  B.BankID,B.BankTransID, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,"
                StrSQL = StrSQL & "B.VendCust, N.Description ,N.DonorType,'" & startWith & "' as StartDate , '" & endWith & "' as EndDate,B.MemberID FROM BankTrans B "
                StrSQL = StrSQL & "LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat = C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID=1"
                StrSQL = StrSQL & "LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat = H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID=2"
                StrSQL = StrSQL & "Inner Join NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
                StrSQL = StrSQL & "Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between '" & startWith & "' and '" & endWith & "'"
                StrSQL = StrSQL & "Group By B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate,B.Vendcust,B.BankTransID,B.MemberID) "
                BeginDate = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month))
                If BeginDate < EndDate Then
                    StrSQL = StrSQL & " UNION "
                Else
                    StrSQL = StrSQL & " order by TransDate "
                End If
            End While
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 6
            GetDonationReceipt(StrSQL)

            'lblErr.Text = "This Category is under Development"
        ElseIf ddlCategory.SelectedValue = "7" Then
            'Vouchers Bank Service Charges
            StrSQL = "SELECT B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.VendCust,'') as VoucherNo," ''_'+RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
            StrSQL = StrSQL & " E.AccountName,B.TransDate,'VPE' as TransType,E.ExpCatCode,  IsNull(B.VendCust,'') as VendCust,B.Reason,B.AddInfo"
            StrSQL = StrSQL & " FROM BankTrans  B "
            StrSQL = StrSQL & " INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode "
            StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo "
            StrSQL = StrSQL & " Where TransType= 'DEBIT' And TransCat = 'CreditCard' and TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
            StrSQL = StrSQL & " Group By B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
            StrSQL = StrSQL & " UNION ALL "
            StrSQL = StrSQL & " SELECT B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ + IsNull(B.VendCust,'') as VoucherNo," '_'+'RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
            StrSQL = StrSQL & " E.AccountName,B.TransDate,'VPE' as TransType,E.ExpCatCode,IsNull(B.VendCust,'') as  VendCust,B.Reason,B.AddInfo"
            StrSQL = StrSQL & " FROM BankTrans B "
            StrSQL = StrSQL & " INNER JOIN ExpenseCategory  E ON B.TransCat= E.ExpCatCode "
            StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo "
            StrSQL = StrSQL & " where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
            StrSQL = StrSQL & " Group By B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 7
            GetDonationReceipt(StrSQL)
            'Response.Write(StrSQL)
        ElseIf ddlCategory.SelectedValue = "8" Then
            ''TransferOut
            StrSQL = "SELECT B.BankID,B.BrokTransID,B.BankCode,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+"
            StrSQL = StrSQL & " Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as VoucherNo,B.TransDate,B.TransCat,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate,'IGT' as TransType,B.AssetClass,B.Ticker,B.Quantity,B.Price,"
            StrSQL = StrSQL & " B.NetAmount as Amount,B.RestType,B1.RestType as RestTypeTo,B1.BankID as ToBankID,N.Description FROM BrokTrans B "
            StrSQL = StrSQL & " Inner join BrokTrans B1 On B.TransType = B1.TransType and B.TransDate= B1.TransDAte and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and 
            StrSQL = StrSQL & " Inner join NSFAccounts N On N.BankID = B.BankId " ' and B.RestType=N.RestrictionType "
            StrSQL = StrSQL & " and (B.RestType=N.RestrictionType Or N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end  ))"
            StrSQL = StrSQL & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            StrSQL = StrSQL & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Transdate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Order by B.TransDate"
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 8
            GetDonationReceipt(StrSQL)

        ElseIf ddlCategory.SelectedValue = "9" Then
            ''Buy Transactions
            StrSQL = ""
            While BeginDate <= EndDate
                iDays = Date.DaysInMonth(BeginDate.Year, BeginDate.Month)
                startWith = BeginDate.ToShortDateString
                endWith = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month) - 1)
                If HasRecords(startWith, endWith) = False Then
                    If Len(MissingDateMsg) > 0 Then
                        MissingDateMsg = MissingDateMsg + ","
                    End If
                    MissingDateMsg = MissingDateMsg + startWith
                End If
                StrSQL = StrSQL & " (SELECT B.BankID,B.BankCode,N.AccNo,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount"
                StrSQL = StrSQL & " ,N.Description, 'IGB' as TransType,B.AssetClass,"
                StrSQL = StrSQL & " 'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,N.Description,'" & startWith & "' as StartDate,'" & endWith & "' as EndDate,B.Disposed_Basis,B.Capital_Gains FROM BrokTrans B "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
                StrSQL = StrSQL & " Where B.Transcat='Buy' and B.TransDate Between '" & startWith & "' and '" & endWith & "' "
                StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass,B.Disposed_Basis,B.Capital_Gains ) "
                BeginDate = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month))
                If BeginDate < EndDate Then
                    StrSQL = StrSQL & " UNION "
                Else
                    StrSQL = StrSQL & " Order by TransDate"
                End If
            End While
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 9
            GetDonationReceipt(StrSQL)
        ElseIf ddlCategory.SelectedValue = "10" Then
            ''Sell Transactions
            StrSQL = ""
            While BeginDate <= EndDate
                iDays = Date.DaysInMonth(BeginDate.Year, BeginDate.Month)
                startWith = BeginDate.ToShortDateString
                endWith = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month) - 1)
                If HasRecords(startWith, endWith) = False Then
                    If Len(MissingDateMsg) > 0 Then
                        MissingDateMsg = MissingDateMsg + ","
                    End If
                    MissingDateMsg = MissingDateMsg + startWith
                End If
                StrSQL = StrSQL & " (SELECT Distinct B.BankID,B.BankCode,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount,N.AccNo" '
                StrSQL = StrSQL & " , 'IGS' as TransType,B.AssetClass,N.Description,B.Ticker," '
                StrSQL = StrSQL & " 'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,'" & startWith & "' as StartDate,'" & endWith & "' as EndDate,B.Disposed_Basis,B.Capital_Gains FROM BrokTrans B "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
                StrSQL = StrSQL & " Where B.Transcat='Sell' and B.TransDate Between '" & startWith & "' and '" & endWith & "' "
                StrSQL = StrSQL & " and N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end  )"
                StrSQL = StrSQL & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass,B.Ticker,B.Disposed_Basis,B.Capital_Gains) "
                BeginDate = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month))
                If BeginDate < EndDate Then
                    StrSQL = StrSQL & " UNION "
                Else
                    StrSQL = StrSQL & " order by TransDate "
                End If
            End While
            Session("Frmdate") = TxtFrom.Text
            Session("ToDate") = txtTo.Text
            Session("ManageVouchersSQl") = StrSQL
            Session("DCatID") = 10
            GetDonationReceipt(StrSQL)
        End If
    End Sub

    Function GetData(ByVal Grdvw As GridView) As String
        Dim dsTransDonation As New DataSet
        Dim Filename As String = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrTrans"))
        If dsTransDonation.Tables(0).Rows.Count > 0 Then
            dsTransDonation.Tables(0).Columns.Remove("DonationID")
            dsTransDonation.Tables(0).Columns.Remove("ChapterID")
            Filename = "DGN" & dsTransDonation.Tables(0).Rows(0)("DepositDate").ToString().Replace("/", "") & dsTransDonation.Tables(0).Rows(0)("DepositSlip") & "_" & Now.Month & "_" & Now.Day & "_" & Now.Year
            Grdvw.DataSource = dsTransDonation.Tables(0)
            Grdvw.DataBind()
        Else
            Grdvw.DataSource = Nothing
            Grdvw.DataBind()
        End If
        Return Filename
    End Function

    Public Sub downfile(ByVal filename As String, ByVal sb As StringBuilder)
        'Dim file As System.IO.FileInfo = New System.IO.FileInfo(filename) '-- if the file exists on the server
        'If file.Exists Then 'set appropriate headers
        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & filename)
        Response.AddHeader("Content-Length", sb.Length.ToString())
        Response.ContentType = "application/octet-stream"
        Response.Write(sb.ToString())
        ' Response.WriteFile(file.FullName)
        Response.End() 'if file does not exist
        'Else
        ' Response.Write("This file does not exist.")
        'End If 'nothing in the URL as HTTP GET
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvregistrationchapter As Control)
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TrDetailView.Visible = False
        btnClose.Visible = False
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.Remove("Frmdate")
        Session.Remove("ToDate")
        Session.Remove("ManageVouchersSQl")
        Session.Remove("DCatID")
        Session.Remove("StrTrans")
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub
    Public Sub LoadErrorData()
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Try
            dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrTrans"))
            If dsTransDonation.Tables(0).Rows.Count > 0 Then
                'trEdit.Visible = True
                btnClose.Visible = True
                TrDetailView.Visible = True
                lblErr.Text = ""
                grdEditVoucher.DataSource = dsTransDonation.Tables(0)
                grdEditVoucher.DataBind()
            Else
                grdEditVoucher.DataSource = Nothing
                grdEditVoucher.DataBind()
                lblErr.Text = "Sorry No detailed view to show"
                ' Tredit.Visible = False
                TrDetailView.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub grdEditVoucher_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdEditVoucher.CancelCommand
        grdEditVoucher.EditItemIndex = -1
        LoadErrorData()
    End Sub
    Protected Sub grdEditVoucher_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)
        DonationID = CInt(e.Item.Cells(1).Text)
        DonorType = CType(e.Item.Cells(6).FindControl("lblDonorType"), Label).Text
        BusType = CType(e.Item.Cells(7).FindControl("lblBusType"), Label).Text
        IRSCat = CType(e.Item.Cells(8).FindControl("lblIRSCat"), Label).Text
        DonationType = CType(e.Item.Cells(9).FindControl("lblDonationType"), Label).Text
        ChapterCode = CType(e.Item.Cells(10).FindControl("lblChapterCode"), Label).Text
        Amount = CType(e.Item.Cells(11).FindControl("lblAmount"), Label).Text
        Session("DonationID") = DonationID
        Session("DonorType") = DonorType
        Session("BusType") = BusType
        Session("IRSCat") = IRSCat
        Session("DonationType") = DonationType
        Session("ChapterCode") = ChapterCode
        LoadErrorData()

    End Sub
    Protected Sub grdEditVoucher_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim row As Integer = CInt(e.Item.ItemIndex)
        Dim ddlTemp As DropDownList

        Dim chapterId As Integer

        ddlTemp = e.Item.FindControl("ddlDonorType")
        DonorType = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlBusType")
        BusType = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlIRSCat")
        IRSCat = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlDonationType")
        DonationType = ddlTemp.SelectedItem.Text

        ddlTemp = e.Item.FindControl("ddlChapterCode")
        ChapterCode = ddlTemp.SelectedItem.Text
        chapterId = ddlTemp.SelectedValue

        Amount = CType(e.Item.FindControl("txtAmount"), TextBox).Text
        If DonorType.Trim = "OWN" Then
            'Change in DonationsInfo & Organization Table (BusType,IRSCat)
            If Session("BusType") <> BusType.Trim Or Session("IRSCat") <> IRSCat.Trim Then
                'Organization Table (BusType,IRSCat)
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update OrganizationInfo set BusType='" & BusType & "',IRSCat='" & IRSCat & "' where AutoMemberID= " & CType(e.Item.FindControl("lblMemberID"), Label).Text)
            End If
        End If
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " Update DonationsInfo SET  DonorType='" & DonorType & "', AMOUNT=" & Amount & ",ChapterId=" & chapterId & ", DonationType='" & DonationType & "' where DonationID = " & Session("DonationID"))
        lblErr.Text = "Updated Successfully"
        '**If validation passess
        grdEditVoucher.EditItemIndex = -1
        '**If Validation fails
        '**grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)

        LoadErrorData()
    End Sub

    Public Sub SetDropDown_BusType(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("BusType")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_DonorType(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("DonorType")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_Chapter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ChapterID,WebFolderName from Chapter order by State,Chaptercode")
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("ChapterCode")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_donationtype(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("DonationType")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_IRSCat(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("IRSCat")))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnGenAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sb As StringBuilder
        Dim i, j As Integer
        Dim ds As DataSet
        lblErr.Text = ""
        Dim Filename As String
        Dim gcnt As Integer
        If ddlCategory.SelectedValue = "1" Then
            'Deposits
            Filename = "DGN" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            sb = New StringBuilder("")
            sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!ENDTRNS" & vbTab)
            For gcnt = 0 To gvDonation.Rows.Count - 1
                strSql = "select A,B,C,D,E,F,Sum(G),H,I,J from (select CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), D.DepositDate, 101)   as D , Case when D.DonationType ='Temp Restricted' and D.Project ='DAF' Then 10108 Else N.AccNo End as  E,C.WebFolderName as F,SUM(D.AMOUNT) as G,'' as H, 'DGN'+ RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) + REPLACE(CONVERT(VARCHAR(10), DepositDate, 101),'/','') + Convert(varchar,D.DepositSlip) as I,0 as J  From DonationsInfo D Inner Join NSFAccounts N ON D.BankID=N.BankID and D.DonationType=N.RestrictionType AND (N.InvIncType IS NULL OR N.InvIncType<>'Inv')  Inner Join Chapter  C On N.ChapterID = C.ChapterID WHERE D.DepositSlip = " & gvDonation.DataKeys(gcnt).Value & " AND D.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'" & " "
                'strSql = strSql & " and ((D.DonationType ='Temp Restricted' and D.Project ='DAF' and N.accNo  =10108) Or ((D.Project is null Or D.Project <>'DAF') and N.accNo  <>10108)) "
                'strSql = strSql & " and ((D.DonationType ='Temp Restricted' and D.Project ='DAF') Or N.accNo  <>10108)" ' and N.accNo  =10108 ((D.Project is null Or D.Project <>'DAF') and N.accNo  <>10108))"
                strSql = strSql & " and ((D.DonationType ='Temp Restricted' and D.Project ='DAF'  and D.BankID<>1) Or (N.accNo  <>10108 and D.BankID=1  ))"
                strSql = strSql & " Group by D.DepositDate,D.DepositSlip,N.AccNo,D.BankID,C.WebFolderName,D.DonationType,D.Project"
                strSql = strSql & " UNION ALL"
                strSql = strSql & " select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), D.DepositDate, 101)  as D ,Acc.AccNo as E,Ch.webfoldername as F,-D.AMOUNT as G,'' as H, 'DGN'+ RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) + REPLACE(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','') + Convert(varchar,D.DepositSlip) as I, 1 as J"
                strSql = strSql & " FROM DonationsInfo D Inner Join  NSFAccounts Acc ON    D.DonationType=Acc.RestrictionType AND Acc.AccType='I' AND Acc.DonorType='IND/SPOUSE'AND (Acc.InvIncType IS NULL OR Acc.InvIncType<>'Inv')  "
                strSql = strSql & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID "
                strSql = strSql & " WHERE D.DonorType <>'OWN' and  D.DepositSlip = " & gvDonation.DataKeys(gcnt).Value & " AND D.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " UNION ALL"
                strSql = strSql & " select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), D.DepositDate, 101)   as D ,Acc.AccNo as E,Ch.webfoldername as F,-D.AMOUNT as G,'' as H, 'DGN'+ RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2) + REPLACE(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','') + Convert(varchar,D.DepositSlip) as I, 1 as J"
                strSql = strSql & " FROM DonationsInfo D Inner Join  NSFAccounts Acc ON D.DonorType = Acc.DonorType  AND D.DonationType=Acc.RestrictionType AND Acc.AccType='I'"
                strSql = strSql & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID"
                strSql = strSql & " Inner Join  OrganizationInfo O ON D.MemberID = O.Automemberid AND D.DonorType = 'OWN' AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END"
                strSql = strSql & " WHERE D.DepositSlip = " & gvDonation.DataKeys(gcnt).Value & " AND D.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " UNION ALL"
                strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                strSql = strSql & " ) TMP Group by A,B,C,D,E,F,H,I,J Order By J,E"
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                ds.Tables(0).Columns.Remove("J")
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
            Next
            sb.AppendLine("ENDTRNS" & vbTab)
            'Download file
            downfile(Filename, sb)
        ElseIf ddlCategory.SelectedValue = "2" Then
            'Other Deposits
            Filename = "OTH" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            sb = New StringBuilder("")
            sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!ENDTRNS" & vbTab)
            For gcnt = 0 To gvDonation.Rows.Count - 1
                strSql = "select A,B,C,D,E,F,Sum(G),H,I,J from (select CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), OD.DepositDate, 101)   as D ,N.AccNo as E,C.WebFolderName as F,SUM(OD.AMOUNT) as G,'' as H, 'OTH'+ RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as I,0 as J  From OtherDeposits OD Inner Join NSFAccounts N ON OD.BankID=N.BankID and OD.RestrictionType=N.RestrictionType and  N.[Level]='L'  INNER JOIN Chapter C ON N.ChapterID =C.ChapterID WHERE OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'" & " Group by OD.DepositDate,OD.DepositSlipNo,N.AccNo,OD.BankID,C.WebFolderName"
                strSql = strSql & " UNION ALL"
                'Fees 1/3 and 2/3
                strSql = strSql & " select  'SPL' as A,'' as B,'GENERAL JOURNAL' as C,T.DepositDate as D, T.AccNo as E,T.WebFolderName as F,-ROUND(Sum(T.Amount),2) as  G,'' as H, T.VoucherNo  as I, 1 as J  from ("
                strSql = strSql & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51200  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "

                'Coaching
                strSql = strSql & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "

                'PrepClub Coaching added on 24-02-2014
                strSql = strSql & " select SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =19 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All"
                strSql = strSql & " select SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =19 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID Union All "

                '*************** Added on 09-08-2014 as Mr.Ramdev wanted to use a separate account 51450 for food sales*****************'
                strSql = strSql & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51450  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.SalesID = 1 AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID UNION ALL "
                '********************************************************************************************************************************************'
                strSql = strSql & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51400  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.SalesID <> 1 AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID UNION ALL "


                '/***************Added on Dec 10 2014 to inlcude Refund in Other Deposits*********************************'
                strSql = strSql & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,Ex.Account as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD inner join ExpenseCategory EX on Ex.ExpCatID =OD.RefundId Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Refund' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,Ex.Account UNION ALL "
                '/**************************************************************************************************************************'

                strSql = strSql & " select SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,CASE WHEN OD.EventID=1 or OD.EventID=2 THEN 51100 ELSE CASE WHEN OD.EventID=3 THEN 51200 ELSE 51300 END END  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sponsorship' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                strSql = strSql & " group by OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID)"
                strSql = strSql & " T inner Join  NSFAccounts N ON T.AccNo = N.AccNo Group by T.DepositDate,T.VoucherNo,T.AccNo,N.Description,T.WebFolderName,T.State,T.Name "
                strSql = strSql & " UNION ALL"
                strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                strSql = strSql & " ) TMP Group by A,B,C,D,E,F,H,I,J Order By J,E"
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                ds.Tables(0).Columns.Remove("J")
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
            Next
            sb.AppendLine("ENDTRNS" & vbTab)
            'Download file
            downfile(Filename, sb)
        ElseIf ddlCategory.SelectedValue = "3" Then
            'CREDIT CARDS
            'Dim frmdate As Date = Convert.ToDateTime(GVCCRevenues.Rows(gcnt).Cells(3).Text)
            'Dim todate As DateTime = Convert.ToDateTime(GVCCRevenues.Rows(gcnt).Cells(4).Text + " 23:59")
            ',Convert(DateTime,'" & todate & "')
            ''strSql = "select A,B,C,D,E,F,Sum(G),H,I,J from (select 'TRNS' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D ,N.AccNo as E,'US_HomeOffice' as F,Sum(NFG.TotalPayment) as G,'' as H, 'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as I,0 as J  From NFG_Transactions NFG Inner Join NSFAccounts N ON N.BankID=1 and N.RestrictionType='Unrestricted' where NFG.MS_TransDate Between '" & GVCCRevenues.Rows(gcnt).Cells(3).Text & "' AND '" & GVCCRevenues.Rows(gcnt).Cells(4).Text & " 23:59' Group BY N.AccNo having SUM(NFG.[TotalPayment]) > 0 or SUM(NFG.[TotalPayment]) < 0"
            ''strSql = strSql & " UNION ALL "
            ''strSql = strSql & " select  'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D, T.AccNo as E,T.WebFolderName as F,-ROUND(Sum(T.Amount),2) as  G,'' as H, 'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as I, 1 as J  from (SELECT SUM(NFG.Fee)*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            ''strSql = strSql & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.EventId=2 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            ''strSql = strSql & " SELECT SUM(NFG.Fee)*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            ''strSql = strSql & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  NFG.Fee is not null and NFG.EventId=3 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
            ''strSql = strSql & " SELECT SUM(NFG.Fee)*2/3 as Amount, 41105 as AccNo,5 as OrderNo,  'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG  where NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having SUM(NFG.Fee)>0 or SUM(NFG.Fee)<0  Union All"
            ''strSql = strSql & " SELECT SUM(NFG.Fee)*1/3 as Amount, 51100 as AccNo,6 as OrderNo,  'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG    where NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having SUM(NFG.Fee)>0 or SUM(NFG.Fee)<0  Union All"
            ''strSql = strSql & " SELECT SUM(NFG.MealsAmount)as Amount, 51100 as AccNo,7 as OrderNo,  'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG   where NFG.MealsAmount>0 and NFG.MealsAmount is not Null and NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having SUM(NFG.MealsAmount)>0 or SUM(NFG.MealsAmount)<0 Union All"
            ''strSql = strSql & " SELECT SUM(NFG.[Contribution Amount])as Amount, 41101 as AccNo,8 as OrderNo,  C.WebFolderName, C.State,C.Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[Contribution Amount] > 0 or NFG.[Contribution Amount] < 0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name Union All"
            ''strSql = strSql & " SELECT SUM(NFG.[TotalPayment])as Amount, 51400 as AccNo,9 as OrderNo,  C.WebFolderName, C.State,C.Name"
            ''strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[TotalPayment] > 0 or NFG.[TotalPayment] < 0) and NFG.EventID=10 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name) T"
            ''strSql = strSql & " inner Join  NSFAccounts N ON T.AccNo = N.AccNo "
            ''strSql = strSql & " Group by T.AccNo,N.Description,T.WebFolderName,T.State,T.Name"


            Filename = "CGN" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            sb = New StringBuilder("")
            sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!ENDTRNS" & vbTab)

            For gcnt = 0 To GVCCRevenues.Rows.Count - 1
                Dim frmdate As Date = Convert.ToDateTime(GVCCRevenues.Rows(gcnt).Cells(3).Text)
                Dim todate As DateTime = Convert.ToDateTime(GVCCRevenues.Rows(gcnt).Cells(4).Text + " 23:59")

                strSql = "select A,B,C,D,E,F,Sum(G),H,I,J from (select 'TRNS' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D ,N.AccNo as E,C.WebfolderName as F,Sum(NFG.TotalPayment) as G,'' as H, 'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as I,0 as J  From NFG_Transactions NFG Inner Join NSFAccounts N ON N.BankID=1 and N.RestrictionType='Unrestricted' Inner  Join Chapter C ON N.ChapterID = C.ChapterID where NFG.MS_TransDate Between '" & GVCCRevenues.Rows(gcnt).Cells(3).Text & "' AND '" & GVCCRevenues.Rows(gcnt).Cells(4).Text & " 23:59' Group By N.AccNo,C.WebFolderName having SUM(NFG.[TotalPayment]) > 0 or SUM(NFG.[TotalPayment]) < 0"
                strSql = strSql & " UNION ALL "
                strSql = strSql & " select  'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D, T.AccNo as E,T.WebFolderName as F,-ROUND(Sum(T.Amount),2) as  G,'' as H, 'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as I, 1 as J  from ("

                ' ''Contestant
                strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

                '' ''DuplicateContestantReg
                strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

                'Registration
                strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

                'Finals and NFG.MatchedStatus='O'  NFG.MatchedStatus='O' and 
                strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
                strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=1 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,6 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"
                strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=1 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

                '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=13 and Amount >0 are summed. ChapterID should be 112 for Coaching
                'Coaching - similar to Finals  and NFG.MatchedStatus='O' NFG.MatchedStatus='O' and 
                strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Coaching' as WebFolderName, 'US' as State,'Coaching' as Name"
                strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=13 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Coaching' as WebFolderName, 'US' as State,'Coaching' as Name"
                strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=13 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

                '***************Added on 07/22/2014 as per new req. If EventID=19 [PrepClub], NFG_Transactions table is used and the records with EventID=19 and Amount >0 are summed by ChapterID.
                'Prepclub
                strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM Registration_PrepClub R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=19 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM Registration_PrepClub R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=19 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"

                '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=4 and Amount >0 are summed. ChapterID should be 117 for Game
                'Game - similar to Finals and NFG.MatchedStatus='O' NFG.MatchedStatus='O' and 
                strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
                strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=4 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
                strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=4 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

                '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=3 and Amount >0 are summed. ChapterID should be ?? for Workshop
                'Workshop - similar to Finals 
                ''To be Done for Workshop
                'StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
                'StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=20 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                'StrSQl = StrSQl & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name"
                'StrSQl = StrSQl & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.MatchedStatus='O' and NFG.EventId=20 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                '*********************************************'
                '*********************************************'
                '*********************************************'

                ''NFG_Supp

                ''NFG_Supp--Contestant
                strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
                'NFG_Supp----Registration
                strSql = strSql & " SELECT (SUM(IsNull(R.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(IsNull(R.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null  and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name  Union All"

                '*************************************************************************************************************************************************************************************************************'
                'NFG_Supp----PrepClub_Registration Added on 08-19-2014 for PrepCLub Refund Records
                strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=19 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"
                strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=19 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name  Union All"


                '/************Meals Amount**************Replacing  51100 with 51450 on 07-10-2013*****************************************************************/
                strSql = strSql & " SELECT SUM(NFG.MealsAmount) as Amount,51450 as AccNo,7 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name"  ' C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Transactions NFG where (NFG.MealsAmount>0 or NFG.MealsAmount<0) and NFG.MealsAmount is not Null and NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.MealsAmount)>0  or SUM(NFG.MealsAmount)<0) Union All"  '  Inner Join Chapter C On NFG.ChapterID = C.ChapterID  Group by C.WebFolderName, C.State,C.Name

                'Donations
                strSql = strSql & " SELECT SUM(NFG.[Contribution Amount])as Amount, 41101 as AccNo,8 as OrderNo,  ISNULL(C.WebFolderName,'US_HomeOffice') as WebFolderName, ISNULL(C.State,'IL') as State,ISNULL(C.Name,'Home Office') as Name "
                strSql = strSql & " FROM NFG_Transactions NFG Left Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[Contribution Amount] > 0 or NFG.[Contribution Amount] < 0) and NFG.EventID not in (10) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name Union All"
                strSql = strSql & " SELECT SUM(NFG.[TotalPayment])as Amount, 51400 as AccNo,9 as OrderNo,  C.WebFolderName, C.State,C.Name"
                strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[TotalPayment] > 0 or NFG.[TotalPayment] < 0) and NFG.EventID=10 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name "
                strSql = strSql & " ) T inner Join  NSFAccounts N ON T.AccNo = N.AccNo "
                strSql = strSql & " Group by T.AccNo,N.Description,T.WebFolderName,T.State,T.Name"

                ''Added To merge CC Revenues Adjustments''05-11-2012
                ''CC Revenue Adjustments are to make the NFG_transactions equal to BankTrans numbers for the same period

                strSql = strSql & " UNION ALL "
                'StrSQl = StrSQl & " Select A,B,C,D,E,F,Sum(G),H,I,J from ("
                strSql = strSql & "select A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101)  as D, Temp.AccNo as E, F,-ROUND(Sum(Temp.Amount),2) as  G,'' as H, VoucherNo as I, J  from "
                strSql = strSql & "(Select  'SPL' as A,'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*2/3 as Amount,41105 as AccNo,'US_HomeOffice' as F,1 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
                strSql = strSql & "(select SUM(Amount) as CreditCC,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                strSql = strSql & "select 0 as CreditCC, SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
                strSql = strSql & ")T1 Group by TransDate  UNION ALL "
                '***********Added on 25-02-2014 to show transactions are not in Contests['Program Service Fees - Contests'] for sep,oct,nov ***********'
                strSql = strSql & "Select  'SPL' as A,'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51150 as AccNo,'US_HomeOffice' as F,1 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
                strSql = strSql & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                strSql = strSql & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
                strSql = strSql & ") T21  where Month(T21.TransDate) in (9,10,11) Group by TransDate UNION ALL "
                '****************Updated on on 25-02-2014 **********************************************************************************************'
                strSql = strSql & "Select  'SPL' as A,'CGN' as TransType,(SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51100 as AccNo,'US_HomeOffice' as F,1 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from"
                strSql = strSql & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                strSql = strSql & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
                strSql = strSql & ") T22  where Month(T22.TransDate) not in (9,10,11) Group by TransDate UNION ALL "
                '****************************************************************************************************************************************'
                strSql = strSql & "Select  'TRNS' as A,'CGN' as TransType,-(SUM(CreditCC)-SUM(Registrations)) as Amount,10101 as AccNo,'US_HomeOffice' as F,0 as J,'CGN01'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10),Convert(DateTime,'" & todate & "'), 101),'/','') as VoucherNo,'" & frmdate & "' as StartDate , '" & todate & "' as EndDate from "
                strSql = strSql & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                strSql = strSql & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
                strSql = strSql & ") T3 Group by TransDate "
                strSql = strSql & " )Temp Group by Temp.AccNo,F,VoucherNo,A,J "

                strSql = strSql & " UNION ALL"
                strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
                strSql = strSql & " ) TMP Group by A,B,C,D,E,F,H,I,J Order By J,E"

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                ds.Tables(0).Columns.Remove("J")
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
            Next
            sb.AppendLine("ENDTRNS" & vbTab)
            'Download file
            downfile(Filename, sb)


            'ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            'If ds.Tables(0).Rows.Count > 0 Then
            '    lblErr.Text = ""
            '    ds.Tables(0).Columns.Remove("J")
            '    Filename = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "") & ".iif" '"VPS" & ds.Tables(0).Rows(0)("D").ToString().Replace("/", "") & gvExp.DataKeys(index).Value.ToString() & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            '    ' fp = File.CreateText(Filename)
            '    'fp.WriteLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            '    'fp.WriteLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            '    'fp.WriteLine("!ENDTRNS" & vbTab)
            '    sb = New StringBuilder("")
            '    sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            '    sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            '    sb.AppendLine("!ENDTRNS" & vbTab)

            '    For i = 0 To ds.Tables(0).Rows.Count - 1
            '        If i > 0 Then
            '            ' fp.WriteLine()
            '            sb.AppendLine()
            '        End If
            '        For j = 0 To ds.Tables(0).Columns.Count - 1
            '            If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
            '                ' fp.Write(ds.Tables(0).Rows(i)(j))
            '                sb.Append(ds.Tables(0).Rows(i)(j))
            '                Exit For
            '            End If
            '            If j = ds.Tables(0).Columns.Count - 1 Then
            '                'fp.Write(ds.Tables(0).Rows(i)(j))
            '                sb.Append(ds.Tables(0).Rows(i)(j))
            '            Else
            '                'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
            '                sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
            '            End If
            '        Next
            '    Next
            '    ' fp.Close()
            '    'Download file
            '    sb.AppendLine()
            '    sb.AppendLine("ENDTRNS" & vbTab)
            '    downfile(Filename, sb)
            'End If

        ElseIf ddlCategory.SelectedValue = "4" Then
            'TDA Accounts
            Dim frmdate As Date = Convert.ToDateTime(GVTDAIncome.Rows(gcnt).Cells(3).Text)
            Dim todate As DateTime = Convert.ToDateTime(GVTDAIncome.Rows(gcnt).Cells(4).Text + " 23:59")
            'TDA Accounts
            strSql = " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,  DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo As E,C.WebFolderName as  F, Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End  as G,Case WHEN B.BankID = 4 Then 'ISCH' ELSE Case WHEN B.BankID = 5 Then 'IGEN' ELSE Case WHEN B.BankID = 6 Then 'IEND' ELSE 'IDAF' End End End + Replace(CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),'/','') +'_'+ Replace(CONVERT(VARCHAR(25),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),101),'/','') As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,0 as Ord,B.BankID FROM BrokTrans B "
            'strSql = strSql & " Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted') OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7))) OR (Acc.AccNo = 15101 AND  B.AssetClass <> 'MMK') "
            strSql = strSql & " Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='MMK') OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK')"
            strSql = strSql & " Inner Join Chapter C ON Acc.ChapterID =C.ChapterID "
            strSql = strSql & " WHERE Acc.IntDiv='Y' AND ((B.AssetClass IN ('MMK','Cash') AND  B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Session("BankIDs") & ") Group By  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo,C.WebFolderName, B.NetAmount , B.Quantity  union All"
            strSql = strSql & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo as E,C.WebFolderName as  F,-SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) AS G,B.Ticker + '' + CONVERT(VARCHAR(25),b.TransDate,101)  As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,1 as Ord,B.BankID FROM BrokTrans B"
            strSql = strSql & " Inner Join NSFAccounts Acc On ((Acc.InvIncType ='Dividend' AND B.TransCat = 'OrdDiv') OR (Acc.InvIncType ='DivCG' AND B.TransCat IN ('STCGDiv','LTCGDiv')) OR (Acc.InvIncType ='Interest' AND B.TransCat = 'Div')) AND ((Acc.RestrictionType ='Unrestricted' AND B.BankID IN (4,5,6)) OR (Acc.RestrictionType ='Temp Restricted' AND B.BankID =7))"
            'strSql = strSql & " Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType is NULL) OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK')"
            strSql = strSql & " Inner Join Chapter C ON Acc.ChapterID =C.ChapterID "
            strSql = strSql & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Session("BankIDs") & ")" 'Group by C.WebFolderName"  'Order By B.BankID,YEAR(TransDate),MONTH(B.TransDate), 13,Acc.AccNo"
            strSql = strSql & " Group By  B.TransDate,B.Ticker,B.BankID,Acc.AccNo,Acc.Description,Acc.AccType,Acc.ChapterID,Acc.RestrictionType,B.TransCat,C.WebfolderName"
            '' Added to include BankIds(1,2,3) on 19-11-2012
            strSql = strSql & " UNION ALL "
            strSql = strSql & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo As E,C.WebFolderName as F,SUM(B.Amount) as G,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End + Replace(CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),'/','') +'_'+ Replace(CONVERT(VARCHAR(25),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),101),'/','') As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,0 as Ord,B.BankID FROM BankTrans B Inner Join NSFAccounts Acc On Acc.BankID=B.BankID AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Cash' AND Acc.BankID in (1,2,3)   Inner Join Chapter C ON Acc.ChapterID =C.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit' and B.TransDate Between '" & frmdate & "' and '" & todate & "' and B.BankID in (" & Session("BankIDs") & ") AND Acc.IntDiv='Y' Group By CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo,C.WebFolderName "
            strSql = strSql & " UNION ALL"
            strSql = strSql & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) as D,Acc.AccNo As E,C.WebFolderName as F,-SUM(B.Amount) as G,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End + Replace(CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),'/','') +'_'+ Replace(CONVERT(VARCHAR(25),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),101),'/','') As H,'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as I, '' AS J,MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,0 as Ord,B.BankID FROM BankTrans B Inner Join NSFAccounts Acc On (Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Interest') AND B.BankID in (1,2,3) Inner Join Chapter C ON Acc.ChapterID =C.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit'  AND B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Session("BankIDs") & ") Group By CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)),YEAR(TransDate),MONTH(B.TransDate),B.BankID, Acc.AccNo,C.WebFolderName Order by D,B.BankId,Acc.Accno"

            'strSql = " SELECT 1 as H, Acc.AccNo as AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate, Acc.Description,'US_HomeOffice' as Class,Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End AS Amount,B.Ticker + '' + CONVERT(VARCHAR(25),b.TransDate,101) As I, MONTH(TransDate) as Mnth,YEAR(TransDate) as Yr,RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID FROM BrokTrans B"
            'strSql = strSql & " Inner Join NSFAccounts Acc On ((Acc.InvIncType ='Dividend' AND B.TransCat = 'OrdDiv') OR (Acc.InvIncType ='DivCG' AND  B.TransCat IN ('STCGDiv','LTCGDiv')) OR (Acc.InvIncType ='Interest' AND B.TransCat = 'Div')) AND ((Acc.RestrictionType ='Unrestricted' AND B.BankID IN (4,5,6)) OR (Acc.RestrictionType ='Temp Restricted' AND B.BankID =7))" '
            ''StrSQL = StrSQL & "  Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType is NULL) OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK')"
            'strSql = strSql & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' "
            'strSql = strSql & " AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Request.QueryString("BID") & ")"
            'strSql = strSql & " Union All "
            'strSql = strSql & " SELECT 0 as H, Acc.AccNo as AccNo,'' as DonorType,'' as DonationType,CONVERT(VARCHAR(10), '" & todate & "', 101)  as DepositDate, Acc.Description,'US_HomeOffice' as Class,-SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) AS Amount,Case WHEN B.BankID = 4 Then 'ISCH' ELSE Case WHEN B.BankID = 5 Then 'IGEN' ELSE Case WHEN B.BankID = 6 Then 'IEND' ELSE 'IDAF' End End End + Replace(CONVERT(VARCHAR(10), '" & frmdate & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & todate & "', 101),'/','') as I, 0 as Mnth,0 as Yr, RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID FROM   BrokTrans B  Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK'  AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted'  AND Acc.InvIncType is NULL) OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND  B.AssetClass <> 'MMK') "
            'strSql = strSql & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & frmdate & "' AND '" & todate & "' AND B.BankID in (" & Request.QueryString("BID") & ") Group By  B.BankID, Acc.AccNo,Acc.Description  "
            'strSql = strSql & " Order By 1 Desc, B.BankID,YEAR(TransDate),MONTH(B.TransDate), Acc.AccNo "

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim cnt As Integer = 0
                Dim CurrBanKID As Integer = 0
                Dim CurrMon As Integer = 0
                Dim row1 As DataRow
                Dim destinationrowcount As Integer = ds.Tables(0).Rows.Count - 1
                Do While cnt <= destinationrowcount
                    If CurrBanKID = 0 Then
                        CurrBanKID = ds.Tables(0).Rows(cnt)("BankID")
                        CurrMon = ds.Tables(0).Rows(cnt)("Mnth")
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    ElseIf CurrBanKID <> ds.Tables(0).Rows(cnt)("BankID") Or CurrMon <> ds.Tables(0).Rows(cnt)("Mnth") Then
                        row1 = ds.Tables(0).NewRow
                        CurrBanKID = ds.Tables(0).Rows(cnt)("BankID")
                        CurrMon = ds.Tables(0).Rows(cnt)("Mnth")
                        row1("A") = "ENDTRNS"
                        ds.Tables(0).Rows.InsertAt(row1, cnt)
                        destinationrowcount = destinationrowcount + 1
                        cnt = cnt + 1
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    End If
                    cnt = cnt + 1
                Loop
                row1 = ds.Tables(0).NewRow
                row1("A") = "ENDTRNS"
                ds.Tables(0).Rows.InsertAt(row1, ds.Tables(0).Rows.Count)

            End If
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("Ord")
                ds.Tables(0).Columns.Remove("Mnth")
                ds.Tables(0).Columns.Remove("BankID")
                ds.Tables(0).Columns.Remove("Yr")
                Filename = GVTDAIncome.Rows(gcnt).Cells(2).Text & ".iif"


                ' fp = File.CreateText(Filename)
                'fp.WriteLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                'fp.WriteLine("!ENDTRNS" & vbTab)
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        ' fp.WriteLine()
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            ' fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            'fp.Write(ds.Tables(0).Rows(i)(j))
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            'fp.Write(ds.Tables(0).Rows(i)(j) & vbTab)
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                ' fp.Close()
                'Download file
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                downfile(Filename, sb)
            End If
        ElseIf ddlCategory.SelectedValue = "5" Then
                'Expense Vouchers
                Filename = "VPS" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)
                For gcnt = 0 To gvExp.Rows.Count - 1
                    strSql = "select A,B,C,D,E,F,Sum(G),H,I,J,K from (select CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), E.Datepaid, 101)   as D ,N.AccNo as E,C.WebFolderName as F,-SUM(E.ExpenseAmount) as G,'' as H, 'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2) + REPLACE(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','') + Convert(varchar,E.CheckNumber) as I,CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as J,0 as K  From ExpJournal E Inner Join NSFAccounts N ON E.BankID=N.BankID and E.RestTypeFrom=N.RestrictionType and N.[Level]='L'  and N.InvIncType IN ('MMK','CASH') Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse I ON E.DonorType <> 'OWN' AND E.ReimbMemberid = I.AutoMemberID Inner Join Chapter C ON N.ChapterID = C.ChapterID WHERE E.CheckNumber = '" & gvExp.DataKeys(gcnt).Value & "' AND E.Datepaid='" & gvExp.Rows(gcnt).Cells(4).Text & "'" & " Group by E.Datepaid,E.CheckNumber,N.AccNo,E.BankID,I.FirstName,I.LastName,O.ORGANIZATION_NAME,E.DonorType,C.WebFolderName"
                    strSql = strSql & " UNION ALL"
                    strSql = strSql & " select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10), EJ.DatePaid, 101)  as D,CASE WHEN EJ.TransType = 'Transfers' THEN BS.Accno ELSE  EC.Account END as E,CASE WHEN EJ.ChapterID=1 and EJ.EventID is null and EJ.TransType = 'GenAdmin' THEN 'US_HomeAdmin' ELSE  CASE WHEN EJ.ChapterID=1 and EJ.EventID = 1 and EJ.Transtype='FinalsExp' THEN  'US_Finals' ELSE   Ch.webfoldername END END as F,EJ.ExpenseAmount as G,'' as H, 'VPS'+RIGHT('00'+ CONVERT(VARCHAR,EJ.BankID),2) + REPLACE(CONVERT(VARCHAR(10), EJ.DatePaid, 101),'/','') + Convert(varchar,EJ.CheckNumber) as I, CASE WHEN EJ.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as J,1 as K "
                    strSql = strSql & " FROM  ExpJournal EJ Left Join Bank B ON B.BankID=EJ.BankID Left Outer Join ExpenseCategory EC ON  EJ.ExpCatID=EC.ExpCatID  "
                strSql = strSql & " Left Outer Join NSFAccounts BS ON  BS.BankID=EJ.ToBankID and BS.RestrictionType=EJ.RestTypeFrom and EJ.TransType = 'Transfers' AND (BS.InvIncType IS NULL OR BS.InvIncType<>'Inv') Inner Join AcctgTransType ATT ON ATT.Code = EJ.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7) Inner Join Chapter Ch ON Ch.ChapterID = CASE WHEN  EJ.ToChapterID IS NULL THEN EJ.ChapterID ELSE EJ.ToChapterID END "
                    strSql = strSql & " Left Join OrganizationInfo O ON EJ.DonorType = 'OWN' and O.AutoMemberid =EJ.ReimbMemberid Left Join IndSpouse I ON EJ.DonorType <> 'OWN' AND EJ.ReimbMemberid = I.AutoMemberID "
                    strSql = strSql & " WHERE  EJ.CheckNumber = '" & gvExp.DataKeys(gcnt).Value & "' AND EJ.DatePaid='" & gvExp.Rows(gcnt).Cells(4).Text & "'"
                    strSql = strSql & " UNION ALL"
                    strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I,'' as J, 2 as K"
                    strSql = strSql & " ) TMP Group by A,B,C,D,E,F,H,I,J,K Order By K,E"
                    ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                    ds.Tables(0).Columns.Remove("K")

                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        If i > 0 Then
                            sb.AppendLine()
                        End If
                        For j = 0 To ds.Tables(0).Columns.Count - 1
                            If j = ds.Tables(0).Columns.Count - 1 Then
                                sb.Append(ds.Tables(0).Rows(i)(j))
                            Else
                                sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                            End If
                        Next
                    Next
                    sb.AppendLine()
                Next
                sb.AppendLine("ENDTRNS" & vbTab)
                'Download file
                downfile(Filename, sb)

        ElseIf ddlCategory.SelectedValue = "6" Then
            'ACT_EFT Donations
            Dim BankId As Integer
            Dim frmdate As Date
            Dim todate As DateTime
            Dim BankTransID As Integer

            Filename = "DGE" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            sb = New StringBuilder("")
            sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!ENDTRNS" & vbTab)
            'Try
            For gcnt = 0 To GVACT_EFTDon.Rows.Count - 1
                BankId = Convert.ToInt32(GVACT_EFTDon.Rows(gcnt).Cells(3).Text)
                frmdate = Convert.ToDateTime(GVACT_EFTDon.Rows(gcnt).Cells(7).Text)
                todate = Convert.ToDateTime(GVACT_EFTDon.Rows(gcnt).Cells(8).Text)
                BankTransID = Convert.ToInt32(GVACT_EFTDon.Rows(gcnt).Cells(15).Text)

                strSql = " Select A,B,C,D,E,F,Sum(G),H,I,J from "
                strSql = strSql & " (Select A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & GVACT_EFTDon.Rows(gcnt).Cells(5).Text & " 11:59:00 PM'), 101)  as D,"
                strSql = strSql & " Temp1.AccNo as E,Temp1.WebFolderName AS F,-ROUND(Sum(Temp1.Amount),2) as  G,'' as H, VoucherNo as I, 1 as J  from "
                strSql = strSql & " ((SELECT 'SPL' as A,B.BankID,B.BankTransID,N.AccNo, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,B.VendCust, N.Description ,N.DonorType,Ch.WebFolderName FROM BankTrans B "
                strSql = strSql & " LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat=C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID = 1"
                strSql = strSql & " LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat=H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID = 2"
                strSql = strSql & " Inner Join NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
                strSql = strSql & " Inner Join Chapter Ch ON N.ChapterId = Ch.ChapterID"
                strSql = strSql & " Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between '" & frmdate & "' and '" & todate & "' and B.BankID =" & BankId & " and Convert(Date,B.TransDate)  = CONVERT (Date,'" & GVACT_EFTDon.Rows(gcnt).Cells(5).Text & "')"
                strSql = strSql & " and B.BankTransID in(" & BankTransID & ") Group By N.AccNo ,B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate ,B.Vendcust,B.BankTransID,Ch.WebFolderName"
                strSql = strSql & " ) UNION ALL "
                strSql = strSql & " (SELECT  'TRNS' as A,B.BankId,B.BankTransID,N.AccNo,-SUM(B.Amount) as Amount,B.Transcat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,B.VendCust, N.Description ,N.DonorType,Ch.WebFolderName FROM BankTrans B  INNER JOIN NSFAccounts N ON  "
                strSql = strSql & "  B.BankID = N.BankID "
                strSql = strSql & " Inner Join Chapter Ch ON N.ChapterId =Ch.ChapterID"
                strSql = strSql & " Where N.RestrictionType = 'Unrestricted' and B.TransCat='Donation' and B.TransDate Between '" & frmdate & "' AND '" & todate & "'"
                strSql = strSql & " and B.BankID =" & BankId & " and Convert(Date,B.TransDate)  = CONVERT (Date,'" & GVACT_EFTDon.Rows(gcnt).Cells(5).Text & "')  and B.BankTransID in(" & BankTransID & ") Group by B.BankId,N.AccNo,B.Transcat,B.VendCust, N.Description ,N.DonorType,B.TransDate,B.BankTransID,Ch.WebFolderName"
                strSql = strSql & " ))Temp1 Group by Temp1.AccNo,Temp1.BankID ,Temp1.VoucherNo,Temp1.BankTransID,Temp1.A,Temp1.WebFolderName"
                strSql = strSql & " )T1 Group by E,F,I,A,B,C,D,G,H,J"
                strSql = strSql & "  UNION ALL "
                strSql = strSql & " Select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,0 as G,'' as H, '' as I, 2 as J"

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                ds.Tables(0).Columns.Remove("J")
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
            Next

            'Catch ex As Exception
            '    Response.Write(ex.ToString())
            'End Try
            sb.AppendLine("ENDTRNS" & vbTab)
            'Download file
            downfile(Filename, sb)

        ElseIf ddlCategory.SelectedValue = "7" Then
            'Vouchers-Bank Service Charges
            Dim BankId As Integer
            Dim frmdate As Date
            Dim todate As DateTime
            Dim VoucherNo As String
            Dim VendCust As String
            Dim ExpCatCode As String

            Filename = "VPE" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            sb = New StringBuilder("")
            sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
            sb.AppendLine("!ENDTRNS" & vbTab)
            For gcnt = 0 To GVBnk_CCFees.Rows.Count - 1
                BankId = Convert.ToInt32(GVBnk_CCFees.Rows(gcnt).Cells(2).Text)
                frmdate = Convert.ToDateTime(GVBnk_CCFees.Rows(gcnt).Cells(5).Text)
                todate = Convert.ToDateTime(GVBnk_CCFees.Rows(gcnt).Cells(5).Text)
                VoucherNo = GVBnk_CCFees.Rows(gcnt).Cells(4).Text
                VendCust = GVBnk_CCFees.Rows(gcnt).Cells(9).Text
                ExpCatCode = GVBnk_CCFees.Rows(gcnt).Cells(8).Text

                ''**
                strSql = "Select A,B,C,D,E,F,Sum(G),H,I,J from "
                strSql = strSql & "(Select A,'' as B,'GENERAL JOURNAL' as C,CONVERT(VARCHAR(10),Convert(DateTime,'" & frmdate & "'), 101)  as D,"
                strSql = strSql & " Temp1.AccNo as E,Temp1.WebFolderName AS F,-ROUND(Sum(Temp1.Amount),2) as  G,'' as H, VoucherNo as I,J  from "
                strSql = strSql & "(SELECT 'SPL' as A,B.BankID,B.TransDate,SUM(Amount) as Amount,N.AccNo,B.TransType, '" & VoucherNo & "' as VoucherNo, 1 as J,Ch.WebFolderName "
                'strSql = strSql & "'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
                strSql = strSql & " FROM BankTrans  B "
                strSql = strSql & " INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode "
                strSql = strSql & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo  "
                strSql = strSql & " INNER JOIN Chapter Ch ON Ch.ChapterID = N.ChapterID "
                strSql = strSql & " Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID = " & BankId & " and TransDate Between '" & frmdate & "' and '" & todate & "' "
                'AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "')"
                strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                'strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,Ch.WebFolderName,VoucherNo "
                strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

                strSql = strSql & " UNION ALL "
                strSql = strSql & " SELECT 'SPL' as A,B.BankID,B.TransDate,SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 1 as J ,Ch.WebFolderName "
                ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
                strSql = strSql & " FROM BankTrans B "
                strSql = strSql & " INNER JOIN ExpenseCategory  E ON B.TransCat= E.ExpCatCode "
                strSql = strSql & " INNER JOIN NSFAccounts N ON E.Account =N.AccNo "
                strSql = strSql & " INNER JOIN Chapter Ch ON Ch.ChapterID = N.ChapterID "
                strSql = strSql & " where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID =  " & BankId & " and TransDate Between '" & frmdate & "' and '" & todate & "'"
                ' AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "')"
                strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                'strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate ,Ch.WebFolderName,VoucherNo "
                strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

                strSql = strSql & " UNION ALL "
                strSql = strSql & " SELECT 'TRNS' as A,B.BankID,B.TransDate,-SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 0 as J ,Ch.WebFolderName "
                ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
                strSql = strSql & " FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' "
                strSql = strSql & " INNER JOIN Chapter Ch ON Ch.ChapterID = N.ChapterID "
                strSql = strSql & " Where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID =  " & BankId & " and TransDate Between '" & frmdate & "' and '" & todate & "' "
                'AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "')"
                strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                'strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,Ch.WebFolderName,VoucherNo "
                strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

                strSql = strSql & " UNION ALL "
                strSql = strSql & " SELECT 'TRNS' as A,B.BankID,B.TransDate,-SUM(Amount) as Amount,N.AccNo,B.TransType,'" & VoucherNo & "' as VoucherNo, 0 as J ,Ch.WebFolderName "
                ''VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ '_'+ RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2) as VoucherNo,B.Reason,B.AddInfo "
                strSql = strSql & " FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' "
                strSql = strSql & " INNER JOIN Chapter Ch ON Ch.ChapterID = N.ChapterID "
                strSql = strSql & " Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID =  " & BankId & " and TransDate Between '" & frmdate & "' and '" & todate & "' "
                'AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpCatCode & "')"
                strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                'strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,B.TransType,B.TransDate,Ch.WebFolderName ,VoucherNo"
                strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,B.TransType,B.TransDate,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebfolderName "

                strSql = strSql & " )Temp1 Group by Temp1.AccNo,Temp1.VoucherNo,Temp1.BankID ,A,J,Temp1.WebfolderName "
                strSql = strSql & " UNION ALL "
                strSql = strSql & " Select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,0 as G,'' as H, '' as I, 2 as J"
                strSql = strSql & " )T1 Group by E,F,I,A,B,C,D,G,H,J"
                'Response.Write(strSql & "<br />********************<br />")

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
                ds.Tables(0).Columns.Remove("J")
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
            Next
            sb.AppendLine("ENDTRNS" & vbTab)
            'Download file
            downfile(Filename, sb)

        ElseIf ddlCategory.SelectedValue = "8" Then
            Dim frmdate As Date = Convert.ToDateTime(GVTransfers.Rows(gcnt).Cells(8).Text)
            Dim todate As DateTime = Convert.ToDateTime(GVTransfers.Rows(gcnt).Cells(9).Text + " 23:59")
            Dim BankID As Integer = Convert.ToInt32(GVTransfers.Rows(gcnt).Cells(2).Text)

            strSql = " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,B.NetAmount as G,'' as H,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            strSql = strSql & " ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as I,1 as J,B.BankID as K ,B.Quantity as  L,B.Ticker as M"
            strSql = strSql & " FROM BrokTrans B "
            strSql = strSql & " Inner join BrokTrans b1 On B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and 
            strSql = strSql & " Inner Join NSFAccounts N On N.BankID=B.BankId" ' and B.RestType=N.RestrictionType "
            strSql = strSql & " and (N.RestrictionType in (Case when B.BankID = 4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end ))"
            strSql = strSql & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            strSql = strSql & " Inner Join Chapter Ch On N.ChapterId = Ch.ChapterID "
            strSql = strSql & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Medium ='IntTransfer' and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
            strSql = strSql & " UNION ALL "
            strSql = strSql & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-(B.NetAmount) as G,'' as H,"
            strSql = strSql & " 'IGT'+ RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+  ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as I,0 as J,B.BankID as K ,-(B.Quantity) as L ,B.Ticker as M"
            strSql = strSql & " FROM BrokTrans B "
            strSql = strSql & " Inner join BrokTrans b1 On B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and 
            strSql = strSql & " Inner Join NSFAccounts N On N.BankID=B1.BankId " 'and B1.RestType=N.RestrictionType 
            strSql = strSql & " and (N.RestrictionType in (Case when B1.BankID =4 and B.BankID <>5 then B.ToRestType Else Case when B.BankID = 4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'UnRestricted' End end end end end ))"
            strSql = strSql & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
            strSql = strSql & " Inner Join Chapter Ch On N.ChapterId = Ch.ChapterID "
            strSql = strSql & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Medium ='IntTransfer' and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
            'strSql = strSql & " UNION ALL "
            'strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J,'' as K,'' as L,'' as M"
            strSql = strSql & " Order by D,J"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)

            If ds.Tables(0).Rows.Count > 0 Then
                For k As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows(k)("G") = 0.0 Then
                        ds.Tables(0).Rows(k)("G") = Convert.ToDouble(ds.Tables(0).Rows(k)("L")) * GetAvgPrice(ds.Tables(0).Rows(k)("K"), ds.Tables(0).Rows(k)("M"), ds.Tables(0).Rows(k)("L"), ds.Tables(0).Rows(k)("D"), frmdate, todate)
                    End If
                Next
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                Dim cnt As Integer = 0
                Dim CurrVoucher As String = ""
                'Dim CurrTransDate As DateTime
                Dim row1 As DataRow
                Dim destinationrowcount As Integer = ds.Tables(0).Rows.Count - 1
                Do While cnt <= destinationrowcount
                    If CurrVoucher = "" Then
                        CurrVoucher = ds.Tables(0).Rows(cnt)("I")
                        'CurrTransDate = ds.Tables(0).Rows(cnt)("D")
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    ElseIf CurrVoucher <> ds.Tables(0).Rows(cnt)("I") Then 'Or CurrTransDate <> ds.Tables(0).Rows(cnt)("D") 
                        row1 = ds.Tables(0).NewRow
                        CurrVoucher = ds.Tables(0).Rows(cnt)("I")
                        'CurrTransDate = ds.Tables(0).Rows(cnt)("D")
                        row1("A") = "ENDTRNS"
                        ds.Tables(0).Rows.InsertAt(row1, cnt)
                        destinationrowcount = destinationrowcount + 1
                        cnt = cnt + 1
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    End If
                    cnt = cnt + 1
                Loop
                row1 = ds.Tables(0).NewRow
                row1("A") = "ENDTRNS"
                ds.Tables(0).Rows.InsertAt(row1, ds.Tables(0).Rows.Count)
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("J")
                ds.Tables(0).Columns.Remove("K")
                ds.Tables(0).Columns.Remove("L")
                ds.Tables(0).Columns.Remove("M")

                Filename = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "") & ".iif"
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next
                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                'Download file
                downfile(Filename, sb)
            End If
        ElseIf ddlCategory.SelectedValue = "9" Then
            Dim frmdate As Date = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(7).Text)
            Dim todate As DateTime = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(8).Text + " 23:59")
            Dim BankID As Integer = Convert.ToInt32(GVBuyTrans.Rows(gcnt).Cells(2).Text)

            strSql = "SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
            strSql = strSql & " Ch.WebFolderName as F,-Sum(B.NetAmount) as G,'' as H"
            strSql = strSql & " ,'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            strSql = strSql & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,1 as J FROM BrokTrans B "
            strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Investment-General Unrestricted'"
            strSql = strSql & " INNER JOIN Chapter Ch On N.ChapterId = Ch.ChapterID "
            strSql = strSql & " Where B.Transcat='Buy' and B.TransDate Between '" & frmdate & "' and '" & todate & "' "
            strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
            strSql = strSql & " UNION ALL"
            strSql = strSql & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
            strSql = strSql & " Ch.WebFolderName as F,Sum(B.NetAmount) as G,'' as H"
            strSql = strSql & " ,'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            strSql = strSql & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,0as J  FROM BrokTrans B "
            strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') and N.Description ='MMKT - General Unrestricted'"
            strSql = strSql & " INNER JOIN Chapter Ch On N.ChapterId = Ch.ChapterID "
            strSql = strSql & " Where B.Transcat='Buy' and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
            strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
            'strSql = strSql & " UNION ALL "
            'strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
            strSql = strSql & " Order by D,I,J"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)

            If ds.Tables(0).Rows.Count > 0 Then
                Dim cnt As Integer = 0
                Dim CurrVoucher As String = ""
                Dim row1 As DataRow
                Dim destinationrowcount As Integer = ds.Tables(0).Rows.Count - 1
                Do While cnt <= destinationrowcount
                    If CurrVoucher = "" Then
                        CurrVoucher = ds.Tables(0).Rows(cnt)("I")
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    ElseIf CurrVoucher <> ds.Tables(0).Rows(cnt)("I") Then 'Or CurrTransDate <> ds.Tables(0).Rows(cnt)("D") 
                        row1 = ds.Tables(0).NewRow
                        CurrVoucher = ds.Tables(0).Rows(cnt)("I")
                        row1("A") = "ENDTRNS"
                        ds.Tables(0).Rows.InsertAt(row1, cnt)
                        destinationrowcount = destinationrowcount + 1
                        cnt = cnt + 1
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    End If
                    cnt = cnt + 1
                Loop
                row1 = ds.Tables(0).NewRow
                row1("A") = "ENDTRNS"
                ds.Tables(0).Rows.InsertAt(row1, ds.Tables(0).Rows.Count)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("J")

                Filename = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "").Trim & ".iif"
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next

                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                'Download file
                downfile(Filename, sb)
            End If

        ElseIf ddlCategory.SelectedValue = "10" Then
            Dim frmdate As Date = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(7).Text)
            Dim todate As DateTime = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(8).Text + " 23:59")
            Dim BankID As Integer = Convert.ToInt32(GVBuyTrans.Rows(gcnt).Cells(2).Text)

            'strSql = "SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
            'strSql = strSql & " Ch.WebFolderName as F,-Sum(B.NetAmount) as G,'' as H"
            'strSql = strSql & " ,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            'strSql = strSql & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,1 as J FROM BrokTrans B "
            'strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and  N.Description ='Investment-DAF Temp. Restricted'" 'N.Description ='Investment-General Unrestricted'"
            'strSql = strSql & " INNER JOIN Chapter Ch On N.ChapterId = Ch.ChapterID "
            'strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & "' "
            'strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
            'strSql = strSql & " UNION ALL"
            'strSql = strSql & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E,"
            'strSql = strSql & " Ch.WebFolderName as F,Sum(B.NetAmount) as G,'' as H"
            'strSql = strSql & " ,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
            'strSql = strSql & " ISNULL(B.AssetClass,'')+'_'+ B.TransCat as I,0 as J  FROM BrokTrans B "
            'strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK')  and N.Description ='MMKT - DAF Temp. Restricted'" 'and N.Description ='MMKT - General Unrestricted'"
            'strSql = strSql & " INNER JOIN Chapter Ch On N.ChapterId = Ch.ChapterID "
            'strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & "'"
            'strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,Ch.WebFolderName"
            ''strSql = strSql & " UNION ALL "
            ''strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J"
            'strSql = strSql & " Order by D,I,J"

            '/************************************************************************/

            'strSql = strSql & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-SUM(Distinct B.Quantity*C.AvgPrice) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,0 as J FROM BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))  Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & " and B.TransDate Between '" & frmdate & "' and '" & todate & "' and N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end) Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and N.Description ='Investment-DAF Temp. Restricted'
            'strSql = strSql & " UNION ALL "
            'strSql = strSql & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,SUM(B.NetAmount) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,0 as J FROM BrokTrans B INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))  Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & "  and B.TransDate Between '" & frmdate & "' and '" & todate & "' and N.RestrictionType in (Case when B.BankID =  4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end) Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " ' N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') 'and N.Description ='MMKT - DAF Temp. Restricted'
            'strSql = strSql & " UNION ALL "
            'strSql = strSql & " Select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,SUM(Distinct B.Quantity*C.AvgPrice) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,1 as J from BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null) and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Securities Sales - Cost' Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.BankID =" & BankID & "  and B.TransDate Between '" & frmdate & "' and '" & todate & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and B.AssetClass not in ('Cash','MMK') 
            'strSql = strSql & " UNION ALL"
            'strSql = strSql & " Select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-SUM(Distinct B.NetAmount) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10),B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I  ,1 as J from BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null) and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Securities sales - Gross' Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell'and B.BankID =" & BankID & "   and B.TransDate Between '" & frmdate & "' and '" & todate & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and B.AssetClass not in ('Cash','MMK') 
            'strSql = strSql & " UNION ALL "
            'strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J "
            'strSql = strSql & " Order by J,G"

            strSql = strSql & " SELECT CASE WHEN(ROW_NUMBER() OVER(ORDER BY N.AccNo)) = 1 then 'TRNS' ELSE 'SPL' END as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-SUM(Distinct B.Quantity*C.AvgPrice) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,0 as J FROM BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares  and N.RestrictionType in (Case when B.BankID =  4 then 'Unrestricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end) and B.TransDate Between '" & frmdate & "' and '" & todate & "' Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and N.Description ='Investment-DAF Temp. Restricted' ''And C.CostBasis = B.CostBasis
            strSql = strSql & " UNION ALL "
            strSql = strSql & " SELECT 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,SUM(B.NetAmount) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,0 as J FROM BrokTrans B INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and  N.InvIncType in ('MMK') and B.AssetClass not in ('Cash','MMK') Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell'  and N.RestrictionType in (Case when B.BankID =  4 then 'Unrestricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end) and B.TransDate Between '" & frmdate & "' and '" & todate & "'  Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'and N.Description ='MMKT - DAF Temp. Restricted' 'N.InvIncType in ('MMK','Inv') '' and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and B.AssetClass not in ('Cash','MMK')
            strSql = strSql & " UNION ALL "
            strSql = strSql & " Select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,SUM(Distinct B.Quantity*C.AvgPrice) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I,1 as J from BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null)and B.AssetClass not in ('Cash','MMK') and N.Description = Case when B.BankID <> 7 then 'Securities Sales - Cost'  Else 'Securities sales - Cost - Temp Rest' End Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares and B.TransDate Between '" & frmdate & "' and '" & todate & "'  Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'And C.CostBasis = B.CostBasis ' 'Securities Sales - Cost' 
            strSql = strSql & " UNION ALL"
            strSql = strSql & " Select 'SPL' as A,'' as B,'GENERAL JOURNAL' as C, B.TransDate as D,N.AccNo As E, Ch.WebFolderName as F,-SUM(Distinct B.NetAmount) as G,'' as H,'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10),B.TransDate, 101),'/','')+ IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as I  ,1 as J from BrokTrans B Inner join CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null)and B.AssetClass not in ('Cash','MMK') and N.Description =Case when B.BankID <> 7 then 'Securities Sales - Gross'  Else 'Securities Sales - Gross - Temp Rest' End Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID Where B.Transcat='Sell' and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares and B.TransDate Between '" & frmdate & "' and '" & todate & "'  Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,Ch.WebFolderName " 'And C.CostBasis = B.CostBasis ''Securities sales - Gross'
            strSql = strSql & " UNION ALL "
            strSql = strSql & " select 'ENDTRNS' as A,'' as B,'' as C,'' as D ,' ' as E,'' as F,' ' as G,'' as H, '' as I, 2 as J "
            strSql = strSql & " Order by J,G"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim cnt As Integer = 0
                Dim CurrVoucher As String = ""
                Dim row1 As DataRow
                Dim destinationrowcount As Integer = ds.Tables(0).Rows.Count - 1
                Do While cnt <= destinationrowcount
                    If CurrVoucher = "" Then
                        CurrVoucher = ds.Tables(0).Rows(cnt)("I")
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    ElseIf CurrVoucher <> ds.Tables(0).Rows(cnt)("I") Then 'Or CurrTransDate <> ds.Tables(0).Rows(cnt)("D") 
                        row1 = ds.Tables(0).NewRow
                        CurrVoucher = ds.Tables(0).Rows(cnt)("I")
                        row1("A") = "ENDTRNS"
                        ds.Tables(0).Rows.InsertAt(row1, cnt)
                        destinationrowcount = destinationrowcount + 1
                        cnt = cnt + 1
                        ds.Tables(0).Rows(cnt)("A") = "TRNS"
                    End If
                    cnt = cnt + 1
                Loop
                row1 = ds.Tables(0).NewRow
                row1("A") = "ENDTRNS"
                ds.Tables(0).Rows.InsertAt(row1, ds.Tables(0).Rows.Count)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                lblErr.Text = ""
                ds.Tables(0).Columns.Remove("J")

                Filename = ds.Tables(0).Rows(0)("I").ToString().Replace("/", "").Trim & ".iif"
                sb = New StringBuilder("")
                sb.AppendLine("!TRNS" & vbTab & "TRNSID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!SPL" & vbTab & "SPLID" & vbTab & "TRNSTYPE" & vbTab & "DATE" & vbTab & "ACCNT" & vbTab & "CLASS" & vbTab & "AMOUNT" & vbTab & "DOCNUM" & vbTab & "MEMO" & vbTab & "NAME")
                sb.AppendLine("!ENDTRNS" & vbTab)

                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If i > 0 Then
                        sb.AppendLine()
                    End If
                    For j = 0 To ds.Tables(0).Columns.Count - 1
                        If ds.Tables(0).Rows(i)(j).ToString().Trim() = "ENDTRNS" Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                            Exit For
                        End If
                        If j = ds.Tables(0).Columns.Count - 1 Then
                            sb.Append(ds.Tables(0).Rows(i)(j))
                        Else
                            sb.Append(ds.Tables(0).Rows(i)(j) & vbTab)
                        End If
                    Next

                Next
                sb.AppendLine()
                sb.AppendLine("ENDTRNS" & vbTab)
                'Download file
                downfile(Filename, sb)
            End If

        End If
    End Sub

    Protected Sub btnAddUpdateGL_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter to date in mm/dd/yyyy format"
            Exit Sub
        ElseIf ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        End If

        lblErr.Text = ""
        Dim VoucherNo As String
        Dim VoucherNumbers As String = ""
        Dim gcnt As Integer
        Dim InsertFlag As Boolean = False

        strSql = ""
        Session("SqlInsert") = ""

        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Status From FiscalYear where StartDate>='" & TxtFrom.Text & "' and EndDate<='" & txtTo.Text & "'") = "C" Then
            lblErr.Text = "Reports are closed for the Year " & (Convert.ToDateTime(TxtFrom.Text).Year) & "_" & (Convert.ToDateTime(TxtFrom.Text).Year) + 1
            Exit Sub
        End If
        Dim Frm_Date As DateTime = TxtFrom.Text
        Dim To_Date As DateTime = txtTo.Text

        If Frm_Date.Day <> 1 Then
            lblErr.Text = "From date should be the beginning of the month."
            Exit Sub
        ElseIf To_Date.Day <> DateTime.DaysInMonth(To_Date.Year, To_Date.Month) Then
            lblErr.Text = "To date should be the end of the month."
            Exit Sub
        End If

        DisplayRecords()
        If lblErr.Text.Trim.Length > 0 Then
            Exit Sub
        End If

        'Validate whether all months has data
        If hfConfirmDespGap.Value.ToLower = "false" Then
            Dim BeginDate As DateTime = Convert.ToDateTime(TxtFrom.Text)
            Dim EndDate As DateTime = Convert.ToDateTime(txtTo.Text)
            Dim startWith As String, endWith As String
            Dim iDays As Integer
            Dim MissingDateMsg As String = ""
            While BeginDate <= EndDate
                iDays = Date.DaysInMonth(BeginDate.Year, BeginDate.Month)
                startWith = BeginDate.ToShortDateString
                endWith = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month) - 1)
                If HasRecords(startWith, endWith) = False Then
                    If Len(MissingDateMsg) > 0 Then
                        MissingDateMsg = MissingDateMsg & ","
                    End If
                    MissingDateMsg = MissingDateMsg & startWith.ToString

                End If
                BeginDate = BeginDate.AddDays(Date.DaysInMonth(BeginDate.Year, BeginDate.Month))
            End While
            If Len(MissingDateMsg) > 0 Then
                ' alert msg
                ' ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('" & MissingDateMsg & "');", True)
                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfirmToSaveMissingData('" & MissingDateMsg.ToString & "');", True)
                Exit Sub
            End If
        End If
        ' validate whether duplication in GL
        If hfOverWrite.Value.ToLower = "false" Then
            If HasDuplicatesGL(TxtFrom.Text, txtTo.Text) = True Then
                ' alert msg
                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfirmToOverWrite();", True)
                Exit Sub
            End If
        End If

        Dim stDtFormat As String, endDtFormat As String, stTransDate, endTransDate As String
        stDtFormat = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(D.DepositDate)-1),D.DepositDate),101) "
        endDtFormat = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,D.DepositDate))),DATEADD(mm,1,D.DepositDate)),101) "
        hfConfirmDespGap.Value = "false"
        hfOverWrite.Value = "false"
        If ddlCategory.SelectedValue = "1" Then
            'Deposits
            Try
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='DGN' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59'"

                For gcnt = 0 To gvDonation.Rows.Count - 1
                    VoucherNo = gvDonation.Rows(gcnt).Cells(4).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, BankDate,TransType, Account, Description, Class, Amount, BankID, DepositSlip, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,ChapterID,RestType,Transcat,Name)  ("
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, D.DepositDate,D.DepositDate,'DGN' as TransType,Case when D.DonationType ='Temp Restricted' and D.Project ='DAF' Then 10108 Else N.AccNo End as AccNo,N.Description,C.WebFolderName as Class,SUM(D.AMOUNT),D.BankID,D.DepositSlip,Null as PayeeID ,Null as PayeeType," & stDtFormat & " as StartDate," & endDtFormat & " as EndDate,Getdate()," & Session("LoginID") & ",'A',N.ChapterID,D.DonationType,'Donation' as TransCat,NULL as Name From DonationsInfo D Inner Join NSFAccounts N ON D.BankID=N.BankID and D.DonationType=N.RestrictionType AND (N.InvIncType IS NULL OR N.InvIncType<>'Inv') INNER JOIN Chapter C ON N.ChapterID=C.ChapterID  WHERE D.DepositSlip = " & gvDonation.DataKeys(gcnt).Value & " AND D.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'" & ""
                    strSql = strSql & " and ((D.DonationType ='Temp Restricted' and D.Project ='DAF'  and D.BankID<>1) Or (N.accNo  <>10108 and D.BankID=1  ))"
                    strSql = strSql & " Group by D.DepositDate,D.DepositSlip,N.AccNo,D.BankID,N.Description,N.ChapterID,D.DonationType,C.WebFolderName,D.DonationType,D.Project Union All "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, D.DepositDate,D.DepositDate,'DGN' as TransType, Acc.AccNo,Acc.Description, Ch.webfoldername as Class,-D.AMOUNT,D.BankID,D.DepositSlip,D.MEMBERID as PayeeID ,D.DonorType as PayeeType," & stDtFormat & " as StartDate," & endDtFormat & " as EndDate,Getdate()," & Session("LoginID") & ",'I',D.ChapterID,D.DonationType"
                    strSql = strSql & ",'Donation' as TransCat,CASE WHEN D.DonorType = 'OWN' then O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as Name"
                    strSql = strSql & " FROM DonationsInfo D  Inner Join NSFAccounts Acc ON Acc.DonorType='IND/SPOUSE' AND D.DonationType=Acc.RestrictionType AND (Acc.InvIncType IS NULL OR Acc.InvIncType<>'Inv') Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID "
                    strSql = strSql & " Left Join OrganizationInfo O ON D.DonorType = 'OWN' and O.AutoMemberid = D.DonationID Left Join IndSpouse I ON D.DonorType <> 'OWN' AND D.MemberID = I.AutoMemberID "
                    strSql = strSql & " WHERE D.DonorType <>'OWN' AND D.DepositSlip=" & gvDonation.DataKeys(gcnt).Value & " AND D.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, D.DepositDate,D.DepositDate,'DGN' as TransType, Acc.AccNo,Acc.Description, Ch.webfoldername as Class,-D.AMOUNT,D.BankID,D.DepositSlip,D.MEMBERID as PayeeID ,D.DonorType as PayeeType," & stDtFormat & " as StartDate," & endDtFormat & " as EndDate,Getdate()," & Session("LoginID") & ",'I',D.ChapterID,D.DonationType "
                    strSql = strSql & " ,'Donation' as TransCat,CASE WHEN D.DonorType = 'OWN' then O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as Name"
                    strSql = strSql & " FROM DonationsInfo D "
                    strSql = strSql & " Inner Join NSFAccounts Acc ON D.DonorType = Acc.DonorType AND D.DonationType=Acc.RestrictionType AND (Acc.InvIncType IS NULL OR Acc.InvIncType<>'Inv')"
                    strSql = strSql & " Inner Join Chapter Ch ON D.ChapterID=Ch.ChapterID Inner Join OrganizationInfo O ON D.MemberID = O.Automemberid AND D.DonorType = 'OWN' AND Acc.BusinessType = CASE WHEN O.IRScat in ('Non-proft,501(c)(3)','Non-proft,PAC', 'Non-proft,Other') then 'Non-Profit' Else 'Profit' END "
                    strSql = strSql & " Left Join IndSpouse I ON D.DonorType <> 'OWN' AND  D.MemberID= I.AutoMemberID "
                    strSql = strSql & " WHERE D.DepositSlip=" & gvDonation.DataKeys(gcnt).Value & " AND D.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "')"
                Next
                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString()
            End Try
            'downfile(VoucherNo, sb)
        ElseIf ddlCategory.SelectedValue = "2" Then
            stDtFormat = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(OD.DepositDate)-1),OD.DepositDate),101) "
            endDtFormat = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,OD.DepositDate))),DATEADD(mm,1,OD.DepositDate)),101) "
            'Other Deposits (OTH)
            strSql = ""
            Try
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='OTH' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59'"

                For gcnt = 0 To gvDonation.Rows.Count - 1
                    VoucherNo = gvDonation.Rows(gcnt).Cells(4).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, BankDate,TransType, Account, Description, Class, Amount, BankID, DepositSlip, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,ChapterID,RestType) "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, OD.DepositDate,OD.DepositDate,'OTH' as TransType, N.AccNo,N.Description,C.WebFolderName as Class,ROUND(Sum(OD.Amount),2) as AMOUNT,OD.BankID," & gvDonation.DataKeys(gcnt).Value & " as DepositSlip,Null as PayeeID , null as PayeeType," & stDtFormat & " as StartDate," & endDtFormat & " as EndDate,Getdate()," & Session("LoginID") & ",'A',N.ChapterID,OD.RestrictionType From OtherDeposits OD Inner Join NSFAccounts N ON OD.BankID=N.BankID and OD.RestrictionType=N.RestrictionType and  N.[Level]='L' INNER JOIN Chapter C ON N.ChapterID=C.ChapterID WHERE OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'" & " Group by OD.DepositDate,OD.DepositSlipNo,N.AccNo,OD.BankID,N.Description,N.ChapterID,OD.RestrictionType,C.WebFolderName Union All "

                    stDtFormat = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(DepositDate)-1),DepositDate),101) "
                    endDtFormat = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,DepositDate))),DATEADD(mm,1,DepositDate)),101) "

                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, T.DepositDate,T.DepositDate,'OTH' as TransType, T.AccNo,N.Description, T.webfoldername as Class,ROUND(-Sum(T.Amount),2) as AMOUNT,T.BankID," & gvDonation.DataKeys(gcnt).Value & " as DepositSlip,Null as PayeeID , null as PayeeType," & stDtFormat & " as StartDate," & endDtFormat & " as EndDate,Getdate()," & Session("LoginID") & ",'I',T.ChapterID,T.RestrictionType  FROM ("
                    strSql = strSql & " select OD.RevenueType,OD.BankID, SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =1 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51100  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID ,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =2 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51200  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =3 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All "

                    'Coaching
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =13 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All "

                    'PrepClub Coaching added on 24-02-2014
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 1/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51150  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =19 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All"
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) * 2/3  as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,41105  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.EventID =19 AND OD.RevenueType = 'Fees' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType Union All "

                    '*************** Added on 09-08-2014 as Mr.Ramdev wanted to use a separate account 51450 for food sales*****************'
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51450  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.SalesID = 1 AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType UNION ALL "
                    '********************************************************************************************************************************************'
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,51400  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sales' AND OD.SalesID <> 1 AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType UNION ALL "

                    '/***************Added on Dec 10 2014 to inlcude Refund in Other Deposits*********************************'
                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,Ex.Account  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD  inner join ExpenseCategory EX on Ex.ExpCatID =OD.RefundId Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Refund' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType ,Ex.Account UNION ALL "
                    '/**************************************************************************************************************************'

                    strSql = strSql & " select OD.RevenueType,OD.BankID,SUM(OD.AMOUNT) as  AMOUNT,CONVERT(VARCHAR(10), OD.DepositDate, 101)  as DepositDate ,CASE WHEN OD.EventID=1 or OD.EventID=2 THEN 51100 ELSE CASE WHEN OD.EventID=3 THEN 51200 ELSE 51300 END END  as AccNo,Ch.webfoldername,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,OD.BankID),2) + REPLACE(CONVERT(VARCHAR(10), OD.DepositDate, 101),'/','') + Convert(varchar,OD.DepositSlipNo) as VoucherNo,Ch.State,Ch.Name,OD.ChapterID,OD.RestrictionType FROM OtherDeposits OD   Inner Join Chapter Ch ON OD.ChapterID=Ch.ChapterID  WHERE OD.RevenueType = 'Sponsorship' AND OD.DepositSlipNo = " & gvDonation.DataKeys(gcnt).Value & " AND OD.DepositDate='" & gvDonation.Rows(gcnt).Cells(5).Text & "'"
                    strSql = strSql & " group by OD.RevenueType,OD.BankID,OD.DepositSlipNo,OD.BankID,Ch.webfoldername,Ch.State,Ch.Name,OD.DepositDate,OD.EventID,OD.ChapterID,OD.RestrictionType)"
                    strSql = strSql & " T Left Join Bank B ON B.BankID=T.BankID inner Join NSFAccounts N ON T.AccNo = N.AccNo Group by T.RevenueType,T.DepositDate,T.VoucherNo,T.AccNo,N.Description,T.WebFolderName,T.State,T.BankID,T.Name,N.RestrictionType,N.BusinessType,B.BankCode,T.ChapterID,T.RestrictionType "
                Next
                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString()
            End Try
        ElseIf ddlCategory.SelectedValue = "3" Then
            'CREDIT CARDS 'CGN

            Try
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='CGN' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59'"

                For gcnt = 0 To GVCCRevenues.Rows.Count - 1
                    Dim frmdate As Date = Convert.ToDateTime(GVCCRevenues.Rows(gcnt).Cells(3).Text)
                    Dim todate As DateTime = Convert.ToDateTime(GVCCRevenues.Rows(gcnt).Cells(4).Text + " 23:59")
                    VoucherNo = GVCCRevenues.Rows(gcnt).Cells(2).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, DepositSlip, PayeeID, PayeeType, StartDate, Enddate,BankDate,CreateDate, CreatedBy,AccountType,ChapterID,RestType) "
                    strSql = strSql & " SELECT CompanyNumber,VoucherNo,DepositDate,TransType,AccNo,Description,Class,SUM(AMOUNT)as Amount,BankID, DepositSlip,PayeeID ,PayeeType,StartDate,EndDate,BankDate, CreatedDate, CreatedBy,AccType,ChapterID,RestType From ("
                    strSql = strSql & " SELECT 1 as CompanyNumber,'" & VoucherNo & "' as VoucherNo,'" & todate & "' as DepositDate,'CGN' as TransType, N.AccNo,N.Description,C.WebFolderName as Class,ROUND(Sum(NFG.TotalPayment),2) as AMOUNT,1 as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate() as CreatedDate," & Session("LoginID") & "as CreatedBy,'A' as AccType,N.ChapterID,N.RestrictionType as RestType FROM NFG_Transactions NFG Inner Join NSFAccounts N ON N.BankID=1 and N.RestrictionType='Unrestricted' Inner Join Chapter C ON N.ChapterID = C.ChapterID where NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & " ' Group By N.AccNo,N.Description,N.ChapterID,C.WebFolderName,N.AccType,N.RestrictionType having SUM(NFG.[TotalPayment]) > 0 or SUM(NFG.[TotalPayment]) < 0 UNION ALL"
                    strSql = strSql & " SELECT 1 as CompanyNumber,'" & VoucherNo & "' as VoucherNo,'" & todate & "' as DepositDate,'CGN' as TransType, T.AccNo,N.Description, T.webfoldername as Class,ROUND(-Sum(T.Amount),2) as AMOUNT,1 as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate() as CreatedDate," & Session("LoginID") & "CreatedBy,'I' as AccType,T.ChapterID,'Unrestricted' as RestType FROM ("
                    ' ''Contestant
                    strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID   Union All"
                    strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID "
                    strSql = strSql & " FROM Contestant Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID   Union All"
                    '' ''DuplicateContestantReg
                    strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    strSql = strSql & " SELECT (SUM(con.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM DuplicateContestantReg Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=2 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    'Registration
                    strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM Registration R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=3 and NFG.MatchedStatus='Y' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"

                    'Finals and NFG.MatchedStatus='O'  NFG.MatchedStatus='O' and 
                    strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name,1 as ChapterID"
                    strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=1 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name,C.ChapterID having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                    strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,6 as OrderNo,'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name,1 as ChapterID"
                    strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=1 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name,C.ChapterID having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

                    '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=13 and Amount >0 are summed. ChapterID should be 112 for Coaching
                    'Coaching - similar to Finals  and NFG.MatchedStatus='O' NFG.MatchedStatus='O' and 
                    strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Coaching' as WebFolderName, 'US' as State,'Coaching' as Name,112 as ChapterID"
                    strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=13 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name,C.ChapterID,NFG.Fee,NFG.asp_session_id,NFG.LateFee,NFG.TotalPayment having (SUM(NFG.Fee)>0 or SUM(NFG.Fee)<0) Union All"
                    strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Coaching' as WebFolderName, 'US' as State,'Coaching' as Name,112 as ChapterID"
                    strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=13 and NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name,C.ChapterID,NFG.Fee,NFG.asp_session_id,NFG.LateFee,NFG.TotalPayment having (SUM(NFG.Fee)>0 or SUM(NFG.Fee)<0) Union All"

                    '***************Added on 07/22/2014 as per new req. If EventID=19 [PrepClub], NFG_Transactions table is used and the records with EventID=19 and Amount >0 are summed by ChapterID.
                    'Prepclub
                    strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM Registration_PrepClub R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=19 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    strSql = strSql & " SELECT (SUM(R.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM Registration_PrepClub R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null and NFG.TotalPayment > 0 and NFG.EventId=19 and NFG.MatchedStatus='O' and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"

                    '*********************Added/Updated on 07/22/2014 **********As per new req. NFG_Transactions table is used and the records with EventID=4 and Amount >0 are summed. ChapterID should be 117 for Game
                    'Game - similar to Finals and NFG.MatchedStatus='O' NFG.MatchedStatus='O' and 
                    strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,5 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name,117 as ChapterID"
                    strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=4 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name,C.ChapterID having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"
                    strSql = strSql & " SELECT (SUM(NFG.Fee)+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,6 as OrderNo,'US_Game' as WebFolderName, 'US' as State,'Game' as Name,117 as ChapterID"
                    strSql = strSql & " FROM NFG_Transactions NFG  Inner Join Chapter C ON NFG.ChapterID = C.ChapterID where NFG.EventId=4 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName,C.State,C.Name,C.ChapterID having (SUM(NFG.Fee)>0  or SUM(NFG.Fee)<0) Union All"

                    ''NFG_Supp
                    ''NFG_Supp--Contestant
                    strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name,C.ChapterID   Union All"
                    strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51100 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=2 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name,C.ChapterID   Union All"
                    'NFG_Supp----Registration
                    strSql = strSql & " SELECT (SUM(IsNull(R.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount, 41105 as AccNo,3 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID  where  NFG.Fee is not null and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    strSql = strSql & " SELECT (SUM(IsNull(R.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51200 as AccNo,4 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM NFG_Supp R Inner Join  NFG_Transactions NFG On NFG.asp_session_id = R.PaymentReference  Inner Join Chapter C On C.ChapterID = R.ChapterID where  NFG.Fee is not null  and NFG.EventId=3 and (NFG.MatchedStatus = R.MatchedStatus Or (R.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    '*************************************************************************************************************************************************************************************************************'
                    'NFG_Supp----PrepClub_Registration Added on 08-19-2014 for PrepCLub Refund Records
                    strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*2/3 as Amount,41105 as AccNo,1 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID   where NFG.Fee is not null  and NFG.EventId=19 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"
                    strSql = strSql & " SELECT (SUM(IsNull(con.Amount_pars,0))+ SUM(NFG.LateFee))*1/3 as Amount, 51150 as AccNo,2 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID"
                    strSql = strSql & " FROM NFG_Supp Con Inner Join  NFG_Transactions NFG On NFG.asp_session_id = Con.PaymentReference Inner Join Chapter C On con.ChapterID = C.ChapterID  where  NFG.Fee is not null  and NFG.EventId=19 and (NFG.MatchedStatus = Con.MatchedStatus Or (Con.MatchedStatus is null and NFG.MatchedStatus is null)) and (NFG.Fee>0  or NFG.Fee<0) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' Group by C.WebFolderName, C.State,C.Name,C.ChapterID  Union All"

                    '/**********Meals Amount**********Replacing  51100 with 51450 on 07-10-2013*****************************************************************/
                    strSql = strSql & " SELECT SUM(NFG.MealsAmount) as Amount,51450 as AccNo,7 as OrderNo, 'US_Finals' as WebFolderName, 'US' as State,'Finals' as Name,1 as ChapterID"  ' C.WebFolderName, C.State,C.Name"
                    strSql = strSql & " FROM NFG_Transactions NFG where (NFG.MealsAmount>0 or NFG.MealsAmount<0) and NFG.MealsAmount is not Null and NFG.EventId=1 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "' having (SUM(NFG.MealsAmount)>0  or SUM(NFG.MealsAmount)<0) Union All"  '  Inner Join Chapter C On NFG.ChapterID = C.ChapterID  Group by C.WebFolderName, C.State,C.Name

                    'Donations
                    strSql = strSql & " SELECT SUM(NFG.[Contribution Amount])as Amount, 41101 as AccNo,8 as OrderNo,  ISNULL(C.WebFolderName,'US_HomeOffice') as WebFolderName, ISNULL(C.State,'IL') as State,ISNULL(C.Name,'Home Office') as Name,ISNULL(C.ChapterID,1) as ChapterID  "
                    strSql = strSql & " FROM NFG_Transactions NFG Left Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[Contribution Amount] > 0 or NFG.[Contribution Amount] < 0) and NFG.EventID not in (10) and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name,C.ChapterID Union All"
                    strSql = strSql & " SELECT SUM(NFG.[TotalPayment])as Amount, 51400 as AccNo,9 as OrderNo,  C.WebFolderName, C.State,C.Name,C.ChapterID "
                    strSql = strSql & " FROM NFG_Transactions NFG Inner Join Chapter C On NFG.ChapterID = C.ChapterID    where  (NFG.[TotalPayment] > 0 or NFG.[TotalPayment] < 0) and NFG.EventID=10 and  NFG.MS_TransDate Between '" & frmdate & "' AND '" & todate & "'  Group by C.WebFolderName, C.State,C.Name,C.ChapterID "
                    strSql = strSql & " ) T inner Join  NSFAccounts N ON T.AccNo = N.AccNo "
                    strSql = strSql & " Group by T.AccNo,N.Description,T.WebFolderName,T.State,T.Name,T.ChapterID"

                    ''Added To Merge CCRevenue Adjustments ''05-11-2012
                    ''CC Revenue Adjustments are to make the NFG_transactions equal to BankTrans numbers for the same period

                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT 1 as CompanyNumber,'" & VoucherNo & "' as VoucherNo,'" & todate & "' as DepositDate,'CGN' as TransType, AccNo,Description, Temp1.Class as Class,ROUND(-Sum(Temp1.Amount),2) as AMOUNT,1 as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate() as CreatedDate,4240 as CreatedBy,Temp1.AccType,Temp1.ChapterID,'Unrestricted' as RestType FROM"
                    strSql = strSql & "(select Temp.AccNo as AccNo,Temp.Description,Temp.F as Class,ROUND(Sum(Temp.Amount),2) as Amount,Temp.ChapterID,Temp.AccType from "
                    strSql = strSql & "(Select (SUM(CreditCC)-SUM(Registrations))*2/3 as Amount,41105 as AccNo,'Donation Fees - Unrestricted' as Description, 'US_HomeOffice' as F ,109 As ChapterID,'I' as AccType from"
                    strSql = strSql & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                    strSql = strSql & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
                    strSql = strSql & " )T1 Group by TransDate"
                    strSql = strSql & " UNION ALL "

                    '***********Added on 25-02-2014 to show transactions are not in Contests['Program Service Fees - Contests'] for sep,oct,nov ***********'
                    strSql = strSql & " Select (SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51150 as AccNo,'Program Service Fees - Coaching' as Description, 'US_HomeOffice' as F, 109 As ChapterID,'I' as AccType from"
                    strSql = strSql & " (select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                    strSql = strSql & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
                    strSql = strSql & " ) T21 where Month(T21.TransDate) in (9,10,11) Group by TransDate "
                    strSql = strSql & " UNION ALL "

                    '****************Updated on on 25-02-2014 **************************************************************'
                    strSql = strSql & " Select (SUM(CreditCC)-SUM(Registrations))*1/3 as Amount,51100 as AccNo,'Program Service Fees - Contests' as Description, 'US_HomeOffice' as F, 109 As ChapterID,'I' as AccType from"
                    strSql = strSql & " (select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                    strSql = strSql & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
                    strSql = strSql & " ) T22 where Month(T22.TransDate) not in (9,10,11) Group by TransDate "
                    '**********************'

                    strSql = strSql & " UNION ALL"
                    strSql = strSql & " Select -(SUM(CreditCC)-SUM(Registrations)) as Amount,10101 as AccNo,'Chase Bank - Unrestricted' as Description,'US_HomeOffice' as F ,109 As ChapterID,'A' as AccType from"
                    strSql = strSql & "(select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
                    strSql = strSql & "select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & frmdate & "' AND '" & todate & "' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))  "
                    strSql = strSql & " )T3 Group by TransDate)"
                    strSql = strSql & " Temp Group by Temp.AccNo,F,Description,Temp.ChapterID,Temp.AccType) Temp1 Group by Temp1.AccNo,Temp1.Class,Temp1.Description,Temp1.ChapterID,Temp1.Acctype "
                    strSql = strSql & " ) CC Group by AccNo,Class,Description,TransType,VoucherNo,AccType,BankID,CompanyNumber"
                    strSql = strSql & ",DepositDate, DepositSlip,PayeeID ,PayeeType,StartDate,EndDate,BankDate, CreatedDate, CreatedBy,ChapterID,RestType"
                Next
                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString())
                lblErr.Text = "Error in Inserting" & ex.ToString()
            End Try
        ElseIf ddlCategory.SelectedValue = "4" Then
            'IGN-Cash Receipts 
            Dim bankID As Integer
            Dim frmdate As Date
            Dim todate As DateTime
            Try
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='IGN' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59'"

                For gcnt = 0 To GVTDAIncome.Rows.Count - 1
                    VoucherNo = GVTDAIncome.Rows(gcnt).Cells(2).Text.Trim
                    bankID = Convert.ToInt32(GVTDAIncome.Rows(gcnt).Cells(6).Text)
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    frmdate = Convert.ToDateTime(GVTDAIncome.Rows(gcnt).Cells(3).Text)
                    todate = Convert.ToDateTime(GVTDAIncome.Rows(gcnt).Cells(4).Text)
                    'stDtFormat = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(" & frmdate & ")-1)," & frmdate & "),101) "
                    '  endDtFormat = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1," & todate & "))),DATEADD(mm,1," & todate & ")),101) "

                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, DepositSlip, PayeeID, PayeeType, StartDate, Enddate,BankDate,CreateDate, CreatedBy,AccountType,Ticker,ChapterID,RestType,Transcat) "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,'" & todate & "' as DepositDate,'IGN' as TransType, Acc.AccNo as AccNo, Acc.Description,C.WebfolderName as Class,-(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) AS Amount, RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate()," & Session("LoginID") & ",Acc.AccType,B.Ticker,Acc.ChapterID,Acc.RestrictionType,B.TransCat FROM BrokTrans B " '
                    strSql = strSql & " Inner Join NSFAccounts Acc On ((Acc.InvIncType ='Dividend' AND B.TransCat = 'OrdDiv') OR (Acc.InvIncType ='DivCG' AND B.TransCat IN ('STCGDiv','LTCGDiv')) OR (Acc.InvIncType ='Interest' AND B.TransCat = 'Div')) AND ((Acc.RestrictionType ='Unrestricted' AND B.BankID IN (4,5,6)) OR (Acc.RestrictionType ='Temp Restricted' AND B.BankID =7)) "
                    strSql = strSql & " Inner Join Chapter C ON Acc.ChapterID=C.ChapterID"
                    strSql = strSql & " WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) "
                    strSql = strSql & " AND B.TransDate Between  '" & frmdate & "' AND '" & todate & "' AND B.BankID=" & bankID & " Group By  B.Ticker,B.BankID,Acc.AccNo,Acc.Description,Acc.AccType,Acc.ChapterID,Acc.RestrictionType,B.TransCat,C.WebfolderName, B.NetAmount , B.Quantity"
                    strSql = strSql & " Union All "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,'" & todate & "' as DepositDate,'IGN' as TransType, Acc.AccNo as AccNo, Acc.Description,C.WebfolderName as Class,(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) AS Amount, RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate()," & Session("LoginID") & ",Acc.AccType,Case WHEN B.BankID = 4 Then 'ISCH' ELSE Case WHEN B.BankID = 5 Then 'IGEN' ELSE Case WHEN B.BankID = 6 Then 'IEND' ELSE 'IDAF' End End End as Ticker,Acc.ChapterID,Acc.RestrictionType,B.TransCat  FROM   BrokTrans B  "
                    strSql = strSql & "  Inner Join NSFAccounts Acc On (Acc.BankID=B.BankID AND B.AssetClass = 'MMK' AND ((Acc.BankID in (4,5,6) AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='MMK') OR (Acc.RestrictionType='Temp Restricted' AND Acc.BankID =7 AND Acc.InvIncType ='MMK'))) OR (Acc.BankID=B.BankID AND Acc.InvIncType='Inv' AND B.AssetClass <> 'MMK') "
                    strSql = strSql & " Inner Join Chapter C ON Acc.ChapterID=C.ChapterID"
                    strSql = strSql & "  WHERE Acc.IntDiv='Y' AND ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) "
                    strSql = strSql & " AND  B.TransDate Between '" & frmdate & "'  AND '" & todate & "' AND B.BankID=" & bankID & " Group By  B.BankID, Acc.AccNo,Acc.Description,Acc.AccType,Acc.ChapterID,Acc.RestrictionType,B.TransCat ,C.WebfolderName, B.NetAmount , B.Quantity"
                    strSql = strSql & " UNION ALL"
                    strSql = strSql & " SELECT 1, '" & VoucherNo & "'  as VoucherNo,'" & todate & "' as DepositDate,'IGN' as TransType,Acc.AccNo as AccNo, Acc.Description,C.WebfolderName as Class,SUM(B.Amount) as Amount,RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate()," & Session("LoginID") & ",Acc.AccType,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End as Ticker,Acc.ChapterID ,Acc.RestrictionType,B.TransCat FROM BankTrans B Inner Join NSFAccounts Acc On Acc.BankID=B.BankID AND Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Cash' AND Acc.BankID in (1,2,3)  Inner Join Chapter C ON Acc.ChapterID=C.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit' AND B.TransDate Between '" & frmdate & "'  AND '" & todate & "' AND B.BankID=" & bankID & " Group By B.BankID, Acc.AccNo,Acc.Description,Acc.AccType,B.TransDate,Acc.ChapterID,Acc.RestrictionType,B.TransCat,C.WebfolderName"
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT 1, '" & VoucherNo & "'  as VoucherNo,'" & todate & "' as DepositDate,'IGN' as TransType,Acc.AccNo as AccNo, Acc.Description,C.WebfolderName as Class,-SUM(B.Amount) as Amount,RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) as BankID,Null as DepositSlip,Null as PayeeID , null as PayeeType,'" & frmdate & "' as StartDate,'" & todate & "' as EndDate,'" & todate & "' as BankDate,Getdate()," & Session("LoginID") & ",Acc.AccType,Case WHEN B.BankID = 1 Then 'ICH' ELSE Case WHEN B.BankID = 2 Then 'IHBS' ELSE Case WHEN B.BankID = 3 Then 'IHBO' End End End as Ticker,Acc.ChapterID,Acc.RestrictionType,B.TransCat FROM BankTrans B Inner Join NSFAccounts Acc On (Acc.RestrictionType='Unrestricted' AND Acc.InvIncType='Interest') AND B.BankID in (1,2,3)  Inner Join Chapter C ON Acc.ChapterID=C.ChapterID Where B.TransCat = 'Interest' and B.TransType='Credit'  AND B.TransDate Between '" & frmdate & "'  AND '" & todate & "' AND B.BankID=" & bankID & " Group By B.BankID, Acc.AccNo,Acc.Description,Acc.AccType,B.TransDate,Acc.ChapterID,Acc.RestrictionType,B.TransCat,C.WebfolderName"
                    strSql = strSql & " Order By BankID,Acc.AccNo,Amount "
                Next
                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString()
            End Try

        ElseIf ddlCategory.SelectedValue = "5" Then
            'Expense Vouchers
            'VoucherNo = "VPS" & TxtFrom.Text.Replace("/", "") & "_" & txtTo.Text.Replace("/", "") & ".iif" ' "_" & Now.Month & "_" & Now.Day & "_" & Now.Year &
            Try
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='VPS' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59';"
                'E.DatePaid
                Dim frmDate As Date, todate As Date
                For gcnt = 0 To gvExp.Rows.Count - 1
                    VoucherNo = gvExp.Rows(gcnt).Cells(3).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    ' stTransDate = gvExp.Rows(gcnt).Cells(4).Text.Trim
                    frmDate = Convert.ToDateTime(gvExp.Rows(gcnt).Cells(4).Text.Trim)
                    stTransDate = New DateTime(frmDate.Year, frmDate.Month, 1)
                    todate = Convert.ToDateTime(stTransDate)
                    'first day of next month minus one day
                    endTransDate = todate.AddMonths(1).AddDays(-1)

                    ' strSql = strSql & " declare @d" & gcnt & " datetime; set @s" & gcnt & "=DateDiff(d,EJ.DatePaid,'" & stTransDate & "'); "
                    ' strSql = strSql & " declare @e" & gcnt & " datetime; set @e" & gcnt & "=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1," & stTransDate & "))),DATEADD(mm,1," & stTransDate & ")),101); "

                    strSql = strSql & "  INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, CheckNumber, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,BankDate,ChapterID,RestType,TransCat,ToBankID,Name ) "
                    'strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, E.DatePaid as DepositDate,'VPS' as TransType,N.Accno, N.Description,Ch.WebfolderName  as Class,-SUM(E.ExpenseAmount) as Amount,E.BankID," & gvExp.DataKeys(gcnt).Value & " as CheckNumber,E.ReimbMemberID as PayeeID , E.DonorType as PayeeType,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate,Getdate()," & Session("LoginID") & ",'A',BT.TransDate,N.ChapterID ,N.RestrictionType,E.ToBankID From ExpJournal E Inner Join NSFAccounts N ON E.BankID=N.BankID and E.RestTypeFrom=N.RestrictionType and N.[Level]='L'  and N.InvIncType IN ('MMK','CASH') left Join BankTrans BT On BT.CheckNumber=E.CheckNumber and BT.BankID=E.BanKID INNER JOIN Chapter Ch ON N.ChapterID = Ch.ChapterID WHERE E.CheckNumber = '" & gvExp.DataKeys(gcnt).Value & "' AND E.Datepaid='" & gvExp.Rows(gcnt).Cells(4).Text & "'" & " Group by E.Datepaid,E.CheckNumber,N.AccNo,E.BankID,N.Description,E.DonorType,E.ReimbMemberID,BT.TransDate,N.ChapterID,N.RestrictionType,E.ToBankID,Ch.WebfolderName UNION ALL "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, E.DatePaid as DepositDate,'VPS' as TransType,N.Accno, N.Description,Ch.WebfolderName  as Class,-SUM(E.ExpenseAmount) as Amount,E.BankID," & gvExp.DataKeys(gcnt).Value & " as CheckNumber,E.ReimbMemberID as PayeeID , E.DonorType as PayeeType,'" & stTransDate & "' as StartDate,'" & endTransDate & "' as EndDate,Getdate()," & Session("LoginID") & ",'A',Case when BT.TransDate IS null Then E.DatePaid Else BT.TransDate End as BankDate,N.ChapterID ,N.RestrictionType,Case When E.TransType='Transfers' Then 'TransferOut' Else E.ExpCatCode End as TransCat,E.ToBankID,CASE WHEN E.DonorType = 'OWN' then O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as Name   From ExpJournal E Inner Join NSFAccounts N ON E.BankID=N.BankID and E.RestTypeFrom=N.RestrictionType and N.[Level]='L'  and N.InvIncType IN ('MMK','CASH') left Join BankTrans BT On BT.CheckNumber=E.CheckNumber and BT.BankID=E.BanKID INNER JOIN Chapter Ch ON N.ChapterID = Ch.ChapterID Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse I ON E.DonorType <> 'OWN' AND E.ReimbMemberid = I.AutoMemberID WHERE E.CheckNumber = '" & gvExp.DataKeys(gcnt).Value & "' AND E.Datepaid='" & frmDate & "'" & " Group by E.Datepaid,E.CheckNumber,N.AccNo,E.BankID,N.Description,E.DonorType,E.ReimbMemberID,BT.TransDate,N.ChapterID,N.RestrictionType,E.ToBankID,Ch.WebfolderName,O.ORGANIZATION_NAME,I.FirstName ,I.LastName,E.TransType,E.ExpCatCode UNION ALL "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, EJ.DatePaid as DepositDate,'VPS' as TransType, CASE WHEN EJ.TransType = 'Transfers'  THEN BS.Accno ELSE  EC.Account END as AccNo,CASE WHEN EJ.TransType = 'Transfers' THEN BS.Description ELSE  EC.AccountName END As Description, CASE WHEN EJ.ChapterID=1 and EJ.EventID is null and EJ.TransType = 'GenAdmin' THEN 'US_HomeAdmin' ELSE  CASE WHEN EJ.ChapterID=1 and EJ.EventID = 1 and EJ.Transtype='FinalsExp' THEN  'US_Finals' ELSE   Ch.webfoldername END END  as Class,SUM(EJ.ExpenseAmount) as Amount,CASE WHEN EJ.TransType = 'Transfers' THEN EJ.ToBankID ELSE EJ.BankID END as BankID," & gvExp.DataKeys(gcnt).Value & " as CheckNumber,EJ.ReimbMemberID as PayeeID , EJ.DonorType as PayeeType,'" & stTransDate & "' as StartDate,'" & endTransDate & "' as EndDate,Getdate()," & Session("LoginID") & ", CASE WHEN EJ.Transtype='FinalsExp' OR EJ.Transtype='ChapterExp' OR EJ.Transtype='GenAdmin' THEN 'E' ELSE CASE WHEN EJ.Transtype='Grants' THEN 'G' ELSE CASE  WHEN EJ.Transtype='Transfers' THEN 'A' ELSE CASE  WHEN EJ.Transtype='Revenue' THEN 'I' ELSE 'E' END END END END ,Case when BT.TransDate Is null Then EJ.DatePaid Else BT.TransDate End as BankDate,EJ.ChapterID,EJ.RestTypeFrom,Case When EJ.TransType='Transfers' Then 'TransferOut' Else EJ.ExpCatCode End as TransCat,EJ.ToBankID "
                    strSql = strSql & " ,CASE WHEN EJ.DonorType = 'OWN' then O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as Name from ExpJournal EJ Left Outer Join ExpenseCategory EC ON  EJ.ExpCatID=EC.ExpCatID  "
                    strSql = strSql & " Left Outer Join NSFAccounts BS ON  BS.BankID=EJ.ToBankID and BS.RestrictionType=EJ.RestTypeFrom and EJ.TransType = 'Transfers' AND (BS.InvIncType IS NULL OR BS.InvIncType<>'Inv') Inner Join AcctgTransType ATT ON ATT.Code = EJ.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7) Inner Join Chapter Ch ON Ch.ChapterID = CASE WHEN  EJ.ToChapterID IS NULL THEN EJ.ChapterID ELSE EJ.ToChapterID END  left Join BankTrans BT on BT.CheckNumber=EJ.CheckNumber and BT.BankID=EJ.BanKID "
                    strSql = strSql & " Left Join OrganizationInfo O ON EJ.DonorType = 'OWN' and O.AutoMemberid =EJ.ReimbMemberid Left Join IndSpouse I ON EJ.DonorType <> 'OWN' AND EJ.ReimbMemberid = I.AutoMemberID"
                    strSql = strSql & " WHERE DateDiff(d,EJ.DatePaid,'" & stTransDate & "')=0 AND EJ.CheckNumber='" & gvExp.DataKeys(gcnt).Value & "'"
                    strSql = strSql & " Group by EJ.CheckNumber,EJ.DonorType,EJ.ReimbMemberID,EJ.DatePaid,EC.Account,EC.AccountName,Ch.webfoldername,Ch.ChapterCode,Ch.ChapterID,ATT.Name,EJ.TransType,BS.Description,BS.Accno,CASE WHEN EJ.TransType = 'Transfers' THEN EJ.TOBankID ELSE EJ.BankID END,EJ.EventID,EJ.ChapterID,BT.TransDate,EJ.RestTypeFrom,EJ.ToBankID,O.ORGANIZATION_NAME,I.FirstName ,I.LastName,EJ.TransType,EJ.ExpCatCode ; "

                Next

                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If

            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString()
            End Try
        ElseIf ddlCategory.SelectedValue = "6" Then
            ''ACT_EFT Donations
            Dim BankID As Integer
            Dim TransDate As String
            Dim BankTransID As Integer
            Try

                strSql = " DELETE FROM GeneralLedgerTemp where TransType='DGE' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59';"
                For gcnt = 0 To GVACT_EFTDon.Rows.Count - 1
                    VoucherNo = GVACT_EFTDon.Rows(gcnt).Cells(4).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    BankID = GVACT_EFTDon.Rows(gcnt).Cells(3).Text.Trim
                    TransDate = GVACT_EFTDon.Rows(gcnt).Cells(5).Text
                    BankTransID = GVACT_EFTDon.Rows(gcnt).Cells(15).Text.Trim

                    strSql = strSql & " declare @s" & gcnt & " datetime; set @s" & gcnt & "=CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101); "
                    strSql = strSql & " declare @e" & gcnt & " datetime; set @e" & gcnt & "=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101); "

                    ' stTransDate = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101) "
                    ' endTransDate = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101) "

                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, CheckNumber, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,BankDate,ChapterID,RestType,TransCat,Name) "
                    strSql = strSql & " SELECT 1,VoucherNo,TransDate,'DGE' as TransType,AccNo,Description,WebfolderName as Class,SUM(Amount) as Amount,BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType, @s" & gcnt & ", @e" & gcnt & ",GETDATE(),4240,AccType,TransDate,ChapterID,RestType,TransCat,VendCust as Name"
                    strSql = strSql & " FROM((SELECT  B.BankID,B.BankTransID,N.AccNo,N.AccType,Ch.WebfolderName , -SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,B.VendCust, N.Description ,N.DonorType,N.ChapterID,N.RestrictionType as RestType "
                    strSql = strSql & " FROM BankTrans B "
                    strSql = strSql & " LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat=C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankId=1 "
                    strSql = strSql & " LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat=H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankId = 2 "
                    strSql = strSql & " Inner Join  NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType =N.Description "
                    strSql = strSql & " Inner Join Chapter Ch ON N.ChapterID = Ch.ChapterID"
                    strSql = strSql & " Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between  @s" & gcnt & " and  @e" & gcnt & " and B.BankID = " & BankID & " and Convert(Date,TransDate) = CONVERT (Date,'" & TransDate & "')  and B.BankTransID = " & BankTransID
                    strSql = strSql & " Group By N.AccNo,N.AccType ,B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate ,B.Vendcust,B.BankTransID,N.ChapterID,N.RestrictionType,Ch.WebfolderName"
                    strSql = strSql & ")UNION ALL "
                    strSql = strSql & "(SELECT B.BankId,B.BankTransID,N.AccNo,N.AccType,Ch.WebfolderName,SUM(B.Amount) as Amount,B.Transcat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,B.VendCust, N.Description ,N.DonorType,N.ChapterID,N.RestrictionType as RestType"
                    strSql = strSql & " FROM BankTrans B  INNER JOIN NSFAccounts N ON  B.BankID =N.BankID "
                    strSql = strSql & " Inner Join Chapter Ch ON N.ChapterID = Ch.ChapterID"
                    strSql = strSql & " Where N.RestrictionType = 'Unrestricted' and B.TransCat='Donation' and B.TransDate Between  @s" & gcnt & " and  @e" & gcnt & ""
                    strSql = strSql & " and B.BankID=" & BankID & " and Convert(Date,TransDate) = CONVERT (Date,'" & TransDate & "')   and B.BankTransID = " & BankTransID & " Group by B.BankId,N.AccNo,N.AccType,B.Transcat,B.VendCust, N.Description ,N.DonorType,B.TransDate,B.BankTransID,N.ChapterID,N.RestrictionType,Ch.WebfolderName"
                    strSql = strSql & " ))Temp1 Group by AccNo,AccType,BankID ,VoucherNo,TransDate ,Description,RestType,TransCat,ChapterID,WebFolderName,VendCust "
                    If gcnt = 30 Then
                        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                            lblErr.Text = ""
                            strSql = ""
                        End If
                    End If
                Next
                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If

            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString() ' & "<BR>" & strSql
            End Try
        ElseIf ddlCategory.SelectedValue = "7" Then
            'Bank Service Charges
            Dim BankID As Integer
            Dim TransDate As String
            Dim VendCust As String
            Dim ExpcatCode As String
            Try
                Dim frmdate As Date
                Dim todate As DateTime
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='VPE' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59';"
                For gcnt = 0 To GVBnk_CCFees.Rows.Count - 1
                    VoucherNo = GVBnk_CCFees.Rows(gcnt).Cells(4).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    BankID = GVBnk_CCFees.Rows(gcnt).Cells(2).Text.Trim
                    TransDate = GVBnk_CCFees.Rows(gcnt).Cells(5).Text
                    frmdate = Convert.ToDateTime(GVBnk_CCFees.Rows(gcnt).Cells(5).Text)
                    todate = Convert.ToDateTime(GVBnk_CCFees.Rows(gcnt).Cells(5).Text)
                    VendCust = GVBnk_CCFees.Rows(gcnt).Cells(9).Text
                    ExpcatCode = GVBnk_CCFees.Rows(gcnt).Cells(8).Text
                    '  stTransDate = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101) "
                    ' endTransDate = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101) "
                    strSql = strSql & " declare @s" & gcnt & " datetime; set @s" & gcnt & "=CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101); "
                    strSql = strSql & " declare @e" & gcnt & " datetime; set @e" & gcnt & "=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101); "

                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, CheckNumber, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,BankDate,ChapterID,RestType,TransCat) "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,'" & frmdate & "' as Date,'VPE' as TransType,AccNo,Description,Class,SUM(Amount) as Amount,BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & "  as EndDate,GETDATE()," & Session("LoginID") & ",AccType,'" & frmdate & "' as BankDate,ChapterID,RestType,Transcat "
                    strSql = strSql & " FROM("
                    strSql = strSql & " SELECT B.BankID,-SUM(Amount) as Amount,N.AccType,C.WebfolderName as Class,N.Description, N.AccNo,N.ChapterID,N.RestrictionType as RestType,B.TransCat FROM BankTrans B INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode INNER JOIN NSFAccounts N ON E.Account = N.AccNo INNER JOIN Chapter C ON N.ChapterID = C.ChapterID Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID = " & BankID & " and TransDate Between @s" & gcnt & "  and @e" & gcnt & " AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpcatCode & "')"
                    strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpcatCode) > 0, " AND B.TransCat='" & ExpcatCode & "'", "")

                    '" & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                    strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,N.ChapterID,N.RestrictionType,B.TransCat,C.WebfolderName "
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT B.BankID,-SUM(Amount) as Amount,N.AccType,C.WebfolderName as Class,N.Description,N.AccNo,N.ChapterID,N.RestrictionType as RestType,B.TransCat FROM BankTrans B INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode INNER JOIN NSFAccounts N ON E.Account = N.AccNo INNER JOIN Chapter C ON N.ChapterID = C.ChapterID where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID = " & BankID & " and TransDate Between @s" & gcnt & " and @e" & gcnt & " AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpcatCode & "')"
                    strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpcatCode) > 0, " AND B.TransCat='" & ExpcatCode & "'", "")

                    '" & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                    strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,N.ChapterID,N.RestrictionType,B.TransCat,C.WebfolderName "
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT B.BankID,SUM(Amount) as Amount,N.AccType,C.WebfolderName as Class,N.Description,N.AccNo,N.ChapterID,N.RestrictionType as RestType,B.TransCat as VoucherNo FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' INNER JOIN Chapter C ON N.ChapterID = C.ChapterID Where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and B.BankID = " & BankID & " and TransDate Between @s" & gcnt & " and @e" & gcnt & " AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpcatCode & "')"
                    strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpcatCode) > 0, " AND B.TransCat='" & ExpcatCode & "'", "")

                    '" & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                    strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,N.ChapterID,N.RestrictionType,B.TransCat,C.WebfolderName "
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT B.BankID,SUM(Amount) as Amount,N.AccType,C.WebfolderName as Class,N.Description,N.AccNo,N.ChapterID,N.RestrictionType as RestType,B.TransCat FROM BankTrans B INNER JOIN NSFAccounts N ON N.BankID = B.BankID and N.RestrictionType ='UnRestricted' INNER JOIN Chapter C ON N.ChapterID = C.ChapterID Where TransType= 'DEBIT' And TransCat = 'CreditCard' and B.BankID = " & BankID & " and TransDate Between @s" & gcnt & " and @e" & gcnt & " AND (B.VendCust='" & VendCust & "' Or B.TransCat='" & ExpcatCode & "')"
                    strSql = strSql & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpcatCode) > 0, " AND B.TransCat='" & ExpcatCode & "'", "")

                    '" & IIf(Len(VendCust) > 0, " AND B.VendCust='" & VendCust & "'", "") & IIf(Len(ExpCatCode) > 0, " AND B.TransCat='" & ExpCatCode & "'", "")
                    strSql = strSql & " Group By B.BankID,N.AccNo,B.Reason,B.AddInfo,N.AccType,N.Description,N.ChapterID,N.RestrictionType,B.TransCat,C.WebfolderName"
                    strSql = strSql & " )T Group by BankID,AccNo,AccType,Description,ChapterID,RestType,TransCat,Class"
                Next

                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString & "<br />" & strSql)
                lblErr.Text = "Error in Inserting" & ex.ToString() ' & "<BR>" & strSql
            End Try
        ElseIf ddlCategory.SelectedValue = "8" Then
            ''TransferOut
            Dim BankID, ToBankID As Integer
            Dim TransDate As String
            Dim Amount As Double
            Dim Ticker As String
            Try
                Dim frmdate As Date
                Dim todate As DateTime
          
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='IGT' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59';"
                For gcnt = 0 To GVTransfers.Rows.Count - 1
                    VoucherNo = GVTransfers.Rows(gcnt).Cells(5).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    BankID = GVTransfers.Rows(gcnt).Cells(2).Text.Trim
                    TransDate = GVTransfers.Rows(gcnt).Cells(6).Text
                    frmdate = Convert.ToDateTime(GVTransfers.Rows(gcnt).Cells(6).Text)
                    todate = Convert.ToDateTime(GVTransfers.Rows(gcnt).Cells(6).Text)
                    Amount = (GVTransfers.Rows(gcnt).Cells(10).Text.ToString())
                    ToBankID = GVTransfers.Rows(gcnt).Cells(18).Text.Trim
                    Ticker = GVTransfers.Rows(gcnt).Cells(13).Text.Trim
                    ' stTransDate = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101) "
                    'endTransDate = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101) "
                    strSql = strSql & " declare @s" & gcnt & " datetime; set @s" & gcnt & "=CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101); "
                    strSql = strSql & " declare @e" & gcnt & " datetime; set @e" & gcnt & "=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101); "

                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, CheckNumber, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,BankDate,ChapterID,RestType,TransCat,ToBankId,Ticker) "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,B.TransDate as Date,'IGT' as TransType,N.AccNo,N.Description,C.WebfolderName as Class," & Decimal.Parse(Amount) & " as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,B1.BankID ,'" & Ticker & "' "
                    strSql = strSql & " FROM BrokTrans B "
                    strSql = strSql & " Inner join BrokTrans b1 On B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and
                    strSql = strSql & " Inner Join NSFAccounts N On N.BankID=B.BankId" ' and B.RestType=N.RestrictionType"
                    strSql = strSql & " and (N.RestrictionType in (Case when B.BankID = 4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end )) "
                    strSql = strSql & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
                    strSql = strSql & " Inner Join Chapter C ON N.ChapterID=C.ChapterID "
                    strSql = strSql & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Medium='IntTransfer' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59 '  and B.BankID= " & BankID & ""
                    strSql = strSql & " Group by B.TransDate,N.AccNo,N.Description,B.BankID,N.AccType,N.ChapterID,N.RestrictionType,B.TransCat,B1.BankID,C.WebfolderName"
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,B.TransDate as Date,'IGT' as TransType,N.AccNo,N.Description,C.WebfolderName as Class," & -Decimal.Parse(Amount) & " as Amount,B1.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,B1.BankID,'" & Ticker & "' "
                    strSql = strSql & " FROM BrokTrans B "
                    strSql = strSql & " Inner Join BrokTrans b1 On B.TransDate =B1.TransDate and B.TransCat<>B1.TransCat " 'B1.TranId=B.TranID and 
                    strSql = strSql & " Inner Join NSFAccounts N On N.BankID=B1.BankId " 'and B1.RestType=N.RestrictionType "
                    strSql = strSql & " and (N.RestrictionType in (Case when B1.BankID =4 and B.BankID <>5 then B.ToRestType Else Case when B.BankID = 4 then 'Temp Restricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'UnRestricted' End end end end end )) "
                    strSql = strSql & " and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
                    strSql = strSql & " Inner Join Chapter C ON N.ChapterID=C.ChapterID "
                    strSql = strSql & " Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Medium='IntTransfer' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59 ' and B.BankID= " & BankID & ""
                    strSql = strSql & " Group by B.TransDate,N.AccNo,N.Description,B.BankID,N.AccType,N.ChapterID,N.RestrictionType,B.TransCat,B1.BankID,C.WebfolderName"
                Next


                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If

            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString() ' & "<BR>" & strSql
            End Try

        ElseIf ddlCategory.SelectedValue = "9" Then
            ''Buy Transactions
            Dim BankID As Integer
            Dim TransDate As String
            Try
                Dim frmdate As Date
                Dim todate As DateTime
                Dim AssetClass As String
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='IGB' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59';"
                For gcnt = 0 To GVBuyTrans.Rows.Count - 1
                    VoucherNo = GVBuyTrans.Rows(gcnt).Cells(4).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    BankID = GVBuyTrans.Rows(gcnt).Cells(2).Text.Trim
                    TransDate = GVBuyTrans.Rows(gcnt).Cells(5).Text
                    frmdate = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(5).Text)
                    todate = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(5).Text)
                    AssetClass = GVBuyTrans.Rows(gcnt).Cells(11).Text
                    '  stTransDate = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101) "
                    ' endTransDate = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101) "
                    strSql = strSql & " declare @s" & gcnt & " datetime; set @s" & gcnt & "=CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101); "
                    strSql = strSql & " declare @e" & gcnt & " datetime; set @e" & gcnt & "=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101); "

                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, CheckNumber, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,BankDate,ChapterID,RestType,TransCat,AssetClass) "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,B.TransDate as Date,'IGB' as TransType,N.AccNo,N.Description,C.WebFolderName as Class,-SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,'" & AssetClass & "' "
                    strSql = strSql & " FROM BrokTrans B "
                    strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) and N.Description ='Investment-General Unrestricted'"
                    strSql = strSql & " INNER JOIN Chapter C ON N.ChapterID=C.ChapterID"
                    strSql = strSql & " Where B.Transcat='Buy' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59' and B.AssetClass='" & AssetClass & "'"
                    strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,N.ChapterID,N.RestrictionType,B.TransCat,C.WebFolderName"
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo,B.TransDate as Date,'IGB' as TransType,N.AccNo,N.Description,C.WebFolderName as Class,SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber,NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,'" & AssetClass & "' "
                    strSql = strSql & " FROM BrokTrans B "
                    strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID  and N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') and N.Description ='MMKT - General Unrestricted'"
                    strSql = strSql & " INNER JOIN Chapter C ON N.ChapterID=C.ChapterID"
                    strSql = strSql & " Where B.Transcat='Buy' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59' and B.AssetClass='" & AssetClass & "'"
                    strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,N.ChapterID,N.RestrictionType,B.TransCat,C.WebFolderName"
                Next

                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString() ' & "<BR>" & strSql
            End Try
        ElseIf ddlCategory.SelectedValue = "10" Then
            ''SELL Transactions
            Dim BankID As Integer
            Dim TransDate As String
            Try
                Dim frmdate As Date
                Dim todate As DateTime
                Dim AssetClass As String
              
                strSql = " DELETE FROM GeneralLedgerTemp where TransType='IGS' and Date between '" & TxtFrom.Text & "' and  '" & txtTo.Text & " 23:59';"
                For gcnt = 0 To GVBuyTrans.Rows.Count - 1
                    VoucherNo = GVBuyTrans.Rows(gcnt).Cells(4).Text.Trim
                    VoucherNumbers = VoucherNumbers & "'" & VoucherNo & "',"
                    BankID = GVBuyTrans.Rows(gcnt).Cells(2).Text.Trim
                    TransDate = GVBuyTrans.Rows(gcnt).Cells(5).Text
                    frmdate = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(5).Text)
                    todate = Convert.ToDateTime(GVBuyTrans.Rows(gcnt).Cells(5).Text)
                    AssetClass = GVBuyTrans.Rows(gcnt).Cells(11).Text
                    '  stTransDate = " CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101) "
                    '  endTransDate = " CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101) "
                    strSql = strSql & " declare @s" & gcnt & " datetime; set @s" & gcnt & "=CONVERT(VARCHAR(10),DATEADD(dd,-(DAY('" & TransDate & "')-1),'" & TransDate & "'),101); "
                    strSql = strSql & " declare @e" & gcnt & " datetime; set @e" & gcnt & "=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,'" & TransDate & "'))),DATEADD(mm,1,'" & TransDate & "')),101); "

                    strSql = strSql & " INSERT INTO GeneralLedgerTemp(CompanyNumber,VoucherNo, Date, TransType, Account, Description, Class, Amount, BankID, CheckNumber, PayeeID, PayeeType, StartDate, Enddate,CreateDate, CreatedBy,AccountType,BankDate,ChapterID,RestType,TransCat,Ticker,Basis) "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, B.TransDate as Date,'IGS' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class, -SUM(Distinct B.Quantity*C.AvgPrice) as Amount,B.BankID,NULL As CheckNumber, NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,B.Ticker ,Case when N.AccNo in (53725,53730) then 'Market' Else Case when N.AccNo in (53750,53755) then 'Cost' Else NULL end end as Basis"
                    strSql = strSql & " FROM BrokTrans B "
                    strSql = strSql & " Inner join  CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker  and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares " 'and B.CostBasis= C.CostBasis
                    strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))  and N.RestrictionType in (Case when B.BankID =  4 then 'Unrestricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end)" 'and N.Description ='Investment-DAF Temp. Restricted' "
                    strSql = strSql & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID"
                    strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59' and B.AssetClass='" & AssetClass & "'  and B.BankID=" & BankID
                    strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebFolderName,B.Ticker"
                    strSql = strSql & " UNION ALL "
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, B.TransDate as Date,'IGS' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class,  SUM(B.NetAmount) as Amount,B.BankID,NULL As CheckNumber, NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,B.Ticker ,Case when N.AccNo in (53725,53730) then 'Market' Else Case when N.AccNo in (53750,53755) then 'Cost' Else NULL end end as Basis"
                    strSql = strSql & " FROM BrokTrans B "
                    strSql = strSql & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID and  N.InvIncType in ('MMK') and B.AssetClass not in ('Cash','MMK') and N.RestrictionType in (Case when B.BankID =  4 then 'Unrestricted' Else Case when B.BankID =5 then 'Unrestricted' Else Case when B.BankID =6 then 'Perm Restricted' else case when B.BankID = 7 then 'Temp Restricted' End end end end)" 'and N.Description ='MMKT - DAF Temp. Restricted' " 'N.InvIncType in ('MMK','Inv') and B.AssetClass not in ('Cash','MMK') '' and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) 
                    strSql = strSql & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
                    strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59' and B.AssetClass='" & AssetClass & "'  and B.BankID=" & BankID
                    strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebFolderName,B.Ticker"
                    strSql = strSql & " UNION ALL"
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, B.TransDate as Date,'IGS' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class,  SUM(Distinct B.Quantity*C.AvgPrice) as Amount,B.BankID,NULL As CheckNumber, NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,B.Ticker ,Case when N.AccNo in (53725,53730) then 'Market' Else Case when N.AccNo in (53750,53755) then 'Cost' Else NULL end end   as Basis"
                    strSql = strSql & " from BrokTrans B Inner join  CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker  and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares " 'and B.CostBasis= C.CostBasis"
                    strSql = strSql & " INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null)and B.AssetClass not in ('Cash','MMK') and N.Description = Case when B.BankID <> 7 then 'Securities Sales - Cost'  Else 'Securities sales - Cost - Temp Rest' End" ''Securities Sales - Cost' "
                    strSql = strSql & " Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID "
                    strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59' and B.AssetClass='" & AssetClass & "'  and B.BankID=" & BankID
                    strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebFolderName,B.Ticker"
                    strSql = strSql & " UNION ALL"
                    strSql = strSql & " SELECT 1,'" & VoucherNo & "' as VoucherNo, B.TransDate as Date,'IGS' as TransType,N.AccNo,N.Description,Ch.WebFolderName as Class, -SUM(Distinct B.NetAmount) as Amount,B.BankID,NULL As CheckNumber, NULL As PayeeID,NUll as PayeeType,@s" & gcnt & " as StartDate,@e" & gcnt & " as EndDate,GETDATE()," & Session("LoginID") & ",N.AccType,B.TransDate as BankDate,N.ChapterID,N.RestrictionType,B.TransCat,B.Ticker ,Case when N.AccNo in (53725,53730) then 'Market' Else Case when N.AccNo in (53750,53755) then 'Cost' Else NULL end end   as Basis"
                    strSql = strSql & " from BrokTrans B Inner join  CostBasisBalances C on C.BankID =B.BankID and C.Ticker=B.Ticker  and B.AvgPrice = Round(C.AvgPrice,4) and C.OutStdShares= B.OutStdShares " 'and B.CostBasis= C.CostBasis"
                    strSql = strSql & " INNER JOIN NSFAccounts N ON (B.BanKID=N.BankID Or N.BankID is null) and B.AssetClass not in ('Cash','MMK') and N.Description = Case when B.BankID <> 7 then 'Securities Sales - Gross'  Else 'Securities Sales - Gross - Temp Rest' End Inner Join Chapter Ch On N.ChapterID = Ch.ChapterID " ''Securities sales - Gross'
                    strSql = strSql & " Where B.Transcat='Sell' and B.TransDate Between '" & frmdate & "' and '" & todate & " 23:59' and B.AssetClass='" & AssetClass & "'  and B.BankID=" & BankID
                    strSql = strSql & " Group by B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.AccNo,N.Description ,N.Acctype,N.ChapterID,N.RestrictionType,B.TransCat,Ch.WebFolderName,B.Ticker"
                    strSql = strSql & " order by Amount "
                Next
                tblDupRec.Visible = False
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql) > 0 Then
                    lblErr.Text = "Records Inserted Successfully"
                End If
            Catch ex As Exception
                lblErr.Text = "Error in Inserting" & ex.ToString()
            End Try
        End If
    End Sub

    Protected Sub btnGLReports_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TrDetailView.Visible = False
        btnClose.Visible = False
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            lblErr.Text = ""
            Dim StrSQL As String
            StrSQL = " SELECT G.GenLedID,G.CompanyNumber, G.VoucherNo, G.Date, G.TransType,G.BankDate, G.Account, G.Description, G.Class,G.Ticker, G.Amount, G.BankID,B.BankName, G.CheckNumber, G.DepositSlip, G.PayeeID, G.PayeeType,CASE WHEN G.PayeeType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as PayeeName, G.StartDate, G.Enddate,G.AccountType  "
            StrSQL = StrSQL & " FROM GeneralLedgerTemp G Left Join Bank B ON B.BankID=G.BankID Left Join OrganizationInfo O ON G.PayeeType = 'OWN' and O.AutoMemberid =G.PayeeID Left Join IndSpouse I ON G.PayeeType <> 'OWN' AND G.PayeeID = I.AutoMemberID WHERE G.Date "
            StrSQL = StrSQL & "  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' order by  G.CompanyNumber,G.TransType,G.BankDate, G.AccountType, G.Account, G.Date "
            Session("ManageVouchersSQl") = StrSQL
            GetGLReports(StrSQL)
        End If
    End Sub
    Private Sub GetGLReports(ByVal StrSQL As String)
        'gvDonation
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
        If dsDonation.Tables(0).Rows.Count > 0 Then
            TrDonation.Visible = True
            gvDonation.Visible = False
            GVCCRevenues.Visible = False
            gvExp.Visible = False
            setFlag = False
            GVGLTemp.DataSource = dsDonation.Tables(0)
            GVGLTemp.DataBind()
            GVGLTemp.Visible = True
        Else
            GVGLTemp.DataSource = Nothing
            GVGLTemp.DataBind()
            lblErr.Text = "Sorry No data found"
            GVGLTemp.Visible = False
        End If
    End Sub

    Protected Sub GVGLTemp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("ManageVouchersSQl"))
        GVGLTemp.DataSource = dsDonation
        GVGLTemp.PageIndex = e.NewPageIndex
        GVGLTemp.DataBind()
    End Sub

    Protected Sub btnGLExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            Dim TempGrid As New GridView
            Dim dsTransDonation As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim StrSQL As String
            StrSQL = " SELECT G.GenLedID,G.CompanyNumber, G.VoucherNo, CONVERT(VARCHAR(11), G.Date, 101) as Date, G.TransType,CONVERT(VARCHAR(11), G.BankDate, 101) as BankDate, G.Account, G.Description, G.Class,G.Ticker, G.Amount, G.BankID,B.BankName, G.CheckNumber, G.DepositSlip, G.PayeeID, G.PayeeType,CASE WHEN G.PayeeType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as PayeeName, G.StartDate, G.Enddate,G.AccountType "
            'StrSQL = " SELECT G.GenLedID,G.CompanyNumber, G.VoucherNo, CONVERT(VARCHAR(11), G.Date, 101) as Date, G.TransType,CONVERT(VARCHAR(11), G.BankDate, 101) as BankDate, G.Account, G.Description, G.Class, G.Amount, G.BankID,B.BankName, G.CheckNumber, G.DepositSlip, G.PayeeID, G.PayeeType,CASE WHEN G.PayeeType = 'OWN' then  O.ORGANIZATION_NAME ELSE I.FirstName +' '+ I.LastName END as PayeeName, G.StartDate, G.Enddate,G.AccountType "
            StrSQL = StrSQL & " FROM GeneralLedgerTemp G Left Join Bank B ON B.BankID=G.BankID Left Join OrganizationInfo O ON G.PayeeType = 'OWN' and O.AutoMemberid =G.PayeeID Left Join IndSpouse I ON G.PayeeType <> 'OWN' AND G.PayeeID = I.AutoMemberID WHERE G.Date "
            StrSQL = StrSQL & "  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' order by  G.CompanyNumber,G.TransType,G.BankDate, G.AccountType, G.Account, G.Date "
            dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)
            TempGrid.DataSource = dsTransDonation
            TempGrid.DataBind()
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=GeneralLedgerTemp" & "_" & Now.Date() & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

            TempGrid.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.[End]()
        End If
    End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        tblDupRec.Visible = False
        If ddlCategory.SelectedValue = 4 Then
            trbanks.Visible = True
            loadbank()
        Else
            trbanks.Visible = False
        End If
        If ddlCategory.SelectedValue = 8 Then
            tdTansHelp.Visible = True
        Else
            tdTansHelp.Visible = False
        End If

        'If ddlCategory.SelectedValue = 3 Then
        '    BtnDetRawData.Visible = True
        'Else
        '    BtnDetRawData.Visible = False
        'End If
        'If ddlCategory.SelectedValue = 3 Then
        '    BtnDetRawData.Visible = True
        '    BtnExportList.Visible = False
        'ElseIf ddlCategory.SelectedValue = 4 Then
        '    BtnDetRawData.Visible = False
        '    BtnExportList.Visible = False
        'Else
        '    BtnDetRawData.Visible = False
        '    BtnExportList.Visible = True
        'End If

    End Sub


    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        Try

            If Session("SqlInsert") <> "" Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Session("SqlInsert")) > 0 Then
                    lblErr.Text = "Records Updated Successfully"
                End If
            End If
            tblDupRec.Visible = False
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        tblDupRec.Visible = False
        lblErr.Text = "Records already exist. No Update Done"
    End Sub
    Protected Sub BtnShowGL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnShowGL.Click
        '' Show GeneralLedger table Data
        Response.Write("<script language='javascript'>")
        Response.Write(" window.open('ShowGeneralLedger.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');")
        Response.Write("</script>")
    End Sub

    Protected Sub BtnDetRawData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDetRawData.Click
        Dim SDate As Date
        Dim EDate As Date
        SDate = TxtFrom.Text
        EDate = txtTo.Text

        If ddlCategory.SelectedValue = 1 Then
            Session("DCatID") = 1
        Else

            Session("DCatID") = 3
        End If
        Response.Redirect("~/ShowVoucher.aspx?Dtype=CCDet&SDt=" & SDate & "&EDt=" & EDate)

    End Sub

    Protected Sub BtnExportList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportList.Click
        Dim TempGrid As New GridView
        Dim Filename As String = GetGridData(TempGrid)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & Filename & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

        If ddlCategory.SelectedValue = "5" Then
            Dim HeaderRow As GridViewRow = New GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert)

            Dim HeaderCell2 As TableCell = New TableCell()
            HeaderCell2.Text = "Check Register"
            HeaderCell2.ColumnSpan = 6
            HeaderCell2.ForeColor = Color.Green
            HeaderCell2.HorizontalAlign = HorizontalAlign.Center
            HeaderRow.Cells.Add(HeaderCell2)

            TempGrid.Controls(0).Controls.AddAt(0, HeaderRow)
            'TempGrid.HeaderRow.Cells(2).Text = "Bank Date"
            'TempGrid.HeaderRow.Cells(3).Text = "Check Date"

            For Each cell As TableCell In TempGrid.HeaderRow.Cells
                If cell.Text = "DateCashed" Then
                    cell.Text = "Bank Date"
                End If
                If cell.Text = "DatePaid" Then
                    cell.Text = "Check Date"
                End If
            Next

            'Dim Column As BoundColumn
            'Column = New BoundColumn
            'Column.DataField = "ID"
            'Column.HeaderText = "Employee ID"
            'TempGrid.Columns.Add(Column)
        End If
        TempGrid.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()

    End Sub
    Function GetGridData(ByVal Grdvw As GridView) As String
        Dim dsTransDonation As New DataSet
        Dim Filename As String = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSQL"))
        If dsTransDonation.Tables(0).Rows.Count > 0 Then
            If ddlCategory.SelectedValue = 1 Or ddlCategory.SelectedValue = 2 Then
                dsTransDonation.Tables(0).Columns.RemoveAt(6)
                dsTransDonation.Tables(0).Columns.RemoveAt(8)
                'ElseIf ddlCategory.SelectedValue = 5 Then

            End If
            Filename = ddlCategory.SelectedItem.Text.Replace(" ", "").Trim & "_" & TxtFrom.Text & "_" & txtTo.Text
            Grdvw.DataSource = dsTransDonation.Tables(0)
            Grdvw.DataBind()

        Else
            Grdvw.DataSource = Nothing
            Grdvw.DataBind()
        End If
        Return Filename
    End Function
End Class
