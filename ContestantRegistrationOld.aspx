<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContestantRegistration.aspx.vb" Inherits="ContestantRegistration" MasterPageFile="NSFMasterPage.master" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
			<table cellspacing="1" cellpadding="1" border="0"  id="Table1" runat="server" width="100%">
			    <tr>
			        <td colspan="3" style="height: 18px" ><asp:hyperlink id="hlinkChild" runat="server" NavigateUrl="MainChild.aspx">Back to Children List</asp:hyperlink></td>
			    </tr>
			    <tr>
			        <td colspan="3" ></td>
			    </tr>
			</table>
			<asp:Label  ID="lblNoContestMsg" runat="server" Font-Bold="True" ForeColor="ForestGreen" ></asp:Label>
			<asp:Panel ID="pnlEligibleContests" runat="server" Width="100%" BorderStyle="Solid" >
			            <table id="tblEligibleContests" runat="server" width="100%"  >
				<tr  style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;">
			        <td  style="width: 35%;" class="mediumwordingbold" >
			            <asp:Label ID="lblSelectedChild" runat="server" CssClass="smallwordingbold" Width="100%" Font-Bold="True"></asp:Label>
			        </td>
			        <td colspan="2" style="font-weight: bold; color: red; font-size: 11px; width: 50%;"><asp:Label ID="lblWarning2" runat="server" Visible="false"></asp:Label></td> 
				</tr> 
			    <tr style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;">
			        <td style="width: 50%;" class="mediumwordingbold">Children: 
			            <asp:DropDownList ID="ddlChildren" runat="server" AutoPostBack="true" CssClass="mediumwordingbold" Width="50%" DataTextField="First_Name" DataValueField="ChildNumber">
			            </asp:DropDownList>
			        </td>
			        <td  style="width: 35%;" class="mediumwordingbold" align="right">
					    <asp:Button ID="btnAddChild" runat="server" Text="Add Child" Visible="False" />
			        </td>
			        <td  style="width: 15%;" class="mediumwordingbold" align="left"> 
					    <asp:Button ID="btnAddSelectedContests" runat="server" Text="Submit" ForeColor="Red" Font-Bold="true" Width="100%" />
			        </td>
			    </tr>
			    <tr style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;">
			        <td  colspan="3"  class="mediumwordingbold">Use the drop down box to select each child.  For each child, click on the contests desired.  Click on Submit to store your selections on server.</td>
			    </tr>
			    <tr style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;">
					<td class="ItemCenter" colspan="3" style="width: 100%" >
						<ASP:DATAGRID id="dgEligibleContests" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
											<COLUMNS>
												<ASP:TemplateColumn>
												    <ItemTemplate>
												        <asp:CheckBox ID="chkSelectContest" runat="server" Checked="false" />
												    </ItemTemplate>
												</ASP:TemplateColumn>
												<ASP:TEMPLATECOLUMN HeaderText="Contest" >
													<HEADERSTYLE Width="300px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
													<ITEMTEMPLATE><%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
													</ITEMTEMPLATE>
												</ASP:TEMPLATECOLUMN>
												<ASP:BOUNDCOLUMN DataField="ChildNumber" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ContestID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="Fee" HeaderText="Fee" DataFormatString="${0:N2}">
                                                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                </ASP:BOUNDCOLUMN>
                                                <ASP:TemplateColumn HeaderText="Contest Date &amp; Time">
                                                    <HeaderStyle Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
											                <%#DataBinder.Eval(Container.DataItem, "ContestDate", "{0:d}")%> <br />
											                <%#DataBinder.Eval(Container.DataItem, "ContestTime")%> <br />
            											    Reg. Deadline: <%#DataBinder.Eval(Container.DataItem, "RegistrationDeadline", "{0:d}")%>
                                                   </ItemTemplate>
                                                </ASP:TemplateColumn>
												<ASP:BOUNDCOLUMN DataField="ContestCategoryID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="EventID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="EventCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductGroupID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductGroupCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="NSFChapterID" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="RegistrationDeadline" Visible="false" DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="State" Visible="False"></ASP:BOUNDCOLUMN>
												<ASP:BOUNDCOLUMN DataField="City" HeaderText="Contest City"  Visible="true">
                                                    <HeaderStyle Font-Bold="True" ForeColor="White" />
												</ASP:BOUNDCOLUMN>
											</COLUMNS>
											<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
                            <AlternatingItemStyle BackColor="LightBlue" />
										</ASP:DATAGRID>
					</td> 
			    </tr>
				</table>
			        </asp:Panel><br />
			<asp:Panel ID="pnlSelectedContests" runat="server" Width="100%" BorderStyle="Solid" >
			    <table id="tblSelectedContests"  runat="server" width="100%">
			    <tr>
					<td style="font-weight: bold; width: 15%; height: 34px;">Selected Contests:</td> 
					<td style="font-weight: bold; color: red; font-size: 11px;  width: 65%; height: 34px;"><asp:Label ID="lblWarning1" runat="server" Visible="false"></asp:Label></td> 
					<td style="width: 20%; height: 34px;"><asp:Button id="btnPayNow" runat="server" text="Pay Now" Width="100%" Font-Bold="true" ForeColor="Red" /></td> 
				</tr>
				<tr style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;">
			        <td  colspan="3"  class="mediumwordingbold">
                        <asp:Label ID="lblPaynow" runat="server" Text="After selecting contests and  pressing on submit , click on Pay Now to go to the next page." Visible ="false" ></asp:Label></td>
			    </tr>
			    <tr>
			        <td colspan="3" >
						<asp:DataGrid id="dgSelectedContests" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="White"></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
							<Columns>
									<asp:BoundColumn Visible="False" DataField="ChildNumber">
                                        <HeaderStyle Font-Bold="True" ForeColor="White" />
                                    </asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="ContestID">
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="PaymentDate">
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundColumn>
									<asp:BoundColumn Visible="False" DataField="Contestant_ID">
                                        <HeaderStyle Font-Bold="True" />
                                    </asp:BoundColumn>
									<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant">
                                        <HeaderStyle Width="15%"  Font-Bold="True" ForeColor="White" />
                                    </asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Contest">
										<HeaderStyle Width="15%"  Font-Bold="True" ForeColor="White"></HeaderStyle>
										<ItemTemplate>
											<%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Date &amp; Time">
										<HeaderStyle Wrap="False" Font-Bold="True" ForeColor="White" Width="15%" ></HeaderStyle>
										<ItemTemplate>
											    <%#DataBinder.Eval(Container.DataItem, "ContestDate", "{0:d}")%> <br />
											    <%#DataBinder.Eval(Container.DataItem, "ContestTime")%> <br />
											    Reg. Deadline: <%#DataBinder.Eval(Container.DataItem, "RegistrationDeadline", "{0:d}")%>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contact Info">
										<HeaderStyle Width="15%" Font-Bold="True" ForeColor="White"></HeaderStyle>
										<ItemTemplate>
										      <asp:Label ID="lblContactInfo" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<ASP:BOUNDCOLUMN DataField="VolunteerName" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="VolunteerPhone" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="State" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="City" Visible="False"></ASP:BOUNDCOLUMN>
									<asp:TemplateColumn HeaderText="Contest Info">
										<HeaderStyle Width="30%" Font-Bold="True" ForeColor="White"></HeaderStyle>
										<ItemTemplate>
											Payment Info<br />
											Amount : US <%#DataBinder.Eval(Container.DataItem, "Fee", "{0:c}")%><br />
											Date Paid :<%# DataBinder.Eval(Container.DataItem, "PaymentDate", "{0:d}") %><br />
											Reference :<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<ASP:BOUNDCOLUMN DataField="ChapterId" Visible="false"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="RegistrationDeadline" Visible="false" DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
									<asp:TemplateColumn>
										<ItemTemplate>
											<asp:LinkButton id="lbRemoveContest" runat="server" CausesValidation="false" CommandName="Select"
												Text="Remove Contest"></asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
                            <AlternatingItemStyle BackColor="LightBlue" />
						</asp:DataGrid>
			        </td>
			    </tr>
			    <tr>
			        <td colspan="3" ><ASP:LABEL id="lblregFee" runat="server" CssClass="mediumwordingbold">Registration Fee Due (US$) :</ASP:LABEL><ASP:LABEL id="RegFee" runat="server" CssClass="mediumwordingbold"></ASP:LABEL></td>
				</tr>
			    <tr><td colspan="3" class="largewording"><asp:label ID="lblMsg" runat="server" Width="99%" ></asp:label></td></tr>
			            </table>
			</asp:Panel><br />
			<asp:Panel ID="pnlPaidContests" runat="server" Width="100%" BorderStyle="Solid">
			    <table id="tblPaidContests"  runat="server" width="100%">
			        <tr>
					    <td style="font-weight: bold; width: 996px;">Paid Contests:</td> 
				    </tr>
				    <tr>
				        <td style="width: 996px">
						<asp:DataGrid id="dgPaidContests" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="White"></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
							<Columns>
									<asp:BoundColumn DataField="ContestantName" HeaderText="Contestant">
                                        <HeaderStyle Width="15%"  Font-Bold="True" ForeColor="White"  />
                                    </asp:BoundColumn>
									<asp:TemplateColumn HeaderText="Contest">
										<HeaderStyle Width="15%" Font-Bold="True" ForeColor="White"></HeaderStyle>
										<ItemTemplate>
											<%# DataBinder.Eval(Container.DataItem, "ContestDesc") %>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contest Date &amp; Time">
										<HeaderStyle Wrap="False" Font-Bold="True" ForeColor="White" Width="20%" ></HeaderStyle>
										<ItemTemplate>
											    <%#DataBinder.Eval(Container.DataItem, "ContestDate", "{0:d}")%> <br />
											    CheckInTime: <%#DataBinder.Eval(Container.DataItem, "CheckInTime")%> <br />
											    ContestTime: <%#DataBinder.Eval(Container.DataItem, "StartTime")%> - <%#DataBinder.Eval(Container.DataItem, "EndTime")%>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Contact Info">
										<HeaderStyle Width="15%"  Font-Bold="True" ForeColor="White"></HeaderStyle>
										<ItemTemplate>
										      <asp:Label ID="lblContactInfo" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<ASP:BOUNDCOLUMN DataField="VolunteerName" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="VolunteerPhone" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="State" Visible="False"></ASP:BOUNDCOLUMN>
									<ASP:BOUNDCOLUMN DataField="City" Visible="False"></ASP:BOUNDCOLUMN>
									<asp:TemplateColumn HeaderText="Contest Info">
										<HeaderStyle Width="30%"  Font-Bold="True" ForeColor="White"></HeaderStyle>
										<ItemTemplate>
											Payment Info<br />
											Amount : US <%#DataBinder.Eval(Container.DataItem, "Fee", "{0:c}")%><br />
											Date Paid :<%# DataBinder.Eval(Container.DataItem, "PaymentDate", "{0:d}") %><br />
											Reference :<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>
											<br />
											<asp:Label id="lblScore" runat="server"></asp:Label><br />
											<br />
											<asp:HyperLink id="hlDownloadLink" runat="server" Visible="False">Click here to 
download practice words as PDF File</asp:HyperLink><br />
											<asp:HyperLink id="hlDOCDownloadLink" runat="server" Visible="False">Click here to 
download practice words as Word Document</asp:HyperLink>
										</ItemTemplate>
									</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
                            <AlternatingItemStyle BackColor="LightBlue" />
						</asp:DataGrid>
				        </td>
				    </tr>
				</table>
			</asp:Panel>
				
			<table id="Table4" runat="server" width="100%">
				<tr style="border-right: black thin solid; border-top: black thin solid; border-left: black thin solid; border-bottom: black thin solid;">
				    <td class="ItemCenter" colspan="2" align="center"  style="font-size: 10px; color: red; font-family: Verdana, Arial;">
											    Contest Time shown above are general. Please refer to the program schedule for the actual time.<br />
												If the Contest Date is blank, the date will be announced shortly<br />
												If the contest time is displayed as 1/1/1900 12:00:00 AM, that means the time 
												is not determined yet for this contest.<br />
												If no Contests are listed please contact your Chapter Coordinator for further 
												processing 
				    </td>
				</tr>
				<tr>
					<td class="ItemCenter">
					    <asp:hyperlink id="hlinkParent" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parents Functions Page</asp:hyperlink></td>
				</tr>
 				<tr>
					<td >
					    <asp:Label ID="lblDebug" runat="server" Visible="true"></asp:Label></td>
				</tr>          </table>
</asp:Content>


 
 
 