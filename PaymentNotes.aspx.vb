﻿Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class PaymentNotes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If LCase(Session("LoggedIn")) <> "true" Then
            '    Server.Transfer("Maintest.aspx")
            'End If
            If IsPostBack = False Then
                loadEvent()
            End If
        Catch ex As Exception
            Server.Transfer("Maintest.aspx")
        End Try
    End Sub

    Private Sub loadEvent()
        Dim i As Integer
        lstEvent.Items.Clear()
        Dim read1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select * from Event where EventID in (1,2,3,5,11,13,18)")
        i = 1
        While read1.Read()
            Dim litem As New ListItem
            litem.Text = read1("Name")
            litem.Value = read1("EventID")
            lstEvent.Items.Add(litem)
            i = i + 1
        End While
        lstEvent.Items.Insert(0, "All Events")
        lstEvent.SelectedIndex = 0
        read1.Close()
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub

    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            lblErr.Text = ""
            Dim i As Integer
            Dim Eventstr As String = ""
            If Not lstEvent.Items(0).Selected = True Then
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If
            Dim StrSQL As String
            StrSQL = " Select [Last Name] as LName, [First Name] as FName, Address, City, State, Zip, [Contribution Date] as CDate,[Contribution Amount] as CAmount, [Donation Type] as DType, [Payment Date] as PDate,unique_id, event_for,asp_session_id, MealsAmount,LateFee, EventId, MemberId, DonorType, ChapterId, Fee,"
            StrSQL = StrSQL & "CONVERT(Money,  SubString(PaymentNotes,CHARINDEX ('RegFee:',PaymentNotes)+7, (CHARINDEX ('Mealsamount',paymentNotes)-(CHARINDEX ('RegFee:',PaymentNotes)+7 )))) as ParsedFee, TotalPayment, EventYear, PaymentNotes,CalcpaymentsNotes, MS_TransDate, Brand from  NFG_Transactions"
            StrSQL = StrSQL & "  WHERE PaymentNotes is not null " & Eventstr & " AND CHARINDEX ('RegFee:',PaymentNotes) > 0 AND CONVERT(Money,  SubString(PaymentNotes,CHARINDEX ('RegFee:',PaymentNotes)+7, (CHARINDEX ('Mealsamount',paymentNotes)-(CHARINDEX ('RegFee:',PaymentNotes)+7 ))))  "

            If ddlCategory.SelectedValue = "2" Then
                StrSQL = StrSQL & " <> "
            Else
                StrSQL = StrSQL & " = "
            End If
            StrSQL = StrSQL & " Fee AND [Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                GVPaymentNotes.DataSource = ds
                GVPaymentNotes.DataBind()
            Else
                GVPaymentNotes.DataSource = Nothing
                GVPaymentNotes.DataBind()
                lblErr.Text = "No records with Payment Notes record to show"
                Exit Sub
            End If
        End If

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy fromat"
            Exit Sub
        ElseIf ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            lblErr.Text = ""
            Dim i As Integer
            Dim Eventstr As String = ""
            If Not lstEvent.Items(0).Selected = True Then
                For i = 1 To lstEvent.Items.Count - 1
                    If lstEvent.Items(i).Selected = True And Eventstr = "" Then
                        Eventstr = "And Eventid in (" & lstEvent.Items(i).Value.ToString()
                    ElseIf lstEvent.Items(i).Selected = True Then
                        Eventstr = Eventstr & ", " & lstEvent.Items(i).Value.ToString()
                    End If
                Next
                If Not Eventstr = "" Then
                    Eventstr = Eventstr & ")"
                End If
            End If

            Dim StrSQL As String
            StrSQL = " Select [Last Name], [First Name], Address, City, State, Zip, [Contribution Date],[Contribution Amount], [Donation Type], [Payment Date],unique_id, event_for,asp_session_id, MealsAmount,LateFee, EventId, MemberId, DonorType, ChapterId, Fee,"
            StrSQL = StrSQL & "CONVERT(Money,  SubString(PaymentNotes,CHARINDEX ('RegFee:',PaymentNotes)+7, (CHARINDEX ('Mealsamount',paymentNotes)-(CHARINDEX ('RegFee:',PaymentNotes)+7 )))) as ParsedFee, TotalPayment, EventYear, PaymentNotes,CalcpaymentsNotes, MS_TransDate, Brand from  NFG_Transactions"
            StrSQL = StrSQL & "  WHERE PaymentNotes is not null " & Eventstr & " AND CHARINDEX ('RegFee:',PaymentNotes) > 0 AND CONVERT(Money,  SubString(PaymentNotes,CHARINDEX ('RegFee:',PaymentNotes)+7, (CHARINDEX ('Mealsamount',paymentNotes)-(CHARINDEX ('RegFee:',PaymentNotes)+7 ))))  "

            If ddlCategory.SelectedValue = "2" Then
                StrSQL = StrSQL & " <> "
            Else
                StrSQL = StrSQL & " = "
            End If
            StrSQL = StrSQL & " Fee AND [Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim TempGrid As New GridView
                TempGrid.DataSource = ds
                TempGrid.DataBind()
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=" & ddlCategory.SelectedItem.Text.Replace(" ", "") & ".xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                TempGrid.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.[End]()
            Else
                lblErr.Text = "No record with Payment Notes record to show"
                Exit Sub
            End If
        End If
    End Sub
End Class
