Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL


Namespace VRegistration

Partial Class ChapterMain
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("LoggedIn") <> "LoggedIn" Then
            Server.Transfer("login.aspx")
        End If
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim objChapter As New Chapter
        If Not Session("ChapterID") Is Nothing Then
            objChapter.GetChapterByID(conn.ConnectionString, Session("ChapterID"))
            With objChapter
                lblChapterName.Text = .ChapterCity
                lblChapterCity.Text = .ChapterCity & ", " & .ChapterState & " - " & .ChapterZip
                lblCoordinatorName.Text = .CoordinatorName
                lblContactPhone.Text = .ChapterPhone
            End With
        End If
    End Sub
End Class

End Namespace

