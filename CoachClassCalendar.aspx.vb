﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Collections.Generic

Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml

Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports System.Web.Script.Serialization
Imports VRegistration


Partial Class CoachClassCalendar
    Inherits System.Web.UI.Page
    Dim cmdText As String
    Dim CancelDateArr As ArrayList = New ArrayList()


    Public apiKey As String = "6sTMyAgRSpCmTSIJIWFP8w"
    Public apiSecret As String = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 89
        'Session("LoginID") = 51336
        'Session("LoggedIn") = "true"
        lblErr.Text = ""
        Response.Redirect("~/CoachClassCalNew.aspx")
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                '  Response.Redirect("~/maintest.aspx")

            End If

            If Page.IsPostBack = False Then


                'If Session("LoginID").ToString() = "51336" Then

                Dim roleID As String = String.Empty
                Try


                    roleID = Session("RoleID").ToString()
                    hdnRoleID.Value = roleID
                Catch ex As Exception
                    roleID = 0
                End Try

                LoadYear()
                loadPhase()
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Parent Functions"
                    hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
                ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Response.Redirect("~/login.aspx?entry=v")
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "88") Then
                    FillEvent()
                    If Session("RoleId").ToString() <> "88" Then
                        FillCoach(ddlCoach)
                    Else
                        FillCoach(ddlCoach)
                        ' FillProductGroup()
                    End If

                    If (Request.QueryString("coachID") Is Nothing) Then
                    Else

                        Dim coachID As String = Request.QueryString("coachID").ToString()
                        Dim semester As String = Request.QueryString("Semester").ToString()
                        ddlPhase.SelectedValue = semester
                        FillCoach(ddlCoach)
                        ddlCoach.SelectedValue = coachID
                        FillProductGroup()
                    End If

                Else
                    Response.Redirect("~/maintest.aspx")
                End If
                'Else
                '    lblMsg.ForeColor = Color.Red
                '    lblMsg.Text = "Coach Class Calendar function is currently not available."
                'End If
            Else
                FillDate()
            End If
        Catch ex As Exception
            Response.Redirect("~/maintest.aspx")
        End Try
    End Sub

    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)

        ddlYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        For i As Integer = 0 To 4
            ddlYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))


        Next
        ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
    End Sub
    Private Sub loadPhase()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2
            ddlPhase.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlPhase.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue)
    End Sub


    ' Add events in dropdown for adding and updating
    Private Sub FillEvent()
        ddlEvent.Items.Add(New ListItem("Coaching", "13"))
    End Sub

    ' Get product group details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProductGroup()

        Dim dsProductGrp As DataSet
        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then
            cmdText = " select distinct p.ProductGroupID,p.ProductGroupCode,p.Name from calsignup c inner join ProductGroup p on c.ProductGroupID=p.ProductGroupID where c.eventid =13 and c.EventYear=" & ddlYear.SelectedItem.Value & " and c.MemberID='" & ddlCoach.SelectedValue & "' and c.ProductId is not Null and c.Accepted='Y' and c.Semester='" & ddlPhase.SelectedValue & "' Order by ProductGroupId"
        Else 'If Session("RoleId") = 89 Then
            cmdText = " select distinct p.ProductGroupID,p.ProductGroupCode,p.Name from calsignup c inner join ProductGroup p on c.ProductGroupID=p.ProductGroupID where c.eventid =13 and c.EventYear=" & ddlYear.SelectedItem.Value & " and c.Memberid=" & Session("LoginID") & " and c.Accepted='Y' and c.Semester='" & ddlPhase.SelectedValue & "'  Order by ProductGroupId"

        End If
        dsProductGrp = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ViewState("PrdGroup") = dsProductGrp.Tables(0)

        ddlProductGroup.DataSource = dsProductGrp.Tables(0)
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Enabled = True
            ddlProductGroup.Items.Insert(0, "Select Product Group")
        Else
            ddlProductGroup.SelectedIndex = 0
            ddlProductGroup.Enabled = False
            ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
        End If
        '
    End Sub
    ' Get product details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProduct()
        If ddlProductGroup.SelectedValue <> "Select Product Group" Then
            If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or (Session("RoleId").ToString() = "89") Then
                cmdText = " select distinct p.ProductID, p.ProductCode, p.Name from calsignup c inner join Product p on c.ProductID=p.ProductID where c.eventid =13 and c.eventyear=" & ddlYear.SelectedItem.Value & " and c.ProductGroupId =" & ddlProductGroup.SelectedItem.Value & " and C.MemberID='" & ddlCoach.SelectedValue & "' and c.ProductId is not Null and c.Accepted='Y' and c.Semester='" & ddlPhase.SelectedValue & "' Order by ProductId"
            Else
                cmdText = " select distinct p.ProductID, p.ProductCode, p.Name from calsignup c inner join Product p on c.ProductID=p.ProductID where c.eventid =13 and c.eventyear=" & ddlYear.SelectedItem.Value & " and c.Memberid=" & Session("LoginID") & " and c.ProductGroupId=" & ddlProductGroup.SelectedValue & " and c.ProductId is not Null and c.Accepted='Y' and c.Semester='" & ddlPhase.SelectedValue & "'  Order by ProductId"
            End If
            Dim dsProduct As DataSet
            dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ddlProduct.DataSource = dsProduct.Tables(0)
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 1 Then
                ddlProduct.Enabled = True
                ddlProduct.Items.Insert(0, "Select Product")
            Else
                ddlProduct.SelectedIndex = 0
                ddlProduct.Enabled = False
                loadLevel()
            End If
        End If
    End Sub
    Private Sub FillCoachByCondition(PrdGroupId As Integer, PrdId As Integer, ctrl As DropDownList)
        Dim dsCoach As DataSet
        cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C "
        cmdText = cmdText & " inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & PrdGroupId & " and C.ProductId=" & PrdId & " and C.EventYear=" & ddlYear.SelectedValue & "  and I.AutomemberId<>" & ddlCoach.SelectedItem.Value & " order by firstname,lastname"
        'End If
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)

        If dsCoach.Tables(0).Rows.Count > 0 Then
            ctrl.DataSource = dsCoach.Tables(0)
            ctrl.DataBind()
            ctrl.Items.Insert(0, New ListItem("[Coach Name]", "Null"))
            ctrl.SelectedIndex = 0
        End If
    End Sub
    Private Sub FillCoach(ddl As DropDownList)
        ddl.Items.Clear()
        Dim dsCoach As DataSet
        If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or Session("RoleId") = 89 Then
            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from calsignup C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.Accepted='Y' and C.EventYear=" & ddlYear.SelectedValue & " and C.Semester='" & ddlPhase.SelectedValue & "' order by firstname, lastname"
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If dsCoach.Tables(0).Rows.Count > 0 Then
                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                If ddl.Items.Count > 0 Then
                    ddl.Items(0).Selected = True
                    If ddl.Items.Count = 1 Then
                        ddl.Enabled = False
                        FillProductGroup()
                    Else
                        ddl.Items.Insert(0, New ListItem("[Coach Name]", "Null"))
                        ddl.SelectedIndex = 0
                        ddl.Enabled = True
                    End If
                End If
            End If
        Else
            cmdText = " select distinct I.AutoMemberId as MemberId, I.FirstName + ' ' + I.LastName as CoachName, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" & Session("LoginID")
            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdText)
            If dsCoach.Tables(0).Rows.Count > 0 Then
                ddl.DataSource = dsCoach.Tables(0)
                ddl.DataBind()
                ddl.Enabled = False
                FillProductGroup()
            End If
        End If
    End Sub
    Private Sub BindCoachClassDetailsInRepeater()
        cmdText = "select count(*) from calsignup  where eventyear=" & hdTable1Year.Value & " and eventid=13 and Accepted='y' and MemberId= " & hdMemberId.Value & " and ProductGroupId= " & ddlProductGroup.SelectedItem.Value & " and  Productid=" & ddlProduct.SelectedItem.Value & " and Semester='" & ddlPhase.SelectedItem.Value & "' and Level='" & ddlLevel.SelectedValue & "' and SessionNO=" & ddlSessionNo.SelectedValue & ""
        If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
            Dim ds As DataSet
            cmdText = "select IP.Firstname +' ' +IP.lastname as Coachname, IP.Email,C.memberID, ProductGroupId,ProductGroupCode,UserID,PWD,VRoom,(SELECT Name FROM PRODUCTGROUP pg WHERE pg.ProductGroupId=c.ProductGroupId) ProductGroupName,Productid, (SELECT Name FROM PRODUCT p WHERE p.ProductId=c.ProductId) ProductName ,ProductCode, Semester,level,sessionno from calsignup c inner join Indspouse IP on (C.memberID=IP.AutoMemberID) where eventyear=" & hdTable1Year.Value & " and eventid=13 and Accepted='y' and C.MemberId= " & hdMemberId.Value & " and productGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Level='" & ddlLevel.SelectedValue & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' group by ProductGroupId,ProductGroupCode,UserID,PWD,VRoom,Productid,ProductCode, Semester,level,sessionno, IP.Firstname,IP.Lastname, IP.Email, C.MemberID "
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                rptCoachClass.DataSource = ds.Tables(0)
                rptCoachClass.DataBind()
            End If
        Else
            rptCoachClass.DataSource = Nothing
            lblMsg.ForeColor = Color.Red
            lblMsg.Text = "No record exists"
        End If
    End Sub
    'Fill product for each selected product group
    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Try
            ddlProduct.Items.Clear()
            FillProduct()
            If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 96 Or Session("RoleId") = 89 Then
            Else
                'FillCoach(ddlCoach)
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged
        Try
            If ddlProduct.SelectedValue <> "0" And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue.ToLower() <> "select product" Then
                loadLevel()
                LoadSessionNo()
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            divTable1.Visible = True
            dvColorStatus.Visible = True
            dvPracticeSection.Visible = True
            ViewState("Date") = Nothing
            Reset()
            rptCoachClass.DataSource = Nothing
            If ddlYear.SelectedValue = String.Empty Then
                ShowMessage("Select Year", True)
            ElseIf ddlProductGroup.Items.Count = 0 Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlProduct.Items.Count = 0 Then
                ShowMessage("Select Product", True)
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                ShowMessage("Select Product", True)
            ElseIf ddlPhase.SelectedItem.Text = "Select Semester" Then
                ShowMessage("Select Semester", True)
            ElseIf ddlCoach.Items.Count = 0 Then
                ShowMessage("Select Coach", True)
            ElseIf ddlCoach.SelectedItem.Text = "[Coach Name]" Then
                ShowMessage("Select Coach", True)

            Else
                rptCoachClass.DataSource = Nothing
                rptCoachClass.DataBind()
                hdMemberId.Value = ddlCoach.SelectedItem.Value
                hdTable1Year.Value = ddlYear.SelectedItem.Value
                BindCoachClassDetailsInRepeater()
                divTable1.Visible = True
                Dim indx As Integer, iVisible As Integer = 0
                For indx = 0 To rptCoachClass.Items.Count - 1
                    If rptCoachClass.Items(indx).Visible = True Then
                        iVisible = 1
                        Exit For
                    End If
                Next
                If iVisible = 0 Then
                    lblMsg.ForeColor = Color.Red
                    lblMsg.Text = "No record exists"
                    BtnAddUpdateMakeupSession.Visible = False
                    btnAddUpdatePractiseSession.Visible = False
                    dvPractiseInfo.Visible = False


                Else
                    BtnAddUpdateMakeupSession.Visible = False
                    btnAddUpdatePractiseSession.Visible = True

                    Dim cmdText As String = String.Empty
                    Dim practiceDay As String = String.Empty
                    Dim practiceDate As String = String.Empty
                    Dim practiseTime As String = String.Empty

                    cmdText = "select Day, StartDate, BeginTime from WebConfLog where memberID=" & ddlCoach.SelectedValue & " and SessionType='Practice' and EventYear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & ""

                    Dim ds As DataSet = New DataSet()
                    ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If ds.Tables(0).Rows.Count > 0 Then
                        practiceDay = ds.Tables(0).Rows(0)("Day").ToString()
                        practiceDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToShortDateString()
                        practiseTime = ds.Tables(0).Rows(0)("BeginTime").ToString()
                        dvPractiseInfo.Visible = True

                        spnDay.InnerText = practiceDay
                        spnDate.InnerText = practiceDate
                        spnTime.InnerText = practiseTime
                    Else
                        dvPractiseInfo.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            'Throw New Exception(ex.Message)
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub
    Private Sub Reset()
        ' divUpdate.Visible = False
        lblMsg.Text = ""
    End Sub
    'Error or Success message function
    Private Sub ShowMessage(msg As String, bIsError As Boolean)
        lblMsg.Text = msg
        lblMsg.ForeColor = Color.Blue
        If bIsError = True Then
            lblMsg.ForeColor = Color.Red
        End If
    End Sub
    Private Function GetDates(ByVal StartDate As DateTime, ByVal EndDate As DateTime, ByVal Day As String) As List(Of DateTime)
        Dim dates As New List(Of DateTime)
        Dim SelectedDays As New List(Of DayOfWeek)
        Select Case Day.ToLower
            Case "su"
                SelectedDays.Add(DayOfWeek.Sunday)
            Case "mo"
                SelectedDays.Add(DayOfWeek.Monday)
            Case "tu"
                SelectedDays.Add(DayOfWeek.Tuesday)
            Case "we"
                SelectedDays.Add(DayOfWeek.Wednesday)
            Case "th"
                SelectedDays.Add(DayOfWeek.Thursday)
            Case "fr"
                SelectedDays.Add(DayOfWeek.Friday)
            Case "sa"
                SelectedDays.Add(DayOfWeek.Saturday)
            Case Else
                ' ...unexpected day of week...
                ' do something here?
        End Select
        Dim dt As DateTime = StartDate.Date
        While dt <= EndDate
            If SelectedDays.Contains(dt.DayOfWeek) Then
                dates.Add(dt)
                Exit While
            End If
            dt = dt.AddDays(1)
        End While
        Return dates
    End Function
    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs)
        lblMsg.Text = ""
        'Casting sender to Dropdown
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)
        'Row of the Grid from where the SelectedIndex change event is fired.
        Dim row As GridViewRow = TryCast(ddl.NamingContainer, GridViewRow)
        ' Get object of Repeater control 
        Dim selIndex As Integer = 0
        Dim selectedIndex As Integer = 0
        selIndex = row.RowIndex
        selectedIndex = row.RowIndex
        Dim rptItem As RepeaterItem = TryCast(ddl.NamingContainer.NamingContainer.NamingContainer, RepeaterItem)
        Dim hdProductGrp As HiddenField = TryCast(rptItem.FindControl("hdProductGrp"), HiddenField)
        Dim hdProduct As HiddenField = TryCast(rptItem.FindControl("hdProduct"), HiddenField)
        Dim lblPhase As Label = TryCast(rptItem.FindControl("lblPhase"), Label)
        Dim lblSessionNo As Label = TryCast(rptItem.FindControl("lblSessionNo"), Label)
        Dim lblLevel As Label = TryCast(rptItem.FindControl("lblLevel"), Label)
        Dim lblDay As Label = TryCast(row.FindControl("lblDay"), Label)
        Dim ddlSubstitute As DropDownList = TryCast(row.FindControl("ddlSubstitute"), DropDownList)
        Dim ddlReason As DropDownList = TryCast(row.FindControl("ddlReason"), DropDownList)
        Dim ddlStatus As DropDownList = TryCast(row.FindControl("ddlStatus"), DropDownList)
        Dim scheduleType As String = TryCast(row.FindControl("lblScheduleType"), Label).Text
        'If Len(Trim(lblCoachClassCalID.Text)) = 0 Then
        Dim hdStatus As HiddenField = TryCast(row.FindControl("hdStatus"), HiddenField)
        Dim hdnWeek As Label = TryCast(row.FindControl("lblWeek"), Label)


        Dim grdView As GridView = rptItem.FindControl("rpt_grdCoachClassCal")
        If selIndex = 0 Then
            selIndex = 0
        ElseIf selIndex > 0 Then
            selIndex = selIndex - 1
        End If
        Dim prevWeekNo As Integer = 0
        'If TryCast(grdView.Rows(selIndex).FindControl("lblWeek"), Label).Text = "" Then
        '    prevWeekNo = 0
        'Else
        '    prevWeekNo = TryCast(grdView.Rows(selIndex).FindControl("lblWeek"), Label).Text
        'End If

        Dim lblHWDate As Label = TryCast(row.FindControl("lblHWDueDate"), Label)
        Dim lblRelDate As Label = TryCast(row.FindControl("lblARelDate"), Label)
        Dim lblSRelDate As Label = TryCast(row.FindControl("lblSRelDate"), Label)
        Dim lblHomeDate As Label = TryCast(row.FindControl("lblHWRelDate"), Label)

        Dim imgHWDate As ImageButton = TryCast(row.FindControl("imgHWRelDate"), ImageButton)
        Dim imgRelDate As ImageButton = TryCast(row.FindControl("imgHWDueDate"), ImageButton)
        Dim imgSRelDate As ImageButton = TryCast(row.FindControl("imgARelDate"), ImageButton)
        Dim imgHomeDate As ImageButton = TryCast(row.FindControl("imgSRelDate"), ImageButton)

        cmdText = "select isnull( max(weekno),0) from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Semester='" + lblPhase.Text + "' and SessionNo=" + lblSessionNo.Text + " and [Status]  in ('On','Substitute','Makeup') and Level = '" & ddlLevel.SelectedValue & "'"

        Dim cntWeek As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        Dim lblWeek As Label = CType(row.FindControl("lblWeek"), Label)

        If hdnWeek.Text <> "" And hdnWeek.Text <> "0" Then

            lblWeek.Text = TryCast(grdView.Rows(selectedIndex).FindControl("lblWeek"), Label).Text


        ElseIf hdStatus.Value <> "On" And hdStatus.Value <> "Substitute" And hdStatus.Value <> "Makeup" Then
            lblWeek.Text = cntWeek + 1
            If (ddl.SelectedItem.Text.ToLower = "on" Or ddl.SelectedItem.Text.ToLower = "substitute" Or ddl.SelectedItem.Text.ToLower = "makeup" Or ddl.SelectedItem.Text = "Not set") Then
                lblWeek.Text = cntWeek + 1
                ' lblWeek.Text = prevWeekNo + 1
            ElseIf ddl.SelectedItem.Text.ToLower = "cancelled" Or ddl.SelectedItem.Text.ToLower = "makeup cancelled" Or ddl.SelectedItem.Text.ToLower = "substitute cancelled" Then

                lblWeek.Text = cntWeek + 1

                'Dim cmdText As String = String.Empty
                'cmdText = "select count(*) as CountSet from CoachClassCal where  EventYear=" & ddlYear.SelectedValue & " and MemberID=" & ddlCoach.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Level='" & lblLevel.Text & "' and SessionNo=" & lblSessionNo.Text & " and (Status='On' or Status='Makeup' or Status='Substitute')"
                'Dim ds As DataSet = New DataSet()
                'ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                'If ds.Tables(0).Rows.Count > 0 Then
                '    If (TryCast(grdView.Rows(selIndex + 1).FindControl("lblWeek"), Label).Text <> "") Then
                '        lblWeek.Text = TryCast(grdView.Rows(selIndex + 1).FindControl("lblWeek"), Label).Text + 1
                '    Else

                '        lblWeek.Text = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString()) + 1
                '    End If


                'End If

                If ddl.SelectedItem.Text.ToLower = "makeup cancelled" Or ddl.SelectedItem.Text.ToLower = "substitute cancelled" Then
                    lblWeek.Text = cntWeek
                End If
            End If
            '' validate
            cmdText = "select classes from EventFees Where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId=" & hdProduct.Value
            Dim iCntClasses As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            'If iCntClasses < CInt(lblWeek.Text) Then
            '    lblMsg.ForeColor = Color.Red
            '    lblMsg.Text = "Week # cannot exceed the limitation"
            'End If
        ElseIf ddl.SelectedItem.Text.ToLower = "makeup cancelled" Or ddl.SelectedItem.Text.ToLower = "substitute cancelled" Or ddl.SelectedItem.Text.ToLower = "cancelled" Or ddl.SelectedItem.Text.ToLower = "on" Or ddl.SelectedItem.Text.ToLower = "substitute" Or ddl.SelectedItem.Text.ToLower = "makeup" Or ddl.SelectedItem.Text = "Not set" Then

            lblWeek.Text = TryCast(grdView.Rows(selIndex).FindControl("lblWeek"), Label).Text
            'lblWeek.Text = prevWeekNo + 1


        End If

        If ddlStatus.SelectedItem.Text = "Cancelled" Or ddlStatus.SelectedItem.Text = "Not set" Or ddlStatus.SelectedItem.Text = "Substitute Cancelled" Or ddlStatus.SelectedItem.Text = "Makeup Cancelled" Then
            lblHWDate.Visible = False
            lblRelDate.Visible = False
            lblSRelDate.Visible = False
            lblHomeDate.Visible = False
            imgHWDate.Visible = False
            imgRelDate.Visible = False
            imgSRelDate.Visible = False
            imgHomeDate.Visible = False
            If ddl.SelectedItem.Text.ToLower = "substitute cancelled" Then
                ddlReason.SelectedValue = ""
                ddlSubstitute.SelectedValue = "Null"
            End If

        Else
            lblHWDate.Visible = True
            lblRelDate.Visible = True
            lblSRelDate.Visible = True
            lblHomeDate.Visible = True

            imgHWDate.Visible = True
            imgRelDate.Visible = True
            imgSRelDate.Visible = True
            imgHomeDate.Visible = True
        End If
        If ddlStatus.SelectedItem.Text = "On" Or ddlStatus.SelectedItem.Text = "Not set" Then
            ddlReason.SelectedValue = ""
        End If
        Try


            If ((hdStatus.Value = "Cancelled" Or hdStatus.Value = "Substitute Cancelled" Or hdStatus.Value = "Makeup Cancelled") And (ddlStatus.SelectedValue = "On" Or ddlStatus.SelectedValue = "Makeup") Or ddlStatus.SelectedValue = "Substitute") Then
                Dim lblDate As Label = TryCast(row.FindControl("lblDate"), Label)
                Dim strDate As String = lblDate.Text
                Dim strHWDate As String = String.Empty
                Dim strHWDueDate As String = String.Empty
                Dim strADate As String = String.Empty
                Dim strSDate As String = String.Empty

                Dim dtClassDate As DateTime = Convert.ToDateTime(strDate)
                strHWDate = dtClassDate.ToShortDateString()
                strHWDueDate = dtClassDate.AddDays(5).ToShortDateString()
                strADate = dtClassDate.AddDays(7).ToShortDateString()
                strSDate = dtClassDate.AddDays(-1).ToShortDateString()

                lblHomeDate.Text = strHWDate
                lblHWDate.Text = strHWDueDate
                lblRelDate.Text = strADate
                lblSRelDate.Text = strSDate
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
        If ddl.SelectedItem.Value = "Substitute" Then : ddlSubstitute.Enabled = True : Else : ddlSubstitute.Enabled = False : End If
        If ddl.SelectedItem.Value = "Substitute" Or ddl.SelectedItem.Value = "Cancelled" Or ddl.SelectedItem.Value = "Makeup" Or ddl.SelectedItem.Value = "Makeup Cancelled" Or ddl.SelectedItem.Value = "Substitute Cancelled" Then : ddlReason.Enabled = True : Else : ddlReason.Enabled = False : End If
    End Sub
    Protected Sub ddlGoTo_SelectedIndexChanged(sender As Object, e As EventArgs)
        'Row of the Grid from where the SelectedIndex change event is fired.
        Dim rptItem As RepeaterItem = TryCast(sender.NamingContainer, RepeaterItem)
        Dim ddlGoTo As DropDownList = TryCast(sender, DropDownList)
        Dim grd As GridView = TryCast(rptItem.FindControl("rpt_grdCoachClassCal"), GridView)
        Dim rep_hdWeekCnt As HiddenField = TryCast(rptItem.FindControl("rep_hdWeekCnt"), HiddenField)
        Dim rep_lnkBtnShowNextWeek As LinkButton = TryCast(rptItem.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
        If ddlGoTo.SelectedItem.Value <> -1 Then
            rep_hdWeekCnt.Value = ddlGoTo.SelectedItem.Value
            hdnTotalClass.Value = ddlGoTo.SelectedItem.Value
            BindCoachClassDetailsInGrid(grd, 0)
            If rep_hdWeekCnt.Value = (ddlGoTo.Items.Count - 1) Then : rep_lnkBtnShowNextWeek.Enabled = False
            Else : rep_lnkBtnShowNextWeek.Enabled = True
            End If


        End If
    End Sub
    Private Sub FillDate()
        Try
            If Not ViewState("Date") Is Nothing Then
                Dim list As New Dictionary(Of String, String)
                list = ViewState("Date")
                For Each pair As KeyValuePair(Of String, String) In list
                    ' Get key.
                    Dim key As String = pair.Key
                    ' Get value.
                    Dim value As String = pair.Value
                    ' Display.
                    TryCast(Page.FindControl(key), Label).Text = TryCast(Page.FindControl(value), HiddenField).Value
                Next
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub
    Protected Sub rpt_grdCoachClassCal_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try


            If e.CommandName <> "Modify" And e.CommandName <> "UpdateCoach" And e.CommandName <> "CancelCoach" Then Exit Sub
            Dim gvPrevRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

            Dim grd As GridView = DirectCast(sender, GridView)
            Dim RowIndex As Integer = gvPrevRow.RowIndex
            Dim prevIndex As Integer = 0
            Dim curStatus As String = String.Empty
            Dim curReason As String = String.Empty
            Dim prevStatus As String = String.Empty
            Dim prevReason As String = String.Empty

            hdnSelIndex.Value = RowIndex
            If RowIndex = 0 Then
                prevIndex = 0

            Else
                prevIndex = RowIndex - 1

            End If
            Dim status As String = String.Empty
            Try
                status = DirectCast(grd.Rows(prevIndex).Cells(7).FindControl("ddlStatus"), DropDownList).SelectedItem.Text
            Catch ex As Exception
                status = ""
            End Try

            If RowIndex = 0 Then
                status = ""
            End If
            Dim weekSeq As String = "0"
            If (RowIndex > 0) Then

                weekSeq = DirectCast(grd.Rows(prevIndex).Cells(15).FindControl("lblWeek"), Label).Text
                prevStatus = DirectCast(grd.Rows(prevIndex).Cells(15).FindControl("ddlStatus"), DropDownList).SelectedValue
                prevReason = DirectCast(grd.Rows(prevIndex).Cells(15).FindControl("ddlReason"), DropDownList).SelectedValue
            Else
                weekSeq = "1"
            End If
            curStatus = DirectCast(grd.Rows(RowIndex).Cells(15).FindControl("ddlStatus"), DropDownList).SelectedValue
            curReason = DirectCast(grd.Rows(RowIndex).Cells(17).FindControl("ddlReason"), DropDownList).SelectedValue
            If (weekSeq <> "" Or (curStatus <> "Select" And curReason <> "Holiday" And weekSeq <> "")) Then

                Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim todaydate As DateTime = DateTime.Now.ToShortDateString()
                Dim classDate As String = DirectCast(gvRow.Cells(1).FindControl("lblDate"), Label).Text

                Dim ddlStatus As DropDownList = DirectCast(gvRow.Cells(7).FindControl("ddlStatus"), DropDownList)
                Dim ddlSubstitute As DropDownList = DirectCast(gvRow.Cells(8).FindControl("ddlSubstitute"), DropDownList)
                Dim ddlReason As DropDownList = DirectCast(gvRow.Cells(9).FindControl("ddlReason"), DropDownList)

                Dim btnModify As Button = DirectCast(gvRow.Cells(0).FindControl("btnModify"), Button)
                Dim btnUpdate As Button = DirectCast(gvRow.Cells(0).FindControl("btnUpdate"), Button)
                Dim btnCancel As Button = DirectCast(gvRow.Cells(0).FindControl("btnCancel"), Button)

                Dim lblSignUpId As Label = DirectCast(gvRow.Cells(1).FindControl("lblSignUpId"), Label)
                Dim lblCoachClassCalID As Label = DirectCast(gvRow.Cells(1).FindControl("lblCoachClassCalID"), Label)
                Dim lblDate As Label = DirectCast(gvRow.Cells(1).FindControl("lblDate"), Label)
                Dim lblTime As Label = DirectCast(gvRow.Cells(1).FindControl("lblTime"), Label)

                Dim SerNo As Integer = DirectCast(gvRow.Cells(5).FindControl("lblSerial"), Label).Text
                Dim WeekSerNo As Integer = DirectCast(gvRow.Cells(6).FindControl("lblWeekNo"), Label).Text
                Dim WeekNo As String = DirectCast(gvRow.Cells(15).FindControl("lblWeek"), Label).Text

                Dim lblHWRelDate As Label = DirectCast(gvRow.Cells(10).FindControl("lblHWRelDate"), Label)
                Dim lblHWDueDate As Label = DirectCast(gvRow.Cells(11).FindControl("lblHWDueDate"), Label)
                Dim lblARelDate As Label = DirectCast(gvRow.Cells(12).FindControl("lblARelDate"), Label)
                Dim lblSRelDate As Label = DirectCast(gvRow.Cells(12).FindControl("lblSRelDate"), Label)

                Dim hdHWRelDates As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdHWRelDates"), HiddenField)
                Dim hdHWDueDate As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdHWDueDate"), HiddenField)
                Dim hdARelDate As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdARelDate"), HiddenField)
                Dim hdSRelDate As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdSRelDate"), HiddenField)
                Dim hdHideModify As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdHideModify"), HiddenField)
                Dim imgHWRelDate As ImageButton = DirectCast(gvRow.Cells(10).FindControl("imgHWRelDate"), ImageButton)
                Dim imgHWDueDate As ImageButton = DirectCast(gvRow.Cells(11).FindControl("imgHWDueDate"), ImageButton)
                Dim imgARelDate As ImageButton = DirectCast(gvRow.Cells(12).FindControl("imgARelDate"), ImageButton)
                Dim imgSRelDate As ImageButton = DirectCast(gvRow.Cells(12).FindControl("imgSRelDate"), ImageButton)

                If e.CommandName = "Modify" Then
                    Dim list As New Dictionary(Of String, String)
                    If Not ViewState("Date") Is Nothing Then : list = ViewState("Date") : End If

                    Try


                        list.Remove(lblHWRelDate.UniqueID)
                        list.Remove(lblHWDueDate.UniqueID)
                        list.Remove(lblARelDate.UniqueID)
                        list.Remove(lblSRelDate.UniqueID)

                        list.Remove(hdHWRelDates.UniqueID)
                        list.Remove(hdHWDueDate.UniqueID)
                        list.Remove(hdARelDate.UniqueID)
                        list.Remove(hdSRelDate.UniqueID)


                    Catch ex As Exception

                    End Try

                    list.Add(lblHWRelDate.UniqueID, hdHWRelDates.UniqueID)
                    list.Add(lblHWDueDate.UniqueID, hdHWDueDate.UniqueID)
                    list.Add(lblARelDate.UniqueID, hdARelDate.UniqueID)
                    list.Add(lblSRelDate.UniqueID, hdSRelDate.UniqueID)

                    hdHWRelDates.Value = lblHWRelDate.Text
                    hdHWDueDate.Value = lblHWDueDate.Text
                    hdARelDate.Value = lblARelDate.Text
                    hdSRelDate.Value = lblSRelDate.Text
                    hdnorgStatus.Value = DirectCast(gvRow.Cells(7).FindControl("ddlStatus"), DropDownList).SelectedValue
                    ViewState("Date") = list
                    ddlStatus.Enabled = True
                    If ddlStatus.SelectedItem.Text = "Substitute" Then
                        ddlReason.Enabled = True
                        ddlSubstitute.Enabled = True
                    End If
                    If ddlStatus.SelectedItem.Text = "Substitute Cancelled" Or ddlStatus.SelectedItem.Text = "Makeup Cancelled" Or ddlStatus.SelectedItem.Text = "Cancelled" Then
                        ddlReason.Enabled = True

                    End If

                    btnModify.Visible = False
                    btnUpdate.Visible = True
                    btnCancel.Visible = True
                    imgHWRelDate.Visible = True
                    imgHWDueDate.Visible = True
                    imgARelDate.Visible = True
                    imgSRelDate.Visible = True

                    imgHWRelDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblHWRelDate.ClientID & "','" & hdHWRelDates.ClientID & "'); return false;")
                    imgHWDueDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblHWDueDate.ClientID & "','" & hdHWDueDate.ClientID & "'); return false;")
                    imgARelDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblARelDate.ClientID & "','" & hdARelDate.ClientID & "'); return false;")
                    imgSRelDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblSRelDate.ClientID & "','" & hdSRelDate.ClientID & "'); return false;")
                    If ddlStatus.SelectedItem.Text = "Cancelled" Or ddlStatus.SelectedItem.Text = "Makeup Cancelled" Or ddlStatus.SelectedItem.Text = "Substitute Cancelled" Then
                        ddlReason.Enabled = True

                        lblHWRelDate.Visible = False
                        lblHWDueDate.Visible = False
                        lblARelDate.Visible = False
                        lblSRelDate.Visible = False

                        imgHWRelDate.Visible = False
                        imgHWDueDate.Visible = False
                        imgARelDate.Visible = False
                        imgSRelDate.Visible = False
                    Else
                        If ddlStatus.SelectedItem.Text = "Makeup" Or ddlStatus.SelectedItem.Text = "Substitute" Then
                            ddlReason.Enabled = True
                        End If
                        lblHWRelDate.Visible = True
                        lblHWDueDate.Visible = True
                        lblARelDate.Visible = True
                        lblSRelDate.Visible = True

                        imgHWRelDate.Visible = True
                        imgHWDueDate.Visible = True
                        imgARelDate.Visible = True
                        imgSRelDate.Visible = True

                    End If



                ElseIf e.CommandName = "UpdateCoach" Then
                    lblMsg.ForeColor = Color.Red
                    lblMsg.Text = ""
                    If ddlStatus.SelectedIndex = 0 Then
                        lblMsg.Text = "Select Status "
                    ElseIf ddlStatus.SelectedItem.Text.ToLower = "cancelled" Or ddlStatus.SelectedItem.Text.ToLower = "substitute" Or ddlStatus.SelectedItem.Text.ToLower = "substitute cancelled" Or ddlStatus.SelectedItem.Text.ToLower = "makeup" Or ddlStatus.SelectedItem.Text.ToLower = "makeup cancelled" Then
                        ' Reason is mandatory if status is cancelled or Substitute
                        If ddlReason.SelectedIndex = 0 Then
                            ' show error msg
                            lblMsg.Text = "Reason is mandatory if status is Cancelled or Substitute"

                        ElseIf ddlSubstitute.SelectedIndex = 0 And ddlStatus.SelectedItem.Text.ToLower = "substitute" Then
                            lblMsg.Text = "Select coach name from substitute "
                        End If
                    ElseIf ddlStatus.SelectedItem.Text.ToLower = "on" Or ddlStatus.SelectedItem.Text.ToLower = "substitute" Then
                        If lblHWRelDate.Text = "__/__/____" And ddlProductGroup.SelectedValue <> "42" Then
                            ' show error msg
                            lblMsg.Text = "Home work date is mandatory if status is on or Substitute"
                        ElseIf lblHWDueDate.Text = "__/__/____" And ddlProductGroup.SelectedValue <> "42" Then
                            ' show error msg
                            lblMsg.Text = "Home work due date is mandatory if status is on or Substitute"
                        ElseIf lblARelDate.Text = "__/__/____" And ddlProductGroup.SelectedValue <> "42" Then
                            ' show error msg
                            lblMsg.Text = "Answer release date is mandatory if status is on or Substitute"
                        ElseIf lblSRelDate.Text = "__/__/____" And ddlProductGroup.SelectedValue <> "42" Then
                            ' show error msg
                            lblMsg.Text = "SRelease date is mandatory if status is on or Substitute"
                        Else

                            'server
                            Dim dtClDate As DateTime
                            Dim dtHWDue As Date
                            Dim dtHWRel As DateTime
                            Dim dtARel As Date
                            Dim dtSRel As Date
                            If lblHWRelDate.Text <> "__/__/____" And lblHWDueDate.Text <> "__/__/____" And lblARelDate.Text <> "__/__/____" And lblSRelDate.Text <> "__/__/____" Then

                                'server

                                'dtClDate = DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                                'dtHWRel = DateTime.ParseExact(lblHWRelDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                                'dtHWDue = Date.ParseExact(lblHWDueDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                                'dtARel = Date.ParseExact(lblARelDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                                'dtSRel = Convert.ToDateTime(lblSRelDate.Text)
                                'dtSRel = Date.ParseExact(lblSRelDate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture)

                                dtClDate = Convert.ToDateTime(lblDate.Text)
                                dtHWRel = Convert.ToDateTime(lblHWRelDate.Text)
                                dtHWDue = Convert.ToDateTime(lblHWDueDate.Text)
                                dtARel = Convert.ToDateTime(lblARelDate.Text)
                                dtSRel = Convert.ToDateTime(lblSRelDate.Text)

                                'local

                                'dtClDate = Convert.ToDateTime("10/12/2016")
                                'dtHWRel = Convert.ToDateTime("10/12/2016")
                                'dtHWDue = Convert.ToDateTime("15/12/2016")
                                'dtARel = Convert.ToDateTime("17/12/2016")
                                'dtSRel = Convert.ToDateTime("9/12/2016")

                            End If



                            'Old validation

                            If ddlProductGroup.SelectedValue <> "42" Then
                                If dtHWRel >= dtHWDue Then
                                    ' show error msg
                                    lblMsg.Text = "Due date should be greater than the Homework date"
                                ElseIf dtARel <= dtHWDue Then
                                    ' show error msg
                                    lblMsg.Text = " Answers date should be greater than both Due Date and Homework Date"
                                    'ElseIf dtSRel <= dtHWDue Then
                                    '    ' show error msg
                                    '    lblMsg.Text = " SRelease date should be greater than both Due Date and Homework Date"
                                End If
                            End If
                        End If
                    End If

                    '    ' validate hw release date
                    '    'Home work release date, HW Due date and Answer Release date are mandatory when Status is On or Substitute
                    '    'Defaults: Homework is Date of the Class, Due date is 5 days after the Homework release date and Answers is 7th day after the HW release date. 

                    Dim hdProductGrp As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdProductGrp"), HiddenField)
                    Dim hdProduct As HiddenField = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("hdProduct"), HiddenField)
                    Dim lblPhase As Label = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("lblPhase"), Label)
                    Dim lblLevel As Label = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("lblLevel"), Label)
                    Dim lblSessionNo As Label = DirectCast(gvRow.NamingContainer.NamingContainer.FindControl("lblSessionNo"), Label)

                    Try


                        If Len(Trim(lblMsg.Text)) = 0 Then
                            lblMsg.ForeColor = Color.DarkBlue
                            If Len(lblCoachClassCalID.Text.Trim) = 0 Or lblCoachClassCalID.Text = 0 Then
                                'insert
                                Try
                                    If ddlStatus.SelectedItem.Text <> "Not set" Then


                                        cmdText = "select CoachPaperId from CoachPapers where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Level='" & lblLevel.Text & "' and WeekId=" & WeekNo & " and DocType='Q'"
                                        Dim CoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                        If CoachPaperId > 0 Then

                                            If ddlStatus.SelectedItem.Text = "Makeup" Then

                                                ddlRecClassDate.Enabled = True
                                                lblUpdateOpt.Text = "Makeup Class"
                                                LblRecClassDate.Visible = True
                                                ddlRecClassDate.Visible = True
                                                LblMakeupClassDate.Text = "Makeup Class Date"
                                                'populateRecClassDateDrpDown()
                                                txtDuration.Visible = False
                                                lblDuration.Visible = False

                                                ddlRecClassDate.Items.Clear()
                                                ddlRecClassDate.Items.Add(New ListItem(lblDate.Text, lblDate.Text))
                                                ddlRecClassDate.SelectedValue = lblDate.Text
                                                ddlRecClassDate.Enabled = False
                                                TxtMkeupDate.Text = ""
                                                ddlMakeupTime.SelectedValue = ""

                                                hdnProductGroupID.Value = ddlProductGroup.SelectedValue
                                                hdnProductID.Value = ddlProduct.SelectedValue
                                                hdnLevel.Value = lblLevel.Text
                                                hdnSessionNo.Value = lblSessionNo.Text
                                                hdnCoachID.Value = ddlCoach.SelectedValue
                                                hdnStatus.Value = ddlStatus.SelectedItem.Text
                                                hdnSerNo.Value = SerNo
                                                hdnWeekNo.Value = WeekNo
                                                hdnSignupID.Value = lblSignUpId.Text
                                                hdnDate.Value = lblDate.Text
                                                hdnTime.Value = lblTime.Text
                                                hdnReason.Value = ddlReason.SelectedItem.Text
                                                hdnIsUpdate.Value = "0"
                                                txtDuration.Visible = False
                                                lblDuration.Visible = False
                                                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)

                                                Exit Sub

                                            End If

                                            If ddlStatus.Text = "On" Then

                                                Dim icount As Integer = 0
                                                Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & lblDate.Text & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & " and Semester='" & ddlPhase.SelectedValue & "'"

                                                'simson
                                                icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                                If icount = 0 Then
                                                    cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                                    cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & lblDate.Text & "', day, '" & lblTime.Text & "', Duration," & SerNo & ", " & WeekNo & ", '" & ddlStatus.SelectedItem.Text & "',null, null, GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                                    lblMsg.Text = "Inserted Successfully"
                                                End If



                                            End If

                                            If ddlStatus.Text = "Cancelled" Then

                                                Dim icount As Integer = 0
                                                Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & lblDate.Text & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & " and Semester='" & ddlPhase.SelectedValue & "' "

                                                'simson
                                                icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                                If (icount = 0) Then


                                                    cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                                    cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & lblDate.Text & "', day, '" & lblTime.Text & "', Duration," & SerNo & ", " & WeekNo & ", '" & ddlStatus.SelectedItem.Text & "',null, null, GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                                    lblMsg.Text = "Inserted Successfully"
                                                End If
                                            End If

                                            If ddlStatus.SelectedItem.Text = "Substitute" Then


                                                Dim icount As Integer = 0
                                                Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & lblDate.Text & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & "  and Semester='" & ddlPhase.SelectedValue & "'"

                                                'simson
                                                icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                                If (icount = 0) Then


                                                    cmdText = "update WebConfLog set SubstituteCoachID=" & ddlSubstitute.SelectedValue & " where MemberID=" & ddlCoach.SelectedItem.Value & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedItem.Value & " and Level='" & lblLevel.Text & "' and [Session]=" & lblSessionNo.Text & " and Semester ='" & ddlPhase.SelectedValue & "'"
                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                                    cmdText = "update CalSignup set SubstituteCoachID=" & ddlSubstitute.SelectedValue & ", SubstituteDate='" & lblDate.Text & "' where MemberID=" & ddlCoach.SelectedItem.Value & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedItem.Value & " and Level='" & lblLevel.Text & "' and [SessionNo]=" & lblSessionNo.Text & " and Semester='" & ddlPhase.SelectedValue & "'"
                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                                    cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                                    cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & lblDate.Text & "', day, '" & lblTime.Text & "', Duration," & SerNo & ", " & WeekNo & ", '" & ddlStatus.SelectedItem.Text & "','" & ddlSubstitute.SelectedValue & "', '" & ddlReason.SelectedValue & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                                    lblMsg.Text = "Inserted Successfully"
                                                End If
                                            End If

                                            If (ddlStatus.SelectedItem.Text = "Makeup Cancelled" Or ddlStatus.SelectedItem.Text = "Substitute Cancelled") Then



                                                If (ddlStatus.SelectedItem.Text = "Makeup Cancelled") Then

                                                    lblMsg.Text = "Status '" & ddlStatus.SelectedItem.Text & "' is only applicable after scheduling Makeup session"
                                                Else

                                                    lblMsg.Text = "Status '" & ddlStatus.SelectedItem.Text & "' is only applicable after scheduling Substitute session"

                                                End If
                                                Dim icount As Integer = 0
                                                Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & lblDate.Text & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & " and Semester='" & ddlPhase.SelectedValue & "' "

                                                'simson
                                                icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                                If (icount = 0) Then


                                                    cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                                    cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & lblDate.Text & "', Day, Time, Duration," & SerNo & ", " & WeekNo & ", '" & ddlStatus.SelectedItem.Text & "'," & ddlSubstitute.SelectedItem.Value & ", '" & ddlReason.SelectedItem.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                                    lblMsg.Text = "Inserted Successfully"
                                                End If
                                            Else

                                            End If

                                            If prevReason = "Holiday" Then

                                                Try


                                                    Dim icount As Integer = 0
                                                    Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & Convert.ToDateTime(lblDate.Text).AddDays(-7).ToShortDateString() & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & " and Semester = '" & ddlPhase.SelectedValue & "' "

                                                    'simson
                                                    icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                                    If (icount = 0) Then


                                                        cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Reason, CreateDate, CreatedBy)"
                                                        cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & Convert.ToDateTime(lblDate.Text).AddDays(-7).ToShortDateString() & "', Day, Time, Duration," & SerNo & ", " & WeekNo & ", 'Cancelled', 'Holiday', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)

                                                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                                    End If
                                                Catch ex As Exception

                                                End Try
                                            End If

                                        Else
                                            ' Dim alertMsg As String = "Coach paper is missing for the week " & WeekNo & " to the selected Product Group and Product. Do you still want to proceed and set the class status " & ddlStatus.SelectedItem.Text & " ?"
                                            hdnWeekNo.Value = WeekNo
                                            hdnStatus.Value = ddlStatus.SelectedItem.Text
                                            hdnStatus.Value = ddlStatus.SelectedItem.Text
                                            hdnSerNo.Value = SerNo
                                            hdnWeekNo.Value = WeekNo
                                            hdnSignupID.Value = lblSignUpId.Text
                                            hdnDate.Value = lblDate.Text
                                            hdnTime.Value = lblTime.Text

                                            hdnCoachID.Value = ddlCoach.SelectedValue
                                            hdnSubCoachID.Value = ddlSubstitute.SelectedValue
                                            hdnProductGroupID.Value = ddlProductGroup.SelectedValue
                                            hdnProductID.Value = ddlProduct.SelectedValue
                                            hdnLevel.Value = lblLevel.Text
                                            hdnSessionNo.Value = lblSessionNo.Text
                                            hdnReason.Value = ddlReason.SelectedValue

                                            ddlRecClassDate.Items.Clear()
                                            ddlRecClassDate.Items.Add(New ListItem(lblDate.Text, lblDate.Text))
                                            ddlRecClassDate.SelectedValue = lblDate.Text
                                            ddlRecClassDate.Enabled = False


                                            TxtMkeupDate.Text = ""
                                            ddlMakeupTime.SelectedValue = ""

                                            If (ddlStatus.SelectedItem.Text <> "Makeup Cancelled" And ddlStatus.SelectedItem.Text <> "Substitute Cancelled" And ddlStatus.SelectedItem.Text <> "Cancelled" And ddlStatus.SelectedItem.Text <> "Not set") Then

                                                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showClassAlert();", True)
                                                Exit Sub
                                            Else
                                                If (ddlStatus.SelectedItem.Text = "Makeup Cancelled") Then

                                                    lblMsg.Text = "Status '" & ddlStatus.SelectedItem.Text & "' is only applicable after scheduling Makeup session"
                                                    lblMsg.ForeColor = Color.Red
                                                ElseIf (ddlStatus.SelectedItem.Text = "Substitute Cancelled") Then

                                                    lblMsg.Text = "Status '" & ddlStatus.SelectedItem.Text & "' is only applicable after scheduling Substitute session"
                                                    lblMsg.ForeColor = Color.Red
                                                Else

                                                    Dim icount As Integer = 0
                                                    Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & lblDate.Text & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & " and Semester='" & ddlPhase.SelectedValue & "'"

                                                    'simson
                                                    icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                                    If (icount = 0) Then


                                                        cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                                        cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & lblDate.Text & "', Day, Time, Duration," & SerNo & ", " & WeekNo & ", '" & ddlStatus.SelectedItem.Text & "'," & ddlSubstitute.SelectedItem.Value & ", '" & ddlReason.SelectedItem.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                                        lblMsg.Text = "Inserted Successfully"
                                                        BindCoachClassDetailsInRepeater()
                                                    End If

                                                End If

                                            End If

                                            Exit Sub
                                            ' lblMsg.Text = "Coach paper is missing for the week " & WeekNo & " to the selected Product Group and Product."
                                        End If
                                    Else

                                        Dim icount As Integer = 0
                                        Dim countText As String = "select count(*) from coachclassCal where Eventyear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and memberID=" & ddlCoach.SelectedItem.Value & " and level='" & ddlLevel.SelectedItem.Text & "' and SessionNo=" & ddlSessionNo.SelectedValue & " and Date='" & lblDate.Text & "' and Status='" & ddlStatus.SelectedItem.Text & "' and WeekNo=" & WeekNo & " and Semester='" & ddlPhase.SelectedValue & "' "

                                        'simson
                                        icount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, countText)
                                        If (icount = 0) Then


                                            cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                                            cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & lblDate.Text & "', Day, Time, Duration," & SerNo & ", " & WeekNo & ", '" & ddlStatus.SelectedItem.Text & "'," & ddlSubstitute.SelectedItem.Value & ", '" & ddlReason.SelectedItem.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(lblSignUpId.Text)
                                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                            lblMsg.Text = "Inserted Successfully"

                                        End If
                                    End If

                                Catch ex As Exception
                                    CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                                End Try
                            Else
                                'update
                                If ddlStatus.SelectedItem.Text <> "Cancelled" And ddlStatus.SelectedItem.Text <> "Not set" And ddlStatus.SelectedItem.Text <> "Makeup Cancelled" And ddlStatus.SelectedItem.Text <> "Substitute Cancelled" And ddlStatus.SelectedItem.Text <> "Makeup" Then
                                    Try



                                        cmdText = "select CoachPaperId from CoachPapers where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & hdProductGrp.Value & " and ProductId= " & hdProduct.Value & " and Level='" & lblLevel.Text & "' and WeekId=" & WeekNo & " and DocType='Q'"
                                        Dim CoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                    Catch ex As Exception
                                        CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                                    End Try
                                    ' If CoachPaperId > 0 Then

                                    If ddlStatus.SelectedItem.Text = "Substitute" Then


                                        cmdText = "update WebConfLog set SubstituteCoachID=" & ddlSubstitute.SelectedValue & " where MemberID=" & ddlCoach.SelectedItem.Value & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedItem.Value & " and Level='" & lblLevel.Text & "' and [Session]=" & lblSessionNo.Text & " and Semester='" & ddlPhase.SelectedValue & "'"
                                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                        cmdText = "update CalSignup set SubstituteCoachID=" & ddlSubstitute.SelectedValue & ", SubstituteDate='" & lblDate.Text & "' where MemberID=" & ddlCoach.SelectedItem.Value & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedItem.Value & " and Level='" & lblLevel.Text & "' and [SessionNo]=" & lblSessionNo.Text & " and Semester='" & ddlPhase.SelectedValue & "'"
                                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                    End If

                                    If ddlStatus.SelectedItem.Text <> "Makeup" Then

                                        cmdText = "UPDATE CoachClassCal SET SerNo=" & SerNo & ", WeekNo = " & WeekNo & " ,Status='" & ddlStatus.SelectedItem.Text & "',Substitute=" & ddlSubstitute.SelectedItem.Value & ", Reason='" & ddlReason.SelectedItem.Value & "' ,ModifyDate=GETDATE(), ModifiedBy=" & Session("LoginID")
                                        cmdText = cmdText + " where CoachClassCalId=" + lblCoachClassCalID.Text
                                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                        lblMsg.Text = "Updated Successfully"

                                    Else

                                    End If
                                    'Else
                                    'lblMsg.Text = "Coach paper is missing for the week " & WeekNo & " to the selected Product Group and Product."
                                    ' End If
                                ElseIf ddlStatus.SelectedItem.Text = "Makeup" Then


                                    hdnCoachClaassID.Value = lblCoachClassCalID.Text

                                    Dim prevReclCLassDate As String = String.Empty
                                    Dim makeupClassDate As String = String.Empty

                                    prevReclCLassDate = DirectCast(grd.Rows(prevIndex).FindControl("lblDate"), Label).Text
                                    If prevIndex = 0 Then
                                        prevIndex = 0
                                    Else
                                        prevIndex = prevIndex + 1
                                    End If
                                    makeupClassDate = DirectCast(grd.Rows(prevIndex).FindControl("lblDate"), Label).Text
                                    ddlRecClassDate.Items.Clear()

                                    ddlRecClassDate.Items.Add(New ListItem(makeupClassDate, makeupClassDate))
                                    ddlRecClassDate.SelectedValue = makeupClassDate
                                    ddlRecClassDate.Enabled = False

                                    If (hdnorgStatus.Value.ToLower() = "makeup") Then

                                        hdnIsUpdate.Value = "1"
                                        TxtMkeupDate.Text = makeupClassDate

                                        ddlMakeupTime.SelectedValue = lblTime.Text & ":00"
                                    Else
                                        TxtMkeupDate.Text = ""
                                        ddlMakeupTime.SelectedValue = ""
                                        hdnOrgStatusUpd.Value = "1"

                                    End If

                                    ddlRecClassDate.Enabled = True
                                    lblUpdateOpt.Text = "Makeup Class"
                                    LblRecClassDate.Visible = True
                                    ddlRecClassDate.Visible = True
                                    LblMakeupClassDate.Text = "Makeup Class Date"
                                    ' populateRecClassDateDrpDown()
                                    txtDuration.Visible = False
                                    lblDuration.Visible = False

                                    hdnStatus.Value = ddlStatus.SelectedItem.Text
                                    hdnSerNo.Value = SerNo
                                    hdnWeekNo.Value = WeekNo
                                    hdnSignupID.Value = lblSignUpId.Text
                                    hdnDate.Value = lblDate.Text
                                    hdnTime.Value = lblTime.Text
                                    hdnReason.Value = ddlReason.SelectedItem.Text
                                    hdnProductGroupID.Value = ddlProductGroup.SelectedValue
                                    hdnProductID.Value = ddlProduct.SelectedValue
                                    hdnLevel.Value = lblLevel.Text
                                    hdnSessionNo.Value = lblSessionNo.Text
                                    hdnCoachID.Value = ddlCoach.SelectedValue
                                    txtDuration.Visible = False
                                    lblDuration.Visible = False
                                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)
                                    'ddlRecClassDate.Items.Clear()

                                    'ddlRecClassDate.Items.Add(New ListItem(makeupClassDate, makeupClassDate))
                                    'Exit Sub
                                Else
                                    hdnWeekNo.Value = WeekNo
                                    hdnStatus.Value = ddlStatus.SelectedItem.Text
                                    hdnStatus.Value = ddlStatus.SelectedItem.Text
                                    hdnSerNo.Value = SerNo
                                    hdnWeekNo.Value = WeekNo
                                    hdnSignupID.Value = lblSignUpId.Text
                                    hdnDate.Value = lblDate.Text
                                    hdnTime.Value = lblTime.Text

                                    hdnCoachID.Value = ddlCoach.SelectedValue
                                    hdnSubCoachID.Value = ddlSubstitute.SelectedValue
                                    hdnProductGroupID.Value = ddlProductGroup.SelectedValue
                                    hdnProductID.Value = ddlProduct.SelectedValue
                                    hdnLevel.Value = lblLevel.Text
                                    hdnSessionNo.Value = lblSessionNo.Text
                                    hdnReason.Value = ddlReason.SelectedValue

                                    If (ddlStatus.SelectedItem.Text = "Makeup Cancelled") Then
                                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert4Makeup();", True)
                                        Exit Sub
                                    ElseIf (ddlStatus.SelectedItem.Text = "Substitute Cancelled") Then
                                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert4Substitute();", True)
                                        Exit Sub
                                    Else
                                        cmdText = "UPDATE CoachClassCal SET SerNo=" & SerNo & ", WeekNo = " & WeekNo & " ,Status='" & ddlStatus.SelectedItem.Text & "',Substitute=" & ddlSubstitute.SelectedItem.Value & ", Reason='" & ddlReason.SelectedItem.Value & "' ,ModifyDate=GETDATE(), ModifiedBy=" & Session("LoginID")
                                        cmdText = cmdText + " where CoachClassCalId=" + lblCoachClassCalID.Text
                                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                        lblMsg.Text = "Updated Successfully"
                                    End If

                                End If
                            End If


                            ' update in coachreldates
                            Try


                                cmdText = "select CoachPaperId from CoachPapers where EventYear=" & ddlYear.SelectedValue & " and EventId=13 and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Level='" & ddlLevel.Text & "' and WeekId=" & WeekNo & " and DocType='Q'"
                                Dim iCoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                If ddlStatus.SelectedItem.Text <> "Not set" Then
                                    Dim coachRelID As Integer = 0




                                    cmdText = "select coachRelID from coachreldates where EventYear=" & ddlYear.SelectedValue & "  and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Level='" & ddlLevel.SelectedValue & "' and Session=" & ddlSessionNo.SelectedValue & " and MemberID=" & ddlCoach.SelectedValue & " and CoachPaperID=" & iCoachPaperId & " and Semester='" & ddlPhase.SelectedValue & "'"

                                    coachRelID = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                    If iCoachPaperId > 0 Then
                                        If ddlStatus.SelectedItem.Text = "Cancelled" Or ddlStatus.SelectedItem.Text = "Not set" Then
                                            If coachRelID > 0 Then


                                                cmdText = "update CoachRelDates Set qreleasedate=null, qdeadlinedate=null, areleasedate=null,sreleasedate=null, ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID").ToString() & " WHERE CoachPaperId=" & iCoachPaperId & " and MemberId= " & ddlCoach.SelectedValue & " and  EventYear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "' and Level='" & ddlLevel.SelectedValue & "' and Session =" & ddlSessionNo.SelectedValue & ""


                                                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                            Else
                                                Dim cmdtext As String = String.Empty


                                                cmdtext = "insert into CoachRelDates (CoachPaperId, MemberID, EventYear, ProductGroupId, ProductGroupCode, ProductId, ProductCode,Semester, Level, Session, QReleaseDate, QDeadlineDate, AReleaseDate, SReleaseDate, CreateDate, CreatedBy) values(" & iCoachPaperId & ", " & ddlCoach.SelectedValue & ", " & ddlYear.SelectedValue & "," & ddlProductGroup.SelectedValue & ", '" & ddlProductGroup.SelectedItem.Text & "', " & ddlProduct.SelectedValue & ", '" & ddlProduct.SelectedItem.Text & "', " & ddlPhase.SelectedValue & ", '" & ddlLevel.SelectedValue & "', " & ddlSessionNo.SelectedValue & ", null, null, null, null, Getdate(), " & Session("LoginID").ToString() & ")"

                                                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdtext)

                                            End If

                                        Else
                                            If lblARelDate.Text <> "__/__/____" And lblHWRelDate.Text <> "__/__/____" And lblHWDueDate.Text <> "__/__/____" Then


                                                If coachRelID > 0 Then


                                                    cmdText = "update CoachRelDates Set qreleasedate='" & lblHWRelDate.Text & "', qdeadlinedate='" & lblHWDueDate.Text & "', areleasedate='" & lblARelDate.Text & "', SReleaseDate='" & lblSRelDate.Text & "', ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID").ToString() & " WHERE CoachPaperId=" & iCoachPaperId & " and MemberId= " & ddlCoach.SelectedValue & " and  EventYear=" & ddlYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "' and Level='" & ddlLevel.SelectedValue & "' and Session =" & ddlSessionNo.SelectedValue

                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)


                                                Else

                                                    Dim cmdtext As String = String.Empty


                                                    cmdtext = "insert into CoachRelDates (CoachPaperId, MemberID, EventYear, ProductGroupId, ProductGroupCode, ProductId, ProductCode,Semester, Level, Session, QReleaseDate, QDeadlineDate, AReleaseDate, SReleaseDate, CreateDate, CreatedBy) values(" & iCoachPaperId & ", " & ddlCoach.SelectedValue & ", " & ddlYear.SelectedValue & "," & ddlProductGroup.SelectedValue & ", '" & ddlProductGroup.SelectedItem.Text & "', " & ddlProduct.SelectedValue & ", '" & ddlProduct.SelectedItem.Text & "', " & ddlPhase.SelectedValue & ", '" & ddlLevel.SelectedValue & "', " & ddlSessionNo.SelectedValue & ", '" & lblHWRelDate.Text & "', '" & lblHWDueDate.Text & "', '" & lblARelDate.Text & "', '" & lblSRelDate.Text & "', Getdate(), " & Session("LoginID").ToString() & ")"

                                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdtext)

                                                    'Dim conn As New SqlConnection(Application("ConnectionString").ToString())
                                                    'Dim sqlCommand As String = "usp_Insert_CoachRelDate"
                                                    'Dim param As SqlParameter() = New SqlParameter(9) {}
                                                    'param(0) = New SqlParameter("@CoachPaperId", iCoachPaperId)
                                                    'param(1) = New SqlParameter("@Semester", "1")

                                                    'param(2) = New SqlParameter("@SessionNo", hdnSessionNo.Value)
                                                    'param(3) = New SqlParameter("@QReleaseDate", lblHWRelDate.Text)
                                                    'param(4) = New SqlParameter("@QDeadlineDate", lblHWDueDate.Text)
                                                    'param(5) = New SqlParameter("@AReleaseDate", lblARelDate.Text)
                                                    'param(6) = New SqlParameter("@SReleaseDate", lblSRelDate.Text)
                                                    'param(7) = New SqlParameter("@CreateDate", System.DateTime.Now)
                                                    'param(8) = New SqlParameter("@CreatedBy", Int32.Parse(Session("LoginID").ToString()))
                                                    'param(9) = New SqlParameter("@MemberId", Int32.Parse(ddlCoach.SelectedValue))

                                                    'SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param)

                                                End If
                                            End If



                                        End If

                                        'SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                    End If
                                End If
                            Catch ex As Exception
                                'Throw New Exception(ex.Message)
                                CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                            End Try
                            hdHideModify.Value = "false"
                            ddlStatus.Enabled = False
                            ddlReason.Enabled = False
                            ddlSubstitute.Enabled = False
                            btnModify.Visible = True
                            btnUpdate.Visible = False
                            btnCancel.Visible = False
                            imgHWRelDate.Visible = False
                            imgHWDueDate.Visible = False
                            imgARelDate.Visible = False
                            imgSRelDate.Visible = False
                            If Not ViewState("Date") Is Nothing Then
                                Dim list As New Dictionary(Of String, String)
                                list = ViewState("Date")
                                If list.ContainsKey(lblHWRelDate.UniqueID) Then
                                    list.Remove(lblHWRelDate.UniqueID)
                                End If
                                If list.ContainsKey(lblHWDueDate.UniqueID) Then
                                    list.Remove(lblHWDueDate.UniqueID)
                                End If
                                If list.ContainsKey(lblARelDate.UniqueID) Then
                                    list.Remove(lblARelDate.UniqueID)
                                End If
                                If list.ContainsKey(lblSRelDate.UniqueID) Then
                                    list.Remove(lblSRelDate.UniqueID)
                                End If
                            End If
                            BindCoachClassDetailsInRepeater()
                            'BindCoachClassDetailsInGrid(sender, 0)
                        End If
                    Catch ex As Exception

                        BindCoachClassDetailsInRepeater()
                        CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                    End Try
                ElseIf e.CommandName = "CancelCoach" Then
                    Reset()
                    hdHideModify.Value = "false"
                    ddlStatus.Enabled = False
                    ddlReason.Enabled = False
                    ddlSubstitute.Enabled = False
                    btnModify.Visible = True
                    btnUpdate.Visible = False
                    btnCancel.Visible = False
                    imgHWRelDate.Visible = False
                    imgHWDueDate.Visible = False
                    imgARelDate.Visible = False
                    imgSRelDate.Visible = False
                    If Not ViewState("Date") Is Nothing Then
                        Dim list As New Dictionary(Of String, String)
                        list = ViewState("Date")
                        If list.ContainsKey(lblHWRelDate.UniqueID) Then
                            list.Remove(lblHWRelDate.UniqueID)
                        End If
                        If list.ContainsKey(lblHWDueDate.UniqueID) Then
                            list.Remove(lblHWDueDate.UniqueID)
                        End If
                        If list.ContainsKey(lblARelDate.UniqueID) Then
                            list.Remove(lblARelDate.UniqueID)
                        End If
                        If list.ContainsKey(lblSRelDate.UniqueID) Then
                            list.Remove(lblSRelDate.UniqueID)
                        End If
                    End If
                    BindCoachClassDetailsInGrid(sender, 0)
                End If
            Else

                'temporyly hidden

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "statusAlert();", True)


            End If
            ' End If
        Catch ex As Exception
            BindCoachClassDetailsInRepeater()
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
            'Throw New Exception(ex.Message)
        End Try
    End Sub



    Protected Sub rep_lnkBtnShowNextWeek_Click(sender As Object, e As EventArgs)
        Reset()
        'Row of the Grid from where the SelectedIndex change event is fired.
        Dim rptItem As RepeaterItem = TryCast(sender.NamingContainer, RepeaterItem)
        Dim grd As GridView = TryCast(rptItem.FindControl("rpt_grdCoachClassCal"), GridView)
        Dim rep_hdWeekCnt As HiddenField = TryCast(rptItem.FindControl("rep_hdWeekCnt"), HiddenField)
        Dim hdHideModify As HiddenField = TryCast(rptItem.FindControl("hdHideModify"), HiddenField)
        hdHideModify.Value = "false"
        rep_hdWeekCnt.Value = (CInt(rep_hdWeekCnt.Value) + 1).ToString
        Dim ddlGoTo As DropDownList = TryCast(rptItem.FindControl("ddlGoTo"), DropDownList)
        ddlGoTo.SelectedIndex = ddlGoTo.Items.IndexOf(ddlGoTo.Items.FindByText(Convert.ToString(rep_hdWeekCnt.Value)))

        ddlGoTo_SelectedIndexChanged(ddlGoTo, New EventArgs)
        'BindCoachClassDetailsInGrid(grd)
    End Sub

    Private Sub BindCoachClassDetailsInGrid(grCtrl As GridView, iShowAll As Integer)
        Dim dt As Date = Now.Date.ToString("yyyy-MM-dd")  ' "2014-12-02"
        Dim rpItem As RepeaterItem = grCtrl.NamingContainer
        Dim iProductId As Integer = DirectCast(rpItem.FindControl("hdProduct"), HiddenField).Value
        Dim iProductGrpId As Integer = DirectCast(rpItem.FindControl("hdProductGrp"), HiddenField).Value
        Dim iPhase As String = DirectCast(rpItem.FindControl("lblPhase"), Label).Text
        Dim strLevel As String = DirectCast(rpItem.FindControl("lblLevel"), Label).Text
        Dim iSessionNo As Integer = DirectCast(rpItem.FindControl("lblSessionNo"), Label).Text
        Dim rep_hdWeekCnt As HiddenField = TryCast(rpItem.FindControl("rep_hdWeekCnt"), HiddenField)
        hdnLevel.Value = strLevel
        hdnSessionNo.Value = iSessionNo
        If hdnTotalClass.Value = "0" Then
            populateRecClassDate(strLevel, iSessionNo)

        End If

        ' rep_hdWeekCnt.Value = 1
        '  iShowAll = 1
        If Len(Trim(rep_hdWeekCnt.Value)) = 0 Then
            rep_hdWeekCnt.Value = "1"
        End If
        ' rep_hdWeekCnt.Value = 15
        'iShowAll = 0
        '@MemberId int, @EventYear int, @ProductGroupId int, @ProductId int, @Level varchar(50), @SessionNo int , @Iterator int
        Dim params() As SqlParameter = {New SqlParameter("@MemberId", SqlDbType.Int), New SqlParameter("@EventYear", SqlDbType.Int), New SqlParameter("@ProductGroupId", SqlDbType.Int), New SqlParameter("@ProductId", SqlDbType.Int), New SqlParameter("@Semester", SqlDbType.NVarChar, 50), New SqlParameter("@Level", SqlDbType.VarChar, 50), New SqlParameter("@SessionNo", SqlDbType.Int), New SqlParameter("@Iterator", SqlDbType.Int), New SqlParameter("@ShowAll", SqlDbType.Int)}
        params(0).Value = hdMemberId.Value
        params(1).Value = hdTable1Year.Value
        params(2).Value = iProductGrpId
        params(3).Value = iProductId
        params(4).Value = iPhase
        params(5).Value = strLevel
        params(6).Value = iSessionNo
        params(7).Value = hdnTotalClass.Value
        params(8).Value = 0
        hdnTotalClass.Value = "0"
        'params(7).Value = rep_hdWeekCnt.Value
        'params(8).Value = iShowAll
        'productgroupID, productID, Semester, level, week#, DocType=Q, 
        Dim ds As DataSet
        Try


            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.StoredProcedure, "usp_GetCoachDetailsByWeek", params)
        Catch ex As Exception

        End Try
        If ds.Tables(0).Rows.Count = 0 Then
            rpItem.Visible = False
            dvColorStatus.Visible = False
        Else
            grCtrl.DataSource = ds.Tables(0)
            grCtrl.DataBind()
            changeGridColor(grCtrl)
            Dim i As Integer = 0
            Dim coachclasscalID As Integer = 0
            Dim weekNo As Integer = 0
            Dim isExists As Integer = 0
            Dim coachPaperID As Integer = 0  'Dim hwDueDate As String = String.Empty
            'Dim hRelDate As String = String.Empty
            'Dim ansRelDate As String = String.Empty
            'Dim sRelDate As String = String.Empty

            Dim status As String = String.Empty
            Try


                For i = 0 To grCtrl.Rows.Count - 1
                    coachclasscalID = DirectCast(grCtrl.Rows(i).FindControl("lblCoachClassCallID"), Label).Text
                    status = DirectCast(grCtrl.Rows(i).FindControl("lblStatus"), Label).Text
                    Dim cmdWeektext As String = String.Empty
                    If (coachclasscalID > 0 And status <> "Cancelled" And status <> "Not set" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled") Then
                        weekNo = DirectCast(grCtrl.Rows(i).FindControl("lblWeekNumber"), Label).Text
                        'hwDueDate = DirectCast(grCtrl.Rows(i).FindControl("lblHDueDt"), Label).Text
                        'hRelDate = DirectCast(grCtrl.Rows(i).FindControl("lbQlRelDt"), Label).Text
                        'sRelDate = DirectCast(grCtrl.Rows(i).FindControl("lblSARelDt"), Label).Text
                        'ansRelDate = DirectCast(grCtrl.Rows(i).FindControl("lblAnsRelDt"), Label).Text


                        Dim hRelDate As String = DirectCast(grCtrl.Rows(i).Cells(10).FindControl("lblHWRelDate"), Label).Text
                        Dim hwDueDate As String = DirectCast(grCtrl.Rows(i).Cells(11).FindControl("lblHWDueDate"), Label).Text
                        Dim ansRelDate As String = DirectCast(grCtrl.Rows(i).Cells(12).FindControl("lblARelDate"), Label).Text
                        Dim sRelDate As String = DirectCast(grCtrl.Rows(i).Cells(13).FindControl("lblSRelDate"), Label).Text

                        cmdWeektext = "select distinct coachpaperID from CoachPapers where Eventyear =" & ddlYear.SelectedValue & " and weekId=" & weekNo & " and DocType='Q' and ProductId='" & ddlProduct.SelectedValue & "' and Level= '" & ddlLevel.SelectedItem.Text & "'"
                        isExists = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdWeektext)
                        If (isExists > 0) Then
                            coachPaperID = isExists
                            cmdWeektext = "select distinct coachpaperID from coachRelDates where Eventyear =" & ddlYear.SelectedValue & " and coachPaperID=" & coachPaperID & "  and ProductId='" & ddlProduct.SelectedValue & "' and Level= '" & ddlLevel.SelectedItem.Text & "' and Session=" & ddlSessionNo.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' and memberID=" & ddlCoach.SelectedValue & ""
                            isExists = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdWeektext)

                            If (isExists > 0) Then
                            Else
                                Dim cmdReltext As String = String.Empty
                                cmdText = "insert into CoachRelDates (CoachPaperId, MemberID, EventYear, ProductGroupId, ProductGroupCode, ProductId, ProductCode,Semester, Level, Session, QReleaseDate, QDeadlineDate, AReleaseDate, SReleaseDate, CreateDate, CreatedBy) values(" & coachPaperID & ", " & ddlCoach.SelectedValue & ", " & ddlYear.SelectedValue & "," & ddlProductGroup.SelectedValue & ", '" & ddlProductGroup.SelectedItem.Text & "', " & ddlProduct.SelectedValue & ", '" & ddlProduct.SelectedItem.Text & "', " & ddlPhase.SelectedValue & ", '" & ddlLevel.SelectedValue & "', " & ddlSessionNo.SelectedValue & ", '" & hRelDate & "', '" & hwDueDate & "', '" & ansRelDate & "', '" & sRelDate & "', Getdate(), " & Session("LoginID").ToString() & ")"



                                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                DirectCast(grCtrl.Rows(i).Cells(10).FindControl("lblHWRelDate"), Label).ForeColor = Color.Black
                                DirectCast(grCtrl.Rows(i).Cells(11).FindControl("lblHWDueDate"), Label).ForeColor = Color.Black
                                DirectCast(grCtrl.Rows(i).Cells(12).FindControl("lblARelDate"), Label).ForeColor = Color.Black
                                DirectCast(grCtrl.Rows(i).Cells(13).FindControl("lblSRelDate"), Label).ForeColor = Color.Black

                                DirectCast(grCtrl.Rows(i).Cells(10).FindControl("lblHWRelDate"), Label).Font.Bold = False
                                DirectCast(grCtrl.Rows(i).Cells(11).FindControl("lblHWDueDate"), Label).Font.Bold = False
                                DirectCast(grCtrl.Rows(i).Cells(12).FindControl("lblARelDate"), Label).Font.Bold = False
                                DirectCast(grCtrl.Rows(i).Cells(13).FindControl("lblSRelDate"), Label).Font.Bold = False

                            End If
                        End If

                    End If
                Next

            Catch ex As Exception

            End Try
            dvColorStatus.Visible = True
        End If
    End Sub

    Protected Sub changeGridColor(ByVal grdCoachClass As GridView)
        Dim dateVal As String = String.Empty
        Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
        Dim status As String = String.Empty
        Dim prevStatus As String = String.Empty
        Dim prevDate As String = String.Empty
        Dim isInc As Integer = 0
        For i As Integer = 0 To grdCoachClass.Rows.Count - 1
            dateVal = DirectCast(grdCoachClass.Rows(i).FindControl("lblDate"), Label).Text
            status = DirectCast(grdCoachClass.Rows(i).FindControl("hdStatus"), HiddenField).Value
            If i <> 0 Then
                prevStatus = DirectCast(grdCoachClass.Rows(i - 1).FindControl("hdStatus"), HiddenField).Value

            Else
                prevStatus = DirectCast(grdCoachClass.Rows(i).FindControl("hdStatus"), HiddenField).Value
            End If

            ' dateVal = Convert.ToDateTime(dateVal.ToString()).ToString("MM/dd/yyyy")
            Dim dtDateVal As DateTime = New DateTime()
            dtDateVal = Convert.ToDateTime(dateVal.ToString())
            If dtTodayDate > dtDateVal And (status = "On" Or status = "Makeup" Or status = "Substitute") Then
                grdCoachClass.Rows(i).Style.Add("background-color", "#58d68d")

            ElseIf dtDateVal = dtTodayDate And (status = "On" Or status = "Makeup" Or status = "Substitute" Or status = "" Or status = "Not set") Then

                grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")
                isInc = isInc + 1
                'ElseIf dtDateVal <> dtTodayDate And dtTodayDate.AddDays(7) > dtDateVal And status <> "Cancelled" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled" And prevStatus <> "" Then
                '    grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")

            ElseIf dtDateVal <> dtTodayDate And dtDateVal > dtTodayDate And status <> "Cancelled" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled" And prevStatus <> "" Then
                If isInc < 1 Then

                    grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")
                End If
                isInc = isInc + 1
            ElseIf dtDateVal <> dtTodayDate And dtDateVal > dtTodayDate And status <> "Notset" And status <> "Makeup Cancelled" And status <> "Substitute Cancelled" And (prevStatus = "" Or status = "") Then
                If isInc < 1 Then

                    grdCoachClass.Rows(i).Style.Add("background-color", "#05a9e7")
                End If
                isInc = isInc + 1

            ElseIf status = "Cancelled" Or status = "Makeup Cancelled" Or status = "Substitute Cancelled" Then
                grdCoachClass.Rows(i).Style.Add("background-color", "#FF0000")

            ElseIf status = "Not set" Or status = "" Then
                grdCoachClass.Rows(i).Style.Add("background-color", "")
            End If
            If status = "Cancelled" Or status = "Makeup Cancelled" Or status = "Substitute Cancelled" Then
                grdCoachClass.Rows(i).Style.Add("background-color", "#FF0000")

            End If

            'temporaryly hidden

            'If dtDateVal < dtTodayDate.AddDays(-7) And status <> "" Then
            '    DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = False
            'ElseIf dtTodayDate.AddDays(7) <= dtDateVal Then
            '    DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = False
            'Else
            '    DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = True
            'End If
            If (dtDateVal <= dtTodayDate) Then
                DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = True
            ElseIf (dtTodayDate.AddDays(7) >= dtDateVal) Then
                DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = True
            ElseIf (dtTodayDate.AddDays(14) >= dtDateVal) Then
                DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = True
            Else
                DirectCast(grdCoachClass.Rows(i).FindControl("btnModify"), Button).Enabled = False
            End If


        Next
    End Sub
    Protected Sub rptCoachClass_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptCoachClass.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim hdProductGrp As HiddenField = DirectCast(e.Item.FindControl("hdProductGrp"), HiddenField)
            Dim hdProduct As HiddenField = DirectCast(e.Item.FindControl("hdProduct"), HiddenField)
            Dim hdTotalClass As HiddenField = DirectCast(e.Item.FindControl("hdTotalClass"), HiddenField)
            Dim lblPhase As Label = DirectCast(e.Item.FindControl("lblPhase"), Label)
            Dim lblSessionNo As Label = DirectCast(e.Item.FindControl("lblSessionNo"), Label)
            Dim lblLevel As Label = DirectCast(e.Item.FindControl("lblLevel"), Label)
            cmdText = "select classes from EventFees Where EventID=13 and EventYear= " & hdTable1Year.Value & " and ProductGroupId=" & hdProductGrp.Value & " and ProductId=" & hdProduct.Value
            Dim iTotalClass As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            'cmdText = "select StartDate, EndDate from CoachingDateCal Where EventId=13 and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Semester=" + lblPhase.Text + " and ScheduleType='term'"

            cmdText = "select StartDate, EndDate from CoachingDateCal Where EventId=13 and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Semester='" + lblPhase.Text + "'"

            Dim dsClass As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If dsClass.Tables(0).Rows.Count = 0 Then
                e.Item.Visible = False
                Exit Sub
            End If
            Dim hdEndDate As HiddenField = DirectCast(e.Item.FindControl("hdEndDate"), HiddenField)

            'server
            hdEndDate.Value = Convert.ToDateTime(dsClass.Tables(0).Rows(0)("EndDate")).ToString("MM/dd/yyyy")

            'll
            ' hdEndDate.Value = Convert.ToDateTime(dsClass.Tables(0).Rows(0)("EndDate")).ToString("dd/MM/yyyy")
            populateRecClassDate(lblLevel.Text, lblSessionNo.Text)
            hdTotalClass.Value = 0
            iTotalClass = hdnTotalClass.Value
            If iTotalClass > 0 Then
                e.Item.Visible = True
                hdTotalClass.Value = iTotalClass
                Dim grid As GridView = DirectCast(e.Item.FindControl("rpt_grdCoachClassCal"), GridView)
                Dim rep_hdWeekCnt As HiddenField = DirectCast(e.Item.FindControl("rep_hdWeekCnt"), HiddenField)
                rep_hdWeekCnt.Value = "1"
                BindCoachClassDetailsInGrid(grid, 1)
                Dim lblTableCnt As Label = DirectCast(e.Item.FindControl("lblTableCnt"), Label)
                lblTableCnt.Text = CInt(e.Item.ItemIndex) + 1

                Dim ddlGoTo As DropDownList = DirectCast(e.Item.FindControl("ddlGoTo"), DropDownList)
                Dim rpt_grdCoachClassCal As GridView = DirectCast(e.Item.FindControl("rpt_grdCoachClassCal"), GridView)
                rep_hdWeekCnt.Value = rpt_grdCoachClassCal.Rows.Count
                Dim k As Integer
                ddlGoTo.Items.Clear()
                ddlGoTo.Items.Add(New ListItem("Select", -1))
                For k = 1 To iTotalClass
                    ddlGoTo.Items.Add(New ListItem(k, k))
                Next
                ddlGoTo.SelectedIndex = ddlGoTo.Items.IndexOf(ddlGoTo.Items.FindByText(Convert.ToString(rep_hdWeekCnt.Value)))

            Else
                e.Item.Visible = False
            End If
        End If
    End Sub
    Protected Sub rpt_grdCoachClassCal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try


            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lbl As Label = CType(e.Row.FindControl("lblSerial"), Label)
                lbl.Text = Convert.ToInt16(e.Row.RowIndex) + 1
                Dim lblWeekNo As Label = CType(e.Row.FindControl("lblWeekNo"), Label)
                lblWeekNo.Text = Convert.ToInt16(e.Row.RowIndex) + 1

                Dim lblWeek As Label = CType(e.Row.FindControl("lblWeek"), Label)

                Dim lblStartDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblStartDate"), Label)
                Dim hdProductGrp As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdProductGrp"), HiddenField)
                Dim hdProduct As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdProduct"), HiddenField)
                Dim hdTotalClass As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdTotalClass"), HiddenField)

                Dim lblPhase As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblPhase"), Label)
                Dim LblLevel As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblLevel"), Label)
                Dim lblSessionNo As Label = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("lblSessionNo"), Label)
                Dim rep_hdWeekCnt As HiddenField = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_hdWeekCnt"), HiddenField)
                Dim hdHideModify As HiddenField = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("hdHideModify"), HiddenField)

                Dim lblDate As Label = DirectCast(e.Row.Cells(1).FindControl("lblDate"), Label)
                Dim lblCoachClassCalID As Label = DirectCast(e.Row.Cells(1).FindControl("lblCoachClassCalID"), Label)
                Dim ddlStatus As DropDownList = DirectCast(e.Row.Cells(7).FindControl("ddlStatus"), DropDownList)
                Dim hdStatus As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdStatus"), HiddenField)
                Dim hdSignupID As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdSignupID"), HiddenField)
                hdnSignupID.Value = hdSignupID.Value
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByText(Convert.ToString(hdStatus.Value)))
                Dim ddlSubstitute As DropDownList = DirectCast(e.Row.Cells(8).FindControl("ddlSubstitute"), DropDownList)
                Dim hdSubstitute As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdSubstitute"), HiddenField)
                FillCoachByCondition(hdProductGrp.Value, hdProduct.Value, ddlSubstitute)
                ddlSubstitute.SelectedIndex = ddlSubstitute.Items.IndexOf(ddlSubstitute.Items.FindByValue(Convert.ToString(hdSubstitute.Value)))
                Dim ddlReason As DropDownList = DirectCast(e.Row.Cells(9).FindControl("ddlReason"), DropDownList)
                Dim hdReason As HiddenField = DirectCast(e.Row.Cells(7).FindControl("hdReason"), HiddenField)
                ddlReason.SelectedIndex = ddlReason.Items.IndexOf(ddlReason.Items.FindByText(Convert.ToString(hdReason.Value)))
                Dim lblHWRelDate As Label = DirectCast(e.Row.Cells(10).FindControl("lblHWRelDate"), Label)
                Dim lblHWDueDate As Label = DirectCast(e.Row.Cells(11).FindControl("lblHWDueDate"), Label)
                Dim lblARelDate As Label = DirectCast(e.Row.Cells(12).FindControl("lblARelDate"), Label)
                Dim lblSRelDate As Label = DirectCast(e.Row.Cells(13).FindControl("lblSRelDate"), Label)
                Dim lblDay As Label = DirectCast(e.Row.Cells(2).FindControl("lblDay"), Label)
                Dim lblScheduleType As Label = DirectCast(e.Row.Cells(10).FindControl("lblScheduleType"), Label)
                'If lblCoachClassCalID.Text = "" Or ddlStatus.SelectedIndex = 0 Then
                '    lblHWRelDate.Text = lblDate.Text
                '    lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                '    lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                'Else
                '    lblHWRelDate.Text = lblDate.Text
                '    lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                '    lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                'End If

                Dim lblRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblRelDt"), Label)
                If lblRelDT.Text.Trim = "" Then
                    If hdProductGrp.Value = "42" Then


                    Else
                        If hdStatus.Value <> "Cancelled" Then



                            'server
                            If hdStatus.Value = "On" And lblRelDT.Text = "" Then

                                lblHWRelDate.Text = lblDate.Text

                                lblHWRelDate.ForeColor = Color.Gray
                                lblHWRelDate.Font.Bold = True

                                lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                                lblHWDueDate.ForeColor = Color.Gray
                                lblHWDueDate.Font.Bold = True

                                lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                                lblARelDate.ForeColor = Color.Gray
                                lblARelDate.Font.Bold = True

                                If hdProductGrp.Value = "31" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                    lblSRelDate.ForeColor = Color.Gray
                                    lblSRelDate.Font.Bold = True

                                ElseIf hdProductGrp.Value = "33" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                    lblSRelDate.ForeColor = Color.Gray
                                    lblSRelDate.Font.Bold = True
                                ElseIf hdProduct.Value = "106" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                    lblSRelDate.ForeColor = Color.Gray
                                    lblSRelDate.Font.Bold = True
                                ElseIf hdProduct.Value = "107" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                    lblSRelDate.ForeColor = Color.Gray
                                    lblSRelDate.Font.Bold = True
                                Else
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                    lblSRelDate.ForeColor = Color.Gray
                                    lblSRelDate.Font.Bold = True
                                End If
                            Else
                                lblHWRelDate.Text = lblDate.Text

                                lblHWDueDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(5)).Date.ToString("MM/dd/yyyy")
                                lblARelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(7)).Date.ToString("MM/dd/yyyy")
                                If hdProductGrp.Value = "31" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                ElseIf hdProductGrp.Value = "33" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                ElseIf hdProduct.Value = "106" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                ElseIf hdProduct.Value = "107" Then
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                Else
                                    lblSRelDate.Text = (DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing).AddDays(-1)).Date.ToString("MM/dd/yyyy")
                                End If



                            End If
                        End If
                    End If
                Else

                    Dim lblDueDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblDueDt"), Label)
                    Dim lblARelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblARelDt"), Label)
                    Dim lblSRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblSRelDt"), Label)
                    lblHWRelDate.Text = lblRelDT.Text
                    'server
                    lblHWDueDate.Text = DateTime.ParseExact(lblDueDT.Text, "MM/dd/yyyy", Nothing)
                    lblARelDate.Text = DateTime.ParseExact(lblARelDT.Text, "MM/dd/yyyy", Nothing)
                    lblSRelDate.Text = DateTime.ParseExact(lblSRelDT.Text, "MM/dd/yyyy", Nothing)

                    'local
                    'lblHWDueDate.Text = DateTime.ParseExact(lblDueDT.Text, "dd/MM/yyyy", Nothing)
                    'lblARelDate.Text = DateTime.ParseExact(lblARelDT.Text, "dd/MM/yyyy", Nothing)
                    'lblSRelDate.Text = DateTime.ParseExact(lblSRelDT.Text, "dd/MM/yyyy", Nothing)

                End If

                Dim curWeek As Integer, TotalWeek As Integer
                Dim hdEndDate As HiddenField = DirectCast(e.Row.NamingContainer.NamingContainer.FindControl("hdEndDate"), HiddenField)
                Dim stWeek As Date, endWeek As Date, curDate As Date, endDate As Date

                'Server
                curDate = DateTime.ParseExact(lblDate.Text, "MM/dd/yyyy", Nothing)
                'local
                'curDate = DateTime.ParseExact(lblDate.Text, "dd/MM/yyyy", Nothing)
                stWeek = DateAdd("d", 0 - curDate.DayOfWeek, curDate)

                endWeek = DateAdd("d", 6 - curDate.DayOfWeek, curDate)

                'server
                endDate = DateTime.ParseExact(hdEndDate.Value, "MM/dd/yyyy", Nothing)
                'local
                'endDate = DateTime.ParseExact(hdEndDate.Value, "dd/MM/yyyy", Nothing)
                If endDate >= stWeek And endDate <= endWeek Then
                    Dim lnkBtnShowNxt As LinkButton = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
                    lnkBtnShowNxt.Enabled = False
                End If

                ' If lblCoachClassCalID.Text = 0 Or ddlStatus.SelectedItem.Text = "Not set" Or ddlStatus.SelectedItem.Text = "Select" Then
                If ddlStatus.SelectedItem.Text = "Select" Then
                    Dim ArrSerNo As ArrayList = New ArrayList()
                    Dim i As Integer = 0
                    Dim strSerial As String = i
                    Dim finalSerial As String = String.Empty
                    Dim strSerNo As StringBuilder = New StringBuilder()
                    For i = 1 To Convert.ToInt32(lbl.Text) - 1
                        ArrSerNo.Add(i)
                        strSerNo.Append(i)
                        strSerial += i & ","
                    Next
                    finalSerial = strSerial.TrimEnd(",")
                    cmdText = "select isnull(max(weekno),0) as weekno from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Semester='" + lblPhase.Text + "' and SessionNo=" + lblSessionNo.Text + " and Level='" & LblLevel.Text & "' and SerNo in(" & finalSerial & ") and Status in ('On','Substitute','Makeup') "
                    Dim dsClass As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If dsClass.Tables(0).Rows.Count > 0 Then
                        curWeek = dsClass.Tables(0).Rows(0).Item("weekno")
                        TotalWeek = hdTotalClass.Value
                        If curWeek >= TotalWeek Then
                            Dim lnkBtnShowNxt As LinkButton = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rep_lnkBtnShowNextWeek"), LinkButton)
                            lnkBtnShowNxt.Enabled = False
                        End If
                    End If

                    '  lblWeek.Text = curWeek + 1
                    lblWeek.Text = ""
                ElseIf ddlStatus.SelectedItem.Text.ToLower = "cancelled" Then

                    Dim ArrSerNo As ArrayList = New ArrayList()
                    Dim i As Integer = 0
                    Dim strSerial As String = String.Empty
                    Dim finalSerial As String = String.Empty
                    Dim strSerNo As StringBuilder = New StringBuilder()
                    For i = 1 To Convert.ToInt32(lbl.Text) - 1
                        ArrSerNo.Add(i)
                        strSerNo.Append(i)
                        strSerial += i & ","
                    Next
                    finalSerial = strSerial.TrimEnd(",")
                    Dim weekSeqText As String = String.Empty

                    Dim grdCoachClass As GridView = TryCast(e.Row.NamingContainer.NamingContainer.FindControl("rpt_grdCoachClassCal"), GridView)
                    Dim lblWeekSeq As Label

                    Try


                        lblWeekSeq = CType(grdCoachClass.Rows(Convert.ToInt16(e.Row.RowIndex) - 1).FindControl("lblWeek"), Label)
                        weekSeqText = lblWeekSeq.Text
                    Catch ex As Exception
                        weekSeqText = "0"
                        finalSerial = "0"
                    End Try

                    Dim cmdText As String = String.Empty
                    cmdText = "select count(*) as CountSet from CoachClassCal where  EventYear=" & ddlYear.SelectedValue & " and MemberID=" & ddlCoach.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Level='" & LblLevel.Text & "' and SessionNo=" & lblSessionNo.Text & " and SerNo in(" & finalSerial & ") and (Status='On' or Status='Makeup' or Status='Substitute')"


                    Dim ds As DataSet = New DataSet()
                    ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                    If ds.Tables(0).Rows.Count > 0 Then

                        If weekSeqText <> "" Then
                            ' lblWeek.Text = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString()) + 1
                        Else
                            lblWeek.Text = ""
                        End If


                    End If
                    Try


                        Dim weekText As String = DirectCast(e.Row.Cells(6).FindControl("lblWeek"), Label).Text
                        Dim irIndex As Integer = 0
                        If e.Row.RowIndex = 0 Then
                            irIndex = 0
                        Else
                            irIndex = e.Row.RowIndex - 1
                        End If
                        Dim prevWeekNo As String = CType(grdCoachClass.Rows(Convert.ToInt16(irIndex)).FindControl("lblWeek"), Label).Text

                        If ((weekText = "0" Or weekText = "") And (prevWeekNo <> "" And prevWeekNo <> "0")) Then

                            Dim prevWeekStatus As String = CType(grdCoachClass.Rows(Convert.ToInt16(irIndex)).FindControl("ddlStatus"), DropDownList).SelectedValue
                            If prevWeekStatus = "On" Or prevWeekStatus = "Makeup" Or prevWeekStatus = "Substitute" Then
                                DirectCast(e.Row.Cells(6).FindControl("lblWeek"), Label).Text = Convert.ToInt32(prevWeekNo) + 1
                            Else
                                DirectCast(e.Row.Cells(6).FindControl("lblWeek"), Label).Text = prevWeekNo
                            End If
                            'cmdText = "select isnull( max(weekno),0) from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + ddlYear.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and Semester=" + ddlPhase.SelectedValue + " and SessionNo=" + ddlSessionNo.Text + " and [Status]  in ('On','Substitute','Makeup') and Level='" & ddlLevel.SelectedValue & "'"

                            'Dim cntWeek As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            'DirectCast(e.Row.Cells(6).FindControl("lblWeek"), Label).Text = cntWeek + 1
                        End If
                    Catch ex As Exception
                        CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                    End Try
                End If
                'Disable Modify Button
                If hdHideModify.Value = "false" Then
                    Dim btnModify As Button = DirectCast(e.Row.Cells(0).FindControl("btnModify"), Button)
                    If ddlStatus.SelectedItem.Text = "Select" Then
                        btnModify.Enabled = True
                        btnModify.Text = "Edit"
                        'Modified by Simson
                        hdHideModify.Value = "true"
                        hdHideModify.Value = "false"
                    Else
                        btnModify.Enabled = True
                        btnModify.Text = "Modify"

                    End If
                    cmdText = "select isnull(max(weekno),0) as weekno from CoachClassCal Where MemberId=" + hdMemberId.Value + " and EventYear=" + hdTable1Year.Value + " and ProductGroupId=" + hdProductGrp.Value + " and ProductId=" + hdProduct.Value + " and Semester='" + lblPhase.Text + "' and SessionNo=" + lblSessionNo.Text + " and Day='" + lblDay.Text + "' and [date] >'" & lblDate.Text & "' and Status in ('On','Substitute') "
                    'server
                    Dim iMaxWeekCnt As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                    'Modified by Simson
                    'If curWeek < iMaxWeekCnt Then
                    '    btnModify.Enabled = False
                    'End If
                    'Server
                    'Dim todayDate As DateTime = DateTime.Now.ToString("MM/dd/yyyy")


                    'If lblARelDate.Text <> "__/__/____" Then

                    '    Dim RelDate As DateTime = Convert.ToDateTime(lblARelDate.Text).ToString("MM/dd/yyyy")
                    '    If RelDate < todayDate And curWeek < iMaxWeekCnt Then
                    '        btnModify.Enabled = False
                    '    End If
                    'Else
                    '    If curWeek < iMaxWeekCnt Then
                    '        btnModify.Enabled = False
                    '    Else
                    '        btnModify.Enabled = True
                    '    End If

                    'End If
                    If hdStatus.Value = "Cancelled" Or hdStatus.Value = "Makeup Cancelled" Or hdStatus.Value = "Substitute Cancelled" Then
                        Dim lblDueDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblDueDt"), Label)
                        Dim lblARelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblARelDt"), Label)
                        Dim lblSRelDT As Label = DirectCast(e.Row.Cells(1).FindControl("lblSRelDt"), Label)

                        lblHWDueDate.Text = "__/__/____"
                        lblARelDate.Text = "__/__/____"
                        lblSRelDate.Text = "__/__/____"
                        lblHWRelDate.Text = "__/__/____"

                        CancelDateArr.Add(lblDate.Text)

                        ' ddlRecClassDate.Items.Clear()
                        '  ddlRecClassDate.Items.Add(New ListItem(lblDate.Text, lblDate.Text))

                    End If
                    'If lblScheduleType.Text = "Thanksgiving" Or lblScheduleType.Text = "Christmas" Then
                    '    btnModify.Enabled = False
                    'End If

                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        FillCoach(ddlCoach)
    End Sub
    'Public Sub ValidatSubstituteSession(Level As String, SubstituteCoachID As String, Semester As String)
    '    Try
    '        Dim CmdText As String = String.Empty
    '        Dim ds As New DataSet()
    '        Dim count As Integer = 0
    '        Dim SessionCreationDate As String = String.Empty
    '        Dim CoachChange As String = String.Empty
    '        'local
    '        Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
    '        ' Server
    '        ' String CurrDate = DateTime.Today.ToString("MM/dd/yyyy");
    '        Dim dt As DateTime = Convert.ToDateTime(CurrDate)
    '        CmdText = "select SessionCreationDate,CoachChange from WebConfControl where EventYear=" + ddYear.SelectedValue + ""
    '        If ddlProductGroup.SelectedValue <> "0" AndAlso ddlProductGroup.SelectedValue <> "All" Then
    '            CmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue + ""
    '        End If

    '        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)
    '        Dim Meetinkey As String = String.Empty
    '        If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                'local
    '                SessionCreationDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("SessionCreationDate").ToString()).ToString("dd/MM/yyyy")
    '                'Server
    '                'SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("MM/dd/yyyy");
    '                CoachChange = ds.Tables(0).Rows(0)("CoachChange").ToString()
    '            End If
    '        End If
    '        If SessionCreationDate <> "" Then
    '            Dim dtSession As DateTime = Convert.ToDateTime(SessionCreationDate)
    '            If dt >= dtSession AndAlso CoachChange.Trim() = "N" Then

    '                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.SubstituteMeetKey,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + DateTime.Now.Year + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + DateTime.Now.Year + "' and CS.MemberID=" + SubstituteCoachID + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + DateTime.Now.Year + " and Approved='Y') and CS.UserID is not null  and CS.Level='" + Level + "' and CS.Semester='" + Semester + "'"

    '                ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

    '                If ds.Tables(0) IsNot Nothing Then
    '                    If ds.Tables(0).Rows.Count > 0 Then
    '                        For Each dr As DataRow In ds.Tables(0).Rows
    '                            count += 1

    '                            Dim IsSameDay As Integer = 0
    '                            Dim IsSameTime As Integer = 0
    '                            Dim WebExID As String = dr("UserID").ToString()
    '                            Dim Pwd As String = dr("PWD").ToString()
    '                            Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
    '                            Dim ScheduleType As String = dr("ScheduleType").ToString()
    '                            Meetinkey = dr("meetingKey").ToString()
    '                            Dim year As String = dr("EventYear").ToString()
    '                            Dim eventID As String = dr("EventID").ToString()
    '                            Dim chapterId As String = "112"
    '                            Dim ProductGroupID As String = dr("ProductGroupID").ToString()
    '                            Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
    '                            Dim ProductID As String = dr("ProductID").ToString()
    '                            Dim ProductCode As String = dr("ProductCode").ToString()
    '                            'Dim Semester As String = dr("Semester").ToString()
    '                            'Dim Level As String = dr("Level").ToString()
    '                            Dim Sessionno As String = dr("SessionNo").ToString()
    '                            Dim CoachID As String = dr("MemberID").ToString()
    '                            Dim CoachName As String = dr("CoachName").ToString()

    '                            Dim MeetingPwd As String = "training"

    '                            Dim [Date] As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
    '                            Dim Time As String = dr("Time").ToString()
    '                            Dim Day As String = dr("Day").ToString()
    '                            Dim BeginTime As String = dr("Begin").ToString()
    '                            Dim EndTime As String = dr("End").ToString()
    '                            Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
    '                            Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
    '                            If BeginTime = "" Then
    '                                BeginTime = "20:00:00"
    '                            End If
    '                            If EndTime = "" Then
    '                                EndTime = "21:00:00"
    '                            End If

    '                            'If txtDuration.Text <> "" Then
    '                            '    Dim Duration As Integer = Convert.ToInt32(txtDuration.Text)
    '                            'End If
    '                            Dim timeZoneID As String = String.Empty

    '                            timeZoneID = "112"


    '                            Dim SignUpId As String = dr("SignupID").ToString()
    '                            Dim Mins As Double = 0.0
    '                            Dim dFrom As DateTime
    '                            Dim dTo As DateTime



    '                            Dim userID As String = Session("LoginID").ToString()


    '                            If dr("SubstituteMeetKey").ToString() = "" Then

    '                                ' RegisterAlternateHost(Meetinkey, EndTime, Day, SignUpId, WebExID, Pwd)
    '                            Else
    '                                'If dr("MakeupMeetKey").ToString() = "" Then
    '                                '    lblerr.Text = "Duplicate exists..!"
    '                                'Else
    '                                '    lblerr.Text = "Training sessions not created yet for the selected coach."
    '                                'End If
    '                            End If
    '                        Next
    '                    Else
    '                        'lblerr.Text = "No child is assigned for the selected Coach.."
    '                    End If
    '                End If
    '            Else
    '                If dt < dtSession Then
    '                    'lblerr.Text = (Convert.ToString("This application can only be run on ") & SessionCreationDate) + ""
    '                ElseIf CoachChange.Trim() = "Y" Then
    '                    'lblerr.Text = "This application cannot be run since the coach change flag is not set to N"
    '                End If
    '            End If
    '        Else
    '            'lblerr.Text = "This application can only be run on....."

    '        End If
    '        'throw new Exception(ex.Message);
    '    Catch ex As Exception

    '    End Try

    'End Sub



    Protected Sub ddlCoach_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCoach.SelectedIndexChanged
        divTable1.Visible = False
        dvColorStatus.Visible = False
        dvPracticeSection.Visible = False

        FillProductGroup()
    End Sub

    Protected Sub BtnAddUpdateMakeupSession_Click(sender As Object, e As EventArgs)
        ddlRecClassDate.Enabled = True
        lblUpdateOpt.Text = "Makeup Class"
        LblRecClassDate.Visible = True
        ddlRecClassDate.Visible = True
        LblMakeupClassDate.Text = "Makeup Class Date"
        ' populateRecClassDateDrpDown()
        txtDuration.Visible = False
        lblDuration.Visible = False
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)

    End Sub
    Protected Sub btnAddUpdatePractiseSession_Click(sender As Object, e As EventArgs)
        ddlRecClassDate.Enabled = True
        lblUpdateOpt.Text = "Practice Class"
        LblRecClassDate.Visible = False
        ddlRecClassDate.Visible = False
        LblMakeupClassDate.Text = "Pratcice Class Date"
        TxtMkeupDate.Text = ""
        ddlMakeupTime.SelectedValue = ""
        txtDuration.Text = ""
        txtDuration.Visible = True
        lblDuration.Visible = True
        dvPractiseSession.Visible = True
        fillPractiseSession()
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)

    End Sub


    Protected Sub BtnSaveMakeupClass_Click(sender As Object, e As EventArgs)
        If lblUpdateOpt.Text = "Makeup Class" Then

            If (hdnIsUpdate.Value = "1") Then
                If (ValidateMakeup()) Then
                    updateZoomSession()
                End If

            Else
                scheduleMakeUpSession()
            End If

        ElseIf lblUpdateOpt.Text = "Practice Class" Then

            If BtnSaveMakeupClass.Text = "Save" Then
                schedulePractiseSession()

            Else
                updatePractiseSession()
            End If

        End If
        'If ValidateMakeup() = "1" Then

        '    Dim Level As String = String.Empty
        '    Dim SessionNo As String = String.Empty
        '    Dim LblLevel As Label = DirectCast(rptCoachClass.Items(0).FindControl("lblLevel"), Label)
        '    Dim LblSessionNo As Label = DirectCast(rptCoachClass.Items(0).FindControl("lblSessionNo"), Label)
        '    Dim iDuration As Integer
        '    Dim SignupID As Integer = 2327
        '    Dim SerNo As String = "7"
        '    Dim WeekNo As String = "7"
        '    Level = LblLevel.Text
        '    SessionNo = LblSessionNo.Text

        '    Dim GrdCoachClass As GridView = DirectCast(rptCoachClass.Items(0).FindControl("rpt_grdCoachClassCal"), GridView)

        '    Dim i As Integer = 0
        '    For i = 0 To GrdCoachClass.Rows.Count - 1

        '        Dim lblCurDate As Label = DirectCast(GrdCoachClass.Rows(i).FindControl("lblDate"), Label)
        '        Dim strCurDate = lblCurDate.Text
        '        Dim RecurClassDate = ddlRecClassDate.Text
        '        If strCurDate = RecurClassDate Then
        '            SerNo = DirectCast(GrdCoachClass.Rows(i).FindControl("lblSerial"), Label).Text
        '            WeekNo = DirectCast(GrdCoachClass.Rows(i).FindControl("lblWeek"), Label).Text
        '            SignupID = DirectCast(GrdCoachClass.Rows(i).FindControl("lblSignUpId"), Label).Text
        '            iDuration = DirectCast(GrdCoachClass.Rows(i).FindControl("lblDuration"), Label).Text
        '        End If
        '    Next

        '    Dim strDur As String = String.Empty
        '    If iDuration >= 1 And iDuration < 1.5 Then
        '        strDur = "60"
        '    ElseIf iDuration >= 1.5 And iDuration < 2 Then
        '        strDur = "90"
        '    ElseIf iDuration >= 2 And iDuration < 2.5 Then
        '        strDur = "120"
        '    ElseIf iDuration >= 2.5 And iDuration < 3 Then
        '        strDur = "150"
        '    ElseIf iDuration >= 3 And iDuration < 3.5 Then
        '        strDur = "180"
        '    End If
        '    hdnDuration.Value = strDur
        '    'Dim dtCurrDay As DateTime = Convert.ToDateTime(TxtMkeupDate.Text)
        '    'Dim strCurDay = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")
        '    Dim strCurDay As String = "Monday"
        '    Dim iWeekNo As Integer = Convert.ToInt32(WeekNo) + 1
        '    hdnDay.Value = strCurDay
        '    Dim CmdText As String = String.Empty
        '    CmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
        '    CmdText = CmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & TxtMkeupDate.Text & "', '" + strCurDay + "', '" & ddlMakeupTime.SelectedValue & "', Duration," & SerNo & ", " & iWeekNo & ", 'Makeup','', '', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(SignupID)
        '    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, CmdText)
        '    lblMsg.Text = "Inserted Successfully"

        '    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "closeConfirmationBox();", True)


        '    Dim sessDuration As Integer = Convert.ToInt32(iDuration) + 30

        '    Dim EndSessTime As TimeSpan = GetTimeFromString(ddlMakeupTime.SelectedValue, sessDuration)
        '    Dim strEndTime As String = EndSessTime.ToString()
        '    Dim strEndSessTime As String = EndSessTime.ToString()
        '    Dim TsStartSessTime As TimeSpan = GetTimeFromStringSubtract(ddlMakeupTime.SelectedValue, -30)
        '    Dim strStartTime As String = TsStartSessTime.ToString()

        '    CmdText = "select distinct CS.UserID,CS.PWD from CalSignup CS   where  CS.[Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and CS.[End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and CS.day='" + strCurDay + "' and CS.eventyear = '" + ddlYear.SelectedValue + "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and CS.day='" + strCurDay + "' and  eventyear = '" + ddlYear.SelectedValue + "' and Accepted='Y') "

        '    ' CmdText = "select * from CalSignup CS left join WebConfLog WC on (CS.MemberID=WC.MemberID)  where  CS.[Begin] not between '" + ddlMakeupTime.SelectedValue + "' and '" + EndSessTime + "' and CS.day='" + day + "' and WC.BeginTime not between '" + ddlMakeupTime.SelectedValue + "' and '" + EndSessTime + "' and CS.eventyear = '" + ddYear.SelectedValue + "' and CS.Accepted='Y' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID not in (select distinct UserID from CalSignUp where [Begin] between '" + ddlMakeupTime.SelectedValue + "' and '" + EndSessTime + "' and CS.day='" + strDay + "' and  eventyear = '" + ddYear.SelectedValue + "' and Accepted='Y') and CS.UserID<>'" + userID + "' and CS.PWD<>'" + Pwd + "'";

        '    Dim ds As DataSet = New DataSet()
        '    ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

        '    Dim ds1 As New DataSet()
        '    CmdText = "select WC.UserID,WC.PWD from WebConfLog WC   where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and  WC.eventyear = '2015' and WC.MemberID in(select CMemberID from CoachReg where EventYear=2015 and Approved='Y')"
        '    ds1 = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdText)

        '    Dim WebExUserID As String = String.Empty
        '    Dim WebExUserPwd As String = String.Empty

        '    Dim Uid As String = String.Empty
        '    For Each dr1 As DataRow In ds.Tables(0).Rows
        '        If ds1.Tables(0).Rows.Count > 0 Then
        '            For Each dr2 As DataRow In ds1.Tables(0).Rows
        '                If dr1("UserID").ToString() <> dr2("UserID").ToString() Then
        '                    WebExUserID = dr1("UserID").ToString()
        '                    WebExUserPwd = dr1("Pwd").ToString()
        '                End If
        '            Next
        '        Else
        '            WebExUserID = dr1("UserID").ToString()
        '            WebExUserPwd = dr1("Pwd").ToString()
        '        End If
        '    Next
        '    If WebExUserID <> "" AndAlso WebExUserPwd <> "" Then
        '        SwitchStudentAndCreateTrainingSession(SignupID)
        '    Else

        '        lblMsg.Text = "Time is not available...!"
        '    End If

        'End If
    End Sub

    Public Function ValidateMakeup() As String
        Dim RetVal = "1"
        If ddlRecClassDate.SelectedValue = "0" Then
            RetVal = "-1"
            lblMakeupErrMsg.Text = "Please select Recurring Class Date"
        ElseIf TxtMkeupDate.Text = "" Then
            RetVal = "-1"
            lblMakeupErrMsg.Text = "Please fill Makeup Class Date"
        ElseIf ddlMakeupTime.SelectedValue = "" Then
            RetVal = "-1"
            lblMakeupErrMsg.Text = "Please fill Time"


            'ElseIf txtDuration.Text = "" Then
            '    RetVal = "-1"
            '    lblMakeupErrMsg.Text = "Please fill Duration"
        ElseIf TxtMkeupDate.Text <> "" Then
            If (validateMakeupDate() = "1") Then
                If (validateMakeupDateAndTime() = "1") Then

                    If (hdnSelIndex.Value = "0" Or valdiatePriorClassStatus() = "1") Then
                    Else
                        RetVal = "-1"
                    End If
                Else
                    RetVal = "-1"

                End If
            Else
                RetVal = "-1"
            End If
        End If

        If (RetVal = "-1") Then
            txtDuration.Visible = False
            lblDuration.Visible = False
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)
        End If

        Return RetVal
    End Function

    Public Function valdiatePriorClassStatus() As String
        Dim retVal As String = "1"

        If ddlRecClassDate.SelectedValue <> "0" AndAlso TxtMkeupDate.Text <> "" Then
            Dim selIndex As Integer = ddlRecClassDate.SelectedIndex
            Dim recClassPrevDate As String = ddlRecClassDate.SelectedValue
            Dim cmdText As String = String.Empty
            Dim count As Integer = 0



            Dim ds As New DataSet()
            Dim priorClassDate As String = Convert.ToDateTime(ddlRecClassDate.SelectedItem.Text).AddDays(-7).ToShortDateString()

            cmdText = (Convert.ToString("select count(*) as CountSet from CoachClassCal where EventYear=" + ddlYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSessionNo.SelectedValue + " and Date='") & priorClassDate) + "'"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    count = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString())
                End If
            End If
            If count = 0 Then
                retVal = "-1"
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlertmsgClassStatus();", True)
            Else

                cmdText = (Convert.ToString("select count(*) as CountSet from CoachClassCal where EventYear=" + ddlYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSessionNo.SelectedValue + " and Date between '") & recClassPrevDate) + "' and  '" + Convert.ToDateTime(recClassPrevDate).AddDays(7).ToShortDateString() + "' and Status='Makeup'"


                ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        count = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString())
                    End If
                End If


                If count = 0 Then
                Else
                    lblMakeupErrMsg.Text = "Duplicate exists."
                    retVal = "-1"
                End If
            End If

        End If

        Return retVal

    End Function

    Public Function validateMakeupDate() As String
        Dim retVal As String = "1"
        Dim dtCurrent As DateTime = DateTime.Now
        Dim strCurrentDate As String = DateTime.Now.ToString("MM/dd/yyyy")

        'string strCurrentDate = DateTime.Now.ToString("dd/MM/yyyy");

        dtCurrent = Convert.ToDateTime(strCurrentDate)
        If ddlRecClassDate.SelectedValue <> "0" AndAlso TxtMkeupDate.Text <> "" Then
            Dim startDate As String = TxtMkeupDate.Text
            Dim dtStartDate As DateTime = Convert.ToDateTime(startDate.ToString())

            Dim dtNextRecDate As DateTime = Convert.ToDateTime(ddlRecClassDate.SelectedValue).AddDays(7)

            Dim dtClassDate As DateTime = Convert.ToDateTime(ddlRecClassDate.SelectedValue)

            If dtStartDate < dtCurrent AndAlso dtClassDate < dtCurrent Then

                lblMakeupErrMsg.Text = "Makeup Date should be greater than Rec Class Date and current date"
                retVal = "-1"
            ElseIf dtStartDate > dtNextRecDate Then
                lblMakeupErrMsg.Text = "Makeup Date should be less than or equal to  next Rec Class Date"

                retVal = "-1"
            Else
                retVal = "1"
            End If
        End If
        Return retVal

    End Function


    Public Function validateMakeupDateAndTime() As String
        Dim ds As DataSet = New DataSet()
        Try
            Dim cmdtext As String = "select BeginTime, EndTime, Duration from WebConfLog where Eventyear='" & ddlYear.SelectedItem.Value & "' and memberID='" & ddlCoach.SelectedValue & "' and ProductGroupId='" & ddlProductGroup.SelectedValue & "' and ProductId='" & ddlProduct.SelectedValue & "' and Level='" & ddlLevel.SelectedItem.Text & "' and Session='" & ddlSessionNo.SelectedValue & "' and SessionType='Recurring Meeting' and Semester='" + ddlPhase.SelectedValue + "'"
            Dim duration As Integer = 0
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("Duration") IsNot Nothing Then
                        duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
                    Else
                        duration = 90
                    End If
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try

        Dim retVal As String = "1"
        If (ds.Tables(0).Rows.Count) Then



            Dim dtCurrent As DateTime = DateTime.Now
            Dim dtCurrentTime As DateTime = DateTime.Now
            'string strCurrentDate = DateTime.Now.ToString("MM/dd/yyyy");
            'Dim strCurrentDate As String = DateTime.Now.ToString("dd/MM/yyyy")
            Dim strCurrentDate As String = DateTime.Now.ToString("MM/dd/yyyy")
            dtCurrent = Convert.ToDateTime(strCurrentDate)
            If ddlRecClassDate.SelectedValue <> "0" AndAlso TxtMkeupDate.Text <> "" Then
                Dim startDate As String = TxtMkeupDate.Text
                Dim startTime As String = ddlMakeupTime.SelectedValue
                Dim startDateTime As String = Convert.ToString(startDate & Convert.ToString(" ")) & startTime
                Dim dtStartDateTime As DateTime = Convert.ToDateTime(startDateTime.ToString())
                Dim dtStartDate As DateTime = Convert.ToDateTime(startDate.ToString())
                Dim dtClassDate As DateTime = Convert.ToDateTime(ddlRecClassDate.SelectedValue)

                Dim nakeupTime As String = Convert.ToString(dtStartDateTime.AddMinutes(90).ToString("MM/dd/yyyy HH:mm:ss"))

                Dim strmakeupTime As String = nakeupTime.Substring(nakeupTime.IndexOf(" "c)).Trim()
                nakeupTime = strmakeupTime.Substring(0, 8).Trim()
                Dim overlapCount As Integer = 0
                Dim dtNextRecDate As DateTime = Convert.ToDateTime(ddlRecClassDate.SelectedValue).AddDays(7)

                If dtNextRecDate.ToShortDateString() = dtStartDateTime.ToShortDateString() Then
                    Dim cmdQuery As String = (Convert.ToString((Convert.ToString("select * from webconflog where Eventyear=" + ddlYear.SelectedItem.Value + " and memberID=" + ddlCoach.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and Session=" + ddlSessionNo.SelectedValue + " and (BeginTime between ('" + ddlMakeupTime.SelectedValue + "') and ('") & nakeupTime) + "') or EndTime between('" + ddlMakeupTime.SelectedValue + "') and ('") & nakeupTime) + "') ) and  SessionType='Recurring Meeting'"


                    Try
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdQuery)
                        If ds.Tables(0) IsNot Nothing Then
                            If ds.Tables(0).Rows.Count > 0 Then
                                overlapCount = ds.Tables(0).Rows.Count
                            End If
                        End If
                    Catch ex As Exception
                        Throw New Exception(ex.Message)
                    End Try

                    If overlapCount > 0 Then
                        lblMakeupErrMsg.Text = "Makeup Time should not between rec class time."
                        retVal = "-1"
                    Else
                        retVal = "1"
                    End If


                ElseIf dtStartDate < dtCurrent AndAlso dtClassDate < dtCurrent Then
                    lblMakeupErrMsg.Text = "Makeup Date should be greater than today date and Rec Class Date"
                    retVal = "-1"
                ElseIf dtStartDateTime <= dtCurrentTime Then
                    lblMakeupErrMsg.Text = "Begin Time should be greater than current time"
                    retVal = "-1"
                Else
                    retVal = "1"
                End If
            End If
        Else
            lblMakeupErrMsg.Text = "Reqular zoom session not yet scheduled."
        End If
        Return retVal

    End Function



    Public Function validatePractiseSession() As String
        Dim RetVal = "1"
        If TxtMkeupDate.Text = "" Then
            RetVal = "-1"
            lblMakeupErrMsg.Text = "Please fill Practice Class Date"
        ElseIf ddlMakeupTime.SelectedValue = "" Then
            RetVal = "-1"
            lblMakeupErrMsg.Text = "Please fill Time"
        ElseIf txtDuration.Text = "" Then
            RetVal = "-1"
            lblMakeupErrMsg.Text = "Please fill Duration"
        ElseIf TxtMkeupDate.Text <> "" Then
            Dim dtCurrent As DateTime = DateTime.Now
            Dim dtCurrentDateTime As DateTime = DateTime.Now.AddMinutes(60)
            Dim strCurrentDate As String = DateTime.Now.ToString("MM/dd/yyyy")

            ' string strCurrentDate = DateTime.Now.ToString("dd/MM/yyyy");

            dtCurrent = Convert.ToDateTime(strCurrentDate)
            Dim startDate As String = TxtMkeupDate.Text
            Dim dtStartDate As DateTime = Convert.ToDateTime(startDate.ToString())

            Dim startDateTime As String = TxtMkeupDate.Text + " " + ddlMakeupTime.SelectedValue
            Dim dtStartDateTime As DateTime = Convert.ToDateTime(startDateTime.ToString())

            If dtStartDate < dtCurrent Then
                lblMakeupErrMsg.Text = "Practice Class Date should be greater than today date"
                RetVal = "-1"
            ElseIf (dtStartDateTime <= dtCurrentDateTime) Then

                lblMakeupErrMsg.Text = "Practice Time should be greater than to current time"
                RetVal = "-1"

            Else
                RetVal = "1"
            End If

        End If
        If (RetVal = "-1") Then
            txtDuration.Visible = True
            lblDuration.Visible = True
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)
        End If
        Return RetVal
    End Function


    Public Sub SwitchStudentAndCreateTrainingSession(SignUpId As String)



    End Sub




    Private Function GetTimeFromString(timeString As String, addMinute As Integer) As TimeSpan
        Dim dateWithTime As DateTime = (DateTime.MinValue).AddMinutes(addMinute)
        DateTime.TryParse(timeString, dateWithTime)
        Return dateWithTime.AddMinutes(addMinute).TimeOfDay
    End Function
    Private Function GetTimeFromStringSubtract(timeString As String, addMinute As Integer) As TimeSpan
        Dim dateWithTime As DateTime = (DateTime.MinValue)
        DateTime.TryParse(timeString, dateWithTime)
        Return dateWithTime.AddMinutes(addMinute).TimeOfDay
    End Function



    Public Sub createZoomMeeting(mode As String, duration As Integer, hostID As String)
        Try


            Dim URL As String = String.Empty
            Dim durationHrs As String = duration
            Dim service As String = "1"
            If mode = "1" Then
                URL = "https://api.zoom.us/v1/meeting/create"
            ElseIf mode = "2" Then
                URL = "https://api.zoom.us/v1/meeting/update"
            End If
            Dim urlParameter As String = String.Empty

            Dim test As String = (DateTime.UtcNow.ToString("s") + "Z").ToString()

            Dim [date] As String = TxtMkeupDate.Text
            Dim time As String = ddlMakeupTime.SelectedValue
            Dim dateTime__1 As String = (Convert.ToString([date] & Convert.ToString("T")) & time) + "Z"
            Dim topic As String = hdnMeetingTitle.Value

            Dim dtFromS As New DateTime()
            Dim dtEnds As DateTime = DateTime.Now
            Dim mins As Double = 40.0
            Dim dtTo As New DateTime()
            If DateTime.TryParse(time, dtFromS) Then
                Dim TS As TimeSpan = dtFromS - dtEnds
                mins = TS.TotalMinutes
                dtTo = dtFromS.AddHours(4)
            End If
            Dim timeFormat As String = dtTo.Hour.ToString() + ":" + dtTo.Minute.ToString() + ":" + dtTo.Second.ToString()

            dateTime__1 = (Convert.ToString([date] & Convert.ToString("T")) & timeFormat) + "Z"
            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hostID + ""
            If mode = "1" Then
                urlParameter += (Convert.ToString("&topic=") & topic) + ""
                urlParameter += "&password=training"
                urlParameter += "&type=2"
                urlParameter += (Convert.ToString("&start_time=") & dateTime__1) + ""
                urlParameter += (Convert.ToString("&duration=") & durationHrs) + ""
                urlParameter += "&timezone=GMT-5:00"
                urlParameter += "&option_jbh=true"
                urlParameter += "&option_host_video=true"
                urlParameter += "&option_audio=both"
            ElseIf mode = "2" Then
                service = "2"
                urlParameter += "&id=" + hdnTrainingSessionKey.Value + ""
                urlParameter += (Convert.ToString("&start_time=") & dateTime__1) + ""
                urlParameter += "&duration=" + durationHrs + ""
                urlParameter += "&timezone=GMT-5:00"
                urlParameter += "&option_jbh=true"
                urlParameter += "&option_host_video=true"
                urlParameter += "&option_audio=both"
            End If




            makeZoomAPICall(urlParameter, URL, service)
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub

    Public Sub makeZoomAPICall(urlParameters As String, URL As String, serviceType As String)
        Try


            Dim objRequest As HttpWebRequest = DirectCast(WebRequest.Create(URL), HttpWebRequest)
            objRequest.Method = "POST"
            objRequest.ContentLength = urlParameters.Length
            objRequest.ContentType = "application/x-www-form-urlencoded"

            ' post data is sent as a stream
            Dim myWriter As StreamWriter = Nothing
            myWriter = New StreamWriter(objRequest.GetRequestStream())
            myWriter.Write(urlParameters)
            myWriter.Close()

            ' returned values are returned as a stream, then read into a string
            Dim postResponse As String
            Dim objResponse As HttpWebResponse = DirectCast(objRequest.GetResponse(), HttpWebResponse)
            Using responseStream As New StreamReader(objResponse.GetResponseStream())
                postResponse = responseStream.ReadToEnd()





                responseStream.Close()
            End Using
            If serviceType = "1" Then
                'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", True)
                'Dim parser = New JavaScriptSerializer()

                'Dim obj As dynamic = parser.Deserialize(Of dynamic)(postResponse)



                ' Dim json As JObject = DirectCast(JsonConvert.DeserializeObject(postResponse), JObject)
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                'Dim serializer = New JavaScriptSerializer()
                hdnTrainingSessionKey.Value = json("id").ToString()
                hdnHostMeetingURL.Value = json("start_url").ToString()
                hdnMeetingURL.Value = json("join_url").ToString()
                hdnMeetingStatus.Value = "SUCCESS"
            ElseIf serviceType = "2" Then
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)
                hdnMeetingStatus.Value = "SUCCESS"
            ElseIf serviceType = "3" Then

                hdnMeetingStatus.Value = "SUCCESS"
                lblMsg.Text = "Makeup Session deleted successfully"



                'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);


            End If
        Catch
            hdnMeetingStatus.Value = "Failure"
        End Try
        'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    End Sub
    Protected Sub btnCreateZoomSession_Click(sender As Object, e As EventArgs)
        SwitchStudentAndCreateTrainingSession(hdnSignupID.Value)
    End Sub

    Protected Sub btnCreateClass_Click(sender As Object, e As EventArgs)
        createClassBeforeCoachPaper()
    End Sub
    Protected Sub btnCancelmakeup_Click(sender As Object, e As EventArgs)
        cancelMakeupSession()
    End Sub
    Protected Sub btnCancelSubstitute_Click(sender As Object, e As EventArgs)
        cancelSubstituteSession()
    End Sub

    Public Sub scheduleMakeUpSession()
        If (ValidateMakeup() = "1") Then


            Dim cmdText As String = Nothing
            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],Vl.UserID,Vl.PWD,VL.HostID,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookup VL on (VL.VRoom=CS.VRoom) where Accepted='Y' and SignUpId=" & hdnSignupID.Value & ""

            Dim duration As Integer
            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows

                        Dim hostID As String = String.Empty
                        hostID = dr("HostID").ToString()
                        Dim WebExID As String = dr("UserID").ToString()
                        Dim WebExPwd As String = dr("PWD").ToString()
                        Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
                        Dim ScheduleType As String = dr("ScheduleType").ToString()

                        Dim year As String = dr("EventYear").ToString()
                        Dim eventID As String = dr("EventID").ToString()

                        Dim ProductGroupID As String = dr("ProductGroupID").ToString()
                        Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
                        Dim ProductID As String = dr("ProductID").ToString()
                        Dim ProductCode As String = dr("ProductCode").ToString()
                        Dim Semester As String = dr("Semester").ToString()
                        Dim Level As String = dr("Level").ToString()
                        Dim Sessionno As String = dr("SessionNo").ToString()
                        Dim CoachID As String = dr("MemberID").ToString()
                        Dim MeetingPwd As String = "training"

                        Dim Time As String = dr("Time").ToString()
                        Dim Day As String = dr("Day").ToString()
                        Dim STime As String = dr("Begin").ToString()
                        Dim ETime As String = dr("End").ToString()

                        Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
                        Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
                        'string endTime = "01:00 AM";
                        'TimeSpan time1 = DateTime.Parse(endTime).Subtract(DateTime.Parse(Time));
                        'TimeSpan time2 = GetTimeFromString1(dr["End"].ToString());
                        'double hours = (time1 - time2).TotalHours;
                        Dim BeginTime As String = dr("Begin").ToString()
                        Dim EndTime As String = dr("End").ToString()

                        Dim timeZoneID As String = "11"
                        Dim TimeZone As String = "EST"
                        ' SignUpId = dr("SignupID").ToString()
                        Dim Mins As Double = 0.0
                        Dim dFrom As DateTime
                        Dim dTo As DateTime


                        Dim sDateFrom As String = BeginTime
                        Dim sDateTo As String = EndTime
                        If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                            Dim TS As TimeSpan = dTo - dFrom

                            Mins = TS.TotalMinutes
                        End If
                        Dim durationHrs As String = Mins.ToString("0")
                        If durationHrs.IndexOf("-") > -1 Then
                            durationHrs = "188"
                        End If

                        duration = Convert.ToInt32(durationHrs)
                        hdnDuration.Value = duration.ToString()

                        If timeZoneID = "11" Then
                            TimeZone = "EST/EDT – Eastern"
                        ElseIf timeZoneID = "7" Then
                            TimeZone = "CST/CDT - Central"
                        ElseIf timeZoneID = "6" Then
                            TimeZone = "MST/MDT - Mountain"
                        End If


                        Dim CoachName As String = String.Empty

                        CoachName = dr("CoachName").ToString()
                        Dim meetingTitle As String = String.Empty
                        meetingTitle = CoachName & "-" & ProductCode & "-" & Level & "-" & Sessionno
                        hdnMeetingTitle.Value = meetingTitle
                        'If dr("MeetingKey").ToString() = "" Or dr("MeetingKey").ToString() = "0" Then
                        startDate = TxtMkeupDate.Text
                        Time = ddlMakeupTime.SelectedValue
                        'durationHrs = txtDuration.Text
                        Day = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")
                        TimeZone = "EST/EDT – Eastern"
                        ' CreateTrainingSession(WebExUserID, WebExPasswd, ScheduleType, Capacity, startDate.Replace("-", "/"), Time, _
                        ' Day, STime, ETime, EndDate.Replace("-", "/"), durationHrs, CoachName, ProductCode)
                        hostID = checkAvailableVrooms(duration)
                        If hostID <> "" Then
                            createZoomMeeting("1", duration, hostID)
                        Else
                            lblErr.Text = "Time is not available.."
                            Exit Sub
                        End If

                        If hdnMeetingStatus.Value = "SUCCESS" Then
                            ' GetHostUrlMeeting(WebExUserID, WebExPasswd)

                            Dim meetingURL As String = hdnHostMeetingURL.Value

                            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD, SessionType,Status)values(" & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "'," & CoachID & ",'" & hdnTrainingSessionKey.Value & "','" & TxtMkeupDate.Text & "','" & TxtMkeupDate.Text & "','" & ddlMakeupTime.SelectedValue & "','" & ddlMakeupTime.SelectedValue & "','" & Semester & "','" & Level & "'," & Sessionno & ",'training'," & duration & "," & timeZoneID & ",'" & TimeZone & "',getDate()," & Session("LoginID").ToString() & ",'" & meetingURL & "','" & Day & "','" & hdnWebExID.Value & "','" & hdnWebExPWD.Value & "','Scheduled Meeting','Active')"

                            'cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "','" & startDate & "','" & startDate & "','" & Time & "',null," & durationHrs & "," & timeZoneID & ",'" & TimeZone & "','" & SignUpId & "'," & CoachID & ",'" & Semester & "','" & Level & "','" & Sessionno & "','" & meetingURL & "','" & hdnTrainingSessionKey.Value & "','" & MeetingPwd & "','Active','" & Day & "','" & UserID & "','" & WebExID & "','" & WebExPwd & "'"

                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                            If lblUpdateOpt.Text = "Makeup Class" Then

                                Dim strQuery As String
                                Dim cmd As SqlCommand
                                strQuery = "Update CalSignup set MakeUpURL=@MakeupURL,MakeupMeetKey=@MakeupMeetKey, MakeUpPwd=@MakeupPwd, MakeUpDate=@MakeupDate,MakeUpTime=@MakeupTime, MakeUpDuration=@MakeupDuration where SignupID=@SignupID"
                                cmd = New SqlCommand(strQuery)
                                cmd.Parameters.AddWithValue("@MakeupURL", meetingURL)
                                cmd.Parameters.AddWithValue("@MakeupMeetKey", hdnTrainingSessionKey.Value)
                                cmd.Parameters.AddWithValue("@MakeupPwd", "training")
                                cmd.Parameters.AddWithValue("@MakeupDate", TxtMkeupDate.Text)
                                cmd.Parameters.AddWithValue("@MakeupTime", ddlMakeupTime.SelectedValue)
                                cmd.Parameters.AddWithValue("@MakeupDuration", duration)
                                cmd.Parameters.AddWithValue("@SignupID", duration)

                                Dim objNSF As NSFDBHelper = New NSFDBHelper()
                                objNSF.InsertUpdateData(cmd)


                                ' cmdText = "Update CalSignup set MakeUpURL='" & meetingURL & "', MakeupMeetKey='" & hdnTrainingSessionKey.Value & "', MakeUpPwd='training',MakeUpDate='" & TxtMkeupDate.Text & "',MakeUpTime='" & ddlMakeupTime.SelectedValue & "',MakeUpDuration='" & duration & "' where SignupID=" + duration + ""
                                'SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                Dim dsChild As New DataSet()
                                Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ProductGroupID & "' and CR.ProductID='" & ProductID & "' and CR.CMemberID=" & CoachID & " and CR.EventYear=" & ddlYear.SelectedValue & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ProductGroupID & "' and ProductID='" & ProductID & "' and CMemberID=" & CoachID & " and Approved='Y' and Level = '" & Level & "' and SessionNo=" & Sessionno & ") and CR.Level='" & Level & "' and CR.SessionNo=" & Sessionno & " and CR.Semester='" & Semester & "'"
                                dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
                                Dim ChidName As String = String.Empty
                                Dim Email As String = String.Empty
                                Dim City As String = String.Empty
                                Dim Country As String = String.Empty
                                Dim ChildNumber As String = String.Empty
                                Dim CoachRegID As String = String.Empty

                                If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                                    If dsChild.Tables(0).Rows.Count > 0 Then
                                        For Each drChild As DataRow In dsChild.Tables(0).Rows
                                            ChidName = drChild("Name").ToString()
                                            Email = drChild("Email").ToString()
                                            City = drChild("City").ToString()
                                            Country = drChild("Country").ToString()
                                            ChildNumber = drChild("ChildNumber").ToString()
                                            CoachRegID = drChild("CoachRegID").ToString()


                                            If hdnMeetingStatus.Value = "SUCCESS" Then


                                                Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & Session("LoginID").ToString() & "' where CoachRegID='" & CoachRegID & "'"
                                                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                                            End If
                                        Next
                                    End If
                                End If

                            End If


                        Else
                        End If
                        'Else

                        'End If
                    Next


                    If lblUpdateOpt.Text = "Makeup Class" Then


                        Dim LblLevel As Label = DirectCast(rptCoachClass.Items(0).FindControl("lblLevel"), Label)
                        Dim LblSessionNo As Label = DirectCast(rptCoachClass.Items(0).FindControl("lblSessionNo"), Label)
                        Dim iDuration As Integer
                        Dim SignupID As Integer = hdnSignupID.Value
                        Dim SerNo As String = hdnSerNo.Value
                        Dim WeekNo As String = hdnWeekNo.Value


                        Dim GrdCoachClass As GridView = DirectCast(rptCoachClass.Items(0).FindControl("rpt_grdCoachClassCal"), GridView)

                        Dim i As Integer = 0
                        For i = 0 To GrdCoachClass.Rows.Count - 1

                            Dim lblCurDate As Label = DirectCast(GrdCoachClass.Rows(i).FindControl("lblDate"), Label)
                            Dim strCurDate = lblCurDate.Text
                            Dim RecurClassDate = ddlRecClassDate.SelectedValue
                            If strCurDate = RecurClassDate Then
                                SerNo = DirectCast(GrdCoachClass.Rows(i).FindControl("lblSerial"), Label).Text
                                'WeekNo = DirectCast(GrdCoachClass.Rows(i).FindControl("lblWeek"), Label).Text
                                'SignupID = DirectCast(GrdCoachClass.Rows(i).FindControl("lblSignUpId"), Label).Text
                                iDuration = DirectCast(GrdCoachClass.Rows(i).FindControl("lblDuration"), Label).Text
                            End If
                        Next

                        Dim strDur As String = String.Empty
                        If iDuration >= 1 And iDuration < 1.5 Then
                            strDur = "60"
                        ElseIf iDuration >= 1.5 And iDuration < 2 Then
                            strDur = "90"
                        ElseIf iDuration >= 2 And iDuration < 2.5 Then
                            strDur = "120"
                        ElseIf iDuration >= 2.5 And iDuration < 3 Then
                            strDur = "150"
                        ElseIf iDuration >= 3 And iDuration < 3.5 Then
                            strDur = "180"
                        End If
                        hdnDuration.Value = strDur
                        'Dim dtCurrDay As DateTime = Convert.ToDateTime(TxtMkeupDate.Text)
                        'Dim strCurDay = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")
                        Dim strCurDay As String = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")
                        Dim iWeekNo As Integer = WeekNo
                        hdnDay.Value = strCurDay

                        If hdnOrgStatusUpd.Value = "" Then


                            cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                            cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & hdnDate.Value & "', Day, Time, Duration," & SerNo & ", " & iWeekNo & ", 'Cancelled',null, '" & hdnReason.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(SignupID)
                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                        Else
                            cmdText = "UPDATE CoachClassCal SET SerNo=" & SerNo & ", WeekNo = " & WeekNo & " ,Status='Cancelled',Substitute=null, Reason='" & hdnReason.Value & "' ,ModifyDate=GETDATE(), ModifiedBy=" & Session("LoginID")
                            cmdText = cmdText + " where CoachClassCalId=" & hdnCoachClaassID.Value
                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                        End If


                        cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
                        cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & TxtMkeupDate.Text & "', '" + strCurDay + "', '" & ddlMakeupTime.SelectedValue & "', " & duration & "," & SerNo & ", " & iWeekNo & ", 'Makeup',null, '" & hdnReason.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(SignupID)
                        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                        lblMsg.Text = "Inserted Successfully"

                        Try
                            cmdText = "select CoachPaperId from CoachPapers where EventYear=" & hdTable1Year.Value & " and EventId=13 and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Level='" & ddlLevel.Text & "' and WeekId=" & WeekNo & " and DocType='Q'"
                            Dim iCoachPaperId As Integer = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            Dim coachRelID As Integer = 0

                            cmdText = "select coachRelID from coachreldates where EventYear=" & hdTable1Year.Value & "  and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Level='" & ddlLevel.SelectedValue & "' and Session=" & ddlSessionNo.SelectedValue & " and MemberID=" & ddlCoach.SelectedValue & " and CoachPaperID=" & iCoachPaperId & ""

                            coachRelID = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                            If iCoachPaperId > 0 Then

                                Dim QRDate As DateTime = Convert.ToDateTime(TxtMkeupDate.Text)
                                Dim QDDate As DateTime = Convert.ToDateTime(TxtMkeupDate.Text).AddDays(5)

                                Dim ARDate As DateTime = Convert.ToDateTime(TxtMkeupDate.Text).AddDays(7)
                                Dim SRDate As DateTime = Convert.ToDateTime(TxtMkeupDate.Text).AddDays(-1)

                                If coachRelID > 0 Then
                                    cmdText = "update CoachRelDates Set   If coachRelID > 0 Then"
                                    cmdText = "update CoachRelDates Set qreleasedate='" & QRDate & "', qdeadlinedate='" & QDDate & "', areleasedate='" & ARDate & "', SReleaseDate='" & SRDate & "', ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID").ToString() & " WHERE CoachPaperId=" & iCoachPaperId & " and MemberId= " & ddlCoach.SelectedValue & " and  EventYear=" & hdTable1Year.Value & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "' and Level='" & ddlLevel.SelectedValue & "' and Session =" & ddlSessionNo.SelectedValue

                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                Else


                                    cmdText = "insert into CoachRelDates (CoachPaperId, MemberID, EventYear, ProductGroupId, ProductGroupCode, ProductId, ProductCode,Semester, Level, Session, QReleaseDate, QDeadlineDate, AReleaseDate, SReleaseDate, CreateDate, CreatedBy) values(" & iCoachPaperId & ", " & ddlCoach.SelectedValue & ", " & ddlYear.SelectedValue & "," & ddlProductGroup.SelectedValue & ", '" & ddlProductGroup.SelectedItem.Text & "', " & ddlProduct.SelectedValue & ", '" & ddlProduct.SelectedItem.Text & "', " & ddlPhase.SelectedValue & ", '" & ddlLevel.SelectedValue & "', " & ddlSessionNo.SelectedValue & ", '" & QRDate & "', '" & QDDate & "', '" & ARDate & "', '" & SRDate & "', Getdate(), " & Session("LoginID").ToString() & ")"

                                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                                End If
                            End If
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                        End Try

                        BindCoachClassDetailsInRepeater()
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "closeConfirmationBox();", True)


                        Exit Sub
                    End If
                End If
            End If
        End If

    End Sub
    Public Sub createClassBeforeCoachPaper()
        If (hdnStatus.Value = "On" Or hdnStatus.Value = "Cancelled") Then


            cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
            cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & hdnDate.Value & "', Day, Time, Duration," & hdnSerNo.Value & ", " & hdnWeekNo.Value & ", '" & hdnStatus.Value & "',null, '" & hdnReason.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(hdnSignupID.Value)
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            lblMsg.Text = "Inserted Successfully"

        ElseIf (hdnStatus.Value = "Substitute") Then

            cmdText = "update WebConfLog set SubstituteCoachID=" & hdnSubCoachID.Value & " where MemberID=" & hdnCoachID.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [Session]=" & hdnSessionNo.Value & ""
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "update CalSignup set SubstituteCoachID=" & hdnSubCoachID.Value & ", SubstituteDate='" & hdnDate.Value & "' where MemberID=" & hdnCoachID.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [SessionNo]=" & hdnSessionNo.Value & ""
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Substitute, Reason, CreateDate, CreatedBy)"
            cmdText = cmdText & " select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo, '" & hdnDate.Value & "', Day, Time, Duration," & hdnSerNo.Value & ", " & hdnWeekNo.Value & ", '" & hdnStatus.Value & "'," & hdnSubCoachID.Value & ", '" & hdnReason.Value & "', GETDATE()," & CInt(Session("LoginID")) & " from calsignup where signupid=" & CInt(hdnSignupID.Value)
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            lblMsg.Text = "Inserted Successfully"

        ElseIf (hdnStatus.Value = "Makeup") Then


            ddlRecClassDate.Enabled = True
            lblUpdateOpt.Text = "Makeup Class"
            LblRecClassDate.Visible = True
            ddlRecClassDate.Visible = True
            LblMakeupClassDate.Text = "Makeup Class Date"
            '  populateRecClassDateDrpDown()
            txtDuration.Visible = False
            lblDuration.Visible = False

            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)

            Exit Sub
        End If
        BindCoachClassDetailsInRepeater()
    End Sub
    Public Sub cancelMakeupSession()
        Try

            Dim sessionKey As String = String.Empty
            Dim cmdMeetingkeyText As String = String.Empty
            cmdMeetingkeyText = "select SessionKey from WebConfLog where EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [Session]=" & hdnSessionNo.Value & " and SessionType='Scheduled Meeting'"
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdMeetingkeyText)

            If ds IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    sessionKey = ds.Tables(0).Rows(0)("SessionKey").ToString()
                End If
            End If
            deleteMeeting(sessionKey)

            If hdnMeetingStatus.Value = "SUCCESS" Then


                cmdText = "delete from WebConfLog where MemberID=" & hdnCoachID.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [Session]=" & hdnSessionNo.Value & " and SessionType='Scheduled Meeting'"
                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                cmdText = "update CalSignup set MakeupMeetkey=null, MakeupDate=null, MakeupURL=null, MakeupTime=null where MemberID=" & hdnCoachID.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [SessionNo]=" & hdnSessionNo.Value & ""

                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                cmdText = "update CoachClassCal set Status='" & hdnStatus.Value & "' where MemberID=" & hdnCoachID.Value & " and [Date]='" & hdnDate.Value & "' and WeekNo=" & hdnWeekNo.Value & " and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [SessionNo]=" & hdnSessionNo.Value & ""


                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                lblMsg.Text = "Makeup session cancelled successfully"
                BindCoachClassDetailsInRepeater()
            Else
                lblErr.Text = "There is a problem in cancelling Makeup session.Please contact the Admin"
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub
    Public Sub cancelSubstituteSession()
        Try


            cmdText = "update WebConfLog set SubstituteCoachID=null where MemberID=" & hdnCoachID.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [Session]=" & hdnSessionNo.Value & ""
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "update CalSignup set SubstituteCoachID=null, SubstituteDate=null where MemberID=" & hdnCoachID.Value & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [SessionNo]=" & hdnSessionNo.Value & ""
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "update CoachClassCal set Status='" & hdnStatus.Value & "', Substitute=null, Reason=null where MemberID=" & hdnCoachID.Value & " and [Date]='" & hdnDate.Value & "' and WeekNo=" & hdnWeekNo.Value & " and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [SessionNo]=" & hdnSessionNo.Value & ""

            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            lblMsg.Text = "Substitute Session cancelled Successfully"

            BindCoachClassDetailsInRepeater()
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub

    Public Sub populateRecClassDate(ByVal level As String, ByVal sessionNo As String)

        Dim cmdText As String = String.Empty
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term' and CS.Semester=CD.Semester) where  CS.MemberID=" + ddlCoach.SelectedValue + " and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.EventYear=" + ddlYear.SelectedValue + " and CS.Accepted='Y' and CS.Level='" + level + "' and CS.SessionNo=" + sessionNo + " and CD.Semester='" + ddlPhase.SelectedValue + "'"

        Dim ds As New DataSet()
        Try
            Dim x As Integer = 0
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dtStartDate As New DateTime()
                    Dim dtEndDate As New DateTime()
                    Dim dtTodayDate As DateTime = DateTime.Now


                    dtStartDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString())
                    Dim strStartDate As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToString("MM/dd/yyyy")
                    Dim EndDate As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("EndDate").ToString()).ToString("MM/dd/yyyy")
                    dtEndDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("EndDate").ToString())
                    Dim day As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToString("dddd")
                    Dim classDay As String = ds.Tables(0).Rows(0)("day").ToString()
                    Dim dtClassDate As DateTime = Convert.ToDateTime(dtStartDate)


                    Dim y As Integer = 0
                    If day = classDay Then

                        While dtClassDate <= dtEndDate
                            Dim [date] As String = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy")

                            x = x + 1

                            dtClassDate = dtClassDate.AddDays(7)
                        End While
                    Else
                        For i As Integer = 1 To 7
                            dtClassDate = dtClassDate.AddDays(1)
                            Dim strDay As String = Convert.ToDateTime(dtClassDate).ToString("dddd")
                            If strDay = classDay Then
                                Exit For
                            End If
                        Next
                        While dtClassDate <= dtEndDate

                            x = x + 1
                            'DDlSubDate.Items.Insert(0, new ListItem(date, date));

                            dtClassDate = dtClassDate.AddDays(7)
                        End While
                    End If

                End If
            End If
            hdnTotalClass.Value = x
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try


    End Sub

    Public Sub populateRecClassDateDrpDown()

        Dim cmdText As String = String.Empty
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term') where  CS.MemberID=" + ddlCoach.SelectedValue + " and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.EventYear=" + ddlYear.SelectedValue + " and CS.Accepted='Y' and CS.Level='" + hdnLevel.Value + "' and CS.SessionNo=" + hdnSessionNo.Value + ""

        Dim ds As New DataSet()
        Try
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim dtStartDate As New DateTime()
                    Dim dtEndDate As New DateTime()
                    Dim dtTodayDate As DateTime = DateTime.Now


                    dtStartDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString())
                    Dim strStartDate As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToString("MM/dd/yyyy")
                    Dim EndDate As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("EndDate").ToString()).ToString("MM/dd/yyyy")
                    dtEndDate = Convert.ToDateTime(ds.Tables(0).Rows(0)("EndDate").ToString())
                    Dim day As String = Convert.ToDateTime(ds.Tables(0).Rows(0)("StartDate").ToString()).ToString("dddd")
                    Dim classDay As String = ds.Tables(0).Rows(0)("day").ToString()
                    Dim dtClassDate As DateTime = Convert.ToDateTime(dtStartDate)

                    ddlRecClassDate.Items.Clear()
                    Dim x As Integer = 0
                    Dim y As Integer = 0
                    If day = classDay Then

                        While dtClassDate <= dtEndDate
                            Dim [date] As String = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy")

                            'string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");



                            If dtTodayDate > dtClassDate AndAlso x = 0 Then
                                ddlRecClassDate.Items.Insert(0, New ListItem([date], [date]))
                                x += 1
                            End If
                            If dtTodayDate < dtClassDate AndAlso y = 0 Then
                                ddlRecClassDate.Items.Insert(0, New ListItem([date], [date]))
                                y += 1
                            End If

                            'DDlSubDate.Items.Insert(0, new ListItem(date, date));

                            dtClassDate = dtClassDate.AddDays(7)
                        End While
                    Else
                        For i As Integer = 1 To 7
                            dtClassDate = dtClassDate.AddDays(1).ToShortDateString()
                            Dim strDay As String = Convert.ToDateTime(dtClassDate).ToString("dddd")
                            If strDay = classDay Then
                                Exit For
                            End If
                        Next
                        While dtClassDate <= dtEndDate
                            Dim [date] As String = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy")
                            'string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");
                            If dtTodayDate > dtClassDate AndAlso x = 0 Then
                                ddlRecClassDate.Items.Insert(0, New ListItem([date], [date]))
                                x += 1
                            End If
                            If dtTodayDate <= dtClassDate AndAlso y = 0 Then
                                ddlRecClassDate.Items.Insert(0, New ListItem([date], [date]))
                                y += 1
                            End If

                            'DDlSubDate.Items.Insert(0, new ListItem(date, date));

                            dtClassDate = dtClassDate.AddDays(7)
                        End While
                    End If
                    ddlRecClassDate.Items.Insert(0, New ListItem("Select", "0"))
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub


    Public Sub deleteMeeting(ByVal meetingKey As String)
        Try
            Dim URL As String = String.Empty
            Dim service As String = "3"
            URL = "https://api.zoom.us/v1/meeting/delete"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg"
            urlParameter += "&id=" + meetingKey + ""

            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub

    Public Sub updateZoomSession()

        Dim sessionKey As String = String.Empty
        Dim duration As Integer = 0
        Dim cmdMeetingkeyText As String = String.Empty
        cmdMeetingkeyText = "select SessionKey,duration from WebConfLog where EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [Session]=" & hdnSessionNo.Value & " and SessionType='Scheduled Meeting'"
        Dim ds As DataSet = New DataSet()
        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdMeetingkeyText)

        If ds IsNot Nothing Then
            If ds.Tables(0).Rows.Count > 0 Then
                sessionKey = ds.Tables(0).Rows(0)("SessionKey").ToString()
                duration = Convert.ToInt32(ds.Tables(0).Rows(0)("duration").ToString())
            End If
        End If
        updateZoomMeeting("2", sessionKey, duration)
        If hdnMeetingStatus.Value = "SUCCESS" Then
            Dim cmdText As String = ""

            cmdText = "Update WebConfLog set StartDate='" & TxtMkeupDate.Text & "', EndDate='" & TxtMkeupDate.Text & "',BeginTime='" & ddlMakeupTime.SelectedValue & "' where SessionKey=" & sessionKey & ""
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdText)

                cmdText = "Update CalSignup set MakeUpDate='" & TxtMkeupDate.Text & "',MakeUpTime='" & ddlMakeupTime.SelectedValue & "' where SignupID=" & hdnSignupID.Value & ""
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdText)

                Dim Day As String = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")

                cmdText = "update CoachClassCal set Date='" & TxtMkeupDate.Text & "', Time='" & ddlMakeupTime.SelectedValue & "', Day='" & Day & "' where MemberID=" & hdnCoachID.Value & " and [Date]='" & hdnDate.Value & "' and WeekNo=" & hdnWeekNo.Value & " and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & hdnProductGroupID.Value & " and ProductID=" & hdnProductID.Value & " and Level='" & hdnLevel.Value & "' and [SessionNo]=" & hdnSessionNo.Value & ""

                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            Catch ex As Exception
                CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
                ' Throw New Exception(ex.Message)
            End Try
            lblMsg.Text = "Makeup Session updated successfully"
            BindCoachClassDetailsInRepeater()
        End If
    End Sub

    Public Sub updateZoomMeeting(ByVal mode As String, ByVal sessionKey As String, ByVal duration As Integer)
        Try

            Dim URL As String = String.Empty
            Dim durationHrs As String = duration
            Dim service As String = "1"
            If mode = "1" Then
                URL = "https://api.zoom.us/v1/meeting/create"
            ElseIf mode = "2" Then
                URL = "https://api.zoom.us/v1/meeting/update"
            End If
            Dim urlParameter As String = String.Empty

            Dim test As String = (DateTime.UtcNow.ToString("s") + "Z").ToString()

            Dim [date] As String = TxtMkeupDate.Text
            Dim time As String = ddlMakeupTime.SelectedValue
            Dim dateTime__1 As String = (Convert.ToString([date] & Convert.ToString("T")) & time) + "Z"
            Dim topic As String = hdnMeetingTitle.Value

            Dim dtFromS As New DateTime()
            Dim dtEnds As DateTime = DateTime.Now
            Dim mins As Double = 40.0
            Dim dtTo As New DateTime()
            If DateTime.TryParse(time, dtFromS) Then
                Dim TS As TimeSpan = dtFromS - dtEnds
                mins = TS.TotalMinutes
                dtTo = dtFromS.AddHours(4)
            End If
            Dim timeFormat As String = dtTo.Hour.ToString() + ":" + dtTo.Minute.ToString() + ":" + dtTo.Second.ToString()

            dateTime__1 = (Convert.ToString([date] & Convert.ToString("T")) & timeFormat) + "Z"
            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg"

            If mode = "2" Then
                service = "2"
                urlParameter += "&id=" & sessionKey & ""
                urlParameter += (Convert.ToString("&start_time=") & dateTime__1) + ""
                urlParameter += "&duration=" + duration.ToString() + ""
                urlParameter += "&timezone=GMT-5:00"
                urlParameter += "&option_jbh=true"
                urlParameter += "&option_host_video=true"
                urlParameter += "&option_audio=both"
            End If




            makeZoomAPICall(urlParameter, URL, service)
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try

    End Sub

    Public Sub schedulePractiseSession()
        If (validatePractiseSession() = "1") Then


            Dim cmdText As String = Nothing
            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],VL.UserID,VL.PWD,VL.hostID,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookup VL on (VL.VRoom=CS.VRoom) where Accepted='Y' and SignUpId=" & hdnSignupID.Value & ""
            Dim duration As Integer
            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        Dim hostID As String = String.Empty
                        hostID = dr("HostID").ToString()
                        Dim WebExID As String = dr("UserID").ToString()
                        Dim WebExPwd As String = dr("PWD").ToString()
                        Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
                        Dim ScheduleType As String = dr("ScheduleType").ToString()

                        Dim year As String = dr("EventYear").ToString()
                        Dim eventID As String = dr("EventID").ToString()

                        Dim ProductGroupID As String = dr("ProductGroupID").ToString()
                        Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
                        Dim ProductID As String = dr("ProductID").ToString()
                        Dim ProductCode As String = dr("ProductCode").ToString()
                        Dim Semester As String = dr("Semester").ToString()
                        Dim Level As String = dr("Level").ToString()
                        Dim Sessionno As String = dr("SessionNo").ToString()
                        Dim CoachID As String = dr("MemberID").ToString()
                        Dim MeetingPwd As String = "training"

                        Dim Time As String = dr("Time").ToString()
                        Dim Day As String = dr("Day").ToString()
                        Dim STime As String = dr("Begin").ToString()
                        Dim ETime As String = dr("End").ToString()

                        Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
                        Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
                        'string endTime = "01:00 AM";
                        'TimeSpan time1 = DateTime.Parse(endTime).Subtract(DateTime.Parse(Time));
                        'TimeSpan time2 = GetTimeFromString1(dr["End"].ToString());
                        'double hours = (time1 - time2).TotalHours;
                        Dim BeginTime As String = dr("Begin").ToString()
                        Dim EndTime As String = dr("End").ToString()

                        Dim timeZoneID As String = "11"
                        Dim TimeZone As String = "EST"
                        ' SignUpId = dr("SignupID").ToString()
                        Dim Mins As Double = 0.0
                        Dim dFrom As DateTime
                        Dim dTo As DateTime


                        Dim sDateFrom As String = BeginTime
                        Dim sDateTo As String = EndTime
                        If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                            Dim TS As TimeSpan = dTo - dFrom

                            Mins = TS.TotalMinutes
                        End If
                        Dim durationHrs As String = Mins.ToString("0")
                        If durationHrs.IndexOf("-") > -1 Then
                            durationHrs = "188"
                        End If

                        duration = Convert.ToInt32(durationHrs)
                        hdnDuration.Value = duration.ToString()

                        If timeZoneID = "11" Then
                            TimeZone = "EST/EDT – Eastern"
                        ElseIf timeZoneID = "7" Then
                            TimeZone = "CST/CDT - Central"
                        ElseIf timeZoneID = "6" Then
                            TimeZone = "MST/MDT - Mountain"
                        End If

                        Dim CoachName As String = String.Empty

                        CoachName = dr("CoachName").ToString()
                        Dim meetingTitle As String = String.Empty
                        meetingTitle = CoachName & "-" & ProductCode & "-" & Level & "-" & Sessionno
                        hdnMeetingTitle.Value = meetingTitle
                        'If dr("MeetingKey").ToString() = "" Or dr("MeetingKey").ToString() = "0" Then
                        startDate = TxtMkeupDate.Text
                        Time = ddlMakeupTime.SelectedValue
                        'durationHrs = txtDuration.Text
                        Day = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")
                        TimeZone = "EST/EDT – Eastern"
                        ' CreateTrainingSession(WebExUserID, WebExPasswd, ScheduleType, Capacity, startDate.Replace("-", "/"), Time, _
                        ' Day, STime, ETime, EndDate.Replace("-", "/"), durationHrs, CoachName, ProductCode)
                        createZoomMeeting("1", duration, hostID)
                        If hdnMeetingStatus.Value = "SUCCESS" Then
                            ' GetHostUrlMeeting(WebExUserID, WebExPasswd)

                            Dim meetingURL As String = hdnHostMeetingURL.Value




                            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD, SessionType,Status)values(" & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "'," & CoachID & ",'" & hdnTrainingSessionKey.Value & "','" & TxtMkeupDate.Text & "','" & TxtMkeupDate.Text & "','" & ddlMakeupTime.SelectedValue & "','" & ddlMakeupTime.SelectedValue & "'," & Semester & ",'" & Level & "'," & Sessionno & ",'training'," & duration & "," & timeZoneID & ",'" & TimeZone & "',getDate()," & Session("LoginID").ToString() & ",'" & meetingURL & "','" & Day & "','" & WebExID & "','" & WebExPwd & "','Practice','Active')"

                            'cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "','" & startDate & "','" & startDate & "','" & Time & "',null," & durationHrs & "," & timeZoneID & ",'" & TimeZone & "','" & SignUpId & "'," & CoachID & ",'" & Semester & "','" & Level & "','" & Sessionno & "','" & meetingURL & "','" & hdnTrainingSessionKey.Value & "','" & MeetingPwd & "','Active','" & Day & "','" & UserID & "','" & WebExID & "','" & WebExPwd & "'"

                            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                            lblMsg.Text = "Practice session scheduled successfully. You can view the practice session in your calendar signup page."
                            lblMsg.ForeColor = Color.Blue
                            BindCoachClassDetailsInRepeater()
                        End If
                    Next
                End If
            End If
        End If
    End Sub


    Public Sub fillPractiseSession()

        Dim cmdtext As String = ""
        Dim ds As New DataSet()

        cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VC.SessionType from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) where VC.MemberID=" & ddlCoach.SelectedValue & " and VC.SessionType='Practice'  order by VC.ProductGroupCode"

        Try


            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then

                    grdPractiseSession.DataSource = ds
                    grdPractiseSession.DataBind()
                    spnNorecPractise.Visible = False

                Else
                    grdPractiseSession.DataSource = ds

                    grdPractiseSession.DataBind()
                    spnNorecPractise.Visible = True

                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub


    Protected Sub grdPractiseSession_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try



            If e.CommandName = "UpdatePractice" Then

                Dim gvPrevRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                gvPrevRow.BackColor = Color.Gray

                Dim RowIndex As Integer = gvPrevRow.RowIndex

                Dim sessionKey As String = DirectCast(grdPractiseSession.Rows(RowIndex).FindControl("lblSessionKey"), Label).Text
                Dim startDate As String = DirectCast(grdPractiseSession.Rows(RowIndex).FindControl("lblStartDate"), Label).Text
                Dim startTime As String = DirectCast(grdPractiseSession.Rows(RowIndex).FindControl("lblStartTime"), Label).Text
                Dim duration As String = DirectCast(grdPractiseSession.Rows(RowIndex).FindControl("lblDuration"), Label).Text

                hdnTrainingSessionKey.Value = sessionKey
                TxtMkeupDate.Text = startDate
                txtDuration.Text = duration
                ddlMakeupTime.SelectedValue = startTime
                BtnSaveMakeupClass.Text = "Update"
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)
            ElseIf e.CommandName = "CancelPractise" Then

                Dim gvPrevRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)


                Dim RowIndex As Integer = gvPrevRow.RowIndex

                Dim sessionKey As String = DirectCast(grdPractiseSession.Rows(RowIndex).FindControl("lblSessionKey"), Label).Text
                hdnTrainingSessionKey.Value = sessionKey

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert4Practise();", True)

            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try

    End Sub

    Public Sub updatePractiseSession()
        updateZoomMeeting("2", hdnTrainingSessionKey.Value, txtDuration.Text)
        If hdnMeetingStatus.Value = "SUCCESS" Then
            Dim cmdText As String = ""

            cmdText = "Update WebConfLog set StartDate='" & TxtMkeupDate.Text & "', EndDate='" & TxtMkeupDate.Text & "',BeginTime='" & ddlMakeupTime.SelectedValue & "', Duration =" & txtDuration.Text & " where SessionKey=" & hdnTrainingSessionKey.Value & ""
            Try


                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
                lblMsg.Text = "Practice Session updated successfully."
                lblMsg.ForeColor = Color.Blue
                fillPractiseSession()
            Catch ex As Exception
                CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
            End Try
        End If

    End Sub

    Protected Sub btnCancelPractice_Click(sender As Object, e As EventArgs)
        cancelPracticeSession()

    End Sub

    Public Sub cancelPracticeSession()
        deleteMeeting(hdnTrainingSessionKey.Value)
        If hdnMeetingStatus.Value = "SUCCESS" Then


            Dim cmdtext As String = String.Empty
            cmdtext = "Delete from WebConflog where SessionKey=" & hdnTrainingSessionKey.Value & ""
            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdtext)
            lblMsg.Text = "Practice session cancelled successfully."
            lblMsg.ForeColor = Color.Blue
            fillPractiseSession()
        End If
        'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", True)
        'lblMakeupErrMsg.Text = "Practice session cancelled successfully."
    End Sub



    Public Function checkAvailableVrooms(ByVal Duration As Integer) As String

        Dim cmdtext As String = String.Empty
        Dim ds As DataSet = New DataSet()
        Dim hostID As String = String.Empty

        Dim strStartTime As String = ddlMakeupTime.SelectedValue
        Dim strStartDay As String = Convert.ToDateTime(TxtMkeupDate.Text).ToString("dddd")
        Duration = Duration + 30
        Dim strEndTime As String = GetTimeFromString(ddlMakeupTime.SelectedValue, Duration).ToString()
        strStartTime = GetTimeFromStringSubtract(ddlMakeupTime.SelectedValue, -30).ToString()

        'cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd from CalSignup CS inner join WebConfLog VC on (CS.Meetingkey=VC.SessionKey or CS.makeupMeetKey=VC.Sessionkey) inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=2016 and ((VC.BeginTime between '" & strStartTime & "' and '" & strEndTime & "') or (VC.EndTime between '" & strStartTime & "' and '" & strEndTime & "')) and VC.Day ='" & strStartDay & "'"

        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd from CalSignup CS inner join WebConfLog VC on (CS.Meetingkey=VC.SessionKey or CS.makeupMeetKey=VC.Sessionkey) inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=2016 and ((VC.BeginTime between '" & strStartTime & "' and '" & strEndTime & "') and VC.Day='" & strStartDay & "') or ((VC.EndTime between '" & strStartTime & "' and '" & strEndTime & "') and VC.Day='" & strStartDay & "') and VC.Day ='" & strStartDay & "'"


        Try
            Dim vRooms As String = String.Empty


            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        vRooms += dr("VRoom").ToString() + ","
                    Next
                End If
            End If


            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=2016 and ((VC.BeginTime between '" + strStartTime + "' and '" + strEndTime + "') and VC.Day='" + strStartDay + "' and SessionType='Practise' and StartDate>= cast(GetDate() as date)) or ((VC.EndTime between '" + strStartTime + "' and '" + strEndTime + "') and VC.Day='" + strStartDay + "' and SessionType='Practise' and StartDate>= cast(GetDate() as date)) and (VC.Day ='" + strStartDay + "' and StartDate >= cast(GetDate() as date)) and SessionType='Practise'"

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        vRooms += dr("VRoom").ToString() + ","
                    Next
                End If
            End If


            If vRooms <> "" Then
                vRooms = vRooms.TrimEnd(",")
            End If

            If vRooms <> "" Then
                cmdtext = "select HostID, USerID, PWD from VirtualRoomLookUp where Vroom not in (" & vRooms & ")"
            Else
                cmdtext = "select HostID, USerID, PWD from VirtualRoomLookUp"
            End If

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    hostID = ds.Tables(0).Rows(0)("HostID").ToString()
                    hdnWebExID.Value = ds.Tables(0).Rows(0)("UserID").ToString()
                    hdnWebExPWD.Value = ds.Tables(0).Rows(0)("PWD").ToString()
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
        Return hostID
    End Function



    Public Sub loadLevel()
        Try


            Dim cmdText As String = ""

            If Session("RoleID").ToString() = "1" OrElse Session("RoleID").ToString() = "2" OrElse Session("RoleID").ToString() = "96" OrElse Session("RoleID").ToString() = "89" Then
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" & ddlPhase.SelectedValue & "'"

                If ddlCoach.SelectedItem.Text <> "All" Then
                    cmdText += " and V.MemberID='" + ddlCoach.SelectedValue + "'"
                End If

                If ddlProductGroup.SelectedValue <> "0" AndAlso ddlProductGroup.SelectedValue <> "" AndAlso ddlProductGroup.SelectedValue <> "All" Then
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + ""
                End If

                If ddlProduct.SelectedValue <> "0" AndAlso ddlProduct.SelectedValue <> "" AndAlso ddlProduct.SelectedItem.Text <> "All" Then
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + ""
                End If
            Else
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" & ddlPhase.SelectedValue & "' and V.MemberID='" + Session("LoginID").ToString() + "'"

                If ddlProductGroup.SelectedValue <> "0" AndAlso ddlProductGroup.SelectedValue <> "" Then
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + ""
                End If

                If ddlProduct.SelectedValue <> "0" AndAlso ddlProduct.SelectedValue <> "" Then
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + ""
                End If
            End If
            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                ddlLevel.DataTextField = "Level"
                ddlLevel.DataValueField = "Level"
                ddlLevel.DataSource = ds
                ddlLevel.DataBind()

                If ds.Tables(0).Rows.Count = 1 Then
                    ddlLevel.Enabled = False
                    LoadSessionNo()
                Else
                    ddlLevel.Enabled = True
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try
    End Sub


    Public Sub LoadSessionNo()
        Try

            Dim cmdText As String = ""

            If Session("RoleID").ToString() = "1" OrElse Session("RoleID").ToString() = "2" OrElse Session("RoleID").ToString() = "96" OrElse Session("RoleID").ToString() = "89" Then
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" & ddlPhase.SelectedValue & "'"

                If ddlCoach.SelectedItem.Text <> "All" Then
                    cmdText += " and V.MemberID='" + ddlCoach.SelectedValue + "'"
                End If

                If ddlProductGroup.SelectedValue <> "0" AndAlso ddlProductGroup.SelectedValue <> "" AndAlso ddlProductGroup.SelectedValue <> "All" Then
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + ""
                End If

                If ddlProduct.SelectedValue <> "0" AndAlso ddlProduct.SelectedValue <> "" AndAlso ddlProduct.SelectedValue <> "All" Then
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + ""
                End If
            Else
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" & ddlPhase.SelectedValue & "' and V.MemberID='" + Session("LoginID").ToString() + "'"

                If ddlProductGroup.SelectedValue <> "0" AndAlso ddlProductGroup.SelectedValue <> "" Then
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + ""
                End If

                If ddlProduct.SelectedValue <> "0" AndAlso ddlProduct.SelectedValue <> "" Then
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + ""
                End If
            End If
            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then

                ddlSessionNo.DataTextField = "SessionNo"
                ddlSessionNo.DataValueField = "SessionNo"
                ddlSessionNo.DataSource = ds
                ddlSessionNo.DataBind()
                ddlSessionNo.Items.Insert(0, New ListItem("Select", "0"))
                If ds.Tables(0).Rows.Count = 1 Then
                    ddlSessionNo.SelectedIndex = 1
                    ddlSessionNo.Enabled = False

                End If
                If ds.Tables(0).Rows.Count > 1 Then
                    ddlSessionNo.Enabled = True

                Else
                    ddlSessionNo.Enabled = False
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog(ddlCoach.SelectedItem.Text, "Exception", ex.Message, "Coach Class Calendar")
        End Try

    End Sub


    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadSessionNo()
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(sender As Object, e As EventArgs)
        FillCoach(ddlCoach)
    End Sub
End Class

