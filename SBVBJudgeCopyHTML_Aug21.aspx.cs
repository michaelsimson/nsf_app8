﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class SBVBJudgeCopyHTML : System.Web.UI.Page
{
    int icTemp;
    int i = 0;
    int itemp = 0;
    int pno = 1;
    string pageTitle = "";
    Boolean pflag = true;
    Boolean pflag1 = true;
    Boolean pflag2 = true;
    Boolean pflag3 = true;
    string Phasedisplay = "";
    string totalPages = "";
    int noofRecordperpage = 0;
    int numberOfPh1records = 0;
    DataRow[] rows1;
    int numberOfPh2records = 0;
    DataRow[] rows2;
    int numberOfPh3records = 0;
    DataRow[] rows3;
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!IsPostBack)
        {
            icTemp = 0;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }
            bindData();
           
        }
        
        
    }


    protected void bindData()
    {
        
        string querystr = Session["selectqry_excelReportSB"].ToString();

        DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, querystr);
        rows1 = dsJVB.Tables[0].Select("Phase = '1'");
        numberOfPh1records = rows1.Length;

        rows2 = dsJVB.Tables[0].Select("Phase = '2'");
        numberOfPh2records = rows2.Length;

        rows3 = dsJVB.Tables[0].Select("Phase = '3'");
        numberOfPh3records = rows3.Length;
        if (Session["ddlProductGroup"].ToString().Equals("SB"))
        {
            
            
            foreach (DataRow dr in dsJVB.Tables[0].Rows)
            {
                string hex = "";
                foreach (char c in dr["Pronunciation"].ToString())
                {
                    int tmp = c;
                    hex += "&#x" + String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString())) + ";";
                }
                dr["Pronunciation"] = hex;
            }
           DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
            int k = 1;
            foreach (DataRow dr in dsJVB.Tables[0].Rows)
            {
                dr["SNo"] = k;
                k++;
            }
            generateHTMLReport(dsJVB.Tables[0]);
               
           
        }
        else
        {
            
            DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
            int k = 1;
            foreach (DataRow dr in dsJVB.Tables[0].Rows)
            {
                dr["SNo"] = k;
                k++;
                if (dr["A"].Equals(DBNull.Value))
                {
                    dr["A"] = "A. ";
                }
                else
                {
                    dr["A"] = "A. " + dr["A"];
                }
                if (dr["B"].Equals(DBNull.Value))
                {
                    dr["B"] = "B. ";
                }
                else
                {
                    dr["B"] = "B. " + dr["B"];
                }

                if (dr["C"].Equals(DBNull.Value))
                {
                    dr["C"] = "C. ";
                }
                else
                {
                    dr["C"] = "C. " + dr["C"];
                }
                if (dr["D"].Equals(DBNull.Value))
                {
                    dr["D"] = "D. ";
                }
                else
                {
                    dr["D"] = "D. " + dr["D"];
                }
                if (dr["E"].Equals(DBNull.Value))
                {
                    dr["E"] = "";
                }
                else
                {
                    dr["E"] = "E. " + dr["E"];
                }
            }
            generateHTMLReport_VB(dsJVB.Tables[0]);
            //rpToPDF.DataSource = dsJVB.Tables[0];
            //rpToPDF.DataBind();
        }
       
    }
    protected void generateHTMLReport(DataTable dt)
    {

        foreach (DataRow dr in dt.Rows)
        {

            icTemp = icTemp + 1;
            if (dr["Phase"].ToString().Equals("1"))
            {
                Phasedisplay = "Phase I ";
                noofRecordperpage = 15;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh1records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else if (dr["Phase"].ToString().Equals("2"))
            {
                Phasedisplay = "Phase II ";
                noofRecordperpage = 20;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh2records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else if (dr["Phase"].ToString().Equals("3"))
            {
                Phasedisplay = "Phase III ";
                noofRecordperpage = 20;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh3records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
           
            string PubUnpDisplay = "";
            if (dr["PubUnp"].ToString().Equals("P"))
            {
                PubUnpDisplay = "Published";
            }
            else
            {
                PubUnpDisplay = "Unpublished";
            }
            string RoundTypeVal = "";
            if (dr["RoundType"].ToString().Equals("Tie Breaker"))
            {
                RoundTypeVal = "- TIE BREAKER WORDS ";
            }
            else
            {
                RoundTypeVal = "";
            }
            if (pno==1)
            {
             
                pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + " Written (" + PubUnpDisplay + ") " + RoundTypeVal + " - Pg " + pno + " of " + (Convert.ToInt32(totalPages)) + " ";
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("</table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=5%><b>S.No</b></th><th width=7.5%><b>Round</b></th><th width=6%><b>Level</b></th><th width=13%><b>Word</b></th><th width=5%><b>POS</b></th><th width=14%><b>Pronunciation</b></th><th width=6.5%><b>Root</b></th><th width=24%><b>Definitions</b></th><th width=25%><b>Sentence</b></th></tr></table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table border=1 cellpadding=2 cellspacing=0 width=100%>"));
                pno++;
            }

            if (dr["Phase"].ToString().Equals("2") && pflag == true)
            {
                itemp = 0;
                pno = 1;
                pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + ": Oral - 3 rounds(" + PubUnpDisplay + ")    Pg " + pno + " of " + totalPages + " ";
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("</table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=5%><b>S.No</b></th><th width=7.5%><b>Round</b></th><th width=6%><b>Level</b></th><th width=13%><b>Word</b></th><th width=5%><b>POS</b></th><th width=14%><b>Pronunciation</b></th><th width=6.5%><b>Root</b></th><th width=24%><b>Definitions</b></th><th width=25%><b>Sentence</b></th></tr></table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));
                pno++;

                pflag = false;
            }
            else if (dr["Phase"].ToString().Equals("3") && pflag1 == true)
            {
                itemp = 0;
                pno = 1;

                pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + ": Elimination (" + PubUnpDisplay + ")    Pg " + pno + " of " + totalPages + " ";

                pno++;
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("</table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=5%><b>S.No</b></th><th width=7.5%><b>Round</b></th><th width=6%><b>Level</b></th><th width=13%><b>Word</b></th><th width=5%><b>POS</b></th><th width=14%><b>Pronunciation</b></th><th width=6.5%><b>Root</b></th><th width=24%><b>Definitions</b></th><th width=25%><b>Sentence</b></th></tr></table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));
                pflag1 = false;
            }

            if (itemp == noofRecordperpage)
            {
                itemp = 0;

                if (dr["Phase"].ToString().Equals("2"))
                {
                    pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + ": Oral - 3 rounds(" + PubUnpDisplay + ")    Pg " + pno + " of " + totalPages + " ";
                }
                else if (dr["Phase"].ToString().Equals("3"))
                {
                    pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + ": Elimination (" + PubUnpDisplay + ")    Pg " + pno + " of " + totalPages + " ";
                }
                else if (dr["Phase"].ToString().Equals("1"))
                {
                    pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + " Written (" + PubUnpDisplay + ") " + RoundTypeVal + " - Pg " + pno + " of " + (Convert.ToInt32(totalPages)) + " ";
                }
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("</table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=5%><b>S.No</b></th><th width=7.5%><b>Round</b></th><th width=6%><b>Level</b></th><th width=13%><b>Word</b></th><th width=5%><b>POS</b></th><th width=14%><b>Pronunciation</b></th><th width=6.5%><b>Root</b></th><th width=24%><b>Definitions</b></th><th width=25%><b>Sentence</b></th></tr></table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));


                pno++;

            }
            else if ((dr["PubUnp"].ToString().Equals("U") && pflag3 == true))
            {
                pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + " Written (" + PubUnpDisplay + ") " + RoundTypeVal + " ";
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("</table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=5%><b>S.No</b></th><th width=7.5%><b>Round</b></th><th width=6%><b>Level</b></th><th width=13%><b>Word</b></th><th width=5%><b>POS</b></th><th width=14%><b>Pronunciation</b></th><th width=6.5%><b>Root</b></th><th width=24%><b>Definitions</b></th><th width=25%><b>Sentence</b></th></tr></table>"));
                rpSB_JudgeCopy.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));

                pflag3 = false;
            }
            rpSB_JudgeCopy.Controls.Add(new LiteralControl("<tr style=font-size:10px;><td align=center width=5%>" + dr["SNo"] + "</td><td align=center width=7.5%>" + dr["Round"] + "</td><td align=center width=6%>" + dr["Level/Sublevel"] + "</td><td align=center width=13% bgcolor=#F2F2F2><b>" + dr["Word"] + "</b></td><td align=center width=5%>" + dr["POS"] + "</td><td align=center width=14% style=font-size:7px;>" + dr["Pronunciation"] + "</td><td align=center width=6.5% style=font-size:7px;>" + dr["Root"] + "</td><td align=center width=24% style=font-size:7px;>" + dr["Definitions"] + "</td><td align=center width=25% style=font-size:7px;>" + dr["Sentence"] + "</td></tr>"));
                        
                                   
                    
                             
            itemp++;
        }

    }

    protected void generateHTMLReport_VB(DataTable dt)
    {

        foreach (DataRow dr in dt.Rows)
        {
            icTemp = icTemp + 1;
            if (dr["Phase"].ToString().Equals("1"))
            {
                Phasedisplay = "Phase I ";
                noofRecordperpage = 10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh1records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else if (dr["Phase"].ToString().Equals("2"))
            {
                Phasedisplay = "Phase II ";
                noofRecordperpage = 10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh2records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }
            else if (dr["Phase"].ToString().Equals("3"))
            {
                Phasedisplay = "Phase III ";
                noofRecordperpage = 10;
                totalPages = (Math.Ceiling(Convert.ToDouble(numberOfPh3records) / Convert.ToDouble(noofRecordperpage))).ToString();
            }

            string PubUnpDisplay = "";
            if (dr["PubUnp"].ToString().Equals("P"))
            {
                PubUnpDisplay = "Published";
            }
            else
            {
                PubUnpDisplay = "Unpublished";
            }
            string RoundTypeVal = "";
            if (dr["RoundType"].ToString().Equals("Tie Breaker"))
            {
                RoundTypeVal = "- TIE BREAKER WORDS ";
            }
            else
            {
                RoundTypeVal = "";
            }
            if (pno == 1)
            {

                pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + " Written - Judge Copy (" + PubUnpDisplay + ") " + RoundTypeVal + "- Pg " + pno + " of " + totalPages + " ";
                rpToPDF.Controls.Add(new LiteralControl("</table>"));
                rpToPDF.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpToPDF.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=7%><b>S.No</b></th><th width=7%><b>Round</b></th><th width=7%><b>Level</b></th><th width=28%><b>Word</b></th><th width=5%><b>Answer</b></th><th width=53%><b>Meaning</b></th></tr></table>"));
                rpToPDF.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));
                pno++;
            }

            if (dr["Phase"].ToString().Equals("2") && pflag == true)
            {
                itemp = 0;
                pno = 1;
                pageTitle = "" + Phasedisplay + " Oral-3 rounds: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (" + PubUnpDisplay + ") - Pg " + pno + " of " + totalPages + " ";
                rpToPDF.Controls.Add(new LiteralControl("</table>"));
                rpToPDF.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpToPDF.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=7%><b>S.No</b></th><th width=7%><b>Round</b></th><th width=7%><b>Level</b></th><th width=28%><b>Word</b></th><th width=5%><b>Answer</b></th><th width=53%><b>Meaning</b></th></tr></table>"));
                rpToPDF.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));
                pno++;

                pflag = false;
            }
            else if (dr["Phase"].ToString().Equals("3") && pflag1 == true)
            {
                 pageTitle = "Phase II Oral-3 rounds: " + Session["PageTitleProductName"].ToString() + " Words - Student Copy  id#        StudentName:  ";
                rpToPDF.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpToPDF.Controls.Add(new LiteralControl("<table border=1 cellpadding=2 cellspacing=0 width=100%><tr><td><table border=1 cellpadding=1.5 cellspacing=1 width=100%><tr style=font-size:8px; ><th width=11%><b>S.No</b></th><th width=14%><b>Level</b></th><th width=75%><b>Word</b></th></tr></table></td><td><table border=1 cellpadding=1.5 cellspacing=1 width=100%><tr style=font-size:8px; ><th width=11%><b>S.No</b></th><th width=14%><b>Level</b></th><th width=75%><b>Word</b></th></tr></table></td><td><table border=1 cellpadding=1.5 cellspacing=1 width=100%><tr style=font-size:8px; ><th width=11%><b>S.No</b></th><th width=14%><b>Level</b></th><th width=75%><b>Word</b></th></tr></table></td></tr></table>"));
                foreach (DataRow dr1 in dt.Select("Phase = '3'"))
                {
                    //rpToPDF.Controls.Add(new LiteralControl(" <table border=1 cellpadding=2 cellspacing=0 width= 100%><tr style=font-size:8px;><td align=center width=11%><b></b></td><td align=center width=14%>" + dr1["Level/Sublevel"] + "</td><td width=75%><b>" + dr1["Word"] + "</b><br/>" + dr1["A"] + "<br/>" + dr1["B"] + "<br/>" + dr1["C"] + "<br/>" + dr1["D"] + "<br/>" + dr1["E"] + "</td></tr></table>"));
                    //rpToPDF.Controls.Add(new LiteralControl("<table border=1 cellpadding=2 cellspacing=0 width=100%><tr><td><table border=1 cellpadding=1.5 cellspacing=1 width=100%><tr style=font-size:8px; ><th width=11%><b>S.No</b></th><th width=14%><b>Level</b></th><th width=75%><b>Word</b></th></tr></table></td><td><table border=1 cellpadding=1.5 cellspacing=1 width=100%><tr style=font-size:8px; ><th width=11%><b>S.No</b></th><th width=14%><b>Level</b></th><th width=75%><b>Word</b></th></tr></table></td><td><table border=1 cellpadding=1.5 cellspacing=1 width=100%><tr style=font-size:8px; ><th width=11%><b>S.No</b></th><th width=14%><b>Level</b></th><th width=75%><b>Word</b></th></tr></table></td></tr></table>"));
                }
                itemp = 0;
                pno = 1;

                pageTitle = "" + Phasedisplay + "Oral - Elimination round :" + Session["PageTitleProductName"].ToString() + " Words - Judge Copy(" + PubUnpDisplay + ")       Pg " + pno + " of " + totalPages + " ";
                pno++;
                rpToPDF.Controls.Add(new LiteralControl("</table>"));
                rpToPDF.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpToPDF.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=7%><b>S.No</b></th><th width=7%><b>Round</b></th><th width=7%><b>Level</b></th><th width=28%><b>Word</b></th><th width=5%><b>Answer</b></th><th width=53%><b>Meaning</b></th></tr></table>"));
                rpToPDF.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));
                pflag1 = false;
               
                
            }

            if (itemp == noofRecordperpage)
            {
                itemp = 0;

                if (dr["Phase"].ToString().Equals("2"))
                {
                    pageTitle = "" + Phasedisplay + " Oral-3 rounds: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (" + PubUnpDisplay + ") - Pg " + pno + " of " + totalPages + " ";
                }
                else if (dr["Phase"].ToString().Equals("3"))
                {
                    pageTitle = "" + Phasedisplay + "Oral - Elimination round :" + Session["PageTitleProductName"].ToString() + " Words - Judge Copy(" + PubUnpDisplay + ")       Pg " + pno + " of " + totalPages + " ";
                }
                else if (dr["Phase"].ToString().Equals("1"))
                {
                    pageTitle = "" + Session["PageTitleProductName"].ToString() + " Words " + Phasedisplay + " Written - Judge Copy (" + PubUnpDisplay + ") " + RoundTypeVal + "- Pg " + pno + " of " + totalPages + " ";
                }
                rpToPDF.Controls.Add(new LiteralControl("</table>"));
                rpToPDF.Controls.Add(new LiteralControl("<b><h3><label>" + pageTitle + "</label></h3></b>"));
                rpToPDF.Controls.Add(new LiteralControl("<table border=1 cellpadding=1 cellspacing=0 width=100% style=height:100%><tr style=font-size:10px; ><th width=7%><b>S.No</b></th><th width=7%><b>Round</b></th><th width=7%><b>Level</b></th><th width=28%><b>Word</b></th><th width=5%><b>Answer</b></th><th width=53%><b>Meaning</b></th></tr></table>"));
                rpToPDF.Controls.Add(new LiteralControl("<table style='page-break-after: always' border=1 cellpadding=2 cellspacing=0 width=100%>"));


                pno++;

            }
            itemp++;
            rpToPDF.Controls.Add(new LiteralControl("<tr style=font-size:10px;><td align=center width=7%>" + dr["SNo"] + "</td><td align=center width=7%>" + dr["Round"] + "</td><td align=center width=7%>" + dr["Level/Sublevel"] + "</td><td align=left width=28% bgcolor=#F2F2F2><b>" + dr["Word"] + "</b></td><td align=center width=5%>" + dr["Answer"] + "</td><td align=center width=53% style=font-size:7px;>" + dr["Meaning"] + "</td></tr>"));
            rpToPDF.Controls.Add(new LiteralControl("<tr style=font-size:7px;><td width=7%></td><td width=7%></td><td width=7%></td><td width=28%>" + dr["A"] + "</br>" + dr["B"] + "</br>" + dr["C"] + "</br>" + dr["D"] + "</br>" + dr["E"] + "</br></td> <td width=5%></td><td width=53%></td></tr>"));
        }

    }
    protected void rpToPDF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        icTemp = icTemp + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoTemp");
        lblSNo.Text = Convert.ToString(icTemp);

    }
    
}