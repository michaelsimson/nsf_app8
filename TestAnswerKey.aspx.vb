﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports VRegistration
Partial Class TestAnswerKey
    Inherits System.Web.UI.Page
    Dim StrAnswerKeyRecID As Integer
    Dim TestNumber As Integer
    Dim SectionNumber As Integer
    Dim Level As String
    Dim QuestionType As String
    Dim QuestionNumberFrom As Integer
    Dim QuestionNumberTo As Integer


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("RoleId") = 1
        'Session("LoginId") = 4240


        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            LoadYear()
            loadPhase()
            LoadProductGroup()
            LoadDropdown(ddlSetNo, 40)

            LoadDropdown(ddlSections, 10)



            ' LoadGrid_CoachPapers()
            ProductStuff()

            Dim pgId As String = String.Empty
            Dim pId As String = String.Empty
            Dim level As String = String.Empty
            Dim weekId As String = String.Empty
            Dim paperType As String = String.Empty
            Dim SecNo As String = String.Empty
            Dim setNum As String = String.Empty
            Dim semester As String = String.Empty
            Try
                If Request.QueryString("pgId") Is Nothing Then
                Else
                    pgId = Request.QueryString("pgId").ToString()
                End If
                If Request.QueryString("pId") Is Nothing Then
                Else
                    pId = Request.QueryString("pId").ToString()
                End If
                If Request.QueryString("level") Is Nothing Then
                Else
                    level = Request.QueryString("level").ToString()
                End If

                If Request.QueryString("pType") Is Nothing Then
                Else
                    paperType = Request.QueryString("pType").ToString()
                End If

                If Request.QueryString("wkId") Is Nothing Then
                Else
                    weekId = Request.QueryString("wkId").ToString()
                End If

                If Request.QueryString("setNum") Is Nothing Then
                Else
                    setNum = Request.QueryString("setNum")
                End If
                If Request.QueryString("sec") Is Nothing Then
                Else
                    SecNo = Request.QueryString("sec").ToString()
                End If
                If Request.QueryString("Sem") Is Nothing Then
                Else
                    semester = Request.QueryString("Sem").ToString()
                End If

            Catch ex As Exception

            End Try

            If (semester <> "") Then
                ddlSemester.SelectedValue = semester


            End If
            If (pgId <> "") Then
                LoadProductGroup()
                ddlProductGroup.SelectedValue = pgId

                LoadProductID()
                If pId <> "" Then
                    ddlProduct.SelectedValue = pId

                End If

                LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
                If (level <> "") Then
                    ddlLevel.SelectedValue = level
                    PopulateWeekNo()
                End If
                If (paperType <> "") Then
                    ddlPaperType.SelectedValue = paperType

                End If
                If (ddlWeekID.SelectedValue <> "") Then
                    ddlWeekID.SelectedValue = weekId
                    ddlSetNo.SelectedValue = weekId
                End If
                If (SecNo <> "") Then
                    ddlSections.SelectedValue = SecNo

                End If
                'If (setNum <> "") Then
                '    ddlSetNo.SelectedValue = setNum
                'End If
                If (pgId <> "" And pId <> "" And level <> "" And paperType <> "" And weekId <> "0" And SecNo <> "") Then
                    LoadTestSections()
                End If

                ' LoadGrid_CoachPapers()
            End If
        End If
    End Sub
    Public Sub ProductStuff()
        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "30") Then
            LoadProductGroup()

            Dim sqltext As String = "    select distinct P.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " & ddlEventYear.SelectedValue & " and cs.Accepted = 'Y' and cs.Semester='" & ddlSemester.SelectedValue & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, sqltext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlProduct.DataSource = ds
                    ddlProduct.DataBind()
                    If ddlProduct.Items.Count > 1 Then
                        ddlProduct.Items.Insert(0, New ListItem("Select Product"))
                        ddlProduct.Items(0).Selected = True
                        ddlProduct.Enabled = True

                    End If
                End If
            End If

        ElseIf Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                'more than one 
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                LoadProductGroup()
            Else
                'only one
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                If ds.Tables(0).Rows.Count > 0 Then
                    prd = ds.Tables(0).Rows(0)(1).ToString()
                    Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                    lblPrd.Text = prd
                    lblPrdGrp.Text = Prdgrp
                Else
                    ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from CalSignup where Memberid=" & Session("LoginID") & " and EventYear=" & DateTime.Now.Year & " and Accepted='Y' and ProductId is not Null ")
                    If ds.Tables(0).Rows.Count > 0 Then
                        prd = ds.Tables(0).Rows(0)(1).ToString()
                        Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                    End If
                End If
                LoadProductGroup()
            End If
        End If
    End Sub
    Private Sub loadPhase()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2

            ddlSemester.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue)
        If (Session("RoleId").ToString() = "88") Then
            Dim cmdtext As String = " select max(Semester) from CalSignup where MemberId=" & Session("LoginId").ToString() & " and EventYear=" & ddlEventYear.SelectedValue & ""
            Try
                Dim Semester As String = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, cmdtext)
                ddlSemester.SelectedValue = Semester
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)

        ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        For i As Integer = 0 To 8
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))


        Next
        ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
    End Sub

    Private Sub LoadProductGroup()
        ddlProduct.Items.Clear()
        Dim strSql As String = String.Empty
        If (Session("RoleID").ToString() = "88") Then
            strSql = "SELECT  Distinct PG.ProductGroupID, PG.Name from ProductGroup PG inner join CalSignup Cs on (PG.ProductGroupID=CS.ProductGroupID and CS.Semester='" + ddlSemester.SelectedValue + "' and CS.EventYear=" + ddlEventYear.SelectedValue + " and CS.Accepted='Y') WHERE PG.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND PG.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & " and CS.MemberID=" & Session("LoginID").ToString() & " order by ProductGroupID"
        Else
            strSql = "SELECT  Distinct PG.ProductGroupID, PG.Name from ProductGroup PG inner join CalSignup Cs on (PG.ProductGroupID=CS.ProductGroupID and CS.Semester='" + ddlSemester.SelectedValue + "' and CS.EventYear=" + ddlEventYear.SelectedValue + " and CS.Accepted='Y') WHERE PG.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND PG.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & " order by ProductGroupID"
        End If

        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlProductGroup.DataSource = drproductgroup
        ddlProductGroup.DataBind()
        If ddlProductGroup.Items.Count < 1 Then
            lblError.Text = "No Product is opened."
        ElseIf ddlProductGroup.Items.Count > 1 Then
            ddlProductGroup.Items.Insert(0, New ListItem("Select", "0"))
            ddlProductGroup.Items(0).Selected = True
            ddlProductGroup.Enabled = True
        Else
            ddlProductGroup.Enabled = False
            LoadProductID()
            LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        End If
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ddlProduct.Enabled = False
        Else
            Dim strSql As String
            Try

                If (Session("RoleID").ToString() = "88") Then
                    strSql = "Select distinct P.ProductID, P.Name from Product P inner join CalSignup Cs on (P.ProductID=CS.ProductID and CS.Semester='" + ddlSemester.SelectedValue + "' and CS.EventYear=" + ddlEventYear.SelectedValue + " and CS.Accepted='Y') where P.EventID=" & ddlEvent.SelectedValue & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & " and CS.MemberID=" & Session("LoginID").ToString() & " order by P.ProductID"
                Else
                    strSql = "Select distinct P.ProductID, P.Name from Product P inner join CalSignup Cs on (P.ProductID=CS.ProductID and CS.Semester='" + ddlSemester.SelectedValue + "' and CS.EventYear=" + ddlEventYear.SelectedValue + " and CS.Accepted='Y') where P.EventID=" & ddlEvent.SelectedValue & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & " order by P.ProductID"
                End If

                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, New ListItem("Select Product"))
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    '    LoadGrid_CoachPapers()
                    'LoadGrid_TestSections(True)
                End If
            Catch ex As Exception
                lblError.Text = ex.ToString
            End Try
        End If
    End Sub
    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'LoadGrid_TestSections()
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ddlProduct.Items.Clear()
        LoadProductID()
        LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
        '   LoadGrid_CoachPapers()
    End Sub

    Protected Sub LoadDropdown(ByVal ddlObject As DropDownList, ByVal MaxLimit As Integer)
        ddlObject.Items.Clear()
        For i As Integer = 0 To MaxLimit - 1
            ddlObject.Items.Insert(i, Convert.ToString(i + 1))
        Next
        ddlObject.Items.Insert(0, "Select")
    End Sub
    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String)
        ddlObject.Items.Clear()
        ddlObject.Enabled = True

        Try


            ddlObject.Items.Clear()
            ddlObject.Enabled = True

            Dim cmdText As String = String.Empty

            If (Session("RoleID") Is Nothing) Then
            Else
                If (Session("RoleID").ToString() = "88") Then
                    cmdText = "select Level as LevelCode from CalSignup where eventYear=" & ddlEventYear.SelectedValue & " and Accepted='Y' and memberID=" & Session("LoginID").ToString() & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester='" + ddlSemester.SelectedValue + "'"

                Else
                    cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" & ddlEventYear.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & ""

                End If
            End If



            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim level As SqlDataReader
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText)
            ddlObject.DataSource = level
            ddlObject.DataValueField = "LevelCode"
            ddlObject.DataTextField = "LevelCode"
            ddlObject.DataBind()
            Dim dt As DataTable = New DataTable()
            dt.Load(level)
            If (ddlObject.Items.Count > 1) Then
                ddlObject.Items.Insert(0, "Select Level")
                ddlObject.Enabled = True

            Else
                PopulateWeekNo()
                ddlObject.Enabled = False
            End If




        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Clear()

        tblDGAnswerKey.Visible = False
        tblDGCoachPaper.Visible = False
        LoadTestSections()
        '  LoadGrid_CoachPapers()
    End Sub


    Public Sub LoadGrid_CoachPapers()
        lblError.Text = ""
        tblDGCoachPaper.Visible = True
        Dim StrWhereCndn As String = ""
        StrWhereCndn = StrWhereCndn & "EventYear=" & ddlEventYear.SelectedValue

        StrWhereCndn = StrWhereCndn & " and Semester='" & ddlSemester.SelectedValue & "'"

        If ddlPaperType.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and PaperType= '" & ddlPaperType.SelectedValue & "'"
        End If
        If ddlProductGroup.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and ProductGroupID= " & ddlProductGroup.SelectedValue & ""
        End If
        If ddlProduct.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and ProductID=" & ddlProduct.SelectedValue & ""
        End If
        If ddlLevel.Items.Count > 0 And ddlLevel.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and Level='" & ddlLevel.SelectedItem.Text & "'"
        End If
        If ddlSetNo.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " and SetNum=" & ddlSetNo.SelectedValue & ""
        End If
        If ddlWeekID.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & " And WeekId = " & ddlWeekID.SelectedItem.Text & ""
        End If
        If ddlSections.SelectedIndex > 0 Then
            StrWhereCndn = StrWhereCndn & "  And Sections = " & ddlSections.SelectedValue & ""
        End If

        If lblPrdGrp.Text <> "" Then
            StrWhereCndn = StrWhereCndn & " and ProductGroupID in (" & lblPrdGrp.Text & ")"
        End If
        If lblPrd.Text <> "" Then
            StrWhereCndn = StrWhereCndn & " and ProductID in (" & lblPrd.Text & ")"
        End If
        StrWhereCndn = StrWhereCndn & " and DocType in ('Q')"


        StrWhereCndn = StrWhereCndn & " and DocType in ('Q')"

        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select '' as TestSetUp,* From CoachPapers Where CoachPaperID in (Select Top 50 CoachPaperID from CoachPapers Where " & StrWhereCndn & " order by CreateDate desc) order by CoachPaperID asc")
        Dim dsRole As DataSet = New DataSet()
        Dim TeamLead As String = String.Empty
        Dim cmdText As String = "select TeamLead from Volunteer where RoleID='" & Session("RoleID") & "' and MemberID='" & Session("LoginID") & "'"
        If ddlProductGroup.SelectedIndex > 0 Then
            cmdText = cmdText & " and ProductGroupID= " & ddlProductGroup.SelectedValue & ""
        End If
        If ddlProduct.SelectedIndex > 0 Then
            cmdText = cmdText & " and ProductID=" & ddlProduct.SelectedValue & ""
        End If

        dsRole = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)
        If dsRole.Tables(0).Rows.Count > 0 Then
            TeamLead = dsRole.Tables(0).Rows(0)("TeamLead").ToString()
        End If

        Dim CoachPaperText As String = String.Empty
        If (Session("RoleID") = 88 And TeamLead = "") Then
            CoachPaperText = "Select Case when C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & ") then 'Yes' else 'No' End as TestSetup,C.* From CoachPapers C inner join CoachRelDates CR on (c.CoachPaperId=CR.CoachPaperId) Where C.CoachPaperID in (Select Top 50 CoachPaperID from CoachPapers Where " & StrWhereCndn & " order by CreateDate desc) and CR.MemberID=" & Session("loginID").ToString() & " order by C.CoachPaperID asc"
        Else
            Dim cmdCount As String = "Select count(CoachPaperID) from CoachPapers Where " & StrWhereCndn & ""
            Dim coachPaperIdCount As Integer = 0
            coachPaperIdCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdCount)
            CoachPaperText = "Select Case when C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & ") then 'Yes' else 'No' End as TestSetup,C.* From CoachPapers C Where C.CoachPaperID in (Select Top " & coachPaperIdCount & " CoachPaperID from CoachPapers Where " & StrWhereCndn & " order by CreateDate desc) order by C.CoachPaperID asc"
        End If



        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, CoachPaperText)
        If ds.Tables(0).Rows.Count > 0 Then
            DGCoachPapers.DataSource = ds
            DGCoachPapers.DataBind()
            lblCoachPaper.Text = "Coach Papers"
            If (ds.Tables(0).Rows.Count = 1) Then
                Try
                    Dim coachPaperID = ds.Tables(0).Rows(0)("CoachpaperID").ToString()
                    Dim Level As String = ds.Tables(0).Rows(0)("level").ToString()
                    Session("Level") = Level
                    LoadGrid_TestSections(coachPaperID)



                Catch ex As Exception

                End Try
            End If
        Else
            lblCoachPaper.Text = "No Matching Records exist in Coach Papers."
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
        End If

    End Sub
    Private Sub LoadGrid_TestSections(ByVal TestSetUpSectionsId As Integer)
        lblError.Text = ""
        tblDGTestSection.Visible = True
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select * from TestSetUpSections where CoachPaperID=" & TestSetUpSectionsId & "  and SectionNumber=" & ddlSections.SelectedValue & "  order by SectionNumber") 'txtTestNumber.Text & "") ' and SectionNumber=" & ddlSectionNo.SelectedValue & "")

        If ds.Tables(0).Rows.Count > 0 Then
            DGTestSection.DataSource = ds
            DGTestSection.DataBind()
            lblTestSection.Text = "Test SetUp Sections"
            If (ds.Tables(0).Rows.Count = 1) Then

                Try

                    Session("CoachPaperID") = ds.Tables(0).Rows(0)("CoachPaperID").ToString()
                    Session("SectionNumber") = ds.Tables(0).Rows(0)("SectionNumber").ToString()
                    Session("QuestionType") = ds.Tables(0).Rows(0)("QuestionType").ToString()


                    If ds.Tables(0).Rows(0)("QuestionType").ToString() = "RadioButton" Then
                        Session("NoOfChoices") = IIf(CStr(ds.Tables(0).Rows(0)("QuestionNumberTo").ToString()) <> "", ds.Tables(0).Rows(0)("QuestionNumberTo").ToString(), 0)
                    End If
                    Session("QuestionNumberFrom") = ds.Tables(0).Rows(0)("QuestionNumberFrom").ToString()
                    Session("QuestionNumberTo") = ds.Tables(0).Rows(0)("QuestionNumberTo").ToString()

                    For i As Integer = 0 To DGTestSection.Items.Count - 1
                        DGTestSection.Items(i).BackColor = Color.White 'e.Item.BackColor = Color.White
                    Next
                    tblDGAnswerKey.Visible = False

                    LoadAnswerKey(Session("CoachPaperID"), Session("SectionNumber"), Session("Level"))
                Catch ex As Exception

                End Try

            End If

        Else
            lblTestSection.Text = "No records exists in the Test Setup Sections table."
            DGTestSection.DataSource = Nothing
            DGTestSection.DataBind()
        End If

    End Sub
    Protected Sub DGCoachPapers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim CoachPaperID As Integer = CInt(e.Item.Cells(3).Text)
        Dim sections As Integer = CInt(e.Item.Cells(10).Text)
        Dim Level As String = e.Item.Cells(6).Text
        Session("Level") = Level
        If e.CommandName = "Select" Then
            tblDGAnswerKey.Visible = False
            LoadGrid_TestSections(CoachPaperID)
            DGTestSection.Visible = True
            LoadProductGroup()
            Dim PGId As String = CType(e.Item.FindControl("lblProductGroupID"), Label).Text
            ddlProductGroup.SelectedValue = PGId
            LoadProductID()
            Dim PId As String = CType(e.Item.FindControl("lblProductID"), Label).Text
            ddlProduct.SelectedValue = PId
            LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)

            ddlLevel.SelectedValue = Level
            Dim PaperType As String = CStr(e.Item.Cells(11).Text)
            ddlPaperType.SelectedValue = PaperType
            Dim WeekNo As String = CStr(e.Item.Cells(8).Text)
            ddlWeekID.SelectedValue = WeekNo

            Dim Setnumber As String = CStr(e.Item.Cells(9).Text)
            ddlSetNo.SelectedValue = Setnumber

        End If
    End Sub
    Protected Sub LoadTestSections()
        '    clear()
        If (ValidateAnswerKey() = "1") Then
            Dim CoachPaperID As String = ddlWeekID.SelectedValue
            LoadGrid_TestSections(CoachPaperID)
        End If
    End Sub
    Public Function ValidateAnswerKey() As String
        Dim retVal As String = "1"
        If ddlEventYear.SelectedValue = "-1" Then
            lblError.Text = "Please select Event Year"
            retVal = "-1"

        ElseIf ddlSemester.SelectedValue = "0" Then
            lblError.Text = "Please select Semester"
            retVal = "-1"
        ElseIf ddlProductGroup.SelectedValue = "0" OrElse ddlProductGroup.SelectedValue = "" Then
            lblError.Text = "Please select Product Group"
            retVal = "-1"
        ElseIf ddlProduct.SelectedValue = "Select Product" OrElse ddlProduct.SelectedValue = "" Then
            lblError.Text = "Please select Product"
            retVal = "-1"

        ElseIf ddlLevel.SelectedValue = "Select Level" OrElse ddlLevel.SelectedValue = "0" Then
            lblError.Text = "Please select Level"
            retVal = "-1"

        ElseIf ddlPaperType.SelectedValue = "" OrElse ddlPaperType.SelectedValue = "0" Then
            lblError.Text = "Please select Papertype"
            retVal = "-1"
        ElseIf ddlWeekID.SelectedValue = "-1" OrElse ddlWeekID.SelectedValue = "0" Then
            lblError.Text = "Please select WeekId"
            retVal = "-1"
        ElseIf ddlSections.SelectedValue = "Select" OrElse ddlSections.SelectedValue = "0" Then
            lblError.Text = "Please select Sections"
            retVal = "-1"
        End If

        Return retVal
    End Function

    Protected Sub DGTestSection_Itemcommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try

            lblError.Text = ""
            'Dim CoachPaperId As Integer
            Session("CoachPaperID") = CInt(e.Item.Cells(3).Text)
            Session("SectionNumber") = CInt(e.Item.Cells(4).Text)
            Session("Level") = CStr(e.Item.Cells(5).Text)
            Session("QuestionType") = CStr(e.Item.Cells(9).Text)
            If CStr(e.Item.Cells(9).Text) = "RadioButton" Then
                Session("NoOfChoices") = IIf(CStr(e.Item.Cells(10).Text) <> "", e.Item.Cells(10).Text, 0)
            End If
            Session("QuestionNumberFrom") = CInt(e.Item.Cells(11).Text)
            Session("QuestionNumberTo") = CInt(e.Item.Cells(12).Text)

            For i As Integer = 0 To DGTestSection.Items.Count - 1
                DGTestSection.Items(i).BackColor = Color.White 'e.Item.BackColor = Color.White
            Next
            If e.CommandName = "Select" Then
                tblDGAnswerKey.Visible = False
                e.Item.BackColor = Color.Gainsboro
                LoadAnswerKey(Session("CoachPaperID"), Session("SectionNumber"), Session("Level"))
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub LoadAnswerKey(ByVal CoachPaperID As Integer, ByVal SectionNumber As Integer, ByVal Level As String)
        Try
            Dim StrSQLInsert As String = ""
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from TestAnswerKey where EventYear=" & ddlEventYear.SelectedValue & "and  CoachPaperID =" & CoachPaperID & " and SectionNumber=" & SectionNumber & IIf(Level <> "", " and Level='" & Level & "'", "") & " and QuestionNumber between " & Session("QuestionNumberFrom") & " and " & Session("QuestionNumberTo")) > 0 Then
                LoadGrid_AnswerKey(CoachPaperID, SectionNumber, Level)
            Else
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * from TestSetUpSections where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperID & " and SectionNumber=" & SectionNumber & IIf(Level <> "", " and Level='" & Level & "'", "") & " and QuestionNumberFrom=" & Session("QuestionNumberFrom") & " and QuestionNumberTo=" & Session("QuestionNumberTo"))
                If ds.Tables(0).Rows.Count > 0 Then
                    StrSQLInsert = StrSQLInsert & "Insert into TestAnswerKey (EventYear,CoachPaperID,SectionNumber,Level,QuestionNumber,CreatedBy,CreateDate) Values"
                    Dim Count As Integer = ds.Tables(0).Rows(0)("QuestionNumberTo") - ds.Tables(0).Rows(0)("QuestionNumberFrom")

                    For i As Integer = 0 To Count  '((ds.Tables(0).Rows(0)("QuestionNumberTo") - ds.Tables(0).Rows(0)("QuestionNumberFrom"))) 'ds.Tables(0).Rows(0)("NumberOfQuestions")
                        If ds.Tables(0).Rows(0)("QuestionNumberFrom") + i <= ds.Tables(0).Rows(0)("QuestionNumberTo") Then
                            StrSQLInsert = StrSQLInsert & "(" & ddlEventYear.SelectedValue & "," & CoachPaperID & "," & SectionNumber & "," & IIf(Level <> "", "'" & Level & "'", "NULL") & "," & ds.Tables(0).Rows(0)("QuestionNumberFrom") + i & "," & Session("LoginID") & ",GETDATE()),"
                        End If
                    Next
                    StrSQLInsert = StrSQLInsert.TrimEnd(",")
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQLInsert) > 0 Then
                        LoadGrid_AnswerKey(CoachPaperID, SectionNumber, Level)
                    End If
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Private Sub LoadGrid_AnswerKey(ByVal CoachPaperID As Integer, ByVal SectionNumber As Integer, ByVal Level As String)
        Try
            tblDGAnswerKey.Visible = True
            Dim CmdText As String = String.Empty
            CmdText = "Select AnswerKeyRecID,CoachPaperID,SectionNumber,Level,QuestionNumber,Case When Correctanswer in ('A','B','C','D','E','F','G','H','I','J') then CorrectAnswer End as CorrectAnswer_DD,Case When Correctanswer not in ('A','B','C','D','E','F','G','H','I','J') then IsNull(Correctanswer,'') end as CorrectAnswer,IsNull(DifficultyLevel,'') as DifficultyLevel,Case When Correctanswer in ('A','B','C','D','E','F','G','H','I','J') then 'RadioButton' Else 'TextArea'end  as QuestionType,IsNull(Manual,'') as Manual from TestAnswerKey where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperID & " and SectionNumber=" & SectionNumber & IIf(Level <> "", " and Level='" & Level & "'", "") & " and QuestionNumber between " & Session("QuestionNumberFrom") & " and " & Session("QuestionNumberTo") & " order by QuestionNumber"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, CmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                tblDGAnswerKey.Visible = True
                dgAnswerKey.DataSource = ds
                dgAnswerKey.DataBind()
                tblDGAnswerKey.Visible = True
                'For i As Integer = 0 To dgAnswerKey.Items.Count - 1

                'Next

                For Each row As DataGridItem In dgAnswerKey.Items
                    Try
                        If (Session("RoleID") Is Nothing) Then
                        Else
                            If (Session("RoleID").ToString() = "88") Then
                                Dim ddlCorrectAnswer As DropDownList
                                ddlCorrectAnswer = CType(row.Cells(5).FindControl("ddlCorrectAnswer"), DropDownList)
                                ddlCorrectAnswer.Enabled = False

                                Dim ddlDiffLevel As DropDownList
                                ddlDiffLevel = CType(row.Cells(6).FindControl("ddlDiffLevel"), DropDownList)
                                ddlDiffLevel.Enabled = False

                                Dim ddlManual As DropDownList
                                ddlManual = CType(row.Cells(6).FindControl("ddlManual"), DropDownList)
                                ddlManual.Enabled = False

                                btnSaveAnswers.Enabled = False
                                btnCancel.Enabled = False
                            End If
                        End If
                    Catch ex As Exception

                    End Try
                Next


                lblAnswerKey.Text = "Test Answer Key"
                If Session("QuestionType") = "RadioButton" Then
                    For Each row As DataGridItem In dgAnswerKey.Items
                        row.Cells(5).FindControl("ddlCorrectAnswer").Visible = True
                        row.Cells(5).FindControl("txtCorrectAnswer").Visible = False

                        '/********************* To show the Options based on Number of Choices********************************/
                        Dim ddlChoice As DropDownList
                        ddlChoice = CType(row.Cells(5).FindControl("ddlCorrectAnswer"), DropDownList)
                        'If ddlChoice.Items.Count > Convert.ToInt32(Session("NoOfChoices")) + 1 Then
                        If Session("NoOfChoices") + 1 < ddlChoice.Items.Count Then
                            For i As Integer = Session("NoOfChoices") + 1 To ddlChoice.Items.Count - 1
                                ddlChoice.Items(i).Enabled = False
                            Next
                        End If
                        'End If
                    Next

                Else
                    For Each row As DataGridItem In dgAnswerKey.Items
                        row.Cells(5).FindControl("ddlCorrectAnswer").Visible = False
                        row.Cells(5).FindControl("txtCorrectAnswer").Visible = True
                    Next
                End If

                ' dgAnswerKey.CurrentPageIndex = 0
            Else
                tblDGAnswerKey.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub dgAnswerKey_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs)

    End Sub

    Private Sub LoadChoices(ByVal ddlObject As DropDownList, ByVal Choices As Integer)
        Choices = Choices + 65 '65 - ASCII Value for A/Display drop down with Cholces A-J
        'ddlObject.Items.Clear()
        For i As Integer = 65 To Choices - 1
            ddlObject.Items.Add(Convert.ToChar(i))
        Next
        ddlObject.Items.Insert(0, "Select")
    End Sub
    Protected Sub dgAnswerKey_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgAnswerKey.PageIndexChanged
        lblError.Text = ""
        dgAnswerKey.Visible = True
        dgAnswerKey.CurrentPageIndex = e.NewPageIndex
        LoadGrid_AnswerKey(Session("CoachPaperID"), Session("SectionNumber"), Session("Level"))
        'dgAnswerKey.EditItemIndex = -1
    End Sub

    Public Sub txtCorrectAnswer_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim CorrectAnswer As String = ""
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            Dim txtflag As Boolean = False
            If (Not dr Is Nothing) Then
                If dr.Item("QuestionType") = "TextArea" Then
                    CorrectAnswer = dr.Item("CorrectAnswer").ToString()
                    Dim txtTemp As TextBox
                    txtTemp = sender
                    txtTemp.Text = CorrectAnswer
                End If
            Else
                Return
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub ddlCorrectAnswer_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim CorrectAnswer As String = ""
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            Dim ddlFlag As Boolean = False
            If (Not dr Is Nothing) Then
                'For Each row As DataGridItem In dgAnswerKey.Items
                '    If row.Cells(5).FindControl("txtCorrectAnswer").Visible = True Then
                '        ddlFlag = True
                '    Else
                '    End If
                'Next

                If dr.Item("QuestionType") = "TextArea" Then 'ddlFlag = True ThenddlFlag = True Then '
                    CorrectAnswer = dr.Item("CorrectAnswer").ToString()
                    Dim txtTemp As TextBox
                    txtTemp = sender
                    txtTemp.Text = CorrectAnswer
                Else
                    CorrectAnswer = dr.Item("CorrectAnswer").ToString()
                    Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                    ddlTemp = sender
                    LoadChoices(ddlTemp, Session("NoOfChoices"))

                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(IIf(CorrectAnswer = "", "Select", CorrectAnswer)))
                End If
            Else
                Return
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub ddlDiffLevel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim DifficultyLevel As String = ""
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)

            If (Not dr Is Nothing) Then
                DifficultyLevel = dr.Item("DifficultyLevel").ToString()
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(IIf(DifficultyLevel = "", "Select", dr.Item("DifficultyLevel"))))
            Else
                Return
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub ddlManual_Prerender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim Manual As String = ""
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)

            If (Not dr Is Nothing) Then
                Manual = dr.Item("Manual").ToString()
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(IIf(Manual = "", "", dr.Item("Manual"))))
            Else
                Return
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub btnSaveAnswers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAnswers.Click
        Try
            Dim StrUpdate As String = ""
            Dim CorrectAnswer As String = ""
            Dim DiffLevel As String = ""
            Dim Manual As String = ""
            lblError.Text = ""
            For Each row As DataGridItem In dgAnswerKey.Items
                CorrectAnswer = IIf(row.Cells(5).FindControl("ddlCorrectAnswer").Visible = True, DirectCast(row.Cells(5).FindControl("ddlCorrectAnswer"), DropDownList).SelectedItem.Value.Trim(), DirectCast(row.Cells(5).FindControl("txtCorrectAnswer"), TextBox).Text)
                DiffLevel = DirectCast(row.Cells(6).FindControl("ddlDiffLevel"), DropDownList).SelectedItem.Value.Trim()
                Manual = DirectCast(row.Cells(6).FindControl("ddlManual"), DropDownList).SelectedItem.Value.Trim()

                StrUpdate = StrUpdate & " Update TestanswerKey Set CorrectAnswer=" & IIf(CorrectAnswer = "", "NULL", "'" & CorrectAnswer & "'") & ",DifficultyLevel=" & IIf(DiffLevel = "", "NULL", "'" & DiffLevel & "'") & ",Manual = " & IIf(Manual = "", "NULL", "'" & Manual & "'") & ",ModifyDate=GETDATE(),ModifiedBy=" & Session("LoginID") & " Where AnswerKeyRecID=" & row.Cells(0).Text & " and EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID =" & row.Cells(1).Text & " and SectionNumber=" & row.Cells(2).Text & IIf(Level <> "", " and Level='" & row.Cells(3).Text & "'", "") & " and QuestionNumber=" & row.Cells(4).Text & " ;"
            Next
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate) > 0 Then
                lblError.Text = " Answer Keys Updated Successfully."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub btnCancelPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
        tblDGCoachPaper.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Clear()
        lblError.Text = ""
        DGTestSection.SelectedIndex = -1
        tblDGAnswerKey.Visible = False
    End Sub
    Private Sub Clear()
        lblTestSection.Text = ""
        DGTestSection.SelectedIndex = -1
        DGCoachPapers.SelectedIndex = -1
        lblError.Text = ""
    End Sub

    Protected Sub btnManualAll_Click(sender As Object, e As EventArgs) Handles btnManualAll.Click
        'from TestAnswerKey where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperID & " and SectionNumber=" & SectionNumber & IIf(Level <> "", " and Level='" & Level & "'", "") & " and QuestionNumber between " & Session("QuestionNumberFrom") & " and " & Session("QuestionNumberTo") & " order by QuestionNumber")
        Try
            If Not Session("CoachPaperID") Is Nothing Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update TestAnswerKey SET Manual='Y' where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID =" & Session("CoachPaperID") & " and SectionNumber=" & Session("SectionNumber") & IIf(Session("Level") <> "", " and Level='" & Session("Level") & "'", "") & " and QuestionNumber between " & Session("QuestionNumberFrom") & " and " & Session("QuestionNumberTo"))
                lblError.Text = " Updated Successfully. Now a coach can provide score for their students"
                'lblError.Text = lblError.Text & "<br>" & "Update TestAnswerKey SET Manual='Y' where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID =" & Session("CoachPaperID") & " and SectionNumber=" & Session("SectionNumber") & IIf(Session("Level") <> "", " and Level='" & Session("Level") & "'", "") & " and QuestionNumber between " & Session("QuestionNumberFrom") & " and " & Session("QuestionNumberTo") & " order by QuestionNumber"
                LoadGrid_AnswerKey(Session("CoachPaperID"), Session("SectionNumber"), Session("Level"))
            End If
        Catch ex As Exception
            lblError.Text = "Getting problem while updating manual for all AnswerKeys"
        End Try


    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim PgId As String = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" & ddlProduct.SelectedValue & "").ToString()
        ddlProductGroup.SelectedValue = PgId
        LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
    End Sub

    Protected Sub ddlSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadProductGroup()
        ProductStuff()
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
    End Sub



    Protected Sub btnClearFilter_Click(sender As Object, e As EventArgs)

        '  LoadGrid_CoachPapers()
        DGTestSection.Visible = False

    End Sub
    Protected Sub PopulateWeekNo()

        Dim CmdText As String = ""

        CmdText = " select distinct CP.WeekId, cp.Coachpaperid from  CoachPapers CP where CP.eventyear=" & ddlEventYear.SelectedValue & " and CP.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CP.ProductId=" & ddlProduct.SelectedValue & " and CP.level='" & ddlLevel.SelectedValue & "' and CP.Semester='" & ddlSemester.SelectedValue & "' and DOCType='Q' order by WeekId ASC"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, CmdText)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlWeekID.DataValueField = "CoachPaperId"
            ddlWeekID.DataTextField = "WeekId"
            ddlWeekID.DataSource = ds
            ddlWeekID.DataBind()

            ddlWeekID.Items.Insert(0, New ListItem("Select", "0"))
        Else
            ddlWeekID.DataSource = ds
            ddlWeekID.DataBind()

        End If
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs)
        PopulateWeekNo()
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
    End Sub
    Protected Sub ddlWeekID_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlSetNo.SelectedValue = ddlWeekID.SelectedItem.Text
        tblDGAnswerKey.Visible = False
        tblDGTestSection.Visible = False
    End Sub
End Class

