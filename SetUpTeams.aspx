﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="SetUpTeams.aspx.cs" Inherits="SetUpTeams" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CausesValidation="false" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <table id="tblContestSchedule" border="0" cellpadding="3" cellspacing="0" width="1200px" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Set Up Teams</h2>
                </center>
            </td>
        </tr>

        <tr id="IDContent" runat="server">
            <td>
                <div style="text-align: center">
                    <asp:Label ID="lbacces" runat="server"></asp:Label>
                </div>
                <br />


                <table style="margin-left: auto; margin-right: auto; width: 65%;">

                    <tr>

                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddYear" runat="server" Width="70px"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">Select year</asp:ListItem>
                                <asp:ListItem Value="-1">All</asp:ListItem>
                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                <asp:ListItem Value="2015">2015</asp:ListItem>
                            </asp:DropDownList>

                        </td>

                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                        <td style="width: 100px" align="left">
                            <asp:DropDownList ID="ddEvent" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged" runat="server" Width="100px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                        <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                            <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" DataValueField="ChapterID" runat="server" Width="100px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                        </td>
                        <td style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddTeams" runat="server" Width="125px" AutoPostBack="True" OnSelectedIndexChanged="ddTeams_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Roles</b>
                        </td>
                        <td style="width: 125px;" align="left">
                            <asp:DropDownList ID="ddlRole" runat="server" Width="125px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div style="text-align: center">
                    <asp:Label ID="lblStatusMsg" runat="server"></asp:Label>
                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div style="width: 60%; margin-left: auto; margin-right: auto;">
                    <div>
                        <asp:RadioButton ID="rbtIndividualCheck" AutoPostBack="true" GroupName="Donor" runat="server" />
                        Select from Volunteer Sign Up Pool 
                   
                        <asp:RadioButton ID="RadioButton1" AutoPostBack="true" Style="margin-left: 225px;" GroupName="Donor" runat="server" />
                        Select from NSF Database (General)
                    </div>
                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div align="center" style="width: 50%; margin-left: auto; margin-right: auto;">
                    <div>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                <asp:Button ID="btnExcel" runat="server" Visible="false" Text="Export to Excel" OnClick="btnExcel_Click" />
                    </div>
                </div>
            </td>
        </tr>
        <tr class="ContentSubTitle">
            <td>
                <table width="75%" style="margin-left: auto; margin-right: auto; font-weight: bold; background-color: #ffffcc;" id="tblAddRole" runat="server" visible="false">
                    <tr class="ContentSubTitle" align="center">
                        <td style="text-align: right">Team Lead</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlTeamLead" runat="server" AutoPostBack="True" Width="125px">
                                <asp:ListItem Value="N">No</asp:ListItem>
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Product Group</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlProductGroup" AutoPostBack="true" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" runat="server" Width="125px">
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: right">Product</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" Width="125px">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: center" colspan="8">
                            <asp:Button ID="btnUpdate" runat="server" Text="Add" Style="text-align: center" OnClick="btnUpdate_Click" />
                            &nbsp;&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_onClick" />
                        </td>

                    </tr>


                </table>
            </td>
        </tr>
        <tr align="center">
            <td>
                <asp:Panel ID="pnlVolunteerSignUp" runat="server" Visible="false">
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div align="center" id="dvTable1Title" runat="server" visible="false" style="font-weight: bold;">Table 1 : Volunteer Sign Up</div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div>
                        <div style="text-align: center">
                            <asp:Label ID="lblVolSignUpText" runat="server"></asp:Label>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div>
                            <asp:GridView ID="GvVolsignup" runat="server"
                                AutoGenerateColumns="False" OnRowCommand="GvVolsignup_RowCommand" Width="1310px" HeaderStyle-BackColor="#ffffcc" PageSize="20" AllowPaging="true" OnPageIndexChanging="GvVolsignup_PageIndexChanging" Style="table-layout: fixed;">

                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="130px">

                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" Text="Add Role" runat="server" CommandName="AddRole" />
                                            <asp:Button ID="btnUpdateTeamLead" Style="width: 130px;" Text="Update Team Lead" runat="server" CommandName="Team Lead" />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" BackColor="SkyBlue" />

                                            <asp:Button ID="btnCancel" Text="Cancel" OnClick="btnCancel_onClick" runat="server" CommandName="Cancel" BackColor="SkyBlue" />

                                        </EditItemTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-Width="57px" HeaderText="Team Lead">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlTeamLeadVol" runat="server">
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="60px" HeaderText="MemberID">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblmemberId" Style="word-wrap: break-word;" Text='<%#Eval("MemberID") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-Width="70px" HeaderText="FirstName">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFirstName" Style="word-wrap: break-word;" Text='<%#Eval("FirstName") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-Width="70px" HeaderText="LastName">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblastname" Style="word-wrap: break-word;" Text='<%#Eval("LastName") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-Width="100px" HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEmail" Style="word-wrap: break-word;" Text='<%#Eval("Email") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderStyle-Width="70px" HeaderText="Hphone">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblhphone" Style="width: 80px; text-align: center; word-wrap: break-word;" Text='<%#Eval("hphone") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderStyle-Width="70px" HeaderText="Cphone">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblcphone" Style="width: 80px; text-align: center; word-wrap: break-word;" Text='<%#Eval("Cphone") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderStyle-Width="40px" HeaderText="TeamId">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTeamID" Text='<%#Eval("TeamId") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderStyle-Width="70px" HeaderText="Team">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTeamname" Style="width: 120px; text-align: left; word-wrap: break-word;" Text='<%#Eval("TeamName") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Year" HeaderStyle-Width="45px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblyear" Text='<%#Eval("YEAR") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Eventname" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblEventName" Style="width: 120px; text-align: left; word-wrap: break-word;" Text='<%#Eval("Eventname") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ChapterName" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblchapterName" Style="word-wrap: break-word;" Text='<%#Eval("ChapterName") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="ProductGroup" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblproductGroup" Style="word-wrap: break-word;" Text='<%#Eval("ProductGroup") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>


                                    <asp:TemplateField HeaderText="Product" HeaderStyle-Width="70px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblproduct" Style="word-wrap: break-word;" Text='<%#Eval("product") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AvailHours" HeaderStyle-Width="55px">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblhrs" Text='<%#Eval("AvailHours") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="City" HeaderStyle-Width="60px">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Style="word-wrap: break-word;" Text='<%#Eval("City") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State" HeaderStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Style="word-wrap: break-word;" Text='<%#Eval("State") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                </Columns>


                            </asp:GridView>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div align="center" id="dvTable2Title" runat="server" visible="false" style="font-weight: bold;">Table 2 : Existing Roles</div>
                        <div align="center">
                            <asp:GridView ID="GridView1" runat="server" ItemStyle-HorizontalAlign="left" HeaderStyle-BackColor="#ffffcc"></asp:GridView>
                        </div>
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td>
                <asp:Panel ID="pIndSearch" runat="server" Width="1200px" Visible="False" HorizontalAlign="Center">
                    <b>Search NSF member</b>
                    <div align="center" style="width: 1200px;">
                        <table border="1" runat="server" id="tblIndSearch" style="text-align: center; margin-bottom: 10px;" width="30%" visible="true" bgcolor="silver">
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Donor Type:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                                        <asp:ListItem Value="Organization">Organization</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Organization Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlState" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>

                        <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                            <b>Search Result</b>
                            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1200px; margin-bottom: 10px;" OnRowCommand="GridMemberDt_RowCommand" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="20" OnPageIndexChanging="GridMemberDt_PageIndexChanging">
                                <Columns>
                                    <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id" CommandName="Select"></asp:ButtonField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" ></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                    <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                    <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="Panel1" runat="server" Visible="False" HorizontalAlign="Center">
                            <b>Search Result</b>
                            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="grdFilterVolunteer" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1200px; margin-bottom: 10px;" OnRowCommand="grdFilterVolunteer_RowCommand" HeaderStyle-BackColor="#ffffcc">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAddRole" runat="server" Text="Add Role" CommandName="Add Role"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewRole" runat="server" Text="View Role" CommandName="View Role"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnUpdateTeamLeadVol" runat="server" Text="Update Team Lead" CommandName="Update Team Lead" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Team Lead">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlTeamLeadVolunteer" runat="server">
                                                <asp:ListItem Value="N">No</asp:ListItem>
                                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AutomemberId" HeaderText="Member Id"></asp:BoundField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                    <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                    <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <div style="clear: both;"></div>
                        <asp:Panel ID="pnlViewRole" runat="server" Visible="False" HorizontalAlign="Center">
                            <b>Current Roles For :
                                <asp:Label ID="lblCurrentVolName" Style="color: #347235;" runat="server"></asp:Label></b>
                            <div style="clear: both;"></div>
                            <asp:Label ID="lblViewRoleText" runat="server" Style="color: Red;"></asp:Label>
                            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="grdViewRoles" DataKeyNames="MemberId" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1200px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
                                <Columns>

                                    <asp:BoundField DataField="VolunteerID" HeaderText="Volunteer Id"></asp:BoundField>
                                    <asp:BoundField DataField="MemberId" HeaderText="Member Id"></asp:BoundField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="RoleCode" HeaderText="Role"></asp:BoundField>
                                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                                    <asp:BoundField DataField="ZoneCode" HeaderText="Zone"></asp:BoundField>
                                    <asp:BoundField DataField="ClusterCode" HeaderText="Cluster"></asp:BoundField>
                                    <asp:BoundField DataField="TeamLead" HeaderText="TeamLead"></asp:BoundField>
                                    <asp:BoundField DataField="National" HeaderText="National"></asp:BoundField>
                                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                                    <asp:BoundField DataField="CreateDate" HeaderText="Created Date" dataformatstring="{0:MM-dd-yyyy}"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnMemberID" Value="0" runat="server" />
    <asp:HiddenField ID="hdnMemberIDs" Value="0" runat="server" />

</asp:Content>
