

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AssignTeachers.aspx.vb" Inherits="AssignTeachers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <div align="left">
      <br />
      <asp:hyperlink CssClass="btn_02" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
   </div>
<%--   <table runat="server"><tr><td align="center">
--%>   <div  runat="server" align="center">
      <table border="0" cellpadding ="3" cellspacing ="0">
         <tr>
            <td align="center" colspan="3">
               <h2>Assign Teacher Roles</h2>
            </td>
         </tr>
         <tr>
            <td align="left">Volunteer Role :</td>
            <td align="left">
               <asp:DropDownList ID="ddlRole" 
                    OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" Width ="231px" 
                    AutoPostBack = "true" runat="server" Height="25px">
                  <asp:ListItem Selected="True" Value="97">Online WorkShop Coordinator</asp:ListItem>
                  <asp:ListItem Value="98">Online WorkShop Teacher</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td align="left">&nbsp;</td>
         </tr>
         <tr>
            <td align="left">Volunteer Name : </td>
            <td align="left">
               <asp:TextBox   ID="txtParent" Width="227px" runat="server" Enabled="false" 
                    Height="20px"></asp:TextBox>
            </td>
            <td align="left">
               <asp:Button ID="Search" runat="server" Text="Search" />
            </td>
         </tr>
         <%--<tr runat ="server" id="TrPrdGrp" visible ="false" >
            <td align="left">Product Group</td>
            <td align="left">
               <asp:DropDownList ID="ddlPrdGroup" Width="170px"   DataTextField="Name" DataValueField="code"  OnSelectedIndexChanged="ddlPrdGroup_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
            </td>
            <td align="left">&nbsp;</td>
         </tr>
         <tr runat ="server" id="TrPrd" visible ="false">
            <td align="left">Product </td>
            <td align="left">
               <asp:DropDownList ID="ddlPrd" Enabled="false" DataTextField="Name"  DataValueField="code"  Width="170px" runat="server"></asp:DropDownList>
            </td>
            <td align="left">&nbsp;</td>
         </tr>--%>
         <tr>
            <td align="center" colspan ="3">
               <asp:Button ID="btnassignteacher" runat="server" Width="270px" 
                    Text="Assign Online Workshop Coordinator" />
            </td>
         </tr>
         <tr>
            <td align="center" colspan="3">
               <asp:Label ID="lblerr" ForeColor = "Red" runat="server" ></asp:Label>
            </td>
         </tr>
      </table>
        <asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
         <b> Search NSF member</b>   
          <div align = "center">
            <table border="1" runat="server" id="tblIndSearch" style="text-align:center;"  width="30%" visible="true" bgcolor="silver" >
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
                  <td align="left" >
                     <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
                  <td  align ="left" >
                     <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
                  <td align="left">
                     <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <%-- <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
                  <td align="left" >
                      <asp:DropDownList ID="ddlState" runat="server">
                      </asp:DropDownList></td>
                  </tr>--%>
               <tr>
                  <td align="right">
                     <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                  </td>
                  <td  align="left">
                     <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>
                  </td>
               </tr>
            </table>
            <asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
         </div>
         <br />
         <asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
            <b> Search Result</b>
            <asp:GridView   HorizontalAlign="Left" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
               <Columns>
                  <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                  <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                  <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                  <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                  <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                  <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                  <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                  <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                  <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                  <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
               </Columns>
            </asp:GridView>
         </asp:Panel>
      </asp:Panel>
   </div> 
<%-- </td></tr></table>
 <table><tr><td align="center">--%>
    <div align="center">
      <asp:DataGrid ID="DGVolunteer" runat="server" DataKeyField="volunteerID" 
         AutoGenerateColumns="False"  OnItemCommand="DGVolunteer_ItemCommand" 
         CellPadding="4" 
         BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
         BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
         <FooterStyle BackColor="#CCCCCC" />
         <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
         <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
            Mode="NumericPages" />
         <ItemStyle BackColor="White" />
         <Columns>
            <ASP:TemplateColumn>
               <ItemTemplate>
                  <asp:LinkButton id="lbtnRemove" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete"  Visible  = '<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:LinkButton>
               </ItemTemplate>
            </ASP:TemplateColumn>
            <asp:Boundcolumn DataField="volunteerID"  HeaderText="Volunteer ID" Visible="false" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Role"  HeaderText="Role" Visible="true" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Vname"  HeaderText="Volunteer Name" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="EMail" HeaderText="EMail"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="City" HeaderText="City"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="State" HeaderText="State"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ProductName" HeaderText="ProductName"/>
         </Columns>
         <HeaderStyle BackColor="White" />
      </asp:DataGrid>
      <br />
      <asp:Label ID="HlblParentID" runat="server" Visible="false"></asp:Label>
   </div>
   <%--</td></tr></table>--%>
</asp:Content>

