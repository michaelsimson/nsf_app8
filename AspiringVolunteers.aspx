<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.AspiringVolunteers" CodeFile="AspiringVolunteers.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Volunteerlist</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center" title="Aspiring Volunteers">
				<TR>
					<TD class="Heading" vAlign="top" align="center" width="100%" colSpan="2">Volunteers 
						List
					</TD>
				</TR>
				<tr>
					<td colspan="2">
						<br>
					</td>
				</tr>
				<tr width="100%">
					<td class="ItemLabel" vAlign="top" noWrap width="50%">NSF Chapter</td>
					<td class="ItemText" vAlign="top" noWrap align="left"><asp:dropdownlist id="ddlNSFChapter" runat="server" AutoPostBack="True" CssClass="SmallFont"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td colspan="2">
						<br>
					</td>
				</tr>
				<tr>
					<td width="100%" colSpan="2"><asp:datagrid id="dgVolunteerList" runat="server" CssClass="GridStyle" AutoGenerateColumns="False"
							AllowSorting="True" BorderWidth="0px" CellSpacing="1" CellPadding="3" DataKeyField="AutoMemberID" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update /" HeaderText="" CancelText="Cancel"
									EditText="Edit" HeaderStyle-CssClass="GridHeader"></asp:EditCommandColumn>
								<asp:TemplateColumn HeaderText="Volunteer Name" ItemStyle-Wrap="False" SortExpression="AutoMemberID">
									<ItemTemplate>
										<asp:HyperLink runat="server" id="hLinkVolunteer"></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Last Name" ItemStyle-Wrap="False" SortExpression="LastName">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' ID="lblLastName" CssClass=SmallFont>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' ID="txtLastName" CssClass=SmallFont>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="First Name" ItemStyle-Wrap="False" SortExpression="FirstName">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' ID="lblFirstName" CssClass=SmallFont>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' ID="txtFirstName" CssClass=SmallFont>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="City" ItemStyle-Wrap="False" SortExpression="City">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>' ID="lblCity" CssClass=SmallFont>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>' ID="txtCity" CssClass=SmallFont>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Home Phone" ItemStyle-Wrap="False" SortExpression="HPhone">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HPhone") %>' ID="lblHomePhone" CssClass=SmallFont>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HPhone") %>' ID="txtHomePhone" CssClass=SmallFont>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="e-mail" ItemStyle-Wrap="False" SortExpression="Email">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>' ID="lblEMail" CssClass=SmallFont>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email") %>' ID="txtEMail" CssClass=SmallFont>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Activity 1" ItemStyle-Wrap="False" SortExpression="VolunteerRole1">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblVolunteerRole1" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:DropDownList runat="server" ID="ddlVolunteerRole1" CssClass="SmallFont"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Activity 2" ItemStyle-Wrap="False" SortExpression="VolunteerRole2">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblVolunteerRole2" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:DropDownList runat="server" ID="ddlVolunteerRole2" CssClass="SmallFont"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Activity 1" ItemStyle-Wrap="False" SortExpression="VolunteerRole3">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblVolunteerRole3" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:DropDownList runat="server" ID="ddlVolunteerRole3" CssClass="SmallFont"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<div>
				<asp:HyperLink id="hlinkChapterFunctions" runat="server" NavigateUrl="ChapterMain.aspx">Back to Chapter Functions</asp:HyperLink>
			</div>
		</form>
	</body>
</HTML>

 

 
 
 