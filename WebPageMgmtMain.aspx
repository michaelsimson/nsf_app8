﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" Title="Web Page Management" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<script runat="server">
    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("AssignWebAccess.aspx")
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("RoleId").ToString() = "90" Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") < 1 Then
                lblErr.Text = "You were not given access privileges.  So you cannot use Create / Update Folders."
                Exit Sub
            End If
        ElseIf Session("RoleId").ToString() = "43" Then
            'have to check access permissions also
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") < 1 Then
                lblErr.Text = "You were not given access privileges.  So you cannot use Create / Update Folders."
                Exit Sub
            End If
        End If
        Response.Redirect("FolderMgmt.aspx")
    End Sub
    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session("RoleId").ToString() = "90" Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") < 1 Then
                lblErr.Text = "You were not given access privileges.  So you cannot give access privileges to others."
                Exit Sub
            End If
        ElseIf Session("RoleId").ToString() = "43" Then
            'have to check access permissions also
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") < 1 Then
                lblErr.Text = "You were not given access privileges.  So you cannot give access privileges to others."
                Exit Sub
            End If
        End If
        Response.Redirect("WebAccessMgmt.aspx")
    End Sub

    Protected Sub LinkButton5_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Upload Download
        If Session("RoleId").ToString() = "90" Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") < 1 Then
                lblErr.Text = "You were not given access privileges.  So you cannot use Download/Upload to download or upload files and webpages"
                Exit Sub
            End If
        ElseIf Session("RoleId").ToString() = "43" Then
            'have to check access permissions also
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") < 1 Then
                lblErr.Text = "You were not given access privileges.  So you cannot use Download/Upload to download or upload files and webpages."
                Exit Sub
            End If
        End If
        Response.Redirect("WebPageMgmt.aspx")
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Or (Session("RoleId").ToString() = "43") Or (Session("RoleId").ToString() = "90") Then
            If Session("RoleId").ToString() = "1" Then
                LinkButton4.Visible = True
            ElseIf Session("RoleId").ToString() = "43" Then
                LinkButton1.Visible = False
                LinkButton3.Visible = False
                LinkButton4.Visible = False
            End If
        Else
            Server.Transfer("maintest.aspx")
        End If
    End Sub

    
    
    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Only for RoleID 1
        Response.Redirect("FileAccessMgmt.aspx")
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div style ="width :1000px;">
<br />
  &nbsp;&nbsp;&nbsp;<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink><br />
<table border="0" cellpadding="3" cellspacing="0" width="250px" align="center" >
<tr><td align="center" style="font-weight:bold">Web Page Management</td></tr>
<tr><td align="center" height="15px"></td></tr> 
<tr><td align="left"><asp:LinkButton ID="LinkButton1" runat="server" 
        onclick="LinkButton1_Click">Assign Roles for Web Folder Access</asp:LinkButton></td></tr>
<tr><td align="left"><asp:LinkButton ID="LinkButton2" runat="server" 
        onclick="LinkButton2_Click">Create/Update Folders</asp:LinkButton></td></tr>
<tr><td align="left"><asp:LinkButton ID="LinkButton3" runat="server" 
        onclick="LinkButton3_Click">Manage Access Privileges</asp:LinkButton></td></tr>
<tr><td align="left"><asp:LinkButton ID="LinkButton4" Visible="false"  
        runat="server" onclick="LinkButton4_Click">Manage File Access Privileges</asp:LinkButton></td></tr>
<tr><td align="left"><asp:LinkButton ID="LinkButton5" runat="server" 
        onclick="LinkButton5_Click">Download/Upload Web Pages</asp:LinkButton></td></tr>
<tr><td align="center">
    <asp:Label ID="lblErr" runat="server" ForeColor ="Red"></asp:Label></td> </tr>
</table><br /><br /></div> 
</asp:Content>

