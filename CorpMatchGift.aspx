<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" EnableEventValidation="false"
    Debug="true" MaintainScrollPositionOnPostback="true" CodeFile="CorpMatchGift.aspx.vb"
    Inherits="CorpMatchGift" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
   
   <%-- <asp:UpdatePanel ID="Upgrid" runat="server" UpdateMode="Always">
        <ContentTemplate>--%>
            <div>
                <script language="javascript" type="text/javascript">
                    function PopupPicker(ctl, w, h) {
                        var PopupWindow = null;
                        settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                        PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
                        PopupWindow.focus();
                    }
                </script>
            </div>


            <div style="text-align: left">
                <br />
                <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="VolunteerFunctions.aspx"
                    runat="server">Back to Volunteer Functions</asp:LinkButton>
                &nbsp;&nbsp;&nbsp;<br />
            </div>
            <div align="center">
                <table border="0" cellpadding="4" cellspacing="0">
                    <tr id="TrMatGiftDet" runat="server" visible="false" style="width: 50%">
                        <td align="right" colspan="6">
                            <table>
                                <tr>
                                    <td align="left" colspan="2">Matching_Gift_URL:</td>
                                    <td style="width: auto">
                                        <asp:Label ID="lblMatGiftURL" runat="server" Enabled="false"></asp:Label>
                                        <%-- <asp:TextBox ID="txtMatGiftURL" runat="server" Enabled="false"></asp:TextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">Matching_Gift_Acc_No:</td>
                                    <td colspan="2">
                                        <asp:Label ID="lblMatGiftAccNo" runat="server" Enabled="false"></asp:Label>

                                        <%--   <asp:TextBox ID="txtMatGiftAccNo" runat="server" Enabled="false"></asp:TextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">Matching_Gift_User_Id:</td>
                                    <td colspan="2">
                                        <asp:Label ID="lblMatGiftUserId" runat="server" Enabled="false"></asp:Label>

                                        <%--   <asp:TextBox ID="txtMatGiftUserId" runat="server" Enabled="false"></asp:TextBox>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">Matching_Gift_Password:</td>
                                    <td colspan="2">
                                        <asp:Label ID="lblMatGiftPwd" runat="server" Enabled="false"></asp:Label>

                                        <%-- <asp:TextBox ID="txtMatGiftPwd" runat="server" Enabled="false"></asp:TextBox>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="7">
                            <h3>United Way, Corp and Other Contributions</h3>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">Organization  </td>
                        <td align="left">
                            <asp:DropDownList ID="DDMatchURL" runat="server" AutoPostBack="true" Width="150px">
                                <asp:ListItem Value="0">Organization Name</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td id="Td1" align="left" runat="server">
                            <asp:TextBox ID="txtOrganization" Enabled="false" Width="150px" runat="server" Visible="false"></asp:TextBox>
                        </td>
                        <td align="left">Org. Check Number</td>
                        <td align="left">
                            <asp:TextBox ID="txtChckNo" Enabled="true" Width="100px" runat="server" AutoPostBack="true"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr id="tremployer" runat="server" visible="false">
                        <td align="left">Emp / Foundation</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmployer" Enabled="false" Width="150px" runat="server"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnEmployer" OnClick="btnEmployer_Click" runat="server" Text="Search" />
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td align="left">Employee Name </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmployee" Enabled="false" Width="150px" runat="server"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnEmployee" OnClick="btnEmployee_Click" runat="server" Text="Search" />
                            &nbsp;
               <asp:DropDownList ID="ddlEmployee" runat="server"></asp:DropDownList>
                        </td>
                        <td align="left">Check Date</td>
                        <td align="left">
                            <asp:TextBox ID="txtChkDate" Enabled="true" Width="100px" runat="server"></asp:TextBox>
                            <a href="javascript:PopupPicker('<%=txtChkDate.ClientID%>', 200, 200);">
                                <img src="images/calendar.gif" border="0"></a>
                            <asp:RequiredFieldValidator ControlToValidate="txtChkDate" ID="RequiredFieldValidator9"
                                runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                        </td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td align="left">Program Type  </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPrgmType" runat="server">
                                <asp:ListItem Value="0" Selected="True">Select Program Type</asp:ListItem>
                                <asp:ListItem>Matching Gift</asp:ListItem>
                                <asp:ListItem>Volunteering</asp:ListItem>
                                <asp:ListItem>Employee Donations</asp:ListItem>
                                 <asp:ListItem>Award</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblCorpEmpID" ForeColor="White" runat="server"></asp:Label>
                        </td>
                        <td align="left">PaymentID</td>
                        <td align="left">
                            <%--    <asp:Button ID="btnSearchDon" OnClick="btnSearchDon_Click" runat="server" Text="Search Donations" Visible="false" />
                            --%>
                            <asp:TextBox ID="txtPaymentID" Enabled="true" Width="100px" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtPaymentID"
                                Display="Dynamic" ErrorMessage="Only Numeric Values"
                                Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                        </td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td align="left">Employee Amount        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmpAmt" Width="150px" runat="server"></asp:TextBox>

                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtEmpAmt"
                                Display="Dynamic" ErrorMessage="Only Numeric Values"
                                Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left">
                            <asp:Literal ID="lblEmpAmt" runat="server"></asp:Literal>
                        </td>
                        <td align="left">Gift Date</td>
                        <td align="left">
                            <asp:TextBox ID="txtGiftDate" Enabled="true" Width="100px" runat="server"></asp:TextBox>
                            <a href="javascript:PopupPicker('<%=txtGiftDate.ClientID%>', 200, 200);">
                                <img src="images/calendar.gif" border="0"></a>


                        </td>
                        <td align="left">Hours </td>
                        <td align="left">
                            <asp:TextBox ID="txtHours" Width="100px" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtHours"
                                Display="Dynamic" ErrorMessage="Only Numeric Values"
                                Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">Employer Amount        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMatchAmt" Width="150px" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtMatchAmt"
                                Display="Dynamic" ErrorMessage="Only Numeric Values"
                                Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left">
                            <asp:Literal ID="lblMatchAmt" runat="server"></asp:Literal>
                        </td>
                        <td align="left">Pay Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPayYear" runat="server"></asp:DropDownList>
                        </td>
                        <td align="left">Email</td>
                        <td align="left">
                            <asp:TextBox ID="txtMemEmail" Width="150px" runat="server" Text=""></asp:TextBox>
                            <asp:RegularExpressionValidator ID="revPrimaryEmailInd" runat="server" ControlToValidate="txtMemEmail"
                                Display="Dynamic"
                                ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator><%--<br />--%>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">Total Amount        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" Width="150px" runat="server"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtAmount"
                                Display="Dynamic" ErrorMessage="Only Numeric Values" Operator="DataTypeCheck"
                                SetFocusOnError="True" Type="Double"></asp:CompareValidator>

                        </td>
                        <td align="left">
                            <asp:Literal ID="ltltotal" runat="server"></asp:Literal>
                        </td>
                        <td align="left">Pay Method</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPayMethod" runat="server">
                                <asp:ListItem Value="0" Selected="True">Select Pay Method</asp:ListItem>
                                <asp:ListItem>Payroll</asp:ListItem>
                                <asp:ListItem>One time</asp:ListItem>
                                <asp:ListItem>Volunteer</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>

                    </tr>
                    <tr>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left">
                            <br />
                        </td>
                        <td align="left">Employer's EmployeeID </td>
                        <td align="left">
                            <asp:TextBox ID="txtEmprMemID" runat="server" Text="" Enabled="false"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtEmprMemID"
                                Display="Dynamic" ErrorMessage="Only Numeric Values" Operator="DataTypeCheck"
                                SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                        </td>
                        <td align="left">&nbsp;</td>
                        <td align="left"><%--<br />--%>
                                
                        </td>

                    </tr>
                    <tr>
                        <td align="left"></td>
                        <td align="center" colspan="3">
                            <asp:Button ID="btnSubmit" runat="server" Text="Add" />
                            &nbsp; &nbsp;&nbsp; &nbsp;
               <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                        </td>
                        <td align="left">
                            <asp:Label ID="lblDonationID" runat="server" ForeColor="White"></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">&nbsp;</td>
                        <td align="left" colspan="3">
                            <asp:Label ID="lblerr" runat="server" ForeColor="Red" Style="text-align: left" Visible="false"></asp:Label>

                        </td>
                        <td align="left">&nbsp;</td>
                    </tr>
                    <tr id="TrPaymentIDErr" runat="server" visible="false">
                        <td colspan="5" runat="server" align="center">
                            <table>
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblPayIDErr" runat="server" Text="Is PaymentID Available" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Button ID="BtnYes" runat="server" Text="Yes" />
                                        &nbsp;
         <asp:Button ID="BtnNo" runat="server" Text="No" />
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </div>
            <table border="0" width="900px" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
                            <b>Search NSF member</b>
                            <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%"
                                visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;State:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find"
                                            CausesValidation="False" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick"
                            CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
                            <br />
                            <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                                <b>Search Result</b>
                                <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt"
                                    DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand"
                                    RowStyle-CssClass="SmallFont">
                                    <Columns>
                                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                        <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                        <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                        <asp:BoundField DataField="Employer" ItemStyle-Width="200px" HeaderText="Employer">
                                        </asp:BoundField>
                                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                        <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="pORGSearch" runat="server" Width="950px" Visible="False">
                            <b>Search Organization</b>
                            <table border="1" runat="server" id="tblORGSearch" style="text-align: center" width="30%"
                                visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">Organization </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="ItemLabel" valign="top" align="left">State </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlOrgState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" align="left">City </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;NSF Chapter</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlNSFChapter" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button ID="btnORGSearch" runat="server" OnClick="btnORGSearch_onClick" Text="Find"
                                            CausesValidation="False" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;					
                        <asp:Button ID="btnORGClose" runat="server" Text="Close" OnClick="btnORGClose_onclick"
                            CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblORGSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
                            <br />
                            <asp:Panel ID="Panel5" runat="server" Visible="False" HorizontalAlign="Center">
                                <b>Search Result</b>
                                <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridORG"
                                    DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridORG_RowCommand"
                                    RowStyle-CssClass="SmallFont">
                                    <Columns>
                                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                        <asp:BoundField DataField="Organization_name" HeaderText="Organization Name"></asp:BoundField>
                                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Panel ID="pEmployerSearch" runat="server" Width="950px" Visible="False">
                            <b>Search Employer</b>
                            <table border="1" runat="server" id="tblEmployerSearch" style="text-align: center"
                                width="30%" visible="true" bgcolor="silver">
                                <tr>
                                    <td class="ItemLabel" valign="top" nowrap align="right">Employer </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmployerName" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="ItemLabel" valign="top" align="left">State </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlEmployerState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ItemLabel" valign="top" align="left">City </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtEmployerCity" runat="server"></asp:TextBox>
                                    </td>
                                    <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;NSF Chapter</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlEmployerNSFChapter" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="Center" colspan="4">
                                        <asp:Button ID="btnEmployerSearch" runat="server" OnClick="btnEmployerSearch_onClick"
                                            Text="Find" CausesValidation="False" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnEmployerClose" runat="server" Text="Close" OnClick="btnEmployerClose_onclick"
                            CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="lblEmployerSearch" ForeColor="red" runat="server" Visible="false"
                                Text="Select the Chapter from the DropDown"></asp:Label>
                            <br />
                            <asp:Panel ID="PanelEmployer" runat="server" Visible="False" HorizontalAlign="Center">
                                <b>Search Result</b>
                                <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridEmployer"
                                    DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridEmployer_RowCommand"
                                    RowStyle-CssClass="SmallFont">
                                    <Columns>
                                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                        <asp:BoundField DataField="Organization_name" HeaderText="Organization Name"></asp:BoundField>
                                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        
                        <br />
                        <br />
                        
                        <asp:Panel ID="PDonations" runat="server" Width="950px" Visible="False">
                    
                        
                            <b>Table 1:  Organization Remittances</b><br />
                                <div id="Div2" runat="server" align="left" >
                          <asp:Button ID="ExpExcel1" runat="server" Text="Export to Excel" Visible="false" /></div>
                          <br />
                            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left"  
                                ID="GridDonations" AllowPaging="true" DataKeyNames="DonationId"
                                AutoGenerateColumns="false" runat="server"
                                OnRowCommand="GridDonations_RowCommand" RowStyle-CssClass="SmallFont"
                                PageSize="50">
                                <Columns>
                                    <asp:ButtonField Text="Select" />
                                    <asp:BoundField DataField="DonationId" HeaderText="Donation Id"></asp:BoundField>
                                    <asp:BoundField DataField="DepositSlip" HeaderText="Deposit Slip# "></asp:BoundField>
                                    <asp:BoundField DataField="Organization_name" HeaderText="Organization Name"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                    <asp:BoundField DataField="OrgMemberID" HeaderText="OrgMemberID"></asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DonationDate" HeaderText="Check Date" DataFormatString="{0:d}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Transaction_number" HeaderText="Check Number"></asp:BoundField>
                                    <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="State" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="Reconcile" HeaderText="Reconcile"></asp:BoundField>

                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:Button ID="btnCloseOrgRem" runat="server" Text="Close Table 1" />
                        </asp:Panel>
                       
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <br />
                        <br />
                        <asp:Panel ID="Panel1" runat="server" Width="950px" Visible="False">
                            <asp:Label ID="LabelACH" runat="server" Visible="false"><b>Table 2: ACH/EFT</b></asp:Label><br />
                               <div id="Div3" runat="server" align="left" >
                          <asp:Button ID="ExpExcel2" runat="server" Text="Export to Excel" Visible="false"  /></div>
                          <br />
                            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left"
                                ID="GridVACHEFT" AllowPaging="true"
                                AutoGenerateColumns="false" runat="server"
                                RowStyle-CssClass="SmallFont"
                                PageSize="50">
                                <Columns>
                                    <asp:ButtonField Text="Select" />
                                    <asp:BoundField DataField="BankID" HeaderText="BankId"></asp:BoundField>
                                    <asp:BoundField DataField="TransDate" HeaderText="TransDate" DataFormatString="{0:d}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="checknumber" HeaderText="checknumber"></asp:BoundField>
                                    <asp:BoundField DataField="VendCust" HeaderText="VendCust"></asp:BoundField>
                                    <asp:BoundField DataField="Reason" HeaderText="Reason"></asp:BoundField>
                                    <asp:BoundField DataField="TransCat" HeaderText="Transcat"></asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BankTransID" HeaderText="BankTransID"></asp:BoundField>
                                    <asp:BoundField DataField="MemberID" HeaderText="MemberID"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                    <asp:BoundField DataField="Description" HeaderText="Description"></asp:BoundField>
                                    <asp:BoundField DataField="Reconcile" HeaderText="Reconcile"></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <asp:Label ID="LabeAchnorecord" runat="server" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:Button ID="BtncloseAcH" runat="server" Text="Close Table 2" />
                        </asp:Panel>
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:Panel ID="PnlCorpEmp" runat="server" Width="950px" Visible="False">
                            <br />
                            <asp:Label ID="lblHead" runat="server" Visible="false"><b>Table 2: Employees behind the Payment</b></asp:Label>
                            <br />
                            <b>
                                <asp:Label ID="lblCorp" runat="server"></asp:Label></b>
                            <br />
                            <asp:Label ID="LabLCorp" runat="server" ForeColor="Red"></asp:Label>
                            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridCorp"
                                DataKeyNames="CorpEmpID" AutoGenerateColumns="false" runat="server" OnRowCommand="GridCorp_RowCommand"
                                RowStyle-CssClass="SmallFont">
                                <Columns>
                                    <asp:BoundField DataField="CorpEmpID" HeaderText="CorpEmp ID"></asp:BoundField>
                                    <asp:BoundField DataField="OrgMemberID" HeaderText="OrgMemberID" Visible="true">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OrgName" HeaderText="Organization Name"></asp:BoundField>
                                    <asp:BoundField DataField="EmployerMembID" HeaderText="EmployerMembID" Visible="true">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EmpName" HeaderText="EmpName"></asp:BoundField>
                                    <asp:BoundField DataField="MemberID" HeaderText="Member ID" Visible="true"></asp:BoundField>
                                    <asp:BoundField DataField="EmployeeName" HeaderText="EmployeeName"></asp:BoundField>
                                    <asp:BoundField DataField="ProgramType" HeaderText="Program Type"></asp:BoundField>
                                    <asp:BoundField DataField="OrgCheckNo" HeaderText="OrgCheckNo"></asp:BoundField>
                                    <asp:BoundField DataField="CheckDate" HeaderText="Check Date" DataFormatString="{0:d}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EmpFunds" HeaderText="Emp Amount" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CompFunds" HeaderText="Match Amount" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TotAmount" HeaderText="Total" DataFormatString="{0:c}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayYear" HeaderText="PayYear"></asp:BoundField>
                                    <asp:BoundField DataField="GiftDate" HeaderText="GiftDate" DataFormatString="{0:d}">
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PaymentID" HeaderText="PaymentID"></asp:BoundField>
                                    <asp:BoundField DataField="PayMethod" HeaderText="PayMethod"></asp:BoundField>
                                    <asp:BoundField DataField="Hours" HeaderText="Hours"></asp:BoundField>
                                    <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        <div>



                              <div id="Div1" runat="server" align="left" >
                          <asp:Button ID="ExpExcel3" runat="server" Text="Export to Excel"  Visible="false" /></div>
                          <br />

                            <asp:GridView ID="GridCorpEmp" runat="server" AllowPaging="true "
                                Style="width: 90%; margin: 0 auto;" AutoGenerateColumns="false"
                                DataKeyNames="CorpEmpID" PageSize="50">
                                <Columns>

                                    <asp:TemplateField>

                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" Text="Modify" runat="server" CommandName="Edit" />

                                        </ItemTemplate>

                                        <EditItemTemplate>

                                            <asp:Button ID="btnupdate" Text="Update" runat="server" CommandName="Delete" BackColor="SkyBlue" />

                                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" BackColor="SkyBlue" />

                                        </EditItemTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CorpEmpID">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lbEmpID" Text='<%#Eval("CorpEmpID")%>' />

                                        </ItemTemplate>

                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="OrgMemberID">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbOrg" Text='<%#Eval("OrgMemberID")%>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="EMemberID">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lbEmployerMemberid" Text='<%#Eval("EmployerMembID")%>' />

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MemberID">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbMemberID" Text='<%#Eval("MemberID")%>' />
                                        </ItemTemplate>
                                         <EditItemTemplate>
    <asp:Label runat="server" ID="lbMember" Text='<%#Eval("MemberID")%>' />
         <asp:Button ID="BtnSearch" runat="server" Text="search"   OnClick="SearchMemberID"
            />
             <asp:TextBox   runat="server" Visible="false"  ID="TxtMember" Text='<%#Eval("MemberID") %>' />
              </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbName" Text='<%#Eval("Name")%>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ProgramType">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbProgramtype" Text='<%#Eval("ProgramType")%>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div style="text-align: right">
                                                <asp:TextBox runat="server" ID="TxtProgramtype" Style="text-align: left" Width="80px"
                                                    Text='<%#Eval("ProgramType")%>' />
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CheckDate">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbCheckdate" Text='<%#Eval("CheckDate", "{0:MM/dd/yyyy}")%>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div style="text-align: right">
                                                <asp:TextBox runat="server" ID="TxtCheckdate" Style="text-align: left" Width="60px"
                                                    Text='<%#Eval("CheckDate")%>' />
                                                <asp:CompareValidator ID="CompareValidCheck11" runat="server" Operator="DataTypeCheck"
                                                    Type="date" ControlToValidate="TxtCheckdate" ErrorMessage="MM/DD/YYYY format" />
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="OrgCheckNo">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbChecknumber" Text='<%#Eval("OrgCheckNo")%>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div style="text-align: right">
                                                <asp:TextBox runat="server" ID="TxtCheckNumber" Style="text-align: right" Width="80px"
                                                    Text='<%#Eval("OrgCheckNo")%>' />
                                                <asp:CompareValidator ID="CompareValidCheck" runat="server" Operator="DataTypeCheck"
                                                    Type="Integer" ControlToValidate="TxtCheckNumber" ErrorMessage="Enter Valid Number" />
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EmpFunds">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lblEmpFunds" Text='<%#Eval("EmpFunds")%>' />

                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                        <EditItemTemplate>
                                            <div style="text-align: right">
                                                <asp:TextBox runat="server" ID="TxtEpmfunds" Style="text-align: right" Width="80px"
                                                    Text='<%#Eval("EmpFunds") %>' />
                                                <asp:CompareValidator ID="CompareValEmp" runat="server" Operator="DataTypeCheck"
                                                    Type="double" ControlToValidate="TxtEpmfunds" ErrorMessage="Enter Valid Amount" />

                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CompFunds">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbCompFunds" Text='<%#Eval("CompFunds")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                        <EditItemTemplate>
                                            <div style="text-align: right">
                                                <asp:TextBox runat="server" ID="TxtCompfunds" Style="text-align: right" Width="80px"
                                                    Text='<%#Eval("CompFunds")%>' />
                                                <asp:CompareValidator ID="CompareValcomp" runat="server" Operator="DataTypeCheck"
                                                    Type="double" ControlToValidate="TxtCompfunds" ErrorMessage="Enter Valid Amount" />
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TotAmount">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbTotAmount" Text='<%#Eval("TotAmount")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                        <EditItemTemplate>
                                            <div style="text-align: right">
                                                <asp:TextBox runat="server" ID="TxtTotAmt" Style="text-align: right" Width="80px"
                                                    Text='<%#Eval("TotAmount")%>' />
                                                <asp:CompareValidator ID="CompareValAmt" runat="server" Operator="DataTypeCheck"
                                                    Type="double" ControlToValidate="TxtTotAmt" ErrorMessage="Enter Valid Amount" />
                                            </div>
                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Reconcile">
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="lbReconcile" Text='<%#Eval("Reconcile")%>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>
                            <br />
                            <asp:Button ID="BtnClose" runat="server" Text="Close Table 3" Visible="false" />


                        </div>
                    </td>
                </tr>
            </table>
            <asp:Label ID="lblOrgID" ForeColor="White" runat="server"></asp:Label>
            <asp:Label ID="lblEmployerID" ForeColor="White" runat="server"></asp:Label>
            <asp:Label ID="lblFlag" ForeColor="White" runat="server"></asp:Label>
            <asp:Label ID="lblEmployeeID" ForeColor="White" runat="server"></asp:Label>
            <asp:Label ID="lblGrid" ForeColor="White" runat="server"></asp:Label>
       <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>

</asp:Content>

