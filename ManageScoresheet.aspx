<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ManageScoresheet.aspx.vb" Inherits="ManageScoresheet" Title="Manage Score Sheets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script language="javascript" type="text/javascript">
        function fnOpen(fl) {
            window.open('DownloadScoreSheets.aspx?file=' + fl);
        }
     
        function PopupTemplateList(year) {
            var PopupWindow = null;
            alert(1);
            settings = 'width=600,height=175,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('ShowMasterTempList.aspx?year=' + year, 'Master Template List', settings);
            PopupWindow.focus();
        }
	</script>
    <div align="left">
        <asp:hyperlink id="hlnkMainPage" navigateurl="~/VolunteerFunctions.aspx" cssclass="btn_02" runat="server">Back To Volunteer Functions</asp:hyperlink>
    </div>
    <div align="right">
        <asp:button id="BtnScoreDetExport" runat="server" text="Export Score Detail" visible="false" />
        <asp:button id="btnShowMasterList" runat="server" text="Show Master List" />
    </div>
    <div align="center" style="width: 100%">
        <table cellpadding="3" cellspacing="0" border="0" align="center">
            <tr>
                <td colspan="3" align="center">
                    <asp:label id="lblChapter" visible="false" cssclass="title02" runat="server"></asp:label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:label id="lblLoginRole_Produts" runat="server" text="" forecolor="Brown"></asp:label>
                </td>
            </tr>
            <tr runat="server" id="trAll">
                <td style="width: 291px">
                    <table cellpadding="3" cellspacing="0" border="0" align="center">
                        <tr>
                            <td colspan="2" align="center" class="title04">Download Score Sheet with Contestant Data</td>
                        </tr>
                        <%--<tr><td align="left">Select Product & Chapter </td><td align="left" >
                     <asp:DropDownList ID="ddlContest" DataTextField="text" Width="300px" DataValueField ="contestID" runat="server">
                     </asp:DropDownList>
                     </td></tr>--%>
                        <tr>
                            <td align="left">Year</td>
                            <td align="left" style="width: 77px">
                                <asp:dropdownlist width="150px" id="ddlYear" autopostback="true" runat="server">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Event</td>
                            <td align="left"
                                style="width: 77px">
                                <asp:dropdownlist width="150px" id="ddlContest" autopostback="true" runat="server">
                           <asp:ListItem Value = "1">Finals</asp:ListItem>
                           <asp:ListItem Value = "2" Selected="True">Regionals</asp:ListItem>
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Chapter</td>
                            <td align="left"
                                style="width: 77px">
                                <asp:dropdownlist width="150px" id="ddlChapter" autopostback="true" runat="server" datatextfield="Chaptercode" datavaluefield="ChapterID">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Contest </td>
                            <td align="left" style="width: 77px">
                                <asp:dropdownlist width="149px" enabled="false" autopostback="true" id="ddlProductGroup" datatextfield="Name" datavaluefield="ProductGroupCode" runat="server" enabletheming="True">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Level&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                            <td align="left" style="width: 77px">
                                <asp:dropdownlist width="150px" enabled="false" autopostback="true" id="ddlProduct" datatextfield="Name" datavaluefield="ContestID" runat="server">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr id="TrRoom_phase" runat="server" visible="false">
                            <td align="left" style="height: 8px">Room/Phase</td>
                            <td align="left" style="width: 77px; height: 8px">
                                <asp:dropdownlist id="ddlRoom_Phase" autopostback="True" runat="server" style="margin-left: 0px; height: 22px;" width="150px"></asp:dropdownlist>
                            </td>
                        </tr>
                        <tr runat="server" id="TrPhase" visible="False">
                            <td align="left"
                                style="height: 8px">Type of Data</td>
                            <td align="left"
                                style="width: 77px; height: 8px">
                                <asp:dropdownlist id="ddlPhase" autopostback="True" runat="server" style="margin-left: 0px; height: 22px;"
                                    width="275px">
                           <asp:ListItem Value="1">Phase 1 - Contestant List Only</asp:ListItem>
                           <asp:ListItem Value="2">Phase 2 - Contestant List Only</asp:ListItem>
                           <asp:ListItem Value="3">Phase 2 - Phase 1 Attendance Only</asp:ListItem>
                           <asp:ListItem Value="4">Phase 1 &amp; 2 - Composite Score Data </asp:ListItem>
                           <asp:ListItem Value="10">Phase 1 &amp; 2 � Contestants in Rank Order</asp:ListItem>        
                           <asp:ListItem Value="9" Enabled="false">Phase 3 � List to Publish</asp:ListItem>
                           <asp:ListItem Value="5" Enabled="false">Phase 3 - Score Sheet</asp:ListItem>
                           <asp:ListItem Value="6" Enabled="false">Phase 1,2& 3 - Composite Scores</asp:ListItem>
                           <asp:ListItem Value="7" Enabled="true">List of Top Contestants</asp:ListItem>
                           <asp:ListItem Value="8" Enabled="true">Rank Certificates</asp:ListItem>
                           <%--<asp:ListItem Value="10" Enabled ="false">Summary Score Data</asp:ListItem>--%>
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr runat="server" id="TrRoom" visible="false ">
                            <td align="left"
                                style="height: 8px">Room Number</td>
                            <td align="left"
                                style="width: 77px; height: 8px">
                                <asp:dropdownlist width="150px" autopostback="false" id="ddlRoom" runat="server">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr runat="server" id="TrTopRank" visible="false">
                            <td align="left" style="height: 8px">Top Ranks</td>
                            <td align="left" style="width: 77px; height: 8px">
                                <asp:dropdownlist width="150px" id="ddlTopRank" runat="server" autopostback="True">
                           <asp:ListItem Value="0"  Enabled="false">No Ranks</asp:ListItem>
                           <asp:ListItem Value="1">Top 1</asp:ListItem>
                           <asp:ListItem Value="2">Top 2</asp:ListItem>
                           <asp:ListItem Value="3">Top 3</asp:ListItem>
                           <asp:ListItem Value="4">Top 4</asp:ListItem>
                           <asp:ListItem Value="5">Top 5</asp:ListItem>
                           <asp:ListItem Value="6">Top 6</asp:ListItem>
                           <asp:ListItem Value="7">Top 7</asp:ListItem>
                           <asp:ListItem Value="8">Top 8</asp:ListItem>
                           <asp:ListItem Value="9">Top 9</asp:ListItem>
                           <asp:ListItem Value="10">Top 10</asp:ListItem>
                           <asp:ListItem Value="11">Top 11</asp:ListItem>
                           <asp:ListItem Value="12">Top 12</asp:ListItem>
                           <asp:ListItem Value="13">Top 13</asp:ListItem>
                           <asp:ListItem Value="14">Top 14</asp:ListItem>
                           <asp:ListItem Value="15">Top 15</asp:ListItem>
                           <asp:ListItem Value="16">Top 16</asp:ListItem>
                           <asp:ListItem Value="17">Top 17</asp:ListItem>
                           <asp:ListItem Value="18">Top 18</asp:ListItem>
                           <asp:ListItem Value="19">Top 19</asp:ListItem>
                           <asp:ListItem Value="20">Top 20</asp:ListItem>
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr id="TrConfirmDwnload" runat="server" visible="false">
                            <td class="announcement_text" colspan="2">
                                <br />
                                <center>
                                    <asp:label id="lblConfirmDwnload" runat="server" align="center" forecolor="Red"></asp:label>
                                    <br />
                                    <asp:button id="BtnConfirmDwnload" onclick="BtnConfirmDwnload_Click" runat="server" text="Confirm" />
                                    &nbsp
                           <asp:button id="BtnCancelDwnload" runat="server" onclick="BtnCancelDwnload_Click" text="Cancel" />
                                    <br />
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:label id="lblUploadCondn" runat="server" forecolor="Red"></asp:label>
                                <br />
                                <br />
                                <asp:button id="BtnDownload" runat="server"
                                    text="Download" height="25px" style="margin-bottom: 0px" />
                                <br />
                                <br />
                                <asp:label id="lbldwError" runat="server" forecolor="Red"></asp:label>
                                <br />
                                <asp:label id="lblDownLdError" runat="server" forecolor="Red"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:label id="lblPrdError" runat="server" forecolor="Red"></asp:label>
                            </td>
                        </tr>
                        <tr runat="server" id="trupload" visible="false">
                            <td align="center" colspan="2"><b>Upload Filled Scoresheet</b></td>
                        </tr>
                        <tr runat="server" id="TrTypeofData1" visible="False">
                            <td align="left" style="height: 8px">Type of Data</td>
                            <td align="left" style="width: 77px; height: 8px">
                                <asp:dropdownlist id="ddlTypeofData1" autopostback="True" runat="server" style="margin-left: 0px"
                                    width="250px">
                           <asp:ListItem Value="0">Phase 1 - Attendance Only</asp:ListItem>
                           <asp:ListItem Value="1">Phase 1 - Score Data</asp:ListItem>
                           <asp:ListItem Value="2">Phase 2 - Score Data</asp:ListItem>
                           <asp:ListItem Value="5" Enabled="false">Ranks after Phase 1&amp;2</asp:ListItem>
                           <asp:ListItem Value="6" Enabled="false">Phase 3 Score Data</asp:ListItem>
                           <asp:ListItem Value="3">Ranks for Certificates</asp:ListItem>
                           <asp:ListItem Value="4">Official Scores &amp; Ranks</asp:ListItem>
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr runat="server" id="TrRoom1" visible="false">
                            <td align="left" style="height: 8px">Room Number</td>
                            <td align="left"
                                style="width: 77px; height: 8px">
                                <asp:dropdownlist width="150px" autopostback="false" id="ddlRoom1" runat="server">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" style="height: 58px">
                                <asp:fileupload id="FileUpload" visible="false" style="margin-left: 75px;" width="250px" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:button id="btnUpload" visible="false" runat="server" text="Upload" height="26px" width="60px" />
                                <br />
                                <asp:label forecolor="Red" id="lblErr" runat="server" text=""></asp:label>
                                <br />
                                <asp:hiddenfield
                                    id="HdnexecQuery" runat="server" />
                                <asp:hiddenfield id="hdnChapterID" runat="server" />
                                <asp:hiddenfield id="HdnScoreDetailSQL" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="25px">&nbsp;&nbsp;</td>
                <td>
                    <table runat="server" visible="false" id="tblMaster" cellpadding="3" cellspacing="0" border="0" align="center">
                        <tr>
                            <td colspan="2" align="center" class="title04">Master Scoresheets Templates<br />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td align="left">Year</td>
                            <td align="left">
                                <asp:dropdownlist width="150px" id="ddlMYear" autopostback="True" runat="server">
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <%--<tr><td align="left">Chapter</td><td align="left">
                     <asp:DropDownList Width="150px" ID="ddlMChapter"  runat="server"  DataTextField="Chaptercode"  DataValueField="ChapterID">
                     </asp:DropDownList></td><td align="left"></td></tr>--%>
                        <tr>
                            <td align="left">Event</td>
                            <td align="left">
                                <asp:dropdownlist width="150px" id="ddlMContest" autopostback="true" runat="server">
                           <asp:ListItem Value="1">Finals</asp:ListItem>
                           <asp:ListItem Value ="2" Selected="True">Regionals</asp:ListItem>
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Contest </td>
                            <td align="left">
                                <asp:dropdownlist width="150px" enabled="false" id="ddlMProductGroup" autopostback="true" datatextfield="Name" datavaluefield="ProductGroupCode" runat="server"></asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Level </td>
                            <td align="left">
                                <asp:dropdownlist width="150px" enabled="false" autopostback="True" id="ddlMProduct" datatextfield="Name" datavaluefield="ProductCode" runat="server"></asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Score Sheet Type</td>
                            <td align="left">
                                <asp:dropdownlist width="150px" autopostback="True" enabled="false" id="ddlMPhase" runat="server">
                           <asp:ListItem Selected="True" Value="0">Global</asp:ListItem>
                           <asp:ListItem Value="2">Phase 2</asp:ListItem>
                        </asp:dropdownlist>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:button id="BtnMDownload"
                                    onclick="BtnMDownload_Click" runat="server" text="Download" height="26px" />
                                <br />
                                <asp:label forecolor="Red" id="lblMdwError" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"><b>Upload Master Scoresheet</b></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:fileupload id="FileUploadMaster" width="200px" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:button id="btnUploadMaster_" onclick="btnUploadMaster_Click"
                                    runat="server" text="Upload" style="height: 26px" />
                                <br />
                                <asp:label forecolor="Red" id="LblMasterErr" runat="server"></asp:label>
                                <asp:hiddenfield id="ExamRecNational" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="TrPhase2Upload" runat="server" align="center" visible="false">
                <td class="announcement_text" align="left" colspan="3">
                    <br />
                    <center><span class="title04">It appears that the score sheet for the room selected was not downloaded.  Do you still want to upload?  Please click  Yes or No</span></center>
                    <br />
                    <center>
                        <asp:button id="BtnPhaseUpYes" runat="Server" text="Yes" />
                        &nbsp;&nbsp;
                  <asp:button id="BtnPhaseUpNo" runat="Server" text="No" />
                    </center>
                </td>
            </tr>
            <tr id="TrUpCheck" runat="server" align="center" visible="false">
                <td class="announcement_text" align="left" colspan="3">
                    <br />
                    <center><span class="title04">Blank Score Sheet with contestant list was not downloaded.  Do you still want to upload?  Please click  Yes or No</span></center>
                    <br />
                    <asp:label id="Label1" runat="server" align="center"></asp:label>
                    <center>
                        <asp:button id="BtnYes" runat="Server" text="Yes" />
                        &nbsp;&nbsp;
                  <asp:button id="BtnNo" runat="Server" text="No" />
                    </center>
                </td>
            </tr>
            <tr runat="server" id="trconfirm" visible="false">
                <td class="announcement_text" align="left" colspan="3">
                    <br />
                    <center><span class="title04">Upload Score Sheet with Contestant Data</span></center>
                    <br />
                    <asp:label id="lblConfirm" runat="server" align="center"></asp:label>
                    <br />
                    <br />
                    <center>
                        <asp:button id="BtnConfirm" onclick="BtnConfirm_Click" runat="server" text="Confirm" enabled="True" />
                        &nbsp;&nbsp;
                  <asp:button id="BtnCancel" runat="server" onclick="BtnCancel_Click" text="Cancel" />
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:label id="lblWarngMsg" runat="server" forecolor="#FF6600"></asp:label>
                </td>
            </tr>
        </table>
    </div>
    <%-- <div >
      <center>
          <asp:DataGrid  id="DGTopList" runat="server"  >
          <COLUMNS>
              <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Rank_Alpha"  HeaderText="Rank#" />
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BadgeNumber"  HeaderText="BadgeNumber"  />
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ParticipantName"  HeaderText="Name" />
          </COLUMNS></asp:DataGrid >
      </center>
      </div> --%>

    <input type="hidden" id="hdnP12ScoreFlag" runat="server" value="0" />
    <input type="hidden" id="hdnP1AttendanceFlag" runat="server" value="0" />
    <input type="hidden" id="hdnP1ScoreFlag" runat="server" value="0" />
</asp:Content>

