﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class RoomGuideSchedule
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            If Convert.ToInt32(Session("selChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("selChapterID")
                ddlChapter.SelectedItem.Text = Session("selChapterName").ToString()
                ddlChapter.Enabled = False
                If (ddlChapter.SelectedValue = "1") Then
                    ddlEvent.SelectedValue = "1"
                Else
                    ddlEvent.SelectedValue = "2"
                End If
                ddlEvent.Enabled = False
                'GetProductGroup(ddlProductGroup)
                GetDate()
                ' LoadBldgID()
            End If
        End If
    End Sub
    Private Sub GetContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        ddlPurpose.DataBind()
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 3
    End Sub

    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup(ByVal ddlObject As DropDownList)
        lblErr.Text = ""
        Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()

            ddlObject.Items.Insert(0, "Select ProductGroup")
            ddlObject.SelectedIndex = 0
        Else
            lblErr.Text = "No ProductGroups Present"
        End If

 
    End Sub
    Private Sub GetDate()
        Dim StrDateSQL As String = ""
        StrDateSQL = "Select Distinct convert(VarChar(10), ContestDate, 101) as Date,ContestDate  From Contest Where Contest_Year=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and nsfChapterID=" & ddlChapter.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDateSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlDate.DataSource = ds
            ddlDate.DataBind()

            ddlDate.Items.Insert(0, "Select ContestDate")
            ddlDate.SelectedIndex = 0
        Else
            lblErr.Text = "No Contest Dates Present for the Selections "
        End If
   
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        TrCopy.Visible = False
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            'GetProductGroup(ddlProductGroup)
            'GetDate()
            ddlPurpose.SelectedIndex = 3
            ClearDropDowns()
            LoadRolesVol()
            'LoadBldgID()
        Else
            GetChapter(ddlChapter)
        End If
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        TrCopy.Visible = False
        If ddlChapter.SelectedValue = 1 Then
            ddlEvent.SelectedValue = 1
        End If
        'GetProductGroup(ddlProductGroup)
        GetDate()
        ddlPurpose.SelectedIndex = 3
        ClearDropDowns()
        LoadRolesVol()
        'LoadBldgID()
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        ClearDropDowns()
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event/Chapter"
            Exit Sub
        End If
        LoadRoomSchData()
    End Sub
    Private Sub LoadRoomSchData()

        Dim SQLRoomsch As String = "Select * from Roomschedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterId=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomsch)
        Try
            GetDate()
            'GetProductGroup(ddlProductGroup) '
            'LoadBldgID()
            lblAddUPdate.Text = ""
            lblErr.Text = ""
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged

        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        LoadProduct(ddlProductGroup, ddlProduct)
        ' LoadPhase()

    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        LoadPhase()
    End Sub
    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ddlProductObj As DropDownList)

        Dim StrSQL As String = "Select Distinct ProductCode,ProductId from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlObject.SelectedValue & " order by ProductId" 'and BldgId='" & ddlBldgID.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlProductObj.DataSource = ds
        'ddlProductObj.DataTextField = "ProductCode"
        'ddlProductObj.DataValueField = "ProductId"
        ddlProductObj.DataBind()

        ddlProductObj.Items.Insert(0, New ListItem("Select Product ", "0"))
        ddlProductObj.SelectedIndex = 0

    End Sub

    Private Sub LoadPhase()

        Dim StrSQL As String = "Select Distinct Phase from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlPhase.DataSource = ds
        'ddlPhase.DataTextField = "Phase"
        'ddlPhase.DataValueField = "Phase"
        ddlPhase.DataBind()

        ddlPhase.Items.Insert(0, New ListItem("Select Phase ", "0"))
        ddlPhase.SelectedIndex = 0

        LoadDGRoomSchedule()

    End Sub

    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        TrCopy.Visible = False
        ddlRoomNo.Enabled = True
        ddlRoomNo.Items.Clear()
        LoadRoomNumber(ddlBldgID.SelectedValue)
        ClearRoles()
        LoadRolesVol()
        'LoadRoleData()
    End Sub
    Private Sub LoadRoomNumber(ByVal ddlBldgIdVal As String)

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo,RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlSeqNo.DataSource = ds
        ddlSeqNo.DataBind()

        ddlSeqNo.Items.Insert(0, "Select SeqNo")
        ddlSeqNo.SelectedIndex = 0

        ddlRoomNo.DataSource = ds
        ddlRoomNo.DataBind()

        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
        ddlRoomNo.SelectedIndex = 0

        'GetProductGroup()
        'GetDate()
    End Sub

    Protected Sub ddlSeqNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSeqNo.SelectedIndexChanged

        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False

        Dim StrSeqNoSQl As String = "Select Distinct RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and SeqNo='" & ddlSeqNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSeqNoSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber")))
            'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
            'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
        Else
            'ddlRoomNo.Enabled = False
            ddlRoomNo.SelectedIndex = 0
            'ddlStartTime.SelectedIndex = 0
            'ddlEndTime.SelectedIndex = 0
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule for the Selected Contest date"
        End If
        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged

        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo")))
            'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
            'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
        Else
            ddlSeqNo.SelectedIndex = 0
            'ddlStartTime.SelectedIndex = 0
            'ddlEndTime.SelectedIndex = 0
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule for the Selected Contest date"
        End If
        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub LoadBldgID()
        ddlRoomNo.Enabled = True

        Dim StrBldgSQlEval As String = ""
        Dim StrBldgSQl As String = ""
        lblErr.Text = ""
        StrBldgSQl = "Select Distinct BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedValue & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)

        ' ddlRoomNo.Items.Clear()
        ' ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlBldgID.DataSource = ds
            ddlBldgID.DataTextField = "BldgID"
            ddlBldgID.DataValueField = "BldgID"
            ddlBldgID.DataBind()
        Else
            lblErr.Text = "No Buildings assigned"
        End If
        ddlBldgID.Items.Insert(0, "Select BldgID")
        ddlBldgID.SelectedIndex = 0
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Or ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please do all the Selections"
            Exit Sub
        End If
        LoadBldgID()
        LoadTimeData()
        CheckRoomReqData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub CheckRoomReqData()
        'Dim RoomGStart As Integer = 0
        lblErr.Text = ""
        Dim RoomGCount As Integer = 10
        For i As Integer = 0 To RoomGCount - 1
            TableRoomGuide.Rows(i).Visible = True
        Next
        'TrRoomGuide_1.Visible = True

        Dim SQLRoomReq As String = "Select IsNull(RoomGuide,0) as RoomGuide From RoomRequirement Where EventYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
        If ds.Tables(0).Rows.Count > 0 Then
            For j As Integer = ds.Tables(0).Rows(0)("RoomGuide") To RoomGCount - 1
                TableRoomGuide.Rows(j).Visible = False
            Next
            If ds.Tables(0).Rows(0)("RoomGuide") = 0 Then
                lblErr.Text = "Room Guides were not Specified for Phase " & ddlPhase.SelectedValue & " in Room Requirement"
            End If
        Else
            lblErr.Text = "Room Requirement Data was not added for the Contest"
        End If
    End Sub
    Private Sub LoadTimeData()
        Dim SQLContestTime As String = "Select * From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "" ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLContestTime)

        'ddlStartTime.DataSource = ds
        'ddlStartTime.DataBind()

        'ddlEndTime.DataSource = ds
        'ddlEndTime.DataBind()
    End Sub

    Private Sub LoadRoleData()
        Dim SQLRoomGuideTeam As String = ""
        Dim ds As DataSet
        Try
            SQLRoomGuideTeam = "Select * From RoomGuideSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomGuideTeam)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlRoomG_1.SelectedIndex = ddlRoomG_1.Items.IndexOf(ddlRoomG_1.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_1") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_1"))))
                ddlRoomG_2.SelectedIndex = ddlRoomG_2.Items.IndexOf(ddlRoomG_2.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_2") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_2"))))
                ddlRoomG_3.SelectedIndex = ddlRoomG_3.Items.IndexOf(ddlRoomG_3.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_3") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_3"))))
                ddlRoomG_4.SelectedIndex = ddlRoomG_4.Items.IndexOf(ddlRoomG_4.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_4") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_4"))))
                ddlRoomG_5.SelectedIndex = ddlRoomG_5.Items.IndexOf(ddlRoomG_5.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_5") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_5"))))
                ddlRoomG_6.SelectedIndex = ddlRoomG_6.Items.IndexOf(ddlRoomG_6.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_6") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_6"))))
                ddlRoomG_7.SelectedIndex = ddlRoomG_7.Items.IndexOf(ddlRoomG_7.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_7") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_7"))))
                ddlRoomG_8.SelectedIndex = ddlRoomG_8.Items.IndexOf(ddlRoomG_8.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_8") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_8"))))
                ddlRoomG_9.SelectedIndex = ddlRoomG_9.Items.IndexOf(ddlRoomG_8.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_9") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_9"))))
                ddlRoomG_10.SelectedIndex = ddlRoomG_10.Items.IndexOf(ddlRoomG_8.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_10") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_10"))))

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
   

    Private Sub LoadRolesVol()

        Dim SQLRoleStr As String = ""
        If ddlEvent.SelectedValue = 1 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Finals='Y' and RoleID=23)"
        ElseIf ddlEvent.SelectedValue = 2 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID > 1 and ChapterID=" & ddlChapter.SelectedValue & " and RoleID=23)"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleStr)

        If ds.Tables(0).Rows.Count > 0 Then

            ddlRoomG_1.DataSource = ds
            ddlRoomG_1.DataBind()
            ddlRoomG_1.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_2.DataSource = ds
            ddlRoomG_2.DataBind()
            ddlRoomG_2.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_3.DataSource = ds
            ddlRoomG_3.DataBind()
            ddlRoomG_3.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_4.DataSource = ds
            ddlRoomG_4.DataBind()
            ddlRoomG_4.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_5.DataSource = ds
            ddlRoomG_5.DataBind()
            ddlRoomG_5.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_6.DataSource = ds
            ddlRoomG_6.DataBind()
            ddlRoomG_6.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_7.DataSource = ds
            ddlRoomG_7.DataBind()
            ddlRoomG_7.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_8.DataSource = ds
            ddlRoomG_8.DataBind()
            ddlRoomG_8.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_9.DataSource = ds
            ddlRoomG_9.DataBind()
            ddlRoomG_9.Items.Insert(0, New ListItem("Select", 0))

            ddlRoomG_10.DataSource = ds
            ddlRoomG_10.DataBind()
            ddlRoomG_10.Items.Insert(0, New ListItem("Select", 0))

            'LoadRoleNames(ddlRoomG_1, "RoleID = 23")
        End If
    End Sub
    Private Sub LoadRoleNames(ByVal ddlObject As DropDownList, ByVal Role As String)

        Dim SQLRoleStr As String = ""
        If ddlEvent.SelectedValue = 1 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Finals='Y' and " & Role & " )"
        ElseIf ddlEvent.SelectedValue = 2 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID > 1 and ChapterID=" & ddlChapter.SelectedValue & " and " & Role & " )"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleStr)
        ddlObject.DataSource = ds
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, New ListItem("Select", 0))

    End Sub
    Private Sub ClearRoles()
        ddlRoomG_1.Items.Clear()
        ddlRoomG_2.Items.Clear()
        ddlRoomG_3.Items.Clear()
        ddlRoomG_4.Items.Clear()
        ddlRoomG_5.Items.Clear()
        ddlRoomG_6.Items.Clear()
        ddlRoomG_7.Items.Clear()
        ddlRoomG_8.Items.Clear()
        ddlRoomG_9.Items.Clear()
        ddlRoomG_10.Items.Clear()
        'LoadRolesVol()
        ' LoadRoleData()
    End Sub
    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click

        Dim SQLRoomGuideInsert As String = ""
        Dim SQLRoomGuideUpdate As String = ""
        Dim SQLRoomGuideEval As String = ""
        Dim SQLRoomGuideExec As String = ""

        lblAddUPdate.Text = ""
        lblErr.Text = ""

        If ddlEvent.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Purpose"
            Exit Sub
        ElseIf ddlDate.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Date"
            Exit Sub
        ElseIf ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select ProductGroup"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Phase"
            Exit Sub
            'ElseIf ddlStartTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select StartTime"
            '    Exit Sub
            'ElseIf ddlEndTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select EndTime"
            '    Exit Sub
        ElseIf ddlBldgID.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select BldgID"
            Exit Sub
        ElseIf ddlRoomNo.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select RoomNo"
            Exit Sub
        End If
        Try
            SQLRoomGuideInsert = "Insert into RoomGuideSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,"
            SQLRoomGuideInsert = SQLRoomGuideInsert & "RGMemberID_1,RGMemberID_2,RGMemberID_3,RGMemberID_4,RGMemberID_5,RGMemberID_6,RGMemberID_7,RGMemberID_8,RGMemberID_9,RGMemberID_10,CreatedBy,CreatedDate)"
            SQLRoomGuideInsert = SQLRoomGuideInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ddlBldgID.SelectedItem.Text & "'," & ddlSeqNo.SelectedValue & ",'" & ddlRoomNo.SelectedItem.Text & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ","
            SQLRoomGuideInsert = SQLRoomGuideInsert & IIf(ddlRoomG_1.SelectedValue = 0, "NULL", ddlRoomG_1.SelectedValue) & "," & IIf(ddlRoomG_2.SelectedValue = 0, "NULL", ddlRoomG_2.SelectedValue) & "," & IIf(ddlRoomG_3.SelectedValue = 0, "NULL", ddlRoomG_3.SelectedValue) & "," & IIf(ddlRoomG_4.SelectedValue = 0, "NULL", ddlRoomG_4.SelectedValue) & ","
            SQLRoomGuideInsert = SQLRoomGuideInsert & IIf(ddlRoomG_5.SelectedValue = 0, "NULL", ddlRoomG_5.SelectedValue) & "," & IIf(ddlRoomG_6.SelectedValue = 0, "NULL", ddlRoomG_6.SelectedValue) & "," & IIf(ddlRoomG_7.SelectedValue = 0, "NULL", ddlRoomG_7.SelectedValue) & "," & IIf(ddlRoomG_8.SelectedValue = 0, "NULL", ddlRoomG_8.SelectedValue) & "," & IIf(ddlRoomG_9.SelectedValue = 0, "NULL", ddlRoomG_9.SelectedValue) & "," & IIf(ddlRoomG_10.SelectedValue = 0, "NULL", ddlRoomG_10.SelectedValue) & "," & Session("LoginID") & ",GetDate() )"

            SQLRoomGuideUpdate = "Update RoomGuideSchedule Set Date='" & ddlDate.SelectedItem.Text & "' ,RGMemberID_1 = " & IIf(ddlRoomG_1.SelectedValue = 0, "NULL", ddlRoomG_1.SelectedValue) & ",RGMemberID_2=" & IIf(ddlRoomG_2.SelectedValue = 0, "NULL", ddlRoomG_2.SelectedValue) & ",RGMemberID_3=" & IIf(ddlRoomG_3.SelectedValue = 0, "NULL", ddlRoomG_3.SelectedValue) & ",RGMemberID_4=" & IIf(ddlRoomG_4.SelectedValue = 0, "NULL", ddlRoomG_4.SelectedValue) & ","
            SQLRoomGuideUpdate = SQLRoomGuideUpdate & " RGMemberID_5=" & IIf(ddlRoomG_5.SelectedValue = 0, "NULL", ddlRoomG_5.SelectedValue) & ",RGMemberID_6=" & IIf(ddlRoomG_6.SelectedValue = 0, "NULL", ddlRoomG_6.SelectedValue) & ",RGMemberID_7=" & IIf(ddlRoomG_7.SelectedValue = 0, "NULL", ddlRoomG_7.SelectedValue) & ",RGMemberID_8=" & IIf(ddlRoomG_8.SelectedValue = 0, "NULL", ddlRoomG_8.SelectedValue) & ",RGMemberID_9=" & IIf(ddlRoomG_9.SelectedValue = 0, "NULL", ddlRoomG_9.SelectedValue) & ",RGMemberID_10=" & IIf(ddlRoomG_10.SelectedValue = 0, "NULL", ddlRoomG_10.SelectedValue) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
            SQLRoomGuideUpdate = SQLRoomGuideUpdate & "Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and SeqNo =" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text.Trim() & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            SQLRoomGuideEval = "Select Count(*) From RoomGuideSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Value & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""




            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLRoomGuideEval) = 0 Then
                'SQLRoomGuideExec = SQLRoomGuideInsert
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLRoomGuideInsert)
                lblAddUPdate.Text = "Added Successfully"
            Else
                'SQLRoomGuideExec = SQLRoomGuideUpdate
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLRoomGuideUpdate)
                lblAddUPdate.Text = "Updated Successfully"
            End If
            LoadDGRoomSchedule()
            ' btnAddUpdate.Text = "Add"
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        'ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ddlPurpose.SelectedIndex = 3
        ddlDate.SelectedIndex = 0
        ClearDropDowns()
        ClearRoles()
        GetDate()
        GetProductGroup(ddlProductGroup)
    End Sub
    Private Sub ClearDropDowns()
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        DGRoomSchedule.Visible = False
        ddlProductGroup.Items.Clear() '.SelectedIndex = 0 '
        ddlProduct.Items.Clear()
        ddlPhase.Items.Clear()
        ' ddlDate.SelectedIndex = 0 '.Items.Clear()
        ddlBldgID.Items.Clear()
        ddlSeqNo.Items.Clear()
        ddlRoomNo.Items.Clear()
        'ddlStartTime.Items.Clear()
        'ddlEndTime.Items.Clear()
        Dim RoomGCount As Integer = 10
        For i As Integer = 0 To RoomGCount - 1
            TableRoomGuide.Rows(i).Visible = True
        Next
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        'ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ClearDropDowns()
        ClearRoles()
        GetDate()
    End Sub
    Protected Sub BtnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopy.Click
        TrCopy.Visible = True
        GetProductGroup(ddlToPG)
    End Sub

    Protected Sub ddlToPG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToPG.SelectedIndexChanged
        LoadProduct(ddlToPG, ddlToProduct)
    End Sub

    Protected Sub ddlToProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToProduct.SelectedIndexChanged
        lblAddUPdate.Text = ""
        Dim StrRoomNoSQl As String = "Select * from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "" ' and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim dsTime As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)
        If dsTime.Tables(0).Rows.Count > 0 Then
            'ddlStartCpTime.DataSource = dsTime
            'ddlStartCpTime.DataBind()
            'ddlEndCpTime.DataSource = dsTime
            'ddlEndCpTime.DataBind()
        Else
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule to copy data"
        End If
    End Sub
    Private Sub LoadDGRoomSchedule()
        DGRoomSchedule.Visible = True
        Dim dgItem As DataGridItem
        Dim lblCapcty As Label
        Dim lbtnRemoveCont As LinkButton
        Dim lbtnEditCont As LinkButton
        Dim lblRoomGd_1 As Label
        Dim lblRoomGd_2 As Label
        Dim lblRoomGd_3 As Label
        Dim lblRoomGd_4 As Label
        Dim lblRoomGd_5 As Label
        Dim lblRoomGd_6 As Label
        Dim lblRoomGd_7 As Label
        Dim lblRoomGd_8 As Label
        Dim lblRoomGd_9 As Label
        Dim lblRoomGd_10 As Label
        Dim lbl_RoomGuideID As Label
        Dim i As Integer = 0
        Dim StrRoomSch As String = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomSch)

        DGRoomSchedule.DataSource = ds
        DGRoomSchedule.DataBind()
        Dim ds1, ds2 As DataSet
        Dim RoomGStart As Integer
        Dim RoomGEnd As Integer
        Try
            For Each dgItem In DGRoomSchedule.Items
                RoomGStart = 17
                RoomGEnd = 24

                lblCapcty = dgItem.FindControl("lblCapacity")
                ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Capacity from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'")
                lblCapcty.Text = ds1.Tables(0).Rows(0)("Capacity")

                lbtnRemoveCont = dgItem.FindControl("lbtnRemove")
                lbtnEditCont = dgItem.FindControl("lbtnEdit")

                lblRoomGd_1 = dgItem.FindControl("lblRoomG_1")
                lblRoomGd_2 = dgItem.FindControl("lblRoomG_2")
                lblRoomGd_3 = dgItem.FindControl("lblRoomG_3")
                lblRoomGd_4 = dgItem.FindControl("lblRoomG_4")
                lblRoomGd_5 = dgItem.FindControl("lblRoomG_5")
                lblRoomGd_6 = dgItem.FindControl("lblRoomG_6")
                lblRoomGd_7 = dgItem.FindControl("lblRoomG_7")
                lblRoomGd_8 = dgItem.FindControl("lblRoomG_8")
                lblRoomGd_9 = dgItem.FindControl("lblRoomG_9")
                lblRoomGd_10 = dgItem.FindControl("lblRoomG_10")
                lbl_RoomGuideID = dgItem.FindControl("lblRoomGuideID")

                For j As Integer = RoomGStart To RoomGEnd
                    DGRoomSchedule.Columns(j).Visible = True   'RoomGuides 
                Next

                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomGuideSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") > 0 Then
                    ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomGuideSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'") ' " & IIf(ddlBldgID.SelectedIndex = 0, "", "  ''''' ") & IIf(ddlRoomNo.SelectedIndex <= 0, "", "
                     If ds2.Tables(0).Rows.Count > 0 Then
                        ' For j As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_1") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_1")), lblRoomGd_1)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_2") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_2")), lblRoomGd_2)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_3") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_3")), lblRoomGd_3)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_4") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_4")), lblRoomGd_4)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_5") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_5")), lblRoomGd_5)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_6") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_6")), lblRoomGd_6)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_7") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_7")), lblRoomGd_7)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_8") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_8")), lblRoomGd_8)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_9") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_9")), lblRoomGd_9)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RGMemberID_10") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RGMemberID_10")), lblRoomGd_10)
                        lbl_RoomGuideID.Text = ds2.Tables(0).Rows(0)("RoomGuideID").ToString
                    Else
                        lblRoomGd_1.Text = ""
                        lblRoomGd_2.Text = ""
                        lblRoomGd_3.Text = ""
                        lblRoomGd_4.Text = ""
                        lblRoomGd_5.Text = ""
                        lblRoomGd_6.Text = ""
                        lblRoomGd_7.Text = ""
                        lblRoomGd_8.Text = ""
                        lblRoomGd_9.Text = ""
                        lblRoomGd_10.Text = ""
                        lbl_RoomGuideID.Text = ""
                    End If
                Else
                    lblRoomGd_1.Text = ""
                    lblRoomGd_2.Text = ""
                    lblRoomGd_3.Text = ""
                    lblRoomGd_4.Text = ""
                    lblRoomGd_5.Text = ""
                    lblRoomGd_6.Text = ""
                    lblRoomGd_7.Text = ""
                    lblRoomGd_8.Text = ""
                    lblRoomGd_9.Text = ""
                    lblRoomGd_10.Text = ""
                    lbl_RoomGuideID.Text = ""
                End If
                Dim SQLRoomReq As String = "Select IsNull(RoomGuide,0) as RoomGuide From RoomRequirement Where EventYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & " "
                Dim dsRoomReq As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
                If dsRoomReq.Tables(0).Rows.Count > 0 Then
                    RoomGStart = RoomGStart + dsRoomReq.Tables(0).Rows(0)("RoomGuide")
                    For k As Integer = RoomGStart To RoomGEnd
                        DGRoomSchedule.Columns(k).Visible = False
                    Next
                End If

                If (lbl_RoomGuideID.Text <> "") Then
                    lbtnRemoveCont.Enabled = True
                    lbtnEditCont.Enabled = True
                Else
                    lbtnRemoveCont.Enabled = False
                    lbtnEditCont.Enabled = False
                End If
                i = i + 1
            Next
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub DisplayName(ByVal MemberID As Integer, ByVal lblRole As Label)
        If MemberID <> 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select (Title + '.' +FirstName +''+ LastName ) as Name  From IndSpouse Where AutoMemberID =" & MemberID)
            lblRole.Text = ds.Tables(0).Rows(0)("Name")
        End If
    End Sub

    Protected Sub DGRoomSchedule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lblRoomGuideID As Label
        lblRoomGuideID = e.Item.FindControl("lblRoomGuideID")
        Dim RoomGuideID As Integer = lblRoomGuideID.Text ' CInt(e.Item.Cells(25).Text)

        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from RoomGuideSchedule Where RoomGuideID=" & RoomGuideID & "")
                LoadDGRoomSchedule()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            'lblRoomSchID.Text = ContTeamID.ToString()
            LoadForUpdate(RoomGuideID)
        End If
    End Sub
    Private Sub LoadForUpdate(ByVal RoomGuideID As Integer)

        ' btnAddUpdate.Text = "Update"
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from RoomGuideSchedule Where RoomGuideID=" & RoomGuideID & "")
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo")  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim BldgID As String = ""
        Try
            If ds.Tables(0).Rows.Count > 0 Then

                'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
                'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))

                ddlBldgID.SelectedIndex = ddlBldgID.Items.IndexOf(ddlBldgID.Items.FindByValue(ds.Tables(0).Rows(0)("BldgID")))
                ddlRoomNo.Items.Clear()
                LoadRoomNumber(ds.Tables(0).Rows(0)("BldgID").ToString.Trim)
                ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo").ToString.Trim()))
                ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber").ToString))

                ddlRoomG_1.SelectedIndex = ddlRoomG_1.Items.IndexOf(ddlRoomG_1.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_1") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_1"))))
                ddlRoomG_2.SelectedIndex = ddlRoomG_2.Items.IndexOf(ddlRoomG_2.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_2") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_2"))))
                ddlRoomG_3.SelectedIndex = ddlRoomG_3.Items.IndexOf(ddlRoomG_3.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_3") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_3"))))
                ddlRoomG_4.SelectedIndex = ddlRoomG_4.Items.IndexOf(ddlRoomG_4.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_4") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_4"))))
                ddlRoomG_5.SelectedIndex = ddlRoomG_5.Items.IndexOf(ddlRoomG_5.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_5") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_5"))))
                ddlRoomG_6.SelectedIndex = ddlRoomG_6.Items.IndexOf(ddlRoomG_6.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_6") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_6"))))
                ddlRoomG_7.SelectedIndex = ddlRoomG_7.Items.IndexOf(ddlRoomG_7.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_7") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_7"))))
                ddlRoomG_8.SelectedIndex = ddlRoomG_8.Items.IndexOf(ddlRoomG_8.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_8") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_8"))))
                ddlRoomG_9.SelectedIndex = ddlRoomG_9.Items.IndexOf(ddlRoomG_9.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_9") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_9"))))
                ddlRoomG_10.SelectedIndex = ddlRoomG_10.Items.IndexOf(ddlRoomG_10.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RGMemberID_10") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RGMemberID_10"))))

            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCopySchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopySchedule.Click
        Dim SQLContTeamCpInsert As String = ""
        Dim SQLContTeamCpUpdate As String = ""
        Dim SQLContTeamCpEval As String = ""
        Dim SQLContTeamCpExec As String = ""
        Dim StrFromContest As String = ""
        Dim RoomGuideID As String = ""
        Dim i, count As Integer
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        If ddlToPG.SelectedIndex = 0 Then
            lblErr.Text = "Please Select Copy Contest"
            Exit Sub
        End If
        Try
            StrFromContest = "Select * From RoomGuideSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrFromContest)

            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "Insert into RoomGuideSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "RGMemberID_1,RGMemberID_2,RGMemberID_3,RGMemberID_4,RGMemberID_5,RGMemberID_6,RGMemberID_7,RGMemberID_8,RGMemberID_9,RGMemberID_10,CreatedBy,CreatedDate)"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ds.Tables(0).Rows(i)("BldgID") & "','" & ds.Tables(0).Rows(i)("SeqNo") & "','" & ds.Tables(0).Rows(i)("RoomNumber") & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlToPG.SelectedValue & ",'" & ddlToPG.SelectedItem.Text & "'," & ddlToProduct.SelectedValue & ",'" & ddlToProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ","
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("RGMemberID_1") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_1")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_2") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_2")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_3") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_3")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_4") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_4")) & ","
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("RGMemberID_5") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_5")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_6") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_6")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_7") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_7")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_8") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_8")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_9") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_9")) & "," & IIf(ds.Tables(0).Rows(i)("RGMemberID_10") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RGMemberID_10")) & "," & Session("LoginID") & ",GetDate() )"

                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & "Update ContestTeamSchedule Set Date='" & ddlDate.SelectedItem.Text & "', BldgID='" & ds.Tables(0).Rows(i)("BldgID") & "',SeqNo=" & ds.Tables(0).Rows(i)("SeqNo") & ", RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "',StartTime='" & ddlStartCpTime.SelectedValue & "',EndTime='" & ddlEndCpTime.SelectedValue & "',RMcMemberID = " & IIf(ds.Tables(0).Rows(i)("RMcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RMcMemberID")) & ",PronMemberID=" & IIf(ds.Tables(0).Rows(i)("PronMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("PronMemberID")) & ",CJMemberID=" & IIf(ds.Tables(0).Rows(i)("CJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("CJMemberID")) & ",AJMemberID=" & IIf(ds.Tables(0).Rows(i)("AJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AJMemberID")) & ","
                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & " LTJudMemberID=" & IIf(ds.Tables(0).Rows(i)("LTJudMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudMemberID")) & ",DictHMemberID=" & IIf(ds.Tables(0).Rows(i)("DictHMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictHMemberID")) & ",GradMemberID=" & IIf(ds.Tables(0).Rows(i)("GradMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID")) & ",ProcMemberID=" & IIf(ds.Tables(0).Rows(i)("ProcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ProcMemberID")) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & " Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

                Next
                SQLContTeamCpEval = "Select Count(*) From RoomGuideSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""      'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") 'Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
                If i <= count Then 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLContTeamCpEval) > 0 Then
                        Dim ds3 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select RoomGuideID From RoomGuideSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductID=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "") ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & ")

                        For j As Integer = 0 To ds3.Tables(0).Rows.Count - 1
                            RoomGuideID = RoomGuideID & ds3.Tables(0).Rows(j)("RoomGuideID") & ","
                        Next
                        RoomGuideID = RoomGuideID.Trim(",")
                        SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Delete From RoomGuideSchedule Where RoomGuideID in(" & RoomGuideID & ")")

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpInsert)
                        lblAddUPdate.Text = "Copied Successfully(Updated)"
                    Else
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpInsert)
                        lblAddUPdate.Text = "Copied Successfully(Added)"
                    End If
                ElseIf i > count Then
                    lblErr.Text = "RoomSchedule data  has fewer records for the Contest"
                Else
                    lblErr.Text = "RoomSchedule data is not yet added for the Contest"
                End If
            End If
            LoadDGRoomSchedule()
            ' btnAddUpdate.Text = "Add"
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        'lblAddUPdate.Text = ""
        ClearDropDowns()
        GetProductGroup(ddlProductGroup)
    End Sub
End Class
