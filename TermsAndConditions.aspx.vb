Imports System.Text


Namespace VRegistration

Partial Class TermsAndConditions

    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
            Dim strScript As String = "javascript:window.parent.location='http://www.northsouth.org';"
            btnDeclineLinkbutton.Attributes.Add("onclick", strScript)
            If Not IsPostBack Then
                If Session("EventId") = 9 Then
                    Table1.visible = False
                    tblFundraising.visible = True
                End If
            End If
    End Sub
        Private Sub btnDeclineLinkbutton_Click_old(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeclineLinkbutton.Click
            If Application("ContestType") = 1 Then
                Dim redirectURL As String

                If Request.ServerVariables("Server_Name") <> "localhost" Then
                    redirectURL = "/app6/UserFunctions.aspx"
                Else
                    redirectURL = "/VRegistration/UserFunctions.aspx"
                End If
                Response.Redirect(redirectURL)
            End If

        End Sub
        Private Sub btnDeclineLinkbutton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeclineLinkbutton.Click
            ' If Session("EventID") = 1 Then
            Dim redirectURL As String
            If Session("EventId") = 3 Then


                If Request.ServerVariables("Server_Name") <> "localhost" Then
                    redirectURL = "/app8/WkSHopRegistration.aspx"
                Else
                    redirectURL = "/VRegistration/WkSHopRegistration.aspx"
                End If
            ElseIf Session("EventId") = 9 Then
                If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
                    redirectURL = "FundRReg.aspx"
                Else
                    redirectURL = "FundRReg.aspx?id=1"
                End If
            ElseIf Session("EventId") = 12 Then
                redirectURL = "walkathon_custom.aspx?id=" & Session("WalkMarathonID")
            Else

                If Request.ServerVariables("Server_Name") <> "localhost" Then
                    redirectURL = "/app8/ContestantRegistration.aspx"
                Else
                    redirectURL = "/VRegistration/ContestantRegistration.aspx"
                End If
            End If
            Response.Redirect(redirectURL)
            'End If

        End Sub

        Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
            If Session("EventId") = 9 Then
                ' Session("DONATIONFOR") = ""
                'Session("Donation") = 0.0
                Session("DoPay") = "TermsAndCondition"
                Response.Redirect("reg_Pay.aspx")
            Else
                Response.Redirect("reg_Donate.aspx")
            End If

        End Sub
End Class

End Namespace

